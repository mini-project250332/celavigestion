<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require APPPATH . "third_party/MX/Controller.php";
require realpath('../vendor/autoload.php');


class ADM_Controller extends MX_Controller
{
	private $_css_libraries = array(
		"fonts/font-awesome/css/all.min.css",
		"fonts/bootstrap-icons/bootstrap-icons.css",
		"lib/bootstrap-5.0.2/css/bootstrap.min.css",
		"lib/jquery-toggles/toggles-full.css",
		"lib/jquery-ui/jquery-ui.min.css",
		"lib/select2/css/select2.css",
		"lib/footer-dialog/footerdialog.css",
		"lib/toastr/toastr.min.css",
		"lib/flatpickr/flatpickr.min.css",
		"lib/choices.js/choices.min.css",
		"lib/choices.js/base.min.css",
		"lib/jquery-ui/jquery-ui.min.css",
		"lib/overlayscrollbars/OverlayScrollbars.min.css",
		"lib/prism/prism-okaidia.css",
		"lib/Ionicons/css/ionicons.min.css",
		"lib/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css",
		"lib/toastr/toastr.min.css",
		"lib/bootstrap-multiselect/css/bootstrap-multiselect.css",
		"css/theme.css",
	);

	private $_js_libraries = array(
		// "js/jquery-resizable.js",
		"lib/jquery/jquery.min.js",
		"lib/jquery-ui/jquery-ui.min.js",
		"lib/moment/moment.min.js",
		"lib/font-awesome/js/all.min.js",
		"js/popper.min.js",
		//"lib/bootstrap-5.0.2/js/bootstrap.min.js",
		"lib/bootstrap-5.0.2/js/bootstrap.bundle.min.js",
		"lib/select2/js/select2.full.min.js",
		"lib/toastr/toastr.min.js",
		"lib/stacked-menu/stacked-menu.min.js",
		"lib/flatpickr/flatpickr.js",
		"lib/flatpickr/l10n/fr.js",
		"lib/choices.js/choices.min.js",
		"lib/cryptojs/cryptojs-aes.min.js",
		"lib/cryptojs/cryptojs-aes-format.js",
		"lib/footer-dialog/footerdialog.js",
		"lib/jquery-toggles/toggles.min.js",
		"lib/notiflix/notiflix-aio-3.2.5.min.js",

		"lib/overlayscrollbars/OverlayScrollbars.min.js",
		"lib/pace/pace.min.js",
		"lib/list.js/list.min.js",
		"lib/prism/prism.js",
		"lib/anchorjs/anchor.min.js",
		"lib/is_js/is.min.js",
		"lib/jquery-loading-overlay/loadingoverlay.min.js",

		"lib/plupload/plupload.full.min.js",
		"lib/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js",
		"lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js",
		"lib/plupload/langs/fr.js",

		"lib/bootstrap-multiselect/js/bootstrap-multiselect.min.js",

		"js/globals_fn.js",
	);

	private $_css_style = array(
		"css/theme.css",
		"css/header.css",
	);

	private $_js_requerie = array(
		"js/app.js",
	);

	private $_template = "";

	protected $_utilisateur_courant = NULL;
	protected $_data = array();
	protected $_css_personnaliser = array();
	protected $_script = array();
	protected $_view_directory = "";
	protected $_const_js = array();

	protected $_access_list = [
		// Acte controller
		["method" => "cruActe", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeDocActe", "read" => "*", "write" => [1, 2, 3]],
		["method" => "UpdateDocumentActe", "read" => "*", "write" => [1, 2, 3]],
		["method" => "UpdateImmo", "read" => "*", "write" => [1, 2, 3]],
		["method" => "UpdateDocumentAgencement", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateDocAgencementTraitement", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeDocMobilier", "read" => "*", "write" => [1, 2, 3]],

		// Bail controller
		["method" => "AddBail", "read" => "*", "write" => [1, 2, 3]],
		["method" => "AddIndexationLoyer", "read" => "*", "write" => [1, 2, 3]],
		["method" => "AddFranciseLoyer", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeFranchise", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeIndexation", "read" => "*", "write" => [1, 2, 3]],
		["method" => "UpdateLoyerReference", "read" => "*", "write" => [1, 2, 3]],
		["method" => "validationBail", "read" => "*", "write" => [1, 2]],
		["method" => "cloturerBail", "read" => "*", "write" => [1, 2]],

		// Setting - controller
		// Gestionnaire
		["method" => "cruGestionnaire", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateInfoGestionnaire", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeGestionnaire", "read" => "*", "write" => [1]],
		["method" => "cruContact", "read" => "*", "write" => [1, 3]],
		["method" => "removeContact", "read" => "*", "write" => [1]],

		// Gestionnaire Users
		["method" => "cruUser", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeUser", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateMailParam", "read" => "*", "write" => [1, 2, 3]],

		// Indexation
		["method" => "AjouterIndice", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeIndice", "read" => "*", "write" => [1, 2, 3]],
		["method" => "cruValeur", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeValeurIndice", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateIndice", "read" => "*", "write" => [1, 2, 3]],

		// Origine client
		["method" => "cruOrigineClient", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeOrigineClient", "read" => "*", "write" => [1, 2, 3]],

		// Origine demande
		["method" => "cruOrigineDemande", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeOrigineDemande", "read" => "*", "write" => [1, 2, 3]],

		// Param Mail
		["method" => "updateMailParam", "read" => "*", "write" => [1, 2, 3]],

		// Produit
		["method" => "cruProduit", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeProduit", "read" => "*", "write" => [1, 2, 3]],

		// Programme
		["method" => "cruProgram", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeProgramme", "read" => "*", "write" => [1]],

		// Taches controlleur
		["method" => "cruTache", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeTache", "read" => "*", "write" => [1, 2, 3]],
		["method" => "valideTache", "read" => "*", "write" => [1, 2, 3]],

		// Loyer controlleur
		["method" => "AddFactureLoyer", "read" => "*", "write" => [1, 2, 3]],
		["method" => "AddEncaissement", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeEncaissement", "read" => "*", "write" => [1, 2, 3]],
		["method" => "DeleteFactureLoyer", "read" => "*", "write" => [1, 2, 3]],

		// Charges controlleur
		["method" => "addCharge", "read" => "*", "write" => [1, 2, 3]],
		["method" => "deleteCharges", "read" => "*", "write" => [1, 2, 3]],
		["method" => "deleteDocCharge", "read" => "*", "write" => [1, 2, 3]],
		["method" => "UpdateDocumentCharge", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateDocChargeTraitement", "read" => "*", "write" => [1, 2, 3]],

		// Prospect controlleur
		["method" => "updateInfoProspect", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateParcoursProspect", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeProspect", "read" => "*", "write" => [1, 2, 3]],
		["method" => "cruLot", "read" => "*", "write" => [1, 2, 3]],
		["method" => "updateLot", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeLot", "read" => "*", "write" => [1, 2, 3]],
		["method" => "AddMandat", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeMandat", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeDocMandat", "read" => "*", "write" => [1, 2, 3]],
		["method" => "cruCom", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeCom", "read" => "*", "write" => [1, 2, 3]],
		["method" => "cruTache", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeTache", "read" => "*", "write" => [1, 2, 3]],
		["method" => "valideTache", "read" => "*", "write" => [1, 2, 3]],
		["method" => "activerProspect", "read" => "*", "write" => [1, 2, 3]],
		["method" => "removeDoc", "read" => "*", "write" => [1, 2, 3]],
		["method" => "transformerClient", "read" => "*", "write" => [1, 2, 3]],

		// Propriétaire
		["method" => "clientAdd", "read" => "*", "write" => [1, 3]],
		["method" => "removeClient", "read" => "*", "write" => [1]],
	];

	public function __construct()
	{
		parent::__construct();
		$this->_utilisateur_courant = $this->session->userdata('utilisateur');
		$this->load_data('utilisateur_courant', $this->session->userdata('utilisateur'));

		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('cookie');
		$this->load->helper('file');
		$this->lang->load('french', 'french');
		$this->load->helper('utils');
	}

	function _remap($method, $params)
	{
		$user = $this->session->userdata('session_utilisateur');
		$role_value = isset($user) ? $user["rol_util_valeur"] : 0;
		$access_user = $role_value;
		$forbidden = false;

		foreach ($this->_access_list as $value) {
			if ($value['method'] == $method) {
				if ($value['read'] != "*" && !in_array($access_user, $value['read'], true)) {
					header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
					$forbidden = true;
				}

				if ($value['write'] != "*" && !in_array($access_user, $value['write'], false)) {
					header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
					$forbidden = true;
				}
			}
		}

		if ($forbidden) {
			$retour = array(
				'status' => 403,
				'message' => "Vous n'avez pas accès à cette fonction",
			);
			echo json_encode($retour);
		} else {
			$this->$method($params);
		}
	}

	/* ---------------------------------- */
	private function load_const_js()
	{
		if (count($this->_const_js)) {
			foreach ($this->_const_js as $const_index => $const_value) {
				$this->_data["const_js"][$const_index] = $const_value;
			}
		}
		$this->_data["const_js"] += [
			'data_session' => 'session_utilisateur',
			'base_url' => base_url(),
			'code_crypt' => CODE_CRYPT,
			'access_write_gestion' => ACCESS_WRITE_GESTION,
			'access_read_setting' => ACCESS_READ_SETTING
		];
	}

	// librarie
	private function load_css_libraries()
	{
		if (count($this->_css_libraries)) {
			foreach ($this->_css_libraries as $lib) {
				$this->_data["css_libraries"][] = $lib;
			}
		}
	}
	private function load_js_libraries()
	{
		if (count($this->_js_libraries)) {
			foreach ($this->_js_libraries as $lib) {
				$this->_data["js_libraries"][] = $lib;
			}
		}
	}
	/* style */
	private function load_css_style()
	{
		if (count($this->_css_style)) {
			foreach ($this->_css_style as $style) {
				$this->_data["css_style"][] = $style;
			}
		}
	}
	// load style personaliser
	protected function load_css_personnaliser()
	{
		if (count($this->_css_personnaliser)) {
			foreach ($this->_css_personnaliser as $style_perso) {
				$this->_data["css_personnaliser"][] = $style_perso;
			}
		}
	}
	// load javascript require
	private function load_js_requerie()
	{
		if (count($this->_js_requerie)) {
			foreach ($this->_js_requerie as $script) {
				$this->_data["js_requerie"][] = $script;
			}
		}
	}
	// load javascript personaliser
	protected function load_script()
	{
		if (count($this->_script)) {
			foreach ($this->_script as $js_p) {
				$this->_data["js_script"][] = $js_p;
			}
		}
	}

	// initialiser data in _data
	public function load_data($var, $data)
	{
		$this->_data[$var] = $data;
	}

	/**
	 * Retourner les vues - de type ajax
	 * @return void
	 */
	public function render_ajax($view)
	{
		$config = HTMLPurifier_Config::createDefault();
		$purifier = new HTMLPurifier($config);
		$this->load->view($purifier->purify($view), $this->_data);
	}

	/**
	 * Retourner les vues - de type page
	 * @return void
	 */

	public function render($page)
	{
		$config = HTMLPurifier_Config::createDefault();
		$purifier = new HTMLPurifier($config);

		$this->load->model("TravailTemps_m");
		$data = array();

		$params = array(
			'columns' => array('SUM(travail_duree) as total'),
			'clause' => array('travail_date' => date('Y-m-d'))
		);

		$request = $this->TravailTemps_m->get($params);

		if (!empty($request)) {
			foreach ($request as $key => $value) {
				$travail_effectue = intval($value->total);
				$hours = intdiv($travail_effectue, 60) . 'H' . ($travail_effectue % 60);
			}
		}

		$_SESSION['tempsEffectue'] = $hours;

		$this->_template = "com/_page";
		$this->_data["page"] = $this->_view_directory . "/" . $page;
		$this->load_css_libraries();
		$this->load_js_libraries();
		$this->load_const_js();
		$this->load_css_style();
		$this->load_css_personnaliser();
		$this->load_js_requerie();
		$this->load_script();
		$this->load->view($purifier->purify($this->_template), $this->_data);
	}

	public function renderComponant($view, $data = NULL)
	{
		$config = HTMLPurifier_Config::createDefault();
		$purifier = new HTMLPurifier($config);
		$this->load->view($purifier->purify($view), $data);
	}

	public function page_ajax($view)
	{
		$this->load->view($view, $this->_data);
	}

	/**** ****/

	public function login_page($page)
	{
		$config = HTMLPurifier_Config::createDefault();
		$purifier = new HTMLPurifier($config);
		$page_login = $this->_view_directory . "/" . $page;
		$this->load_css_libraries();
		$this->load_js_libraries();
		$this->load_const_js();
		$this->load_css_style();
		$this->load_css_personnaliser();
		$this->load_js_requerie();
		$this->load_script();
		$this->load->view($purifier->purify($page_login), $this->_data);
	}

	/* ------------------- */

	function control_data($input)
	{
		$data = array();
		foreach ($input as $key => $value_key) {
			foreach ($value_key as $key_key => $value) {
				$data += [
					$key => [
						"type" => (isset($value_key["type_data"])) ? $value_key["type_data"] : ((isset($value_key["type"])) ? $value_key["type"] : "text"),
						"required" => (isset($value_key["required"]) && $value_key["required"] == "true") ? verify_value((isset($value_key["value"]) ? $value_key["value"] : '')) : true,
						"format" => verify_format((isset($value_key["type_data"])) ? $value_key["type_data"] : ((isset($value_key["type"])) ? $value_key["type"] : "text"), (isset($value_key["value"]) ? $value_key["value"] : ''), (isset($value_key["format_required"])) ? $value_key["format_required"] : null),
						"length" => verify_length($value, ((isset($value_key["max_length"])) ? $value_key["max_length"] : null), ((isset($value_key["min_length"])) ? $value_key["min_length"] : null))
					]
				];
			}
		}
		$control = array();
		if (isset($data)) {
			foreach ($data as $key_d => $value_d) {
				foreach ($value_d as $key_vd => $value_vd) {
					if ($value_vd == false) {
						$control += [
							$key_d => [
								$key_vd => $this->message_control_champ($key_vd)
							]
						];
					}
				}
			}
		}
		$output = array(
			'return' => ((empty($control)) ? true : false),
			'information' => ((!empty($control)) ? $control : '')
		);
		return $output;
	}

	function message_control_champ($key)
	{
		$output = '';
		switch ($key) {
			case 'required':
				$output = "Ce champ ne doit pas être vide.";
				break;
			case 'format':
				$output = "L'information que vous avez entrée est du format incorrect.";
				break;
			case 'length':
				$output = "Les nombres des caractères sont incorrectes.";
				break;
		}
		return $output;
	}

	/***** get liste utilisateur *****/
	function getListUser()
	{
		$this->load->model('Utilisateur_m', 'users');
		$params = array(
			'clause' => array('util_etat' => 1),
			'columns' => array(
				'util_id as id', 'util_nom as nom', 'util_prenom as prenom', 'util_mail as email',
				'c_typeutilisateur.tutil_libelle as type_user', 'c_role_utilisateur.rol_util_libelle as rol_util_libelle', 'c_role_utilisateur.rol_util_id as rol_util_id'

			),
			'order_by_columns' => "util_nom ASC",
			'join' => array(
				'c_typeutilisateur' => 'c_typeutilisateur.tutil_id = c_utilisateur.tutil_id',
				'c_role_utilisateur' => 'c_role_utilisateur.rol_util_id  = c_utilisateur.rol_util_id',
			),
		);
		$request = $this->users->get($params);
		return $request;
	}

	function getOriginClient()
	{
		$this->load->model('OrigineClient_m', 'origin_cli');
		$params = array(
			//'clause' => array('util_etat' => 1),
			'columns' => array('origine_cli_id as id', 'origine_cli_libelle as libelle'),
			//'order_by_columns' => "origine_cli_libelle ASC",
		);
		$request = $this->origin_cli->get($params);
		return $request;
	}

	function getOriginDemande()
	{
		$this->load->model('OrigineDemande_m', 'origin_dem');
		$params = array(
			//'clause' => array('util_etat' => 1),
			'columns' => array('origine_dem_id as id', 'origine_dem_libelle as libelle'),
			//'order_by_columns' => "origine_dem_libelle ASC",
		);
		$request = $this->origin_dem->get($params);
		return $request;
	}

	public function getlisteProduit()
	{
		$this->load->model('Produit_m');
		$params = array(
			'columns' => array('produit_id', 'produit_libelle'),
			'clause' => array('produit_etat' => 1)
		);
		$request = $this->Produit_m->get($params);
		return $request;
	}

	public function getlisteProduitClient()
	{
		$this->load->model('ProduitClient_m');
		$params = array(
			'clause' => array('produitcli_etat' => 1),
			'columns' => array('produitcli_id', 'produitcli_libelle')
		);
		$request = $this->ProduitClient_m->get($params);
		return $request;
	}

	public function getlisteParcoursVente()
	{
		$this->load->model('ParcoursVente_m');
		$params = array(
			'columns' => array('vente_id', 'vente_libelle')
		);
		$request = $this->ParcoursVente_m->get($params);
		return $request;
	}

	public function getlisteTypeCommunication()
	{
		$this->load->model('TypeCommunication_m');
		$params = array(
			'columns' => array('type_comprospect_id', 'type_comprospect_libelle')
		);
		$request = $this->TypeCommunication_m->get($params);
		return $request;
	}

	public function getTypeLot()
	{

		$this->load->model('TypeLotProspect_m');
		$params = array(
			'columns' => array('typelot_id', 'typelot_libelle')
		);
		$request = $this->TypeLotProspect_m->get($params);
		return $request;
	}

	public function getData_lotClient($cliLot_id)
	{

		$this->load->model('ClientLot_m');
		$params = array(
			'clause' => array(
				'cliLot_id' => $cliLot_id,
			),
			'method' => 'row',
			'join' => array(
				'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
				'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
				'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
				'c_syndicat' => 'c_syndicat.syndicat_id = c_lot_client.syndicat_id',
			),
			'join_orientation' => 'left'
		);
		$request = $this->ClientLot_m->get($params);
		return $request;
	}

	public function getData_lotProspect($lot_id)
	{
		$this->load->model('Lot_m');
		$params = array(
			'clause' => array(
				'lot_id' => $lot_id,
			),
			'method' => 'row',
			'join' => array(
				'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_prospect.gestionnaire_id',
				'c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id',
			),
		);
		$request = $this->Lot_m->get($params);
		return $request;
	}

	public function getTypologieLot()
	{
		$this->load->model('Typologie_m');
		$params = array(
			'columns' => array('typologielot_id', 'typologielot_libelle')
		);
		$request = $this->Typologie_m->get($params);
		return $request;
	}

	public function getListGestionnaire()
	{
		$this->load->model('Gestionnaire_m');
		$params = array(
			'clause' => array('gestionnaire_etat' => 1),
			'columns' => array('gestionnaire_id', 'gestionnaire_nom'),
			'order_by_columns' => 'gestionnaire_nom ASC'
		);
		$request = $this->Gestionnaire_m->get($params);
		return $request;
	}

	public function getListeUtilisateur()
	{
		$this->load->model('Utilisateur_m');
		$params = array(
			'clause' => array('util_etat' => 1),
			'columns' => array('util_id', 'util_prenom', 'util_nom'),
			'order_by_columns' => 'util_prenom ASC'
		);
		$request = $this->Utilisateur_m->get($params);
		return $request;
	}

	public function getListSyndicat()
	{
		$this->load->model('Syndicat_m');
		$params = array(
			'clause' => array('syndicat_etat' => 1),
			'columns' => array('syndicat_id', 'syndicat_nom'),
			'order_by_columns' => 'syndicat_nom ASC'
		);
		$request = $this->Syndicat_m->get($params);
		return $request;
	}

	public function getListPays()
	{
		$this->load->model('Pays_m');
		$params = array(
			'columns' => array('paysmonde_id', 'paysmonde_libelle', 'paysmonde_indicatif', 'paysmonde_libellecourt')
		);
		$request = $this->Pays_m->get($params);
		return $request;
	}

	public function getListPaysMonde()
	{
		$this->load->model('PaysMonde_m');
		$params = array(
			'columns' => array('pays_id', 'pays_libelle', 'pays_indicatif', 'pays_libellecourt')
		);
		$request = $this->PaysMonde_m->get($params);
		return $request;
	}

	public function getNationalite()
	{
		$this->load->model('Nationalite_m');
		$params = array(
			'order_by_columns' => "libelle ASC",
		);
		$request = $this->Nationalite_m->get($params);
		return $request;
	}

	function countAllData($model, $clause)
	{
		$this->load->model($model, 'model');
		$params = array(
			'clause' => $clause,
			'columns' => array('COUNT(*) as allData'),
		);
		$request = $this->model->get($params);
		return intval($request[0]->allData);
	}

	public function getData_client($client_id)
	{
		$this->load->model('Client_m');
		$params = array(
			'clause' => array('client_id' => $client_id),
			'method' => 'row',
			'columns' => array('client_nom', 'client_prenom', 'client_id')
		);
		$request = $this->Client_m->get($params);
		return $request;
	}

	public function getData_prospect($prospect_id)
	{
		$this->load->model('Prospect_m');
		$params = array(
			'clause' => array('prospect_id' => $prospect_id),
			'method' => 'row',
			'columns' => array('prospect_nom', 'prospect_prenom', 'prospect_id')
		);
		$request = $this->Prospect_m->get($params);
		return $request;
	}

	function getClient($client_id)
	{
		$this->load->model('Client_m');
		$param = array(
			'clause' => array('client_id' => $client_id),
			'method' => 'row',
			'columns' => array('client_nom', 'client_prenom', 'client_entreprise'),
		);
		$request = $this->Client_m->get($param);
		return (!empty($request)) ? $request->client_nom . " " . $request->client_prenom . " " . $request->client_entreprise : '';
	}

	public function getTypeMandat()
	{
		$this->load->model('TypeMandat_m');
		$params = array(
			'columns' => array('type_mandat_id', 'type_mandat_libelle')
		);
		$request = $this->TypeMandat_m->get($params);
		return $request;
	}

	public function getRibCelavi()
	{
		$this->load->model('Rib_m');
		$request = $this->Rib_m->get(array(
			'method' => 'row'
		));
		return $request;
	}

	// Deconnection 

	function logout()
	{
		$this->session->sess_destroy();
		return redirect(__CLASS__);
	}
}
