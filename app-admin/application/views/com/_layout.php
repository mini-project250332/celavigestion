<?php $this->load->view("com/_headerPage"); ?>
<?php if (isset($page))
    $this->load->view($page) ?>

<div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modalLg" style=" height:540px !important;">

        </div>
    </div>
</div>

<div class="modal fade" id="myModalXl" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" id="modalXl">

        </div>
    </div>
</div>

<div class="modal fade" id="modalForm-Encaissement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" id="modal-main-contentEncaissement">

        </div>
    </div>
</div>