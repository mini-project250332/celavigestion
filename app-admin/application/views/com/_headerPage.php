<!-- data -->
<div class="d-none">

</div>

<header class="header-app">
	<div class="left-header">
		<!-- <div class="div-logo">
			<div class="sm-logo-img">
				<img src="<?php echo base_url('../assets/img/cest-la-vie_logo_icone.png'); ?>">
			</div>
		</div> -->
		<div class="leftcontainer-header">
			<div class="libelle-header">
				<h1>
					<img src="<?php echo base_url('../assets/img/celavi.jpg'); ?>">
					<span></span>
				</h1>
			</div>
			<div class="menu-header">
				<ul class="menu-web part-left">
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "taches") ? 'active' : ''; ?>">
						<a class="" href="<?php echo base_url('Taches/Taches'); ?>">
							<span>Tâches </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "prospects") ? 'active' : ''; ?>">
						<a class="" id="proprietaire-menu-prospect" href="<?php echo base_url('Prospections'); ?>">
							<span>Prospects </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "clients") ? 'active' : ''; ?>">
						<a class="" id="proprietaire-menu-web" href="<?php echo base_url('Clients'); ?>">
							<span> Propriétaires </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "dossiers") ? 'active' : ''; ?>">
						<a class="" href="<?php echo base_url('Dossiers/Dossiers'); ?>">
							<span> Dossiers </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "lots") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('Lots/Lots'); ?>">
							<span> Lots </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "mandats") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('Mandats/Mandats'); ?>">
							<span> Mandats </span>
						</a>
					</li>
					<li class="menu-item exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "expertcomptable") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('ExpertComptable/ExpertComptable'); ?>">
							<span> Experts Comptables </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable btn_limit_access <?php echo (isset($menu_principale) && $menu_principale == "facturation") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('Facturation/Facturation'); ?>">
							<span> Facturation </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable btn_limit_access <?php echo (isset($menu_principale) && $menu_principale == "exportcomptable") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('ExportComptable/ExportComptable'); ?>">
							<span> Comptabilité </span>
						</a>
					</li>
					<li class="menu-item no_exper_comptable <?php echo (isset($menu_principale) && $menu_principale == "communication") ? 'active' : ''; ?>">
						<a classs="" href="<?php echo base_url('Communications/Communications'); ?>">
							<span> Communication </span>
						</a>
					</li>
				</ul>

				<ul class="menu-web part-right no_exper_comptable ">
					<div class="mt-1 contain_filter_global only_visuel_gestion">
						<div class="">
							<div class="input-group">
								<input class="form-control border-end-0 border m-0" type="search" value="" placeholder="nom, prénom, entreprise" id="filtre_global">
								<span class="input-group-append">
									<button class="btn btn-outline-secondary bg-white border-start-0 border-bottom-0 border ms-n5" type="button" style="height : 28px;">
										<i class="fa fa-search fs--1"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<a type="button" class="btn btn-primary only_visuel_gestion <?php echo (isset($menu_principale) && $menu_principale == "") ? 'active' : ''; ?>" href="<?php echo base_url('SaisieTemps/SaisieTemps'); ?>" style="margin-right: 10px;">
						<span class="badge bg-secondary total">
							<?= ($_SESSION['tempsEffectue'] == "0H0") ? "0H00" : $_SESSION['tempsEffectue'] ?>
						</span> effectuée(s)
					</a>
					<button class="btn btn-sm btn-outline-primary only_visuel_gestion" id="btnSaisieTempsGeneral">
						<i class="fas fa-clock"></i>
					</button>
					<li class=" not_access_read icon-menu only_visuel_gestion btn_limit_access ms-3 <?php echo (isset($menu_principale) && $menu_principale == "setting") ? 'active' : ''; ?>">
						<a href="<?php echo base_url('Setting'); ?>">
							<span>
								<i class="fas fa-cog"></i>
							</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="right-header">
		<div class="icon-profil">
			<a id="toggle_dropdownCompte" class="" href="javascript:void(0);">
				<img src="<?php echo base_url('assets/images/user.png'); ?>" alt="Tsilavina">
			</a>
			<div class="dropdown-compte">
				<div class="div-content-compte">
					<div class="div-compte">
						<div class="photo-compte-user">
							<img src="<?php echo base_url('assets/images/user.png'); ?>">
							<div class="btn-change-photo">

							</div>
						</div>
						<div class="fs--1">
							<?= $_SESSION['session_utilisateur']['util_nom'] . ' ' . $_SESSION['session_utilisateur']['util_prenom'] ?>
						</div>
						<a class="btn btn-sm btn-outline-primary w-100">
							Gérer votre compte
						</a>
					</div>
					<div>
						<a href="<?php echo base_url('Auth/logout'); ?>" class="btn btn-sm btn-primary w-100">
							Se déconnecter
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<script>
	access_right("*", [1, 4]);
</script>