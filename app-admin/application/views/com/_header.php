<?php
if (!$this->session->has_userdata("session_utilisateur")) {
    $token = ($this->session->has_userdata("token")) ? $this->session->userdata('token') : NULL;
    $this->session->sess_destroy();
    redirect('Auth/logout/' . $token);
}
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta name="description" content="<?php echo description; ?>">
    <meta name="author" content="<?php echo author; ?>">
    <title><?php echo title; ?></title>
    <!-- Meta -->

    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />
    <!-- Style par default com/_css -->
    <?php $this->load->view("com/_css"); ?>

    <!-- Style personnaliser -->
    <?php if (isset($css_personnaliser)) {
        foreach ($css_personnaliser as $css_p) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_p . VERSION_SCRIPT) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Javascript constant application -->
    <script type="text/javascript">
        const const_app = {
            <?php
            if (isset($const_js)) {
                foreach ($const_js as $js_n => $val_n) {
                    echo $js_n . " : '" . $val_n . "',\n";
                }
            }
            ?>
        }
    </script>

    <!-- Lirairie JS par default "require" -->
    <?php
    if (isset($js_libraries)) {
        foreach ($js_libraries as $js_lib) {
            echo '<script src="' . base_url('../assets/' . $js_lib . VERSION_SCRIPT) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <?php
    if (isset($js_requerie)) {
        foreach ($js_requerie as $js_r) {
            echo '<script src="' . base_url('assets/' . $js_r . VERSION_SCRIPT) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <style type="text/css">
        .pagination-list-js>.pagination li {
            display: inline-block !important;
            color: #0b1727;
            border: 1px solid #edf2f9;
        }

        .pagination-list-js>.pagination li a {
            color: #0b1727;
            padding: 0.25rem 1rem;
            font-size: 0.9rem;
        }

        .pagination-list-js>.pagination li.active {
            border-color: #2c7be5;
            background-color: #2c7be5;
        }

        .pagination-list-js>.pagination li.active a {
            color: #fff;
        }

        .checkbox_check_identite_mandat, .checkbox_check_identite_acte{
            border-color: #e63757;
            background-color: #e798a6;
        }

        .checkbox_check_identite_mandat:checked, .checkbox_check_identite_acte:checked{
            border-color: #00d27a;
            background-color: #00d27a;
        }
    </style>

    <script type="text/javascript">
        var text_sessionExpirer = "<?= $this->lang->line('session_expirer'); ?>";
    </script>
</head>

<body>