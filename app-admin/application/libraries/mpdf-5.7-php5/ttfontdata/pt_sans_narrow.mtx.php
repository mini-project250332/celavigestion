<?php
$name='PTSans-Narrow';
$type='TTF';
$desc=array (
  'Ascent' => 1018,
  'Descent' => -276,
  'CapHeight' => 700,
  'Flags' => 4,
  'FontBBox' => '[-394 -245 905 993]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 750,
);
$up=-75;
$ut=50;
$ttffile='/home/www/fw/application/libraries/ttfonts/PTN57F.ttf';
$TTCfontID='0';
$originalsize=383848;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='pt_sans_narrow';
$panose=' 8 2 2 b 5 6 2 2 3 2 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>