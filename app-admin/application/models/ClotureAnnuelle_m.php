<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ClotureAnnuelle_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_cloture_annuelle";
    protected $_primary_key = "cloture_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function getListclotureAnnuelle($dossier_id)
    {
        $query = $this->db->query(
            "SELECT c_cloture_annuelle.*,
            t1.util_nom as util_ask_nom,
            t1.util_prenom as util_ask_prenom,
            t2.util_nom as util_validated_nom,
            t2.util_prenom as util_validated_prenom,
            t3.util_nom as util_exp_nom,
            t3.util_prenom as util_exp_prenom
            FROM c_cloture_annuelle
            LEFT JOIN c_utilisateur t1 ON t1.util_id = c_cloture_annuelle.util_demande_validation 
            LEFT JOIN c_utilisateur t2 ON t2.util_id = c_cloture_annuelle.util_validation 
            LEFT JOIN c_utilisateur t3 ON t3.util_id = c_cloture_annuelle.util_expert_comptable 
            WHERE c_cloture_annuelle.dossier_id = $dossier_id
            ORDER BY  c_cloture_annuelle.annee DESC"
        );
        $result = $query->result();
        return $result;
    }

    public function getListDossierComptable($annee,$clause,$etat){
        $query = $this->db->query(
            "SELECT c_cloture_annuelle.*, 
            t1.util_nom as util_exp_nom, t1.util_prenom as util_exp_prenom, 
            t2.util_nom as util_nom, t2.util_prenom as util_prenom, c_dossier.dossier_id,client_nom,client_prenom,cloture_jour_fin,cloture_mois_fin,c_client.client_id,
            COUNT(c_lot_client.dossier_id) as nombre_de_lots
            FROM c_cloture_annuelle 
            LEFT JOIN c_dossier ON c_dossier.dossier_id = c_cloture_annuelle.dossier_id 
            LEFT JOIN c_client ON c_client.client_id = c_dossier.client_id 
            LEFT JOIN c_lot_client ON c_dossier.dossier_id = c_lot_client.dossier_id 
            LEFT JOIN c_utilisateur t1 ON t1.util_id = c_cloture_annuelle.util_expert_comptable
            LEFT JOIN c_utilisateur t2 ON t2.util_id = c_dossier.util_id
            WHERE c_cloture_annuelle.annee = $annee AND c_cloture_annuelle.util_expert_comptable = $clause AND c_cloture_annuelle.etat = $etat 
            GROUP BY c_dossier.dossier_id"
        );
        $result = $query->result();
        return $result;
    }

    public function getListSuiviComptable($clause,$etat){
        $query = $this->db->query(
            "SELECT c_cloture_annuelle.*, 
            t1.util_nom as util_exp_nom, t1.util_prenom as util_exp_prenom, 
            t2.util_nom as util_nom, t2.util_prenom as util_prenom, c_dossier.dossier_id,client_nom,client_prenom,cloture_jour_fin,cloture_mois_fin,c_client.client_id,
            date_validation
            FROM c_cloture_annuelle 
            LEFT JOIN c_dossier ON c_dossier.dossier_id = c_cloture_annuelle.dossier_id 
            LEFT JOIN c_client ON c_client.client_id = c_dossier.client_id 
            LEFT JOIN c_lot_client ON c_dossier.dossier_id = c_lot_client.dossier_id 
            LEFT JOIN c_utilisateur t1 ON t1.util_id = c_cloture_annuelle.util_expert_comptable
            LEFT JOIN c_utilisateur t2 ON t2.util_id = c_dossier.util_id
            LEFT JOIN c_document_2031 ON c_dossier.dossier_id = c_document_2031.dossier_id
            WHERE c_cloture_annuelle.util_expert_comptable = $clause AND c_cloture_annuelle.etat = $etat 
            GROUP BY c_dossier.dossier_id"
        );
        $result = $query->result();
        return $result;
    }
}
