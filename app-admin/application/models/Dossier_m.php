<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dossier_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_dossier";
    protected $_primary_key = "dossier_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function get_data_dossier($dossier_id)
    {
        $res = $this->db->select('*')
            ->from($this->_table)
            ->join('c_forme_juridique', 'c_forme_juridique.c_forme_juridique_id = c_dossier.c_forme_juridique_id', 'left')
            ->join('c_sie', 'c_sie.sie_id = c_dossier.sie_id', 'left')
            ->join('c_cgp', 'c_cgp.cgp_id = c_dossier.cgp_id', 'left')
            ->join('c_utilisateur', 'c_utilisateur.util_id = c_dossier.util_id', 'left')
            ->where('dossier_id', $dossier_id)
            ->get()
            ->row();
        return $res;
    }
}