<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Mandat_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_mandat";
        protected $_primary_key = "mandat_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        }

        public function getMandatProspect(){

            $res = $this->db->select("c_prospect.prospect_id , GROUP_CONCAT(mandat_montant) as mandat_montant")
            ->from($this->_table)
            ->join('c_prospect','c_prospect.prospect_id  = c_mandat.prospect_id','left')    
            ->where('mandat_etat = 1')    
            ->group_by('c_mandat.prospect_id')
            ->get()
            ->result();
        return $res;

        }        
        
    }