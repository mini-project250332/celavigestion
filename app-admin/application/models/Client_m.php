<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_client";
    protected $_primary_key = "client_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getNomClient($data)
    {
        return $this->db->select("GROUP_CONCAT(
            CASE 
                WHEN c_client.client_prenom IS NOT NULL AND c_client.client_prenom != '' THEN CONCAT(c_client.client_nom, ' ', c_client.client_prenom) 
                ELSE c_client.client_nom 
            END
            SEPARATOR ', ') AS clients_concatenated")
            ->from('c_client')
            ->where_in('client_id', $data)
            ->get()
            ->row()
            ->clients_concatenated;
    }
}
