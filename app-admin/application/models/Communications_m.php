<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Communications_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_communications";
        protected $_primary_key = "coms_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        } 
    }