<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reglement_ventilation_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_reglementventilation";
    protected $_primary_key = "idc_reglementVentilation";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getPrelevement($prelevement)
    {
        $res = $this->db->select(
            'c_dossier.dossier_id,
            GROUP_CONCAT(DISTINCT CONCAT(c_client.client_nom, " ", c_client.client_prenom) SEPARATOR ", ") AS clients,
            c_facture.fact_id, 
            c_facture.fact_num, 
            c_reglementventilation.montant, 
            c_dossier.iban, 
            c_dossier.bic, 
            c_mandat_sepa.sepa_numero',
            false
        )
            ->from($this->_table)
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)')
            ->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id')
            ->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id')
            ->where_in('c_reglementventilation.idc_reglementVentilation', $prelevement)
            ->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero')
            ->get()
            ->result();
        return $res;
    }

    function listereglement2()
    {

        $res = $this->db->select(

            'c_reglements.idc_reglements,
            c_reglements.mode_reglement,
            c_reglements.date_reglement,
            c_reglements.montant_total,
            c_mode_paiement_facturation.libelle,
            c_reglementventilation.dossier_id,
            GROUP_CONCAT(DISTINCT CONCAT(c_client.client_nom, " ", c_client.client_prenom) SEPARATOR ", ") AS clients,
            c_facture.fact_id, 
            c_facture.fact_num, 
            c_reglementventilation.montant',
        )
            ->from($this->_table)
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture', 'left')
            ->join('c_reglements', 'c_reglements.idc_reglements = c_reglementventilation.idc_reglements')
            ->join('c_dossier', 'c_dossier.dossier_id = c_reglementventilation.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)')
            ->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_reglements.mode_reglement')
            ->group_by('c_reglementventilation.idc_reglementVentilation')
            ->order_by('c_reglementventilation.idc_reglements', 'desc')
            ->get()
            ->result();

        return $res;
    }

    function listereglement($clause, $like, $pagination1, $pagination)
    {
        $this->db->select(
            'c_reglements.idc_reglements,
        c_reglements.mode_reglement,
        c_reglements.date_reglement,
        c_reglements.dossier_id,
        c_reglements.montant_total,
        c_reglements.reglement_type,
        c_mode_paiement_facturation.libelle,
        c_utilisateur.util_prenom,
        c_utilisateur.util_nom,

        JSON_ARRAYAGG(
            JSON_OBJECT(
                "fact_id", c_facture.fact_id,
                "fact_num", c_facture.fact_num,
                "montant", c_reglementventilation.montant,
                "idc_reglementVentilation", c_reglementventilation.idc_reglementVentilation,
                "docpath", (
                    SELECT doc_facturation_path 
                    FROM c_document_facturation 
                    WHERE c_document_facturation.fact_id = c_facture.fact_id
                    AND c_reglementventilation.id_facture = c_facture.fact_id
                    LIMIT 1
                ),
                "clients", (
                    SELECT GROUP_CONCAT(
                        COALESCE(c_client.client_nom, ""), " ",
                        COALESCE(c_client.client_prenom, "")
                    ) 
                    FROM c_dossier 
                    JOIN c_client ON FIND_IN_SET(c_client.client_id, c_dossier.client_id) 
                    WHERE c_dossier.dossier_id = c_reglementventilation.dossier_id
                ),
                "clientsId", (
                    SELECT GROUP_CONCAT(c_client.client_id) 
                    FROM c_dossier 
                    JOIN c_client ON FIND_IN_SET(c_client.client_id, c_dossier.client_id) 
                    WHERE c_dossier.dossier_id = c_reglementventilation.dossier_id
                )
            )
        ) AS reglement_ventilations',
            false // Définir le deuxième paramètre sur false pour désactiver l'échappement automatique des noms de colonnes
        );
        $this->db->from($this->_table);
        $this->db->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture', 'left');
        $this->db->join('c_reglements', 'c_reglements.idc_reglements = c_reglementventilation.idc_reglements');
        $this->db->join('c_dossier', 'c_dossier.dossier_id =  c_reglements.dossier_id');
        $this->db->join('c_utilisateur', 'c_utilisateur.util_id =  c_dossier.util_id', 'left');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_reglements.mode_reglement');
        $this->db->where('YEAR(c_reglements.date_reglement)', $clause['annereglement']);

        //Filtre

        if (isset($clause['f.fact_modepaiement']) && $clause['f.fact_modepaiement'] != '') {
            $this->db->where('c_reglements.mode_reglement', $clause['f.fact_modepaiement']);
        }

        if (isset($like['f.dossier_id']) && $like['f.dossier_id'] != '') {
            $this->db->where('c_reglements.dossier_id', intval($like['f.dossier_id']));
        }

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            // Vous devez utiliser isset pour vérifier si $like['f.dossier_id'] est défini
            // et si oui, l'utiliser pour construire la clause where_in()
            if (isset($like['facture']) && !empty($like['facture'])) {
                $this->db->where_in('c_reglements.dossier_id', $like['facture']);
            }
        }


        if (isset($like['client']) && $like['client'] != '') {
            $this->db->join('c_dossier', 'c_dossier.dossier_id = c_reglementventilation.dossier_id');
            $this->db->join('(SELECT dossier_id, GROUP_CONCAT(client_nom SEPARATOR ", ") AS all_clients FROM c_dossier JOIN c_client ON FIND_IN_SET(c_client.client_id, c_dossier.client_id) GROUP BY dossier_id) AS d', 'd.dossier_id = c_dossier.dossier_id');
            $this->db->like('d.all_clients', $like['client'], 'both');
        }

        if (isset($like['ms.sepa_numero']) && $like['ms.sepa_numero'] != '') {
            $this->db->join('c_dossier', 'c_dossier.dossier_id = c_reglementventilation.dossier_id');
            $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id');
            $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id');
            $this->db->like('c_mandat_sepa.sepa_numero', $like['ms.sepa_numero'], 'both');
        }

        $this->db->group_by('c_reglements.idc_reglements');
        $this->db->order_by('c_reglements.idc_reglements', 'desc');
        $this->db->limit($pagination1);
        $this->db->offset($pagination);

        $res =  $this->db->get();

        // Convertir le résultat en JSON
        // foreach ($res as &$row) {
        //     $row->reglement_ventilations = json_decode($row->reglement_ventilations);
        // }

        return $res->result();


        // // Convertir le résultat en JSON
        // foreach ($res as &$row) {
        //     $row->reglement_ventilations = json_decode($row->reglement_ventilations);
        // }
        // return $res;
    }

    function listereglements($clause, $like)
    {
        $this->db->select(
            'c_reglements.idc_reglements,
        c_reglements.mode_reglement,
        c_reglements.date_reglement,
        c_reglements.montant_total,
        c_mode_paiement_facturation.libelle,
        JSON_ARRAYAGG(
            JSON_OBJECT(
                "fact_id", c_facture.fact_id,
                "fact_num", c_facture.fact_num,
                "montant", c_reglementventilation.montant,
                "clients", (
                    SELECT GROUP_CONCAT(c_client.client_nom, " ", c_client.client_prenom) 
                    FROM c_dossier 
                    JOIN c_client ON FIND_IN_SET(c_client.client_id, c_dossier.client_id) 
                    WHERE c_dossier.dossier_id = c_reglementventilation.dossier_id
                ),
                 "clientsId", (
                    SELECT GROUP_CONCAT(c_client.client_id) 
                    FROM c_dossier 
                    JOIN c_client ON FIND_IN_SET(c_client.client_id, c_dossier.client_id) 
                    WHERE c_dossier.dossier_id = c_reglementventilation.dossier_id
                )
                
            )
        ) AS reglement_ventilations',
            false // Définir le deuxième paramètre sur false pour désactiver l'échappement automatique des noms de colonnes
        );
        $this->db->from($this->_table);
        $this->db->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture', 'left');
        $this->db->join('c_reglements', 'c_reglements.idc_reglements = c_reglementventilation.idc_reglements');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_reglements.mode_reglement');
        $this->db->where('YEAR(c_reglements.date_reglement)', $clause['annereglement']);

        //Filtre

        if (isset($clause['f.fact_modepaiement']) && $clause['f.fact_modepaiement'] != '') {
            $this->db->where('c_reglements.mode_reglement', $clause['f.fact_modepaiement']);
        }

        if (isset($like['f.dossier_id']) && $like['f.dossier_id'] != '') {
            $this->db->where('c_reglements.dossier_id', intval($like['f.dossier_id']));
        }

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            $this->db->like('c_facture.fact_num', $like['f.fact_num'], 'both');
        }

        if (isset($like['client']) && $like['client'] != '') {
            $this->db->join('c_dossier', 'c_dossier.dossier_id = c_reglements.dossier_id');
            $this->db->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id');
            $this->db->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)');
            $this->db->like('c_client.client_nom', $like['client'], 'both');
        }

        if (isset($like['ms.sepa_numero']) && $like['ms.sepa_numero'] != '') {
            $this->db->join('c_dossier', 'c_dossier.dossier_id = c_reglements.dossier_id');
            $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id');
            $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id');
            $this->db->like('c_mandat_sepa.sepa_numero', $like['ms.sepa_numero'], 'both');
        }



        $this->db->group_by('c_reglements.idc_reglements');
        $this->db->order_by('c_reglements.idc_reglements', 'desc');


        $res =  $this->db->get();


        // Convertir le résultat en JSON
        // foreach ($res as &$row) {
        //     $row->reglement_ventilations = json_decode($row->reglement_ventilations);
        // }

        return $res->result();
    }


    function getTotalVentile($id_facture)
    {
        $this->db->select_sum('montant', 'montant');
        $this->db->from('c_reglementventilation');
        $this->db->where('id_facture', $id_facture);
        $query = $this->db->get();
        $result = $query->row();
        $total_montant = $result->montant != NULL ? $result->montant : 0;
        return $total_montant;
    }

    function getTropPerçusReglement($idregelement)
    {
        $this->db->select_sum('montant', 'montant');
        $this->db->from('c_reglementventilation');
        $this->db->where('idc_reglements', $idregelement);
        $this->db->where('id_facture', null);
        $query = $this->db->get();
        $result = $query->row();
        $total_montant = $result->montant != NULL ? $result->montant : 0;
        return $total_montant;
    }

    function getTotalventilReglement($idregelement)
    {
        $this->db->select_sum('montant', 'montant');
        $this->db->from('c_reglementventilation');
        $this->db->where('idc_reglements', $idregelement);
        $query = $this->db->get();
        $result = $query->row();
        $total_montant = $result->montant != NULL ? $result->montant : 0;
        return $total_montant;
    }
}
