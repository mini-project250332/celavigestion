<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Prospect_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_prospect";
        protected $_primary_key = "prospect_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        }

        public function updateProspect($data,$prospect_id){
            
            $this->db->where('prospect_id', $prospect_id);

            $result = $this->db->update('c_prospect',$data);
            return $result;	
        }
        
    }