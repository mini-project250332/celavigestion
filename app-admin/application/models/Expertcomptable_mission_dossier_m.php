<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expertcomptable_mission_dossier_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_expertcomptable_mission_dossier";
    protected $_primary_key = "expert_MD_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function getMission($annee, $expert_id)
    {
        $startDate = $annee . '-01-01 00:00:00';
        $endDate = ($annee + 1) . '-01-01 00:00:00';

        $query =
            "SELECT 
        assigner.*,
        mission.*,
        dossier.*,
        juridique.*,
        client.client_id,
        client.client_nom,
        client.client_prenom
        FROM c_expertcomptable_mission_dossier AS mission
        INNER JOIN c_expertcomptable_assiger_dossier AS assigner ON mission.assiger_ECD_id = assigner.assiger_ECD_id
        INNER JOIN c_dossier AS dossier ON dossier.dossier_id  = assigner.dossier_id
        INNER JOIN c_forme_juridique AS juridique ON juridique.c_forme_juridique_id  = dossier.c_forme_juridique_id
        INNER JOIN c_client AS client ON client.client_id  = dossier.client_id
        WHERE ((date_debut_mission >= '$startDate'  AND date_debut_mission < '$endDate') 
        OR (date_resiliation_mission >= '$startDate' AND date_resiliation_mission <= '$endDate')
        OR (date_resiliation_mission >= '$startDate' AND date_resiliation_mission >= '$endDate'))";

        if ($expert_id != NULL) {
            $query .= " AND assigner.expert_id = $expert_id";
        }

        $requete = $this->db->query($query);
        $result = $requete->result();
        return $result;
    }
}
