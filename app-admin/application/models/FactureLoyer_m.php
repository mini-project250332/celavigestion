<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FactureLoyer_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facture_loyer";
    protected $_primary_key = "fact_loyer_id ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function get_mensuel($bail_id, $year)
    {
        $res = $this->db->select("
        periode_mens_libelle,
        SUM(facture_nombre) AS facture_nombre,
        SUM(fact_loyer_app_ht) AS fact_loyer_app_ht,
        SUM(fact_loyer_app_tva) AS fact_loyer_app_tva, 
        SUM(fact_loyer_app_ttc) AS fact_loyer_app_ttc,
        SUM(fact_loyer_park_ht) AS fact_loyer_park_ht, 
        SUM(fact_loyer_park_tva) AS fact_loyer_park_tva,
        SUM(fact_loyer_park_ttc) AS fact_loyer_park_ttc,
        SUM(fact_loyer_charge_ht) AS fact_loyer_charge_ht,
        SUM(fact_loyer_charge_tva) AS fact_loyer_charge_tva,
        SUM(fact_loyer_charge_ttc) AS fact_loyer_charge_ttc,
        ")
            ->from($this->_table)
            ->join('c_periode_mensuel','c_periode_mensuel.periode_mens_id  = c_facture_loyer.periode_mens_id ')
            ->where('bail_id = '.$bail_id .' AND facture_annee = '.$year.' AND c_facture_loyer.periode_mens_id IS NOT NULL' )
            ->group_by('c_facture_loyer.periode_mens_id')
            ->get()
            ->result();
        return $res;
    }

    public function updateEmailSent($fact_loyer_id, $isSent)
    {
        $res = $this->db
            ->set('fact_email_sent', $isSent, FALSE)
            ->where('fact_loyer_id  = ' . $fact_loyer_id)
            ->update($this->_table);
        return $res;
    }
}