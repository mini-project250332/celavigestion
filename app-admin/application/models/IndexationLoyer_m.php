<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class IndexationLoyer_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_indexation_loyer";
    protected $_primary_key = "indlo_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function getIdnexation($bail_id, $year)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->join('c_bail', 'c_bail.bail_id = c_indexation_loyer.bail_id')
            ->join('c_indice_valeur_loyer', 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base')
            ->where('c_indexation_loyer.etat_indexation = 1 AND c_indexation_loyer.bail_id =' . $bail_id . ' AND YEAR(c_indexation_loyer.indlo_fin)<=' . $year)
            ->get()
            ->row();
        return $res;
    }

    public function update_where_date($date, $bail_id)
    {
        $data = array(
            'indlo_indince_non_publie' => 2
        );
        $this->db->where('indlo_debut <', $date);
        $this->db->where('bail_id', $bail_id);
        $this->db->update($this->_table, $data);
    }

    public function delete_where_index_date($date, $bail_id)
    {
        $this->db->from($this->_table);
        $this->db->where('indlo_debut', $date);
        $this->db->where('bail_id', $bail_id);
        $this->db->delete();
    }
}
