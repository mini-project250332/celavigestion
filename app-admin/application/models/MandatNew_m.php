<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class MandatNew_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_mandat_new";
        protected $_primary_key = "mandat_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        }    
        
        public function get_document($table)
        {
            $res = $this->db->select("*")
                ->from($this->_table)
                ->where_in('mandat_id', $table)
                ->get()
                ->result();
            return $res;
        }
        
    }