<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Program_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_programme";
        protected $_primary_key = "progm_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        }

        public function liste_lotplus_cinqlot(){
            $query = "SELECT c_programme.progm_nom, c_programme.progm_id, COUNT(*) as nbr FROM c_programme INNER JOIN c_lot_client ON c_programme.progm_id = c_lot_client.progm_id LEFT JOIN c_mandat_new ON c_mandat_new.cliLot_id = c_lot_client.cliLot_id WHERE c_mandat_new.etat_mandat_id = 3 GROUP BY c_lot_client.progm_id HAVING nbr > 4 ORDER BY c_programme.progm_nom ASC";
            $requete = $this->db->query($query);
            $result = $requete->result();
            return $result;
        }
        
    }