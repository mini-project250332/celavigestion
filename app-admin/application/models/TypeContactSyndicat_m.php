<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class TypeContactSyndicat_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_contact_syndicat";
    protected $_primary_key = "type_cont_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
