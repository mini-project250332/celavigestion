<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BailArt_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_bail_art";
    protected $_primary_key = "bart_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
