<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class DocumentSie_m extends ADM_Model
    {   
        protected $_base_donnee_defaut = FALSE;
        protected $_table = "c_document_sie";
        protected $_primary_key = "doc_sie_id";
        protected $_order = "";
        protected $_filter = "intval";

        function __construct()
        {
            parent::__construct();
        }

        public function get_document($table)
        {
            $res = $this->db->select("*")
                ->from($this->_table)
                ->where_in('doc_sie_id', $table)
                ->join("c_lot_client", "c_lot_client.cliLot_id = c_document_sie.cliLot_id")
                ->join("c_mandat_new","c_mandat_new.cliLot_id  = c_document_sie.cliLot_id")
                ->get()
                ->result();
            return $res;
        }

    }