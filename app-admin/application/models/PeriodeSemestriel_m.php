<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PeriodeSemestriel_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_periode_semestre";
    protected $_primary_key = "periode_semestre_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}