<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MotDePasseFruit_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_mot_de_passe_fruit";
    protected $_primary_key = "mtpfr_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getRandomPasswordFuit()
    {
        $result = $this->db->select('*')->from($this->_table)->get()->result();
        $count = count($result);
        $index = rand(0, $count - 1);
        return $result[$index];
    }
}