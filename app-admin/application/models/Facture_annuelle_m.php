<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facture_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facture";
    protected $_primary_key = "fact_id";
    protected $_order = "";
    protected $_filter = "intval";

    function getFacture($clause, $like, $pagination1, $pagination)
    {
        $this->db->select('*,ROUND(c_facture.montant_ttc - SUM( c_reglementventilation.montant), 2) AS reste_a_payer, c_dossier.dossier_id');
        $this->db->from('c_facture');
        $this->db->join('c_document_facturation', 'c_document_facturation.fact_id = c_facture.fact_id');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facture.mode_paiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id', 'left');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left');
        $this->db->where('annee', $clause['annee']);
        $this->db->where('etat_paiement', $clause['etat_paiement']);

        if (isset($clause['c_facture.mode_paiement']) && $clause['c_facture.mode_paiement'] != '') {
            $this->db->where('c_facture.mode_paiement', $clause['c_facture.mode_paiement']);
        }

        if (isset($like['c_facture.dossier_id']) && $like['c_facture.dossier_id'] != '') {
            $this->db->where('c_facture.dossier_id', intval($like['c_facture.dossier_id']));
        }

        if (isset($like['c_facture.fact_num']) && $like['c_facture.fact_num'] != '') {
            $this->db->like('c_facture.fact_num', $like['c_facture.fact_num'], 'both');
        }

        if (isset($like['c_mandat_sepa.sepa_numero']) && $like['c_mandat_sepa.sepa_numero'] != '') {
            $this->db->like('c_mandat_sepa.sepa_numero', $like['c_mandat_sepa.sepa_numero'], 'both');
        }

        $this->db->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_facture.montant_ttc, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero');
        $this->db->order_by('c_facture.fact_id', 'DESC');
        $this->db->limit($pagination1);
        $this->db->offset($pagination);
        $query = $this->db->get();
        return $query->result();
    }

    function getFactureCount($clause, $like)
    {
        $this->db->select('*,ROUND(c_facture.montant_ttc - SUM( c_reglementventilation.montant), 2) AS reste_a_payer, c_dossier.dossier_id');
        $this->db->from('c_facture');
        $this->db->join('c_document_facturation', 'c_document_facturation.fact_id = c_facture.fact_id');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facture.mode_paiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id', 'left');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left');
        $this->db->where('annee', $clause['annee']);
        $this->db->where('etat_paiement', $clause['etat_paiement']);

        if (isset($clause['c_facture.mode_paiement']) && $clause['c_facture.mode_paiement'] != '') {
            $this->db->where('c_facture.mode_paiement', $clause['c_facture.mode_paiement']);
        }

        if (isset($like['c_facture.dossier_id']) && $like['c_facture.dossier_id'] != '') {
            $this->db->where('c_facture.dossier_id', intval($like['c_facture.dossier_id']));
        }

        if (isset($like['c_facture.fact_num']) && $like['c_facture.fact_num'] != '') {
            $this->db->like('c_facture.fact_num', $like['c_facture.fact_num'], 'both');
        }

        if (isset($like['c_mandat_sepa.sepa_numero']) && $like['c_mandat_sepa.sepa_numero'] != '') {
            $this->db->like('c_mandat_sepa.sepa_numero', $like['c_mandat_sepa.sepa_numero'], 'both');
        }
        $this->db->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_facture.montant_ttc, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero');
        $query = $this->db->get();
        return $query->result();
    }

    public function getPrelevement($table_fact_num)
    {
        $table_fact_num = implode(',', $table_fact_num);
        $query = "SELECT 
                c_dossier.dossier_id, 
                c_facture.fact_id, 
                c_facture.fact_num, 
                c_facture.montant_ttc, 
                c_dossier.iban, 
                c_dossier.bic, 
                c_mandat_sepa.sepa_numero, 
                ROUND(c_facture.montant_ttc - COALESCE(SUM(c_reglementventilation.montant), 0), 2) AS reste_a_payer,
                (SELECT GROUP_CONCAT(DISTINCT CONCAT(client_nom, ' ', client_prenom) SEPARATOR ', ') FROM c_client WHERE FIND_IN_SET(c_client.client_id, d.client_id)) AS clients
                FROM 
                c_facture 
                JOIN 
                c_dossier ON c_dossier.dossier_id = c_facture.dossier_id 
                JOIN 
                (SELECT dossier_id, client_id FROM c_dossier) AS d ON d.dossier_id = c_dossier.dossier_id 
                JOIN 
                (SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa ON latest_mandat_sepa.dossier_id = c_dossier.dossier_id 
                JOIN 
                c_mandat_sepa ON c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id 
                LEFT JOIN 
                c_reglementventilation ON c_reglementventilation.id_facture = c_facture.fact_id 
                WHERE 
                c_facture.fact_id IN ($table_fact_num) 
                GROUP BY 
                c_dossier.dossier_id, 
                c_facture.fact_id, 
                c_facture.fact_num, 
                c_facture.montant_ttc, 
                c_dossier.iban, 
                c_dossier.bic, 
                c_mandat_sepa.sepa_numero";

        $query = $this->db->query($query); // Exécute la requête SQL
        return $query->result(); // Retourne les résultats
    }


    function getResteAPayer($idfacture, $montant)
    {
        $res = $this->db->select(
            'ROUND((c_facture.montant_ttc - COALESCE(SUM(c_reglementventilation.montant), 0) - ' . $montant . '), 2) AS reste_a_payer',
            false
        )
            ->from($this->_table)
            ->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left')
            ->where('fact_id', $idfacture)
            ->get()
            ->result();

        if (isset($res[0]->reste_a_payer)) {
            return $res[0]->reste_a_payer;
        } else {
            return null;
        }
    }

    function getfactureAnnuelleByid($fact_id)
    {
        $this->db->select('*,ROUND(c_facture.montant_ttc - SUM( c_reglementventilation.montant), 2) AS reste_a_payer, c_dossier.dossier_id');
        $this->db->from('c_facture');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facture.mode_paiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id', 'left');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left');
        $this->db->where('c_facture.fact_id', $fact_id);
        $this->db->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_facture.montant_ttc, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero');
        $query = $this->db->get();
        return $query->row();
    }
}
