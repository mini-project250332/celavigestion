<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class HistoriqueMail_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_historique_mail";
    protected $_primary_key = "hist_mail_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }


}