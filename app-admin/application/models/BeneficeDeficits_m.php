<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BeneficeDeficits_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_benefice_deficits";
    protected $_primary_key = "id_benefice_deficits";
    protected $_order = "";
    protected $_filter = "intval";
}