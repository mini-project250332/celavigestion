<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ClientSelectionne_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_proprietaire_seclectionne";
    protected $_primary_key = "prop_select_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
