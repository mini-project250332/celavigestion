<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FactureArticle_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facture_articles";
    protected $_primary_key = "faa_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getPrgmNom($fact_id)
    {
        $this->db->select("GROUP_CONCAT(progm_nom) AS progm_nom")
            ->from('c_facture_articles')
            ->where('fact_id', $fact_id)
            ->join('c_lot_client', 'c_lot_client.cliLot_id = c_facture_articles.lot_id', 'left')
            ->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id', 'left')
            ->group_by('c_facture_articles.fact_id');

        $query = $this->db->get();
        return $query->row();
    }

    function getArticle($fact_id)
    {
        $this->db->select("*")
            ->from('c_facture_articles')
            ->where('fact_id', $fact_id)
            ->join('c_lot_client', 'c_lot_client.cliLot_id = c_facture_articles.lot_id', 'left')
            ->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }
}
