<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mode_paiement_fact_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_mode_paiement_facturation";
    protected $_primary_key = "id_mode_paie";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function get_mode_paieReglement($data)
    {
        $res = $this->db->select('*')
            ->from($this->_table)
            ->where_in('id_mode_paie', $data)
            ->get()
            ->result();
        return $res;
    }
}
