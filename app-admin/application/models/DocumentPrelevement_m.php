<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DocumentPrelevement_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_document_prelevement";
    protected $_primary_key = "doc_prelevement_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getDocprelevement($data)
    {
        $this->db->select('doc_prelevement_id, doc_prelevement_nom, doc_prelevement_path')
            ->from('c_document_prelevement')
            ->where_in('doc_prelevement_id', $data);
        $query = $this->db->get();
        return $query->result();
    }
}
