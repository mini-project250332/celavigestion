<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class TypeEncaissement_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_encaissement";
    protected $_primary_key = "typeEnc_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
