<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AccessProprietaire_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_access_proprietaire";
    protected $_primary_key = "accp_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function isExist($login)
    {
        $text = $login;
        $text .= "%";
        $res = $this->db->select("*")->from($this->_table)->where('accp_login LIKE "' . $text . '"')->get()->result();
        return count($res);
    }
}