<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MandatSepa_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_mandat_sepa";
    protected $_primary_key = "sepa_id";
    protected $_order = "";
    protected $_filter = "intval";


    function __construct()
    {
        parent::__construct();
    }


    public function getlastSepa(){
        $query = "SELECT sepa_rum,`sepa_id` FROM c_mandat_sepa ORDER BY sepa_id DESC LIMIT 1";
        $requete = $this->db->query($query);
        $result = $requete->result();
        return $result;
    }

}


