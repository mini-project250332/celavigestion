<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class NaturePriseEffet_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_nature_prise_effet";
    protected $_primary_key = "natef_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
