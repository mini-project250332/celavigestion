<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CommunicationClient_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_communication_client";
    protected $_primary_key = "comclient_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
