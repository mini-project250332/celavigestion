<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prelevement_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_prelevement";
    protected $_primary_key = "idc_prelevement";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }


    function getPrelevement($pagination_data)
    {
        $this->db->select("c_prelevement.*, c_banque.*, c_document_prelevement.*, ROUND(SUM(c_reglements.montant_total), 2) as total_reglements")
            ->from($this->_table)
            ->join('c_document_prelevement', 'c_document_prelevement.doc_prelevement_prelevementID = c_prelevement.idc_prelevement', 'left')
            ->join('c_banque', 'c_banque.banque_id = c_prelevement.id_banque')
            ->join('c_reglements', 'c_reglements.idc_prelevement = c_prelevement.idc_prelevement')
            ->group_by('c_prelevement.idc_prelevement');
        $this->db->order_by('c_prelevement.idc_prelevement', 'DESC');
        $this->db->limit($pagination_data[1]);
        $this->db->offset($pagination_data[0]);

        $res = $this->db->get()->result();
        return $res;
    }


    function getListlotTotalPrelevement()
    {
        $this->db->select("c_prelevement.*, c_banque.*, c_document_prelevement.*, ROUND(SUM(c_reglements.montant_total), 2) as total_reglements")
            ->from($this->_table)
            ->join('c_document_prelevement', 'c_document_prelevement.doc_prelevement_prelevementID = c_prelevement.idc_prelevement')
            ->join('c_banque', 'c_banque.banque_id = c_prelevement.id_banque')
            ->join('c_reglements', 'c_reglements.idc_prelevement = c_prelevement.idc_prelevement')
            ->group_by('c_prelevement.idc_prelevement');
        $res = $this->db->get()->result();
        return $res;
    }

    public function getPrelevementFacturebyId($idc_prelevement)
    {
        $this->db->select('*, (SELECT GROUP_CONCAT(DISTINCT CONCAT(client_nom, " ", client_prenom) SEPARATOR ", ") FROM c_client WHERE FIND_IN_SET(c_client.client_id, d.client_id)) AS clients')
            ->from($this->_table)
            ->join('c_reglements', 'c_reglements.idc_prelevement = c_prelevement.idc_prelevement')
            ->join('c_dossier', 'c_dossier.dossier_id = c_reglements.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id')
            ->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id')
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.idc_reglements')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->where('c_reglements.idc_prelevement', $idc_prelevement);
        $res = $this->db->get()->result();
        return $res;
    }

    function getPrelevementAvecId($idprelevement)
    {
        $this->db->select("c_prelevement.*, c_banque.*, c_document_prelevement.*, ROUND(SUM(c_reglements.montant_total), 2) as total_reglements")
            ->from($this->_table)
            ->join('c_document_prelevement', 'c_document_prelevement.doc_prelevement_prelevementID = c_prelevement.idc_prelevement')
            ->join('c_banque', 'c_banque.banque_id = c_prelevement.id_banque')
            ->join('c_reglements', 'c_reglements.idc_prelevement = c_prelevement.idc_prelevement')
            ->group_by('c_prelevement.idc_prelevement')
            ->where('c_prelevement.idc_prelevement', $idprelevement);
        $res = $this->db->get()->result();
        return $res;
    }
}
