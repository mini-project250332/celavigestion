<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ImmoGeneral_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_immo_general";
    protected $_primary_key = "immo_general_id";
    protected $_order = "";
    protected $_filter = "intval";

}
