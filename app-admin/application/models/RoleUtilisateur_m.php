<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class RoleUtilisateur_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_role_utilisateur";
    protected $_primary_key = "rol_util_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }


}