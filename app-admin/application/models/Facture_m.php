<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facture_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facture";
    protected $_primary_key = "fact_id";
    protected $_order = "";
    protected $_filter = "intval";

    function listFacture_exportPDF($data)
    {
        $this->db->select("c_facture.*, c_document_facturation.*, c_dossier.*,
        (SELECT GROUP_CONCAT(DISTINCT lot_id SEPARATOR ', ') FROM c_facture_articles WHERE c_facture_articles.fact_id = c_facture.fact_id) AS cliLot_id")
            ->from('c_facture')
            ->join('c_document_facturation', 'c_document_facturation.fact_id = c_facture.fact_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->where('fact_annee', $data['annee']);

        if (!empty($data['num_dossier_facture'])) {
            $this->db->like('c_facture.dossier_id', $data['num_dossier_facture']);
        }

        if (!empty($data['num_facture'])) {
            $this->db->like('c_facture.fact_num', $data['num_facture']);
        }

        // ->order_by('c_facture.fact_id', 'DESC')
        $query = $this->db->get();
        return $query->result();
    }


    function listFacture($data)
    {
        $this->db->select("c_facture.*, c_document_facturation.*, c_dossier.*, CONCAT(c_utilisateur.util_prenom, ' ', c_utilisateur.util_nom) AS user,
        (SELECT GROUP_CONCAT(DISTINCT lot_id SEPARATOR ', ') FROM c_facture_articles WHERE c_facture_articles.fact_id = c_facture.fact_id) AS cliLot_id")
            ->from('c_facture')
            ->join('c_document_facturation', 'c_document_facturation.fact_id = c_facture.fact_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->join('c_utilisateur', 'c_utilisateur.util_id = c_dossier.util_id', 'left')
            ->where('fact_annee', $data['annee']);

        if (!empty($data['num_dossier_facture'])) {
            $this->db->like('c_facture.dossier_id', $data['num_dossier_facture']);
        }

        if (!empty($data['num_facture'])) {
            $this->db->like('c_facture.fact_num', $data['num_facture']);
        }

        if ($data['etatfacture'] != '') {
            $this->db->where('c_facture.fact_etatpaiement', intval($data['etatfacture']));
        }

        if ($data['date_debut'] != '') {
            $this->db->where('c_facture.fact_date >=', $data['date_debut']);
        }

        if ($data['date_fin'] != '') {
            $this->db->where('c_facture.fact_date <=', $data['date_fin']);
        }

        $this->db->order_by('c_facture.fact_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function listFacturecount($data)
    {
        $this->db->select("c_facture.*, c_document_facturation.*, c_dossier.*,
        (SELECT GROUP_CONCAT(DISTINCT lot_id SEPARATOR ', ') FROM c_facture_articles WHERE c_facture_articles.fact_id = c_facture.fact_id) AS cliLot_id")
            ->from('c_facture')
            ->join('c_document_facturation', 'c_document_facturation.fact_id = c_facture.fact_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->where('fact_annee', $data['annee']);

        if (!empty($data['num_dossier_facture'])) {
            $this->db->like('c_facture.dossier_id', $data['num_dossier_facture']);
        }

        if (!empty($data['num_facture'])) {
            $this->db->like('c_facture.fact_num', $data['num_facture']);
        }

        if (!empty($data['etatfacture']) && $data['etatfacture'] != 'null') {
            $this->db->where('c_facture.fact_etatpaiement', intval($data['etatfacture']));
        }

        if ($data['etatfacture'] != '') {
            $this->db->where('c_facture.fact_etatpaiement', intval($data['etatfacture']));
        }

        if ($data['date_debut'] != '') {
            $this->db->where('c_facture.fact_date >=', $data['date_debut']);
        }

        if ($data['date_fin'] != '') {
            $this->db->where('c_facture.fact_date <=', $data['date_fin']);
        }

        $this->db->order_by('c_facture.fact_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getFacture($clause, $like)
    {
        $this->db->select('*, ROUND(SIGN(f.fact_montantttc) * (ABS(f.fact_montantttc) - IFNULL(SUM(ABS(rv.montant)), 0)), 2) AS reste_a_payer, d.dossier_id');
        $this->db->select('(SELECT GROUP_CONCAT(DISTINCT lot_id SEPARATOR ", ") FROM c_facture_articles WHERE c_facture_articles.fact_id = f.fact_id) AS cliLot_id');
        $this->db->select("CONCAT(user.util_prenom, ' ', user.util_nom) AS user");
        $this->db->from('c_facture f');
        $this->db->join('c_document_facturation df', 'df.fact_id = f.fact_id');
        $this->db->join('c_dossier d', 'd.dossier_id = f.dossier_id')
            ->join('c_utilisateur user', 'user.util_id = d.util_id', 'left');
        $this->db->join('c_mode_paiement_facturation mpf', 'mpf.id_mode_paie = f.fact_modepaiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = d.dossier_id', 'left');
        $this->db->join('c_mandat_sepa ms', 'ms.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation rv', 'rv.id_facture = f.fact_id', 'left');
        $this->db->where('f.fact_annee', $clause['fact_annee']);
        $this->db->where('f.fact_etatpaiement', $clause['fact_etatpaiement']);

        if (isset($clause['f.fact_modepaiement']) && $clause['f.fact_modepaiement'] != '') {
            $this->db->where('f.fact_modepaiement', $clause['f.fact_modepaiement']);
        }

        if (isset($like['f.dossier_id']) && $like['f.dossier_id'] != '') {
            $this->db->where('f.dossier_id', intval($like['f.dossier_id']));
        }

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            $this->db->like('f.fact_num', $like['f.fact_num'], 'both');
        }

        if (isset($like['ms.sepa_numero']) && $like['ms.sepa_numero'] != '') {
            $this->db->like('ms.sepa_numero', $like['ms.sepa_numero'], 'both');
        }

        $this->db->group_by('d.dossier_id, f.fact_id, f.fact_num, f.fact_montantttc, d.iban, d.bic, ms.sepa_numero');
        $this->db->order_by('f.fact_id', 'DESC');
        // $this->db->limit($pagination1);
        // $this->db->offset($pagination);
        $query = $this->db->get();

        return $query->result();
    }

    function getFactureCount($clause, $like)
    {
        $this->db->select('*, ROUND(f.fact_montantttc - SUM(rv.montant), 2) AS reste_a_payer, d.dossier_id');
        $this->db->select('(SELECT GROUP_CONCAT(DISTINCT lot_id SEPARATOR ", ") FROM c_facture_articles WHERE c_facture_articles.fact_id = f.fact_id) AS cliLot_id');
        $this->db->from('c_facture f');
        $this->db->join('c_document_facturation df', 'df.fact_id = f.fact_id');
        $this->db->join('c_dossier d', 'd.dossier_id = f.dossier_id');
        $this->db->join('c_mode_paiement_facturation mpf', 'mpf.id_mode_paie = f.fact_modepaiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = d.dossier_id', 'left');
        $this->db->join('c_mandat_sepa ms', 'ms.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation rv', 'rv.id_facture = f.fact_id', 'left');
        $this->db->where('f.fact_annee', $clause['fact_annee']);
        $this->db->where('f.fact_etatpaiement', $clause['fact_etatpaiement']);

        if (isset($clause['f.fact_modepaiement']) && $clause['f.fact_modepaiement'] != '') {
            $this->db->where('f.fact_modepaiement', $clause['f.fact_modepaiement']);
        }

        if (isset($like['f.dossier_id']) && $like['f.dossier_id'] != '') {
            $this->db->where('f.dossier_id', intval($like['f.dossier_id']));
        }

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            $this->db->like('f.fact_num', $like['f.fact_num'], 'both');
        }

        if (isset($like['ms.sepa_numero']) && $like['ms.sepa_numero'] != '') {
            $this->db->like('ms.sepa_numero', $like['ms.sepa_numero'], 'both');
        }

        $this->db->group_by('d.dossier_id, f.fact_id, f.fact_num, f.fact_montantttc, d.iban, d.bic, ms.sepa_numero');
        $this->db->order_by('f.fact_id', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }




    function getFactureCountold($clause, $like)
    {
        $this->db->select('*');
        $this->db->from('c_facture f');
        $this->db->join('c_document_facturation df', 'df.fact_id = f.fact_id');
        $this->db->join('c_dossier d', 'd.dossier_id = f.dossier_id');
        $this->db->join('c_mode_paiement_facturation mpf', 'mpf.id_mode_paie = f.fact_modepaiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = d.dossier_id', 'left');
        $this->db->join('c_mandat_sepa ms', 'ms.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation rv', 'rv.id_facture = f.fact_id', 'left');
        $this->db->where('f.fact_annee', $clause['fact_annee']);
        $this->db->where('f.fact_etatpaiement', $clause['fact_etatpaiement']);

        if (isset($clause['f.fact_modepaiement']) && $clause['f.fact_modepaiement'] != '') {
            $this->db->where('f.fact_modepaiement', $clause['f.fact_modepaiement']);
        }

        if (isset($like['f.dossier_id']) && $like['f.dossier_id'] != '') {
            $this->db->where('f.dossier_id', intval($like['f.dossier_id']));
        }

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            $this->db->like('f.fact_num', $like['f.fact_num'], 'both');
        }

        if (isset($like['ms.sepa_numero']) && $like['ms.sepa_numero'] != '') {
            $this->db->like('ms.sepa_numero', $like['ms.sepa_numero'], 'both');
        }

        $this->db->group_by('d.dossier_id, f.fact_id, f.fact_num, f.fact_montantttc, d.iban, d.bic, ms.sepa_numero');
        $query = $this->db->get();

        return $query->result();
    }


    public function getPrelevement($table_fact_num)
    {
        $table_fact_num = implode(',', $table_fact_num);

        $this->db->select('
            c_dossier.dossier_id,
            c_facture.fact_id,
            c_facture.fact_num,
            c_facture.fact_montantttc,
            c_dossier.iban,
            c_dossier.bic,
            c_mandat_sepa.sepa_numero
        ');

        // Calcul du reste_a_payer
        $this->db->select('ROUND(SIGN(c_facture.fact_montantttc) * (ABS(c_facture.fact_montantttc) - IFNULL(SUM(ABS(c_reglementventilation.montant)), 0)), 2) AS reste_a_payer', FALSE);

        // Calcul de la liste des clients
        $this->db->select('(SELECT GROUP_CONCAT(DISTINCT CONCAT(client_nom, " ", client_prenom) SEPARATOR ", ") FROM c_client WHERE FIND_IN_SET(c_client.client_id, d.client_id)) AS clients', FALSE);

        $this->db->from('c_facture');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id');
        $this->db->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left');
        $this->db->where("c_facture.fact_id IN ($table_fact_num)");
        $this->db->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_facture.fact_montantttc, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero');

        $query = $this->db->get(); // Exécute la requête active record
        return $query->result(); // Retourne les résultats
    }





    function getfactureAnnuelleByid($fact_id)
    {
        $this->db->select('
            c_dossier.dossier_id,
            c_facture.fact_id,
            c_facture.fact_num,
            c_facture.fact_montantttc,
            c_dossier.iban,
            c_dossier.bic,
            c_mandat_sepa.sepa_numero
        ');

        // Calcul du reste_a_payer
        $this->db->select('ROUND(SIGN(c_facture.fact_montantttc) * (ABS(c_facture.fact_montantttc) - IFNULL(SUM(ABS(c_reglementventilation.montant)), 0)), 2) AS reste_a_payer', FALSE);
        $this->db->from('c_facture');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facture.fact_modepaiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id', 'left');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.id_facture = c_facture.fact_id', 'left');
        $this->db->where('c_facture.fact_id', $fact_id);
        $this->db->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_facture.fact_montantttc, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero');

        $query = $this->db->get();
        return $query->row();
    }

    function getFactureDossier($dossier_id)
    {
        $this->db->select('f.fact_id,f.fact_etatpaiement, f.fact_num, f.fact_idFactureDu, f.fact_date, f.fact_modepaiement, f.fact_montantht, f.fact_montanttva, f.fact_montantttc, f.fact_commentaire, f.fact_JustificationAvoir,
        mpf.id_mode_paie, ROUND(SIGN(f.fact_montantttc) * (ABS(f.fact_montantttc) - IFNULL(SUM(rv.montant), 0)), 2) AS reste_a_payer, d.dossier_id, 
        u.util_prenom, u.util_nom, re.libelle_reglement, re.date_action, SUM(re.montant_total) AS montant, SUM(rv.montant) AS montantventil, df.doc_facturation_path');
        $this->db->from('c_facture f');
        $this->db->join('c_dossier d', 'd.dossier_id = f.dossier_id');
        $this->db->join('c_mode_paiement_facturation mpf', 'mpf.id_mode_paie = f.fact_modepaiement');
        $this->db->join('c_document_facturation df', 'df.fact_id = f.fact_id');
        $this->db->join('c_reglementventilation rv', 'rv.id_facture = f.fact_id', 'left');
        $this->db->join('c_reglements re', 're.idc_reglements = rv.idc_reglements', 'left');
        // $this->db->join('c_facture_articles fa', 'fa.fact_id = f.fact_id', 'left');
        $this->db->join('c_utilisateur u', 'u.util_id = re.util_id', 'left');
        $this->db->where('f.dossier_id', $dossier_id);
        $this->db->group_by('d.dossier_id, f.fact_id, f.fact_num, f.fact_montantttc, d.iban, d.bic');
        $this->db->order_by('f.fact_id', 'DESC');

        $query = $this->db->get();
        return $query->result();
    }

    public function getResteApayer($factid)
    {
        $this->db->select('ROUND(SIGN(f.fact_montantttc) * (ABS(f.fact_montantttc) - IFNULL(SUM(ABS(rv.montant)), 0)), 2) AS reste_a_payer');
        $this->db->from('c_facture f');
        $this->db->join('c_reglementventilation rv', 'rv.id_facture = f.fact_id', 'left');
        $this->db->join('c_reglements re', 're.idc_reglements = rv.idc_reglements', 'left');
        $this->db->where('f.fact_id', $factid);
        $query = $this->db->get();
        $result = $query->row();
        $restePayer = $result->reste_a_payer != NULL ? $result->reste_a_payer : 0;
        return $restePayer;
    }

    public function getDossierIds($like)
    {

        if (isset($like['f.fact_num']) && $like['f.fact_num'] != '') {
            $this->db->select('c_facture.dossier_id');
            $this->db->from('c_facture');
            $this->db->like('c_facture.fact_num', $like['f.fact_num'], 'both');
            $query = $this->db->get(); // Exécute la requête active record
            return $query->result();
        }
    }

    function getFactureBydate($date)
    {
        $this->db->select('c_facture.fact_id, c_facture.fact_num, c_facture.fact_date, c_facture.fact_montantht, c_facture.fact_montanttva, c_facture.fact_montantttc, c_facture.fact_idFactureDu,
        c_article.art_cpt_comptable, c_article.art_code_tva, c_article.art_pourcentage_tva, c_article.art_compte_comptable,
        c_client.client_nom, c_client.client_prenom, c_dossier.dossier_id')
            ->from('c_facture')
            ->join('c_facture_articles', 'c_facture_articles.fact_id = c_facture.fact_id')
            ->join('c_article', 'c_article.art_id = c_facture_articles.art_id')
            ->join('c_lot_client', 'c_lot_client.cliLot_id = c_facture_articles.lot_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_lot_client.dossier_id')
            ->join('c_client', 'c_client.client_id = c_lot_client.cli_id')
            ->where('c_facture.fact_date <= ', $date)
            ->where('c_facture.transcomp_id IS NULL')
            ->group_by('c_facture. fact_id');
        $query = $this->db->get();
        return $query->result();
    }
}
