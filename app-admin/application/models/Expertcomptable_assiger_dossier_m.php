<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expertcomptable_assiger_dossier_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_expertcomptable_assiger_dossier";
    protected $_primary_key = "assiger_ECD_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}