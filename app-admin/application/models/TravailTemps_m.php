<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TravailTemps_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_travail_effectue";
    protected $_primary_key = "travail_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
