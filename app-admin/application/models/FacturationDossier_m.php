<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FacturationDossier_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facturation_lot";
    protected $_primary_key = "id_fact_lot";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getallstatus($annee)
    {
        $res = $this->db->select('*')
            ->from($this->_table)
            ->where('annee', $annee)
            ->where_in('c_facturation_status', array(1, 2))
            ->get()
            ->result();
        return $res;
    }

    function obtenir_facturations_liste($annee)
    {
        $this->db->select('c_facturation_lot.dossier_id');
        $this->db->select('c_facturation_lot.mode_paiement');
        $this->db->select('c_mode_paiement_facturation.libelle');
        $this->db->select('SUM(c_facturation_lot.montant_ht_facture) AS sommeht');
        $this->db->select('c_dossier.client_id');
        $this->db->select('c_mandat_sepa.sepa_iban, c_mandat_sepa.sepa_numero, c_mandat_sepa.sepa_etat');
        $this->db->select("CONCAT(c_utilisateur.util_prenom, ' ', c_utilisateur.util_nom) AS user");
        $this->db->from('c_facturation_lot');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facturation_lot.dossier_id');
        $this->db->join('c_utilisateur', 'c_utilisateur.util_id = c_dossier.util_id', 'left');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement');
        $this->db->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id', 'left');
        $this->db->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id', 'left');

        $this->db->where('c_facturation_lot.annee', $annee);
        $this->db->where('c_facturation_lot.c_facturation_status', 1);
        $this->db->group_by('c_dossier.dossier_id');
        $query = $this->db->get();
        return $query->result();
    }

    function obtenir_facturations($table, $annee)
    {
        $this->db->select('c_facturation_lot.dossier_id, c_facturation_lot.mode_paiement, c_mode_paiement_facturation.libelle AS libelle_mode_paiement, SUM(c_facturation_lot.montant_ht_facture) AS sommeht, c_dossier.client_id');
        $this->db->from('c_facturation_lot');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_facturation_lot.dossier_id');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement');
        $this->db->where('c_facturation_lot.annee', $annee);
        $this->db->where('c_facturation_lot.c_facturation_status', 1);
        $this->db->where_in('c_facturation_lot.dossier_id', $table);
        $this->db->group_by('c_dossier.dossier_id');
        $query = $this->db->get();
        return $query->result();
    }
}
