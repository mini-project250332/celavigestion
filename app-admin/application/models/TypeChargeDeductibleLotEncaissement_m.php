<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TypeChargeDeductibleLotEncaissement_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_charge_deductible_lot_encaissement";
    protected $_primary_key = "tpchrg_deduct_id";
    protected $_order = "";
    protected $_filter = "intval";

}
