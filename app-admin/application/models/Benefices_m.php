<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Benefices_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_benefices";
    protected $_primary_key = "id_benefices ";
    protected $_order = "";
    protected $_filter = "intval";

    public function get_data_benefices($dossier_id)
    {
        $res = $this->db->select("c_benefices.id_benefices ,benefices_annee, benefices, GROUP_CONCAT(deficit_utilise) as deficit_utilise, GROUP_CONCAT(deficits_annee) as deficits_annee")
            ->from($this->_table)
            ->join('c_benefice_deficits','c_benefice_deficits.id_benefices = c_benefices.id_benefices ','left')
            ->join('c_deficits','c_deficits.id_deficits = c_benefice_deficits.id_deficits ','left')
            ->where('c_benefices.dossier_id', $dossier_id)
            ->group_by('c_benefices.benefices_annee')
            ->get()
            ->result();
        return $res;
    }
}