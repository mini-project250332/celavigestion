<?php

use phpDocumentor\Reflection\Types\Null_;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Transfer_comptable_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_transfert_comptable";
    protected $_primary_key = "transcomp_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
