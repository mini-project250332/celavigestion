<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Deficits_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_deficits";
    protected $_primary_key = "id_deficits ";
    protected $_order = "";
    protected $_filter = "intval";

    public function get_data_deficite($dossier_id)
    {
        $res = $this->db->select("c_deficits.id_deficits as id_deficits , deficits_annee, deficite_anterieur, GROUP_CONCAT(deficit_utilise) as deficit_utilise, GROUP_CONCAT(benefices_annee) as benefices_annee")
            ->from($this->_table)
            ->join('c_benefice_deficits','c_benefice_deficits.id_deficits  = c_deficits.id_deficits','left')
            ->join('c_benefices','c_benefices.id_benefices   = c_benefice_deficits.id_benefices','left')
            ->where('c_deficits.dossier_id', $dossier_id)
            ->group_by('c_deficits.deficits_annee')
            ->get()
            ->result();
        return $res;
    }

    public function get_data_deficite_update($dossier_id)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->join('c_benefice_deficits','c_benefice_deficits.id_deficits  = c_deficits.id_deficits','left')
            ->join('c_benefices','c_benefices.id_benefices   = c_benefice_deficits.id_benefices','left')
            ->where('c_deficits.dossier_id', $dossier_id)
            ->group_by('c_deficits.deficits_annee')
            ->get()
            ->result();
        return $res;
    } 
}