<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cron_reindexation_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_cron_reindex_indice";
    protected $_primary_key = "cron_index_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
