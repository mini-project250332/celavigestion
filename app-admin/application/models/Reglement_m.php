<?php

use phpDocumentor\Reflection\Types\Null_;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reglement_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_reglements";
    protected $_primary_key = "idc_reglements";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function getPrelevement($reglement)
    {
        $res = $this->db->select(
            'c_dossier.dossier_id,
                GROUP_CONCAT(DISTINCT CONCAT(c_client.client_nom, " ", c_client.client_prenom) SEPARATOR ", ") AS clients,
                c_facture.fact_id, 
                c_facture.fact_num, 
                c_reglements.montant_total, 
                c_dossier.iban, 
                c_dossier.bic, 
                c_mandat_sepa.sepa_numero,
                c_mandat_sepa.sepa_date_signature,
                c_reglements.idc_reglements,
                c_reglements.idc_prelevement',

            false
        )
            ->from($this->_table)
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.	idc_reglements')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)')
            ->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id')
            ->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id')
            ->join('c_prelevement', 'c_prelevement.idc_prelevement = c_reglements.idc_prelevement', 'left')
            ->where_in('c_reglements.idc_reglements', $reglement)
            ->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero')
            ->get()
            ->result();
        return $res;
    }

    function listereglement($clause)
    {
        $res = $this->db->select(
            'c_dossier.dossier_id,
                GROUP_CONCAT(DISTINCT CONCAT(c_client.client_nom, " ", c_client.client_prenom) SEPARATOR ", ") AS clients,
                c_facture.fact_id, 
                c_facture.fact_num, 
                c_reglements.montant_total, 
                c_dossier.iban, 
                c_dossier.bic, 
                c_mandat_sepa.sepa_numero,
                c_mandat_sepa.sepa_date_signature,
                c_reglements.idc_reglements,
                c_reglements.idc_prelevement,
                c_mode_paiement_facturation.libelle',
            false
        )
            ->from($this->_table)
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.	idc_reglements')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)')
            ->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id')
            ->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id')
            ->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_reglements.mode_reglement')
            ->join('c_prelevement', 'c_prelevement.idc_prelevement = c_reglements.idc_prelevement', 'left')
            ->where('c_facture.fact_annee', $clause['fact_annee'])
            ->where('c_facture.fact_etatpaiement', $clause['fact_etatpaiement'])
            ->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero')
            ->get()
            ->result();
        return $res;
    }

    function getPrelevementParIdPrelevement($idprelevement)
    {
        $res = $this->db->select(
            'c_dossier.dossier_id,
            GROUP_CONCAT(DISTINCT CONCAT(c_client.client_nom, " ", c_client.client_prenom) SEPARATOR ", ") AS clients,
            c_facture.fact_id, 
            c_facture.fact_num, 
            c_reglements.montant_total, 
            c_dossier.iban, 
            c_dossier.bic, 
            c_mandat_sepa.sepa_numero,
            c_mandat_sepa.sepa_date_signature,
            c_reglements.idc_reglements,
            c_reglements.idc_prelevement'
        )
            ->from($this->_table)
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.idc_reglements')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->join('c_dossier', 'c_dossier.dossier_id = c_facture.dossier_id')
            ->join('(SELECT dossier_id, client_id FROM c_dossier) AS d', 'd.dossier_id = c_dossier.dossier_id')
            ->join('c_client', 'FIND_IN_SET(c_client.client_id, d.client_id)')
            ->join('(SELECT dossier_id, MAX(sepa_id) AS max_sepa_id FROM c_mandat_sepa GROUP BY dossier_id) AS latest_mandat_sepa', 'latest_mandat_sepa.dossier_id = c_dossier.dossier_id')
            ->join('c_mandat_sepa', 'c_mandat_sepa.sepa_id = latest_mandat_sepa.max_sepa_id')
            ->join('c_prelevement', 'c_prelevement.idc_prelevement = c_reglements.idc_prelevement', 'left')
            ->where('c_prelevement.idc_prelevement', $idprelevement)
            ->group_by('c_dossier.dossier_id, c_facture.fact_id, c_facture.fact_num, c_dossier.iban, c_dossier.bic, c_mandat_sepa.sepa_numero')
            ->get()
            ->result();
        return $res;
    }

    function getTotalReglementDu($idregelementDu)
    {
        $this->db->select_sum('montant_total', 'montant_total');
        $this->db->from('c_reglements');
        $this->db->where('id_reglementDu', $idregelementDu);
        $query = $this->db->get();
        $result = $query->row();
        $total_montantDu = $result->montant_total != NULL ? $result->montant_total : 0;
        return $total_montantDu;
    }

    function verifierRemboursementTropPerçus($idregelementDu)
    {
        $this->db->select_sum('c_reglementventilation.montant', 'montant_total');
        $this->db->from('c_reglements');
        $this->db->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.idc_reglements');
        $this->db->where('c_reglements.id_reglementDu', $idregelementDu);
        $this->db->where('c_reglementventilation.id_facture', null);
        // $this->db->group_by('c_reglements.id_reglementDu');
        $query = $this->db->get();
        $result = $query->row();
        $total_montantDu = $result->montant_total != NULL ? $result->montant_total : 0;
        return $total_montantDu;
    }


    function getFactureReglement($idreglement)
    {
        $res = $this->db->select(
            'c_facture.fact_id, 
            c_facture.fact_num,
            c_facture.dossier_id,
            c_facture.fact_modepaiement'
        )
            ->from('c_reglements')
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.idc_reglements')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->where('c_reglements.idc_reglements', $idreglement)
            ->get()
            ->row();
        return $res;
    }

    function checkRejetfacture($idreglementAVerifier)
    {
        $res = $this->db->select('*')
            ->from('c_reglements')
            ->where('c_reglements.reglement_type', 2)
            ->where('c_reglements.id_reglementDu', $idreglementAVerifier)
            ->get()
            ->result();
        return $res;
    }

    function getReglementBydate($date)
    {
        $this->db->select('c_facture.fact_num, c_facture.fact_date, c_facture.fact_montantttc,
        c_reglements.idc_reglements, c_reglements.dossier_id, c_reglements.date_reglement, c_reglements.montant_total, c_reglements.idc_prelevement, c_reglements.reglement_type, c_reglements.mode_reglement,
        c_banque.banque_code_journal, c_banque.banque_cpt_comptable,
        c_client.client_nom, c_client.client_prenom')
            ->from('c_reglements')
            ->join('c_reglementventilation', 'c_reglementventilation.idc_reglements = c_reglements.idc_reglements')
            ->join('c_banque', 'c_banque.banque_id = c_reglements.id_banque')
            ->join('c_facture', 'c_facture.fact_id = c_reglementventilation.id_facture')
            ->join('c_facture_articles', 'c_facture_articles.fact_id = c_facture.fact_id')
            ->join('c_article', 'c_article.art_id = c_facture_articles.art_id')
            ->join('c_lot_client', 'c_lot_client.cliLot_id = c_facture_articles.lot_id')
            ->join('c_client', 'c_client.client_id = c_lot_client.cli_id')
            ->where('c_reglements.date_reglement <= ', $date)
            ->where('c_reglements.transcomp_id IS NULL')
            ->group_by('c_reglements.idc_reglements')
            ->order_by('c_reglements.idc_prelevement ASC');
        $query = $this->db->get();
        return $query->result();
    }
}
