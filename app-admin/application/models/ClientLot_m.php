<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ClientLot_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_lot_client";
    protected $_primary_key = "cliLot_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function listLot($data_post)
    {
        $this->db->select("c_lot_client.cliLot_id, c_lot_client.cliLot_principale, c_lot_client.cli_id, 
        c_programme.progm_nom, c_programme.progm_ville, 
        c_gestionnaire.gestionnaire_nom, 
        c_typelot.typelot_libelle,
        c_dossier.client_id,
        c_client.client_nom, 
        CASE WHEN c_client.client_prenom IS NOT NULL AND c_client.client_prenom != '' 
             THEN CONCAT(c_client.client_nom, ' ', c_client.client_prenom) 
             ELSE c_client.client_nom 
        END AS client")
            ->from($this->_table)
            ->where('c_lot_client.cliLot_etat', 1)
            ->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id')
            ->join('c_gestionnaire', 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id', 'left')
            ->join('c_typelot', 'c_typelot.typelot_id = c_lot_client.typelot_id', 'left')
            ->join('c_dossier', 'c_dossier.dossier_id = c_lot_client.dossier_id', 'left')
            ->join('c_taxe_fonciere_lot', 'c_taxe_fonciere_lot.cliLot_id = c_lot_client.cliLot_id', 'left')
            ->join('c_bail', 'c_bail.cliLot_id = c_lot_client.cliLot_id', 'left')
            ->join('c_mandat_new', 'c_mandat_new.cliLot_id = c_lot_client.cliLot_id', 'left')
            ->join('c_client', 'c_client.client_id = c_lot_client.cli_id');

        if ($data_post['charge_client'] != '' && $data_post['charge_client'] != -1) {
            $this->db->where('c_dossier.util_id', $data_post['charge_client']);
        }

        if ($data_post['gestionnaire_id'] == "*") {
            $this->db->where('c_bail.cliLot_id IS NULL');
        }

        if ($data_post['gestionnaire_id'] != "*" && ($data_post['gestionnaire_id'] != '' || $data_post['gestionnaire_id'] != 0)) {
            $this->db->where('c_lot_client.gestionnaire_id', $data_post['gestionnaire_id']);
        }

        if ($data_post['filtre_tom'] != 'Tous' && $data_post['filtre_tom'] != 'non_renseigne') {
            $this->db->where('c_taxe_fonciere_lot.taxe_valide', $data_post['filtre_tom']);
            $this->db->where('c_taxe_fonciere_lot.annee', $data_post['annee_tom']);
        }

        if ($data_post['charge_client'] == "-1") {
            $this->db->where('c_dossier.util_id IS NULL');
        }

        if ($data_post['filtre_lot'] == "1") {
            $this->db->where('c_mandat_new.etat_mandat_id != 6');
        }

        if ($data_post['filtre_lot'] == "2") {
            $this->db->where('c_mandat_new.etat_mandat_id = 6');
        }

        if ($data_post['bail_valid'] == "1") {
            $this->db->where('c_bail.bail_valide', $data_post['bail_valid']);
        }

        if ($data_post['bail_valid'] == "0") {
            $this->db->where('c_bail.bail_valide', $data_post['bail_valid']);
        }

        $this->db->group_by('c_lot_client.cliLot_id');
        $res = $this->db->get()->result();
        return $res;
    }

    function getLotFacturation($annee, $dossier_id)
    {
        $this->db->select('c_lot_client.cliLot_id, c_programme.progm_nom, c_facturation_lot.montant_ht_facture, c_facturation_lot.montant_ht_figurant, c_mode_paiement_facturation.id_mode_paie, c_mode_paiement_facturation.libelle, c_dossier.iban, c_dossier.bic, c_dossier.file_rib, c_dossier.dossier_id');
        $this->db->from('c_lot_client');
        $this->db->join('c_dossier', 'c_dossier.dossier_id = c_lot_client.dossier_id', 'left');
        $this->db->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id');
        $this->db->join('c_facturation_lot', 'c_facturation_lot.cliLot_id = c_lot_client.cliLot_id', 'left');
        $this->db->join('c_mode_paiement_facturation', 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement', 'left');
        $this->db->where('cliLot_etat', 1);
        $this->db->where('c_lot_client.dossier_id', $dossier_id);
        $this->db->group_start();
        $this->db->where('c_facturation_lot.annee', $annee);
        $this->db->or_where('c_facturation_lot.id_fact_lot IS NULL');
        $this->db->group_end();

        $res = $this->db->get()->result();
        return $res;
    }

    function getListlot($proprietaire, $programme, $pagination_data)
    {
        $this->db->select("
        CONCAT(c_utilisateur.util_prenom, ' ', c_utilisateur.util_nom) AS user,
        c_dossier.dossier_id, 
        c_mandat_new.etat_mandat_id, 
        c_lot_client.cliLot_id, 
        c_programme.progm_nom,
        c_client.client_id,
        c_client.client_nom, 
        c_client.client_prenom")
            ->from($this->_table)
            ->join('c_mandat_new', 'c_mandat_new.cliLot_id = c_lot_client.cliLot_id', 'left')
            ->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_lot_client.dossier_id')
            ->join('c_utilisateur', 'c_utilisateur.util_id = c_dossier.util_id', 'left')
            ->join('c_client', 'c_client.client_id = c_dossier.client_id')
            ->where('c_lot_client.cliLot_etat', 1)
            ->group_start()
            ->where_not_in('c_mandat_new.etat_mandat_id', array(3))
            ->or_where('c_mandat_new.cliLot_id IS NULL')
            ->group_end();

        if ($proprietaire != null) {
            $this->db->like('c_client.client_nom', $proprietaire);
        }

        if ($programme != null) {
            $this->db->like('c_programme.progm_nom', $programme);
        }

        $this->db->limit($pagination_data[1]);
        $this->db->offset($pagination_data[0]);

        $res = $this->db->get()->result();
        return $res;
    }

    function getListlotTotal($proprietaire, $programme)
    {
        $this->db->select("
        c_dossier.dossier_id, 
        c_mandat_new.etat_mandat_id, 
        c_lot_client.cliLot_id, 
        c_programme.progm_nom,
        c_client.client_nom, 
        c_client.client_prenom")
            ->from($this->_table)
            ->join('c_mandat_new', 'c_mandat_new.cliLot_id = c_lot_client.cliLot_id', 'left')
            ->join('c_programme', 'c_programme.progm_id = c_lot_client.progm_id')
            ->join('c_dossier', 'c_dossier.dossier_id = c_lot_client.dossier_id')
            ->join('c_client', 'c_client.client_id = c_dossier.client_id')
            ->where('c_lot_client.cliLot_etat', 1)
            ->group_start()
            ->where_not_in('c_mandat_new.etat_mandat_id', array(3))
            ->or_where('c_mandat_new.cliLot_id IS NULL')
            ->group_end();

        if ($proprietaire != null) {
            $this->db->like('c_client.client_nom', $proprietaire);
        }

        if ($programme != null) {
            $this->db->like('c_programme.progm_nom', $programme);
        }

        $res = $this->db->get()->result();
        return $res;
    }
}
