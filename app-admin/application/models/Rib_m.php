<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rib_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_rib";
    protected $_primary_key = "rib_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
