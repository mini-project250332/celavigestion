<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AccueilIntranet_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_message_accueil_intranet";
    protected $_primary_key = "mess_id";
    protected $_order = "";
    protected $_filter = "intval";
}
