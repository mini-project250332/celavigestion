<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Article_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_article";
    protected $_primary_key = "art_id";
    protected $_order = "";
    protected $_filter = "intval";

}
