<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modelmail_m extends ADM_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_param_modelemail";
    protected $_primary_key = "pmail_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}