<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facture extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facture";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/historique/historique.js"
        );

        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function get_facture_loyer()
    {
        $this->load->helper("exportation");
        $this->load->model('FactureLoyer_m', 'Facture');
        $this->load->model('Rib_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST['data'];

                $param = array(
                    'clause' => array('fact_loyer_id' => intval($data_post['fact_loyer_id'])),
                    'limit' => 1,
                    'method' => 'row'
                );

                $get_facture = $this->Facture->get($param);

                if (!is_null($get_facture->periode_mens_id)) {
                    $_data['facture'] = $facture = $this->get_mensuel($get_facture->bail_id, $get_facture->fact_loyer_id);
                    $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_mens_libelle . '_' . $facture->emetteur_nom;
                    $view = "Facture/exportation_loyer/export_facture_mensuel";
                    $cliLot_id = $facture->cliLot_id;
                    $annee = $facture->facture_annee;
                } elseif (!is_null($get_facture->periode_timestre_id)) {
                    $_data['facture'] = $facture = $this->get_trimestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
                    $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_trimesrte_libelle . '_' . $facture->emetteur_nom;
                    $view = "Facture/exportation_loyer/export_facture_trimestre";
                    $cliLot_id = $facture->cliLot_id;
                    $annee = $facture->facture_annee;
                } elseif (!is_null($get_facture->periode_semestre_id)) {
                    $_data['facture'] = $facture = $this->get_semestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
                    $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_semestre_libelle . '_' . $facture->emetteur_nom;
                    $view = "Facture/exportation_loyer/export_facture_semestriel";
                    $cliLot_id = $facture->cliLot_id;
                    $annee = $facture->facture_annee;
                } else {
                    $_data['facture'] = $facture = $this->get_annuel($get_facture->bail_id, $get_facture->fact_loyer_id);
                    $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_annee_libelle . '_' . $facture->emetteur_nom;
                    $view = "Facture/exportation_loyer/export_facture_annuel";
                    $cliLot_id = $facture->cliLot_id;
                    $annee = $facture->facture_annee;
                }

                $data_retour = array(
                    'cliLot_id' => $cliLot_id,
                    'year' => $annee
                );

                $param_rib =  array(
                    'clause' => array(),
                    'order_by_columns' => "rib_id  DESC",
                    'method' => "row"
                );

                $_data['rib'] = $this->Rib_m->get($param_rib);

                $retour = retour(true, "success", $data_retour, array("message" => "Facturation  réussi"));
                echo json_encode($retour);
                // $this->load->view($view, $_data);

                if (isset($data_post['no_need_pdf']) && ($data_post['no_need_pdf'] == "true" || $data_post['no_need_pdf'] == true && $data_post['no_need_pdf'] == 1)) {

                } else {
                    exporation_pdf($view, $file_name, $_data, $cliLot_id, $annee, $data_post['fact_loyer_id']);
                }

            }
        }
    }

    private function get_mensuel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');

        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_mensuel' => 'c_periode_mensuel.periode_mens_id  = c_facture_loyer.periode_mens_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );
        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_semestriel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');

        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_semestre' => 'c_periode_semestre.periode_semestre_id = c_facture_loyer.periode_semestre_id ',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );

        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_trimestriel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');

        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_trimestre' => 'c_periode_trimestre.periode_timestre_id  = c_facture_loyer.periode_timestre_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );

        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_annuel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');

        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_annee' => 'c_periode_annee.periode_annee_id = c_facture_loyer.periode_annee_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );

        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }
}