<table class="table fs--1 mb-0">
  <thead class="bg-300 text-900">
    <tr class="text-center">
      <th scope="col" style="border-right: 1px solid #ffff;">Date</th>
      <th scope="col" style="border-right: 1px solid #ffff;">Utilisateur</th>
      <th scope="col" style="border-right: 1px solid #ffff;">Propriétaire/Dossier/Lot</th>
      <th scope="col" style="border-right: 1px solid #ffff;">Type</th>
      <th scope="col" style="border-right: 1px solid #ffff;">Commentaire</th>
      <th scope="col" style="border-right: 1px solid #ffff;">Durée(min)</th>
    </tr>
  </thead>
  <tbody>    
    <?php foreach ($saisie as $saisietemps): ?>
      <?php
      $type_saisie = $saisietemps->saisie_temps_id;
      $value_temps;
      switch ($type_saisie) {
        case 1:
          $value_temps="";
          break;
        case 2:
          $value_temps = $saisietemps->data_client->client_nom . ' ' . $saisietemps->data_client->client_prenom;
          break;
        case 3:
          $dossier_id = str_pad($saisietemps->data_dossier->dossier_id, 4, '0', STR_PAD_LEFT) ;
          $value_temps = $dossier_id. ' ' .'('.$saisietemps->data_dossier->client_nom. ' ' . $saisietemps->data_dossier->client_prenom.')';
          break;
        case 4:
          $value_temps = $saisietemps->data_lot->progm_nom . ' ' .'('. $saisietemps->data_lot->client_nom. ' ' . $saisietemps->data_lot->client_prenom .')';   
          break;        
      }
      
    ?>
    <tr class="text-center">
      <td><?=date("d/m/Y",strtotime(str_replace('/', '-',$saisietemps->travail_date)))?></td>
      <td><?=$saisietemps->util_prenom?></td>
      <td>
        <?=$value_temps;?>
      </td>
      <td><?=$saisietemps->type_travail_libelle?></td>
      <td><?=$saisietemps->travail_commentaire?></td>
      <td><?=$saisietemps->travail_duree?></td>
    </tr>    
    <?php endforeach; ?>
    <tr class="text-center bg-300">
      <td class="fw-bold">TOTAL</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td class="fw-bold"><?=$totalhours?></td>
    </tr>
  </tbody>
</table>