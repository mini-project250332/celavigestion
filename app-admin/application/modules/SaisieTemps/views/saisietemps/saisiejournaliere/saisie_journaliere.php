<div class="contenair-title">
	<div class="px-2">
		<input type="hidden" value="" id="client_id_pour_rafraichir">
		<h5>Consultation des temps effectués</h5>
	</div>
	<div class="px-2">
        <select class="form-select m-1 select_admin" id="util_id" name="util_id" style="width: 100%" style="visibility : hidden;">
            <?php foreach ($utilisateur as $key => $value): ?>
                <option value="<?= $value->id; ?>" <?=($value->id == $_SESSION['session_utilisateur']['util_id']) ? 'selected' : ''; ?>><?= $value->prenom; ?></option>
            <?php endforeach; ?>
        </select>
        <input class="form-control mb-0 calendar text-center" id="filtre_date" name="filtre_date" type="text">
	</div>
</div>

 <div class="" id="content-saisie">

 </div>

 <script>

	only_visuel_gestion(["1"],"select_admin");
   	$(document).ready(function () {
    	getListeTempsPasse();
	});

    flatpickr(".calendar",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
			defaultDate: moment().format('DD-MM-YYYY'),
		}
	);    

	$('#filtre_date').trigger('change');

 </script>