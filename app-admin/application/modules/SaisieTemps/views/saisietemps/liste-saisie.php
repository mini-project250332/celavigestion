<div class="contenair-content">

	<ul class="nav nav-tabs" id="saisieTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-journalier" data-bs-toggle="tab" href="#tab-journalier" role="tab" aria-controls="tab-journalier" aria-selected="true">Vision journalière</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="hebdomadaire-tab" data-bs-toggle="tab" href="#tab-hebdomadaire" role="tab" aria-controls="tab-hebdomadaire" aria-selected="false">Vision hebdomadaire</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="mensuelle-tab" data-bs-toggle="tab" href="#tab-mensuelle" role="tab" aria-controls="tab-mensuelle" aria-selected="false">Vision mensuelle</a>
		</li>		
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="saisieTabContent">
		<div class="tab-pane fade show active" id="tab-journalier" role="tabpanel" aria-labelledby="journalier-tab">
            <?php $this->load->view('saisiejournaliere/saisie_journaliere'); ?>
		</div>
		<div class="tab-pane fade" id="tab-hebdomadaire" role="tabpanel" aria-labelledby="hebdomadaire-tab">
			
		</div>
		<div class="tab-pane fade" id="tab-mensuelle" role="tabpanel" aria-labelledby="mensuelle-tab">
			
		</div>		
	</div>
</div>