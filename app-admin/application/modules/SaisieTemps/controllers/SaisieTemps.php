<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SaisieTemps extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "SaisieTemps";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/saisietemps/saisietemps.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "saisietemps";
        $this->_data['sous_module'] = "saisietemps/contenaire-saisie";
        $this->render('contenaire');
    }

    public function listeTempsPasse()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['utilisateur'] = $this->getListUser();
                $this->renderComponant("SaisieTemps/saisietemps/liste-saisie",$data);
            }
        }
    }

    public function listeTempsJournaliere()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TravailTemps_m');
                $data_post = $_POST['data'];
                $clause = array();

                if ($data_post['filtre_date'] != '') {                    
                    $clause['travail_date'] = date_sql($data_post['filtre_date'], '-', 'jj/MM/aaaa');                    
                }

                if ($data_post['util_id'] != '') {                    
                    $clause['c_utilisateur.util_id'] = $data_post['util_id'];                   
                }

                $params = array(
                    'columns' => array('travail_id', 'travail_date', 'type_travail_libelle','travail_commentaire', 'travail_duree', 'cliLot_id', 'dossier_id', 'client_id', 'c_utilisateur.util_id', 'util_prenom', 'c_type_saisie_temps.saisie_temps_id', 'c_type_travail.type_travail_id'),
                    'clause' =>$clause,
                    'join' => array(
                        'c_utilisateur' => 'c_utilisateur.util_id = c_travail_effectue.util_id',
                        'c_type_travail' => 'c_type_travail.type_travail_id = c_travail_effectue.type_travail_id',
                        'c_type_saisie_temps' => 'c_type_saisie_temps.saisie_temps_id = c_travail_effectue.saisie_temps_id',
                    )
                );

                $request = $this->TravailTemps_m->get($params);

                $result = array();
                foreach ($request as $key => $value) {

                  switch ($value->saisie_temps_id) {
                    case 1:
                        $result[] = $request[$key];
                    break;
                    case 2:
                        $request[$key]->data_client = $this->getData_client($value->client_id);
                        break;
                    case 3:
                        $request[$key]->data_dossier = $this->getData_dossier($value->dossier_id);
                    break;
                    case 4:
                        $request[$key]->data_lot = $this->getData_lot($value->cliLot_id);
                    break;                    
                   
                  }
                   
                }

                $params_total  = array(
                    'columns' => array('SUM(travail_duree) as total','c_utilisateur.util_id'),
                    'clause' => $clause,
                    'join' => array( 'c_utilisateur' => 'c_utilisateur.util_id = c_travail_effectue.util_id')
                );                
                $request_total = $this->TravailTemps_m->get($params_total);

                if (!empty($request_total)) {                
                    $totalSaisie = $request_total[0]->total;                    
                    $data['totalhours'] = intdiv($totalSaisie, 60).'H'. ($totalSaisie % 60);
                 }

                $data['saisie'] = $request;

                $this->renderComponant("SaisieTemps/saisietemps/saisiejournaliere/liste-saisie-journaliere", $data);
            }
        }
    }

    public function getData_dossier($dossier_id)
    {
        $this->load->model('Dossier_m');
        $params = array(
            'clause' => array('dossier_id' => $dossier_id),
            'columns' => array('dossier_id', 'c_client.client_id', 'client_nom', 'client_prenom'),
            'method' => 'row',
            'join' => array(
                'c_client' => 'c_client.client_id = c_dossier.client_id',
            ),
        );
        $request = $this->Dossier_m->get($params);
        return $request;
    }

    public function getData_lot($cliLot_id)
    {
        $this->load->model('ClientLot_m');
        $params = array(
            'clause' => array('cliLot_id' => $cliLot_id),
            'method' => 'row',
            'join' => array(
                'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id',
            ),
        );
        $request = $this->ClientLot_m->get($params);
        return $request;
    }

    public function getTypeTravail()
    {

        $this->load->model('TypeTravail_m');
        $params = array(
            'columns' => array('type_travail_id', 'type_travail_libelle'),
        );
        $request = $this->TypeTravail_m->get($params);
        return $request;
    }

    /******Temps général********/

    public function FormSaisieTemps()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['typeTravail'] = $this->getTypeTravail();
            }

            $this->renderComponant("Clients/clients/form-saisietemps", $data);
        }
    }

    public function AjoutTempsGeneral()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('TravailTemps_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $data['travail_date'] = date('Y-m-d H:i:s', strtotime($data['travail_date']));
                    $data['saisie_temps_id'] = 1;
                   
                    $request = $this->TravailTemps_m->save_data($data);
                    if (!empty($request)) {
                        $retour = retour(true, "success", array("message" => "Temps ajouté avec succès","duree"=>$data['travail_duree']));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***********Saisie temps sur un client***********/

    public function FormSaisieTempsClient()
    {
        if (is_ajax()) {
            $this->load->model('Client_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('client_id' => $data_post['client_id']),
                    'method' => "row",
                    'columns' => array('client_id', 'client_nom', 'client_prenom', 'client_entreprise'),
                );

                $data['client_id'] = $data_post['client_id'];
                $data['client'] = $this->Client_m->get($params);
                $data['typeTravail'] = $this->getTypeTravail();
            }

            $this->renderComponant("Clients/clients/form-saisieTempsClient", $data);
        }
    }

    public function AjoutTempsClient()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('TravailTemps_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $data['travail_date'] = date('Y-m-d H:i:s', strtotime($data['travail_date']));
                    $data['saisie_temps_id'] = 2;
                   
                    $request = $this->TravailTemps_m->save_data($data);
                    if (!empty($request)) {
                        $retour = retour(true, "success", array("message" => "Temps ajouté avec succès","duree"=>$data['travail_duree']));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***********Saisie temps sur un dossier***********/

    public function FormSaisieTempsDossier()
    {
        if (is_ajax()) {
            $this->load->model('Dossier_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('dossier_id' => $data_post['dossier_id']),
                    'method' => "row",
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                    )
                );

                $data['dossier_id'] = $data_post['dossier_id'];
                $data['dossier'] = $this->Dossier_m->get($params);
                $data['typeTravail'] = $this->getTypeTravail();
            }

            $this->renderComponant("Clients/clients/dossier/form-saisieTempsDossier", $data);
        }
    }

    public function AjoutTempsDossier()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('TravailTemps_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $data['travail_date'] = date('Y-m-d H:i:s', strtotime($data['travail_date']));
                    $data['saisie_temps_id'] = 3;

                    $request = $this->TravailTemps_m->save_data($data);
                    if (!empty($request)) {
                        $retour = retour(true, "success", array("message" => "Temps ajouté avec succès","duree"=>$data['travail_duree']));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***********Saisie temps sur un lot***********/

    public function FormSaisieTempsLot()
    {
        if (is_ajax()) {
            $this->load->model('ClientLot_m');
            $this->load->model('Client_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'method' => "row",
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    )
                );

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['lot'] = $this->ClientLot_m->get($params);
                $data['typeTravail'] = $this->getTypeTravail();

                $data['client'] = $this->Client_m->get(
                    array('clause' => array("client_id" => $data_post['client_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/form-saisieTempsLot", $data);
        }
    }

    public function AjoutTempsLot()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('TravailTemps_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $data['travail_date'] = date('Y-m-d H:i:s', strtotime($data['travail_date']));
                    $data['saisie_temps_id'] = 4;
                    // $round5 = 5;
                    // $data['travail_duree'] = round(($data['travail_duree'] + $round5 / 2) / $round5) * $round5;

                    $request = $this->TravailTemps_m->save_data($data);
                    if (!empty($request)) {
                        $retour = retour(true, "success", array("message" => "Temps ajouté avec succès","duree"=>$data['travail_duree']));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
