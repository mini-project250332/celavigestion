<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Carbon\Carbon;

class Acte extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Acte";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/acte/acte.js",
            "js/historique/historique.js"
        );

        $this->_css_personnaliser = array(
            //"css/acte/acte.css"
        );
    }

    /**********Acte***********/

    public function listActe()
    {
        $this->load->model('Acte_m');
        $this->load->model('Immo_m');
        $this->load->model('ClientLot_m');
        $this->load->model('MandatNew_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $data['mandat'] = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'method' => 'row',
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id  = c_mandat_new.util_id'),
                        'join_orientation' => 'left'
                    )
                );

                $request_lot = $this->ClientLot_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                        ),
                    )
                );

                $data['client'] = $client = $this->ClientLot_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                        'columns' => array('c_client.*'),
                        'join' => array(
                            'c_client' => 'c_client.client_id = c_lot_client.cli_id'
                        ),
                    )
                );

                $this->load->model('HistoriqueVerifieIdentite_m');
                $data['data_verification_identite'] = $data_verification_identite = $this->HistoriqueVerifieIdentite_m->get(
                    array(
                        'clause' => array('client_id' => $client->client_id),
                        'columns' => array('historique_verificationidentite.*', 'util_prenom', 'util_nom'),
                        'join' => array(
                            'c_utilisateur' => 'c_utilisateur.util_id  = historique_verificationidentite.util_id'
                        ),
                    )
                );

                $taux_terrain = '10%';
                $taux_construction = '90%';
                $taux_cp = false;

                $cp = array('75', '78', '95', '91', '77', '92', '93', '94');
                $cp_commune = array(
                    '59',
                    '62',
                    '80',
                    '76',
                    '27',
                    '14',
                    '50',
                    '35',
                    '22',
                    '29',
                    '56',
                    '44',
                    '85',
                    '17',
                    '33',
                    '40',
                    '64',
                    '971',
                    '972',
                    '973',
                    '97500',
                    '66',
                    '11',
                    '34',
                    '30',
                    '13',
                    '83',
                    '06',
                    '2B',
                    '2A'
                );
                $cp_metropole = array(
                    'BORDEAUX',
                    'BREST',
                    'CLERMONT-FERRAND',
                    'DIJON',
                    'GRENOBLE',
                    'LILLE',
                    'LYON',
                    'MARSEILLE',
                    'METZ',
                    'MONTPELLIER',
                    'NANCY',
                    'NANTES',
                    'NICE',
                    'ORLÉANS',
                    'RENNES',
                    'ROUEN',
                    'SAINT ÉTIENNE',
                    'STRASBOURG',
                    'TOULOUSE',
                    'TOULON',
                    'TOURS'
                );

                foreach ($cp as $key => $value) {
                    if (substr($request_lot->progm_cp, 0, 2) === $value) {
                        $taux_terrain = '40%';
                        $taux_construction = '60%';
                        $taux_cp = true;
                    }
                   
                }

                if ($taux_cp == false) {
                    foreach ($cp_commune as $key => $value) {
                        if ((substr($request_lot->progm_cp, 0, 2)) === $value || (substr($request_lot->progm_cp, 0, 3) === $value)) {
                            $taux_terrain = '30%';
                            $taux_construction = '70%';
                            $taux_cp = true;
                        }
                    }
                }

                if ($taux_cp == false) {
                    foreach ($cp_metropole as $key => $value) {
                        if ($request_lot->progm_ville === $value) {
                            $taux_terrain = '30%';
                            $taux_construction = '70%';
                            $taux_cp = true;
                        }
                    }
                }

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );               
                
                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $data['acte'] = (!empty($request_acte) ? $request_acte : []);
                if (!empty($data['acte'])) {
                    $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                    $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                    $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                    $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                    $diff_in_days = (round($datediff / (60 * 60 * 24)));
                    // /********Annuité*********/
                    $annuiteOeuvre = (($data['acte']->acte_valeur_construction_ht * 40) / 100) / 50;
                    $annuiteFacade = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 20;
                    $annuiteInstallation = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 15;
                    $annuiteAgencement = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 10;
                    $annuiteMobilier = ($data['acte']->acte_valeur_mobilier_ht) / 7;

                    $data['annuiteOeuvre'] = $annuiteOeuvre;
                    $data['annuiteFacade'] = $annuiteFacade;
                    $data['annuiteInstallation'] = $annuiteInstallation;
                    $data['annuiteAgencement'] = $annuiteAgencement;
                    $data['annuiteMobilier'] = $annuiteMobilier;

                    // /**********Première année*******/
                    $premiereAnneeOeuvre = round($annuiteOeuvre / 365 * $diff_in_days);
                    $premiereAnneeFacade = round($annuiteFacade / 365 * $diff_in_days);
                    $premiereAnneeInstallation = round($annuiteInstallation / 365 * $diff_in_days);
                    $premiereAnneeAgencement = round($annuiteAgencement / 365 * $diff_in_days);
                    $premiereAnneeMobilier = round($annuiteMobilier / 365 * $diff_in_days);

                    $annee_prec = date("Y") - 1;
                    $diffAnnee = $annee_prec - $acte_annee;

                    $amortOeuvre = null;
                    $amortFacade = null;
                    $amortInstallation = null;
                    $amortAgencement = null;
                    $amortMobilier = null;

                    $lastamortOeuvre = null;
                    $lastamortFacade = null;
                    $lastamortInstallation = null;
                    $lastamortAgencement = null;
                    $lastamortMobilier = null;

                    $resteAmortOeuvre = null;
                    $resteAmortFacade = null;
                    $resteAmortInstallation = null;
                    $resteAmortAgencement = null;
                    $resteAmortMobilier = null;

                    if ($acte_annee >= $annee_prec) {
                        $lastamortOeuvre = $annuiteOeuvre * 0;
                        $amortOeuvre = 0;
                    } else {
                        $lastamortOeuvre = $annuiteOeuvre * ($diffAnnee - 1);
                        $amortOeuvre = round($lastamortOeuvre + $premiereAnneeOeuvre);
                    }

                    if ($acte_annee >= $annee_prec) {
                        $lastamortFacade = $annuiteFacade * 0;
                        $amortFacade = 0;
                    } else {
                        $lastamortFacade = $annuiteFacade * ($diffAnnee - 1);
                        $amortFacade = round($lastamortFacade + $premiereAnneeFacade);
                    }

                    if ($acte_annee >= $annee_prec) {
                        $lastamortInstallation = $annuiteFacade * 0;
                        $amortInstallation = 0;
                    } elseif ($diffAnnee > 15) {
                        $lastamortInstallation = $annuiteInstallation * 14;
                        $amortInstallation = round($lastamortInstallation + $premiereAnneeFacade);
                    } else {
                        $lastamortInstallation = $annuiteInstallation * ($diffAnnee - 1);
                        $amortInstallation = round($lastamortInstallation + $premiereAnneeInstallation);
                    }

                    if ($acte_annee >= $annee_prec) {
                        $lastamortAgencement = $annuiteFacade * 0;
                        $amortAgencement = 0;
                    } elseif ($diffAnnee > 10) {
                        $lastamortAgencement = $annuiteAgencement * 9;
                        $amortAgencement = round($lastamortAgencement + $premiereAnneeAgencement);
                    } else {
                        $lastamortAgencement = $annuiteAgencement * ($diffAnnee - 1);
                        $amortAgencement = round($lastamortAgencement + $premiereAnneeAgencement);
                    }

                    if ($acte_annee >= $annee_prec) {
                        $lastamortMobilier = $annuiteMobilier * 0;
                        $amortMobilier = 0;
                    } elseif ($diffAnnee > 7) {
                        $lastamortMobilier = $annuiteMobilier * 6;
                        $amortMobilier = round($lastamortMobilier + $premiereAnneeMobilier);
                    } else {
                        $lastamortMobilier = $annuiteMobilier * ($diffAnnee - 1);
                        $amortMobilier = round($lastamortMobilier + $premiereAnneeMobilier);
                    }

                    if ($acte_annee >= $annee_prec) {
                        $resteAmortOeuvre = 0;
                        $resteAmortFacade = 0;
                        $resteAmortInstallation = 0;
                        $resteAmortAgencement = 0;
                        $resteAmortMobilier = 0;
                    } else {
                        $resteAmortOeuvre = round($request_immo->immo_gros_oeuvre - $amortOeuvre);
                        $resteAmortFacade = round($request_immo->immo_facade - $amortFacade);
                        $resteAmortInstallation = round($request_immo->immo_installation - $amortInstallation);
                        $resteAmortAgencement = round($request_immo->immo_agencement - $amortAgencement);

                        $resteAmortMobilier = !empty($request_immo->immo_mobilier) ? round(floatval($request_immo->immo_mobilier) - $amortMobilier) : 0;
                    }

                    $data['amortOeuvre'] = $amortOeuvre;
                    $data['amortFacade'] = $amortFacade;
                    $data['amortInstallation'] = $amortInstallation;
                    $data['amortAgencement'] = $amortAgencement;
                    $data['amortMobilier'] = $amortMobilier;

                    $data['resteAmortOeuvre'] = $resteAmortOeuvre;
                    $data['resteAmortFacade'] = $resteAmortFacade;
                    $data['resteAmortInstallation'] = $resteAmortInstallation;
                    $data['resteAmortAgencement'] = $resteAmortAgencement;
                    $data['resteAmortMobilier'] = $resteAmortMobilier;

                    $immo_mobilier = !empty($request_immo->immo_mobilier) ? floatval($request_immo->immo_mobilier) : 0;

                    $totalValeur = $request_immo->immo_gros_oeuvre + $request_immo->immo_facade + $request_immo->immo_installation + $request_immo->immo_agencement + $immo_mobilier;
                    $totalAmort = $amortOeuvre + $amortFacade + $amortInstallation + $amortAgencement + $amortMobilier;
                    $totalResteAmort = $resteAmortOeuvre + $resteAmortFacade + $resteAmortInstallation + $resteAmortAgencement + $resteAmortMobilier;

                    $data['totalValeur'] = $totalValeur;
                    $data['totalAmort'] = $totalAmort;
                    $data['totalResteAmort'] = $totalResteAmort;

                }

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['taux_terrain'] = $taux_terrain;
                $data['taux_construction'] = $taux_construction;
                $data['document'] = $this->getDocumentActe();

                $this->renderComponant("Clients/clients/lot/actes/list-acte", $data);
            }
        }
    }

    public function getDocumentActe()
    {

        $this->load->model('DocumentActe_m');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_acte_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_acte.util_id')
        );

        $request = $this->DocumentActe_m->get($params);
        return $request;
    }

    public function cruActe()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Acte_m');
                $this->load->model('Immo_m');

                $data_post = $_POST["data"];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action_acte'];
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    unset($data['action_acte']);
                    unset($data['undefined']);

                    $clause = null;

                    if ($action === "edit") {
                        $clause = array("acte_id" => $data['acte_id']);
                        $claus = array("cliLot_id" => $data['cliLot_id'], );
                        unset($data['acte_id']);
                        //unset($data['cliLot_id']);
                    }

                    $request = $this->Acte_m->save_data($data, $clause);

                    $data_retour['acte_id'] = ($action == "add") ? $request : $_POST['data']['acte_id']['value'];
                    $data_retour['action_acte'] = 'edit';

                    if ($action === "add") {
                        $data_immo = array(
                            'immo_gros_oeuvre' => 0,
                            'immo_facade' => 0,
                            'immo_installation' => 0,
                            'immo_agencement' => 0,
                            'immo_mobilier' => 0,
                            'cliLot_id' => $data['cliLot_id']
                        );

                        $request_immo = $this->Immo_m->save_data($data_immo);

                    } elseif (!empty($data['acte_valeur_construction_ht']) && $action === "edit") {
                        $data_immo = array(
                            'immo_gros_oeuvre' => ($data['acte_valeur_construction_ht'] * 40) / 100,
                            'immo_facade' => ($data['acte_valeur_construction_ht'] * 20) / 100,
                            'immo_installation' => ($data['acte_valeur_construction_ht'] * 20) / 100,
                            'immo_agencement' => ($data['acte_valeur_construction_ht'] * 20) / 100,
                            'immo_mobilier' => $data['acte_valeur_mobilier_ht'],
                            'cliLot_id' => $data['cliLot_id']
                        );

                        $request_immo = $this->Immo_m->save_data($data_immo, $claus);
                    }

                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Information enregistrée avec succès" : "Modification réussie"));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    public function formUploadDocActe()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/actes/formUploadDocActe', $data);
    }

    public function uploadfileActe()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/acte/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_acte_nom' => $original_name,
                'doc_acte_creation' => date('Y-m-d H:i:s'),
                'doc_acte_path' => str_replace('\\', '/', $filePath),
                'doc_acte_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentActe_m');
            $this->DocumentActe_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function FormUpdateActe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentActe_m');
                $data['document'] = $this->DocumentActe_m->get(
                    array('clause' => array("doc_acte_id" => $data_post['doc_acte_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/actes/formUpdateActe", $data);
        }
    }

    public function UpdateDocumentActe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentActe_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_acte_id = $data['doc_acte_id'];
                    unset($data['doc_acte_id']);

                    $clause = array("doc_acte_id" => $doc_acte_id);
                    $request = $this->DocumentActe_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDocActe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentActe_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_acte_id' => $data['doc_acte_id']),
                    'columns' => array('doc_acte_id'),
                    'method' => "row",
                );
                $request_get = $this->DocumentActe_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_acte_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_acte_id' => $data['doc_acte_id']);
                    $data_update = array('doc_acte_etat' => 0);
                    $request = $this->DocumentActe_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_acte_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /**********Immo*************/

    public function getActeImmo()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('Acte_m');
                $data['act_donnee'] = $this->Acte_m->get(
                    array('clause' => array("cliLot_id" => $data_post['cliLot_id']), 'method' => "row")
                );

                echo json_encode($data);
            }
        }
    }

    public function getImmoGeneral()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);
                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                // /********Annuité*********/

                $annuiteOeuvre = (($data['acte']->acte_valeur_construction_ht * 40) / 100) / 50;
                $annuiteFacade = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 20;
                $annuiteInstallation = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 15;
                $annuiteAgencement = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 10;
                $annuiteMobilier = ($data['acte']->acte_valeur_mobilier_ht) / 7;

                // /**********Première année*******/
                $premiereAnneeOeuvre = ($annuiteOeuvre / 365 * $diff_in_days);
                $premiereAnneeFacade = ($annuiteFacade / 365 * $diff_in_days);
                $premiereAnneeInstallation = ($annuiteInstallation / 365 * $diff_in_days);
                $premiereAnneeAgencement = ($annuiteAgencement / 365 * $diff_in_days);
                $premiereAnneeMobilier = ($annuiteMobilier / 365 * $diff_in_days);

                $derniereAnneeOeuvre = $annuiteOeuvre - $premiereAnneeOeuvre;
                $derniereAnneeFacade = $annuiteFacade - $premiereAnneeFacade;
                $derniereAnneeInstallation = $annuiteInstallation - $premiereAnneeInstallation;
                $derniereAnneeAgencement = $annuiteAgencement - $premiereAnneeAgencement;
                $derniereAnneeMobilier = $annuiteMobilier - $premiereAnneeMobilier;

                $totalAnnuite = $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $annuiteAgencement + $annuiteMobilier;
                $totalPremiereAnnee = $premiereAnneeOeuvre + $premiereAnneeFacade + $premiereAnneeInstallation + $premiereAnneeAgencement + $premiereAnneeMobilier;
                $totalAnnee7 = $premiereAnneeOeuvre + $premiereAnneeFacade + $premiereAnneeInstallation + $premiereAnneeAgencement + $derniereAnneeMobilier;
                $totalReste = $data['immo']->immo_gros_oeuvre + $data['immo']->immo_facade + $data['immo']->immo_installation + $data['immo']->immo_agencement + $data['immo']->immo_mobilier;

                $totalAnnee20 = $annuiteOeuvre + $derniereAnneeFacade;
                $totalAnnee19 = $annuiteOeuvre + $annuiteFacade;
                $totalAnnee15 = $annuiteOeuvre + $annuiteFacade + $derniereAnneeInstallation;
                $totalAnnee14 = $annuiteOeuvre + $annuiteFacade + $annuiteInstallation;
                $totalAnnee10 = $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $derniereAnneeAgencement;
                $totalAnnee9 = $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $annuiteAgencement;
                $totalAnnee7 = $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $annuiteAgencement + $derniereAnneeMobilier;

                $data['premiereAnneeOeuvre'] = $premiereAnneeOeuvre;
                $data['premiereAnneeFacade'] = $premiereAnneeFacade;
                $data['premiereAnneeInstallation'] = $premiereAnneeInstallation;
                $data['premiereAnneeAgencement'] = $premiereAnneeAgencement;
                $data['premiereAnneeMobilier'] = $premiereAnneeMobilier;

                $data['derniereAnneeOeuvre'] = $derniereAnneeOeuvre;
                $data['derniereAnneeFacade'] = $derniereAnneeFacade;
                $data['derniereAnneeInstallation'] = $derniereAnneeInstallation;
                $data['derniereAnneeAgencement'] = $derniereAnneeAgencement;
                $data['derniereAnneeMobilier'] = $derniereAnneeMobilier;

                $data['annuiteOeuvre'] = $annuiteOeuvre;
                $data['annuiteFacade'] = $annuiteFacade;
                $data['annuiteInstallation'] = $annuiteInstallation;
                $data['annuiteAgencement'] = $annuiteAgencement;
                $data['annuiteMobilier'] = $annuiteMobilier;
                $data['totalAnnuite'] = $totalAnnuite;
                $data['totalReste'] = $totalReste;

                $data['totalPremiereAnnee'] = $totalPremiereAnnee;
                $data['totalAnnee7'] = $totalAnnee7;
                $data['totalAnnee20'] = $totalAnnee20;
                $data['totalAnnee19'] = $totalAnnee19;
                $data['totalAnnee15'] = $totalAnnee15;
                $data['totalAnnee14'] = $totalAnnee14;
                $data['totalAnnee10'] = $totalAnnee10;
                $data['totalAnnee9'] = $totalAnnee9;
                $data['totalAnnee7'] = $totalAnnee7;

                $this->renderComponant("Clients/clients/lot/immo/immo_general/list-immo", $data);
            }
        }
    }

    public function formAlertImmo()
    {
        if (is_ajax()) {
            $data_post = $_POST["data"];
            $data['annee'] = $data_post['annee'];
            $data['oeuvre'] = $data_post['oeuvre'];
            $data['facade'] = $data_post['facade'];
            $data['installation'] = $data_post['installation'];
            $data['agence'] = $data_post['agence'];
            $data['mobilier'] = $data_post['mobilier'];
            $data['total'] = $data_post['total'];
            $data['cliLot_id'] = $data_post['cliLot_id'];

            $this->renderComponant("Clients/clients/lot/immo/immo_general/form-update", $data);
        }
    }

    public function formInfoImmo()
    {
        $this->load->model('ImmoGeneral_m');
        if (is_ajax()) {
            $data_post = $_POST["data"];
            $data['immo_general_id'] = $data_post['immo_general_id'];

            $params = array(
                'clause' => array('immo_general_id ' => $data_post['immo_general_id']),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_immo_general.util_id')
            );

            $data['info_immo'] = $this->ImmoGeneral_m->get($params);


            $this->renderComponant("Clients/clients/lot/immo/immo_general/form-info-immo", $data);
        }
    }


    public function UpdateImmo()
    {
        $this->load->model('ImmoGeneral_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);

                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    ;
                    $data_retour = array("cliLot_id" => $data_post['cliLot_id']);
                    $data['immo_general_date_modification'] = Carbon::now()->toDateTimeString();
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $request = $this->ImmoGeneral_m->save_data($data);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification réussie"));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    public function getImmoGrosOeuvre()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                $annuiteOeuvre = (($data['acte']->acte_valeur_construction_ht * 40) / 100) / 50;
                $data['annuiteOeuvre'] = $annuiteOeuvre;

                $premiereAnneeOeuvre = ($annuiteOeuvre / 365 * $diff_in_days);
                $data['premiereAnneeOeuvre'] = $premiereAnneeOeuvre;
                $data['document'] = $this->getDocumentGrosOeuvre();

                $this->renderComponant("Clients/clients/lot/immo/immo-gros-oeuvre/list-immo-gros-oeuvre", $data);
            }
        }
    }

    public function formUploadDocGrosOeuvre()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/immo/immo-gros-oeuvre/formUploadDocOeuvre', $data);
    }

    public function uploadfileOeuvre()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/immo/immo_gros_oeuvre/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_oeuvre_nom' => $original_name,
                'doc_oeuvre_creation' => date('Y-m-d H:i:s'),
                'doc_oeuvre_path' => str_replace('\\', '/', $filePath),
                'doc_oeuvre_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentOeuvre_m', 'Oeuvre');
            $this->Oeuvre->save_data($data);
            echo json_encode($data);
        }
    }

    public function getDocumentGrosOeuvre()
    {

        $this->load->model('DocumentOeuvre_m', 'Oeuvre');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_oeuvre_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_oeuvre.util_id')
        );

        $request = $this->Oeuvre->get($params);
        return $request;
    }

    public function removeDocOeuvre()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentOeuvre_m', 'Oeuvre');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_oeuvre_id' => $data['doc_oeuvre_id']),
                    'columns' => array('doc_oeuvre_id'),
                    'method' => "row",
                );
                $request_get = $this->Oeuvre->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_oeuvre_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_oeuvre_id' => $data['doc_oeuvre_id']);
                    $data_update = array('doc_oeuvre_etat' => 0);
                    $request = $this->Oeuvre->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_oeuvre_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateOeuvre()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentOeuvre_m', 'Oeuvre');
                $data['document'] = $this->Oeuvre->get(
                    array('clause' => array("doc_oeuvre_id" => $data_post['doc_oeuvre_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/immo/immo-gros-oeuvre/FormUpdateOeuvre", $data);
        }
    }

    public function UpdateDocumentOeuvre()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentOeuvre_m', 'Oeuvre');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_oeuvre_id = $data['doc_oeuvre_id'];
                    unset($data['doc_oeuvre_id']);

                    $clause = array("doc_oeuvre_id" => $doc_oeuvre_id);
                    $request = $this->Oeuvre->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocOeuvreTraitement()
    {
        $this->load->model('DocumentOeuvre_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_oeuvre_id' => $data_post['doc_oeuvre_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentOeuvre_m->get($params);
                if ($request->doc_oeuvre_traite == 1) {
                    $data['doc_oeuvre_traite'] = 0;
                    $data['doc_oeuvre_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_oeuvre_id' => $data_post['doc_oeuvre_id']);
                    $this->DocumentOeuvre_m->save_data($data, $clause);
                } else {
                    $data['doc_oeuvre_traite'] = 1;
                    $data['doc_oeuvre_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_oeuvre_id' => $data_post['doc_oeuvre_id']);
                    $this->DocumentOeuvre_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }


    public function getPathOeuvre()
    {

        $this->load->model('DocumentOeuvre_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_oeuvre_id', 'doc_oeuvre_path'),
                    'clause' => array('doc_oeuvre_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getImmoFacade()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                $annuiteFacade = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 20;
                $data['annuiteFacade'] = $annuiteFacade;

                $premiereAnneeFacade = ($annuiteFacade / 365 * $diff_in_days);
                $data['premiereAnneeFacade'] = $premiereAnneeFacade;
                $data['document'] = $this->getDocumentFacade();

                $this->renderComponant("Clients/clients/lot/immo/immo-facade/list-immo-facade", $data);
            }
        }
    }

    public function getDocumentFacade()
    {

        $this->load->model('DocumentFacade_m');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_facade_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_facade.util_id')
        );

        $request = $this->DocumentFacade_m->get($params);
        return $request;
    }

    public function formUploadDocFacade()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/immo/immo-facade/formUploadDocFacade', $data);
    }

    public function uploadfileFacade()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/immo/immo_facade/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_facade_nom' => $original_name,
                'doc_facade_creation' => date('Y-m-d H:i:s'),
                'doc_facade_path' => str_replace('\\', '/', $filePath),
                'doc_facade_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentFacade_m', 'Facade');
            $this->Facade->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeDocFacade()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentFacade_m', 'Facade');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_facade_id' => $data['doc_facade_id']),
                    'columns' => array('doc_facade_id'),
                    'method' => "row",
                );
                $request_get = $this->Facade->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_facade_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_facade_id' => $data['doc_facade_id']);
                    $data_update = array('doc_facade_etat' => 0);
                    $request = $this->Facade->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_facade_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateFacade()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentFacade_m', 'Facade');
                $data['document'] = $this->Facade->get(
                    array('clause' => array("doc_facade_id" => $data_post['doc_facade_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/immo/immo-facade/FormUpdateFacade", $data);
        }
    }

    public function UpdateDocumentFacade()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentFacade_m', 'Facade');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_facade_id = $data['doc_facade_id'];
                    unset($data['doc_facade_id']);

                    $clause = array("doc_facade_id" => $doc_facade_id);
                    $request = $this->Facade->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocFacadeTraitement()
    {
        $this->load->model('DocumentFacade_m', 'Facade');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_facade_id' => $data_post['doc_facade_id']
                    ),
                    'method' => "row"
                );
                $request = $this->Facade->get($params);
                if ($request->doc_facade_traite == 1) {
                    $data['doc_facade_traite'] = 0;
                    $data['doc_facade_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_facade_id' => $data_post['doc_facade_id']);
                    $this->Facade->save_data($data, $clause);
                } else {
                    $data['doc_facade_traite'] = 1;
                    $data['doc_facade_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_facade_id' => $data_post['doc_facade_id']);
                    $this->Facade->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function getPathFacade()
    {

        $this->load->model('DocumentFacade_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_facade_id', 'doc_facade_path'),
                    'clause' => array('doc_facade_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getImmoInstallation()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                $annuiteInstallation = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 15;
                $data['annuiteInstallation'] = $annuiteInstallation;

                $premiereAnneeInstallation = ($annuiteInstallation / 365 * $diff_in_days);
                $data['premiereAnneeInstallation'] = $premiereAnneeInstallation;
                $data['document'] = $this->getDocumentInstallation();

                $this->renderComponant("Clients/clients/lot/immo/immo-installation/list-immo-installation", $data);
            }
        }
    }

    public function getDocumentInstallation()
    {

        $this->load->model('DocumentInstallation_m', 'Installation');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_installation_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_installation.util_id')
        );

        $request = $this->Installation->get($params);
        return $request;
    }

    public function formUploadDocInstallation()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/immo/immo-installation/formUploadDocInstallation', $data);
    }

    public function uploadfileInstallation()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/immo/immo_installation/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_installation_nom' => $original_name,
                'doc_installation_creation' => date('Y-m-d H:i:s'),
                'doc_installation_path' => str_replace('\\', '/', $filePath),
                'doc_installation_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentInstallation_m', 'Installation');
            $this->Installation->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeDocInstallation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentInstallation_m', 'Installation');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_installation_id' => $data['doc_installation_id']),
                    'columns' => array('doc_installation_id'),
                    'method' => "row",
                );
                $request_get = $this->Installation->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_installation_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_installation_id' => $data['doc_installation_id']);
                    $data_update = array('doc_installation_etat' => 0);
                    $request = $this->Installation->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_installation_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateInstallation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentInstallation_m', 'Installation');
                $data['document'] = $this->Installation->get(
                    array('clause' => array("doc_installation_id" => $data_post['doc_installation_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/immo/immo-installation/FormUpdateInstallation", $data);
        }
    }

    public function UpdateDocumentInstallation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentInstallation_m', 'Installation');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_installation_id = $data['doc_installation_id'];
                    unset($data['doc_installation_id']);

                    $clause = array("doc_installation_id" => $doc_installation_id);
                    $request = $this->Installation->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocInstallationTraitement()
    {
        $this->load->model('DocumentInstallation_m', 'Installation');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_installation_id' => $data_post['doc_installation_id']
                    ),
                    'method' => "row"
                );
                $request = $this->Installation->get($params);
                if ($request->doc_installation_traite == 1) {
                    $data['doc_installation_traite'] = 0;
                    $data['doc_installation_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_installation_id' => $data_post['doc_installation_id']);
                    $this->Installation->save_data($data, $clause);
                } else {
                    $data['doc_installation_traite'] = 1;
                    $data['doc_installation_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_installation_id' => $data_post['doc_installation_id']);
                    $this->Installation->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function getPathInstallation()
    {

        $this->load->model('DocumentInstallation_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_installation_id', 'doc_installation_path'),
                    'clause' => array('doc_installation_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }


    public function getImmoAgencement()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                $annuiteAgencement = (($data['acte']->acte_valeur_construction_ht * 20) / 100) / 10;
                $data['annuiteAgencement'] = $annuiteAgencement;

                $premiereAnneeAgencement = ($annuiteAgencement / 365 * $diff_in_days);
                $data['premiereAnneeAgencement'] = $premiereAnneeAgencement;
                $data['document'] = $this->getDocumentAgencement();


                $this->renderComponant("Clients/clients/lot/immo/immo_agencement/list-immo-agencement", $data);
            }
        }
    }

    public function getDocumentAgencement()
    {

        $this->load->model('DocumentAgencement_m', 'Agencement');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_agencement_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_agencement.util_id')
        );

        $request = $this->Agencement->get($params);
        return $request;
    }

    public function formUploadDocAgencement()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/immo/immo_agencement/formUploadDocAgencement', $data);
    }

    public function uploadfileAgencement()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/immo/immo_agencement/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_agencement_nom' => $original_name,
                'doc_agencement_creation' => date('Y-m-d H:i:s'),
                'doc_agencement_path' => str_replace('\\', '/', $filePath),
                'doc_agencement_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentAgencement_m', 'Agencement');
            $this->Agencement->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeDocAgencement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentAgencement_m', 'Agencement');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_agencement_id' => $data['doc_agencement_id']),
                    'columns' => array('doc_agencement_id'),
                    'method' => "row",
                );
                $request_get = $this->Agencement->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_agencement_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_agencement_id' => $data['doc_agencement_id']);
                    $data_update = array('doc_agencement_etat' => 0);
                    $request = $this->Agencement->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_agencement_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateAgencement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentAgencement_m', 'Agencement');
                $data['document'] = $this->Agencement->get(
                    array('clause' => array("doc_agencement_id" => $data_post['doc_agencement_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/immo/immo_agencement/FormUpdateAgencement", $data);
        }
    }

    public function UpdateDocumentAgencement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentAgencement_m', 'Agencement');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_agencement_id = $data['doc_agencement_id'];
                    unset($data['doc_agencement_id']);

                    $clause = array("doc_agencement_id" => $doc_agencement_id);
                    $request = $this->Agencement->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocAgencementTraitement()
    {
        $this->load->model('DocumentAgencement_m', 'Agencement');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_agencement_id' => $data_post['doc_agencement_id']
                    ),
                    'method' => "row"
                );
                $request = $this->Agencement->get($params);
                if ($request->doc_agencement_traite == 1) {
                    $data['doc_agencement_traite'] = 0;
                    $data['doc_agencement_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_agencement_id' => $data_post['doc_agencement_id']);
                    $this->Agencement->save_data($data, $clause);
                } else {
                    $data['doc_agencement_traite'] = 1;
                    $data['doc_agencement_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_agencement_id' => $data_post['doc_agencement_id']);
                    $this->Agencement->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function getPathAgencement()
    {

        $this->load->model('DocumentAgencement_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_agencement_id', 'doc_agencement_path'),
                    'clause' => array('doc_agencement_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getImmoMobilier()
    {
        $this->load->model('Immo_m');
        $this->load->model('Acte_m');
        $this->load->model('ImmoGeneral_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_immo = $this->Immo_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_acte = $this->Acte_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row',
                    )
                );

                $request_general = $this->ImmoGeneral_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                $data['immo_general'] = (!empty($request_general) ? $request_general : []);

                $data['immo'] = (!empty($request_immo) ? $request_immo : []);
                $data['acte'] = (!empty($request_acte) ? $request_acte : []);

                $acte_date_debut_amortissement = strtotime($data['acte']->acte_date_debut_amortissement);
                $acte_annee = date("Y", strtotime($data['acte']->acte_date_debut_amortissement));
                $date_fin_amortissement = strtotime("Last day of December", strtotime($data['acte']->acte_date_debut_amortissement));
                $datediff = $date_fin_amortissement - $acte_date_debut_amortissement;
                $diff_in_days = (round($datediff / (60 * 60 * 24)));

                $annuiteMobilier = ($data['acte']->acte_valeur_mobilier_ht) / 7;
                $data['annuiteMobilier'] = $annuiteMobilier;

                $premiereAnneeMobilier = ($annuiteMobilier / 365 * $diff_in_days);
                $data['premiereAnneeMobilier'] = $premiereAnneeMobilier;
                $data['document'] = $this->getDocumentMobilier();


                $this->renderComponant("Clients/clients/lot/immo/immo-mobilier/list-immo-mobilier", $data);
            }
        }
    }

    public function getDocumentMobilier()
    {

        $this->load->model('DocumentMobilier_m', 'Mobilier');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(

            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_mobilier_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_mobilier.util_id')
        );

        $request = $this->Mobilier->get($params);
        return $request;
    }

    public function formUploadDocMobilier()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/immo/immo-mobilier/formUploadDocMobilier', $data);
    }

    public function uploadfileMobilier()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/immo/immo_mobilier/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_mobilier_nom' => $original_name,
                'doc_mobilier_creation' => date('Y-m-d H:i:s'),
                'doc_mobilier_path' => str_replace('\\', '/', $filePath),
                'doc_mobilier_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentMobilier_m', 'Mobilier');
            $this->Mobilier->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeDocMobilier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentMobilier_m', 'Mobilier');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_mobilier_id' => $data['doc_mobilier_id']),
                    'columns' => array('doc_mobilier_id'),
                    'method' => "row",
                );
                $request_get = $this->Mobilier->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_mobilier_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_mobilier_id' => $data['doc_mobilier_id']);
                    $data_update = array('doc_mobilier_etat' => 0);
                    $request = $this->Mobilier->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_mobilier_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateMobilier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentMobilier_m', 'Mobilier');
                $data['document'] = $this->Mobilier->get(
                    array('clause' => array("doc_mobilier_id" => $data_post['doc_mobilier_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/immo/immo-mobilier/FormUpdateMobilier", $data);
        }
    }

    public function UpdateDocumentMobilier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentMobilier_m', 'Mobilier');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_mobilier_id = $data['doc_mobilier_id'];
                    unset($data['doc_mobilier_id']);

                    $clause = array("doc_mobilier_id" => $doc_mobilier_id);
                    $request = $this->Mobilier->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocMobilierTraitement()
    {
        $this->load->model('DocumentMobilier_m', 'Mobilier');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_mobilier_id' => $data_post['doc_mobilier_id']
                    ),
                    'method' => "row"
                );
                $request = $this->Mobilier->get($params);
                if ($request->doc_mobilier_traite == 1) {
                    $data['doc_mobilier_traite'] = 0;
                    $data['doc_mobilier_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_mobilier_id' => $data_post['doc_mobilier_id']);
                    $this->Mobilier->save_data($data, $clause);
                } else {
                    $data['doc_mobilier_traite'] = 1;
                    $data['doc_mobilier_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_mobilier_id' => $data_post['doc_mobilier_id']);
                    $this->Mobilier->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function getPathMobilier()
    {

        $this->load->model('DocumentMobilier_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_mobilier_id', 'doc_mobilier_path'),
                    'clause' => array('doc_mobilier_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getPathActe()
    {

        $this->load->model('DocumentActe_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_acte_id', 'doc_acte_path'),
                    'clause' => array('doc_acte_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentActe_m->get($params);
                echo json_encode($request);
            }
        }
    }
}