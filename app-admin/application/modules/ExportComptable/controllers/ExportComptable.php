<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class ExportComptable extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "ExportComptable";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/exportcomptable/exportcomptable.js",
        );
        $this->_css_personnaliser = array(
            "css/exportcomptable/exportcomptable.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "exportcomptable";
        $this->_data['sous_module'] = "exportcomptable" . DIRECTORY_SEPARATOR . "contenaire-exportcomptable";
        $this->render('contenaire');
    }

    function page_exportcomptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("ExportComptable/exportcomptable/exportcomptable");
            }
        }
    }

    function getContentExportComptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("ExportComptable/exportcomptable/content-exportcomptable");
            }
        }
    }

    function listeExportcomptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_m', 'reglement');
                $this->load->model('Transfer_comptable_m', 'transfert_comptable');
                $facture = $this->facture_annuelle->getFactureBydate($_POST['data']['filtreDateExport']);
                $data['count_facture'] = !empty($facture) ? count($facture) : 0;
                $reglement = $this->reglement->getReglementBydate($_POST['data']['filtreDateExport']);
                $data['count_reglement'] = !empty($reglement) ? count($reglement) : 0;
                $data['transfert_comptable'] = $this->transfert_comptable->get([
                    'join' => ['c_utilisateur' => 'c_utilisateur.util_id = c_transfert_comptable.transcomp_util_id'],
                    'order_by_columns' => "c_transfert_comptable.transcomp_id DESC",
                ]);
                $this->renderComponant("ExportComptable/exportcomptable/list-exportcomptable", $data);
            }
        }
    }

    function generer_fichier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_m', 'reglement');
                $this->load->model('Transfer_comptable_m', 'transfert_comptable');
                $retour = retour(false, "error", 0, array("message" => "Erreur lors de la génération du fichier export"));
                $facture = $this->facture_annuelle->getFactureBydate($_POST['data']['filtreDateExport']);
                $count_facture = !empty($facture) ? count($facture) : 0;
                $reglement = $this->reglement->getReglementBydate($_POST['data']['filtreDateExport']);
                $count_reglement = !empty($reglement) ? count($reglement) : 0;

                $data_save_transfer = [
                    'transcomp_date' => date('Y-m-d H:i:s'),
                    'transcomp_util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'transcomp_nbecrit' => intval($count_facture + $count_reglement),
                    'transcomp_fichier' => NULL
                ];

                $request = $this->transfert_comptable->save_data($data_save_transfer);

                if (!empty($request)) {
                    $last_trans = $this->transfert_comptable->get([
                        'order_by_columns' => "transcomp_id DESC",
                        'method' => 'row'
                    ]);
                    $data = ['transcomp_id' => $last_trans->transcomp_id];
                    if (!empty($facture)) {
                        foreach ($facture as $value) {
                            $clause = ["fact_id" => $value->fact_id];
                            $this->facture_annuelle->save_data($data, $clause);
                        }
                    }
                    if (!empty($reglement)) {
                        foreach ($reglement as $value) {
                            $clause = ["idc_reglements" => $value->idc_reglements];
                            $this->reglement->save_data($data, $clause);
                        }
                    }
                    $this->exportCSV($facture, $reglement, $last_trans->transcomp_id);
                    $retour = retour(true, "success", $_POST['data']['filtreDateExport'], array("message" => "Le fichier export a été générer avec succès."));
                }

                echo json_encode($retour);
            }
        }
    }

    private function exportCSV($data_facture, $data_reglement, $transcomp_id)
    {
        $this->load->helper("exportation");
        $this->load->model('Entreprise_m', 'journaux');
        $journaux_compta = $this->journaux->get(['columns' => ['journal_ventes'], 'method' => 'row']);
        $data = [["Journal", "Date pièce", "N° Compte", "Lib compte", "N° pièce", "Lib pièce", "Lib. mouvement", "TVA", "Taux TVA", "Débit", "Crédit"]];
        if (!empty($data_facture)) {
            foreach ($data_facture as $key => $value) {
                $date_plus_10_days = date('d/m/Y', strtotime($value->fact_date . ' +10 days'));
                $table_ht = [
                    $journaux_compta->journal_ventes,
                    date('d/m/Y', strtotime($value->fact_date)),
                    $value->art_cpt_comptable,
                    "",
                    $value->fact_num,
                    $value->fact_num,
                    preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                    $value->art_code_tva,
                    $value->art_pourcentage_tva,
                    $value->fact_idFactureDu == NULL ? "" : $this->format_number_csv(abs($value->fact_montantht)),
                    $value->fact_idFactureDu == NULL ? $this->format_number_csv(abs($value->fact_montantht)) : "",
                    $date_plus_10_days
                ];
                $table_tva = [
                    $journaux_compta->journal_ventes,
                    date('d/m/Y', strtotime($value->fact_date)),
                    $value->art_compte_comptable,
                    "",
                    $value->fact_num,
                    $value->fact_num,
                    preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                    $value->art_code_tva,
                    $value->art_pourcentage_tva,
                    $value->fact_idFactureDu == NULL ? "" : $this->format_number_csv(abs($value->fact_montanttva)),
                    $value->fact_idFactureDu == NULL ? $this->format_number_csv(abs($value->fact_montanttva)) : "",
                    $date_plus_10_days
                ];
                $table_ttc = [
                    $journaux_compta->journal_ventes,
                    date('d/m/Y', strtotime($value->fact_date)),
                    '411C' . str_pad($value->dossier_id, 5, '0', STR_PAD_LEFT),
                    preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                    $value->fact_num,
                    $value->fact_num,
                    preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                    "",
                    "",
                    $value->fact_idFactureDu == NULL ? $this->format_number_csv(abs($value->fact_montantttc)) : "",
                    $value->fact_idFactureDu == NULL ? "" : $this->format_number_csv(abs($value->fact_montantttc)),
                    $date_plus_10_days
                ];

                array_push($data, $table_ht, $table_tva, $table_ttc);
            }
        }

        if (!empty($data_reglement)) {
            $last_preleve = null;
            foreach ($data_reglement as $key => $value) {
                if ($value->idc_prelevement != NULL && $value->reglement_type == 0) {
                    if ($last_preleve == $value->idc_prelevement) {
                        $data_table = [
                            $value->banque_code_journal,
                            date('d/m/Y', strtotime($value->date_reglement)),
                            '411C' . str_pad($value->dossier_id, 5, '0', STR_PAD_LEFT),
                            preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                            'PRE' . str_pad($value->idc_prelevement, 5, '0', STR_PAD_LEFT),
                            $value->fact_num,
                            preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                            "",
                            "",
                            "",
                            $this->format_number_csv(abs($value->montant_total)),
                            ""
                        ];
                        array_push($data, $data_table);
                    } else {
                        $last_preleve = $value->idc_prelevement;
                        $sum = array_sum(array_column(array_filter($data_reglement, function ($obj) use ($last_preleve) {
                            return $obj->idc_prelevement == $last_preleve && $obj->reglement_type == 0;
                        }), 'montant_total'));
                        $data_table = [
                            $value->banque_code_journal,
                            date('d/m/Y', strtotime($value->date_reglement)),
                            $value->banque_cpt_comptable,
                            "",
                            'PRE' . str_pad($value->idc_prelevement, 5, '0', STR_PAD_LEFT),
                            'PRELEVEMENT ' . $value->idc_prelevement,
                            'PRELEVEMENT ' . $value->idc_prelevement,
                            "",
                            "",
                            $this->format_number_csv($sum),
                            "",
                            ""
                        ];
                        $data_table1 = [
                            $value->banque_code_journal,
                            date('d/m/Y', strtotime($value->date_reglement)),
                            '411C' . str_pad($value->dossier_id, 5, '0', STR_PAD_LEFT),
                            preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                            'PRE' . str_pad($value->idc_prelevement, 5, '0', STR_PAD_LEFT),
                            $value->fact_num,
                            preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                            "",
                            "",
                            "",
                            $this->format_number_csv(abs($value->montant_total)),
                            ""
                        ];
                        array_push($data, $data_table, $data_table1);
                    }
                } else {
                    if ($value->reglement_type == 0) {
                        if ($value->mode_reglement == 2) {
                            $prefix = "VIR";
                            $full_prefix = "VIREMENT";
                        } else {
                            $prefix = "LOY";
                            $full_prefix = "LOYER";
                        }
                    } elseif ($value->reglement_type == 1) {
                        $prefix = "RBT";
                        $full_prefix = "REMBOURSEMENT";
                    } else {
                        $prefix = "RJT";
                        $full_prefix = "REJET";
                    }
                    $data_table = [
                        $value->banque_code_journal,
                        date('d/m/Y', strtotime($value->date_reglement)),
                        $value->banque_cpt_comptable,
                        "",
                        $prefix . str_pad($value->idc_reglements, 5, '0', STR_PAD_LEFT),
                        $full_prefix . ' ' . $value->idc_reglements,
                        preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                        "",
                        "",
                        $value->reglement_type == 0 ? $this->format_number_csv(abs($value->montant_total)) : "",
                        $value->reglement_type == 0 ? "" : $this->format_number_csv(abs($value->montant_total)),
                        ""
                    ];
                    $data_table1 = [
                        $value->banque_code_journal,
                        date('d/m/Y', strtotime($value->date_reglement)),
                        '411C' . str_pad($value->dossier_id, 5, '0', STR_PAD_LEFT),
                        preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                        $prefix . str_pad($value->idc_reglements, 5, '0', STR_PAD_LEFT),
                        $value->fact_num,
                        preg_replace('/\s+/', ' ', $value->client_nom . ' ' . $value->client_prenom),
                        "",
                        "",
                        $value->reglement_type == 0 ? "" : $this->format_number_csv(abs($value->montant_total)),
                        $value->reglement_type == 0 ? $this->format_number_csv(abs($value->montant_total)) : "",
                        ""
                    ];
                    array_push($data, $data_table, $data_table1);
                }
            }
        }

        exportation_csv_compta($data, $transcomp_id);
    }

    private function format_number_csv($number, $negatif = false)
    {
        $result = number_format($number, 2, ",", "");
        if ($negatif == true && floatval($number) > 0) return "- " . $result;
        return $result;
    }
}
