 <?php if ($count_facture > 0 || $count_reglement > 0) : ?>
     <div class="row ms-4 mb-3">
         <ul>
             <li><?= $count_facture ?> facture(s) / avoir(s)</li>
             <li><?= $count_reglement ?> règlement(s) / remboursement(s)</li>
         </ul>
     </div>
 <?php else : ?>
     <div class="row ms-4 mb-3">
         Il n'y a aucune facture et aucun règlement à exporter en comptabilité.
     </div>
 <?php endif ?>

 <?php if ($count_facture > 0 || $count_reglement > 0) : ?>
     <div class="row mb-3">
         <div class="col text-end">
             <button type="button" class="btn btn-primary" id="generer_fichier">
                 Générer le fichier d'export
             </button>
             <!-- <button type="button" class="btn btn-success d-none">
                 Télécharger le fichier d'export
             </button> -->
         </div>
     </div>
 <?php endif ?>

 <div class="row  mb-3">
     <div class="col">
         <h5>Liste des exports déjà effectués :</h5>
     </div>
 </div>

 <div class="row">
     <div class="col">
         <div class="table-wrapper table_export">
             <table class="table table-hover table-sm fs--1 mb-0">
                 <thead class="text-900 fixed-header text-center">
                     <tr class="text-center">
                         <th>Numéro du transfert</th>
                         <th>Date de l'export</th>
                         <th>Utilisateur </th>
                         <th>Nombre</th>
                         <th>Fichier </th>
                     </tr>
                 </thead>
                 <tbody>
                     <?php if (!empty($transfert_comptable)) : ?>
                         <?php foreach ($transfert_comptable as $trns) : ?>
                             <tr>
                                 <td class="text-center"><?= $trns->transcomp_id ?></td>
                                 <td class="text-center"><?= date('d/m/Y H:i:s', strtotime($trns->transcomp_date)) ?></td>
                                 <td class="text-center"><?= $trns->util_prenom . ' ' . $trns->util_nom ?></td>
                                 <td class="text-center"><?= $trns->transcomp_nbecrit ?></td>
                                 <td class="text-center">
                                     <span class="badge_download badge badge-soft-success fs--1" onclick="forceDownload('<?= addslashes(base_url() . $trns->transcomp_fichier) ?>')">Télécharger le fichier</span>
                                 </td>
                             </tr>
                         <?php endforeach ?>
                     <?php endif ?>
                 </tbody>
             </table>
         </div>
     </div>
 </div>