<div class="col ms-4 mt-3 me-4">
    <div class="col">
        <div class="mb-2">
            <label class="form-label col-form-label">Vous pouvez réaliser votre export comptable pour les éléments suivants non encore exportés jusqu'au :</label>
            <div class="col-3">
                <input type="date" class=" col-2 form-control" name="filtreDateExport" id="filtreDateExport" value="<?= date('Y-m-d') ?>">
            </div>
        </div>
    </div>

    <div id="liste-exportcomptable">

    </div>

    <script type="text/javascript">
        listeExportcomptable("<?= date('Y-m-d') ?>");
    </script>