<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
use Dompdf\Dompdf;

class Reporting extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Reporting";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/reporting/reporting.js"
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function PageReporting()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $this->load->model('Reporting_m');

                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                $request_reporting =  $this->Dossier_m->get(
                    array(
                        'clause' => array('c_dossier.dossier_id ' => $data_post['dossier_id']),
                    )
                );

                $data['reporting'] = $request_reporting;

                $request_annee_mandat = $this->Dossier_m->get(
                    array(
                        'clause' => array('c_dossier.dossier_id ' => $data_post['dossier_id']),
                        'order_by_columns' => 'c_mandat_new.mandat_date_signature DESC',
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.dossier_id = c_dossier.dossier_id',
                            'c_mandat_new' => 'c_mandat_new.cliLot_id = c_lot_client.cliLot_id'
                        ),
                        'method' => 'result',
                    )
                );

                $data['date_signature'] = $request_annee_mandat;

                if (!empty($data['date_signature'])) {
                    foreach ($data['date_signature'] as $key => $value) {
                        $date[] = array(
                            'annee' => $value->mandat_date_signature
                        );
                    }
                    $data['max_date'] = max($date);
                }

                $params = array(
                    'clause' => array('dossier_id ' => $data['dossier_id']),
                );

                $data['reporting'] = $this->Reporting_m->get($params);

                $this->renderComponant("Clients/clients/dossier/reporting/page-reporting", $data);
            }
        }
    }

    public function getContenuReporting()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Reporting_m');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];
                $trimestre = array(
                    'T1' => 'PREMIER TRIMESTRE',
                    'T2' => 'DEUXIEME TRIMESTRE',
                    'T3' => 'TROISIEME TRIMESTRE',
                    'T4' => 'QUATRIEME TRIMESTRE'
                );
                $action = $data_post['action'];

                $date_reporting = (strtotime($data_post['annee_reporting']));
                $Year_reporting = date('Y', $date_reporting);
                $year_now = date('Y');

                $resultats = array();
                for ($annee = $Year_reporting; $annee <= $year_now; $annee++) {
                    $resultats[] = $annee;
                }

                if ($action == "add") {
                    foreach ($resultats as $key => $annee) {
                        foreach ($trimestre as $key_trimestre => $value) {
                            $data = array(
                                'reporting_trimestre' => $key_trimestre,
                                'reporting_nom_trimestre' => $value,
                                'reporting_etat' => 'Non généré',
                                'dossier_id' => $data['dossier_id'],
                                'reporting_annee' => $annee,
                                'reporting_date_debut' => $data_post['annee_reporting']
                            );
                            $this->Reporting_m->save_data($data);
                        }
                    }
                }

                $params = array(
                    'clause' => array('dossier_id ' => $data['dossier_id']),
                );

                $data['reporting'] = $this->Reporting_m->get($params);
                $data['annee_reporting'] = $data['reporting'][0]->reporting_annee;
                $data['date_reporting'] = $data_post['annee_reporting'];

                $this->renderComponant("Clients/clients/dossier/reporting/contenu-reporting", $data);
            }
        }
    }

    public function listeReporting()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Reporting_m');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];
                $params = array(
                    'clause' => array(
                        'dossier_id ' => $data['dossier_id'],
                        'reporting_annee' => $data_post['reporting_annee']
                    ),
                );

                $data['reporting'] = $this->Reporting_m->get($params);

                $dateTime = new DateTime($data_post['date_reporting']);
                $numero_trimestre = ceil($dateTime->format('n') / 3);

                if ($numero_trimestre == '1') {
                    $data['trimestre'] = "T1";
                } elseif ($numero_trimestre == '2') {
                    $data['trimestre'] = "T2";
                } elseif ($numero_trimestre == '3') {
                    $data['trimestre'] = "T3";
                } else {
                    $data['trimestre'] = "T4";
                }

                $data['reporting_annee'] = $data_post['reporting_annee'];
                $date_reporting = (strtotime($data['reporting'][0]->reporting_date_debut));
                $data['year_reporting'] = date('Y', $date_reporting);

                $this->renderComponant("Clients/clients/dossier/reporting/list-reporting", $data);
            }
        }
    }


    public function Generatepdf_reporting()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Reporting_m');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['reporting_id'] = $data_post['reporting_id'];
                $action = $data_post['action'];

                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data_retour = array("dossier_id" => $data['dossier_id'], "reporting_id" => $data['reporting_id']);

                $clause = array('reporting_id' => $data['reporting_id']);

                $request = $this->Reporting_m->save_data($data, $clause);

                if ($action == "edit") {
                    if (!empty($request)) {
                        $gp = $this->pdf_reporting($data['dossier_id'], $data['reporting_id']);

                        $retour = retour(true, "success", $data_retour, array("message" => $data['reporting_id']  . "-" . $gp));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    public function pdf_reporting($dossier_id, $reporting_id)
    {

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }
        $this->load->model('Reporting_m');

        $mpdf = new \Mpdf\Mpdf();
        $data = array();
        $params = array(
            'clause' => array(
                'c_reporting.dossier_id' => $dossier_id,
                'c_reporting.reporting_id' => $reporting_id,
                'c_lot_client.cliLot_etat' => 1
            ),
            'columns' => array('*', 'c_utilisateur.util_prenom,c_utilisateur.util_id'),
            'join' => array(
                'c_dossier' => 'c_reporting.dossier_id = c_dossier.dossier_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                'c_client' => 'c_client.client_id = c_dossier.client_id',
                'c_lot_client' => 'c_lot_client.dossier_id = c_dossier.dossier_id',
                'c_programme' => 'c_lot_client.progm_id = c_programme.progm_id',
                'c_gestionnaire' => 'c_lot_client.gestionnaire_id = c_gestionnaire.gestionnaire_id',
                'c_typologielot' => 'c_lot_client.typologielot_id = c_typologielot.typologielot_id',
                'c_acte' => 'c_lot_client.cliLot_id = c_acte.cliLot_id',
                'c_pno' => 'c_lot_client.cliLot_id = c_pno.cliLot_id',
            ),
            'join_orientation' => 'left',
            'method' => 'result'
        );

        $data['reporting'] = $data_reporting = $this->Reporting_m->get($params);

        $contenu_pdf = array(
            'nom_proprietaire' => $data_reporting[0]->client_nom . ' ' . $data_reporting[0]->client_prenom,
            'nom_util' => $data_reporting[0]->util_nom . ' ' . $data_reporting[0]->util_prenom,
            'nom_trimestre' => $data_reporting[0]->reporting_nom_trimestre,
            'num_trimestre' => $data_reporting[0]->reporting_trimestre,
            'rep_annee' => $data_reporting[0]->reporting_annee,
            'progm_pourcentage_evaluation' => $data_reporting[0]->progm_pourcentage_evaluation,
            'data_lot' => array()
        );

        foreach ($data_reporting as $key => $value) {
            $data_lot = array(
                'nom_lot' => $value->progm_nom,
                'date_achat' => $value->acte_date_notarie,
                'exploitant' => $value->gestionnaire_nom,
                'typologie' => $value->typologielot_libelle,
                'montant_pno' => $value->pno_montant,
                'pno_syndic' => $value->pno_syndic,
                'pno_client' => $value->pno_client,
                'pno_celavi' => $value->pno_celavi,
                'nom_programme' => $value->progm_nom,
                'date_achat' => $value->acte_date_notarie,
                'exploitant' => $value->gestionnaire_nom,
                'typologie' => $value->typologielot_libelle,
                'pno_montant' => $value->pno_montant
            );

            $data_lot['bail'] = $this->getInfoBail($value->cliLot_id);

            if ($value->reporting_trimestre == 'T1') {
                $data_lot['periode_timestre_id'] = 1;
            } elseif ($value->reporting_trimestre == 'T2') {
                $data_lot['periode_timestre_id'] = 2;
            } elseif ($value->reporting_trimestre == 'T3') {
                $data_lot['periode_timestre_id'] = 3;
            } else {
                $data_lot['periode_timestre_id'] = 4;
            }

            $loyers_facture = NULL;
            $loyers_encaisse = NULL;
            $solde_restant = NULL;
            $taxe_fonciere = NULL;
            $capital_restant = NULL;

            if (!empty($data_lot['bail']->bail_id)) {
                $loyers_facture = $this->getloyerFacture($data_lot['bail']->bail_id, $value->reporting_annee, $data_lot['periode_timestre_id']);
            }

            if (!empty($data_lot['bail']->bail_id)) {
                $loyers_encaisse = $this->getloyerEncaisse($data_lot['bail']->bail_id, $value->reporting_annee, $data_lot['periode_timestre_id'],$value->reporting_trimestre,);
            }

            if (!empty($data_lot['bail']->bail_id)) {
                $solde_restant = $this->getSoldeRestant($data_lot['bail']->bail_id, $value->reporting_annee, $value->reporting_trimestre, $data_lot['periode_timestre_id']);
            }

            $taxe_fonciere = $this->getTaxefonciere($value->reporting_annee, $value->cliLot_id);
            $capital_restant = $this->getCapitalRestantPret($value->reporting_annee, $value->cliLot_id);

            $data_lot['loyers_facture'] = $loyers_facture;
            $data_lot['loyers_encaisse'] = $loyers_encaisse != NULL ? $loyers_encaisse . 'HT'  : 'Information non fournie';
            $data_lot['solde_restant'] = $solde_restant != NULL ? $solde_restant . 'HT'  : 'Information non fournie';
            $data_lot['taxe_fonciere'] = $taxe_fonciere != NULL ? $taxe_fonciere : 'Information non fournie';

            if (!empty($data_lot['loyers_facture']['loyers'])) {
                if ($data_lot['bail']->pdl_id == 2) {
                    $data_lot['valeur_bien'] =  ($data_lot['loyers_facture']['loyers'] * 4) / ($data_reporting[0]->progm_pourcentage_evaluation / 100);
                } elseif ($data_lot['bail']->pdl_id == 3) {
                    $data_lot['valeur_bien'] =  ($data_lot['loyers_facture']['loyers'] * 2) / ($data_reporting[0]->progm_pourcentage_evaluation / 100);
                } else {
                    $data_lot['valeur_bien'] = " ";
                }
            }else{
                $data_lot['valeur_bien'] = " ";
            }

            $data_lot['prix_revient'] = $value->acte_type_acquisition == "Acquisition ancien" ? ($value->acte_valeur_acquisition_ht * 1.08 . ' ' . 'HT') : ($value->acte_valeur_acquisition_ht * 1.04 . ' ' . 'HT');
            $data_lot['value_brute'] = !empty($data_lot['loyers_facture']['loyers']) ? $data_lot['valeur_bien'] - $data_lot['prix_revient'] : "";
            $data_lot['capital_restant'] = !empty($data_lot['loyers_facture']['loyers']) ? $data_lot['valeur_bien'] - $capital_restant : 'Information non fournie';
            
            $bail_fin = new DateTime($data_lot['bail']->bail_fin);
            $date_now = new DateTime();
            // if ($bail_fin > $date_now) {
            $interval = $date_now->diff($bail_fin);
            $data_lot['years'] = $interval->format('%y');
            $data_lot['months'] = $interval->format('%m');
            $data_lot['days'] = $interval->format('%d');
            // }
            array_push($contenu_pdf['data_lot'], $data_lot);
        }

        $data['contenu_pdf'] = $contenu_pdf;

        // print_r($data['contenu_pdf']);

        // die;

        $html = $this->load->view("Clients/clients/dossier/reporting/modele-reporting", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);

        $filename = "Reporting";

        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/dossier/reporting/" . $dossier_id;
        $url_doc = "../documents/clients/dossier/reporting/$dossier_id/$url";
        $this->makeDirPath($url_path);

        $url_file = $url_path . "/" . $url;
        if (file_exists($url_doc)) {
            $counter = 1;
            do {
                $url = $filename . '_' . $counter . '.pdf';
                $url_doc = "../documents/clients/dossier/reporting/$dossier_id/$url";
                $url_file = $url_path . "/" . $url;
                $counter++;
            } while (file_exists($url_doc));
        }

        $clause = array("reporting_id" => $reporting_id);
        $data = array(
            'reporting_path' => str_replace('\\', '/', $url_doc),
            'reporting_date_generation' => date('Y-m-d'),
            'reporting_etat' => "Généré"
        );
        $this->Reporting_m->save_data($data, $clause);
        $mpdf->Output($url_file, 'F');
        return $url_path;
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function getPathReporting()
    {
        $this->load->model('Reporting_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('reporting_id', 'reporting_path'),
                    'clause' => array('reporting_id' => $data_post['id_doc'])
                );

                $request = $this->Reporting_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getInfoBail($cliLot_id)
    {
        $this->load->model('Bail_m');

        $params_bail = array(
            'clause' => array('c_lot_client.cliLot_id' => $cliLot_id),
            'columns' => array('*', 'c_bail.bail_id'),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_periodicite_loyer' => 'c_bail.pdl_id = c_periodicite_loyer.pdl_id',
                'c_periode_indexation' => 'c_bail.prdind_id = c_periode_indexation.prdind_id',
                'c_indice_loyer' => 'c_bail.indloyer_id = c_indice_loyer.indloyer_id',
                'c_moment_facturation_bail' => 'c_bail.momfac_id = c_moment_facturation_bail.momfac_id',
                'c_type_echeance' => 'c_bail.tpech_id = c_type_echeance.tpech_id',
                'c_facture_loyer' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_type_bail' => 'c_bail.tpba_id = c_type_bail.tpba_id',
            ),
            'order_by_columns' => 'c_bail.bail_id DESC',
            'join_orientation' => 'left',
            'method' => 'row',
        );

        $request = $this->Bail_m->get($params_bail);
        return $request;
    }

    public function getloyerFacture($bail_id, $reporting_annee, $periode_timestre_id)
    {
        $this->load->model('FactureLoyer_m');
        $this->load->model('Bail_m');

        $params_bail = array(
            'clause' => array('bail_id' => $bail_id),
            'method' => 'row'
        );

        $resultat = NULL;

        $bail = $this->Bail_m->get($params_bail);

        switch ($bail->pdl_id) {
            case 1:
                $array_mensuel = array(
                    1 => array(1, 2, 3),
                    2 => array(4, 5, 6),
                    3 => array(7, 8, 9),
                    4 => array(10, 11, 12),
                );

                $total_facture = NULL;
                $date_facture = array();
                $facture_de_loyer = array();
                foreach ($array_mensuel[$periode_timestre_id] as $key => $value) {
                    $params_facture = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'periode_mens_id' => $value,
                            'facture_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);

                    if (!empty($facture_de_loyer)) {
                        $total_facture += $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                        array_push($date_facture, $facture_de_loyer->fact_loyer_date);
                    }
                }                
                $resultat = array(
                    'loyers' =>  $total_facture != NULL ? $total_facture : '',
                    'date_facture' => $date_facture != NULL ? $date_facture : ''
                );
                break;

            case 3:
                $array_mensuel = array(
                    1 => array(1, 2, 3),
                    2 => array(4, 5, 6),
                    3 => array(7, 8, 9),
                    4 => array(10, 11, 12),
                );

                $array_semestriel = array(
                    1 => array(1, 2, 3, 4, 5, 6),
                    2 => array(7, 8, 9, 10, 11, 12),
                );

                $found_keys = array();
                $mensuel_values = $array_mensuel[$periode_timestre_id];

                foreach ($mensuel_values as $value) {
                    foreach ($array_semestriel as $key => $semestriel_values) {
                        if (in_array($value, $semestriel_values)) {
                            $found_keys = $key;
                        }
                    }
                }

                $total_facture = NULL;
                $date_facture = NULL;

                if (!empty($found_keys)) {
                    $params_facture = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'periode_semestre_id' => $found_keys,
                            'facture_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                    if (!empty($facture_de_loyer)) {
                        $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                        $date_facture = $facture_de_loyer->fact_loyer_date;
                    }
                }
                $resultat = array(
                    'loyers' =>  $total_facture != NULL ? $total_facture : '',
                    'date_facture' => $date_facture != NULL ? $date_facture : ''
                );
                break;

            case 4:
                $params_facture = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'facture_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
                $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                $total_facture = NULL;
                $date_facture = NULL;
                if (!empty($facture_de_loyer)) {
                    $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                    $date_facture = $facture_de_loyer->fact_loyer_date;
                }

                $resultat = array(
                    'loyers' =>  $total_facture != NULL ? $total_facture : '',
                    'date_facture' => $date_facture != NULL ? $date_facture : ''
                );
                break;

            case 5:
                # code...
                break;

            default:
                $params_facture = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'periode_timestre_id' => $periode_timestre_id,
                        'facture_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
                $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                $total_facture = NULL;
                $date_facture = NULL;
                if (!empty($facture_de_loyer)) {
                    $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                    $date_facture = $facture_de_loyer->fact_loyer_date;
                }

                $resultat = array(
                    'loyers' =>  $total_facture != NULL ? $total_facture : '',
                    'date_facture' => $date_facture != NULL ? $date_facture : ''
                );
                break;
        }

        return $resultat;
    }

    public function getloyerEncaisse($bail_id, $reporting_annee, $periode_timestre_id,$reporting_trimestre)
    {
        $this->load->model('Encaissement_m');
        $this->load->model('Bail_m');

        $params_bail = array(
            'clause' => array('bail_id' => $bail_id),
            'method' => 'row'
        );

        $resultat = NULL;

        $bail = $this->Bail_m->get($params_bail);

        switch ($bail->pdl_id) {
            case 1:
                $array_mensuel = array(
                    1 => array('Janvier', 'Février', 'Mars'),
                    2 => array('Avril', 'Mai', 'Juin'),
                    3 => array('Juillet', 'Août', 'Septembre'),
                    4 => array('Octobre', 'Novembre', 'Décembre'),
                );

                $total_encaisse = NULL;
                $loyers_encaisse = array();
                foreach ($array_mensuel[$periode_timestre_id] as $key => $value) {
                    $params_encaisse = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'enc_periode' => $value,
                            'enc_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );

                    $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                    if (!empty($loyers_encaisse)) {
                        $total_encaisse += $loyers_encaisse->enc_montant;
                    }
                }
                $resultat =  $total_encaisse != NULL ? $total_encaisse : 'Information non fournie';

                break;

            case 3:
                $array_mensuel = array(
                    1 => array('Janvier', 'Février', 'Mars'),
                    2 => array('Avril', 'Mai', 'Juin'),
                    3 => array('Juillet', 'Août', 'Septembre'),
                    4 => array('Octobre', 'Novembre', 'Décembre'),
                );

                $array_semestriel = array(
                    1 => array('Janvier', 'Février', 'Mars','Avril', 'Mai', 'Juin'),
                    2 => array('Juillet', 'Août', 'Septembre','Octobre', 'Novembre', 'Décembre'),
                );

                $found_keys = array();
                $mensuel_values = $array_mensuel[$periode_timestre_id];

                foreach ($mensuel_values as $value) {
                    foreach ($array_semestriel as $key => $semestriel_values) {
                        if (in_array($value, $semestriel_values)) {
                            $found_keys = $key;
                        }
                    }
                }

                if ($found_keys == 1) {
                    $enc_periode = 'S1';
                }else{
                    $enc_periode = 'S2';
                }

                $total_encaisse = NULL;

                if (!empty($found_keys)) {
                    $params_encaisse = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'enc_periode' => $enc_periode,
                            'enc_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                    if (!empty($loyers_encaisse)) {
                        $total_encaisse = $loyers_encaisse->enc_montant;
                    }
                }
                $resultat = $total_encaisse != NULL ? $total_encaisse : 'Information non fournie';
                break;

            case 4:
                $params_encaisse = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'enc_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
        
                $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                $total_encaisse = NULL;
        
                if (!empty($loyers_encaisse)) {
                    $total_encaisse = $loyers_encaisse->enc_montant;
                }
        
                $resultat = $total_encaisse != NULL ? $total_encaisse : 'Information non fournie';
                break;

            case 5:
                # code...
                break;

            default:
                $params_encaisse = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'enc_periode' => $reporting_trimestre,
                        'enc_annee' => $reporting_annee
                    ),                    
                    'method' => 'row'
                );
        
                $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                $total_encaisse = NULL;
        
                if (!empty($loyers_encaisse)) {
                    $total_encaisse = $loyers_encaisse->enc_montant;
                }

                $resultat = $total_encaisse != NULL ? $total_encaisse : 'Information non fournie';
                break;
        }

        return $resultat;
    }

    public function getSoldeRestant($bail_id, $reporting_annee, $reporting_trimestre, $periode_timestre_id)
    {
        $this->load->model('Encaissement_m');
        $this->load->model('Bail_m');
        $this->load->model('FactureLoyer_m');

        $params_bail = array(
            'clause' => array('bail_id' => $bail_id),
            'method' => 'row'
        );

        $resultat = NULL;

        $bail = $this->Bail_m->get($params_bail);

        switch ($bail->pdl_id) {
            case 1:
              //encaissement
                $array_mensuel_encaisse = array(
                    1 => array('Janvier', 'Février', 'Mars'),
                    2 => array('Avril', 'Mai', 'Juin'),
                    3 => array('Juillet', 'Août', 'Septembre'),
                    4 => array('Octobre', 'Novembre', 'Décembre'),
                );

                $total_encaisse = NULL;
                $loyers_encaisse = array();
                foreach ($array_mensuel_encaisse[$periode_timestre_id] as $key => $value) {
                    $params_encaisse = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'enc_periode' => $value,
                            'enc_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );

                    $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                    if (!empty($loyers_encaisse)) {
                        $total_encaisse += $loyers_encaisse->enc_montant;
                    }
                }

                //Facture loyers
                $array_mensuel_loyers = array(
                    1 => array(1, 2, 3),
                    2 => array(4, 5, 6),
                    3 => array(7, 8, 9),
                    4 => array(10, 11, 12),
                );

                $total_facture = NULL;
                $facture_de_loyer = array();
                foreach ($array_mensuel_loyers[$periode_timestre_id] as $key => $value) {
                    $params_facture = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'periode_mens_id' => $value,
                            'facture_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);

                    if (!empty($facture_de_loyer)) {
                        $total_facture += $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                    }
                } 

                $total_solde_restant = NULL;
                $total_solde_restant = $total_encaisse - $total_facture;

                $resultat =  $total_solde_restant != NULL ? $total_solde_restant : 'Information non fournie';

                break;

            case 3:
                //encaissement
                $array_mensuel_encaisse = array(
                    1 => array('Janvier', 'Février', 'Mars'),
                    2 => array('Avril', 'Mai', 'Juin'),
                    3 => array('Juillet', 'Août', 'Septembre'),
                    4 => array('Octobre', 'Novembre', 'Décembre'),
                );

                $array_semestriel_encaisse = array(
                    1 => array('Janvier', 'Février', 'Mars','Avril', 'Mai', 'Juin'),
                    2 => array('Juillet', 'Août', 'Septembre','Octobre', 'Novembre', 'Décembre'),
                );

                $found_keys = array();
                $mensuel_values = $array_mensuel_encaisse[$periode_timestre_id];

                foreach ($mensuel_values as $value) {
                    foreach ($array_semestriel_encaisse as $key => $semestriel_values) {
                        if (in_array($value, $semestriel_values)) {
                            $found_keys = $key;
                        }
                    }
                }

                if ($found_keys == 1) {
                    $enc_periode = 'S1';
                }else{
                    $enc_periode = 'S2';
                }

                $total_encaisse = NULL;

                if (!empty($found_keys)) {
                    $params_encaisse = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'enc_periode' => $enc_periode,
                            'enc_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                    if (!empty($loyers_encaisse)) {
                        $total_encaisse = $loyers_encaisse->enc_montant;
                    }
                }

                //facture de loyer
                $array_mensuel_facture = array(
                    1 => array(1, 2, 3),
                    2 => array(4, 5, 6),
                    3 => array(7, 8, 9),
                    4 => array(10, 11, 12),
                );

                $array_semestriel_facture = array(
                    1 => array(1, 2, 3, 4, 5, 6),
                    2 => array(7, 8, 9, 10, 11, 12),
                );

                $found_keys = array();
                $mensuel_values = $array_mensuel_facture[$periode_timestre_id];

                foreach ($mensuel_values as $value) {
                    foreach ($array_semestriel_facture as $key => $semestriel_values) {
                        if (in_array($value, $semestriel_values)) {
                            $found_keys = $key;
                        }
                    }
                }

                $total_facture = NULL;

                if (!empty($found_keys)) {
                    $params_facture = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                            'periode_semestre_id' => $found_keys,
                            'facture_annee' => $reporting_annee
                        ),
                        'method' => 'row'
                    );
                    $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                    if (!empty($facture_de_loyer)) {
                        $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                    }
                }

                $total_solde_restant = NULL;
                $total_solde_restant = $total_encaisse - $total_facture;
                $resultat =  $total_solde_restant != NULL ? $total_solde_restant : 'Information non fournie';
                break;

            case 4:
                //encaissement
                $params_encaisse = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'enc_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
        
                $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                $total_encaisse = NULL;
        
                if (!empty($loyers_encaisse)) {
                    $total_encaisse = $loyers_encaisse->enc_montant;
                }

                //Facture de loyers
                $params_facture = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'facture_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
                $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                $total_facture = NULL;
                if (!empty($facture_de_loyer)) {
                    $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                }
        
                $total_solde_restant = NULL;
                $total_solde_restant = $total_encaisse - $total_facture;
                $resultat =  $total_solde_restant != NULL ? $total_solde_restant : 'Information non fournie';
                break;

            case 5:
                # code...
                break;

            default:
                //Facture de loyer
                $params_facture = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'periode_timestre_id' => $periode_timestre_id,
                        'facture_annee' => $reporting_annee
                    ),
                    'method' => 'row'
                );
                $facture_de_loyer = $this->FactureLoyer_m->get($params_facture);
                $total_facture = NULL;
                if (!empty($facture_de_loyer)) {
                    $total_facture = $facture_de_loyer->fact_loyer_app_ht + $facture_de_loyer->fact_loyer_park_ht - $facture_de_loyer->fact_loyer_charge_ht;
                }

                //encaissement

                $params_encaisse = array(
                    'clause' => array(
                        'bail_id' => $bail_id,
                        'enc_periode' => $reporting_trimestre,
                        'enc_annee' => $reporting_annee
                    ),                    
                    'method' => 'row'
                );
        
                $loyers_encaisse = $this->Encaissement_m->get($params_encaisse);
                $total_encaisse = NULL;
        
                if (!empty($loyers_encaisse)) {
                    $total_encaisse = $loyers_encaisse->enc_montant;
                }

                $total_solde_restant = NULL;

                $total_solde_restant = $total_encaisse - $total_facture;
                $resultat =  $total_solde_restant != NULL ? $total_solde_restant : 'Information non fournie';

                break;
        }

        return $resultat;
    }

    public function getTaxefonciere($reporting_annee, $cliLot_id)
    {
        $this->load->model('Taxe_m');

        $params_taxe = array(
            'clause' => array(
                'annee' => $reporting_annee,
                'c_lot_client.cliLot_id' => $cliLot_id
            ),
            'join' => array('c_lot_client' => 'c_lot_client.cliLot_id = c_taxe_fonciere_lot.cliLot_id'),
            'method' => 'row'
        );

        $request = $this->Taxe_m->get($params_taxe);

        return $request;
    }

    public function getCapitalRestantPret($reporting_annee, $cliLot_id){
        $this->load->model('Pret_m');

        $params_pret = array(
            'columns' => array('pret_capital_restant'),
            'clause' => array(                
                'pret_annee' => $reporting_annee,
                'c_lot_client.cliLot_id' => $cliLot_id
            ),
            'join' => array('c_lot_client' => 'c_lot_client.cliLot_id = c_pret_lot.cliLot_id'),
            'method' => 'row'
        );

        $request = $this->Pret_m->get($params_pret);
        $capital_restant = $request->pret_capital_restant;

        return $capital_restant;
    }
}
