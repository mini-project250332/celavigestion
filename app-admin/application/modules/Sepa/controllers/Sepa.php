<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
class Sepa extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Sepa";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/clients/clients.js",
            "js/sepa/sepa.js",
        );

        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function listMandatSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatSepa_m');

                $data_post = $_POST['data'];
                $data = array();

                $params = array(
                    'clause' => array(
                        'c_dossier.dossier_id' => $data_post['dossier_id'],
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_mandat_sepa.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_sepa.etat_mandat_id'
                    )
                );

                $data['sepa'] = $this->MandatSepa_m->get($params);            
                $data['dossier_id'] = $data_post['dossier_id'];

                $this->renderComponant("Clients/clients/dossier/sepa/liste-sepa", $data);
            }
        }
    }

    public function FormMandatSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();

                if ($data_post['action'] == "edit") {
                    $this->load->model('MandatSepa_m');
                    $params = array(
                        'clause' => array('sepa_id ' => $data_post['sepa_id']),
                        'method' => "row"
                    );
                    $data['sepa'] = $this->MandatSepa_m->get($params);
                }

                $data['action'] = $data_post['action'];
                $data['dossier_id'] = $data_post['dossier_id'];
            }

            $this->renderComponant("Clients/clients/dossier/sepa/form-mandat-sepa", $data);
        }
    }

    public function AddMandatSepa()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('MandatSepa_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array());
                $action = $data['action'];
                unset($data['action']);

                $data_retour = array("dossier_id" => $data['dossier_id'], "action" => $action);
                $dossier_id = $data['dossier_id'];

                $data['sepa_date_creation'] = date('Y-m-d');
                $data['etat_mandat_id'] = 1;   
                $data['sepa_etat'] = (isset($data['sepa_etat']) && $data['sepa_etat'] == 1) ? 1 : 0;

                // verifucation IBAN & BIC 
                if(empty(trim($data['sepa_iban']))){
                    $retour['form_validation']['sepa_iban']['required'] = "Ce champ ne doit pas être vide.";
                }

                if(empty(trim($data['sepa_bic']))){
                    $retour['form_validation']['sepa_bic']['required'] = "Ce champ ne doit pas être vide.";
                }

                if(!empty(trim($data['sepa_iban'])) && !verifyIBAN($data['sepa_iban'])){
                    $retour['form_validation']['sepa_iban']['required'] = "IBAN saisi est incorrect.";
                }

                if (empty($retour['form_validation'])) {
                    
                    $clause = null;

                    if (!empty($_FILES)) {
                        $target_dir = "../documents/clients/dossier/mandat_sepa/$dossier_id/";
                        $file = $_FILES['file']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file']['tmp_name'];
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $this->makeDirPath($target_dir);
                        if(file_exists($path_filename_ext)){
                            $i = 1;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                            while (file_exists($new_path_filename_ext)) {
                                $i++;
                                $new_filename = $filename . "_" . $i . "." . $ext;
                                $new_path_filename_ext = $target_dir . $new_filename;
                            }
                            $path_filename_ext = $new_path_filename_ext;
                        }
                        move_uploaded_file($temp_name, $path_filename_ext);
                        $data['sepa_rib'] = $path_filename_ext;
                    }

                    if ($action == "edit") {
                        $request_get = $this->MandatSepa_m->get(
                            array(
                                'clause' => array('sepa_id' => $data['sepa_id']),
                                'method' => 'row'
                            )
                        );
                        $clause = array("sepa_id" => $data['sepa_id']);
                        $data_retour['sepa_id'] = $data['sepa_id'];
                        if ($request_get->sepa_rib != NULL) {
                            unset($data['sepa_id']);
                        }
                    }

                    if(isset($data['file']) && trim($data['file']) == "undefined"){
                        unset($data['file']);
                    }

                    if(empty($clause)){
                        $params_data = array(
                            'columns' => array('sepa_id','dossier_id', 'sepa_numero')
                        );
                        $data_sepa = $this->MandatSepa_m->get($params_data); 
                        $data['sepa_numero'] = $this->generateSepaNumero($dossier_id,$data_sepa);
                    }

                    $request = $this->MandatSepa_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $sepa_id = ($action == "edit") ? $clause['sepa_id'] : $request;
                        $gp = $this->generatePdfMandatSepa($dossier_id,$sepa_id);
                        $retour = retour(true, "success", $data_retour, array("message" => $dossier_id . "-" . $gp));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***** *****/

    // Fonction pour générer le sepa_numero en suivant les règles spécifiées
    function generateSepaNumero($dossierId, $existingSepaNumeros) {
        // filter by format sepa_numero
        $filteredSeries_trueFormat = array_filter($existingSepaNumeros, function ($item) use ($dossierId) {
            return preg_match('/^\d+[A-Z]$/', $item->sepa_numero);
        });
        $filteredSeries_trueFormat = !empty($filteredSeries_trueFormat) ? array_values($filteredSeries_trueFormat) : [];
        // filter by dossier id
        $filteredSeries_trueFormat_dossierId = array_filter($filteredSeries_trueFormat, function ($item) use ($dossierId) {
            return $item->dossier_id == $dossierId;
        });
        $filteredSeries_trueFormat_dossierId = !empty($filteredSeries_trueFormat_dossierId) ? array_values($filteredSeries_trueFormat_dossierId) : [];

        $format_data = array_map(function($item){
            preg_match('/^(\d+)([A-Z])$/',$item->sepa_numero, $matches);
            $item->digits = $matches[1];
            $item->letter = $matches[2];
            return $item;
        }, !empty($filteredSeries_trueFormat_dossierId) ? $filteredSeries_trueFormat_dossierId : $filteredSeries_trueFormat);


        if(!empty($filteredSeries_trueFormat_dossierId)){
            usort($filteredSeries_trueFormat_dossierId, function ($a, $b) {
                return strcmp($a->letter, $b->letter);
            });
            $lastElement = end($filteredSeries_trueFormat_dossierId);
            return  $lastElement->digits.chr(ord($lastElement->letter) + 1);
        }else if(empty($filteredSeries_trueFormat_dossierId) && !empty($filteredSeries_trueFormat)){

            usort($filteredSeries_trueFormat, function ($a, $b) {
                return strcmp($a->digits, $b->digits);
            });

            $lastElement = end($filteredSeries_trueFormat);
            return (intval($lastElement->digits) + 1).'A';
        }else{
            return '10001A';
        }
    }

    /***** *****/

    public function checkSeparRm ($dossier_id){
        $this->load->model('MandatSepa_m');
        $params = array(
            'clause' => array (
                'dossier_id' => $dossier_id
            )
        );
        $this->MandatSepa_m->get($params);

    }

    public function generatePdfMandatSepa($dossier_id,$sepa_id)
    {
        $this->load->library('mpdf-5.7-php7/Mpdf');        

        $this->load->helper("url");
        $this->load->model('MandatSepa_m');
        $data = array();
        $params = array(
            'clause' => array(
                'c_mandat_sepa.dossier_id' => $dossier_id,
                'sepa_id' => $sepa_id
            ),
            'join' => array(
                'c_dossier' => 'c_mandat_sepa.dossier_id = c_dossier.dossier_id',
                'c_client' => 'c_client.client_id = c_dossier.client_id',
                'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_sepa.etat_mandat_id'
            ),
            'join_orientation' => 'left',
            'method' => "row"
        );

        $data['sepa'] = $this->MandatSepa_m->get($params);

        $mpdf = new \Mpdf\Mpdf();
 
        $html = $this->load->view("Clients/clients/dossier/modele_mandat_sepa/mandat_sepa", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);

        $filename = 'Sepa_N_' . $data['sepa']->sepa_id . '_' . $data['sepa']->client_nom . '_';
        $filename = str_replace(array(" ", "&"), "_", $filename);

        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/dossier/mandat_sepa/" . $dossier_id;
        $url_doc = "../documents/clients/dossier/mandat_sepa/$dossier_id/$url";
        $this->makeDirPath($url_path);

        $url_file = $url_path . "/" . $url;
        if (file_exists($url_doc)) {
            $counter = 1;
            do {
                $url = $filename . '_' . $counter . '.pdf';
                $url_doc = "../documents/clients/dossier/mandat_sepa/$dossier_id/$url";
                $url_file = $url_path . "/" . $url;
                $counter++;
            } while (file_exists($url_doc));
        }

        $clause = array(
            "dossier_id" => $dossier_id,
            'sepa_id' => $sepa_id
        );
        $data = array(
            'sepa_path' => str_replace('\\', '/', $url_doc),
        );
        $this->MandatSepa_m->save_data($data, $clause);
        
        $mpdf->Output($url_file, 'F');
        return $url_path;
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function sendMandatSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('MandatSepa_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('sepa_id' => $data['sepa_id']),
                    'columns' => array('sepa_id'),
                    'method' => "row",
                );
                $request_get = $this->MandatSepa_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['sepa_id'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous vraiment basculer ce mandat SEPA à l'état 'Mandat envoyé' ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('sepa_id' => $data['sepa_id']);
                    $data_update = array('etat_mandat_id' => 2, 'sepa_date_envoi' => date('Y-m-d'));
                    $request = $this->MandatSepa_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['sepa_id']),
                            'message' => "Mandat envoyé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function SignerMandatSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatSepa_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'clause' => array('sepa_id' => $data_post['sepa_id']),
                    'method' => 'row'
                );

                $data['sepa'] = $this->MandatSepa_m->get($params);

                $data['sepa_id'] = $data_post['sepa_id'];
                $data['dossier_id'] = $data_post['dossier_id'];
            }

            $this->renderComponant("Clients/clients/dossier/sepa/sign-mandat-sepa", $data);
        }
    }

    public function AddSignMandatSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatSepa_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("dossier_id" => $data['dossier_id']);
                $sepa_id = $data['sepa_id'];
                $dossier_id = $data['dossier_id'];
                $data['etat_mandat_id'] = 3;
                $data['sepa_date_signature'] = date_sql($data['sepa_date_signature'], '-', 'jj/MM/aaaa');
                $data['sepa_path'] = NULL;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/clients/dossier/mandat_sepa/$dossier_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if(file_exists($path_filename_ext)){
                        $i = 1;
                        $new_filename = $filename . "_" . $i . "." . $ext;
                        $new_path_filename_ext = $target_dir . $new_filename;
                        while (file_exists($new_path_filename_ext)) {
                            $i++;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                        }
                        $path_filename_ext = $new_path_filename_ext;
                    }
                    move_uploaded_file($temp_name, $path_filename_ext);
                    $data['sepa_path'] = $path_filename_ext;
                }

                $clause = array("sepa_id" => $sepa_id);

                $request = $this->MandatSepa_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Signature ajoutée avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function getPathSepa()
    {

        $this->load->model('MandatSepa_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('sepa_id', 'sepa_path'),
                    'clause' => array('sepa_id' => $data_post['id_doc'])
                );

                $request = $this->MandatSepa_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function removefileSepa()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatSepa_m');

                $data_post = $_POST['data'];

                $data_pno = array(
                    
                    'sepa_rib' => NULL
                );

                $sepa_id = $data_post['sepa_id'];
                unset($data_post['sepa_id']);

                $clause = array("sepa_id" => $sepa_id);

                $request = $this->MandatSepa_m->save_data($data_pno, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "Fichier Supprimé"));
                }

                echo json_encode($retour);
            }
        }
    }

}
