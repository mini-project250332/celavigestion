<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class ExpertComptable extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "ExpertComptable";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/expert_comptable/expert_comptable.js", 
            "js/expert_comptable/completude.js", 
            "js/expert_comptable/dossier.js", 
            "js/expert_comptable/suivi.js",
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "expertcomptable";
        $this->_data['sous_module'] = "expertcomptable" . DIRECTORY_SEPARATOR . "contenaire-expertcomptable";
        $this->render('contenaire');
    }

    public function page_expertComptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("ExpertComptable/expertcomptable/expertcomptable");
            }
        }
    }
}
