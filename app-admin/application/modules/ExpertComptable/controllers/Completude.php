<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Completude extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "ExpertComptable";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function index(){
        $this->get_completudeTab();
    }

    public function get_completudeTab()
    {   
        $data['filtre'] = array();
        $liste_util = $this->getListUser();

        $expert_comptable = array_filter($liste_util, function($util){
            return $util->rol_util_id == 6;
        });
        $data['expert_comptable'] = array_values($expert_comptable);
        $this->renderComponant("ExpertComptable/completude/page_completude",$data);
    }

    public function get_listeCompletude(){
        $data_post = $_POST['data'];
        // Taxe F. (si la taxe foncière a été réceptionnée et validée par un gestionnaire de compte, on affiche le montant)
        $params_taxeFonciere = array(
            'clause' => array(
                'taxe_valide' => 1,
                'annee' => $data_post['annee']
                // cliLot_id
            ),
            'columns' => array('taxe_montant', 'taxe_tom','taxe_valide','cliLot_id')
        );
        $this->load->model('Taxe_m');
        $dataTaxeFonciere = $this->Taxe_m->get($params_taxeFonciere);

        // Bail (si un bail est actif pour le lot)
        $params_bail = array(
            'clause' => array(
                'bail_cloture' => 0,
                //cliLot_id
            ),
            'columns' => array('cliLot_id','bail_debut','bail_fin','bail_valide')
        );
        $this->load->model('Bail_m');
        $dataBail = $this->Bail_m->get($params_bail);

        // Mandat CELAVI (indication si il y a un mandat signé actif avec CELAVI) (oui / non)
        $params_mandat = array(
            'clause' => array(
                'c_etat_mandat.etat_mandat_id' => 3 // (mandat signé)
                //lot_id , mandat_etat
            ), 
            'join' => array(
                'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id'
            ),
            'columns' => array('cliLot_id','mandat_montant_ht','mandat_etat','etat_mandat_libelle')
        );
        $this->load->model('MandatNew_m');
        $dataMandat = $this->MandatNew_m->get($params_mandat);

        // Prêt (si les informations de prêts ont été renseignées pour l'année sélectionnée et pour le lot concernée) (oui / non)
        $params_pret = array(
            'clause' => array(
                'pret_etat' => 1,
                'pret_annee' => $data_post['annee']
                // cliLot_id
            ),
        );
        $this->load->model('Pret_m');
        $dataPret = $this->Pret_m->get($params_pret);

        /*
        Rep. Charge (s'il y a eu au moins une dépense saisie pour le type "Décomptes de charges..." et pour l'année sélectionnée pour le lot concerné, on met une coche verte).
        */
        $params_repCharge = array(
            'clause' => array(
                'annee' => $data_post['annee'],
                'c_type_charge_lot.tpchrg_lot_id' => 2
                // cliLot_id
            ),
            'join' => array(
                'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id '
            ),
        );
        $this->load->model('Charges_m');
        $dataCharges = $this->Charges_m->get($params_repCharge);

        // Acte (si il y a au moins un document dans la ged d'acte / acquisition, c'est ok, on met la coche verte)
        $params_documentActe = array(
            'clause' => array(
                'doc_acte_etat' => 1,
            ),
            'columns' => array('cliLot_id','doc_acte_nom')
        );
        $this->load->model('DocumentActe_m');
        $dataDocumentActe = $this->DocumentActe_m->get($params_documentActe);

        // Recuperer tous les lots 
        $param_lots = array(
            'clause' => array('cliLot_etat' => 1),
            'join' => array(
                //'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                'c_acte' => 'c_acte.cliLot_id = c_lot_client.cliLot_id'
            ),
            'join_orientation' => 'left',
            'group_by_columns' => 'c_lot_client.cliLot_id',
            'columns' => array('c_lot_client.cliLot_id','c_acte.acte_id','acte_type_acquisition','c_programme.progm_nom','c_programme.progm_num','dossier_id','c_programme.progm_id')
        );
        $this->load->model('ClientLot_m');
        $dataLots = $this->ClientLot_m->get($param_lots);

        // format données lots 
        foreach ($dataLots as $key => $value) {
            $cliLot_id = $value->cliLot_id;
            
            // taxe fonciere
            $taxe_foncieres = array_filter($dataTaxeFonciere, function($taxe) use ($cliLot_id) {
                return $taxe->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->taxe_foncieres = array_values($taxe_foncieres);
            
            // bail
            $bails = array_filter($dataBail, function($bail) use ($cliLot_id) {
                return $bail->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->bails = array_values($bails);
            
            // mandat
            $mandats = array_filter($dataMandat, function($mandat) use ($cliLot_id) {
                return $mandat->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->mandats = array_values($mandats);

            // Charge
            $charges = array_filter($dataCharges, function($charge) use ($cliLot_id) {
                return $charge->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->charges = array_values($charges);
            
            // prets
            $prets = array_filter($dataPret, function($pret) use ($cliLot_id) {
                return $pret->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->prets = array_values($prets);

            // document acte
            $document_acts = array_filter($dataDocumentActe, function($doc_act) use ($cliLot_id) {
                return $doc_act->cliLot_id == $cliLot_id;
            });
            $dataLots[$key]->document_acts = array_values($document_acts);
        }

        //Liste utilisateur (Suivie clientèle);
        $data['liste_util'] = $liste_util = $this->getListUser();

        $clause_countAllDossierCloturer = array(
            'etat' => 2,
            'c_cloture_annuelle.annee' => $data_post['annee'], 
        );


        $data["countAllResult"] = $this->countAllDossierCloturer('Dossier_m',$clause_countAllDossierCloturer);
        // $nb_total = $data["countAllResult"];
        // $data['pagination_page'] = $data_post['pagination_page'];
        // $data['rows_limit'] = $rows_limit;
        // $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
        // $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
        // $data["active_li_pagination"] = $pagination_data[0];

        // Dossier
        $this->load->model('Dossier_m', 'dossier');
        $params = array(
            'clause' => array(
                'c_cloture_annuelle.annee' => $data_post['annee'], 
            ),
            'join' => array(
                //'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                'c_cloture_annuelle' => 'c_cloture_annuelle.dossier_id = c_dossier.dossier_id' 
            ),
            'columns' => array('c_dossier.dossier_id','util_id','c_dossier.client_id'),
            //'limit' => array('offset' => $pagination_data[1], 'limit' => $pagination_data[0]),
        );

        if(!empty($data_post['expert'])){
            $params['clause']['util_expert_comptable'] =  $data_post['expert'];
        }

        $dossiers = $this->dossier->get($params);
        //$dossiers = array_slice($dossiers, intval($pagination_data[0]), intval($pagination_data[0])+200, true);

        // Nom propriétaire
        foreach ($dossiers as $key => $value) {
            $dossiers[$key]->client = null;
            if (!empty($value->client_id)) {
                $array_client = explode(',', $value->client_id);
                $client = array();
                foreach ($array_client as $value_client) {
                    array_push($client, $this->getClient($value_client));
                }
                $dossiers[$key]->client = $dossiers[$key]->client = implode(', ',$client);
            }
            // Suivi clientèlle
            $util_id = $value->util_id; 
            $dossiers[$key]->suivi = array_filter($liste_util, function($util) use ($util_id) {
                return $util->id == $util_id;
            });
            // lots
            $dossier_id = $value->dossier_id; 
            $lots = array_filter($dataLots, function($lot) use ($dossier_id) {
                return $lot->dossier_id == $dossier_id;
            });
            $dossiers[$key]->lots =array_values($lots);
        }

        $data['dossiers'] = $dossiers = array_filter($dossiers, function($lot) {
            return !empty($lot->client);
        });
        $this->renderComponant("ExpertComptable/completude/liste_completude",$data);
    }


    function countAllDossierCloturer($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
            'join' => array(
                'c_cloture_annuelle' => 'c_cloture_annuelle.dossier_id = c_dossier.dossier_id' 
            )
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }


    /*
        function countAllData($model, $clause)
        {
            $this->load->model($model, 'model');
            $params = array(
                'clause' => $clause,
                'columns' => array('COUNT(*) as allData'),
            );
            $request = $this->model->get($params);
            return $request[0]->allData;
        }
    */

























    public function get_completudeTab_copy()
    {   
        $data_post = $_POST['data'];
        if(isset($data_post['pagination_page'])){
            $pagination_data = explode('-', $data_post['pagination_page']);
        }
        $params = array(
            'clause' => array('cliLot_etat' => 1),
            'method' => "result",
            //'columns' => array('c_dossier.dossier_id','c_lot_client.cliLot_id','c_dossier.client_id'),
            'join' => array(
                'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                'c_client' => 'c_dossier.client_id = c_client.client_id',
                'c_taxe_fonciere_lot' => 'c_taxe_fonciere_lot.cliLot_id = c_lot_client.cliLot_id',
                'c_bail' => 'c_bail.cliLot_id = c_lot_client.cliLot_id',
                'c_mandat_new' => 'c_lot_client.cliLot_id = c_mandat_new.cliLot_id',
                'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id'
            ),
            'join_orientation' => 'left',
            'group_by_columns' => 'c_lot_client.cliLot_id',
            //'limit' => array('offset' => 1, 'limit' => 200),
        );

        // 'limit' => array('offset' => $pagination_data[1], 'limit' => $pagination_data[0]),
        $data['gestionnaire'] = $this->getListGestionnaire();

        $this->load->model('ClientLot_m');
        $data['liste_lots'] = $this->ClientLot_m->get($params);

        foreach ($data['liste_lots'] as $key => $value) {
            $data['liste_lots'][$key]->client = $this->getClient($value->client_id);
        }

        $data['liste_lots'] = array_filter($data['liste_lots'], function($lot) {
            return !empty($lot->client);
        });

       $this->renderComponant("ExpertComptable/completude/page_completude",$data);
    }
}
