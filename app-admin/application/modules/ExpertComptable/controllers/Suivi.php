<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Suivi extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Suivi";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function getSuiviDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                
                $this->renderComponant("ExpertComptable/suivi/contenu-suivi");
            }
        }
    }

    public function getSuivi()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClotureAnnuelle_m','cloture'); 
                $clause = $_SESSION['session_utilisateur']['util_id']; 
                
                $data['suivi'] = $this->cloture->getListSuiviComptable($clause,2);                 

                $this->renderComponant("ExpertComptable/suivi/liste-suivi",$data);
            }
        }
    }
}
