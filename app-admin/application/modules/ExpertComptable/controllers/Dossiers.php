<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Dossiers extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Dossiers";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function getDossierExpertcomptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m');

                $params = array(
                    'clause' => array(
                        'c_utilisateur.rol_util_id' => 6,
                        'util_etat' => 1
                    ),
                    'join' => array('c_role_utilisateur' => 'c_utilisateur.rol_util_id = c_role_utilisateur.rol_util_id')
                );

                $data['expert_comptable'] = $this->Utilisateur_m->get($params);

                $this->renderComponant("ExpertComptable/dossiers/contenu-dossiers", $data);
            }
        }
    }

    public function getDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Expertcomptable_mission_dossier_m', 'dossier_mission');
                $this->load->model('ClientLot_m', 'cli_lot');
                $this->load->model('Utilisateur_m', 'utilisateur');

                $datapost = $_POST['data'];
                $expert_id = NULL;

                if ($datapost['util_expert_comptable'] != '') {
                    $expert_id = $datapost['util_expert_comptable'];
                }

                if ($_SESSION['session_utilisateur']['rol_util_valeur'] == 6) {
                    $expert_id = $_SESSION['session_utilisateur']['util_id'];
                }

                $dossier_mission = $this->dossier_mission->getMission($datapost['annee'], $expert_id);

                $dossier_mission = array_map(function ($value) {
                    $value->nbre_lot = 0;
                    $value->tva_lot = '';
                    $value->defiscalisation = '';
                    $value->charge_clientele = '';
                    $value->expert_comptable = '';

                    $value->date_cloture = date('Y-m-d', strtotime(date('Y') . '-' . intval($value->cloture_mois_fin) . '-' . intval($value->cloture_jour_fin)));

                    $timestamp = strtotime($value->date_cloture);
                    $newTimestamp = strtotime('+3 months', $timestamp);
                    $value->date_cloture_liasse = date('d/m/Y', $newTimestamp);
                    $value->date_cloture = date('d/m/Y', strtotime($value->date_cloture));

                    $charge_clientele = $this->utilisateur->get(array(
                        'clause' => array('util_id' => $value->util_id),
                        'method' => "row"
                    ));

                    $expert_comptable = $this->utilisateur->get(array(
                        'clause' => array('util_id' => $value->expert_id),
                        'method' => "row"
                    ));

                    $value->charge_clientele = !empty($charge_clientele) ? $charge_clientele->util_prenom . ' ' . $charge_clientele->util_nom : 'Aucune';
                    $value->expert_comptable = $expert_comptable->util_prenom . ' ' . $expert_comptable->util_nom;;

                    $lot = $this->cli_lot->get(array(
                        'clause' => array('dossier_id' => $value->dossier_id)
                    ));

                    if (!empty($lot)) {
                        $value->nbre_lot = count($lot);

                        foreach ($lot as $value_lot) {
                            if ($value_lot->cliLot_principale == 1) {
                                $value->tva_lot = $value_lot->tva;
                                $value->defiscalisation = $value_lot->defiscalisation;
                            }
                        }
                    }

                    return $value;
                }, $dossier_mission);

                $mergedArray = array_values(array_reduce($dossier_mission, function ($carry, $item) {
                    $dossierId = $item->dossier_id;
                    if (!isset($carry[$dossierId])) {
                        $carry[$dossierId] = $item;
                    } else {
                        $existingExperts = explode('<br>', $carry[$dossierId]->expert_comptable);
                        $newExperts = explode('<br>', $item->expert_comptable);

                        $mergedExperts = implode('<br>', array_unique(array_merge($existingExperts, $newExperts)));
                        $carry[$dossierId]->expert_comptable = $mergedExperts;
                    }
                    return $carry;
                }, []));

                $data['dossier'] = $mergedArray;
                $this->renderComponant("ExpertComptable/dossiers/liste-dossiers", $data);
            }
        }
    }
}
