<style>
    thead {
        background: #2569C3;
    }

    th {
        border-right: 1px solid #ffff;
        color: white;
    }

    td {
        border: 1px bold grey;
        border-style: solid;
        border-width: 1px;
    }
</style>
<div class="row mt-3 mb-2">
    <div class="col">
        <table class="table table-hover table-striped overflow-hidden fs--1 mb-0">
            <thead class="text-900">
                <tr class="text-center">
                    <th class="p-2">Numéro dossier</th>
                    <th class="p-2">Noms dossiers</th>
                    <th class="p-2">Dossier complet</th>
                    <th class="p-2">CELAVIGESTION Contrôle Admin</th>
                    <th class="p-2">1ère connexion CABINET</th>
                    <th class="p-2">Dernière connexion CABINET </th>
                    <th class="p-2">CABINET Liasse</th>
                    <th class="p-2">CELAVIGESTION Liasse validation</th>
                    <th class="p-2">CELAVIGESTION envoi liasse client</th>
                    <th class="p-2">Conseiller clientèle</th>
                    <th class="p-2">Actions</th>
                </tr>
            </thead>
            <tbody class="text-center">
                <?php foreach ($suivi as $key => $suivis) : ?>                    
                    <tr>
                        <td><?= str_pad($suivis->dossier_id, 4, '0', STR_PAD_LEFT);  ?></td>
                        <td><?= $suivis->client_nom . ' ' . $suivis->client_prenom ?></td>
                        <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $suivis->date_demande_validation))); ?></td>
                        <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $suivis->date_validation))); ?></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td></td>
                        <td><?= $suivis->util_prenom ?></td>
                        <td>
                            <button data-id="<?= $suivis->dossier_id; ?>" class="btn btn-sm btn-outline-primary icon-btn btnFicheDossier" data-client_id="<?= $suivis->client_id ?>" type="button">
                                <span class="fas fa-play fs--1"></span>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>