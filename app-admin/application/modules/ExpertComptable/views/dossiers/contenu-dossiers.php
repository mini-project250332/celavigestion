<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Liste des dossiers</h4>
    </div>
    <div class="px-3">
        <select id="util_expert_comptable" class="form-select no_account_manager">
            <option value="">Tous les experts comptables </option>
            <?php foreach ($expert_comptable as $value) : ?>
                <option value="<?= $value->util_id ?>">
                    <?= $value->util_prenom . ' ' . $value->util_nom ?>
                </option>
            <?php endforeach; ?>
        </select> &nbsp;&nbsp;
        <select id="annee_dossier" class="form-select">
            <?php for ($i = intval(date("Y")); $i >= 2023; $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>

    </div>
</div>


<div class="w-100" id="content-dossiers">

</div>

<script>
    only_visuel_gestion(["1", "2", "3", "4", "5"], "no_account_manager");
    getContentDossier();
</script>