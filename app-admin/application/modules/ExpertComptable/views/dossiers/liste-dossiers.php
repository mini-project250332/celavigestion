<style>
    thead {
        background: #2569C3;
    }

    th {
        border-right: 1px solid #ffff;
        color: white;
    }

    td {
        border: 1px bold grey;
        border-style: solid;
        border-width: 1px;
    }
</style>
<div class="row mt-3 mb-2">
    <div class="col">
        <table class="table table-hover table-striped overflow-hidden fs--1 mb-0">
            <thead class="text-900">
                <tr class="text-center">
                    <th class="p-2">N° dossier</th>
                    <th class="p-2">Noms dossiers</th>
                    <th class="p-2">Forme juridique</th>
                    <th class="p-2">Nbre de lots</th>
                    <th class="p-2">Date clôture</th>
                    <th class="p-2">Date limite liasse</th>
                    <th class="p-2">TVA </th>
                    <th class="p-2">CGA </th>
                    <th class="p-2">Censi Bouvard </th>
                    <th class="p-2">Chargée de clientèle</th>
                    <th class="p-2">Nom EC</th>
                    <th class="p-2">Actions</th>
                </tr>
            </thead>
            <tbody class="text-center">
                <?php if (!empty($dossier)) : ?>
                    <?php foreach ($dossier as $dossiers) : ?>
                        <tr>
                            <td><?= str_pad($dossiers->dossier_id, 4, '0', STR_PAD_LEFT);  ?></td>
                            <td><?= $dossiers->client_nom . ' ' . $dossiers->client_prenom ?></td>
                            <td><?= $dossiers->c_forme_juridique_libelle ?></td>
                            <td><?= $dossiers->nbre_lot ?></td>
                            <td><?= $dossiers->date_cloture ?></td>
                            <td><?= $dossiers->date_cloture_liasse ?></td>
                            <td><?= $dossiers->tva_lot ?></td>
                            <td><?= $dossiers->mention_cga == 1 ? 'OUI' : 'NON' ?></td>
                            <td><?= $dossiers->defiscalisation ?></td>
                            <td><?= $dossiers->charge_clientele ?></td>
                            <td><?= $dossiers->expert_comptable ?></td>
                            <td>
                                <button data-id="<?= $dossiers->dossier_id; ?>" class="btn btn-sm btn-outline-primary icon-btn btnFicheDossier" data-client_id="<?= $dossiers->client_id ?>" type="button">
                                    <span class="fas fa-play fs--1"></span>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>
</div>