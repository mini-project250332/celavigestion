<div class="contenair-content">
    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="dossier-tab" data-bs-toggle="tab" href="#tab-dossier" role="tab" aria-controls="tab-dossier" aria-selected="true">Dossiers</a>
        </li>
        <li class="nav-item" onclick="get_completudeTab();">
            <a class="nav-link" id="completude" data-bs-toggle="tab" href="#tab-completude" role="tab" aria-controls="tab-completude" aria-selected="false">Complétude</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="suivi" data-bs-toggle="tab" href="#tab-suivi" role="tab" aria-controls="tab-suivi" aria-selected="false">Suivis</a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-2" id="lotTabContent">
        <div class="tab-pane fade show active" id="tab-dossier" role="tabpanel" aria-labelledby="dossier-tab" style="overflow-y: unset !important">
            <?php $this->load->view('dossiers/dossiers'); ?>
        </div>
        <div style="position:relative;" class="tab-pane fade" id="tab-completude" role="tabpanel" aria-labelledby="completude-tab">
            <div class="alert alert-primary text-center" role="alert">
                En cours de chargerment...
            </div>
        </div>
        <div class="tab-pane fade" id="tab-suivi" role="tabpanel" aria-labelledby="suivi-tab" style="overflow-y: unset !important">
            <?php $this->load->view('suivi/suivi'); ?>
        </div>
    </div>
</div>