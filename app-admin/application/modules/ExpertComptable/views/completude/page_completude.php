<style>
    thead {
        background: #2569C3;
    }

    th {
        border-right: 1px solid #ffff;
        color: white;
    }

    td {
        border: 1px bold grey;
        border-style: solid;
        border-width: 1px;
    }
</style>
<!-- <div class="contenair-title mb-1">
    <div class="px-2">
        <h5 class="fs-0">Complétude</h5>
    </div>
</div> -->
<?php //var_dump($_SESSION["session_utilisateur"]); ?>
<div class="container-titre">
    <div class="row m-0 justify-content-end">
        <div class="col d-flex align-items-center"></div>
        <div class="col-auto ml-auto mb-0">
            <div class="mb-2 row m-0 align-items-center">
                <label class="col form-label mb-0">Années</label>
                <div class="col px-0">
                    <select name="filtre-annees" id="filtre-annees" class="form-select" style="width:130px;">
                        <?php $currentY = date('Y'); ?>
                        <?php for ($i=intval($currentY); $i > 2020 ; $i--):?> 
                            <option value="<?=$i?>"><?=$i?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
        <?php //($_SESSION['session_utilisateur']['rol_util_valeur']==6) ? 'd-none' : '';?>
        <div class="col-auto ml-auto mb-0 <?= ($_SESSION['session_utilisateur']['rol_util_valeur']==6) ? '' : '';?>" >
            <div class="mb-2 row m-0 align-items-center">
                <label class="col-auto px-2 mb-1 form-label">Expert comptable</label>
                <div class="col px-0">
                    <select name="expert-comptable" id="expert-comptable" class="form-select w-100">
                        <option value="">Tous</option>
                        <?php foreach ($expert_comptable as $key => $value): ?>
                            <option value="<?=$value->id?>" <?php ($_SESSION["session_utilisateur"]['rol_util_valeur']==6 && $_SESSION["session_utilisateur"]['util_id'] == $value->id) ? 'selected':'' ?>>
                                <?=$value->nom.' '.$value->prenom;?>
                            </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="list-completude" class="container-list"></div>

<script type="text/javascript">
    getListeCompletude();
</script>