<div class="mb-2">
    <table class="table table-sm fs--1 mb-0">
        <thead class="text-900">
            <tr class="text-center">
                <th class="p-2 px-0">N° dossier</th>
                <th class="p-2 text-start">Nom des propriétaires</th>
                <th class="p-2 text-start">Nom du programme</th>
                <th class="p-2" >Mandat CELAVI</th>
                <th class="p-2">Lettre EC</th>
                <th class="p-2">Acte</th>
                <th class="p-2">Bail </th>
                <th class="p-2">Taxe foncière</th>
                <th class="p-2">Rep.Charge</th>
                <th class="p-2">Prêt </th>
                <th class="p-2 text-start">Conseiller clientèle</th>
                <th class="p-2">actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($dossiers)):?>
                <?php foreach ($dossiers as $key => $dossier):?>
                    <?php $countsLots = count($dossier->lots);?>
                    <?php if($countsLots > 1): ?>

                        <tr class="align-middle">
                            <td class="p-2 text-center" rowspan="<?=$countsLots;?>"><?=$dossier->dossier_id;?></td>
                            <td class="text-start" rowspan="<?=$countsLots;?>"><?=$dossier->client;?></td>

                            <td class="text-start px-1"><?=$dossier->lots[0]->progm_nom; ?></td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->mandats) && !empty($dossier->lots[0]->mandats)):?>
                                    <span class="badge badge-soft-success fs--1">Oui</span>
                                <?php else: ?>
                                    <span class="badge badge-soft-secondary fs--1">Non</span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">***</td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->document_acts) && !empty($dossier->lots[0]->document_acts)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                 <?php if(isset($dossier->lots[0]->bails) && !empty($dossier->lots[0]->bails)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->taxe_foncieres) && !empty($dossier->lots[0]->taxe_foncieres)):?>
                                    <?=$dossier->lots[0]->taxe_foncieres[0]->taxe_montant; ?> €
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->charges) && !empty($dossier->lots[0]->charges)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->prets) && !empty($dossier->lots[0]->prets)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>

                            <td class="text-start" rowspan="<?=$countsLots;?>">
                                <?php if(!empty($dossier->suivi)): ?>
                                    <?= array_values($dossier->suivi)[0]->nom.' '.array_values($dossier->suivi)[0]->prenom;?>
                                <?php endif; ?>
                            </td>

                            <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                    <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                </button>
                            </td>
                        </tr>
                        <?php foreach (array_values($dossier->lots) as $key_lot => $lot): ?>
                            <?php 
                                $border_space = ($key_lot == ($countsLots -1 )) ? " border-bottom-width: 1px;" : '';
                            ?>
                            <?php if($key_lot != 0):?>
                                <tr class="align-middle" style="<?=$border_space?>">
                                    <td class="text-start px-1"><?=$lot->progm_nom?></td>
                                    <td class="text-center">
                                        <?php if(isset($lot->mandats) && !empty($lot->mandats)):?>
                                            <span class="badge badge-soft-success fs--1">Oui</span>
                                        <?php else: ?>
                                            <span class="badge badge-soft-secondary fs--1">Non</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">***</td>
                                    <td class="text-center">
                                        <?php if(isset($lot->document_acts) && !empty($lot->document_acts)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->bails) && !empty($lot->bails)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->taxe_foncieres) && !empty($lot->taxe_foncieres)):?>
                                            <?=$lot->taxe_foncieres[0]->taxe_montant; ?> €
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->charges) && !empty($lot->charges)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->prets) && !empty($lot->prets)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                        <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                            <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                        </button>
                                    </td>
                                </tr>
                        <?php endif;?>
                        <?php endforeach;?>
                    <?php else: ?>
                        <tr class="align-middle">
                            <td class="p-2 text-center"><?=$dossier->dossier_id;?></td>
                            <td class="text-start"><?=$dossier->client;?></td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots) && !empty($dossier->lots)):?>
                                    <?=$dossier->lots[0]->progm_nom; ?>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->mandats) && !empty($dossier->lots[0]->mandats)):?>
                                    <span class="badge badge-soft-success fs--1">Oui</span>
                                <?php else: ?>
                                    <span class="badge badge-soft-secondary fs--1">Non</span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">***</td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->document_acts) && !empty($dossier->lots[0]->document_acts)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                 <?php if(isset($dossier->lots[0]->bails) && !empty($dossier->lots[0]->bails)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots[0]->taxe_foncieres) && !empty($dossier->lots[0]->taxe_foncieres)):?>
                                    <?=$dossier->lots[0]->taxe_foncieres[0]->taxe_montant; ?> €
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots[0]->charges) && !empty($dossier->lots[0]->charges)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->prets) && !empty($dossier->lots[0]->prets)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(!empty($dossier->suivi)): ?>
                                    <?= array_values($dossier->suivi)[0]->nom.' '.array_values($dossier->suivi)[0]->prenom;?>
                                <?php endif; ?>
                            </td>
                            <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                    <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endif;?>
                <?php endforeach;?>
            <?php else:?>
                <tr>
                    <td class="text-center py-2" colspan="12">
                        Aucun enregistrement
                    </td>
                </tr>
            <?php endif;?>
        </tbody>
    </table>
    
</div>