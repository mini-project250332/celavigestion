<div class="row m-0 py-1 pb-2 d-none">
    <div class="col-md-auto p-0 d-none">
        <button class="btn btn-sm bg-light  fw-semi-bold">
            Ligne
        </button>
    </div>
    <div class="col-md-auto p-0 d-none">
        <select class="form-select row_view" id="row_view" name="row_view" width="80px">
            <?php foreach (unserialize(row_view) as $key => $value) : ?>
                <option value="<?= $value; ?>" <?= ($value == intval($rows_limit)) ? 'selected' : ''; ?>>
                    <?= $value; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="pagination_view" id="pagination_view">
    </div>
    <div class="col p-1 ">
        <div class="d-flex align-items-center position-relative">
            <div class="flex-1 pt-1">
                <h6 class="mb-0 fw-semi-bold"> Resulat(s) :
                    <?= $countAllResult ?> enregistré(s)
                </h6>
            </div>
        </div>
    </div>
    <div class="col-md-auto p-0">
        <button class="btn btn-sm btn-secondary mx-1 only_visuel_gestion"><i class="fas fa-file-excel"></i>
            Exporter</button>
        <button id="btnFormNewClient" class="btn btn-sm btn-primary no_account_manager" type="button">
            <span class="bi bi-person-plus-fill me-1"></span>
            Nouveau propriétaire
        </button>
    </div>
</div>

<div class="mb-2">
    <table class="table table-sm fs--1 mb-0">
        <thead class="text-900">
            <tr class="text-center">
                <th class="p-2">N° dossier</th>
                <th class="p-2 text-start">Nom des propriétaires</th>
                <th class="p-2 text-start">Nom du programme</th>
                <th class="p-2" >Mandat CELAVI</th>
                <th class="p-2">Lettre EC</th>
                <th class="p-2">Acte</th>
                <th class="p-2">Bail </th>
                <th class="p-2">Taxe foncière</th>
                <th class="p-2">Rep.Charge</th>
                <th class="p-2">Prêt </th>
                <th class="p-2 text-start">Conseiller clientèle</th>
                <th class="p-2">actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($dossiers)):?>
                <?php foreach ($dossiers as $key => $dossier):?>
                    <?php $countsLots = count($dossier->lots);?>
                    <?php if($countsLots > 1): ?>

                        <tr class="align-middle">
                            <td class="p-2" rowspan="<?=$countsLots;?>"><?=$dossier->dossier_id;?></td>
                            <td class="text-start" rowspan="<?=$countsLots;?>"><?=$dossier->client;?></td>

                            <td class="text-start px-1"><?=$dossier->lots[0]->progm_nom; ?></td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->mandats) && !empty($dossier->lots[0]->mandats)):?>
                                    <span class="badge badge-soft-success fs--1">Oui</span>
                                <?php else: ?>
                                    <span class="badge badge-soft-secondary fs--1">Non</span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">***</td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->document_acts) && !empty($dossier->lots[0]->document_acts)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                 <?php if(isset($dossier->lots[0]->bails) && !empty($dossier->lots[0]->bails)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->taxe_foncieres) && !empty($dossier->lots[0]->taxe_foncieres)):?>
                                    <?=$dossier->lots[0]->taxe_foncieres[0]->taxe_montant; ?> €
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->charges) && !empty($dossier->lots[0]->charges)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->prets) && !empty($dossier->lots[0]->prets)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>

                            <td class="text-start" rowspan="<?=$countsLots;?>">
                                <?php if(!empty($dossier->suivi)): ?>
                                    <?= array_values($dossier->suivi)[0]->nom.' '.array_values($dossier->suivi)[0]->prenom;?>
                                <?php endif; ?>
                            </td>

                            <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                    <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                </button>
                            </td>
                        </tr>
                        <?php foreach (array_values($dossier->lots) as $key_lot => $lot): ?>
                            <?php 
                                $border_space = ($key_lot == ($countsLots -1 )) ? " border-bottom-width: 1px;" : '';
                            ?>
                            <?php if($key_lot != 0):?>
                                <tr class="align-middle" style="<?=$border_space?>">
                                    <td class="text-start px-1"><?=$lot->progm_nom?></td>
                                    <td class="text-center">
                                        <?php if(isset($lot->mandats) && !empty($lot->mandats)):?>
                                            <span class="badge badge-soft-success fs--1">Oui</span>
                                        <?php else: ?>
                                            <span class="badge badge-soft-secondary fs--1">Non</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">***</td>
                                    <td class="text-center">
                                        <?php if(isset($lot->document_acts) && !empty($lot->document_acts)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->bails) && !empty($lot->bails)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->taxe_foncieres) && !empty($lot->taxe_foncieres)):?>
                                            <?=$lot->taxe_foncieres[0]->taxe_montant; ?> €
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->charges) && !empty($lot->charges)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if(isset($lot->prets) && !empty($lot->prets)):?>
                                            <i class="fas fa-check-circle text-success fa-lg"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                        <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                            <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                        </button>
                                    </td>
                                </tr>
                        <?php endif;?>
                        <?php endforeach;?>
                    <?php else: ?>
                        <tr class="align-middle">
                            <td class="p-2"><?=$dossier->dossier_id;?></td>
                            <td class="text-start"><?=$dossier->client;?></td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots) && !empty($dossier->lots)):?>
                                    <?=$dossier->lots[0]->progm_nom; ?>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->mandats) && !empty($dossier->lots[0]->mandats)):?>
                                    <span class="badge badge-soft-success fs--1">Oui</span>
                                <?php else: ?>
                                    <span class="badge badge-soft-secondary fs--1">Non</span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">***</td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->document_acts) && !empty($dossier->lots[0]->document_acts)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                 <?php if(isset($dossier->lots[0]->bails) && !empty($dossier->lots[0]->bails)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots[0]->taxe_foncieres) && !empty($dossier->lots[0]->taxe_foncieres)):?>
                                    <?=$dossier->lots[0]->taxe_foncieres[0]->taxe_montant; ?> €
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(isset($dossier->lots[0]->charges) && !empty($dossier->lots[0]->charges)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if(isset($dossier->lots[0]->prets) && !empty($dossier->lots[0]->prets)):?>
                                    <i class="fas fa-check-circle text-success fa-lg"></i>
                                <?php endif; ?>
                            </td>
                            <td class="text-start">
                                <?php if(!empty($dossier->suivi)): ?>
                                    <?= array_values($dossier->suivi)[0]->nom.' '.array_values($dossier->suivi)[0]->prenom;?>
                                <?php endif; ?>
                            </td>
                            <td class="d-flex justify-content-center" style="padding-right: .25rem !important;">
                                <button data-id="" class="btn btn-sm btn-outline-primary icon-btn" style="width: 34px;height: 34px;" id="" type="button">
                                    <i class="fas fa-play fa-w-12" style="font-size: 1.5rem !important;"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endif;?>
                <?php endforeach;?>
            <?php else:?>
                <tr>
                    <td class="text-center py-2" colspan="12">
                        Aucun enregistrement
                    </td>
                </tr>
            <?php endif;?>
        </tbody>
    </table>
    
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-<?= $value; ?>" class="page-item pagination-vue-table <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>