<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Liste des dossiers</h5>
    </div>
    <div class="px-2">
    </div>
</div>
<div class="contenair-list">
    <div class="row">
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Nom propriétaire</label>
                <input class="form-control mb-0" type="text" id="text_filtre_dossier" name="text_filtre_dossier">
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Gestionnaire</label>
                <select class="form-select fs--1" id="gestionnaire_id_doss" name="gestionnaire_id_doss">
                    <option value="">Tous</option>
                    <?php foreach ($gestionnaire as $gestionnaires) : ?>
                        <option value="<?= $gestionnaires->gestionnaire_id; ?>">
                            <?= $gestionnaires->gestionnaire_nom; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" name="filtre_programme" id="filtre_programme" placeholder="Nom,ville">
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Numéro dossier</label>
                <input class="form-control mb-0" type="text" name="filtre_dossier_menu" id="filtre_dossier_menu">
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Numéro SIRET</label>
                <input class="form-control mb-0" type="text" name="filtre_siret_menu" id="filtre_siret_menu">
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Numéro mandat</label>
                <input class="form-control mb-0" type="text" name="filtre_mandat_menu" id="filtre_mandat_menu">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Acompte TVA</label>
                <select class="form-select fs--1" id="acompte_tva" name="acompte_tva">
                    <option value="">Tous</option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Date creation</label>
                <div class="d-flex">
                    <input class="form-control mb-0 calendar" id="date_filtre" name="date_filtre" type="text">
                    <button class="btn btn-sm btn-outline-primary d-flex justify-content-center ms-1 d-none" id="clear_date_filtre">
                        <i class="fas fa-times align-self-center"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-2">
                <label class="form-label">Conseiller clientèle</label>
                <select class="form-select fs--1" id="filtre_util_dossier" name="filtre_util_dossier">
                    <option value="0">Tous les utilisateurs</option>
                    <?php foreach ($utilisateur as $util) : ?>
                        <option value="<?= $util->util_id ?>"><?= $util->util_prenom ?>&nbsp;<?= $util->util_nom ?></option>
                    <?php endforeach ?>
                    <option value="-1">Non renseigné</option>
                </select>
            </div>
        </div>
    </div>
    <div id="data-list" class="col p-0">

    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        listDossier();
    });
    var date_filtre = flatpickr("#date_filtre", {
        altInput: true,
        allowInput: true,
        "mode": "range",
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        onChange: function() {
            $('#clear_date_filtre').removeClass('d-none');
            listDossier();
        },
    });

    $(document).on('click', '#clear_date_filtre', function(e) {
        date_filtre.clear();
        $(this).toggleClass('d-none');
        listDossier();
    })
</script>