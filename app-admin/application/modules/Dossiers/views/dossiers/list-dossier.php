<div class="row m-0 py-1">
	<div class="col-md-auto p-0">
		<button class="btn btn-sm bg-light  fw-semi-bold">
			Ligne
		</button>
	</div>
	<div class="col-md-auto p-0">
		<select class="form-select row_view_dossier" id="row_view_dossier" name="row_view_dossier" width="80px">
			<?php foreach (unserialize(row_view) as $key => $value) : ?>
				<option value="<?= $value; ?>" <?= ($value == intval($rows_limit)) ? 'selected' : ''; ?>>
					<?= $value; ?>
				</option>
			<?php endforeach; ?>
		</select>

		<input type="hidden" name="pagination_view_dossie" id="pagination_view_dossie">
	</div>
	<div class="col p-1 ">
		<div class="d-flex align-items-center position-relative">
			<div class="flex-1 pt-1">
				<h6 class="mb-0 fw-semi-bold"> Resulat(s) :
					<?= $countAllResult ?> enregistré(s)
				</h6>
			</div>
		</div>
	</div>
	<div class="col-md-auto p-0">
		<button class="btn btn-sm btn-secondary mx-1 only_visuel_gestion" id="export-Excel-Dossier" style="display: inline-block;"><svg class="svg-inline--fa fa-file-excel fa-w-12" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-excel" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm60.1 106.5L224 336l60.1 93.5c5.1 8-.6 18.5-10.1 18.5h-34.9c-4.4 0-8.5-2.4-10.6-6.3C208.9 405.5 192 373 192 373c-6.4 14.8-10 20-36.6 68.8-2.1 3.9-6.1 6.3-10.5 6.3H110c-9.5 0-15.2-10.5-10.1-18.5l60.3-93.5-60.3-93.5c-5.2-8 .6-18.5 10.1-18.5h34.8c4.4 0 8.5 2.4 10.6 6.3 26.1 48.8 20 33.6 36.6 68.5 0 0 6.1-11.7 36.6-68.5 2.1-3.9 6.2-6.3 10.6-6.3H274c9.5-.1 15.2 10.4 10.1 18.4zM384 121.9v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z"></path></svg><!-- <i class="fas fa-file-excel"></i> -->
			Exporter</button>
	</div>
</div>
<div class="table-responsive scrollbar pt-2">
    <table class="table table-sm fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Numéro
                    <span class="tri-dossier" style="margin-left: 10%; cursor:pointer;">
                        <i class="fas fa-sort-amount-down-alt"></i>
                    </span>
                </th>
                <th scope="col">Noms propriétaires
                    <span class="tri-dossier" style="margin-left: 10%; cursor:pointer;">
                        <i class="fas fa-sort-amount-down-alt"></i>
                    </span>
                </th>
                <th scope="col">Date création
                    <span class="tri-dossier" style="margin-left: 10%; cursor:pointer;">
                        <i class="fas fa-sort-amount-down-alt"></i>
                    </span>
                </th>
                <th scope="col">Conseiller clientèle</th>
                <th scope="col">Status</th>
                <th class="text-end" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($listDossier)) : ?>
                <?php foreach ($listDossier as $key => $value) : ?>
                    <?php
                    $dossier_etat = $value->dossier_etat;
                    $etat;
                    if ($dossier_etat == 1) {
                        $etat = "Actif";
                    } else {
                        $etat = "Inactif";
                    }
                    ?>
                    <?php if (!empty($dossier_lot_unique)) : ?>
                        <tr id="rowDossier-<?= $value->dossier_id; ?>" class="align-middle <?= in_array($value->dossier_id, $dossier_lot_unique) ? 'bg-warning text-white' : 'bg-200' ?>">
                        <?php else : ?>
                        <tr id="rowDossier-<?= $value->dossier_id; ?>" class="align-middle">
                        <?php endif ?>
                        <td class="px-2">
                            <div class="d-flex align-items-center position-relative">
                                <div class="flex-1">
                                    <h6 class="mb-0 fw-semi-bold">
                                        <a class="stretched-link text-900" href="#">
                                            <?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></a>
                                    </h6>
                                </div>
                            </div>
                        </td>
                        <td><?= $value->nom_prenom; ?></td>
                        <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $value->dossier_d_creation))); ?></td>
                        <td><?= !empty($value->utilNom) || !empty($value->utilNutilPrenomom) ? $value->utilNom . ' ' . $value->utilPrenom : 'Non renseigné' ?></td>
                        <td><?= $etat; ?></td>
                        <td class="btn-action-table">
                            <div class="d-flex justify-content-end ">
                                <button data-id="<?= $value->dossier_id; ?>" data-client_id="<?= $value->client_id ?>" class="btn btn-sm btn-outline-primary icon-btn fiche-dossier" data-nom="<?= $value->client_nom ?>" type="button">
                                    <span class="fas fa-play fa-w-14 fs-1"></span>
                                </button>
                            </div>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td class="text-center fs-1" colspan="6"> Aucun dossier trouvé</td>
                    </tr>
                <?php endif ?>
        </tbody>
    </table>
</div>
<div class="d-flex justify-content-end pt-2">
	<div class="d-flex align-items-center p-0">
		<nav aria-label="Page navigation example">
			<ul id="pagination-ul-dossier" class="pagination pagination-sm mb-0">
				<li class="page-item" id="pagination-vue-preview-dossier">
					<a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
						Précédent
					</a>
				</li>
				<?php foreach ($li_pagination as $key => $value) : ?>
					<li data-offset="<?= $value; ?>" id="pagination-vue-table-dossier-<?= $value; ?>" class="page-item pagination-vue-table-dossier <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
						<a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
							<?= $key + 1; ?>
						</a>
					</li>
				<?php endforeach; ?>

				<li class="page-item" id="pagination-vue-next-dossier">
					<a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
						Suivant
					</a>
				</li>
			</ul>
		</nav>
	</div>
</div>