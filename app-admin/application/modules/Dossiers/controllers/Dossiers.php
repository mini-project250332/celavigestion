<?php

use Faker\Calculator\Iban;

class Dossiers extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Dossiers";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/dossiers/dossiers.js",
            "js/fiscal/fiscal.js",
            "js/saisietemps/saisietemps.js"
        );
        $this->_css_personnaliser = array(
            "css/clients/clients.css",
            "css/select2.min.css",
            "css/historique/historique.css",
            "css/loyer/loyer.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "dossiers";
        $this->_data['sous_module'] = "dossiers/contenaire-dossier";

        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['utilisateur'] = $this->getListeUtilisateur();
                $this->renderComponant("Dossiers/dossiers/header-liste", $data);
            }
        }
    }

    function getListDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $like = array();
                $or_like = array();
                $clause = array('dossier_etat' => 1);
                $data_post = $_POST['data'];
                $rows_limit = intval($data_post['row_data']);

                if ($data_post['filtre_dossier_menu'] != '') {
                    $clause['c_dossier.dossier_id'] = $data_post['filtre_dossier_menu'];
                }

                if ($data_post['filtre_siret_menu'] != '') {
                    $like['dossier_siret'] = $data_post['filtre_siret_menu'];
                }

                if ($data_post['text_filtre_dossier'] != '') {
                    $like['client_nom'] = $data_post['text_filtre_dossier'];
                }

                if ($data_post['filtre_programme'] != '') {
                    $like['progm_nom'] = $data_post['filtre_programme'];
                    $or_like['progm_ville'] = $data_post['filtre_programme'];
                }

                if ($data_post['filtre_mandat_menu'] != '') {
                    $clause['mandat_cli_id'] = $data_post['filtre_mandat_menu'];
                }

                if ($data_post['acompte_tva'] != '') {
                    $clause['acompte_tva'] = $data_post['acompte_tva'];
                }

                if ($data_post['util_id'] != 0) {
                    $clause['c_dossier.util_id'] = $data_post['util_id'];
                }

                if ($data_post['util_id'] == -1) {
                    $clause =  array('c_dossier.util_id' => NULL);
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['client_date_creation >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['client_date_creation <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['client_date_creation'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }

                $pagination_data = explode('-', $data_post['pagination_page']);

                $join = array(
                    'c_sie' => 'c_sie.sie_id = c_dossier.sie_id',
                    'c_client' => 'c_client.client_id = c_dossier.client_id',
                    'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_mandat_client' => 'c_mandat_client.client_id = c_client.client_id',
                    'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                );

                $params = array(
                    'clause' => $clause,
                    'like' => $like,
                    'or_like' => $or_like,
                    'columns' => array(
                        'c_dossier.dossier_id',
                        'dossier_d_creation',
                        'dossier_d_modification',
                        'dossier_etat',
                        'dossier_rcs',
                        'dossier_siret',
                        'dossier_ape',
                        'dossier_tva_intracommunautaire',
                        'dossier_adresse1',
                        'dossier_adresse2',
                        'dossier_adresse3',
                        'dossier_cp',
                        'dossier_ville',
                        'dossier_pays',
                        'dossier_type_adresse',
                        'regime',
                        'cloture_jour',
                        'cloture_mois',
                        'cloture_jour_fin',
                        'cloture_mois_fin',
                        'sie_libelle',
                        'c_sie.sie_id',
                        'c_client.client_id',
                        'client_nom',
                        'client_prenom',
                        'client_email as nom_prenom',
                        'cin',
                        'acompte_tva',
                        'c_utilisateur.util_nom as utilNom',
                        'c_utilisateur.util_prenom as utilPrenom',
                    ),
                    'join' => $join,
                    'join_orientation' => 'left',
                    'order_by_columns' => 'c_dossier.dossier_id ASC',
                    'limit' => array('offset' => $pagination_data[1], 'limit' => $pagination_data[0]),
                    'distinct' => true
                );

                $request = $this->dossier->get($params);
                $selfdossier = $this->dossier->get(array());

                $client = array();
                $client = array_map(function ($value) {
                    $array_client = explode(',', $value->client_id);
                    $table_nom = array_map([$this, 'getClient'], $array_client);
                    $nom_prenom = implode(',', $table_nom);

                    return [
                        'dossier_id' => $value->dossier_id,
                        'nom_prenom' => $nom_prenom
                    ];
                }, $selfdossier);

                array_map(function ($value_request) use ($client) {
                    foreach ($client as $value) {
                        if ($value_request->dossier_id == $value['dossier_id']) {
                            $value_request->nom_prenom = $value['nom_prenom'];
                        }
                    }
                }, $request);

                $data["listDossier"] = $request;
                $nbr = count($request);
                $data["countAllResult"] = $this->countAllData('Dossier_m', array('dossier_etat' => 1));

                if (
                    $data_post['text_filtre_dossier'] != '' ||
                    $data_post['filtre_dossier_menu'] != '' ||
                    $data_post['filtre_siret_menu'] != '' ||
                    $data_post['gestionnaire_id_doss'] != '' ||
                    $data_post['date_filtre'] != '' ||
                    $data_post['filtre_programme'] != '' ||
                    $data_post['filtre_mandat_menu'] != '' ||
                    $data_post['acompte_tva'] != '' ||
                    $data_post['util_id'] != 0
                ) {
                    $data["countAllResult"] = $this->countUser($clause, $like, $or_like, $join);
                }

                $nb_total = $data["countAllResult"];

                $data['pagination_page'] = $data_post['pagination_page'];
                $data['rows_limit'] = $rows_limit;

                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];



                $result_dossier_lot_unique = $this->get_client_lot_unique();
                $dossier_lot_unique = array();

                if (!empty($result_dossier_lot_unique)) {
                    foreach ($result_dossier_lot_unique as $value) {
                        array_push($dossier_lot_unique, $value->dossier_id);
                    }
                }
                $data['dossier_lot_unique'] = $dossier_lot_unique;

                $this->renderComponant("Dossiers/dossiers/list-dossier", $data);
            }
        }
    }

    public function export_csv_dossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $this->load->helper("exportation");
                $like = array();
                $or_like = array();
                $clause = array('dossier_etat' => 1);
                $data_post = $_POST['data'];


                if ($data_post['filtre_dossier_menu'] != '') {
                    $clause['c_dossier.dossier_id'] = $data_post['filtre_dossier_menu'];
                }

                if ($data_post['filtre_siret_menu'] != '') {
                    $like['dossier_siret'] = $data_post['filtre_siret_menu'];
                }

                if ($data_post['text_filtre_dossier'] != '') {
                    $like['client_nom'] = $data_post['text_filtre_dossier'];
                }

                if ($data_post['filtre_programme'] != '') {
                    $like['progm_nom'] = $data_post['filtre_programme'];
                    $or_like['progm_ville'] = $data_post['filtre_programme'];
                }

                if ($data_post['filtre_mandat_menu'] != '') {
                    $clause['mandat_cli_id'] = $data_post['filtre_mandat_menu'];
                }

                if ($data_post['acompte_tva'] != '') {
                    $clause['acompte_tva'] = $data_post['acompte_tva'];
                }

                if ($data_post['util_id'] != 0) {
                    $clause['c_dossier.util_id'] = $data_post['util_id'];
                }

                if ($data_post['util_id'] == -1) {
                    $clause =  array('c_dossier.util_id' => NULL);
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['client_date_creation >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['client_date_creation <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['client_date_creation'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }



                $join = array(
                    'c_sie' => 'c_sie.sie_id = c_dossier.sie_id',
                    'c_client' => 'c_client.client_id = c_dossier.client_id',
                    'c_lot_client' => 'c_client.client_id = c_lot_client.cliLot_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_mandat_client' => 'c_mandat_client.client_id = c_client.client_id',
                    'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                );

                $params = array(
                    'clause' => $clause,
                    'like' => $like,
                    'or_like' => $or_like,
                    'columns' => array(
                        'c_dossier.dossier_id',
                        'dossier_d_creation',
                        'dossier_d_modification',
                        'dossier_etat',
                        'dossier_rcs',
                        'dossier_siret',
                        'dossier_ape',
                        'dossier_tva_intracommunautaire',
                        'dossier_adresse1',
                        'dossier_adresse2',
                        'dossier_adresse3',
                        'dossier_cp',
                        'dossier_ville',
                        'dossier_pays',
                        'dossier_type_adresse',
                        'regime',
                        'cloture_jour',
                        'cloture_mois',
                        'cloture_jour_fin',
                        'cloture_mois_fin',
                        'sie_libelle',
                        'c_sie.sie_id',
                        'c_client.client_id',
                        'client_nom',
                        'client_prenom',
                        'client_email as nom_prenom',
                        'cin',
                        'acompte_tva',
                        'c_utilisateur.util_nom as utilNom',
                        'c_utilisateur.util_prenom as utilPrenom',
                    ),
                    'join' => $join,
                    'join_orientation' => 'left',
                    'order_by_columns' => 'c_dossier.dossier_id ASC',
                    'distinct' => true
                );

                $request = $this->dossier->get($params);
                $selfdossier = $this->dossier->get(array());

                $client = array();
                $client = array_map(function ($value) {
                    $array_client = explode(',', $value->client_id);
                    $table_nom = array_map([$this, 'getClient'], $array_client);
                    $nom_prenom = implode(',', $table_nom);

                    return [
                        'dossier_id' => $value->dossier_id,
                        'nom_prenom' => $nom_prenom
                    ];
                }, $selfdossier);

                array_map(function ($value_request) use ($client) {
                    foreach ($client as $value) {
                        if ($value_request->dossier_id == $value['dossier_id']) {
                            $value_request->nom_prenom = $value['nom_prenom'];
                        }
                    }
                }, $request);

                // var_dump($request); die();
                $excel_url = exportation_excel_dossier($request);
                echo $excel_url;
            }
        }
    }

    public function countUser($clause, $like, $or_like, $join)
    {
        $this->load->model('Dossier_m', 'dossier');
        $request = $this->dossier->get(
            array(
                'clause' => $clause,
                'like' => $like,
                'or_like' => $or_like,
                'columns' => array(
                    'c_dossier.dossier_id',
                    'dossier_d_creation',
                    'dossier_d_modification',
                    'dossier_etat',
                    'dossier_rcs',
                    'dossier_siret',
                    'dossier_ape',
                    'dossier_tva_intracommunautaire',
                    'dossier_adresse1',
                    'dossier_adresse2',
                    'dossier_adresse3',
                    'dossier_cp',
                    'dossier_ville',
                    'dossier_pays',
                    'dossier_type_adresse',
                    'regime',
                    'cloture_jour',
                    'cloture_mois',
                    'cloture_jour_fin',
                    'cloture_mois_fin',
                    'sie_libelle',
                    'c_sie.sie_id',
                    'c_client.client_id',
                    'client_nom',
                    'client_prenom',
                    'client_email as nom_prenom',
                    'cin',
                    'acompte_tva',
                    'c_utilisateur.util_nom as utilNom',
                    'c_utilisateur.util_prenom as utilPrenom',
                ),
                'join' => $join,
                'join_orientation' => 'left',
                'order_by_columns' => 'c_dossier.dossier_id ASC',
                'distinct' => true,
            )
        );

        return !empty($request) ? count($request) : 0;
    }

    function countAllData($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }

    public function get_client_lot_unique()
    {
        $this->load->model('Client_m', 'client');
        $this->load->model('ClientLot_m', 'lot');

        $table_unique = array();

        $params = array(
            'clause' => array('c_lot_client.typelot_id' => 7),
            'join' => array(
                'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
            ),
            'group_by_columns' => 'c_dossier.dossier_id',
        );

        $resultat_lot = $this->lot->get($params);

        if (!empty($resultat_lot)) {
            foreach ($resultat_lot as $key => $value) {
                $param_row = array(
                    'clause' => array('c_lot_client.dossier_id' => $value->dossier_id),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    ),
                );
                $check_list_row = $this->lot->get($param_row);

                if (count($check_list_row) < 2) {
                    foreach ($check_list_row as $val) {
                        array_push($table_unique, $val);
                    }
                }
            }
        }
        return $table_unique;
    }

    public function getListeLotbyDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('ClientLot_m', 'client_lot');

                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'c_lot_client.dossier_id ' => $data_post['dossier_id']
                    ),
                    'method' => "result",
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                        'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id'
                    ),
                );

                $data_lot = $this->client_lot->get($params);


                $listelot = array();

                // if (!empty($data_lot)) {
                //     foreach ($data_lot as $key => $value) {
                //         $avoir_charge = $this->getCharge($value->cliLot_id);
                //         $avoir_facture = $this->getFacture($value->cliLot_id);
                //         $avoir_encaiss = $this->getEncaissement($value->cliLot_id);
                //         if ($avoir_charge == false && $avoir_facture == false &&  $avoir_encaiss == false) {
                //             $array_lot = array(
                //                 'cliLot_id' => $value->cliLot_id,
                //                 'dossier_id' => $value->dossier_id,
                //                 'progm_nom' => $value->progm_nom,
                //                 'progm_ville' => $value->progm_ville,
                //                 'typelot_libelle' => $value->typelot_libelle,
                //                 'gestionnaire_nom' => $value->gestionnaire_nom,
                //                 'cliLot_principale' => $value->cliLot_principale,
                //                 'avoir_data' => false
                //             );
                //             array_push($listelot, $array_lot);
                //         } else {
                //             $array_lot = array(
                //                 'cliLot_id' => $value->cliLot_id,
                //                 'dossier_id' => $value->dossier_id,
                //                 'progm_nom' => $value->progm_nom,
                //                 'progm_ville' => $value->progm_ville,
                //                 'typelot_libelle' => $value->typelot_libelle,
                //                 'gestionnaire_nom' => $value->gestionnaire_nom,
                //                 'cliLot_principale' => $value->cliLot_principale,
                //                 'avoir_data' => true
                //             );
                //             array_push($listelot, $array_lot);
                //         }
                //     }
                // }
                if (!empty($data_lot)) {
                    foreach ($data_lot as $key => $value) {
                        $avoir_charge = $this->getCharge($value->cliLot_id);
                        $avoir_facture = $this->getFacture($value->cliLot_id);
                        $avoir_encaiss = $this->getEncaissement($value->cliLot_id);
                        if ($avoir_charge == false && $avoir_facture == false && $avoir_encaiss == false) {
                            $array_lot = array(
                                'cliLot_id' => $value->cliLot_id,
                                'dossier_id' => $value->dossier_id,
                                'progm_id' => $value->progm_id,
                                'progm_nom' => $value->progm_nom,
                                'progm_ville' => $value->progm_ville,
                                'typelot_libelle' => $value->typelot_libelle,
                                'gestionnaire_nom' => $value->gestionnaire_nom,
                                'cliLot_principale' => $value->cliLot_principale,
                                'cliLot_ville' => $value->cliLot_ville,
                                'avoir_data' => false
                            );
                            array_push($listelot, $array_lot);
                        } else {
                            $array_lot = array(
                                'cliLot_id' => $value->cliLot_id,
                                'dossier_id' => $value->dossier_id,
                                'progm_id' => $value->progm_id,
                                'progm_nom' => $value->progm_nom,
                                'progm_ville' => $value->progm_ville,
                                'typelot_libelle' => $value->typelot_libelle,
                                'gestionnaire_nom' => $value->gestionnaire_nom,
                                'cliLot_principale' => $value->cliLot_principale,
                                'cliLot_ville' => $value->cliLot_ville,
                                'avoir_data' => true
                            );
                            array_push($listelot, $array_lot);
                        }
                    }
                }
                $data['lot'] = $listelot;
                // var_dump($data['lot']);
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['client_id'] = $data_post['client_id'];
                $data['dossier'] = $this->getDossier();

                $this->renderComponant("Clients/clients/dossier/lots_dossiers/list-lot_dossier", $data);
            }
        }
    }

    private function getCharge($cliLot_id)
    {
        $this->load->model('Charges_m');
        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
            'join' => array(
                'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id'
            ),
        );
        $avoir_data = false;
        $charge = $this->Charges_m->get($param);
        if (!empty($charge)) {
            $avoir_data = true;
        }
        return $avoir_data;
    }

    private function getFacture($cliLot_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('FactureLoyer_m', 'facture');

        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
        );

        $bailLot = $this->bail->get($param);
        $avoir_data = false;
        foreach ($bailLot as $key => $value) {
            $param_facure = array('clause' => array('bail_id' => $value->bail_id));
            $facture = $this->facture->get($param_facure);
            if (!empty($facture)) {
                $avoir_data = true;
            }
        }
        return $avoir_data;
    }

    private function getEncaissement($cliLot_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('Encaissement_m', 'encaiss');

        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
        );

        $bailLot = $this->bail->get($param);
        $avoir_data = false;
        foreach ($bailLot as $key => $value) {
            $param_encaiss = array('clause' => array('bail_id' => $value->bail_id));
            $encaiss = $this->encaiss->get($param_encaiss);
            if (!empty($encaiss)) {
                $avoir_data = true;
            }
        }
        return $avoir_data;
    }

    private function getDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('dossier_etat' => 1),
                    'columns' => array('dossier_id', 'dossier_libelle', 'client_id')
                );
                $request = $this->dossier->get($params);
                $data["client_id"] = $data_post['client_id'];
                $liste_dossier = array();
                foreach ($request as $key => $value) {
                    if ($value->client_id != "") {
                        $array_client = explode(',', $value->client_id);
                        if (in_array($data["client_id"], $array_client)) {
                            $client = array();
                            foreach ($array_client as $value_client) {
                                array_push($client, $this->getClient($value_client));
                            }
                            $request[$key]->client = implode(' , ', $client);
                            $liste_dossier[] = $request[$key];
                        }
                    }
                }
                return $liste_dossier;
            }
        }
    }

    public function getFormLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['client_id'] = $data_post['client_id'];

                $this->load->model('ClientLot_m');

                $data['action'] = $data_post['action'];

                $data['dossier_id'] = $data_post['dossier_id'];
                $data['dossier'] = $this->getDossier();
                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['produitcli'] = $this->getlisteProduitClient();

                $clause = array(
                    'cli_id' => $data_post['client_id'],
                    'cliLot_principale' => 1
                );
                $lot_principal_request = $this->ClientLot_m->get(
                    array(
                        'columns' => ['cliLot_principale'],
                        'clause' => $clause
                    )
                );

                $data["lot_verify_principal"] = (!empty($lot_principal_request)) ? 2 : 1;

                $this->renderComponant("Clients/clients/dossier/lots_dossiers/form-lot", $data);
            }
        }
    }

    public function getListProgram()
    {

        $this->load->model('Program_m');
        $params = array(
            'clause' => array('progm_etat' => 1),
            'columns' => array('progm_id', 'progm_nom')
        );
        $request = $this->Program_m->get($params);
        return $request;
    }

    public function getDocumentDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('DocumentDossier_m');

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('doc_id', 'doc_nom', 'doc_path', 'doc_date_creation'),
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_etat' => 1
                    ),
                );

                $data["dossier_id"] = $data_post['dossier_id'];
                $data['document'] = $this->DocumentDossier_m->get($params);

                $this->renderComponant("Clients/clients/dossier/document_dossier/list-document", $data);
            }
        }
    }

    public function formUpload()
    {
        $data_post = $_POST['data'];
        $data['dossier_id'] = $data_post['dossier_id'];

        $this->renderComponant('Clients/clients/dossier/document_dossier/formulaire_upload_file', $data);
    }

    public function uploadfile()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        // $cli_id = $_POST['cli_id'];
        $dossier_id = $_POST['dossier_id'];

        // Chemin du dossier
        $targetDir = "../documents/dossier/$dossier_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_nom' => $original_name,
                'doc_date_creation' => date('Y-m-d H:i:s'),
                'doc_path' => str_replace('\\', '/', $filePath),
                'doc_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'dossier_id' => $dossier_id
            );

            $this->load->model('DocumentDossier_m');
            $this->DocumentDossier_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getDocpath()
    {
        $this->load->model('DocumentDossier_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_id', 'doc_path'),
                    'clause'  => array('doc_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentDossier_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function FormUpdateDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['dossier_id'] = $data_post['dossier_id'];

                $this->load->model('DocumentDossier_m');
                $data['document'] = $this->DocumentDossier_m->get(
                    array('clause' => array("doc_id" => $data_post['doc_id']), 'method' => "row")
                );
            }

            $this->renderComponant('Clients/clients/dossier/document_dossier/formUpdateDossier', $data);
        }
    }

    public function UpdateDocumentDossiers()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentDossier_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("dossier_id" => $data['dossier_id']);
                    $doc_id = $data['doc_id'];
                    unset($data['doc_id']);

                    $clause = array("doc_id" => $doc_id);
                    $request = $this->DocumentDossier_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDocDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentDossier_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_id' => $data['doc_id']),
                    'columns' => array('doc_id'),
                    'method' => "row",
                );
                $request_get = $this->DocumentDossier_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_id' => $data['doc_id']);
                    $data_update = array('doc_etat' => 0);
                    $request = $this->DocumentDossier_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formAlertDeleteDossier()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/dossier/lots_dossiers/form-alert");
        }
    }

    public function Alerte_cin()
    {
        if (is_ajax()) {
            $data['nom'] = $_POST['data']['nom'];
            $this->renderComponant("Dossiers/dossiers/form-alert-cin", $data);
        }
    }

    public function getFacturation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'client_lot');
                $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
                $this->load->model('MandatNew_m');
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('FactureArticle_m', 'facture_articles');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                // Information lot
                $anneeMoinsUn = date("Y", strtotime("-1 year"));
                $data_lot = $this->client_lot->getLotFacturation($anneeMoinsUn, $data_post['dossier_id']);
                $retrieveMandatId = function ($value) {
                    $mandat = $this->MandatNew_m->get(array(
                        'clause' => array('cliLot_id' => $value->cliLot_id),
                        'order_by_columns' => "mandat_id DESC",
                        'method' => 'row'
                    ));
                    $value->mandat_id = NULL;

                    if (!empty($mandat)) {
                        $value->mandat_id = $mandat->mandat_id;
                    }

                    return $value;
                };
                $data['data_lot'] = $data_lot = array_map($retrieveMandatId, $data_lot);
                $data['mode_paiement'] = $this->mode_paiement->get(array());

                // Information facture 
                $facture = $this->facture_annuelle->getFactureDossier($data_post['dossier_id']);

                if (!empty($facture)) {
                    $facture = array_map(function ($item) {
                        $item->progm_nom = $this->facture_articles->getPrgmNom($item->fact_id)->progm_nom;
                        return $item;
                    }, $facture);

                    $data['facture'] = $facture;
                }

                $data['mode_reglement'] = [2 => 'VIREMENT', 3 => 'PRELEVEMENT', 4 => 'PRIS SUR LES LOYERS'];

                if ($data_post['action'] == 'fiche') {
                    $this->renderComponant("Clients/clients/dossier/facturation/content-facturation", $data);
                } else {
                    $this->renderComponant("Clients/clients/dossier/facturation/form-facturation", $data);
                }
            }
        }
    }

    function infoReglement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Reglement_ventilation_m', 'ventilationreglement');
                $datapost = $_POST['data'];

                $data['reglements'] = $this->ventilationreglement->get(
                    [
                        'clause' => ['id_facture' => $datapost['fact_id']],
                        'join' => ['c_reglements' => 'c_reglements.idc_reglements = c_reglementventilation.idc_reglements',]
                    ]
                );
                $data['mode_reglement'] = [2 => 'VIREMENT', 3 => 'PRELEVEMENT', 4 => 'PRIS SUR LES LOYERS'];
                $this->renderComponant("Clients/clients/dossier/facturation/reglementInfo", $data);
            }
        }
    }

    function pageReglementDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
                $this->load->model('Banque_m', 'banque');
                $this->load->model('Facture_m', 'facture_annuelle');
                $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
                $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2, 4]);
                $data['facture'] = $this->facture_annuelle->getfactureAnnuelleByid($_POST['data']['fact_id']);
                $data['dossier_id'] = $_POST['data']['dossier_id'];
                $this->renderComponant("Clients/clients/dossier/facturation/pageReglementDossier", $data);
            }
        }
    }

    function saveReglementDossier()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montantreglment = floatval(str_replace(",", ".", $data['montant_sur_reglement']));
                $saveReglement = [
                    'mode_reglement' => $data['mode_reglement'],
                    'date_reglement' => $data['datereglement'],
                    'id_banque' => $data['banque_celavi_reglement'],
                    'dossier_id' => $data['dossier_id_reglment'],
                    'montant_total' => $montantreglment,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_reglement']
                ];
                $request = $this->reglement->save_data($saveReglement);

                if (!empty($request)) {
                    $this->GererVentilation($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $data['dossier_id_reglment'], $message);
                }
            }
            echo json_encode($retour);
        }
    }

    private function GererVentilation($data)
    {
        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Reglement_m', 'reglement');
        $montantreglment = round(floatval(str_replace(",", ".", $data['montant_sur_reglement'])), 2);
        $totalVentilation = floatval($this->ventilation->getTotalVentile($data['fact_id_reglement']));
        $totalVentilation = round($totalVentilation, 2);
        $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $data['fact_id_reglement']], 'method' => 'row']);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);
        $montantfacture = round(floatval($facture->fact_montantttc) - ($totalVentilation + $montantreglment), 2);

        $saveVentil = [
            'dossier_id' => $data['dossier_id_reglment'],
            'id_facture' => $facture->fact_id,
            'montant' => ($montantfacture < 0) ? round((floatval($facture->fact_montantttc) - $totalVentilation), 2) : round($montantreglment, 2),
            'idc_reglements' => $last_reglement->idc_reglements,
        ];
        $this->ventilation->save_data($saveVentil);

        if ($montantfacture <= 0) {
            $savefacture = ['fact_etatpaiement' => 1];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);

            if ($montantfacture < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossier_id_reglment'],
                    'id_facture' => NULL,
                    'montant' => abs($montantfacture),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }

    function verifierFacture_avoir()
    {
        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $datapost = $_POST['data'];
                $idFacture = $datapost['idfacture'];

                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $idFacture], 'method' => 'row']);
                $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);
                $sommmeventilationrseult = $this->ventilation->get(['columns' => ['montant'], 'clause' => ['id_facture' => $idFacture]]);

                $data['last_date'] = !empty($lastfacturaton) ? date('Y-m-d', strtotime($lastfacturaton->fact_date)) : date('Y-m-d');
                $data['facture'] = $facture;
                $data['title'] = "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cependant, cette facture a déjà été payée intégralement par un précédent règlement. L'avoir sera considéré comme \"impayé / non remboursé\". Il pourra être ventilé sur une autre facture de dû ou remboursé ultérieurement.";

                if ($facture->fact_etatpaiement != 1) {
                    if (!empty($sommmeventilationrseult)) {
                        $data['title'] = "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cette facture a déjà reçu un paiement partiel. L'avoir ventilera le reste à payer de la facture qui sera considéré comme \"payée\" et l'avoir sera noté comme \"non remboursé/ventilé intégralement\". Il pourra faire l'objet d'une affectation sur une autre facture ou d'un remboursement.";
                    } else {
                        $data['title'] =   "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cette facture sera considérée comme payée par l'avoir. L'avoir sera également considéré comme payé/remboursé intégralement.";
                    }
                }

                $this->renderComponant("Clients/clients/dossier/facturation/modal_valide_factureAvoir", $data);
            }
        }
    }

    function save_FactureAvoir()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $this->load->model('FactureArticle_m', 'facture_article');
                $this->load->model('FacturationDossier_m', 'factu_dossier');
                $datapost = $_POST['data'];
                $idFacture = $datapost['idfacture'];

                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $idFacture], 'method' => 'row']);
                $sommmeventilationrseult = $this->ventilation->get(['columns' => ['montant'], 'clause' => ['id_facture' => $idFacture]]);
                $data['annee'] = $anneeChoisi = date('Y');
                $anneeprecedent = $anneeChoisi - 1;

                $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

                if (!empty($lastfacturaton)) {
                    $numero = explode("-", $lastfacturaton->fact_num);
                    $num = intval($numero[1]) + 1;
                    $data['numero_facture'] = $anneeChoisi . '-' . str_pad($num, 5, '0', STR_PAD_LEFT);
                } else {
                    $data['numero_facture'] = "$anneeChoisi-00001";
                }

                $montantAvoir = $facture->fact_montantttc;
                $data_update['fact_etatpaiement'] = 1;
                $data_updateLot['c_facturation_status'] = 0;
                $clause = array('fact_id' => $idFacture);
                $idFactureAvoir = null;

                //Récuperer facture article 
                $lotarticle = $this->facture_article->getArticle($facture->fact_id);


                if ($facture->fact_etatpaiement != 1) {
                    if (!empty($sommmeventilationrseult)) {

                        $sommmeventilation =  array_sum(array_column($sommmeventilationrseult, 'montant'));
                        $montant = round($montantAvoir - $sommmeventilation, 2);

                        $dataFacture_Avoir = [
                            'fact_annee' => date('Y'),
                            'fact_num' => $data['numero_facture'],
                            'fact_date' => $datapost['dateFactureAvoir'],
                            'fact_montantht' => -1 * $facture->fact_montantht,
                            'fact_montanttva' => -1 * $facture->fact_montanttva,
                            'fact_montantttc' => -1 * $facture->fact_montantttc,
                            'dossier_id' => $facture->dossier_id,
                            'fact_modepaiement' => 2,
                            'fact_idFactureDu' => $facture->fact_id,
                            'fact_JustificationAvoir' => $datapost['facture_justification'],

                        ];

                        $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);
                        $this->facture_annuelle->save_data($data_update, $clause);

                        foreach ($lotarticle as $key => $value) {

                            $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                            $dataSaveFactureArticle['art_id'] =  $value->art_id;
                            $dataSaveFactureArticle['lot_id'] = $value->lot_id;
                            $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                            $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                            $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;
                            $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                            $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                            $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                            $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;




                            $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                            if ($value->lot_id != null) {
                                $clauseAprepare = array(
                                    'cliLot_id' => $value->lot_id,
                                    'dossier_id' => $facture->dossier_id,
                                    'annee' => $anneeprecedent
                                );

                                $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                            }
                        }

                        $dataVentilation = [
                            'dossier_id' => $facture->dossier_id,
                            'id_facture' => $facture->fact_id,
                            'montant' => $montant,
                            'Id_factureAvoir' => $idFactureAvoir
                        ];
                        $saveVentilation = $this->ventilation->save_data($dataVentilation);

                        $dataVentilationReste = [
                            'dossier_id' => $facture->dossier_id,
                            'montant' => $sommmeventilation,
                            'id_facture' => $idFactureAvoir
                        ];
                        $saveVentilationreste = $this->ventilation->save_data($dataVentilationReste);
                    } else {
                        $dataFacture_Avoir = [
                            'fact_annee' => date('y'),
                            'fact_num' => $data['numero_facture'],
                            'fact_date' => $datapost['dateFactureAvoir'],
                            'fact_montantht' => -1 * $facture->fact_montantht,
                            'fact_montanttva' => -1 * $facture->fact_montanttva,
                            'fact_montantttc' => -1 * $facture->fact_montantttc,
                            'dossier_id' => $facture->dossier_id,
                            'fact_modepaiement' => 2,
                            'fact_etatpaiement' => 1,
                            'fact_idFactureDu' => $facture->fact_id,
                            'fact_JustificationAvoir' => $datapost['facture_justification'],

                        ];
                        $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);
                        $this->facture_annuelle->save_data($data_update, $clause);

                        foreach ($lotarticle as $key => $value) {

                            $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                            $dataSaveFactureArticle['art_id'] =  $value->art_id;
                            $dataSaveFactureArticle['lot_id'] = $value->lot_id;

                            $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                            $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                            $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;

                            $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                            $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                            $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                            $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;

                            $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                            if ($value->lot_id != null) {
                                $clauseAprepare = array(
                                    'cliLot_id' => $value->lot_id,
                                    'dossier_id' => $facture->dossier_id,
                                    'annee' => $anneeprecedent
                                );

                                $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                            }
                        }

                        $dataVentilation = [
                            'dossier_id' => $facture->dossier_id,
                            'id_facture' => $facture->fact_id,
                            'montant' => $montantAvoir,
                            'Id_factureAvoir' => $idFactureAvoir
                        ];
                        $saveVentilation = $this->ventilation->save_data($dataVentilation);
                    }
                } else {
                    $dataFacture_Avoir = [
                        'fact_annee' => date('Y'),
                        'fact_num' => $data['numero_facture'],
                        'fact_date' => $datapost['dateFactureAvoir'],
                        'fact_montantht' => -1 * $facture->fact_montantht,
                        'fact_montanttva' => -1 * $facture->fact_montanttva,
                        'fact_montantttc' => -1 * $facture->fact_montantttc,
                        'dossier_id' => $facture->dossier_id,
                        'fact_modepaiement' => 2,
                        'fact_idFactureDu' => $facture->fact_id,
                        'fact_JustificationAvoir' => $datapost['facture_justification'],
                    ];

                    $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);

                    foreach ($lotarticle as $key => $value) {
                        $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                        $dataSaveFactureArticle['art_id'] =  $value->art_id;
                        $dataSaveFactureArticle['lot_id'] = $value->lot_id;
                        $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                        $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                        $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;
                        $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                        $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                        $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                        $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;

                        $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                        //etat à facturer
                        if ($value->lot_id != null) {
                            $clauseAprepare = array(
                                'cliLot_id' => $value->lot_id,
                                'dossier_id' => $facture->dossier_id,
                                'annee' => $anneeprecedent
                            );

                            $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                        }
                    }
                }

                $lotarticleFactureAvoir = $this->facture_article->get(
                    array(
                        'clause' => array(
                            'c_facture_articles.fact_id' => $idFactureAvoir,
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_articles.lot_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                        ),
                    )
                );

                $this->Facturer_Avoir($idFactureAvoir, $lotarticleFactureAvoir);
            }
        }
    }

    private function Facturer_Avoir($IdfactureAvoir, $factureArticle)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('DocumentFacturation_m', 'doc_facturation');
            $this->load->model('Article_m', 'article');
            $this->load->model('Facture_m', 'facture_annuelle');
            $this->load->model('MandatSepa_m', 'mandat_sepa');
            $this->load->helper("exportation");

            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la changement du règlement",
            );

            $factureAvoir = $this->facture_annuelle->get(['clause' => ['fact_id' => $IdfactureAvoir], 'method' => 'row']);
            $factureDu = $this->facture_annuelle->get(['clause' => ['fact_id' => $factureAvoir->fact_idFactureDu], 'method' => 'row']);

            $data['annee'] = $anneeChoisi = $factureAvoir->fact_annee;
            $precedent = $anneeChoisi - 1;
            $data['article'] = $this->article->get(['clause' => ['art_id' => 1], 'method' => 'row']);
            $data['lotarticles'] = $factureArticle;

            $data['dossier'] = $this->factu_dossier->get(
                array(
                    'clause' => array('c_facturation_lot.dossier_id' => $factureAvoir->dossier_id, 'c_facturation_lot.annee' => $precedent),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['sepa'] = $this->mandat_sepa->get(
                [
                    'clause' => ['dossier_id' => $factureAvoir->dossier_id],
                    'order_by_columns' => "sepa_id DESC",
                    'method' => 'row'
                ]
            );

            $data['lot_principale'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $factureAvoir->dossier_id, 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['numero_facture'] = $factureAvoir->fact_num;
            $data['numero_facture_du'] = $factureDu->fact_num;
            $data['factureAvoir'] = $factureAvoir;
            $data['rib'] = $this->getRibCelavi();
            $data['date_fature'] = $factureAvoir->fact_date;
            $file_name = 'facturation_annuelle_' .  $factureAvoir->fact_num;
            $view = 'Facturation/paiements/facture_impaye/export.php';

            $pdf =  exportfacture_pdf($view, $file_name, $data, $factureAvoir->fact_annee, $factureAvoir->dossier_id, $factureAvoir->fact_id);
            $this->saveIndepense($factureArticle, $factureAvoir->fact_date);
            $retour = array(
                'status' => 200,
                'data' => array(),
                'message' => "La facture d'avoir a été créée",
                'pdf' => $pdf,
                'dossier_id' => $factureAvoir->dossier_id
            );

            echo json_encode($retour);
        }
    }

    private function saveIndepense($lot, $date_fature)
    {
        $this->load->model('Charges_m', 'charge');
        $this->load->model('DocumentCharges_m', 'doc_charge');
        $this->load->model('DocumentFacturation_m', 'doc_facture');
        $data = [];

        foreach ($lot as $key => $value) {
            $data['date_facture'] = $date_fature;
            $data['cliLot_id'] = $value->lot_id;
            $data['montant_ht'] = $value->faa_puht;
            $data['tva'] = $value->faa_tauxtva;
            $data['tva_taux'] = $value->faa_tvapourcentage;
            $data['montant_ttc'] =  $value->faa_prixttc;
            $data['annee'] = date('Y', strtotime($date_fature));
            $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
            $data['charge_date_saisie'] = date('Y-m-d');
            $data['charge_date_validation'] = date('Y-m-d');
            $data['charge_etat_valide'] = 1;
            $data['type_charge_id'] = 18;

            $request = $this->charge->save_data($data);

            if (!empty($request)) {
                $charge = $this->charge->get(['order_by_columns' => "charge_id DESC", 'method' => 'row']);
                $facture = $this->doc_facture->get(['order_by_columns' => "doc_id DESC", 'method' => 'row']);
                $data_file['doc_charge_nom'] = $facture->doc_facturation_nom;
                $data_file['doc_charge_creation'] = $facture->doc_facturation_creation;
                $data_file['doc_charge_etat'] = 1;
                $data_file['doc_charge_traite'] = 0;
                $data_file['doc_charge_annee'] = $value->annee;
                $data_file['cliLot_id'] = $value->cliLot_id;
                $data_file['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data_file['charge_id'] = $charge->charge_id;
                $data_file['doc_charge_path'] = $facture->doc_facturation_path;
                $request = $this->doc_charge->save_data($data_file);
            }
        }
    }

    private function saveIndepenseRejet($lot, $date_fature)
    {
        $this->load->model('Charges_m', 'charge');
        $this->load->model('DocumentCharges_m', 'doc_charge');
        $this->load->model('DocumentFacturation_m', 'doc_facture');
        $data = [];

        foreach ($lot as $key => $value) {
            $data['date_facture'] = $date_fature;
            $data['cliLot_id'] = $value->lot_id;
            $data['montant_ht'] = $value->faa_puht;
            $data['tva'] = $value->faa_tauxtva;
            $data['tva_taux'] = $value->faa_tvapourcentage;
            $data['montant_ttc'] =  $value->faa_prixttc;
            $data['annee'] = date('Y', strtotime($date_fature));
            $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
            $data['charge_date_saisie'] = date('Y-m-d');
            $data['charge_date_validation'] = date('Y-m-d');
            $data['charge_etat_valide'] = 1;
            $data['type_charge_id'] = 16;

            $request = $this->charge->save_data($data);

            if (!empty($request)) {
                $charge = $this->charge->get(['order_by_columns' => "charge_id DESC", 'method' => 'row']);
                $facture = $this->doc_facture->get(['order_by_columns' => "doc_id DESC", 'method' => 'row']);
                $data_file['doc_charge_nom'] = $facture->doc_facturation_nom;
                $data_file['doc_charge_creation'] = $facture->doc_facturation_creation;
                $data_file['doc_charge_etat'] = 1;
                $data_file['doc_charge_traite'] = 0;
                $data_file['doc_charge_annee'] = $value->annee;
                $data_file['cliLot_id'] = $value->cliLot_id;
                $data_file['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data_file['charge_id'] = $charge->charge_id;
                $data_file['doc_charge_path'] = $facture->doc_facturation_path;
                $request = $this->doc_charge->save_data($data_file);
            }
        }
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function updateFacturation()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ClientLot_m', 'client_lot');
            $this->load->model('Dossier_m', 'dossier');
            $this->load->model('FacturationDossier_m', 'factu_dossier');

            $retour = retour(false, "error", 0, array("message" => "Error"));
            $data_dossier['file_rib'] = NULL;
            $anneeMoinsUn = date("Y", strtotime("-1 year"));

            if (!empty($_FILES)) {
                if (!empty($_FILES['file_rib'])) {
                    $target_dir = "../documents/ClientFacturation_RIB/";
                    $file = $_FILES['file_rib']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file_rib']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    $counter = 1;
                    while (file_exists($path_filename_ext)) {
                        $filename = $filename . '_' . $counter;
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $counter++;
                    }
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data_dossier['file_rib'] = $path_filename_ext;
                }
            }

            $data_post = $_POST;
            $dossier_id = $data_post['fact_dossier_id'];

            $data['dossier_id'] = $data_post['fact_dossier_id'];

            $params = array(
                'columns' => array('c_lot_client.cliLot_id'),
                'clause' => array(
                    'cliLot_etat' => 1,
                    'c_lot_client.dossier_id ' => $data_post['fact_dossier_id']
                ),
                'method' => "result",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_mandat_new' => 'c_mandat_new.cliLot_id =  c_lot_client.cliLot_id',
                    'c_facturation_lot' => 'c_facturation_lot.cliLot_id = c_lot_client.cliLot_id',
                    'c_mode_paiement_facturation' => 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement'
                ),
                'join_orientation' => 'left',
            );

            $data_lot = $this->client_lot->get($params);

            foreach ($data_lot as $key => $value) {
                $param_facturation = array(
                    'clause' => array(
                        'cliLot_id' => $value->cliLot_id,
                        'annee' => $anneeMoinsUn,
                    ),
                    'method' => 'row'
                );
                $check_facturationDossier = $this->factu_dossier->get($param_facturation);

                if (!empty($check_facturationDossier)) {
                    $clause_fac = array('cliLot_id' => $value->cliLot_id, 'annee' => $anneeMoinsUn);
                    $data = array(
                        'cliLot_id' => $value->cliLot_id,
                        'mode_paiement' => $data_post['mode_paiement'],
                        'montant_ht_facture' => $data_post["montant_ht_facture_$value->cliLot_id"],
                        'montant_ht_figurant' => $data_post["montant_ht_figurant_$value->cliLot_id"],
                        'dossier_id' => $dossier_id,
                    );
                    $this->factu_dossier->save_data($data, $clause_fac);
                } else {
                    $data = array(
                        'cliLot_id' => $value->cliLot_id,
                        'mode_paiement' => $data_post['mode_paiement'],
                        'montant_ht_facture' => $data_post["montant_ht_facture_$value->cliLot_id"],
                        'montant_ht_figurant' => $data_post["montant_ht_figurant_$value->cliLot_id"],
                        'dossier_id' => $dossier_id,
                        'annee' => $anneeMoinsUn
                    );
                    $this->factu_dossier->save_data($data);
                }
            }

            if ($data_dossier['file_rib'] == NULL) {
                unset($data_dossier['file_rib']);
            }
            $clause_dossier = array('dossier_id' => $dossier_id);
            $data_post['iban'] = str_replace(' ', '', $data_post['iban']);
            $data_dossier['iban'] = $data_post['iban'];
            $data_dossier['bic'] = $data_post['bic'];

            $this->dossier->save_data($data_dossier, $clause_dossier);

            $retour = retour(true, "success", $dossier_id, array("message" => "Enregistrement réussie."));
            echo json_encode($retour);
        }
    }

    public function removefilerib()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $data_post = $_POST['data'];
                $param = array('dossier_id' => $data_post['dossier_id']);
                $data_dossier = array(
                    'file_rib' => NULL,
                );
                $request = $this->Dossier_m->save_data($data_dossier, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    function pageRemboursementAvoir()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Facture_m', 'facture_annuelle');
            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2, 4]);
            $data['facture'] = $this->facture_annuelle->getfactureAnnuelleByid($_POST['data']['fact_id']);
            $data['dossier_id'] = $_POST['data']['dossier_id'];
            $this->renderComponant("Clients/clients/dossier/facturation/pageRemboursementAvoir", $data);
        }
    }

    function saveRemboursementAvoir()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montantreglment = floatval(str_replace(",", ".", $data['montant_sur_remboursementsAvoir']));
                $saveReglement = [
                    'mode_reglement' => $data['mode_remboursementsAvoir'],
                    'date_reglement' => $data['dateremboursementsAvoir'],
                    'id_banque' => $data['banque_celavi_remboursementsAvoir'],
                    'dossier_id' => $data['dossier_id_reglmentAvoir'],
                    'montant_total' => $montantreglment,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_remboursementsAvoir'],
                    'reglement_type' => 1,
                ];
                $request = $this->reglement->save_data($saveReglement);
                $data['dossier_id'] = $data['dossier_id_reglmentAvoir'];

                if (!empty($request)) {
                    $this->GererVentilationAvoir($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $data['dossier_id'], $message);
                }
            }
            echo json_encode($retour);
        }
    }

    private function GererVentilationAvoir($data)
    {
        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Reglement_m', 'reglement');
        $montantreglment = round(floatval(str_replace(",", ".", $data['montant_sur_remboursementsAvoir'])), 2);
        $totalVentilation = floatval($this->ventilation->getTotalVentile($data['fact_id_remboursementsAvoir']));
        $totalVentilation = round($totalVentilation, 2);
        $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $data['fact_id_remboursementsAvoir']], 'method' => 'row']);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);

        $montantfacture = round(floatval($facture->fact_montantttc) + ($totalVentilation + $montantreglment), 2);

        $saveVentil = [
            'dossier_id' => $data['dossier_id_reglmentAvoir'],
            'id_facture' => $facture->fact_id,
            'montant' => ($montantfacture > 0) ? round((floatval($facture->fact_montantttc) + $totalVentilation), 2) : round($montantreglment, 2),
            'idc_reglements' => $last_reglement->idc_reglements,
        ];
        $this->ventilation->save_data($saveVentil);

        if ($montantfacture >= 0) {
            $savefacture = ['fact_etatpaiement' => 1];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);

            if ($montantfacture > 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossier_id_reglmentAvoir'],
                    'id_facture' => NULL,
                    'montant' => abs($montantfacture),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }

    function pageRemboursement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Reglement_ventilation_m', 'ventilation');
            $this->load->model('Reglement_m', 'reglement');

            $datapost = $_POST['data'];
            $data['reglementdata']  = $this->reglement->get([
                'clause' => ['idc_reglements' => $datapost['Idreglement']],
                'method' => 'row'
            ]);

            $data['montantTroPerçus'] = floatval($this->ventilation->getTropPerçusReglement($datapost['Idreglement']));

            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2]);
            $data['dossier_id'] = $_POST['data']['dossier_id'];

            $this->renderComponant("Clients/clients/dossier/facturation/page-remboursement", $data);
        }
    }

    function pageRejetPrelevement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Reglement_ventilation_m', 'ventilation');
            $this->load->model('Reglement_m', 'reglement');

            $datapost = $_POST['data'];
            $data['reglementdata']  = $this->reglement->get([
                'clause' => ['idc_reglements' => $datapost['Idreglement']],
                'method' => 'row'
            ]);

            $data['montantTroPerçus'] = floatval($this->ventilation->getTropPerçusReglement($datapost['Idreglement']));
            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2]);
            $data['dossier_id'] = $_POST['data']['dossier_id'];

            $this->renderComponant("Clients/clients/dossier/facturation/page-Rejet_Prelevement", $data);
        }
    }

    function saveRemboursement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);

                $montant_sur_Remboursement = round(floatval(str_replace(",", ".", $data['montant_sur_Remboursement'])), 2);

                $datamontantPercus = round(floatval($data['montanPercus']), 2);

                $montantReglementsDu = round(floatval($this->reglement->getTotalReglementDu($data['idreglementRembourse'])), 2);

                $resteventilReglementsDu = round(floatval($this->reglement->verifierRemboursementTropPerçus($data['idreglementRembourse'])), 2);



                $dataSaveRemboursement = [
                    'mode_reglement' => $data['mode_Remboursement'],
                    'date_reglement' => $data['dateRemboursement'],
                    'id_banque' => $data['banque_celavi_Remboursement'],
                    'dossier_id' => $data['dossierRemboursement'],
                    'montant_total' => -1 * $montant_sur_Remboursement,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_Remboursement'],
                    'reglement_type' => 1,
                    'id_reglementDu' => $data['idreglementRembourse']
                ];
                $request = $this->reglement->save_data($dataSaveRemboursement);


                if (!empty($request)) {
                    $datamontantPercus == 0 ? $this->RemboursementReglementSansMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu) : $this->RemboursementReglementAvecMontantPerçus($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $data['dossierRemboursement'], $message);
                }
            }
            echo json_encode($retour);
        }
    }


    private function RemboursementReglementSansMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu)
    {




        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');
        $this->load->model('Facture_m', 'facture_annuelle');

        $montantRemboursement = round(floatval(str_replace(",", ".", $data['montant_sur_Remboursement'])), 2);

        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture !=' => null],
        ]);


        $reglementData  = $this->reglement->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse']],
            'method' => 'row'
        ]);




        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);

        $montantReglement = round(floatval($reglementData->montant_total) + ($montantReglementsDu - $montantRemboursement), 2);

        // var_dump($montantReglement);
        // die();

        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRemboursement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? round(0, 2) : -1 * $montantRemboursement,
                'idc_reglements' => $last_reglement->idc_reglements,
            ];


            $this->ventilation->save_data($saveVentil);
            $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $value->id_facture], 'method' => 'row']);
            $savefacture = ['fact_etatpaiement' => 0];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);
        }



        if ($montantReglement <= 0) {
            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRemboursement'],
                    'id_facture' => NULL,
                    'montant' => ($resteventilReglementsDu < 0) ? -1 * $montantRemboursement : $montantReglement,
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }


    private function RemboursementReglementAvecMontantPerçus($data)
    {

        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');
        $this->load->model('Facture_m', 'facture_annuelle');
        $montantRemboursement = round(floatval(str_replace(",", ".", $data['montant_sur_Remboursement'])), 2);
        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture !=' => null],
        ]);


        $venitlationSupprimer  = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture' => null],
            'method' => 'row'
        ]);



        $montantperçus = round(floatval($this->ventilation->getTropPerçusReglement($data['idreglementRembourse'])), 2);

        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);

        $montantReglement = round(floatval($montantperçus -  $montantRemboursement), 2);


        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRemboursement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? -1 * $montantReglement : round(0, 2),
                'idc_reglements' => $last_reglement->idc_reglements,
            ];
            $this->ventilation->save_data($saveVentil);

            if ($montantReglement < 0) {
                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $value->id_facture], 'method' => 'row']);
                $savefacture = ['fact_etatpaiement' => 0];
                $clause = ['fact_id' => $facture->fact_id];
                $this->facture_annuelle->save_data($savefacture, $clause);
            }
        }


        if ($montantReglement <= 0) {

            $clause_delete = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->delete_data($clause_delete);

            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRemboursement'],
                    'id_facture' => NULL,
                    'montant' => $montantReglement,
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        } else {

            $date_update = array('montant' => $montantReglement);
            $clauseventil = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->save_data($date_update, $clauseventil);
        }
    }


    function saveRejet_Prelevement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montant_sur_Rejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_Prelevement'])), 2);
                $datamontantPercus = round(floatval($data['montanPercusRejet']), 2);


                $montantReglementsDu = round(floatval($this->reglement->getTotalReglementDu($data['idreglementRejetPrelevement'])), 2);

                $resteventilReglementsDu = round(floatval($this->reglement->verifierRemboursementTropPerçus($data['idreglementRejetPrelevement'])), 2);

                $infofacture = $this->reglement->getFactureReglement($data['idreglementRejetPrelevement']);
                $message = "Enregistrement non effectué.";
                $classe = "danger";
                $retour = retour(true, "danger", $data['dossierRejet_Prelevement'], $message);

                $checkRejet = $this->reglement->checkRejetfacture($data['idreglementRejetPrelevement']);

                if (empty($checkRejet)) {
                    if (!empty($infofacture)) {
                        $dataSaveRemboursement = [
                            'mode_reglement' => 2,
                            'date_reglement' => $data['dateRejet_Prelevement'],
                            'id_banque' => $data['banque_celavi_Rejet_Prelevement'],
                            'dossier_id' => $data['dossierRejet_Prelevement'],
                            'montant_total' => -1 * $montant_sur_Rejet_Prelevement,
                            'date_action' => date('Y-m-d H:i:s'),
                            'util_id' => $_SESSION['session_utilisateur']['util_id'],
                            'libelle_reglement' => $data['libelle_Rejet_Prelevement'],
                            'reglement_type' => 2,
                            'id_reglementDu' => $data['idreglementRejetPrelevement']
                        ];
                        $request = $this->reglement->save_data($dataSaveRemboursement);


                        if (!empty($request)) {
                            $datamontantPercus == 0 ? $this->RejetPrelevementSnasMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu) : $this->RejetePrelevementAvecMontantPerçus($data);

                            if ($data['activerFacture'] == 1) {
                                $pdf = $this->facturerDossier($data['dossierRejet_Prelevement'], $infofacture);
                            }

                            $message = "Enregistrement réussie avec succès.";
                            $classe = "success";
                            $retour = retour(true, "success", $data['dossierRejet_Prelevement'], $message);
                        }
                    }
                }
            }
            echo json_encode($retour);
        }
    }




    private function RejetPrelevementSnasMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu)
    {


        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');
        $this->load->model('Facture_m', 'facture_annuelle');

        $montant_sur_Rejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_Prelevement'])), 2);

        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRejetPrelevement'], 'id_facture !=' => null],
        ]);


        $reglementData  = $this->reglement->get([
            'clause' => ['idc_reglements' => $data['idreglementRejetPrelevement']],
            'method' => 'row'
        ]);




        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);
        $montantReglement = round(floatval($reglementData->montant_total) + ($montantReglementsDu - $montant_sur_Rejet_Prelevement), 2);


        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRejet_Prelevement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? round(0, 2) : -1 * $montant_sur_Rejet_Prelevement,
                'idc_reglements' => $last_reglement->idc_reglements,
            ];


            $this->ventilation->save_data($saveVentil);

            $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $value->id_facture], 'method' => 'row']);
            $savefacture = ['fact_etatpaiement' => 0];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);
        }



        if ($montantReglement <= 0) {
            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRejet_Prelevement'],
                    'id_facture' => NULL,
                    'montant' => ($resteventilReglementsDu < 0) ? -1 * $montant_sur_Rejet_Prelevement : $montantReglement,
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }


    private function RejetePrelevementAvecMontantPerçus($data)
    {

        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');
        $this->load->model('Facture_m', 'facture_annuelle');

        $montant_sur_Rejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Remboursement'])), 2);

        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRejetPrelevement'], 'id_facture !=' => null],
        ]);


        $venitlationSupprimer  = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRejetPrelevement'], 'id_facture' => null],
            'method' => 'row'
        ]);



        $montantperçus = round(floatval($this->ventilation->getTropPerçusReglement($data['idreglementRejetPrelevement'])), 2);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);


        $montantReglement = round(floatval($montantperçus -  $montant_sur_Rejet_Prelevement), 2);


        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRemboursement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? round(floatval(abs($montantReglement)), 2) : round(0, 2),
                'idc_reglements' => $last_reglement->idc_reglements,
            ];
            $this->ventilation->save_data($saveVentil);

            if ($montantReglement < 0) {
                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $value->id_facture], 'method' => 'row']);
                $savefacture = ['fact_etatpaiement' => 0];
                $clause = ['fact_id' => $facture->fact_id];
                $this->facture_annuelle->save_data($savefacture, $clause);
            }
        }


        if ($montantReglement <= 0) {

            $clause_delete = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->delete_data($clause_delete);

            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRemboursement'],
                    'id_facture' => NULL,
                    'montant' => $montantReglement,
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        } else {

            $date_update = array('montant' => $montantReglement);
            $clauseventil = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->save_data($date_update, $clauseventil);
        }
    }


    function facturerDossier($dossier_id, $facture)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('DocumentFacturation_m', 'doc_facturation');
            $this->load->model('Article_m', 'article');
            $this->load->model('Facture_m', 'facture_annuelle');
            $this->load->model('FactureArticle_m', 'facture_article');
            $this->load->model('MandatSepa_m', 'mandat_sepa');
            $this->load->helper("exportation");


            $data['annee'] = $anneeChoisi = date('Y');
            $precedent = $anneeChoisi - 1;

            $data['article'] = $article = $this->article->get(['clause' => ['art_id' => 2], 'method' => 'row']);

            $data['facture'] = $facture;
            $data['dossier'] = $this->facture_annuelle->get(
                array(
                    'clause' => array('c_facture.dossier_id' => $dossier_id, 'c_facture.fact_annee' => $anneeChoisi),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facture.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['sepa'] = $this->mandat_sepa->get(
                [
                    'clause' => ['dossier_id' => $data['dossier']->dossier_id],
                    'order_by_columns' => "sepa_id DESC",
                    'method' => 'row'
                ]
            );



            $data['lot_principale'] = $lotprincipal = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $dossier_id, 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                    'method' => 'row'
                )
            );


            $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

            if (!empty($lastfacturaton)) {
                $numero = explode("-", $lastfacturaton->fact_num);
                $num = intval($numero[1]) + 1;
                $data['numero_facture'] = $anneeChoisi . '-' . str_pad($num, 5, '0', STR_PAD_LEFT);
            } else {
                $data['numero_facture'] = "$anneeChoisi-00001";
            }

            $data['rib'] = $this->getRibCelavi();
            $data['date_fature'] = date('Y-m-d');

            $data_save = [
                'fact_annee' => $anneeChoisi,
                'fact_num' => $data['numero_facture'],
                'fact_date' => $data['date_fature'],
                // 'article_id' => $data['article']->art_id,
                'dossier_id' => $dossier_id,
                // 'cliLot_id' => implode(',', $datapost['tab_lot']),
            ];

            $totalht = $article->art_tarif;


            $data_save['fact_modepaiement'] = $facture->fact_modepaiement;

            // Arrondir le résultat à 2 décimales si nécessaire
            $totalht = round($totalht, 2);
            $data_save['fact_montantht'] = $totalht;
            $data_save['fact_montanttva'] = $tva = round(($totalht * 20) / 100, 2);
            $data_save['fact_montantttc'] = round(($totalht + $tva), 2);


            $request = $this->facture_annuelle->save_data($data_save);

            if ($request) {
                $id_fact = $data['factureCree'] = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);
                $dataSaveFactureArticle = [];

                $dataSaveFactureArticle['fact_id'] = $id_fact->fact_id;
                $dataSaveFactureArticle['art_id'] =  $data['article']->art_id;
                $dataSaveFactureArticle['lot_id'] = empty($lotprincipal) ? null : $lotprincipal->cliLot_id;
                $dataSaveFactureArticle['art_libellefr'] = $data['article']->art_libelle_fr;
                $dataSaveFactureArticle['art_libelleen'] =  $data['article']->art_libelle_en;
                $dataSaveFactureArticle['faa_puht'] = $totalht;
                $dataSaveFactureArticle['faa_prixht'] = $totalht;
                $dataSaveFactureArticle['faa_tauxtva'] = $tva;
                $dataSaveFactureArticle['faa_prixttc'] = $totalht + $tva;
                $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);


                $file_name = 'facturation_annuelle_' .  $data['numero_facture'];
                $view = 'Clients/clients/dossier/facturation/export';

                $data['lotarticle'] =  $this->facture_article->get(
                    array(
                        'clause' => array(
                            'c_facture_articles.fact_id' => $id_fact->fact_id
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_articles.lot_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                        ),
                    )
                );

                exportfacture_pdf($view, $file_name, $data, $anneeChoisi, $dossier_id, $id_fact->fact_id);

                $this->saveIndepenseRejet($data['lotarticle'], $data['date_fature']);

                $this->createMailcontenu($dossier_id, $anneeChoisi);
            }
        }
    }

    private function createMailcontenu($dossier_id, $anneeFacture)
    {
        $this->load->model('Dossier_m', 'dossier');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Modelmail_m', 'c_param_modelemail');

        $get_client = $this->dossier->get([
            'clause' => ['dossier_id' => $dossier_id],
            'join' => [
                'c_client' => 'c_client.client_id = c_dossier.client_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
            ],
            'method' => 'row'
        ]);

        $facture = $this->facture_annuelle->get(
            [
                'join' => ['c_document_facturation' => 'c_document_facturation.fact_id = c_facture.fact_id'],
                'order_by_columns' => 'c_facture.fact_id DESC',
                'method' => 'row'
            ]
        );


        $model_id = ($get_client->client_nationalite == "Française" || $get_client->client_nationalite == "Belgique") ? 25 : 26;


        $model_mail = $this->c_param_modelemail->get([
            'clause' => ['pmail_id' => $model_id],
            'method' => 'row'
        ]);

        $contenu = $model_mail->pmail_contenu;
        $contenu = str_replace("@prenom", $get_client->client_prenom, $contenu);
        $contenu = str_replace("@nom", $get_client->client_nom, $contenu);
        $contenu = str_replace("@annee", $anneeFacture, $contenu);
        $contenu = str_replace("@chargee_de_clientele", $get_client->util_prenom . ' ' . $get_client->util_nom, $contenu);

        $subject = $model_mail->pmail_objet;
        $subject = str_replace("@facture_annuelle", 'Facture ' . $anneeFacture, $subject);

        // $contenu_pdf = file_get_contents(base_url() . $facture->doc_facturation_path);
        // $base64_pdf = base64_encode($contenu_pdf);
        // $piece_jointe = [
        //     'ContentType' => "application/pdf",
        //     'Filename' => $facture->doc_facturation_nom,
        //     'Base64Content' => $base64_pdf
        // ];

        $message = [
            array(
                'From' => [
                    'Email' => "noreply@celavigestion.fr",
                    'Name' => "CELAVI Gestion"
                ],
                'To' => [
                    [
                        'Email' => trim($get_client->client_email),
                        // 'Email' => trim('ratiazafy123@gmail.com'),
                        'Name' => $get_client->client_prenom . ' ' . $get_client->client_nom
                    ]
                ],
                'Subject' =>  $subject,
                'TextPart' => $contenu,
                'HTMLPart' => nl2br(htmlspecialchars($contenu)),
                // 'Attachments' => [$piece_jointe]
            ),
        ];

        $this->sendEmailViaMailjet($message);
    }

    private function sendEmailViaMailjet($mailjetParams)
    {
        $this->load->library('MailjetService');
        $mailjet = new MailjetService();
        $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
        $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
        return $response;
    }
}
