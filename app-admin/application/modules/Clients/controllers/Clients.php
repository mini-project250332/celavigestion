<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Clients extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Clients";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/app.js",
            "js/clients/clients.js",
            "js/historique/historique.js",
            "js/bail/bail.js",
            "js/bail/loyer-reference.js",
            "js/loyer/loyer.js",
            "js/acte/acte.js",
            "js/immo/immo.js",
            "js/dossiers/dossiers.js",
            "js/charges/charges.js",
            "js/fiscal/fiscal.js",
            "js/mail/mail.js",
            "js/saisietemps/saisietemps.js",
            "js/pno/pno_client.js",
            "js/reporting/reporting.js",
            "js/sepa/sepa.js",
            "js/cloture_annuelle/cloture_annuelle.js",
            "js/copropriete/copropriete.js",
            "js/experts_comptables/experts_comptables.js"
        );
        $this->_css_personnaliser = array(
            "css/clients/clients.css",
            "css/select2.min.css",
            "css/historique/historique.css",
            "css/loyer/loyer.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "clients";
        $this->_data['sous_module'] = "clients" . DIRECTORY_SEPARATOR . "contenaire-clients";
        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['produits'] = $this->getlisteProduit();
                $data['originecli'] = $this->getOriginClient();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['utilisateur'] = $this->getListeUtilisateur();
                $this->renderComponant("Clients/clients/header-liste", $data);
            }
        }
    }

    public function listeClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $like = array();
                $or_like = array();
                $clause = array('client_etat' => 1);
                $data_post = $_POST['data'];

                $rows_limit = intval($data_post['row_data']);

                if ($data_post['origine_cli_id'] != '') {
                    $clause['c_origine_client.origine_cli_id'] = $data_post['origine_cli_id'];
                }

                if ($data_post['gestionnaire_id'] != '') {
                    $clause['c_gestionnaire.gestionnaire_id'] = $data_post['gestionnaire_id'];
                }

                if ($data_post['text_filtre'] != '') {

                    $like['client_nom'] = $data_post['text_filtre'];
                    $or_like['client_prenom'] = $data_post['text_filtre'];
                    $or_like['client_email'] = $data_post['text_filtre'];
                    $or_like['client_email1'] = $data_post['text_filtre'];
                    $or_like['client_phonemobile'] = $data_post['text_filtre'];
                    $or_like['client_phonefixe'] = $data_post['text_filtre'];
                }

                if ($data_post['filtre_pgm'] != '') {
                    $like['progm_nom'] = $data_post['filtre_pgm'];
                    $or_like['progm_ville'] = $data_post['filtre_pgm'];
                }

                if ($data_post['filtre_dossier'] != '') {
                    $clause['c_dossier.dossier_id'] = $data_post['filtre_dossier'];
                }

                if ($data_post['filtre_siret'] != '') {
                    $like['dossier_siret'] = $data_post['filtre_siret'];
                }

                if ($data_post['filtre_mandat'] != '') {
                    $clause['mandat_cli_id'] = $data_post['filtre_mandat'];
                }

                if ($data_post['cin_client'] == 2) {
                    $clause['etat_cin'] = 0;
                }

                if ($data_post['cin_client'] == 1) {
                    $clause['etat_cin'] = 1;
                }

                if ($data_post['utilisateur'] != 0) {
                    $clause['c_dossier.util_id'] = $data_post['utilisateur'];
                }

                if ($data_post['utilisateur'] == -1) {
                    $clause = array('c_dossier.util_id' => NULL);
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['client_date_creation >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['client_date_creation <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['client_date_creation'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }

                $pagination_data = explode('-', $data_post['pagination_page']);

                $join = array(
                    'c_origine_client' => 'c_origine_client.origine_cli_id = c_client.origine_cli_id',
                    'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_client.paysmonde_id',
                    'c_pays' => 'c_pays.pays_id = c_client.pays_id',
                    'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                    'c_dossier' => 'c_client.client_id  = c_dossier.client_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_gestionnaire.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                    'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                );

                $data = array();
                $this->load->model('Client_m', 'client');
                $params = array(
                    'clause' => $clause,
                    'like' => $like,
                    'or_like' => $or_like,
                    'columns' => array(
                        'c_client.client_id as id',
                        'client_nom as nom',
                        'client_prenom as prenom',
                        'client_entreprise as entreprise',
                        'client_phonemobile as mobile',
                        'client_phonefixe as fixe',
                        'client_email as mail',
                        'client_adresse1 as adresse1',
                        'etat_client',
                        'client_adresse2 as adresse2',
                        'client_adresse3 as adresse3',
                        'client_cp as cp',
                        'client_ville as ville',
                        'client_pays as pays',
                        'client_nationalite as nationalite',
                        'client_commentaire as commentaire',
                        'client_date_creation as date_creation',
                        'cin',
                        'c_origine_client.origine_cli_id as origineCliId',
                        'c_origine_client.origine_cli_libelle as origineCliLib',
                        'c_utilisateur.util_id as utilId',
                        'c_utilisateur.util_nom as utilNom',
                        'c_utilisateur.util_prenom as utilPrenom',
                        'c_paysmonde.paysmonde_id',
                        'paysmonde_indicatif',
                        'c_pays.pays_id',
                        'pays_indicatif',
                        'c_programme.progm_nom',
                        'c_contactgestionnaire.cnctGestionanire_nom',
                        'c_contactgestionnaire.cnctGestionnaire_prenom'
                    ),
                    'join' => $join,
                    'join_orientation' => 'left',
                    'limit' => array('offset' => $pagination_data[1], 'limit' => $pagination_data[0]),
                    'group_by_columns' => "c_client.client_id",
                    'order_by_columns' => "c_client.client_id ASC",
                );

                $data["listClients"] = $this->client->get($params);

                $result_client_lot_unique = $this->get_client_lot_unique();
                $client_lot_unique = array();

                if (!empty($result_client_lot_unique)) {
                    foreach ($result_client_lot_unique as $value) {
                        array_push($client_lot_unique, $value->client_id);
                    }
                }
                $data['client_lot_unique'] = $client_lot_unique;

                $data["countAllResult"] = $this->countAllData('Client_m', array('client_etat' => 1));

                if (
                    $data_post['origine_cli_id'] != '' ||
                    $data_post['gestionnaire_id'] != '' ||
                    $data_post['text_filtre'] != '' ||
                    $data_post['filtre_pgm'] != '' ||
                    $data_post['filtre_dossier'] != '' ||
                    $data_post['filtre_siret'] != '' ||
                    $data_post['filtre_mandat'] != '' ||
                    $data_post['cin_client'] == 2 ||
                    $data_post['cin_client'] == 1 ||
                    $data_post['utilisateur'] != 0
                ) {
                    $data["countAllResult"] = $this->countUser($clause, $like, $or_like, $join);
                }

                $nb_total = $data["countAllResult"];

                $data['pagination_page'] = $data_post['pagination_page'];
                $data['rows_limit'] = $rows_limit;

                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];

                $this->renderComponant("Clients/clients/liste-clients", $data);
            }
        }
    }

    public function countUser($clause, $like, $or_like, $join)
    {
        $this->load->model('Client_m', 'client');
        $request = $this->client->get(
            array(
                'clause' => $clause,
                'like' => $like,
                'or_like' => $or_like,
                'join' => $join,
                'join_orientation' => 'left',
                'group_by_columns' => "c_client.client_id",
                'order_by_columns' => "c_client.client_id ASC",
            )
        );

        return !empty($request) ? count($request) : 0;
    }

    public function get_client_lot_unique()
    {
        $this->load->model('Client_m', 'client');
        $this->load->model('ClientLot_m', 'lot');

        $table_unique = array();

        $params = array(
            'clause' => array('c_lot_client.typelot_id' => 7),
            'join' => array(
                'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
            ),
            'group_by_columns' => 'c_dossier.dossier_id',
        );

        $resultat_lot = $this->lot->get($params);

        if (!empty($resultat_lot)) {
            foreach ($resultat_lot as $key => $value) {
                $param_row = array(
                    'clause' => array('c_lot_client.dossier_id' => $value->dossier_id),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    ),
                );
                $check_list_row = $this->lot->get($param_row);

                if (count($check_list_row) < 2) {
                    foreach ($check_list_row as $val) {
                        array_push($table_unique, $val);
                    }
                }
            }
        }
        return $table_unique;
    }

    public function ficheClient()
    {
        $this->load->model('Client_m', 'client');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('client_id' => $data_post['client_id']),
                    'method' => "row",
                    'columns' => array('client_id', 'client_nom', 'client_prenom', 'client_entreprise', 'cin'),
                    'join' => array(
                        'c_origine_client' => 'c_origine_client.origine_cli_id = c_client.origine_cli_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_client.util_id'
                    ),
                );
                $data['listeUser'] = $this->getListUser();
                $data['listeOrigineClient'] = $this->getOriginClient();
                $data['client'] = $this->client->get($params);
                $this->renderComponant("Clients/clients/fiche-client", $data);
            }
        } else {
            $this->_data['client_id'] = $_GET['client'];

            $this->_data['menu_principale'] = "clients";
            $this->_data['sous_module'] = "clients" . DIRECTORY_SEPARATOR . "contenaire-clients";
            $this->render('contenaire');
        }
    }

    function pageficheClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m', 'client');
                $this->load->model('AccessProprietaire_m');
                $this->load->model('AccessProprietaireClient_m');
                $this->load->model('MotDePasseFruit_m');
                $this->load->model('UtilisateurClient_m');
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('client_id' => $data_post['client_id']),
                    'method' => "row",
                    'join' => array(
                        'c_origine_client' => 'c_origine_client.origine_cli_id = c_client.origine_cli_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_client.util_id',
                        'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_client.paysmonde_id',
                        'c_pays' => 'c_pays.pays_id = c_client.pays_id'
                    ),
                );

                $data_cli = $this->client->get($params);

                $data['client'] = $data_cli;

                $data['listeUser'] = $this->getListUser();
                $data['listeOrigineClient'] = $this->getOriginClient();
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();
                $data['nationalite'] = $this->getNationalite();

                // ****** Information de connexion location meublée******
                $paramsAccess = array(
                    'clause' => array('client_id ' => $data_cli->client_id),
                    'join' => array(
                        'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire.mtpfr_id'
                    ),
                    'method' => 'row'
                );
                $infoCnx = $this->AccessProprietaire_m->get($paramsAccess);

                if (!isset($infoCnx)) {
                    $newPasswordData = $this->MotDePasseFruit_m->getRandomPasswordFuit();
                    $separator = (isset($data_cli->client_prenom) && $data_cli->client_prenom != "") ? '.' : '';
                    $login = $data_cli->client_prenom . $separator . $data_cli->client_nom;

                    $count = $this->AccessProprietaire_m->isExist($login);
                    if ($count > 0)
                        $login .= $count;

                    $data_access["client_id"] = $data_cli->client_id;
                    $data_access["mtpfr_id"] = $newPasswordData->mtpfr_id;
                    $data_access["accp_login"] = $login;
                    $this->AccessProprietaire_m->save_data($data_access);
                    $infoCnx = $this->AccessProprietaire_m->get($paramsAccess);
                }
                $data['accessClient'] = $infoCnx;

                // ****** Information de connexion Intranet******
                $paramsAccessClient = array(
                    'clause' => array('client_id ' => $data_cli->client_id),
                    'join' => array(
                        'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire_client.mtpfr_id'
                    ),
                    'method' => 'row'
                );
                $infoCnxIntranet = $this->AccessProprietaireClient_m->get($paramsAccessClient);

                if (!isset($infoCnxIntranet)) {
                    $newPasswordData = $this->MotDePasseFruit_m->getRandomPasswordFuit();
                    $separator = (isset($data_cli->client_prenom) && $data_cli->client_prenom != "") ? '' : '';
                    $newString = $data_cli->client_nom . $separator . $data_cli->client_prenom;
                    $login = str_replace('&', '.', $newString);
                    $login = str_replace(' ', '', $login);

                    $count = $this->AccessProprietaireClient_m->isExist($login);
                    if ($count > 0)
                        $login .= $count;

                    $data_access["client_id"] = $data_cli->client_id;
                    $data_access["mtpfr_id"] = $newPasswordData->mtpfr_id;
                    $data_access["accp_login"] = $login;
                    $this->AccessProprietaireClient_m->save_data($data_access);
                    $infoCnxIntranet = $this->AccessProprietaireClient_m->get($paramsAccessClient);
                }

                $data['accessClientIntranet'] = $infoCnxIntranet;

                // Information mot de passe intranet

                $paramsMdpClient = array(
                    'clause' => array('c_client.client_id ' => $data_cli->client_id),
                    'join' => array(
                        'c_client' => 'c_utilisateur_client.client_id = c_client.client_id'
                    ),
                    'method' => 'row'
                );

                $data['mdp'] = $this->UtilisateurClient_m->get($paramsMdpClient);

                // *******************

                if ($data_post['page'] == "form") {
                    $this->renderComponant("Clients/clients/identification/form-update-identification", $data);
                } else {
                    $this->renderComponant("Clients/clients/identification/fiche-identification", $data);
                }
            }
        }
    }

    function getPays($pays_id)
    {

        $this->load->model('Pays_m');
        $params = array(
            'columns' => array('pays_id', 'pays_libelle'),
            'method' => 'row'
        );
        $request = $this->Pays_m->get($params);
        return $request;
    }

    public function updateInfoClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m', 'client');
                $this->load->model('AccessProprietaireClient_m');
                $data = $_POST;

                /*
                    $cin = NULL;
                    $cin2 = NULL;

                    if (!empty($_FILES['file'])) {
                        $target_dir = "../documents/cin/" . $data['client_id'] . '/';

                        $file = $_FILES['file']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file']['tmp_name'];
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $this->makeDirPath($target_dir);

                        $counter = 1;
                        while (file_exists($path_filename_ext)) {
                            $filename = $filename . '_' . $counter;
                            $path_filename_ext = $target_dir . $filename . "." . $ext;
                            $counter++;
                        }
                        if (!file_exists($path_filename_ext)) {
                            move_uploaded_file($temp_name, $path_filename_ext);
                        }

                        $data['etat_cin'] = 1;
                        $cin = $path_filename_ext;
                    }


                    if (!empty($_FILES['file2'])) {
                        $target_dir = "../documents/cin/" . $data['client_id'] . '/';

                        $file = $_FILES['file2']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file2']['tmp_name'];

                        // Créer le chemin complet avec le nom du fichier
                        $path_filename_ext = $target_dir . $filename . "." . $ext;

                        $this->makeDirPath($target_dir);
                        $counter = 1;
                        while (file_exists($path_filename_ext)) {
                            $filename = $filename . '_' . $counter;
                            $path_filename_ext = $target_dir . $filename . "." . $ext;
                            $counter++;
                        }
                        if (!file_exists($path_filename_ext)) {
                            move_uploaded_file($temp_name, $path_filename_ext);
                        }
                        $cin2 = $path_filename_ext;
                    }
                */

                $data = array(
                    'client_nom' => $data['client_nom'],
                    'client_prenom' => $data['client_prenom'],
                    'client_entreprise' => $data['client_entreprise'],
                    'client_nationalite' => $data['client_nationalite'],
                    'client_residence_fiscale' => $data['client_residence_fiscale'],
                    'client_situation_matrimoniale' => $data['client_situation_matrimoniale'],
                    // 'client_date_fin_validite_cni' => $data['client_date_fin_validite_cni'],
                    'client_email' => $data['client_email'],
                    'client_email1' => $data['client_email1'],
                    'paysmonde_id' => $data['paysmonde_id'],
                    'client_phonemobile' => $data['client_phonemobile'],
                    'pays_id' => $data['pays_id'],
                    'client_phonefixe' => $data['client_phonefixe'],
                    'client_adresse1' => $data['client_adresse1'],
                    'client_adresse2' => $data['client_adresse2'],
                    'client_adresse3' => $data['client_adresse3'],
                    'client_cp' => $data['client_cp'],
                    'client_ville' => $data['client_ville'],
                    'client_pays' => $data['client_pays'],
                    'origine_cli_id' => $data['origine_cli_id'],
                    'client_date_creation' => $data['client_date_creation'],
                    'util_id' => $data['util_id'],
                    'client_commentaire' => $data['client_commentaire'],
                    'accp_login' => $data['accp_login'],
                    'client_id' => $data['client_id']
                );

                /*
                    if ($cin != NULL) {
                        $data['cin'] = $cin;
                    }
                    if ($cin2 != NULL) {
                        $data['cin2'] = $cin2;
                    }
                */

                $data_login = array(
                    'accp_login' => $data['accp_login']
                );

                unset($data['accp_login']);

                $retour = retour(false, "error", 0, array("message" => "Erreur de la modification"));
                $client_id = $data['client_id'];
                unset($data['client_id']);
                unset($data['file']);
                unset($data['file2']);
                $clause = array("client_id" => $client_id);
                $request = $this->client->save_data($data, $clause);
                $request_login = $this->AccessProprietaireClient_m->save_data($data_login, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "La modification a été réussi avec succès."));
                }
                echo json_encode($retour);
            }
        }
    }

    public function formNewClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['listeUser'] = $this->getListUser();
                $data['listeOrigineClient'] = $this->getOriginClient();
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();
                $data['nationalite'] = $this->getNationalite();
                $this->renderComponant("Clients/clients/form-client", $data);
            }
        }
    }

    function clientAdd()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Client_m', 'client');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $data['client_etat'] = 1;
                    $data['client_date_creation'] = date_sql($data['client_date_creation'], '-', 'jj/MM/aaaa');
                    $request = $this->client->save_data($data);

                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => "L'information du nouvel client a été enregistrée avec succès"));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    function removeClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Client_m', 'client');
                $this->load->model('Prospect_m');

                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('client_id' => $data['client_id']),
                    'columns' => array('client_id', 'client_nom', 'client_prenom', 'client_entreprise'),
                    'method' => "row",
                );
                $request_get = $this->client->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['client_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le client " . ((trim($request_get->client_entreprise) != "") ? '[ ' . $request_get->client_entreprise . ' ] ' : '') . $request_get->client_nom . " " . $request_get->client_prenom . " ? ",
                            'btnConfirm' => "Supprimer le client",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('client_id' => $data['client_id']);
                    //$data_update = array('client_etat' => 0);
                    $request = $this->client->delete_data($clause);

                    $data_update_prospect = array('prospect_after_id' => null);
                    $clause_prospect = array('prospect_after_id' => $data['client_id']);
                    $this->Prospect_m->save_data($data_update_prospect, $clause_prospect);

                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['client_id']),
                            'message' => "Le client " . ((trim($request_get->client_entreprise) != "") ? '[ ' . $request_get->client_entreprise . ' ] ' : '') . $request_get->client_nom . " " . $request_get->client_prenom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    /******Dossier******/

    function getListDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array(
                        'dossier_id',
                        'dossier_d_creation',
                        'dossier_d_modification',
                        'dossier_etat',
                        'dossier_rcs',
                        'dossier_siret',
                        'dossier_ape',
                        'dossier_tva_intracommunautaire',
                        'dossier_adresse1',
                        'dossier_adresse2',
                        'dossier_adresse3',
                        'dossier_cp',
                        'dossier_ville',
                        'dossier_pays',
                        'dossier_type_adresse',
                        'regime',
                        'cloture_jour',
                        'cloture_mois',
                        'cloture_jour_fin',
                        'cloture_mois_fin',
                        'sie_libelle',
                        'c_sie.sie_id',
                        'client_id'
                    ),
                    'join' => array(
                        'c_sie' => 'c_sie.sie_id = c_dossier.sie_id'
                    )
                );
                $request = $this->dossier->get($params);

                $data["client_id"] = $data_post['client_id'];

                $data["Nombre_client"] = count($request);

                $data["countAllResult"] = $this->countAllData('Dossier_m', array('dossier_etat' => 1));

                $liste_dossier = array();
                foreach ($request as $key => $value) {
                    if ($value->client_id != "") {
                        $array_client = explode(',', $value->client_id);
                        if (in_array($data["client_id"], $array_client)) {
                            $client = array();
                            foreach ($array_client as $key_cli => $value_client) {
                                $client_val = $this->getClient($value_client);
                                if (!empty($client_val)) {
                                    array_push($client, $client_val);
                                } else {
                                    unset($array_client[$key_cli]);
                                }
                            }
                            $request[$key]->client = implode(', ', $client);
                            $request[$key]->client_id = implode(',', $array_client);
                            $liste_dossier[] = $request[$key];
                        }
                    }
                }

                $result_dossier_lot_unique = $this->get_client_lot_unique();
                $dossier_lot_unique = array();

                if (!empty($result_dossier_lot_unique)) {
                    foreach ($result_dossier_lot_unique as $value) {
                        array_push($dossier_lot_unique, $value->dossier_id);
                    }
                }
                $data['dossier_lot_unique'] = $dossier_lot_unique;

                $data["listDossier"] = $liste_dossier;

                $this->renderComponant("Clients/clients/dossier/list-dossier", $data);
            }
        }
    }

    public function getFicheDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m');
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('Client_m', 'client');

                $data_post = $_POST["data"];
                $data['client_id'] = $data_post['client_id'];

                $params = array(
                    'clause' => array('dossier_id' => $data_post['dossier_id']),
                    'method' => "row",
                    'join' => array(
                        'c_forme_juridique' => 'c_forme_juridique.c_forme_juridique_id = c_dossier.c_forme_juridique_id',
                        'c_sie' => 'c_sie.sie_id = c_dossier.sie_id',

                    )
                );
                $request = $this->dossier->get($params);

                $liste_dossier = null;

                if (!empty($request->client_id)) {
                    $array_client = explode(',', $request->client_id);
                    if (in_array($data["client_id"], $array_client)) {
                        $client = array_map(function ($value_client) {
                            $client_data = $this->getClient($value_client);
                            $tableclient_data = $this->Client_m->get(array(
                                'clause' => array('client_id' => $value_client),
                                'method' => 'row',
                                'columns' => array('client_id', 'client_nom', 'client_prenom'),
                            ));
                            return (!empty($client_data)) ? array('client_data' => $client_data, 'tableclient_data' => $tableclient_data) : null;
                        }, $array_client);

                        $client = array_filter($client);

                        $request->client = implode(' , ', array_column($client, 'client_data'));
                        $request->tableclient = array_column($client, 'tableclient_data');
                        $liste_dossier = $request;
                    }
                }
                $data['dossier'] = $liste_dossier;
                $data['client'] = $this->Client_m->get(
                    array(
                        'clause' => array('client_id' => $data_post["client_id"]),
                        'method' => 'row',
                    )
                );
                $this->renderComponant("Clients/clients/dossier/fiche-dossiers", $data);
            }
        }
    }

    function getFormDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('Client_m', 'client');
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Utilisateur_m');

                $data['client_id'] = $data_post['client_id'];
                if ($data_post['action'] == "edit" || $data_post['action'] == "fiche") {
                    $this->load->model('Dossier_m', 'dossier');
                    $data['dossier'] = $this->dossier->get_data_dossier($data_post['dossier_id']);

                    $clientTabid = explode(',', $data['dossier']->client_id);

                    $data['rofData'] = array_map(function ($client_id) {
                        $clientIndivision = $this->client->get(
                            array(
                                'clause' => array('client_id' => $client_id),
                                'method' => 'row'
                            )
                        );
                        return $clientIndivision;
                    }, $clientTabid);
                }

                $request_id = array(
                    'columns' => array('dossier_id'),
                    'order_by_columns' => "dossier_id DESC",
                    'limit' => 1,
                    'method' => 'row'
                );
                $data['doss_id'] = $this->dossier->get($request_id);
                $data['action'] = $data_post['action'];

                $data['client'] = $this->client->get(
                    array(
                        'clause' => array('client_etat' => 1),
                        'columns' => array("client_nom", "client_prenom", "client_id", "client_entreprise", "client_rof"),
                    )
                );

                $data['lot'] = $this->lot->get(
                    array(
                        'clause' => array(
                            'cli_id' => $data['client_id'],
                            'cliLot_principale' => 1,
                            'dossier_id' => $data_post["dossier_id"]
                        ),
                        'join' => array('c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'),
                        'method' => 'row',
                    )
                );

                $data['sie'] = $this->getSie();
                $data['formejuridique'] = $this->getFormeJuridique();
                $data['cgp'] = $this->getCgp();

                $paramsUser = array(
                    'columns' => array('util_id', 'util_nom', 'util_prenom')
                );

                $data['utilisateurs'] = $this->Utilisateur_m->get($paramsUser);

                $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
                $params = array(
                    'clause' => array('dossier_id' => $data_post["dossier_id"]),
                    'join' => array(
                        'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                        'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
                    ),
                    'columns' => array(
                        'expert_MD_id',
                        'c_expertcomptable_assiger_dossier.assiger_ECD_id',
                        'date_assigner',
                        'expert_id',
                        'util_nom as expert_nom',
                        'util_prenom as expert_prenom',
                        'date_creation as date_creation_mission',
                        'date_debut_mission',
                        'date_resiliation_mission',
                        'fichier_mission',
                        'fichier_resiliation',
                        'dossier_id',
                    ),
                    'order_by_columns' => "date_debut_mission ASC",
                );

                $data['data_missionsEC'] = $this->mission->get($params);

                if ($data['action'] == "fiche") {
                    $this->renderComponant("Clients/clients/dossier/fiche-dossier", $data);
                } elseif ($data['action'] == "add") {
                    $this->renderComponant("Clients/clients/dossier/form-dossier", $data);
                } else {
                    $this->renderComponant("Clients/clients/dossier/identification_dossier/formUpdate-dossier", $data);
                }
            }
        }
    }

    public function getSie()
    {

        $this->load->model('Sie_m');
        $params = array(
            'columns' => array('sie_id', 'sie_libelle'),
            'order_by_columns' => "sie_libelle ASC",
        );

        $request = $this->Sie_m->get($params);
        return $request;
    }

    public function getFormeJuridique()
    {

        $this->load->model('FormeJuridique_m');
        $params = array(
            'columns' => array('c_forme_juridique_id', 'c_forme_juridique_libelle'),
            'order_by_columns' => "c_forme_juridique_id ASC",
        );

        $request = $this->FormeJuridique_m->get($params);
        return $request;
    }


    public function getCgp()
    {
        $this->load->model('Cgp_m');
        $params = array(
            'clause' => array('cgp_etat' => 1)
        );

        $request = $this->Cgp_m->get($params);
        return $request;
    }

    public function getInfoCgp()
    {
        $this->load->model('Cgp_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['cgp_id'] = $data_post['cgp_id'];

                $request_cgp_info = $this->Cgp_m->get(
                    array(
                        'clause' => array('cgp_id' => $data_post['cgp_id']),
                        'method' => 'row',
                    )
                );

                $data_retour = $request_cgp_info;

                $retour = retour(true, "success", $data_retour, array("message" => "Info cgp"));

                echo json_encode($retour);
            }
        }
    }

    public function FormCgp()
    {
        $this->load->model('Cgp_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Cgp_m');
                    $data['cgp'] = $this->Cgp_m->get(
                        array('clause' => array("progm_id" => $data_post['cgp_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
            }
            $this->renderComponant("Clients/clients/form-cgp", $data);
        }
    }


    function FormClient()
    {
        $this->load->model('Gestionnaire_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $this->load->model('Client_m', 'client');
                $data['client'] = $this->client->get(
                    array(
                        'clause' => array('client_etat' => 1),
                        'columns' => array("client_nom", "client_prenom", "client_id"),
                    )
                );
                $data['action'] = $data_post['action'];
            }

            $this->renderComponant("Clients/clients/dossier/form-client", $data);
        }
    }


    function cruDossier()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Dossier_m', 'dossier');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data['sie_id'] = 1;
                    $data['dossier_type_adresse'] = "Lot principal";
                    unset($data['action']);
                    unset($data['search_terms']);
                    unset($data['dossier_id']);
                    // $data['client_id'] = implode(',',$data['client_id']);
                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("dossier_id" => $data['dossier_id']);
                        $data_retour['dossier_id'] = $data['dossier_id'];
                        unset($data['dossier_id']);
                        $data['dossier_d_modification'] = Carbon::now()->toDateTimeString();
                    } else {
                        $data['dossier_d_creation'] = Carbon::now()->toDateTimeString();
                    }
                    $request = $this->dossier->save_data($data, $clause);
                    $data_retour = array("client_id" => $data['client_id'], "action" => $action, "dossier_id" => $request);
                    if (!empty($request)) {
                        $retour = retour(
                            true,
                            "success",
                            $data_retour,
                            array(
                                "message" => ($action == "add") ? "Nouveau dossier créé avec succès" : "La modification a été réussi 
                         avec succès."
                            )
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDossier()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('Cgp_m');
                $this->load->model('Client_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("client_id" => $data['client_id'], "action" => $action);
                    unset($data['action']);
                    unset($data['search_terms']);
                    $clause = null;
                    if ($action == "edit") {

                        $clause = array("dossier_id" => $data['dossier_id']);
                        $clause_cgp = array("cgp_id" => $data['cgp_id']);

                        foreach ($data['client_id'] as $key => $value) {
                            $clause_client = array('client_id' => $value);
                            $data_client = array(
                                'client_rof' => isset($data['client_rof__' . $value]) ? $data['client_rof__' . $value] : NULL
                            );

                            $request_client = $this->Client_m->save_data($data_client, $clause_client);
                        }

                        $data['client_id'] = implode(',', $data['client_id']);

                        if ($data['indivision'] == 'Non') {
                            $client_rof = NULL;
                        }
                        $dataDossier = array(
                            'dossier_rcs' => $data['dossier_rcs'],
                            'dossier_siret' => $data['dossier_siret'],
                            'dossier_ape' => $data['dossier_ape'],
                            'dossier_tva_intracommunautaire' => $data['dossier_tva_intracommunautaire'],
                            'dossier_adresse1' => $data['dossier_adresse1'],
                            'dossier_adresse2' => $data['dossier_adresse2'],
                            'dossier_adresse3' => $data['dossier_adresse3'],
                            'dossier_cp' => $data['dossier_cp'],
                            'dossier_ville' => $data['dossier_ville'],
                            'dossier_pays' => $data['dossier_pays'],
                            'regime' => $data['regime'],
                            'cloture_jour' => $data['cloture_jour'],
                            'cloture_mois' => $data['cloture_mois'],
                            'cloture_jour_fin' => $data['cloture_jour_fin'],
                            'cloture_mois_fin' => $data['cloture_mois_fin'],
                            'dossier_type_adresse' => $data['dossier_type_adresse'],
                            'client_id' => $data['client_id'],
                            'sie_id' => $data['sie_id'],
                            'cgp_id' => $data['cgp_id'],
                            'indivision' => $data['indivision'],
                            'dossier_etat' => $data['dossier_etat'],
                            'util_id' => $data['util_id'],
                            'mention_cga' => $data['mention_cga'],
                            'c_forme_juridique_id' => $data['forme_juridique']
                        );

                        $data_cgp = array(
                            'cgp_email' => $data['cgp_email'],
                            'cgp_tel' => $data['cgp_tel'],
                            'cgp_adresse1' => $data['cgp_adresse1'],
                            'cgp_adresse2' => $data['cgp_adresse2'],
                            'cgp_adresse3' => $data['cgp_adresse3'],
                            'cgp_cp' => $data['cgp_cp'],
                            'cgp_ville' => $data['cgp_ville'],
                            'cgp_pays' => $data['cgp_pays']
                        );

                        $data_retour['dossier_id'] = $data['dossier_id'];
                        unset($data['dossier_id']);
                        $data['dossier_d_modification'] = Carbon::now()->toDateTimeString();
                    } else {
                        $data['dossier_d_creation'] = Carbon::now()->toDateTimeString();
                    }

                    $request = $this->dossier->save_data($dataDossier, $clause);
                    $request_cgp = $this->Cgp_m->save_data($data_cgp, $clause_cgp);

                    if (!empty($request)) {
                        $retour = retour(
                            true,
                            "success",
                            $data_retour,
                            array(
                                "message" => ($action == "add") ? "Nouveau dossier créé avec succès" : " Modification réuissie avec succès."
                            )
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    function removeDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Dossier_m', 'dossier');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('dossier_id' => $data['dossier_id']),
                    'columns' => array('dossier_id', 'dossier_num', 'dossier_libelle'),
                    'method' => "row",
                );
                $request_get = $this->dossier->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['dossier_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le dossier " . ((trim($request_get->dossier_num) != "") ? '[ N° ' . $request_get->dossier_num . ' ] ' : '') . $request_get->dossier_libelle,
                            'btnConfirm' => "Supprimer le dossier",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('dossier_id' => $data['dossier_id']);
                    $data_update = array('dossier_etat' => 0);
                    $request = $this->dossier->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['dossier_id']),
                            'message' => "Le dossier " . ((trim($request_get->dossier_num) != "") ? '[ N° ' . $request_get->dossier_num . ' ] ' : '') . $request_get->dossier_libelle . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }



    /**** function all ****/

    function countAllData($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }

    public function getListProgram()
    {

        $this->load->model('Program_m');
        $params = array(
            'clause' => array('progm_etat' => 1),
            'columns' => array('progm_id', 'progm_nom'),
            'order_by_columns' => 'progm_nom ASC'
        );
        $request = $this->Program_m->get($params);
        return $request;
    }

    /*****************Communication****************/

    public function listCommunication()
    {
        $this->load->model('CommunicationClient_m');
        $this->load->model('Client_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array(
                        'comclient_id',
                        'comclient_texte',
                        'comclient_date_creation',
                        'c_type_communication_prospect.type_comprospect_id',
                        'type_comprospect_libelle',
                        'c_utilisateur.util_id',
                        'util_nom',
                        'util_prenom'
                    ),
                    'clause' => array(
                        'client_id' => $data_post['client_id'],
                        'etatcli_com' => 1
                    ),
                    'join' => array(
                        'c_type_communication_prospect' => 'c_type_communication_prospect.type_comprospect_id = c_communication_client.type_comprospect_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_communication_client.util_id'
                    ),
                );

                $data["client_id"] = $data_post['client_id'];
                $data['com'] = $this->CommunicationClient_m->get($params);
                $data['tache'] = $this->liste_tache();
                $data['tachevalide'] = $this->liste_tache_valide();

                $data['client'] = $this->Client_m->get(
                    array('clause' => array("client_id" => $data_post['client_id']), 'method' => "row")
                );

                $data["countAllResult"] = $this->countAllData('CommunicationClient_m', array('etatcli_com' => 1, 'client_id' => $data_post['client_id']));
                $nb_total = $data["countAllResult"];

                $this->renderComponant("Clients/clients/communication/liste-com", $data);
            }
        }
    }

    public function getFormCom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $data['client_id'] = $data_post['client_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('CommunicationClient_m');
                    $params = array(
                        'clause' => array('comclient_id ' => $data_post['comclient_id']),
                        'method' => "row"
                    );
                    $data['com'] = $request = $this->CommunicationClient_m->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['utilisateur'] = $this->getListUser();
                $data['typecom'] = $this->getlisteTypeCommunication();

                $this->renderComponant("Clients/clients/communication/form-com", $data);
            }
        }
    }

    function cruCom()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('CommunicationClient_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("client_id" => $data['client_id'], "action" => $action);
                    $data['etatcli_com'] = 1;
                    unset($data['action']);
                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("comclient_id" => $data['comclient_id']);
                        $data_retour['comclient_id'] = $data['comclient_id'];
                        unset($data['comclient_id']);
                    } else {
                        $data['comclient_date_creation'] = Carbon::now()->toDateTimeString();
                    }

                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                    $request = $this->CommunicationClient_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Message ajouté" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeCom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('CommunicationClient_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('comclient_id' => $data['comclient_id']),
                    'columns' => array('comclient_id'),
                    'method' => "row",
                );
                $request_get = $this->CommunicationClient_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['comclient_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce message ?",
                            'btnConfirm' => "Supprimer le message",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('comclient_id' => $data['comclient_id']);
                    $data_update = array('etatcli_com' => 0);
                    $request = $this->CommunicationClient_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['comclient_id']),
                            'message' => "Message supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /**********tache***********/

    public function getFormTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m');
                $data_post = $_POST['data'];

                $data['client_id'] = $data_post['client_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('Tache_m');
                    $params = array(
                        'clause' => array('tache_id ' => $data_post['tache_id']),
                        'method' => "row"
                    );
                    $data['tache'] = $request = $this->Tache_m->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['utilisateur'] = $this->getListUser();
                $data['client'] = $this->getlisteClient();
                $data['theme'] = $this->getlisteTheme();

                $data['clients'] = $this->Client_m->get(
                    array('clause' => array("client_id" => $data_post['client_id']), 'method' => "row")
                );

                $this->renderComponant("Clients/clients/taches/form-tache", $data);
            }
        }
    }

    public function getlisteTheme()
    {
        $this->load->model('Theme_m');
        $params = array(
            'columns' => array('theme_id', 'theme_libelle')
        );
        $request = $this->Theme_m->get($params);
        return $request;
    }

    public function getlisteClient()
    {
        $this->load->model('Client_m');
        $params = array(
            'clause' => array('client_etat' => 1),
            'columns' => array('client_id', 'client_prenom', 'client_nom')
        );
        $request = $this->Client_m->get($params);
        return $request;
    }

    public function liste_tache()
    {

        $this->load->model('Tache_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('tache_id', 'tache_nom', 'tache_date_echeance', 'tache_createur', 'tache_date_creation', 'participant', 'tache_prioritaire', 'tache_date_validationn'),
                    'clause' => array(
                        'client_id' => $data_post['client_id'],
                        'tache_etat' => 1,
                        'tache_valide' => 0
                    ),
                    'order_by_columns' => "tache_date_echeance",
                    // 'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_tacheclient.tachecli_createur')
                );
                $request = $this->Tache_m->get($params);
                foreach ($request as $key => $value) {
                    if ($value->participant != "") {
                        $array_util = explode(',', $value->participant);
                        $utilisateur = array();
                        foreach ($array_util as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->utilisateur = implode(' , ', $utilisateur);
                    }
                }

                return $request;
            }
        }
    }

    public function liste_tache_valide()
    {

        $this->load->model('Tache_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('tache_id', 'tache_nom', 'tache_date_echeance', 'tache_createur', 'tache_date_creation', 'participant', 'tache_prioritaire', 'tache_date_validationn'),
                    'clause' => array(
                        'client_id' => $data_post['client_id'],
                        'tache_etat' => 1,
                        'tache_valide' => 1
                    ),
                    'order_by_columns' => "tache_date_echeance",
                    // 'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_tacheclient.tachecli_createur')
                );
                $request = $this->Tache_m->get($params);
                foreach ($request as $key => $value) {
                    if ($value->participant != "") {
                        $array_util = explode(',', $value->participant);
                        $utilisateur = array();
                        foreach ($array_util as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->utilisateur = implode(' , ', $utilisateur);
                    }
                }

                return $request;
            }
        }
    }

    function getUtil($util_id)
    {
        $this->load->model('Utilisateur_m', 'util');
        $param = array(
            'clause' => array('util_id' => $util_id),
            'method' => 'row',
            'columns' => array('util_prenom'),
        );
        $request = $this->util->get($param);
        return (!empty($request)) ? $request->util_prenom : '';
    }

    function cruTache()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Tache_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("client_id" => $data['client_id'], "action" => $action);
                    $data['tache_etat'] = 1;
                    $data['tache_createur'] = $_SESSION['session_utilisateur']['util_prenom'];

                    $data['tache_date_creation'] = date('Y-m-d H:i:s', strtotime($data['tache_date_creation']));
                    $data['tache_date_echeance'] = date('Y-m-d H:i:s', strtotime($data['tache_date_echeance']));
                    $data['type_tache_id'] = 3;

                    unset($data['action']);
                    unset($data['search_terms']);
                    $data['participant'] = implode(',', $data['participant']);

                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("tache_id" => $data['tache_id']);
                        $data_retour['tache_id'] = $data['tache_id'];
                        unset($data['tache_id']);
                    }
                    // else{
                    //     $data['tache_date_creation'] = date_sql($data['tache_date_creation'],'-','jj/MM/aaaa');
                    // $data['tache_date_echeance'] = date_sql($data['tache_date_echeance'],'-','jj/MM/aaaa');
                    // }
                    $request = $this->Tache_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Tâche ajoutée avec succès" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('TacheClient_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tachecli_id' => $data['tachecli_id']),
                    'columns' => array('tachecli_id'),
                    'method' => "row",
                );
                $request_get = $this->TacheClient_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tachecli_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce tâche ? ",
                            'btnConfirm' => "Supprimer la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tachecli_id' => $data['tachecli_id']);
                    $data_update = array('tachecli_etat' => 0);
                    $request = $this->TacheClient_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tachecli_id']),
                            'message' => "La tâche a bien été supprimée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function valideTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation de la tâche…",
                );
                $this->load->model('TacheClient_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tachecli_id' => $data['tachecli_id']),
                    'columns' => array('tachecli_id'),
                    'method' => "row",
                );
                $request_get = $this->TacheClient_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tachecli_id'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous vraiment valider cette tâche ? ",
                            'btnConfirm' => "Valider la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tachecli_id' => $data['tachecli_id']);
                    $data_update = array(
                        'tachecli_valide' => 1,
                        'tachecli_date_validation' => Carbon::now()->toDateTimeString()
                    );
                    $request = $this->TacheClient_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tachecli_id']),
                            'message' => "Tâche validée avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    /********Lot********/

    public function getLotDossier()
    {
        $this->load->model('ClientLot_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $data_post['client_id']
                    ),
                    'order_by_columns' => 'c_lot_client.cliLot_principale ASC',
                    'method' => "result",
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                        'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id'
                    ),
                    // 'clause' =>array(')
                );

                $data_lot = $this->ClientLot_m->get($params);

                $listelot = array();

                if (!empty($data_lot)) {
                    foreach ($data_lot as $key => $value) {
                        $avoir_charge = $this->getCharge($value->cliLot_id);
                        $avoir_facture = $this->getFacture($value->cliLot_id);
                        $avoir_encaiss = $this->getEncaissement($value->cliLot_id);
                        if ($avoir_charge == false && $avoir_facture == false && $avoir_encaiss == false) {
                            $array_lot = array(
                                'cliLot_id' => $value->cliLot_id,
                                'dossier_id' => $value->dossier_id,
                                'progm_id' => $value->progm_id,
                                'progm_nom' => $value->progm_nom,
                                'progm_ville' => $value->progm_ville,
                                'typelot_libelle' => $value->typelot_libelle,
                                'gestionnaire_nom' => $value->gestionnaire_nom,
                                'cliLot_principale' => $value->cliLot_principale,
                                'cliLot_ville' => $value->cliLot_ville,
                                'avoir_data' => false
                            );
                            array_push($listelot, $array_lot);
                        } else {
                            $array_lot = array(
                                'cliLot_id' => $value->cliLot_id,
                                'dossier_id' => $value->dossier_id,
                                'progm_id' => $value->progm_id,
                                'progm_nom' => $value->progm_nom,
                                'progm_ville' => $value->progm_ville,
                                'typelot_libelle' => $value->typelot_libelle,
                                'gestionnaire_nom' => $value->gestionnaire_nom,
                                'cliLot_principale' => $value->cliLot_principale,
                                'cliLot_ville' => $value->cliLot_ville,
                                'avoir_data' => true
                            );
                            array_push($listelot, $array_lot);
                        }
                    }
                }

                $data['lot'] = $listelot;
                $data['dossier'] = $this->getDossier();
                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['client_id'] = $data_post['client_id'];

                // $data["countAllResult"] = $this->countAllData('ClientLot_m',array('dossier_id' => 1));
                // $total_lot = $data["countAllResult"];

                $this->renderComponant("Clients/clients/lot/list-lot", $data);
            }
        }
    }

    private function getCharge($cliLot_id)
    {
        $this->load->model('Charges_m');
        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
            'join' => array(
                'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id'
            ),
        );
        $avoir_data = false;
        $charge = $this->Charges_m->get($param);
        if (!empty($charge)) {
            $avoir_data = true;
        }
        return $avoir_data;
    }

    private function getFacture($cliLot_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('FactureLoyer_m', 'facture');

        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
        );

        $bailLot = $this->bail->get($param);
        $avoir_data = false;
        foreach ($bailLot as $key => $value) {
            $param_facure = array('clause' => array('bail_id' => $value->bail_id));
            $facture = $this->facture->get($param_facure);
            if (!empty($facture)) {
                $avoir_data = true;
            }
        }
        return $avoir_data;
    }

    private function getEncaissement($cliLot_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('Encaissement_m', 'encaiss');

        $param = array(
            'clause' => array('cliLot_id' => $cliLot_id),
        );

        $bailLot = $this->bail->get($param);
        $avoir_data = false;
        foreach ($bailLot as $key => $value) {
            $param_encaiss = array('clause' => array('bail_id' => $value->bail_id));
            $encaiss = $this->encaiss->get($param_encaiss);
            if (!empty($encaiss)) {
                $avoir_data = true;
            }
        }
        return $avoir_data;
    }

    public function getFormLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['client_id'] = $data_post['client_id'];

                $this->load->model('ClientLot_m');

                $data['action'] = $data_post['action'];

                $data['dossier'] = $this->getDossier();
                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['produitcli'] = $this->getlisteProduitClient();

                $lot_principal = array();
                $clause = array(
                    'cli_id' => $data_post['client_id'],
                    'cliLot_principale' => 1
                );
                $lot_principal_request = $this->ClientLot_m->get(
                    array(
                        'columns' => ['cliLot_principale'],
                        'clause' => $clause
                    )
                );

                $data["lot_verify_principal"] = (!empty($lot_principal_request)) ? 2 : 1;

                $this->renderComponant("Clients/clients/lot/form-lot", $data);
            }
        }
    }

    public function formAlertAjoutLot()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/lot/form-alert");
        }
    }

    public function getFicheLot()
    {
        $this->load->model('ClientLot_m');
        $this->load->model('Client_m');
        $this->load->model('Bail_m');
        $this->load->model('IndexationLoyer_m');
        $this->load->model('IndiceValeurLoyer_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['client_id'] = $data_post['client_id'];
                $params = array(
                    'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                    'method' => "row",
                    'join' => array('c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'),
                );
                $data['lot'] = $request = $this->ClientLot_m->get($params);

                $data['client'] = $this->Client_m->get(
                    array('clause' => array("client_id" => $data_post['client_id']), 'method' => "row")
                );

                $data['gestionnaires'] = $this->getListGestionnaire();

                // Dernier bail
                $bailParams = array(
                    'clause' => array(
                        'c_bail.cliLot_id' => $data_post['cliLot_id'],
                        'c_bail.bail_date_cloture' => null
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'orderBy' => "c_bail.bail_id DESC"
                );
                $bail = $this->Bail_m->get($bailParams);

                // Les baux
                $bauxParams = array(
                    'clause' => array(
                        'c_bail.cliLot_id' => $data_post['cliLot_id'],
                    ),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'orderBy' => "c_bail.bail_id DESC"
                );
                $baux = $this->Bail_m->get($bauxParams);

                $data['bail'] = $bail;
                $data['baux'] = $baux;

                $data['listLot'] = $this->ClientLot_m->get(
                    array(
                        'clause' => array('cli_id ' => $data_post['client_id'], 'cliLot_etat' => 1),
                        'join' => array('c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'),
                    )
                );

                $this->renderComponant("Clients/clients/lot/fiche-lot", $data);
            }
        }
    }

    public function getModalIndexationLoyer()
    {
        $this->load->model('ClientLot_m');
        $this->load->model('Client_m');
        $this->load->model('IndiceValeurLoyer_m');
        $this->load->model('Bail_m');
        $this->load->model('IndexationLoyer_m');
        $this->load->model('TauxTVA_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                // Dernier bail
                $bailParams = array(
                    'clause' => array(
                        'c_bail.cliLot_id' => $data_post['cliLot_id'],
                        'c_bail.bail_date_cloture' => null
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'orderBy' => "c_bail.bail_id DESC"
                );
                $bail = $this->Bail_m->get($bailParams);

                // Les baux
                $bauxParams = array(
                    'clause' => array(
                        'c_bail.cliLot_id' => $data_post['cliLot_id'],
                    ),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'orderBy' => "c_bail.bail_id DESC"
                );

                $baux = $this->Bail_m->get($bauxParams);
                $indice_valeur_loyer = null;
                $indexations = null;

                if (isset($bail)) {
                    // Les valeurs des indices

                    $indice_valeur_loyer = $this->IndiceValeurLoyer_m->orderByDateParution($bail->indloyer_id);

                    // Les indexations pour le bail en cours
                    $indexationParams = array(
                        'clause' => array(
                            'c_indexation_loyer.bail_id' => $bail->bail_id,
                            'c_indexation_loyer.etat_indexation' => 1,
                        ),
                        'join' => array(
                            'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                            'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                        ),
                        'orderBy' => "c_indexation_loyer.indlo_id DESC"
                    );
                    $indexations = $this->IndexationLoyer_m->get($indexationParams);
                }

                $tva = $this->TauxTVA_m->get(array());

                $data['bail'] = $bail;
                $data['baux'] = $baux;
                $data['indice_valeur_loyer'] = $indice_valeur_loyer;
                $data['indexations'] = $indexations;
                $data['tva'] = $tva;

                $this->renderComponant("Clients/clients/lot/bails/modal-indexation-loyer", $data);
            }
        }
    }

    public function getModalHistoriqueBail()
    {
        $this->load->model('Bail_m');
        $this->load->model('PeriodiciteLoyer_m');
        $this->load->model('TypeBail_m');
        $this->load->model('NaturePriseEffet_m');
        $this->load->model('TypeEcheance_m');
        $this->load->model('IndiceLoyer_m');
        $this->load->model('PeriodeIndexation_m');
        $this->load->model('MomentFacturation_m');
        $this->load->model('BailArt_m');
        $this->load->model('ClientLot_m');
        $this->load->model('IndiceValeur_m');
        $this->load->model('IndexationLoyer_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $paramBaux = array(
                    'clause' => array(
                        'c_bail.cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                        'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                        'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                        'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                        'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                        'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    )
                );

                $baux = $this->Bail_m->get($paramBaux);

                $data['baux'] = $baux;

                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['periodicite_loyer'] = $this->PeriodiciteLoyer_m->get(array());
                $data['type_bail'] = $this->TypeBail_m->get(array());
                $data['nature_prise_effet'] = $this->NaturePriseEffet_m->get(array());
                $data['type_echeance'] = $this->TypeEcheance_m->get(array());
                $data['indice_loyer'] = $this->IndiceLoyer_m->get(array());
                $data['periode_indexation'] = $this->PeriodeIndexation_m->get(array());
                $data['moment_facturation'] = $this->MomentFacturation_m->get(array());
                $data['bail_art'] = $this->BailArt_m->get(array());

                $this->renderComponant("Clients/clients/lot/bails/modal-historique-bail", $data);
            }
        }
    }


    public function getModalHistoriqueContentBail()
    {
        $this->load->model('Bail_m');
        $this->load->model('PeriodiciteLoyer_m');
        $this->load->model('TypeBail_m');
        $this->load->model('NaturePriseEffet_m');
        $this->load->model('TypeEcheance_m');
        $this->load->model('IndiceLoyer_m');
        $this->load->model('PeriodeIndexation_m');
        $this->load->model('MomentFacturation_m');
        $this->load->model('BailArt_m');
        $this->load->model('ClientLot_m');
        $this->load->model('IndiceValeur_m');
        $this->load->model('IndexationLoyer_m');
        $this->load->model('FranchiseLoyer_m');
        $this->load->model('IndiceValeurLoyer_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $paramBail = array(
                    'clause' => array(
                        'c_bail.bail_id' => $data_post['bail_id']
                    ),
                    'method' => 'row',
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                        'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                        'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                        'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                        'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                        'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_bail.bail_util_id_cloture'
                    ),
                    'join_orientation' => 'left',
                );

                $bail = $this->Bail_m->get($paramBail);

                $data['bail'] = $bail;
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['periodicite_loyer'] = $this->PeriodiciteLoyer_m->get(array());
                $data['type_bail'] = $this->TypeBail_m->get(array());
                $data['nature_prise_effet'] = $this->NaturePriseEffet_m->get(array());
                $data['type_echeance'] = $this->TypeEcheance_m->get(array());
                $data['indice_loyer'] = $this->IndiceLoyer_m->get(array());
                $data['periode_indexation'] = $this->PeriodeIndexation_m->get(array());
                $data['moment_facturation'] = $this->MomentFacturation_m->get(array());
                $data['bail_art'] = $this->BailArt_m->get(array());

                $paramsFranchise = array(
                    'clause' => array('c_franchise_loyer.bail_id' => $data_post['bail_id'], 'c_franchise_loyer.etat_franchise' => 1),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_franchise_loyer.bail_id'
                    ),
                );

                $data['franchises'] = $this->FranchiseLoyer_m->get($paramsFranchise);

                $paramsIndexation = array(
                    'clause' => array(
                        'c_indexation_loyer.bail_id' => $data_post['bail_id'],
                        'c_indexation_loyer.etat_indexation' => 1
                    ),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                        'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                    ),
                );

                $data['indexations'] = $this->IndexationLoyer_m->get($paramsIndexation);
                $data['indices'] = $this->IndiceValeurLoyer_m->get(array());

                $this->renderComponant("Clients/clients/lot/bails/modal-history-content", $data);
            }
        }
    }

    public function FicheLot()
    {
        $this->load->model('ClientLot_m');
        $this->load->model('Client_m');
        $this->load->model('Rib_m');
        $this->load->model('DocumentLot_m');
        $this->load->model('DocumentSyndic_m');
        $this->load->model('DocumentSie_m');
        $this->load->model('DocumentSip_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                // $data['client_id'] = $data_post['client_id'];
                $params = array(
                    'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                    'method' => "row",
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id'
                    ),
                );

                $param_rib = array(
                    'clause' => array(),
                    'order_by_columns' => "rib_id  DESC",
                    'method' => "row"
                );

                $paramsDocLot = array(
                    'clause' => array(
                        'c_document_locataire.cliLot_id' => $data_post['cliLot_id']
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_lot_client' => 'c_document_locataire.cliLot_id = c_lot_client.cliLot_id',
                        'c_utilisateur' => 'c_document_locataire.util_id = c_utilisateur.util_id'
                    ),
                    'join_orientation' => 'left'
                );

                $paramsDocSyndic = array(
                    'clause' => array(
                        'c_document_syndic.cliLot_id' => $data_post['cliLot_id']
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_lot_client' => 'c_document_syndic.cliLot_id = c_lot_client.cliLot_id',
                        'c_utilisateur' => 'c_document_syndic.util_id = c_utilisateur.util_id'
                    ),
                    'join_orientation' => 'left'
                );

                $paramsDocSie = array(
                    'clause' => array(
                        'c_document_sie.cliLot_id' => $data_post['cliLot_id']
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_lot_client' => 'c_document_sie.cliLot_id = c_lot_client.cliLot_id',
                        'c_utilisateur' => 'c_document_sie.util_id = c_utilisateur.util_id'
                    ),
                    'join_orientation' => 'left'
                );

                $paramsDocSip = array(
                    'clause' => array(
                        'c_document_sip.cliLot_id' => $data_post['cliLot_id']
                    ),
                    'method' => "row",
                    'join' => array(
                        'c_lot_client' => 'c_document_sip.cliLot_id = c_lot_client.cliLot_id',
                        'c_utilisateur' => 'c_document_sip.util_id = c_utilisateur.util_id'
                    ),
                    'join_orientation' => 'left'
                );

                $data['lot'] = $request = $this->ClientLot_m->get($params);
                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['dossier'] = $this->getDossierlot();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['syndicat'] = $this->getListSyndicat();
                $data['produitcli'] = $this->getlisteProduitClient();
                $data['echeance'] = $this->getEcheance();
                $data['rib'] = $this->Rib_m->get($param_rib);
                $data['document'] = $this->DocumentLot_m->get($paramsDocLot);
                $data['doc_syndic'] = $this->DocumentSyndic_m->get($paramsDocSyndic);
                $data['doc_sie'] = $this->DocumentSie_m->get($paramsDocSie);
                $data['doc_sip'] = $this->DocumentSip_m->get($paramsDocSip);

                $data['page'] = $data_post['page'];
                // $data['action'] = $data_post['action'];

                // $data['client'] = $this->Client_m->get(
                //     array('clause' => array("client_id" => $data_post['client_id']), 'method' => "row")
                // );
                if ($data_post['page'] == "form") {
                    $this->renderComponant("Clients/clients/lot/identification_lot/form-update-identification-lot", $data);
                } else {
                    $this->renderComponant("Clients/clients/lot/identification_lot/fiche-identification-lot", $data);
                }
            }
        }
    }

    function getDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('dossier_etat' => 1),
                    'columns' => array('dossier_id', 'dossier_libelle', 'client_id')
                );
                $request = $this->dossier->get($params);
                $data["client_id"] = $data_post['client_id'];
                $liste_dossier = array();
                foreach ($request as $key => $value) {
                    if ($value->client_id != "") {
                        $array_client = explode(',', $value->client_id);
                        if (in_array($data["client_id"], $array_client)) {
                            $client = array();
                            foreach ($array_client as $value_client) {
                                array_push($client, $this->getClient($value_client));
                            }
                            $request[$key]->client = implode(' , ', $client);
                            $liste_dossier[] = $request[$key];
                        }
                    }
                }
                return $liste_dossier;
            }
        }
    }

    function getDossierlot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('dossier_etat' => 1),
                    'columns' => array('dossier_id', 'dossier_libelle', 'client_id')
                );
                $request = $this->dossier->get($params);

                return $request;
            }
        }
    }

    function getEcheance()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TypeEcheance_m');
                $params = array(
                    'columns' => array('tpech_id', 'tpech_valeur')
                );

                $request = $this->TypeEcheance_m->get($params);

                return $request;
            }
        }
    }

    function cruLot()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('ClientLot_m');
                $this->load->model('Program_m');
                $this->load->model('MandatCli_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $data_lot = array(
                        'gestionnaire_id' => $data['gestionnaire_id'],
                        'dossier_id' => $data['dossier_id'],
                        'typelot_id' => $data['typelot_id'],
                        'progm_id' => $data['progm_id'],
                        'typologielot_id' => $data['typologielot_id'],
                        'cliLot_mode_paiement' => "Virement",
                        'cliLot_encaiss_loyer' => "Compte Celavigestion",
                        'produitcli_id' => implode(',', $data['produitcli_id']),
                        'cliLot_etat' => 1,
                        'cliLot_principale' => $data['cliLot_principale'],
                        'cli_id' => $data['cli_id']
                    );

                    $action = $data['action'];
                    $data_retour = array("client_id" => $data['cli_id'], "action" => $action);
                    $data['etat_lot'] = 1;
                    unset($data['action']);
                    $clause = null;
                    $clause_pgm = array("progm_id" => $data['progm_id']);
                    if ($action == "edit") {
                        $clause = array("cliLot_id" => $data['cliLot_id']);
                        $data_retour['cliLot_id'] = $data['cliLot_id'];
                        unset($data['cliLot_id']);
                        unset($data['undefined']);

                        if ($data['cliLot_principale'] == 1) {
                            // $claus = array('client_id' => $data['client_id']);
                            $data_update = array('cliLot_principale' => 2);
                            $this->ClientLot_m->save_data($data_update, $clause);
                        }
                    }
                    $request = $this->ClientLot_m->save_data($data_lot, $clause);
                    // $request_pgm = $this->Program_m->save_data($data_pgm,$clause_pgm);
                    $insert_id = $this->db->insert_id();
                    $data_mandat = array(
                        'cliLot_id' => $insert_id,
                        'client_id' => $data['cli_id']
                    );
                    $request_mandat = $this->MandatCli_m->save_data($data_mandat);

                    if ($action == "add") {
                        $data_retour['cliLot_id'] = $request;
                    }
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Lot ajouté avec succès" : "La modification est effectuée avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateInfoLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m');
                $this->load->model('Program_m');

                $data = $_POST;

                $data['rib'] = NULL;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $dossier_id = $data['dossier_id'];
                if (!empty($_FILES)) {
                    $target_dir = "../documents/Lot_rib/$dossier_id/";
                    $file = $_FILES['file_0']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file_0']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;

                    $this->makeDirPath($target_dir);
                    if (file_exists($path_filename_ext)) {
                        $i = 1;
                        $new_filename = $filename . "_" . $i . "." . $ext;
                        $new_path_filename_ext = $target_dir . $new_filename;
                        while (file_exists($new_path_filename_ext)) {
                            $i++;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                        }
                        $path_filename_ext = $new_path_filename_ext;
                    }
                    move_uploaded_file($temp_name, $path_filename_ext);
                    $data['rib'] = $path_filename_ext;
                }


                $data_pgm = array(
                    'progm_adresse1' => $data['progm_adresse1'],
                    'progm_adresse2' => $data['progm_adresse2'],
                    'progm_adresse3' => $data['progm_adresse3'],
                    'progm_cp' => $data['progm_cp'],
                    'progm_ville' => $data['progm_ville'],
                    'progm_pays' => $data['progm_pays']
                );

                if ($data['cliLot_iban_client'] != '' || $data['cliLot_iban_client'] != NULL) {
                    $data['cliLot_iban_client'] = str_replace(' ', '', $data['cliLot_iban_client']);
                }

                $data_lot = array(
                    'gestionnaire_id' => $data['gestionnaire_id'],
                    'syndicat_id' => $data['syndicat_id'],
                    'dossier_id' => $data['dossier_id'],
                    'typelot_id' => $data['typelot_id'],
                    'progm_id' => $data['progm_id'],
                    'typologielot_id' => $data['typologielot_id'],
                    'cliLot_encaiss_loyer' => $data['cliLot_encaiss_loyer'],
                    'cliLot_mode_paiement' => $data['cliLot_mode_paiement'],
                    'cliLot_iban_client' => $data['cliLot_iban_client'],
                    'cliLot_bic_client' => $data['cliLot_bic_client'],
                    'produitcli_id' => $data['produitcli_id'],
                    'cliLot_etat' => 1,
                    'cliLot_principale' => $data['cliLot_principale'],
                    'activite' => $data['activite'],
                    'defiscalisation' => $data['defiscalisation'],
                    'fin_defiscalisation' => $data['fin_defiscalisation'],
                    'tva' => $data['tva'],
                    'rembst_tom' => $data['rembst_tom'],
                    // 'facture_loyer' => $data['facture_loyer'],
                    'facture_charge' => $data['facture_charge'],
                    'facture_tom' => $data['facture_tom'],
                    'comptabilite' => $data['comptabilite'],
                    'cliLot_num' => $data['cliLot_num'],
                    'cliLot_num_parking' => $data['cliLot_num_parking'],
                    'cliLot_adresse1' => $data['cliLot_adresse1'],
                    'cliLot_adresse2' => $data['cliLot_adresse2'],
                    'cliLot_adresse3' => $data['cliLot_adresse3'],
                    'cliLot_cp' => $data['cliLot_cp'],
                    'cliLot_ville' => $data['cliLot_ville'],
                    'cliLot_pays' => $data['cliLot_pays'],
                    'envoi_loye_gest' => $data['envoi_loye_gest']
                );

                if ($data['rib'] != NULL) {
                    $data_lot['rib'] = $data['rib'];
                }

                unset($data['undefined']);

                $cliLot_id = $data['cliLot_id'];
                unset($data['cliLot_id']);
                unset($data['page']);

                if ($data['cliLot_principale'] == 1) {
                    $claus = array('dossier_id' => $data['dossier_id']);
                    $data_update = array('cliLot_principale' => 2);
                    $this->ClientLot_m->save_data($data_update, $claus);
                }

                $clause = array("cliLot_id" => $cliLot_id);
                $clause_pgm = array("progm_id" => $data['progm_id']);

                $request = $this->ClientLot_m->save_data($data_lot, $clause);
                if ($data['progm_id'] != 309) {
                    $request_pgm = $this->Program_m->save_data($data_pgm, $clause_pgm);
                }
                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "Modification reussie."));
                }

                echo json_encode($retour);
            }
        }
    }

    public function remove_ribLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m');
                $this->load->model('Program_m');

                $data_post = $_POST['data'];


                $param = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'method' => 'row'
                );

                $data = $this->ClientLot_m->get($param);

                $data_lot = array(
                    'gestionnaire_id' => $data->gestionnaire_id,
                    'dossier_id' => $data->dossier_id,
                    'typelot_id' => $data->typelot_id,
                    'progm_id' => $data->progm_id,
                    'typologielot_id' => $data->typologielot_id,
                    'cliLot_encaiss_loyer' => $data->cliLot_encaiss_loyer,
                    'cliLot_mode_paiement' => $data->cliLot_mode_paiement,
                    'cliLot_iban_client' => $data->cliLot_iban_client,
                    'cliLot_bic_client' => $data->cliLot_bic_client,
                    'produitcli_id' => $data->produitcli_id,
                    'cliLot_etat' => 1,
                    'cliLot_principale' => $data->cliLot_principale,
                    'rib' => NULL
                );

                $cliLot_id = $data_post['cliLot_id'];
                //    $data['client_date_creation'] = date_sql($data['client_date_creation'],'-','jj/MM/aaaa');
                unset($data_post['cliLot_id']);

                $clause = array("cliLot_id" => $cliLot_id);

                $request = $this->ClientLot_m->save_data($data_lot, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "Fichier Supprimé"));
                }

                echo json_encode($retour);
            }
        }
    }

    /****Programme****/

    public function FormProgramme()
    {
        $this->load->model('Program_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Program_m');
                    $data['programme'] = $this->Program_m->get(
                        array('clause' => array("progm_id" => $data_post['progm_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
            }
            $this->renderComponant("Clients/clients/form-programme", $data);
        }
    }

    /*******Gestionnaire**********/

    public function FormGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TypeContactGestionnaire_m', 'type_contact');
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Gestionnaire_m', 'gestionnaire');
                    $data['gestionnaire'] = $this->gestionnaire->get(
                        array('clause' => array("gestionnaire_id" => $data_post['gestionnaire_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
                $data['type_contact'] = $this->type_contact->get(array());
            }
            $this->renderComponant("Clients/clients/form-gestionnaire", $data);
        }
    }

    public function FormSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TypeContactSyndicat_m', 'type_contact');
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Syndicat_m', 'syndicat');
                    $data['syndicat'] = $this->syndicat->get(
                        array('clause' => array("syndicat_id" => $data_post['syndicat_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
                $data['type_contact'] = $this->type_contact->get(array());
            }
            $this->renderComponant("Clients/clients/form-syndicat", $data);
        }
    }

    /***Document propriétaire**/

    public function listDocument()
    {
        $this->load->model('DocumentClient_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('doc_id', 'doc_nom', 'doc_path', 'doc_date_creation', 'c_utilisateur.util_id', 'util_prenom'),
                    'clause' => array(
                        'client_id' => $data_post['client_id'],
                        'doc_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_client.util_id')
                );

                $data["client_id"] = $data_post['client_id'];
                $data['document'] = $this->DocumentClient_m->get($params);

                $this->renderComponant("Clients/clients/document/list-document", $data);
            }
        }
    }

    public function formUpload()
    {

        $data_post = $_POST['data'];
        $data['client_id'] = $data_post['client_id'];

        $this->renderComponant('Clients/clients/document/formulaire_upload_file', $data);
    }

    public function uploadfile()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        // $cli_id = $_POST['cli_id'];
        $client_id = $_POST['client_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/$client_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_nom' => $original_name,
                'doc_date_creation' => date('Y-m-d H:i:s'),
                'doc_path' => str_replace('\\', '/', $filePath),
                'doc_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'client_id' => $client_id
            );

            $this->load->model('DocumentClient_m');
            $this->DocumentClient_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getDocpath()
    {
        $this->load->model('DocumentClient_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_id', 'doc_path'),
                    'clause' => array('doc_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentClient_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function FormUpdateDocClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['client_id'] = $data_post['client_id'];

                $this->load->model('DocumentClient_m');
                $data['document'] = $this->DocumentClient_m->get(
                    array('clause' => array("doc_id" => $data_post['doc_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/document/formUpdateDocument", $data);
        }
    }

    public function UpdateDocumentClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentClient_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("client_id" => $data['client_id']);
                    $doc_id = $data['doc_id'];
                    unset($data['doc_id']);

                    $clause = array("doc_id" => $doc_id);
                    $request = $this->DocumentClient_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removedocClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentClient_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_id' => $data['doc_id']),
                    'columns' => array('doc_id', 'doc_nom'),
                    'method' => "row",
                );
                $request_get = $this->DocumentClient_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_id' => $data['doc_id']);
                    $data_update = array('doc_etat' => 0);
                    $request = $this->DocumentClient_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    /**********Document taxe foncière***********/

    public function listeDocumentTaxe()
    {
        $this->load->model('DocumentTaxe_m');
        $this->load->model('Taxe_m');
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('doc_taxe_id', 'doc_taxe_nom', 'doc_taxe_path', 'doc_taxe_creation', 'util_prenom', 'c_utilisateur.util_id', 'annee', 'doc_taxe_traite', 'doc_taxe_traitement', 'prenom_util'),
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'annee' => $data_post['taxeannee'],
                        'doc_taxe_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_lot_taxe.util_id')
                );

                $data["cliLot_id"] = $data_post['cliLot_id'];
                $data['document'] = $this->DocumentTaxe_m->get($params);
                $data['htaxe'] = $this->getHistoriquetaxe($data_post['cliLot_id']);

                $request_taxe = $this->Taxe_m->get(
                    array(
                        'columns' => array(
                            'taxe_montant',
                            'taxe_tom',
                            'taxe_id',
                            'taxe_valide',
                            'util_prenom',
                            'taxe_date_validation',
                            'annee',
                        ),
                        'clause' => array(
                            'cliLot_id' => $data_post['cliLot_id'],
                            'annee' => $data_post['taxeannee'],
                        ),
                        'method' => 'row',
                    )
                );

                $request_date = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'order_by_columns' => 'mandat_date_signature DESC',
                        'method' => 'result'
                    )
                );

                $data['date_signature'] = $request_date;

                if (!empty($data['date_signature'])) {
                    foreach ($data['date_signature'] as $key => $value) {
                        $date[] = array(
                            'annee' => $value->mandat_date_signature
                        );
                    }
                    $data['max_date'] = max($date);
                }

                $data['taxe'] = (!empty($request_taxe) ? $request_taxe : []);

                $this->renderComponant("Clients/clients/lot/taxes/list-document", $data);
            }
        }
    }

    public function listeDocTaxe()
    {
        $this->load->model('DocumentTaxe_m');
        $this->load->model('Taxe_m');
        $this->load->model('ClientLot_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['rembst_tom'] = $this->ClientLot_m->get(
                    array(
                        'columns' => array('rembst_tom'),
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row'
                    )
                );

                $all_document = $this->DocumentTaxe_m->get(
                    array(
                        'columns' => array(
                            'c_document_lot_taxe.doc_taxe_id',
                            'c_document_lot_taxe.doc_taxe_nom',
                            'c_document_lot_taxe.doc_taxe_path',
                            'c_document_lot_taxe.doc_taxe_creation',
                            'c_document_lot_taxe.doc_taxe_traitement',
                            'c_document_lot_taxe.doc_taxe_etat',
                            'c_document_lot_taxe.doc_taxe_traite',
                            'c_document_lot_taxe.util_id',
                            'c_document_lot_taxe.cliLot_id',
                            'c_document_lot_taxe.util_id',
                            'c_document_lot_taxe.util_client',
                            'c_document_lot_taxe.annee',
                            'c_utilisateur.util_prenom',
                            'c_client.client_nom',
                        ),
                        'clause' => array(
                            'cliLot_id' => $data_post['cliLot_id'],
                            'annee' => $data_post['annee'],
                            'doc_taxe_etat' => 1
                        ),
                        'join' => array(
                            'c_utilisateur' => 'c_utilisateur.util_id = c_document_lot_taxe.util_id',
                            'c_utilisateur_client' => 'c_utilisateur_client.client_id = c_document_lot_taxe.util_client',
                            'c_client' => 'c_client.client_id = c_document_lot_taxe.util_client'
                        ),
                        'join_orientation' => 'left'
                    )
                );

                $data['document'] = $all_document;

                $data["cliLot_id"] = $data_post['cliLot_id'];
                $data['htaxe'] = $this->getHistoritaxe($data_post['cliLot_id']);
                $request_taxe = $this->Taxe_m->get(
                    array(
                        'columns' => array('taxe_montant', 'taxe_tom', 'taxe_id', 'taxe_valide', 'util_prenom', 'taxe_date_validation', 'annee', 'taxe_date_cloture', 'taxe_cloture'),
                        'clause' => array(
                            'cliLot_id' => $data_post['cliLot_id'],
                            'annee' => $data_post['annee']
                        ),
                        'method' => 'row'
                    )
                );

                $data['annee'] = $data_post['annee'];

                $data['taxe'] = (!empty($request_taxe) ? $request_taxe : []);
                $this->renderComponant("Clients/clients/lot/taxes/document", $data);
            }
        }
    }

    public function getHistoriquetaxe($taxe_id)
    {
        $this->load->model('Historiquetaxe_m');
        $params = array(
            'clause' => array('taxe_id' => $taxe_id),
            'columns' => array('htaxe_id', 'htaxe_date_validation', 'util_prenom', 'htaxe_valide', 'annee')
        );
        $request = $this->Historiquetaxe_m->get($params);
        return $request;
    }

    public function getHistoritaxe($taxe_id)
    {
        $this->load->model('Historiquetaxe_m');
        $data_post = $_POST["data"];
        $params = array(
            'clause' => array(
                'taxe_id' => $taxe_id,
                'annee' => $data_post['annee']
            ),
            'columns' => array('htaxe_id', 'htaxe_date_validation', 'util_prenom', 'htaxe_valide', 'annee')
        );
        $request = $this->Historiquetaxe_m->get($params);
        return $request;
    }

    public function formUploadTaxe()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];
        $data['annee'] = $data_post['annee'];
        $this->renderComponant('Clients/clients/lot/taxes/form_upload_taxe', $data);
    }

    public function uploadfileTaxe()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $annee = $_POST['annee'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/taxe/$annee/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_taxe_nom' => $original_name,
                'doc_taxe_creation' => date('Y-m-d H:i:s'),
                'doc_taxe_path' => str_replace('\\', '/', $filePath),
                'doc_taxe_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
                'annee' => $annee
            );

            $this->load->model('DocumentTaxe_m');
            $this->DocumentTaxe_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getpathDoc()
    {
        $this->load->model('DocumentTaxe_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_taxe_id', 'doc_taxe_path'),
                    'clause' => array('doc_taxe_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentTaxe_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function removedocTaxe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentTaxe_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_taxe_id' => $data['doc_taxe_id']),
                    'columns' => array('doc_taxe_id', 'doc_taxe_nom'),
                    'method' => "row",
                );
                $request_get = $this->DocumentTaxe_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_taxe_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_taxe_id' => $data['doc_taxe_id']);
                    $data_update = array('doc_taxe_etat' => 0);
                    $request = $this->DocumentTaxe_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_taxe_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocTaxeTraitement()
    {
        $this->load->model('DocumentTaxe_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array(
                        'doc_taxe_id' => $data_post['doc_taxe_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentTaxe_m->get($params);
                if ($request->doc_taxe_traite == 1) {
                    $data['doc_taxe_traite'] = 0;
                    $data['doc_taxe_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_taxe_id' => $data_post['doc_taxe_id']);
                    $this->DocumentTaxe_m->save_data($data, $clause);
                } else {
                    $data['doc_taxe_traite'] = 1;
                    $data['doc_taxe_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_taxe_id' => $data_post['doc_taxe_id']);
                    $this->DocumentTaxe_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function FormUpdateTaxe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentTaxe_m');
                $data['document'] = $this->DocumentTaxe_m->get(
                    array('clause' => array("doc_taxe_id" => $data_post['doc_taxe_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/taxes/formUpdatetaxe", $data);
        }
    }

    public function UpdateDocumentTaxe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentTaxe_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_taxe_id = $data['doc_taxe_id'];
                    unset($data['doc_taxe_id']);

                    $clause = array("doc_taxe_id" => $doc_taxe_id);
                    $request = $this->DocumentTaxe_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function cruTaxe()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Taxe_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action_taxe'];
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $data['util_prenom'] = $_SESSION['session_utilisateur']['util_prenom'];

                    unset($data['action_taxe']);

                    if ($action == "edit") {
                        $clause = array("taxe_id" => $data['taxe_id']);
                        unset($data['taxe_id']);
                        unset($data['cliLot_id']);
                        $request = $this->Taxe_m->save_data($data, $clause);
                    } else {
                        $request = $this->Taxe_m->save_data($data);
                    }

                    $data_retour['taxe_id'] = ($action == "add") ? $request : $_POST['data']['taxe_id']['value'];
                    $data_retour['util_prenom'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $data_retour['taxe_date_validation'] = Carbon::now()->toDateTimeString();
                    $data_retour['action'] = 'edit';

                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Information enregistrée avec succès" : "Modification réussie"));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    public function valideTaxe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation de la taxe…",
                );
                $this->load->model('Taxe_m');
                $this->load->model('Historiquetaxe_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('taxe_id' => $data['taxe_id']),
                    'columns' => array('taxe_id'),
                    'method' => "row",
                );
                $request_get = $this->Taxe_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['taxe_id'],
                            'annee' => $data['annee'],
                            'title' => "Confirmation de la validation",
                            'text' => "Êtes-vous sûr de vouloir valider ces informations ? ",
                            'btnConfirm' => "Valider",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('taxe_id' => $data['taxe_id']);
                    $data_update = array(
                        'taxe_valide' => 1,
                        'taxe_date_validation' => Carbon::now()->toDateTimeString()
                    );
                    $data_taxe = array(
                        'htaxe_valide' => 1,
                        'htaxe_date_validation' => Carbon::now()->toDateTimeString(),
                        'util_prenom' => $_SESSION['session_utilisateur']['util_prenom'],
                        'annee' => $data['annee'],
                        'taxe_id' => $data['taxe_id']
                    );
                    $request = $this->Taxe_m->save_data($data_update, $clause);
                    $request_taxe = $this->Historiquetaxe_m->save_data($data_taxe);

                    if ($request) {

                        $this->load->model('Historiquetaxe_m');
                        $params = array(
                            'clause' => array('taxe_id' => $data['taxe_id']),
                            'columns' => array('htaxe_id', 'htaxe_date_validation', 'util_prenom', 'htaxe_valide')
                        );
                        $request_taxe = $this->Historiquetaxe_m->get($params);
                        foreach ($request_taxe as $key => $value) {
                            $request_taxe[$key]->htaxe_date_validation = date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $value->htaxe_date_validation)));
                        }

                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['taxe_id'],
                                'history' => $request_taxe
                            ),
                            'message' => "Informations validées",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function annulerTaxe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation de la taxe…",
                );
                $this->load->model('Taxe_m');
                $this->load->model('Historiquetaxe_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('taxe_id' => $data['taxe_id']),
                    'columns' => array('taxe_id'),
                    'method' => "row",
                );
                $request_get = $this->Taxe_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['taxe_id'],
                            'annee' => $data['annee'],
                            'title' => "Demande de confirmation",
                            'text' => "Êtes-vous sûr de vouloir annuler la validation de ces informations ? ",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('taxe_id' => $data['taxe_id']);
                    $data_update = array(
                        'taxe_valide' => 0,
                        'taxe_date_validation' => Carbon::now()->toDateTimeString()
                    );
                    $data_taxe = array(
                        'htaxe_valide' => 0,
                        'htaxe_date_validation' => Carbon::now()->toDateTimeString(),
                        'util_prenom' => $_SESSION['session_utilisateur']['util_prenom'],
                        'annee' => $data['annee'],
                        'taxe_id' => $data['taxe_id']
                    );

                    $request = $this->Taxe_m->save_data($data_update, $clause);
                    $request_taxe = $this->Historiquetaxe_m->save_data($data_taxe);

                    if ($request) {

                        $this->load->model('Historiquetaxe_m');
                        $params = array(
                            'clause' => array('taxe_id' => $data['taxe_id']),
                            'columns' => array('htaxe_id', 'htaxe_date_validation', 'util_prenom', 'htaxe_valide')
                        );
                        $request_taxe = $this->Historiquetaxe_m->get($params);
                        foreach ($request_taxe as $key => $value) {
                            $request_taxe[$key]->htaxe_date_validation = date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $value->htaxe_date_validation)));
                        }

                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['taxe_id'],
                                'history' => $request_taxe
                            ),
                            'message' => "Informations annulées",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*******Prêt**********/

    public function listeDocumentPret()
    {
        $this->load->model('DocumentPret_m');
        $this->load->model('EmpruntsPresence_m');
        $this->load->model('Pret_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['date'] = $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;
                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_pret_etat' => 1
                    ),
                    'join' => array(
                        'c_utilisateur' => 'c_utilisateur.util_id = c_document_pret.util_id',
                        'c_client' => 'c_client.client_id = c_document_pret.client_id'
                    ),
                    'join_orientation' => 'left'
                );

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['document'] = $this->DocumentPret_m->get($params);

                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'pret_etat' => 1,
                        'pret_annee' => $date
                    ),
                    'order_by_columns' => 'pret_annee ASC',
                    'method' => 'row'
                );

                $request = $this->Pret_m->get($params);
                $data['pret'] = $request;

                $request_id = array(
                    'columns' => array('pret_id'),
                    'clause' => array('cliLot_id' => intval($data_post['cliLot_id'])),
                    'order_by_columns' => "pret_id DESC",
                    'limit' => 1,
                    'method' => 'row'
                );
                $data['pret_id'] = $this->Pret_m->get($request_id);

                $data['presence_pret'] = $this->EmpruntsPresence_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row'
                    )
                );

                $this->renderComponant("Clients/clients/lot/prets/list-document", $data);
            }
        }
    }

    public function addPresencePret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('EmpruntsPresence_m');
                $data_post = $_POST['data'];

                $check_presence = $this->EmpruntsPresence_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                        'method' => 'row'
                    )
                );

                $data = array(
                    'etat_presence' => !empty($check_presence) ? $check_presence->etat_presence : $data_post['presence_emprunts'],
                    'cliLot_id' => $data_post['cliLot_id'],
                    'commentaire' => !empty($check_presence) ? $check_presence->commentaire : $data_post['commentaire'],
                    'util_id' => !empty($check_presence) ? $check_presence->util_id : $_SESSION['session_utilisateur']['util_id'],
                    'util_nom' => !empty($check_presence) ? $check_presence->util_nom : $_SESSION['session_utilisateur']['util_nom'] . ' ' . $_SESSION['session_utilisateur']['util_prenom'],
                    'util_date_declar' => !empty($check_presence) ? $check_presence->util_date_declar : date('Y-m-d'),
                    'util_role' => !empty($check_presence) ? $check_presence->util_role : 'conseiller',
                );


                if (empty($check_presence)) {
                    $this->EmpruntsPresence_m->save_data($data);
                }

                if ($data_post['action'] == 'modif') {
                    $data = array(
                        'etat_presence' => $data_post['presence_emprunts'],
                        'cliLot_id' => $data_post['cliLot_id'],
                        'commentaire' => $data_post['commentaire'],
                        'util_id' => $_SESSION['session_utilisateur']['util_id'],
                        'util_nom' => $_SESSION['session_utilisateur']['util_nom'] . ' ' . $_SESSION['session_utilisateur']['util_prenom'],
                        'util_date_declar' => date('Y-m-d'),
                        'util_role' => 'conseiller',
                    );

                    $clause = array('id_p_presence' => $check_presence->id_p_presence);
                    $this->EmpruntsPresence_m->save_data($data, $clause);
                }

                echo json_encode($data);
            }
        }
    }

    public function listPret()
    {

        $this->load->model('Pret_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array(
                        'pret_id',
                        'pret_annee',
                        'pret_capital_restant_prec',
                        'pret_capital_deblocage',
                        'pret_ecart_crd',
                        'pret_remboursement',
                        'pret_capital_amorti',
                        'pret_capital_restant',
                        'pret_interet',
                        'pret_assurance'
                    ),
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'pret_etat' => 1
                    ),
                    'order_by_columns' => 'pret_annee ASC'
                );

                $request = $this->Pret_m->get($params);
                return $request;
            }
        }
    }

    public function formUploadPret()
    {

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/prets/form_upload_pret', $data);
    }

    public function uploadfilePret()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/Pret/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_pret_nom' => $original_name,
                'doc_pret_creation' => date('Y-m-d H:i:s'),
                'doc_pret_path' => str_replace('\\', '/', $filePath),
                'doc_pret_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentPret_m');
            $this->DocumentPret_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getpathPret()
    {
        $this->load->model('DocumentPret_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_pret_id', 'doc_pret_path'),
                    'clause' => array('doc_pret_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentPret_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function AddAnneePret()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/lot/prets/formAddAnneePret");
        }
    }

    public function removeDocPret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentPret_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_pret_id' => $data['doc_pret_id']),
                    'columns' => array('doc_pret_id'),
                    'method' => "row",
                );
                $request_get = $this->DocumentPret_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_pret_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_pret_id' => $data['doc_pret_id']);
                    $data_update = array('doc_pret_etat' => 0);
                    $request = $this->DocumentPret_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_pret_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formUpdateDocPret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentPret_m');
                $data['document'] = $this->DocumentPret_m->get(
                    array('clause' => array("doc_pret_id" => $data_post['doc_pret_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Clients/clients/lot/prets/formUpdatePret", $data);
        }
    }

    public function UpdateDocumentPret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentPret_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_pret_id = $data['doc_pret_id'];
                    unset($data['doc_pret_id']);

                    $clause = array("doc_pret_id" => $doc_pret_id);
                    $request = $this->DocumentPret_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocPretTraitement()
    {
        $this->load->model('DocumentPret_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_pret_id' => $data_post['doc_pret_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentPret_m->get($params);
                if ($request->doc_pret_traite == 1) {
                    $data['doc_pret_traite'] = 0;
                    $data['doc_pret_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_pret_id' => $data_post['doc_pret_id']);
                    $this->DocumentPret_m->save_data($data, $clause);
                } else {
                    $data['doc_pret_traite'] = 1;
                    $data['doc_pret_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_pret_id' => $data_post['doc_pret_id']);
                    $this->DocumentPret_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function removePret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Pret_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('pret_id' => $data['pret_id']),
                    'columns' => array('pret_id'),
                    'method' => "row",
                );
                $request_get = $this->Pret_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['pret_id'],
                            'cliLot_id' => $data['cliLot_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer toutes les données de cette année ? ",
                            'btnConfirm' => "Supprimer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('pret_id' => $data['pret_id']);
                    $data_update = array('pret_etat' => 0);
                    $request = $this->Pret_m->delete_data($clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['pret_id'],
                                'cliLot_id' => $data['cliLot_id']
                            ),
                            'message' => "Suppression réussie",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formAlertDeletePret()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/lot/prets/form-alert");
        }
    }

    public function formAlertUpdatePret()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/lot/prets/form-alert-update");
        }
    }

    function AddPret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Pret_m');
                $data_post = $_POST['data'];
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data_retour = array("cliLot_id" => $data_post['cliLot_id']);

                $data['pret_etat'] = 1;
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['pret_annee'] = str_replace(",", ".", $data_post['pret_annee']);

                $data['pret_capital_restant_prec'] = str_replace(",", ".", $data_post['pret_capital_restant_prec']);
                $data['pret_capital_deblocage'] = str_replace(",", ".", $data_post['pret_capital_deblocage']);
                $data['pret_ecart_crd'] = str_replace(",", ".", $data_post['pret_ecart_crd']);
                $data['pret_remboursement'] = str_replace(",", ".", $data_post['pret_remboursement']);
                $data['pret_capital_amorti'] = str_replace(",", ".", $data_post['pret_capital_amorti']);
                $data['pret_capital_restant'] = str_replace(",", ".", $data_post['pret_capital_restant']);
                $data['pret_interet'] = str_replace(",", ".", $data_post['pret_interet']);
                $data['pret_assurance'] = str_replace(",", ".", $data_post['pret_assurance']);

                $request = $this->Pret_m->save_data($data);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement prêt réussi"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function UpdatePret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pret_m');
                $data_post = $_POST['data'];

                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data_retour = array("cliLot_id" => $data_post['cliLot_id']);

                $data['pret_etat'] = 1;
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $data['pret_capital_restant_prec'] = str_replace(",", ".", $data_post['pret_capital_restant_prec']);
                $data['pret_capital_deblocage'] = str_replace(",", ".", $data_post['pret_capital_deblocage']);
                $data['pret_ecart_crd'] = str_replace(",", ".", $data_post['pret_ecart_crd']);
                $data['pret_remboursement'] = str_replace(",", ".", $data_post['pret_remboursement']);
                $data['pret_capital_amorti'] = str_replace(",", ".", $data_post['pret_capital_amorti']);
                $data['pret_capital_restant'] = str_replace(",", ".", $data_post['pret_capital_restant']);
                $data['pret_interet'] = str_replace(",", ".", $data_post['pret_interet']);
                $data['pret_assurance'] = str_replace(",", ".", $data_post['pret_assurance']);

                $pret_id = $data_post['pret_id'];
                unset($data[$pret_id]);

                $clause = array("pret_id" => $pret_id);
                $request = $this->Pret_m->save_data($data, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Modification réussie."));
                }

                echo json_encode($retour);
            }
        }
    }

    /******Mandat********/

    public function listeMandat()
    {
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST['data'];
                $data = array();
                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'mandat_etat' => 1
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_mandat_new.client_id',
                        'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id'
                    )
                );

                $data['mandat'] = $mandat = $this->MandatNew_m->get($params);
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
                $data['docmandat'] = $this->getDocMandat();

                $this->load->model('Client_m');
                $data['client'] = $client = $this->Client_m->get(
                    array(
                        'clause' => array('client_id' => $data['client_id']),
                        'method' => 'row',
                        'columns' => array('c_client.*')
                    )
                );

                $this->load->model('HistoriqueVerifieIdentite_m');
                $data['data_verification_identite'] = $data_verification_identite = $this->HistoriqueVerifieIdentite_m->get(
                    array(
                        'clause' => array('client_id' => $client->client_id),
                        'columns' => array('historique_verificationidentite.*', 'util_prenom', 'util_nom'),
                        'join' => array(
                            'c_utilisateur' => 'c_utilisateur.util_id  = historique_verificationidentite.util_id'
                        ),
                    )
                );

                $data['etat_mandat'] = NULL;

                if (!empty($mandat)) {
                    $countmandat = count($mandat);
                    $data['etat_mandat'] = $mandat[$countmandat - 1]->etat_mandat_id;
                }

                $this->renderComponant("Clients/clients/lot/mandats/list-mandat", $data);
            }
        }
    }

    public function getDocMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $this->load->model('DocMandatClient_m', 'document');

                $params = array(
                    'columns' => array('doc_mandat_cli_id', 'doc_mandat_cli_nom', 'doc_mandat_cli_path', 'doc_mandat_cli_creation', 'c_utilisateur.util_id', 'util_prenom'),
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_mandat_cli_etat' => 1,
                        'doc_mandat_cli_type' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_client_mandat.util_id')
                );
                $request = $this->document->get($params);
                return $request;
            }
        }
    }

    public function FormMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();

                if ($data_post['action'] == "edit") {
                    $this->load->model('MandatNew_m');
                    $params = array(
                        'clause' => array('mandat_id ' => $data_post['mandat_id']),
                        'method' => "row"
                    );
                    $data['mandat'] = $this->MandatNew_m->get($params);
                }

                $data['action'] = $data_post['action'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
                $data['typemandat'] = $this->getTypeMandat();
            }

            $this->renderComponant("Clients/clients/lot/mandats/form-mandat", $data);
        }
    }

    public function alertMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Clients/clients/lot/mandats/form-mandat-alert");
            }
        }
    }

    public function sendMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('MandatNew_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('mandat_id' => $data['mandat_id']),
                    'columns' => array('mandat_id'),
                    'method' => "row",
                );
                $request_get = $this->MandatNew_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['mandat_id'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous vraiment basculer ce mandat à l'état 'Mandat envoyé' ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('mandat_id' => $data['mandat_id']);
                    $data_update = array('etat_mandat_id' => 2, 'mandat_date_envoi' => date('Y-m-d'));
                    $request = $this->MandatNew_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['mandat_id']),
                            'message' => "Mandat envoyé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***Signtature mandat***/

    public function SignerMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'clause' => array('mandat_id' => $data_post['mandat_id']),
                    'method' => 'row'
                );

                $data['mandat'] = $this->MandatNew_m->get($params);

                $data['mandat_id'] = $data_post['mandat_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
            }

            $this->renderComponant("Clients/clients/lot/mandats/sign-mandat", $data);
        }
    }

    public function AddSignMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id']);
                $mandat_id = $data['mandat_id'];
                $cliLot_id = $data['cliLot_id'];
                $data['etat_mandat_id'] = 3;
                $data['mandat_date_signature'] = date_sql($data['mandat_date_signature'], '-', 'jj/MM/aaaa');
                $data['mandat_path'] = NULL;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/clients/lots/mandat/$cliLot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;

                    $this->makeDirPath($target_dir);
                    if (file_exists($path_filename_ext)) {
                        $i = 1;
                        $new_filename = $filename . "_" . $i . "." . $ext;
                        $new_path_filename_ext = $target_dir . $new_filename;
                        while (file_exists($new_path_filename_ext)) {
                            $i++;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                        }
                        $path_filename_ext = $new_path_filename_ext;
                    }
                    move_uploaded_file($temp_name, $path_filename_ext);

                    $data['mandat_path'] = $path_filename_ext;
                }

                $clause = array("mandat_id" => $mandat_id);

                if ($data['mandat_date_creation'] == '2023-04-20') {
                    $data['mandat_date_creation'] = $data['mandat_date_signature'];
                    $data['mandat_date_envoi'] = $data['mandat_date_signature'];
                }

                $request = $this->MandatNew_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Signature ajoutée avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    /********Cloturer mandat**********/

    public function cloturerMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $this->load->model('TypeClotureMandat_m', 'cloture');
                $this->load->model('Bail_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'columns' => array('type_cloture_id', 'type_cloture_libelle'),
                );

                $params_bail = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'order_by_columns' => 'bail_id DESC',
                    'method' => 'row'
                );

                $data['bail'] = $this->Bail_m->get($params_bail);



                $default_date = date('Y-12-31');

                $data['default_date'] = $default_date;
                $data['type_cloture'] = $this->cloture->get($params);
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
            }

            $this->renderComponant("Clients/clients/lot/mandats/cloture-mandat", $data);
        }
    }

    public function ClotureMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id']);
                $mandat_id = $data['mandat_id'];
                $cliLot_id = $data['cliLot_id'];
                $data['etat_mandat_id'] = 6;
                $data['mandat_fichier_justificatif'] = NULL;
                $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data['mandat_date_action_cloture'] = date('Y-m-d :H:i:s');

                if (!empty($_FILES)) {
                    $target_dir = "../documents/clients/lots/mandat/$cliLot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (file_exists($path_filename_ext)) {
                        $i = 1;
                        $new_filename = $filename . "_" . $i . "." . $ext;
                        $new_path_filename_ext = $target_dir . $new_filename;
                        while (file_exists($new_path_filename_ext)) {
                            $i++;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                        }
                        $path_filename_ext = $new_path_filename_ext;
                    }
                    move_uploaded_file($temp_name, $path_filename_ext);
                    $data['mandat_fichier_justificatif'] = $path_filename_ext;
                }

                $clause = array("mandat_id" => $mandat_id);
                unset($data['bail_id']);
                unset($data['pdl_id']);
                unset($data['bail_loyer_variable']);

                $request = $this->MandatNew_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Mandat clôturé avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function AddMandat()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('MandatNew_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id'], "action" => $action);
                    $cliLot_id = $data['cliLot_id'];
                    $data['mandat_date_creation'] = date('Y-m-d H:i:s', strtotime($data['mandat_date_creation']));
                    $data['mandat_etat'] = 1;
                    $data['etat_mandat_id'] = 1;
                    $data['mandat_type_id'] = 1;
                    unset($data['action']);
                    $type_mandat_id = $data['type_mandat_id'];

                    $clause = null;
                    $request = null; // Ajout d'une initialisation de $request

                    if ($action == "edit") {
                        $clause = array("mandat_id" => $data['mandat_id']);
                        $data_retour['mandat_id'] = $data['mandat_id'];
                        unset($data['mandat_id']);
                        $request = $this->MandatNew_m->save_data($data, $clause);
                    } else {
                        $mandat = $this->MandatNew_m->get(['clause' => ['cliLot_id' => $data['cliLot_id'], 'etat_mandat_id' => 1], 'method' => 'row']);
                        if (empty($mandat)) {
                            $request = $this->MandatNew_m->save_data($data);
                        }
                    }

                    if (!empty($request)) {
                        $gp = $this->generatePdf_modelMandat($cliLot_id, $type_mandat_id);

                        $retour = retour(true, "success", $data_retour, array("message" => $cliLot_id . "-" . $gp));
                    }
                }
                echo json_encode($retour);
                die;
            }
        }
    }

    public function generatePdf_modelMandat($cliLot_id, $type_mandat_id)
    {
        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.1") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");
        $this->load->model('MandatNew_m');
        $this->load->model('Rib_m');
        $data = array();
        $params = array(
            'clause' => array('c_mandat_new.cliLot_id' => $cliLot_id),
            'join' => array(
                'c_client' => 'c_client.client_id = c_mandat_new.client_id',
                'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                'c_lot_client' => 'c_lot_client.cli_id = c_mandat_new.client_id',
                'c_programme' => 'c_lot_client.progm_id = c_programme.progm_id',
                'c_dossier' => 'c_lot_client.dossier_id = c_dossier.dossier_id',
                'c_facturation_lot' => 'c_facturation_lot.dossier_id = c_dossier.dossier_id'
            ),
            'join_orientation' => 'left',
            'method' => "row"
        );

        $data['mandat'] = $this->MandatNew_m->get($params);

        $param_rib = array(
            'clause' => array(),
            'order_by_columns' => "rib_id  DESC",
            'method' => "row"
        );
        $data['rib'] = $this->Rib_m->get($param_rib);

        $mpdf = new \Mpdf\Mpdf();
        // $stylesheet = file_get_contents('./assets/css/prospections/modelMandat.css');

        if ($type_mandat_id == 1) {
            $html = $this->load->view("Clients/clients/lot/modele_mandat/pack_confort", $data, true);
        } else {
            $html = $this->load->view("Clients/clients/lot/modele_mandat/pack_confort", $data, true);
        }

        //$mpdf->WriteHTML($stylesheet,1);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);

        $filename = 'Mandat_N_' . str_pad($data['mandat']->mandat_id, 4, '0', STR_PAD_LEFT) . '_' . $data['mandat']->client_nom . '_' . $data['mandat']->progm_nom;
        $filename = str_replace(array(" ", "&"), "_", $filename);

        // if ($type_mandat_id == 1) {
        //     $filename = "pack_confort";
        // } else if ($type_mandat_id == 2) {
        //     $filename = "pack_essentiel";
        // } else {
        //     $filename = "pack_serenite";
        // }

        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/mandat/" . $cliLot_id;
        $url_doc = "../documents/clients/lots/mandat/$cliLot_id/$url";
        $this->makeDirPath($url_path);

        $url_file = $url_path . "/" . $url;
        if (file_exists($url_doc)) {
            $counter = 1;
            do {
                $url = $filename . '_' . $counter . '.pdf';
                $url_doc = "../documents/clients/lots/mandat/$cliLot_id/$url";
                $url_file = $url_path . "/" . $url;
                $counter++;
            } while (file_exists($url_doc));
        }

        $clause = array("cliLot_id" => $cliLot_id);
        $data = array(
            'mandat_path' => str_replace('\\', '/', $url_doc),
        );
        $this->MandatNew_m->save_data($data, $clause);

        $mpdf->Output($url_file, 'F');
        return $url_path;
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    /********Retracter mandat**********/

    public function retracterMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $this->load->model('TypeClotureMandat_m', 'cloture');
                $this->load->model('Bail_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'columns' => array('mandat_id', 'mandat_date_signature'),
                    'clause' => array('mandat_id' => $data_post['mandat_id']),
                    'method' => 'row'
                );

                $params_bail = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'order_by_columns' => 'bail_id DESC',
                    'method' => 'row'
                );
                $data['bail'] = $this->Bail_m->get($params_bail);
                $data['mandat'] = $this->MandatNew_m->get($params);
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
                $data['date_now'] = date('Y-m-d');
            }

            $this->renderComponant("Clients/clients/lot/mandats/retract-mandat", $data);
        }
    }

    public function RetractMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id']);
                $mandat_id = $data['mandat_id'];
                $cliLot_id = $data['cliLot_id'];
                $data['etat_mandat_id'] = 5;
                $data['mandat_fichier_justificatif'] = NULL;
                $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data['mandat_date_action_cloture'] = date('Y-m-d :H:i:s');

                if (!empty($_FILES)) {
                    $target_dir = "../documents/clients/lots/mandat/$cliLot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (file_exists($path_filename_ext)) {
                        $i = 1;
                        $new_filename = $filename . "_" . $i . "." . $ext;
                        $new_path_filename_ext = $target_dir . $new_filename;
                        while (file_exists($new_path_filename_ext)) {
                            $i++;
                            $new_filename = $filename . "_" . $i . "." . $ext;
                            $new_path_filename_ext = $target_dir . $new_filename;
                        }
                        $path_filename_ext = $new_path_filename_ext;
                    }
                    move_uploaded_file($temp_name, $path_filename_ext);
                    $data['mandat_fichier_justificatif'] = $path_filename_ext;
                }

                $clause = array("mandat_id" => $mandat_id);
                unset($data['bail_id']);
                unset($data['pdl_id']);
                unset($data['bail_loyer_variable']);
                $request = $this->MandatNew_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Mandat rétracté avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    /********Retracter mandat**********/

    public function suspensionMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $this->load->model('TypeClotureMandat_m', 'cloture');
                $this->load->model('Bail_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'columns' => array('mandat_id', 'mandat_date_signature'),
                    'clause' => array('mandat_id' => $data_post['mandat_id']),
                    'method' => 'row'
                );

                $params_bail = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'order_by_columns' => 'bail_id DESC',
                    'method' => 'row'
                );
                $data['bail'] = $this->Bail_m->get($params_bail);
                $data['mandat'] = $this->MandatNew_m->get($params);
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
                $data['date_now'] = date('Y-m-d');
            }

            $this->renderComponant("Clients/clients/lot/mandats/suspendre-mandat", $data);
        }
    }

    public function SuspendreMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id']);
                    $mandat_id = $data['mandat_id'];
                    $data['etat_mandat_id'] = 7;
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                    $data['mandat_date_supension'] = date('Y-m-d :H:i:s');
                    $bail_id = $data['bail_id'];
                    $pdl_id = $data['pdl_id'];
                    $bail_loyer_variable = $data['bail_loyer_variable'];

                    $clause = array("mandat_id" => $mandat_id);
                    unset($data['bail_id']);
                    unset($data['pdl_id']);
                    unset($data['bail_loyer_variable']);
                    $request = $this->MandatNew_m->save_data($data, $clause);

                    if (!empty($request)) {
                        $retour = array(
                            'status' => true,
                            'type' => "success",
                            'client_id' => $data['client_id'],
                            'cliLot_id' => $data['cliLot_id'],
                            'bail_id' => $bail_id,
                            'pdl_id' => $pdl_id,
                            'bail_loyer_variable' => $bail_loyer_variable,
                            'message' => "Mandat suspendu avec succès"
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /********Annuler suspension Mandat**********/

    public function annulersuspensionMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $this->load->model('TypeClotureMandat_m', 'cloture');
                $this->load->model('Bail_m');
                $data_post = $_POST["data"];
                $data = array();

                $params = array(
                    'clause' => array('mandat_id' => $data_post['mandat_id']),
                    'method' => 'row',
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id  = c_mandat_new.util_id'),
                );

                $params_bail = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'order_by_columns' => 'bail_id DESC',
                    'method' => 'row'
                );
                $data['bail'] = $this->Bail_m->get($params_bail);
                $data['mandat'] = $this->MandatNew_m->get($params);
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['client_id'] = $data_post['client_id'];
            }

            $this->renderComponant("Clients/clients/lot/mandats/annuler-suspension", $data);
        }
    }

    public function AnnulerSuspension_Mandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("client_id" => $data['client_id'], "cliLot_id" => $data['cliLot_id']);
                    $mandat_id = $data['mandat_id'];
                    $data['etat_mandat_id'] = 3;
                    $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                    $data['mandat_date_supension'] = date('Y-m-d :H:i:s');
                    $bail_id = $data['bail_id'];
                    $pdl_id = $data['pdl_id'];
                    $bail_loyer_variable = $data['bail_loyer_variable'];

                    $clause = array("mandat_id" => $mandat_id);
                    unset($data['bail_id']);
                    unset($data['pdl_id']);
                    unset($data['bail_loyer_variable']);
                    $request = $this->MandatNew_m->save_data($data, $clause);

                    if (!empty($request)) {
                        $retour = array(
                            'status' => true,
                            'type' => "success",
                            'client_id' => $data['client_id'],
                            'cliLot_id' => $data['cliLot_id'],
                            'bail_id' => $bail_id,
                            'pdl_id' => $pdl_id,
                            'bail_loyer_variable' => $bail_loyer_variable,
                            'message' => "Suspension annulée avec succès"
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    public function FormUploadMandat()
    {

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];
        $data['client_id'] = $data_post['client_id'];
        $this->renderComponant('Clients/clients/lot/mandats/formulaire_upload_file', $data);
    }

    public function uploadfileMandat()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/mandat/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_mandat_cli_nom' => $original_name,
                'doc_mandat_cli_creation' => date('Y-m-d H:i:s'),
                'doc_mandat_cli_path' => str_replace('\\', '/', $filePath),
                'doc_mandat_cli_etat' => 1,
                'doc_mandat_cli_type' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id
            );

            $this->load->model('DocMandatClient_m');
            $this->DocMandatClient_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getPathMandat()
    {

        $this->load->model('DocMandatClient_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_mandat_cli_id', 'doc_mandat_cli_path'),
                    'clause' => array('doc_mandat_cli_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getPath()
    {

        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('mandat_id', 'mandat_path'),
                    'clause' => array('mandat_id' => $data_post['id_doc'])
                );

                $request = $this->MandatNew_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getPathFicJustificatif()
    {

        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('mandat_id', 'mandat_fichier_justificatif'),
                    'clause' => array('mandat_id' => $data_post['id_doc'])
                );

                $request = $this->MandatNew_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function removeMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('MandatCli_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('mandat_cli_id' => $data['mandat_cli_id']),
                    'columns' => array('mandat_cli_id'),
                    'method' => "row",
                );
                $request_get = $this->MandatCli_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['mandat_cli_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce mandat ?",
                            'btnConfirm' => "Supprimer le mandat",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('mandat_cli_id' => $data['mandat_cli_id']);
                    $data_update = array('mandat_cli_etat' => 0);
                    $request = $this->MandatCli_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['mandat_cli_id']),
                            'message' => "document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDocMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocMandatClient_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_mandat_cli_id' => $data['doc_mandat_cli_id']),
                    'columns' => array('doc_mandat_cli_id'),
                    'method' => "row",
                );
                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_mandat_cli_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_mandat_cli_id' => $data['doc_mandat_cli_id']);
                    $data_update = array('doc_mandat_cli_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_mandat_cli_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*******Bail**********/

    public function listeDocumentBail()
    {
        $this->load->model('PeriodiciteLoyer_m');
        $this->load->model('TypeBail_m');
        $this->load->model('NaturePriseEffet_m');
        $this->load->model('TypeEcheance_m');
        $this->load->model('IndiceLoyer_m');
        $this->load->model('PeriodeIndexation_m');
        $this->load->model('MomentFacturation_m');
        $this->load->model('BailArt_m');
        $this->load->model('ClientLot_m');
        $this->load->model('IndiceValeur_m');
        $this->load->model('IndexationLoyer_m');
        $this->load->model('PeriodeMensuel_m');
        $this->load->model('MandatNew_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $params = array(
                    'clause' => array('cliLot_id ' => $data_post['lot_client_id']),
                    'method' => "row",
                );
                $data['lot'] = $this->ClientLot_m->get($params);
                $data['mandat'] = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'method' => 'row',
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id  = c_mandat_new.util_id'),
                        'join_orientation' => 'left'
                    )
                );

                $param_periodicite = array(
                    'order_by_columns' => "pdl_valeur DESC",
                );

                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['periodicite_loyer'] = $this->PeriodiciteLoyer_m->get($param_periodicite);
                $data['type_bail'] = $this->TypeBail_m->get(array());
                $data['nature_prise_effet'] = $this->NaturePriseEffet_m->get(array());
                $data['type_echeance'] = $this->TypeEcheance_m->get(array());
                $data['indice_loyer'] = $this->IndiceLoyer_m->get(array());
                $data['periode_indexation'] = $this->PeriodeIndexation_m->get(array());
                $data['moment_facturation'] = $this->MomentFacturation_m->get(array());
                $data['bail_art'] = $this->BailArt_m->get(array());
                $data['document'] = $this->getDocumentBail();

                $param_periode_mens = array(
                    'limit' => 12,
                );

                $data['periode_mensuel'] = $this->PeriodeMensuel_m->get($param_periode_mens);

                $this->load->model('Bail_m');
                $paramsBail = array(
                    'clause' => array('c_bail.cliLot_id' => $data_post['lot_client_id']),
                    'join' => array('c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',),
                    'method' => 'row'
                );
                $bail = $this->Bail_m->get($paramsBail);

                $indice_valeur_loyer = null;
                if ($bail) {
                    $data['tva_lot'] = $bail->tva;

                    $indice_valeur_loyer = $this->IndiceValeur_m->orderAllByDateParution();

                    $data['bail_id'] = $bail->bail_id;

                    // Get loyer de reference
                    $paramIndexation = array(
                        'clause' => array(
                            'bail_id ' => $bail->bail_id,
                            'indlo_ref_loyer' => 1
                        ),
                        'method' => "row"
                    );

                    $data['indexation_ref'] = $this->IndexationLoyer_m->get($paramIndexation);
                }
                $data['indice_valeur_loyer'] = $indice_valeur_loyer;

                $params_check_siret_tva = array(
                    'clause' => array('cliLot_id ' => $data_post['lot_client_id']),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    ),
                    'method' => "row",
                );
                $data['check_siret_tva'] = $this->ClientLot_m->get($params_check_siret_tva);

                $this->renderComponant("Clients/clients/lot/bails/list-bails", $data);
            }
        }
    }

    public function getDocumentBail()
    {

        $this->load->model('DocumentBail_m');

        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $params = array(
            'columns' => array(
                'doc_bail_id',
                'doc_bail_nom',
                'doc_bail_path',
                'doc_bail_creation',
                'util_prenom',
                'c_utilisateur.util_id',
                'doc_bail_traitement',
                'doc_bail_etat',
                'doc_bail_traite',
                'prenom_util',
                'doc_protocole'
            ),
            'clause' => array(
                'cliLot_id' => $data_post['cliLot_id'],
                'doc_bail_etat' => 1
            ),
            'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_bail.util_id')
        );

        $request = $this->DocumentBail_m->get($params);
        return $request;
    }

    public function getpathDocbail()
    {
        $this->load->model('DocumentBail_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_bail_id', 'doc_bail_path'),
                    'clause' => array('doc_bail_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentBail_m->get($params);
                echo json_encode($request);
            }
        }
    }

    /*******Loyer**********/
    public function listeDocumentLoyer()
    {
        $this->load->model('DocumentLoyer_m');
        $this->load->model('ClientLot_m');
        $this->load->model('MandatNew_m');
        $this->load->model('Bail_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array(
                        'doc_loyer_id',
                        'doc_loyer_nom',
                        'doc_loyer_path',
                        'doc_loyer_creation',
                        'util_prenom',
                        'c_utilisateur.util_id',
                        'doc_loyer_traitement',
                        'doc_loyer_etat',
                        'doc_loyer_traite',
                        'prenom_util'
                    ),
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_loyer_etat' => 1,
                        'doc_loyer_annee' => date("Y")
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_loyer.util_id')
                );

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $paramLotClient = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                    ),
                    'columns' => array(
                        'typelot_libelle',
                        'cliLot_id',
                    ),
                    'method' => 'row',
                    'join' => array('c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id')
                );

                $data['mandat'] = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id  = c_mandat_new.util_id'),
                        'join_orientation' => 'left',
                        'method' => 'row'
                    )
                );

                $request_date = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'order_by_columns' => 'mandat_date_signature DESC',
                        'method' => 'result'
                    )
                );

                $data['date_signature'] = $request_date;

                if (!empty($data['date_signature'])) {
                    foreach ($data['date_signature'] as $key => $value) {
                        $date[] = array(
                            'annee' => $value->mandat_date_signature
                        );
                    }
                    $data['max_date'] = max($date);
                }

                $data['lot_info'] = $this->ClientLot_m->get($paramLotClient);

                $data['document'] = $this->DocumentLoyer_m->get($params);

                $bail = $this->Bail_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    )
                );

                if (!empty($bail)) {
                    $minYear = PHP_INT_MAX;
                    $maxYear = PHP_INT_MIN;

                    foreach ($bail as $item) {
                        $startYear = intval(date('Y', strtotime($item->bail_debut)));
                        $endYear = intval(date('Y', strtotime($item->bail_fin)));

                        if ($startYear < $minYear) {
                            $minYear = $startYear;
                        }

                        if ($endYear > $maxYear) {
                            $maxYear = $endYear;
                        }
                    }
                    $data['filtre_array'][0] = $minYear;
                    $data['filtre_array'][1] = $maxYear;
                }
                $this->renderComponant("Clients/clients/lot/loyers/list-loyers", $data);
            }
        }
    }

    public function formUploadDocLoyer()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];
        $data['annee'] = $data_post['annee'];
        $this->renderComponant('Clients/clients/lot/loyers/form_upload_loyer', $data);
    }

    public function uploadfileLoyer()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $annee = $_POST['annee'];
        $cliLot_id = $_POST['cliLot_id'];
        $fact_loyer_id = isset($_POST['fact_loyer_id']) ? $_POST['fact_loyer_id'] : null;

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/Loyer/$annee/$cliLot_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_loyer_nom' => $original_name,
                'doc_loyer_creation' => date('Y-m-d H:i:s'),
                'doc_loyer_path' => str_replace('\\', '/', $filePath),
                'doc_loyer_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
                'doc_loyer_annee' => $annee,
                'fact_loyer_id ' => $fact_loyer_id
            );

            $this->load->model('DocumentLoyer_m');
            $this->DocumentLoyer_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getPathLoyer()
    {
        $this->load->model('DocumentLoyer_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_loyer_id', 'doc_loyer_path'),
                    'clause' => array('doc_loyer_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentLoyer_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getInfoProgramme()
    {
        $this->load->model('Program_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['progm_id'] = $data_post['progm_id'];

                $request_progrmme_info = $this->Program_m->get(
                    array(
                        'clause' => array('progm_id' => $data_post['progm_id']),
                        'method' => 'row',
                    )
                );

                $data_retour = $request_progrmme_info;

                $retour = retour(true, "success", $data_retour, array("message" => "Info programme"));

                echo json_encode($retour);
            }
        }
    }

    /**********Upload doc lot************/

    public function formUploadDocLot()
    {
        $this->renderComponant('Clients/clients/lot/identification_lot/formUploadFileLot');
    }

    public function removeLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('ClientLot_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cliLot_id' => $data['cliLot_id']),
                    'columns' => array('cliLot_id'),
                    'method' => "row",
                );
                $request_get = $this->ClientLot_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cliLot_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce lot ? ",
                            'btnConfirm' => "Supprimer le lot",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cliLot_id' => $data['cliLot_id']);
                    $data_update = array('cliLot_etat' => 0);
                    $request = $this->ClientLot_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cliLot_id']),
                            'message' => "La tâche a bien été supprimée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function addCredentialsClients()
    {
        $this->load->model('Client_m');
        $this->load->model('AccessProprietaire_m');
        $this->load->model('MotDePasseFruit_m');


        $clients = $this->Client_m->get(array());
        foreach ($clients as $key => $client) {
            $paramsAccess = array(
                'clause' => array('client_id ' => $client->client_id),
                'join' => array(
                    'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire.mtpfr_id'
                ),
                'method' => 'row'
            );
            $infoCnx = $this->AccessProprietaire_m->get($paramsAccess);

            if (!isset($infoCnx)) {
                $newPasswordData = $this->MotDePasseFruit_m->getRandomPasswordFuit();
                $separator = (isset($client->client_prenom) && $client->client_prenom != "") ? '.' : '';
                $login = $client->client_prenom . $separator . $client->client_nom;

                $count = $this->AccessProprietaire_m->isExist($login);
                if ($count > 0)
                    $login .= $count;

                $data_access["client_id"] = $client->client_id;
                $data_access["mtpfr_id"] = $newPasswordData->mtpfr_id;
                $data_access["accp_login"] = $login;
                $this->AccessProprietaire_m->save_data($data_access);
            }
        }
    }

    public function removefilecin()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m');
                $data_post = $_POST['data'];
                $param = array('client_id' => $data_post['client_id']);
                $data_client = array(
                    'cin' => NULL,
                    'etat_cin' => 0
                );
                $request = $this->Client_m->save_data($data_client, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }


    public function removefilecin2()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m');
                $data_post = $_POST['data'];
                $param = array('client_id' => $data_post['client_id']);
                $data_client = array(
                    'cin2' => NULL,
                );
                $request = $this->Client_m->save_data($data_client, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    function updateDateSignatureMandatClient()
    {
        $this->load->model('MandatCli_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la mise à jour",
                );

                $data = decrypt_service($_POST["data"]);

                $post['mandat_cli_date_signature'] = $data['mandat_cli_date_signature'];
                $clause = array("cliLot_id" => $data['cliLot_id']);

                $success = $this->MandatCli_m->save_data($post, $clause);

                if ($success) {
                    $retour = array(
                        'status' => 200,
                        'data' => array('cliLot_id' => $data['cliLot_id']),
                        'message' => "Prospect transformé avec succès",
                    );
                }
                echo json_encode($retour);
            }
        }
    }

    /******Generate doc locataire exploitant***/

    public function genererDocExploitant()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model("DocumentLot_m");
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la génération",
                );

                $data = decrypt_service($_POST["data"]);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cliLot_id'],
                            'title' => "Confirmation de la génération ",
                            'text' => "Voulez-vous vraiment générer ce document ?",
                            'btnConfirm' => "Générer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $data = array(
                        'doc_date_creation' => date('Y-m-d'),
                        'cliLot_id' => $data['cliLot_id']
                    );
                    $request = $this->DocumentLot_m->save_data($data);

                    $genererPdf = $this->genererpdfLocataire($data['cliLot_id']);
                    if ($genererPdf == true) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cliLot_id']),
                            'message' => "document généré avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function genererpdfLocataire($cliLot_id)
    {

        $this->load->model("DocumentLot_m");

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");

        $data = array();
        $params = array(
            'clause' => array('c_document_locataire.cliLot_id' => $cliLot_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_document_locataire.cliLot_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id',
                'c_programme' => 'c_lot_client.progm_id = c_programme.progm_id'
            ),
            'method' => "row",
            'join_orientation' => 'left'

        );

        $data['lot'] = $this->DocumentLot_m->get($params);

        $mpdf = new \Mpdf\Mpdf();

        $html = $this->load->view("Clients/clients/lot/exportDocLot/doc_exploitant", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $filename = "Locataire_Exploitant";
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/locataire/$cliLot_id";
        $url_doc = "../documents/clients/lots/locataire/$cliLot_id/$url";

        $this->makeDirPath($url_path);

        $clause = array("cliLot_id" => $cliLot_id);

        $data = array(
            'doc_lot_path' => str_replace('\\', '/', $url_doc),
        );

        $request = $this->DocumentLot_m->save_data($data, $clause);

        $url_file = $url_path . "/" . $filename . ".pdf";

        $mpdf->Output($url_file, 'F');

        return true;
    }

    /******Generate doc syndic***/

    public function genererDocSyndic()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model("DocumentSyndic_m");
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la génération",
                );

                $data = decrypt_service($_POST["data"]);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cliLot_id'],
                            'title' => "Confirmation de la génération ",
                            'text' => "Voulez-vous vraiment générer ce document ?",
                            'btnConfirm' => "Générer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $data = array(
                        'doc_syndic_date_creation' => date('Y-m-d'),
                        'cliLot_id' => $data['cliLot_id']
                    );
                    $request = $this->DocumentSyndic_m->save_data($data);

                    $genererPdf = $this->genererpdfSyndic($data['cliLot_id']);
                    if ($genererPdf == true) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cliLot_id']),
                            'message' => "document généré avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function genererpdfSyndic($cliLot_id)
    {

        $this->load->model("DocumentSyndic_m");

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");

        $data = array();
        $params = array(
            'clause' => array('c_document_syndic.cliLot_id' => $cliLot_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_document_syndic.cliLot_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id',
                'c_programme' => 'c_lot_client.progm_id = c_programme.progm_id'
            ),
            'method' => "row",
            'join_orientation' => 'left'

        );

        $data['syndic'] = $this->DocumentSyndic_m->get($params);

        $mpdf = new \Mpdf\Mpdf();

        $html = $this->load->view("Clients/clients/lot/exportDocLot/doc_syndic", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $filename = "Syndic";
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/syndic/$cliLot_id";
        $url_doc = "../documents/clients/lots/syndic/$cliLot_id/$url";

        $this->makeDirPath($url_path);

        $clause = array("cliLot_id" => $cliLot_id);

        $data = array(
            'doc_syndic_path' => str_replace('\\', '/', $url_doc),
        );

        $request = $this->DocumentSyndic_m->save_data($data, $clause);

        $url_file = $url_path . "/" . $filename . ".pdf";

        $mpdf->Output($url_file, 'F');

        return true;
    }

    /******Generate doc sie***/

    public function genererDocSie()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model("DocumentSie_m");
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la génération",
                );

                $data = decrypt_service($_POST["data"]);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cliLot_id'],
                            'title' => "Confirmation de la génération ",
                            'text' => "Voulez-vous vraiment générer ce document ?",
                            'btnConfirm' => "Générer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $data = array(
                        'doc_sie_date_creation' => date('Y-m-d'),
                        'cliLot_id' => $data['cliLot_id']
                    );
                    $request = $this->DocumentSie_m->save_data($data);

                    $genererPdf = $this->genererpdfSie($data['cliLot_id']);
                    if ($genererPdf == true) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cliLot_id']),
                            'message' => "document généré avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function genererpdfSie($cliLot_id)
    {

        $this->load->model("DocumentSie_m");

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");

        $data = array();
        $params = array(
            'clause' => array('c_document_sie.cliLot_id' => $cliLot_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_document_sie.cliLot_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id',
                'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                'c_sie' => 'c_sie.sie_id = c_dossier.sie_id'
            ),
            'method' => "row",
            'join_orientation' => 'left'

        );

        $data['sie'] = $this->DocumentSie_m->get($params);

        $mpdf = new \Mpdf\Mpdf();

        $html = $this->load->view("Clients/clients/lot/exportDocLot/doc_sie", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $filename = "Service_des_Impôts_des_Entreprises";
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/sie/$cliLot_id";
        $url_doc = "../documents/clients/lots/sie/$cliLot_id/$url";

        $this->makeDirPath($url_path);

        $clause = array("cliLot_id" => $cliLot_id);

        $data = array(
            'doc_sie_path' => str_replace('\\', '/', $url_doc),
        );

        $request = $this->DocumentSie_m->save_data($data, $clause);

        $url_file = $url_path . "/" . $filename . ".pdf";

        $mpdf->Output($url_file, 'F');

        return true;
    }

    /******Generate doc sip***/

    public function genererDocSip()
    {

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model("DocumentSip_m");
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la génération",
                );

                $data = decrypt_service($_POST["data"]);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cliLot_id'],
                            'title' => "Confirmation de la génération ",
                            'text' => "Voulez-vous vraiment générer ce document ?",
                            'btnConfirm' => "Générer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $data = array(
                        'doc_sip_date_creation' => date('Y-m-d'),
                        'cliLot_id' => $data['cliLot_id']
                    );
                    $request = $this->DocumentSip_m->save_data($data);

                    $genererPdf = $this->genererpdfSip($data['cliLot_id']);
                    if ($genererPdf == true) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cliLot_id']),
                            'message' => "document généré avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function genererpdfSip($cliLot_id)
    {

        $this->load->model("DocumentSip_m");

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");

        $data = array();
        $params = array(
            'clause' => array('c_document_sip.cliLot_id' => $cliLot_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_document_sip.cliLot_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id',
                'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                'c_sie' => 'c_sie.sie_id = c_dossier.sie_id'
            ),
            'method' => "row",
            'join_orientation' => 'left'

        );

        $data['sip'] = $this->DocumentSip_m->get($params);

        $mpdf = new \Mpdf\Mpdf();

        $html = $this->load->view("Clients/clients/lot/exportDocLot/doc_sip", $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $filename = "Service_des_Impôts_des_Particuliers";
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/sip/$cliLot_id";
        $url_doc = "../documents/clients/lots/sip/$cliLot_id/$url";

        $this->makeDirPath($url_path);

        $clause = array("cliLot_id" => $cliLot_id);

        $data = array(
            'doc_sip_path' => str_replace('\\', '/', $url_doc),
        );

        $request = $this->DocumentSip_m->save_data($data, $clause);

        $url_file = $url_path . "/" . $filename . ".pdf";

        $mpdf->Output($url_file, 'F');

        return true;
    }

    public function alert_Iban()
    {
        if (is_ajax()) {
            $this->renderComponant("Clients/clients/form-alert_iban");
        }
    }

    public function historiqueAdresse()
    {
        if (is_ajax()) {
            $this->load->model('HistoriqueAdresse_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('client_id' => $data_post['client_id']),
                    'order_by_columns' => 'hist_date_modif DESC'
                );

                $data['historique'] = $this->HistoriqueAdresse_m->get($params);
            }

            $this->renderComponant("Clients/clients/historique/form-historique-adresse", $data);
        }
    }

    public function getDocCopropriete()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCopropriete_m');

                $data['cliLot_id'] = $_POST['data']['cliLot_id'];
                $data['document'] = $this->DocumentCopropriete_m->get(array(
                    'clause' => array('cliLot_id' => $_POST['data']['cliLot_id'], 'doc_etat' => 1, 'comment_etat' => 0),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_copropriete.util_id')
                ));

                $data['commentairecopropriete'] = $this->DocumentCopropriete_m->get(array(
                    'clause' => array('cliLot_id' => $_POST['data']['cliLot_id'], 'doc_etat' => 1, 'comment_etat' => 1),
                    'method' => 'row'
                ));

                $this->renderComponant("Clients/clients/lot/copropriete/list_document_copropriete", $data);
            }
        }
    }

    public function formUploadCopropriete()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST['data'];

                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->renderComponant("Clients/clients/lot/copropriete/form-upload-copropriete", $data);
            }
        }
    }

    public function uploadfileCopropriete()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id_copropriete'];

        // Chemin du document
        $targetDir = "../documents/clients/lot/doc_copropriete/$cliLot_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_nom' => $original_name,
                'doc_date_creation' => date('Y-m-d H:i:s'),
                'doc_path' => str_replace('\\', '/', $filePath),
                'doc_etat' => 1,
                'util_id ' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
            );

            $this->load->model('DocumentCopropriete_m');
            $this->DocumentCopropriete_m->save_data($data);

            echo json_encode($data);
        }
    }

    public function deleteDocCopropriete()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentCopropriete_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_id' => $data['doc_id']),
                    'columns' => array('doc_id'),
                    'method' => "row",
                );

                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_id' => $data['doc_id']);
                    $data_update = array('doc_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modalupdateDocCopropriete()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $this->load->model('DocumentCopropriete_m');
                $data['document'] = $this->DocumentCopropriete_m->get(
                    array('clause' => array("doc_id" => $data_post['doc_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Clients/clients/lot/copropriete/form-update-copropriete", $data);
        }
    }

    public function UpdateDocumentCopropriete()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCopropriete_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array(
                        "cliLot_id" => $data['cliLot_id'],
                    );
                    $doc_id = $data['doc_id'];
                    unset($data['doc_id']);

                    $clause = array("doc_id" => $doc_id);
                    $request = $this->DocumentCopropriete_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function addModifcommentaireDocCopropriete()
    {
        $this->load->model('DocumentCopropriete_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $retour = retour(null, null, null, array("message" => "Erreur lors de la modification."));

                $check_commentarie = $this->DocumentCopropriete_m->get(
                    array(
                        'clause' => array('cliLot_id' => $data_post['cliLot_id'], 'comment_etat' => 1),
                        'method' => 'row'
                    )
                );

                $data_retour = $data_save = array(
                    'doc_etat' => 1,
                    'comment_etat' => 1,
                    'commentaire' => $data_post['commentaire'],
                    'cliLot_id' => $data_post['cliLot_id'],
                    'util_id' => $_SESSION['session_utilisateur']['util_id']
                );

                if (!empty($check_commentarie)) {
                    $clause = array("doc_id" => $check_commentarie->doc_id);
                    $request = $this->DocumentCopropriete_m->save_data($data_save, $clause);
                } else {
                    $request = $this->DocumentCopropriete_m->save_data($data_save);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement reussie."));
                }

                echo json_encode($retour);
            }
        }
    }

    public function getpathDocCopropriete()
    {
        $this->load->model('DocumentCopropriete_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_id', 'doc_path'),
                    'clause'  => array('doc_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentCopropriete_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function Modifier_restant_du()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pret_m');
                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                $data['getPret'] = $this->Pret_m->get(
                    array(
                        'clause' => array('cliLot_id' => $_POST['data']['cliLot_id'], 'pret_annee' => $date),
                        'method' => 'row'
                    )
                );

                $data['cliLot_id'] = $_POST['data']['cliLot_id'];

                $this->renderComponant("Clients/clients/lot/prets/formModifPret", $data);
            }
        }
    }

    public function updateMontantdu()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pret_m');
                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = array("pret_annee" => $date, "cliLot_id" => $data['cliLot_id']);
                    $data_save = array(
                        'pret_capital_restant_du' => $data['pret_capital_restant_du']
                    );
                    $request = $this->Pret_m->save_data($data_save, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => "La modification a été réussi avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    /****** ******/

    function checked_identity(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = retour(false, "error", 0, array("message" => "Erreur de la modification"));
                $data_post = $_POST['data'];
                $this->load->model('Client_m');

                $clause = array('client_id' => $data_post['client_id']);
                $data = array(
                    'check_identite' => ($data_post['isChecked'] == 1) ? 1 : 0,
                );
                $request = $this->Client_m->save_data($data, $clause);

                if($request){
                    $retour = retour(true, "success",array('content'=>$data_post['content']), array("message" => "La modification a été réussi avec succès."));

                    $this->load->model('HistoriqueVerifieIdentite_m');
                    $data_historique = array(
                        'histvi_date' => date('Y-m-d H:i'),
                        'histvi_action' => $data['check_identite'],
                        'util_id' => $_SESSION['session_utilisateur']['util_id'],
                        'client_id'=> $data_post['client_id']
                    );
                    $this->HistoriqueVerifieIdentite_m->save_data($data_historique, null);
                }

                echo json_encode($retour);
            }
        }
    }
}
