<div class="container-app vue_limit_access">
	<input type="number" disabled value="<?php echo $this->session->userdata['session_utilisateur']['util_id']; ?>"
		id="util_id" hidden />

	<?php $this->load->view($sous_module); ?>
</div>

<script>
	access_right([1, 3, 4, 5], "*");
</script>