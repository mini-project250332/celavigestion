<div id="contenair-client" class="contenair-main">



</div>

<!-- Expert comptable -->

<div class="modal fade" id="myModal_tableau_assignerExpertComptable" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content" id="modal-main-content-tableau_assignerExpertComptable" style="overflow-y: auto;">

        </div>
    </div>
</div>


<div class="modal fade" id="myModal_assignerExpertComptable" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content" id="modal-main-content-assignerExpertComptable" style="overflow-y: auto;">

        </div>
    </div>
</div>

<div class="modal fade" id="myModal_resilierMission" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content" id="modal-main-content-resilierMission" style="overflow-y: auto;">

        </div>
    </div>
</div>


<div class="modal fade" id="myModal_modif_affectationMission" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content" id="modal-main-content-modif_affectationMission" style="overflow-y: auto;">

        </div>
    </div>
</div>

<!-- -->

<div class="modal fade" id="modalFormCloture" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-cloture" style="width:600px !important;">

        </div>
    </div>
</div>

<div class="modal fade" id="modalForm" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-content" style="width:600px !important;">

        </div>
    </div>
</div>

<div class="modal fade" id="modalFormAlert_Siret" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-alertSiretTva">

        </div>
    </div>
</div>

<div class="modal fade" id="FormModal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content" id="modalContent">
        </div>
    </div>
</div>

<div class="modal fade" id="ModalFormLg" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modalContentLg" style=" height:770px !important;">

        </div>
    </div>
</div>

<div class="modal fade" id="modalMailHistoryContent" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div id="contain_tab_mail_historique_loyer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormLot" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content" id="modal-main-content-lot">

        </div>
    </div>
</div>

<div class="modal fade" id="reglmt_modal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-reglmt_modal">

        </div>
    </div>
</div>

<div class="modal fade" id="myModalValiderFactureAvoir" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="modalMainFactureAvoir">

        </div>
    </div>
</div>

<div class="modal fade" id="modalValidationReference" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content" id="LoyerReference">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_valid_transform">
                    Confirmation de changement de valeur
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">

                    </div>
                    <input type="hidden" id="bail_id_reference">

                    <div class="col-10">
                        <p class="text-center fs--1 m-0">Attention ! Si vous changez cette information, les indexations seront supprimées et vous devrez régénérer l'indexation avec le bouton "Générer les indexations de loyer.<br><br> <br>
                            Voulez-vous vraiment continuer cette opération ?
                        </p>
                    </div>
                    <div class="col">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="confirmer_changement_reference" class="btn btn-success" data-bs-dismiss="modal">Confirmer</button>
                <button type="button" id="annuler_changement_reference" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalValidationAvenant" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-valdiation">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_valid_transform">
                    Confirmation du nouveau avenant
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="annulerAvenant"></button>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250"> Date de la prise en compte :</label>
                                <input type="date" class="form-control auto_effect" id="bail_avenant_date_debut" value="<?= date('Y-m-d') ?>">
                                <input type="hidden" id="bail_id_avenant">
                                <input type="hidden" id="cliLot_id_avenant">

                                <input type="hidden" id="bail_date_prochaine_indexation">
                                <input type="hidden" id="bail_facturation_loyer_celaviegestion">
                                <input type="hidden" id="bail_forfait_charge_indexer">
                                <input type="hidden" id="bail_remboursement_tom">
                                <input type="hidden" id="bail_var_appliquee">
                                <input type="hidden" id="bail_var_capee">
                                <input type="hidden" id="bail_var_plafonnee">
                                <input type="hidden" id="gestionnaire_id">
                                <input type="hidden" id="indloyer_id">
                                <input type="hidden" id="indval_id">
                                <input type="hidden" id="momfac_id">
                                <input type="hidden" id="natef_id">
                                <input type="hidden" id="pdl_id">
                                <input type="hidden" id="prdind_id">
                                <input type="hidden" id="premier_mois_trimes">
                                <input type="hidden" id="preneur_id">
                                <input type="hidden" id="tpba_id">
                                <input type="hidden" id="tpech_id">
                                <input type="hidden" id="premier_mois_trimes">
                                <input type="hidden" id="bail_art_605">
                                <input type="hidden" id="bail_art_606">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="confirmerAvenant" class="btn btn-success" data-bs-dismiss="modal">Confirmer</button>
                <button type="button" id="annulerAvenant" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalValidationbailcloture" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-valdiation">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_valid_transform">
                    Confirmation de la clôture d'un bail
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="annulerAvenant"></button>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250"> Date de la clôture :</label>
                                <input type="date" class="form-control auto_effect" id="bail_date_cloture" value="<?= date('Y-m-d') ?>">
                                <input type="hidden" id="bail_id_cloture">
                                <input type="hidden" id="cliLot_id_cloture">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="confirmerCloture" class="btn btn-success" data-bs-dismiss="modal">Confirmer</button>
                <button type="button" id="annulerCloture" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalValidationProtocol" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-modal_protocol">

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        <?php if (isset($client_id) && strpos($_SERVER['REQUEST_URI'], 'Clients/ficheClient?client=')) : ?>
            var data_request = {
                client_id: '<?= $client_id; ?>'
            };
            ficheClient(data_request);
        <?php else : ?>
            pgClient();
        <?php endif; ?>
    });
</script>