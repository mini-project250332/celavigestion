<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire modification client : VIGOUROUX Guillaume (N° XXXXXX)</h5>
	</div>
	<div class="px-2">
				
		<button type="button" id="btnAnnuler" class="btn btn-sm btn-secondary mx-1">
			<span class="bi bi-x me-1"></span>
			<span> Annuler </span>
		</button>
				
		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>			
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<div class="row m-0">
	<div class="col p-0">

		<ul class="nav nav-tabs" id="myTab" role="tablist">
  			<li class="nav-item">
  				<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
  			</li>
  			<li class="nav-item">
  				<a class="nav-link" id="dossier-tab" data-bs-toggle="tab" href="#tab-dossier" role="tab" aria-controls="tab-dossier" aria-selected="false">Dossiers</a>
  			</li>
			<li class="nav-item">
				<a class="nav-link" id="document-tab" data-bs-toggle="tab" href="#tab-document" role="tab" aria-controls="tab-document" aria-selected="false">Documents client</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="communication-tab" data-bs-toggle="tab" href="#tab-communication" role="tab" aria-controls="tab-communication" aria-selected="false">Communication</a>
			</li>
		</ul>

		<div class="tab-content border-x border-bottom p-3" id="myTabContent">
  			<div class="tab-pane fade show active" id="tab-identification" role="tabpanel" aria-labelledby="identification-tab">
  				<?php $this->load->view('identification'); ?>
  			</div>
  			<div class="tab-pane fade" id="tab-dossier" role="tabpanel" aria-labelledby="dossier-tab">
  				<?php $this->load->view('dossiers'); ?>
  			</div>

			<div class="tab-pane fade" id="tab-document" role="tabpanel" aria-labelledby="document-tab">
				<?php $this->load->view('documents'); ?>
			</div>

			<div class="tab-pane fade" id="tab-communication" role="tabpanel" aria-labelledby="communication-tab">
				<?php $this->load->view('communication'); ?>
			</div>
		</div>
	</div>
</div>
