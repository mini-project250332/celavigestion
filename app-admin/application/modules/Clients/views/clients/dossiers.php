<div class="contenair-title">
    <div class="row m-0">
        <div class="px-2">
            <h5 class="fs-1">Gestion des dossiers</h5>
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <button class="btn btn-sm btn-primary m-1" type="submit">
            <span class="fas fa-plus me-1"></span>			
            <span> Ajouter </span>
        </button>
    </div>
</div>
<div class="table-responsive scrollbar pt-2">
    <table class="table table-sm table-striped fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Numéro</th>
                <th scope="col">Nom dossier</th>
                <th scope="col">Nombre de lots</th>
                <th scope="col">Adresse programme</th>
                <th scope="col">Date création</th>
                <th class="text-end" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr class="align-middle">
                <td>1234</td>
                <td>VIGOUROUX MTP</td>
                <td>1</td>
                <td>28 rue xxx <br>
                    34000 MONTPELLIER
                </td>
                <td>11/05/2022</td>
                <td class="btn-action-table">
                    <div class="d-flex justify-content-end ">
                        <a href="#" class="p-0 m-0">
                            <button class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" type="button">
                                <span class="bi bi-pencil-square fs-1 m-1"></span>
                            </button>
                        </a>
                        <button class="btn btn-sm btn-outline-danger icon-btn " type="button">
                            <span class="bi bi-trash fs-1"></span>
                        </button>
                    </div>
                </td>
            </tr>
            <tr class="align-middle">
                <td>1235</td>
                <td>VIGOUROUX XXX</td>
                <td>2</td>
                <td>36 rue xxx <br>
                    34000 MONTPELLIER
                </td>
                <td>11/05/2022</td>
                <td class="btn-action-table">
                    <div class="d-flex justify-content-end ">
                        <a href="#" class="p-0 m-0">
                            <button class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" type="button">
                                <span class="bi bi-pencil-square fs-1 m-1"></span>
                            </button>
                        </a>
                        <button class="btn btn-sm btn-outline-danger icon-btn " type="button">
                            <span class="bi bi-trash fs-1"></span>
                        </button>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>