<div class="modal-header" style="background-color : #636E7E !important">
	<h5 class="modal-title text-white text-center">
		<?php if (isset($encaissement)) : ?>
			<?= $this->lang->line('title_formulaire_modif_encaissement') ?>
		<?php else : ?>
			<?= $this->lang->line('title_formulaire_encaissement') ?>
		<?php endif; ?>
	</h5>
	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>

<?php echo form_tag('Clients/Encaissement/crudEncaissement', array('method' => "post", 'class' => '', 'id' => 'cruEncaissement')); ?>

<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
<input type="hidden" name="cli_id" id="cliLot_id" value="<?= $cli_id; ?>">

<?php if (isset($encaissement)) : ?>
	<input type="hidden" name="enc_id" id="enc_id" value="<?= $encaissement->enc_id; ?>">
<?php endif; ?>

<div class="modal-body">

	<div class="row m-0">
		<div class="col py-1">

			<div class="form-group inline">
				<div class="row m-0">
					<label class="col-md-4" data-width="200"><?= $this->lang->line('montant_encaissement') ?> * : </label>
					<div class="col input-group">
						<input type="text" placeholder="0" class="form-control mb-0 chiffre" id="enc_montant" name="enc_montant" style="text-align: right; height : 36px;" <?= (isset($encaissement->enc_montant)) ? 'value="' . $encaissement->enc_montant . '"' : ""; ?>>
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2">€ TTC</span>
						</div>
					</div>
					<div class="help mt-1"></div>
				</div>
			</div>

			<div class="form-group inline">
				<div class="row m-0">
					<label data-width="200"> <?= $this->lang->line('date_encaissement') ?> * : </label>
					<div class="col input-group">
						<input type="date" class="form-control" id="enc_date" name="enc_date" <?= (isset($encaissement->enc_date)) ? 'value="' . date("Y-m-d", strtotime($encaissement->enc_date)) . '"' : ""; ?>>
					</div>
					<div class="help mt-1"></div>
				</div>
			</div>

			<div class="form-group inline">
				<div class="row m-0">
					<label class="col-md-4" data-width="200"><?= $this->lang->line('type_encaissement') ?> * : </label>
					<div class="col">
						<select class="form-select fs--1" id="typeEnc_id" name="typeEnc_id" style="left: 3px;">
							<?php if (!empty($type_enc)) : ?>
								<?php foreach ($type_enc as $value) : ?>
									<option value="<?= $value->typeEnc_id; ?>" <?= (isset($encaissement->type_enc) && $encaissement->type_enc == $value->typeEnc_id) ? 'selected' : ""; ?>>
										<?= $value->typeEnc_libelle; ?>
									</option>
								<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
					<div class="help"></div>
				</div>
			</div>

			<div class="form-group inline">
				<div class="row m-0">
					<label class="col-md-4" data-width="200"><?= $this->lang->line('fichier_encaissement') ?> :</label>
					<div class="col">
						<input class="form-control fs--1" type="file" id="fichier_encaissement" name="fichier_encaissement[]" multiple="multiple" style="line-height: 1.5!important;">
						<div class="help"></div>
					</div>
					<div class="help"></div>
				</div>
			</div>

			<div class="">
				<?php if (isset($document_enc)) : ?>
					<?php foreach ($document_enc as $key => $value) : ?>
						<div class="ms-10 doc_15 doc_encaissement_<?= $value->doc_encaissement_id; ?>" id="doc_encaissement_<?= $value->doc_encaissement_id; ?>">
							<span class="badge rounded-pill badge-soft-secondary" data-url="" title="Télécharger fichier" style="cursor: pointer;">
								<?= $value->doc_encaissement_nom; ?> </span>
							<span class="badge rounded-pill badge-soft-light btn-close text-white supprimer_ficher" data-cli_lot_id="<?= $cliLot_id; ?>" data-id="<?= $value->doc_encaissement_id; ?>" style="cursor: pointer;" title="Supprimer fichier">.</span>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<div class="form-group inline">
                <div class="row m-0">
                    <label class="col-md-4" data-width="200">Information : </label>
                    <div class="col">
                        <textarea id="enc_info" name="enc_info" class="form-control" style="min-height:100px;"><?=(isset($encaissement->enc_info)) ? $encaissement->enc_info : "";?></textarea>
                    </div>
                    <div class="help"></div>
                </div>
            </div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
		<button type="submit" class="btn btn-primary">Enregistrer</button>
	</div>
</div>
<?php echo form_close(); ?>
<script>
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y",
		defaultDate: moment().format('DD-MM-YYYY'),
	});
</script>