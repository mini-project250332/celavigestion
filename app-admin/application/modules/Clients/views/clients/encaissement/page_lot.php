<div class="col-12 w-100 h-100">
    <div class="row w-100">
        <div class="col">
            <h5 class="pt-2"><?= $this->lang->line('list_encaissement'); ?></h5>
        </div>
        <div class="col">
            <div class="row pt-2">
                <div class="col-3"></div>
                <div class="col-3 text-center">
                    <select class="form-select filtre-date-encaissement" name="filtre-date-<?= $cliLot_id; ?>" id="filtre-date-<?= $cliLot_id; ?>">
                        <?php for ($i = intval(date('Y') + 1); $i > 2021; $i--) : ?>
                            <option value="<?= $i ?>" <?= $i == intval(date('Y')) ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="col"></div>
            </div>
        </div>
        <div class="col"></div>
    </div>
    <hr>
</div>

<div id="liste-encaissement-lot_<?= $cliLot_id ?>"></div>

<script>
    $(document).ready(function() {
        getListeEncaissement(<?= $cliLot_id; ?>, <?= intval(date('Y')) ?>);
    });
</script>