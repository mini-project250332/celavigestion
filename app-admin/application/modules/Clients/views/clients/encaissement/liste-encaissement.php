
<?php
$lists_encaissement = array_filter($encaissements, function ($encaissement) {
    return $encaissement->enc_control == 0;
});

$cloture_encaissement = array_filter($encaissements, function ($encaissement) {
    return $encaissement->enc_control == 1;
});
$cloture_encaissement = array_values($cloture_encaissement);
?>

<div class="pannel_contain">
    <div class="d-flex m-0 panel-container">
        <div class="col-md-6 px-1 panel-left">
            <div class="d-flex flex-column pb-1">
                <div class="col pt-1">
                    <div class="row m-0">
                        <div class="col">
                            <div class="d-flex">
                                <span>
                                    Clôture de la saisie par le propriétaire: 
                                </span>
                                <?php if (!empty($cloture_encaissement)): ?>
            
                                            effectuée le  <?= date('d/m/Y', strtotime($cloture_encaissement[0]->enc_date_cloture)); ?>
            
                                            <button data-cliLot_id="<?= $cloture_encaissement[0]->clilot_id; ?>" data-enc_id="<?= $cloture_encaissement[0]->enc_id; ?>" class="btn btn-sm btn-danger ms-2 annuler_clotureEnc">
                                                <span class="fas fa-times"></span>
                                            </button>

                                <?php endif; ?>

                                <?php if (empty($cloture_encaissement)): ?>
                                            Non effectuée 
                                            <button data-cliLot_id="<?= $cliLot_id; ?>" data-annee="<?= $annee; ?>" class="btn  btn-sm btn-success valider_clotureEnc">
                                                <span class="fas fa-check"></span>
                                            </button>
                                <?php endif; ?>
                            </div>
                            
                            <?php if (!empty($cloture_encaissement)): ?>
                                        <div>
                                            <span style="font-weight: 500 !important;" class="mb-0 px-2">Commentaire :</span>
                                            <div class="alert alert-info border-2 p-2 mt-1" style="white-space: unset !important;" role="alert">
                                        
                                               <p class="m-0 text-break"><?= $cloture_encaissement[0]->enc_info; ?></p>
                                            </div>
                                        </div>      
                            <?php endif; ?>
                            
                        </div>
                        <div class="col-auto text-end">
                            <button data-id="<?= $cliLot_id; ?>" class="btn btn-outline-primary mb-2 btn_ajoutEncaissement">
                                <i class="fas fa-plus"></i>&nbsp;
                                <?= $this->lang->line('plus_encaissement') ?>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <table class="table table-bordered table-hover">
                                <thead class="fs--1 text-white" style="background:#1F659E;">
                                    <tr class="text-center">
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                            <?= $this->lang->line('date_encaissement') ?>
                                        </th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                            <?= $this->lang->line('type_encaissement') ?>
                                        </th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;" width="30%">
                                            <?= $this->lang->line('montant_encaissement') ?>
                                        </th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                            <?= $this->lang->line('fichier_encaissement') ?>
                                        </th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                            <?= $this->lang->line('titre-action') ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-200">
                                    <?php if (!empty($lists_encaissement)): ?>
                                                <?php foreach ($lists_encaissement as $key => $value): ?>
                                                            <tr class="align-middle">
                                                                <td class="text-center">
                                                                    <?= date('d/m/Y', strtotime($value->enc_date)); ?>
                                                                </td>
                                                                <td class="text-center">
                                                                    <span>
                                                                        <?= $value->typeEnc_libelle; ?>
                                                                    </span>
                                                                </td>
                                                                <td class="text-end">
                                                                    <span>
                                                                        <?= format_number_($value->enc_montant) . ' ' . '€'; ?>
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <div class="">
                                                                        <?php if (!empty($value->files)): ?>
                                                                                    <?php foreach ($value->files as $file): ?>
                                                                                                <i class="fas fa-cloud-download-alt text-info mb-1" onclick="forceDownload('<?= base_url() . $file->doc_encaissement_path ?>')" data-url="<?= base_url($file->doc_encaissement_path) ?>" style="cursor: pointer;"></i>
                                                                                                <span onclick="apercuDocEncaissement(<?= $file->doc_encaissement_id; ?>);" style="width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor: pointer;" class="badge badge-soft-info mt-1 py-1 fs--0 text-start doc_encaissement_<?= $file->doc_encaissement_id; ?>">
                                                                                                    <span style=""><?= $file->doc_encaissement_nom; ?></span>
                                                                                                </span>
                                                                                    <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center ">
                                                                    <div class="d-flex">
                                                                    <button class="btn btn-sm btn-outline-info icon-btn me-1 btn-info-enc" type="button" data-info="<?= $value->enc_info; ?>" data-langVide="">
                                                                            <span class="fas fa-info"></span>
                                                                        </button>
                                                                        <button data-id="<?= $value->enc_id; ?>" data-cli_lot_id="<?= $value->clilot_id; ?>" class="btn btn-sm btn-outline-primary icon-btn me-1 btn_editEncaissement" type="button" <?= ($value->enc_valide == 1) ? "disable" : ""; ?>>
                                                                            <span class="fas fa-pen"></span>
                                                                        </button>

                                                                        <button class="btn btn-sm btn-outline-danger icon-btn btn_supprimeEncaissement" type="button" data-id="<?= $value->enc_id; ?>" data-cli_lot_id="<?= $value->clilot_id; ?>" <?= ($value->enc_valide == 1) ? "disable" : ""; ?>>
                                                                            <span class="fas fa-trash-alt"></span>
                                                                        </button>

                                                       
                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="splitter m-2"></div>

        <div class="col-md-6 px-1 panel-right">
            <div id="apercu_list_docEnc_<?= $cliLot_id ?>">
                <div id="nonApercu" style="display:none;">
                    <div class="text-center text-danger pt-10">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        <?= $this->lang->line('apercu_doc'); ?>
                    </div>
                </div>
                <div id="apercuEnc_<?= $cliLot_id ?>">
                    <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                        <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                        <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                        <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-finSaisiEncaissement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-finSaisiEncaissement">

            <div class="modal-header">
                <div class="rounded-top-lg ps-4 pe-6">
                    <h5 class="mb-1 text-center-"  id="titre-modal-finSaisiEncaissement"></h5>
                </div>
            </div>

            <div class="modal-body p-0 pb-3">
                <div class="row m-0 ">
                    <div class="col-12 px-4 pt-4 pb-2 text-center">
                        <span id="text-modal-finSaisiEncaissement"></span>
                    </div>
                    <input type="hidden" id="confirm-cliLot_id">
                    <div class="col-12">
                        <div class="px-3">
                            <label class="col-auto">&nbsp;Commentaire : </label>
                        </div>
                        <div class="form-group inline">
                            <div class="row m-0">
                                <div class="col">
                                    <textarea id="confirm-enc_info" name="enc_info" style="min-height: 100px;" class="form-control"></textarea>
                                </div>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer py-2">
                <button type="button" id="btnConfirm-finSaisiEncaissement" class="btn btn-success d-flex">
                    Confirmer
                </button>
                <button type="button" id="btnAnnuler-finSaisiEncaissement" class="btn btn-secondary d-flex" data-bs-dismiss="modal">
                    Annuler
                </button>
            </div>

        </div>
    </div>
</div>