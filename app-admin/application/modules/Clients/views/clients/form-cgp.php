<?php echo form_tag('Setting/Cgp/cruCgp', array('method' => "post", 'class' => '', 'id' => 'AddCgp')); ?>

<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter un CGP</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php if (isset($cgp->cgp_id)) : ?>
	<input type="hidden" name="cgp_id" id="cgp_id" value="<?= $cgp->cgp_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">		
					<div class="form-group inline">
						<div>
							<label data-width="200"> Nom </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_nom) ? $cgp->cgp_nom : ""; ?>" id="cgp_nom" name="cgp_nom">
							<div class="help"></div>
						</div>
					</div>
                    <div class="form-group inline">
						<div>
							<label data-width="200"> Prénom </label>
							<input type="text" data-type="email" class="form-control" value="<?= isset($cgp->cgp_prenom) ? $cgp->cgp_prenom : ""; ?>" id="cgp_prenom" name="cgp_prenom">
							<div class="help"></div>
						</div>
					</div>
                    <div class="form-group inline">
						<div>
							<label data-width="200"> Email </label>
							<input data-type ="email" type="text" class="form-control" value="<?= isset($cgp->cgp_email) ? $cgp->cgp_email : ""; ?>" id="cgp_email" name="cgp_email">
							<div class="help"></div>
						</div>
					</div>
                    <div class="form-group inline">
						<div>
							<label data-width="200"> Téléphone </label>
							<input type="text" class="form-control " value="<?= isset($cgp->cgp_tel) ? $cgp->cgp_tel : ""; ?>" id="cgp_tel" name="cgp_tel">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 1 </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_adresse1) ? $cgp->cgp_adresse1 : ""; ?>" id="cgp_adresse1" name="cgp_adresse1">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 2 </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_adresse2) ? $cgp->cgp_adresse2 : ""; ?>" id="cgp_adresse2" name="cgp_adresse2">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 3 </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_adresse3) ? $cgp->cgp_adresse3 : ""; ?>" id="cgp_adresse3" name="cgp_adresse3">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Code postal </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_cp) ? $cgp->cgp_cp : ""; ?>" id="cgp_cp" name="cgp_cp">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Ville </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_ville) ? $cgp->cgp_ville : ""; ?>" id="cgp_ville" name="cgp_ville">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Pays </label>
							<input type="text" class="form-control maj" value="<?= isset($cgp->cgp_pays) ? $cgp->cgp_pays : ""; ?>" id="cgp_pays" name="cgp_pays">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline switch">
						<div>
							<label data-width="250"> Statut cgp</label>
							<div class="form-check form-switch">
								<span> Inactif </span>
								<input class="form-check-input" type="checkbox" id="cgp_etat" name="cgp_etat" <?php echo isset($cgp->cgp_etat) ? ($cgp->cgp_etat == "1" ? "checked" : "") : "checked"; ?>>
								<span> Actif </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </div>
	</div>
</div>
<?php echo form_close(); ?>