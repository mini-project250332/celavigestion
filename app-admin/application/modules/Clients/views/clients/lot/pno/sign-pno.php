<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter signature PNO</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Pno/AddSignPno',array('method'=>"post", 'class'=>'px-2','id'=>'AddSignPno')); ?>
<input type="hidden" name="pno_id" id="pno_id" value="<?=$pno_id;?>">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?=$cliLot_id;?>">
<input type="hidden" name="client_id" id="client_id" value="<?=$client_id;?>">
<input type="hidden" name="pno_date_creation" id="pno_date_creation" value="<?=$pno->pno_date_creation;?>">

<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Date de signature : </label>
                    <input type="text" class="form-control calendar" id="pno_date_signature" name="pno_date_signature" 
                    value="">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> PNO signé (pdf) : </label>
                    <input type="file" class="form-control" id="path_pno" name="path_pno">
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
	
	flatpickr(".calendar",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
			// defaultDate: moment().format('DD-MM-YYYY'),
		}
	); 

</script>