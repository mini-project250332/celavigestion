<div class="modal fade" id="modal_historique_bail" tabindex="-1" aria-labelledby="modal_historique_bail"
    aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_history">
                    Historique du bail
                </h5>
            </div>
            <div class="modal-body">
                <div class="row mt-2">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Type
                                    </th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Debut
                                    </th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Fin
                                    </th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Status
                                    </th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <?php foreach ($baux as $keyBail => $bail): ?>
                                    <tr>
                                        <td class="td-modal td_bail">
                                            <?php if ($bail->bail_avenant == 1)
                                                echo "Avenant";
                                            else
                                                echo "Bail"; ?>
                                        </td>
                                        <td class="td-modal td_bail">
                                            <?= date("d/m/Y", strtotime(str_replace('/', '-', $bail->bail_debut))); ?>
                                        </td>
                                        <td class="td-modal td_bail">
                                            <?= date("d/m/Y", strtotime(str_replace('/', '-', $bail->bail_fin))); ?>
                                        </td>
                                        <td class="td-modal td_bail">
                                            <?php if ($bail->bail_cloture == 1)
                                                echo "Cloturé";
                                            else
                                                echo "En cours"; ?>
                                        </td>
                                        <td class="td-modal td_bail">
                                            <button class="btn btn-primary btn-sm bold mx-1" ata-bs-toggle="modal"
                                                data-bs-target="#modal_detail_historique_bail" data-backdrop="static"
                                                data-keyboard="false" id="btn_modal_detail_historique_bail"
                                                data-bail_id="<?= $bail->bail_id ?>">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_detail_historique_bail" tabindex="-1" aria-labelledby="modal_detail_historique_bail"
    aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_detail_history">
                    Detail du bail
                </h5>
            </div>
            <div class="modal-body">
                <div id="content-detail-bail-history">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close_modal_detail_historique_bail">Fermer</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).on('click', '#btn_modal_detail_historique_bail', function (e) {
        const bail_id = $(this).attr('data-bail_id');
        $("#modal_detail_historique_bail").modal('toggle');
        getModalHistoriqueContentBail(bail_id);
    });

    $(document).on('click', '#close_modal_detail_historique_bail', function (e) {
        $("#modal_detail_historique_bail").modal('hide');
    });
</script>