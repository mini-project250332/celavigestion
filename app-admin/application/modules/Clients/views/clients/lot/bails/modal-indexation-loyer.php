<?php
if (!isset($indexations)) {
    $indexations = array();
}
?>

<div class="modal fade" id="modal_indexation_loyer" tabindex="-1" aria-labelledby="modal_indexation_loyer"
    aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title indexation_title" id="modal_indexation_loyer">
                    <span class="ajout_indexation_title">
                        <?php if (isset($indexations) && count($indexations) === 0) { ?>
                            Ajout de la première indexation de référence
                        <?php } else { ?>
                            Ajout d'une nouvelle période d'indexation
                        <?php } ?>
                    </span>
                    <span class="update_indexation_title">
                        Modification d'une période d'indexation
                    </span>
                </h5>

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?php if (isset($indexations) && count($indexations) === 0) { ?>
                    <input type="hidden" value="0" id="indexations_count">
                <?php } else if (isset($indexations) && count($indexations) > 0) { ?>
                        <input type="hidden" value="1" id="indexations_count">
                <?php } else { ?>
                        <input type="hidden" value="-1" id="indexations_count">
                <?php } ?>

                <input type="hidden" value="<?= count($indexations) ?>" id="indexations_count_value">

                <input type="hidden" id="indlo_id">
                <?php if(isset($bail->bail_forfait_charge_indexer)) :?>
                <input type="hidden" id="bail_forfait_charge_indexer"
                    value="<?php echo $bail->bail_forfait_charge_indexer; ?>">
                <?php endif ?>

                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <label data-width="250">Date de début :</label>
                    </div>
                    <div class="col-lg-8 col-sm-12">
                        <?php
                        if (isset($indexations) && count($indexations) === 0) { ?>
                            <input type="date" class="form-control input auto_effect" id="indlo_debut" value="<?php if (isset($bail))
                                echo $bail->bail_date_premiere_indexation; ?>">
                        <?php } else if (isset($indexations)) { ?>
                                <input type="date" class="form-control input auto_effect" id="indlo_debut" value="<?php if (isset($indexations))
                                    echo $indexations[count($indexations) - 1]->indlo_fin; ?>">
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php if(isset($bail->bail_forfait_charge_indexer)) :?>
                    <input type="hidden" id="periodicite_bail" value="<?php echo $bail->prdind_valeur; ?>" />
                <?php endif ?>
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <label>Date de prochaine indexation :</label>
                    </div>
                    <div class="col-lg-8 col-sm-12">
                        <?php
                        if (isset($indexations) && count($indexations) === 0) { ?>
                            <input type="date" class="form-control input" id="indlo_fin" value="<?php if (isset($bail))
                                echo $bail->bail_date_prochaine_indexation; ?>">
                        <?php } else {
                            if (isset($indexations)) {
                                $date = $indexations[count($indexations) - 1]->indlo_fin;
                                $newDate = date('Y-m-d', strtotime($date . ' + ' . $bail->prdind_valeur . ' years'));
                            }
                            ?>
                            <input type="date" value="<?php if (isset($indexations))
                                echo $newDate; ?>" class="form-control input" id="indlo_fin">
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <label>Indice de référence :</label>
                    </div>
                    <div class="col-lg-8 col-sm-12">
                        <?php if (isset($indice_valeur_loyer) && count($indice_valeur_loyer) > 0) { ?>
                            <select class="form-select form-control select auto_effect" id="indlo_indice_base"
                                aria-label="Default select example">
                                <option value="">Valeur de l'indice :</option>
                                <?php foreach ($indice_valeur_loyer as $key => $indice): ?>
                                    <option value="<?php echo $indice["indval_id"]; ?>&<?php echo $indice["indval_valeur"]; ?>"
                                        <?php
                                        if (count($indexations) === 1 && $indexations[count($indexations) - 1]->indlo_indice_base === $indice["indval_id"])
                                            echo "selected"
                                                ?>             <?php
                                        if (count($indexations) > 1 && $indexations[count($indexations) - 1]->indlo_indice_reference === $indice["indval_id"])
                                            echo "selected"
                                                ?>>
                                        <?php echo $indice["indval_trimestre"]; ?> &nbsp;
                                        <?php echo $indice["indval_annee"]; ?> &nbsp;
                                        (<?php echo $indice["indval_valeur"]; ?>)
                                    </option>
                                <?php endforeach ?>
                            </select>
                        <?php } else { ?>
                            <div class="empty-select">Aucune valeur pour l'indice de base
                                <?php if (isset($bail))
                                    echo $bail->indloyer_description; ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <?php if (isset($indexations) && count($indexations) > 0) { ?>
                    <div class="row contain_first_indexation">
                        <div class="col-lg-4 col-sm-12">
                            <label>Nouvel indice :</label>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <?php if (isset($indice_valeur_loyer) && count($indice_valeur_loyer) > 0) { ?>
                                <select class="form-select form-control select" id="indlo_indice_reference"
                                    aria-label="Default select example">
                                    <option value="<?php ?>">Valeur de l'indice :</option>
                                    <?php foreach ($indice_valeur_loyer as $key => $indice): ?>
                                        <option value="<?php echo $indice["indval_id"]; ?>&<?php echo $indice["indval_valeur"]; ?>"
                                            <?php
                                            if (count($indexations) === 1 && $indexations[count($indexations) - 1]->indlo_indice_base === $indice["indval_id"])
                                                echo "selected"
                                                    ?>                 <?php
                                            if (count($indexations) > 1 && $indexations[count($indexations) - 1]->indlo_indice_reference === $indice["indval_id"])
                                                echo "selected"
                                                    ?>>
                                            <?php echo $indice["indval_trimestre"]; ?> &nbsp;
                                            <?php echo $indice["indval_annee"]; ?> &nbsp;
                                            (<?php echo $indice["indval_valeur"]; ?>)
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            <?php } else { ?>
                                <div class="empty-select">Aucune valeur pour l'indice de référence
                                    <?php if (isset($bail))
                                        echo $bail->indloyer_description; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>


                <?php if (isset($indexations) && count($indexations) > 0) { ?>
                    <div class="row contain_first_indexation d-none">
                        <div class="col-lg-4 col-sm-12">
                            <label>Variation :</label>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <input type="number" id="indlo_variation_loyer" disabled class="form-control input" value="0"
                                style="text-align:right;margin-left: 20.3%;" maxlength="3" max="100">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" id="indlo_variation_capee" class="form-control input" placeholder="0" maxlength="3"
                        max="100" value="<?= $bail->bail_var_capee ?>">

                    <input type="hidden" id="indlo_variation_plafonnee" class="form-control input" placeholder="0"
                        value="<?= $bail->bail_var_plafonnee ?>">

                    <!-- <div class="row contain_first_indexation">
                        <div class="col-lg-4 col-sm-12">
                            <label>Variation capée :</label>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <div class="input-group">
                                <input type="number" id="indlo_variation_capee" class="form-control input" placeholder="0"
                                    maxlength="3" max="100" value="<?= $bail->bail_var_capee ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contain_first_indexation">
                        <div class="col-lg-4 col-sm-12">
                            <label>Variation plafonnée :</label>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <div class="input-group">
                                <input type="number" id="indlo_variation_plafonnee" class="form-control input"
                                    placeholder="0" value="<?= $bail->bail_var_plafonnee ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div>
                    </div> -->


                    <div class="row contain_first_indexation">
                        <div class="col-lg-4 col-sm-12">
                            <label>Variation appliquée :</label>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <div class="input-group">
                                <input type="number" id="indlo_variation_appliquee" disabled
                                    class="auto_effect form-control input" value="0" maxlength="3" max="100">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="row mt-2">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2 text-black" style="border-right: 1px solid #ffff;width:15%;">Nouveaux
                                        Loyers</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                        Appartement</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Parking
                                    </th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Forfait
                                        charges</th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">Montant HT</th>
                                    <td class="td-modal">
                                        <?php
                                        $defaultAppartementHT = null;

                                        if (isset($indexations) && count($indexations) > 0) {
                                            $defaultAppartementHT = $indexations[count($indexations) - 1]->indlo_appartement_ht;
                                        }
                                        ?>
                                        <div class="input-group">
                                            <input type="number" value="<?= $defaultAppartementHT; ?>"
                                                id="indlo_appartement_ht"
                                                class="auto_effect form-control mb-0 input text-right">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <?php
                                        $defaultParkingHT = null;

                                        if (isset($indexations) && count($indexations) > 0) {
                                            $defaultParkingHT = $indexations[count($indexations) - 1]->indlo_parking_ht;
                                        }
                                        ?>
                                        <div class="input-group">
                                            <input type="number" value="<?= $defaultParkingHT; ?>" id="indlo_parking_ht"
                                                class="form-control mb-0 input text-right auto_effect">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <?php
                                        $defaultChargeHT = null;

                                        if (isset($indexations) && count($indexations) > 0 && $bail->bail_forfait_charge_indexer === "1") {
                                            $defaultChargeHT = $indexations[count($indexations) - 1]->indlo_charge_ht;
                                        } else if (count($indexations) > 0 && $bail->bail_forfait_charge_indexer === "0") {
                                            $defaultChargeHT = $indexations[0]->indlo_charge_ht;
                                        }
                                        ?>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge">-</span>
                                            </div>
                                            <input type="number" value="<?= $defaultChargeHT; ?>" id="indlo_charge_ht"
                                                class="form-control mb-0 input text-right auto_effect">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TVA</th>
                                    <td class="td-modal">
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge group-chevron-down">
                                                    <span class="fas fa-chevron-down"></span>
                                                </span>
                                            </div>
                                            <select id="indlo_appartement_tva" value=""
                                                class="form-control mb-0 select text-right">
                                                <?php foreach ($tva as $key => $item): ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>" <?php if (isset($indexations) && count($indexations) === 0 && $item->tva_valeur === "10")
                                                           echo "selected"; ?>     <?php if (isset($indexations) && count($indexations) > 0 && $indexations[count($indexations) - 1]->indlo_appartement_tva === $item->tva_valeur)
                                                                      echo "selected"; ?>>
                                                        <?php echo $item->tva_libelle; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>


                                            <!-- <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div> -->
                                        </div>
                                        <div class="input-group input_tva_euro">
                                            <input type="number" id="indlo_appartement_tva_euro" disabled
                                                class="form-control input text-right auto_effect" value="0">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge group-chevron-down">
                                                    <span class="fas fa-chevron-down"></span>
                                                </span>
                                            </div>
                                            <select id="indlo_parking_tva" value=""
                                                class="form-control mb-0 select text-right">
                                                <?php foreach ($tva as $key => $item): ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>" <?php if (isset($indexations) && count($indexations) === 0 && $item->tva_valeur === "20")
                                                           echo "selected"; ?>     <?php if (isset($indexations) && count($indexations) > 0 && $indexations[count($indexations) - 1]->indlo_parking_tva === $item->tva_valeur)
                                                                      echo "selected"; ?>>
                                                        <?php echo $item->tva_libelle; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                            <!-- <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div> -->
                                        </div>
                                        <div class="input-group input_tva_euro">
                                            <input type="number" id="indlo_parking_tva_euro" disabled
                                                class="form-control input text-right auto_effect" value="0">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge group-chevron-down">
                                                    <span class="fas fa-chevron-down"></span>
                                                </span>
                                            </div>
                                            <select id="indlo_charge_tva" class="form-control mb-0 select text-right">
                                                <?php foreach ($tva as $key => $item): ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>" <?php if (isset($indexations) && count($indexations) === 0 && $item->tva_valeur === "20")
                                                           echo "selected"; ?>     <?php if (isset($indexations) && count($indexations) > 0 && $indexations[count($indexations) - 1]->indlo_charge_tva === $item->tva_valeur)
                                                                      echo "selected"; ?>>
                                                        <?php echo $item->tva_libelle; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                            <!-- <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div> -->
                                        </div>
                                        <div class="input-group input_tva_euro">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge">-</span>
                                            </div>
                                            <input type="number" id="indlo_charge_tva_euro" disabled
                                                class="form-control input text-right auto_effect" value="0">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">Montant TTC</th>
                                    <td class="td-modal">
                                        <div class="input-group">
                                            <input type="number" id="indlo_appartement_ttc" disabled
                                                class="form-control mb-0 input text-right auto_effect">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <div class="input-group">
                                            <input type="number" id="indlo_parking_ttc" disabled
                                                class="form-control mb-0 input text-right auto_effect">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-modal">
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text group-charge">-</span>
                                            </div>
                                            <input type="number" id="indlo_charge_ttc" disabled
                                                class="form-control mb-0 input text-right auto_effect">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index"
                                                    id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" id="addNewIndexationLoyer">Ajouter</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal confirm ajout 2eme indexation -->
<div class="modal fade" id="confim_modal_indexation_loyer" tabindex="-1" aria-labelledby="confim_modal_indexation_loyer"
    aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header header-confirm-indexation">
                Confirmation de l'ajout d'indexation
            </div>
            <div class="modal-body body-confirm-indexation">
                Si vous ajouter l'indexation, le loyer de référence ainsi que l'indice de référence ne seront plus
                modifiable. Voulez vous continuer ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                    id="cancelNewIndexationLoyer">Annuler</button>
                <button type="button" class="btn btn-primary" id="confirmNewIndexationLoyer">Confirmer</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modal_indexation_loyer').on('shown.bs.modal', function () {
        updateAppartementTTC();
        updateParkingTTC();
        updateChargeTTC();
    });


    $("#confirmNewIndexationLoyer").bind("click", function (event) {
        event.preventDefault();
        console.log("confirm");
        $("#indexations_count_value").val(2);
        $("#cancelNewIndexationLoyer").trigger('click');
        sendNewIndexationLoyer();
    });

    $("#addNewIndexationLoyer").bind("click", function (event) {
        const count_indexation = $("#indexations_count_value").val();
        if (count_indexation == 1) {
            $("#confim_modal_indexation_loyer").modal('show');
            return false;
        }
        event.preventDefault();
        sendNewIndexationLoyer();

    });

    function sendNewIndexationLoyer() {
        console.log("sendNewIndexationLoyer");
        const indlo_id = $("#indlo_id").val();
        const indlo_debut = $("#indlo_debut").val();
        const indlo_fin = $("#indlo_fin").val();
        const indlo_appartement_ht = $("#indlo_appartement_ht").val();
        const indlo_appartement_tva = $("#indlo_appartement_tva").val();
        const indlo_appartement_ttc = $("#indlo_appartement_ttc").val();
        const indlo_parking_ht = $("#indlo_parking_ht").val();
        const indlo_parking_tva = $("#indlo_parking_tva").val();
        const indlo_parking_ttc = $("#indlo_parking_ttc").val();
        const bail_id = $("#bail_id").val();
        const indlo_charge_ht = $("#indlo_charge_ht").val();
        const indlo_charge_tva = $("#indlo_charge_tva").val();
        const indlo_charge_ttc = $("#indlo_charge_ttc").val();
        const indlo_autre_prod_ht = $("#indlo_autre_prod_ht").val() ? $("#indlo_autre_prod_ht").val() : 0;
        const indlo_autre_prod_tva = $("#indlo_autre_prod_tva").val() ? $("#indlo_autre_prod_tva").val() : 0;
        const indlo_autre_prod_ttc = $("#indlo_autre_prod_ttc").val() ? $("#indlo_autre_prod_ttc").val() : 0;
        const indlo_indice_base = $("#indlo_indice_base").val() ? $("#indlo_indice_base").val().split('&')[0] : null;
        const indlo_indice_reference = $("#indlo_indice_reference").val() ? $("#indlo_indice_reference").val().split('&')[0] : null;
        const indlo_variation_plafonnee = $("#indlo_variation_plafonnee").val() ? $("#indlo_variation_plafonnee").val() : 0;
        const indlo_variation_capee = $("#indlo_variation_capee").val() ? $("#indlo_variation_capee").val() : 0;
        const indlo_variation_loyer = $("#indlo_variation_loyer").val() ? $("#indlo_variation_loyer").val() : 0;
        const indlo_variation_appliquee = $("#indlo_variation_appliquee").val() ? $("#indlo_variation_appliquee").val() : 0;

        const indexations_count = $("#indexations_count").val();

        let isValidIndice = true;
        if (!indlo_id && parseInt(indexations_count) >= 0) {
            isValidIndice = (indexations_count === "0") ? (indlo_indice_base ? true : false) : (indlo_indice_reference ? true : false);
        }

        if (indlo_debut && indlo_fin && indlo_appartement_ht && indlo_appartement_tva && indlo_appartement_ttc &&
            indlo_parking_ht && indlo_parking_tva && indlo_parking_ttc && indlo_charge_ht && indlo_charge_tva &&
            indlo_charge_ttc && indlo_indice_base && isValidIndice) {
            addIndexationLoyer(indlo_id, indlo_debut, indlo_fin,
                indlo_appartement_ht, indlo_appartement_tva, indlo_appartement_ttc,
                indlo_parking_ht, indlo_parking_tva, indlo_parking_ttc,
                bail_id, indlo_charge_ht, indlo_charge_tva, indlo_charge_ttc,
                indlo_autre_prod_ht, indlo_autre_prod_tva, indlo_autre_prod_ttc,
                indlo_indice_base, indlo_indice_reference, indlo_variation_plafonnee, indlo_variation_capee,
                indlo_variation_loyer, indlo_variation_appliquee, "modal_indexation_loyer",);
        } else {
            Notiflix.Notify.failure("Veuillez compléter les champs.", {
                position: 'left-bottom',
                timeout: 3000
            });
        }
    }

    // Calcul TTC
    $("#indlo_appartement_ht").on("keyup", function (event) {
        updateAppartementTTC();
    });

    $("#indlo_appartement_tva").on("change", function (event) {
        updateAppartementTTC();
    });

    function updateAppartementTTC() {
        // if (!$("#indlo_appartement_ht").val()) $("#indlo_appartement_ht").val(0);
        if (!$("#indlo_appartement_tva").val()) $("#indlo_appartement_tva").val("0");
        let ht = $("#indlo_appartement_ht").val();
        let tva = $("#indlo_appartement_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#indlo_appartement_tva_euro").val(tva);
        const ttc = (+ht) + (+tva);
        $("#indlo_appartement_ttc").val(ttc);

        fix2Decimal("indlo_appartement_tva_euro", tva);
        fix2Decimal("indlo_appartement_ttc", ttc);
    }

    $("#indlo_parking_ht").on("keyup", function (event) {
        updateParkingTTC();
    });

    $("#indlo_parking_tva").on("change", function (event) {
        updateParkingTTC();
    });

    function updateParkingTTC() {
        // if (!$("#indlo_parking_ht").val()) $("#indlo_parking_ht").val(0);
        if (!$("#indlo_parking_tva").val()) $("#indlo_parking_tva").val(0);
        let ht = $("#indlo_parking_ht").val();
        let tva = $("#indlo_parking_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#indlo_parking_tva_euro").val(tva);
        const ttc = (+ht) + (+tva);
        $("#indlo_parking_ttc").val(ttc);

        fix2Decimal("indlo_parking_tva_euro", tva);
        fix2Decimal("indlo_parking_ttc", ttc);
    }

    $("#indlo_charge_ht").on("keyup", function (event) {
        updateChargeTTC();
    });

    $("#indlo_charge_tva").on("change", function (event) {
        updateChargeTTC();
    });

    function updateChargeTTC() {
        if (!$("#indlo_charge_tva").val()) $("#indlo_charge_tva").val(0);

        let ht = $("#indlo_charge_ht").val();
        let tva = $("#indlo_charge_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#indlo_charge_tva_euro").val(tva);
        const ttc = (+ht) + (+tva);
        $("#indlo_charge_ttc").val(ttc);

        fix2Decimal("indlo_charge_tva_euro", tva);
        fix2Decimal("indlo_charge_ttc", ttc);
    }

    $("#indlo_autre_prod_ht").on("keyup", function (event) {
        updateAutreProdTTC();
    });

    $("#indlo_autre_prod_tva").on("change", function (event) {
        updateAutreProdTTC();
    });

    function updateAutreProdTTC() {
        // if (!$("#indlo_autre_prod_ht").val()) $("#indlo_autre_prod_ht").val(0);
        if (!$("#indlo_autre_prod_tva").val()) $("#indlo_autre_prod_tva").val(0);
        let ht = $("#indlo_autre_prod_ht").val();
        let tva = $("#indlo_autre_prod_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#indlo_autre_prod_tva_euro").val(tva);
        const ttc = (+ht) + (+tva);
        $("#indlo_autre_prod_ttc").val(ttc);

        fix2Decimal("indlo_autre_prod_tva_euro", tva);
        fix2Decimal("indlo_autre_prod_ttc", ttc);
    }

    function fix2Decimal(id, value) {
        if (!value || value === 0) return false;
        let newValue = +value;
        newValue = newValue.toFixed(2);
        $(`#${id}`).val(newValue);
    }

    // Calcul variation
    $("#indlo_indice_base").bind("change", function (event) {
        updateVariation();
    });

    $("#indlo_indice_reference").bind("change", function (event) {
        updateVariation();
    });

    $("#indlo_variation_loyer").bind("keyup", function (event) {
        updateVariationAppliquee();
    });

    $("#indlo_variation_plafonnee").on("keyup", function (event) {
        updateVariationAppliquee();
    });

    $("#indlo_variation_capee").on("keyup", function (event) {
        updateVariationAppliquee();
    });

    function updateVariation() {
        let indlo_indice_base = $("#indlo_indice_base").val();
        indlo_indice_base = indlo_indice_base ? parseFloat(indlo_indice_base.split("&")[1]) : null;

        let indlo_indice_reference = $("#indlo_indice_reference").val();
        indlo_indice_reference = indlo_indice_reference ? parseFloat(indlo_indice_reference.split("&")[1]) : null;

        if (indlo_indice_base && indlo_indice_reference) {
            let variation = (indlo_indice_reference - indlo_indice_base) / indlo_indice_base;
            variation = (variation * 100).toFixed(2);
            $("#indlo_variation_loyer").val(variation);
            updateVariationAppliquee();
        }
    }

    function updateVariationAppliquee() {
        let indlo_variation_loyer = $("#indlo_variation_loyer").val();

        let indlo_variation_plafonnee = $("#indlo_variation_plafonnee").val();

        let indlo_variation_capee = $("#indlo_variation_capee").val();
        indlo_variation_capee = (indlo_variation_capee * indlo_variation_loyer) / 100;

        let valArray = [];
        if (parseInt(indlo_variation_loyer) > 0) valArray.push(indlo_variation_loyer);
        if (parseInt(indlo_variation_plafonnee) > 0) valArray.push(indlo_variation_plafonnee);
        if (parseInt(indlo_variation_capee) > 0) valArray.push(indlo_variation_capee);

        let min = +indlo_variation_loyer;

        for (let indice in valArray) {
            if (min > +valArray[indice]) min = +valArray[indice];
        }
        $("#indlo_variation_appliquee").val(min.toFixed(2));
        updateHTByVariationAppliquee();
    }

    function updateHTByVariationAppliquee() {
        const variation = $("#indlo_variation_appliquee").val();

        const appartementHT = $("#indlo_appartement_ht").val();
        const newAppartementHT = (+appartementHT) + ((+appartementHT) * (+variation) / 100);
        $("#indlo_appartement_ht").val(newAppartementHT.toFixed(2));


        const parkingHT = $("#indlo_parking_ht").val();
        const newParkingHT = (+parkingHT) + ((+parkingHT) * (+variation) / 100);
        $("#indlo_parking_ht").val(newParkingHT.toFixed(2));

        // If indexed
        if ($("#bail_forfait_charge_indexer").val() === "1") {
            const chargeHT = $("#indlo_charge_ht").val();
            const newChargeHT = (+chargeHT) + ((+chargeHT) * (+variation) / 100);
            $("#indlo_charge_ht").val(newChargeHT.toFixed(2));
        }

        const autreProdHT = $("#indlo_autre_prod_ht").val();
        const newAutreProdHT = (+autreProdHT) + ((+autreProdHT) * (+variation) / 100);
        $("#indlo_autre_prod_ht").val(newAutreProdHT.toFixed(2));

        updateAppartementTTC();
        updateParkingTTC();
        updateChargeTTC();
        updateAutreProdTTC();
    }

    // Date debut

    $("#indlo_debut").bind("change", function (event) {
        const periodicite_bail = $("#periodicite_bail").val();
        const indlo_fin = $("#indlo_fin").val();
        const indlo_debut = $("#indlo_debut").val();

        const today = new Date(indlo_debut);
        const newToday = new Date(today.setFullYear(today.getFullYear() + parseInt(periodicite_bail)));

        const year = newToday.getFullYear();
        let month = newToday.getMonth();
        month = ((month + 1) < 10) ? `0${(month + 1)}` : (month + 1);
        let day = newToday.getDate();
        day = (day < 10) ? `0${day}` : day;
        $("#indlo_fin").val(`${year}-${month}-${day}`);

    });
</script>

<style>
    .input_tva_euro {
        margin-top: 11px;
        margin-bottom: -25px;
    }

    .td-modal {
        padding-right: 0px !important;
    }

    .input-group-text-index {
        background: white;
        /* padding: 5px; */
        width: 35px;
        display: flex;
        justify-content: center;
    }

    .group-chevron-down {
        height: 36px;
        font-size: 11px;
        width: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 0px;
    }
</style>