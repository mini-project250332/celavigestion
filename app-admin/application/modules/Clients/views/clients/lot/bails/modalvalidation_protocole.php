<div class="modal-header">
    <h5 class="modal-title">
        <?= $titre_modal ?>
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="annulerProtocol"></button>
</div>

<input type="hidden" name="id_protocol"  id="id_protocol" value="<?= isset($protocol) ? $protocol->id_protocol : '' ?>">

<div class="modal-body">
    <div class="col">
        <div class="form-group inline p-0" id="fichier">
            <div class="col">
                <br>
                <label data-width="400">Fichier du protocole :</label>
                <input type="file" class="form-control auto_effect" id="file_protocol" name="file_protocol">
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Date de signature du protocole :</label>
                <input type="date" class="form-control auto_effect" id="date_sign_protocole" name="date_sign_protocole" value="<?= isset($protocol) ? $protocol->date_sign_protocole : date('Y-m-d') ?>">
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Date de prise d'effet du protocole :</label>
                <input type="date" class="form-control auto_effect" id="date_debut_protocole" name="date_debut_protocole" value="<?= isset($protocol) ? $protocol->date_debut_protocole : date('Y-m-d') ?>">
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Date de fin du protocole :</label>
                <input type="date" class="form-control auto_effect" id="date_fin_protocole" name="date_fin_protocole" value="<?= isset($protocol) ? $protocol->date_fin_protocole : date('Y-m-d') ?>">
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Type d'abandon :</label>
                <select class="form-select auto_effect" id="type_abandon" name="type_abandon">
                    <option value="0" <?= isset($protocol) && $protocol->type_abandon == 0 ? 'selected' : '' ?>>Abandon total</option>
                    <option value="1" <?= isset($protocol) && $protocol->type_abandon == 1 ? 'selected' : '' ?>>Abandon partiel</option>
                </select>
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Pourcentage de prise en charge des factures :</label>
                <input type="number" class="form-control auto_effect" id="pourcentage_protocol" name="pourcentage_protocol" value="<?= isset($protocol) ? $protocol->pourcentage_protocol : 0 ?>" style="text-align: right;">
                <div class="input-group-append">
                    <span class="input-group-text input-group-text-index" style="height: 32px;">%</span>
                </div>
            </div>
        </div>

        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="400">Informations complémentaires :</label>
                <textarea class="form-control auto_effect" id="info_complementaire" name="info_complementaire" style="height: 50px;"><?= isset($protocol) ? $protocol->info_complementaire : '' ?></textarea>
            </div>
        </div>

        <br>
    </div>
</div>

<div class="modal-footer">
    <button type="submit" id="confirmerProtocol" class="btn btn-primary" data-bs-dismiss="modal">Confirmer</button>
    <button type="button" id="annulerProtocol" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
</div>

<script>
    $(document).ready(function() {

        $('#type_abandon').on('change', function() {
            if ($(this).val() == 0) {
                $('#pourcentage_protocol').attr('disabled', true);
                validationValue();
            } else {
                $('#pourcentage_protocol').attr('disabled', false);
                validationValue();
            }
        });

        $('#pourcentage_protocol').attr({
            "min": 0,
            "max": 100
        });

        $('#pourcentage_protocol').on('input', function() {
            var val = $(this).val();
            if (val < 0 || val > 100) {
                $(this).val('');
            }
        });

        $('#type_abandon').trigger('change');

        $('#file_protocol').on('change', function() {
            validationValue();
        });

        $('#date_sign_protocole').on('change', function() {
            validationValue();
        });

        $('#date_debut_protocole').on('change', function() {
            validationValue();
        });

        $('#date_fin_protocole').on('change', function() {
            validationValue();
        });

        $('#date_sign_protocole').on('keyup', function() {
            validationValue();
        });

        $('#date_debut_protocole').on('keyup', function() {
            validationValue();
        });

        $('#date_fin_protocole').on('keyup', function() {
            validationValue();
        });

        $('#pourcentage_protocol').on('keyup', function() {
            validationValue();
        });

        function validationValue() {
            if ($('#type_abandon').val() == 0) {
                if ($('#file_protocol').val() && $('#date_sign_protocole').val() && $('#date_debut_protocole').val() && $('#date_fin_protocole').val()) {
                    $('#confirmerProtocol').attr('disabled', false);
                } else {
                    $('#confirmerProtocol').attr('disabled', true);
                }
            } else {
                if ($('#file_protocol').val() && $('#date_sign_protocole').val() && $('#date_debut_protocole').val() && $('#date_fin_protocole').val() && $('#pourcentage_protocol').val()) {
                    $('#confirmerProtocol').attr('disabled', false);
                } else {
                    $('#confirmerProtocol').attr('disabled', true);
                }
            }
        }
    });
</script>