<div class="h-100 w-100" id="contenair-bail">

</div>


<div id="contain_modal_indexation_loyer"></div>


<div id="contain_modal_historique_bail"></div>

<div class="modal fade" id="modal_franchise" tabindex="-1" aria-labelledby="modal_franchise" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header " style="background-color : #2569C3 !important;">
                <h5 class="modal-title franchise_title addNewFranchise text-white">Ajout d'une nouvelle période de franchise de
                    Loyer</h5>
                <h5 class="modal-title franchise_title updateNewFranchise text-white">Modification d'une période de franchise de
                    Loyer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <input type="hidden" id="franc_id" />
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Date de début :</label>
                                <input type="date" class="form-control input" value="" id="franc_debut">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Date de fin :</label>
                                <input type="date" class="form-control input" value="" id="franc_fin">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Type de déduction :</label>
                                <select name="" id="type_deduction" class="form-select">
                                    <option value="1">Déduction totale</option>
                                    <option value="2">Déduction partielle</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none" id="display_ht_deduit">
                    <div class="row">
                        <div class="col">
                            <div class="form-group inline p-0">
                                <div class="col">
                                    <label data-width="250">Mode de calcul : &nbsp;</label>
                                    <select name="franchise_mode_calcul" id="franchise_mode_calcul" class="form-select">
                                        <option value="0" selected>Montant Fixe</option>
                                        <option value="1">Pourcentage</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-none row" id="display_pourcentage">
                        <div class="col">
                            <div class="form-group inline p-0">
                                <div class="col">
                                    <label data-width="250">Pourcentage: &nbsp;</label>
                                    <input type="number" class="form-control input" value="" id="franchise_pourcentage" style="text-align: right;width:96%;">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                    </div>
                                    <div class="help help_franchise_pourcentage text-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="display_montant_ht_deduit">
                        <div class="col">
                            <div class="form-group inline p-0">
                                <div class="col">
                                    <label data-width="250">Montant HT déduit: &nbsp;</label>
                                    <input type="number" class="form-control input" value="" id="montant_ht_deduit" style="text-align: right;width:96%;">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                                    </div>
                                    <div class="help help_montant_ht_deduit text-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group inline p-0">
                                <div class="col">
                                    <label data-width="250">Taux TVA : &nbsp;</label>
                                    <select name="" id="taux_tva_franchise" class="form-select">
                                        <option value="0">0%</option>
                                        <option value="2.1">2.10%</option>
                                        <option value="5.5">5.5%</option>
                                        <option value="10">10%</option>
                                        <option value="20">20%</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="display_montant_ttc_deduit">
                        <div class="col">
                            <div class="form-group inline p-0">
                                <div class="col">
                                    <label data-width="300">Montant TTC déduit : &nbsp;</label>
                                    <div class="input-group-prepend prepend-charge">
                                        <span class="input-group-text input-group-text-index">-</span>
                                    </div>
                                    <input type="number" class="form-control input" value="" id="montant_ttc_deduit" style="text-align: right;width:105%;">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                                    </div>
                                    <div class="help help_montant_ttc_deduit text-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Libellé sur facture :</label>
                                <input type="text" class="form-control input" value="" id="franc_explication">
                                <div class="help help_franc_explication text-danger"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="">Annuler</button>
                <button type="button" class="btn btn-primary addNewFranchise" id="addNewFranchise">Enregistrer</button>
                <button type="button" class="btn btn-primary d-none updateNewFranchise" id="updateNewFranchise">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" value="<?= $lot->cliLot_id; ?>" id="lot_client_id">

<script type="text/javascript">
    $(document).ready(function() {
        // getDocBails(<?= $lot->cliLot_id; ?>);
    });

    $('#type_deduction').on('change', function() {
        if ($(this).val() == 2) {
            $('#display_ht_deduit').removeClass('d-none');
        } else {
            if (!$('#display_ht_deduit').hasClass('d-none')) {
                $('#display_ht_deduit').addClass('d-none');
            }
        }
    });

    $("#addNewFranchise").bind("click", function(event) {
        event.preventDefault();
        const franc_debut = $("#franc_debut").val();
        const franc_fin = $("#franc_fin").val();
        const franc_explication = $("#franc_explication").val();
        const type_deduction = $("#type_deduction").val();
        const montant_ht_deduit = $("#montant_ht_deduit").val().replace(/,/g, '.');
        const taux_tva_franchise = $("#taux_tva_franchise").val();
        const montant_ttc_deduit = $("#montant_ttc_deduit").val().replace(/,/g, '.');
        const bail_id = $("#bail_id").val();
        const franchise_mode_calcul = $("#franchise_mode_calcul").val();
        const franchise_pourcentage = $("#franchise_pourcentage").val();

        if (new Date(franc_fin) < new Date(franc_debut)) {
            Notiflix.Notify.failure("Date de fin ultérieure à la date de début.", {
                position: 'left-bottom',
                timeout: 3000
            });
            return false;
        }

        $('.help_montant_ttc_deduit').empty();
        $('.help_montant_ttc_deduit').empty();
        $('.help_franchise_pourcentage').empty();
        if (type_deduction == 1) {
            if (franc_explication) {
                addFranchiseLoyer(null, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
            } else {
                $('.help_franc_explication').text('Ce champ de doit pas être vide');
            }
        } else {
            if (franchise_mode_calcul == 0) {
                if (franc_debut && franc_fin && montant_ttc_deduit && franc_explication) {
                    addFranchiseLoyer(null, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
                } else {
                    if (!montant_ttc_deduit) {
                        $('.help_montant_ttc_deduit').text('Ce champ de doit pas être vide');
                    }

                    if (!franc_explication) {
                        $('.help_franc_explication').text('Ce champ de doit pas être vide');
                    }
                }
            } else {
                if (franc_debut && franc_fin && franchise_pourcentage && franc_explication) {
                    addFranchiseLoyer(null, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
                } else {
                    if (!franchise_pourcentage) {
                        $('.help_franchise_pourcentage').text('Ce champ de doit pas être vide');
                    }

                    if (!franc_explication) {
                        $('.help_franc_explication').text('Ce champ de doit pas être vide');
                    }
                }
            }
        }
    });

    $("#updateNewFranchise").bind("click", function(event) {
        const franc_id = $("#franc_id").val();
        updateFranchise(franc_id);
    });
</script>

<style>
    .input,
    .select {
        height: 36px !important;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .input-group-text-index {
        border-radius: 0px !important;
    }

    .empty-select {
        color: red;
        width: 100%;
        text-align: right;
        padding-top: 5px;
        font-style: italic;
    }

    span.input-group-text.group-charge {
        background: white;
        border-right: none;
        user-select: none;
    }
</style>