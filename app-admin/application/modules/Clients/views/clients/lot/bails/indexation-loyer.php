<style>
    .text-black {
        color: black;
    }

    .text-left {
        text-align: left !important;
        color: black;
    }

    .text-right {
        text-align: right !important;
        color: black;
    }
</style>

<input type="hidden" id="countIndexationLoyer" value="<?= isset($indexations) ? count($indexations) : 0 ?>">

<div class="row mb-4">
    <div class="col table_table">
        <table class="table table-hover fs--1 ">
            <thead class="text-white" style="background:#2569C3;">
                <tr class="text-center">
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Début</th>
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Fin</th>
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Indice</th>
                    <th class="p-2" colspan="4" style="border-right: 1px solid #ffff;">Montant</th>
                </tr>
                <tr class="text-center">
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Type</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">HT</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">TVA</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">TTC</th>
                </tr>
            </thead>
            <tbody class="bg-200 table_indexation_loyer">
                <?php if (isset($indexations) && count($indexations) > 1) { ?>
                    <?php
                    $index = 0;
                    foreach ($indexations as $key => $indexation) : ?>
                        <?php if ($indexation->indlo_ref_loyer == 0 && ($indexation->indlo_indince_non_publie == 0)) { ?>
                            <tr class="text-center" id="indexation_row-<?= $indexation->indlo_id ?>">
                                <td>
                                    <div class="text-black">
                                        <?php
                                        if (isset($indexation->indlo_debut))
                                            echo date_format_fr($indexation->indlo_debut);
                                        else
                                            echo "-"; ?>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-black">
                                        <?php
                                        if (isset($indexation->indlo_fin))
                                            echo date_format_fr($indexation->indlo_fin);
                                        else
                                            echo "-"; ?>
                                    </div>
                                </td>

                                <td>
                                    <?php
                                    $indx_libelle = $indexation->indval_libelle;
                                    $indx_valeur = $indexation->indval_valeur;

                                    if (intval($index) > 0) {
                                        foreach ($indices as $indice) {
                                            if ($indice->indval_id === $indexation->indlo_indice_reference) {
                                                $indx_libelle = $indice->indval_libelle;
                                                $indx_valeur = $indice->indval_valeur;
                                            }
                                        }
                                    }
                                    ?>

                                    <div class="text-black">
                                        <?php echo $indx_libelle; ?>
                                        <div>
                                            <b>
                                                <?php echo $indx_valeur; ?> <?= $indexation->mode_calcul == 2 ? '%' : '' ?>
                                            </b>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-left">Appartement</div>
                                    <div class="text-left">Parking</div>
                                    <div class="text-left">Forfait charges</div>
                                    <div class="text-left"><b>Total</b></div>
                                </td>

                                <td>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_appartement_ht); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_parking_ht); ?>
                                    </div>
                                    <div class="text-right">
                                        <?= $indexation->indlo_charge_ht != 0 ? '-' : '' ?> <?php echo format_number_($indexation->indlo_charge_ht); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php
                                        $totalA = 0;
                                        $totalA = $indexation->indlo_appartement_ht + $indexation->indlo_parking_ht - $indexation->indlo_charge_ht + $indexation->indlo_autre_prod_ht;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalA); ?>
                                        </b>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_appartement_tva; ?>%) &nbsp;
                                        <?php
                                        $newA = 0;
                                        $newA = $indexation->indlo_appartement_ht * $indexation->indlo_appartement_tva / 100;
                                        echo format_number_($newA);
                                        ?>
                                    </div>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_parking_tva; ?>%) &nbsp;
                                        <?php
                                        $newB = 0;
                                        $newB = $indexation->indlo_parking_ht * $indexation->indlo_parking_tva / 100;
                                        echo format_number_($newB);
                                        ?>
                                    </div>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_charge_tva; ?>%) &nbsp;
                                        <?php
                                        $newC = 0;
                                        $newC = $indexation->indlo_charge_ht * $indexation->indlo_charge_tva / 100;
                                        echo $newC != 0 ? '-' : '';
                                        echo  format_number_($newC);
                                        ?>
                                    </div>

                                    <div class="text-right">
                                        <?php
                                        $totalB = 0;
                                        $totalB = $newA + $newB - $newC;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalB); ?>
                                        </b>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_appartement_ttc); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_parking_ttc); ?>
                                    </div>
                                    <div class="text-right">
                                        <?= $indexation->indlo_charge_ttc != 0 ? '-' : '' ?> <?php echo format_number_($indexation->indlo_charge_ttc); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php
                                        $totalC = 0;
                                        $totalC = $indexation->indlo_appartement_ttc + $indexation->indlo_parking_ttc - $indexation->indlo_charge_ttc + $indexation->indlo_autre_prod_ttc;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalC); ?>
                                        </b>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php
                        $index = $index + 1;
                    endforeach ?>
                    <?php
                    $countIndexation = count($indexations);
                    $dernierIndexation = $indexations[$countIndexation - 1];
                    $increment = $dernierIndexation->prdind_id == 1 ? 1 : 3;

                    if ($indexation->indlo_indince_non_publie == 0) {
                        $nextDateDebut = date('Y-m-d', strtotime('+1 day', strtotime($dernierIndexation->indlo_fin)));
                        $nextDateFin = date('Y-m-d', strtotime('+' . $increment . ' years -1 day', strtotime($dernierIndexation->indlo_fin)));
                    } else {
                        $nextDateDebut = date('Y-m-d', strtotime('+1 day', strtotime($dernierIndexation->indlo_fin)));
                        $nextDateFin = date('Y-m-d', strtotime('+' . $increment . ' years -1 day', strtotime($nextDateDebut)));
                    }
                    ?>

                    <?php if ($indexation->indlo_indince_non_publie == 0 || $indexation->indlo_indince_non_publie == 1 || $indexation->indlo_indince_non_publie == 2) :
                        for ($i = 0; $i < 5; $i++) :
                    ?>
                            <tr class="text-center">
                                <td>
                                    <div class="text-black">
                                        <?= date_format_fr($nextDateDebut) ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="text-black">
                                        <?= date_format_fr($nextDateFin) ?>
                                    </div>
                                </td>
                                <td><b>En attente de la publication <br> d'indice</b></td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        <?php
                            $nextDateDebut = date('Y-m-d', strtotime('+' . $increment . ' years', strtotime($nextDateDebut)));
                            $nextDateFin = date('Y-m-d', strtotime('+' . ($increment) . 'year -1 day', strtotime($nextDateDebut)));
                            if ($i == 3) {
                                $nextDateDebutObj = new DateTime($nextDateDebut);
                                $nextDateFinObj = clone $nextDateDebutObj;
                                $nextDateFinObj->add(new DateInterval('P' . $increment . 'Y'));
                                $nextDateFinObj->sub(new DateInterval('P1D'));
                                $nextDateFin = $nextDateFinObj->format('Y-m-d');
                            }
                        endfor;
                        ?>
                    <?php endif ?>
                <?php } ?>
            </tbody>
        </table>

        <?php if (!isset($indexations) || count($indexations) === 1) { ?>
            <div class="text-center mt-2">
                Aucune indexation de loyer
            </div>
        <?php } ?>
    </div>
</div>

<script>
    only_visuel_gestion();
</script>