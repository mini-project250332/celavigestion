<div class="row">
    <div class="col">
        <table class="table table-hover fs--1">
            <thead class="text-white" style="background:#2569C3;">
                <tr class="text-center">
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Début</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Fin</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;" width="30%">Libellé sur facture</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Type de déduction</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">TTC déduit</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Pourcentage</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Actions</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php foreach ($franchises as $key => $franchise) : ?>
                    <tr class="text-center" id="franchise_row-<?php echo $franchise->franc_id ?>">
                        <td class="franchise-view-<?php echo $key ?>">
                            <?php echo date_format_fr($franchise->franc_debut); ?>
                        </td>
                        <td class="franchise-view-<?php echo $key ?>">
                            <?php echo date_format_fr($franchise->franc_fin); ?>
                        </td>
                        <td class="franc_explication_field franchise-view-<?php echo $key ?>">
                            <div>
                                <?php echo $franchise->franc_explication ?>
                            </div>
                        </td>
                        <td class="franchise-view-<?php echo $key ?>">
                            <?= $franchise->type_deduction == 1 ? 'Totale' : 'Partielle' ?>
                        </td>
                        <td class="franchise-view-<?php echo $key ?>">
                            <?= $franchise->montant_ttc_deduit == 0 || $franchise->montant_ttc_deduit == '' ? '-' : format_number_($franchise->montant_ttc_deduit) ?>
                        </td>
                        <td class="franchise-view-<?php echo $key ?>">
                            <?= $franchise->franchise_pourcentage == 0 || $franchise->franchise_pourcentage == '' ? '-' : format_number_($franchise->franchise_pourcentage) ?>
                        </td>
                        <td>
                            <button class="only_visuel_gestion updateFranchise btn btn-sm btn-outline-primary franchise-view-<?php echo $key ?>" data-id="<?php echo $franchise->franc_id ?>" data-franc_debut="<?php echo $franchise->franc_debut ?>" data-franc_fin="<?php echo $franchise->franc_fin ?>" data-franc_explication="<?php echo $franchise->franc_explication ?>" data-type_deduction="<?php echo $franchise->type_deduction ?>" data-montant_ht_deduit="<?php echo $franchise->montant_ht ?>" data-tva="<?php echo $franchise->tva ?>" data-montant_ttc_deduit="<?php echo $franchise->montant_ttc_deduit ?>" data-franchise_mode_calcul="<?php echo $franchise->franchise_mode_calcul ?>" data-franchise_pourcentage="<?php echo $franchise->franchise_pourcentage ?>">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                            <button class="only_visuel_gestion btn btn-sm btn-outline-danger removeFranchise franchise-view-<?php echo $key ?>" id="removeFranchise" data-id="<?php echo $franchise->franc_id ?>">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>

        <?php if (count($franchises) === 0) { ?>
            <p class="text-center">Aucune franchise de loyer créée</p>
        <?php } ?>

    </div>
</div>

<style>
    .franc_explication_field div {
        white-space: initial;
        text-align: left;
        word-break: break-word;
    }
</style>

<script>
    only_visuel_gestion();

    $(".updateFranchise").bind("click", function(event) {
        event.preventDefault();
        $('#addfranchisemodal').trigger("click");

        const franc_id = $(this).attr('data-id');
        const franc_debut = $(this).attr('data-franc_debut');
        const franc_fin = $(this).attr('data-franc_fin');
        const franc_explication = $(this).attr('data-franc_explication');
        const montant_ht_deduit = $(this).attr('data-montant_ht_deduit');
        const tva = $(this).attr('data-tva');
        const montant_ttc_deduit = $(this).attr('data-montant_ttc_deduit');
        const type_deduction = $(this).attr('data-type_deduction');
        const franchise_mode_calcul = $(this).attr('data-franchise_mode_calcul');
        const franchise_pourcentage = $(this).attr('data-franchise_pourcentage');

        $("#franc_id").val(franc_id);
        $("#franc_debut").val(franc_debut);
        $("#franc_fin").val(franc_fin);
        $("#franc_explication").val(franc_explication);
        $("#montant_ht_deduit").val(montant_ht_deduit);
        $("#taux_tva_franchise").val(tva);
        $("#type_deduction").val(type_deduction);
        $("#montant_ttc_deduit").val(montant_ttc_deduit);
        $("#franchise_mode_calcul").val(franchise_mode_calcul);
        $("#franchise_pourcentage").val(franchise_pourcentage);
        $('#type_deduction').trigger('change');
        $('#franchise_mode_calcul').trigger('change');
        $('.addNewFranchise').addClass("d-none");
        $('.updateNewFranchise').removeClass("d-none");
    });

    if ($('.bail_action_valid').hasClass('d-none')) {
        $('.removeFranchise').attr('disabled', true);
        $('.updateFranchise').attr('disabled', true);
    } else {
        $('.removeFranchise').attr('disabled', false);
        $('.updateFranchise').attr('disabled', false);
    }
</script>