<input type="hidden" id="indexation_ref_id" value="<?= $indexation_ref->indlo_id ?>" />

<input type="hidden" id="count_indexation_loyer_ref" value="<?= count($indexations) ?>" />

<div class="row mt-4">
    <div class="col">
        <table class="table table-hover fs--1">
            <thead class="text-white">
                <tr class="text-center">
                    <th class="p-2 text-black" style="border-right: 1px solid #ffff;width:15%;">
                        Loyer de référence
                    </th>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                        Appartement
                    </th>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                        Parking
                    </th>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                        Forfait charges
                    </th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <tr>
                    <th class="text-white p-2" style="background:#2569C3;">Montant HT</th>
                    <td class="td-modal">
                        <?php
                        $defaultAppartementHT = 0;
                        if (isset($indexation_ref)) {
                            $defaultAppartementHT = $indexation_ref->indlo_appartement_ht;
                        }
                        ?>
                        <div class="input-group">
                            <input type="number" value="<?= $defaultAppartementHT; ?>" id="indlo_appartement_ht_ref" class="form-control mb-0 input text-right auto_effect input_first_disabled_ref input_loyer_ref">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <?php
                        $defaultParkingHT = 0;
                        if (isset($indexation_ref)) {
                            $defaultParkingHT = $indexation_ref->indlo_parking_ht;
                        }
                        ?>
                        <div class="input-group">
                            <input type="number" value="<?= $defaultParkingHT; ?>" id="indlo_parking_ht_ref" class="form-control mb-0 input text-right auto_effect input_first_disabled_ref input_loyer_ref">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <?php
                        $defaultChargeHT = 0;
                        if (isset($indexation_ref)) {
                            $defaultChargeHT = $indexation_ref->indlo_charge_ht;
                        }
                        ?>
                        <div class="input-group mb-0">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge">-</span>
                            </div>
                            <input type="number" value="<?= $defaultChargeHT; ?>" id="indlo_charge_ht_ref" class="form-control mb-0 input text-right auto_effect input_first_disabled_ref input_loyer_ref">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-white p-2" style="background:#2569C3;">TVA</th>
                    <td class="td-modal">
                        <div class="input-group">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge group-chevron-down">
                                    <span class="fas fa-chevron-down"></span>
                                </span>
                            </div>
                            <select id="indlo_appartement_tva_ref" value="" class="auto_effect form-control mb-0 select text-right input_loyer_ref input_first_disabled_ref">
                                <?php foreach ($tva as $key => $item) : ?>
                                    <option value="<?php echo $item->tva_valeur; ?>" <?= $indexation_ref->indlo_appartement_tva == $item->tva_valeur ? 'selected' : '' ?>>
                                        <?php echo $item->tva_libelle; ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="input-group input_tva_euro">
                            <input type="number" id="indlo_appartement_tva_euro_ref" disabled class="form-control input text-right auto_effect" value="0">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <div class="input-group">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge group-chevron-down">
                                    <span class="fas fa-chevron-down"></span>
                                </span>
                            </div>
                            <select id="indlo_parking_tva_ref" value="" class="auto_effect form-control mb-0 select text-right input_loyer_ref input_first_disabled_ref">
                                <?php foreach ($tva as $key => $item) : ?>
                                    <option value="<?php echo $item->tva_valeur; ?>" <?= $indexation_ref->indlo_parking_tva == $item->tva_valeur ? 'selected' : '' ?>>
                                        <?php echo $item->tva_libelle; ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="input-group input_tva_euro">
                            <input type="number" id="indlo_parking_tva_euro_ref" disabled class="form-control input text-right auto_effect" value="0">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge group-chevron-down">
                                    <span class="fas fa-chevron-down"></span>
                                </span>
                            </div>
                            <select id="indlo_charge_tva_ref" class="auto_effect form-control mb-0 select text-right input_loyer_ref input_first_disabled_ref">
                                <?php foreach ($tva as $key => $item) : ?>
                                    <option value="<?php echo $item->tva_valeur; ?>" <?= $indexation_ref->indlo_charge_tva == $item->tva_valeur ? 'selected' : '' ?>>
                                        <?php echo $item->tva_libelle; ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="input-group input_tva_euro">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge">-</span>
                            </div>
                            <input type="number" id="indlo_charge_tva_euro_ref" disabled class="form-control input text-right auto_effect" value="0">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-white p-2" style="background:#2569C3;">Montant TTC</th>
                    <td class="td-modal">
                        <?php
                        $defaultAppartTTC = 0;
                        if (isset($indexation_ref)) {
                            $defaultAppartTTC = $indexation_ref->indlo_appartement_ttc;
                        }
                        ?>
                        <div class="input-group">
                            <input type="number" id="indlo_appartement_ttc_ref" disabled value="<?= $defaultAppartTTC; ?>" class="form-control mb-0 input text-right auto_effect">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <?php
                        $defaultParkingTTC = 0;
                        if (isset($indexation_ref)) {
                            $defaultParkingTTC = $indexation_ref->indlo_parking_ttc;
                        }
                        ?>
                        <div class="input-group">
                            <input type="number" id="indlo_parking_ttc_ref" disabled value="<?= $defaultParkingTTC; ?>" class="form-control mb-0 input text-right auto_effect">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                    <td class="td-modal">
                        <?php
                        $defaultChargeTTC = 0;
                        if (isset($indexation_ref)) {
                            $defaultChargeTTC = $indexation_ref->indlo_charge_ttc;
                        }
                        ?>
                        <div class="input-group mb-0">
                            <div class="input-group-prepend prepend-charge">
                                <span class="input-group-text group-charge">-</span>
                            </div>
                            <input type="number" id="indlo_charge_ttc_ref" disabled value="<?= $defaultChargeTTC; ?>" class="form-control mb-0 input text-right auto_effect">
                            <div class="input-group-append">
                                <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    updateAppartLoyerRefTable(false);
    updateParkingLoyerRefTable(false);
    updateChargeLoyerRefTable(false);

    $(document).ready(() => {
        if ($('.btn_finish_bail').hasClass('d-none')) {
            $(".input_first_disabled_ref").attr("disabled", true);
        } else {
            $(".input_first_disabled_ref").attr("disabled", false);
        }

        var count_indexation = $("#count_indexation_loyer_ref").val();
        if (parseInt(count_indexation) > 1) {
            // $(".input_loyer_ref").removeClass("input_first_disabled_ref");    
            // $(".input_loyer_ref").attr("disabled", true);    
        }
    })

    $(document).on("click", ".btn_edit_bail", function(event) {
        event.preventDefault();
        var count_indexation = $("#count_indexation_loyer_ref").val();
        // if (parseInt(count_indexation) > 1) return false;
        $(".input_first_disabled_ref").attr("disabled", false);
    });

    $(document).on("click", ".btn_finish_bail", function(event) {
        $(".input_first_disabled_ref").attr("disabled", true);
    });
</script>