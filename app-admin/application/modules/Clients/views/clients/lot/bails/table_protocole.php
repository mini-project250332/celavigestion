<input type="hidden" id="etat_button_protocole" value="<?= $etat_button_protocol ?>">
<?php if ($protocol != NULL) { ?>
    <div class="row">
        <div class="col float-start">
            <h5 class="title_bail">Protocole :</h5>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table class="table table-hover fs--1">
                <thead class="text-white">
                    <tr class="text-center">
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Début
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Fin
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Signé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Type d'abandon
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            % charge
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;" width="35%">
                            Etat du protocole
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Fichier
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-200">
                    <?php foreach ($protocol as $protocols) : ?>
                        <tr>
                            <td class="text-center"><?= date('d/m/Y', strtotime($protocols->date_debut_protocole)) ?></td>
                            <td class="text-center"><?= date('d/m/Y', strtotime($protocols->date_fin_protocole)) ?></td>
                            <td class="text-center"><?= date('d/m/Y', strtotime($protocols->date_sign_protocole)) ?></td>
                            <td class="text-center"><?= $protocols->type_abandon == 0 ? 'Abandon Total' : 'Abandon Partiel' ?></td>
                            <td class="text-center"><?= $protocols->pourcentage_protocol ?></td>
                            <td class="text-center"><?= $protocols->etat_protocol == 0 ? 'Non validé' : 'Validé' ?></td>
                            <td class="text-center">
                                <span class="badge rounded-pill badge-soft-warning" title="Télécharger fichier" onclick="forceDownload('<?= base_url() . $protocols->doc_path ?>')" data-url="<?= base_url($protocols->doc_path) ?>" style="cursor: pointer; font-size : 12px;"><?= 'Télécharger' ?></span>
                            </td>
                            <td class="text-center">
                                <?php if ($protocols->etat_protocol == 0) : ?>
                                    <button class="btn btn-sm btn-outline-warning"  title="Modifier Protocole" id="modifierProtocol" data-id="<?= $protocols->id_protocol ?>"><span class="bi bi-pencil-square fs--1"></span></button>
                                    <button class="btn btn-sm btn-outline-info" title="Demander la Validation par email" id="envoiMailProtocol" data-id="<?= $protocols->id_protocol ?>"><span class="fas fa-envelope fs--1"></span></button>
                                    <button class="btn btn-sm btn-outline-success only_visuel_gestion" title="Valider Protocole" id="validerProtocol" data-id="<?= $protocols->id_protocol ?>"><span class="fas fa-check fs--1"></span></button>
                                <?php else : ?>
                                    <button class="btn btn-sm btn-outline-primary" title="Consulter Protocole" id="consulterProtocol" data-id="<?= $protocols->id_protocol ?>"><span class="fas fa-eye fs--1"></span></button>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <hr class="my-0">
<?php } ?>

<script>
    only_visuel_gestion();

    $(document).ready(function() {
        if ($('#etat_button_protocole').val() == 1) {
            $('#btn_new_protocole').attr('disabled', false);
            $('#btn_demande_valider_protocol').attr('disabled', true);
        } else {
            $('#btn_new_protocole').attr('disabled', true);
            $('#btn_demande_valider_protocol').attr('disabled', false);
        }
    });
</script>