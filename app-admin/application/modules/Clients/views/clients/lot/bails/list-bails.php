<input type="hidden" value="<?php echo $lot->gestionnaire_id ?>" id="first_gestionnaire_id" />
<input type="hidden" id="isAvenant" value="0">
<?php if (isset($mandat) && !empty($mandat)) : ?>
    <input type="hidden" id="mandat_cli_date_signature" value="<?= $mandat->mandat_date_signature ?>">
    <input type="hidden" id="etat_mandat_id" value="<?= isset($mandat->etat_mandat_id) ? $mandat->etat_mandat_id : "" ?>">
    <input type="hidden" id="mandat_date_fin" value="<?= isset($mandat->mandat_date_fin) ? $mandat->mandat_date_fin : "" ?>">
<?php endif ?>
<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col-3">
            <h5 class="fs-1">Documents</h5>
        </div>
        <?php
        $date_now = date('Y-m-d');
        $date_fin_mandat = isset($mandat) && !empty($mandat) && $mandat->mandat_date_fin != NULL ? $mandat->mandat_date_fin : "";
        $mandat_date_signature = isset($mandat) && !empty($mandat) && $mandat->mandat_date_signature != NULL ? $mandat->mandat_date_signature : '';
        ?>
        <div class="col-6 pt-1 <?= !empty($mandat) && isset($mandat->etat_mandat_id) ? ($mandat->etat_mandat_id == 7 ? '' : 'd-none') : 'd-none' ?>">
            <div class="alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                <?php if (isset($mandat) && !empty($mandat)) : ?>
                    Le mandat pour ce lot a été suspendu par <?= $mandat->util_prenom ?> le <?= date("d/m/Y", strtotime(str_replace('/', '-', $mandat->mandat_date_supension))) ?> pour la raison suivante : "<?= $mandat->mandat_motif_suspension ?>". Vous ne pouvez plus modifier ce lot
                <?php endif ?>
            </div>
        </div>
        <div class="col-6">
            <?php if ($date_fin_mandat != "") {
                if ($date_fin_mandat >= $date_now) {
                    echo '<div class= "alert alert-warning text-center m-0 p-0 fs--1" role="alert">
                            Attention, le mandat sera clôturé le ' . date("d/m/Y", strtotime(str_replace('/', '-', $date_fin_mandat))) . '
                        </div>';
                } else if ($date_fin_mandat < $date_now) {
                    echo '<div class= "alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                            Attention, le mandat est clôturé depuis le ' . date("d/m/Y", strtotime(str_replace('/', '-', $date_fin_mandat))) . '
                        </div>';
                }
            } elseif ($mandat_date_signature == '') {
                echo '<div class="alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                    aucune mandat signé
                </div>';
            } ?>
        </div>

        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 only_visuel_gestion <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" data-id="<?= $cliLot_id ?>" id="ajoutDocBail">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left" style="width: 848px;">
        <div class="row m-0">
            <div class="col-3 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Protocole</label>
            </div>
            <div class="col-3 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0" style=" height: calc(25vh - 60px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
            <div class=" col-12">
                <?php foreach ($document as $documents) { ?>
                    <div class="row apercu_doc" id="docrowbail-<?= $documents->doc_bail_id ?>">
                        <div class="col-3 pt-2">
                            <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_bail_id ?>" onclick="apercuDocumentBail(<?= $documents->doc_bail_id ?>)">
                                <?= $documents->doc_bail_nom ?>
                            </div>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-user"></i>
                                <?= $documents->util_prenom ?>
                            </label>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-calendar"></i>
                                <?= $documents->doc_bail_creation ?>
                            </label>
                        </div>

                        <div class="col-2 text-center">
                            <?= $documents->doc_protocole == 1 ? 'Oui' : 'Non' ?>
                        </div>

                        <div class="col-3 text-center">
                            <div class="btn-group fs--1">
                                <?php
                                $message = "Document non traité";
                                if (!empty($documents->doc_bail_traitement)) {
                                    if ($documents->doc_bail_traite == 1) {
                                        $message = "Document traité par " . $documents->util_prenom . " le " . $documents->doc_bail_traitement;
                                    } else {
                                        $message = "Document annulé par " . $documents->util_prenom . " le " . $documents->doc_bail_traitement;
                                    }
                                }
                                ?>
                                <button class="btn btn-sm <?= ($documents->doc_bail_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill  btnTraiteBail" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>" data-id=" <?= $documents->doc_bail_id ?>">
                                    <i class="fas fa-check"></i>
                                </button> &nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocBail" data-id="<?= $documents->doc_bail_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer" data-lot="<?= $cliLot_id; ?>">
                                    <i class="far fa-edit"></i>
                                </span>&nbsp;
                                <span class="btn btn-sm btn-outline-primary rounded-pill">
                                    <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_bail_path ?>','<?= $documents->doc_bail_nom ?>')" data-url="<?= base_url($documents->doc_bail_path) ?>">
                                    </i>
                                </span>&nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocBail" data-id="<?= $documents->doc_bail_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                    <i class="far fa-trash-alt"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <hr class="my-0">
        <br>

        <div class="tableau">
            <div class="row aucun_bail">
                <div class="col-12 text-center">
                    <div class="alert alert-secondary text-center" role="alert">
                        <div class="text-center">
                            Aucun Bail n'a été crée pour ce lot.
                        </div>
                        <div class="text-center only_visuel_gestion">
                            Voulez-vous créer un premier bail ?
                        </div>
                        <div>
                            <button class="btn btn-sm btn-primary mt-2 <?= isset($mandat->etat_mandat_id) ? (($mandat->etat_mandat_id == 6) ? 'd-none' : '') : "" ?> only_visuel_gestion" id="<?= $mandat_date_signature == '' ? 'btn_bail_alert' : 'btn_creer_bail' ?>">
                                Renseigner les caractéristiques du bail
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" id="countIndexation" />

            <div class="detail_bail" style="display: none;">
                <div class="row mb-2">
                    <div class="col-7">
                        <!-- Bail en cours ou tacite du 24/10/2022 -->
                        <input id="bail_id" type="hidden">
                        <div class="d-flex flex-row align-items-center">
                            <h5 class="title_bail" id="tacite_en_cours"></h5>
                            <div id="bail_validation"></div>
                        </div>
                    </div>
                    <div class="col-5 text-right" style="display: flex; justify-content: end">
                        <div class="contain_btn_action_bail contain_btn_edit_bail">
                            <button class="btn btn-sm btn-primary mx-2 button_modif <?= $mandat_date_signature != '' ? 'btn_edit_bail' : '' ?>  only_visuel_gestion 
                            <?= isset($mandat->etat_mandat_id) ?
                                ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ?
                                    'd-none' : '') : "" ?>" id="<?= $mandat_date_signature == '' ? 'btn_bail_alert' : '' ?>">
                                Modifier
                            </button>
                            <button class="btn btn-sm btn-primary mx-2 d-none btn_finish_bail only_visuel_gestion">
                                Terminer
                            </button>
                        </div>
                        <div class="contain_btn_action_bail">
                            <div class="dropdown">
                                <!-- Icon Button -->
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropDownBailOption" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </button>
                                <!-- Dropdown Menu -->
                                <div class="dropdown-menu" aria-labelledby="dropDownBailOption">
                                    <button class="dropdown-item" id="btn_history_bail" data-bs-toggle="modal" data-bs-target="#modal_historique_bail" data-backdrop="static" data-keyboard="false">
                                        Historique du bail
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="btn_new_bail">
                                        Créer un nouveau bail
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="btn_new_avenant">
                                        Enregistrer un avenant
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="btn_new_protocole">
                                        Enregistrer un protocole
                                    </button>
                                    <button class="dropdown-item bail_action_cloture btn_bail_cloture <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>">
                                        Clôturer le bail
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="btn_demande_cloture" data-cliLot_id="<?= $cliLot_id ?>">
                                        Demander la validation par mail pour clôturer le bail
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="btn_demande_creer_avenant" data-cliLot_id="<?= $cliLot_id ?>">
                                        Demander la validation par mail pour créer un avenant
                                    </button>
                                    <button class="dropdown-item <?= isset($mandat->etat_mandat_id) ? (($mandat->etat_mandat_id == 6) ? 'd-none' : '') : "" ?>" id="btn_demande_valider_protocol" data-cliLot_id="<?= $cliLot_id ?>">
                                        Demander la validation par mail d'un protocole
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" value="" id="cliLot_id" />

                <div class="row mt-2">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Preneur à bail :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" style="margin-left: 4%;" id="preneur_id">
                                    <?php foreach ($gestionnaire as $gestionnaires) : ?>
                                        <option value="<?= $gestionnaires->gestionnaire_id ?>">
                                            <?= $gestionnaires->gestionnaire_nom ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- <div class="">
                                    <button type="button" class="btn btn-sm btn-primary" style="height: 32px"><i class="fas fa-plus-circle"></i></button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Type de bail :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" style="margin-left: 8%;" id="type_bail_id">
                                    <?php foreach ($type_bail as $type) : ?>
                                        <option value="<?= $type->tpba_id ?>">
                                            <?= $type->tpba_description ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Exploitant (Gestionnaire) :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" style="margin-left: 4%;" id="gestionnaire_id_bail">
                                    <?php foreach ($gestionnaire as $gestionnaires) : ?>
                                        <option value="<?= $gestionnaires->gestionnaire_id ?>">
                                            <?= $gestionnaires->gestionnaire_nom ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label>Date de première facturation<br>par Celavigestion :</label>
                                <input type="date" class="form-control input_first_disabled auto_effect" id="bail_date_echeance">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                            <div class="col-lg-5">
                                <label>Date début mission</label>
                            </div>
                            <div class="col-lg-7">
                                <input type="date" class="form-control input_first_disabled auto_effect" id="bail_date_debut_mission" disabled>
                            </div>
                        </div>
                     <div class="row">
                            <div class="col-lg-6">
                                <label>Date début de signature mandat :</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="date" class="form-control auto_effect" id="date_signature_mandat" value="<?= $mandat->mandat_cli_date_signature ?>" disabled>
                            </div>
                        </div> -->
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Nature de la prise d'effet :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="nature_prise_effet_id">
                                    <?php foreach ($nature_prise_effet as $nature) : ?>
                                        <option value="<?= $nature->natef_id ?>">
                                            <?= $nature->natef_description ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Date de la prise d'effet :</label>
                                <input type="date" class="form-control input_first_disabled auto_effect" id="bail_debut">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Durée :</label>
                                <select id="bail_annee" class="form-select input_first_disabled auto_effect" aria-label="Default select example" style="margin-left: 29.5%">
                                    <option value="0">0 an</option>
                                    <option value="1">1 an</option>
                                    <option value="2">2 ans</option>
                                    <option value="3">3 ans</option>
                                    <option value="4">4 ans</option>
                                    <option value="5">5 ans</option>
                                    <option value="6">6 ans</option>
                                    <option value="7">7 ans</option>
                                    <option value="8">8 ans</option>
                                    <option value="9">9 ans</option>
                                    <option value="10">10 ans</option>
                                    <option value="11">11 ans</option>
                                    <option value="12">12 ans</option>
                                    <option value="13">13 ans</option>
                                    <option value="14">14 ans</option>
                                    <option value="15">15 ans</option>
                                    <option value="16">16 ans</option>
                                    <option value="17">17 ans</option>
                                    <option value="18">18 ans</option>
                                    <option value="19">19 ans</option>
                                    <option value="20">20 ans</option>
                                </select>
                                <select id="bail_mois" class="form-select input_first_disabled auto_effect" aria-label="Default select example">
                                    <option value="0">0 mois</option>
                                    <option value="1">1 mois</option>
                                    <option value="2">2 mois</option>
                                    <option value="3">3 mois</option>
                                    <option value="4">4 mois</option>
                                    <option value="5">5 mois</option>
                                    <option value="6">6 mois</option>
                                    <option value="7">7 mois</option>
                                    <option value="8">8 mois</option>
                                    <option value="9">9 mois</option>
                                    <option value="10">10 mois</option>
                                    <option value="11">11 mois</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Date fin :</label>
                                <input type="date" class="form-control auto_effect" disabled id="bail_fin" style="margin-left: 5%;" id="bail_fin">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Tacite prolongation :</label>
                                <input type="text" class="form-control auto_effect" value="" style="margin-left: 8.5%;" id="tacite_prolongation" disabled>
                                <!-- disabled -->
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <label data-width="250">Résiliation triennale :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" style="margin-left: 4.5%;" id="bail_resiliation_triennale">
                                    <option value="1">oui</option>
                                    <option value="0">non</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: -1%;">
                    <div class="col-10 text-start">
                        <h5 class="title_bail">Franchise de loyers :</h5>
                    </div>
                    <div class="col-2 text-end" style="margin-top: 0%;">
                        <div class="contain_addfranchisemodal">
                            <button class="btn btn-sm btn-primary <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?> only_visuel_gestion" data-bs-toggle="modal" data-bs-target="#modal_franchise" style="margin-top:12%;" id="addfranchisemodal">
                                Ajouter
                            </button>
                        </div>
                    </div>
                </div>

                <div id="container-franchise-loyer">

                </div>

                <div class="row mt-2">
                    <div class="col">
                        <h5 class="title_bail">Informations sur les loyers :</h5>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Périodicité des loyers :</label>
                            </div>

                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="periodicite_loyer_id">
                                    <?php foreach ($periodicite_loyer as $periode) : ?>
                                        <option value="<?= $periode->pdl_id ?>">
                                            <?= $periode->pdl_description ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row d-none" id="1er_trimes_decal_affichage">
                            <div class="col-lg-5">
                                <label>1er mois de 1er trimestre :</label>
                            </div>

                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" id="premier_mois_trimes">
                                    <?php $disabled_month = array(1, 4, 7, 10); ?>
                                    <?php foreach ($periode_mensuel as $p) : ?>
                                        <option value="<?= $p->periode_mens_id ?>" <?= in_array($p->periode_mens_id, $disabled_month) ? 'disabled' : '' ?>><?= $p->periode_mens_libelle ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 11px">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Période d'indexation :</label>
                            </div>
                            <div class="col-lg-7">
                                <select id="periode_indexation" class="form-select input_first_disabled periode_indexation auto_effect" aria-label="Default select example">
                                    <?php foreach ($periode_indexation as $periode_index) : ?>
                                        <?php if ($periode_index->prdind_id == 3) :  ?>
                                            <option data-id="<?= $periode_index->prdind_valeur ?>" value="<?= $periode_index->prdind_id ?>">
                                                Pas d'indexation
                                            </option>
                                        <?php else : ?>
                                            <option data-id="<?= $periode_index->prdind_valeur ?>" value="<?= $periode_index->prdind_id ?>">
                                                <?= $periode_index->prdind_valeur ?> an<?php if (intval($periode_index->prdind_valeur) > 1)
                                                                                            echo 's'; ?>
                                            </option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Date de paiement :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="type_echeance_id">
                                    <?php foreach ($type_echeance as $tp_echeance) : ?>
                                        <option value="<?= $tp_echeance->tpech_id ?>">
                                            <?= $tp_echeance->tpech_valeur ?> <?php if ($tp_echeance->tpech_valeur == 1)
                                                                                    echo '<sup style="position: relative; left: -3px;">er</sup>' ?>&nbsp;du mois
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Date de la première indexation :</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="date" class="form-control input_first_disabled auto_effect m-0" id="bail_date_premiere_indexation">
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Moment de paiement :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="moment_facturation_bail_id">
                                    <?php foreach ($moment_facturation as $moment_factu) : ?>
                                        <option value="<?= $moment_factu->momfac_id ?>">
                                            <?= $moment_factu->momfact_nom ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: -15px">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Indices utilisés :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="indice_loyer_id">
                                    <option class="non_applicable d-none" value="non_applicable">Non applicable</option>
                                    <?php foreach ($indice_loyer as $indice) : ?>
                                        <option class="applicable" value="<?= $indice->indloyer_id ?>">
                                            <?= $indice->indloyer_description ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Date de la première indexation <br>
                                    par Celavigestion : </label>
                            </div>
                            <div class="col-lg-6">
                                <input type="date" class="form-control auto_effect" id="bail_date_prochaine_indexation" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Nature du loyer :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="bail_loyer_variable">
                                    <option value="0">Fixe</option>
                                    <option value="1" selected>Fixe et variable</option>
                                    <option value="2">Variable</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row d-none" id="mod_calcul">
                            <div class="col-lg-5">
                                <label>Mode de calcul :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="bail_mode_calcul">
                                    <option value="0">Variation entre deux indices (standard)</option>
                                    <option value="1">Variation sur les moyennes annuelles sur 3 ans</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col">
                        <div class="row">
                            <div class="col-lg-7">
                                <label>Forfait charges déductible indexé :</label>
                            </div>
                            <div class="col-lg-5">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="bail_forfait_charge_indexer">
                                    <option value="1" selected>oui</option>
                                    <option value="0">non</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">
                            <div class="col-lg-5">
                                <label>Indices de référence :</label>
                            </div>
                            <div class="col-lg-7">
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="indice_valeur_loyer_id">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="contain_tab_loyer_reference">

                </div>

                <div class="row mb-2">
                    <div class="main_indexation_loyer">
                        <div class="col-12 d-flex justify-content-between">
                            <div class="text-start">
                                <h5 class="title_bail">Indexation de loyers :</h5>
                            </div>
                            <div class="text-end">
                                <!-- <button class="btn btn-sm btn-primary only_visuel_gestion" data-bs-toggle="modal" data-bs-target="#modal_indexation_loyer" id="addindexationmodal" data-backdrop="static" data-keyboard="false">Ajouter</button> -->
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <label>Variation plafonnée :</label>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="input-group">
                                            <input type="number" id="bail_var_plafonnee" class="input_first_disabled auto_effect form-control input" placeholder="0">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <label>Augmentation capée :</label>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="input-group">
                                            <input type="number" id="bail_var_capee" class="input_first_disabled auto_effect form-control input" placeholder="0" maxlength="3" max="100">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <label>Augmentation minimum :</label>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="input-group">
                                            <input type="number" id="bail_var_min" class="input_first_disabled auto_effect form-control input" maxlength="3" min="-100" max="100">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <label for="">Indice de plafonnement :</label>
                                    </div>
                                    <div class="col-lg-7">
                                        <select class="form-select input_first_disabled auto_effect" id="indice_valeur_plafonnement">

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="top:-19px">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="text-end">
                                    <button class="btn btn-sm btn-primary mt-2 <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="generer_indexation">Générer les indexations de loyers</button>
                                </div>
                            </div>
                        </div>

                        <div id="container-indexation-loyer">

                        </div>
                    </div>
                </div>

                <br>
                <hr class="my-0">

                <div class="row">
                    <div class="col">
                        <div id="container-protocol">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col float-start">
                        <h5 class="title_bail">Actions disponibles :</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col float-start">
                        <div class="form-check">
                            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="bail_remboursement_tom">
                            <label class="form-check-label" for="bail_remboursement_tom">
                                Remboursement de la TOM
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="bail_facturation_loyer_celaviegestion">
                            <label class="form-check-label" for="bail_facturation_loyer_celaviegestion">
                                Facturation des loyers par CELAVI Gestion
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="bail_charge_recuperable">
                            <label class="form-check-label" for="bail_charge_recuperable">
                                Charges récupérables
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col float-start">
                        <h5 class="title_bail">Autres informations :</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Art 605 :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="bail_art_605_id">
                                    <?php foreach ($bail_art as $bail_605) : ?>
                                        <option value="<?= $bail_605->bart_id ?>">
                                            <?= $bail_605->bart_nom ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">

                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <br>
                                <label data-width="250">Art 606 :</label>
                                <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="bail_art_606_id">
                                    <?php foreach ($bail_art as $bail_606) : ?>
                                        <option value="<?= $bail_606->bart_id ?>">
                                            <?= $bail_606->bart_nom ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col">

                    </div>

                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label data-width="250">Commentaire :</label>
                            <textarea class="form-control input_first_disabled auto_effect" id="bail_commentaire" rows="5" style="height: 50%;"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-start">
                        <h5 class="title_bail">Actions sur le Bail :</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col text-start">
                        <!-- <h5 class="title_bail">Historique des actions :</h5> -->
                        <input type="hidden" id="siret_dossier" value="<?= $check_siret_tva->dossier_siret == NULL ? 1 : 0 ?>">
                        <input type="hidden" id="tvaintracommunautaire_dossier" value="<?= $check_siret_tva->dossier_tva_intracommunautaire == NULL ? 1 : 0 ?>">

                        <?php if ($check_siret_tva->dossier_siret == NULL || ($tva_lot == 'REEL SIMPLIFIE' && $check_siret_tva->dossier_tva_intracommunautaire == NULL)) {
                            $button_valide_class = 'alert_tva_siret';
                            $button_demande_class = 'alert_tva_siret';
                        } elseif ($check_siret_tva->dossier_siret == NULL || ($tva_lot == 'REEL NORMAL' && $check_siret_tva->dossier_tva_intracommunautaire == NULL)) {
                            $button_valide_class = 'alert_tva_siret';
                            $button_demande_class = 'alert_tva_siret';
                        } else {
                            $button_valide_class = 'btn_bail_validation';
                            $button_demande_class = 'ask_bail_validation_in_bail';
                        } ?>

                        <!-- <button class="bail_actions btn btn-default bail_action_valid <?= $button_valide_class ?> bail_validation_contain" data-valid="1">Valider le bail</button> -->
                        <!-- <button class="bail_actions btn btn-primary <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="<?= $button_demande_class ?>" data-cli_lot_id="<?= $cliLot_id ?>" data-bail_id="<?= isset($bail_id) ? $bail_id : '' ?>">Demander la validation du bail par mail</button> -->
                        <button class="bail_actions btn btn-default bail_action_valid btn_bail_validation" data-valid="1">Valider le bail</button>
                        <button class="bail_actions btn btn-default bail_action_devalid btn_bail_validation bail_validation_contain <?= isset($mandat->etat_mandat_id) ? (($mandat->etat_mandat_id == 6) ? 'd-none' : '') : "" ?>" data-valid="0">Annuler la validation du bail</button>
                        <button class="bail_actions btn btn-primary <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" id="ask_bail_validation_in_bail" data-cli_lot_id="<?= $cliLot_id ?>" data-bail_id="<?= isset($bail_id) ? $bail_id : '' ?>">Demander la validation du bail par mail</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=" split m-2"></div>

    <div class="col-6 panel-right">
        <div id="apercudocBail">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true">Aperçu non disponible pour ce type de
                        fichier</i>
                </div>
            </div>
            <div id="apercuBail">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal doc -->

<div class="modal fade" id="modalAjoutBail" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadBail">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resend_modal_validation_bail_mail_in_bail" tabindex="-1" aria-labelledby="resend_modal_validation_bail_mail" aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header header-confirm-indexation">
                Confirmation de renvoi d'email
            </div>
            <div class="modal-body body-confirm-indexation">
                Voulez-vous renvoyer le mail de demande de validation aux administrateurs ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="cancelNewIndexationLoyer_in_bail">
                    Annuler la validation du bail
                </button>
                <button type="button" class="btn btn-primary" id="confirmResendModalValidationBailMail">Réenvoyer</button>
            </div>
        </div>
    </div>
</div>

<script>
    const delayTime = 1000;
    let timeoutId;

    only_visuel_gestion(["1", "2"], "bail_validation_contain");

    $(".panel-left").resizable({
        handleSelector: ".split",
        resizeHeight: false,
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function() {
        getBail($('#lot_client_id').val());
        $(".input_first_disabled").attr("disabled", true);
        getModalIndexationLoyer($('#lot_client_id').val());
        getProtocoleBail($('#lot_client_id').val());
    });

    $(document).on("click", ".btn_edit_bail", function(event) {
        event.preventDefault();
        $(".btn_finish_bail").removeClass('d-none');
        $(".btn_edit_bail").addClass('d-none');
        $(".input_first_disabled").attr("disabled", false);

        //Disable Indice
        const countIndexation = $("#countIndexation").val();
    })

    $(document).on('click', '#addfranchisemodal', function() {
        const date = new Date();
        const year = date.getFullYear();
        const month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        const today = year + '-' + month + '-' + day;

        $('.help_montant_ttc_deduit').empty();

        $('#franc_debut').val(today);
        $('#franc_fin').val(today);
        $('#franc_explication').val('');
        $('#type_deduction').val(1);
        $('#montant_ht_deduit').val('');
        $('#taux_tva_franchise').val(0);
        $('#montant_ttc_deduit').val('');
        $('#type_deduction').trigger('change');

        $('.addNewFranchise').removeClass("d-none");
        $('.updateNewFranchise').addClass("d-none");
    });

    $(".btn_finish_bail").bind("click", function(event) {
        event.preventDefault();
        $(".btn_edit_bail").removeClass('d-none');
        $(".btn_finish_bail").addClass('d-none');
        $(".input_first_disabled").attr("disabled", true);
    })

    function updateBailInfo() {
        var bail_remboursement_tom = $('#bail_remboursement_tom').is(':checked') ? 1 : 0;
        var bail_facturation_loyer_celaviegestion = $('#bail_facturation_loyer_celaviegestion').is(':checked') ? 1 : 0;
        var bail_charge_recuperable = $('#bail_charge_recuperable').is(':checked') ? 1 : 0;
        setTimeout(() => {
            $(".input_first_disabled_ref").attr("disabled", false);
        }, 1500);

        addBail(
            $('#bail_id').val(),
            $('#bail_debut').val(),
            $('#bail_fin').val(),
            $('#bail_resiliation_triennale').val(),
            $('#bail_loyer_variable').val(),
            bail_remboursement_tom,
            bail_facturation_loyer_celaviegestion,
            bail_charge_recuperable,
            $('#bail_date_premiere_indexation').val(),
            $('#bail_date_prochaine_indexation').val(),
            $('#bail_commentaire').val(),
            $('#bail_annee').val(),
            $('#bail_mois').val(),
            $('#periodicite_loyer_id').val(),
            $('#nature_prise_effet_id').val(),
            $('#type_bail_id').val(),
            $('#lot_client_id').val(),
            $('#gestionnaire_id_bail').val(),
            $('#type_echeance_id').val(),
            $('#bail_art_605_id').val(),
            $('#bail_art_606_id').val(),
            $('#indice_loyer_id').val(),
            $('#moment_facturation_bail_id').val(),
            $('#periode_indexation').val(),
            $('#bail_forfait_charge_indexer').val(),
            $('#bail_date_echeance').val(),
            $('#preneur_id').val(),
            $('#bail_indexation').val(),
            $('#indice_valeur_loyer_id').val(),
            $('#bail_var_capee').val(),
            $('#bail_var_plafonnee').val(),
            $('#bail_var_appliquee').val(),
            0,
            null,
            $('#premier_mois_trimes').val(),
            $('#bail_date_debut_mission').val(),
            $('#bail_var_min').val(),
            $('#bail_mode_calcul').val(),
            $('#indice_valeur_plafonnement').val()
        );
    }

    $("#btn_creer_bail").one("click", function(event) {
        event.preventDefault();
        $(this).prop('disabled', true);
        $(".aucun_bail").hide();
        $(".detail_bail").show();

        let currentDate = new Date();
        currentDate = new Date(currentDate.setMonth(currentDate.getMonth() + 12));

        addBail(
            null,
            convertDate(new Date()),
            convertDate(currentDate),
            0,
            0,
            0,
            0,
            0,
            convertDate(new Date()),
            convertDate(currentDate),
            '',
            1,
            0,
            1,
            1,
            1,
            $('#lot_client_id').val(),
            $('#first_gestionnaire_id').val(),
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            convertDate(new Date()),
            $('#first_gestionnaire_id').val(),
            1,
            null,
            null,
            null,
            0,
            0
        );
    });

    $("#bail_annee").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        updateBailDateFin();
    })

    $("#bail_mois").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        updateBailDateFin();
    })

    $("#bail_debut").bind("change", function(event) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            if (!$("#bail_date_premiere_indexation").val()) {
                $("#bail_date_premiere_indexation").val(event.target.value);
                $("#bail_date_premiere_indexation").trigger("change");
            }
            updateBailDateFin();
        }, delayTime);
    });

    $("#bail_date_premiere_indexation").bind("change", function(event) {
        const bail_debut = $('#bail_debut').val();
        const date_1er_index = $(this).val();

        var d = new Date(bail_debut); // Create a new Date object from the input date
        var formattedDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();

        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            updateDateProchaineIndexation();
        }, delayTime);
    })

    $("#periode_indexation").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        var periode_indexation = $(this).val();
        var indice_loyer_id = $('#indice_loyer_id').val();
        display_modecalcul_bail();

        if (periode_indexation != 3) {
            $('#bail_date_premiere_indexation').prop('disabled', false);
            $('#bail_date_premiere_indexation').attr('type', 'date');

            $('#bail_date_prochaine_indexation').attr('type', 'date');

            $('#indice_loyer_id').prop('disabled', false);
            if ($('.applicable').hasClass('d-none') && !$('.non_applicable').hasClass('d-none')) {
                $('.non_applicable').addClass('d-none');
                $('.applicable').removeClass('d-none');
                $('#indice_loyer_id').val(1);
                $('#indice_valeur_loyer_id').val(0);
            }

            $('#indice_valeur_loyer_id').prop('disabled', false);
            $('#generer_indexation').prop('disabled', false);

            if (!$('#bail_date_premiere_indexation').hasClass('input_first_disabled') && !$('#indice_valeur_loyer_id').hasClass('input_first_disabled') && !$('#indice_loyer_id').hasClass('input_first_disabled')) {
                $('#bail_date_premiere_indexation').addClass('input_first_disabled');
                $('#indice_loyer_id').addClass('input_first_disabled');
                $('#indice_valeur_loyer_id').addClass('input_first_disabled');
            }
        }
        updateDateProchaineIndexation();
    });

    function display_modecalcul_bail() {
        if ($('#periode_indexation') == 2 && $('#mod_calcul').hasClass('d-none') && $('#indice_loyer_id').val() != 7) {
            $('#mod_calcul').removeClass('d-none');
        } else {
            if (!$('#mod_calcul').hasClass('d-none')) {
                $('#mod_calcul').addClass('d-none');
            }
        }
    }

    function updateBailDateFin() {
        const bail_annee = $("#bail_annee").val();
        const bail_mois = $("#bail_mois").val();
        const bail_debut = $("#bail_debut").val();
        const date_debut = new Date(bail_debut);
        let date_fin = new Date(date_debut.setFullYear(date_debut.getFullYear() + parseInt(bail_annee)));
        date_fin = new Date(date_fin.setMonth(date_fin.getMonth() + parseInt(bail_mois)));
        const monthly = date_fin.getMonth() + 1;
        const month = monthly < 10 ? '0' + monthly : monthly;
        const date = date_fin.getDate() < 10 ? '0' + date_fin.getDate() : date_fin.getDate();
        $("#bail_fin").val(`${date_fin.getFullYear()}-${month}-${date}`);

        setTimeout(() => {
            updateBailInfo();
        }, 3000);
    }

    function updateDateProchaineIndexation() {
        const date_premiere_indexation = $("#bail_date_premiere_indexation").val();
        const mandat_cli_date_signature = $('#mandat_cli_date_signature').val();
        var etat_mandat_id = $('#etat_mandat_id').val();

        if (mandat_cli_date_signature == '0000-00-00' || mandat_cli_date_signature == null || mandat_cli_date_signature == '' || etat_mandat_id == 5) {
            Notiflix.Notify.failure("Vous devez remplir la date de signature de mandat dans le menu mandat", {
                position: 'left-bottom',
                timeout: 3000
            });
        } else {
            const periode_indexation = $("#periode_indexation option:selected").attr("data-id");
            var premiere_indexation_date = new Date(date_premiere_indexation);
            var mandat_signature = new Date(mandat_cli_date_signature);
            var month = (premiere_indexation_date.getMonth() + 1).toString().padStart(2, '0');
            var day = premiere_indexation_date.getDate().toString().padStart(2, '0');
            var month_mandat = (mandat_signature.getMonth() + 1).toString().padStart(2, '0');
            var day_mandat = mandat_signature.getDate().toString().padStart(2, '0');
            var year = parseInt(mandat_signature.getFullYear());
            var year_first_index = parseInt(premiere_indexation_date.getFullYear());

            if (periode_indexation == 1) {
                var first_indexation_date = new Date(date_premiere_indexation);
                for (; first_indexation_date < mandat_signature; first_indexation_date.setFullYear(first_indexation_date.getFullYear() + 1)) {}
                var year_fist = first_indexation_date.getFullYear();
                var month_first = String(first_indexation_date.getMonth() + 1).padStart(2, '0');
                var day_first = String(first_indexation_date.getDate()).padStart(2, '0');
                var formattedDate = year_fist + '-' + month_first + '-' + day_first;
            } else {
                var first_indexation_date = new Date(date_premiere_indexation);

                for (; first_indexation_date < mandat_signature; first_indexation_date.setFullYear(first_indexation_date.getFullYear() + 3)) {}
                var year_fist = first_indexation_date.getFullYear();
                var month_first = String(first_indexation_date.getMonth() + 1).padStart(2, '0');
                var day_first = String(first_indexation_date.getDate()).padStart(2, '0');
                var formattedDate = year_fist + '-' + month_first + '-' + day_first;
            }

            const date1 = new Date(date_premiere_indexation);
            const date2 = new Date(formattedDate);

            if (date1 > date2) {
                formattedDate = date_premiere_indexation;
            }

            $("#bail_date_prochaine_indexation").val(formattedDate);
            updateLoyerReference(true, formattedDate);
        }
    }

    $("#gestionnaire_id_bail").bind("change", function(event) {
        updateBailInfo();
    });

    $("#preneur_id").bind("change", function(event) {
        updateBailInfo();
    });

    $("#nature_prise_effet_id").bind("change", function(event) {
        updateBailInfo();
    });

    $("#periodicite_loyer_id").bind("change", function(event) {
        if ($(this).val() == 5) {
            $('#1er_trimes_decal_affichage').removeClass('d-none');
        } else {
            $('#1er_trimes_decal_affichage').removeClass('d-none');
            $('#1er_trimes_decal_affichage').addClass('d-none');
        }
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#bail_resiliation_triennale").bind("change", function(event) {
        updateBailInfo();
    });

    $("#type_echeance_id").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#moment_facturation_bail_id").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#bail_mode_calcul").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#indice_valeur_plafonnement").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#bail_loyer_variable").bind("change", function(event) {
        const val = event.target.value;
        if (val === "2") {
            $(".main_indexation_loyer").hide();
            $("#bail_forfait_charge_indexer").val("0");
            $("#bail_forfait_charge_indexer").attr("disabled", true);
        } else {
            $(".main_indexation_loyer").show();
            $("#bail_forfait_charge_indexer").attr("disabled", false);
        }
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#bail_indexation").bind("change", function(event) {
        updateBailInfo();
    });

    // Test *****
    $("#bail_date_echeance").bind("change", function(event) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            updateBailInfo();
        }, delayTime);
    });

    // End Test *****
    $("#bail_forfait_charge_indexer").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#type_bail_id").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_remboursement_tom").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_facturation_loyer_celaviegestion").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_charge_recuperable").bind("change", function(event) {
        updateBailInfo();
    });

    $("#indice_loyer_id").bind("change", function(event) {
        const countIndexation = $("#countIndexation").val();
        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            updateBailInfo();
        }
    });

    $("#bail_art_605_id").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_art_606_id").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_commentaire").bind("change", function(event) {
        updateBailInfo();
    });

    $("#premier_mois_trimes").bind("change", function(event) {
        updateBailInfo();
    });

    $("#bail_var_capee").bind("keyup", function(event) {
        const countIndexation = $("#countIndexation").val();

        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(function() {
                updateBailInfo();
            }, delayTime);
        }
    });

    $("#bail_var_plafonnee").bind("keyup", function(event) {
        const countIndexation = $("#countIndexation").val();

        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(function() {
                updateBailInfo();
            }, delayTime);
        }
    });

    $("#bail_var_min").bind("keyup", function(event) {
        const countIndexation = $("#countIndexation").val();

        if (countIndexation > 1) {
            var data_request = {
                action: "demande",
                bail_id: $('#bail_id').val(),
            };
            IndexationExist(data_request);
        } else {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(function() {
                updateBailInfo();
            }, delayTime);
        }
    });
</script>