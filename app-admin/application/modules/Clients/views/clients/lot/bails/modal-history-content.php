<div class="row form-group inline p-0 title-detail-bail">
    <div class="col-4">
        <?php if ($bail->bail_avenant == 1)
            echo "Avenant ";
        else
            echo "Bail " ?>
        <?php if ($bail->bail_cloture == 1)
            echo "cloturé ";
        else
            echo "en cours " ?>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-lg-4">
        <label>
            Crée le
            <?= date("d/m/Y", strtotime(str_replace('/', '-', $bail->bail_debut))); ?>
        </label>
    </div>
    <div class="col-lg-8">
        <label>
            <?php
            $cloture = date("d/m/Y", strtotime(str_replace('/', '-', $bail->bail_date_cloture)));
            if ($bail->bail_cloture == 1) {
                echo "Cloturé le  $cloture";
            } ?>

            <?php
            if ($bail->bail_util_id_cloture !== null) {
                echo " par $bail->util_prenom $bail->util_nom";
            }
            ?>
        </label>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Preneur à bail :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_preneur_id">
            <?php foreach ($gestionnaire as $gestionnaires) : ?>
                <option value="<?= $gestionnaires->gestionnaire_id ?>" <?= $bail->preneur_id == $gestionnaires->gestionnaire_id ? 'selected' : '' ?>>
                    <?= $gestionnaires->gestionnaire_nom ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Type de bail :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_type_bail_id">
            <?php foreach ($type_bail as $type) : ?>
                <option value="<?= $type->tpba_id ?>" <?= $bail->tpba_id == $type->tpba_id ? 'selected' : '' ?>>
                    <?= $type->tpba_description ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Exploitant (Gestionnaire) :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_gestionnaire_id">
            <?php foreach ($gestionnaire as $gestionnaires) : ?>
                <option value="<?= $gestionnaires->gestionnaire_id ?>" <?= $bail->gestionnaire_id == $gestionnaires->gestionnaire_id ? 'selected' : '' ?>>
                    <?= $gestionnaires->gestionnaire_nom ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Nature de la prise d'effet :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="histo_nature_prise_effet_id" disabled>
            <?php foreach ($nature_prise_effet as $nature) : ?>
                <option value="<?= $nature->natef_id ?>" <?= $bail->natef_id == $nature->natef_id ? 'selected' : '' ?>>
                    <?= $nature->natef_description ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Date de la prise d'effet :</label>
    </div>
    <div class="col">
        <input type="date" disabled class="form-control input_first_disabled auto_effect" id="histo_bail_debut" value="<?= $bail->bail_debut ?>">
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Durée :</label>
    </div>
    <div class="col">
        <select id="histo_bail_annee" disabled class="form-select input_first_disabled auto_effect" aria-label="Default select example" value="<?= $bail->bail_annee ?>">
            <?php
            $listYear = array(
                array("text" => "0 an", "value" => "0"),
                array("text" => "1 an", "value" => "1"),
                array("text" => "2 ans", "value" => "2"),
                array("text" => "3 ans", "value" => "3"),
                array("text" => "4 ans", "value" => "4"),
                array("text" => "5 ans", "value" => "5"),
                array("text" => "6 ans", "value" => "6"),
                array("text" => "7 ans", "value" => "7"),
                array("text" => "8 ans", "value" => "8"),
                array("text" => "9 ans", "value" => "9"),
                array("text" => "10 ans", "value" => "10"),
                array("text" => "11 ans", "value" => "11"),
                array("text" => "12 ans", "value" => "12"),
                array("text" => "13 ans", "value" => "13"),
                array("text" => "14 ans", "value" => "14"),
                array("text" => "15 ans", "value" => "15"),
                array("text" => "16 ans", "value" => "16"),
                array("text" => "17 ans", "value" => "17"),
                array("text" => "18 ans", "value" => "18"),
                array("text" => "19 ans", "value" => "19"),
                array("text" => "20 ans", "value" => "20"),
            )
            ?>
            <?php foreach ($listYear as $year_) : ?>
                <option value="<?= $year_['value'] ?>" <?php if ($bail->bail_annee == $year_['value'])
                                                            echo "selected"; ?>>
                    <?= $year_['text'] ?>
                </option>
            <?php endforeach ?>
        </select>

        <select id="histo_bail_mois" class="form-select input_first_disabled auto_effect" aria-label="Default select example" disabled value="<?= $bail->bail_mois ?>">
            <?php
            $listMonth = array(
                array("text" => "0 mois", "value" => "0"),
                array("text" => "1 mois", "value" => "1"),
                array("text" => "2 mois", "value" => "2"),
                array("text" => "3 mois", "value" => "3"),
                array("text" => "4 mois", "value" => "4"),
                array("text" => "5 mois", "value" => "5"),
                array("text" => "6 mois", "value" => "6"),
                array("text" => "7 mois", "value" => "7"),
                array("text" => "8 mois", "value" => "8"),
                array("text" => "9 mois", "value" => "9"),
                array("text" => "10 mois", "value" => "10"),
                array("text" => "11 mois", "value" => "11")
            )
            ?>

            <?php foreach ($listMonth as $month_) : ?>
                <option value="<?= $year_['value'] ?>" <?php if ($bail->bail_mois == $month_['value'])
                                                            echo "selected"; ?>>
                    <?= $month_['text'] ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Date fin :</label>
    </div>
    <div class="col">
        <input type="date" class="form-control auto_effect" disabled id="histo_bail_fin" id="bail_fin" value="<?= $bail->bail_fin ?>">
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Tacite prolongation :</label>
    </div>
    <div class="col">
        <?php
        $tacite = 'oui';
        if ($bail->bail_avenant == 1) {
            $tacite = 'non';
        } else {
            if ($bail->bail_cloture == 1) {
                $tacite = 'non';
            } else if ($bail->bail_fin != "0000-00-00") {
                if (strtotime($bail->bail_fin) <= strtotime(date('Y-m-d'))) {
                    $tacite = 'oui';
                } else {
                    $tacite = 'non';
                }
            } else {
                $tacite = 'non';
            }
        }
        ?>

        <input type="text" class="form-control auto_effect" id="histo_tacite_prolongation" disabled value="<?= $tacite ?>">
        <!-- disabled -->
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label data-width="250">Résiliation triennale :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" disabled id="histo_bail_resiliation_triennale">
            <option value="1" <?= $bail->bail_resiliation_triennale == '1' ? 'selected' : '' ?>>oui</option>
            <option value="0" <?= $bail->bail_resiliation_triennale == '0' ? 'selected' : '' ?>>non</option>
        </select>
    </div>
</div>

<div class="row mt-2">
    <div class="col">
        <h5 class="title_bail">Informations sur les loyers :</h5>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Périodicité des loyers :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_periodicite_loyer_id">
            <?php foreach ($periodicite_loyer as $periode) : ?>
                <option value="<?= $periode->pdl_id ?>" <?= $bail->pdl_id == $periode->pdl_id ? 'selected' : '' ?>>
                    <?= $periode->pdl_description ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Moment de facturation :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_moment_facturation_bail_id">
            <?php foreach ($moment_facturation as $moment_factu) : ?>
                <option value="<?= $moment_factu->momfac_id ?>" <?= $bail->momfac_id == $moment_factu->momfac_id ? 'selected' : '' ?>>
                    <?= $moment_factu->momfact_nom ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Date de première facturation :</label>
    </div>
    <div class="col">
        <input type="date" disabled class="form-control input_first_disabled auto_effect" id="histo_bail_date_echeance" value="<?= $bail->bail_date_echeance ?>">
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Date de paiement :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_type_echeance_id">
            <?php foreach ($type_echeance as $tp_echeance) : ?>
                <option value="<?= $tp_echeance->tpech_id ?>" <?= $bail->tpech_id == $tp_echeance->tpech_id ? 'selected' : '' ?>>
                    <?= $tp_echeance->tpech_valeur ?> <?php if ($tp_echeance->tpech_valeur == 1)
                                                            echo '<sup style="position: relative; left: -3px;">er</sup>' ?>&nbsp;du mois
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Période d'indexation :</label>
    </div>
    <div class="col">
        <select id="histo_periode_indexation" class="form-select input_first_disabled periode_indexation auto_effect" aria-label="Default select example" disabled>
            <?php foreach ($periode_indexation as $periode_index) : ?>
                <option data-id="<?= $periode_index->prdind_valeur ?>" value="<?= $periode_index->prdind_id ?>" <?= $bail->prdind_id == $periode_index->prdind_id ? 'selected' : '' ?>>
                    <?= $periode_index->prdind_valeur ?> an<?php if (intval($periode_index->prdind_valeur) > 1)
                                                                echo 's'; ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Date de la première indexation :</label>
    </div>
    <div class="col">
        <input type="date" disabled class="form-control input_first_disabled auto_effect" id="histo_bail_date_premiere_indexation" value="<?= $bail->bail_date_premiere_indexation ?>">
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Date de la prochaine indexation :</label>
    </div>
    <div class="col">
        <input type="date" class="form-control auto_effect" id="histo_bail_date_prochaine_indexation" disabled value="<?= $bail->bail_date_prochaine_indexation ?>">
    </div>
</div>


<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Indices utilisés :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_indice_loyer_id">
            <?php foreach ($indice_loyer as $indice) : ?>
                <option value="<?= $indice->indloyer_id ?>" <?= $bail->indloyer_id == $indice->indloyer_id ? 'selected' : '' ?>>
                    <?= $indice->indloyer_description ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Indices de référence :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_indice_valeur_loyer_id">
            <option value="0">les valeurs : </option>
            <?php foreach ($indices as $valeur_indice) : ?>
                <option value="<?= $valeur_indice->indval_id ?>" <?= $bail->indval_id == $valeur_indice->indval_id ? 'selected' : '' ?> class="indice_valeur_loyer_option indice_valeur_loyer
                                                                                                                <?= $valeur_indice->indloyer_id ?>">
                    <?php echo $valeur_indice->indval_trimestre; ?> &nbsp;
                    <?php echo $valeur_indice->indval_annee; ?> &nbsp;
                    (<?php echo $valeur_indice->indval_valeur; ?>)
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>


<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Nature du loyer :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" aria-label="Default select example" id="histo_bail_loyer_variable" disabled>
            <option value="0" <?= $bail->bail_loyer_variable == 0 ? 'selected' : '' ?>>Fixe</option>
            <option value="1" <?= $bail->bail_loyer_variable == 1 ? 'selected' : '' ?>>Fixe et variable</option>
            <option value="2" <?= $bail->bail_loyer_variable == 2 ? 'selected' : '' ?>>Variable</option>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Forfait charges déductible indexé :</label>
    </div>
    <div class="col">
        <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_bail_forfait_charge_indexer">
            <option value="1" <?= $bail->bail_forfait_charge_indexer == 1 ? 'selected' : '' ?>>oui</option>
            <option value="0" <?= $bail->bail_forfait_charge_indexer == 0 ? 'selected' : '' ?>>non</option>
        </select>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Variation capée :</label>
    </div>
    <div class="col">
        <div class="input-group">
            <input type="number" id="histo_bail_var_capee" disabled class="input_first_disabled auto_effect form-control input" placeholder="0" maxlength="3" max="100" value="<?= $bail->bail_var_capee ?>">
            <div class="input-group-append">
                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Variation plafonnée :</label>
    </div>
    <div class="col">
        <div class="input-group">
            <input type="number" id="histo_bail_var_capee" disabled class="input_first_disabled auto_effect form-control input" placeholder="0" value="<?= $bail->bail_var_plafonnee ?>">
            <div class="input-group-append">
                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
</div>

<div class="row form-group inline p-0">
    <div class="col-4">
        <label>Augmentation minimum :</label>
    </div>
    <div class="col">
        <div class="input-group">
            <input type="number" id="histo_bail_var_capee" disabled class="input_first_disabled auto_effect form-control input" placeholder="" value="<?= $bail->bail_var_min ?>">
            <div class="input-group-append">
                <span class="input-group-text input-group-text-index" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col float-start">
        <h5 class="title_bail">Actions disponibles :</h5>
    </div>
</div>
<div class="row">
    <div class="col float-start">
        <div class="form-check">
            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="histo_bail_remboursement_tom" disabled <?php if ($bail->bail_remboursement_tom == 1)
                                                                                                                                            echo "checked"; ?>>
            <label class="form-check-label" for="bail_remboursement_tom">
                Remboursement de la TOM
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="histo_bail_facturation_loyer_celaviegestion" disabled checked="false">
            <!-- <?php if ($bail->bail_facturation_loyer_celaviegestion == 1)
                        echo "checked"; ?> -->
            <label class="form-check-label" for="bail_facturation_loyer_celaviegestion">
                Facturation des loyers par CELAVI Gestion
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input input_first_disabled auto_effect" type="checkbox" id="histo_bail_charge_recuperable" disabled <?php if ($bail->bail_charge_recuperable == 1)
                                                                                                                                                echo "checked"; ?>>
            <label class="form-check-label" for="bail_charge_recuperable">
                Charges récupérables
            </label>
        </div>
    </div>
</div>


<div class="row">
    <div class="col float-start">
        <h5 class="title_bail">Franchise de loyer :</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        <table class="table table-hover fs--1">
            <thead class="text-white" style="background:#2569C3;">
                <tr class="text-center">
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Début</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Fin</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;" width="60%">Explication</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php foreach ($franchises as $keyF => $franchise) : ?>
                    <tr class="text-center" id="histo_franchise_row-<?php echo $franchise->franc_id ?>">
                        <td class="histo_franchise-view-<?php echo $keyF ?>">
                            <?php echo date_format_fr($franchise->franc_debut); ?>
                        </td>
                        <td class="histo_ranchise-view-<?php echo $keyF ?>">
                            <?php echo date_format_fr($franchise->franc_fin); ?>
                        </td>
                        <td class="franc_explication_field histo_franchise-view-<?php echo $keyF ?>">
                            <div>
                                <?php echo $franchise->franc_explication ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>

        <?php if (count($franchises) === 0) { ?>
            <p class="text-center">Aucune franchise de loyer créée</p>
        <?php } ?>

    </div>
</div>

<div class="row">
    <div class="col float-start">
        <h5 class="title_bail">Indexations de loyer :</h5>
    </div>
</div>

<div class="row mb-4">
    <div class="col table_table">
        <table class="table table-hover fs--1 ">
            <thead class="text-white" style="background:#2569C3;">
                <tr class="text-center">
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Début</th>
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Fin</th>
                    <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;">Indice</th>
                    <th class="p-2" colspan="4" style="border-right: 1px solid #ffff;">Montant</th>
                </tr>
                <tr class="text-center">
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">Type</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">HT</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">TVA</th>
                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">TTC</th>
                </tr>
            </thead>
            <tbody class="bg-200 table_indexation_loyer">
                <?php if (isset($indexations) || count($indexations) > 0) { ?>
                    <?php
                    $index = 0;
                    foreach ($indexations as $key => $indexation) : ?>
                        <?php if ($indexation->indlo_ref_loyer == 0) { ?>
                            <tr class="text-center" id="indexation_row-<?= $indexation->indlo_id ?>">
                                <td>
                                    <div class="text-black">
                                        <?php
                                        if (isset($indexation->indlo_debut))
                                            echo date_format_fr($indexation->indlo_debut);
                                        else
                                            echo "-"; ?>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-black">
                                        <?php
                                        if (isset($indexation->indlo_fin))
                                            echo date_format_fr($indexation->indlo_fin);
                                        else
                                            echo "-"; ?>
                                    </div>
                                </td>

                                <td>
                                    <?php
                                    $indx_libelle = $indexation->indval_libelle;
                                    $indx_valeur = $indexation->indval_valeur;

                                    if (intval($index) > 0) {
                                        foreach ($indices as $indice) {
                                            if ($indice->indval_id === $indexation->indlo_indice_reference) {
                                                $indx_libelle = $indice->indval_libelle;
                                                $indx_valeur = $indice->indval_valeur;
                                            }
                                        }
                                    }

                                    ?>

                                    <div class="text-black">
                                        <?php echo $indx_libelle; ?>
                                        <div>
                                            <b>
                                                <?php echo $indx_valeur; ?>
                                            </b>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-left">Appartement</div>
                                    <div class="text-left">Parking</div>
                                    <div class="text-left">Forfait charges</div>
                                    <div class="text-left"><b>Total</b></div>

                                </td>

                                <td>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_appartement_ht); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_parking_ht); ?>
                                    </div>
                                    <div class="text-right"> -
                                        <?php echo format_number_($indexation->indlo_charge_ht); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php
                                        $totalA = 0;
                                        $totalA = $indexation->indlo_appartement_ht + $indexation->indlo_parking_ht - $indexation->indlo_charge_ht + $indexation->indlo_autre_prod_ht;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalA); ?>
                                        </b>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_appartement_tva; ?>%) &nbsp;
                                        <?php
                                        $newA = 0;
                                        $newA = $indexation->indlo_appartement_ht * $indexation->indlo_appartement_tva / 100;
                                        echo format_number_($newA);
                                        ?>
                                    </div>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_parking_tva; ?>%) &nbsp;
                                        <?php
                                        $newB = 0;
                                        $newB = $indexation->indlo_parking_ht * $indexation->indlo_parking_tva / 100;
                                        echo format_number_($newB);
                                        ?>
                                    </div>
                                    <div class="text-right">
                                        (
                                        <?php echo $indexation->indlo_charge_tva; ?>%) &nbsp;
                                        -
                                        <?php
                                        $newC = 0;
                                        $newC = $indexation->indlo_charge_ht * $indexation->indlo_charge_tva / 100;
                                        echo format_number_($newC);
                                        ?>
                                    </div>

                                    <div class="text-right">
                                        <?php
                                        $totalB = 0;
                                        $totalB = $newA + $newB - $newC;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalB); ?>
                                        </b>
                                    </div>
                                </td>

                                <td>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_appartement_ttc); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php echo format_number_($indexation->indlo_parking_ttc); ?>
                                    </div>
                                    <div class="text-right"> -
                                        <?php echo format_number_($indexation->indlo_charge_ttc); ?>
                                    </div>
                                    <div class="text-right">
                                        <?php
                                        $totalC = 0;
                                        $totalC = $indexation->indlo_appartement_ttc + $indexation->indlo_parking_ttc - $indexation->indlo_charge_ttc + $indexation->indlo_autre_prod_ttc;
                                        ?>
                                        <b>
                                            <?php echo format_number_($totalC); ?>
                                        </b>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php
                        $index = $index + 1;
                    endforeach ?>
                <?php } ?>

            </tbody>
        </table>

        <?php if (!isset($indexations) || count($indexations) === 0) { ?>
            <div class="text-center mt-2">
                Aucune indexation de loyer
            </div>
        <?php } ?>
    </div>
</div>


<div class="row">
    <div class="col float-start">
        <h5 class="title_bail">Autres informations :</h5>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="250">Art 605 :</label>
                <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_bail_art_605_id">
                    <?php foreach ($bail_art as $bail_605) : ?>
                        <option value="<?= $bail_605->bart_id ?>" <?= $bail->bail_art_605 == $bail_605->bart_id ? 'selected' : '' ?>>
                            <?= $bail_605->bart_nom ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </div>

    <div class="col">

    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group inline p-0">
            <div class="col">
                <br>
                <label data-width="250">Art 606 :</label>
                <select class="form-select input_first_disabled auto_effect" disabled aria-label="Default select example" id="histo_bail_art_606_id">
                    <?php foreach ($bail_art as $bail_606) : ?>
                        <option value="<?= $bail_606->bart_id ?>" <?= $bail->bail_art_606 == $bail_606->bart_id ? 'selected' : '' ?>>
                            <?= $bail_606->bart_nom ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </div>

    <div class="col">

    </div>
</div>
<div class="row">
    <div class="col">
        <div class="mb-3">
            <label data-width="250">Commentaire :</label>
            <textarea class="form-control input_first_disabled auto_effect" id="histo_bail_commentaire" rows="5" style="height: 50%;" disabled value="<?= $bail->bail_commentaire ?>"></textarea>
        </div>
    </div>
</div>