<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Information de modification</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php
    foreach ($info_immo as $key => $value) {
        $utilisateur = $value->util_prenom;
        $date = $value->immo_general_date_modification;
        $commentaire = $value->immo_general_justification;
    }
?>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <table class="table table-borderless fs--1 mb-0">
                        <tbody>
                            <tr class="border-bottom">
                                <th class="ps-0">Information modifiée par  : </th>
                                <th class="pe-0 text-end">
                                <?=$utilisateur?>
                                </th>
                            </tr>
                            <tr class="border-bottom">
                                <th class="ps-0">Date  : </th>
                                <th class="pe-0 text-end">
                                <?=date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $date)))?>
                                </th>
                            </tr>
                            <tr class="border-bottom">
                                <th class="ps-0">Commentaire : </th>
                                <th class="pe-0 text-end">
                                <?=$commentaire?>
                                </th>
                            </tr>                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
    </div>
</div>