<div class="contenair-immo" id="immo">
    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "general">
            <a class="nav-link active" id="general-tab" data-bs-toggle="tab" href="#tab-general" role="tab"
                aria-controls="tab-general" aria-selected="true">Général</a>
        </li>
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "gros-oeuvre">
            <a class="nav-link" id="gros-oeuvre-tab" data-bs-toggle="tab" href="#tab-gros-oeuvre" role="tab"
                aria-controls="tab-gros-oeuvre" aria-selected="false">Gros oeuvre</a>
        </li>
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "facade-etancheite">
            <a class="nav-link" id="facade-etancheite-mandat" data-bs-toggle="tab" href="#tab-facade-etancheite"
                role="tab" aria-controls="tab-facade-etancheite" aria-selected="false">Façades Etanchéité</a>
        </li>
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "installation-igt">
            <a class="nav-link" id="installation-igt-tab" data-bs-toggle="tab" href="#tab-installation-igt" role="tab"
                aria-controls="tab-installation-igt" aria-selected="false">Installation (IGT)</a>
        </li>
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "agencement">
            <a class="nav-link" id="agencement-tab" data-bs-toggle="tab" href="#tab-agencement" role="tab"
                aria-controls="tab-agencement" aria-selected="false">Agencements</a>
        </li>
        <li class="nav-item charger-sous-ongletImmo" data-cliLot_id ="<?=$lot->cliLot_id;?>" data-onglet = "mobilier">
            <a class="nav-link" id="mobilier-tab" data-bs-toggle="tab" href="#tab-mobilier" role="tab"
                aria-controls="tab-mobilier" aria-selected="false">Mobilier</a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-1" id="lotTabContent">
        <div class="tab-pane fade show active" id="tab-general" role="tabpanel" aria-labelledby="general-tab">
            <?php $this->load->view('immo/immo_general/immo-general'); ?>
        </div>

        <div class="tab-pane fade" id="tab-gros-oeuvre" role="tabpanel" aria-labelledby="gros-oeuvre-tab">
            <?php $this->load->view('immo/immo-gros-oeuvre/immo-gros-oeuvre'); ?>
        </div>
        <div class="tab-pane fade" id="tab-facade-etancheite" role="tabpanel" aria-labelledby="facade-etancheite-tab">
            <?php $this->load->view('immo/immo-facade/immo-facade'); ?>
        </div>
        <div class="tab-pane fade" id="tab-installation-igt" role="tabpanel" aria-labelledby="installation-igt-tab">
            <?php $this->load->view('immo/immo-installation/immo-installation'); ?>
        </div>
        <div class="tab-pane fade" id="tab-agencement" role="tabpanel" aria-labelledby="agencement-tab">
            <?php $this->load->view('immo/immo_agencement/immo-agencement'); ?>
        </div>
        <div class="tab-pane fade" id="tab-mobilier" role="tabpanel" aria-labelledby="mobilier-tab">
            <?php $this->load->view('immo/immo-mobilier/immo-mobilier'); ?>
        </div>

    </div>

</div>

<div class="m-2">
    <div class="alert alert-secondary text-center" id="message_immo" style="display : block">
        <div class="text-center">
            <p class="m-1">
                Impossible d'accéder aux informations de l'immobilisation, veuillez d'abord renseigner la valeur de
                l'acquisition
                et la date de début d'amortissements dans le sous onglet acte.
            </p>
        </div>
    </div>
</div>

<script>
    // getActeImmo(<?= $lot->cliLot_id ?>)

    $(document).on('click', '.charger-sous-ongletImmo', function (e) {
        e.preventDefault();
    
        if($(this).attr('data-onglet')==="general"){
            getImmoGeneral($(this).attr('data-cliLot_id'));
        }

        if($(this).attr('data-onglet')==="gros-oeuvre"){
            getImmoGrosOeuvre($(this).attr('data-cliLot_id'));
        }

        if($(this).attr('data-onglet')==="facade-etancheite"){
            getImmofacade($(this).attr('data-cliLot_id'));
        }

        if($(this).attr('data-onglet')==="installation-igt"){
            getImmoInstallation($(this).attr('data-cliLot_id'));
        }

        if($(this).attr('data-onglet')==="agencement"){
            getImmoAgencement($(this).attr('data-cliLot_id'));
        }

        if($(this).attr('data-onglet')==="mobilier"){
            getImmoMobilier($(this).attr('data-cliLot_id'));
        }


    });

    only_visuel_gestion();
</script>