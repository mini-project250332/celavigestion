<div class="contenair-title">
    <div class="row m-0">
        <!-- <div class="col-7">
            <h5 class="fs-1">Documents immobilisations</h5>
        </div>         -->
    </div>
</div>

<?php
if (!empty($acte)) {

    $date_debut = strtotime($acte->acte_date_debut_amortissement);
    $date_fin = strtotime("Last day of December", strtotime($acte->acte_date_debut_amortissement));
    $datediff = $date_fin - $date_debut;
    // var_dump($datediff);
    $dateDebut = date("Y", strtotime($acte->acte_date_debut_amortissement));
    $dateFin =  date("Y", strtotime($dateDebut . '+50 year'));
}

?>

<table class="table table-hover fs--1">
    <thead class="text-white" style="background:#2569C3;">
        <tr>
            <th class="p-3 text-center" rowspan="2" style="border-right: 1px solid #ffff;">Année</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">Gros Oeuvre</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">Façades Etanchéité</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">Installations (IGT)</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">Agencements</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">Mobilier</th>
            <th class="p-2 text-center" colspan="2" style="border-right: 1px solid #ffff;">TOTAL</th>
            <th class="p-3 text-center" rowspan="2" style="border-right: 1px solid #ffff;">Action</th>
        </tr>
        <tr>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste</th>
            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Annuité</th>
        </tr>
    </thead>
    <tbody class="bg-200">
        <?php if (!empty($acte->acte_date_debut_amortissement)) :
            $resteGrosOeuvre = $immo->immo_gros_oeuvre - $premiereAnneeOeuvre;
            $resteFacade = $immo->immo_facade - $premiereAnneeFacade;
            $resteInstallation = $immo->immo_installation - $premiereAnneeInstallation;
            $resteAgencement = $immo->immo_agencement - $premiereAnneeAgencement;
            $resteMobilier = $immo->immo_mobilier - $premiereAnneeMobilier;
            $reste = $totalReste - $totalPremiereAnnee; ?>
            <?php for ($i = $dateDebut, $an = 1; $i <= $dateFin, $an <= 51; $i++, $an++) : ?>
                <?php
                $fascade = 0;
                $Installation = 0;
                $Agencement = 0;
                $Mobilier = 0;
                $totalAnnee = 0;
                if ($an > 21) $resteFacade = 0;
                if ($an > 16) $resteInstallation = 0;
                if ($an > 11) $resteAgencement = 0;
                if ($an > 8) $resteMobilier = 0;
                if ($an == 21) $fascade = $derniereAnneeFacade;
                elseif ($an < 21) $fascade = $annuiteFacade;
                if ($an == 16) $Installation = $derniereAnneeInstallation;
                elseif ($an < 16) $Installation = $annuiteInstallation;
                if ($an == 11) $Agencement = $derniereAnneeAgencement;
                elseif ($an < 11) $Agencement = $annuiteAgencement;
                if ($an == 8) $Mobilier = $derniereAnneeMobilier;
                elseif ($an < 8) $Mobilier = $annuiteMobilier;
                if ($an >= 20) $totalAnnee = $annuiteOeuvre;
                elseif ($an < 20) $totalAnnee = $totalAnnuite;
                if ($an == 21) $totalAnnee = $totalAnnee20;
                elseif ($an < 21) $totalAnnee = $totalAnnee19;
                if ($an == 16) $totalAnnee = $totalAnnee15;
                elseif ($an < 16) $totalAnnee = $totalAnnee14;
                if ($an == 11) $totalAnnee = $totalAnnee10;
                elseif ($an < 11) $totalAnnee = $totalAnnee9;
                if ($an == 8) $totalAnnee = $totalAnnee7;
                elseif ($an < 8) $totalAnnee = $totalAnnuite;
                ?>
                <?php if ($an == 1) : ?>
                    <tr class="<?= !empty($immo_general) ? 'bg-warning text-white' : 'bg-200' ?>">
                        <!-- <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> (<?= $an ?>) </td> -->
                        <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> </td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($immo->immo_gros_oeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($premiereAnneeOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($immo->immo_facade) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($premiereAnneeFacade) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($immo->immo_installation) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($premiereAnneeInstallation) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($immo->immo_agencement) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($premiereAnneeAgencement) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($immo->immo_mobilier) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($premiereAnneeMobilier) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($totalReste) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($totalPremiereAnnee) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;">
                            <!-- <button class="btn btn-sm btn-outline-primary" id="update_immo" data-lot="<?= $cliLot_id ?>" data-id="<?= $i ?>" data-oeuvre ="<?= format_number_($immo->immo_gros_oeuvre) ?>" 
                            data-facade ="<?= format_number_($immo->immo_facade) ?>" data-installation ="<?= format_number_($immo->immo_installation) ?>" 
                            data-agence ="<?= format_number_($immo->immo_agencement) ?>" data-mobilier ="<?= format_number_($immo->immo_mobilier) ?>" data-total ="<?= format_number_($totalReste) ?>" >
                                <i class="fas fa-pencil-alt"></i>
                            </button> -->
                        </td>
                    </tr>
                <?php elseif ($an == 51) : ?>
                    <tr id="annee-<?= $an ?>">
                        <!-- <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> (<?= $an ?>) </td> -->
                        <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> </td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteGrosOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"> <?= format_number_($derniereAnneeOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteFacade) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_(0) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteInstallation) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_(0) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteAgencement) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_(0) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteMobilier) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_(0) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($reste) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($derniereAnneeOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;">
                            <button class="btn btn-sm btn-outline-primary" id="update_immo" data-lot="<?= $cliLot_id ?>" data-id="<?= $i ?>" data-oeuvre="<?= format_number_($resteGrosOeuvre) ?>" data-facade="<?= format_number_($resteFacade) ?>" data-installation="<?= format_number_($resteInstallation) ?>" data-agence="<?= format_number_($resteAgencement) ?>" data-mobilier="<?= format_number_($resteMobilier) ?>" data-total="<?= format_number_($reste) ?>">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                        </td>
                    </tr>
                <?php else : ?>
                    <?php if (!empty($immo_general)) :
                        foreach ($immo_general as $key => $value) {
                            if ($i == $value->immo_general_annee) {
                                $resteGrosOeuvre = $value->immo_general_gros_oeuvre;
                                $resteFacade = $an > 21 ? 0 : $value->immo_general_facade;
                                $resteInstallation =  $an > 16 ? 0 : $value->immo_general_installation;
                                $resteAgencement = $an > 11 ? 0 : $value->immo_general_agencement;
                                $resteMobilier = $an > 8 ? 0 : $value->immo_general_mobilier;
                                $reste = $value->immo_general_total;

                                $nb_anneeGrosOeuvre = 51 - ($an);
                                $nb_anneeFacade = 21 - ($an);
                                $nb_anneeInstallation = 16 - ($an);
                                $nb_anneeAgencement = 11 - ($an);
                                $nb_anneeMobilier = 8 - ($an);
                                $diff_in_days = round($datediff / (60 * 60 * 24));
                                                             
                                $annuiteOeuvre = ($resteGrosOeuvre / ($nb_anneeGrosOeuvre * 365 + $diff_in_days)) * 365;
                                $annuiteFacade = ($resteFacade / ($nb_anneeFacade * 365 + $diff_in_days)) * 365;                                                          
                                $annuiteInstallation = ($resteInstallation / ($nb_anneeInstallation * 365 + $diff_in_days)) * 365;                               
                                $annuiteAgencement = ($resteAgencement / ($nb_anneeAgencement * 365 + $diff_in_days)) * 365;                               
                                $annuiteMobilier = ($resteMobilier  / ($nb_anneeMobilier * 365 + $diff_in_days)) * 365;                                
                                $totalAnnee =   floatval($annuiteOeuvre) +  floatval($annuiteFacade) + floatval($annuiteInstallation)  +  floatval($annuiteAgencement)  + floatval($annuiteMobilier);
                            }
                            
                            $annee = $value->immo_general_annee;
                             
                        }
                    endif ?>
                    <?php  
                        $annuiteFacade = $an > 21 ? 0 : $annuiteFacade; 
                        $annuiteFacade = $an == 21 ? ($resteFacade ) : $annuiteFacade;  
                        $annuiteInstallation = $an > 16 ? 0 : $annuiteInstallation; 
                        $annuiteInstallation = $an == 16 ? ($resteInstallation) : $annuiteInstallation;
                        $annuiteAgencement = $an > 11 ? 0 : $annuiteAgencement; 
                        $annuiteAgencement = $an == 11 ? ($resteAgencement) : $annuiteAgencement;
                        $annuiteMobilier = $an > 7 ? 0 : $annuiteMobilier; 
                        $annuiteMobilier = $an == 8 ? ($resteMobilier) : $annuiteMobilier;
                        $reste = $an > 21 ? $resteGrosOeuvre :  $reste;
                        $reste = $an < 22 ? $resteGrosOeuvre + $resteFacade  :  $reste;
                        $reste = $an < 17 ? $resteGrosOeuvre + $resteFacade + $resteInstallation :  $reste;
                        $reste = $an < 12 ? $resteGrosOeuvre + $resteFacade + $resteInstallation + $resteAgencement :  $reste;
                        $reste = $an < 9 ? $resteGrosOeuvre + $resteFacade + $resteInstallation + $resteAgencement +  $resteMobilier:  $reste;
                        $totalAnnee = $an > 21 ? $annuiteOeuvre :  $totalAnnee;
                        $totalAnnee = $an < 22 ? $annuiteOeuvre + $annuiteFacade  :  $totalAnnee;
                        $totalAnnee = $an < 17 ? $annuiteOeuvre + $annuiteFacade + $annuiteInstallation :  $totalAnnee;
                        $totalAnnee = $an < 12 ? $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $annuiteAgencement :  $totalAnnee;
                        $totalAnnee = $an < 9 ? $annuiteOeuvre + $annuiteFacade + $annuiteInstallation + $annuiteAgencement +  $annuiteMobilier:  $totalAnnee;
                    ?>
                    <tr class="<?= !empty($immo_general) ? ($i < $annee  ? 'bg-warning text-white' : 'bg-200' ) : 'bg-200' ?>" id="annee-<?= $an ?>">
                        <!-- <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> (<?= $an ?>) </td> -->
                        <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?>  </td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteGrosOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($annuiteOeuvre) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteFacade) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($annuiteFacade) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteInstallation) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($annuiteInstallation) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteAgencement) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($annuiteAgencement) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($resteMobilier) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($annuiteMobilier) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($reste) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= format_number_($totalAnnee) ?></td>
                        <td class="p-2 text-center" style="border-right: 1px solid #ffff;">
                            <?php if (!empty($immo_general)) : ?>
                                <button class="btn btn-sm btn-outline-primary" id="<?= $i < $annee ? 'info_immo' : 'update_immo' ?>" 
                                    data-lot="<?= $cliLot_id ?>" data-id="<?= $i ?>" data-oeuvre="<?= ($resteGrosOeuvre) ?>" data-facade="<?= ($resteFacade) ?>" data-installation="<?= ($resteInstallation) ?>" 
                                    data-agence="<?= ($resteAgencement) ?>" data-mobilier="<?= ($resteMobilier) ?>" data-total="<?= ($reste) ?>" data-immo-id ="<?=$value->immo_general_id?>">
                                    <i class="<?= $i < $annee ? 'fas fa-eye fs--1' : 'fas fa-pencil-alt' ?>"></i>
                                </button>
                            <?php else : ?>
                                <button class="btn btn-sm btn-outline-primary" id="update_immo" data-lot="<?= $cliLot_id ?>" data-id="<?= $i ?>" data-oeuvre="<?= ($resteGrosOeuvre) ?>" data-facade="<?= ($resteFacade) ?>" data-installation="<?= ($resteInstallation) ?>" data-agence="<?= ($resteAgencement) ?>" data-mobilier="<?= ($resteMobilier) ?>" data-total="<?= ($reste) ?>">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php
                    $resteGrosOeuvre -= $annuiteOeuvre;
                    $resteFacade -= $annuiteFacade;
                    $resteInstallation -= $annuiteInstallation;
                    $resteAgencement -= $annuiteAgencement;
                    $resteMobilier -= $annuiteMobilier;
                    $reste -= $totalAnnee;
                    ?>
                <?php endif ?>
            <?php endfor ?>
        <?php endif ?>
    </tbody>
</table>