<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Modification de l'année <?=$annee?></h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Acte/UpdateImmo',array('method'=>"post", 'class'=>'px-2','id'=>'UpdateImmo')); ?>
<div class="modal-body">
    <div class="row m-0 text-center">
        <div class="alert alert-secondary text-center">
            <p class="fs--1 m-0 text-center"> 
              Attention, si vous modifiez l'année <?=$annee?>, vous ne pourrez plus modifier les années précédentes.
            </p>
        </div> 
    </div>
    <p class="text-center">Redéfinissez les restes à amortir</p>
    <input type="hidden" name ="immo_general_annee" id= "immo_general_annee" value="<?=$annee?>">
    <input type="hidden" name ="cliLot_id" id= "cliLot_id" value="<?=$cliLot_id?>">
    <div class="row mt-2">
        <div class="col">
            <table class="table table-hover fs--1">
                <thead class="text-white">
                    <tr class="text-center">
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3; width : 30%">Décomposition</th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3; width : 35%">Reste à amortir</th>                      
                        
                    </tr>
                </thead>
                <tbody class="bg-200">
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> Gros oeuvre</th>
                        <td> 
                            <input type="text" value="<?=round($oeuvre, 2)?>" name ="immo_general_gros_oeuvre" id= "immo_general_gros_oeuvre" class=" form-control mb-0 input text-right">
                        </td>                        
                    </tr>
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> Facades étenchéité</th>
                        <td> 
                            <input type="text" value="<?=round($facade,2)?>" name ="immo_general_facade" id= "immo_general_facade" class=" form-control mb-0 input text-right">
                        </td>                       
                    </tr>
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> Installations (IGT)</th>
                        <td> 
                            <input type="text" name ="immo_general_installation" id= "immo_general_installation" value="<?=round($installation,2)?>" class=" form-control mb-0 input text-right">
                        </td>                        
                    </tr>
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> Agencements</th>
                        <td> 
                            <input type="text" name ="immo_general_agencement" id= "immo_general_agencement" value="<?=round($agence,2)?>" class=" form-control mb-0 input text-right">
                        </td>                        
                    </tr>
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> Mobilier</th>
                        <td> 
                            <input type="text" name ="immo_general_mobilier" id= "immo_general_mobilier" value="<?=round($mobilier,2)?>" class=" form-control mb-0 input text-right">
                        </td>                        
                    </tr>
                    <tr>
                        <th class="text-white p-2" style="background:#2569C3;"> TOTAL</th>
                        <td> 
                            <input type="text" name ="immo_general_total" id= "immo_general_total" value="<?=round($total,2)?>" disabled class=" form-control mb-0 input text-right">
                        </td>                        
                    </tr>                   
                    
                </tbody>
            </table>
        </div>  
        <div class="form-group inline" style="left : 10px">
            <div class="m-0 mt-1">  
                <label class="" data-width="250"> Justifiez la modification : </label>
                <textarea class="form-control" id="immo_general_justification" name="immo_general_justification" style="height: 90px; width: 63%" data-formatcontrol="true" data-require="true"></textarea>
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>
<?php echo form_close(); ?>

<script>

$(document).on('keyup', '#immo_general_gros_oeuvre', function (e) { 
    e.preventDefault();
    totalimmo();       
});
$(document).on('keyup', '#immo_general_facade', function (e) { 
    e.preventDefault();
    totalimmo();       
});

$(document).on('keyup', '#immo_general_installation', function (e) { 
    e.preventDefault();
    totalimmo();       
});

$(document).on('keyup', '#immo_general_agencement', function (e) { 
    e.preventDefault();
    totalimmo();       
});

$(document).on('keyup', '#immo_general_mobilier', function (e) { 
    e.preventDefault();
    totalimmo();       
});

function totalimmo (){
    var immo_general_gros_oeuvre = parseFloat($('#immo_general_gros_oeuvre').val());
    var immo_general_facade = parseFloat($('#immo_general_facade').val());
    var immo_general_installation = parseFloat($('#immo_general_installation').val());
    var immo_general_agencement = parseFloat($('#immo_general_agencement').val());
    var immo_general_mobilier = parseFloat($('#immo_general_mobilier').val());

    var total = (immo_general_gros_oeuvre + immo_general_facade + immo_general_installation + immo_general_agencement + immo_general_mobilier).toFixed(2);
    $('#immo_general_total').val(total);
}


</script>




 
