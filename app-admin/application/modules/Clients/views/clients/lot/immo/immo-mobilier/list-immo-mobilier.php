<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Documents Mobiliers</h5>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1" data-id="<?= $cliLot_id ?>" id="ajoutDocMobilier">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<?php
    if(!empty($acte)){

        $date_debut = strtotime($acte->acte_date_debut_amortissement );
        $date_fin = strtotime("Last day of December",strtotime($acte->acte_date_debut_amortissement ));
        $datediff = $date_fin - $date_debut;
        
        $dateDebut = date("Y",strtotime($acte->acte_date_debut_amortissement));
        $dateFin =  date("Y",strtotime($dateDebut.'+7 year'));       
    }    
?>

<div class="row pt-2 m-0 panel-container" id="">
    <div class="col-6 panel-left" style="width: 907px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0" style=" height: calc(45vh - 200px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
            <div class="col-12">
            <?php foreach ($document as $documents) { ?>
                <div class="row apercu_docMobilier" id="docrowMobilier-<?= $documents->doc_mobilier_id ?>">
                    <div class="col-4 pt-2">
                        <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_mobilier_id ?>" onclick="apercuDocumentMobilier(<?= $documents->doc_mobilier_id ?>)"> <?= $documents->doc_mobilier_nom ?></div>
                    </div>
                    <div class="col-2 text-center">
                        <label for=""><i class="fas fa-user"></i> <?= $documents->util_prenom ?></label>
                    </div>
                    <div class="col-2 text-center">
                        <label for=""><i class="fas fa-calendar"></i> <?= $documents->doc_mobilier_creation ?></label>
                    </div>
                    <div class="col-4 text-center">
                        <div class="btn-group fs--1" style="left: 17%;">
                            <?php
                            $message = "Document non traité";
                            if (!empty($documents->doc_mobilier_traitement)) {
                                if ($documents->doc_mobilier_traite == 1) {
                                    $message = "Document traité par " . $documents->util_prenom . " le " . $documents->doc_mobilier_traitement;
                                } else {
                                    $message = "Document annulé par " . $documents->util_prenom . " le " . $documents->doc_mobilier_traitement;
                                }
                            }
                            ?>
                            <button class="only_visuel_gestion btn btn-sm <?= ($documents->doc_mobilier_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill  btnTraiteMobilier" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>" data-id=" <?= $documents->doc_mobilier_id ?>">
                                <i class="fas fa-check"></i>
                            </button> &nbsp;
                            <span class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocMobilier" data-id="<?= $documents->doc_mobilier_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer" data-lot="<?= $cliLot_id; ?>">
                                <i class="far fa-edit"></i>
                            </span>&nbsp;
                            <span class="only_visuel_gestion btn btn-sm btn-outline-primary rounded-pill">
                                <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_mobilier_path ?>','<?= $documents->doc_mobilier_nom ?>')" data-url="<?= base_url($documents->doc_mobilier_path) ?>">
                                </i>
                            </span>&nbsp;
                            <span class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocMobilier" data-id="<?= $documents->doc_mobilier_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                <i class="far fa-trash-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
        <hr class="my-0">
        <div class="row m-0">
            <div class="col p-3">
                <p class="fs--1 fw-bold"> 
                <?=!empty($acte->acte_date_debut_amortissement) ?  "Date début d'amortissement". " ".date('d/m/Y',strtotime(($acte->acte_date_debut_amortissement )))." - ".date('d/m/Y',strtotime("Last day of December",strtotime($acte->acte_date_debut_amortissement ))). " " ."(".round($datediff / (60 * 60 * 24)). "jours".")" : "" ?>
                </p>
            </div>
        </div>
        <hr class="my-0">
        <table class="table table-hover fs--1"> 
            <thead class="text-white" style="background:#2569C3;">
                <tr>
                    <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Décomposition</th>
                    <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Valeur</th>                   
                </tr>
            </thead>
            <tbody class="bg-300">
                <tr>
                    <td class="fw-bold text-center" style="border-right: 1px solid #ffff; border-bottom: 1px solid black">Mobilier (7)</td>
                    <td class="text-center fw-bold" style="border-right: 1px solid #ffff;border-bottom: 1px solid black">
                        <p id="oeuvre"><?=!empty($immo->immo_gros_oeuvre) ? $immo->immo_gros_oeuvre : "" ?></p>
                    </td>
                </tr>
                <tr>
                    <td class="fw-bold text-white text-center" style="border-right: 1px solid #ffff; background:#2569C3;">Année</td> 
                    <td style="border-right: 1px solid #ffff; background:#2569C3;"></td>
                </tr>              

                <?php if(!empty($acte->acte_date_debut_amortissement )) : 
                    $resteMobilier = $immo->immo_mobilier - $premiereAnneeMobilier;?>
                    <?php for($i = $dateDebut, $an= 1 ; $i <= $dateFin, $an<=8; $i++ , $an++) : ?>
                        <?php if($an == 1 ) :?>
                            <tr>
                                <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> Année <?=$an?></td>                           
                                <td class="p-2 text-center"><?=format_number_($immo->immo_mobilier)?></td>  
                            </tr>
                        <?php else : ?>  
                            <?php if (!empty($immo_general)) :
                                foreach ($immo_general as $key => $value) {
                                    if ($i == $value->immo_general_annee) {
                                        $resteMobilier = $value->immo_general_mobilier;   
                                        $nb_anneeMobilier = 8 - ($an);
                                        $diff_in_days = round($datediff / (60 * 60 * 24));  
                                        $annuiteMobilier = ($resteMobilier  / ($nb_anneeMobilier * 365 + $diff_in_days)) * 365; 
                                    }                                                                       
                                }

                            endif ?>
                          
                            <tr>
                                <td class="p-2 text-center fw-bold" style="border-right: 1px solid #ffff;"><?= $i ?> Année <?=$an?></td>                               
                                <td class="p-2 text-center"><?=format_number_($resteMobilier)?></td>  
                            </tr>
                            <?php 
                            $resteMobilier -= $annuiteMobilier ;
                            ?>
                        <?php endif ?>   
                    <?php endfor ?>
                <?php endif ?>
            </tbody>
        </table>
    </div>
    <div class="splitter m-2" style="height: auto;">

    </div>
    <div class="col-6 panel-right">
        <div id="apercu_docMobilier">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de fichier
                </div>

            </div>
            <div id="apercuMobilier">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAjoutMobilier" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadMobilier">

            </div>
        </div>
    </div>
</div>


<script>
    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });
</script>