<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Renommer le fichier</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Acte/UpdateDocumentFacade',array('method'=>"post", 'class'=>'px-2','id'=>'UpdateDocumentFacade')); ?>
 
	<?php if(isset($document->doc_facade_id)):?>
        <input type="hidden" name="doc_facade_id" id="doc_facade_id" value="<?= isset($document->doc_facade_id) ? $document->doc_facade_id : "" ;?> ">
    <?php endif;?>

    <input type="hidden" name="cliLot_id" id="cliLot_id" value="<?=$cliLot_id;?>">
    <div class="modal-body">

        <div class="row m-0">
    		<div class="col py-1">
    			<div class="">
	                <div class="">
		                <div class="form-group inline">
		                    <div>
		                        <label data-width="100"> Nom : </label>
		                        <input type="text" class="form-control" id="doc_facade_nom" name="doc_facade_nom" 
                                value="<?=  $document->doc_facade_nom ?> ">
		                        <div class="help"></div>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </div>
<?php echo form_close(); ?>


 
