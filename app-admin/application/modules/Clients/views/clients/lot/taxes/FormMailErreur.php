<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Envoye du mail (Déclaration erreur sur la taxe)</h5>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <select class="form-select" name="emeteur_erreur" id="emeteur_erreur">
                                <?php foreach ($liste_emetteur as $key => $value) : ?>
                                    <option value="<?= $value->mess_id ?>" <?= $default_user == $value->util_id ? 'selected' : '' ?>>
                                        <?= $value->util_prenom ?>&nbsp;<?= $value->util_nom ?>&nbsp;(<?= $value->mess_email ?>)
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataires :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_erreur" value="<?= $destinataire->cnctGestionnaire_email1 ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataires en copie :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_erreur_copie" value="">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object_erreur" value="<?= $object_mail ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message-text_erreur" style="height : 270px;"><?= $mailMessage ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <label data-width="200" for="recipient-name" class="col-form-label d-none" id="signature_erreur">Aucune</label>
                            <img src="" alt="" id="image_signature" style="width:400px; height:130px;" class="d-none">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-2">
                            <label data-width="200" class="col-form-label" style="font-size: 13.5px!important;">Fichiers joints :</label>
                        </div>
                        <div class="col-10 mt-1 list_doc">
                            <p style="font-weight: 500;font-size: 14px; margin-left: -26px;">Pas de fichier joints</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
    <button type="submit" class="btn btn-primary" id="sendMailTaxeErreur">Envoyer</button>
</div>
</div>