<style>
    .document_selectionner {
        background-color: red;
    }
</style>
<div class="contenair-title">
    <div class="row m-0" style="width: 43%">
        <div class="col-7">
            <h5 class="fs-1">Documents taxe foncière</h5>
        </div>
        <div class="col-3">
            <select class="form-select fs--1 m-1 no_effect" id="anneetaxe" name="anneetaxe" style="width: 96%;" data-id="<?= $cliLot_id; ?>">
                <?php for ($i = intval(date("Y") + 1); $i >= intval(date("Y")) - 1; $i--) { ?>
                    <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                        <?= $i ?>
                    </option>
                <?php } ?>
                <option value="0">Années antérieures</option>
            </select>
            <div class="fs--1 text-danger text-center"><i><?= empty($date_signature) ? "aucun mandat signé !"  : "" ?></i></div>
        </div>
        <div class="col-2">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $cliLot_id; ?>" id="ajoutdocTaxe">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row pt-3 m-0 panel-container" id="content-taxe">

</div>

<script>
    getDoctaxeAnnee(<?= $cliLot_id; ?>);
</script>