<style>
    .document_selectionner {
        background-color: red;

    }
</style>

<div class="col-5 panel-left">
    <div class="row m-0">
        <div class="col-4 bold">
            <label for="">Nom</label>
        </div>
        <div class="col-2 bold text-center">
            <label for="">Dépôt</label>
        </div>
        <div class="col-2 bold text-center">
            <label for="">Création</label>
        </div>
        <div class="col-4 bold text-center">
            <label for="">Actions</label>
        </div>
    </div>
    <hr class="my-0">
    <div class="row pt-3 m-0" style=" height: calc(40vh - 200px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
        <div class="col-12">
            <?php foreach ($document as $documents) { ?>
                <div class="row apercu_docu" id="rowdoc-<?= $documents->doc_taxe_id ?>">
                    <div class="col-4 pt-2">
                        <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_taxe_id ?>" onclick="apercuDocumentTaxe(<?= $documents->doc_taxe_id ?>)"> <?= $documents->doc_taxe_nom ?></div>
                    </div>
                    <div class="col-2 text-center">
                        <label for=""><i class="fas fa-user"></i> <?= isset($documents->util_prenom) && $documents->util_prenom != NULL ? $documents->util_prenom : $documents->client_nom  ?></label>
                    </div>
                    <div class="col-2 text-center">
                        <label for=""><i class="fas fa-calendar"></i> <?= $documents->doc_taxe_creation ?></label>
                    </div>
                    <div class="col-4 text-center">
                        <div class="btn-group fs--1" style="left: 17%;">
                            <?php
                            $message = "Document non traité";
                            if (!empty($documents->doc_taxe_traitement)) {
                                if ($documents->doc_taxe_traite == 1) {
                                    $message = "Document traité par " . $documents->util_prenom . " le " . $documents->doc_taxe_traitement;
                                } else {
                                    $message = "Document annulé par " . $documents->util_prenom . " le " . $documents->doc_taxe_traitement;
                                }
                            }
                            ?>
                            <span>
                                <button class="only_visuel_gestion btn btn-sm <?= ($documents->doc_taxe_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill btnTraitetaxe" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>" data-id="<?= $documents->doc_taxe_id ?>">
                                    <i class="fas fa-check"></i>
                                </button>
                            </span>&nbsp;
                            <span class="btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocTaxe only_visuel_gestion" data-id="<?= $documents->doc_taxe_id ?>" data-lot="<?= $cliLot_id; ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
                                <i class="far fa-edit"></i>
                            </span>&nbsp;
                            <span class="btn btn-sm btn-outline-primary rounded-pill">
                                <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_taxe_path ?>','<?= $documents->doc_taxe_nom ?>')" data-url="<?= base_url($documents->doc_taxe_path) ?>">
                                </i>
                            </span>&nbsp;
                            <span class="btn btn-sm btn-outline-danger rounded-pill btnSuppDocTaxe only_visuel_gestion" data-id="<?= $documents->doc_taxe_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                <i class="far fa-trash-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
    <hr class="my-0">
    <!-- Information taxe foncière -->
    <div class="information">
        <?php echo form_tag('Clients/cruTaxe', array('method' => "post", 'class' => 'px-2', 'id' => 'cruTaxe')); ?>
        <input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
        <input type="hidden" name="annee" id="annee">
        <div class="py-3 row">
            <div class="col">
                <h5>Informations taxe foncière <span id="annees"><?php $i = intval(date("Y")); ?> <?= $i ?> : </span></h5>
            </div>
            <div class=" col d-md-flex justify-content-md-end">
                <button type="submit" class="btn btn-primary btn-sm float-right d-none"><i class="far fa-edit"></i> Modifier</button>
            </div> <br><br>
            <div class="form-group inline px-3">
                <label class="fs--1" data-width="150">Le propriétaire n'a pas de taxe foncière : </label>
                <input class="form-check-input mb-2 p-2" type="checkbox" id="taxe_cloture" name="taxe_cloture" <?= isset($taxe->taxe_cloture) ? ($taxe->taxe_cloture == "1" ? "checked" : "") : ""; ?>>
            </div>
        </div>
        <input type="hidden" id="action_taxe" name="action_taxe" value="<?= !empty($taxe->taxe_montant) ? 'edit' : 'add'; ?>" />
        <input type="hidden" id="taxe_id" name="taxe_id" value="<?= !empty($taxe->taxe_id) ? $taxe->taxe_id : ""; ?>" />
        <div class="form-group inline montant_taxe <?= isset($taxe->taxe_cloture) ? ($taxe->taxe_cloture == "1" ? "d-none" : "") : ""; ?>">
            <div>
                <label data-width="250"> Montant taxe foncière :</label>
                <input type="text" class="form-control chiffre auto_effect" <?php if (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) echo 'disabled'; ?> value="<?= !empty($taxe->taxe_montant) ? $taxe->taxe_montant : number_format(0, 2, '.', ' '); ?>" id="taxe_montant" name="taxe_montant" data-formatcontrol="true" data-require="true" autocomplete="off" style="width: 50%;margin-left: 47%; text-align: right; " <?= $annee == 0 ? "disabled" : "" ?>>
                <div class="help"></div>
            </div>
        </div>
        <div class="form-group inline montant_taxe <?= isset($taxe->taxe_cloture) ? ($taxe->taxe_cloture == "1" ? "d-none" : "") : ""; ?>">
            <div>
                <label data-width="250"> Montant TOM :</label>
                <input type="text" class="form-control chiffre auto_effect" <?php if (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) echo 'disabled'; ?> value="<?= !empty($taxe->taxe_tom) ? $taxe->taxe_tom : number_format(0, 2, '.', ' '); ?>" id="taxe_tom" name="taxe_tom" data-formatcontrol="true" data-require="true" autocomplete="off" style="width: 53%;margin-left: 50%; text-align: right;" <?= $annee == 0 ? "disabled" : "" ?>>
                <div class="help"></div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <hr class="mb-1">
    <!-- Actions sur la taxe foncière -->
    <div class="information action <?= isset($taxe->taxe_cloture) ? ($taxe->taxe_cloture == "1" ? "d-none" : "") : ""; ?>">
        <div class="row py-4">
            <div class="col">
                <h5>Actions sur la taxe foncière <span id="anneee"><?php $i = intval(date("Y")); ?> <?= $i ?> : </span>
                    <!-- <label class="labelinfotaxe <?= (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) ? 'block' : 'd-none' ?>"> 
                            Informations validées par 
                            <span id="prenom"> <?= !empty($taxe->util_prenom) ? $taxe->util_prenom : ''; ?> </span>
                            le <span id="date"><?= !empty($taxe->taxe_date_validation) ? date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $taxe->taxe_date_validation))) : ''; ?></span>  
                        </label> -->
                </h5>
            </div>
            <div class="col text-end">
                <button class="btn btn-sm btn-secondary fs--1" type="button" id="get_historique_taxe">Historique d'email</button>
            </div>
        </div>
        <div class="only_visuel_gestion auto_effect">
            <button type="submit" id="validetaxe" data-id="<?= !empty($taxe->taxe_id) ? $taxe->taxe_id : ""; ?>" class="btn btn-primary btn-sm col-5 btnValideInfotaxe <?= (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) ? 'd-none' : 'block' ?>">
                Valider les informations
            </button>
            <button type="submit" id="annulertaxe" data-id="<?= !empty($taxe->taxe_id) ? $taxe->taxe_id : ""; ?>" class="btn btn-danger btn-sm col-5 btnAnnulerInfotaxe <?= (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) ? 'block' : 'd-none' ?>">
                Annuler la validation
            </button>
            <button type="button" class="btn btn-secondary btn-sm col-5 declarerErreur">Déclarer erreur</button>
        </div>
        <br>
        <div class="only_visuel_gestion auto_effect">

            <button class="btn btn-sm btn-primary col-5 envoiTom m-0 <?= (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) ? 'block' : 'd-none' ?>" <?= $rembst_tom->rembst_tom == "NON" ? 'disabled' : '' ?>>
                <span> Envoyer un mail au Gestionnaire </span>
            </button>

            <button class="btn btn-sm btn-primary col-5 envoiTomAuClient m-0 <?= (!empty($taxe->taxe_valide) && $taxe->taxe_valide == 1) ? 'block' : 'd-none' ?>" <?= $rembst_tom->rembst_tom == "NON" ? 'disabled' : '' ?>>
                <span> Envoyer un mail au Propriétaire </span>
            </button>
        </div>


        <!-- <ul id="liste-history" class="mt-3" style="list-style-type : none">
            <?php foreach ($htaxe as $htaxes) { ?>
                <li class="fs--1">
                    <?php if ($htaxes->htaxe_valide == 1) {
                        echo 'Informations validées par 
                            <span id="prenom"> ' . $htaxes->util_prenom . '</span>
                            le <span id="date"> ' . date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $htaxes->htaxe_date_validation))) . '</span>';
                    } else {
                        echo
                        'Informations annulées par 
                            <span id="prenom"> ' . $htaxes->util_prenom . '</span>
                            le <span id="date"> ' . date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $htaxes->htaxe_date_validation))) . '</span>';
                    }
                    ?>
                </li>
            <?php } ?>
        </ul> -->

        <div id="tom-history" class="history-container">

        </div>

    </div>
</div>
<div class="splitter m-2">

</div>
<div class="col-5 panel-right">
    <div id="aperculistdocTaxe">
        <div id="nonApercu" style="display:none;">
            <div class="text-center text-danger pt-10">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de fichier
            </div>

        </div>
        <div id="apercu">
            <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade" id="modalAjoutDoc" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="contenaireUpload">

            </div>
        </div>
    </div>
</div>

<script>
    deactivate_input_gestion();
    only_visuel_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();

        var utilId = $("#util_id").val();
    });

    $(document).on('click', '#taxe_cloture', function() {
        if ($('#taxe_cloture').is(':checked')) {
            $("#taxe_cloture").prop("checked", true);
            $('.montant_taxe').addClass('d-none')
            $('.action').addClass('d-none')
        } else {
            $("#taxe_cloture").prop("checked", false);
            $('.montant_taxe').removeClass('d-none')
            $('.action').removeClass('d-none')
        }
        submitTaxe();
    });
</script>