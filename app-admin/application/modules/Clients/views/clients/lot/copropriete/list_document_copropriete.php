<div class="contenair-title">
    <input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_copropriete">
    <div class="row m-0" style="width: 50%">
        <div class="col-7">
            <h5 class="fs-1">Documents Copropriété</h5>
        </div>
        <div class="col-3">

        </div>
        <div class="col-2">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1" data-id="<?= $cliLot_id ?>" id="ajoutdocCopropriete">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left" style="width: 881px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0">
            <div class="col-12" style="height: calc(50vh - 21px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
                <div class=" col-12">
                    <?php if (!empty($document)) : ?>
                        <?php foreach ($document as $documents) { ?>
                            <div class="row apercu_doc" id="docrowcopropriete-<?= $documents->doc_id ?>">
                                <div class="col-4 pt-2">
                                    <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_id ?>" onclick="apercuDocumentCopropriete(<?= $documents->doc_id ?>)"> <?= $documents->doc_nom ?></div>
                                </div>
                                <div class="col-2 text-center">
                                    <label for=""><i class="fas fa-user"></i>
                                        <?= $documents->util_prenom ?>
                                    </label>
                                </div>
                                <div class="col-2 text-center">
                                    <label for=""><i class="fas fa-calendar"></i>
                                        <?= $documents->doc_date_creation ?>
                                    </label>
                                </div>
                                <div class="col-4 text-center">
                                    <div class="btn-group fs--1" style="left: 17%;">
                                        <span class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocCopropriete" data-id="<?= $documents->doc_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer" data-cliLot_id="<?= $cliLot_id; ?>">
                                            <i class="far fa-edit"></i>
                                        </span>&nbsp;
                                        <span class="only_visuel_gestion btn btn-sm btn-outline-primary rounded-pill">
                                            <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_path ?>',' <?= $documents->doc_nom ?>')" data-url="<?= base_url($documents->doc_path) ?>">
                                            </i>
                                        </span>&nbsp;
                                        <span class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocCopropriete" data-id="<?= $documents->doc_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                            <i class="far fa-trash-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <hr class="my-0"><br>

        <!-- Commentaire -->
        <div class="row">
            <div class="col-md-12">
                <label for="commentaire">Commentaire :</label>
                <textarea class="form-control" name="commentaire_doc_Copropriete" id="commentaire_doc_Copropriete" style="height: 80px;"><?= !empty($commentairecopropriete) ? $commentairecopropriete->commentaire : '' ?></textarea>
            </div>
        </div>
    </div>
    <div class="split m-1"></div>
    <div class="col-6 panel-right">
        <div id="apercuFiscaldocCopropriete">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>
            </div>
            <div id="apercudoc_Copropriete">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });
</script>