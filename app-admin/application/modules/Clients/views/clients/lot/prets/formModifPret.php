<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Modifier le montant restant dû</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>

<div class="modal-body">
    <?php echo form_tag('Clients/updateMontantdu', array('method' => "post", 'class' => 'px-2', 'id' => 'updateMontantdu')); ?>
    <input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id ?>">
    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="150"> Montant restant dû : </label>
                    <input type="number" id="pret_capital_restant_du" name="pret_capital_restant_du" class="form-control input" data-require="true" value="<?= $getPret->pret_capital_restant_du ?>" style="text-align:right">
                    <div class="input-group-append">
                        <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                    </div>
                    <div class=" help"> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Valider</button>
    </div>
    <?php echo form_close(); ?>
</div>