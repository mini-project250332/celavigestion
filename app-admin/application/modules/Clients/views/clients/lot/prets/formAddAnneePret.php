<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajout de la première année d'emprunts</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
 
<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>                          
                            <span class="alert alert-warning">
                                <p class="fs--1 text-center"> 
                                    Vous allez ajouter la première année d'emprunts.
                                    Attention, par la suite, vous ne pourrez ajouter que des années ultérieures.
                                </p>
                            </span> 
                        </div>
                    </div>
                    <hr class="mt-2">
                    <div class="form-group inline">
                        <div>
                            <label data-width="150"> Première année : </label>
                            <select class="form-select fs--1" id="anneePret" name="">
                                <?php for($i = intval(date("Y")) + 1 ; $i >= intval(Year) ; $i--){?>
                                    <option value="<?= $i ?>" <?= ($i==date("Y"))?"selected" : ""?>> 
                                        <?= $i ?>
                                    </option>
                                <?php }?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary validerAnnee">Valider</button>
    </div>
</div>


 
