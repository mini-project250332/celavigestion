<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Documents Emprunts</h5>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $cliLot_id ?>" id="ajoutDocPret">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row pt-3 m-0 panel-container" id="">

    <div class="col-6 panel-left" style="width: 911px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0" style=" height: calc(45vh - 200px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
            <div class="col-12">
                <?php foreach ($document as $documents) { ?>

                    <div class="row apercu_docu" id="docrow-<?= $documents->doc_pret_id ?>">
                        <div class="col-4 pt-2">
                            <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_pret_id ?>" onclick="apercuDocumentPret(<?= $documents->doc_pret_id ?>)">
                                <?= $documents->doc_pret_nom ?>
                            </div>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-user"></i>
                                <?php if (isset($documents->util_prenom) &&  $documents->util_prenom != NULL) {
                                    $nom = $documents->util_prenom;
                                } else {
                                    $nom = $documents->client_nom . ' ' . $documents->client_prenom;
                                }
                                ?>
                                <?= $nom ?>
                            </label>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-calendar"></i>
                                <?= $documents->doc_pret_creation ?>
                            </label>
                        </div>
                        <div class="col-4 text-center">
                            <div class="btn-group fs--1" style="left: 17%;">
                                <?php
                                $message = "Document non traité";
                                if (!empty($documents->doc_pret_traitement)) {
                                    if ($documents->doc_pret_traite == 1) {
                                        $message = "Document traité par " . $documents->util_prenom . " le " . $documents->doc_pret_traitement;
                                    } else {
                                        $message = "Document annulé par " . $documents->util_prenom . " le " . $documents->doc_pret_traitement;
                                    }
                                }
                                ?>
                                <button class="only_visuel_gestion btn btn-sm <?= ($documents->doc_pret_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill  btnTraitePret" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>" data-id=" <?= $documents->doc_pret_id ?>">
                                    <i class="fas fa-check"></i>
                                </button> &nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocPret" data-id="<?= $documents->doc_pret_id ?>" data-lot="<?= $cliLot_id; ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
                                    <i class="far fa-edit"></i>
                                </span>&nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-primary rounded-pill">
                                    <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_pret_path ?>','<?= $documents->doc_pret_nom ?>')" data-url="<?= base_url($documents->doc_pret_path) ?>">
                                    </i>
                                </span>&nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocPret" data-id="<?= $documents->doc_pret_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                    <i class="far fa-trash-alt"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <hr class="my-0">
        <div class="tableau">
            <div class="py-3 row">
                <div class="col">
                    <h5>Plan de financement (échéancier emprunts)</h5>
                </div>
                <!-- <div class="col d-md-flex justify-content-md-end">
                    <button class="only_visuel_gestion btn btn-sm btn-primary m-1 <?= empty($pret) ? 'add-annee' : 'ajouterAnnee' ?>" id="ajouterAnnee">
                        <span class="fas fa-plus me-1"></span>
                        <span> Année </span>
                    </button>
                </div> -->
            </div>

            <div class="py-2 row">
                <div class="col-4">
                    <p>Présence d'emprunts pour ce lot :</p>
                </div>
                <div class="col-4">
                    <select class="form-select auto_effect" name="presence_emprunts" id="presence_emprunts">
                        <option value="1" <?= !empty($presence_pret) && $presence_pret->etat_presence == 1 ? 'selected' : ' ' ?> data-id="<?= $cliLot_id ?>">Oui</option>
                        <option value="2" <?= !empty($presence_pret) && $presence_pret->etat_presence == 2 ? 'selected' : ' ' ?> data-id="<?= $cliLot_id ?>">Non</option>
                    </select>
                </div>
                <div class="col-4" id="date_declaration_of_emprunt"></div>
            </div>
            <?php if (isset($pret->pret_date_modif)) : ?>
                <div class="py-2 row">
                    <div class="col-8">
                        <p>Capital restant dû <?= $date ?> saisi sur l'extranet par le propriétaire le <?= date("d/m/Y", strtotime(str_replace('/', '-', $pret->pret_date_modif))) ?> :</p>
                    </div>
                    <div class="col fw-bold p-0">
                        <p><?= $pret->pret_capital_restant_du . ' ' . '€' ?></p>
                    </div>
                    <div class="col"></div>
                </div>
                <div>
                    <button class="only_visuel_gestion btn btn-sm btn-primary m-1" id="Modifier_restant_du" data-id="<?= $cliLot_id ?>">
                        <span> Modifier le montant restant dû </span>
                    </button>
                </div>
            <?php else : ?>
                <div class="py-2 row">
                    <p>Le capital restant dû <?= $date ?> n'a pas été saisi sur l'Extranet par le propriétaire.</p>
                </div>
            <?php endif; ?>
            <!-- <div class="" style="margin-left: -21px; width: 105%;" id="contenu_emprunts_tab">
                <div class="table-responsive">
                    <table class="table table-hover fs--1">
                        <thead class="text-white" style="background:#2569C3;">
                            <tr>
                                <th class="p-0 text-center pb-4" style="border-right: 1px solid #ffff;">Année</th>
                                <th class="p-2" style="border-right: 1px solid #ffff;">Capital <br>
                                    Restant <br>
                                    du N-1
                                </th>
                                <th class="p-0 text-center pb-3" style="border-right: 1px solid #ffff;">Déblocage <br>
                                    Capital
                                </th>
                                <th class="p-0 pb-2 text-center" style="border-right: 1px solid #ffff;">Ecart CRD <br>
                                    N-1 Réel <br>
                                    et théorique
                                </th>
                                <th class="p-0 text-center pb-4" style="border-right: 1px solid #ffff;">Remboursement </th>
                                <th class="p-2 text-center pb-3" style="border-right: 1px solid #ffff;">Capital <br>
                                    Amorti N
                                </th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Capital <br>
                                    restant du <br>
                                    fin de période
                                </th>
                                <th class="p-2 pb-4" style="border-right: 1px solid #ffff;">Intérêts payés
                                </th>
                                <th class="p-2 pb-4" style="border-right: 1px solid #ffff;">Assurance</th>
                                <th class=" p-2 pb-4 only_visuel_gestion">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="bg-200">
                            <?php foreach ($pret as $prets) : ?>
                                <tr id="rowmontant-<?= $prets->pret_id ?>" class="table-pret">
                                    <input type="hidden" id="pret_id" name="pret_id" value="<?= $prets->pret_id ?>">
                                    <td class="bold" style="border-right: 1px solid #ffff;">
                                        <span class="aannee">
                                            <?= $prets->pret_annee ?>
                                        </span>
                                        <input type="text" id="" class="form-control mb-0 d-none auto_effect" style="width: 122%;margin-left: -3px; ">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_capital_restant_prec, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_capital_restant_prec-<?= $prets->pret_id ?>" name="pret_capital_restant_prec" class="form-control mb-0 input_montant auto_effect" style="width : 137%;margin-left:-11px;display : none;" value="<?= $prets->pret_capital_restant_prec ?>">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_capital_deblocage, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_capital_deblocage-<?= $prets->pret_id ?>" name="pret_capital_deblocage" class="form-control mb-0 input_montant auto_effect" style="width: 137%;margin-left: -12px;display : none;" value="<?= $prets->pret_capital_deblocage ?>">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_ecart_crd, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_ecart_crd-<?= $prets->pret_id ?>" name="pret_ecart_crd" class="form-control mb-0 input_montant auto_effect" style="width:125%;margin-left: -9px;display : none;" value="<?= $prets->pret_ecart_crd ?>">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_capital_amorti + $prets->pret_interet + $prets->pret_assurance, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_remboursement-<?= $prets->pret_id ?>" name="pret_remboursement" class="form-control mb-0 input_montant auto_effect" style="width: 121%;margin-left: -9px; display : none;" value="<?= $prets->pret_remboursement ?>" disabled>
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_capital_amorti, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_capital_amorti-<?= $prets->pret_id ?>" name="pret_capital_amorti" class="form-control mb-0 input_montant auto_effect" style="width : 131%;margin-left: -10px; display : none;" value="<?= $prets->pret_capital_amorti ?>">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant capital_restant auto_effect" data-capital="<?= $prets->pret_capital_restant_prec - $prets->pret_capital_amorti ?>">
                                            <?= number_format($prets->pret_capital_restant_prec - $prets->pret_capital_amorti, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_capital_restant-<?= $prets->pret_id ?>" name="pret_capital_restant" class="form-control mb-0 input_montant auto_effect" style="width : 121%;margin-left: -10px; display : none;" value="<?= $prets->pret_capital_restant ?>" disabled>
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_interet, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_interet-<?= $prets->pret_id ?>" name="pret_interet" class="form-control mb-0 input_montant auto_effect" style="width: 121%; right: 10px; display : none;" value="<?= $prets->pret_interet ?>">
                                    </td>
                                    <td class="text-center" style="border-right: 1px solid #ffff;">
                                        <span class="montant">
                                            <?= number_format($prets->pret_assurance, 2, ",", ".") ?>
                                        </span>
                                        <input type="text" id="pret_assurance-<?= $prets->pret_id ?>" name="pret_assurance" class="form-control mb-0 input_montant auto_effect" style="width : 127%;margin-left: -10px; display : none;" value="<?= $prets->pret_assurance ?>">
                                    </td>
                                    <td class="only_visuel_gestion text-end" style="border-right: 1px solid #ffff;" id="action_in_contenu">
                                        <div>
                                            <span class="btn btn-sm btn-warning p-1 fs--2 modif_pret  <?= ($prets->pret_id == $pret_id->pret_id) ? 'updateMontant' : 'alertUpdate' ?>" id="updateMontant-<?= $prets->pret_id ?>" style="width: 23px;height: 24px" data-id="<?= $prets->pret_id ?>">
                                                <i class=" fas fa-edit" style="margin : 2px"></i>
                                            </span>
                                            <button class="btn btn-sm btn-danger p-1 fs--2 suppr_pret <?= ($prets->pret_id == $pret_id->pret_id) ? 'deleteMontant' : 'alertPret' ?>" id="deleteMontant-<?= $prets->pret_id ?>" style="width: 23px;height: 24px" data-id="<?= $prets->pret_id ?>" data-lot="<?= $cliLot_id ?>">
                                                <i class=" fas fa-trash-alt" style="margin : 2px"></i>
                                            </button>
                                            <span class="btn btn-sm btn-primary p-1 fs--2 validerMontant " id="validerMontant-<?= $prets->pret_id ?>" data-id="<?= $prets->pret_id ?>" style="display : none;width: 23px;height: 24px">
                                                <i class="fas fa-check fs--1 " style="margin : 1px"></i>
                                            </span>
                                            <span class="btn btn-sm btn-danger p-1 fs--2 annulerMontant " id="annulerMontant-<?= $prets->pret_id ?>" style="display : none;width: 
                                            25px;height: 24px">
                                                <i class="fas fa-window-close fs-1 " style="margin : -3px"></i>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            <tr id="row-montant" class="d-none">
                                <input type="hidden" name="cliLot_id" value="<?= $cliLot_id ?>">
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_annee" name="pret_annee" class="form-control mb-0 pret_annee auto_effect" style="width: 122%;margin-left: -3px;">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_capital_restant_prec" name="pret_capital_restant_prec" class="form-control mb-0 " style="width: 117%;margin-left:-5px">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_capital_deblocage" name="pret_capital_deblocage" class="form-control mb-0 auto_effect" style="width: 120%;margin-left: -7px">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_ecart_crd" name="pret_ecart_crd" class="form-control mb-0 " style="width: 113%;margin-left: -6px">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_remboursement" name="pret_remboursement" class="form-control mb-0 auto_effect" style="width: 108%;margin-left: -5px" disabled>
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_capital_amorti" name="pret_capital_amorti" class="form-control mb-0 auto_effect" style="width : 120%;margin-left: -8px">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_capital_restant" name="pret_capital_restant" class="form-control mb-0 auto_effect" style="width : 112%;margin-left: -6px" disabled>
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_interet" name="pret_interet" class="form-control mb-0 auto_effect" style="width: 111%; right: 6px">
                                </td>
                                <td class="p-2" style="border-right: 1px solid #ffff">
                                    <input type="text" id="pret_assurance" name="pret_assurance" class="form-control mb-0 auto_effect" style="width : 118%;margin-left: -6px">
                                </td>
                                <td class="only_visuel_gestion p-2" style="border-right: 1px solid #ffff">
                                    <div>
                                        <button type="submit" class="btn btn-sm btn-primary p-1 fs--1" style="width: 23px;height: 24px" id="validerAjoutMontant">
                                            <i class="fas fa-check fs--1 " style="margin : 1px"></i>
                                        </button>
                                        <span class="btn btn-sm btn-danger p-1 fs--2" id="annulerAjoutMontant" style="width: 25px;height: 24px">
                                            <i class="fas fa-window-close fs-1" style="margin : -3px"></i>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> -->

            <div class="row mt-3 mb-1">
                <div class="col">
                    <p>Commentaire :</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <textarea name="commentaire_emprunts" id="commentaire_emprunts" class="form-control" style="height: 100px;"><?= !empty($presence_pret) ? $presence_pret->commentaire : '' ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="splitter m-2">

    </div>
    <div class="col-6 panel-right">
        <div id="apercu_docPret">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>
            </div>
            <div id="apercuPret">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade" id="modalAjout" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="Upload">

            </div>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });

    addPresencePret($('#presence_emprunts').val(), $("#presence_emprunts option:selected").data("id"), $('#commentaire_emprunts').val(), 'view');
</script>