<div class="uploader_file_pret">

</div>
<div class="clearfix p-t-5 p-b-5">
    <div class="float-left">
    </div>
    <div class="d-flex justify-content-end">
        <button type="button" id="stopped-upload" class="btn btn-danger d-none btn-sm mx-2 text-white">
            <i class="fas fa-times-circle red"></i>
            Annuler
        </button>
        <button type="button" id="add-files-pret" class="btn btn-info btn-sm bold mx-1">
            <i class="fas fa-plus-circle"></i>
            Ajouter des fichiers
        </button>
        <button type="button" id="start-files-pret" class="btn btn-success btn-sm d-none text-white">
            <i class="fas fa-cloud-upload-alt green "></i>
            Démarrer l'envoi
        </button>
    </div>
</div>

<input id="url_upload_pret" type="hidden" name="url_upload_pret" value="<?php echo base_url("Clients/Clients/uploadfilePret"); ?>">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id ?>">

<script type="text/javascript">
    $(document).ready(function() {
        var url_upload_pret = $('#url_upload_pret').val();
        $(".uploader_file_pret").pluploadQueue({
            browse_button: 'add-files-pret',
            runtimes: 'html5,flash,silverlight,html4',
            chunk_size: '1mb',
            url: url_upload_pret,
            rename: true,
            unique_names: true,
            sortable: true,
            dragdrop: true,
            flash_swf_url: '<?php echo base_url("assets/js/plupload/js/Moxie.swf"); ?>',
            silverlight_xap_url: '<?php echo base_url("assets/js/plupload/js/Moxie.xap"); ?>',
        });

        var uploader = $('.uploader_file_pret').pluploadQueue();
        uploader.bind('Browse', function() {

        });
        uploader.bind('FilesRemoved', function() {
            if (uploader.files.length > 0) {
                $('#start-files-pret').removeClass('d-none');
            } else {
                $('#start-files-pret').addClass('d-none');
                $('#stopped-upload').addClass('d-none');
            }
        });
        uploader.bind('FilesAdded', function() {
            if (uploader.files.length > 0) {
                $('#start-files-pret').removeClass('d-none');
            } else {
                $('#start-files-pret').addClass('d-none');
                $('#stopped-upload').addClass('d-none');
            }
        });
        uploader.bind('UploadComplete', function() {
            // upload finish

        });
        uploader.bind('FileUploaded', function(response) {
            if (uploader.files.length > 0) {
                if (uploader.files.length == (uploader.total.uploaded + uploader.total.failed)) {
                    $("#modalAjout").modal('hide');
                    $('.modal-backdrop').remove();
                    var cliLot_id = $('#cliLot_id').val();
                    getDocPret(cliLot_id);
                }
            }
        });

        $('#start-files-pret').click(function(e) {
            e.preventDefault();
            var check = false;
            if (uploader.files.length > 0) {
                var files_names = {};
                var pathname_url = window.location.pathname;
                var cliLot_id = $('#cliLot_id').val();

                $("div.plupload_file_name").each(function(ind, elem) {
                    var content = $(this).html();
                    var infos_file = {};
                    if (content != $.trim('Nom du fichier')) {

                        if ($(this).parent().attr("id") != null) {
                            infos_file.originalName = content;
                            infos_file.fileName = $(this).parent().attr("id");
                            files_names[ind] = infos_file;
                        }
                    }
                });
                $('#stopped-upload').removeClass('d-none');

                uploader.settings.multipart_params = {
                    cliLot_id: cliLot_id,
                    fichiers: files_names, // Injection des noms des fichiers 
                };

                uploader.start();
            }
        });

        $('#stopped-upload').click(function(e) {
            uploader.stop();
        });
    });
</script>