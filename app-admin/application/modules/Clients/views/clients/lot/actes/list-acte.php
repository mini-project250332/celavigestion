<?php $mandat_date_signature =  isset($mandat) && !empty($mandat) && $mandat->mandat_date_signature != NULL ? $mandat->mandat_date_signature : ''; ?>

<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col-3">
            <h5 class="fs-1">Documents actes</h5>
        </div>
        <div class="col-6">
            <?php if ($mandat_date_signature == '') {
                echo '<div class="alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                    aucune mandat signé
                </div>';
            } ?>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1" data-id="<?= $cliLot_id ?>" id="ajoutdocActe">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<?php
if (!empty($acte)) {
    $date_debut = strtotime($acte->acte_date_debut_amortissement);
    $date_fin = strtotime("Last day of December", strtotime($acte->acte_date_debut_amortissement));
    $datediff = $date_fin - $date_debut;
    //echo round($datediff / (60 * 60 * 24));
}
?>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-5 panel-left" style="width: 911px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        
        <div class="row pt-3 m-0" style=" height: calc(45vh - 200px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
            <div class="col-12">
                <?php foreach ($document as $documents) { ?>
                    <div class="row apercu_doc" id="docrowActe-<?= $documents->doc_acte_id ?>">
                        <div class="col-4 pt-2">
                            <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_acte_id ?>" onclick="apercuDocumentActe(<?= $documents->doc_acte_id ?>)">
                                <?= $documents->doc_acte_nom ?>
                            </div>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-user"></i>
                                <?= $documents->util_prenom ?>
                            </label>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-calendar"></i>
                                <?= $documents->doc_acte_creation ?>
                            </label>
                        </div>
                        <div class="col-4 text-center">
                            <div class="btn-group fs--1" style="left: 17%;">
                                <span>
                                    <button class="only_visuel_gestion btn btn-sm btn-success rounded-pill <?= ($documents->doc_acte_traite == 1) ? '' : 'd-none' ?> btnNonTraiteActe" id="TraiterActe" data-bs-toggle="tooltip" data-bs-placement="bottom" title="document traité par <?= $documents->prenom_util ?> le <?= $documents->doc_acte_traitement ?>" data-id="<?= $documents->doc_acte_id ?>">
                                        <i class="fas fa-check"></i>
                                    </button>
                                </span>
                                <button class="only_visuel_gestion btn btn-sm btn-secondary rounded-pill <?= ($documents->doc_acte_traite == 1) ? 'd-none' : '' ?> btnTraiteActe" id="nonTraiterActe" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= !empty($documents->prenom_util) ? 'traitement document annulé par ' . $documents->prenom_util . ' ' . 'le ' . $documents->doc_acte_traitement . ' ' : 'Document non traité'; ?>" data-id="<?= $documents->doc_acte_id ?>">
                                    <i class="fas fa-check"></i>
                                </button> &nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocActe" data-id="<?= $documents->doc_acte_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer" data-lot="<?= $cliLot_id; ?>">
                                    <i class="far fa-edit"></i>
                                </span>&nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-primary rounded-pill">
                                    <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_acte_path ?>','<?= $documents->doc_acte_nom ?>')" data-url="<?= base_url($documents->doc_acte_path) ?>">
                                    </i>
                                </span>&nbsp;
                                <span class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocActe" data-id="<?= $documents->doc_acte_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                    <i class="far fa-trash-alt"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <hr class="my-0">

        <div class="row mt-2 m-0 px-2">
            <div class="col">

                <?php 
                    $verify_identite = ($client->check_identite == 1) ? true : false;
                ?>
                <div class="">
                    <div class="form-group form-check mb-0">
                        <input data-context="acte" data-client_id="<?=$client->client_id;?>" type="checkbox" class="form-check-input checkbox_check_identite_acte" id="checkbox_check_identite_acte" <?=($verify_identite) ? 'checked' : '';?>>
                        <label class="form-check-label" for="check_identite" style="font-size: 1rem !important;" >
                            J’ai vérifié l’identité du propriétaire avec sa pièce d’identité.
                        </label>
                    </div>
                    <div class="mb-2">
                        <span class="fst-italic fs--1">
                            <?php if(isset($data_verification_identite) && !empty($data_verification_identite)): ?>
                                <?php $dernierAction = $data_verification_identite[count($data_verification_identite) - 1]; ?>
                                <?=($dernierAction->histvi_action == 1) ? 'coché' : 'décoché';?> 
                                par
                                <?=$dernierAction->util_prenom.' '.$dernierAction->util_nom;?>
                                le
                                <?=date('d/m/Y h:i',strtotime($dernierAction->histvi_date));?>
                            <?php endif;?>
                        </span>
                    </div>
                </div>

            </div>
        </div>

        <hr class="my-0">

        <div class="row mt-2">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1" id="<?= $mandat_date_signature != '' ? 'updateActe' : 'btn_bail_alert_acte' ?>">
                    <span class="fas fa-edit me-1"></span>
                    <span> Modifier </span>
                </button>
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1 d-none terminerActe">
                    <span class="fas fa-check me-1"></span>Terminer
                </button>
            </div>
        </div>
        <div class="information-acte">
            <?php echo form_tag('Acte/cruActe', array('method' => "post", 'class' => 'px-2', 'id' => 'cruActe')); ?>
            <input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
            <input type="hidden" id="acte_id" name="acte_id" value="<?= !empty($acte->acte_id) ? $acte->acte_id : ""; ?>" />
            <input type="hidden" id="immo_id" name="immo_id" value="<?= !empty($immo->immo_id) ? $immo->immo_id : ""; ?>" />
            <input type="hidden" id="action_acte" name="action_acte" value="<?= empty($acte->acte_id) ? 'add' : 'edit'; ?>" />
            <div class="row py-3">
                <div class="col">
                    <h5 class="title_acte">Informations générales de l'acquisition</h5>
                    <div class=" col d-md-flex justify-content-md-end">
                        <button type="submit" class="btn btn-primary btn-sm float-right d-none"><i class="far fa-edit"></i> Modifier</button>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250">Type d'acquisition :</label>
                            <select class="form-select auto_effect" id="acte_type_acquisition" name="acte_type_acquisition" disabled>
                                <option value="Acquisition ancien" <?= isset($acte->acte_type_acquisition) ? ($acte->acte_type_acquisition ==  "Acquisition ancien" ? "selected " : "") : ""; ?>>Acquisition ancien</option>
                                <option value="Reprise" <?= isset($acte->acte_type_acquisition) ? ($acte->acte_type_acquisition ==  "Reprise" ? "selected " : "") : ""; ?>>Reprise</option>
                                <option value="VEFA" <?= isset($acte->acte_type_acquisition) ? ($acte->acte_type_acquisition ==  "VEFA" ? "selected " : "") : ""; ?>>VEFA</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="200">Date de l’acte notarié :</label>
                            <input type="date" class="form-control auto_effect" id="acte_date_notarie" name="acte_date_notarie" value="<?= !empty($acte->acte_date_notarie) ? date('Y-m-d', strtotime($acte->acte_date_notarie)) : ''; ?>" disabled>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h5 class="title_acte">Montants de l'acquisition </h5>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250"> Valeur acquisition HT :</label>
                            <input type="text" class="form-control text-end chiffre auto_effect" id="acte_valeur_acquisition_ht" name="acte_valeur_acquisition_ht" value="<?= !empty($acte->acte_valeur_acquisition_ht) ? $acte->acte_valeur_acquisition_ht : ''; ?>" style="margin-left: 15px" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="300">Frais d'acquisition :</label>
                            <input type="text" class="form-control text-end auto_effect chiffre" id="acte_frais_acquisition" name="acte_frais_acquisition" value="<?= !empty($acte->acte_frais_acquisition) ? $acte->acte_frais_acquisition : 0; ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250"> Valeur terrain HT :</label>
                            <input type="text" class="form-control text-end chiffre auto_effect" id="acte_valeur_terrain_ht" name="acte_valeur_terrain_ht" value="<?= !empty($acte->acte_valeur_terrain_ht) ? $acte->acte_valeur_terrain_ht : ''; ?>" style="margin-left: 44px;" disabled>
                            <input type="text" class="form-control auto_effect" id="acte_taux_terrain" name="acte_taux_terrain" style="margin-left: 2px; width: 42%; text-align:right;" value="<?= empty($acte->acte_taux_terrain) ? $taux_terrain : $acte->acte_taux_terrain ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250"> Valeur Construction HT :</label>
                            <input type="text" class="form-control text-end chiffre auto_effect" id="acte_valeur_construction_ht" name="acte_valeur_construction_ht" value="<?= !empty($acte->acte_valeur_construction_ht) ? $acte->acte_valeur_construction_ht : ''; ?>" style="margin-left: 2px;" disabled>
                            <input type="text" id="acte_taux_construction" name="acte_taux_construction" class="form-control auto_effect" style="margin-left: 2px; width: 42%; text-align:right;" value="<?= empty($acte->acte_taux_construction) ? $taux_construction : $acte->acte_taux_construction ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250"> Valeur mobilier HT :</label>
                            <input type="text" class="form-control text-end chiffre auto_effect" id="acte_valeur_mobilier_ht" name="acte_valeur_mobilier_ht" value="<?= !empty($acte->acte_valeur_mobilier_ht) ? $acte->acte_valeur_mobilier_ht : ''; ?>" style="margin-left: 30px;" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <h5 class="title_acte">Pour la TVA par rapport à l'acquisition</h5>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250"> Montant TVA Immo :</label>
                            <input type="text" class="form-control chiffre auto_effect" id="acte_montant_tva_immo" name="acte_montant_tva_immo" value="<?= !empty($acte->acte_montant_tva_immo) ? $acte->acte_montant_tva_immo : ''; ?>" style="margin-left: 79px" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250">Montant TVA Frais d'acquisition :</label>
                            <input type="text" class="form-control chiffre auto_effect" id="acte_montant_tva_frais_acquisition" name="acte_montant_tva_frais_acquisition" value="<?= !empty($acte->acte_montant_tva_frais_acquisition) ? $acte->acte_montant_tva_frais_acquisition : ''; ?>" style="margin-left: -3px" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250">Date demande remboursement :</label>
                            <input type="date" class="form-control auto_effect" id="acte_date_remboursement" name="acte_date_remboursement" value="<?= !empty($acte->acte_date_remboursement) ? date('Y-m-d', strtotime($acte->acte_date_remboursement)) : ''; ?>" style="margin-left: 2px" disabled>
                        </div>
                    </div>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="250">Date de règlement :</label>
                            <input type="date" class="form-control auto_effect" id="acte_date_reg" name="acte_date_reg" value="<?= !empty($acte->acte_date_reg) ? date('Y-m-d', strtotime($acte->acte_date_reg)) : ''; ?>" style="margin-left: 85px" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <h5 class="title_acte">Informations pour les amortissements</h5>
                    <div class="form-group inline p-0">
                        <div class="">
                            <label data-width="100"> Date début amortissements :</label>
                            <input type="date" class="form-control auto_effect" id="acte_date_debut_amortissement" name="acte_date_debut_amortissement" value="<?= !empty($acte->acte_date_debut_amortissement) ? date('Y-m-d', strtotime($acte->acte_date_debut_amortissement)) : ''; ?>" style="width: 50%;margin-left: 92px;" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-group inline p-0">
                        <div class="message_verification d-none">
                            <p class=" alert alert-secondary text-wrap text-center fs--6"> <i>Avez-vous bien vérifié la date de début d’amortissement par rapport au démarrage du bail ?</i></p>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="form-group inline p-0">
                            <div class="">
                                <label data-width="300">Commentaire :</label>
                                <textarea name="commentaire_acte" id="commentaire_acte" class="form-control auto_effect" style="height: 100px;" disabled><?= !empty($acte) ? $acte->commentaire_acte : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <hr class="my-0">
        <div class="row">
            <div class="col">
                <h5 class="title_acte">Tableau récapitulatif d'immobilisation</h5>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover fs--1">
                        <thead class="text-white" style="background:#2569C3;">
                            <tr>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Décomposition</th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Valeur immobilisation</th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Durée</th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Taux répartition</th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Amorts effectués <?= date('Y') - 1 ?></th>
                                <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Reste à amortir</th>
                            </tr>
                        </thead>
                        <tbody class="bg-300">
                            <tr>
                                <td class=" text-center p-2" style="border-right: 1px solid #ffff;">Gros Œuvre</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="oeuvre">
                                        <?= !empty($immo) ? format_number_($immo->immo_gros_oeuvre) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">50</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">40%</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="amortOeuvre"><?= !empty($immo) ? format_number_($amortOeuvre) : "" ?></p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="resteAmortOeuvre">
                                        <?= !empty($immo) ? format_number_($resteAmortOeuvre) : "" ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class=" text-center p-2" style="border-right: 1px solid #ffff;">Facades étenchéité</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="facade">
                                        <?= !empty($immo) ? format_number_($immo->immo_facade) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">20</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">20%</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="amortFacade">
                                        <?= !empty($immo) ? format_number_($amortFacade) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="resteAmortfacade"><?= !empty($immo) ? format_number_($resteAmortFacade) : "" ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class=" text-center p-2" style="border-right: 1px solid #ffff;">Installations (IGT)</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="installation">
                                        <?= !empty($immo) ? format_number_($immo->immo_installation) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">15</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">20%</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="amortInstallation">
                                        <?= !empty($immo) ? format_number_($amortInstallation) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="resteAmortInstallation"><?= !empty($immo) ? format_number_($resteAmortInstallation) : "" ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class=" text-center p-2" style="border-right: 1px solid #ffff;">Agencements </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="agencement">
                                        <?= !empty($immo) ? format_number_($immo->immo_agencement) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">10</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">20%</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="amortAgencement">
                                        <?= !empty($immo) ? format_number_($amortAgencement) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="resteAmortAgencement"><?= !empty($immo) ? format_number_($resteAmortAgencement) : "" ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">Mobilier</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="mobilier">
                                        <?= !empty($immo->immo_mobilier) ? format_number_($immo->immo_mobilier) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">7</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">N/A</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="amortMobilier">
                                        <?= !empty($immo) ? format_number_($amortMobilier) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0" id="resteAmortMobilier"><?= !empty($immo) ? format_number_($resteAmortMobilier) : "" ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center p-2 fw-bold" style="border-right: 1px solid #ffff;">TOTAL</td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0 fw-bold" id="totalValeur">
                                        <?= !empty($immo) ? format_number_($totalValeur) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;"></td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;"></td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0 fw-bold" id="totalAmort">
                                        <?= !empty($immo) ? format_number_($totalAmort) : "" ?>
                                    </p>
                                </td>
                                <td class="text-center p-2" style="border-right: 1px solid #ffff;">
                                    <p class="p-0 m-0 fw-bold" id="totalResteAmort"><?= !empty($immo) ? format_number_($totalResteAmort) : "" ?></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="splitter m-2" style="height: auto;">

    </div>
    <div class="col-5 panel-right">
        <div id="apercu_doc_Acte">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>
            </div>
            <div id="apercuActe">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal doc -->

<div class="modal fade" id="modalAjoutActe" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadActe">

            </div>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion();
    deactivate_input_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    flatpickr(".calendar", {
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        //defaultDate: moment().format('DD-MM-YYYY'),
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });
</script>