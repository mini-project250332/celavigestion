<div class="d-flex justify-content-center align-items-center flex-column contain_bail_invalide_info">
    <div class="text-center w-50" style="white-space: normal;">
        Cliquez sur le bouton <b>"Créer les factures de loyer et activer l'automatisation"</b> pour générer les factures de loyer et activer l'automatisation de la génération des factures.
    </div>

    <div class="button_valid_bail mt-2">
        <button class="btn btn-primary" id="creerFacture" data-bail_id="<?= $bail[$nombreBail - 1]->bail_id ?>">
            Créer les factures de loyer et activer l'automatisation
        </button>
    </div>
</div>

<script>
    // get if mail envoyé + detail
    $(document).ready(() => {
        getHistorique(6, 1, $("#bail_id_sent").val(), "", "true", "verifyEmailBailValidationSent");
    })
</script>

<style>
    .contain_bail_invalide_info {
        margin: 10px;
        background: whitesmoke;
        padding: 20px;
        line-height: 30px;
    }

    .contain_bail_invalide_info div:first-child {
        font-weight: 500;
    }
</style>