<div class="contain_table_history_mail">
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2 contain_thead">Date</th>
                <th class="p-2 contain_thead">Destinataire</th>
                <th class="p-2 contain_thead">Objet</th>
                <th class="p-2 contain_thead">Facture</th>
                <th class="p-2 contain_thead">Pièces jointes</th>
                <th class="p-2 contain_thead">Action</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <?php foreach ($historiques as $key => $history): ?>
                <tr class="text-center">
                    <td class="text-center">
                        <?= date("d/m/Y H:m", strtotime(str_replace('/', '-', $history->histo_date))); ?>
                    </td>
                    <td class="text-center">
                        <?= $history->hist_mail_destinataire ?>
                    </td>
                    <td class="text-center">
                        <?= $history->hist_mail_objet ?>
                    </td>
                    <td class="text-center">
                        <?= $history->fact_loyer_num ?>
                    </td>
                    <td class="text-center">
                        <?= count($history->hist_mail_fichiers_joints) ?>
                        <?php foreach ($history->hist_mail_fichiers_joints as $pj) { ?>
                            <input type="hidden" value="<?= $pj[0]->doc_loyer_path ?>" class="input_pj_<?= $key ?>" />
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-sm btn-outline-primary btn_download_pj" data-key="<?= $key ?>">
                            <i class="fas fa-download"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php if (count($historiques) == 0) { ?>
        <div class="text-center w-100">
            Aucun historique
        </div>
    <?php } ?>
</div>

<script>
    $(".btn_download_pj").bind("click", function (event) {
        event.preventDefault();
        const key = $(this).attr("data-key");
        $(`.input_pj_${key}`).each((index, elem) => {
            download_pj($(elem).attr('value'));
        });
    });

    function download_pj(url) {
        const download_url = `${const_app.base_url}${url}`;
        window.location.href = download_url;
    }
</script>