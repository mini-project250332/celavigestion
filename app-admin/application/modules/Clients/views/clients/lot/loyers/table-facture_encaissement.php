<?php
$cliLot_mode_paiement = "";
$cliLot_encaiss_loyer = "";
$cliLot_iban_client = "";
if (isset($lot_facture)) {
    $cliLot_mode_paiement = $lot_facture->cliLot_mode_paiement;
    $cliLot_encaiss_loyer = " sur " . $lot_facture->cliLot_encaiss_loyer;
    $cliLot_iban_client = $lot_facture->cliLot_iban_client;
?>
    <input type="hidden" value="<?= $lot_facture->cliLot_id ?>" id="cli_lot_id" />
<?php } ?>

<input type="hidden" value="<?php if ($bail_loyer[0]->bail_facturation_loyer_celaviegestion == 1)
                                echo "1";
                            else
                                echo "0"; ?>" id="paidByCELAVI" />

<div class="row m-0">
    <div class="col px-0">
        <?php foreach ($bail_loyer as $bail_l) :
            switch ($bail_l->pdl_id):
                    // Mensuel
                case 1: ?>
                    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    $total_facture_ttc = 0;
                    ?>
                    <div class="row m-0">
                        <div class="col-auto px-0">
                            <table class="table table-hover fs--1">
                                <thead>
                                    <tr class="text-white">
                                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                            <br />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div class="text-uppercase">loyers facturés</div>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div>Appartement</div>
                                            <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                                            <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                                            <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div>Parking</div>
                                            <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TVA facturée</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div> Forfait charges</div>
                                            <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TVA facturée</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div>Franchise de loyer</div>
                                            <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TVA facturé</span><br>
                                            <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL FACTURES TTC</th>
                                    </tr>

                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            TOTAL ENCAISSEMENT
                                        </th>
                                    </tr>

                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            Solde
                                            <div></div>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col table_facture px-0" style="margin-left : 0px !important;margin-right:0px !important;">
                            <table class="table table-hover fs--1" style="width: 10px;overflow-x:auto">
                                <thead>
                                    <tr class="text-white">
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <th class="p-2 colored_table"> <?= $mois_table['nom'] ?> </th>
                                        <?php endforeach ?>

                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <th class="p-2 colored_table"> Variable </th>
                                        <?php endif ?>

                                        <th class="p-2 colored_table"> Total </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-200">
                                    <!-- Appartement -->
                                    <tr>
                                        <?php
                                        $totalAppartementHT = 0;
                                        $totalAppartementTVA = 0;
                                        $totalAppartementTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2 space-top">
                                                <br>
                                                <?php
                                                $appartementHT = 0;
                                                $appartementTVA = 0;
                                                $appartementTTC = 0;
                                                $loop = 0;
                                                $franchiseTTC = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $appartementHT += $facture->fact_loyer_app_ht;
                                                        $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                                        $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                                        $totalAppartementHT += $facture->fact_loyer_app_ht;
                                                        $totalAppartementTVA += $appartementTVA;
                                                        $totalAppartementTTC += $appartementTTC;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] += $appartementTTC - $franchiseTTC;
                                                $total_facture_ttc += $appartementTTC - $franchiseTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="appartement">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTVA) ?></div>
                                                    <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $appartementHT = $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                                $appartementTVA = $indexation_mois['indexation'][0]->indlo_appartement_ttc - $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                                $appartementTTC = $indexation_mois['indexation'][0]->indlo_appartement_ttc;

                                                                $total_facture_ttc_periode[$key_table] += $appartementTTC;
                                                                $total_facture_ttc += $appartementTTC;
                                                            }
                                                        }
                                                    } ?>
                                                    <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                                <?php } ?>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans appartement -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2 space-top">
                                                <?php
                                                $appartementHT = 0;
                                                $appartementTVA = 0;
                                                $appartementTTC = 0;
                                                $appartementTTCBail = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $appartementHT += $facture->fact_loyer_app_ht;
                                                        $appartementTVA += $facture->fact_loyer_app_ttc - $facture->fact_loyer_app_ht;
                                                        $appartementTTC += $facture->fact_loyer_app_ttc;

                                                        $totalAppartementHT += $facture->fact_loyer_app_ht;
                                                        $totalAppartementTVA += $appartementTVA;
                                                        $totalAppartementTTC += $appartementTTC;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }

                                                $total_facture_ttc_variable += $appartementTTC;
                                                $total_facture_ttc += $appartementTTC;
                                                ?>
                                                <br>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="appartement">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans appartement -->
                                        <td class="p-2 space-top fw-bold">
                                            <br>
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="appartement">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalAppartementHT += $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                        $totalAppartementTVA += $indexation_mois['indexation'][0]->indlo_appartement_ttc - $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                        $totalAppartementTTC += $indexation_mois['indexation'][0]->indlo_appartement_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Parking -->
                                    <tr>
                                        <?php
                                        $totalParkingHT = 0;
                                        $totalParkingTVA = 0;
                                        $totalParkingTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $parkingHT = 0;
                                                $parkingTVA = 0;
                                                $parkingTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $parkingHT += $facture->fact_loyer_park_ht;
                                                        $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                                        $parkingTTC += $facture->fact_loyer_park_ttc;

                                                        $totalParkingHT += $facture->fact_loyer_park_ht;
                                                        $totalParkingTVA += $parkingTVA;
                                                        $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] += $parkingTTC;
                                                $total_facture_ttc += $parkingTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="parking">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $parkingHT = $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                                $parkingTVA = $indexation_mois['indexation'][0]->indlo_parking_ttc - $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                                $parkingTTC = $indexation_mois['indexation'][0]->indlo_parking_ttc;

                                                                $total_facture_ttc_periode[$key_table] += $parkingTTC;
                                                                $total_facture_ttc += $parkingTTC;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                    <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTVA) ?></div>
                                                    <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                                <?php } ?>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans parking -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $parkingHT = 0;
                                                $parkingTVA = 0;
                                                $parkingTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $parkingHT += $facture->fact_loyer_park_ht;
                                                        $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                                        $parkingTTC += $facture->fact_loyer_park_ttc;

                                                        $totalParkingHT += $facture->fact_loyer_park_ht;
                                                        $totalParkingTVA += $parkingTVA;
                                                        $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_variable += $parkingTTC;
                                                $total_facture_ttc += $parkingTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="parking">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans parking -->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="parking">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalParkingHT += $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                        $totalParkingTVA += $indexation_mois['indexation'][0]->indlo_parking_ttc - $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                        $totalParkingTTC += $indexation_mois['indexation'][0]->indlo_parking_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Forfait charges -->
                                    <tr>
                                        <?php
                                        $totalChargeHT = 0;
                                        $totalChargeTVA = 0;
                                        $totalChargeTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $chargeHT = 0;
                                                $chargeTVA = 0;
                                                $chargeTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $chargeHT += $facture->fact_loyer_charge_ht;
                                                        $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                                        $chargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $totalChargeHT += $facture->fact_loyer_charge_ht;
                                                        $totalChargeTVA += $chargeTVA;
                                                        $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] -= $chargeTTC;
                                                $total_facture_ttc -= $chargeTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="charge">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $chargeHT = $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                                $chargeTVA = $indexation_mois['indexation'][0]->indlo_charge_ttc - $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                                $chargeTTC = $indexation_mois['indexation'][0]->indlo_charge_ttc;

                                                                $total_facture_ttc_periode[$key_table] -= $chargeTTC;
                                                                $total_facture_ttc -= $chargeTTC;
                                                            }
                                                        }
                                                    } ?>
                                                    <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                                <?php } ?>
                                            <?php endforeach ?>

                                            <!-- Variable dans Forfait charges -->
                                            <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $chargeHT = 0;
                                                $chargeTVA = 0;
                                                $chargeTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $chargeHT += $facture->fact_loyer_charge_ht;
                                                        $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                                        $chargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $totalChargeHT += $facture->fact_loyer_charge_ht;
                                                        $totalChargeTVA += $chargeTVA;
                                                        $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_variable -= $chargeTTC;
                                                $total_facture_ttc -= $chargeTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="charge">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans Forfait charges -->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="charge">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalChargeHT += $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                        $totalChargeTVA += $indexation_mois['indexation'][0]->indlo_charge_ttc - $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                        $totalChargeTTC += $indexation_mois['indexation'][0]->indlo_charge_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Franchise de loyer -->
                                    <tr>
                                        <?php
                                        $totalFranchiseHT = 0;
                                        $totalFranchiseTVA = 0;
                                        $totalFranchiseTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $franchiseHT = 0;
                                                $franchiseTVA = 0;
                                                $franchiseTTC = 0;

                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $franchiseTVA = $franchiseTTC - $franchiseHT;
                                                        $totalFranchiseHT += $franchiseHT;
                                                        $totalFranchiseTVA += $franchiseTVA;
                                                        $totalFranchiseTTC += $franchiseTTC;
                                                    }
                                                }

                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="franchise"></span>
                                                </div><br>
                                                <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($franchiseTVA) ?></div>
                                                <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans franchise -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $franchiseHT = 0;
                                                $franchiseTVA = 0;
                                                $franchiseTTC = 0;

                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $franchiseTVA = $franchiseTTC - $franchiseHT;
                                                        $totalFranchiseHT += $franchiseHT;
                                                        $totalFranchiseTVA += $franchiseTVA;
                                                        $totalFranchiseTTC += $franchiseTTC;
                                                    }
                                                }
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="franchise"></span>
                                                </div><br>
                                                <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                                                <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans franchsie-->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="franchise"></span>
                                            </div><br>
                                            <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                                            <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                                            <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                                        </td>
                                    </tr>

                                    <!-- Total Factures TTC -->
                                    <?php foreach ($list_mois as $key_fact_ttc => $mois_table) : ?>
                                        <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                            <?= format_number_($total_facture_ttc_periode[$key_fact_ttc]) ?>
                                        </td>
                                    <?php endforeach ?>
                                    <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                                        <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                            <?= format_number_($total_facture_ttc_variable) ?>
                                        </td>
                                    <?php endif ?>
                                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                        <?= format_number_($total_facture_ttc) ?>
                                    </td>


                                    <!-- Encaissement mensieul -->
                                    <tr class="text-end">
                                        <?php
                                        $total_mensuel = 0;
                                        $encaissement_tab_variable = 0;
                                        ?>
                                        <?php foreach ($indexation_mensuel as $key_mensuel => $value) : ?>
                                            <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                                                <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                                                    <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                                        <?php if ($value_s['nom'] == $value['nom']) : ?>
                                                            <div>
                                                                <?php
                                                                $total_mensuel += $value_s['somme'];
                                                                echo format_number_($value_s['somme']);
                                                                ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </td>
                                        <?php endforeach; ?>

                                        <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                                            <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                                                <?php
                                                $totalEncaissement = 0;
                                                $loop = 0;
                                                foreach ($encaissements as $encaissement) :
                                                    if ($encaissement->enc_periode === "variable") {
                                                        $totalEncaissement += $encaissement->enc_montant;
                                                        $loop += 1;
                                                        $total_mensuel += $encaissement->enc_montant;
                                                    } ?>
                                                <?php endforeach; ?>
                                                <?php $encaissement_tab_variable = $totalEncaissement; ?>
                                                <?php echo format_number_($totalEncaissement); ?>
                        </div>
                        </td>
                    <?php endif ?>

                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <b>
                            <?php echo format_number_($total_mensuel); ?>
                        </b>
                    </td>
                    </tr>

                    <!-- Solde Mensuel -->
                    <tr class="text-end">
                        <?php
                        $solde_total = 0;
                        $solde = 0;
                        foreach ($indexation_mensuel as $key_mensuel => $mensuel) : ?>
                            <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                                <?php
                                if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                                    <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                        <?php if ($value_s['nom'] == $mensuel['nom']) : ?>
                                            <?php
                                            $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                                $value_s['somme'] - $total_facture_ttc_periode[$key_mensuel]
                                                : $value_s['somme'] - $indexation_mensuel_total_ttc_theorique_par_colonne[$key_mensuel];
                                            $solde_total += $solde;
                                            ?>
                                            <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                                <?= format_number_($solde) ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </td>
                        <?php endforeach ?>

                        <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                            <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                                <?php
                                $solde_variable = 0;
                                $solde_variable -= $encaissement_tab_variable;
                                $solde_total += $solde_variable;
                                ?>
                                <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                                    <?= format_number_($solde_variable) ?>
                                </div>
                            </td>
                        <?php endif ?>

                        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                            <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                                <?= format_number_($solde_total) ?>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                    </table>
                    </div>
    </div>
    <?php break; ?>

    <!-- trimestriel -->
<?php
                case 2: ?>
    <?php
                    $listTimestre = array(
                        array("nom" => "T1", "value" => "Mars"),
                        array("nom" => "T2", "value" => "Juin"),
                        array("nom" => "T3", "value" => "Sept"),
                        array("nom" => "T4", "value" => "Déc"),
                    );
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0);
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T1</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T2</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T3</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T4</th>

                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase">loyers facturés</div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Appartement</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>

                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $appartementHT = $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_trimes['indexation'][0]->indlo_appartement_ttc - $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_trimes['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;

                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalAppartementHT += $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_trimes['indexation'][0]->indlo_appartement_ttc - $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_trimes['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Parking</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $parkingHT = $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_trimes['indexation'][0]->indlo_parking_ttc - $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_trimes['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalParkingHT += $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_trimes['indexation'][0]->indlo_parking_ttc - $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_trimes['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Forfait charges</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $chargeHT = $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_trimes['indexation'][0]->indlo_charge_ttc - $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_trimes['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalChargeHT += $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_trimes['indexation'][0]->indlo_charge_ttc - $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_trimes['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    Franchise de loyer<br>
                    <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TVA facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                </th>
                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;

                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="franchise">
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?></div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- Total Factures TTC -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL FACTURES TTC</th>
                <?php foreach ($listTimestre as $key_fact_trimestre => $trimestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_trimestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement Trimestriel civile-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL ENCAISSEMENT</th>
                <?php
                    $totalTimestre = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($listTimestre as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <div>
                                        <?php
                                        $totalTimestre += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalTimestre += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?>
</div>
</td>
<?php endif ?>

<td class="p-2" style="border-bottom: 1px solid #bfadad;">
    <b>
        <?php echo format_number_($totalTimestre); ?>
    </b>
</td>
</tr>

<!-- Solde Timestre-->
<tr class="text-end">
    <th class="text-white text-start p-2" style="background:#2569C3;">Solde</th>
    <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listTimestre as $key_trimestre_encaiss => $trimestre) : ?>
        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
            <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                    <?php if ($value_s['nom'] == $trimestre['value']) : ?>
                        <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_trimestre_encaiss]
                                        : $value_s['somme'] - $indexation_trimestriel_total_ttc_theorique_par_colonne[$key_trimestre_encaiss];
                                    $solde_total += $solde;
                        ?>
                        <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde) ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

        </td>
    <?php endforeach ?>

    <?php if ($bail_l->bail_loyer_variable == 1) : ?>
        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
            <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
            ?>
            <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                <?= format_number_($solde_variable) ?>
            </div>
        </td>
    <?php endif ?>

    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
        <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
            <?= format_number_($solde_total) ?>
        </div>
    </td>
</tr>

</tbody>
</table>
<?php break; ?>

<?php
                case 3: ?>
    <?php
                    $listSemestriel = array(
                        array("nom" => "S1", "value" => "Juin"),
                        array("nom" => "S2", "value" => "Déc")
                    );
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0);
                    $total_facture_ttc = 0;
    ?>
    <!-- semestriel -->
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">S1</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">S2</th>

                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase">loyers facturés</div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Appartement</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $appartementHT = $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_semestr['indexation'][0]->indlo_appartement_ttc - $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_semestr['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_semestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalAppartementHT += $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_semestr['indexation'][0]->indlo_appartement_ttc - $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_semestr['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <!-- Parking -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Parking</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>
                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $parkingHT = $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_semestr['indexation'][0]->indlo_parking_ttc - $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_semestr['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_semestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalParkingHT += $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_semestr['indexation'][0]->indlo_parking_ttc - $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_semestr['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <!-- Forfait charges -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Forfait charges</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $chargeHT = $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_semestr['indexation'][0]->indlo_charge_ttc - $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_semestr['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_semestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans forfait charges -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        if ($bail_l->bail_facturation_loyer_celaviegestion == 1) {
                            foreach ($factures as $facture) {
                                if ($facture->periode_semestre_id == "3") {
                                    $chargeHT += $facture->fact_loyer_charge_ht;
                                    $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                    $chargeTTC += $facture->fact_loyer_charge_ttc;
                                    $totalChargeHT += $facture->fact_loyer_charge_ht;
                                    $totalChargeTVA += $chargeTVA;
                                    $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                    $loop += 1;
                                    $totalLoop += 1;
                                }
                            }
                            $total_facture_ttc_variable -= $chargeTTC;
                            $total_facture_ttc -= $chargeTTC;
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>

                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="charge">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalChargeHT += $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_semestr['indexation'][0]->indlo_charge_ttc - $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_semestr['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    Franchise de loyer<br>
                    <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TVA facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- TOTAL FACTURES TTC -->

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL FACTURES TTC</th>
                <?php foreach ($listSemestriel as $key_fact_semestre => $semestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_semestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>

            <!-- total Encaissement Semestriel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL ENCAISSEMENT</th>
                <?php
                    $totalSemestriel = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($listSemestriel as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <div>
                                        <?php
                                        $totalSemestriel += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalSemestriel += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?></div>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($totalSemestriel); ?>
                    </b>
                </td>
            </tr>

            <!-- Solde Semestriel-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">Solde</th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listSemestriel as $key_semestre_encaiss => $semestre) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $semestre['value']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_semestre_encaiss]
                                        : $value_s['somme'] - $indexation_semestriel_total_ttc_theorique_par_colonne[$key_semestre_encaiss];
                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <?php break; ?>

    <!-- annuel -->
<?php
                case 4: ?>
    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = 0;
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"><?= $annee ?></th>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?><th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th> <?php endif ?>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase">loyers facturés</div>
                </th>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Appartement</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php
                    $appartementHT = 0;
                    $appartementTVA = 0;
                    $appartementTTC = 0;
                    $loop = 0;
                    $annee_id = 0;
                    $franchiseTTC = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;

                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode += $appartementTTC - $franchiseTTC;
                    $total_facture_ttc += $appartementTTC - $franchiseTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="appartement">
                            (<?= $loop ?>)</span>
                    </div>
                    <?php if ($loop != 0) { ?>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $appartementHT = $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                    $appartementTVA = $indexation_ann['indexation'][0]->indlo_appartement_ttc - $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                    $appartementTTC = $indexation_ann['indexation'][0]->indlo_appartement_ttc;

                                    $total_facture_ttc_periode += $appartementTTC;
                                    $total_facture_ttc += $appartementTTC;
                                }
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                                $total_facture_ttc_variable += $appartementTTC;
                                $total_facture_ttc += $appartementTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalAppartementHT += $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_ann['indexation'][0]->indlo_appartement_ttc - $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_ann['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Parking</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;

                    $parkingHT = 0;
                    $parkingTVA = 0;
                    $parkingTTC = 0;
                    $loop = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingTTC += $facture->fact_loyer_park_ttc;

                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode += $parkingTTC;
                    $total_facture_ttc += $parkingTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="parking">
                            (<?= $loop ?>)</span>
                    </div>

                    <?php if ($loop != 0) { ?>

                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $parkingHT = $indexation_ann['indexation'][0]->indlo_parking_ht;
                                    $parkingTVA = $indexation_ann['indexation'][0]->indlo_parking_ttc - $indexation_ann['indexation'][0]->indlo_parking_ht;
                                    $parkingTTC = $indexation_ann['indexation'][0]->indlo_parking_ttc;

                                    $total_facture_ttc_periode += $parkingTTC;
                                    $total_facture_ttc += $parkingTTC;
                                }
                            }
                        } ?>

                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingTTC = 0;
                        $loop = 0;
                        $parkingTTCBail = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingTTC += $facture->fact_loyer_park_ttc;

                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingTTC;
                        $total_facture_ttc += $parkingTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?></div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>

                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalParkingHT += $indexation_ann['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_ann['indexation'][0]->indlo_parking_ttc - $indexation_ann['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_ann['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Forfait charges</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php
                    $chargeHT = 0;
                    $chargeTVA = 0;
                    $chargeTTC = 0;
                    $loop = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;

                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode -= $chargeTTC;
                    $total_facture_ttc -= $chargeTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="charge">
                            (<?= $loop ?>)</span>
                    </div>

                    <?php if ($loop != 0) { ?>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $chargeHT = $indexation_ann['indexation'][0]->indlo_charge_ht;
                                    $chargeTVA = $indexation_ann['indexation'][0]->indlo_charge_ttc - $indexation_ann['indexation'][0]->indlo_charge_ht;
                                    $chargeTTC = $indexation_ann['indexation'][0]->indlo_charge_ttc;

                                    $total_facture_ttc_periode -= $chargeTTC;
                                    $total_facture_ttc -= $chargeTTC;
                                }
                            }
                        } ?>

                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        $chargeTTCBail = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;

                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $total_facture_ttc_variable -= $chargeTTC;
                                $total_facture_ttc -= $chargeTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charge -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="charge">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>

                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalChargeHT += $indexation_ann['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_ann['indexation'][0]->indlo_charge_ttc - $indexation_ann['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_ann['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>

                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    Franchise de loyer<br>
                    <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TVA facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;

                    $franchiseHT = 0;
                    $franchiseTVA = 0;
                    $franchiseTTC = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>

                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                    <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                </td>
                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL FACTURES TTC</th>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc_periode) ?>
                </td>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement annuel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL ENCAISSEMENT</th>
                <?php
                    $total_annuel = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($indexation_annuel as $key_annuel => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['nom']) : ?>
                                    <div>
                                        <?php
                                        $total_annuel += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $total_annuel += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?></div>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($total_annuel); ?>
                    </b>
                </td>
            </tr>


            <!-- Solde Mensuel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">Solde</th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($indexation_annuel as $key_annuel => $annuel) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $annuel['nom']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode
                                        : $value_s['somme'] - $indexation_annuel_total_ttc_theorique_par_colonne[$key_annuel];

                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>

        </tbody>
    </table>
    <?php break; ?>
    <!-- trimestriel décalée -->
<?php
                case 5: ?>
    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0);
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <?php foreach ($listTimestre as $list) : ?>
                    <?php
                        $debut = date('m/Y', strtotime($list['debut']));
                        $fin = date('m/Y', strtotime($list['fin']));
                    ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                        <?= $list['nom'] ?> <br>
                        <span style="font-size: 10px"><i>(<?= $debut . ' - ' . $fin ?>)</i></span>
                    </th>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase">loyers facturés</div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Appartement</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>

                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?></div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $appartementHT = $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_trim['indexation'][0]->indlo_appartement_ttc - $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_trim['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalAppartementHT += $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_trim['indexation'][0]->indlo_appartement_ttc - $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_trim['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Parking</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $parkingHT = $indexation_trim['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_trim['indexation'][0]->indlo_parking_ttc - $indexation_trim['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_trim['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalParkingHT += $indexation_trim['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_trim['indexation'][0]->indlo_parking_ttc - $indexation_trim['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_trim['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div>Forfait charges</div>
                    <div class="fw-normal">&emsp; &ensp;HT facturé</div>
                    <div class="fw-normal">&emsp; &ensp;TVA facturée</div>
                    <div class="fw-normal">&emsp; &ensp;TTC facturé</div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $chargeHT = $indexation_trim['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_trim['indexation'][0]->indlo_charge_ttc - $indexation_trim['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_trim['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalChargeHT += $indexation_trim['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_trim['indexation'][0]->indlo_charge_ttc - $indexation_trim['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_trim['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    Franchise de loyer<br>
                    <span class="fw-normal">&emsp; &ensp;HT facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TVA facturé</span><br>
                    <span class="fw-normal">&emsp; &ensp;TTC facturé</span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans Franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans Franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- Total Factures TTC -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL FACTURES TTC</th>
                <?php foreach ($listTimestre as $key_fact_trimestre => $trimestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_trimestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement Trimestriel-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">TOTAL ENCAISSEMENT</th>
                <?php
                    $totalTimestre = 0;
                    $encaissement_tab_variable = 0;
                    $loop = 0;
                ?>
                <?php foreach ($listTimestre as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <!-- <div class="count_encaissement" data-enc_periode="<?= $listTimestre[$key]['nom'] ?>">
                                                        (<?php
                                                            $loop += 1;
                                                            echo $loop;
                                                            ?>)
                                                    </div> -->
                                    <div>
                                        <?php
                                        $totalTimestre += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalTimestre += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php echo format_number_($totalEncaissement); ?></div>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($totalTimestre); ?>
                    </b>
                </td>
            </tr>

            <!-- Solde Timestre-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">Solde</th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listTimestre as $key_semestre_encaiss => $semestre) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $semestre['value']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_semestre_encaiss]
                                        : $value_s['somme'] - $indexation_trimestriel_total_ttc_theorique_par_colonne[$key_semestre_encaiss];
                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <?php break; ?>
<?php endswitch ?>
<?php endforeach ?>
</div>
</div>

<button class="btn btn-sm btn-primary d-none" data-bs-toggle="modal" data-bs-target="#modal_detail_encaissement" id="btn_detail_encaissement" data-backdrop="static" data-keyboard="false">
    <i class="fas fa-plus me-1"></i>
    Détail Encaissement
</button>

<button class="btn btn-sm btn-primary d-none" data-bs-toggle="modal" data-bs-target="#modal_detail_facture" id="btn_detail_facture" data-backdrop="static" data-keyboard="false">
    <i class="fas fa-plus me-1"></i>
    Détail Facture
</button>


<!-- Modal encaissement -->
<div class="modal fade" id="modal_encaissement" tabindex="-1" aria-labelledby="modal_encaissement" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title only_visuel_gestion" id="exampleModalLabel">Ajout d'un encaissement</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" value="<?= $bail_loyer[0]->bail_id ?>" id="bail_id" />
                <input type="hidden" value="<?= $annee_courant ?>" id="annee_courant" />
                <input type="hidden" value="<?= $bail_loyer[0]->pdl_id ?>" id="pdl_id" />
                <input type="hidden" value="<?= $bail_loyer[0]->premier_mois_trimes ?>" id="premier_mois_trimes" />
                <input type="hidden" value="<?= $bail_loyer[0]->momfac_id ?>" id="momfac_id" />
                <input type="hidden" value="<?= $bail_loyer[0]->bail_loyer_variable ?>" id="bail_loyer_variable" />
                <input type="hidden" value="" id="enc_id" />

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Période de l'encaissement :</label>
                                </div>
                                <div class="col">
                                    <?php
                                    $periodicity = $bail_loyer[0]->pdl_description;
                                    $numberPeriodicity = $bail_loyer[0]->pdl_valeur;
                                    $loyerVariable = $bail_loyer[0]->bail_loyer_variable;
                                    $tabPeriodicity = array();

                                    if ($numberPeriodicity === "12") {
                                        foreach ($list_full_mois as $key => $mois) {
                                            $period = array(
                                                "value" => $mois["nom"],
                                                "description" => $mois["nom"]
                                            );
                                            array_push($tabPeriodicity, $period);
                                        }
                                    }

                                    if ($numberPeriodicity === "2") {
                                        $list_semestre = array("S1", "S2");
                                        foreach ($list_semestre as $semestre) {
                                            $period = array(
                                                "value" => $semestre,
                                                "description" => $semestre
                                            );
                                            array_push($tabPeriodicity, $period);
                                        }
                                    }

                                    if ($numberPeriodicity === "4") {
                                        $list_trimestre = array("T1", "T2", "T3", "T4");
                                        foreach ($list_trimestre as $trimestre) {
                                            $period = array(
                                                "value" => $trimestre,
                                                "description" => $trimestre
                                            );
                                            array_push($tabPeriodicity, $period);
                                        }
                                    }

                                    if ($numberPeriodicity === "1") {
                                        $period = array(
                                            "value" => $annee_courant,
                                            "description" => null
                                        );
                                        array_push($tabPeriodicity, $period);
                                    }

                                    ?>
                                    <select class="form-control form-select" value="" id="enc_periode">
                                        <?php
                                        foreach ($tabPeriodicity as $key => $tab) : ?>
                                            <option value="<?php echo $tab["value"]; ?>">
                                                <?php echo $tab["description"]; ?> <?php if (isset($tab["description"]))
                                                                                        echo ""; ?> <?= $annee_courant ?>
                                            </option>
                                        <?php
                                        endforeach ?>

                                        <?php if ($loyerVariable === "1" || $bail_l->bail_loyer_variable === "2") { ?>
                                            <option value="variable">
                                                variable <?= $annee_courant ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Date de l'encaissement :</label>
                                </div>
                                <div class="col">
                                    <?php
                                    $defaultDate = date("Y-m-d");
                                    ?>
                                    <input type="date" id="enc_date" class="form-control" value="<?= $defaultDate; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row d-none">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Mode de l'encaissement :</label>
                                </div>
                                <div class="col">
                                    <select class="form-select" aria-label="Default select example" id="enc_mode">
                                        <option value="virement">VIREMENT</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Montant encaissé :</label>
                                </div>
                                <div class="col">
                                    <div class="input-group">
                                        <input type="number" class="form-control input text-end" placeholder="0" id="enc_montant">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-index" id="basic-addon2">€</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="title-tva-collected">
                    <label>
                        Répartition pour la TVA collectée :
                    </label>
                </div>

                <div class="error-ttc-montant">
                    <label>
                        Le montant total TTC doit être identique au montant encaissé
                    </label>
                </div>

                <div class="row">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2" style="border-right: 1px solid #ffff;width:25%;"></th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">TTC</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">TVA</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">HT</th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">Appartement</th>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input type="number" placeholder="0" disabled class="form-control mb-0 input_loyer" id="encaissement_loyer_app_ttc" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="main-input-group">
                                            <div class="input-group input-group-tva">
                                                <select id="encaissement_loyer_app_tva" class="select_tva input_loyer form-control mb-2 select text-right">
                                                    <?php foreach ($tva as $key => $item) : ?>
                                                        <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "10")
                                                                                                                echo "selected"; ?>>
                                                            <?php echo $item->tva_valeur; ?>
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-montant-tva">
                                                <input disabled placeholder="0" type="number" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_app_tva_pource" style="text-align: right;"><br>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input disabled type="number" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_app_ht" style="text-align: right;" value="0">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">Parking</th>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input type="number" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_park_ttc" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="main-input-group">
                                            <div class="input-group input-group-tva">
                                                <select id="encaissement_loyer_park_tva" class="input_loyer form-control mb-2 select text-right">
                                                    <?php foreach ($tva as $key => $item) : ?>
                                                        <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                echo "selected"; ?>>
                                                            <?php echo $item->tva_valeur; ?>
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-montant-tva">
                                                <input disabled placeholder="0" type="number" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_park_tva_pource" style="text-align: right;"><br>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input disabled type="number" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_park_ht" value="0" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">Forfait charges</th>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" placeholder="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_ttc" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="main-input-group">
                                            <div class="input-group input-group-tva">
                                                <select id="encaissement_loyer_charge_tva" class="input_loyer form-control mb-2 select text-right">
                                                    <?php foreach ($tva as $key => $item) : ?>
                                                        <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                echo "selected"; ?>>
                                                            <?php echo $item->tva_valeur; ?>
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-montant-tva">
                                                <input disabled placeholder="0" type="number" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_tva_pource" style="text-align: right;"><br>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input type="number" placeholder="0" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_ht" style="text-align: right;" disabled>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">
                                        <div>Charges déductibles</div>
                                        <select class="form-select form-control select text-left" id="typeChargeDeductible">
                                            <?php foreach ($typeChargeDeductibleLotEncaissement as $chrgDeductLot) : ?>
                                                <option value="<?php echo $chrgDeductLot->tpchrg_deduct_id; ?>">
                                                    <?php echo $chrgDeductLot->tpchrg_deduct_nom; ?>
                                                </option>
                                            <?php endforeach ?>
                                        </select>
                                    </th>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="input-group">
                                                <div class="input-group-prepend prepend-charge">
                                                    <span class="input-group-text">-</span>
                                                </div>
                                                <input type="number" placeholder="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_deductible_ttc" style="text-align: right;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="main-input-group">
                                                <div class="input-group input-group-tva">
                                                    <select id="encaissement_loyer_charge_deductible_tva" class="input_loyer form-control mb-2 select text-right">
                                                        <?php foreach ($tva as $key => $item) : ?>
                                                            <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                    echo "selected"; ?>>
                                                                <?php echo $item->tva_valeur; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-montant-tva">
                                                    <input disabled placeholder="0" type="number" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_deductible_tva_pource" style="text-align: right;"><br>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="input-group">
                                                <input type="number" placeholder="0" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_charge_deductible_ht" style="text-align: right;" disabled>
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">
                                        <div>Produits</div>
                                        <select class="form-select form-control select text-left" id="typeProduit">
                                            <?php foreach ($typeProduitLotEncaissement as $produitLot) : ?>
                                                <option value="<?php echo $produitLot->tpchrg_prod_id; ?>">
                                                    <?php echo $produitLot->tpchrg_prod_nom; ?>
                                                </option>
                                            <?php endforeach ?>
                                        </select>
                                    </th>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="input-group">
                                                <input type="number" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_produit_ttc" style="text-align: right;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="main-input-group">
                                                <div class="input-group input-group-tva">
                                                    <select id="encaissement_loyer_produit_tva" class="input_loyer form-control mb-2 select text-right">
                                                        <?php foreach ($tva as $key => $item) : ?>
                                                            <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                    echo "selected"; ?>>
                                                                <?php echo $item->tva_valeur; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-montant-tva">
                                                    <input disabled placeholder="0" type="number" value="0" class="input_loyer form-control mb-0" id="encaissement_loyer_produit_tva_pource" style="text-align: right;"><br>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement flex">
                                        <div>
                                            <div class="input-group">
                                                <input disabled type="number" value="0" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_produit_ht" style="text-align: right;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TOTAL</th>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input type="number" disabled placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_total_ttc" value="0" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input type="number" disabled placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_total_tva" value="0" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="td-encaissement">
                                        <div class="input-group">
                                            <input disabled type="number" value="0" placeholder="0" class="form-control mb-0 input_loyer" id="encaissement_loyer_total_ht" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row mt-1">
                    <div class="col">
                        <label>Commentaire :</label>
                    </div>
                    <div class="col-12">
                        <textarea class="form-control" id="encaissement_commentaire" rows="4" style="height: 100%;"></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer only_visuel_gestion">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" id="btnAddEncaissement">Enregistrer</button>
            </div>
        </div>
    </div>
</div>



<!-- Charge -->
<div class="modal fade" id="modal_charge" tabindex="-1" aria-labelledby="modal_charge" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajout d'une charge</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Numéro de facture :</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" value="" id="fact_loyer_num">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Date de facture :</label>
                                </div>
                                <div class="col">
                                    <input type="date" class="form-control" value="<?= date("Y-m-d") ?>" id="fact_loyer_date">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col">

                        <?php foreach ($bail_loyer as $bail_l) :
                            switch ($bail_l->pdl_id):
                                case 1: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($mensuel as $mens) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $mens->periode_mens_id == 13) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $mens->periode_mens_id ?>"><?= $mens->periode_mens_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 2: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($trimestriel as $trimes) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $trimes->periode_timestre_id == 5) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $trimes->periode_timestre_id ?>"><?= $trimes->periode_trimesrte_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 3: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($semestriel as $sem) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $sem->periode_semestre_id == 3) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $sem->periode_semestre_id ?>"><?= $sem->periode_semestre_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 4: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php $date = date_create($bail_l->bail_debut);
                                                    $date = date_format($date, "Y"); ?>

                                                    <?php foreach ($annuel as $ans) : ?>
                                                        <?php if ($ans->periode_annee_libelle == $date) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } elseif ($bail_l->bail_loyer_variable == 1 && $ans->periode_annee_id == 0) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                            <?php endswitch; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2" style="border-right: 1px solid #ffff;width:25%;"></th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Charges déductibles</th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">HT facturé</th>
                                    <td><input type="number" class="form-control mb-0" id="" style="text-align: right;"></td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TVA facturée</th>
                                    <td>
                                        <input type="number" class="form-control mb-0" id="" style="text-align: right;"><br>
                                        <input type="number" class="form-control mb-0" id="" style="text-align: right;" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TTC facturé</th>
                                    <td><input type="number" class="form-control mb-0" id="" style="text-align: right;" disabled></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer only_visuel_gestion">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" id="AddFactureLoyer">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<!-- Debut modal produit -->
<div class="modal fade" id="modal_produit" tabindex="-1" aria-labelledby="modal_produit" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajout d'une Produit</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Numéro de facture :</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" value="" id="fact_loyer_num">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Date de facture :</label>
                                </div>
                                <div class="col">
                                    <input type="date" class="form-control" value="<?= date("Y-m-d") ?>" id="fact_loyer_date">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col">

                        <?php foreach ($bail_loyer as $bail_l) :
                            switch ($bail_l->pdl_id):
                                case 1: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($mensuel as $mens) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $mens->periode_mens_id == 13) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $mens->periode_mens_id ?>"><?= $mens->periode_mens_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 2: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($trimestriel as $trimes) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $trimes->periode_timestre_id == 5) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $trimes->periode_timestre_id ?>"><?= $trimes->periode_trimesrte_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 3: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php foreach ($semestriel as $sem) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $sem->periode_semestre_id == 3) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $sem->periode_semestre_id ?>"><?= $sem->periode_semestre_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 4: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select" aria-label="Default select example" id="">
                                                    <?php $date = date_create($bail_l->bail_debut);
                                                    $date = date_format($date, "Y"); ?>
                                                    <?php foreach ($annuel as $ans) : ?>
                                                        <?php if ($ans->periode_annee_libelle == $date) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } elseif ($bail_l->bail_loyer_variable == 1 && $ans->periode_annee_id == 0) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                            <?php endswitch; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2" style="border-right: 1px solid #ffff;width:25%;"></th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Autres produits</th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">HT facturé</th>
                                    <td><input type="number" class="form-control mb-0" id="fact_loyer_produit_ht" style="text-align: right;"></td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TVA facturée</th>
                                    <td>
                                        <input type="number" class="form-control mb-0" id="fact_loyer_produit_tva_pource" style="text-align: right;"><br>
                                        <input type="number" class="form-control mb-0" id="fact_loyer_produit_tva" style="text-align: right;" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TTC facturé</th>
                                    <td><input type="number" class="form-control mb-0" id="fact_loyer_produit_ttc" style="text-align: right;" disabled></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer only_visuel_gestion">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" id="AddFactureLoyer">Enregistrer</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin modal produit -->

<!-- Debut modal facture -->
<div class="modal fade" id="modal_facture" tabindex="-1" aria-labelledby="modal_facture" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title creation-facture only_visuel_gestion" id="title_modal_facture">Création d'une facture par CELAVI Gestion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <input type="hidden" value="" id="fact_id" />
            <input type="hidden" value="" id="fact_avoir" />

            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <?php foreach ($bail_loyer as $bail_l) :
                            switch ($bail_l->pdl_id):
                                case 1: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select auto_effect" aria-label="Default select example" id="periode_mens_id">
                                                    <?php foreach ($mensuel as $mens) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $mens->periode_mens_id == 13) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $mens->periode_mens_id ?>">
                                                                <?= $mens->periode_mens_libelle ?>&nbsp;<?= $annee_courant ?>
                                                            </option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 2: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select auto_effect" aria-label="Default select example" id="periode_timestre_id">
                                                    <?php foreach ($trimestriel as $trimes) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $trimes->periode_timestre_id == 5) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $trimes->periode_timestre_id ?>">
                                                                <?= $trimes->periode_trimesrte_libelle ?>&nbsp;<?= $annee_courant ?>
                                                            </option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 3: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select auto_effect" aria-label="Default select example" id="periode_semestre_id">
                                                    <?php foreach ($semestriel as $sem) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $sem->periode_semestre_id == 3) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $sem->periode_semestre_id ?>">
                                                                <?= $sem->periode_semestre_libelle ?>&nbsp;<?= $annee_courant ?>
                                                            </option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 4: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation&nbsp;:</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select auto_effect" aria-label="Default select example" id="periode_annee_id">
                                                    <?php $date = date_create($bail_l->bail_debut);
                                                    $date = date_format($date, "Y"); ?>
                                                    <?php foreach ($annuel as $ans) : ?>
                                                        <?php if ($ans->periode_annee_libelle == $date) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } elseif ($bail_l->bail_loyer_variable == 1 && $ans->periode_annee_id == 0) { ?>
                                                            <option value="<?= $ans->periode_annee_id ?>"><?= $ans->periode_annee_libelle ?></option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                                <?php
                                case 5: ?>
                                    <div class="form-group inline p-0">
                                        <div class="col">
                                            <div class="col-4">
                                                <label data-width="250">Période de facturation :</label>
                                            </div>
                                            <div class="col">
                                                <select class="form-select auto_effect" aria-label="Default select example" id="periode_timestre_id">
                                                    <?php foreach ($trimestriel as $key => $trimes) : ?>
                                                        <?php if ($bail_l->bail_loyer_variable == 0 && $trimes->periode_timestre_id == 5) {
                                                            break;
                                                        } else { ?>
                                                            <option value="<?= $trimes->periode_timestre_id ?>" <?= $trimes->periode_timestre_id == 1 ? 'selected' : '' ?>>
                                                                <?= $trimes->periode_trimesrte_libelle ?>&nbsp;<?= $annee_courant ?>
                                                            </option>
                                                        <?php } ?>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; ?>
                            <?php endswitch; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Date de facturation&nbsp;:</label>
                                </div>
                                <div class="col">
                                    <input type="date" class="form-control auto_effect" value="<?= date("Y-m-d") ?>" id="fact_loyer_date">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group inline p-0">
                            <div class="col">
                                <div class="col-4">
                                    <label data-width="250">Numéro de facture&nbsp;:</label>
                                </div>
                                <div class="col">
                                    <input type="hidden" id="fact_view_num" value="<?= str_pad($numero_facture, 5, '0', STR_PAD_LEFT); ?>">
                                    <input type="text" class="form-control auto_effect" value="" id="fact_loyer_num" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <table class="table table-hover fs--1">
                            <thead class="text-white">
                                <tr class="text-center">
                                    <th class="p-2" style="border-right: 1px solid #ffff;width:25%;"></th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Appartement</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Parking</th>
                                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Forfait charges</th>

                                </tr>
                            </thead>
                            <tbody class="bg-200">
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">HT facturé</th>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" placeholder="0" class="form-control mb-0 input_loyer auto_effect" id="fact_loyer_app_ht" value="" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" placeholder="0" class="form-control mb-0 input_loyer auto_effect" id="fact_loyer_park_ht" value="" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text inverse-avoir">-</span>
                                            </div>
                                            <input type="number" placeholder="0" class="form-control mb-0 input_loyer auto_effect" id="fact_loyer_charge_ht" value="" style="text-align: right;">
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TVA facturée</th>
                                    <td>
                                        <div class="input-group">
                                            <select id="fact_loyer_app_tva" class="input_loyer form-control mb-2 auto_effect select text-right">
                                                <?php foreach ($tva as $key => $item) : ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>">
                                                        <?php echo $item->tva_valeur; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" disabled placeholder="0" value="" class="input_loyer form-control auto_effect mb-0" id="fact_loyer_app_tva_pource" style="text-align: right;"><br>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>

                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <select id="fact_loyer_park_tva" class="input_loyer form-control auto_effect mb-2 select text-right">
                                                <?php foreach ($tva as $key => $item) : ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>">
                                                        <?php echo $item->tva_valeur; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input disabled placeholder="0" type="number" value="" class="input_loyer form-control mb-0 auto_effect" id="fact_loyer_park_tva_pource" style="text-align: right;"><br>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <select id="fact_loyer_charge_tva" class="input_loyer form-control mb-2 auto_effect select text-right">
                                                <?php foreach ($tva as $key => $item) : ?>
                                                    <option value="<?php echo $item->tva_valeur; ?>">
                                                        <?php echo $item->tva_valeur; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">%</span>
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text inverse-avoir">-</span>
                                            </div>
                                            <input disabled placeholder="0" type="number" value="" class="input_loyer form-control mb-0 auto_effect" id="fact_loyer_charge_tva_pource" style="text-align: right;"><br>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <th class="text-white p-2" style="background:#2569C3;">TTC facturé</th>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" placeholder="0" value="" class="input_loyer form-control mb-0 auto_effect" id="fact_loyer_app_ttc" style="text-align: right;" disabled>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-avoir d-none">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="number" placeholder="0" value="" class="input_loyer form-control mb-0 auto_effect" id="fact_loyer_park_ttc" style="text-align: right;" disabled>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend prepend-charge">
                                                <span class="input-group-text inverse-avoir">-</span>
                                            </div>
                                            <input type="number" placeholder="0" value="" class="input_loyer form-control mb-0 auto_effect" id="fact_loyer_charge_ttc" style="text-align: right;" disabled>
                                            <div class="input-group-append">
                                                <span class="input-group-text group-loyer" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="contain_total_loyer">
                            <div class="label_total_loyer"> Total HT </div>
                            <div class="valeur_total_loyer" id="total_loyer_ht"></div>
                        </div>
                        <div class="contain_total_loyer">
                            <div class="label_total_loyer"> Total TVA </div>
                            <div class="valeur_total_loyer" id="total_loyer_tva"></div>
                        </div>
                        <div class="contain_total_loyer">
                            <div class="label_total_loyer"> Total TTC </div>
                            <div class="valeur_total_loyer" id="total_loyer_ttc"></div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col">
                        <label>Commentaire :</label>
                    </div>
                    <div class="col-12">
                        <textarea class="form-control" id="facture_commentaire" rows="4" style="height: 100%;"></textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4">
                        <label>Mode de paiement :</label>
                    </div>
                    <div class="col">
                        <div class="mode_paiement">
                            <span id="paiement_mode"> <?= $cliLot_mode_paiement ?> </span>
                            <span id="paiement_compte"> <?= $cliLot_encaiss_loyer ?> </span>
                            <span id="paiement_iban"> <?= $cliLot_iban_client ?> </span>
                        </div>
                        <input type="hidden" value="<?= $cliLot_mode_paiement ?> <?= $cliLot_encaiss_loyer ?> <?= $cliLot_iban_client ?>" id="mode_paiement_label" />
                    </div>
                </div>

                <input type="hidden" id="moment_facturation" value="<?= $bail_loyer[0]->momfact_nom ?>" />
                <input type="hidden" id="type_echeance" value="<?= $bail_loyer[0]->tpech_valeur ?>" />

                <div class="row">
                    <div class="col-4">
                        <label>Echéance :</label>
                    </div>
                    <div class="col">
                        <div class="echeance_paiement">
                            <span id="echeance_mois"> <?= $bail_loyer[0]->tpech_valeur ?> du mois </span>
                            <span id="echeance_moment_facturation"> (<?= $bail_loyer[0]->momfact_nom ?>) </span> =>
                            <span id="echeance_date"> </span>
                        </div>
                        <input type="hidden" value="virement sur compte client FR76 XXXXXXXXXXX" id="echeance_paiement_label" />
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <div class="card rounded-0 h-100">
                                    <div class="card-body">
                                        <h5 class="fs-0">Emetteur de la facture&nbsp;(Propriétaire)</h5>

                                        <div class="info_proprietaire">
                                            <span class="span_emetteur_nom"><?= $addresse_emetteur['emetteur'] ?></span>
                                            <input type="hidden" value="<?= $addresse_emetteur['emetteur'] ?>" id="emetteur_nom">
                                        </div>

                                        <?php if (!empty($addresse_emetteur['adress1'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_adress1"><?= $addresse_emetteur['adress1'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['adress1'] ?>" id="emet_adress1">
                                            </div>
                                        <?php endif ?>


                                        <?php if (!empty($addresse_emetteur['adress2'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_adress2"><?= $addresse_emetteur['adress2'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['adress2'] ?>" id="emet_adress2">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_emetteur['adress3'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_adress3"><?= $addresse_emetteur['adress3'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['adress3'] ?>" id="emet_adress3">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_emetteur['cp'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_cp"><?= $addresse_emetteur['cp'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['cp'] ?>" id="emet_cp">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_emetteur['ville'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_ville"><?= $addresse_emetteur['ville'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['ville'] ?>" id="emet_ville">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_emetteur['pays'])) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_emet_pays"><?= $addresse_emetteur['pays'] ?></span>
                                                <input type="hidden" value="<?= $addresse_emetteur['pays'] ?>" id="emet_pays">
                                            </div>
                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card rounded-0 h-100">
                                    <div class="card-body">
                                        <h5 class="fs-0">Destinataire de la facture&nbsp;(Gestionnaire)</h5>

                                        <div class="info_proprietaire">
                                            <span class="span_dest_nom"><?= $addresse_gestionnaire[0]->gestionnaire_nom ?></span>
                                            <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_nom ?>" id="dest_nom">
                                        </div>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_adresse1)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_adresse1"><?= $addresse_gestionnaire[0]->gestionnaire_adresse1 ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_adresse1 ?>" id="dest_adresse1">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_adresse2)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_adresse2"><?= $addresse_gestionnaire[0]->gestionnaire_adresse2 ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_adresse2 ?>" id="dest_adresse2">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_adresse3)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_adresse3"><?= $addresse_gestionnaire[0]->gestionnaire_adresse3 ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_adresse3 ?>" id="dest_adresse3">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_cp)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_cp"><?= $addresse_gestionnaire[0]->gestionnaire_cp ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_cp ?>" id="dest_cp">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_ville)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_ville"><?= $addresse_gestionnaire[0]->gestionnaire_ville ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_ville ?>" id="dest_ville">
                                            </div>
                                        <?php endif ?>

                                        <?php if (!empty($addresse_gestionnaire[0]->gestionnaire_pays)) : ?>
                                            <div class="info_proprietaire">
                                                <span class="span_dest_pays"><?= $addresse_gestionnaire[0]->gestionnaire_pays ?></span>
                                                <input type="hidden" value="<?= $addresse_gestionnaire[0]->gestionnaire_pays ?>" id="dest_pays">
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer only_visuel_gestion">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary btn-add-facture-loyer" id="AddFactureLoyer">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

<!-- Fin modal Facture -->

<script>
    deactivate_input_gestion();
    only_visuel_gestion();

    $(document).ready(function() {
        const pdl_id = $("#pdl_id").val();
        let periode = 0;
        if (pdl_id === "1") periode = $("#periode_mens_id").val();
        if (pdl_id === "2") periode = $("#periode_timestre_id").val();
        if (pdl_id === "3") periode = $("#periode_semestre_id").val();
        if (pdl_id === "4") periode = $("#periode_annee_id").val();
        $(".error-ttc-montant").hide();
        fetchIndexations(periode);

        const paidByCELAVI = $("#paidByCELAVI").val();
        if (paidByCELAVI == 1) {
            $("#btnAddFactureLoyer").show();
        } else {
            $("#btnAddFactureLoyer").hide();
        }
    });

    function calculDateEcheanceFacture() {
        const pdl_id = $("#pdl_id").val();
        const periodicite_id = pdl_id;
        const type_echeance = $("#type_echeance").val();
        const moment_facturation = $("#moment_facturation").val();

        const date_echeance = (+type_echeance) < 10 ? '0' + type_echeance : type_echeance;
        let annee_echeance = $("#annee_loyer").val();
        let mois_echeance = 0;
        if (moment_facturation == "échu") {
            if (periodicite_id === "1") mois_echeance = parseInt($("#periode_mens_id").val()) + 1;
            if (periodicite_id === "2") {
                const periode = parseInt($("#periode_timestre_id").val());
                mois_echeance = (periode * 3) + 1;
            }
            if (periodicite_id === "3") {
                const periode = parseInt($("#periode_semestre_id").val());
                mois_echeance = (periode * 6) + 1;
            }
            if (periodicite_id === "4") mois_echeance = 13;
        }

        if (moment_facturation == "échoir") {
            if (periodicite_id === "1") mois_echeance = parseInt($("#periode_mens_id").val());
            if (periodicite_id === "2") {
                const periode = parseInt($("#periode_timestre_id").val());
                mois_echeance = (periode * 3) - 2;
            }
            if (periodicite_id === "3") {
                const periode = parseInt($("#periode_semestre_id").val());
                mois_echeance = (periode * 6) - 5;
            }
            if (periodicite_id === "4") mois_echeance = 0;
        }

        if (mois_echeance > 12) {
            mois_echeance = mois_echeance - 12;
            annee_echeance = (+annee_echeance) + 1;
        }

        if (mois_echeance < 1) {
            mois_echeance = 12 - mois_echeance;
            annee_echeance = (+annee_echeance) - 1;
        }

        mois_echeance = (+mois_echeance) < 10 ? '0' + mois_echeance : mois_echeance;

        const date_echeance_label = `${date_echeance}/${mois_echeance}/${annee_echeance}`;
        $("#echeance_date").text(`${date_echeance_label}`);

        $("#echeance_paiement_label").val(`${type_echeance} du mois (${moment_facturation}) => ${date_echeance_label}`);
    }

    $(document).on('keyup', '#fact_loyer_date', function() {
        var val = $('#annee_loyer').val();
        var annee = val.split('-', 1).join();
        var numero_fact_suivant = $('#fact_view_num').val();
        const numero = annee + '-' + numero_fact_suivant;
        $('#fact_loyer_num').val(numero);
    });

    $('#fact_loyer_date').trigger('keyup');

    $(".count_encaissement").bind("click", function(event) {
        $("#btn_detail_encaissement").click();
        const bail_id = $("#bail_id").val();
        const enc_periode = $(this).attr('data-enc_periode');
        const enc_annee = $("#annee_courant").val();
        getDetailEncaissement(bail_id, enc_annee, enc_periode);
    });

    $(".facture_count").bind("click", function(event) {
        $("#btn_detail_facture").click();
        const bail_id = $("#bail_loyer").val();
        const periode = $(this).attr('data-periode');
        const periode_id = $(this).attr('data-periode_id');
        const label = $(this).attr('data-label');
        const facture_annee = $("#annee_courant").val();
        const cli_lot_id = $("#cli_lot_id").val();

        getDetailFacture(bail_id, facture_annee, periode, periode_id, cli_lot_id);
    });

    $("#btnAddEncaissement").bind("click", function(event) {
        const enc_date = $("#enc_date").val();
        const enc_periode = $("#enc_periode").val();
        const enc_montant = $("#enc_montant").val() ? $("#enc_montant").val() : 0;
        const enc_mode = $("#enc_mode").val();
        const enc_annee = $("#annee_loyer").val();
        const bail_id = $("#bail_id").val();
        const pdl_id = $("#pdl_id").val();
        const bail_loyer_variable = $("#bail_loyer_variable").val();

        const enc_id = $("#enc_id").val() ? $("#enc_id").val() : null;

        const enc_app_ttc = $("#encaissement_loyer_app_ttc").val() ? $("#encaissement_loyer_app_ttc").val() : 0;
        const enc_park_ttc = $("#encaissement_loyer_park_ttc").val() ? $("#encaissement_loyer_park_ttc").val() : 0;
        const enc_charge_ttc = $("#encaissement_loyer_charge_ttc").val() ? $("#encaissement_loyer_charge_ttc").val() : 0;
        const enc_charge_deductible_ttc = $("#encaissement_loyer_charge_deductible_ttc").val() ? $("#encaissement_loyer_charge_deductible_ttc").val() : 0;
        const enc_produit_ttc = $("#encaissement_loyer_produit_ttc").val() ? $("#encaissement_loyer_produit_ttc").val() : 0;
        const enc_total_ttc = $("#encaissement_loyer_total_ttc").val();

        const enc_app_tva = $("#encaissement_loyer_app_tva").val();
        const enc_park_tva = $("#encaissement_loyer_park_tva").val();
        const enc_charge_tva = $("#encaissement_loyer_charge_tva").val();
        const enc_charge_deductible_tva = $("#encaissement_loyer_charge_deductible_tva").val();
        const enc_produit_tva = $("#encaissement_loyer_produit_tva").val();

        const enc_app_montant_tva = $("#encaissement_loyer_app_tva_pource").val();
        const enc_park_montant_tva = $("#encaissement_loyer_park_tva_pource").val();
        const enc_charge_montant_tva = $("#encaissement_loyer_charge_tva_pource").val();
        const enc_charge_deductible_montant_tva = $("#encaissement_loyer_charge_deductible_tva_pource").val();
        const enc_produit_montant_tva = $("#encaissement_loyer_produit_tva_pource").val();
        const enc_total_montant_tva = $("#encaissement_loyer_total_tva").val();

        const enc_app_ht = $("#encaissement_loyer_app_ht").val();
        const enc_park_ht = $("#encaissement_loyer_park_ht").val();
        const enc_charge_ht = $("#encaissement_loyer_charge_ht").val();
        const enc_charge_deductible_ht = $("#encaissement_loyer_charge_deductible_ht").val();
        const enc_produit_ht = $("#encaissement_loyer_produit_ht").val();
        const enc_total_ht = $("#encaissement_loyer_total_ht").val();

        const enc_commentaire = $("#encaissement_commentaire").val();

        const tpchrg_deduct_id = $("#typeChargeDeductible").val();
        const tpchrg_prod_id = $("#typeProduit").val();


        if (enc_montant == 0 ||
            (parseFloat($("#enc_montant").val()).toFixed(2) != parseFloat($("#encaissement_loyer_total_ttc").val()).toFixed(2))) {
            Notiflix.Notify.failure("Montant de l'encaissement incorrect.", {
                position: 'left-bottom',
                timeout: 3000
            });
            return false;
        }

        if (enc_app_ttc < 0 || enc_park_ttc < 0 || enc_charge_ttc < 0 || enc_charge_deductible_ttc < 0 || enc_produit_ttc < 0) {
            Notiflix.Notify.failure("Veuillez vérifier les montants.", {
                position: 'left-bottom',
                timeout: 3000
            });
            return false;
        }

        if (enc_date && enc_periode && enc_montant > 0 && enc_mode && bail_id) {
            addEncaissement(
                enc_date, enc_periode, enc_montant, enc_mode, enc_annee,
                bail_id, pdl_id, bail_loyer_variable, enc_id,
                enc_app_ttc, enc_park_ttc, enc_charge_ttc, enc_charge_deductible_ttc, enc_produit_ttc,
                enc_total_ttc, enc_app_tva, enc_park_tva, enc_charge_tva, enc_charge_deductible_tva,
                enc_produit_tva, enc_app_montant_tva, enc_park_montant_tva, enc_charge_montant_tva,
                enc_charge_deductible_montant_tva, enc_produit_montant_tva, enc_total_montant_tva,
                enc_app_ht, enc_park_ht, enc_charge_ht, enc_charge_deductible_ht, enc_produit_ht,
                enc_total_ht, enc_commentaire, tpchrg_deduct_id, tpchrg_prod_id
            )
        } else {
            Notiflix.Notify.failure("Veuillez compléter les champs.", {
                position: 'left-bottom',
                timeout: 3000
            });
        }
    })

    $("#periode_mens_id, #periode_timestre_id, #periode_semestre_id, #periode_annee_id").bind("change", function(event) {
        const value = event.target.value;
        fetchIndexations(value);
    });

    function fetchIndexations(periode = null) {
        const bail_id = $("#bail_id").val();
        periodes = periode != 0 ? periode : 1;

        var data_request = {
            'bail_id': bail_id,
            'periode': periodes
        }
        var type_request = 'POST';
        var url_request = `${const_app.base_url}Loyer/getIndexationValue`;
        var type_output = 'json';

        fn.ajax_request(type_request, url_request, type_output, data_request, 'retourGetIndexationValue');
        $('.pace-progress').css('display', 'none');
        $('.pace-progress-inner').css('display', 'none');
    }

    function retourGetIndexationValue(response) {
        const indexations = response?.data_retour?.indexations;
        let periode = response?.data_retour?.request?.periode;
        const pdl_id = $("#pdl_id").val();
        // trimestriel
        if (pdl_id === "2") {
            if (periode === "1") {
                month_periode = [1, 2, 3];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "2") {
                month_periode = [4, 5, 6];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "3") {
                month_periode = [7, 8, 9];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "4") {
                month_periode = [10, 11, 12];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
        }
        // trimestrielle decalée
        else if (pdl_id === "5") {
            const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            const pivot = $("#premier_mois_trimes").val();

            arr.sort((a, b) => {
                if (a < pivot && b >= pivot) {
                    return 1;
                } else if (a >= pivot && b < pivot) {
                    return -1;
                } else {
                    return a - b;
                }
            });
            const chunkedArray = chunkArray(arr, 3);
            if (periode === "1") {
                month_periode = chunkedArray[0];

                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "2") {
                month_periode = chunkedArray[1];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "3") {
                month_periode = chunkedArray[2];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "4") {
                month_periode = chunkedArray[3];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
        }
        // semestriel
        else if (pdl_id === "3") {
            if (periode === "1") {
                month_periode = [1, 2, 3, 4, 5, 6];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            }
            if (periode === "2") {
                month_periode = [7, 8, 9, 10, 11, 12];
                for (let index = 0; index < month_periode.length; index++) {
                    var month = month_periode[index];
                    if (getIndexperiode(indexations, month) == true) break;
                }
            };
        }
        // annuel
        else if (pdl_id === "4") {
            month_periode = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            for (let index = 0; index < month_periode.length; index++) {
                var month = month_periode[index];
                if (getIndexperiode(indexations, month) == true) break;
            }
        } else {
            getIndexperiode(indexations, periode);
        }
    }

    function chunkArray(arr, chunkSize) {
        const chunkedArray = [];
        for (let i = 0; i < arr.length; i += chunkSize) {
            const chunk = arr.slice(i, i + chunkSize);
            chunkedArray.push(chunk);
        }
        return chunkedArray;
    }

    function getIndexperiode(indexations, month) {
        let foundPeriode = false;
        const annee = $("#annee_loyer").val();
        month = month < 10 ? "0" + month : month;
        for (let i = 0; i < indexations.length; i++) {
            const indexation = indexations[i];
            const targetDate = new Date(`${annee}-${month}-01`)
            const beginDate = new Date(indexation.indlo_debut);
            const endOfDate = new Date(indexation.indlo_fin);
            const startMonth = beginDate.getMonth();
            const startYear = beginDate.getFullYear();
            const endMonth = endOfDate.getMonth();
            const endYear = endOfDate.getFullYear();
            const startDate = new Date(startYear, startMonth, 1);
            const endDate = new Date(endYear, endMonth, 0);

            if (targetDate >= startDate && targetDate <= endDate) {
                foundPeriode = true;
                $("#fact_loyer_app_ht").val(indexation.indlo_appartement_ht);
                $("#fact_loyer_park_ht").val(indexation.indlo_parking_ht);
                $("#fact_loyer_charge_ht").val(indexation.indlo_charge_ht);

                $("#fact_loyer_app_tva").val(indexation.indlo_appartement_tva);
                $("#fact_loyer_park_tva").val(indexation.indlo_parking_tva);
                $("#fact_loyer_charge_tva").val(indexation.indlo_charge_tva);

                const mtAppTVA = (((+indexation.indlo_appartement_ht) * (+indexation.indlo_appartement_tva)) / 100).toFixed(2);
                const mtParkingTVA = (((+indexation.indlo_parking_ht) * (+indexation.indlo_parking_tva)) / 100).toFixed(2);
                const mtChargeTVA = (((+indexation.indlo_charge_ht) * (+indexation.indlo_charge_tva)) / 100).toFixed(2);

                $("#fact_loyer_app_tva_pource").val(mtAppTVA);
                $("#fact_loyer_park_tva_pource").val(mtParkingTVA);
                $("#fact_loyer_charge_tva_pource").val(mtChargeTVA);

                $("#fact_loyer_app_ttc").val(indexation.indlo_appartement_ttc);
                $("#fact_loyer_park_ttc").val(indexation.indlo_parking_ttc);
                $("#fact_loyer_charge_ttc").val(indexation.indlo_charge_ttc);
            }
        }

        if (!foundPeriode) {
            $("#fact_loyer_app_ht").val(null);
            $("#fact_loyer_park_ht").val(null);
            $("#fact_loyer_charge_ht").val(null);

            $("#fact_loyer_app_tva").val("10");
            $("#fact_loyer_park_tva").val("20");
            $("#fact_loyer_charge_tva").val("20");

            $("#fact_loyer_app_tva_pource").val(null);
            $("#fact_loyer_park_tva_pource").val(null);
            $("#fact_loyer_charge_tva_pource").val(null);

            $("#fact_loyer_app_ttc").val(null);
            $("#fact_loyer_park_ttc").val(null);
            $("#fact_loyer_charge_ttc").val(null);
        }

        calculTotalLoyer();
        calculDateEcheanceFacture();

        return foundPeriode;
    }

    // Calcul TTC
    $("#fact_loyer_app_ht").bind("keyup", function(event) {
        updateAppartementTTCLoyer();
    });

    $("#fact_loyer_app_tva").bind("change", function(event) {
        updateAppartementTTCLoyer();
    });

    function updateAppartementTTCLoyer() {
        let ht = $("#fact_loyer_app_ht").val();
        let tva = $("#fact_loyer_app_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#fact_loyer_app_tva_pource").val(tva.toFixed(2));
        const ttc = (+ht) + (+tva);
        $("#fact_loyer_app_ttc").val(ttc.toFixed(2));

        fix2Decimal("fact_loyer_app_tva_pource", tva);
        fix2Decimal("fact_loyer_app_ttc", ttc);

        calculTotalLoyer();
    }

    $("#fact_loyer_park_ht").on("keyup", function(event) {
        updateParkingTTCLoyer();
    });

    $("#fact_loyer_park_tva").on("change", function(event) {
        updateParkingTTCLoyer();
    });

    function updateParkingTTCLoyer() {
        let ht = $("#fact_loyer_park_ht").val();
        let tva = $("#fact_loyer_park_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#fact_loyer_park_tva_pource").val(tva);
        const ttc = (+ht) + (+tva);
        $("#fact_loyer_park_ttc").val(ttc);

        fix2Decimal("fact_loyer_park_tva_pource", tva);
        fix2Decimal("fact_loyer_park_ttc", ttc);

        calculTotalLoyer();
    }

    $("#fact_loyer_charge_ht").on("keyup", function(event) {
        updateChargeTTCLoyer();
    });

    $("#fact_loyer_charge_tva").on("change", function(event) {
        updateChargeTTCLoyer();
    });

    function updateChargeTTCLoyer() {
        let ht = $("#fact_loyer_charge_ht").val();
        let tva = $("#fact_loyer_charge_tva").val();
        tva = (+ht) * ((+tva) / 100);
        $("#fact_loyer_charge_tva_pource").val(tva);
        const ttc = (+ht) + (+tva);
        $("#fact_loyer_charge_ttc").val(ttc);

        fix2Decimal("fact_loyer_charge_tva_pource", tva);
        fix2Decimal("fact_loyer_charge_ttc", ttc);

        calculTotalLoyer();
    }

    function fix2Decimal(id, value) {
        if (!value || value === 0) return false;
        let newValue = +value;
        newValue = newValue.toFixed(2);
        $(`#${id}`).val(newValue);
    }

    function calculTotalLoyer() {
        let htApp = $("#fact_loyer_app_ht").val();
        let htPark = $("#fact_loyer_park_ht").val();
        let htCharge = $("#fact_loyer_charge_ht").val();
        let totalHT = (+htApp) + (+htPark) - (+htCharge);
        totalHT = totalHT.toFixed(2);

        let tvaApp = $("#fact_loyer_app_tva_pource").val();
        let tvaPark = $("#fact_loyer_park_tva_pource").val();
        let tvaCharge = $("#fact_loyer_charge_tva_pource").val();
        let totalTVA = (+tvaApp) + (+tvaPark) - (+tvaCharge);
        totalTVA = totalTVA.toFixed(2);

        let ttcApp = $("#fact_loyer_app_ttc").val();
        let ttcPark = $("#fact_loyer_park_ttc").val();
        let ttcCharge = $("#fact_loyer_charge_ttc").val();
        let totalTTC = (+ttcApp) + (+ttcPark) - (+ttcCharge);
        totalTTC = totalTTC.toFixed(2);

        $("#total_loyer_ht").text(`${totalHT} €`);
        $("#total_loyer_tva").text(`${totalTVA} €`);
        $("#total_loyer_ttc").text(`${totalTTC} €`);
    }

    // Calcul Tableau Encaissement
    function calculApartTTCEncaissement() {
        const montant_encaisse = $("#enc_montant").val() ? parseFloat($("#enc_montant").val()) : 0;
        const montant_ttc_parking = $("#encaissement_loyer_park_ttc").val() ? parseFloat($("#encaissement_loyer_park_ttc").val()) : 0;
        const montant_ttc_forfait_charges = $("#encaissement_loyer_charge_ttc").val() ? parseFloat($("#encaissement_loyer_charge_ttc").val()) : 0;
        const montant_ttc_charges_deductibles = $("#encaissement_loyer_charge_deductible_ttc").val() ? parseFloat($("#encaissement_loyer_charge_deductible_ttc").val()) : 0;
        const montant_ttc_produits = $("#encaissement_loyer_produit_ttc").val() ? parseFloat($("#encaissement_loyer_produit_ttc").val()) : 0;
        const montant_ttc_appartement = montant_encaisse - montant_ttc_parking + montant_ttc_forfait_charges + montant_ttc_charges_deductibles - montant_ttc_produits;
        $("#encaissement_loyer_app_ttc").val(montant_ttc_appartement.toFixed(2));
        calculTotalEncaissement();
        calculTVA_HT("encaissement_loyer_app_ttc", "encaissement_loyer_app_tva", "encaissement_loyer_app_tva_pource", "encaissement_loyer_app_ht");
    }

    $("#enc_montant").bind("keyup", function(event) {
        const val = event.target.value;
        if (!val || parseFloat(val) < 0) {
            Notiflix.Notify.failure("Montant encaissé invalide.", {
                position: 'left-bottom',
                timeout: 3000
            });
            return false;
        }
        calculApartTTCEncaissement();
        calculTVA_HT("encaissement_loyer_app_ttc", "encaissement_loyer_app_tva", "encaissement_loyer_app_tva_pource", "encaissement_loyer_app_ht");
    });

    $("#encaissement_loyer_park_ttc").bind("keyup", function(event) {
        calculTVA_HT("encaissement_loyer_park_ttc", "encaissement_loyer_park_tva", "encaissement_loyer_park_tva_pource", "encaissement_loyer_park_ht");
        calculApartTTCEncaissement();
    });

    $("#encaissement_loyer_charge_ttc").bind("keyup", function(event) {
        calculTVA_HT("encaissement_loyer_charge_ttc", "encaissement_loyer_charge_tva", "encaissement_loyer_charge_tva_pource", "encaissement_loyer_charge_ht");
        calculApartTTCEncaissement();
    });

    $("#encaissement_loyer_charge_deductible_ttc").bind("keyup", function(event) {
        calculTVA_HT("encaissement_loyer_charge_deductible_ttc", "encaissement_loyer_charge_deductible_tva", "encaissement_loyer_charge_deductible_tva_pource", "encaissement_loyer_charge_deductible_ht");
        calculApartTTCEncaissement();
    });

    $("#encaissement_loyer_produit_ttc").bind("keyup", function(event) {
        calculTVA_HT("encaissement_loyer_produit_ttc", "encaissement_loyer_produit_tva", "encaissement_loyer_produit_tva_pource", "encaissement_loyer_produit_ht");
        calculApartTTCEncaissement();
    });

    function calculTotalEncaissement() {
        const encaissement_loyer_app_ttc = $("#encaissement_loyer_app_ttc").val() ? parseFloat($("#encaissement_loyer_app_ttc").val()) : 0;
        const encaissement_loyer_park_ttc = $("#encaissement_loyer_park_ttc").val() ? parseFloat($("#encaissement_loyer_park_ttc").val()) : 0;
        const encaissement_loyer_charge_ttc = $("#encaissement_loyer_charge_ttc").val() ? parseFloat($("#encaissement_loyer_charge_ttc").val()) : 0;
        const encaissement_loyer_charge_deductible_ttc = $("#encaissement_loyer_charge_deductible_ttc").val() ? parseFloat($("#encaissement_loyer_charge_deductible_ttc").val()) : 0;
        const encaissement_loyer_produit_ttc = $("#encaissement_loyer_produit_ttc").val() ? parseFloat($("#encaissement_loyer_produit_ttc").val()) : 0;

        const encaissement_loyer_app_tva_pource = $("#encaissement_loyer_app_tva_pource").val() ? parseFloat($("#encaissement_loyer_app_tva_pource").val()) : 0;
        const encaissement_loyer_park_tva_pource = $("#encaissement_loyer_park_tva_pource").val() ? parseFloat($("#encaissement_loyer_park_tva_pource").val()) : 0;
        const encaissement_loyer_charge_tva_pource = $("#encaissement_loyer_charge_tva_pource").val() ? parseFloat($("#encaissement_loyer_charge_tva_pource").val()) : 0;
        const encaissement_loyer_charge_deductible_tva_pource = $("#encaissement_loyer_charge_deductible_tva_pource").val() ? parseFloat($("#encaissement_loyer_charge_deductible_tva_pource").val()) : 0;
        const encaissement_loyer_produit_tva_pource = $("#encaissement_loyer_produit_tva_pource").val() ? parseFloat($("#encaissement_loyer_produit_tva_pource").val()) : 0;

        const encaissement_loyer_app_ht = $("#encaissement_loyer_app_ht").val() ? parseFloat($("#encaissement_loyer_app_ht").val()) : 0;
        const encaissement_loyer_park_ht = $("#encaissement_loyer_park_ht").val() ? parseFloat($("#encaissement_loyer_park_ht").val()) : 0;
        const encaissement_loyer_charge_ht = $("#encaissement_loyer_charge_ht").val() ? parseFloat($("#encaissement_loyer_charge_ht").val()) : 0;
        const encaissement_loyer_charge_deductible_ht = $("#encaissement_loyer_charge_deductible_ht").val() ? parseFloat($("#encaissement_loyer_charge_deductible_ht").val()) : 0;
        const encaissement_loyer_produit_ht = $("#encaissement_loyer_produit_ht").val() ? parseFloat($("#encaissement_loyer_produit_ht").val()) : 0;

        const encaissement_loyer_total_ttc = encaissement_loyer_app_ttc + encaissement_loyer_park_ttc - encaissement_loyer_charge_ttc - encaissement_loyer_charge_deductible_ttc + encaissement_loyer_produit_ttc;
        const encaissement_loyer_total_tva = encaissement_loyer_app_tva_pource + encaissement_loyer_park_tva_pource - encaissement_loyer_charge_tva_pource - encaissement_loyer_charge_deductible_tva_pource + encaissement_loyer_produit_tva_pource;
        const encaissement_loyer_total_ht = encaissement_loyer_app_ht + encaissement_loyer_park_ht - encaissement_loyer_charge_ht - encaissement_loyer_charge_deductible_ht + encaissement_loyer_produit_ht;

        $("#encaissement_loyer_total_ttc").val(encaissement_loyer_total_ttc.toFixed(2));
        $("#encaissement_loyer_total_tva").val(encaissement_loyer_total_tva.toFixed(2));
        $("#encaissement_loyer_total_ht").val(encaissement_loyer_total_ht.toFixed(2));

        setTimeout(() => {
            if (parseFloat($("#enc_montant").val()).toFixed(2) != parseFloat($("#encaissement_loyer_total_ttc").val()).toFixed(2)) {
                $(".error-ttc-montant").show();
            } else {
                $(".error-ttc-montant").hide();
            }
        }, 1000);
    }

    $("#encaissement_loyer_app_tva").bind("change", function(event) {
        calculTVA_HT("encaissement_loyer_app_ttc", "encaissement_loyer_app_tva", "encaissement_loyer_app_tva_pource", "encaissement_loyer_app_ht");
    });

    $("#encaissement_loyer_park_tva").bind("change", function(event) {
        calculTVA_HT("encaissement_loyer_park_ttc", "encaissement_loyer_park_tva", "encaissement_loyer_park_tva_pource", "encaissement_loyer_park_ht");
    });

    $("#encaissement_loyer_charge_tva").bind("change", function(event) {
        calculTVA_HT("encaissement_loyer_charge_ttc", "encaissement_loyer_charge_tva", "encaissement_loyer_charge_tva_pource", "encaissement_loyer_charge_ht");
    });

    $("#encaissement_loyer_charge_deductible_tva").bind("change", function(event) {
        calculTVA_HT("encaissement_loyer_charge_deductible_ttc", "encaissement_loyer_charge_deductible_tva", "encaissement_loyer_charge_deductible_tva_pource", "encaissement_loyer_charge_deductible_ht");
    });

    $("#encaissement_loyer_produit_tva").bind("change", function(event) {
        calculTVA_HT("encaissement_loyer_produit_ttc", "encaissement_loyer_produit_tva", "encaissement_loyer_produit_tva_pource", "encaissement_loyer_produit_ht");
    });

    function calculTVA_HT(id_ttc, id_tva, id_tva_source, id_ht) {
        const ttc = $(`#${id_ttc}`).val() ? parseFloat($(`#${id_ttc}`).val()) : 0;
        const tva = $(`#${id_tva}`).val() ? parseFloat($(`#${id_tva}`).val()) : 0;

        const val = event.target.value;
        if (parseFloat(ttc) < 0) {
            Notiflix.Notify.failure(`Montant invalide.`, {
                position: 'left-bottom',
                timeout: 3000
            });
            return false;
        }

        const ht = (ttc / (1 + (tva / 100))).toFixed(2);
        $(`#${id_ht}`).val(ht);

        const valueTVA = (ttc - ht).toFixed(2);
        $(`#${id_tva_source}`).val(valueTVA);
    }
</script>

<style>
    .colored_table {
        border-right: 1px solid #ffff;
        background: #2569C3 !important;
        text-align: center;
    }

    .count_encaissement,
    .facture_count {
        cursor: pointer;
        font-weight: 500;
        transition: all 0.3s;
    }

    .count_encaissement:hover,
    .facture_count:hover {
        color: #2569C3;
    }

    .input_loyer {
        height: 36px;
        padding: 5px;
    }

    .group-loyer {
        width: 20px;
        display: flex;
        justify-content: center;
    }

    .contain_total_loyer {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .label_total_loyer {
        background: #2569c3;
        width: 50%;
        color: white;
        padding: 7px;
        border: 1px solid;
    }

    .valeur_total_loyer {
        padding: 7px;
        background: #edf2f9;
        width: 50%;
        text-align: right;
        border: 1px solid white;
    }

    .mode_paiement,
    .echeance_paiement {
        margin-bottom: 10px;
        font-weight: 500;
    }

    .title-tva-collected label {
        font-size: 14px;
        border-bottom: 1px solid;
    }

    .error-ttc-montant {
        background: #ff5656;
        padding: 10px;
        color: white;
        margin-bottom: 10px;
    }

    .main-input-group {
        display: flex;
    }

    .input-group.input-group-tva {
        width: 60%;
    }

    .input-group.input-group-montant-tva {
        margin-left: 5px;
    }

    .td-encaissement {
        padding: 3px 5px !important;
    }

    .td-encaissement.flex div:first-child {
        display: flex;
        height: 65px;
        align-items: center;
    }

    select#typeChargeDeductible,
    select#typeProduit {
        margin: 0;
        padding-left: 5px;
    }

    select#encaissement_loyer_charge_deductible_tva,
    select#encaissement_loyer_produit_tva {
        margin-bottom: 0px !important;
    }
</style>