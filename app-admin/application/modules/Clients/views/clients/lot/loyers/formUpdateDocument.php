<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Renommer un fichier</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Loyer/UpdateDocument', array('method' => "post", 'class' => 'px-2', 'id' => 'UpdateDocument')); ?>

<input type="hidden" name="doc_loyer_id" id="doc_loyer_id" value="<?= $doc_loyer_id; ?> ">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
<input type="hidden" name="doc_loyer_annee" id="doc_loyer_annee" value="<?= $annee_loyer; ?>">

<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>
                            <label data-width="100"> Nom : </label>
                            <input type="text" class="form-control" id="doc_loyer_nom" name="doc_loyer_nom" value="<?= $document->doc_loyer_nom ?> ">
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>
<?php echo form_close(); ?>