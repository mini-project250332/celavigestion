<div class="uploader_file_loyer mt-4">

</div>
<div class="clearfix p-t-5 p-b-5">
    <div class="float-left">
    </div>
    <div class="d-flex justify-content-end">
        <button type="button" id="stopped-upload" class="btn btn-danger d-none btn-sm mx-2 text-white">
            <i class="fas fa-times-circle red"></i>
            Annuler
        </button>
        <button type="button" id="add-files-Loyer" class="btn btn-info btn-sm bold mx-1">
            <i class="fas fa-plus-circle"></i>
            Ajouter des fichiers
        </button>
        <button type="button" id="start-files-getLoyer" class="btn btn-success btn-sm d-none text-white">
            <i class="fas fa-cloud-upload-alt green "></i>
            Démarrer l'envoi
        </button>
    </div>
</div>

<input id="url_upload_loyer" type="hidden" name="url_upload_loyer" value="<?php echo base_url("Clients/Clients/uploadfileLoyer"); ?>">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id ?>">

<script type="text/javascript">
    $(document).ready(function() {
        var url_upload_loyer = $('#url_upload_loyer').val();
        $(".uploader_file_loyer").pluploadQueue({
            browse_button: 'add-files-Loyer',
            runtimes: 'html5,flash,silverlight,html4',
            chunk_size: '1mb',
            url: url_upload_loyer,
            rename: true,
            unique_names: true,
            sortable: true,
            dragdrop: true,
            flash_swf_url: '<?php echo base_url("assets/js/plupload/js/Moxie.swf"); ?>',
            silverlight_xap_url: '<?php echo base_url("assets/js/plupload/js/Moxie.xap"); ?>'
        });

        var uploader = $('.uploader_file_loyer').pluploadQueue();
        uploader.bind('FilesRemoved', function() {
            updateUploadButtonVisibility();
        });
        uploader.bind('FilesAdded', function() {
            updateUploadButtonVisibility();
        });
        uploader.bind('UploadComplete', function() {
            // upload finish
        });
        uploader.bind('FileUploaded', function(response) {
            if (uploader.files.length == (uploader.total.uploaded + uploader.total.failed)) {
                var annee = $('#annee_loyer').val();
                var cliLot_id = $('#cliLot_id').val();
                $("#modalAjoutLoyer").modal('hide');
                getLoyers(annee, cliLot_id);
            }
        });

        $('#start-files-getLoyer').click(function(e) {
            e.preventDefault();
            startFileUpload();
        });

        $('#stopped-upload').click(function(e) {
            e.preventDefault();
            uploader.stop();
        });

        function updateUploadButtonVisibility() {
            if (uploader.files.length > 0) {
                $('#start-files-getLoyer').removeClass('d-none');
            } else {
                $('#start-files-getLoyer, #stopped-upload').addClass('d-none');
            }
        }

        function startFileUpload() {
            if (uploader.files.length > 0) {
                var files_names = {};
                var cliLot_id = $('#cliLot_id').val();
                var annee = $('#annee_loyer').val();
                $("div.plupload_file_name").each(function(ind, elem) {
                    var content = $(this).html();
                    var infos_file = {};
                    if (content != 'Nom du fichier') {
                        if ($(this).parent().attr("id") != null) {
                            infos_file.originalName = content;
                            infos_file.fileName = $(this).parent().attr("id");
                            files_names[ind] = infos_file;
                        }
                    }
                });
                $('#stopped-upload').removeClass('d-none');
                uploader.settings.multipart_params = {
                    cliLot_id: cliLot_id,
                    fichiers: files_names,
                    annee: annee
                };
                uploader.start();
            }
        }
    });
</script>