<div>
    <input type="hidden" value="<?= $bail->bail_id ?>" id="detail_bail_id" />
    <input type="hidden" value="<?= $bail->pdl_id ?>" id="detail_pdl_id" />
    <input type="hidden" value="<?= $bail->bail_loyer_variable ?>" id="detail_bail_loyer_variable" />
    <input type="hidden" value="<?= $enc_annee ?>" id="detail_enc_annee" />
    <input type="hidden" value="<?= $enc_periode ?>" id="detail_enc_periode" />


    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2 contain_thead">Année</th>
                <th class="p-2 contain_thead">Période</th>
                <th class="p-2 contain_thead">Date</th>
                <th class="p-2 contain_thead">Montant</th>
                <th class="p-2 contain_thead">Commentaire</th>
                <th class="p-2 contain_thead">Action</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <?php if (isset($detail_encaissement) && count($detail_encaissement) > 0) { ?>
                <?php foreach ($detail_encaissement as $key => $encaissement): ?>
                    <tr class="view-encaissement-<?= $key ?>">
                        <td class="text-center view_enc_annee_<?= $key ?>">
                            <?php echo $encaissement->enc_annee ?>
                        </td>
                        <td class="text-center view_enc_periode_<?= $key ?>">
                            <?php echo $encaissement->enc_periode ?>
                        </td>
                        <td class="text-center view_enc_date_<?= $key ?>">
                            <?= date("d/m/Y", strtotime(str_replace('/', '-', $encaissement->enc_date))); ?>
                        </td>
                        <td class="text-end view_enc_montant_<?= $key ?>">
                            <?php echo format_number_($encaissement->enc_montant) ?> €
                        </td>
                        <td class="text-center view_enc_commentaire_<?= $key ?>">
                            <div class="enc_commentaire">
                                <?php echo $encaissement->enc_commentaire ?>
                            </div>
                        </td>
                        <td>
                            <div class="text-center action_detail_encaissmenent">
                                <button class="btn btn-sm btn-outline-primary btn_edit_encaissement" data-id="<?= $key ?>"
                                    data-enc_id="<?= $encaissement->enc_id ?>" data-enc_date="<?= $encaissement->enc_date ?>"
                                    data-enc_periode="<?= $encaissement->enc_periode ?>"
                                    data-enc_annee="<?= $encaissement->enc_annee ?>"
                                    data-enc_montant="<?= $encaissement->enc_montant ?>"
                                    data-enc_mode="<?= $encaissement->enc_mode ?>" data-bail_id="<?= $encaissement->bail_id ?>"
                                    data-enc_app_ttc="<?= $encaissement->enc_app_ttc ?>"
                                    data-enc_park_ttc="<?= $encaissement->enc_park_ttc ?>"
                                    data-enc_charge_ttc="<?= $encaissement->enc_charge_ttc ?>"
                                    data-enc_charge_deductible_ttc="<?= $encaissement->enc_charge_deductible_ttc ?>"
                                    data-enc_produit_ttc="<?= $encaissement->enc_produit_ttc ?>"
                                    data-enc_total_ttc="<?= $encaissement->enc_total_ttc ?>"
                                    data-enc_app_tva="<?= $encaissement->enc_app_tva ?>"
                                    data-enc_park_tva="<?= $encaissement->enc_park_tva ?>"
                                    data-enc_charge_tva="<?= $encaissement->enc_charge_tva ?>"
                                    data-enc_charge_deductible_tva="<?= $encaissement->enc_charge_deductible_tva ?>"
                                    data-enc_produit_tva="<?= $encaissement->enc_produit_tva ?>"
                                    data-enc_app_montant_tva="<?= $encaissement->enc_app_montant_tva ?>"
                                    data-enc_park_montant_tva="<?= $encaissement->enc_park_montant_tva ?>"
                                    data-enc_charge_montant_tva="<?= $encaissement->enc_charge_montant_tva ?>"
                                    data-enc_charge_deductible_montant_tva="<?= $encaissement->enc_charge_deductible_montant_tva ?>"
                                    data-enc_produit_montant_tva="<?= $encaissement->enc_produit_montant_tva ?>"
                                    data-enc_total_montant_tva="<?= $encaissement->enc_total_montant_tva ?>"
                                    data-enc_app_ht="<?= $encaissement->enc_app_ht ?>"
                                    data-enc_park_ht="<?= $encaissement->enc_park_ht ?>"
                                    data-enc_charge_ht="<?= $encaissement->enc_charge_ht ?>"
                                    data-enc_charge_deductible_ht="<?= $encaissement->enc_charge_deductible_ht ?>"
                                    data-enc_produit_ht="<?= $encaissement->enc_produit_ht ?>"
                                    data-enc_total_ht="<?= $encaissement->enc_total_ht ?>"
                                    data-enc_commentaire="<?php echo htmlentities($encaissement->enc_commentaire, ENT_QUOTES); ?>"
                                    data-tpchrg_deduct_id="<?= $encaissement->tpchrg_deduct_id ?>"
                                    data-tpchrg_prod_id="<?= $encaissement->tpchrg_prod_id ?>">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                                <button class="only_visuel_gestion btn btn-sm btn-outline-danger btn_delete_encaissement"
                                    data-enc_id="<?= $encaissement->enc_id ?>">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php } else { ?>
                <div class="text-center mb-3">Aucun encaissement</div>
            <?php } ?>
        </tbody>
    </table>
</div>

<script>
    only_visuel_gestion();

    $(".btn_edit_encaissement").bind("click", function (event) {
        $("#modal_detail_encaissement").modal('hide');

        $("#btnAddNewEncaissement").trigger("click");

        const id = $(this).attr("data-id");
        const enc_id = $(this).attr("data-enc_id");
        const enc_date = $(this).attr("data-enc_date");
        const enc_periode = $(this).attr("data-enc_periode");
        const enc_annee = $(this).attr("data-enc_annee");
        const enc_montant = $(this).attr("data-enc_montant");
        const enc_mode = $(this).attr("data-enc_mode");
        const bail_id = $(this).attr("data-bail_id");
        const enc_app_ttc = $(this).attr("data-enc_app_ttc");
        const enc_park_ttc = $(this).attr("data-enc_park_ttc");
        const enc_charge_ttc = $(this).attr("data-enc_charge_ttc");
        const enc_charge_deductible_ttc = $(this).attr("data-enc_charge_deductible_ttc");
        const enc_produit_ttc = $(this).attr("data-enc_produit_ttc");
        const enc_total_ttc = $(this).attr("data-enc_total_ttc");
        const enc_app_tva = $(this).attr("data-enc_app_tva");
        const enc_park_tva = $(this).attr("data-enc_park_tva");
        const enc_charge_tva = $(this).attr("data-enc_charge_tva");
        const enc_charge_deductible_tva = $(this).attr("data-enc_charge_deductible_tva");
        const enc_produit_tva = $(this).attr("data-enc_produit_tva");
        const enc_app_montant_tva = $(this).attr("data-enc_app_montant_tva");
        const enc_park_montant_tva = $(this).attr("data-enc_park_montant_tva");
        const enc_charge_montant_tva = $(this).attr("data-enc_charge_montant_tva");
        const enc_charge_deductible_montant_tva = $(this).attr("data-enc_charge_deductible_montant_tva");
        const enc_produit_montant_tva = $(this).attr("data-enc_produit_montant_tva");
        const enc_total_montant_tva = $(this).attr("data-enc_total_montant_tva");
        const enc_app_ht = $(this).attr("data-enc_app_ht");
        const enc_park_ht = $(this).attr("data-enc_park_ht");
        const enc_charge_ht = $(this).attr("data-enc_charge_ht");
        const enc_charge_deductible_ht = $(this).attr("data-enc_charge_deductible_ht");
        const enc_produit_ht = $(this).attr("data-enc_produit_ht");
        const enc_total_ht = $(this).attr("data-enc_total_ht");
        const enc_commentaire = $(this).attr("data-enc_commentaire");
        const tpchrg_deduct_id = $(this).attr("data-tpchrg_deduct_id");
        const tpchrg_prod_id = $(this).attr("data-tpchrg_prod_id");

        $("#enc_id").val(enc_id);
        $("#enc_montant").val(enc_montant);
        $("#enc_date").val(enc_date);
        $("#enc_periode").val(enc_periode);

        $("#encaissement_commentaire").val(enc_commentaire);

        $("#encaissement_loyer_app_ttc").val(enc_app_ttc);
        $("#encaissement_loyer_app_tva").val(enc_app_tva);
        $("#encaissement_loyer_app_tva_pource").val(enc_app_montant_tva);
        $("#encaissement_loyer_app_ht").val(enc_app_ht);

        $("#encaissement_loyer_park_ttc").val(enc_park_ttc);
        $("#encaissement_loyer_park_tva").val(enc_park_tva);
        $("#encaissement_loyer_park_tva_pource").val(enc_park_montant_tva);
        $("#encaissement_loyer_park_ht").val(enc_park_ht);

        $("#encaissement_loyer_charge_ttc").val(enc_charge_ttc);
        $("#encaissement_loyer_charge_tva").val(enc_charge_tva);
        $("#encaissement_loyer_charge_tva_pource").val(enc_charge_montant_tva);
        $("#encaissement_loyer_charge_ht").val(enc_charge_ht);

        $("#encaissement_loyer_charge_deductible_ttc").val(enc_charge_deductible_ttc);
        $("#encaissement_loyer_charge_deductible_tva").val(enc_charge_deductible_tva);
        $("#encaissement_loyer_charge_deductible_tva_pource").val(enc_charge_deductible_montant_tva);
        $("#encaissement_loyer_charge_deductible_ht").val(enc_charge_deductible_ht);

        $("#encaissement_loyer_produit_ttc").val(enc_produit_ttc);
        $("#encaissement_loyer_produit_tva").val(enc_produit_tva);
        $("#encaissement_loyer_produit_tva_pource").val(enc_produit_montant_tva);
        $("#encaissement_loyer_produit_ht").val(enc_produit_ht);

        $("#encaissement_loyer_total_ttc").val(enc_total_ttc);
        $("#encaissement_loyer_total_tva").val(enc_total_montant_tva);
        $("#encaissement_loyer_total_ht").val(enc_total_ht);

        $("#typeChargeDeductible").val(tpchrg_deduct_id);
        $("#typeProduit").val(tpchrg_prod_id);

        $("#btnAddEncaissement").text("Modifier");
    });

    $(".btn_delete_encaissement").bind("click", function (event) {
        const enc_id = $(this).attr("data-enc_id");
        console.log("enc_id ", enc_id);
        removeEncaissement(enc_id);
    });

    $('#modal_encaissement').on('hidden.bs.modal', function () {
        $("#enc_id").val("");
        $("#enc_montant").val(0);
        $("#encaissement_commentaire").val("");

        $("#encaissement_loyer_app_ttc").val(0);
        $("#encaissement_loyer_app_tva").val("10");
        $("#encaissement_loyer_app_tva_pource").val("10");
        $("#encaissement_loyer_app_ht").val(0);

        $("#encaissement_loyer_park_ttc").val(0);
        $("#encaissement_loyer_park_tva").val("20");
        $("#encaissement_loyer_park_tva_pource").val(0);
        $("#encaissement_loyer_park_ht").val(0);

        $("#encaissement_loyer_charge_ttc").val(0);
        $("#encaissement_loyer_charge_tva").val("20");
        $("#encaissement_loyer_charge_tva_pource").val(0);
        $("#encaissement_loyer_charge_ht").val(0);

        $("#encaissement_loyer_charge_deductible_ttc").val(0);
        $("#encaissement_loyer_charge_deductible_tva").val("20");
        $("#encaissement_loyer_charge_deductible_tva_pource").val(0);
        $("#encaissement_loyer_charge_deductible_ht").val(0);

        $("#encaissement_loyer_produit_ttc").val(0);
        $("#encaissement_loyer_produit_tva").val("20");
        $("#encaissement_loyer_produit_tva_pource").val(0);
        $("#encaissement_loyer_produit_ht").val(0);

        $("#encaissement_loyer_total_ttc").val(0);
        $("#encaissement_loyer_total_tva").val(0);
        $("#encaissement_loyer_total_ht").val(0);

        $("#btnAddEncaissement").text("Enregistrer");
    });
</script>

<style>
    .input-detail-encaissement {
        margin: 0;
        padding: 0;
        padding-left: 5px;
        width: 100px;
        height: 36px;
    }

    .contain_enc_annee {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 60px;
    }

    .input-enc-montant {
        width: 80px !important;
        padding-right: 5px;
    }

    .contain_thead {
        border-right: 1px solid white !important;
        background: #2569C3 !important;
    }

    .enc_commentaire {
        text-align: left;
        word-break: break-word;
    }

    .action_detail_encaissmenent {
        min-width: 90px;
    }
</style>