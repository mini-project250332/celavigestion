<div class="contenair-title" id="header-loyer-document">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Documents</h5>
        </div>
        <?php
        $date_now = date('Y-m-d');
        $date_fin_mandat = NULL;
        if (isset($mandat->mandat_date_fin)) {
            $date_fin_mandat = $mandat->mandat_date_fin;
        }
        ?>
        <div class="col-6 pt-1 <?= isset($mandat->etat_mandat_id) ? ($mandat->etat_mandat_id == 7 ? '' : 'd-none') : 'd-none' ?>">
            <div class="alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                Le mandat pour ce lot a été suspendu par <?= $mandat->util_prenom ?> le <?= date("d/m/Y", strtotime(str_replace('/', '-', $mandat->mandat_date_supension))) ?> pour la raison suivante : "<?= $mandat->mandat_motif_suspension ?>". Vous ne pouvez plus modifier ce lot
            </div>
        </div>
        <div class="col <?= isset($mandat->etat_mandat_id) ? ($mandat->etat_mandat_id == 6 ? '' : 'd-none') : 'd-none' ?>">
            <?php if ($date_fin_mandat != NULL) {
                if ($date_fin_mandat >= $date_now) {
                    echo '<div class= "alert alert-warning text-center m-0 p-0 fs--1" role="alert">
                            Attention, le mandat sera clôturé le ' . date("d/m/Y", strtotime(str_replace('/', '-', $date_fin_mandat))) . '
                        </div>';
                } else if ($date_fin_mandat < $date_now) {
                    echo '<div class= "alert alert-danger text-center m-0 p-0 fs--1" role="alert">
                            Attention, le mandat est clôturé depuis le ' . date("d/m/Y", strtotime(str_replace('/', '-', $date_fin_mandat))) . '
                        </div>';
                }
            } ?>
        </div>
        <div class="col">
            <?php
            if (isset($max_date)) {
                $date_signature = $max_date['annee'];
                $max_year = strtotime($date_signature);
                $year = date('Y', $max_year) - 2;
            } else {
                $year = 2022;
            }
            ?>
            <select class="no_effect form-select fs--1 m-1" id="annee_loyer">
                <?php if (isset($filtre_array)) : ?>
                    <?php $year = intval($filtre_array[1]) > intval(date("Y")) ? intval($filtre_array[1]) : intval(date("Y") + 1) ?>
                    <?php for ($i = $filtre_array[0]; $i <= $year; $i++) : ?>
                        <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor ?>
                <?php else : ?>
                    <?php for ($i = intval(date("Y")) + 1; $i >= intval($year); $i--) : ?>
                        <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor ?>
                <?php endif ?>
            </select>
            <div class="fs--1 text-danger text-center"><i><?= empty($date_signature) ? "aucun mandat signé !"  : "" ?></i></div>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1 <?= isset($mandat->etat_mandat_id) ? ((($mandat->etat_mandat_id == 6 && $mandat->mandat_date_fin < date('Y-m-d')) || $mandat->etat_mandat_id == 7) ? 'd-none' : '') : "" ?>" data-id="<?= $cliLot_id ?>" id="ajoutDocLoyer">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
                <button class="btn btn-sm btn-primary m-1" data-cliLot_id="<?= $cliLot_id ?>" id="viewHistoriqueMail">
                    <span class="fas fa-history me-1"></span>
                    <span> Historique </span>
                </button>
            </div>
        </div>
    </div>
    <?php if (isset($lot_info) && $lot_info->typelot_libelle == "Plus à bail") { ?>
        <div class="div_client_plus_a_bail">
            Propriétaire plus à bail
        </div>
    <?php } ?>
</div>


<div class="h-100 w-100" id="container-loyer-facture">


</div>


<!-- modal -->
<div class="modal fade" id="modalAjoutLoyer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadLoyer">

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        getLoyers(<?= date('Y') ?>, <?= $cliLot_id; ?>);
    });
</script>