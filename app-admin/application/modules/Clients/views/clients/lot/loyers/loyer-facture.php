<?php function formatDateFr($date)
{
    $date = str_replace('-"', '/', $date);
    $newDate = date("d/m/Y", strtotime($date));
    return $newDate;
}
?>

<input type="hidden" value="<?= $cliLot_id; ?>" id="cli_lot_id" />

<div class="row pt-3 m-0 panel-container" id="">

    <div class="col-6 panel-left" style="width: 889px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0">
            <div class="col-12" style=" height: calc(25vh - 50px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
                <?php
                foreach ($document as $documents) { ?>
                    <div class="row apercu_docu" id="docrowloyer-<?= $documents->doc_loyer_id ?>">
                        <div class="col-4 pt-2">
                            <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_loyer_id ?>" onclick="apercuDocumentLoyer(<?= $documents->doc_loyer_id ?>)"> <?= $documents->doc_loyer_nom ?></div>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-user"></i>
                                <?= $documents->util_prenom ?>
                            </label>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-calendar"></i>
                                <?= $documents->doc_loyer_creation ?>
                            </label>
                        </div>
                        <div class="col-4 text-start">
                            <div class="btn-group fs--1" style="left: 17%;">
                                <span>
                                    <button class="only_visuel_gestion btn btn-sm btn-success rounded-pill <?= ($documents->doc_loyer_traite == 1) ? '' : 'd-none' ?> btnNonTraiteLoyer" id="TraiterLoyer" data-bs-toggle="tooltip" data-bs-placement="bottom" title="document traité par <?= $documents->prenom_util ?> le <?= $documents->doc_loyer_traitement ?>" data-id="<?= $documents->doc_loyer_id ?>">
                                        <i class="fas fa-check"></i>
                                    </button>
                                </span>
                                <button class="only_visuel_gestion btn btn-sm btn-secondary rounded-pill <?= ($documents->doc_loyer_traite == 1) ? 'd-none' : '' ?> btnTraiteLoyer" id="nonTraiterLoyer" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= !empty($documents->prenom_util) ? 'traitement document annulé par ' . $documents->prenom_util . ' ' . 'le ' . $documents->doc_loyer_traitement . ' ' : 'Document non traité'; ?>" data-id="<?= $documents->doc_loyer_id ?>">
                                    <i class="fas fa-check"></i>
                                </button> &nbsp;

                                <span class="btn btn-sm btn-outline-warning rounded-pill renommer-fact" data-id="<?= $documents->doc_loyer_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
                                    <i class="far fa-edit"></i>
                                </span>&nbsp;

                                <span class="btn btn-sm btn-outline-primary rounded-pill">
                                    <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_loyer_path ?>','<?= trim($documents->doc_loyer_nom) ?>')" data-url="<?= base_url($documents->doc_loyer_path) ?>">
                                    </i>
                                </span>&nbsp;

                                <span class="only_visuel_gestion btn btn-sm btn-outline-info  rounded-pill" data-id="<?= $documents->doc_loyer_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Envoi Mail" id="modalDocumentFacture">
                                    <i class="fas fa-envelope"></i>
                                </span>

                                <?php if (isset($documents->fact_email_sent) && $documents->fact_email_sent != 0) { ?>
                                    <span class="btn btn-sm btn-outline-history rounded-pill" style="margin-left: 2px;" data-id="<?= $documents->doc_loyer_id ?>" data-fact_id="<?= $documents->fact_loyer_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Historique Mail" id="modalHistoryDocumentFacture">
                                        <i class="fas fa-history"></i>
                                    </span>
                                <?php } ?>


                                <!-- No facture but simple file -->
                                <?php if (!isset($documents->fact_loyer_num)) { ?>
                                    <span class="btn btn-sm btn-outline-danger rounded-pill" id="deleteDocLoyer" data-doc_loyer_id="<?= $documents->doc_loyer_id ?>">
                                        <i class="far fa-trash-alt"></i>
                                    </span>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <hr class="my-0"><br>

        <!-- tableau de condition du bail if valide ou if il y a facture -->
        <div class="tableau" id="tableau_detail_bail">

            <div class="detail_bail">
                <?php if (isset($bail) && !empty($bail)) : ?>
                    <div class="row">
                        <div class="col-4 fw-bold">
                            <u>
                                <span style="font-size: 15px;">Consultation des factures et encaissements</span style="font-size: 95%;">
                            </u>
                        </div>
                        <div class="col-4 text-center">
                            <select class="no_effect form-select" aria-label="Default select example" id="bail_loyer" style="margin-left: 33px;">
                                <?php $countbail  = !empty($bail) ? count($bail) : 0; ?>
                                <?php foreach ($bail as  $key => $bails) : ?>
                                    <option value="<?= $bails->bail_id ?>" <?= $key == ($countbail - 1) ? 'selected' : '' ?> data-pdl_id="<?= $bails->pdl_id ?>" data-bail_loyer_variable="<?= $bails->bail_loyer_variable ?>">Bail du&nbsp;<?= formatDateFr($bails->bail_debut) ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-4 text-end">
                            <div class="form-group inline">
                                <!-- 
                                    <button class="only_visuel_gestion btn btn-sm btn-primary <?= $bails->bail_cloture == 1 ? 'd-none' : '' ?>" data-bs-toggle="modal" data-bs-target="#modal_encaissement" id="btnAddNewEncaissement">
                                        <i class="fas fa-plus me-1"></i>Encaissement
                                    </button> 
                                -->
                                <button class="only_visuel_gestion btn btn-sm btn-primary btnAddFactureLoyer" id="btnAddFactureLoyer" data-bs-toggle="modal" data-bs-target="#modal_facture">
                                    <i class="fas fa-plus me-1"></i> Facture
                                </button>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div id="container-table-facture_encaissement">

                    </div>

                <?php else : ?>
                    <div class="row">
                        <div class="col-12 text-center only_visuel_gestion">
                            <div class="alert alert-secondary text-center" role="alert">
                                <div class="text-center">
                                    Création de facture du loyer impossible car aucun bail n'a été crée&nbsp;. <br>
                                    Veuillez créer un bail dans le sous menu Bail !
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <br>
        </div>
    </div>
    <div class="split m-2">

    </div>
    <div class="col-6 panel-right">
        <div id="apercu_docLoyer">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercuLoyer">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="contain_btn_history text-start w-100">
        <button class="btn btn-primary py-2 px-4 btn-sm" id="btn_loyer_historique">
            Voir historique du loyer
        </button>
        <div id="contain_loyer_historique">
        </div>
    </div> -->
</div>


<!-- Modal detail encaissement -->
<div class="modal fade" id="modal_detail_encaissement" data-bs-backdrop="static" role="dialog" tabindex="-1" aria-labelledby="modal_detail_encaissement" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Détail des encaissements</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="contain-list-detail-encaissement">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal detail facture -->
<div class="modal fade" id="modal_detail_facture" data-bs-backdrop="static" role="dialog" tabindex="-1" aria-labelledby="modal_detail_facture" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Détail des factures</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="contain-list-detail-facture">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<?php if (isset($bail) && !empty($bail)) :
    foreach ($bail as $key => $bails) :
        if ($key == ($countbail - 1)) :
            $first_bail = $bails->bail_id;
            $pdl_id = $bails->pdl_id;
            $bail_loyer_variable = $bails->bail_loyer_variable;
?>
            <script>
                getTableFacture(<?= $first_bail ?>, <?= $pdl_id ?>, <?= $bail_loyer_variable ?>, $('#annee_loyer').val(), $('#cli_lot_id').val());
            </script>
<?php break;
        endif;
    endforeach;
endif;
?>

<script>
    $(".panel-left").resizable({
        handleSelector: ".split",
        resizeHeight: false,
    });
    only_visuel_gestion();
    <?php if (isset($bail) && !empty($bail)) : ?>
        verifyLoyerValide($('#annee_loyer').val(), $("#cli_lot_id").val());
    <?php endif ?>
</script>