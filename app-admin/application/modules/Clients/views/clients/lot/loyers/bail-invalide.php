<div class="d-flex justify-content-center align-items-center flex-column contain_bail_invalide_info">
    <div class="text-center w-50" style="white-space: normal;">
        Attention, les informations du loyer pour le lot concerné ne sont pas disponibles
        car le bail n'a pas été validé par un administrateur.
        Vous pouvez demander la validation du bail en cliquant sur le bouton ci-dessous :
    </div>

    <?php if ($check_siret_tva->dossier_siret == NULL || ($bail[$nombreBail - 1]->tva == 'REEL SIMPLIFIE' && $check_siret_tva->dossier_tva_intracommunautaire == NULL)) {
        $button_demande_class_loyer = 'alert_tva_siret_loyer';
    } elseif ($check_siret_tva->dossier_siret == NULL || ($bail[$nombreBail - 1]->tva == 'REEL NORMAL' && $check_siret_tva->dossier_tva_intracommunautaire == NULL)) {
        $button_demande_class_loyer = 'alert_tva_siret_loyer';
    } else {
        $button_demande_class_loyer = 'ask_bail_validation';
    } ?>

    <div class="button_valid_bail mt-2">
        <button class="btn btn-primary" id="<?= $button_demande_class_loyer ?>" data-bail_id="<?= $bail[0]->bail_id ?>" data-cli_lot_id="<?= $cliLot_id ?>">
            Demander la validation du bail par mail
        </button>
    </div>

    <div id="message_error_mail_bail" class="text-center w-50" style="color: red;white-space: normal;"></div>

    <div id="message_success_mail_bail" class="text-center w-50" style="color: #2c7be5;white-space: normal;"></div>

    <div id="message_information_mail_bail" class="text-center w-50" style="color: #2c7be5;white-space: normal;"></div>

    <input type="hidden" id="mail_bail_sent" value="0" />
    <input type="hidden" id="bail_id_sent" value="<?= $bail[0]->bail_id ?>" />

    <!-- <div class="instruct_valid_bail">
        Veuillez valider dans l'onglet "Bail"
    </div> -->
</div>

<!-- Modal confirmation -->
<!-- Modal confirm ajout 2eme indexation -->
<div class="modal fade" id="resend_modal_validation_bail_mail" tabindex="-1" aria-labelledby="resend_modal_validation_bail_mail" aria-hidden="true" data-bs-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header header-confirm-indexation">
                Confirmation de renvoi d'email
            </div>
            <div class="modal-body body-confirm-indexation">
                Voulez-vous renvoyer le mail de demande de validation aux administrateurs ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="cancelNewIndexationLoyer">
                    Annuler la validation du bail
                </button>
                <button type="button" class="btn btn-primary" id="confirmResendModalValidationBailMail">Réenvoyer</button>
            </div>
        </div>
    </div>
</div>

<script>
    // get if mail envoyé + detail
    $(document).ready(() => {
        getHistorique(6, 1, $("#bail_id_sent").val(), "", "true", "verifyEmailBailValidationSent");
    })
</script>

<style>
    .contain_bail_invalide_info {
        margin: 10px;
        background: whitesmoke;
        padding: 20px;
        line-height: 30px;
    }

    .contain_bail_invalide_info div:first-child {
        font-weight: 500;
    }
</style>