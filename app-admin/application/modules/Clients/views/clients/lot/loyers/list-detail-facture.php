<div>
    <input type="hidden" value="<?= $bail->bail_id ?>" id="detail_facture_bail_id" />
    <input type="hidden" value="<?= $bail->pdl_id ?>" id="detail_facture_pdl_id" />
    <input type="hidden" value="<?= $bail->bail_loyer_variable ?>" id="detail_facture_bail_loyer_variable" />
    <input type="hidden" value="<?= $requests["facture_annee"] ?>" id="detail_facture_enc_annee" />
    <input type="hidden" value="<?= $requests["periode"] ?>" id="detail_facture_periode" />
    <input type="hidden" value="<?= $requests["periode_id"] ?>" id="detail_facture_periode_id" />
    <input type="hidden" value="<?= $requests["cli_lot_id"] ?>" id="cli_lot_id" />

    <?php if (isset($factures) && count($factures) > 0) { ?>
        <table class="table table-hover fs--1">
            <thead class="text-white">
                <tr class="text-center">
                    <th class="p-2 contain_thead">Numéro</th>
                    <th class="p-2 contain_thead">Date</th>
                    <th class="p-2 contain_thead">Appartement HT</th>
                    <th class="p-2 contain_thead">Parking HT</th>
                    <th class="p-2 contain_thead">Forfait charges HT</th>
                    <th class="p-2 contain_thead">Total TVA</th>
                    <th class="p-2 contain_thead">Total TTC</th>
                    <th class="p-2 contain_thead">Action</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php foreach ($factures as $keyFacture => $facture) : ?>
                    <tr class="text-center">
                        <td class="text-center num-facture">
                            <?php echo $facture->fact_loyer_num ?>
                        </td>
                        <td class="text-center">
                            <?= date("d/m/Y", strtotime(str_replace('/', '-', $facture->fact_loyer_date))); ?>
                        </td>
                        <td class="text-end">
                            <?= format_number_($facture->fact_loyer_app_ht) ?>€
                        </td>
                        <td class="text-end">
                            <?= format_number_($facture->fact_loyer_park_ht) ?>€
                        </td>
                        <td class="text-end">
                            <?= format_number_($facture->fact_loyer_charge_ht) ?>€
                        </td>
                        <td class="text-end">
                            <?php
                            $totalTVA = 0;
                            $appTVA = $facture->fact_loyer_app_ht * $facture->fact_loyer_app_tva / 100;
                            $parkTVA = $facture->fact_loyer_park_ht * $facture->fact_loyer_park_tva / 100;
                            $chargeTVA = $facture->fact_loyer_charge_ht * $facture->fact_loyer_charge_tva / 100;
                            $totalTVA = $appTVA + $parkTVA - $chargeTVA;
                            ?>
                            <?= format_number_($totalTVA) ?>€
                        </td>
                        <td class="text-end">
                            <?php
                            $totalTTC = 0;
                            $totalTTC = $facture->fact_loyer_park_ttc + $facture->fact_loyer_app_ttc - $facture->fact_loyer_charge_ttc;
                            ?>
                            <?= format_number_($totalTTC) ?>€
                        </td>
                        <td>
                            <?php if ($facture->fact_email_sent == 0) { ?>
                                <div class="action-detail-facture">
                                    <span class="btn btn-sm btn-outline-primary btn_edit_facture" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Modifier" data-id="<?= $keyFacture ?>" data-fact_loyer_id="<?= $facture->fact_loyer_id ?>" data-fact_loyer_num="<?= $facture->fact_loyer_num ?>" data-fact_loyer_date="<?= $facture->fact_loyer_date ?>" data-fact_loyer_app_ht="<?= $facture->fact_loyer_app_ht ?>" data-fact_loyer_app_tva="<?= $facture->fact_loyer_app_tva ?>" data-fact_loyer_app_ttc="<?= $facture->fact_loyer_app_ttc ?>" data-fact_loyer_park_ht="<?= $facture->fact_loyer_park_ht ?>" data-fact_loyer_park_tva="<?= $facture->fact_loyer_park_tva ?>" data-fact_loyer_park_ttc="<?= $facture->fact_loyer_park_ttc ?>" data-fact_loyer_charge_ht="<?= $facture->fact_loyer_charge_ht ?>" data-fact_loyer_charge_tva="<?= $facture->fact_loyer_charge_tva ?>" data-fact_loyer_charge_ttc="<?= $facture->fact_loyer_charge_ttc ?>" data-bail_id="<?= $facture->bail_id ?>" data-periode_semestre_id="<?= $facture->periode_semestre_id ?>" data-periode_timestre_id="<?= $facture->periode_timestre_id ?>" data-periode_annee_id="<?= $facture->periode_annee_id ?>" data-periode_mens_id="<?= $facture->periode_mens_id ?>" data-facture_nombre="<?= $facture->facture_nombre ?>" data-facture_annee="<?= $facture->facture_annee ?>" data-dossier_id="<?= $facture->dossier_id ?>" data-mode_paiement="<?= $facture->mode_paiement ?>" data-facture_commentaire="<?php echo htmlentities($facture->facture_commentaire, ENT_QUOTES); ?>" data-echeance_paiement="<?= $facture->echeance_paiement ?>" data-emetteur_nom="<?= $facture->emetteur_nom ?>" data-emet_adress1="<?= $facture->emet_adress1 ?>" data-emet_adress2="<?= $facture->emet_adress2 ?>" data-emet_adress3="<?= $facture->emet_adress3 ?>" data-emet_cp="<?= $facture->emet_cp ?>" data-emet_pays="<?= $facture->emet_pays ?>" data-emet_ville="<?= $facture->emet_ville ?>" data-dest_nom="<?= $facture->dest_nom ?>" data-dest_adresse1="<?= $facture->dest_adresse1 ?>" data-dest_adresse2="<?= $facture->dest_adresse2 ?>" data-dest_adresse3="<?= $facture->dest_adresse3 ?>" data-dest_cp="<?= $facture->dest_cp ?>" data-dest_pays="<?= $facture->dest_pays ?>" data-dest_ville="<?= $facture->dest_ville ?>">
                                        <i class="fas fa-pencil-alt"></i>
                                    </span>&nbsp;
                                    <span class="only_visuel_gestion btn btn-sm btn-outline-danger btn_delete_facture" data-fact_id="<?= $facture->fact_loyer_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                        <i class="far fa-trash-alt"></i>
                                    </span>
                                </div>
                            <?php } else { ?>
                                <div class="action-detail-facture only_visuel_gestion">
                                    <span class="btn btn-sm btn-outline-primary btn_edit_facture" data-fact_avoir="1" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Modifier" data-id="<?= $keyFacture ?>" data-fact_loyer_id="<?= $facture->fact_loyer_id ?>" data-fact_loyer_num="<?= $facture->fact_loyer_num ?>" data-fact_loyer_date="<?= $facture->fact_loyer_date ?>" data-fact_loyer_app_ht="<?= $facture->fact_loyer_app_ht ?>" data-fact_loyer_app_tva="<?= $facture->fact_loyer_app_tva ?>" data-fact_loyer_app_ttc="<?= $facture->fact_loyer_app_ttc ?>" data-fact_loyer_park_ht="<?= $facture->fact_loyer_park_ht ?>" data-fact_loyer_park_tva="<?= $facture->fact_loyer_park_tva ?>" data-fact_loyer_park_ttc="<?= $facture->fact_loyer_park_ttc ?>" data-fact_loyer_charge_ht="<?= $facture->fact_loyer_charge_ht ?>" data-fact_loyer_charge_tva="<?= $facture->fact_loyer_charge_tva ?>" data-fact_loyer_charge_ttc="<?= $facture->fact_loyer_charge_ttc ?>" data-bail_id="<?= $facture->bail_id ?>" data-periode_semestre_id="<?= $facture->periode_semestre_id ?>" data-periode_timestre_id="<?= $facture->periode_timestre_id ?>" data-periode_annee_id="<?= $facture->periode_annee_id ?>" data-periode_mens_id="<?= $facture->periode_mens_id ?>" data-facture_nombre="<?= $facture->facture_nombre ?>" data-facture_annee="<?= $facture->facture_annee ?>" data-dossier_id="<?= $facture->dossier_id ?>" data-mode_paiement="<?= $facture->mode_paiement ?>" data-facture_commentaire="<?php echo htmlentities($facture->facture_commentaire, ENT_QUOTES); ?>" data-echeance_paiement="<?= $facture->echeance_paiement ?>" data-emetteur_nom="<?= $facture->emetteur_nom ?>" data-emet_adress1="<?= $facture->emet_adress1 ?>" data-emet_adress2="<?= $facture->emet_adress2 ?>" data-emet_adress3="<?= $facture->emet_adress3 ?>" data-emet_cp="<?= $facture->emet_cp ?>" data-emet_pays="<?= $facture->emet_pays ?>" data-emet_ville="<?= $facture->emet_ville ?>" data-dest_nom="<?= $facture->dest_nom ?>" data-dest_adresse1="<?= $facture->dest_adresse1 ?>" data-dest_adresse2="<?= $facture->dest_adresse2 ?>" data-dest_adresse3="<?= $facture->dest_adresse3 ?>" data-dest_cp="<?= $facture->dest_cp ?>" data-dest_pays="<?= $facture->dest_pays ?>" data-dest_ville="<?= $facture->dest_ville ?>">
                                        avoir
                                    </span>&nbsp;
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php } else { ?>
        <div class="mt-2 mb-2 text-center">
            Aucune facture
        </div>
    <?php } ?>
</div>

<style>
    .contain_thead {
        border-right: 1px solid white !important;
        background: #2569C3 !important;
    }

    .action-detail-facture {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .num-facture {
        white-space: nowrap;
    }
</style>

<script>
    only_visuel_gestion();

    $(".btn_delete_facture").bind("click", function(event) {
        const fact_id = $(this).attr("data-fact_id");
        localStorage.setItem("deletedFacture", "true"); // To know that document list will reload on close
        deleteFacture(fact_id);
    });

    $(".btn_edit_facture").bind("click", function(event) {
        localStorage.setItem("deletedFacture", "false");
        $("#modal_detail_facture").hide();
        $('.modal-backdrop').remove();

        $("#btnAddFactureLoyer").trigger("click");

        const id = $(this).attr("data-id");
        const fact_avoir = $(this).attr("data-fact_avoir");
        const fact_loyer_id = $(this).attr("data-fact_loyer_id");
        const fact_loyer_num = $(this).attr("data-fact_loyer_num");
        const fact_loyer_date = $(this).attr("data-fact_loyer_date");
        const fact_loyer_app_ht = $(this).attr("data-fact_loyer_app_ht");
        const fact_loyer_app_tva = $(this).attr("data-fact_loyer_app_tva");
        const fact_loyer_app_ttc = $(this).attr("data-fact_loyer_app_ttc");
        const fact_loyer_park_ht = $(this).attr("data-fact_loyer_park_ht");
        const fact_loyer_park_tva = $(this).attr("data-fact_loyer_park_tva");
        const fact_loyer_park_ttc = $(this).attr("data-fact_loyer_park_ttc");
        const fact_loyer_charge_ht = $(this).attr("data-fact_loyer_charge_ht");
        const fact_loyer_charge_tva = $(this).attr("data-fact_loyer_charge_tva");
        const fact_loyer_charge_ttc = $(this).attr("data-fact_loyer_charge_ttc");
        const bail_id = $(this).attr("data-bail_id");
        const periode_semestre_id = $(this).attr("data-periode_semestre_id");
        const periode_timestre_id = $(this).attr("data-periode_timestre_id");
        const periode_annee_id = $(this).attr("data-periode_annee_id");
        const periode_mens_id = $(this).attr("data-periode_mens_id");
        const facture_nombre = $(this).attr("data-facture_nombre");
        const facture_annee = $(this).attr("data-facture_annee");
        const dossier_id = $(this).attr("data-dossier_id");
        const mode_paiement = $(this).attr("data-mode_paiement");
        const facture_commentaire = $(this).attr("data-facture_commentaire");
        const echeance_paiement = $(this).attr("data-echeance_paiement");
        const emetteur_nom = $(this).attr("data-emetteur_nom");
        const emet_adress1 = $(this).attr("data-emet_adress1");
        const emet_adress2 = $(this).attr("data-emet_adress2");
        const emet_adress3 = $(this).attr("data-emet_adress3");
        const emet_cp = $(this).attr("data-emet_cp");
        const emet_pays = $(this).attr("data-emet_pays");
        const emet_ville = $(this).attr("data-emet_ville");
        const dest_nom = $(this).attr("data-dest_nom");
        const dest_adresse1 = $(this).attr("data-dest_adresse1");
        const dest_adresse2 = $(this).attr("data-dest_adresse2");
        const dest_adresse3 = $(this).attr("data-dest_adresse3");
        const dest_cp = $(this).attr("data-dest_cp");
        const dest_pays = $(this).attr("data-dest_pays");
        const dest_ville = $(this).attr("data-dest_ville");

        const fact_loyer_app_tva_pource = fact_loyer_app_ht * fact_loyer_app_tva / 100;
        const fact_loyer_park_tva_pource = fact_loyer_park_ht * fact_loyer_park_tva / 100;
        const fact_loyer_charge_tva_pource = fact_loyer_charge_ht * fact_loyer_charge_tva / 100;

        $(".mode_paiement_label").val(mode_paiement);

        $("#fact_avoir").val(fact_avoir);

        $("#fact_id").val(fact_loyer_id);
        $("#fact_loyer_date").val(fact_loyer_date);
        $("#fact_loyer_num").val(fact_loyer_num);

        $("#fact_loyer_app_ht").val(fact_loyer_app_ht);
        $("#fact_loyer_park_ht").val(fact_loyer_park_ht);
        $("#fact_loyer_charge_ht").val(fact_loyer_charge_ht);

        $("#fact_loyer_app_tva").val(fact_loyer_app_tva);
        $("#fact_loyer_app_tva_pource").val(fact_loyer_app_tva_pource);

        $("#fact_loyer_park_tva").val(fact_loyer_park_tva);
        $("#fact_loyer_park_tva_pource").val(fact_loyer_park_tva_pource);

        $("#fact_loyer_charge_tva").val(fact_loyer_charge_tva);
        $("#fact_loyer_charge_tva_pource").val(fact_loyer_charge_tva_pource);

        $("#fact_loyer_app_ttc").val(fact_loyer_app_ttc);
        $("#fact_loyer_park_ttc").val(fact_loyer_park_ttc);
        $("#fact_loyer_charge_ttc").val(fact_loyer_charge_ttc);

        $("#facture_commentaire").val(facture_commentaire);

        let htApp = fact_loyer_app_ht;
        let htPark = fact_loyer_park_ht;
        let htCharge = fact_loyer_charge_ht;
        let totalHT = (+htApp) + (+htPark) - (+htCharge);
        totalHT = totalHT.toFixed(2);

        let tvaApp = fact_loyer_app_tva_pource;
        let tvaPark = fact_loyer_park_tva_pource;
        let tvaCharge = fact_loyer_charge_tva_pource;
        let totalTVA = (+tvaApp) + (+tvaPark) - (+tvaCharge);
        totalTVA = totalTVA.toFixed(2);

        let ttcApp = fact_loyer_app_ttc;
        let ttcPark = fact_loyer_park_ttc;
        let ttcCharge = fact_loyer_charge_ttc;
        let totalTTC = (+ttcApp) + (+ttcPark) - (+ttcCharge);
        totalTTC = totalTTC.toFixed(2);

        $("#total_loyer_ht").text(`${totalHT} €`);
        $("#total_loyer_tva").text(`${totalTVA} €`);
        $("#total_loyer_ttc").text(`${totalTTC} €`);

        $(".mode_paiement").text(mode_paiement);
        $(".mode_paiement_label").val(mode_paiement);

        $(".echeance_paiement").text(echeance_paiement);
        $(".echeance_paiement_label").val(echeance_paiement);

        $(".echeance_paiement_label").val(echeance_paiement);
        $(".echeance_paiement_label").val(echeance_paiement);

        $("#periode_semestre_id").val(periode_semestre_id);
        $("#periode_timestre_id").val(periode_timestre_id);
        $("#periode_annee_id").val(periode_annee_id);
        $("#periode_mens_id").val(periode_mens_id);

        $("#emetteur_nom").val(emetteur_nom);
        $("#span_emetteur_nom").val(emetteur_nom);
        $("#emet_cp").val(emet_cp);
        $("#span_emet_cp").val(emet_cp);
        $("#emet_adress1").val(emet_adress1);
        $("#span_emet_adress1").val(emet_adress1);
        $("#emet_adress2").val(emet_adress2);
        $("#span_emet_adress2").val(emet_adress2);
        $("#emet_adress3").val(emet_adress3);
        $("#span_emet_adress3").val(emet_adress3);
        $("#emet_pays").val(emet_pays);
        $("#span_emet_pays").val(emet_pays);
        $("#emet_ville").val(emet_ville);
        $("#span_emet_ville").val(emet_ville);

        $("#dest_nom").val(dest_nom);
        $("#span_dest_nom").val(dest_nom);
        $("#dest_adresse1").val(dest_adresse1);
        $("#span_dest_adresse1").val(dest_adresse1);
        $("#dest_adresse2").val(dest_adresse2);
        $("#span_dest_adresse2").val(dest_adresse2);
        $("#dest_adresse3").val(dest_adresse3);
        $("#span_dest_adresse3").val(dest_adresse3);
        $("#dest_cp").val(dest_cp);
        $("#span_dest_cp").val(dest_cp);
        $("#dest_pays").val(dest_pays);
        $("#span_dest_pays").val(dest_pays);
        $("#dest_ville").val(dest_ville);
        $("#span_dest_ville").val(dest_ville);

        if (fact_avoir && fact_avoir == 1) {
            // Creation d'un avoir
            $("#title_modal_facture").text("Création d'un avoir par CELAVI Gestion");

            // Deactivate fields
            $("#periode_semestre_id").prop("disabled", true);
            $("#periode_timestre_id").prop("disabled", true);
            $("#periode_annee_id").prop("disabled", true);
            $("#periode_mens_id").prop("disabled", true);
            $("#fact_loyer_app_ht").prop("disabled", true);
            $("#fact_loyer_park_ht").prop("disabled", true);
            $("#fact_loyer_charge_ht").prop("disabled", true);

            $('#fact_loyer_app_tva').prop("disabled", true);
            $('#fact_loyer_park_tva').prop("disabled", true);
            $('#fact_loyer_charge_tva').prop("disabled", true);

            // Inverse
            $(".prepend-avoir").removeClass("d-none");
            $(".inverse-avoir").text("+");


            if (totalHT > 0) $("#total_loyer_ht").text(`- ${totalHT} €`);
            if (totalTVA > 0) $("#total_loyer_tva").text(`- ${totalTVA} €`);
            if (totalTTC > 0) $("#total_loyer_ttc").text(`- ${totalTTC} €`);
        } else {
            $("#title_modal_facture").text("Modification d'une facture par CELAVI Gestion");
        }
    });


    $('#modal_detail_facture').on('hidden.bs.modal', function() {
        if (localStorage.getItem("deletedFacture") === "true") {
            const enc_annee = $("#detail_facture_enc_annee").val();
            const cli_lot_id = $("#cli_lot_id").val();
            getLoyers(enc_annee, cli_lot_id);
        }
        localStorage.setItem("deletedFacture", "false");
    });

    $('#modal_facture').on('hidden.bs.modal', function() {
        $("#title_modal_facture").text("Création d'une facture par CELAVI Gestion");
        $(".prepend-avoir").addClass("d-none");
        $(".inverse-avoir").text("-");
        $("#fact_avoir").val("");
    });
</script>