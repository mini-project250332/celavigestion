<div class="row ms-1 mt-3 me-1">
	<div class="col">
		<h5 class="fs-1">Fiche Lot : <?php echo $lot->progm_nom ?>
			(Propriétaire : <?= $client->client_nom . ' ' . $client->client_prenom; ?>)
		</h5>
	</div>
	<div class="col d-flex align-items-center justify-content-end">
		<button class="btn btn-sm btn-outline-primary btnSaisieTempsLot ms-1 me-1" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" style="white-space: nowrap;">
			<i class="fas fa-clock"></i>
		</button>

		<div class="dropdown">
			<!-- Icon Button -->
			<button class="btn btn-sm btn-secondary dropdown-toggle ms-1 me-1" id="dropDownBailOption" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 29px;">
				<?php foreach ($listLot as $list) :
					if ($list->cliLot_id == $lot->cliLot_id) : ?>
						<?= $list->progm_nom ?> (<?= $list->cliLot_id ?>)
				<?php endif;
				endforeach  ?>
			</button>
			<!-- Dropdown Menu -->
			<?php $compte = count($listLot); ?>
			<?php if ($compte  > 1) : ?>
				<div class="dropdown-menu" aria-labelledby="dropDownBailOption">
					<?php foreach ($listLot as $list) : if ($list->cliLot_id != $lot->cliLot_id) : ?>
							<button class="dropdown-item btn btn-sm btn getFicheLot" id="getFicheLot" data-client_id="<?= $client_id ?>" data-cliLot_id="<?= $list->cliLot_id ?>">
								<?= $list->progm_nom ?> (<?= $list->cliLot_id ?>)
							</button>
					<?php endif;
					endforeach  ?>
				</div>
			<?php endif ?>
		</div>

		<button id="" data-id="<?= $client_id ?>" class="btn btn-sm btn-secondary ms-1 me-1 retourClient" style="white-space: nowrap;">
			<i class="fas fa-undo me-1"></i>
			<span>Fiche propriétaire</span>
		</button>
		<button id="" data-dossier_id="<?= $lot->dossier_id ?>" data-client_id="<?= $client_id ?>" class="btn btn-sm btn-secondary ms-1 me-1 retourDossier" style="white-space: nowrap;">
			<i class="fas fa-undo me-1"></i>
			<span>Fiche dossier</span>
		</button>
	</div>
</div>

<hr class="mb-0">

<div class="contenair-content">

	<ul class="nav nav-tabs" id="lot_Tab_list" role="tablist">

		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="identification">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification_lot" role="tab" aria-controls="tab-identification" aria-selected="true">
				Identification
			</a>
		</li>

		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="mandat">
			<a class="nav-link" id="taxe-mandat" data-bs-toggle="tab" href="#tab-mandat" role="tab" aria-controls="tab-mandat" aria-selected="false">
				Mandat
			</a>
		</li>

		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="pno">
			<a class="nav-link" id="taxe-pno" data-bs-toggle="tab" href="#tab-pno" role="tab" aria-controls="tab-pno" aria-selected="false">
				PNO
			</a>
		</li>

		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="acte-acquisition">
			<a class="nav-link" id="acte-tab" data-bs-toggle="tab" href="#tab-acte" role="tab" aria-controls="tab-acte" aria-selected="false">
				Acte/Acquisition
			</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="bail">
			<a class="nav-link" id="bail-tab" data-bs-toggle="tab" href="#tab-bail" role="tab" aria-controls="tab-bail" aria-selected="false">
				Bail
			</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="loyers">
			<a class="nav-link" id="loyer-tab" data-bs-toggle="tab" href="#tab-loyer" role="tab" aria-controls="tab-loyer" aria-selected="false">
				Loyers
			</a>
		</li>

		<!--- --->
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="encaissements">
			<a class="nav-link" id="encaissement-tab" data-bs-toggle="tab" href="#tab-encaissement" role="tab" aria-controls="tab-loyer" aria-selected="false">
				Encaissements
			</a>
		</li>
		<!--- --->

		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="taxe-fonciere">
			<a class="nav-link" id="taxe-tab" data-bs-toggle="tab" href="#tab-taxe" role="tab" aria-controls="tab-taxe" aria-selected="false">
				Taxe foncière
			</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="depense">
			<a class="nav-link" id="charges-tab" data-bs-toggle="tab" href="#tab-charges" role="tab" aria-controls="tab-charges" aria-selected="false">
				Dépenses
			</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="emprunts">
			<a class="nav-link" id="pret-tab" data-bs-toggle="tab" href="#tab-pret" role="tab" aria-controls="tab-pret" aria-selected="false">Emprunts</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="immobilisation">
			<a class="nav-link" id="immo-tab" data-bs-toggle="tab" href="#tab-immo" role="tab" aria-controls="tab-immo" aria-selected="false">
				Immobilisation
			</a>
		</li>
		<li class="nav-item charge-page-onglet" data-id="<?= $lot->cliLot_id ?>" data-client="<?= $client_id ?>" data-onglet="copropriete">
			<a class="nav-link" id="copropriete-tab" data-bs-toggle="tab" href="#tab-copropriete" role="tab" aria-controls="tab-copropriete" aria-selected="false">
				Copropriété
			</a>
		</li>
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="lotTabContent">
		<div class="tab-pane tab_lot fade show active" id="tab-identification_lot" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification_lot/identification'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-mandat" role="tabpanel" aria-labelledby="mandat-tab">
			<?php $this->load->view('mandats/mandat'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-pno" role="tabpanel" aria-labelledby="pno-tab">
			<?php $this->load->view('pno/pno'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-acte" role="tabpanel" aria-labelledby="acte-tab">
			<?php $this->load->view('actes/acte'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-bail" role="tabpanel" aria-labelledby="bail-tab">
			<?php $this->load->view('bails/bail'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-loyer" role="tabpanel" aria-labelledby="loyer-tab">
			<?php $this->load->view('loyers/loyer'); ?>
		</div>
		<!---- ---->
		<div class="tab-pane tab_lot fade" id="tab-encaissement" role="tabpanel" aria-labelledby="encaissement-tab">
			<?php $this->load->view('encaissements/encaissements'); ?>
		</div>
		<!---- ----->
		<div class="tab-pane tab_lot 0fade" id="tab-taxe" role="tabpanel" aria-labelledby="taxe-tab">
			<?php $this->load->view('taxes/taxe'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-charges" role="tabpanel" aria-labelledby="charges-tab">
			<?php $this->load->view('charges/charges'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-pret" role="tabpanel" aria-labelledby="pret-tab">
			<?php $this->load->view('prets/pret'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-immo" role="tabpanel" aria-labelledby="immo-tab">
			<?php $this->load->view('immo/immo'); ?>
		</div>
		<div class="tab-pane tab_lot fade" id="tab-copropriete" role="tabpanel" aria-labelledby="copropriete-tab">
			<?php $this->load->view('copropriete/copropriete'); ?>
		</div>
	</div>
</div>

</div>

<script type="text/javascript">

	$(document).on('click', '.charge-page-onglet', function (e) {
	    e.preventDefault();

	    if($(this).attr('data-onglet')==="identification"){
	    	getFicheIdentificationLot($(this).attr('data-id'), 'fiche');
	    }

	    if($(this).attr('data-onglet')==="mandat"){
	    	getLotMandat($(this).attr('data-id'),$(this).attr('data-client'));
	    }

	    if($(this).attr('data-onglet')==="pno"){
	    	getLotPno($(this).attr('data-id'),$(this).attr('data-client'));
	    }

	    if($(this).attr('data-onglet')==="acte-acquisition"){
	    	getDocActe($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="bail"){
	    	getDocBails($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="loyers"){
	    	getDocLoyers($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="encaissements"){
	    	getListeEncaissement_info($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="taxe-fonciere"){
	    	getDoctaxe($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="depense"){
	    	getMenuCharges($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="emprunts"){
	    	getDocPret($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="immobilisation"){
	    	getActeImmo($(this).attr('data-id'));
	    	getImmoGeneral($(this).attr('data-id'));
	    }

	    if($(this).attr('data-onglet')==="copropriete"){
	    	getDocCopropriete($(this).attr('data-id'));
	    }

	});

	only_visuel_gestion();
</script>