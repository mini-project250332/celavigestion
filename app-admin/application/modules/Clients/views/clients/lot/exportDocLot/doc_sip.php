<style type="text/css">
    @page {
        margin: 3%;
    }

    img {
        width: 35%;
    }

    .column {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
        font-size: 13px;
    }

    .left {
        width: 50%;
    }

    .right {
        width: 45%;
        margin-left: 5%;
    }

    .name{
        left: 10% !important;
    }

    .cp{
        left: 15% !important;
    }

    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    .text-objet {
        font-weight: bold;
    }

    .signature {
        font-family: Arial, Helvetica, sans-serif;
        margin-left: 65%;
    }
    .footer{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 11px;
        text-align: center;
        
    }
</style>
<div class="row">
    <div class="column left">
        <img src="<?= base_url('../assets/img/logo.jpeg') ?>" alt="">
    </div>
    <div class="column right">
        <br><br><br><br><br>
        <?php
            $sieName = explode(" ", $sip->sie_libelle);
            $nomsie = $sieName[1];
        ?>
        <span>Service des Impôts des Particuliers de <?=$nomsie?></span> <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=$sip->sie_adresse1.' '.$sip->sie_adresse2.' '.$sip->sie_adresse3?></span><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span ><?=$sip->sie_cp.' '.$sip->sie_ville?></span><br><br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>SAINT-NAZAIRE, le <?= date("d/m/Y", strtotime(str_replace('/', '-', $sip->doc_sip_date_creation))); ?></span>
    </div>
</div>
<div class="column">
    <div class="text-objet">
        <br><br><br>
        <span>LRAR</span> <br><br>
        <span>Objet : Mandat d'administration de biens</span><br>
        <span>PJ : copie du mandat de gérance</span>
    </div> <br><br>
    <div class="text-corp">
        <span>Madame,Monsieur</span> <br><br>
        <span>
            Je me permets de vous informer que la société CELAVIGESTION est détentrice d'un mandat d'administration de bien pour le compte de Monsieur <?=$sip->client_nom.' '.$sip->client_prenom?>,
           dont le numéro Siret est le <?=$sip->dossier_siret?> et qui est propriétaire à <?=$sip->client_adresse1.' '.$sip->client_adresse2.' '.$sip->client_adresse3?>.
        </span> <br><br>
        <span>
            Par la présente et de par mon statut de mandataire, je vous saurais gré de me faire parvenir toutes correspondances en lien avec le bien de mes clients ainsi que sa taxe foncière annuelle.
        </span> <br><br>       
            Vous pouvez nous faire parvenir l'ensemble de ces documents à l'adresse e-mail suivante : <i>clementine@celavigestion.fr</i> et/ou par courrier postal à l'adresse
            suivante :
        </span><br><br>
        <span>
            CELAVI GESTION <br>
            39 Route de Fondeline, Parc de Brais <br>
            44 600 SAINT-NAZAIRE <br>
        </span><br><br>
        <span>Dans l'attente d'une confirmation de votre part, je vous prie d'agréer, Madame, Monsieur, l'expression de mes salutations les meilleures.</span>
    </div>
    <div class="signature">
        <br><br><br>
        <span>Clémentine ROUSSELOT</span><br>
        <span>Chargée de Clientèle</span>
    </div><br><br><br><br><br>
</div>
<div class="footer">
    <span class="text-objet">CELAVIGESTION</span>
    <span>
        SAS au capital de 100.000 €, Siret : 477 782 346 00027 RCS Saint-Nazaire , Ape 683IZ, Numéro TVA intracommunautaire FR69477782346. Adresse : Parc de Brais, 39 route
        de Fondeline, 44600 Saint-Nazaire, France. Carte professionnelle transaction et gestion N°CPI 4402 2018 000 035 231 délivrée par la CCI Nantes-Saint Nazaire, garantie
        gestion à hauteur de 650.000€ souscrites auprès de GALIAN, 89 rue de la Boétie 75008 PARIS
    </span>
</div>