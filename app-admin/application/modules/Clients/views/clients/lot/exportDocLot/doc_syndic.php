<style type="text/css">
    @page {
        margin: 3%;
    }

    img {
        width: 35%;
    }

    .column {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
        font-size: 13px;
    }

    .left {
        width: 60%;
    }

    .right {
        width: 30%;
        margin-left: 5%;
    }

    .name{
        left: 10% !important;
    }

    .cp{
        left: 15% !important;
    }

    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    .text-objet {
        font-weight: bold;
    }

    .signature {
        font-family: Arial, Helvetica, sans-serif;
        margin-left: 65%;
    }
    .footer{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 11px;
        text-align: center;
        
    }
</style>
<div class="row">
    <div class="column left">
        <img src="<?= base_url('../assets/img/logo.jpeg') ?>" alt="">
    </div>
    <div class="column right">
        <br><br><br><br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="name">SYNDIC LG PROPERTIES</span> <br>
        <span>73 BOULEVARD PRESIDENT WILSON</span><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cp">64000 Pau</span><br><br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>SAINT-NAZAIRE,le <?= date("d/m/Y", strtotime(str_replace('/', '-', $syndic->doc_syndic_date_creation))); ?></span>
    </div>
</div>
<div class="column">
    <div class="text-objet">
        <br><br><br>
        <span>LRAR</span> <br><br>
        <span>Objet : Mandat d'administration de biens</span><br>
        <span>PJ : copie du mandat de gérance + RIB</span>
    </div> <br><br>
    <div class="text-corp">
        <span>Madame,Monsieur</span> <br><br>
        <span>
           Nous avons le plaisir de vous informer que la société CELAVIGESTION est détentrice d'un mandat d'admininstration de biens pour le compte de Monsieur <?=$syndic->client_nom.' '.$syndic->client_prenom?>,
           propriétaire au sein de la Résidence le <?=$syndic->progm_nom?>.
        </span> <br><br>
        <span>
            De par notre statut de mandataire, nous vous informons que dorénavant, notre société règlera les charges de syndic de copropriété pour le compte de notre client.
        </span> <br><br>
        <span>
            Nous vous remercions de nous faire parvenir tous les appels de fonds, les décomptes de charges, ou tous autres documents (ex : convovation, PV d'AG,règlement de
            copropriété) et courriers (ex : mise en demeure de payer) qui concerneraient Monsieur <?=$syndic->client_nom.' '.$syndic->client_prenom?>
        </span><br><br>
        <span>
            Vous pouvez nous faire parvenir l'ensemble de ces documents à l'adresse e-mail suivante : <i>clementine@celavigestion.fr</i> et ou par courrier postal à l'adresse
            suivante :
        </span><br><br>
        <span>
            CELAVI GESTION <br>
            39 Route de Fondeline, Parc de Brais <br>
            44 600 SAINT-NAZAIRE <br>
        </span><br><br>
        <span>Dans l'attente d'une confirmation de votre part, je vous prie d'agréer, Madame, Monsieur, l'expression de mes salutations les meilleures.</span>
    </div>
    <div class="signature">
        <br><br><br>
        <span>Clémentine ROUSSELOT</span><br>
        <span>Chargée de Clientèle</span>
    </div><br><br><br><br><br>
</div>
<div class="footer">
    <span>
        SCELAVIGESTION SAS au capital de 100.000€, siret 477 782 346 00035, RCS Saint Nazaire, APE 6831Z, Numéro intracommunautaire FR69477782346, 
        siège social : parc de Brais, 39 route de Fondeline, 44600 Saint-Nazaire.
        Carte professionnelle G N° CPI 4402 2018 035 231 délivrée par la CCI de Nantes Saint-Nazaire. Garantie Gestion 650.000€.
    </span>
</div>