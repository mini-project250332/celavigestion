<head>
    <link rel="stylesheet" href="../assets/css/clientions/modelMandat.css">
    <link rel="stylesheet" href="../assets/css/clientions/bootstrap.min.css">
</head>
<style>
    @page {
        margin: 4%;
    }
</style>

<body style='word-wrap:break-word'>

    <div class="WordSection1">
        <div class="logoclv" style='text-align:center'>
            <img src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo">
        </div><br><br>

        <div class="titre" style="text-align: center;">
            <img width="654" height="194" src="<?php echo base_url('../assets/img/image002.png'); ?>">
        </div>

        <br clear="ALL">
        <br>
        <br>
        <div class="log"> <img width="794" height="382" src="<?php echo base_url('../assets/img/image003.jpg'); ?>"> </div>

        <br clear="ALL">
        <br>

        <div class="inscriptions" style="background:#31849B;border:1px solid black; font-family: 'Times New Roman'; ">
            <p class="e" style=" line-height: 0.5;text-align: center;">
                <b><span style="color:white;">Inscription au registre des mandats : N° <?= str_pad($mandat->mandat_id, 5, '0', STR_PAD_LEFT); ?></span></b>
            </p>
            <p class="e" style=" line-height: 0.5; margin-left: 2px;">
                <b><span style="font-size: 13px;color:white">Un numéro de mandat par lot/en cas de cession d’un des lots un avenant au mandat sera proposé</span></b>
            </p>
            <p class="e" style=" line-height: 0.5; margin-left: 2px;">
                <i><span style="color:white">Prévu par la loi 70-9 du 2 janvier 1970 et par l’article 65 du Décret N° 72.678 du 20 juillet 1972</span></i>
            </p>
        </div>
    </div>
    <br><br>
    <div class="WordSection2" style="page-break-before: always;">

        <p class="">
            <b><span>ENTRE-LES SOUSSIGNES : </span></b>
        </p>
        <p class="">
            <b><span>DESIGNATION DU (DES) MANDANT(S)</span></b>
        </p>
    </div>

    <div class="Section3">
        <span style="font-size: 13px;"><b>Nom, prénom :</b> <?= $mandat->client_nom . ' ' . $mandat->client_prenom; ?></span>
        <br><br>
        <p style=" line-height: 0.5;font-size: 13px;">Tel personnel : <?= $mandat->client_phonemobile; ?></p>
        <p style=" line-height: 0.5;font-size: 13px;">Email: <?= $mandat->client_email; ?></p>
        <p style=" line-height: 0.5;font-size: 13px;">Demeurant : <?= $mandat->client_adresse1 . ' ' . $mandat->client_cp . ' ' . $mandat->client_ville . ' ' . $mandat->client_pays; ?></p>
        <br>
        <p style="font-size: 13px;">Propriétaire du bien ci-dessous désigné,</p>
    </div>
    <div class="section5" style="text-align:justify;font-size: 14px;font-family: 'Times New Roman', Times, serif;">
        <p class=""><b><span>DESIGNATION DU MANDATAIRE,</span></b></p><br>
        <p class="" style='text-align:justify;font-size: 13px;'>
            La Société « CELAVIGESTION», S.A.S. au capital de 100 000 Euros dont le siège social est à Saint-Nazaire (44600), 39 route de Fondeline, Parc de Brais, immatriculée au
            Registre du Commerce et des Sociétés sous le numéro B 477 782 346 représentée par son représentant légal, Monsieur Bertrand Le Mire, désignée comme le mandataire dans la
            suite des présentes, titulaire de la carte professionnelle CPI 4402 2018 000 035 231 et disposant d’une garantie financière« gestion immobilière » de 620.000 €, délivrée
            par GALIAN, 89 rue de la Boétie, 75008 Paris, N° A12924809, disposant également d’une assurance responsabilité civile professionnelle auprès de MMA IARD, police N° 120
            137 405, 14 boulevard Marie et Alexandre Oyon, 72030 Le Mans Cedex 9, désigné comme <b style='font-size: 13.5px;'><u>LE MANDATAIRE</u></b> dans la suite des présentes.</span></i>
        </p>

        <p style='text-align:justify;font-size: 13px;'>La Société « CELAVIGESTION» ayant satisfait aux obligations de la Loi n°70-9 du 2 janvier 1970 et de son décret d’application n°72-678 du 20 juillet 1972, par :</p>
        <p style='text-align:justify;font-size: 13px;'>1 - la possession de la carte professionnelle n°CPI 4402 2018 000 035 231 délivrée par la CCI de Nantes, portant sur l’activité « gestion immobilière ». </p>
        <p style='text-align:justify;font-size: 13px;'>2 - la souscription auprès de la Caisse de Garantie de l’Immobilier ‘’GALIAN’’, dont le siège social est à
            PARIS 8° - 89 rue de La Boétie, d’une garantie financière des sommes et valeurs reçues au titre des activités de gestion immobilière visées par la Loi du 2 janvier 1970
            et son décret d’application,
        </p>
        <p style='text-align:justify;font-size: 13px;'>3 - la souscription auprès de la Compagnie MMA IARD, dont le siège social est au Mans, 14 boulevard Marie et Alexandre Oyon, d’une police d’assurance responsabilité
            civile professionnelle des agents immobiliers sous le N° 120 137 405.
        </p>
    </div>
    <br>
    <div class="section6" style="text-align:justify;font-family: 'Times New Roman', Times, serif;">
        <p class="" style="text-align:center; font-size: 15px; margin-left: 37% !important;">
            <b><span>Conditions particulières</span></b>
        </p><br>
        <p class=""><b><span>DESIGNATION DU BIEN</span></b></p>
        <p class="" style="line-height: 0.5;font-size: 13px;">
            <b><span>
                    Dans la résidence dénommée : <?= $mandat->progm_nom; ?>
            </b>
        </p>
        <p class="" style="line-height: 0.5;font-size: 13px;">
            <b>
                <span>Située: <?= $mandat->progm_id = 309 ? $mandat->cliLot_adresse1 . ' ' . $mandat->cliLot_cp . ' ' . $mandat->cliLot_ville . ' ' . $mandat->cliLot_pays : $mandat->progm_adresse1 . ' ' . $mandat->progm_cp . ' ' . $mandat->progm_ville . ' ' . $mandat->progm_pays; ?></span>
            </b>
        </p>
        <table class="table" style="border-collapse: collapse; width:50%">
            <tbody>
                <tr>
                    <td style="border:1px solid black;font-size: 13px;width:50%;padding-top:2mm">Numéro de lot</td>
                    <td style="border:1px solid black;font-size: 13px;width:50%;padding-top:2mm"><?=$mandat->cliLot_num?></td>
                </tr>
                <tr>
                    <td style=" border:1px solid black;font-size: 13px;width:50%;padding-top:2mm"> Parking</td>
                    <td style="border:1px solid black;font-size: 13px;width:50%;padding-top:2mm"><?=$mandat->cliLot_num_parking?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="article1" style="page-break-before: always;">

        <p class="">
            <b><u><span style="font-size:15px; font-family: 'Times New Roman', Times, serif;">ARTICLE 1 : MISSIONS ET POUVOIRS DU MANDATAIRE</span></u></b>
        </p>
        <table class="table" style="border-collapse: collapse;border: solid #31849B;font-size:15px; font-family: 'Times New Roman'">

            <tr style="background: #31849B; ">
                <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: solid #31849B;"><b>PILOTAGE DE L’INVESTISSEMENT</b></td>
                <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: solid #31849B;"><b>MISSIONS</b></td>
            </tr>

            <tbody>
                <tr>
                    <td class="" style="border: 2px solid #31849B;font-size:13px;padding-left:2mm;padding-right:1mm;">
                        <br>
                        <p><u>Contrôle des baux :</u></p><br>
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- lecture et analyse du bail</p>
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- calcul des loyers à recevoir selon le bail et comparaison avec les montants reçus</p>
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- préparation au renouvellement du bail avec alertes 1 an et 6 mois avant la fin de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bail</p> <br>

                        <p><u>Analyse du portefeuille :</u></p><br>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Indication des cash-flows (recettes et dépenses)</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- calcul du TRI et rendement</p> <br>

                        <p><u>Bibliothèque de documents</u></p><br>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tous vos documents sont accessibles sur votre espace personnel : acte, bail, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;factures de loyer, dépenses, appels de fonds.</p>

                    </td><br><br>
                    <td style="text-align: center;border: 2px solid #31849B;">
                        <p style="padding-left : 35%"><img src="<?php echo base_url('../assets/img/check.jpg'); ?>"> </p> <br><br> <br>
                        <p style="padding-left : 35%"><img src="<?php echo base_url('../assets/img/check.jpg'); ?>"> </p> <br> <br> <br>
                        <p style="padding-left : 35%"><img src="<?php echo base_url('../assets/img/check.jpg'); ?>"> </p> <br> <br>
                    </td>
                </tr>
                <tr class="" style="background: #31849B; ">
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: solid #31849B;"><b>GESTION LOCATIVE ET ADMINISTRATIVE</b></td>
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: solid #31849B;"><b>MISSION</b></td>
                </tr>
                <tr>
                    <td style="border: 2px solid #31849B;font-size:13px;padding-left:2mm;padding-right:1mm;">
                        <br>
                        <p><u> Gestion administrative :</u></p> <br>
                        <p style="padding-left : 10%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Assistance téléphonique</p><br>
                        <p style="padding-left : 10%;margin : 0px ;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Être l’interlocuteur du mandant vis à vis du syndic, du locataire-exploitant, pour les &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;missions suivantes exclusivement :</p>
                        <table>
                            <tr>
                                <td style="width: 25%;">&nbsp;</td>
                                <td style="font-size:13px;">
                                    <ul>
                                        <li>Demande de devis PNO</li>
                                        <li>Gestion des changements d’adresse et domiciliations bancaires</li>
                                        <li>Retard de paiement ou loyers impayés (voir gestion des contentieux)</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <p style="padding-left : 10%;margin : 0px ;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Être l’interlocuteur du mandant auprès de votre expert-comptable :</p>

                        <table>
                            <tr>
                                <td style="width: 25%;">&nbsp;</td>
                                <td style="font-size:13px;">
                                    <ul>
                                        <li>Collecte des documents*</li>
                                        <li>Transmission des documents à votre expert-comptable</li>
                                        <li>Accompagnement sur l’utilisation de la plateforme à la déclaration des revenus de location meublée.</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <p style="color:white">.</p>
                        <p><u>Gestion locative :</u></p> <br>

                        <p> - Procéder à la révision des loyers, calcul des indexations et émettre les factures de loyer</p> <br>
                        <p> - Emettre les factures de remboursement de charges récupérables sur demande expresse du mandant</p> <br>
                        <p> - Demande de remboursement de la taxe sur les ordures ménagères **</p> <br><br>

                        <p style="font-size:10px">
                            <i>*Ces documents doivent être fournis par le mandant, le mandataire ne pourrait être tenu responsable s’il manquait des documents lors des déclarations fiscales établies par l’expert-comptable ou le mandant lui-même.</i>
                        </p><br>
                        <p style="font-size:10px">
                            <i>**Si le rôle de taxe foncière a été transmis au mandataire</i>
                        </p>
                        <br><br><br><br><br><br><br>
                    </td>
                    <td class="" style="text-align: center;border: 2px solid #31849B;">
                        <p style="padding-left : 35%;padding-top : 25%"><img src="<?php echo base_url('../assets/img/check.jpg'); ?>"> </p> <br><br><br><br><br><br>
                        <p style="padding-left : 35%; padding-top : 50%"><img src="<?php echo base_url('../assets/img/check.jpg'); ?>"> </p>
                    </td>
                </tr>
                <tr class="" style="background: #31849B;">
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: 2px solid #31849B;"><b>GESTION DES CONTENTIEUX</b></td>
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: 2px solid #31849B;"><b>MISSIONS</b></td>
                </tr>
                <tr>
                    <td style="border: 2px solid #31849B;font-size:13px;padding-left:2mm;padding-right:1mm;">
                        <br>
                        <p>
                            - Procéder aux relances auprès des exploitants-locataires en cas d’impayés ou retard de loyers ou de toutes charges dues
                            par les locataires dans le cadre des obligations découlant de leurs baux*, à la demande expresse du mandant
                        </p>
                        <br>
                        <p>
                            - Faire délivrer toute sommation, commandement, assignation, sous réserve de l’accord du mandant et diligenter toute procédure, toute action résolutoire ou autre **
                        </p>
                        <br>
                        <p>
                            - Etablissement et gestion des demandes de dégrèvement
                        </p>
                        <br>
                        <p>
                            - Suivi du dossier contentieux avec l’huissier ou l’avocat **/***
                        </p>
                        <p style="color: white;">.</p>
                    </td>
                    <td style="border: 2px solid #31849B; font-size:13px;">
                        <p>Email de relance : gratuit Lettre recommandée : 25€ HT</p> <br>
                        <p>Commandement de payer : 99€ HT plus coût de l’huissier</p> <br> <br>
                        <p>Sur devis</p>
                    </td>
                </tr>
                <tr style="background: #31849B;">
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: 2px solid #31849B;"><b>MISSIONS PARTICULIERES</b></td>
                    <td style="color:white;text-align:center;height:10px;padding-bottom: 23px;border: 2px solid #31849B;"><b>MISSION</b></td>
                </tr>
                <tr>
                    <td style="border: 2px solid #31849B;font-size:13px;padding-left:2mm;padding-right:1mm;">
                        <br>
                        <p style="margin : 0px ;">- Déclaration ou modification d’activité auprès du Greffe</p>
                        <p style="margin : 0px ;">- Gestion des avis à tiers détenteur</p>
                        <p style="margin : 0px ;">- Clôture du dossier auprès du Greffe</p>
                        <p style="color: white;">.</p>
                    </td>
                    <td class="text-center;" style="border: 2px solid #31849B;font-size:13px;padding-left:2mm;padding-right:1mm;">
                        <p style="margin : 0px ;"><b>89€ HT</b></p>
                        <p style="margin : 0px ;"><b>55€ HT</b></p>
                        <p style="margin : 0px ;"><b>295€ HT</b></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="font-size:11px; font-family: 'Times New Roman', Times, serif;padding: -2px;">
            <p style="margin : 0px ;"><i><span>* coût de la LRAR non inclus 15€ TTC </span></i></p>
            <p style="margin : 0px ;"><i><span>** frais d’huissier non compris, </span></i></p>
            <p style="margin : 0px ;"><i><span>*** frais d’avocat non compris, sur devis</span></i></p>
        </div>
    </div>
    <br>

    <div class="article2" style="font-size:13px; font-family: 'Times New Roman', Times, serif;">
        <p class=""><b><span style="padding-right : 25px">1.1</span>
                PILOTAGE DE L’INVESTISSEMENT</b>
        </p>
        <div style="margin-left: 20px;">
            <p>
                Calcul des loyers théoriques : Celavigestion calculera les loyers à recevoir selon le bail et avenants en cours.
                Celavigestion ne peut être tenue pour responsable d’un quelconque manquement dans le cas où le mandant n’aurait pas transmis à Celavigestion les avenants au bail signés
                avec l’exploitant ou un nouveau bail remplaçant celui en cours.
            </p>
        </div>
        <div style="margin-left: 20px;">
            <p>
                Celavigestion fournira au mandant tous les trimestres le montant des loyers théorique du trimestre en cours.
                La comparaison avec le montant reçu sera effectuée par Celavigestion à condition que le mandant fournisse avant la fin du mois suivant le trimestre le montant reçu par lui.
                Celavigestion ne pourra être tenue pour responsable d’un quelconque manquement dans le cas où le mandant n’aurait pas fourni dans le délai imparti le montant des loyers reçus par lui-même.
            </p>
        </div>
        <div style="margin-left: 20px;">
            <p>
                La préparation au renouvellement du bail consiste à fournir au mandant la documentation générale sur le renouvellement du bail commercial
                un an avant l’échéance du bail mentionnant les options qui s’offrent au mandant.
                Il ne s’agit en aucun cas d’une consultation juridique de quelque nature que ce soit.
                Au cas où le mandant souhaiterait un conseil juridique, Celavigestion lui présentera des avocats spécialisés en matière de baux commerciaux en location meublée.
            </p>
        </div>

        <p class=""><b><span style="padding-right : 25px">1.2</span>
                GESTION LOCATIVE ET ADMINISTRATIVE</b>
        </p>
        <div style="margin-left: 20px;">
            <p>
                Celavigestion ne peut être tenu responsable de quelque manquement dans le cas où le mandant n’aurait pas fourni les documents permettant de mener à bien
                la mission telle que la copie recto verso du rôle de la taxe foncière, des appels de fonds ou reddition de compte de la copropriété (liste non exhaustive).
            </p>
        </div>
        <br><br>

        <p class=""><b><span style="padding-right : 25px">1.3</span>
                GESTION DES CONTENTIEUX</b>
        </p>
        <div style="margin-left: 20px;">
            <p>
                La gestion des contentieux sera directement gérée par Celavigestion sur demande expresse du mandant qui effectuera une première relance gratuite
                par email auprès du locataire. En cas de non réponse, une seconde relance par LRAR sera effectuée par Celavigestion après du locataire (coût 25€ HT).
                Dans le cas où Celavigestion n’obtiendrait pas le paiement intégral des sommes dues, Celavigestion devra en informer le mandant et proposer des actions,
                commandement de payer par huissier et/ou remise du dossier à l’avocat. <br>
                Celavigestion tiendra informé le mandant de l’ensemble des actions en recouvrement et cela trimestriellement.
            </p>
        </div>
        <p class=""><b><span style="padding-right : 25px">1.4</span>
                REDDITION DES COMPTES</b>
        </p>
        <div style="margin-left: 20px;">
            <p>
                Le mandataire, conformément à l’article 66 alinéa 1 du décret du 20 juillet 1972 rendra compte de sa gestion au Mandant.
                Le Mandataire adressera au mandant au 30 du mois suivant la fin de chaque trimestre civil, par email,
                une synthèse des actes accomplis en son nom ainsi qu’un état de synthèse concernant le pilotage de l’investissement.
            </p>
        </div>
        <p class=""><b><u><span style="font-size:15px; font-family: 'Times New Roman', Times, serif;">ARTICLE 2 : RÉMUNÉRATION </span></u></b></p>

        <table class="table" style="border-collapse: collapse;border: solid #31849B;font-size:15px; font-family: 'Times New Roman';width: 100%">
            <thead class="text-center" style="border: solid #31849B; background: #31849B; color:white;">
                <tr style="background: #31849B; ">
                    <td style="color:white;text-align:center;padding-bottom: 10px;"><b>HONORAIRES</b></td>
                    <td style="color:white;text-align:center;padding-bottom: 10px;"><b>Tarifs</b></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="color:white;text-align:center;padding-bottom: 20px;border: 1px solid #31849B">
                        <p><b><u><span style="color:#31849B;font-size:13px;text-decoration:underline">Mission sans encaissement</span></u></b></p>
                    <td style="text-align:center;padding-bottom: 20px;"> <b><?= $mandat->mandat_montant_ht ?>&nbsp;€ HT / an pour un lot.</b></td>
                </tr>
            </tbody>
        </table><br>

        <p class="" style="text-align:center; font-size: 17px;page-break-before: always;">
            <b><span>Conditions générales</span></b>
        </p>
    </div><br>

    <div class="article3" style="text-align:justify; font-size:11.0pt;font-family: 'Times New Roman', Times, serif;">
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 1 : OBLIGATIONS DU MANDANT</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Le mandat s’engage à :
        </p>
        <div style="font-size: 12px;">
            <p style="padding-left:10mm;">
                - &nbsp;&nbsp;&nbsp;&nbsp;Informer le mandataire dans un délai de 30 jours à compter du changement, de toute évolution relative à son état civil et &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;à la cession de son bien objet du mandat,<br>
                - &nbsp;&nbsp;&nbsp;&nbsp;A mettre à la disposition du mandataire, dans les 30 jours de la réception des documents, tout document nécessaire à &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l’exécution de sa mission,<br>
                - &nbsp;&nbsp;&nbsp;&nbsp;A vérifier que les états et documents produits par nos soins sont conformes aux informations fournies par vos soins et à &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nous informer sans retard de tout manquement ou erreur,<br>
                - &nbsp;&nbsp;&nbsp;&nbsp;A porter à notre connaissance dans les 30 jours de l’évènement tout fait nouveau ou exceptionnel susceptible d’affecter la &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mission du mandataire, notamment la signature d’un nouveau bail, d’un avenant, la réception d’un congé donné par le &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;locataire, la vente du bien, des loyers impayés. Cette liste n’étant pas exhaustive.
            </p>
        </div>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 2 : HONORAIRES</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Les honoraires sont de 295€ HT pour un lot. Pour un démarrage de mission postérieur au 31 août de l’année,
            les honoraires feront l’objet d’un prorata temporis établi par mois, tout mois commencé étant dû.
        </p>
        <p style="font-size: 12px;">
            Pour un second lot dans la même résidence, le tarif appliqué au second lot sera de 49€ HT par an.
            Ce tarif fera l’objet d’un prorata temporis conformément au paragraphe ci-dessus.
        </p>
        <p style="font-size: 12px;">
            Pour les lots supplémentaires dans des résidences différentes du premier lot, le tarif sera de 295€ HT par an par lot supplémentaire diminué de 30%.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 3 : PAIEMENT DES HONORAIRES</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Les honoraires sont payés uniquement par prélèvement automatique en une seule fois au 15 février de l’année.
            Tout rejet de prélèvement fera l’objet d’un coût de 15€ HT pour la gestion administrative. Le prélèvement sera représenté le 15 mars de l’année.
        </p>
        <p style="font-size: 12px;">
            En cas de démarrage dans l’année en cours, le paiement des honoraires se fera par prélèvement à 30 jours de la date de signature du mandat. Tout rejet de prélèvement, quelqu’en soit la cause imputable au mandant, fera l’objet d’un coût de 15€ HT pour la gestion administrative.
            Le prélèvement sera représenté le 15 du mois suivant.
        </p>
        <p style="font-size: 12px;">
            En cas de non-paiement des honoraires, le mandat fera l’objet d’une suspension de services jusqu’au paiement intégral des honoraires sans pouvoir excéder le 31 décembre de l’année.
            En cas de non-paiement des honoraires au 31 décembre de l’année en cours, le mandat sera rompu de plein droit.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 4 : REVISION DES HONORAIRES</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Les honoraires fixes feront l’objet d’une révision tous les ans au premier janvier de chaque année après une année complète révolue, sur la base de l’indice des prix de production des services aux entreprises Françaises CPF 68.32, administration de biens immobiliers pour compte de tiers 010546170, publié par l’INSEE (https://www.insee.fr/fr/statistiques/serie/010546170). L’indice de base sera l’indice le dernier indice définitif publié au 31 décembre N-1 (usuellement l’indice du T2 N-1 publié au 30 novembre de chaque année).
            En cas de disparition de l’indice, il sera pris en compte le nouvel indice proposé par l’INSEE.
        </p>
        <br>
        <p style="font-size: 12px;">
            <b>
                <u>ARTICLE 5 : DURÉE</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            La date d’effet du mandat court à compter de la signature des présentes et ce jusqu’au 31 décembre de l’année en cours. Ce contrat se renouvellera ensuite tacitement d’année en année.
            L’une ou l’autre des parties pourra résilier le présent mandat au terme de chaque année à condition d’en informer l’autre partie,
            par lettre recommandée avec accusé de réception, six mois avant la date anniversaire que représente la date d’échéance du contrat initial,
            à savoir le 1er janvier de chaque année. Le délai de préavis commencera à courir à compter du lendemain du jour de la première présentation de la lettre recommandée.
            Les honoraires encaissés feront l’objet d’un remboursement prorata temporis.
        </p>
        <p style="font-size: 12px;">
            Le Mandataire aura la possibilité de résilier le présent mandat, à tout moment, par lettre recommandée avec accusé de réception dans les cas suivants :
        </p>
        <p style="font-size: 12px;">
            - le Mandant refuserait ou ne donnerait pas suite à une demande formulée par le Mandataire.
        </p>
        <p style="font-size: 12px;">
            - cas de force majeure empêchant le Mandataire d’accomplir tout ou partie de ses obligations.
        </p>
        <p style="font-size: 12px;">
            - à défaut du paiement de ses honoraires au 31 décembre de l’année en cours.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 6 : PROCURATION DONNÉE</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Le mandant donne procuration au mandataire aux fins de signer pour son compte les documents d’immatriculation,
            de radiation ou de modification POi, FMCB, P2, P4i et tout document nécessaire au greffe
            et au Service des Impôts des Entreprises ou guichet unique de l’INPI dans le cadre de l’exploitation du bien référencé. <br>
            Le mandant donne procuration au mandataire aux fins de signer pour son compte tout contrat d’assurance PNO après accord du client quant à la souscription de l’assurance PNO <br>
            Le mandant donne procuration au mandataire aux fins de récupérer auprès de son ancien expert-comptable ou son expert-comptable actuel les documents qui lui manqueraient dans son dossier fiscal.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 7 : SUBSTITUTION - CESSION – FUSION</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            En cas de cession de son fonds de commerce par le Mandataire, en cas de fusion, d’apport ou si celui-ci confie l’exploitation dudit fonds à un locataire gérant, le présent mandat se poursuivra au profit du cessionnaire ou du locataire gérant,
            ce que le mandant accepte expressément sous réserve que le successeur du mandataire remplisse les conditions issues de la loi du 02 janvier 1970. <br>
            Le mandant devra être avisé dans les meilleurs délais, et au plus tard dans les six mois de la substitution de la cession ou de la location gérance du fonds de commerce. <br>
            Le mandant aura la faculté de résilier le présent mandat dans le mois qui suivra la réception de la lettre l’avisant de l’événement. S’il use de cette faculté, le mandant devra faire connaitre sa décision au nouveau mandataire
            ou au mandataire substitué par lettre recommandé avec accusé de réception. <br>
            Ce mandat est conclu intuitu personae. Il ne pourra aucunement être transmis en cas de décès du mandant ni en cas de cession du bien concerné.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 8 : ATTRIBUTION DE COMPETENCE</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            En cas de contestation sur l’exécution du présent mandat si les parties ont la qualité de commerçant, le tribunal du domicile du Mandataire sera seul compétent.
            Le présent mandat est régi par les dispositions du décret N°72- 768 du 20 juillet 1972, modifié par le décret N°2005-1315 du 21 octobre 2005, pris en application de la loi 70-9.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 9 : MEDIATION</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Conformément à l’article L211-3 du code de la consommation, le Mandant est informé qu’il a le droit de recourir à un médiateur de la consommation en vue de la résolution amiable du litige qui pourrait l’opposer au Mandataire. <br>
            Les modalités de cette médiation sont organisées par les articles L611-1 et suivants et R612-1 du Code de la consommation.
            En cas de litige relatif à la bonne exécution du mandat, une solution amiable sera recherchée prioritairement à toute action judiciaire.
        </p>
        <p style="font-size: 13px;">
            Aussi, pour le traitement d’une réclamation, le mandant peut prendre contact avec son chargé de clientèle chez Celavigestion.
        </p>
        <p style="font-size: 12px;">
            En cas de réponse jugée insatisfaisante ou d’absence de réponse dans un délai de 30 jours, le mandant peut s’adresser par courrier au service de médiation dont l’adresse
            à la date des présentes est la suivante : Philippe TREMAIN, médiateur près la Cours d’Appel et du Tribunal Administratif, <a href=""> contact@mcpmediation.org, www.mcpmediation.org</a>
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 10: INFORMATIQUE ET LIBERTÉS</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Conformément à la loi n° 78-17 du 6 janvier 1978, le mandant autorise expressément le mandataire à saisir l’ensemble des informations contenues dans les présentes sur fichier informatique, le mandant pouvant exercer son droit d’accès et de rectification des données personnelles le concernant auprès du mandataire.
        </p>
        <p style="font-size: 12px;">
            Conformément à la loi n° 78-17 du 6 janvier 1978, le mandant autorise expressément le mandataire à saisir l’ensemble des informations contenues dans les présentes sur fichier informatique, le mandant pouvant exercer son droit d’accès et de rectification des données personnelles le concernant auprès du mandataire.
        </p>
        <br>
        <p style="font-size: 13px;">
            <b>
                <u>ARTICLE 11 : ELECTION DE DOMICILE</u>
            </b>
        </p>
        <p style="font-size: 12px;">
            Pour l’exécution des présentes et leurs suites, les parties font élection de domicile à leurs adresses respectives telles qu’indiquées en tête des présentes. <br>
            En cas de modification, chacune des parties devra en informer l’autre par lettre recommandée avec demande accusée de réception dans les 15 jours. A défaut, toute notification faite à l’adresse indiquée en tête des présentes, sera réputée valablement faite. <br>
            Les conditions particulières annexées font partie intégrante des conditions générales du mandat d’administration de biens, de sorte que l’ensemble forme un tout indissociable et indivisible.
        </p>
    </div>
    <br>
    <div style="border: 1px solid black;padding-left:7mm">
        <div class="signature">

            <p class="" style='border:none;padding:0cm'><span style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black;background:white'>Fait en 2 exemplaires, dont un sera remis au Mandant après
                    enregistrement. </span></p>

            <p class="" style='border:none;padding:0cm'><span style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black;background:white'> </span></p>

            <p class="" style='border:none;padding:0cm'><span style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black; background:white'>A <?= $mandat->mandat_lieu_facturation ?> ,    le <?= date("d/m/Y", strtotime(str_replace('/', '-', $mandat->mandat_date_creation))) ?> </span></p>

            <p class="" style='border:none;padding:0cm'><span> </span></p>

            <p class="" style='border:none;padding:0cm'><span> </span></p>

            <p class="" style='border:none;padding:0cm'><span style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;background:white'>Le Mandant      </span><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>[Signature1/] </span><span style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
background:white'>                                                            
                    Le Mandataire </span><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>[Signature2/]
                </span><span style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;
color:black;background:white'>   </span></p>

            <p class="" style='border:none;padding:0cm'><span style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
background:white'> </span></p>

            <p class="" style='border:none;padding:0cm'><span style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
background:white'> <b><i>« lu et approuvé, bon pour mandat »                                    «
                            lu et approuvé, mandat accepté »</i></b></span></p>

            <p class="" style='border:none;padding:0cm'><span> </span></p>

        </div>
        <br><br><br><br><br>
    </div>

    <br><br><br><br><br>
    <p style="font-size: 50px;color:black;position:sticky">________________________________</p>

    <p style="font-size: 10px;padding-top:-9mm;text-align:center">
        <b>Celavigestion</b>, Sas au capital de 100000 €, Siret : 477 782 346 00035 RCS Saint-Nazaire, Ape 6831Z, Numéro TVA intracommunautaire FR69477782346. <br>
        Adresse : 39 route de Fondeline, parc de Brais, 44600 Saint-Nazaire, France. Carte professionnelle transaction et gestion N° CPI 4402 2018 000 035 231 délivrée
        par la CCI de Nantes, garantie gestion à hauteur de 620.000€ souscrites auprès de GALIAN, 89 rue de la Boétie, 75008 Paris
    </p>

    <div class="logoclv" style='text-align:center'>
        <img src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo" style="height:125px;width:150px">
    </div><br><br>

    <div style="font-size: 13px;border-top: 1px solid black;padding-left:7mm; border-bottom: 1px solid black;border-right: 1px solid black">
        <p>
            En signant ce formulaire de mandat, vous autorisez (A) CELAVIGESTION à envoyer des instructions à votre banque pour
            débiter votre compte, et (B) votre banque à débiter votre compte conformément aux instructions de CELAVIGESTION
        </p>
        <p>
            Vous bénéficiez du droit d'être remboursé par votre banque suivant les conditions décrites dans la convention que vous avez
            passée avec elle. Une demande de remboursement doit être présentée dans les 8 semaines suivant la date de débit de votre
            compte pour un prélèvement autorisé.
        </p>
        <p>Référence unique du mandat : <b><?= str_pad($mandat->mandat_id, 5, '0', STR_PAD_LEFT); ?></b> &nbsp; Identifiant créancier SEPA : : ICS: <b>FR45ZZZ532215</b></p>

        <table style="width: 100%;">
            <tr>
                <td style="width:55%;color:#31849B;">
                    <b> Débiteur : </b>
                </td>
                <td style="width:45%;color:#31849B;">
                    <b> Créancier : CELAVIGESTION </b>
                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>
                    <br> Votre Nom : <?= $mandat->client_nom ?>&nbsp;<?= $mandat->client_prenom ?><br>&nbsp;
                </td>
                <td rowspan="2">
                    39 route de Fondeline <br>44600 SAINT NAZAIRE <br>FRANCE
                </td>
            </tr>

            <tr>
                <td>
                    Votre Adresse : <?= $mandat->client_adresse1 ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Code postal : <?= $mandat->client_cp ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Ville : <?= $mandat->client_ville ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Pays : <?= $mandat->client_pays ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    IBAN : <?= $mandat->iban ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    BIC : <?= $mandat->bic ?><br>&nbsp;
                </td>
            </tr>
        </table>

        <p>
            Paiement : Récurrent/Répétitif <img src="<?php echo base_url('../assets/img/checbox.png'); ?>" alt="logo" style="height:10px;width:15px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="text-decoration: line-through;">Ponctuel<input type="checkbox" id="myCheckbox"> </span>
        </p>
        <p>
            A : <b>________________________________</b> Le : <b>____________________________________________</b>
        </p>
        <p>
            <span style="color:#31849B"><b>Signature :</b> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Signature1/]
        </p> <br><br><br><br><br><br><br><br><br><br><br>
        <p>
            Nota : vos droits concernant le présent mandat sont expliqués dans un document que vous pouvez obtenir auprès de votre
            banque.
        </p>
    </div>


</body>