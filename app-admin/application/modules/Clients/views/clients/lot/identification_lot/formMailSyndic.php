<style>
    .badge {
        cursor: pointer;
    }
</style>
<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Envoi de mail (Demande d'information au Syndic)</h5>
</div>
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?=$cliLot_id?>">
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <select class="form-select" name="emeteur_syndic" id="emeteur_syndic">
                                <?php foreach ($liste_emetteur as $key => $value) : ?>
                                    <option value="<?= $value->mess_id ?>" <?= $default_user == $value->util_id ? 'selected' : '' ?>>
                                        <?= $value->util_prenom ?>&nbsp;<?= $value->util_nom ?>&nbsp;(<?= $value->mess_email ?>)
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataires :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_syndic" value="<?= isset($destinataire->cnctSyndicat_email1) ? $destinataire->cnctSyndicat_email1 :"" ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataire en copie :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_syndic_copie" value="">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object_syndic" value="<?= $object_mail ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message_text_syndic" style="height : 315px;"><?= $mailMessage ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <label data-width="200" for="recipient-name" class="col-form-label d-none" id="signature_syndic">Aucune</label>
                            <img src="" alt="" id="image_signature" style="width:400px; height:130px;" class="d-none">
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-2">
                                <label data-width="200" class="col-form-label" style="font-size: 13.5px!important;">Fichiers joints :</label>
                            </div>
                            <div class="col-10 mt-1 list_doc">
                                <?php if (!empty($document)) : ?>
                                    <?php
                                        $fileSyndic = explode("/", $document->doc_syndic_path);
                                        $nom_fichier = $fileSyndic[6];    
                                        $rib = explode("/", $document->rib);
                                        $nom_fic = $document->rib != NULL ? $rib[3] : 'Pas de fichier';
                                    ?>
                                    <div class="line-<?= $document->doc_syndic_id ?> doc">
                                        <input class="document_syndic" type="hidden" value="<?= $document->doc_syndic_id ?>">
                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $document->doc_syndic_path ?>')" data-url="<?= base_url($document->doc_syndic_path) ?>"><?=$nom_fichier?></span>
                                        <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefile" data-id="<?= $document->doc_syndic_id ?>">.</span><br>
                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $document->mandat_path ?>')" data-url="<?= base_url($document->mandat_path) ?>"><?= str_pad($document->mandat_id, 4, '0', STR_PAD_LEFT) ?></span>
                                        <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefilemandat" data-id="<?= $document->mandat_id ?>">.</span><br>
                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $document->rib ?>')" data-url="<?= base_url($document->rib) ?>"><?= $nom_fic ?></span>
                                        <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefileRib" data-id="<?= $document->cliLot_id ?>">.</span><br>
                                    </div>
                                <?php else : ?>
                                    <p style="font-weight: 500;font-size: 14px; margin-left: -26px;">Pas de fichier joints</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary" data-id="<?= $doc_syndic_id ?>" id="sendMailSyndic">Envoyer</button>
    </div>
</div>