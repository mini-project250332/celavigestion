<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <button type="submit" id="save-lot" class="btn btn-sm btn-primary mx-1">
            <i class="fas fa-check-circle"></i>
            <span> Terminer </span>
        </button>
    </div>
</div>

<?php if (isset($lot->cliLot_id)) : ?>
    <input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $lot->cliLot_id; ?>">
<?php endif; ?>
<?php $iban = $rib->rib_iban;
$bic = $rib->rib_bic; ?>

<input type="hidden" name="page" id="page" value="<?= $page ?>">
<div id="" class="row m-0">

    <div class="col-12 p-0 py-1">
        <div class="row m-0">
            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-body ">
                        <h5 class="fs-0">Identification</h5>
                        <hr class="mt-0">
                        <div class="mt-4">
                            <table class="table table-borderless fs--1 mb-0">
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="p-0">
                                            <div class="form-group inline">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Dossier </label>
                                                    <select class="form-select fs--1" id="dossier_id" name="dossier_id">
                                                        <?php foreach ($dossier as $dossiers) : ?>
                                                            <option value=" <?= $dossiers->dossier_id; ?>" <?php echo isset($lot->dossier_id) ? (($lot->dossier_id !== NULL) ? (($lot->dossier_id == $dossiers->dossier_id) ? "selected" : " ") : "") : "" ?>>
                                                                <?= str_pad($dossiers->dossier_id, 4, '0', STR_PAD_LEFT); ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="p-0">
                                            <div class="form-group inline">
                                                <div class="col-sm-12">
                                                    <label data-width="250"> Programme </label>
                                                    <select class="form-select fs--1" id="progm_id" name="progm_id" style="margin-right : 3px; width : 147%;">
                                                        <?php foreach ($program as $programme) : ?>
                                                            <option value="<?= $programme->progm_id ?>" <?php echo isset($lot->progm_id) ? (($lot->progm_id !== NULL) ? (($lot->progm_id == $programme->progm_id) ? "selected" : " ") : "") : "" ?>>
                                                                <?= $programme->progm_nom ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <div class="help"></div>
                                                    <div class="">
                                                        <button id="addProgramme" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group inline alertPrgm">
                                                <div class="col-12">
                                                    <label data-width="250" style="font-size: 11px; font-style:italic;">
                                                        Attention, l'adresse du programme peut être utilisée pour plusieurs
                                                        lots. <br>
                                                        Si vous la modifiez, elle sera modifiée pour les autres lots
                                                        également.
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- adresse prgm -->
                                    <tr class="border-bottom adresse-prgm adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 1 </label>
                                                    <input type="text" class="form-control maj mt-2" id="progm_adresse1" name="progm_adresse1" value="<?php echo isset($lot->progm_adresse1) ? $lot->progm_adresse1 : "" ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 2</label>
                                                    <input type="text" class="form-control maj mt-2" id="progm_adresse2" name="progm_adresse2" value="<?php echo isset($lot->progm_adresse2) ? $lot->progm_adresse2 : "" ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 3 </label>
                                                    <input type="text" class="form-control maj mt-2" id="progm_adresse3" name="progm_adresse3" value="<?= isset($lot->progm_adresse3) ? $lot->progm_adresse3 : ""; ?>">
                                                    <div class="help"> </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Code postal </label>
                                                    <input type="text" class="form-control mt-2" id="progm_cp" name="progm_cp" value="<?= isset($lot->progm_cp) ? $lot->progm_cp : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Ville </label>
                                                    <input v-on:keyup='input_form' type="text" class="form-control maj mt-2" id="progm_ville" name="progm_ville" value="<?= isset($lot->progm_ville) ? $lot->progm_ville : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-prgm">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Pays </label>
                                                    <input type="text" class="form-control maj mt-2" id="progm_pays" name="progm_pays" value="<?= isset($lot->progm_pays) ? $lot->progm_pays : ""; ?>">
                                                    <div cclass="help fs-11 height-15 m-0"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- adresse lot -->
                                    <tr class="border-bottom adresse-lot d-none adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 1 </label>
                                                    <input type="text" class="form-control maj mt-2" id="cliLot_adresse1" name="cliLot_adresse1" value="<?= isset($lot->cliLot_adresse1) ? $lot->cliLot_adresse1 : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 2</label>
                                                    <input type="text" class="form-control maj mt-2" id="cliLot_adresse2" name="cliLot_adresse2" value="<?= isset($lot->cliLot_adresse2) ? $lot->cliLot_adresse2 : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Adresse 3 </label>
                                                    <input type="text" class="form-control maj mt-2" id="cliLot_adresse3" name="cliLot_adresse3" value="<?= isset($lot->cliLot_adresse3) ? $lot->cliLot_adresse3 : ""; ?>">
                                                    <div class="help"> </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Code postal </label>
                                                    <input type="text" class="form-control mt-2" id="cliLot_cp" name="cliLot_cp" value="<?= isset($lot->cliLot_cp) ? $lot->cliLot_cp : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Ville </label>
                                                    <input v-on:keyup='input_form' type="text" class="form-control maj mt-2" id="cliLot_ville" name="cliLot_ville" value="<?= isset($lot->cliLot_ville) ? $lot->cliLot_ville : ""; ?>">
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom adresse-lot d-none">
                                        <td class="p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="150"> Pays </label>
                                                    <input type="text" class="form-control maj mt-2" id="cliLot_pays" name="cliLot_pays" value="<?= isset($lot->cliLot_pays) ? $lot->cliLot_pays : ""; ?>">
                                                    <div cclass="help fs-11 height-15 m-0"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-body ">
                        <h5 class="fs-0">Informations du lot</h5>
                        <hr class="mt-0">
                        <div class="mt-4">
                            <table class="table table-borderless fs--1 mb-0">
                                <tbody>
                                    <tr class="border-bottom">
                                        <div class="form-group inline">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Lot principal </label>
                                                <select class="form-select fs--1" id="cliLot_principale" name="cliLot_principale" style="width:104%;">
                                                    <?php
                                                    $array_type = unserialize(clilotprincipale);
                                                    foreach ($array_type as $type) {
                                                        echo '<option value="' . $type['value'] . '"  ' . (($lot->cliLot_principale == $type['value']) ? "selected" : "") . '  >' . $type['clilotprincipal'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Type de bail </label>
                                                <select class="form-select fs--1" id="typelot_id" name="typelot_id">
                                                    <?php foreach ($typelot as $typelots) : ?>
                                                        <option value="<?= $typelots->typelot_id ?>" <?php echo isset($lot->typelot_id) ? (($lot->typelot_id !== NULL) ? (($lot->typelot_id == $typelots->typelot_id) ? "selected" : " ") : "") : "" ?>>
                                                            <?= $typelots->typelot_libelle ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="200"> Typologie </label>
                                                    <select class="form-select fs--1" id="typologielot_id" name="typologielot_id">
                                                        <?php foreach ($typologie as $typologie) : ?>
                                                            <option value="<?= $typologie->typologielot_id ?>" <?php echo isset($lot->typologielot_id) ? (($lot->typologielot_id !== NULL) ? (($lot->typologielot_id == $typologie->typologielot_id) ? "selected" : " ") : "") : "" ?>>
                                                                <?= $typologie->typologielot_libelle ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="form-group inline p-0">
                                                <div class="col-sm-12">
                                                    <label data-width="200"> Prestations souscrites </label>
                                                    <select class="form-select fs--1 js-choice" id="produitcli_id" name="produitcli_id" multiple="multiple" size="1" data-require="true">
                                                        <option value="">
                                                        </option>
                                                    </select>
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Activité : </label>
                                                <select class="form-select fs--1" id="activite" name="activite">
                                                    <option value="LMNP" <?php echo ($lot->activite == "LMNP") ? "selected" : " " ?>>
                                                        LMNP
                                                    </option>
                                                    <option value="BIC HOTELIER" <?php echo ($lot->activite == "BIC HOTELIER") ? "selected" : " " ?>>
                                                        BIC HOTELIER
                                                    </option>
                                                    <option value="FONCIER" <?php echo ($lot->activite == "FONCIER") ? "selected" : " " ?>>
                                                        FONCIER
                                                    </option>
                                                </select>
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Défiscalisation : </label>
                                                <select class="form-select fs--1" id="defiscalisation" name="defiscalisation">
                                                    <option value="NON" <?php echo ($lot->defiscalisation == "NON") ? "selected" : " " ?>>
                                                        NON
                                                    </option>
                                                    <option value="CENSI BOUVARD" <?php echo ($lot->defiscalisation == "CENSI BOUVARD") ? "selected" : " " ?>>
                                                        CENSI BOUVARD
                                                    </option>
                                                    <option value="PINEL" <?php echo ($lot->defiscalisation == "PINEL") ? "selected" : " " ?>>
                                                        PINEL
                                                    </option>
                                                    <option value="" <?php echo ($lot->defiscalisation == "") ? "selected" : " " ?>>

                                                    </option>
                                                </select>
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom p-0">
                                        <div class="form-group inline">
                                            <div class="col-sm-12">
                                                <label data-width="150"> Fin défiscalisation : </label>
                                                <input type="date" class="form-control" id="fin_defiscalisation" name="fin_defiscalisation" value="<?= isset($lot->fin_defiscalisation) ? $lot->fin_defiscalisation : ""; ?>">
                                                <input type="text" class="form-control" id="non_fin_defiscalisation" value="N/A">
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Remboursement TOM : </label>
                                                <select class="form-select fs--1" id="rembst_tom" name="rembst_tom">
                                                    <option value="NON" <?php echo ($lot->rembst_tom == "NON") ? "selected" : " " ?>>
                                                        NON
                                                    </option>
                                                    <option value="OUI" <?php echo ($lot->rembst_tom == "OUI") ? "selected" : " " ?>>
                                                        OUI
                                                    </option>
                                                    <option value="" <?php echo ($lot->rembst_tom == "") ? "selected" : " " ?>>

                                                    </option>
                                                </select>
                                                <div class="help"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline">
                                            <div class="form-group inline">
                                                <div class="col-sm-12">
                                                    <label data-width="200"> Comptabilité : </label>
                                                    <select class="form-select fs--1" id="comptabilite" name="comptabilite">
                                                        <option value="PLATEFORME" <?php echo ($lot->comptabilite == "PLATEFORME") ? "selected" : " " ?>>
                                                            PLATEFORME
                                                        </option>
                                                        <option value="LMNP EXPERTS" <?php echo ($lot->comptabilite == "LMNP EXPERTS") ? "selected" : " " ?>>
                                                            LMNP EXPERTS
                                                        </option>
                                                        <option value="GEFI" <?php echo ($lot->comptabilite == "GEFI") ? "selected" : " " ?>>
                                                            GEFI
                                                        </option>
                                                        <option value="AUTRE" <?php echo ($lot->comptabilite == "AUTRE") ? "selected" : " " ?>>
                                                            AUTRE
                                                        </option>
                                                    </select>
                                                    <div class="help"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="200"> Numéro du lot : </label>
                                                <input type="text" class="form-control" id="cliLot_num" name="cliLot_num" value="<?= isset($lot->cliLot_num) ? $lot->cliLot_num : ""; ?>">
                                                <div cclass="help fs-11 height-15 m-0"></div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="150"> Numéro du parking : </label>
                                                <input type="text" class="form-control" id="cliLot_num_parking" name="cliLot_num_parking" value="<?= isset($lot->cliLot_num_parking) ? $lot->cliLot_num_parking : ""; ?>">
                                                <div cclass="help fs-11 height-15 m-0"></div>
                                            </div>
                                        </div>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col  p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-body">
                        <h5 class="fs-0 pb-2">Informations paiements loyers</h5>
                        <hr class="mt-0">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="250"> Encaissement des loyers : </label>
                                            <select class="form-select fs--1" id="cliLot_encaiss_loyer" name="cliLot_encaiss_loyer">
                                                <option value="Compte Celavigestion" <?php echo ($lot->cliLot_encaiss_loyer == "Compte Celavigestion") ? "selected" : " " ?>>
                                                    Compte Celavigestion
                                                </option>
                                                <option value="Compte client" <?php echo ($lot->cliLot_encaiss_loyer == "Compte client") ? "selected" : " " ?>>
                                                    Compte client
                                                </option>
                                                <option value="Plus de loyer car plus à bail" <?php echo ($lot->cliLot_encaiss_loyer == "Plus de loyer car plus à bail") ? "selected" : " " ?>>
                                                    Plus de loyer car plus à bail
                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Mode de paiement :</label>
                                            <select class="form-select fs--1" id="cliLot_mode_paiement" name="cliLot_mode_paiement">
                                                <option value="Virement" <?php echo ($lot->cliLot_mode_paiement == "Virement") ? "selected" : " " ?>>
                                                    Virement
                                                </option>
                                                <option value="Chèque" <?php echo ($lot->cliLot_mode_paiement == "Chèque") ? "selected" : " " ?>>
                                                    Chèque
                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline doc">
                                        <?php if ($lot->rib) : ?>
                                            <?php $nom_fic = explode("/", $lot->rib); ?>
                                            <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $lot->rib ?>')" data-url="<?= base_url($lot->rib) ?>" style="cursor: pointer;"><?= $nom_fic[3] ?></span>
                                            <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefilerib" style="cursor: pointer;">.</span><br>
                                        <?php endif ?>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <input type="file" class="form-control" id="rib" name="rib">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline clientiban">
                                        <div class="col-sm-12">
                                            <label data-width="200"> IBAN client :</label>
                                            <input type="text" class="form-control" id="cliLot_iban_client" name="cliLot_iban_client" value="<?= isset($lot->cliLot_iban_client) ? $lot->cliLot_iban_client : ""; ?>" autocomplete="off">
                                            <div class="help help_iban"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline clientiban">
                                        <div class="col-sm-12">
                                            <label data-width="200"> BIC client :</label>
                                            <input type="text" class="form-control" id="cliLot_bic_client" name="cliLot_bic_client" value="<?= isset($lot->cliLot_bic_client) ? $lot->cliLot_bic_client : ""; ?>" maxlength="30">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline clvIban" style="display : none;">
                                        <div class="col-sm-12">
                                            <label data-width="250"> IBAN Celavigestion :</label>
                                            <input type="text" class="form-control auto_effect" id="clv_iban" name="clv_iban" value="<?= $iban ?>" disabled>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline clvIban" style="display : none;">
                                        <div class="col-sm-12">
                                            <label data-width="250"> BIC Celavigestion :</label>
                                            <input type="text" class="form-control auto_effect" id="clv_bic" name="clv_bic" value="<?= $bic ?>" disabled>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="200"> TVA : </label>
                                            <select class="form-select fs--1" id="tva" name="tva">
                                                <option value="AUTOLIQUIDATION" <?php echo ($lot->tva == "AUTOLIQUIDATION") ? "selected" : " " ?>>
                                                    AUTOLIQUIDATION
                                                </option>
                                                <option value="REEL SIMPLIFIE CA3" <?php echo ($lot->tva == "REEL SIMPLIFIE CA3" || $lot->tva == "REEL SIMPLIFIE") ? "selected" : " " ?>>
                                                    REEL SIMPLIFIE CA3
                                                </option>
                                                <option value="REEL SIMPLIFIE CA12" <?php echo ($lot->tva == "REEL SIMPLIFIE CA12") ? "selected" : " " ?>>
                                                    REEL SIMPLIFIE CA12
                                                </option>
                                                <option value="REEL NORMAL" <?php echo ($lot->tva == "REEL NORMAL") ? "selected" : " " ?>>
                                                    REEL NORMAL
                                                </option>
                                                <option value="NON ASSUJETI" <?php echo ($lot->tva == "NON ASSUJETI") ? "selected" : " " ?>>
                                                    NON ASSUJETI
                                                </option>
                                                <option value="" <?php echo ($lot->rembst_tom == "") ? "selected" : " " ?>>

                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <!-- <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Facture loyer : </label>
                                            <select class="form-select fs--1" id="facture_loyer" name="facture_loyer">
                                                <option value="NON" <?php echo ($lot->facture_loyer == "NON") ? "selected" : " " ?>>
                                                    NON
                                                </option>
                                                <option value="OUI" <?php echo ($lot->facture_loyer == "OUI") ? "selected" : " " ?>>
                                                    OUI
                                                </option>
                                                <option value="Plus de facture car plus à bail" <?php echo ($lot->facture_loyer == "Plus de facture car plus à bail") ? "selected" : " " ?>>
                                                    Plus de facture car plus à bail
                                                </option>
                                                <option value="" <?php echo ($lot->facture_loyer == "") ? "selected" : " " ?>>

                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr> -->
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Facture charge : </label>
                                            <select class="form-select fs--1" id="facture_charge" name="facture_charge">
                                                <option value="NON" <?php echo ($lot->facture_charge == "NON") ? "selected" : " " ?>>
                                                    NON
                                                </option>
                                                <option value="OUI" <?php echo ($lot->facture_charge == "OUI") ? "selected" : " " ?>>
                                                    OUI
                                                </option>
                                                <option value="" <?php echo ($lot->facture_charge == "") ? "selected" : " " ?>>

                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Facture TOM : </label>
                                            <select class="form-select fs--1" id="facture_tom" name="facture_tom">
                                                <option value="NON" <?php echo ($lot->facture_tom == "NON") ? "selected" : " " ?>>
                                                    NON
                                                </option>
                                                <option value="OUI" <?php echo ($lot->facture_tom == "OUI") ? "selected" : " " ?>>
                                                    OUI
                                                </option>
                                                <option value="" <?php echo ($lot->facture_tom == "") ? "selected" : " " ?>>

                                                </option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-body ">
                        <h5 class="fs-0">Gestionnaire et syndic</h5>
                        <hr class="mt-0">
                        <div class="mt-4">
                            <table class="table table-borderless fs--1 mb-0">
                                <tbody>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="250"> Gestionnaire </label>
                                                <select class="form-select fs--1" id="gestionnaire_id" name="gestionnaire_id">
                                                    <?php foreach ($gestionnaire as $gestionnaires) : ?>
                                                        <option value="<?= $gestionnaires->gestionnaire_id ?>" <?php echo isset($lot->gestionnaire_id) ? (($lot->gestionnaire_id !== NULL) ? (($lot->gestionnaire_id == $gestionnaires->gestionnaire_id) ? "selected" : " ") : "") : "" ?>>
                                                            <?= $gestionnaires->gestionnaire_nom ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="help"></div>&nbsp;
                                                <div class="m-0">
                                                    <button id="addGestionnaire" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
                                                </div>

                                            </div>
                                        </div>
                                    </tr>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="250"> Syndic </label>
                                                <select class="form-select fs--1" id="syndicat_id" name="syndicat_id">
                                                    <?php foreach ($syndicat as $syndicats) : ?>
                                                        <option value="<?= $syndicats->syndicat_id ?>" <?php echo isset($lot->syndicat_id) ? (($lot->syndicat_id !== NULL) ? (($lot->syndicat_id == $syndicats->syndicat_id) ? "selected" : " ") : "") : "" ?>>
                                                            <?= $syndicats->syndicat_nom ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="help"></div>&nbsp;
                                                <div class="m-0">
                                                    <button id="addSyndicat" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="px-2">
                            <hr class="">
                        </div>

                        <div class="mt-4">

                            <table class="table table-borderless fs--1 mb-0">
                                <tbody>
                                    <tr class="border-bottom">

                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="250"> Envoi loyer au gestionnaire </label>
                                                <select class="form-select fs--1" id="envoi_loye_gest" name="envoi_loye_gest">
                                                    <option value="1" <?= ($lot->envoi_loye_gest == 1) ? 'selected' : ''; ?>>Oui</option>
                                                    <option value="0" <?= isset($lot->envoi_loye_gest) ? (($lot->envoi_loye_gest == 0) ? 'selected' : '') : 'selected'; ?>>Non</option>
                                                </select>
                                                <div class="help"></div>&nbsp;
                                            </div>
                                        </div>


                                    </tr>
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal doc -->

<div class="modal fade" id="modalAjoutLot" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadLot">

            </div>
        </div>
    </div>
</div>

<?php
$produit_default = array();
foreach ($produitcli as $produits) {
    $produit_default[] = array(
        'id' => $produits->produitcli_id,
        'text' => $produits->produitcli_libelle,
    );
}

if ($page == "form") {
    $edit_exist = explode(',', $lot->produitcli_id);
    $produit_array = array();
    $items = array();
    foreach ($produit_default as $key) {
        if (in_array($key['id'], $edit_exist)) {
            $produit_array[] = array(
                'id' => $key['id'],
                'text' => $key['text'],
            );
        } else {
            $items[] = array(
                'id' => $key['id'],
                'text' => $key['text'],
            );
        }
    }
    $produit_default = $items;
}
?>

<script>
    $(document).ready(function() {
        flatpickr(".calendar", {
            "locale": "fr",
            enableTime: false,
            dateFormat: "d-m-Y"
        });

        $('#cliLot_iban_client').keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });

        $(document).on('change', '#cliLot_encaiss_loyer', function(e) {
            var selected_option = $('#cliLot_encaiss_loyer').val();
            if (selected_option == "Compte client") {
                $(".clvIban").hide();
                $(".clientiban").show();
            } else {
                $(".clientiban").hide();
                $(".clvIban").show();
            }
        });

        var selected_option = $('#cliLot_encaiss_loyer').val();
        if (selected_option == "Compte client") {
            $(".clvIban").hide();
            $(".clientiban").show();
        } else {
            $(".clientiban").hide();
            $(".clvIban").show();
        }
    });

    var defiscalisation = $('#defiscalisation').val();
    if (defiscalisation == "NON") {
        $("#fin_defiscalisation").hide();
        $("#non_fin_defiscalisation").show();
    } else {
        $("#non_fin_defiscalisation").hide();
        $("#fin_defiscalisation").show();
    }

    var selected_prgm = $('#progm_id').val();

    if (selected_prgm == 309) {
        $('.alertPrgm').addClass('d-none');
        $('.adresse-prgm').addClass('d-none');
        $('.adresse-lot').removeClass('d-none');
    } else {
        $('.alertPrgm').removeClass('d-none');
        $('.adresse-prgm').removeClass('d-none');
        $('.adresse-lot').addClass('d-none');
    }

    $(document).on('change', '#defiscalisation', function(e) {
        var defiscalisation = $('#defiscalisation').val();
        if (defiscalisation == "NON") {
            $("#fin_defiscalisation").hide();
            $("#non_fin_defiscalisation").show();
        } else {
            $("#non_fin_defiscalisation").hide();
            $("#fin_defiscalisation").show();
        }
    });

    var produit_default = <?= json_encode($produit_default) ?>;

    var element = document.querySelector('.js-choice');
    var choices = new Choices(element, {
        removeItems: true,
        removeItemButton: true,
        choices: produit_default.map((item) => ({
            value: item.id,
            label: item.text
        }))
    });

    <?php if ($page == "form") : ?>
        var liste_produit = <?= json_encode($produit_array); ?>;
        //choices.removeActiveItems();
        choices.setValue(liste_produit.map((item) => ({
            value: item.id,
            label: item.text
        })));
    <?php endif; ?>

    // $('#cliLot_encaiss_loyer').trigger('change');

    $(document).on('change', '#typelot_id', function(e) {
        var selected_option = $('#typelot_id').val();
        if (selected_option == 7) {
            $("#rembst_tom").val('NON');
            $("#cliLot_encaiss_loyer").val('Plus de loyer car plus à bail');
            $("#facture_loyer").val('Plus de facture car plus à bail');
            $("#gestionnaire_id").val(243);
        } else {
            $("#rembst_tom").val('OUI');
            $("#cliLot_encaiss_loyer").val('Compte Celavigestion');
            $("#facture_loyer").val('NON');
            $("#gestionnaire_id").val(2);
        }
    });
</script>