<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class="px-2">

		<button type="button" data-id="<?= $lot->cliLot_id ?>" id="btn-update-lot" class="btn btn-sm btn-primary btn-update-client btn_limit_access">
			<span class="fas fa-edit fas me-1"></span>
			<span> Modifier </span>
		</button>

	</div>
</div>
<?php $iban = $rib->rib_iban;
$bic = $rib->rib_bic; ?>
<div class="contenair-content">
	<div class="col-12 p-0 py-1">
		<div class="row m-0">
			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Identification</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Dossier : </th>
									<th class="pe-0 text-end">
										<?php echo str_pad($lot->dossier_id, 4, '0', STR_PAD_LEFT); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Programme : </th>
									<th class="pe-0 text-end">
										<?php echo $lot->progm_nom ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 1 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_id == 309 ? $lot->cliLot_adresse1 : $lot->progm_adresse1 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 2 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_id == 309 ? $lot->cliLot_adresse2 : $lot->progm_adresse2 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 3 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_id == 309 ? $lot->cliLot_adresse3 : $lot->progm_adresse3 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Code postal : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_id == 309 ? $lot->cliLot_cp : $lot->progm_cp ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Ville : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_id == 309 ? $lot->cliLot_ville : $lot->progm_ville ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0 pb-0">Pays : </th>
									<th class="pe-0 text-end pb-0">
										<?= $lot->progm_id == 309 ? $lot->cliLot_pays : $lot->progm_pays ?>
									</th>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Informations du lot</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<?php
									$type = $lot->cliLot_principale;
									$value;
									if ($type == 1) {
										$value = "Oui";
									} else {
										$value = "Non";
									}
									?>
									<th class="ps-0">Lot principal : </th>
									<th class="pe-0 text-end">
										<?= $value ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Type de bail : </th>
									<th class="pe-0 text-end">
										<?php foreach ($typelot as $typelots) : ?>
											<?php echo isset($lot->typelot_id) ? (($lot->typelot_id !== NULL) ? (($lot->typelot_id == $typelots->typelot_id) ? $typelots->typelot_libelle : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Typologie : </th>
									<th class="pe-0 text-end">
										<?php foreach ($typologie as $typologie) : ?>
											<?php echo isset($lot->typologielot_id) ? (($lot->typologielot_id !== NULL) ? (($lot->typologielot_id == $typologie->typologielot_id) ? $typologie->typologielot_libelle : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Prestations souscrites : </th>
									<th class="pe-0 text-end">
										<?php
										$produitcli_id = explode(',', $lot->produitcli_id);
										$filtre_produit = array_filter($produitcli, function ($produit_row) use ($produitcli_id) {
											return in_array($produit_row->produitcli_id, $produitcli_id);
										});
										$nom = array();
										foreach ($filtre_produit as $key => $value) {
											array_push($nom, $value->produitcli_libelle);
										}
										echo implode(', ', $nom);
										?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Activité : </th>
									<th class="pe-0 text-end">
										<?= $lot->activite ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Défiscalisation : </th>
									<th class="pe-0 text-end">
										<?= $lot->defiscalisation ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Fin défiscalisation : </th>
									<th class="pe-0 text-end">
										<?= $lot->fin_defiscalisation == NULL || $lot->fin_defiscalisation == '0000-00-00' ? 'N/A' : date("d/m/Y", strtotime($lot->fin_defiscalisation)) ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Remboursement TOM : </th>
									<th class="pe-0 text-end">
										<?= $lot->rembst_tom ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Comptabilité : </th>
									<th class="pe-0 text-end">
										<?= $lot->comptabilite ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Numéro du lot : </th>
									<th class="pe-0 text-end">
										<?= $lot->cliLot_num ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Numéro de parking : </th>
									<th class="pe-0 text-end">
										<?= $lot->cliLot_num_parking ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col  p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Informations paiements loyers</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Encaissement des loyers : </th>
									<th class="pe-0 pt-2 text-end">
										<?= $lot->cliLot_encaiss_loyer ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Mode de paiement : </th>
									<th class="pe-0 pt-2 text-end">
										<?= $lot->cliLot_mode_paiement ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> RIB : </th>
									<th class="pe-0 pt-2 text-end">
										<?php if(!empty($lot->rib)): ?>
			                                <?php 
			                                    $infoFichier = pathinfo($lot->rib);
			                                    $fichier = $infoFichier['filename'].'.'.$infoFichier['extension'];
			                                 ?>

				                            <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= addslashes(base_url() . $lot->rib) ?>')" data-url="<?= base_url($lot->rib) ?>" style="cursor: pointer;"><?= $fichier ?></span>
			                            <?php endif ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										<?= ($lot->cliLot_encaiss_loyer == "Compte Celavigestion") ? "IBAN Celavigestion :" : "IBAN client : " ?>
									</th>
									<th class="pe-0 pt-2 text-end">
										<?php if ($lot->cliLot_encaiss_loyer == "Compte Celavigestion") : ?>
											<?= formatIBAN($iban) ?>
										<?php else :  ?>
											<?= !empty($lot->cliLot_iban_client) ? formatIBAN($lot->cliLot_iban_client) : '' ?>
										<?php endif ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										<?= ($lot->cliLot_encaiss_loyer == "Compte client") ? "BIC client :" : "BIC Celavigestion : " ?>
									</th>
									<th class="pe-0 pt-2 text-end">
										<?php if ($lot->cliLot_encaiss_loyer == "Compte Celavigestion") : ?>
											<?= $bic ?>
										<?php else :  ?>
											<?= !empty($lot->cliLot_bic_client) ? $lot->cliLot_bic_client : '' ?>
										<?php endif ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">TVA : </th>
									<th class="pe-0 text-end">
										<?= $lot->tva ?>
									</th>
								</tr>
								<!-- <tr class="border-bottom">
									<th class="ps-0">Facture loyer : </th>
									<th class="pe-0 text-end">
										<?= $lot->facture_loyer ?>
									</th>
								</tr> -->
								<tr class="border-bottom">
									<th class="ps-0">Facture charge : </th>
									<th class="pe-0 text-end">
										<?= $lot->facture_charge ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Facture TOM : </th>
									<th class="pe-0 text-end">
										<?= $lot->facture_tom ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Gestionnaire et Syndic</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Gestionnaire : </th>
									<th class="pe-0 text-end">
										<?php foreach ($gestionnaire as $gestionnaires) : ?>
											<?php echo isset($lot->gestionnaire_id) ? (($lot->gestionnaire_id !== NULL) ? (($lot->gestionnaire_id == $gestionnaires->gestionnaire_id) ? $gestionnaires->gestionnaire_nom : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Syndic : </th>
									<th class="pe-0 text-end">
										<?php foreach ($syndicat as $syndicats) : ?>
											<?php echo isset($lot->syndicat_id) ? (($lot->syndicat_id !== NULL) ? (($lot->syndicat_id  == $syndicats->syndicat_id) ? $syndicats->syndicat_nom : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<table class="table table-borderless fs--1 mb-0 mt-2">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Envoi loyer au gestionnaire : </th>
									<th class="pe-0 text-end">
										<?php echo isset($lot->envoi_loye_gest) ? (($lot->envoi_loye_gest == 1) ? 'Oui' : 'Non') : 'Non'; ?>
									</th>
								</tr>

							</tbody>
						</table>

					</div>
				</div>

			</div>

		</div>

	</div>
</div>
<div class="contenair-content">
	<div class="col-12 p-0 py-1">
		<div class="row m-0">
			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0 text-center">Demande d'informations au locataire Exploitant</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<button class="btn btn-sm btn-primary m-0 genererDocExploitant" data-id="<?= $lot->cliLot_id ?>">
											<span> Générer le document </span>
										</button>
										<button class="btn btn-sm btn-primary m-0 envoiExploitant" data-lot="<?= $lot->cliLot_id ?>" data-id="<?= isset($document->doc_lot_id) ? $document->doc_lot_id : "" ?>">
											<span> Envoyer le document </span>
										</button>
									</th>
								</tr>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<?php
										$nom_fichier = "";
										if (!empty($document)) {
											$fileExploitant = explode("/", $document->doc_lot_path);
											$nom_fichier = $fileExploitant[6];
										} else {
											$nom_fichier = "Pas de document généré";
										}
										?>
										<?php if ($nom_fichier == "Pas de document généré") : ?>
											<span><i><?= $nom_fichier ?></i></span>
										<?php else : ?>
											<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $document->doc_lot_path ?>')" data-url="<?= base_url($document->doc_lot_path) ?>" style="cursor: pointer;"><?= $nom_fichier ?></span>
										<?php endif ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0 text-center">
										<?php if (isset($document->util_id)) : ?>
											<i>Envoi par mail effectué le <?= date("d/m/Y H:i:s", strtotime($document->doc_date_envoi)) ?> par <?= $document->util_prenom ?></i>
										<?php endif ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0 text-center">Demande d'informations au syndic</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<button class="btn btn-sm btn-primary m-0 genererDocSyndic" data-id="<?= $lot->cliLot_id ?>">
											<span> Génrérer le document </span>
										</button>
										<button class="btn btn-sm btn-primary m-0 envoiSyndic" data-lot="<?= $lot->cliLot_id ?>" data-id="<?= isset($doc_syndic->doc_syndic_id) ? $doc_syndic->doc_syndic_id : "" ?>">
											<span> Envoyer le document </span>
										</button>
									</th>
								</tr>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<?php
										$nom_fichier_syndic = "";
										if (!empty($doc_syndic)) {
											$fileSyndic = explode("/", $doc_syndic->doc_syndic_path);
											$nom_fichier_syndic = $fileSyndic[6];
										} else {
											$nom_fichier_syndic = "Pas de document généré";
										}
										?>
										<?php if ($nom_fichier_syndic == "Pas de document généré") : ?>
											<span><i><?= $nom_fichier_syndic ?></i></span>
										<?php else : ?>
											<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $doc_syndic->doc_syndic_path ?>')" data-url="<?= base_url($doc_syndic->doc_syndic_path) ?>" style="cursor: pointer;"><?= $nom_fichier_syndic ?></span>
										<?php endif ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0 text-center">
										<?php if (isset($doc_syndic->util_id)) : ?>
											<i>Envoi par mail effectué le <?= date("d/m/Y H:i:s", strtotime($doc_syndic->doc_syndic_date_envoi)) ?> par <?= $doc_syndic->util_prenom ?></i>
										<?php endif ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1 d-none">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0 text-center">Demande d'informations au SIE</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<button class="btn btn-sm btn-primary m-0 genererDocSie" data-id="<?= $lot->cliLot_id ?>">
											<span> Générer le document </span>
										</button>
										<button class="btn btn-sm btn-primary m-0 envoiSie" data-lot="<?= $lot->cliLot_id ?>" data-id="<?= isset($doc_sie->doc_sie_id) ? $doc_sie->doc_sie_id : "" ?>">
											<span> Envoyer le document </span>
										</button>
									</th>
								</tr>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<?php
										$nom_fichier_sie = "";
										if (!empty($doc_sie)) {
											$fileSie = explode("/", $doc_sie->doc_sie_path);
											$nom_fichier_sie = $fileSie[6];
										} else {
											$nom_fichier_sie = "Pas de document généré";
										}
										?>
										<?php if ($nom_fichier_sie == "Pas de document généré") : ?>
											<span><i><?= $nom_fichier_sie ?></i></span>
										<?php else : ?>
											<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $doc_sie->doc_sie_path ?>')" data-url="<?= base_url($doc_sie->doc_sie_path) ?>" style="cursor: pointer;"><?= $nom_fichier_sie ?></span>
										<?php endif ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0">
										<?php if (isset($doc_sie->util_id)) : ?>
											<i>Envoi par mail effectué le <?= date("d/m/Y H:i:s", strtotime($doc_sie->doc_sie_date_envoi)) ?> par <?= $doc_sie->util_prenom ?></i>
										<?php endif ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1 d-none">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0 text-center">Demande d'informations au SIP</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<button class="btn btn-sm btn-primary m-0 genererDocSip" data-id="<?= $lot->cliLot_id ?>">
											<span> Générer le document </span>
										</button>
										<button class="btn btn-sm btn-primary m-0 envoiSip" data-lot="<?= $lot->cliLot_id ?>" data-id="<?= isset($doc_sip->doc_sip_id) ? $doc_sip->doc_sip_id : "" ?>">
											<span> Envoyer le document </span>
										</button>
									</th>
								</tr>
								<tr class="border-bottom text-center">
									<th class="ps-0">
										<?php
										$nom_fichier_sip = "";
										if (!empty($doc_sip)) {
											$fileSip = explode("/", $doc_sip->doc_sip_path);
											$nom_fichier_sip = $fileSip[6];
										} else {
											$nom_fichier_sip = "Pas de document généré";
										}
										?>
										<?php if ($nom_fichier_sip == "Pas de document généré") : ?>
											<span><i><?= $nom_fichier_sip ?></i></span>
										<?php else : ?>
											<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $doc_sip->doc_sip_path ?>')" data-url="<?= base_url($doc_sip->doc_sip_path) ?>" style="cursor: pointer;"><?= $nom_fichier_sip ?></span>
										<?php endif ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0">
										<?php if (isset($doc_sip->util_id)) : ?>
											<i>Envoi par mail effectué le <?= date("d/m/Y H:i:s", strtotime($doc_sip->doc_sip_date_envoi)) ?> par <?= $doc_sip->util_prenom ?></i>
										<?php endif ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	access_right("*", [1, 3]);
</script>