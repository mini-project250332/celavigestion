<div class="contenair-title" style="">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Liste des mandats</h5>
        </div>

        <input type="hidden" id="cliLot_id" value="<?= $cliLot_id ?>" />
        <input type="hidden" id="client_id" value="<?= $client_id ?>" />

        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 <?= $etat_mandat == NULL ||  $etat_mandat == 5 || $etat_mandat == 6 || $etat_mandat == 7 ? 'ajoutMandat' : 'alertMandat' ?>" data-id="<?= $cliLot_id ?>" data-client="<?= $client_id ?>">
                    <span class="fas fa-plus me-1"></span>
                    <span> Mandat </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left">


        <div class="row m-0 px-2">
            <div class="col">

                <?php 
                    $verify_identite = ($client->check_identite == 1) ? true : false;
                ?>
                <div class="">
                    <div class="form-group form-check">
                        <input data-context="mandats" data-client_id="<?=$client->client_id;?>" type="checkbox" class="form-check-input checkbox_check_identite_mandat" id="checkbox_check_identite_mandat" <?=($verify_identite) ? 'checked' : '';?>>
                        <label class="form-check-label" for="check_identite" style="font-size: 1rem !important;">
                            J’ai vérifié l’identité du signataire de notre mandat avec sa pièce d’identité.
                        </label>
                    </div>

                    <div class="mb-2">
                        <span class="fst-italic fs--1">
                            <?php if(isset($data_verification_identite) && !empty($data_verification_identite)): ?>
                                <?php $dernierAction = $data_verification_identite[count($data_verification_identite) - 1]; ?>
                                <?=($dernierAction->histvi_action == 1) ? 'coché' : 'décoché';?> 
                                par
                                <?=$dernierAction->util_prenom.' '.$dernierAction->util_nom;?>
                                le
                                <?=date('d/m/Y h:i',strtotime($dernierAction->histvi_date));?>
                            <?php endif;?>
                        </span>
                    </div>
                </div>

            </div>
        </div>
        <hr class="my-0">

        <?php if (empty($mandat)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun mandat créé
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="table-responsive">
                <table class="table table-hover fs--1">
                    <thead class="text-white">
                        <tr class="text-center">
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Numéro
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Créé le
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Envoyé le
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Signé le
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Montant (€ HT)
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Clôturé le
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Etat
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-200 fs--1">
                        <?php foreach ($mandat as $mandats) : ?>
                            <tr class="apercu_docu" id="rowdoc-<?= $mandats->mandat_id ?>">
                                <td class="text-center bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $mandats->mandat_id ?>" onclick="<?= ($mandats->etat_mandat_id == 5 || $mandats->etat_mandat_id == 6) ?  'apercuFicJustificatif(' . $mandats->mandat_id . ')' : 'apercuMandat(' . $mandats->mandat_id . ')' ?>">
                                    <?= str_pad($mandats->mandat_id, 4, '0', STR_PAD_LEFT) ?>
                                </td>
                                <td class="text-center"><?= date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_creation))) ?></td>
                                <td class="text-center"><?= isset($mandats->mandat_date_envoi) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_envoi))) : '-' ?></td>
                                <td class="text-center"><?= isset($mandats->mandat_date_signature) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_signature))) : '-' ?></td>
                                <td class="text-center"><?= $mandats->mandat_montant_ht ?></td>
                                <td class="text-center"><?= isset($mandats->mandat_date_fin) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_fin))) : '-' ?></td>
                                <td class="text-center"><?= $mandats->etat_mandat_libelle ?></td>
                                <td class="text-center">
                                    <div class="btn-group fs--1">
                                        <span class="btn btn-sm btn-outline-warning rounded-pill updatemandatCli <?= $mandats->etat_mandat_id != 1 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier" data-mandat="<?= $mandats->mandat_id ?>" data-id="<?= $cliLot_id ?>" data-client="<?= $client_id ?>">
                                            <i class="fas fa-edit"></i>
                                        </span>&nbsp;
                                        <span class="btn btn-sm <?= $mandats->etat_mandat_id == 1 || $mandats->etat_mandat_id == 2 ? 'btn-danger' : 'btn-secondary' ?> rounded-pill">
                                            <i class="<?= $mandats->etat_mandat_id == 1 || $mandats->etat_mandat_id == 2 ? '' : 'text-danger' ?> far fa-file-pdf" onclick="forceDownload('<?= base_url() . (($mandats->etat_mandat_id == 5 || $mandats->etat_mandat_id == 6) ? $mandats->mandat_fichier_justificatif : $mandats->mandat_path) ?>')" data-url="<?= base_url($mandats->mandat_path) ?>">
                                            </i>
                                        </span>&nbsp;
                                        <span class="btn btn-sm btn-outline-secondary sendMandatCli rounded-pill <?= $mandats->etat_mandat_id != 1  ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-file-import"></i>
                                        </span>
                                        <span class="btn btn-sm btn-outline-primary rounded-pill signerMandatCli <?= $mandats->etat_mandat_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Signer" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-file-signature"></i>
                                        </span>&nbsp;
                                        <!-- <span class="btn btn-sm btn-outline-warning rounded-pill clos_sans_suiteMandat <?= $mandats->etat_mandat_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Clos sans suite"
                                            data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-archive"></i>
                                        </span> -->
                                        <span class="btn btn-sm btn-outline-danger rounded-pill suspensionMandat p-1 <?= $mandats->etat_mandat_id != 3 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Suspendre" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-stop-circle fs-1"></i>
                                        </span>&nbsp;
                                        <span class="btn btn-sm btn-danger rounded-pill annulersuspensionMandat <?= $mandats->etat_mandat_id != 7 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Annuler suspension" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-window-close "></i>
                                        </span>&nbsp;
                                        <span class="btn btn-sm btn-outline-warning rounded-pill retracterMandat <?= $mandats->etat_mandat_id != 3 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Retracter" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-file-archive"></i>
                                        </span>&nbsp;
                                        <span class="btn btn-sm btn-outline-danger rounded-pill cloturerMandat <?= $mandats->etat_mandat_id != 3 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Clôturer" data-id="<?= $mandats->mandat_id ?>">
                                            <i class="fas fa-hand-paper"></i>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
    <div class="splitter m-2">

    </div>
    <div class="col-5 panel-right">
        <div id="apercu_list_docMandat">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercuMandat">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion(["1", "2", "3"], "ajoutMandat");
    only_visuel_gestion(["1", "2", "3"], "alertMandat");
    only_visuel_gestion(["1", "2", "3"], "sendMandatCli");
    only_visuel_gestion(["1", "2", "3"], "signerMandatCli");
    only_visuel_gestion(["1", "3"], "clos_sans_suiteMandat");
    only_visuel_gestion(["1", "3"], "cloturerMandat");
    only_visuel_gestion(["1", "3"], "retracterMandat");
    only_visuel_gestion(["1", "3"], "suspensionMandat");

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });
</script>