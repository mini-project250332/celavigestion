<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Information mandat</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>
                            <span class="alert alert-warning">
                                <p class="fs--1 text-center">
                                    Il est essentiel de restreindre la création d'un nouveau mandat tant que le mandat précédent n'a pas été clôturé, rétracté ou suspendu.
                                </p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>