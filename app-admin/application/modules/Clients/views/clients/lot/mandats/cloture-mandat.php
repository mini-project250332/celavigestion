<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Clôturer un mandat</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Clients/ClotureMandat', array('method' => "post", 'class' => 'px-2', 'id' => 'ClotureMandat')); ?>
<input type="hidden" name="mandat_id" id="mandat_id" value="<?= $mandat_id; ?>">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
<input type="hidden" name="client_id" id="client_id" value="<?= $client_id; ?>">
<input type="hidden" name="bail_id" id="bail_id_mandat" value="<?= isset($bail->bail_id) ? $bail->bail_id : "" ; ?>">
<input type="hidden" name="pdl_id" id="pdl_id" value="<?= isset($bail->pdl_id) ? $bail->pdl_id : ""; ?>">
<input type="hidden" name="bail_loyer_variable" id="bail_loyer_variable" value="<?= isset($bail->bail_loyer_variable) ? $bail->bail_loyer_variable : ""; ?>">

<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Type de clôture : </label>
                    <select class="form-select" id="type_cloture_id" name="type_cloture_id">
                        <?php foreach ($type_cloture as $key => $value) : ?>
                            <option value="<?= $value->type_cloture_id ?>"><?= $value->type_cloture_libelle ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Date de la demande : </label>
                    <input type="date" class="form-control" id="mandat_date_demande" name="mandat_date_demande" value="<?=date('Y-m-d')?>">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Date de fin présumée : </label>
                    <input type="date" class="form-control" id="mandat_date_fin" name="mandat_date_fin" value="<?= $default_date ?>">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Fichier justificatif : </label>
                    <input type="file" class="form-control" id="mandat_fichier_justificatif" name="mandat_fichier_justificatif">
                    <div class="text-danger help error_file"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
    $(document).ready(function() {
        $(document).on('change', '#type_cloture_id', function(e) {
            const date = new Date();
            var selected_option = $('#type_cloture_id').val();
            var date_demande = $('#mandat_date_demande').val();
            var year = date.getFullYear();
            var formattedDate = year + "-" + '12' + "-" + '31';
            if (selected_option == 2 && date_demande !== "") {
                $("#mandat_date_fin").val(date_demande);
            } else {
                $("#mandat_date_fin").val(formattedDate);
            }
        });
    });

    $(document).on('keyup', '#mandat_date_demande', function(e) {
        e.preventDefault();
        const date = new Date();
        var day = date.setDate(01);
        var month = date.setMonth(07);
        var year = date.getFullYear();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }

        var default_date = year + "-" + '12' + "-" + '31';
        var July = year + "-" + '07' + "-" + '01';
        var formattedDate = (year + 1) + "-" + '12' + "-" + '31';
        var dateDemande = $('#mandat_date_demande').val();
        var selected_option = $('#type_cloture_id').val();

        if (dateDemande >= July) {
            $('#mandat_date_fin').val(formattedDate);
        } else {
            $('#mandat_date_fin').val(default_date);
        }

        // if (selected_option == 2 && date_demande !== "") {
        //     $("#mandat_date_fin").val(date_demande);
        // } else {
        //     $("#mandat_date_fin").val(formattedDate);
        // }

    });
</script>