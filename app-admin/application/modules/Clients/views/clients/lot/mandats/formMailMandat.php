<style>
    .badge {
        cursor: pointer;
    }
</style>
<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Envoye du mail (Proposition mandat)</h5>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <select class="form-select" name="emeteur_mandat" id="emeteur_mandat">
                                <?php foreach ($liste_emetteur as $key => $value) : ?>
                                    <option value="<?= $value->mess_id ?>" <?= $default_user == $value->util_id ? 'selected' : '' ?>>
                                        <?= $value->util_prenom ?>&nbsp;<?= $value->util_nom ?>&nbsp;(<?= $value->mess_email ?>)
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataires :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_mandat" value="<?= $destinataire->cnctGestionnaire_email1 ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataire en copie :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_mandat_copie" value="">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object_mandat" value="<?= $object_mail ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message_text_mandat" style="height : 315px;"><?= $mailMessage ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <label data-width="200" for="recipient-name" class="col-form-label d-none" id="signature_mandat">Aucune</label>
                            <img src="" alt="" id="image_signature" style="width:400px; height:130px;" class="d-none">
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-2">
                                <label data-width="200" class="col-form-label" style="font-size: 13.5px!important;">Fichiers joints :</label>
                            </div>
                            <div class="col-10 mt-1 list_doc">
                                <?php if (!empty($document)) : ?>
                                    <div class="line-<?= $document->mandat_cli_id ?> doc">
                                        <input class="document_mandat" type="hidden" value="<?= $document->mandat_cli_id ?>">
                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $document->mandat_cli_path ?>','<?= str_pad($document->mandat_cli_id, 5, '0', STR_PAD_LEFT) ?> <?= $document->type_mandat_libelle ?>')" data-url="<?= base_url($document->mandat_cli_path) ?>"> <?= str_pad($document->mandat_cli_id, 5, '0', STR_PAD_LEFT) ?> <?= $document->type_mandat_libelle ?></span>
                                        <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefile" data-id="<?= $document->mandat_cli_id ?>">.</span><br>
                                    </div>
                                <?php else : ?>
                                    <p style="font-weight: 500;font-size: 14px; margin-left: -26px;">Pas de fichier joints</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary" id="sendMailMandat">Envoyer</button>
    </div>
</div>