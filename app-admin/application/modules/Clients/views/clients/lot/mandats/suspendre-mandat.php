<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Suspension du mandat</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Clients/SuspendreMandat', array('method' => "post", 'class' => 'px-2', 'id' => 'SuspendreMandat')); ?>
<input type="hidden" name="mandat_id" id="mandat_id" value="<?= $mandat_id; ?>">
<input type="hidden" name="mandat_date_signature" id="mandat_date_signature" value="<?= $mandat->mandat_date_signature; ?>">
<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
<input type="hidden" name="client_id" id="client_id" value="<?= $client_id; ?>">
<input type="hidden" name="bail_id" id="bail_id_mandat" value="<?= isset($bail->bail_id) ? $bail->bail_id : "" ; ?>">
<input type="hidden" name="pdl_id" id="pdl_id" value="<?= isset($bail->pdl_id) ? $bail->pdl_id : ""; ?>">
<input type="hidden" name="bail_loyer_variable" id="bail_loyer_variable" value="<?= isset($bail->bail_loyer_variable) ? $bail->bail_loyer_variable : ""; ?>">

<div class="modal-body">
    <div class="row m-0">
        <div class="col">
            <div class="form-group inline">
                <div class="text-center fs--1">
                    <p class="m-0">
                        <i class="fas fa-exclamation-triangle"></i>
                        Attention, vous allez suspendre le mandat, veuillez indiquer la raison de la suspension
                    </p>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Raison de la suspension: </label>
                    <select class="form-select fs--1" id="mandat_motif_suspension" name="mandat_motif_suspension">
                        <option value="Impayé">Impayé</option>
                    </select>
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Valider la suspension</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>

</script>