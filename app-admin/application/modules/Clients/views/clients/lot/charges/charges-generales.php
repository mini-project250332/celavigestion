<div class="contenair-title">
    <input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_charge">
    <div class="row m-0" style="width: 50%">
        <div class="col-7">
            <h5 class="fs-1">Documents Dépenses</h5>
        </div>
        <div class="col-3">
            <select class="no_effect form-select fs--1 m-1" id="annee_charges" style="width: 96%">
                <?php for ($i = intval(date("Y") + 1); $i >= intval(date("Y")) - 1; $i--) : ?>
                    <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                        <?= $i ?>
                    </option>
                <?php endfor ?>
                <option value="0">Années antérieures</option>
            </select>
            <div class="fs--1 text-danger text-center"><i><?= empty($date_signature) ? "aucun mandat signé !"  : "" ?></i></div>
        </div>
        <div class="col-2">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $cliLot_id ?>" id="ajoutDocCharge">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="h-100 w-100" id="container-charge-document">


</div>

<!-- modal -->
<div class="modal fade" id="modalAjoutCharge" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadCharge">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    getListDocumentCharge($('#annee_charges').val(), $('#cliLot_id_charge').val());
</script>