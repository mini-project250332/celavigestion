<style>
    .input_charge {
        height: 36px;
        padding: 5px;
    }
</style>
<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left" style="width: 900px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0">
            <div class="col-12" style=" height: calc(25vh - 50px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
                <div class=" col-12">
                    <?php foreach ($document as $documents) { ?>
                        <div class="row apercu_doc" id="docrowcharge-<?= $documents->doc_charge_id ?>">
                            <div class="col-4 pt-2 <?= isset($documents->charge_id) ? "d-none" : "" ?>">
                                <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_charge_id ?>" onclick="apercuDocumentCharge(<?= $documents->doc_charge_id ?>)">
                                    <?= $documents->doc_charge_nom ?>
                                </div>
                            </div>
                            <div class="col-2 text-center <?= isset($documents->charge_id) ? "d-none" : "" ?>">
                                <label for=""><i class="fas fa-user"></i>
                                    <?= $documents->util_prenom ?>
                                </label>
                            </div>
                            <div class="col-2 text-center <?= isset($documents->charge_id) ? "d-none" : "" ?>">
                                <label for=""><i class="fas fa-calendar"></i>
                                    <?= $documents->doc_charge_creation ?>
                                </label>
                            </div>
                            <div class="col-4 text-center">
                                <div class="fs--1 <?= isset($documents->charge_id) ? "d-none" : "" ?>" style="left: 17%;">
                                    <?php
                                    $message = "Document non traité";
                                    if (!empty($documents->doc_charge_traitement)) {
                                        if ($documents->doc_charge_traite == 1) {
                                            $message = "Document traité par " . $documents->util_prenom . " le " . $documents->doc_charge_traitement;
                                        } else {
                                            $message = "Document annulé par " . $documents->util_prenom . " le " . $documents->doc_charge_traitement;
                                        }
                                    }
                                    ?>
                                    <button class="btn btn-sm <?= ($documents->doc_charge_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill  btnTraiteCharge" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>" data-id=" <?= $documents->doc_charge_id ?>">
                                        <i class="fas fa-check"></i>
                                    </button>
                                    <span class="btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocCharge" data-id="<?= $documents->doc_charge_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer" data-lot="<?= $cliLot_id; ?>" data-doc_charge_annee="<?= $documents->doc_charge_annee ?>">
                                        <i class="far fa-edit"></i>
                                    </span>
                                    <span class="btn btn-sm btn-outline-primary rounded-pill">
                                        <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_charge_path ?>','<?= trim($documents->doc_charge_nom) ?>')" data-url="<?= base_url($documents->doc_charge_path) ?>">
                                        </i>
                                    </span>
                                    <span class="btn btn-sm btn-outline-secondary rounded-pill joinCharge" data-id="<?= $documents->doc_charge_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Rattacher à une dépense" data-lot="<?= $cliLot_id; ?>" data-doc_charge_annee="<?= $documents->doc_charge_annee ?>">
                                        <i class="fas fa-link"></i>
                                    </span>
                                    <span class="btn btn-sm btn-outline-danger rounded-pill btnSuppDocCharge" data-id="<?= $documents->doc_charge_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                        <i class="far fa-trash-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr class="my-0"><br>
        <!-- information charges -->
        <div class="row">
            <div class="col">
                <h5>Dépenses <span><?= $annee == 0 ? "année antérieure" : $annee ?> : </span></h5>
            </div>
            <div class="col text-end">
                <button class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#ModalCharge" id="add_depense">
                    <i class="fas fa-plus me-1"></i> Dépense
                </button>
            </div>
        </div>
        <div class="py-2 row">
            <div class="col">

                Clôture de la saisie par le propriétaire :
                <?php if (!empty($cloture)) : ?>
                    <span class="badge badge-soft-info p-1" style="font-size: 14px"> Effectuée le <?= date("d/m/Y", strtotime($cloture->charge_date_cloture)) ?> </span> &nbsp;
                    <button class="btn btn-sm btn-danger" id="annuler_saisie" data-id="<?= $cloture->cliLot_id ?>" data-annee="<?= $cloture->annee ?>">
                        <i class="fas fa-times"></i>
                    </button>
                    <div>
                        <span style="font-weight: 500 !important;">Commentaire :</span>
                        <div class="alert alert-info border-2 p-2 mt-1" role="alert">

                            <p class="m-0 text-break"><?= $cloture->c_charge_commentaire ?></p>
                        </div>
                    </div>



                <?php else : ?>
                    <span class="badge badge-soft-info p-1" style="font-size: 14px"> Non effectuée </span> &nbsp;
                    <button class="btn btn-sm btn-success" id="valider_saisie">
                        <i class="fas fa-check"></i>
                    </button>
                <?php endif; ?>

            </div>
        </div>

        <!-- tableau charge -->
        <div id="container-charge-table">

        </div>
    </div>
    <div class="split m-2">

    </div>
    <div class="col-6 panel-right">
        <div id="apercuChargedoc">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercuCharge">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="ModalCharge">
    <div class="modal-dialog  modal-lg  modal-dialog-centered">
        <div class="modal-content">

            <input type="hidden" id="charge_id">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title charge_modal"></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="col">
                    <div class="row">
                        <div class="col-4">
                            <label data-width="250">Type de Facture :</label>
                        </div>
                        <div class="col-8">
                            <select class="form-select form-control" id="type_charge_id">
                                <?php foreach ($type_charge as $key => $value) : ?>
                                    <option value="<?= $value->tpchrg_lot_id ?>">
                                        <?= $value->tpchrg_lot_nom ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label data-width="250">Date de la Facture :</label>
                        </div>
                        <div class="col-8">
                            <input type="date" class="form-control" id="date_facture">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <label data-width="250">Montant TTC de la dépenses :</label>
                        </div>
                        <div class="col-8">
                            <div class="input-group">
                                <input type="number" placeholder="0" class="form-control mb-0 input_charge" id="montant_charge_ttc" style="text-align: right;">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">€</span>
                                </div>
                            </div>

                        </div>
                    </div>


                    <br>
                    <div class="row">
                        <div class="col-4">
                            <label data-width="250">Fichiers :</label>
                        </div>
                        <div class="col-8">
                            <div class="input-group">
                                <input type="file" class="form-control m-1 w-50" id="file_<?= $cliLot_id ?>" name="doc_charge_path" multiple>

                            </div>
                            <div class="name_doc">

                            </div>
                        </div>
                    </div>


                    <br>
                    <div class="row">
                        <div class="col">
                            <table class="table table-hover fs--1">
                                <thead class="text-white" style="background:#2569C3;">
                                    <tr>
                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Montant HT
                                        </th>
                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;">TVA</th>
                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Montant TTC
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-200">
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" placeholder="0" class="form-control mb-0 input_charge auto_effect" id="charge_ht" style="text-align: right;" disabled>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="main-input-group">
                                                <div class="input-group">
                                                    <select id="charge_tva_taux" class="select_tva input_charge form-select mb-0 select auto_effect">
                                                        <?php foreach ($tva as $key => $item) : ?>
                                                            <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                    echo "selected"; ?>>
                                                                <?php echo $item->tva_valeur; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                        <option value="1">Autre</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">%</span>
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-montant-tva">
                                                    <input placeholder="0" type="number" value="0" class="input_charge form-control mb-0 auto_effect" id="charge_tva" style="text-align: right;"><br>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">€</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" placeholder="0" class="form-control mb-0 input_charge auto_effect" id="charge_ttc" style="text-align: right;" value="0" disabled>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" id="addCharge">Enregistrer</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    getListCharge(<?= $cliLot_id; ?>, <?= $annee; ?>);

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });
</script>