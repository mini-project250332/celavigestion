<div class="uploader_file_charge mt-4">

</div>
<div class="clearfix p-t-5 p-b-5">
    <div class="float-left">
    </div>
    <div class="d-flex justify-content-end">
        <button type="button" id="stopped-upload" class="btn btn-danger d-none btn-sm mx-2 text-white">
            <i class="fas fa-times-circle red"></i>
            Annuler
        </button>
        <button type="button" id="add-files-upload_charge" class="btn btn-info btn-sm bold mx-1">
            <i class="fas fa-plus-circle"></i>
            Ajouter des fichiers
        </button>
        <button type="button" id="start-files-upload_charge" class="btn btn-success btn-sm d-none text-white">
            <i class="fas fa-cloud-upload-alt green "></i>
            Démarrer l'envoi
        </button>
    </div>
</div>

<input id="url_upload_charge" type="hidden" name="url_upload_charge" value="<?php echo base_url("Charges/Charges/uploadfileCharges"); ?>">
<input type="hidden" name="cliLot_id_charge" id="cliLot_id_charge" value="<?= $cliLot_id ?>">

<script type="text/javascript">
    $(document).ready(function() {
        var url_upload_charge = $('#url_upload_charge').val();
        $(".uploader_file_charge").pluploadQueue({
            browse_button: 'add-files-upload_charge',
            runtimes: 'html5,flash,silverlight,html4',
            chunk_size: '1mb',
            url: url_upload_charge,
            rename: true,
            unique_names: true,
            sortable: true,
            dragdrop: true,
            flash_swf_url: '<?php echo base_url("assets/js/plupload/js/Moxie.swf"); ?>',
            silverlight_xap_url: '<?php echo base_url("assets/js/plupload/js/Moxie.xap"); ?>',
        });

        var uploader = $('.uploader_file_charge').pluploadQueue();
        uploader.bind('Browse', function() {

        });
        uploader.bind('FilesRemoved', function() {
            if (uploader.files.length > 0) {
                $('#start-files-upload_charge').removeClass('d-none');
            } else {
                $('#start-files-upload_charge').addClass('d-none');
                $('#stopped-upload').addClass('d-none');
            }
        });
        uploader.bind('FilesAdded', function() {
            if (uploader.files.length > 0) {
                $('#start-files-upload_charge').removeClass('d-none');
            } else {
                $('#start-files-upload_charge').addClass('d-none');
                $('#stopped-upload').addClass('d-none');
            }
        });
        uploader.bind('UploadComplete', function() {
            // upload finish

        });
        uploader.bind('FileUploaded', function(response) {
            if (uploader.files.length > 0) {
                if (uploader.files.length == (uploader.total.uploaded + uploader.total.failed)) {
                    var annee = $('#annee_charges').val();
                    var cliLot_id_charge = $('#cliLot_id_charge').val();
                    $("#modalAjoutCharge").modal('hide');
                    $('.modal-backdrop').remove();
                    getListDocumentCharge(annee, cliLot_id_charge);
                }
            }
        });

        $('#start-files-upload_charge').click(function(e) {
            e.preventDefault();
            var check = false;
            if (uploader.files.length > 0) {
                var files_names = {};
                var pathname_url = window.location.pathname;
                var cliLot_id_charge = $('#cliLot_id_charge').val();
                var annee = $('#annee_charges').val();
                $("div.plupload_file_name").each(function(ind, elem) {
                    var content = $(this).html();
                    var infos_file = {};
                    if (content != $.trim('Nom du fichier')) {

                        if ($(this).parent().attr("id") != null) {
                            infos_file.originalName = content;
                            infos_file.fileName = $(this).parent().attr("id");
                            files_names[ind] = infos_file;
                        }
                    }
                });
                $('#stopped-upload').removeClass('d-none');

                uploader.settings.multipart_params = {
                    cliLot_id_charge: cliLot_id_charge,
                    fichiers: files_names, // Injection des noms des fichiers 
                    annee: annee
                };

                uploader.start();
            }
        });

        $('#stopped-upload').click(function(e) {
            uploader.stop();
        });
    });
</script>