<style>
    span.badge-soft-info {
        max-width: 120px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .span_conten {
        display: flex;
        align-items: center;
    }

    .span_conten .fas {
        margin-right: 5px;
    }

    .button-container {
        display: flex;
        align-items: center;
    }
</style>
<input type="hidden" id="annee_charge" value="<?= $annee ?>">

<table class="table table-hover fs--1">
    <thead class="text-white" style="background:#2569C3;">
        <tr>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Type de facture</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Date de la facture</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" colspan="3">Montant</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Date saisie</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Saisie par</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Date validation</th>
            <th class="text-center" style="border-right: 1px solid #ffff; white-space: nowrap;" rowspan="2">Fichiers</th>
            <th class="text-center" style="border-right: 1px solid #ffff;" rowspan="2">Actions</th>
        </tr>
        <tr>
            <th class="text-center" style="border-right: 1px solid #ffff;">HT</th>
            <th class="text-center" style="border-right: 1px solid #ffff;">TVA</th>
            <th class="text-center" style="border-right: 1px solid #ffff;">TTC</th>
        </tr>
    </thead>
    <tbody class="bg-200">
        <?php if (!empty($list_charge)) : ?>
            <?php foreach ($list_charge as $key => $value) : ?>
                <tr id="charge_row-<?= $value->charge_id ?>">
                    <td class="text-center p-2" style="border-right: 1px solid #ffff;"><?= $value->tpchrg_lot_nom ?></td>
                    <td class="text-center p-2" style="border-right: 1px solid #ffff;"><?= date("d/m/Y", strtotime($value->date_facture)); ?></td>
                    <td class="text-end p-2" style="border-right: 1px solid #ffff;"><?= format_number_($value->montant_ht) ?></td>
                    <td class="text-end p-2" style="border-right: 1px solid #ffff;"><?= format_number_($value->tva) ?></td>
                    <td class="text-end p-2" style="border-right: 1px solid #ffff;"><?= format_number_($value->montant_ttc) ?></td>
                    <td class="text-end p-2" style="border-right: 1px solid #ffff;"><?= isset($value->charge_date_saisie) ? date("d/m/Y", strtotime($value->charge_date_saisie)) : ""; ?></td>
                    <td class="text-center p-2" style="border-right: 1px solid #ffff;"><?= isset($value->prenom_util) ? $value->prenom_util : "-" ?></td>
                    <td class="text-center p-2" style="border-right: 1px solid #ffff;"><?= isset($value->charge_date_validation) ? date("d/m/Y", strtotime($value->charge_date_validation)) : "-"; ?></td>
                    <td class="text-left" style="border-right: 1px solid #ffff;">
                        <?php if (!empty($value->doc_charge)) : ?>
                            <?php foreach ($value->doc_charge as $key => $value_charge) : ?>
                                <div class="span_conten">
                                    <i class="fas fa-cloud-download-alt text-info" onclick="forceDownload('<?= base_url() . $value_charge->doc_charge_path ?>')" data-url="<?= base_url($value_charge->doc_charge_path) ?>" style="cursor: pointer;"></i>
                                    <span class="badge rounded-pill badge-soft-info" style="cursor: pointer;" onclick="apercuDocumentCharge(<?= $value_charge->doc_charge_id ?>)">
                                        <?= $value_charge->doc_charge_nom ?>
                                    </span>
                                </div> <br>
                            <?php endforeach; ?>
                        <?php endif;  ?>
                    </td>

                    <td style="border-right: 1px solid #ffff;">
                        <div class="button-container">
                            <button class="btn btn-sm btn-outline-info icon-btn ms-1 btn_info_dep" data-info="<?= $value->c_charge_commentaire ?>" data-langVide="Aucune information">
                                <svg class="svg-inline--fa fa-info fa-w-6" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="info" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" data-fa-i2svg="">
                                    <path fill="currentColor" d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z"></path>
                                </svg>
                            </button>
                            <button class="btn btn-sm btn-outline-primary only_visuel_gestion ms-1" id="modifierDepense" data-bs-toggle="modal" data-bs-target="#ModalCharge" data-charge_id="<?= $value->charge_id ?>">
                                <i class="fas fa-pencil-alt"></i>
                            </button>
                        </div>
                        <div class="button-container mt-1">
                            <?php if ($value->charge_etat_valide == 1) : ?>
                                <button class="btn btn-sm btn-success">
                                    <i class="fas fa-check-double"></i>
                                </button>
                            <?php else : ?>
                                <button class="btn btn-sm btn-secondary only_visuel_gestion ms-1" id="validerDepense" data-lot=" <?= $cliLot_id ?>" data-charge_id="<?= $value->charge_id ?>">
                                    <i class="fas fa-check"></i>
                                </button>
                            <?php endif; ?>
                            <button class="btn btn-sm btn-outline-danger only_visuel_gestion ms-1" id="supprCharge" data-charge_id="<?= $value->charge_id ?>">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td class="text-center fw-bold" colspan="6">Aucune charge pour l'année <?= $annee == 0 ? "antérieure" : $annee ?> </td>
            </tr>
        <?php endif ?>
    </tbody>
    <?php if (!empty($list_charge)) : ?>
        <tfoot class="bg-200">
            <tr>
                <td class="fw-bold text-center" style="border-right: 1px solid #ffff;border-top: 1px solid #ffff;" colspan="2">TOTAL</td>
                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff;"><?= format_number_($totat_ht); ?></td>
                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff;"><?= format_number_($total_tva); ?></td>
                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff;"><?= format_number_($total_ttc); ?></td>
            </tr>
        </tfoot>
    <?php endif ?>
</table>
<script>
    only_visuel_gestion();
</script>