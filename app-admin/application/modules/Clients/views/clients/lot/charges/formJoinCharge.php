<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Rattacher un document à une dépense</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Charges/UpdateChargeDoc', array('method' => "post", 'class' => 'px-2', 'id' => 'UpdateChargeDoc')); ?>


<input type="hidden" name="doc_charge_id" id="doc_charge_id" value="<?= $doc_charge_id; ?> ">
<input type="hidden" name="doc_charge_annee" id="doc_charge_annee" value="<?= $doc_charge_annee; ?> ">


<input type="hidden" name="cliLot_id" id="cliLot_id" value="<?= $cliLot_id; ?>">
<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>
                            <label data-width="100"> Dépenses : </label>
                            <select class="form-select form-control" id="charge_id" name="charge_id">
                                <?php foreach ($charge as $key => $value) : ?>
                                    <option value="<?= $value->charge_id ?>">
                                        <?= $value->tpchrg_lot_nom.' le '.date("d/m/Y", strtotime($value->date_facture)).' ('.$value->montant_ht.' €'.')' ?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>
<?php echo form_close(); ?>