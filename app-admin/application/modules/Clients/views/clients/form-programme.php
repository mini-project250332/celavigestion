<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter un programme</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Setting/Programme/cruProgram',array('method'=>"post", 'class'=>'','id'=>'AddProgram')); ?>
	<?php if(isset($programme->progm_id)):?>
        <input type="hidden" name="progm_id" id="progm_id" value="<?=$programme->progm_id;?>">
    <?php endif;?>

    <input type="hidden" name="action" id="action" value="<?=$action?>">       
    <div class="modal-body">

        <div class="row m-0">
			<div class="col"></div>

    		<div class="col py-2">

    			<div class="card rounded-0 h-100">
	                <div class="card-body">

					<div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Numero </label>
		                        <input type="text" class="form-control" value ="<?= isset($programme->progm_num) ? $programme->progm_num : "" ;?>" id="progm_num" name="progm_num">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Nom *</label>
		                        <input type="text" class="form-control maj" value ="<?= isset($programme->progm_nom) ? $programme->progm_nom : "" ;?>" id="progm_nom" name="progm_nom" data-formatcontrol="true" data-require="true">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 1  </label>
		                        <input type="text" class="form-control" value ="<?= isset($programme->progm_adresse1) ? $programme->progm_adresse1 : "" ;?>" id="progm_adresse1" name="progm_adresse1">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 2 </label>
		                        <input type="text" class="form-control" value ="<?= isset($programme->progm_adresse2) ? $programme->progm_adresse2 : "" ;?>" id="progm_adresse2" name="progm_adresse2">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 3 </label>
		                        <input type="text" class="form-control" value ="<?= isset($programme->progm_adresse3) ? $programme->progm_adresse3 : "" ;?>" id="progm_adresse3" name="progm_adresse3">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
						    <div>
						        <label data-width="200"> Code postal </label>
						        <input type="text" class="form-control" value ="<?= isset($programme->progm_cp) ? $programme->progm_cp : "" ;?>" id="progm_cp" name="progm_cp">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Ville </label>
						        <input type="text" class="form-control maj" value ="<?= isset($programme->progm_ville) ? $programme->progm_ville : "" ;?>" id="progm_ville" name="progm_ville">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Pays </label>
						        <input type="text" class="form-control maj" value ="<?= isset($programme->progm_pays) ? $programme->progm_pays : "" ;?>" id="progm_pays" name="progm_pays">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline switch">
	                        <div>
	                            <label data-width="250"> Statut programme</label>
	                            <div class="form-check form-switch">
	                                <span> Inactif </span>
	                                <input class="form-check-input" type="checkbox" id="progm_etat" name="progm_etat"  <?php echo isset($programme->progm_etat) ? ($programme->progm_etat=="1" ? "checked" :"") : "checked" ;?>>
	                                <span> Actif </span>
	                            </div>
	                        </div>
	                    </div>

		            </div>
		        </div>
    		</div>
    		<div class="col"></div>
    	</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </div>
    </div>
<?php echo form_close(); ?>


 
