<?php echo form_tag('Clients/cruCom', array('method' => "post", 'class' => 'px-2', 'id' => 'cruCom')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <button type="button" data-id="<?= $client_id; ?>" id="annulerAddCom" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler </span>
        </button>

        <button type="submit" id="save-com" class="btn btn-sm btn-primary mx-1 only_visuel_gestion">
            <i class="fas fa-plus me-1"></i>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<?php if (isset($com->comclient_id)): ?>
    <input type="hidden" name="comclient_id" id="comclient_id" value="<?= $com->comclient_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<input type="hidden" name="client_id" id="client_id" value="<?= $client_id; ?>">

<div class="row m-0">
    <div class="col-3">


    </div>
    <div class="col py-2">

        <div class="card rounded-0">
            <div class="card-body">

                <div class="form-group inline">
                    <div>
                        <label data-width="250"> Type de communication : </label>
                        <select class="form-select fs--1" id="type_comprospect_id" name="type_comprospect_id">
                            <?php foreach ($typecom as $type): ?>
                                <option value=" <?= $type->type_comprospect_id; ?>" <?php echo isset($com->type_comprospect_id) ? (($com->type_comprospect_id == $type->type_comprospect_id) ? "selected" : " ") : "" ?>>
                                    <?= $type->type_comprospect_libelle; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <div class="help"></div>
                    </div>
                </div>
                <!-- 
                    <div class="form-group inline">
                        <div>
                            <label data-width="250"> Réalisé(e) par : </label>
                            <select class="form-select fs--1" id="util_id" name="util_id">
                                <?php foreach ($utilisateur as $utilisateurs): ?>
                                    <option value="<?= $utilisateurs->id; ?>" <?php echo isset($com->id) ? (($com->id == $utilisateurs->id) ? "selected" : " ") : "" ?>>
                                        <?= $utilisateurs->prenom; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                -->
                <div class="form-group inline">
                    <div>
                        <label data-width="250"> Message : </label>
                        <textarea class="form-control" id="comclient_texte" name="comclient_texte" style="height: 150px"
                            data-formatcontrol="true"
                            data-require="true"><?= isset($com->comclient_texte) ? $com->comclient_texte : ""; ?></textarea>
                        <div class="help"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-3">


    </div>
</div>
<?php echo form_close(); ?>

<script>only_visuel_gestion()</script>