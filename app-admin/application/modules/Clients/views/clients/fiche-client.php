<div class="contenair-title">
	<div class="px-2">
		<input type="hidden" value="<?= $client->client_id ?>" id="client_id_pour_rafraichir">
		<h5 class="fs-1">Fiche propriétaire : <?= $client->client_nom . ' ' . $client->client_prenom; ?>
			<?= ((trim($client->client_entreprise) != "") ? "[ " . $client->client_entreprise . " ]" : "") ?> (N°
			<?= str_pad($client->client_id, 4, '0', STR_PAD_LEFT); ?>)
		</h5>
	</div>
	<div class="px-2">
		<button class="btn btn-sm btn-outline-primary btnSaisieTempsClient" data-id="<?= $client->client_id ?>">
			<i class="fas fa-clock"></i>
		</button>
		<button id="retourListeClient" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo me-1"></i>
			<span>Retour</span>
		</button>
	</div>
</div>

<div class="contenair-content">
	<ul class="nav nav-tabs" id="clientTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="dossier-tab" data-bs-toggle="tab" href="#tab-dossier" role="tab" aria-controls="tab-dossier" aria-selected="false">Dossiers</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="lots-tab" data-bs-toggle="tab" href="#tab-lots" role="tab" aria-controls="tab-lots" aria-selected="false">Lots</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="document-tab" data-bs-toggle="tab" href="#tab-document" role="tab" aria-controls="tab-document" aria-selected="false">Documents propriétaires</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="communication-tab" data-bs-toggle="tab" href="#tab-communication" role="tab" aria-controls="tab-communication" aria-selected="false">Communication</a>
		</li>
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="clientTabContent">
		<div class="tab-pane fade show active" id="tab-identification" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification/identification'); ?>
		</div>
		<div class="tab-pane fade" id="tab-dossier" role="tabpanel" aria-labelledby="dossier-tab">
			<?php $this->load->view('dossier/dossiers'); ?>
		</div>

		<div class="tab-pane fade" id="tab-lots" role="tabpanel" aria-labelledby="lots-tab">
			<?php $this->load->view('lot/lot'); ?>
		</div>

		<div class="tab-pane fade" id="tab-document" role="tabpanel" aria-labelledby="document-tab">
			<?php $this->load->view('document/documents'); ?>
		</div>

		<div class="tab-pane fade" id="tab-communication" role="tabpanel" aria-labelledby="communication-tab">
			<?php $this->load->view('communication/communication'); ?>
		</div>
	</div>
</div>