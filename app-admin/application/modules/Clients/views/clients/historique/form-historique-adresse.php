<div class="modal-header" style="background-color : #2569C3 !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">
        Historique de modification par le propriétaire sur l'Extranet
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>

<div class="modal-body">
    <div class="table-responsive scrollbar" id="table-historique">
        <table class="table table-hover table-striped overflow-hidden">
            <?php if (empty($historique)) : ?>
                <div class="row">
                    <div class="col-12 pt-2">
                        <div class="text-center">
                            Aucune modification effectuée pour ce propriétaire
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <thead>
                    <tr>
                        <th scope="col">Date et Heure de modification</th>
                        <th scope="col">Type</th>
                        <th scope="col">Adresses</th>
                        <th scope="col">Email 1</th>
                        <th scope="col">Email 2</th>
                        <th scope="col">Téléphone 1</th>
                        <th scope="col">Téléphone 2</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($historique as $key => $value) : ?>
                        <?php if ($key < 5) : ?>
                            <tr class="align-middle fs--1" data-row="visible">
                            <?php else : ?>
                            <tr class="align-middle fs--1" data-row="hidden" style="display: none;">
                            <?php endif; ?>
                                <td class="text-nowrap"><?= date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $value->hist_date_modif))); ?></td>
                                <td class="text-nowrap">
                                    Nouvelles infos <br>
                                    Anciennes infos
                                </td>
                                <td class="text-nowrap">
                                    <?= $value->hist_adresse_1_nouv ?> <br>
                                    <?= $value->hist_adresse_1_ancien ?>
                                </td>
                                <td class="text-nowrap">
                                    <?= $value->hist_email_1_nouv ?> <br>
                                    <?= $value->hist_email_1_ancien ?>
                                </td>
                                <td class="text-nowrap">
                                    <?= $value->hist_email_2_nouv ?> <br>
                                    <?= $value->hist_email_2_ancien ?>
                                </td>
                                <td class="text-nowrap">
                                    <?= $value->hist_tel_1_nouv ?> <br>
                                    <?= $value->hist_tel_1_ancien ?>
                                </td>
                                <td class="text-nowrap">
                                    <?= $value->hist_tel_2_nouv ?> <br>
                                    <?= $value->hist_tel_2_ancien ?>
                                </td>
                            </tr>                            
                        <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>
    </div>
    <div class="text-end" style="color: #2569C3; text-decoration:underline">
        <span id="voir-plus" type="button">Voir plus</span>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>

<script>
    $(document).ready(function () {
        var numRowsToShow = 5;
        var $visibleRows = $('tr[data-row="visible"]');
        var $hiddenRows = $('tr[data-row="hidden"]');
        var numRowsHidden = $hiddenRows.length;

        $(document).on('click', '#voir-plus', function () {
            var numRowsToToggle = Math.min(numRowsToShow, numRowsHidden);
            var $additionalVisibleRows = $hiddenRows.slice(0, numRowsToToggle);

            $additionalVisibleRows.show();
            $additionalVisibleRows.attr('data-row', 'visible');
            $hiddenRows = $hiddenRows.slice(numRowsToToggle);
            numRowsHidden -= numRowsToToggle;

            if (numRowsHidden === 0) {
                $(this).hide();
            }
        });

        if (numRowsHidden === 0) {
            $('#voir-plus').hide();
        }
    });
</script>