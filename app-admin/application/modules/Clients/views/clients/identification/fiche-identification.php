<div id="div-fiche-client" class="row m-0">
	<?php
	$phoneNumber = $client->client_phonemobile;
	$numero;
	$number = null;
	if ($client->paysmonde_id == 66) {
		$array = str_split($phoneNumber, 2);
		if ($client->client_phonemobile != "") {
			$array[0] = '(' . $array[0][0] . ')' . isset($array[0][1]) && !empty($array[0][1])  ? $array[0][1] : '';
			$numero = implode(".", $array);
		} else {
			$numero = $client->client_phonemobile;
		}
	} else {
		$numero = $client->client_phonemobile;
	}

	if ($client->client_phonefixe != "" && $client->client_phonefixe != "0") {
		$phone = $client->client_phonefixe;
		if ($client->pays_id == 66) {
			$array = str_split($phone, 2);
			$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
			$number = implode(".", $array);
		} else {
			$number = $client->client_phonefixe;
		}
	}

	?>

	<div class="col-12 p-0 py-1">
		<div class="row m-0">
			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Identité</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Nom : </th>
									<th class="pe-0 text-end">
										<?= $client->client_nom; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Prénom : </th>
									<th class="pe-0 text-end">
										<?= $client->client_prenom; ?>
									</th>
								</tr>

								<tr class="border-bottom">
									<th class="ps-0">Entreprise : </th>
									<th class="pe-0 text-end">
										<?= $client->client_entreprise; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Nationnalité : </th>
									<th class="pe-0 text-end">
										<?= $client->client_nationalite; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Résidence fiscale : </th>
									<th class="pe-0 text-end">
										<?= $client->client_residence_fiscale; ?>
									</th>
								</tr>
							</tbody>
						</table>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Situation matrimoniale : </th>
									<th class="pe-0 text-end">
										<?= $client->client_situation_matrimoniale; ?>
									</th>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<div class="contenair-title">
							<div class="px-2">
								<h5 class="fs-0">Contact</h5>
							</div>
							<button type="button" id="histo-contact" data-id="<?= $client->client_id ?>" class="btn btn-sm btn-primary mx-1">
								<span> Historique </span>
							</button>
						</div>
						<table class="table table-borderless fs--1 mb-4">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Email 1 : </th>
									<th class="pe-0 text-end">
										<?= $client->client_email; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Email 2 : </th>
									<th class="pe-0 text-end">
										<?= $client->client_email1; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Télephone 1 : </th>
									<th class="pe-0 text-end">
										<?= $numero != '' ? $client->paysmonde_indicatif : '' ?>
										<?= $numero ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Télephone 2 : </th>
									<th class="pe-0 text-end">
										<?= (trim($client->client_phonefixe) != "") ? $client->pays_indicatif . ' ' . $number : ''; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<div class="contenair-title">
							<div class="px-2">
								<h5 class="fs-0">Adresses</h5>
							</div>
							<button type="button" id="histo-contact" data-id="<?= $client->client_id ?>" class="btn btn-sm btn-primary mx-1">
								<span> Historique </span>
							</button>
						</div>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 1 : </th>
									<th class="pe-0 text-end">
										<?= $client->client_adresse1; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 2 : </th>
									<th class="pe-0 text-end">
										<?= $client->client_adresse2; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 3 : </th>
									<th class="pe-0 text-end">
										<?= $client->client_adresse3; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Code postal : </th>
									<th class="pe-0 text-end">
										<?= $client->client_cp; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Ville : </th>
									<th class="pe-0 text-end">
										<?= $client->client_ville; ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0 pb-0">Pays : </th>
									<th class="pe-0 text-end pb-0">
										<?= $client->client_pays; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<hr class="mt-2">

					</div>
				</div>
			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-50">
					<div class="card-body ">
						<h5 class="fs-0">Autres informations</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Origine client : </th>
									<th class="pe-0 text-end">
										<?= $client->origine_cli_libelle; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Date création : </th>
									<th class="pe-0 text-end">
										<?= date("d/m/Y", strtotime(str_replace('/', '-', $client->client_date_creation))); ?>
									</th>
								</tr>
								<tr class="border-bottom d-none">
									<th class="ps-0">Suivi par : </th>
									<th class="pe-0 text-end">
										<?= $client->util_nom . ' ' . $client->util_prenom; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<h5 class="fs-0 mt-4">Commentaire</h5>

						<a class="border-bottom-0 notification-unread notification rounded-0 border-x-0 border-300 mt-2">
							<div class="notification-body">
								<p class="mb-1 ">
									<?= $client->client_commentaire; ?>
								</p>
							</div>
						</a>

					</div>
				</div>

				<div class="card rounded-0 h-50">
					<div class="card-body">
						<h5 class="fs-0">Identifiants Espace Intranet</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Identifiant : </th>
									<th class="pe-0 text-end">
										<?= $accessClientIntranet->accp_login; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Mot de passe : </th>
									<th class="pe-0 text-end">
										<?= isset($mdp) ? 'Mot de passe changé le' . ' ' . date("d/m/Y", strtotime(str_replace('/', '-', $mdp->util_datecreate))) : $accessClientIntranet->mtpfr_nom; ?>
									</th>
								</tr>
							</tbody>
						</table>
						<br>
						<h5 class="fs-0">Identifiants Location Meublée</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Identifiant : </th>
									<th class="pe-0 text-end">
										<?= $accessClient->accp_login; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Mot de passe : </th>
									<th class="pe-0 text-end">
										<?= $accessClient->mtpfr_nom; ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>