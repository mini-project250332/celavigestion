<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" /> -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
<div class="row m-0">
	<div class="col-12 p-0 py-1">
		<div class="row m-0">
			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">

						<h5 class="fs-0 mb-2">Identité</h5>
						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="250"> Nom *</label>
								<input type="text" class="form-control maj" value="<?= $client->client_nom; ?>" id="client_nom" name="client_nom" data-formatcontrol="true" data-require="true">
								<div class="client_nom_help help text-danger"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="250"> Prénom</label>
								<input type="text" class="form-control maj" value="<?= $client->client_prenom; ?>" id="client_prenom" name="client_prenom">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="250"> Entreprise </label>
								<input type="text" class="form-control maj" id="client_entreprise" value="<?= $client->client_entreprise; ?>" name="client_entreprise">
								<div class="help"></div>
							</div>
						</div>

						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="250"> Nationnalité *</label>
								<select class="form-select" id="client_nationalite" name="client_nationalite">
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle == "Française") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle == "Britannique") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
								</select>
								<div class="help"></div>
							</div>
						</div>

						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="250"> Résidence fiscale </label>
								<select class="form-select" id="client_residence_fiscale" name="client_residence_fiscale">
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle == "Française") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle == "Britannique") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
									<?php foreach ($nationalite as $nationalites) :
										if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
											<option value="<?= $nationalites->libelle ?>" <?php echo ($client->client_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
									<?php endif;
									endforeach; ?>
								</select>
								<div class="help"></div>
							</div>
						</div>

						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="250"> Situation matrimoniale *</label>
								<select class="form-select" id="client_situation_matrimoniale" value="<?= $client->client_situation_matrimoniale; ?>" name="client_situation_matrimoniale">
									<?php foreach (unserialize(situation) as $key => $value) : ?>
										<option value="<?= $value ?>" <?php echo ($client->client_situation_matrimoniale == $value) ? "selected" : " " ?>>
											<?= $value; ?>
										</option>
									<?php endforeach; ?>
								</select>
								<div class="help"></div>
							</div>
						</div>


				
					</div>
				</div>
			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">

						<h5 class="fs-0 mb-2">Contact</h5>
						<hr class="mt-1">
						<div class="form-group inline">
							<div>
								<label data-width="150"> Email 1 *</label>
								<input data-type="email" type="text" class="form-control" value="<?= $client->client_email; ?>" id="client_email" name="client_email" data-formatcontrol="true" data-require="true">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Email 2</label>
								<input data-type="email" type="text" class="form-control" value="<?= $client->client_email1; ?>" id="client_email1" name="client_email1">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Téléphone 1 * </label>
								<select class="form-control select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id" name="paysmonde_id">
									<?php foreach ($pays_indicatif as $indicatif) : ?>
										<option value="<?= $indicatif->paysmonde_id ?>" <?php echo isset($client->paysmonde_id) ? (($client->paysmonde_id !== NULL) ? (($client->paysmonde_id == $indicatif->paysmonde_id) ? "selected" : " ") : "") : "" ?>>
											<?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
										</option>
									<?php endforeach; ?>
								</select>
								<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="client_phonemobile" name="client_phonemobile" style="height: 37px; width: 80%;left: 4px" value="<?= $client->client_phonemobile ?>">
								<div class="client_phonemobile_help help text-danger" style="margin-top: 9px;"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Télephone 2</label>
								<select class="form-select select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id" name="pays_id">
									<?php foreach ($pays as $indicatifs) : ?>
										<option value="<?= $indicatifs->pays_id ?>" <?php echo isset($client->pays_id) ? (($client->pays_id !== NULL) ? (($client->pays_id == $indicatifs->pays_id) ? "selected" : " ") : "") : "" ?>>
											<?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
										</option>
									<?php endforeach; ?>
								</select>
								<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="client_phonefixe" name="client_phonefixe" style="height: 37px;width: 80%; left: 4px" value="<?= $client->client_phonefixe ?>">
								<div class="help"></div>
							</div>
						</div>

						<h5 class="fs-0">Adresse</h5>

						<hr>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Adresse 1 </label>
								<input type="text" class="form-control maj" value="<?= $client->client_adresse1; ?>" id="client_adresse1" name="client_adresse1">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Adresse 2</label>
								<input type="text" class="form-control maj" value="<?= $client->client_adresse2; ?>" id="client_adresse2" name="client_adresse2">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Adresse 3</label>
								<input type="text" class="form-control maj" value="<?= $client->client_adresse3; ?>" id="client_adresse3" name="client_adresse3">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Code postal </label>
								<input type="text" class="form-control" value="<?= $client->client_cp; ?>" id="client_cp" name="client_cp">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Ville </label>
								<input type="text" class="form-control maj" value="<?= $client->client_ville; ?>" id="client_ville" name="client_ville">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="150"> Pays *</label>
								<input type="text" class="form-control maj" value="<?= $client->client_pays; ?>" id="client_pays" name="client_pays" data-formatcontrol="true" data-require="true">
								<div class="client_pays_help text-danger help"></div>
							</div>
						</div>

						<hr>

					</div>
				</div>
			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Autres informations</h5>
						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="200"> Origine client </label>
								<select class="form-select " id="origine_cli_id" name="origine_cli_id">
									<?php foreach ($listeOrigineClient as $key => $value) : ?>
										<option value="<?= $value->id; ?>" <?= ($client->origine_cli_id == $value->id ? "selected" : ""); ?>><?= $value->libelle; ?></option>
									<?php endforeach; ?>
								</select>
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Date de création</label>
								<?php $date_time = $client->client_date_creation;
								$format_date = date("Y-m-d", strtotime($date_time)) ?>
								<input type="date" class="form-control" id="client_date_creation" name="client_date_creation" data-formatcontrol="true" data-require="true" value="<?= $format_date ?>">
								<div class="client_date_creation_help help"></div>
							</div>
						</div>

						<div class="form-group inline d-none">
							<div>
								<label data-width="200"> Suivi par</label>
								<select class="form-select util_id" id="util_id" name="util_id" data-formatcontrol="true" data-require="true">
									<?php foreach ($listeUser as $key => $value) : ?>
										<option value="<?= $value->id; ?>" <?php echo isset($client->util_id) ? (($client->util_id !== NULL) ? (($client->util_id == $value->id) ? "selected" : " ") : "") : "" ?>>
											<?= $value->nom . ' ' . $value->prenom; ?>
										</option>
									<?php endforeach; ?>
								</select>
								<div class="help"></div>
							</div>
						</div>

						<hr>

						<div class="form-group inline textarea_form">
							<div>
								<label data-width="200"> Commentaire </label>
								<textarea class="form-control" id="client_commentaire" name="client_commentaire" style="height: 100px;"><?= $client->client_commentaire; ?></textarea>
								<div class="help"></div>
							</div>
						</div>

						<hr>
						<h5 class="fs-0">Identifiants Espace Intranet</h5>

						<hr class="mt-1">

						<div class="form-group inline">
							<div>
								<label data-width="200"> Identifiant : </label>
								<input type="text" class="form-control" value="<?= $accessClientIntranet->accp_login; ?>" id="accp_login" name="accp_login" data-formatcontrol="true" data-require="true">
								<div class="help help_login text-danger"></div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y"
	});

	// $(".select2").val(66);

	$(document).ready(function() {
		$('.select2').select2({
			theme: 'bootstrap-5',
			//placeholder: "Select a state",
		});
	});
</script>