<?php echo form_tag('Clients/updateInfoClient', array('method' => "post", 'class' => 'px-2', 'id' => 'updateInfoClient')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class="px-2">

        <button type="submit" id="save-update-client" class="btn btn-sm btn-primary mx-1 d-none">
            <i class="fas fa-check-circle"></i>
            <span> Terminer </span>
        </button>

        <button type="button" data-id="<?= $client->client_id; ?>" id="btn-update-client"
            class="only_visuel_gestion btn btn-sm btn-primary btn-update-client">
            <span class="fas fa-edit fas me-1"></span>
            <span> Modifier </span>
        </button>

    </div>
</div>

<input type="hidden" name="client_id" id="client_id" value="<?= $client->client_id; ?>">

<div id="fiche-identifiant-client">

</div>
<?php echo form_close(); ?>


<script type="text/javascript">
    $(document).ready(function () {
        getFicheIdentificationClient(<?= $client->client_id; ?>, 'fiche');
    });
</script>