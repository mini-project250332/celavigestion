<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">
        Saisie d'un temps effectué  
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('SaisieTemps/AjoutTempsGeneral',array('method'=>"post", 'class'=>'px-2','id'=>'AjoutTempsGeneral')); ?>

<div class="modal-body">

    <div class="row">
        <div class="col">
            <div class="alert alert-secondary fs--1 text-center" role="alert">
                Aucun propriétaire, dossier et lot concernés
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="150"> Date : </label>
                    <input type="text" class="form-control fs--1" id="travail_date" name="travail_date" 
                    value="">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="150"> Type : </label>
                        <select class="form-select fs--1" name="type_travail_id" id="type_travail_id">
                            <?php foreach ($typeTravail as $key => $value) :?> 
                                <option value="<?=$value->type_travail_id?>"><?=$value->type_travail_libelle?></option>.
                            <?php endforeach;?>                           
                        </select>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div class="input-group">
                    <label data-width="150"> temps passé : </label>
                    <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="travail_duree" name="travail_duree" style="text-align: right; height : 36px;margin-left:-4%">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">min</span>
                    </div>
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group inline">
                <div>
                    <label data-width="150"> Commentaire : </label>
                        <textarea class="form-control" id="travail_commentaire" name="travail_commentaire" placeholder="Laissez un commentaire" style="height: 150px"></textarea>
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
	
	flatpickr("#travail_date",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
			defaultDate: moment().format('DD-MM-YYYY'),
		}
	);

</script>