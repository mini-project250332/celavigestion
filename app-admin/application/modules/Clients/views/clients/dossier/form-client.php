<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter un client</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Prospections/UpdateDocument',array('method'=>"post", 'class'=>'px-2','id'=>'UpdateDocument')); ?>
 
	<?php if(isset($client->client_id)):?>
        <input type="hidden" name="client_id" id="client_id" value="<?= isset($client->client_id) ? $client->client_id: "" ;?> ">
    <?php endif;?>

    <!-- <input type="hidden" name="prospect_id" id="prospect_id" value="<?=$client->client_id;?>"> -->
    <input type="hidden" name="action" id="action" value="<?=$action?>">
    <div class="modal-body">

        <div class="row m-0">
    		<div class="col py-1">
    			<div class="">
	                <div class="">
		                <div class="form-group inline">
		                    <div>
		                        <label data-width="100"> Nom client: </label>
		                        <!-- <select class="form-select fs--1" id="client_id" name="client_id">
                                    <?php foreach ($client as $clients ) :?>
                                        <option value="<?= $clients->client_id ?>">
                                        <?= $clients->client_nom .''.$clients->client_prenom ?>
                                        </option>
                                    <?php endforeach;?>
                                </select> -->
                                <select  class="form-select fs--1 js-choice" id="client_id" name="client_id"  multiple="multiple" size="1" data-require="true">
                                    <option value=""></option>
                                </select>
		                        <div class="help"></div>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </div>
<?php echo form_close(); ?>
 
<?php 
    $client_default = array();
    foreach ($client as $clients ){
        $client_default[] = array(
            'id' => $clients->client_id,
            'text' => $clients->client_prenom,
        );
    }

    if($action=="edit"){
        $edit_exist = explode(',',$tache->prospect_participant);
        $util_array = array();
        $items = array();
        foreach ($util_default as $key) {
            if(in_array($key['id'],$edit_exist)){
                $util_array[] = array(
                    'id' => $key['id'],
                    'text' => $key['text'],
                );
            }else{
                $items[] = array(
                    'id' => $key['id'],
                    'text' => $key['text'],
                );
            }
        }
        $util_default = $items;
    }
?>