<div class="contenair-title">
	<div class="px-2"></div>
	<div class="px-2">
		<button type="button" data-client="<?= $client_id; ?>" data-id="<?= $dossier->dossier_id; ?>" id="btn-update-dossier" class="only_visuel_gestion btn btn-sm btn-primary btn-update-client">
			<span class="fas fa-edit fas me-1"></span>
			<span> Modifier</span>
		</button>
	</div>
</div>

<div class="contenair-content">
	<div class="col-12">
		<div class="row m-0 ">
			<div class="col  p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Information générales</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Propriétaire(s): </th>
									<th class="pe-0 pt-2 text-end">
										<?php
										$client_id = explode(',', $dossier->client_id);
										$filtre_client = array_filter($client, function ($client_row) use ($client_id) {
											return in_array($client_row->client_id, $client_id);
										});
										$nom = array();
										$i = 1;
										foreach ($filtre_client as $keyy => $value) {
											array_push($nom, $value->client_nom);
											echo $value->client_nom . " " . $value->client_prenom;
											if (isset($value->client_entreprise) && $value->client_entreprise != "")
												echo " (" . $value->client_entreprise . ")";
											if ($i < count($filtre_client))
												echo ", ";
											echo "<br/>";
											$i++;
										}
										?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Indivision : </th>
									<th class="pe-0 pt-2 text-end">
										<?php if ($dossier->indivision == "Oui") {
											foreach ($rofData as $rof) {
												if (!empty($rof)) {
													echo $rof->client_nom . ' ' . $rof->client_prenom . '&nbsp;&nbsp;' . $rof->client_rof . '%' . '<br>';
												}
											}
										} else {
											echo "Non";
										} ?>

									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Forme juridique : </th>
									<th class="pe-0 text-end">

										<?= $dossier->c_forme_juridique_libelle ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Numero dossier : </th>
									<th class="pe-0 pt-2 text-end">
										<?= str_pad($dossier->dossier_id, 4, '0', STR_PAD_LEFT); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Status dossier : </th>
									<th class="pe-0 text-end">
										<?= ($dossier->dossier_etat == 1) ? "Actif" : "Inactif"; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Date création : </th>
									<th class="pe-0 text-end">
										<?= date("d-m-Y", strtotime(str_replace('/', '-', $dossier->dossier_d_creation))); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Date modification : </th>
									<th class="pe-0 text-end">
										<?= date("d-m-Y", strtotime(str_replace('/', '-', $dossier->dossier_d_modification))); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Suivi par : </th>
									<th class="pe-0 text-end">
										<?php
										if ($dossier->util_id == null) {
											echo "Aucune (à renseigner)";
										} else {
											echo $dossier->util_nom . ' ' . $dossier->util_prenom;
										}
										?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col  p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Adresses de facturation loyers et honoraires</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Type d'adresse : </th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->dossier_type_adresse ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Adresse 1 :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_adresse1) ? $dossier->dossier_adresse1 : (!empty($lot) ? $lot->progm_adresse1 : "") ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Adresse 2 :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_adresse2) ? $dossier->dossier_adresse1 : (!empty($lot) ? $lot->progm_adresse2 : "") ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Adresse 3 :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_adresse3) ? $dossier->dossier_adresse3 : (!empty($lot) ? $lot->progm_adresse3 : "") ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Code postal :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_cp) ? $dossier->dossier_cp : (!empty($lot) ? $lot->progm_cp : "") ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Ville :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_ville) ? $dossier->dossier_ville : (!empty($lot) ? $lot->progm_ville : "") ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2">
										Pays :
									</th>
									<th class="pe-0 pt-2 text-end">
										<?= !empty($dossier->dossier_pays) ? $dossier->dossier_pays : (!empty($lot) ? $lot->progm_pays : "") ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col  p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Fiscalité et comptabilité</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> SIE :</th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->sie_libelle ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> RCS :</th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->dossier_rcs ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> SIRET : </th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->dossier_siret ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> APE : </th>
									<th class="pe-0 text-end">
										<?php echo isset($dossier->dossier_ape) ? $dossier->dossier_ape : "6820A" ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> TVA intracommunautaire : </th>
									<th class="pe-0 text-end">
										<?= $dossier->dossier_tva_intracommunautaire ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Régime : </th>
									<th class="pe-0 text-end">
										<?= $dossier->regime ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Clôture : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cloture_jour ?> /
										<?= $dossier->cloture_mois ?> au
										<?= $dossier->cloture_jour_fin ?> /
										<?= $dossier->cloture_mois_fin ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">CGA : </th>
									<th class="pe-0 text-end">
										<?= ($dossier->mention_cga == 1)  ? "Oui" : "Non"  ?>

									</th>
								</tr>
							</tbody>
						</table>

						<div class="d-flex flex-column py-2 mt-3">

							<div class="text-800 fw-semi-bold px-1 mb-2">
								<span> Expert comptable : </span>
							</div>

							<?php if (!empty($data_missionsEC)) : ?>

								<?php $dernier_mission_expertC = $data_missionsEC[count($data_missionsEC) - 1]; ?>

								<div class="d-flex flex-column p-3 bg-light">
									<div class="d-flex justify-content-between">
										<div class="d-flex align-items-center position-relative">
											<div class="avatar avatar-xl">
												<span class="fas fa-user-circle fa-w-16 fs-5 me-2"></span>
											</div>
											<div class="ms-2 px-2 d-flex flex-column">
												<div>
													<span class="mb-1 fs-0 fw-semi-bold">
														<?= $dernier_mission_expertC->expert_nom . ' ' . $dernier_mission_expertC->expert_prenom; ?>
													</span>
												</div>
												<div>
													<?php if (!empty($dernier_mission_expertC->date_resiliation_mission)) : ?>
														<?php
														$resiliationDate = new DateTime($dernier_mission_expertC->date_resiliation_mission);
														$currentDate = new DateTime();
														?>
														<?php if ($resiliationDate < $currentDate) : ?>
															<span class="badge rounded-0 badge-soft-danger fs--1 text-center">
																Résilié au <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_resiliation_mission)); ?>
															</span>
														<?php else : ?>
															<?php
															$debutDate = new DateTime($dernier_mission_expertC->date_debut_mission);
															?>
															<?php if ($debutDate > $currentDate) : ?>
																<span class="badge rounded-0 badge-soft-info fs--1 text-center">
																	À venir [ début le <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_debut_mission)); ?> , fin au <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_resiliation_mission)); ?> ]
																</span>
															<?php else : ?>
																<span class="badge rounded-0 badge-soft-success fs--1 text-center">
																	En cours [ depuis le <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_debut_mission)); ?> , fin au <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_resiliation_mission)); ?> ]
																</span>
															<?php endif; ?>
														<?php endif; ?>
													<?php else : ?>
														<?php
														$debutDate = new DateTime($dernier_mission_expertC->date_debut_mission);
														$currentDate = new DateTime();
														?>
														<?php if ($debutDate > $currentDate) : ?>
															<span class="badge rounded-0 badge-soft-info fs--1 text-center">
																À venir [ début le <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_debut_mission)); ?> ]
															</span>
														<?php else : ?>
															<span class="badge rounded-0 badge-soft-success fs--1 text-center">
																En cours [ depuis le <?= date('d/m/Y', strtotime($dernier_mission_expertC->date_debut_mission)); ?> ]
															</span>
														<?php endif; ?>
													<?php endif; ?>

												</div>
											</div>
										</div>
										<div class="d-flex align-items-center">

											<button data-id="<?= $dossier->dossier_id; ?>" id="<?= $dossier->dossier_id; ?>" class="btn btn-outline-primary icon-btn p-0 m-1 d-flex align-items-center justify-content-center btn-data_affectationExpertComptable" type="button" style="height:30px;width: 30px;">
												<span class="fas fa-play fs-0 m-1"></span>
											</button>

										</div>
									</div>

									<?php if (!empty($dernier_mission_expertC->fichier_mission)) : ?>
										<hr class="my-2">
										<div class="d-flex flex-column">
											<div class="pb-1">
												<span class="mb-0 py-2">
													<span class="fw-semi-bold fs-0">Lettre de mission : </span>
												</span>
											</div>
											<div class="d-flex flex-column">

												<div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
													<span class="fs--1 px-1" style="width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
														<?php $pathInfo_fichier_mission = pathinfo($dernier_mission_expertC->fichier_mission); ?>
														<?= $pathInfo_fichier_mission['basename']; ?>
													</span>
													<div>
														<button onclick="forceDownload('<?= base_url($dernier_mission_expertC->fichier_mission); ?>')" data-url="<?= base_url($dernier_mission_expertC->fichier_mission); ?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center" type="button" style="height:25px;width: 25px;cursor: pointer">
															<span class="fas fa-download fs-0 m-1"></span>
														</button>
													</div>
												</div>

											</div>
										</div>
									<?php endif; ?>

									<?php if (!empty($dernier_mission_expertC->fichier_resiliation)) : ?>
										<hr class="my-2">
										<div class="d-flex flex-column">
											<div class="pb-1">
												<span class="mb-0 py-2">
													<span class="fw-semi-bold fs-0">Pièce justificative de la résiliation de la mission : </span>
												</span>
											</div>
											<div class="d-flex flex-column">

												<div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
													<span class="fs--1 px-1" style="width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
														<?php $pathInfo_fichier_resiliation = pathinfo($dernier_mission_expertC->fichier_resiliation); ?>
														<?= $pathInfo_fichier_resiliation['basename']; ?>
													</span>
													<div>
														<button onclick="forceDownload('<?= base_url($dernier_mission_expertC->fichier_resiliation); ?>')" data-url="<?= base_url($dernier_mission_expertC->fichier_resiliation); ?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center" type="button" style="height:25px;width: 25px;cursor: pointer">
															<span class="fas fa-download fs-0 m-1"></span>
														</button>
													</div>
												</div>

											</div>
										</div>
									<?php endif; ?>
								</div>

							<?php else : ?>

								<div class="d-flex flex-column ">

									<div class="d-flex justify-content-between">
										<div class="d-flex align-items-center position-relative">

											<div class="alert alert-warning border-1 d-flex align-items-center mt-2 py-2 px-3" role="alert">
												<div class="rounded-circle border-1 border-warning">
													<span class="fas fa-exclamation-circle text-warning fs-1"></span>
												</div>
												<p class="mb-0 flex-1 ps-2">
													Aucun expert comptable
												</p>
											</div>

										</div>
										<div class="d-flex align-items-center">

											<button data-id="<?= $dossier->dossier_id; ?>" id="<?= $dossier->dossier_id; ?>" class="btn btn-outline-primary icon-btn p-0 m-1 d-flex align-items-center justify-content-center btn-data_affectationExpertComptable" type="button" style="height:30px;width: 30px;">
												<span class="fas fa-play fs-0 m-1"></span>
											</button>

										</div>
									</div>
								</div>

							<?php endif; ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col  p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">CGP</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Nom et prénom :</th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->cgp_nom . ' ' . $dossier->cgp_prenom ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Email : </th>
									<th class="pe-0 pt-2 text-end">
										<?= $dossier->cgp_email ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Téléphone : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_tel ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Adresse 1 : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_adresse1 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Adresse 2 : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_adresse2 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Adresse 3 : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_adresse3 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Code postal : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_cp ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Ville : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_ville ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Pays : </th>
									<th class="pe-0 text-end">
										<?= $dossier->cgp_pays ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	only_visuel_gestion();
</script>