<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Suppression impossible</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>
                            <span class="alert alert-warning">
                                <p class="fs--1 text-center"> 
                                    Attention, il ne sera possible de supprimer ce lots que s'il n'y a pas eu de factures ou de dépenses ou d'encaissements de saisis.
                                </p>
                            </span> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
    </div>
</div>



 
