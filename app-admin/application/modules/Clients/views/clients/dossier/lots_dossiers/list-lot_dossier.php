<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <div class="d-flex justify-content-end">
            <input type="hidden" value="<?= $dossier_id ?>" id="dossier_id">
            <button id="btnlot_creation" class="btn btn-sm btn-primary only_visuel_gestion" data-id="<?= $client_id; ?>"><i class="fas fa-plus-circle"></i> Lots</button>
        </div>
    </div>
</div>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped fs--1 mb-0">
        <?php if (empty($lot)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun lot trouvé
                    </div>
                </div>
            </div>
        <?php else :  ?>
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Nom programme</th>
                    <th scope="col">Numéro dossier</th>
                    <th scope="col">Ville</th>
                    <th scope="col">Lot principal</th>
                    <th scope="col">Type du Lot</th>
                    <th scope="col">Nom gestionnaire</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lot as $lots) { ?>
                    <?php
                    $type = $lots['cliLot_principale'];
                    $value;
                    if ($type == 1) {
                        $value = "Oui";
                    } else {
                        $value = "Non";
                    }
                    ?>
                    <tr id="rowLot-<?= $lots['cliLot_id']; ?>">
                        <td><?= $lots['progm_nom'] ?></td>
                        <td><?= str_pad($lots['dossier_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <!-- <td><?= $lots['progm_ville'] ?></td> -->
                        <td><?= $lots['progm_id'] == 309 ? $lots['cliLot_ville'] : $lots['progm_ville'] ?></td>
                        <td><?= $value ?></td>
                        <td><?= $lots['typelot_libelle'] ?></td>
                        <td><?= $lots['gestionnaire_nom'] ?></td>
                        <td class="text-end">
                            <div>
                                <div class="d-flex justify-content-end ">
                                    <button data-id="<?= $lots['cliLot_id'] ?>" data-client_id="<?= $client_id; ?>" class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 btnFicheLot" type="button">
                                        <span class="fas fa-play fs-1 m-1"></span>
                                    </button>
                                    <button data-id="<?= $lots['cliLot_id'] ?>" class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 only_visuel_gestion <?= $lots['avoir_data'] == true ? 'btnConfirmSupLotAlert' : 'btnConfirmSupLotDossier' ?>" type="button">
                                        <span class="bi bi-trash fs-1 m-1"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        <?php endif ?>
    </table>
</div>

<script>
    only_visuel_gestion();
</script>