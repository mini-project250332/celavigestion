<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter signature mandat SEPA</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Sepa/AddSignMandatSepa',array('method'=>"post", 'class'=>'px-2','id'=>'AddSignMandatSepa')); ?>
<input type="hidden" name="sepa_id" id="sepa_id" value="<?=$sepa_id;?>">
<input type="hidden" name="dossier_id" id="dossier_id" value="<?=$dossier_id;?>">

<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Date de signature : </label>
                    <input type="text" class="form-control calendar" id="sepa_date_signature" name="sepa_date_signature" 
                    value="">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Mandat signé (pdf) : </label>
                    <input type="file" class="form-control" id="sepa_path" name="sepa_path">
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
	
	flatpickr(".calendar",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
		}
	); 

</script>