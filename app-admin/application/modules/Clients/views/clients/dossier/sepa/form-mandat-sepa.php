<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel"><?= isset($sepa->sepa_id) ? "Modifier un mandat SEPA" : "Créer un mandat SEPA"; ?> </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>

<?php echo form_tag('Sepa/AddMandatSepa', array('method' => "post", 'class' => 'px-2', 'id' => 'AddMandatSepa')); ?>

<input type="hidden" name="dossier_id" id="dossier_id" value="<?= $dossier_id; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">

<?php if(isset($sepa->sepa_id)): ?>
    <input type="hidden" name="sepa_id" id="sepa_id" value="<?= isset($sepa->sepa_id) ? $sepa->sepa_id : ""; ?>">
<?php endif;?>

<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Date de création : </label>
                    <input type="date" style="background-color : #EDF2F9 !important" class="form-control" id="sepa_date_creation" name="sepa_date_creation" value="<?= date('Y-m-d') ?>" disabled>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> IBAN * : </label>
                    <input type="text" class="form-control" id="sepa_iban" name="sepa_iban" data-require="true" value="<?= isset($sepa->sepa_iban) ? $sepa->sepa_iban : ""; ?>" <?= isset( $sepa->sepa_id) ? ($sepa->etat_mandat_id == 3 ? "disabled" : "") : ""; ?>>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> BIC * : </label>
                    <input type="text" class="form-control maj" id="sepa_bic" name="sepa_bic" data-require="true" value="<?= isset($sepa->sepa_bic) ? $sepa->sepa_bic : ""; ?>" <?= isset( $sepa->sepa_id) ? ($sepa->etat_mandat_id == 3 ? "disabled" : "") : ""; ?>>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline mx-8" id="doc">
                <?php if (isset($sepa->sepa_rib)) : ?>
                    <?php if(!empty($sepa->sepa_rib)): ?>
                    <?php 
                        $infoFichier = pathinfo($sepa->sepa_rib);
                        $fichier = $infoFichier['filename'].'.'.$infoFichier['extension'];
                     ?>
                    <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $sepa->sepa_rib ?>')" data-url="<?= base_url($sepa->sepa_rib) ?>" style="cursor: pointer;">
                        <?= $fichier ?>
                    </span>

                    <?php else :  ?>
                        Pas de fichier
                    <?php endif ?>
                    <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removeribSepa" style="cursor: pointer;">.</span><br>
                <?php endif ?>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> RIB : </label>
                    <input type="file" class="form-control" id="sepa_rib" name="sepa_rib">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline switch">
                <div>
                    <label data-width="150"> Statut :</label>
                    <div class="form-check form-switch">
                        <span> Inactif </span>
                        <input class="form-check-input fs--1" type="checkbox" id="sepa_etat" name="sepa_etat" value="1" <?php echo isset($sepa->sepa_etat) ? ($sepa->sepa_etat == "1" ? "checked" : "") : "checked"; ?>>
                        <span> Actif </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>


</script>