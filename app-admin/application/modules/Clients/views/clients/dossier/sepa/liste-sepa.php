<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Liste des sepas SEPA</h5>
        </div>

        <input type="hidden" id="dossier_id" value="<?= $dossier_id ?>" />

        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 ajoutMandatSepa" data-id="<?= $dossier_id ?>">
                    <span class="fas fa-plus me-1"></span>
                    <span> Mandat SEPA </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left">
        <?php if (empty($sepa)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun mandat SEPA créé
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="table-responsive">
                <table class="table table-hover fs--1">
                    <thead class="text-white">
                        <tr class="text-center">
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Numéro
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                IBAN
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                BIC
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                RIB
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Signé le
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Etat
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Status
                            </th>
                            <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-200 fs--1">
                        <?php foreach ($sepa as $sepas) : ?>
                            <tr class="apercu_docSepa" id="rowdoc-<?= $sepas->sepa_id ?>">
                                <td class="text-center fw-bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $sepas->sepa_id ?>" onclick="apercuSepa(<?= $sepas->sepa_id?>)">
                                    <?= $sepas->sepa_numero?>
                                </td>
                                <td class="text-center">
                                    <?= isset($sepas->sepa_iban) ? $sepas->sepa_iban : '-' ?>
                                </td>
                                <td class="text-center">
                                    <?= isset($sepas->sepa_bic) ? $sepas->sepa_bic : '-' ?>
                                </td>
                                <td class="text-center">
                                    <?php if(!empty($sepas->sepa_rib)): ?>
                                    
                                        <?php 
                                            $infoFichier = pathinfo($sepas->sepa_rib);
                                            $fichier = $infoFichier['filename'].'.'.$infoFichier['extension'];
                                         ?>

                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $sepas->sepa_rib ?>')" data-url="<?= base_url($sepas->sepa_rib) ?>" style="cursor: pointer;"><?=$fichier;?></span>

                                    <?php else :  ?>
                                        Pas de fichier
                                    <?php endif ?>

                                </td>
                                <td class="text-center">
                                    <?= isset($sepas->sepa_date_signature) ? date("d/m/Y", strtotime(str_replace('/', '-', $sepas->sepa_date_signature))) : '-' ?>
                                </td>
                                <td class="text-center">
                                    <?= $sepas->etat_mandat_libelle ?>
                                </td>
                                <td>
                                <?php
                                    $statut = $sepas->sepa_etat;
                                    $value;
                                    if ($statut == 1) {
                                        $value = "Actif";
                                    } else {
                                        $value = "Inactif";
                                    }
                                ?>
                                    <?= $value ?>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group fs--1">
                                        <span class="btn btn-sm btn-outline-warning rounded-pill updateMandatSepa" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier" data-sepa="<?= $sepas->sepa_id ?>" data-id="<?= $dossier_id ?>">
                                            <i class="fas fa-edit"></i>
                                        </span>&nbsp;

                                        <?php if(!empty($sepas->sepa_path)):?>

                                            <span class="btn btn-sm btn-danger rounded-pill">
                                                <i class="far fa-file-pdf" onclick="forceDownload('<?= base_url() . $sepas->sepa_path ?>')" data-url="<?= base_url($sepas->sepa_path) ?>"></i>
                                            </span>&nbsp;

                                        <?php endif;?>

                                        <span class="btn btn-sm btn-outline-secondary sendMandatSepa rounded-pill <?= $sepas->etat_mandat_id != 1  ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer" data-id="<?= $sepas->sepa_id ?>">
                                            <i class="fas fa-file-import"></i>
                                        </span>
                                        <span class="btn btn-sm btn-outline-primary rounded-pill signerMandatSepa <?= $sepas->etat_mandat_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Signer" data-id="<?= $sepas->sepa_id ?>">
                                            <i class="fas fa-file-signature"></i>
                                        </span>&nbsp;
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
    <div class="splitter m-2">

    </div>
    <div class="col-5 panel-right">
        <div id="apercu_list_Sepa">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercuMandatSepa">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });
</script>