<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Fiche Dossier : <?= str_pad($dossier->dossier_id, 4, '0', STR_PAD_LEFT); ?>
            (Propriétaire : <?= $dossier->client ?>)
        </h5>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-outline-primary btnSaisieTempsDossier" data-id="<?= $dossier->dossier_id ?>">
            <i class="fas fa-clock"></i>
        </button>

        <?php foreach ($dossier->tableclient as $key => $value) { ?>
            <button id="" data-id="<?= $value->client_id ?>" data-nom="<?= $value->client_nom ?>" class="btn btn-sm btn-secondary mx-1 retourClient">
                <i class="fas fa-undo me-1"></i>
                <span>Accès <?= $value->client_nom . ' ' . $value->client_prenom ?></span>
            </button>
        <?php } ?>
    </div>
</div>

<div class="contenair-content">

    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification_dossier" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="lots-tab" data-bs-toggle="tab" href="#tab-lots" role="tab" aria-controls="tab-lots" aria-selected="true">Lots</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="document-dossier-tab" data-bs-toggle="tab" href="#tab-document-dossier" role="tab" aria-controls="tab-document-dossier" aria-selected="true">Documents Dossier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fiscal-tab" data-bs-toggle="tab" href="#tab-fiscal" role="tab" aria-controls="tab-fiscal" aria-selected="false">Fiscal</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="reporting-tab" data-bs-toggle="tab" href="#tab-reporting" role="tab" aria-controls="tab-reporting" aria-selected="false">Reporting</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="facturation-tab" data-bs-toggle="tab" href="#tab-facturation " role="tab" aria-controls="tab-facturation " aria-selected="false">Facturation</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="sepa-tab" data-bs-toggle="tab" href="#tab-sepa " role="tab" aria-controls="tab-sepa " aria-selected="false">SEPA</a>
        </li>
        <!--
            <li class="nav-item">
                <a class="nav-link" id="cloture_annuelle-tab" data-bs-toggle="tab" href="#tab-cloture_annuelle " role="tab" aria-controls="tab-cloture_annuelle " aria-selected="false">Clôture annuelle</a>
            </li>
        -->
    </ul>

    <div class="tab-content border-x border-bottom p-1" id="DossierTabContent">
        <div class="tab-pane fade show active" id="tab-identification_dossier" role="tabpanel" aria-labelledby="identification-tab">
            <?php $this->load->view('identification_dossier/identification'); ?>
        </div>
        <div class="tab-pane fade" id="tab-lots" role="tabpanel" aria-labelledby="lots-tab">
            <?php $this->load->view('lots_dossiers/dossier-lots'); ?>
        </div>
        <div class="tab-pane fade" id="tab-document-dossier" role="tabpanel" aria-labelledby="document-dossier-tab">
            <?php $this->load->view('document_dossier/dossier-document'); ?>
        </div>
        <div class="tab-pane fade" id="tab-fiscal" role="tabpanel" aria-labelledby="fiscal-tab">
            <?php $this->load->view('fiscal/fiscal'); ?>
        </div>
        <div class="tab-pane fade" id="tab-reporting" role="tabpanel" aria-labelledby="reporting-tab">
            <?php $this->load->view('reporting/reporting'); ?>
        </div>
        <div class="tab-pane fade" id="tab-facturation" role="tabpanel" aria-labelledby="facturation-tab">
            <?php $this->load->view('facturation/facturation'); ?>
        </div>
        <div class="tab-pane fade" id="tab-sepa" role="tabpanel" aria-labelledby="sepa-tab">
            <?php $this->load->view('sepa/sepa'); ?>
        </div>
        <!--
            <div class="tab-pane fade" id="tab-cloture_annuelle" role="tabpanel" aria-labelledby="sepa-cloture_annuelle">
                <?php $this->load->view('cloture_annuelle/cloture_annuelle'); ?>
            </div>
        -->
    </div>
</div>