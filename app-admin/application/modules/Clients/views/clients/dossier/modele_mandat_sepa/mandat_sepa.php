<head>
    <link rel="stylesheet" href="../assets/css/clientions/modelMandat.css">
    <link rel="stylesheet" href="../assets/css/clientions/bootstrap.min.css">
</head>
<style>
    @page {
        margin: 4%;
    }
</style>

<body style='word-wrap:break-word'>
    <div class="logoclv" style='text-align:center'>
        <img src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo" style="height:125px;width:150px">
    </div><br><br>

    <div style="font-size: 13px;border-top: 1px solid black;padding-left:7mm; border-bottom: 1px solid black;border-right: 1px solid black;border-left: 1px solid black">
        <p>
            En signant ce formulaire de mandat, vous autorisez (A) CELAVIGESTION à envoyer des instructions à votre banque pour
            débiter votre compte, et (B) votre banque à débiter votre compte conformément aux instructions de CELAVIGESTION
        </p>
        <p>
            Vous bénéficiez du droit d'être remboursé par votre banque suivant les conditions décrites dans la convention que vous avez
            passée avec elle. Une demande de remboursement doit être présentée dans les 8 semaines suivant la date de débit de votre
            compte pour un prélèvement autorisé.
        </p>
        <p>Référence unique du mandat : <b><?=$sepa->sepa_numero ?></b> &nbsp; Identifiant créancier SEPA : ICS: <b>FR45ZZZ532215</b></p>

        <table style="width: 100%;">
            <tr>
                <td style="width:55%;color:#31849B;">
                    <b> Débiteur : </b>
                </td>
                <td style="width:45%;color:#31849B;">
                    <b> Créancier : CELAVIGESTION </b>
                </td>
            </tr>

            <tr>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>
                    <br> Votre Nom : <?=$sepa->client_nom?> &nbsp;<?=$sepa->client_prenom?><br>&nbsp;
                </td>
                <td rowspan="2">
                    39 route de Fondeline <br>44600 SAINT NAZAIRE <br>FRANCE
                </td>
            </tr>

            <tr>
                <td>
                    Votre Adresse :  <?=$sepa->client_adresse1?> <br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Code postal : <?=$sepa->client_cp ?><br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Ville : <?=$sepa->client_ville ?> <br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    Pays : <?=$sepa->client_pays ?> <br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    IBAN : <?=$sepa->sepa_iban ?> <br>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    BIC : <?=$sepa->sepa_bic ?> <br>&nbsp;
                </td>
            </tr>
        </table>

        <p>
            Paiement : Récurrent/Répétitif <img src="<?php echo base_url('../assets/img/checbox.png'); ?>" alt="logo" style="height:10px;width:15px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="text-decoration: line-through;">Ponctuel<input type="checkbox" id="myCheckbox"> </span>
        </p>
        <p>
            A : <b>________________________________</b> Le : <b>____________________________________________</b>
        </p>
        <p>
            <span style="color:#31849B"><b>Signature :</b> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Signature1/]
        </p> <br><br><br><br><br><br><br><br><br><br><br>
        <p>
            Nota : vos droits concernant le présent mandat sont expliqués dans un document que vous pouvez obtenir auprès de votre
            banque.
        </p>
    </div>
</body>