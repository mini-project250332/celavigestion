<style>
    .document_selectionner {
        background-color: red;
    }
</style>
<div class="contenair-title">
    <div class="row m-0">
        <div class="px-2">
            <h5 class="fs-1">Gestion des documents</h5>
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <button class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $dossier_id; ?>"
            id="ajoutdoc_dossier">
            <span class="fas fa-plus me-1"></span>
            <span> Ajouter </span>
        </button>
    </div>
</div>
<div class="row pt-3 m-0 panel-container">
    <div class="col-5 panel-left">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0">
            <div class="col-12"
                style=" height: calc(80vh - 100px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
                <?php foreach ($document as $documents) { ?>
                    <div class="row apercu_docu" id="rowdoc-<?= $documents->doc_id ?>">
                        <div class="col-4 pt-2">
                            <div class="fs--1 bold"
                                style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3"
                                id="<?= $documents->doc_id ?>" onclick="apercuDocument_dossier(<?= $documents->doc_id ?>)">
                                <?= $documents->doc_nom ?>
                            </div>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-user"></i>
                                <?= $_SESSION['session_utilisateur']['util_prenom'] ?>
                            </label>
                        </div>
                        <div class="col-2 text-center">
                            <label for=""><i class="fas fa-calendar"></i>
                                <?= $documents->doc_date_creation ?>
                            </label>
                        </div>
                        <div class="col-4 text-end">
                            <div class="btn-group">
                                <span
                                    class="btn btn-sm btn-outline-primary rounded-pill btnUpdtadeDocDossier only_visuel_gestion"
                                    data-id="<?= $documents->doc_id ?>" data-dossier="<?= $dossier_id; ?>">
                                    <i class="far fa-edit"></i>
                                </span>&nbsp;
                                <span class="btn btn-sm btn-outline-success rounded-pill">
                                    <i class="fas fa-download"
                                        onclick="forceDownload('<?= base_url() . $documents->doc_path ?>','<?= $documents->doc_nom ?>')"
                                        data-url="<?= base_url($documents->doc_path) ?>">
                                    </i>
                                </span>&nbsp;
                                <span
                                    class="btn btn-sm btn-outline-danger rounded-pill btnSuppDocDossier only_visuel_gestion"
                                    data-id="<?= $documents->doc_id ?>">
                                    <i class="far fa-trash-alt"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="splitter m-2">

    </div>
    <div class="col-5 panel-right">
        <div id="apercu_list_doc">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercu">
                <div id="bloc_controle" class="bloc_control mb-3 only_visuel_gestion" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i
                            class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i
                            class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i
                            class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade" id="modalAjoutDoc_dossier" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="contenaireUpload_dossier">

            </div>
        </div>
    </div>
</div>

<script>
    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    only_visuel_gestion();

</script>