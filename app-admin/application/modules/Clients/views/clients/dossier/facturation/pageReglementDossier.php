<?php echo form_tag('Dossiers/saveReglementDossier', array('method' => "post", 'class' => '', 'id' => 'saveReglementDossier')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Enregistrement d'un règlement pour la facture numéro <b><?= $facture->fact_num ?></b> (Montant total : <?= $facture->fact_montantttc ?> € TTC - Reste à payer : <?= $facture->reste_a_payer != NULL ? format_number_(abs($facture->reste_a_payer)) : format_number_($facture->fact_montantttc) ?> €)</h4>
    </div>
    <div class="px-2">
        <button type="button" class="btn btn-sm btn-secondary" id="annulerReglementDossier" data-dossier_id="<?= $dossier_id ?>"> Annuler</button> &nbsp;
        <button type="submit" class="btn btn-sm btn-primary" id="enregistrerReglementDossier"> Enregister</button>&nbsp;
    </div>
</div>

<input type="hidden" name="fact_id_reglement" id="fact_id_reglement" value="<?= $facture->fact_id ?>">
<input type="hidden" name="dossier_id_reglment" id="dossier_id_reglment" value="<?= $facture->dossier_id ?>">

<div class="row w-100 m-0 mt-4">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="mode_reglement" class="form-label">Mode de règlement * : </label>
                <select name="mode_reglement" id="mode_reglement" class="form-select">
                    <?php foreach ($mode_paiement as $mode) : ?>
                        <option value="<?= $mode->id_mode_paie ?>"><?= $mode->libelle  ?></option>
                    <?php endforeach ?>
                </select>
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="banque_celavi" class="form-label">Choix de la banque CELAVI * : </label>
                <select name="banque_celavi_reglement" id="banque_celavi_reglement" class="form-select">
                    <?php foreach ($banque as $banques) : ?>
                        <option value="<?= $banques->banque_id ?>"><?= 'IBAN: ' . $banques->banque_iban . ' -BIC: ' . $banques->banque_bic  ?></option>
                    <?php endforeach ?>
                </select>
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="datereglement" class="form-label">Date d'encaissement * :</label>
                <input type="date" class="form-control" name="datereglement" id="datereglement" data-require="true" value="<?= date('Y-m-d') ?>">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="montant_sur_reglement" class="form-label">Montant * :</label>
                <input type="text" class="form-control text-end chiffre" name="montant_sur_reglement" id="montant_sur_reglement" data-require="true" min="0" value="<?= $facture->reste_a_payer != NULL ? format_number_(abs($facture->reste_a_payer)) : format_number_($facture->fact_montantttc) ?>">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0 mb-4">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="montant_sur_reglement" class="form-label">Libellé du règlement :</label>
                <input type="text" class="form-control" name="libelle_reglement" id="libelle_reglement">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<?php echo form_close(); ?>