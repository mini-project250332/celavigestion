<div class="modal-header bg-success">
    <h5 class="modal-title text-white" id="MoadalAvoir">Enregistrer un Avoir pour la facture <?= $facture->fact_num ?></h5>
</div>
<div class="modal-body">
    <div>
        <h6><?= $title ?> </h6>
        <hr>
    </div>
    <div class="row">
        <div class="col-4">
            <label for="exampleInput">Date de l'Avoir :</label>
        </div>
        <div class="col-8">
            <input type="hidden" id="numFactAvoir" value="<?= $facture->fact_id ?>">
            <input type="date" class="form-control" id="date_factureAvoir" value="<?= $last_date ?>" ">
        </div>
        <div class=" col-4">
            <label for="exampleInput">Justification (*) :</label>
        </div>
        <div class="col-8">
            <textarea class="form-control" id="justificationAvoir" style="height: 100px;"></textarea>
            <div id="justificationError" style="color: red; font-size: 12px; margin-top: 0px;"></div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="text-danger text-center fs--1"><i class="text_alert"></i></div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" id="confirmer_creationAvoirDossier">Confirmer</button>
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>