<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
        @page {
            margin: 3%;
        }

        .title {
            font-size: 13px;
        }

        .row {
            margin-bottom: 2rem;
        }

        .row_cadre {
            margin-bottom: 1rem;
        }

        .celavilogohouse {
            width: 90px;
        }

        .celaviword {
            width: 225px;
        }

        .numero_facture {
            font-size: 19px;
            font-weight: bold;
        }

        .column {
            font-family: Arial, Helvetica, sans-serif;
            float: left;
            padding: 3px;
        }

        .left {
            width: 65%;
        }

        .right {
            width: 32%;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-family: Arial, Helvetica, sans-serif;
        }

        .first_table th {
            background-color: #eaeaea;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
        }

        .first_table th,
        .first_table td {
            color: black;
            text-align: center;
            font-size: 13px;
        }

        .footer_exp {
            font-family: Arial, Helvetica, sans-serif;
            border: 1px solid black;
            text-align: center;
            width: 95%;
            font-size: 12px;
            margin-left: 1.5%;
            padding: 1%;
        }

        .text {
            font-size: 12px;
        }

        .second_table th {
            background-color: #Eaeaea;
        }

        .second_table th,
        .second_table td {
            color: black;
            text-align: center;
            border: none;
            font-size: 12px;
        }
    </style>
</head>

<body>

    <?php
    $totalht = 0;
    $mode_virement = array(
        1 => 'NON RENSEIGNER',
        2 => 'VIREMENT',
        3 => 'PRELEVEMENT',
        4 => 'SUR LOYERS ENCAISSES'
    );
    ?>

    <div style="margin-top: 1rem; margin-bottom: 1rem; text-align: center;">
        <img class="celavilogohouse" src="<?php echo base_url('../assets/img/celavilogohouse.png'); ?>" alt="Logo Celavi House"> <br>
        <img class="celaviword" src="<?php echo base_url('../assets/img/celaviword.png'); ?>" alt="Logo Celavi Word">
    </div>

    <div class="row">
        <div class="title">
            Parc de Brais 39 route de Fondeline <br>
            44600 Saint-Nazaire <br>
            <?= $dossier->util_prenom . ' ' . $dossier->util_nom ?> <br>
            <?= $dossier->util_mail  ?> <br>
            <?= formaterNumeroTelephone($dossier->util_phonefixe) ?>
        </div>
    </div>

    <div class="row">
        <div class="column left">
            <span class="numero_facture">
                Facture N° <?= $numero_facture ?>
            </span>
        </div>
        <div class="column right">
            <div class="title">
                <?= $dossier->client_nom . ' ' . $dossier->client_prenom ?> <br>
                <?= $lot_principale->progm_nom ?> <br>
                <?= $lot_principale->progm_cp . ' ' . $lot_principale->progm_ville ?> <br>
            </div>
        </div>
    </div>

    <div class=" row">
        <table class="first_table">
            <thead>
                <tr style="background-color: #ddd;">
                    <th style="border-left: 1px solid balck;">Numero</th>
                    <th>Date</th>
                    <th>Dossier</th>
                    <th>Date échéance</th>
                    <th>Mode de réglement</th>
                    <th style="border-right: 1px solid balck;">Num TVA intracom</th>
                </tr>
            </thead>
            <tbody>
                <tr style="text-align:center;">
                    <td width="15%"><?= $numero_facture ?></td>
                    <td><?= date('d/m/Y', strtotime($date_fature)) ?></td>
                    <td><?= str_pad($dossier->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                    <td><?= date('d/m/Y', strtotime($date_fature . ' +10 days')) ?></td>
                    <td><?= array_key_exists($factureCree->fact_modepaiement, $mode_virement) ? $mode_virement[$factureCree->fact_modepaiement] : '' ?></td>
                    <td><?= $dossier->dossier_tva_intracommunautaire ?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="row">
        <table class="first_table">
            <thead>
                <tr style="background-color: #ddd;">
                    <th width="40%" style="border-left: 1px solid balck;">Article</th>
                    <th width="20%">Lot</th>
                    <th>Qt</th>
                    <th>Prix Uni. HT</th>
                    <th>TVA</th>
                    <th style="border-right: 1px solid balck;">Total HT</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lotarticle as $lots) : ?>

                    <tr style="text-align:center;">
                        <td style="text-align:left;">
                            <?= $article->art_libelle_fr ?><br>
                            <?= $article->art_libelle_en ?><br>
                            Facture concernée : <?= $facture->fact_num ?>
                        </td>
                        <td><?= $lots->progm_nom ?></td>
                        <td><?= $lots->faa_qte ?></td>
                        <td style="text-align:right;"><?= format_number_($lots->faa_puht) ?> €</td>
                        <td><?= $lots->faa_tvapourcentage ?> %</td>
                        <td style="text-align:right;"><?= format_number_($lots->faa_puht) ?> €</td>
                    </tr>
                <?php endforeach ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2" style="text-align: right;">
                        <b>Total HT :</b> <br>
                        <b>TVA :</b> <br>
                        <b>Total TTC :</b>
                    </td>
                    <td colspan="2" style="text-align: right;font-weight:bold">
                        <b><?= format_number_($factureCree->fact_montantht) ?> €</b> <br>
                        <b><?= format_number_($factureCree->fact_montanttva) ?> €</b> <br>
                        <b><?= format_number_($factureCree->fact_montantttc) ?> €</b>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="row">
        <table class="first_table">
            <thead>
                <tr>
                    <th style="border-left: 1px solid balck;">
                        <?= array_key_exists($dossier->mode_paiement, $mode_virement) ? $mode_virement[$dossier->mode_paiement] : '' ?> <br><br>
                        Echéance de paiement / Deadline : <?= date('d/m/Y', strtotime($date_fature . ' +10 days')) ?>
                    </th>
                    <th>
                        <br> <br>
                        A PAYER :
                    </th>
                    <th style="border-right: 1px solid balck;">
                        <br> <br>
                        <?= format_number_($factureCree->fact_montantttc) ?> €
                    </th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="row">
        <table class="second_table">
            <?php if ($dossier->mode_paiement == 2) : ?>
                <thead>
                    <tr class="table-secondary">
                        <th>
                            Le virement doit être effectué avant le <?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?> avec la référence <?= $numero_facture ?> sur le compte : / The transfer must be made before <?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?> with the reference <?= $numero_facture ?> to the following account :
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <b> IBAN</b>: <?= formaterIBAN($rib->rib_iban) ?> - <b> BIC</b>: <?= $rib->rib_bic ?>
                        </td>
                    </tr>
                </tbody>
            <?php elseif ($dossier->mode_paiement == 3) : ?>
                <thead>
                    <tr class="table-secondary">
                        <th>
                            Prélevé automatiquement sur votre compte bancaire : / Automatically debited from your bank account :
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <b> IBAN</b>: <?= formaterIBAN(masquerIBAN($lot_principale->iban)) ?> - <b> BIC</b>: <?= $lot_principale->bic ?>
                        </td>
                    </tr>
                </tbody>
            <?php elseif ($dossier->mode_paiement == 4) : ?>
                <thead>
                    <tr class="table-secondary">
                        <th>
                            Prélevé automatiquement sur vos loyers / Automatically deducted from your rent
                        </th>
                    </tr>
                </thead>
            <?php endif ?>
        </table>
    </div>

    <div class="row_cadre">
        <div class="footer_exp">
            <b>SASU CELAVI GESTION - Parc de Brais 39 route de Fondeline 44600 SAINT NAZAIRE</b> <br>
            SASU au Capital de 100000 Euros - RCS SAINT NAZAIRE B 477 782 346 - Siret : 477 782 346 00035 - APE : 6831Z <br>
            Numéro de TVA intracommunautaire : FR69477782346 <br>
            <br>
            En cas de retard de paiement, une pénalité égale à 3 fois le taux d’intérêt légal sera exigible (Article L441-10, alinéa 12 du Code du Commerce). Pour tout professionnel, en sus des indemnités de retard, toute somme, y compris l’acompte, non payée à sa date d’exigibilité produira de plein droit le paiement d’une indemnité forfaitaire de 40 euros due au titre des frais de recouvrement (Article 441-6, I al. 12 et D.441-5 du Code du Commerce).
            <br> <br>
            Pas d’escompte pour règlement anticipé.
            <br> <br>
            Carte professionnelle gestion N° CPI 4402 2018 000 035 231 délivrée par la CCI de Nantes SAINT NAZAIRE le 1er juillet 2021, garantie gestion à
            hauteur de 620.000€ souscrites auprès de GALIAN, 89 rue de la Boétie 75008 PARIS <br>
        </div>
    </div>

    <div class="">
        <div class="text">
            Mentions légales pour les règlements par prélévement SEPA -> ICS FR45ZZZ532215 - RUM <?= $sepa->sepa_numero ?>
        </div>
    </div>

</body>

</html>