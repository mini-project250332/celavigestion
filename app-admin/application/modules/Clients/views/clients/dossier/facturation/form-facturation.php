<?php echo form_tag('Dossiers/updateFacturation', array('method' => "post", 'class' => 'px-2', 'id' => 'updateFacturation')); ?>

<div class="contenair-title">
    <div class="row m-0">
        <div class="px-2">
            <h5 class="fs-1">Données de facturation de 2023 : </h5>
        </div>
    </div>
    <div class="px-2">
        <?php if (!empty($data_lot)) :  ?>
            <button type="submit" class="only_visuel_gestion btn btn-sm btn-primary" id="terminer_facturation">
                <i class="fas fa-check-circle"></i>
                <span> Terminer</span>
            </button>
        <?php endif ?>
    </div>
</div>

<input type="hidden" name="fact_dossier_id" id="fact_dossier_id" value="<?= $dossier_id; ?>">

<?php if (!empty($data_lot)) : ?>
    <div class="row mt-3 mb-1 ms-3">
        <div class="col-3">
            <div class="row">
                <div class="col-auto">
                    <label for="inputField" class="form-label">
                        <h5>Mode de paiement : </h5>
                    </label>
                </div>
                <div class="col-auto">
                    <select class="form-select auto_effect" name="mode_paiement" id="mode_paiement">
                        <?php foreach ($mode_paiement as $mdpaie) : ?>
                            <option value="<?= $mdpaie->id_mode_paie ?>" <?= $data_lot[0]->id_mode_paie == $mdpaie->id_mode_paie ? 'selected' : '' ?>><?= $mdpaie->libelle ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <div class="col-3">
                    <label for="inputField" class="form-label">
                        <h5>IBAN : </h5>
                    </label>
                </div>
                <div class="col-8">
                    <input type="text" class="form-control auto_effect" name="iban" id="iban" value="<?= !empty($data_lot[0]->iban) &&  $data_lot[0]->iban != NULL ? $data_lot[0]->iban : '' ?>" autocomplete="off">
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <div class="col-3">
                    <label for="inputField" class="form-label">
                        <h5>BIC : </h5>
                    </label>
                </div>
                <div class="col-8">
                    <input type="text" class="form-control auto_effect" name="bic" id="bic" value="<?= !empty($data_lot[0]->bic) &&  $data_lot[0]->bic ? $data_lot[0]->bic : '' ?>">
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <div class="col-3">
                    <label for="inputField" class="form-label">
                        <h5>RIB : </h5>
                    </label>
                </div>
                <div class="col-8">
                    <?php if ($data_lot[0]->file_rib) : ?>
                        <?php
                        $nom_fic_rib = explode("/", $data_lot[0]->file_rib);
                        $count_rib_explode = count($nom_fic_rib);
                        ?>
                        <div class="doc">
                            <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $data_lot[0]->file_rib ?>','<?= $nom_fic_rib[$count_rib_explode - 1] ?>')" data-url="<?= base_url($data_lot[0]->file_rib) ?>" style="cursor:pointer"><?= $nom_fic_rib[$count_rib_explode - 1] ?></span>
                            <span class="badge rounded-pill badge-soft-light btn-close text-white" data-id="<?= $data_lot[0]->dossier_id ?>" id="removefilerib" style="cursor: pointer;">.</span><br>
                        </div>
                    <?php endif ?>
                    <input type="file" class="form-control auto_effect" name="file_rib" id="file_rib">
                </div>
            </div>
        </div>

    </div>
<?php endif ?>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped fs--1 mb-0">
        <?php if (empty($data_lot)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun lot trouvé
                    </div>
                </div>
            </div>
        <?php else :  ?>
            <thead class="bg-300 text-900">
                <tr>
                    <th class="text-center" scope="col">Numéro Lot</th>
                    <th class="text-center" scope="col">Nom programme</th>
                    <th class="text-center" scope="col">Numéro Mandat</th>
                    <th class="text-center" scope="col">MONTANT HT FACTURÉ EN 2023</th>
                    <th class="text-center" scope="col">MONTANT HT FIGURANT AU MANDAT</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data_lot as $key => $value) : ?>
                    <tr>
                        <td class="text-center"><?= str_pad($value->cliLot_id, 4, '0', STR_PAD_LEFT) ?></td>
                        <td class="text-center"><?= $value->progm_nom ?></td>
                        <td class="text-center"><?= !empty($value->mandat_id) ? str_pad($value->mandat_id, 4, '0', STR_PAD_LEFT) : '-' ?></td>
                        <td class="text-center p-1">
                            <input type="text" class="form-control m-0" style="text-align: right;" name="montant_ht_facture_<?= $value->cliLot_id ?>" id="montant_ht_facture_<?= $value->cliLot_id ?>" value="<?= round($value->montant_ht_facture, 2) ?>">
                        </td>
                        <td class="text-center p-1">
                            <input type="text" class="form-control m-0" style="text-align: right;" name="montant_ht_figurant_<?= $value->cliLot_id ?>" id="montant_ht_figurant_<?= $value->cliLot_id ?>" value="<?= round($value->montant_ht_figurant, 2) ?>">
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        <?php endif ?>
    </table>
</div>

<?php echo form_close(); ?>

<script>
    only_visuel_gestion();

    $(document).ready(function() {
        $('#iban').keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
    });
</script>