<?php
$regls_type = array(
    0 => 'REGLEMENT',
    1 => 'REMBOURSEMENT',
    2 => 'REJET DE PRELEVEMENT'
);
?>

<div class="modal-content" id="LoyerReference">
    <div class="modal-header">
        <h5 class="modal-title">
            Détail des règlements/remboursements pour cette facture :
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col">
                <table class="table table-bordered table-hover fs--1">
                    <thead class="text-white bg-primary">
                        <tr class="text-center">
                            <th>Numéro du règlement</th>
                            <th>Type </th>
                            <th>Mode </th>
                            <th>Date du règlement / remboursement</th>
                            <th>Montant </th>
                            <th class="no_account_manager">Action </th>
                        </tr>
                    </thead>
                    <tbody class="bg-200">
                        <?php if (!empty($reglements)) :  ?>
                            <?php foreach ($reglements as $rgl) : ?>
                                <tr>
                                    <?php $prefixe = $rgl->reglement_type == 0 ? 'REG' : 'RBT' ?>
                                    <td class="text-center"><?= $prefixe . '' . $rgl->idc_reglements ?></td>
                                    <td class="text-center"><?= isset($regls_type[$rgl->reglement_type]) ? $regls_type[$rgl->reglement_type] : '' ?></td>
                                    <td class="text-center"><?= array_key_exists($rgl->mode_reglement, $mode_reglement) ? $mode_reglement[$rgl->mode_reglement] : 'NON RENSEIGNE' ?></td>
                                    <td class="text-center"><?= date('d/m/Y', strtotime($rgl->date_reglement)) ?></td>
                                    <td class="text-end"><?= number_format($rgl->montant_total, 2) ?></td>
                                    <td style="white-space: nowrap;" class="no_account_manager">
                                        <button type="button" title="Enregistrer un remboursement" <?= $rgl->reglement_type == 0 ? '' : 'disabled'  ?> class="btn btn-sm btn-outline-danger saisir_Remboursements" data-reglementId="<?= $rgl->idc_reglements ?>" data-dossier_id="<?= $rgl->dossier_id ?>">
                                            <i class="fas fa-money-bill"></i> <i class="fas fa-minus-circle" style="height: 10px; width: 10px; margin-left: -6px; margin-bottom: 6px; padding: 0.5px;"></i>
                                        </button>

                                        <button type="button" title="Enregistrer un rejet de prélèvement" <?= $rgl->reglement_type == 0 ? '' : 'disabled'  ?> class="btn btn-sm btn-outline-warning saisir_RejetPrelevement" data-reglementId="<?= $rgl->idc_reglements ?>" data-dossier_id="<?= $rgl->dossier_id ?>">
                                            <i class="fas fa-money-bill"></i> <i class="fas fa-times-circle" style="height: 10px; width: 10px; margin-left: -6px; margin-bottom: 6px; padding: 0.5px;"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php else : ?>
                            <tr>
                                <td class="fw-bold text-center" colspan="6">Aucun règlement</td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" id="annuler_changement_reference" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
    </div>
</div>

<script>
    only_visuel_gestion(["1", "4"], "no_account_manager");
</script>