<style>
    .montant_reglt {
        cursor: pointer;
    }
</style>
<div class="contenair-title">
    <div class="row m-0">
        <div class="px-2">
            <h5 class="fs-1">Données de facturation de 2023 : </h5>
        </div>
    </div>
    <div class="px-2">
        <?php if (!empty($data_lot)) :  ?>
            <button type="button" data-id="<?= $dossier_id; ?>" id="modifierFacturationDossier" class="only_visuel_gestion btn btn-sm btn-primary btn-update-client">
                <span class="fas fa-edit fas me-1"></span>
                <span> Modifier</span>
            </button>
        <?php endif ?>
    </div>
</div>

<?php if (!empty($data_lot)) : ?>
    <div class="row mt-3 mb-3">
        <div class="col ms-4">
            <h5>Mode de paiement : <?= !empty($data_lot[0]->id_mode_paie) ? $data_lot[0]->libelle : $mode_paiement[0]->libelle ?></h5>
        </div>

        <div class="col ms-4">
            <h5>IBAN : <?= !empty($data_lot[0]->iban) &&  $data_lot[0]->iban != NULL ? formatIBAN($data_lot[0]->iban) : 'Non renseigné' ?></h5>
        </div>

        <div class="col ms-4">
            <h5>BIC : <?= !empty($data_lot[0]->bic) &&  $data_lot[0]->bic ? $data_lot[0]->bic : 'Non renseigné' ?></h5>
        </div>
        <div class="col ms-4">

            <h5>RIB : &nbsp;
                <?php if ($data_lot[0]->file_rib) : ?>
                    <?php
                    $nom_fic_rib = explode("/", $data_lot[0]->file_rib);
                    $count_rib_explode = count($nom_fic_rib);
                    ?>
                    <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $data_lot[0]->file_rib ?>','<?= $nom_fic_rib[$count_rib_explode - 1] ?>')" data-url="<?= base_url($data_lot[0]->file_rib) ?>" style="cursor:pointer"><?= $nom_fic_rib[$count_rib_explode - 1] ?></span>
                <?php else : ?>
                    Pas de Fichier
                <?php endif ?>
            </h5>
        </div>
    </div>
<?php endif ?>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped fs--1 mb-0">
        <?php if (empty($data_lot)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun lot trouvé
                    </div>
                </div>
            </div>
        <?php else :  ?>
            <thead class="bg-300 text-900">
                <tr>
                    <th class="text-center" scope="col">Numéro Lot</th>
                    <th class="text-center" scope="col">Nom programme</th>
                    <th class="text-center" scope="col">Numéro Mandat</th>
                    <th class="text-center" scope="col">MONTANT HT FACTURÉ EN 2023</th>
                    <th class="text-center" scope="col">MONTANT HT FIGURANT AU MANDAT</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data_lot as $key => $value) : ?>
                    <tr>
                        <td class="text-center"><?= str_pad($value->cliLot_id, 4, '0', STR_PAD_LEFT) ?></td>
                        <td class="text-center"><?= $value->progm_nom ?></td>
                        <td class="text-center"><?= !empty($value->mandat_id) ? str_pad($value->mandat_id, 4, '0', STR_PAD_LEFT) : '-' ?></td>
                        <td class="text-center"><?= !empty($value->montant_ht_facture) ? number_format($value->montant_ht_facture, 2) : number_format(0, 2) ?></td>
                        <td class="text-center"><?= !empty($value->montant_ht_figurant) ? number_format($value->montant_ht_figurant, 2) : number_format(0, 2) ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        <?php endif ?>
    </table>
</div>

<div class="col mt-4 ms-4">
    <h5 class="fs-1">
        Factures du client 411C<?= str_pad($dossier_id, 5, '0', STR_PAD_LEFT); ?>
    </h5>
</div>

<div class="col mt-3">
    <table class="table table-sm table-striped fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th class="text-center" scope="col">Numéro </th>
                <th class="text-center" scope="col">Type </th>
                <th class="text-center" scope="col" width="15%">Programme </th>
                <th class="text-center" scope="col">Date facture </th>
                <th class="text-center" scope="col">Mode règlement</th>
                <th class="text-center" scope="col">Montant HT</th>
                <th class="text-center" scope="col">Montant TVA</th>
                <th class="text-center" scope="col">Montant TTC</th>
                <th class="text-center" scope="col">Reste à payer</th>
                <th class="text-center" scope="col">Actions </th>
            </tr>
        </thead>
        <tbody>
            <?php $totalht = 0;
            $totaltva = 0;
            $totalttc = 0;
            $restepayer = 0; ?>
            <?php if (isset($facture) && !empty($facture)) : ?>
                <?php foreach ($facture as $fact) : ?>
                    <?php $totalht += $fact->fact_montantht;
                    $totaltva += $fact->fact_montanttva;
                    $totalttc += $fact->fact_montantttc;
                    $restepayer += $fact->fact_etatpaiement == 0 ? abs($fact->reste_a_payer) : 0; ?>
                    <tr>
                        <td class="text-center"><?= $fact->fact_num ?></td>
                        <td class="text-center"><?= $fact->fact_idFactureDu == NULL ? 'FACTURE' : 'AVOIR' ?></td>
                        <td class="text-center"> <?= $fact->progm_nom ?></td>
                        <td class="text-center"> <?= date('d/m/Y', strtotime($fact->fact_date)) ?></td>
                        <td class="text-center"> <?= array_key_exists($fact->id_mode_paie, $mode_reglement) ? $mode_reglement[$fact->id_mode_paie] : 'NON RENSEIGNE' ?></td>
                        <td class="text-center"><?= number_format($fact->fact_montantht, 2) ?></td>
                        <td class="text-center"><?= number_format($fact->fact_montanttva, 2) ?></td>
                        <td class="text-center">
                            <?= number_format($fact->fact_montantttc, 2) ?> <br>
                            <span class="montant_reglt text-primary fs--2" data-id="<?= $fact->fact_id ?>"><?= $fact->montant != NULL ? number_format($fact->montant, 2) : number_format(0, 2) ?></span>
                        </td>
                        <td class="text-center"><?= $fact->fact_etatpaiement == 0 ? number_format(abs($fact->reste_a_payer), 2) : number_format(0, 2) ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-sm btn-outline-success" title="Télécharger la facture" onclick="forceDownload('<?= addslashes(base_url() . $fact->doc_facturation_path) ?>')">
                                <span class="fas fa-download"> </span>
                            </button>
                            <?php
                            $date_info = date('d/m/Y', strtotime($fact->date_action));
                            $heure =  'à ' . date('H', strtotime($fact->date_action)) . 'H' . date('i', strtotime($fact->date_action));
                            $commentaire = $fact->fact_idFactureDu == null ? $fact->fact_commentaire : $fact->fact_JustificationAvoir;
                            $info_bulle = "Saisi le $date_info $heure par $fact->util_prenom $fact->util_nom \nCommentaire : $commentaire";
                            ?>
                            <button type="button" class="btn btn-sm btn-outline-info" title="<?= $info_bulle ?>">
                                <span class="fas fa-info"> </span>
                            </button>
                            <button type="button" class="btn btn-sm no_account_manager btn-outline-primary saisier_reglement_dossier" title="Enregistrer un règlement" <?= $fact->fact_etatpaiement == 1 || $fact->id_mode_paie == 3 || $fact->fact_idFactureDu != NULL ? 'disabled' : '' ?> data-fact_id="<?= $fact->fact_id ?>" data-dossier_id="<?= $fact->dossier_id ?>">
                                <span class="fas fa-money-bill"></span>
                            </button>
                            <button type="button" title="Créer un avoir" class="btn btn-sm no_account_manager btn-outline-danger creerAvoirDossier" data-fact_id="<?= $fact->fact_id ?>" <?= $fact->fact_idFactureDu != NULL ? 'disabled' : '' ?>>
                                <span class="fas fa-file-invoice-dollar"></span> <span class="fas fa-minus-circle" style="height: 10px; width : 10px; margin-left: -6px; margin-bottom: 6px;"></span>
                            </button>
                            <button type="button" title="Enregistrer un remboursement" <?= $fact->fact_idFactureDu == null ? 'disabled' : ''  ?> class="btn btn-sm no_account_manager btn-outline-danger saisir_RemboursementsAvoir" data-fact_id="<?= $fact->fact_id ?>" data-dossier_id="<?= $fact->dossier_id ?>">
                                <i class="fas fa-money-bill"></i> <i class="fas fa-minus-circle" style="height: 10px; width: 10px; margin-left: -6px; margin-bottom: 6px; padding: 0.5px;"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-primary" title="Envoyer un e-mail">
                                <span class="fas fa-envelope"></span>
                            </button>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="fw-bold text-center" colspan="5"> TOTAL </td>
                <td class="fw-bold text-center"><?= number_format($totalht, 2) ?></td>
                <td class="fw-bold text-center"><?= number_format($totaltva, 2) ?></td>
                <td class="fw-bold text-center"><?= number_format($totalttc, 2) ?></td>
                <td class="fw-bold text-center"><?= number_format(abs($restepayer), 2) ?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>

<script>
    only_visuel_gestion(["1", "4"], "no_account_manager");
</script>