<?php echo form_tag('Clients/cruDossier',array('method'=>"post", 'class'=>'px-2','id'=>'cruDossier')); ?>

    <div class="contenair-title">
        <div class="px-2">
            <h5 class="fs-0"></h5>
        </div>
        <div class="px-2">
            <button type="button" data-id="<?=$client_id;?>" id="annulerFormDossier" class="btn btn-sm btn-secondary mx-1">
                <i class="fas fa-times me-1"></i>
                <span> Annuler</span>
            </button>

            <button type="submit" id="save-dossier" class="btn btn-sm btn-primary mx-1">
                <i class="fas fa-plus me-1"></i>
                <span> Enregistrer </span>
            </button>
        </div>
    </div>

    <?php if(isset($dossier->dossier_id)):?>
        <input type="hidden" name="dossier_id" id="dossier_id" value="<?=$dossier->dossier_id;?>">
    <?php endif;?>

    <input type="hidden" name="action" id="action" value="<?=$action?>">
    <input type="hidden" name="client_id" id="client_id" value="<?=$client_id;?>">

    <div class="row m-0">
        <div class="col">
       

        </div>
        <div class="col py-2">

            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Numero dossier </label>
                            <input type="text" class="form-control" value ="<?= isset($doss_id) ? str_pad($doss_id->dossier_id + 1, 4, '0', STR_PAD_LEFT) : '0001' ;?>" id="dossier_id" name="dossier_id" disabled>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline switch">
                        <div>
                            <label data-width="250"> Statut dossier</label>
                            <div class="form-check form-switch">
                                <span> Inactif </span>
                                <input class="form-check-input" type="checkbox" id="dossier_etat" name="dossier_etat"  <?php echo isset($dossier->dossier_etat) ? ($dossier->dossier_etat=="1" ? "checked" :"") : "checked" ;?>>
                                <span> Actif </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col">
            

        </div>
    </div>
<?php echo form_close(); ?>


