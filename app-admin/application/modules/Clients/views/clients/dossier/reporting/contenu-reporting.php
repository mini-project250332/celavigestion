<div class="contenair-title">
    <div class="row m-0" style="width: 60%">
        <div class="col-7">
            <h5 class="fs-1">Reporting client</h5>
        </div>
        <div class="col-3">            
            <select class="form-select fs--1 m-1 no_effect" id="reporting_annee" name="reporting_annee" style="width: 96%;" data-id="<?= $dossier_id; ?>">
                <?php for ($i = intval(date("Y")); $i >= intval($annee_reporting); $i--) { ?>
                    <option value="<?= $i ?>" <?= $i ?>>
                        <?= $i ?>
                    </option>
                <?php } ?>
            </select>    
            <!-- <?= ($i == $reporting[0]->reporting_annee) ? "selected" : "" ?>         -->
        </div>
    </div>
</div>
<input type="hidden" id="date_reporting" value="<?=$date_reporting?>">
<div class="row pt-3 m-0 panel-container" id="content-reporting">

</div>

<script>
    getReportingAnnee(<?= $dossier_id; ?>);
</script>