<div class="col-5 panel-left" style="width: 911px;">    
    <div class="row tableau_reporting  mt-2">
        <div class="col">
            <table class="table table-hover fs--1">
                <thead class="text-white">
                    <tr class="">
                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Période
                        </th>
                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Date de génération
                        </th>
                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Date d'envoi
                        </th>
                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Etat
                        </th>                        
                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;background:#2569C3;width: 20%;">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-200 fs--1">
                    <?php foreach ($reporting as $key => $value) : ?>
                        <tr class="text-center apercu_reporting <?= (($value->reporting_trimestre < $trimestre) && $year_reporting == $value->reporting_annee ) ? 'bg-300' : '' ?>" id="rowdoc-<?= isset($value->reporting_id) ? $value->reporting_id : ""; ?>">
                            <td style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $value->reporting_id ?>" onclick="apercuReporting(<?= $value->reporting_id ?>)"> <b><?= $value->reporting_trimestre ?></b> </td>
                            <td> <?= isset($value->reporting_date_generation) ? date("d/m/Y", strtotime(str_replace('/', '-', $value->reporting_date_generation))) : "-" ?> </td>
                            <td> <?= isset($value->reporting_date_envoi) ? date("d/m/Y", strtotime(str_replace('/', '-', $value->reporting_date_envoi))) : "-" ?> </td>
                            <td> <?= $value->reporting_etat ?> </td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn-sm btn-outline-warning icon-btn btn-genenerPdf" type="button" data-id="<?= $value->reporting_id ?>" data-dossier="<?= $dossier_id ?>"
                                    <?= (($value->reporting_trimestre < $trimestre) && $year_reporting == $value->reporting_annee ) ? 'disabled' : '' ?>>
                                        <i class="fas fa-file-pdf"></i> Générer
                                    </button> &nbsp;
                                    <button class="btn btn-outline-secondary envoyer-reporting" data-id="<?=$value->reporting_id?>" data-dossier="<?=$dossier_id?> type="button">
                                        <i class="fas fa-share-square"></i> Envoyer
                                    </button> &nbsp;
                                    <button class="btn btn-outline-primary " type="button" onclick="forceDownload('<?= base_url() . $value->reporting_path ?>')" data-url="<?= base_url($value->reporting_path) ?>">
                                        <i class="fas fa-download"></i> Télecharger
                                    </button>

                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="splitter m-2 ">

</div>
<div class="col-5 panel-right">
    <div id="apercu_doc_reporting">
        <div id="nonApercu" style="display:none;">
            <div class="text-center text-danger pt-10">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                fichier
            </div>
        </div>
        <div id="apercuReporting">
            <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
            </div>
        </div>
    </div>
</div>