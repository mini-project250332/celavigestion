<?php if (!empty($max_date['annee'])) : ?>
    <div class="row questionnaire_reporting">
        <div class="col-3">

        </div>
        <?php
        if (isset($max_date)) {
            $date_signature = $max_date['annee'];
            $date = new DateTime($date_signature);
            $date->add(new DateInterval('P3M'));
            $date->modify('first day of next month');
            if ($date_signature >= date('Y-07-01')) {
                $dateDonnee = date('Y-10-15');
            } else {
                $dateDonnee = $date->setDate($date->format('Y'), $date->format('m'), 15);
            }

            $max_year = strtotime($date_signature);
            $year = date('Y', $max_year);
        } else {
            $year = 2022;
        }
        ?>
        <input type="hidden" id="dossier_id" value="<?=$dossier_id?>">
        <input type="hidden" id="etat_reporting" value="<?=empty($reporting) ? 0 : 1 ?>">
        
        <div class="col">
            <div class="card overflow-hidden bg-200" style="margin-top: 10%;">
                <div class="card-body">
                    <h4 class="card-title text-center">A partir de quelle date souhaitez-vous gérer le reporting client ?</h4>
                </div>
                <div class="form-group inline date_reporting">
                    <div class="" style="margin-left: 18%;">
                        <label data-width="100"> Date : </label>
                        <input type="date" class="form-control" value="<?= $date->format('Y-m-d') ?>" id="annee_reporting" name="" style="width : 50%">                        
                        <button type="button" class="btn btn-primary m-2" id="valider_date_reporting" style="bottom: 10px;">Valider</button>
                        <div class="help"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">

        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-3">

        </div>

        <div class="col">
            <div class="card overflow-hidden bg-200" style="margin-top: 10%;">
                <div class="card-body">
                    <h4 class="card-title text-center">Vous ne pouvez pas encore gérer le reporting pour ce client <br>
                        car aucun mandat n'a été signé
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-3">

        </div>
    </div>
<?php endif ?>

<div id="contenu-reporting">

</div>