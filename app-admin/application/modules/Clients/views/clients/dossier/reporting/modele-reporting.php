<html>

<head>
    <style>
        @page {
            margin: 0;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            position: relative;
        }

        .background_image {
            width: 100%;
            height: 1118.74px;
            object-fit: cover;
        }

        .container {
            position: absolute;
            top: 0;
            left: 38%;
            width: 100%;
            height: 100%;
            background-color: #102138;
        }

        .container-left {
            position: absolute;
            top: 0;
            width: 80%;
            height: 100%;
            background-color: white;
        }

        .content {
            position: absolute;
            top: 0;
            left: 10%;
            transform: translate(-50%, -50%);
            text-align: left;
            margin-left: 5%;
            padding-top: 5%;
            color: white;
        }

        .content-center {
            position: absolute;
            top: 27%;
            left: 13%;
            width: 100%;
            height: 33%;
            background-color: #5978a0;
        }

        .content-center-resume {
            position: absolute;
            top: 5%;
            /* right: 13%; */
            width: 90%;
            height: 18%;
            background-color: #5978a0;
            text-align: center;
            color: #fff;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 50px;
            letter-spacing: 6px;
            margin-left: 3px;
        }

        .titre1 {
            font-family: 'Times New Roman', Times, serif;
            color: #fff;
            font-size: 20px;
            letter-spacing: 3px;
        }

        .titre2 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 45px;
            margin-left: 17%;
            letter-spacing: 3px;
            font-weight: bold;
        }

        .titre3 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 25px;
            letter-spacing: 3px;
            font-weight: bold;
        }

        .name {
            margin-top: -12%;
            margin-left: 20%;
            font-size: 25px;
        }

        .image_logo {
            background-color: #fff;
            width: 130px;
            height: 130px;
            margin-left: 3%;
        }

        .logo {
            width: 50%;
            height: 50%;
            margin-top: 25%;
            margin-left: 25%;
        }

        .text1 {
            color: #eae317;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-top: 105%;
            padding-left: -2%;
            font-size: 20px;
            letter-spacing: 2px;
        }

        .entete {
            background-color: #5978a0;
            width: 100%;
            text-align: center;
            height: 135px;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            letter-spacing: 5px;
            padding-top: 3%;
        }

        .title {
            color: #ea3f3f;
            font-size: 30px;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            letter-spacing: 4px;
        }

        .liste {
            line-height: 0.2;
        }

        .hr-container {
            position: relative;
            width: 45%;
        }

        .hr-left {
            position: absolute;
            border: none;
            height: 5px;
            color: black;
        }

        .hr-right {
            position: absolute;
            border: none;
            height: 5px;
            width: 200px;
            text-align: right;
            color: black;
        }

        .footer {
            background-color: #102138;
            color: #fff;
            height: 32px;
            font-size: 13px;
            letter-spacing: 2px;
            margin-top: 40px;
        }

        .span-left {
            background-color: #5978a0;
            height: 217px;
            width: 80%;
            text-align: center;
            color: #fff;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 50px;
            letter-spacing: 6px;
            padding-top: 15px;
        }

        .content-left {
            font-size: 50px;
            padding-top: 40%;
            margin-left: 5%
        }
    </style>
</head>

<body>
    <img class="background_image" src="<?php echo base_url('../assets/img/image1.jpeg'); ?>">
    <div class="container">
        <div class="content">
            <p class="titre1"><?= $contenu_pdf['nom_trimestre'] ?> <?= $contenu_pdf['rep_annee'] ?> </p>
            <p class="text1"> <b>PRÉPARÉ PAR : <br> <?= $contenu_pdf['nom_util'] ?> 
            </p>
        </div>
    </div>

    <div class="content-center">
        <p class="titre2">RAPPORT DE <br>
            GESTION <br>
            <span class="titre3"><?= $contenu_pdf['nom_trimestre'] ?> <?= $contenu_pdf['rep_annee'] ?></span>
        </p>

        <br>
        <div class="image_logo">
            <img class="logo" src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo">
        </div>
        <div>
            <p class="name"><i>Monsieur <?= $contenu_pdf['nom_proprietaire'] ?></i></p>
        </div>
    </div>
    <?php foreach ($contenu_pdf['data_lot'] as $key => $value) : ?>

        <div style="page-break-before: always;">
            <br><br>
            <div class="entete">
                <p>
                    <span style="font-size: 45px;"><b>VOTRE INVESTISSEMENT</b></span> <br>
                    <span style="font-size: 25px; color: #eae317;"> <b><?= $value['nom_programme'] ?></b> </span>
                </p>
            </div>
            <div style="padding-left: 5%; padding-top: 3%;">
                <span class="title">
                    <b>Valorisation</b>
                </span>
            </div> <br>
            <div class="section-left" style="padding-left: 5%;">
                <p class="liste">Date d'achat:</p>
                <p class="liste">Exploitant</p>
                <p class="liste">Typologie</p>
                <p class="liste">Durée restante de votre bail :</p>
                <p class="liste">Prix de revient</p>
                <div class="hr-container">
                    <hr class="hr-left">
                </div>
                <p style="font-size: 17px; letter-spacing: 3px;">
                    Valeur estimée de votre bien <br>
                    Plus-Value Brute <br>
                    Solde après remboursement du prêt <br>
                </p>
                <div class="hr-container">
                    <hr class="hr-left">
                </div>
            </div>
            <div class="section-right" style="padding-left: 55%; padding-top: -290px; margin-right: 5%">
                <p class="liste" style="text-align: right;"><?= isset($value['date_achat']) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['date_achat']))) : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['exploitant']) ? $value['exploitant'] : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['typologie']) ? $value['typologie'] : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->bail_id) ? $value['years'] . ' ' . 'ans' . ',' . $value['months'] . ' ' . 'mois' : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= ($value['prix_revient'] != 0) ? $value['prix_revient'] : 'Information non fournie' ?></p>
                <div class="" style="width: 5% !important;">
                    <hr class="hr-right">
                </div>
                <p style="font-size: 15px; text-align: right; margin-top: 2%">
                    <?= ($value['valeur_bien']) != 0 ? round($value['valeur_bien'],2). 'HT' : "Information non fournie" ?><br>
                    <?= ($value['value_brute']) != 0 ? round($value['value_brute'],2). 'HT' : "Information non fournie" ?><br>
                    <?= ($value['value_brute']) != 0 ? round($value['capital_restant'],2).'HT' : "Information non fournie"?> <br>
                </p>
                <div class="" style="width: 5% !important;">
                    <hr class="hr-right">
                </div>
            </div> <br>
            <div style="padding-left: 5%;">
                <span class="title">
                    <b>Votre assurance PNO</b>
                </span>
            </div>
            <div style="padding-left: 5%;padding-bottom: -2%">
                <?php if ($value['pno_client'] == 1 || $value['pno_syndic'] == 1) : ?>
                    <p style="font-size: 18px; letter-spacing: 2px;">Montant de votre assurance PNO : <?= isset($value['pno_montant']) ? $value['pno_montant'] . ' ' . 'Euros' : "Information non fournie" ?></p>
                <?php elseif ($value->pno_celavi == 2 && ($value['pno_syndic'] == 2 || $value['pno_syndic'] == 3) && $value['pno_client'] == 2) : ?>
                    <p style="font-size: 14px; letter-spacing: 2px;">Selon les informations fournies, vous n’avez pas souscrit à l’assurance obligatoire PNO. Votre bien n’est donc pas couvert pour certains risques.</p>
                <?php else : ?>
                    <p style="font-size: 14px; letter-spacing: 2px;"> Montant de votre assurance PNO : Information non fournie </p>
                <?php endif; ?>
            </div>
            <div style="padding-left: 5%;padding-right: 5%;">
                <p style="font-size : 18px;"><i>Une obligation de la LOI ALUR, CELAVIGESTION peut vous proposer une assurance PNO à partir de 49 Euros TTC par lot</i> </p>
            </div>
            <div style="padding-left: 5%;">
                <span class="title">
                    <b>Votre bail</b>
                </span>
            </div>
            <div class="section-left" style="padding-left: 5%;">
                <p class="liste">Nom du preneur :</p>
                <p class="liste">Type de Bail :</p>
                <p class="liste">Durée de votre bail :</p>
                <p class="liste">Durée restante de votre bail :</p>
                <p class="liste">Paiement du loyer :</p>
                <p class="liste">Période d'indexation :</p>
                <p class="liste">Indice d'indexation :</p>
                <div class="hr-container">
                    <hr class="hr-left">
                </div>
            </div>
            <div class="section-right" style="padding-left: 50%; padding-top: -25%; margin-right: 5%">
                <p class="liste" style="text-align: right;"><?= isset($value['exploitant']) ? $value['exploitant'] : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->tpba_description) ? $value['bail']->tpba_description : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->bail_annee) ? $value['bail']->bail_annee . ' ' . 'ans' . ',' . $value['bail']->bail_mois . ' ' . 'mois' : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->bail_id) ? $value['years'] . ' ' . 'ans' . ',' . $value['months'] . ' ' . 'mois' : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->pdl_description) ? $value['bail']->pdl_description . ' à terme' . ' ' . $value['bail']->momfact_nom . ' ' . 'le' . ' ' . $value['bail']->tpech_valeur . ' ' . 'du mois' : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->prdind_valeur) ? $value['bail']->prdind_valeur . ' ' . 'an(s)' : "Information non fournie" ?></p>
                <p class="liste" style="text-align: right;"><?= isset($value['bail']->indloyer_description) ? $value['bail']->indloyer_description : "Information non fournie" ?></p>
                <div class="hr-container" style="width: 5% !important;">
                    <hr class="hr-right">
                </div>
            </div>
            <p style="font-size: 14px;margin-left: 5%;">Votre bien est exploité sous bail commercial. La méthode d’évaluation retenue est la méthode du rendement. La rentabilité attendue par un investisseur sur ce type de bien serait d’environ <?=isset($contenu_pdf['progm_pourcentage_evaluation']) ? $contenu_pdf['progm_pourcentage_evaluation'].'%' : "XX (pourcentage)" ?> </p>
            <p style="font-size: 10px;margin-left: 5%; line-height: 0.2">Cette évaluation n’est qu’une première estimation de ce que pourrait être la valeur de votre bien.</p>
            <p style="font-size: 10px;margin-left: 5%; font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                Une évaluation réalisée par notre équipe devra confirmer le prix proposé dans le cas où vous souhaiteriez mettre en vente votre bien. En effet, une étude plus approfondie de nombreux éléments devra êtreréalisée :
                marché immobilier actuel, état général de l’immeuble, étude détaillée du bail et de ses avenants, situation locative réelle (solvabilité du locataire, paiement des loyers, etc), localisation, travaux àprévoir, état du mobilier, etc.
            </p>
            <!-- <img src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo" style="padding-left: 24px;width: 7%;height: 7%;"><br> -->
            <!-- <div class="footer">
            <div style="padding-left: 5%;padding-top: -7px;">
                <p>
                    CELAVIGESTION
                </p>
            </div>
            <div style="margin-left: 85%;margin-right: 5%;padding-top: -43px;">
                <p>
                    PAGE 2
                </p>
            </div>
        </div> -->
        </div>
        <div class="section3" style="page-break-before: always;"></div>
        <img class="background_image" src="<?php echo base_url('../assets/img/image2.jpeg'); ?>">
        <div class="container-left">
            <div class="content-left">
                <div style="text-align: center; color:#ea3f3f; letter-spacing: 3px; font-size: 30px;font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; padding-top: 5%;padding-bottom: -5%">
                    <span>
                        <b>
                            <?php if($value['bail']->pdl_id == 3) :?>
                                <?=$contenu_pdf['nom_trimestre']=='T1' || $contenu_pdf['nom_trimestre']=='T2' ? 'SEMESTRE S1' : 'SEMESTRE S2'?>
                            <?php endif; ?> 
                        </b>
                     </span>
                </div>
                <p>
                    <span style="color:#eae317;font-size: 25px; letter-spacing:9px;font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                        <b><?= !empty($value['loyers_facture']['loyers']) ? $value['loyers_facture']['loyers'] . 'HT' : "Information non fournie" ?></b>
                    </span> <br>
                    <span style="font-size: 20px;letter-spacing: 2px">Montant des loyers Facturés</span>
                </p>
                <p style="margin-top: 18%">
                    <span style="color:#eae317;font-size: 25px; letter-spacing:9px;font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                        <b><?= $value['loyers_encaisse'] ?></b>
                    </span> <br>
                    <span style="font-size: 20px;letter-spacing: 2px">Montant des Loyers encaissés</span>
                </p>
                <p style="margin-top: 18%">
                    <span style="color:#eae317;font-size: 25px; letter-spacing:9px;font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                        <b><?= $value['solde_restant'] ?></b>
                    </span> <br>
                    <span style="font-size: 20px;letter-spacing: 2px">Solde restant dû</span>
                </p>
            </div><br><br>
            <div>
                <img src="<?php echo base_url('../assets/img/logo.jpeg'); ?>" alt="logo" style="padding-left: 24px;width: 11%; height: 10%;">
            </div>
        </div>
        <div class="content-center-resume">
            <p style="margin-top: 5%">
                <b>RÉSUMÉ FINANCIER <br>
                    AU <?php if ($contenu_pdf['nom_trimestre'] == 'PREMIER TRIMESTRE') {
                            echo '1ER TRIMESTRE';
                        } elseif ($contenu_pdf['nom_trimestre'] == 'DEUXIEME TRIMESTRE') {
                            echo '2EME TRIMESTRE';
                        } elseif ($contenu_pdf['nom_trimestre'] == 'TROISIEME TRIMESTRE') {
                            echo '3EME TRIMESTRE';
                        } else {
                            echo '4EME TRIMESTRE';
                        }

                        ?>
                </b>
            </p>
        </div>
        <div class="section4" style="page-break-before: always;">
            <br><br>
            <div class="entete">
                <p>
                    <span style="font-size: 45px;"><b>ACTIONS MENEES</b></span>
                </p>
            </div><br>
            <div style="text-align: center; color:#ea3f3f; letter-spacing: 3px; font-size: 30px;font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                <!-- <span><b><?= $value->progm_nom ?></b> </span> -->
            </div> <br>
            <p style="font-size: 15px;margin-left: 5%"><i>Nous vous relatons ci-dessous l’ensemble des actions et interventions réalisées pour votre compte dans le cadre du mandat qui nous uni.</i> </p>
            <div style="letter-spacing: 3px;font-size: 15px;margin-left: 5%">
                <?php if(isset($value['bail']->bail_id)) : ?>
                    <p>Analyse, lecture et identification des points de facturations <br>et clauses de votre bail</b></p>
                    <p style="line-height: 0.2">Calcul de votre indexation</p>
                <?php endif; ?>           
                <p style="line-height: 0.2">Demande de Remboursement TEOM: <b><?= isset($value['taxe_fonciere']->taxe_date_envoi_mail) ? $value['taxe_fonciere']->taxe_date_envoi_mail  : "Information non fournie" ?></b></p>
                <p style="line-height: 0.2">Etablissement des factures: 
                    <b>
                        <?php 
                            switch ($value['bail']->pdl_id) {
                                case 1:
                                    echo  isset($value['loyers_facture']['date_facture'][0]) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture'][0]))).','.date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture'][1]))).','.date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture'][2]))) : "Information non fournie";
                                    break;
                                case 3:
                                    echo  isset($value['loyers_facture']['date_facture']) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture']))) : "Information non fournie";
                                    break;
                                case 4:
                                    echo  isset($value['loyers_facture']['date_facture']) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture']))) : "Information non fournie";
                                    break;
                                case 5:
                                    echo isset($value['loyers_facture']['date_facture']) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture']))) : "Information non fournie";
                                    break;                                
                                default:
                                    echo isset($value['loyers_facture']['date_facture']) ? date("d/m/Y", strtotime(str_replace('/', '-', $value['loyers_facture']['date_facture']))) : "Information non fournie";
                                    break;
                            }
                        ?>
                    </b>
                </p>
                <p style="line-height: 0.2">Relance loyers impayés: <b>Non géré dans l'application</b></p>
                <p style="line-height: 0.2">Actions et temps passés : <b> Informations non fournies</b></p>
            </div><br><br>
        </div>
    <?php endforeach; ?>
</body>

</html>