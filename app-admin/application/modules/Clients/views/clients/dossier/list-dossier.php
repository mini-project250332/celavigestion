<div class="contenair-title">
    <div class="row m-0">
        <div class="px-2">
            <h5 class="fs-1">Liste des dossiers</h5>
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <button data-id="<?= $client_id; ?>" id="btnFormAddDossier" class="btn btn-sm btn-primary m-1 only_visuel_gestion" type="submit">
            <span class="fas fa-plus me-1"></span>
            <span> Ajouter </span>
        </button>
    </div>
</div>
<div class="table-responsive scrollbar pt-2">
    <table class="table table-sm table-striped fs--1 mb-0">
        <?php if (empty($listDossier)): ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun dossier trouvé
                    </div>
                </div>
            </div>
        <?php else: ?>
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Numéro</th>
                    <th scope="col">Noms propriétaires</th>
                    <th scope="col">Date création</th>
                    <th scope="col">Status</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>

            <tbody>

                <?php foreach ($listDossier as $key => $value): ?>
                    <?php
                        $dossier_etat = $value->dossier_etat;
                        $etat;
                        if($dossier_etat==1){
                            $etat = "Actif";
                        }else{
                            $etat = "Inactif";
                        }
                    ?>
                    <?php if (!empty($dossier_lot_unique)) : ?>
                        <tr id="rowDossier-<?= $value->dossier_id; ?>" class="align-middle <?= in_array($value->dossier_id, $dossier_lot_unique) ? 'bg-warning text-white' : 'bg-200' ?>">
                    <?php else : ?>
                        <tr id="rowDossier-<?= $value->dossier_id; ?>" class="align-middle">
                    <?php endif ?>
                        <td class="px-2">
                            <div class="d-flex align-items-center position-relative">
                                <div class="flex-1">
                                    <h6 class="mb-0 fw-semi-bold">
                                        <a class="stretched-link text-900" href="#">
                                            <?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></a>
                                    </h6>
                                </div>
                            </div>
                        </td>
                        <td><?= $value->client; ?></td>
                        <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $value->dossier_d_creation))); ?></td>
                        <td><?= $etat; ?></td>
                        <td class="btn-action-table">

                            <div class="d-flex justify-content-end ">

                                <button data-client_id="<?= $client_id; ?>" data-id="<?= $value->dossier_id; ?>"
                                    class="btn btn-sm btn-outline-primary icon-btn fiche-dossier-client" type="button" id="dossier_btn_<?=$value->dossier_id ?>">
                                    <span class="fas fa-play fa-w-14 fs-1"></span>
                                </button>

                                <button data-client_id="<?= $client_id; ?>" data-id="<?= $value->dossier_id; ?>"
                                    class="btn btn-sm btn-outline-danger icon-btn delete-dossier-client only_visuel_gestion"
                                    type="button">
                                    <span class="bi bi-trash fs-1"></span>
                                </button>
                            </div>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif ?>
    </table>

</div>

<script>
    const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const dossier_id = urlParams.get('_do@t_id');
	if (dossier_id) $(`#dossier_btn_${dossier_id}`).trigger("click");
</script>