<div class="col ms-2">
    <div class="modal-header">
        <h5 class="modal-title" id="title_modal_valid_transform">
            Confirmation de la validation
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col">
                <p class="text-center fs--1">Voulez-vous vraiment demander la clôturer ? </p>
                <div class="form-group inline">
                    <div>
                        <label class="fs--1" data-width="200"> Expert Comptable : </label>
                        <select class="form-select fs--1" id="util_expert_comptable" name="util_expert_comptable">
                            <option value="<?= NULL ?>">Non choisi</option>
                            <?php foreach ($util_expert as $utilisateurs) : ?>
                                <option value="<?= $utilisateurs->util_id; ?>">
                                    <?= $utilisateurs->util_prenom . ' ' . $utilisateurs->util_nom; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <div class="help text-danger message_erreur_exp"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-id="<?= $cloture_id ?>" id="confirmDemande" class="btn btn-success">Confirmer</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
    </div>
</div>