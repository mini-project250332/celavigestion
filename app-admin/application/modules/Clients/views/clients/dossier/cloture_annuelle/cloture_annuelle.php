<div class="h-100 w-100">
    <div class="contenair-title">
        <div class="col-5">
            <h5 class="fs-1 ms-2">Clôture annuelle </h5>
        </div>
        <div class="col">
            <input type="hidden" name="dossier_id_annee_cloture" id="dossier_id_annee_cloture" value="<?= $dossier->dossier_id; ?>">
        </div>
    </div>

    <div class="ms-2" id="contenair-cloture_annuelle">

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        getCloture_Annuelle(<?= $dossier->dossier_id; ?>);
    });
</script>