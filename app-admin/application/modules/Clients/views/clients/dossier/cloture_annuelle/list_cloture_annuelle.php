<div class="col pt-4">
    <table class="table table-sm table-striped fs--1">
        <thead class="bg-300 text-900">
            <tr>
                <th class="text-center" scope="col">Année</th>
                <th class="text-center" scope="col">Demande de validation</th>
                <th class="text-center" scope="col">Validation </th>
                <th class="text-center" scope="col">Expert comptable</th>
                <th class="text-center" scope="col">Etat </th>
                <th class="text-center" scope="col">Actions </th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($liste_cloture_annuelle)) :
                foreach ($liste_cloture_annuelle as $liste) : ?>
                    <tr>
                        <td class="text-center"><?= $liste->annee ?></td>
                        <td class="text-center">
                            <?php if (!empty($liste->date_demande_validation)) : ?>
                                <?= date('d/m/Y', strtotime($liste->date_demande_validation)) ?> <br>
                                Démandé par <?= $liste->util_ask_prenom . ' ' . $liste->util_ask_nom ?>
                            <?php else : ?>
                                -
                            <?php endif ?>
                        </td>
                        <td class="text-center">
                            <?php if (!empty($liste->date_validation)) : ?>
                                <?= date('d/m/Y', strtotime($liste->date_validation)) ?> <br>
                                Validé par <?= $liste->util_validated_prenom  . ' ' . $liste->util_validated_nom ?>
                            <?php else : ?>
                                -
                            <?php endif ?>
                        </td>
                        <td class="text-center">
                            <?= !empty($liste->util_expert_comptable) ? $liste->util_exp_prenom  . ' ' . $liste->util_exp_nom : '-' ?>
                        </td>
                        <td class="text-center">
                            <?php
                            $dataKey = $liste->etat;
                            if (array_key_exists($dataKey, $etat_array)) {
                                $value = $etat_array[$dataKey];
                                echo $value;
                            }  ?>
                        </td>
                        <td class="text-center">
                            <!-- if there is one File cloture -->

                            <?php if (!empty($last_cloture) && $liste->annee <= $last_cloture->annee + 1) : ?>
                                <?php if ($liste->etat == 0) : ?>
                                    <button class="btn btn-outline-primary demander_cloture" data-id="<?= $liste->cloture_id ?>" title="demander la validation"><i class="fas fa-user-check fa-lg"></i></button>
                                <?php elseif ($liste->etat == 1) : ?>
                                    <button class="btn btn-outline-success valider_cloture validation_containt" data-id="<?= $liste->cloture_id ?>" title="valider "><i class="fas fa-check-square fa-lg"></i></button>
                                <?php elseif ($liste->etat == 2) : ?>
                                    <button class="btn btn-outline-danger annuler_cloture validation_containt" data-id="<?= $liste->cloture_id ?>" title="annuler"><i class="fas fa-window-close fa-lg"></i></button>
                                <?php endif ?>
                                <!-- if there isn't one File  cloture -->
                            <?php else : ?>
                                <?php if (!empty($last_annee) && $liste->annee <= $last_annee->annee) : ?>
                                    <?php if ($liste->etat == 0) : ?>
                                        <button class="btn btn-outline-primary demander_cloture" data-id="<?= $liste->cloture_id ?>" title="demander la validation"><i class="fas fa-user-check fa-lg"></i></button>
                                    <?php elseif ($liste->etat == 1) : ?>
                                        <button class="btn btn-outline-success valider_cloture validation_containt" data-id="<?= $liste->cloture_id ?>" title="valider "><i class="fas fa-check-square fa-lg"></i></button>
                                    <?php elseif ($liste->etat == 2) : ?>
                                        <button class="btn btn-outline-danger annuler_cloture validation_containt" data-id="<?= $liste->cloture_id ?>" title="annuler"><i class="fas fa-window-close fa-lg"></i></button>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php endif ?>
                            
                        </td>
                    </tr>
                <?php endforeach;
            else :  ?>
                <tr>
                    <td class="text-center" colspan="5">
                        <h5>Aucune Donnée</h5>
                    </td>
                </tr>
            <?php endif   ?>
        </tbody>
    </table>
</div>

<script>
    only_visuel_gestion(["1", "2"], "validation_containt");
</script>