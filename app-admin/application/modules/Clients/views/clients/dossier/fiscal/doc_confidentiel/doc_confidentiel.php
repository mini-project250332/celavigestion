<div class="w-100" id="contenair-fiscal-doc_confidentiel">

</div>

<!-- modal -->
<div class="modal fade" id="modalAjoutConfidentiel" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="UploadConfidentiel">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        getDocConfidentiel(<?= $dossier->dossier_id; ?>);
    });
</script>