<div class="contenair-title m-0 p-0">
    <?php echo form_tag('Fiscal/AddAcompteTva', array('method' => "post", 'class' => 'px-2', 'id' => 'AddAcompteTva')); ?>
    <div class="row m-0" style="width: 43%">
        <input type="hidden" name="dossier_id" id="dossier_id" value="<?= $dossier_id ?>">
        <div class="col-6">
            <h5 class="fs-1 mt-3">Documents de la déclaration TVA </h5>
            <div class="form-group inline mt-3">
                <label class="fs-1" data-width="150">Acompte TVA </label>
                <input class="form-check-input m-2 p-2" type="checkbox" name="acompte_tva" <?php echo isset($dossier->acompte_tva) ? ($dossier->acompte_tva == "1" ? "checked" : "") : "checked"; ?> id="acompte_tva">
            </div>
        </div>
        <div class="col-4 mt-3">
            <select class="form-select fs--1 m-1 no_effect" id="doc_tva_annee" name="doc_tva_annee" style="width: 86%;" data-id="<?= $dossier_id ?>">
                <?php for ($i = intval(date("Y")); $i >= 2022; $i--) { ?>
                    <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                        <?= $i ?>
                    </option>
                <?php } ?>
                <option value="0">Liasses antérieures</option>
            </select>
        </div>
        <div class="col-2 mt-3">
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $dossier_id ?>" id="ajoutdocTva">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<div class="row pt-3 m-0 panel-container" id="content-tva">

</div>

<script>
    getDoctvaAnnee(<?= $dossier_id ?>);
</script>