<style>
    .table_td {
        border-right: 1px solid #ffff;
        border-bottom: 1px solid #ffff;
    }
</style>

<div class="row">
    <div class="col">
        <table class="table table-hover fs--1">
            <thead class="text-white" style="background:#2569C3;">
                <tr>
                    <th class="p-2 text-center table_td">Année</th>
                    <th class="p-2 text-center table_td">Déficits antérieurs</th>
                    <th class="p-2 text-center table_td">Bénéfices imputés</th>
                    <th class="p-2 text-center table_td">Déficits reportables</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php foreach ($deficits as $key => $value) : ?>
                    <?php if ($value['deficits_annee'] <= (intval(date('Y')) - 10)) : ?>
                        <tr>
                            <td class="text-center p-2 table_td">
                                <?= $value['deficits_annee'] ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                
                            </td>
                            <td class="text-end p-2 table_td">
                                
                            </td>
                            <td class="text-center p-2 table_td">
                                <?= 'Prescrit' ?>
                            </td>
                        </tr>
                    <?php else : ?>    
                        <tr>
                            <td class="text-center p-2 table_td">
                                <?= $value['deficits_annee'] ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?= format_number_($value['deficite_anterieur']) ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?php if($value['benefice_impute'] != 0) : ?>
                                    <?php foreach($value['benefice_impute'] as $keys_impute => $impute) : ?>
                                        <?= $keys_impute.' : '.format_number_($impute) ?><br>
                                    <?php endforeach ?>
                                <?php else : ?>
                                    <?= format_number_($value['benefice_impute']) ?>
                                <?php endif ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?= format_number_($value['deficit_repor']) ?>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
                <tr id="charge_row-">
                    <td class="text-center p-2 fw-bold table_td">TOTAL</td>
                    <td class="text-end p-2 fw-bold table_td"> <?= format_number_($total_deficit) ?></td>
                    <td class="text-end p-2 fw-bold table_td"><?= format_number_($total_benefice_impute) ?></td>
                    <td class="text-end p-2 fw-bold table_td"><?= format_number_($total_repor) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>