<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">
        <?= ($action == "add") ? 'Ajouter la déclaration pour le TVA' . ' ' . $doc_tva_annee : 'Modifier la déclaration pour le TVA' . ' ' . $doc_tva_annee ?>
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Fiscal/AddDocTva', array('method' => "post", 'class' => 'px-2', 'id' => 'AddDocTva')); ?>
<input type="hidden" name="dossier_id" id="dossier_id" value="<?= $dossier_id; ?>">
<input type="hidden" name="doc_tva_id" id="doc_tva_id" value="<?= !empty($tva->doc_tva_id) ? $tva->doc_tva_id : "" ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="doc_tva_annee" id="doc_tva_annee" value="<?= $doc_tva_annee; ?>">
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Type de montant : </label>
                    <select class="form-select fs--1" id="doc_type_montant" name="doc_type_montant" style="left: 3px;">
                        <option value="TVA à payer" <?=!empty($tva->doc_type_montant) ? (($tva->doc_type_montant == "TVA à payer") ? "selected" : " ") : " " ?>>TVA à payer</option>
                        <option value="Crédit de TVA" <?=!empty($tva->doc_type_montant) ? (($tva->doc_type_montant == "Crédit de TVA") ? "selected" : " ") : " " ?>>Crédit de TVA</option>
                    </select>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Montant TVA : </label>
                    <div class="input-group">
                        <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="doc_tva_montant" name="doc_tva_montant" style="text-align: right; height : 36px;" value="<?= !empty($tva->doc_tva_montant) ? $tva->doc_tva_montant : "" ?>">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">€</span>
                        </div>
                    </div>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline <?= ($action == "add") ? '' : 'd-none' ?>" style="margin-left: 140px;">
                <?php if (!empty($tva->doc_tva_path)) : ?>
                    <?php $nom_fic = explode("/", $tva->doc_tva_path); ?>
                    <span class="badge rounded-pill badge-soft-secondary">
                        <?= $nom_fic[7] ?></span>
                    <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefiletva" data-id="<?= $tva->doc_tva_id ?>" style="cursor: pointer;">.</span><br>
                <?php endif ?>
            </div>
            <div class="form-group inline <?= ($action == "add") ? '' : 'd-none' ?>">
                <div>
                    <label data-width="200"> Fichier déclaration de TVA : </label>
                    <input type="file" class="form-control" id="doc_tva_path" name="doc_tva_path">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline <?= ($action != "add") ? '' : 'd-none' ?>">
                <div>
                    <label data-width="300"> Nom du fichier : </label>
                    <?php $nom_fic = '';
                    if ($tva->doc_tva_nom == NULL) {
                        $tva_path = explode("/", $tva->doc_tva_path);
                        $nom_fic = $tva_path[7];
                    } else {
                        $nom_fic = $tva->doc_tva_nom;
                    }
                    ?>
                    <input type="text" class="form-control" id="doc_tva_nom" name="doc_tva_nom" style=" height : 36px;" value="<?= $nom_fic ?>">
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>