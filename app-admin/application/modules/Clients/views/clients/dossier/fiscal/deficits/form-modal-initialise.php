 <!-- Modal Header -->
 <div class="modal-header">
     <h4 class="modal-title">Initialisation de l'exercice</h4>
     <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
 </div>

 <!-- Modal body -->
 <div class="modal-body">
     <div class="col">
         <div class="row">
             <div class="col-4">
                 <label for="">Année d'initialisation : </label>
             </div>
             <div class="col-8">
                 <select class="form-select" id="annee_initial">
                     <?php
                        $last_10 = intval(date("Y")) - 9;
                        $date_now = intval(date("Y"));
                        for ($i = $last_10; $i <= intval($date_now); $i++) : ?>
                         <option value="<?= $i ?>" <?= ($i == $last_10) ? "selected" : "" ?>> <?= $i ?></option>
                     <?php endfor ?>
                 </select>
             </div>
         </div>
     </div>
 </div>

 <!-- Modal footer -->
 <div class="modal-footer">
     <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
     <button type="button" class="btn btn-primary initialyse">Initialiser</button>
 </div>