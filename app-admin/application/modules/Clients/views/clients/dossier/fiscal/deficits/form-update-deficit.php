<div class="modal-header">
    <h4 class="modal-title">Modification des déficits</h4>
    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
</div>

<!-- Modal body -->
<div class="modal-body">
    <input type="hidden" value="<?= $dossier_id  ?>" id ="dossier_id_modal_modif">
    <div class="col">
        <table class="table table-hover">
            <thead class="text-white" style="background:#2569C3;">
                <tr>
                    <th class="p-2 text-center table_td">Année</th>
                    <th class="p-2 text-center table_td">Déficits antérieurs</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php $i = 1; ?>
                <?php foreach ($deficits as $key => $value) : ?>
                    <tr>
                        <td class="text-center p-2 table_td col-4">
                            <?= $value->deficits_annee ?>
                        </td>
                        <td class="text-end p-1 mb-0 table_td">
                            <input type="text" class="form-control mb-0 deficite chiffre" value="<?= $value->deficite_anterieur == 0 ? '' : $value->deficite_anterieur ?>" data-annee="<?= $value->deficits_annee ?>" placeholder="" style="text-align: right;">
                        </td>
                    </tr>
                    <?php $i ++;?> 
                <?php endforeach ?>
                <input type="hidden" value="<?= $i ?>" id="nb_parcours">
            </tbody>
        </table>
    </div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
    <button type="button" class="btn btn-primary enregistrer_deficite">Enregistrer</button>
</div>