<style>
    .contenair-content-fiscal {
        height: 100%;
        overflow-y: auto !important;
        padding: 0.5rem;
    }
</style>
<div class="contenair-content-fiscal">
    <ul class="nav nav-tabs" id="FiscalTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="deficits-tab" data-bs-toggle="tab" href="#tab-deficits" role="tab" aria-controls="tab-deficits" aria-selected="true">Déficits</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="amort-tab" data-bs-toggle="tab" href="#tab-amort" role="tab" aria-controls="tab-amort" aria-selected="false">Amortissements</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="2031-tab" data-bs-toggle="tab" href="#tab-2031" role="tab" aria-controls="tab-2031" aria-selected="false">2031</a>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" id="2033-tab" data-bs-toggle="tab" href="#tab-2033" role="tab" aria-controls="tab-2033"
                aria-selected="false">2033</a>
        </li> -->
        <li class="nav-item">
            <a class="nav-link" id="2042-tab" data-bs-toggle="tab" href="#tab-2042" role="tab" aria-controls="tab-2042" aria-selected="false">2042</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tva-tab" data-bs-toggle="tab" href="#tab-tva" role="tab" aria-controls="tab-tva" aria-selected="false">TVA</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="doc_confidentiel-tab" data-bs-toggle="tab" href="#tab-doc_confidentiel" role="tab" aria-controls="tab-doc_confidentiel" aria-selected="false">Documents confidentiels</a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-1" id="FiscalTabContent">
        <div class="tab-pane fade show active" id="tab-deficits" role="tabpanel" aria-labelledby="deficits-tab">
            <?php $this->load->view('fiscal/deficits/deficits'); ?>
        </div>
        <div class="tab-pane fade" id="tab-amort" role="tabpanel" aria-labelledby="amort-tab">
            <?php $this->load->view('fiscal/amortissement/amort'); ?>
        </div>
        <div class="tab-pane fade" id="tab-2031" role="tabpanel" aria-labelledby="2031-tab">
            <?php $this->load->view('fiscal/2031/page_2031'); ?>
        </div>
        <!-- <div class="tab-pane fade" id="tab-2033" role="tabpanel" aria-labelledby="2033-tab">
            <?php $this->load->view('fiscal/2033/page_2033'); ?>
        </div> -->
        <div class="tab-pane fade" id="tab-2042" role="tabpanel" aria-labelledby="2042-tab">
            <?php $this->load->view('fiscal/2042/page_2042'); ?>
        </div>
        <div class="tab-pane fade" id="tab-tva" role="tabpanel" aria-labelledby="tva-tab">
            <?php $this->load->view('fiscal/tva/tva'); ?>
        </div>
        <div class="tab-pane fade" id="tab-doc_confidentiel" role="tabpanel" aria-labelledby="doc_confidentiel-tab">
            <?php $this->load->view('fiscal/doc_confidentiel/doc_confidentiel'); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormFiscal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content " id="modal-main-content-fiscal">

        </div>
    </div>
</div>

<div class="modal fade" id="miniModalFormFiscal" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content " id="modal-main-content-fiscal-mini">

        </div>
    </div>
</div>

<script>
    only_visuel_gestion();
</script>