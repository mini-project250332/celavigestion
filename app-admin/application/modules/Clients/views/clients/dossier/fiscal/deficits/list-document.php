<div class="contenair-title">
    <input type="hidden" value="<?= $dossier_id; ?>" id="dossier_fiscal">
    <div class="row m-0" style="width: 50%">
        <div class="col-7">
            <h5 class="fs-1">Documents Fiscal</h5>
        </div>
        <div class="col-3">

        </div>
        <div class="col-2">
            <div class="d-flex justify-content-end">
                <button class="only_visuel_gestion btn btn-sm btn-primary m-1" data-id="<?= $dossier_id ?>"
                    id="ajoutDocFiscal">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left" style="width: 881px;">
        <div class="row m-0">
            <div class="col-4 bold">
                <label for="">Nom</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Dépôt</label>
            </div>
            <div class="col-2 bold text-center">
                <label for="">Création</label>
            </div>
            <div class="col-4 bold text-center">
                <label for="">Actions</label>
            </div>
        </div>
        <hr class="my-0">
        <div class="row pt-3 m-0">
            <div class="col-12"
                style=" height: calc(25vh - 50px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
                <div class=" col-12">
                    <?php foreach ($document as $documents) { ?>
                        <div class="row apercu_doc" id="docrowfiscal-<?= $documents->doc_fiscal_id ?>">
                            <div class="col-4 pt-2">
                                <div class="fs--1 bold"
                                    style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3"
                                    id="<?= $documents->doc_fiscal_id ?>"
                                    onclick="apercuDocumentFiscal(<?= $documents->doc_fiscal_id ?>)"> <?=
                                          $documents->doc_fiscal_nom ?></div>
                            </div>
                            <div class="col-2 text-center">
                                <label for=""><i class="fas fa-user"></i>
                                    <?= $documents->util_prenom ?>
                                </label>
                            </div>
                            <div class="col-2 text-center">
                                <label for=""><i class="fas fa-calendar"></i>
                                    <?= $documents->doc_fiscal_creation ?>
                                </label>
                            </div>
                            <div class="col-4 text-center">
                                <div class="btn-group fs--1" style="left: 17%;">
                                    <?php
                                    $message = "Document non traité";
                                    if (!empty($documents->doc_fiscal_traitement)) {
                                        if ($documents->doc_fiscal_traite == 1) {
                                            $message = "Document traité par " . $documents->prenom_util . " le " . $documents->doc_fiscal_traitement;
                                        } else {
                                            $message = "Document annulé par " . $documents->prenom_util . " le " . $documents->doc_fiscal_traitement;
                                        }
                                    }
                                    ?>
                                    <button
                                        class="only_visuel_gestion btn btn-sm <?=($documents->doc_fiscal_traite == 1) ? 'btn-success' : 'btn-secondary' ?> rounded-pill  btnTraiteFiscal"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $message ?>"
                                        data-id=" <?= $documents->doc_fiscal_id ?>">
                                        <i class="fas fa-check"></i>
                                    </button> &nbsp;
                                    <span
                                        class="only_visuel_gestion btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocfiscal"
                                        data-id="<?= $documents->doc_fiscal_id ?>" data-bs-toggle="tooltip"
                                        data-bs-placement="bottom" title="Renommer" data-dossier="<?= $dossier_id; ?>">
                                        <i class="far fa-edit"></i>
                                    </span>&nbsp;
                                    <span class="only_visuel_gestion btn btn-sm btn-outline-primary rounded-pill">
                                        <i class="fas fa-download"
                                            onclick="forceDownload('<?= base_url() . $documents->doc_fiscal_path ?>',' <?= $documents->doc_fiscal_nom ?>')"
                                            data-url="<?= base_url($documents->doc_fiscal_path) ?>">
                                        </i>
                                    </span>&nbsp;
                                    <span
                                        class="only_visuel_gestion btn btn-sm btn-outline-danger rounded-pill btnSuppDocFiscal"
                                        data-id="<?= $documents->doc_fiscal_id ?>" data-bs-toggle="tooltip"
                                        data-bs-placement="bottom" title="Supprimer">
                                        <i class="far fa-trash-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr class="my-0"><br>
        <!-- information Fiscal -->
        <?php if (empty($deficits_info)): ?>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="alert alert-secondary text-center" role="alert">
                        <div class="text-center">
                            Pas de données fiscale pour le moment pour ce lot.
                        </div>
                        <div class="only_visuel_gestion text-center">
                            Souhaitez-vous initialiser les informations fiscales ?
                        </div>
                        <div>
                            <button class="only_visuel_gestion btn btn-sm btn-primary mt-2" id="btn_initialise_fiscal"
                                data-id="<?= $dossier_id ?>">Initialiser</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="tableau">
                <!-- Tableau Deficits -->
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="">Déficits :</h5>
                            </div>
                            <div class="col-6 text-end">
                                <button class="btn btn-sm btn-primary ModifDeficits only_visuel_gestion">
                                    Modifier Déficits
                                </button>
                                <button class="btn btn-sm btn-primary alertDeficit only_visuel_gestion">
                                    <i class="fas fa-plus me-1"></i> Année Déficit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="container-table-deficits">

                </div>
                <br>
                <!-- Tableau Benefices -->
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="">Bénéfices :</h5>
                            </div>
                            <div class="col-6 text-end">
                                <button class="btn btn-sm btn-primary only_visuel_gestion" id="addBenefice">
                                    <i class="fas fa-plus me-1"></i> Année Bénéfice
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="container-table-benefices">

                </div>
            </div>
        <?php endif ?>
    </div>
    <div class="split m-2">

    </div>
    <div class="col-6 panel-right">
        <div id="apercuFiscaldoc">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>
            </div>
            <div id="apercuFiscal">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i
                            class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i
                            class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i
                            class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    only_visuel_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });

    $(document).ready(function () {
        $('[data-bs-toggle="tooltip"]').tooltip();
        <?php if (!empty($deficits_info)): ?>
            getTableDeficit(<?= $dossier_id; ?>);
            getTableBenefice(<?= $dossier_id; ?>);
        <?php endif ?>
    });
</script>