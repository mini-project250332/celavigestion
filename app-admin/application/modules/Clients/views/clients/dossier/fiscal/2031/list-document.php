<div class="contenair-title">
    <div class="row m-0" style="width: 43%">
        <div class="col-6">
            <h5 class="fs-1">Documents de la liasse fiscale  </h5>
        </div>
        <div class="col-4">
            <select class="form-select fs--1 m-1 no_effect" id="doc_2031_annee" name="doc_2031_annee" style="width: 86%;"
                data-id="<?= $dossier_id ?>">
                <?php for ($i = intval(date("Y")); $i >= 2022; $i--) { ?>
                    <option value="<?= $i ?>" <?=($i == date("Y")) ? "selected" : "" ?>>
                        <?= $i ?>
                    </option>
                <?php } ?>
                <option value="0">Liasses antérieures</option>
            </select>
        </div>
        <div class="col-2">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 only_visuel_gestion" data-id="<?= $dossier_id ?>" id="ajoutdoc2031">
                    <span class="fas fa-plus me-1"></span>
                    <span> Fichier </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row pt-3 m-0 panel-container" id="content-2031">
    
</div>

<script>
    getDoc2031Annee(<?= $dossier_id ?>);
</script>