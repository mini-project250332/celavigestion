<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">
        Modifier le document
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Fiscal/Updatedoc2031', array('method' => "post", 'class' => 'px-2', 'id' => 'Updatedoc2031')); ?>
<input type="hidden" name="dossier_id" id="dossier_id" value="<?= $dossier_id; ?>">
<input type="hidden" name="doc_2031_id" id="doc_2031_id" value="<?= $doc_2031_id; ?>">
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Renommer le document : </label>
                    <div class="input-group">
                        <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="doc_2031_nom" name="doc_2031_nom" value="<?= $document->doc_2031_nom?>">                       
                    </div>                  
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>