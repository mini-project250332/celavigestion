<div class="col-5 panel-left">
    <div class="row m-0">
        <div class="col p-0 bold text-center">
            <label for="">Nom</label>
        </div>
        <div class="col p-0 bold text-center">
            <label for="">Dépôt</label>
        </div>
        <div class="col p-0 bold text-center">
            <label for="">Création</label>
        </div>        
        <div class="col p-0 bold text-center">
            <label for="">Envoyé le</label>
        </div>
        <div class="col p-0 bold text-center">
            <label for="">Actions</label>
        </div>
    </div>
    <hr class="my-0">
    <div class="row pt-3 m-0" style=" height: calc(40vh - 200px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
        <div class="col-12">
            <?php foreach ($document as $documents) { ?>

                <div class="row apercuDoc2031" id="rowdoc-<?= $documents->doc_2031_id ?>">
                    <div class="col p-0 text-center">
                        <div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_2031_id ?>" onclick="apercuDocument2031(<?= $documents->doc_2031_id ?>)">
                            <?= $documents->doc_2031_nom ?>
                        </div>
                    </div>
                    <div class="col p-0 text-center">
                        <label for=""><i class="fas fa-user"></i> <?= $documents->util_prenom ?></label>
                    </div>
                    <div class="col p-0 text-center">
                        <label for=""><i class="fas fa-calendar"></i> <?= date("d/m/Y", strtotime(str_replace('/', '-',$documents->doc_2031_date_creation)))?> <br>
                            <?= date("H:i:s", strtotime($documents->doc_2031_date_creation))?>
                        </label>
                    </div>                    
                    <div class="col p-0 text-center">
                        <label for=""><?= !empty($documents->doc_2031_date_envoi) ? '<i class="fas fa-calendar"></i>' : "" ?> <?= !empty($documents->doc_2031_date_envoi) ? date("d/m/Y", strtotime(str_replace('/', '-',$documents->doc_2031_date_envoi))) : "-"  ?></label>
                    </div>
                    <div class="col p-0 text-center">
                        <div class="btn-group fs--1">
                            <span class="btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDoc2031 only_visuel_gestion" data-id="<?= $documents->doc_2031_id ?>" data-dossier ="<?=$dossier_id?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
                                <i class="far fa-edit"></i>
                            </span>&nbsp;
                            <span class="btn btn-sm btn-outline-primary rounded-pill envoi2031 " data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer" data-id="<?= $documents->doc_2031_id ?>" data-dossier ="<?=$dossier_id?>">
                                <i class="fas fa-envelope"></i>
                            </span>&nbsp;
                            <span class="btn btn-sm btn-outline-danger rounded-pill btnSuppDoc2031 only_visuel_gestion" data-id="<?= $documents->doc_2031_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
                                <i class="far fa-trash-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="splitter m-2">

</div>
<div class="col-5 panel-right">
    <div id="aperculistdoc2031">
        <div id="nonApercu" style="display:none;">
            <div class="text-center text-danger pt-10">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de fichier
            </div>

        </div>
        <div id="apercu2031">
            <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade" id="modalAjoutDoc2031" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="contenaireUpload2031">

            </div>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion();

    $(".panel-left").resizable({
        handleSelector: ".splitter",
        resizeHeight: false,
    });
</script>