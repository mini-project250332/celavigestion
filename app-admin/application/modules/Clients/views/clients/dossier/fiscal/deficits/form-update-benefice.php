<div class="modal-header">
    <h4 class="modal-title">Ajout du Bénéfice</h4>
    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
</div>

<!-- Modal body -->
<div class="modal-body">

    <input type="hidden" value="<?= $dossier_id  ?>" id="cliLot_id_modal_ajout">
    <div class="col">
        <div class="row p-2">
            <div class="col-5">
                Année du Bénéfice :
            </div>
            <div class="col-7">
                <select class="form-select" id="benefices_annee">
                    <?php foreach($date_liste as $value) : ?>
                        <option value="<?= $value ?>"><?= $value ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </div>

    <div class="col">
        <div class="row p-2">
            <div class="col-5">
                Bénéfices :
            </div>
            <div class="col-7">
                <input type="text" class="form-control  mb-0 chiffre" id="benefices" placeholder="0" style="text-align: right;">
            </div>
        </div>
    </div>

</div>

<!-- Modal footer -->
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
    <button type="button" class="btn btn-primary enregistrer_benefice">Enregistrer</button>
</div>