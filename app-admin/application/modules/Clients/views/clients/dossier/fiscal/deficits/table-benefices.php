<style>
    .table_td {
        border-right: 1px solid #ffff;
        border-bottom: 1px solid #ffff;
    }
</style>

<div class="row">
    <div class="col">
        <table class="table table-hover fs--1">
            <thead class="text-white" style="background:#2569C3;">
                <tr>
                    <th class="p-2 text-center table_td">Année</th>
                    <th class="p-2 text-center table_td col-3">Bénéfices</th>
                    <th class="p-2 text-center table_td col-3">Déficits utilisés</th>
                    <th class="p-2 text-center table_td col-3">Bénéfices avant amortissement</th>
                    <th class="p-2 text-center table_td only_visuel_gestion">Actions</th>
                </tr>
            </thead>
            <tbody class="bg-200">
                <?php if (!empty($benefices)): ?>
                    <?php $nbr_array = count($benefices);
                    $foreach_index = 1; ?>
                    <?php foreach ($benefices as $key => $value): ?>
                        <tr id="row_benefice-<?= $value['id_benefices'] ?>">
                            <td class="text-center p-2 table_td">
                                <?= $value['benefices_annee'] ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?= format_number_($value['benefices']) ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?php if ($value['deficit_utilise'] != 0): ?>
                                    <?php foreach ($value['deficit_utilise'] as $keys_impute => $impute): ?>
                                        <?= $keys_impute . ' : ' . format_number_($impute) ?><br>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <?= format_number_($value['deficit_utilise']) ?>
                                <?php endif ?>
                            </td>
                            <td class="text-end p-2 table_td">
                                <?= format_number_($value['benefice_amor']) ?>
                            </td>
                            <td class="only_visuel_gestion text-end p-2 table_td text-center">
                                <button
                                    class="btn btn-sm btn-outline-danger <?= $nbr_array == $foreach_index ? 'SupprimerBenefice' : 'alertBenefice' ?>"
                                    title="Supprimer" data-id="<?= $value['id_benefices'] ?>"> <i
                                        class="far fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        <?php $foreach_index++; ?>
                    <?php endforeach ?>
                </tbody>
            <?php else: ?>
                <tr id="charge_row-">
                    <td class="text-center p-2 table_td fw-bold" colspan="5">
                        Aucune bénéfice ajouté
                    </td>
                </tr>
            <?php endif ?>
        </table>
    </div>
</div>