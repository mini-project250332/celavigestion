<style>
    .table_td
    {
        border-right: 1px solid #ffff;
        border-bottom: 1px solid #ffff;
    }
</style>


<div class="contenair-title">
    <input type="hidden" value="<?= $dossier_id; ?>" id="cliLot_id_charge">
</div>

<table class="table table-hover fs--1">
    <thead>
        <tr>
            <th class="p-2 text-center table_td" ></th>
            <th class="p-2 text-center table_td" ></th>
            <th class="p-2 text-center text-white bg-primary table_td">CGA ou Viseur</th>
            <th class="p-2 text-center table_td"></th>
            <th class="p-2 text-center text-white bg-primary table_td">Sans</th>
            <th class="p-2 text-center table_td"></th>
            <th class="p-2 text-center text-white bg-primary table_td">CGA ou Viseur</th>
            <th class="p-2 text-center table_td"></th>
            <th class="p-2 text-center text-white bg-primary table_td">Sans</th>
            <th class="p-2 text-center table_td"></th>
            <th class="p-2 text-center text-white bg-primary table_td">CGA ou Viseur</th>
            <th class="p-2 text-center table_td"></th>
            <th class="p-2 text-center text-white bg-primary table_td">Sans</th>
        </tr>
    </thead>
    <tbody class="bg-200">
       <tr>
            <td class="p-2 table_td">Régime du bénéfice réel</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td">Revenus imposable</td>
            <td class="p-2 fw-bold table_td">5NA</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5NK</td>
            <td class="p-2 fw-bold text-center table_td" style="background-color:#ecc17c ;">0</td>
            <td class="p-2 fw-bold table_td">5OA</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5OK</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5PA</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5PK</td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td">Location des gîtes ruraux dejà soumis </td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td">aux prélèvements sociaux</td>
            <td class="p-2 fw-bold table_td">5NM</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5KM</td>
            <td class="p-2 fw-bold text-center table_td"></td>
            <td class="p-2 fw-bold table_td">5OM</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5LM</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5PM</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5MM</td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td">Déficits</td>
            <td class="p-2 fw-bold table_td">5NY</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5NZ</td>
            <td class="p-2 fw-bold text-center table_td"></td>
            <td class="p-2 fw-bold table_td">5OY</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5LZ</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5PY</td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5PZ</td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td">5GA</td>
            <td class="p-2 table_td">2013</td>
            <td class="p-2 fw-bold table_td">5GB</td>
            <td class="p-2 table_td">2014</td>
            <td class="p-2 fw-bold table_td">5GC</td>
            <td class="p-2 table_td">2015</td>
            <td class="p-2 fw-bold table_td">5GD</td>
            <td class="p-2 table_td">2016</td>
            <td class="p-2 fw-bold table_td">5GE</td>
            <td class="p-2 table_td">2017</td>
            <td class="p-2 fw-bold table_td">5GF</td>
            <td class="p-2 table_td">2018</td>
       </tr>
       <tr>
            <td class="p-2 table_td">Deficits des années antérieures </td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">1500</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">1200</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
       </tr>
       <tr>
            <td class="p-2 table_td">non encore déduits </td>
            <td class="p-2 fw-bold table_td">5GG</td>
            <td class="p-2 table_td">2019</td>
            <td class="p-2 fw-bold table_td">5GH</td>
            <td class="p-2 table_td">2020</td>
            <td class="p-2 fw-bold table_td">5GI</td>
            <td class="p-2 table_td">2021</td>
            <td class="p-2 fw-bold table_td">5GJ</td>
            <td class="p-2 table_td">2022</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td"></td>
       </tr>
       <tr>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">1000</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td">0</td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td"></td>
            <td class="p-2 fw-bold table_td"></td>
            <td class="p-2 table_td"></td>
       </tr>
    </tbody>
</table>