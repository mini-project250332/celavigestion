<style>
	.choices {
		width: 100%;
	}

	.input_charge {
		height: 30px;
		padding: 5px;
	}

	.select2-container--bootstrap-5.select2-container--open .select2-selection {
		left: 3px !important;
	}

	.select2-container--open .select2-dropdown--below {
		width: 500px !important;
	}
</style>
<div class="contenair-title">
	<div class="px-2"></div>

	<div class="px-2">
		<button type="submit" data-client_id="<?= $client_id; ?>" data-id="<?= $dossier->dossier_id; ?>" id="retour_fiche_dossier" class="btn btn-sm btn-primary">
			<i class="fas fa-check-circle"></i>
			<span> Terminer</span>
		</button>
	</div>
</div>
<?php if (isset($dossier->dossier_id)) : ?>
	<input type="hidden" name="dossier_id" id="dossier_id" value="<?= $dossier->dossier_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<div class="contenair-content">
	<div class="col-12">
		<div class="row m-0 ">
			<div class="col-3 p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Information générales</h5>
						<hr class="mt-0">
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="150"> Propriétaire(s) :</label>
											<select class="form-select fs--1 js-choice" id="client_id" name="client_id" multiple="multiple" size="1" data-require="true">
												<option value=""></option>
											</select>
										</div>
									</div>
								</tr>
								<br>
								<tr class="border-bottom">

									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="150"> Forme juridique :</label>
											<select class="form-select fs--1" id="forme_juridique" name="forme_juridique">
												<?php
												foreach ($formejuridique as $formejuridiques) : ?>
													<option value="<?= $formejuridiques->c_forme_juridique_id; ?>" <?php echo isset($dossier->c_forme_juridique_id) ? (($dossier->c_forme_juridique_id !== NULL) ? (($dossier->c_forme_juridique_id == $formejuridiques->c_forme_juridique_id) ? "selected" : " ") : "") : "" ?>>
														<?= $formejuridiques->c_forme_juridique_libelle; ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								</tr>
								<br>
								<div class="indivision-section">
									<tr class="border-bottom">
										<div class="form-group inline indivision-section" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Indivision : </label>
												<select class="form-select" id="indivision" name="indivision">
													<option value="Non" <?= $dossier->indivision == "Non" ? "selected" : "" ?>>
														Non</option>
													<option value="Oui" <?= $dossier->indivision == "Oui" ? "selected" : "" ?>>
														Oui</option>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline client_rof indivision-section" style="display : none;">
											<div class="col-sm-12">
												<label data-width="200"> Pourcentage : </label>
											</div>
											<?php foreach ($rofData as $rof) : if (!empty($rof)) : ?>
													<div class="col-sm-12 p-0 m-0">
														<label data-width="200"><?= $rof->client_nom . ' ' . $rof->client_prenom  ?> :</label>
														<input type="text" class="form-control text-end inputValue" id="client_rof__<?= $rof->client_id ?>" name="client_rof__<?= $rof->client_id ?>" value="<?= $rof->client_rof ?>" min="0" max="100" onkeyup=enforceMinMax(this)>
														<span class="input-group-text chiffre " style="height: 32px;text-align:right">%</span>
													</div>
											<?php endif;
											endforeach; ?>
										</div>
									</tr>
								</div>
								<tr class="border-bottom">
									<div class="form-group inline switch">
										<div>
											<label data-width="300"> Statuts dossier:</label>
											<div class="form-check form-switch fs--1">
												<span> Inactif </span>
												<input class="form-check-input" type="checkbox" id="dossier_etat" name="dossier_etat" <?php echo isset($dossier->dossier_etat) ? ($dossier->dossier_etat == "1" ? "checked" : "") : ""; ?>>
												<span> Actif </span>
											</div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0 pt-2"> Numero dossier : </th>
									<th class="pe-0 pt-2 text-end">
										<?= str_pad($dossier->dossier_id, 4, '0', STR_PAD_LEFT); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Status dossier : </th>
									<th class="pe-0 text-end">
										<?= ($dossier->dossier_etat == 1) ? "Actif" : "Non actif"; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Date modification : </th>
									<th class="pe-0 text-end">
										<?= date("d-m-Y", strtotime(str_replace('/', '-', $dossier->dossier_d_modification))); ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0"> Suivi par : </th>
									<th class="pe-0 text-end">
										<select class="form-select fs--1" id="util_id" name="util_id">
											<?php
											foreach ($utilisateurs as $utilisateur) : ?>
												<option value="<?= $utilisateur->util_id; ?>" <?php echo isset($dossier->util_id) ? (($dossier->util_id !== NULL) ? (($dossier->util_id == $utilisateur->util_id) ? "selected" : " ") : "") : "" ?>>
													<?= $utilisateur->util_nom . ' ' . $utilisateur->util_prenom; ?>
												</option>
											<?php endforeach; ?>
										</select>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-3 p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Adresses de facturation loyers et honoraires</h5>
						<hr class="mt-0">
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<div class="form-group inline p-0">
										<div class="col-sm-12">
											<label data-width="150"> Type d'adresse : </label>
											<select class="form-select fs--1" id="dossier_type_adresse" name="dossier_type_adresse">
												<option value="Lot principal" <?php echo ($dossier->dossier_type_adresse == "Lot principal") ? "selected" : " " ?>>
													Lot principal
												</option>
												<option value="Adresse spécifique" <?php echo ($dossier->dossier_type_adresse == "Adresse spécifique") ? "selected" : " " ?>>
													Adresse spécifique
												</option>
											</select>
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<div class="specifique">
									<tr class="border-bottom">
										<div class="form-group inline p-0 specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 1 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse1" name="dossier_adresse1" value="<?= $dossier->dossier_adresse1 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 2 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse2" name="dossier_adresse2" value="<?= $dossier->dossier_adresse2 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom p-0">
										<div class="form-group inline specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 3 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse3" name="dossier_adresse3" value="<?= $dossier->dossier_adresse3 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Code postal : </label>
												<input type="text" class="form-control" id="dossier_cp" name="dossier_cp" value="<?= $dossier->dossier_cp ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Ville : </label>
												<input type="text" class="form-control maj" id="dossier_ville" name="dossier_ville" value="<?= $dossier->dossier_ville ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 specifique" style="display : none;">
											<div class="col-sm-12">
												<label data-width="150"> Pays : </label>
												<input type="text" class="form-control maj" id="dossier_pays" name="dossier_pays" value="<?= $dossier->dossier_pays ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</div>
								<div class="">
									<tr class="border-bottom">
										<div class="form-group inline p-0 lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 1 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse1" name="" value="<?= !empty($lot) ? $lot->progm_adresse1 : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 2 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse2" name="" value="<?= !empty($lot) ? $lot->progm_adresse2 : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom p-0">
										<div class="form-group inline lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 3 : </label>
												<input type="text" class="form-control maj" id="dossier_adresse3" name="" value="<?= !empty($lot) ? $lot->progm_adresse3 : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Code postal : </label>
												<input type="text" class="form-control maj" id="dossier_cp" name="" value="<?= !empty($lot) ? $lot->progm_cp : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Ville : </label>
												<input type="text" class="form-control maj" id="dossier_ville" name="" value="<?= !empty($lot) ? $lot->progm_ville : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0 lot_principal">
											<div class="col-sm-12">
												<label data-width="150"> Pays : </label>
												<input type="text" class="form-control maj" id="dossier_pays" name="" value="<?= !empty($lot) ? $lot->progm_pays : "" ?>" disabled>
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</div>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-3 p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body">
						<h5 class="fs-0">Fiscalité et comptabilité</h5>
						<hr class="mt-0">
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="250"> SIE : </label>
											<select class="select2" id="sie_id" name="sie_id" style="width: 500px;">
												<?php foreach ($sie as $value_sie) : ?>
													<option value=" <?= $value_sie->sie_id; ?>" <?php echo isset($dossier->sie_id) ? (($dossier->sie_id == $value_sie->sie_id) ?
																									"selected" : " ") : "" ?>>
														<?= $value_sie->sie_libelle; ?>
													</option>
												<?php endforeach; ?>
											</select>
											<div class="help"></div>
										</div>
									</div>
								</tr>

								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200"> RCS : </label>
											<input type="text" class="form-control" id="dossier_rcs" name="dossier_rcs" value="<?= $dossier->dossier_rcs ?>">
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200"> SIRET : </label>
											<input type="text" class="form-control chiffre" id="dossier_siret" name="dossier_siret" value="<?= $dossier->dossier_siret ?>" maxlength="14">
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200"> APE : </label>
											<input type="text" class="form-control" id="dossier_ape" name="dossier_ape" value="<?php echo isset($dossier->dossier_ape) ? $dossier->dossier_ape : "6820A" ?>">
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200"> TVA <br>
												intracommunautaire :
											</label>
											<input type="text" class="form-control" id="dossier_tva_intracommunautaire" name="dossier_tva_intracommunautaire" value="<?= $dossier->dossier_tva_intracommunautaire ?>">
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200"> Régime : </label>
											<select class="form-select fs--1" id="regime" name="regime">
												<option value="BIC REEL SIMPLIFIE" <?php echo ($dossier->regime == "BIC REEL SIMPLIFIE") ? "selected" : " " ?>>
													BIC REEL SIMPLIFIE
												</option>
												<option value="FONCIER REEL" <?php echo ($dossier->regime == "FONCIER REEL") ? "selected" : " " ?>>
													FONCIER REEL
												</option>
											</select>
											<div class="help"></div>
										</div>
									</div>
								</tr>
								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="150"> Clôture : </label>
											<select class="form-select fs--1" id="cloture_jour" name="cloture_jour" style="width: 25%;">
												<option value="01" <?php echo ($dossier->cloture_jour == "01") ? "selected" : " " ?>>01 </option>
												<option value="02" <?php echo ($dossier->cloture_jour == "02") ? "selected" : " " ?>>02 </option>
												<option value="03" <?php echo ($dossier->cloture_jour == "03") ? "selected" : " " ?>>03 </option>
												<option value="04" <?php echo ($dossier->cloture_jour == "04") ? "selected" : " " ?>>04 </option>
												<option value="05" <?php echo ($dossier->cloture_jour == "05") ? "selected" : " " ?>>05 </option>
												<option value="06" <?php echo ($dossier->cloture_jour == "06") ? "selected" : " " ?>>06 </option>
												<option value="07" <?php echo ($dossier->cloture_jour == "07") ? "selected" : " " ?>>07 </option>
												<option value="08" <?php echo ($dossier->cloture_jour == "08") ? "selected" : " " ?>>08 </option>
												<option value="09" <?php echo ($dossier->cloture_jour == "09") ? "selected" : " " ?>>09 </option>
												<option value="10" <?php echo ($dossier->cloture_jour == "10") ? "selected" : " " ?>>10 </option>
												<option value="11" <?php echo ($dossier->cloture_jour == "11") ? "selected" : " " ?>>11 </option>
												<option value="12" <?php echo ($dossier->cloture_jour == "12") ? "selected" : " " ?>>12 </option>
												<option value="13" <?php echo ($dossier->cloture_jour == "13") ? "selected" : " " ?>>13 </option>
												<option value="14" <?php echo ($dossier->cloture_jour == "14") ? "selected" : " " ?>>14 </option>
												<option value="15" <?php echo ($dossier->cloture_jour == "15") ? "selected" : " " ?>>15 </option>
												<option value="16" <?php echo ($dossier->cloture_jour == "16") ? "selected" : " " ?>>16 </option>
												<option value="17" <?php echo ($dossier->cloture_jour == "17") ? "selected" : " " ?>>17 </option>
												<option value="18" <?php echo ($dossier->cloture_jour == "18") ? "selected" : " " ?>>18 </option>
												<option value="19" <?php echo ($dossier->cloture_jour == "19") ? "selected" : " " ?>>19 </option>
												<option value="20" <?php echo ($dossier->cloture_jour == "20") ? "selected" : " " ?>>20 </option>
												<option value="21" <?php echo ($dossier->cloture_jour == "21") ? "selected" : " " ?>>21 </option>
												<option value="22" <?php echo ($dossier->cloture_jour == "22") ? "selected" : " " ?>>22 </option>
												<option value="23" <?php echo ($dossier->cloture_jour == "23") ? "selected" : " " ?>>23 </option>
												<option value="24" <?php echo ($dossier->cloture_jour == "24") ? "selected" : " " ?>>24 </option>
												<option value="25" <?php echo ($dossier->cloture_jour == "25") ? "selected" : " " ?>>25 </option>
												<option value="26" <?php echo ($dossier->cloture_jour == "26") ? "selected" : " " ?>>26 </option>
												<option value="27" <?php echo ($dossier->cloture_jour == "27") ? "selected" : " " ?>>27 </option>
												<option value="28" <?php echo ($dossier->cloture_jour == "28") ? "selected" : " " ?>>28 </option>
												<option value="29" <?php echo ($dossier->cloture_jour == "29") ? "selected" : " " ?>>29 </option>
												<option value="30" <?php echo ($dossier->cloture_jour == "30") ? "selected" : " " ?>>30 </option>
												<option value="31" <?php echo ($dossier->cloture_jour == "31") ? "selected" : " " ?>>31 </option>
											</select>
											<select class="form-select fs--1" id="cloture_mois" name="cloture_mois" style="width: 25%;">
												<option value="01" <?php echo ($dossier->cloture_mois == "01") ? "selected" : " " ?>>01 </option>
												<option value="02" <?php echo ($dossier->cloture_mois == "02") ? "selected" : " " ?>>02 </option>
												<option value="03" <?php echo ($dossier->cloture_mois == "03") ? "selected" : " " ?>>03 </option>
												<option value="04" <?php echo ($dossier->cloture_mois == "04") ? "selected" : " " ?>>04 </option>
												<option value="05" <?php echo ($dossier->cloture_mois == "05") ? "selected" : " " ?>>05 </option>
												<option value="06" <?php echo ($dossier->cloture_mois == "06") ? "selected" : " " ?>>06 </option>
												<option value="07" <?php echo ($dossier->cloture_mois == "07") ? "selected" : " " ?>>07 </option>
												<option value="08" <?php echo ($dossier->cloture_mois == "08") ? "selected" : " " ?>>08 </option>
												<option value="09" <?php echo ($dossier->cloture_mois == "09") ? "selected" : " " ?>>09 </option>
												<option value="10" <?php echo ($dossier->cloture_mois == "10") ? "selected" : " " ?>>10 </option>
												<option value="11" <?php echo ($dossier->cloture_mois == "11") ? "selected" : " " ?>>11 </option>
												<option value="12" <?php echo ($dossier->cloture_mois == "12") ? "selected" : " " ?>>12 </option>
											</select>
											<span class="p-1"> au </span>
											<select class="form-select fs--1" id="cloture_jour_fin" name="cloture_jour_fin" style="width: 25%;">
												<option value="02" <?php echo ($dossier->cloture_jour_fin == "02") ? "selected" : " " ?>>02 </option>
												<option value="03" <?php echo ($dossier->cloture_jour_fin == "03") ? "selected" : " " ?>>03 </option>
												<option value="04" <?php echo ($dossier->cloture_jour_fin == "04") ? "selected" : " " ?>>04 </option>
												<option value="05" <?php echo ($dossier->cloture_jour_fin == "05") ? "selected" : " " ?>>05 </option>
												<option value="06" <?php echo ($dossier->cloture_jour_fin == "06") ? "selected" : " " ?>>06 </option>
												<option value="07" <?php echo ($dossier->cloture_jour_fin == "07") ? "selected" : " " ?>>07 </option>
												<option value="08" <?php echo ($dossier->cloture_jour_fin == "08") ? "selected" : " " ?>>08 </option>
												<option value="09" <?php echo ($dossier->cloture_jour_fin == "09") ? "selected" : " " ?>>09 </option>
												<option value="10" <?php echo ($dossier->cloture_jour_fin == "10") ? "selected" : " " ?>>10 </option>
												<option value="11" <?php echo ($dossier->cloture_jour_fin == "11") ? "selected" : " " ?>>11 </option>
												<option value="12" <?php echo ($dossier->cloture_jour_fin == "12") ? "selected" : " " ?>>12 </option>
												<option value="13" <?php echo ($dossier->cloture_jour_fin == "13") ? "selected" : " " ?>>13 </option>
												<option value="14" <?php echo ($dossier->cloture_jour_fin == "14") ? "selected" : " " ?>>14 </option>
												<option value="15" <?php echo ($dossier->cloture_jour_fin == "15") ? "selected" : " " ?>>15 </option>
												<option value="16" <?php echo ($dossier->cloture_jour_fin == "16") ? "selected" : " " ?>>16 </option>
												<option value="17" <?php echo ($dossier->cloture_jour_fin == "17") ? "selected" : " " ?>>17 </option>
												<option value="18" <?php echo ($dossier->cloture_jour_fin == "18") ? "selected" : " " ?>>18 </option>
												<option value="19" <?php echo ($dossier->cloture_jour_fin == "19") ? "selected" : " " ?>>19 </option>
												<option value="20" <?php echo ($dossier->cloture_jour_fin == "20") ? "selected" : " " ?>>20 </option>
												<option value="21" <?php echo ($dossier->cloture_jour_fin == "21") ? "selected" : " " ?>>21 </option>
												<option value="22" <?php echo ($dossier->cloture_jour_fin == "22") ? "selected" : " " ?>>22 </option>
												<option value="23" <?php echo ($dossier->cloture_jour_fin == "23") ? "selected" : " " ?>>23 </option>
												<option value="24" <?php echo ($dossier->cloture_jour_fin == "24") ? "selected" : " " ?>>24 </option>
												<option value="25" <?php echo ($dossier->cloture_jour_fin == "25") ? "selected" : " " ?>>25 </option>
												<option value="26" <?php echo ($dossier->cloture_jour_fin == "26") ? "selected" : " " ?>>26 </option>
												<option value="27" <?php echo ($dossier->cloture_jour_fin == "27") ? "selected" : " " ?>>27 </option>
												<option value="28" <?php echo ($dossier->cloture_jour_fin == "28") ? "selected" : " " ?>>28 </option>
												<option value="29" <?php echo ($dossier->cloture_jour_fin == "29") ? "selected" : " " ?>>29 </option>
												<option value="30" <?php echo ($dossier->cloture_jour_fin == "30") ? "selected" : " " ?>>30 </option>
												<option value="31" <?php echo ($dossier->cloture_jour_fin == "31") ? "selected" : " " ?>>31 </option>
											</select>
											<select class="form-select fs--1" id="cloture_mois_fin" name="cloture_mois_fin" style="width: 26%;">
												<option value="01" <?php echo ($dossier->cloture_mois_fin == "01") ? "selected" : " " ?>>01 </option>
												<option value="02" <?php echo ($dossier->cloture_mois_fin == "02") ? "selected" : " " ?>>02 </option>
												<option value="03" <?php echo ($dossier->cloture_mois_fin == "03") ? "selected" : " " ?>>03 </option>
												<option value="04" <?php echo ($dossier->cloture_mois_fin == "04") ? "selected" : " " ?>>04 </option>
												<option value="05" <?php echo ($dossier->cloture_mois_fin == "05") ? "selected" : " " ?>>05 </option>
												<option value="06" <?php echo ($dossier->cloture_mois_fin == "06") ? "selected" : " " ?>>06 </option>
												<option value="07" <?php echo ($dossier->cloture_mois_fin == "07") ? "selected" : " " ?>>07 </option>
												<option value="08" <?php echo ($dossier->cloture_mois_fin == "08") ? "selected" : " " ?>>08 </option>
												<option value="09" <?php echo ($dossier->cloture_mois_fin == "09") ? "selected" : " " ?>>09 </option>
												<option value="10" <?php echo ($dossier->cloture_mois_fin == "10") ? "selected" : " " ?>>10 </option>
												<option value="11" <?php echo ($dossier->cloture_mois_fin == "11") ? "selected" : " " ?>>11 </option>
												<option value="12" <?php echo ($dossier->cloture_mois_fin == "12") ? "selected" : " " ?>>12 </option>
											</select>
											<div class="help"></div>
										</div>
									</div>
								</tr>

								<tr class="border-bottom">
									<div class="form-group inline">
										<div class="col-sm-12">
											<label data-width="200">CGA : </label>
											<select class="form-select fs--1" id="mention_cga" name="mention_cga">
												<option value="0" <?php echo ($dossier->mention_cga  == 0) ? "selected" : " " ?>>
													Non
												</option>
												<option value="1" <?php echo ($dossier->mention_cga  == 1) ? "selected" : " " ?>>
													Oui
												</option>
											</select>
											<div class="help"></div>
										</div>
									</div>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-3 p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">CGP</h5>
						<hr class="mt-0">
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline">
											<div class="col-sm-12">
												<label data-width="150"> Nom : </label>
												<select class="form-select fs--1" id="cgp_id" name="cgp_id" style="margin-left: 4%">
													<option value=""></option>
													<?php foreach ($cgp as $value) : ?>
														<option value="<?= $value->cgp_id ?>" <?php echo isset($dossier->cgp_id) ? (($dossier->cgp_id !== NULL) ? (($dossier->cgp_id == $value->cgp_id) ? "selected" : " ") : "") : "" ?>>
															<?= $value->cgp_nom . ' ' . $value->cgp_prenom ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
												<div class="">
													<button id="addCgp" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
												</div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Email :</label>
												<input type="text" class="form-control " id="cgp_email" name="cgp_email" value="<?php echo isset($dossier->cgp_email) ? $dossier->cgp_email : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Téléphone :</label>
												<input type="text" class="form-control " id="cgp_tel" name="cgp_tel" value="<?php echo isset($dossier->cgp_tel) ? $dossier->cgp_tel : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 1 :</label>
												<input type="text" class="form-control maj" id="cgp_adresse1" name="cgp_adresse1" value="<?php echo isset($dossier->cgp_adresse1) ? $dossier->cgp_adresse1 : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 2 :</label>
												<input type="text" class="form-control maj" id="cgp_adresse2" name="cgp_adresse2" value="<?php echo isset($dossier->cgp_adresse2) ? $dossier->cgp_adresse2 : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 3 :</label>
												<input type="text" class="form-control maj" id="cgp_adresse3" name="cgp_adresse3" value="<?php echo isset($dossier->cgp_adresse3) ? $dossier->cgp_adresse3 : "" ?>">
												<div class="help"> </div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Code postal :</label>
												<input type="text" class="form-control" id="cgp_cp" name="cgp_cp" value="<?php echo isset($dossier->cgp_cp) ? $dossier->cgp_cp : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Ville :</label>
												<input type="text" class="form-control maj" id="cgp_ville" name="cgp_ville" value="<?php echo isset($dossier->cgp_ville) ? $dossier->cgp_ville : "" ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Pays :</label>
												<input type="text" class="form-control maj" id="cgp_pays" name="cgp_pays" value="<?php echo isset($dossier->cgp_pays) ? $dossier->cgp_pays : "" ?>">
												<div class="help fs-11 height-15 m-0"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$client_default = array();
foreach ($client as $clients) {
	if (isset($clients->client_entreprise) && $clients->client_entreprise != "") {
		$client_default[] = array(
			'id' => $clients->client_id,
			'text' => $clients->client_nom . " " . $clients->client_prenom . " (" . $clients->client_entreprise . ")",
		);
	} else {
		$client_default[] = array(
			'id' => $clients->client_id,
			'text' => $clients->client_nom . " " . $clients->client_prenom,
		);
	}
}

if ($action == "edit") {
	$edit_exist = explode(',', $dossier->client_id);
	$client_array = array();
	$items = array();
	foreach ($client_default as $key) {
		if (in_array($key['id'], $edit_exist)) {
			$client_array[] = array(
				'id' => $key['id'],
				'text' => $key['text'],
			);
		} else {
			$items[] = array(
				'id' => $key['id'],
				'text' => $key['text'],
			);
		}
	}
	$client_default = $items;
}
?>

<script>
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y"
	});

	// $(".select2").val(66);

	$(document).ready(function() {
		$('.select2').select2({
			theme: 'bootstrap-5',
			//placeholder: "Select a state",
		});
	});

	var client_default = <?= json_encode($client_default) ?>;
	var element = document.querySelector('.js-choice');
	var choices = new Choices(element, {
		removeItems: true,
		removeItemButton: true,
		choices: client_default.map((item) => ({
			value: item.id,
			label: item.text
		}))
	});

	<?php if ($action == "edit") : ?>
		var liste_client = <?= json_encode($client_array); ?>;
		//choices.removeActiveItems();
		choices.setValue(liste_client.map((item) => ({
			value: item.id,
			label: item.text
		})));
	<?php endif; ?>
	$(document).ready(function() {
		changetypedossier();
		changetypeformejuridique();
	});
</script>