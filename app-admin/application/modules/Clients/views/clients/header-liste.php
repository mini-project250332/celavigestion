<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Liste des propriétaires</h5>
	</div>
	<div class="px-2">

	</div>
</div>

<div class="contenair-list">
	<div class="row">
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Nom, Email, Contact</label>
				<input class="form-control mb-0" type="text" id="text_filtre" name="text_filtre">
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Gestionnaire</label>
				<select class="form-select fs--1" id="gestionnaire_id" name="gestionnaire_id">
					<option value="">Tous</option>
					<?php foreach ($gestionnaire as $gestionnaires) : ?>
						<option value="<?= $gestionnaires->gestionnaire_id; ?>">
							<?= $gestionnaires->gestionnaire_nom; ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Programme</label>
				<input class="form-control mb-0" type="text" name="filtre_pgm" id="filtre_pgm" placeholder="Nom,ville">
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Numéro dossier</label>
				<input class="form-control mb-0" type="text" name="filtre_dossier" id="filtre_dossier">
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Numéro SIRET</label>
				<input class="form-control mb-0" type="text" name="filtre_siret" id="filtre_siret">
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Numéro mandat</label>
				<input class="form-control mb-0" type="text" name="filtre_mandat" id="filtre_mandat">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Origine client</label>
				<select class="form-select fs--1" id="origine_cli_id" name="origine_cli_id">
					<option value="">Tous</option>
					<?php foreach ($originecli as $origineclient) : ?>
						<option value="<?= $origineclient->id; ?>">
							<?= $origineclient->libelle; ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Date creation</label>
				<div class="d-flex">
					<input class="form-control mb-0 calendar" id="date_filtre" name="date_filtre" type="text">
					<button class="btn btn-sm btn-outline-primary d-flex justify-content-center ms-1 d-none" style="width:32px;height: 30px;" id="clear_date_filtre">
						<i class="fas fa-times align-self-center"></i>
					</button>
				</div>
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">CIN</label>
				<select class="form-select fs--1" id="cin_client" name="cin_client">
					<option value="0" selected>Tous</option>
					<option value="1">Avec</option>
					<option value="2">Sans</option>
				</select>
			</div>
		</div>
		<div class="col-2">
			<div class="mb-2">
				<label class="form-label">Suivi de clientèle</label>
				<select class="form-select fs--1" id="filtre_util" name="filtre_util">
					<option value="0">Tous les utilisateurs</option>
					<?php foreach ($utilisateur as $util) : ?>
						<option value="<?= $util->util_id ?>"><?= $util->util_prenom ?>&nbsp;<?= $util->util_nom ?></option>
					<?php endforeach ?>
					<option value="-1">Non renseigné</option>
				</select>
			</div>
		</div>
	</div>


	<div id="data-list" class="">


	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		listClient();
	});

	var date_filtre = flatpickr("#date_filtre", {
		altInput: true,
		allowInput: true,
		"mode": "range",
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y",
		onChange: function() {
			$('#clear_date_filtre').removeClass('d-none');
			listClient();
		},
	});

	$(document).on('click', '#clear_date_filtre', function(e) {
		date_filtre.clear();
		$(this).toggleClass('d-none');
		listClient();
	})
</script>