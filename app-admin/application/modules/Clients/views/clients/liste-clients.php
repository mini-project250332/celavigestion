<?php
function RemoveSpecialChar($str)
{
	$res = str_replace(
		array(
			'\'',
			'"',
			'-',
			' ',
			'.',
			',',
			';',
			'<',
			'>'
		),
		'',
		$str
	);

	return $res;
} ?>

<input type="hidden" value="<?php echo base_url('Clients'); ?>" id="client-open-tab-url" />

<div class="row m-0 py-1">
	<div class="col-md-auto p-0">
		<button class="btn btn-sm bg-light  fw-semi-bold">
			Ligne
		</button>
	</div>
	<div class="col-md-auto p-0">
		<select class="form-select row_view" id="row_view" name="row_view" width="80px">
			<?php foreach (unserialize(row_view) as $key => $value) : ?>
				<option value="<?= $value; ?>" <?= ($value == intval($rows_limit)) ? 'selected' : ''; ?>>
					<?= $value; ?>
				</option>
			<?php endforeach; ?>
		</select>

		<input type="hidden" name="pagination_view" id="pagination_view">
	</div>
	<div class="col p-1 ">
		<div class="d-flex align-items-center position-relative">
			<div class="flex-1 pt-1">
				<h6 class="mb-0 fw-semi-bold"> Resulat(s) :
					<?= $countAllResult ?> enregistré(s)
				</h6>
			</div>
		</div>
	</div>
	<div class="col-md-auto p-0">
		<button class="btn btn-sm btn-secondary mx-1 only_visuel_gestion"><i class="fas fa-file-excel"></i>
			Exporter</button>
		<button id="btnFormNewClient" class="btn btn-sm btn-primary no_account_manager" type="button">
			<span class="bi bi-person-plus-fill me-1"></span>
			Nouveau propriétaire
		</button>
	</div>
</div>

<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm fs--1 mb-0" id="table_clients">
				<thead class="bg-300 text-900">
					<tr>
						<th class="px-2 th_clients" scope="col">Propriétaire (Entreprise/Nom)
							<span class="tri-clients" style="cursor:pointer;">
								<i class="fas fa-sort-amount-down-alt"></i>
							</span>
						</th>
						<th class="px-2 no-sort" scope="col">Contacts</th>
						<th class="no-sort" scope="col">Programme</th>
						<th class="no-sort" scope="col">Exploitants</th>
						<th class="no-sort" scope="col">Origine client</th>
						<th class="no-sort" scope="col">Suivi par</th>
						<th class="th_clients" scope="col">Date creation
							<span class="tri-clients" style=" cursor:pointer;">
								<i class="fas fa-sort-amount-down-alt"></i>
							</span>
						</th>
						<th class="text-end no-sort">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($listClients as $key => $value) : ?>
						<?php
						$phoneNumber = RemoveSpecialChar($value->mobile);
						$numero = '';
						if (isset($value->paysmonde_id) && $value->paysmonde_id == 66) {
							$array = str_split($phoneNumber, 2);
							if ($value->mobile != "") {
								$array[0] = '(' . $array[0][0] . ')' . isset($array[0][1]) && !empty($array[0][1])  ? $array[0][1] : '';
								$numero = implode(".", $array);
							} else {
								$numero = $value->mobile;
							}
						} else {
							$numero = $value->mobile;
						}

						if (isset($value->fixe) && !empty($value->fixe)) {
							$phone = RemoveSpecialChar($value->fixe);

							if ($phone !== null) {
								if ($value->pays_id == 66) {
									$array = str_split($phone, 2);
									$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
									$number = implode(".", $array);
								} else {
									$number = $value->fixe;
								}
							} else {

								$number = "";
							}
						} else {

							$number = "";
						}

						?>
						<?php if (!empty($client_lot_unique)) : ?>
							<tr id="rowClient-<?= $value->id; ?>" class="align-middle <?= in_array($value->id, $client_lot_unique) ? 'bg-warning text-white' : 'bg-200' ?>">
							<?php else : ?>
							<tr id="rowClient-<?= $value->id; ?>" class="align-middle">
							<?php endif ?>
							<td class="px-2">
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<?php if (trim($value->entreprise) != "") : ?>
											<h6 class="mb-0 fw-semi-bold"><a class="stretched-link" href="#">
													<?= $value->entreprise; ?>
												</a></h6>
										<?php endif; ?>
										<p class="text-800 fs--1 mb-0 <?= in_array($value->id, $client_lot_unique) ? 'text-white' : '' ?>">
											<?= $value->nom . ' ' . $value->prenom; ?>
											<button class="btn btn-falcon-primary me-1 mb-1 btnFormUpdateProspect">
												<span class="badge badge-soft-primary <?= $value->etat_client == 1 ? '' : 'd-none' ?>">P</span>
											</button>
										</p>
									</div>
								</div>
							</td>
							<td class="px-2">
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">
											<a class="text-900" href="#">
												<i class="fas fa-envelope fa-w-16 text-primary"></i> Email :
												<span class="px-2">
													<?= $value->mail; ?>
												</span>
											</a>
										</h6>
										<p class="text-800 fs--1 mb-0">
											<i class="fas fa-phone-alt fa-w-16 text-primary"></i> Téléphone(s) :
											<span class="px-2">
												<?= $value->paysmonde_indicatif; ?>
												<?= $numero; ?>
												<?= (trim($value->fixe) != "") ? ' / ' . $value->pays_indicatif . ' ' . $number . '' : ''; ?>
											</span>
										</p>
									</div>
								</div>
							</td>
							<td>
								<?= !empty($value->progm_nom) ? $value->progm_nom : 'Non renseigné' ?>
							</td>
							<td>
								<?= !empty($value->cnctGestionanire_nom) || !empty($value->cnctGestionnaire_prenom) ? $value->cnctGestionanire_nom . ' ' . $value->cnctGestionnaire_prenom : 'Non renseigné' ?>
							</td>
							<td class="">
								<?= $value->origineCliLib; ?>
							</td>
							<td>
								<?= !empty($value->utilNom) || !empty($value->utilNutilPrenomom) ? $value->utilPrenom . ' ' . $value->utilNom : 'Non renseigné' ?>
							</td>
							<td class="creation">
								<span>
									<?= date("d/m/Y", strtotime(str_replace('/', '-', $value->date_creation))); ?>
								</span>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->id; ?>" id="lot_<?= $value->id; ?>" class="btn btn-sm btn-outline-primary icon-btn btnFicheClient" type="button">
										<span class="fas fa-play fs-1"></span>
									</button>
									<button data-id="<?= $value->id; ?>" class="btn btn-outline-primary btnFicheCLientNewTab" type="button">
										<span class="fas fa-plus open_tab_client"></span>
									</button>
									<button data-id="<?= $value->id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup no_account_manager" type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
							</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="d-flex justify-content-end pt-2">
	<div class="d-flex align-items-center p-0">
		<nav aria-label="Page navigation example">
			<ul id="pagination-ul" class="pagination pagination-sm mb-0">
				<li class="page-item" id="pagination-vue-preview">
					<a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
						Précédent
					</a>
				</li>
				<?php foreach ($li_pagination as $key => $value) : ?>
					<li data-offset="<?= $value; ?>" id="pagination-vue-table-<?= $value; ?>" class="page-item pagination-vue-table <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
						<a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
							<?= $key + 1; ?>
						</a>
					</li>
				<?php endforeach; ?>

				<li class="page-item" id="pagination-vue-next">
					<a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
						Suivant
					</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<script>
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);

	let client_id = null;
	let client_nom = null;

	if (urlParams.has('_p@roprietaire') && urlParams.has('_nom')) {
		client_id = urlParams.get('_p@roprietaire');
		client_nom = urlParams.get('_nom');
	}

	if (localStorage.getItem('ClientName') !== 'undefined' && localStorage.getItem('Clientid')) {
		client_nom = localStorage.getItem('ClientName');
		client_id = localStorage.getItem('Clientid');
	}

	if (client_nom) {
		$('#text_filtre').val(client_nom);
		listClient();
	}

	if (client_id) {
		$(`#lot_${client_id}`).trigger("click");
	}

	only_visuel_gestion(["1", "3"], "no_account_manager");

	const client_tab = urlParams.get('_open@client_tab');
	if (client_tab) {
		ficheClient({
			client_id: client_tab
		});
	}
</script>