<?php echo form_tag('Clients/clientAdd', array('method' => "post", 'class' => '', 'id' => 'formAddAjout')); ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
<!-- MDB -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.0.0/mdb.min.css" rel="stylesheet" /> -->

<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire d'ajout d'un nouveau propriétaire</h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnuler" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<div class="contenair-content">

	<div class="row m-0">
		<div class="col-12 p-0 py-1">
			<div class="row m-0">
				<div class="col p-1">
					<div class="card rounded-0 h-100">
						<div class="card-body ">

							<h5 class="fs-0 mb-2">Identité</h5>
							<hr class="mt-1">

							<div class="form-group inline">
								<div>
									<label data-width="250"> Nom *</label>
									<input type="text" class="form-control maj" id="client_nom" name="client_nom" data-formatcontrol="true" data-require="true">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="250"> Prénom *</label>
									<input type="text" class="form-control maj" id="client_prenom" name="client_prenom" data-formatcontrol="true" data-require="true">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="250"> Entreprise </label>
									<input type="text" class="form-control maj" id="client_entreprise" name="client_entreprise">
									<div class="help"></div>
								</div>
							</div>

							<hr class="mt-1">

							<div class="form-group inline">
								<div>
									<label data-width="250"> Nationnalité *</label>
									<select class="form-select" id="client_nationalite" name="client_nationalite">
										<?php foreach ($nationalite as $nationalites) :
											if ($nationalites->libelle == "Française") : ?>
												<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
										<?php endif;
										endforeach; ?>
										<?php foreach ($nationalite as $nationalites) :
											if ($nationalites->libelle == "Britannique") : ?>
												<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
										<?php endif;
										endforeach; ?>
										<?php foreach ($nationalite as $nationalites) :
											if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
												<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
										<?php endif;
										endforeach; ?>
									</select>
									<div class="help"></div>
								</div>
							</div>

							<hr class="mt-1">

							<div class="form-group inline">
								<div>
									<label data-width="250"> Situation matrimoniale *</label>
									<select class="form-select" id="client_situation_matrimoniale" name="client_situation_matrimoniale">
										<?php foreach (unserialize(situation) as $key => $value) : ?>
											<option><?= $value; ?></option>
										<?php endforeach; ?>
									</select>
									<div class="help"></div>
								</div>
							</div>

							<hr class="mt-1">

						</div>
					</div>
				</div>

				<div class="col p-1">
					<div class="card rounded-0 h-100">
						<div class="card-body ">
							<h5 class="fs-0">Contact</h5>
							<hr class="mt-1">
							<div class="form-group inline">
								<div>
									<label data-width="200"> Email 1 *</label>
									<input data-type="email" type="text" class="form-control" id="client_email" name="client_email" data-formatcontrol="true" data- require="true">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Email 2</label>
									<input data-type="email" type="text" class="form-control" id="client_email1" name="client_email1">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="250"> Télephone 1 *</label>
									<select class="form-select select2 select2-hidden-accessible" style="width: 30%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id" name="paysmonde_id" data-formatcontrol="true" data-require="true">
										<?php foreach ($pays_indicatif as $indicatif) : ?>
											<option value="<?= $indicatif->paysmonde_id ?>" title="<?= $indicatif->paysmonde_libelle ?>">
												<?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
											</option>
										<?php endforeach; ?>
									</select>
									<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="client_phonemobile" name="client_phonemobile" style="height: 37px;width: 89%;left: 4px;">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="250"> Télephone 2</label>
									<select class="form-select select2 select2-hidden-accessible" style="width: 30%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id" name="pays_id">
										<?php foreach ($pays as $indicatifs) : ?>
											<option value="<?= $indicatifs->pays_id ?>" title="<?= $indicatifs->pays_libelle ?>">
												<?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
											</option>
										<?php endforeach; ?>
									</select>
									<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="client_phonefixe" name="client_phonefixe" style="height: 37px;width: 89%;left: 4px;">
									<div class="help"></div>
								</div>
							</div>

							<h5 class="fs-0">Adresse</h5>

							<hr>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 1 </label>
									<input type="text" class="form-control maj" id="client_adresse1" name="client_adresse1">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 2 </label>
									<input type="text" class="form-control maj" id="client_adresse2" name="client_adresse2">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 3 </label>
									<input type="text" class="form-control maj" id="client_adresse3" name="client_adresse3">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Code postal </label>
									<input type="text" class="form-control" id="client_cp" name="client_cp">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Ville </label>
									<input type="text" class="form-control maj" id="client_ville" name="client_ville">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Pays *</label>
									<input type="text" class="form-control maj" id="client_pays" name="client_pays" data-formatcontrol="true" data-require="true" value="FRANCE">
									<div class="help"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col p-1">
					<div class="card rounded-0 h-100">
						<div class="card-body ">
							<h5 class="fs-0">Autres informations</h5>
							<hr class="mt-1">

							<div class="form-group inline">
								<div>
									<label data-width="200"> Date de creation </label>
									<input type="text" class="form-control calendar" id="client_date_creation" name="client_date_creation" data-formatcontrol="true" data- require="true">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Origine client </label>
									<select class="form-select " id="" name="origine_cli_id">
										<?php foreach ($listeOrigineClient as $key => $value) : ?>
											<option value="<?= $value->id; ?>"><?= $value->libelle; ?></option>
										<?php endforeach; ?>
									</select>
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline d-none">
								<div>
									<label data-width="200"> Suivi par</label>
									<select class="form-select" id="util_id" name="util_id" data-formatcontrol="true" data-require="true">
										<?php foreach ($listeUser as $key => $value) : ?>
											<option value="<?= $value->id; ?>" <?= ($value->id == $_SESSION['session_utilisateur']['util_id']) ? 'selected' : ""; ?>>
												<?= $value->nom . ' ' . $value->prenom; ?>
											</option>
										<?php endforeach; ?>
									</select>
									<div class="help"></div>
								</div>
							</div>
							<hr>
							<div class="form-group inline textarea_form">
								<div>
									<label data-width="200"> Commentaire </label>
									<textarea class="form-control" id="client_commentaire" name="client_commentaire" style="height: 100px"></textarea>
									<div class="help"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo form_close(); ?>


<script type="text/javascript">
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y",
		defaultDate: moment().format('DD-MM-YYYY'),
	});

	$(document).ready(function() {
		$('[data-bs-toggle="tooltip"]').tooltip();
	});

	$(".select2").val(66);

	$(document).ready(function() {
		$('.select2').select2({
			theme: 'bootstrap-5',
			//placeholder: "Indicatif",		
		});
	});
</script>