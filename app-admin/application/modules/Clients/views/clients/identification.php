<div class="row m-0 ">
	<div class="col-md-5 p-1 m-0">

		<div class=" p-2 m-0">

			<div class="form-group inline mt-3">
			<div>
				<label data-width="150"> Nom prospect *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Télephone *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Email *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Adresse 1 *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Adresse 2</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Adresse 3</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Code postale *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Ville *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Pays *</label>
				<input type="text" class="form-control " id="" name="">
				<div class="help"></div>
			</div>
		</div>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Nationnalité *</label>
				<select class="form-select ">
					<option value="1">Français</option>
					<option value="2">2</option>
				</select>
				<div class="help"></div>
			</div>
		</div>

		<hr>

		<div class="form-group inline">
			<div>
				<label data-width="150"> Communication *</label>
				<textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
				<div class="help"></div>
			</div>
		</div>

	</div>

	</div>
	<div class="col-md-7 p-1 m-0">

		<div class=" p-2 m-0">
				<div class="row m-0">
					<div class="col">
						<div class="form-group">
						<div>
							<label> Date demande</label>
							<select class="form-select ">
								<option value="1">Français</option>
								<option value="2">2</option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					</div>

					<div class="col">
						<div class="form-group">
						<div>
							<label>  Origine client </label>
							<select class="form-select ">
								<option value="1">Années</option>
								<option value="2">2</option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					</div>

					<div class="col">
						<div class="form-group">
						<div>
							<label> Suivi par</label>
							<select class="form-select ">
								<option value="1">Français</option>
								<option value="2">2</option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					</div>
					
				</div>
				
				<div class="row m-0">
					<div class="d-flex justify-content-end">
						<button class="btn btn-sm btn-primary m-1" type=""><i class="fas fa-users"></i> Accèder</button>
					</div>
				</div>

		</div>
	
	</div>
</div>