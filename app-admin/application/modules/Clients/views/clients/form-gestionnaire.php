<div class="modal-header" style="background-color : #636E7E !important">
	<h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter un gestionnaire</h5>
	<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Setting/Gestionnaire/cruGestionnaire', array('method' => "post", 'class' => 'px-2', 'id' => 'AddGestion')); ?>

<?php if (isset($gestionnaire->gestionnaire_id)) : ?>
	<input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?= $gestionnaire->gestionnaire_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<div class="modal-body">
	<!-- With contact form -->
	<div class="row mb-1">
		<div class="col-4 py-1 mb-2 p">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<h5 class="fs-1 text-center pb-2">Informations du gestionnaire</h5>
					<hr class="mt-0">
					<div class="form-group inline">
						<div>
							<label data-width="200"> Nom *</label>
							<input type="text" class="form-control maj" id="gestionnaire_nom" name="gestionnaire_nom" data-formatcontrol="true" data-require="true">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 1 </label>
							<input type="text" class="form-control maj" id="gestionnaire_adresse1" name="gestionnaire_adresse1">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 2 </label>
							<input type="text" class="form-control maj" id="gestionnaire_adresse2" name="gestionnaire_adresse2">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 3 </label>
							<input type="text" class="form-control maj" id="gestionnaire_adresse3" name="gestionnaire_adresse3">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Code postal </label>
							<input type="text" class="form-control" id="gestionnaire_cp" name="gestionnaire_cp">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Ville </label>
							<input type="text" class="form-control maj" id="gestionnaire_ville" name="gestionnaire_ville">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Pays </label>
							<input type="text" class="form-control maj" id="gestionnaire_pays" name="gestionnaire_pays">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline switch">
						<div>
							<label data-width="250"> Statut gestionnaire</label>
							<div class="form-check form-switch">
								<span> Inactif </span>
								<input class="form-check-input" type="checkbox" id="gestionnaire_etat" name="gestionnaire_etat" <?php echo isset($gestionnaire->gestionnaire_etat) ? ($gestionnaire->gestionnaire_etat == "1" ? "checked" : "") : "checked"; ?>>
								<span> Actif </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-4 py-1 mb-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<h5 class="fs-1 text-center pb-2">Informations du contact</h5>
					<hr class="mt-0">

					<div class="form-group inline">
						<div class="col-sm-12">
							<label data-width="200"> Nom * </label>
							<input type="text" class="form-control maj" value="<?= isset($contact->cnctGestionanire_nom) ? $contact->cnctGestionanire_nom : ""; ?>" id="cnctGestionanire_nom" name="cnctGestionanire_nom" data-formatcontrol="true" data-require="true">
							<div class="help"></div>
						</div>

						<div class="form-group inline p-0">
							<div class="col-sm-12">
								<label data-width="200"> Prénom * </label>
								<input type="text" class="form-control" value="<?= isset($contact->cnctGestionnaire_prenom) ? $contact->cnctGestionnaire_prenom : ""; ?>" id="cnctGestionnaire_prenom" name="cnctGestionnaire_prenom">
								<div class="help"> </div>
							</div>
						</div>

						<div class="form-group inline p-0">
							<div class="col-sm-12">
								<label data-width="200"> Fonction * </label>
								<input type="text" class="form-control maj" value="<?= isset($contact->cnctGestionnaire_fonction) ? $contact->cnctGestionnaire_fonction : ""; ?>" id="cnctGestionnaire_fonction" name="cnctGestionnaire_fonction">
								<div class="help"> </div>
							</div>
						</div>

						<div class="form-group inline p-0">
							<div class="col-sm-12">
								<label data-width="200"> Service *</label>
								<input type="text" class="form-control maj" value="<?= isset($contact->cnctGestionnaire_service) ? $contact->cnctGestionnaire_service : ""; ?>" id="cnctGestionnaire_service" name="cnctGestionnaire_service">
								<div class="help"></div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-4 py-1 mb-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<h5 class="fs-1 text-center pb-2">Détails du contact</h5>
					<hr class="mt-0">

					<div class="form-group inline p-0">
						<div class="col-sm-12">
							<label data-width="200"> Email 1 * </label>
							<input type="text" class="form-control" value="<?= isset($contact->cnctGestionnaire_email1) ? $contact->cnctGestionnaire_email1 : ""; ?>" id="cnctGestionnaire_email1" name="cnctGestionnaire_email1" data-formatcontrol="true" data-require="true">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline p-0">
						<div class="col-sm-12">
							<label data-width="200"> Email 2</label>
							<input type="text" class="form-control" value="<?= isset($contact->cnctGestionnaire_email2) ? $contact->cnctGestionnaire_email2 : ""; ?>" id="cnctGestionnaire_email2" name="cnctGestionnaire_email2">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline p-0">
						<div class="col-sm-12">
							<label data-width="200"> Téléphone 1 </label>
							<input v-on:keyup='input_form' type="text" class="form-control" value="<?= isset($contact->cnctGestionnaire_tel1) ? $contact->cnctGestionnaire_tel1 : ""; ?>" id="cnctGestionnaire_tel1" name="cnctGestionnaire_tel1">
							<div class="help"> </div>
						</div>
					</div>

					<div class="form-group inline p-0">
						<div class="col-sm-12">
							<label data-width="200"> Téléphone 2 </label>
							<input type="text" class="form-control" value="<?= isset($contact->cnctGestionnaire_tel2) ? $contact->cnctGestionnaire_tel2 : ""; ?>" id="cnctGestionnaire_tel2" name="cnctGestionnaire_tel2">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline p-0">
						<div class="col-sm-12">
							<label data-width="200"> Type </label>
							<select class="form-control form-select fs--1" id="type_cont_id" name="type_cont_id" value="<?= isset($contact->type_cont_id) ? $contact->type_cont_id : ""; ?>">
								<?php foreach ($type_contact as $type) { ?>
									<option value="<?= $type->type_cont_id; ?>" <?php if (isset($contact->type_cont_id) && $type->type_cont_id == $contact->type_cont_id)
																					echo "selected"; ?>>
										<?php echo $type->type_cont_libelle; ?>
									</option>
								<?php } ?>
							</select>
							<div class="help"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
		<button type="submit" class="btn btn-primary">Ajouter</button>
	</div>

	<?php echo form_close(); ?>