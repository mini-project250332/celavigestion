<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Carbon\Carbon;
class Pnoprospect extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Pnoprospect";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/pno/pno_prospect.js",
        );

        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function pagePno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('Pno_m');
                $data['pno'] = $this->Pno_m->get(
                    array(
                    'clause' => array("lot_id" => $data_post['lot_id']),
                    'join' => array('c_etat_pno' => 'c_etat_pno.etat_pno_id = c_pno.etat_pno_id'),
                    'method' => 'row'
                    )
                );

                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
                $this->renderComponant("Prospections/prospects/lot/pno/page-pno", $data);
            }
        }
    }

    public function AddPno(){
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Pno_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("lot_id" => $data['lot_id'],"prospect_id" => $data['prospect_id']);
                    unset($data['action']);
                    unset($data['undefined']);    
                    
                    unset($data['pno_path']);
               
                    $data['pno_celavi'] = $data['ouicelaviegestion'];
                    $data['pno_syndic'] = $data['ouisyndic'];
                    $data['pno_client'] = $data['ouiclient'];

                    if ($data['pno_celavi'] == 1) {
                        $data['pno_celavi'] = 1;
                        $data['pno_syndic'] = 0;
                        $data['pno_client'] = 0;
                    }else{
                        $data['pno_celavi'] = 2;                       
                    }

                    if ($data['pno_syndic'] == 1) {
                        $data['pno_syndic'] = 1;
                    } else if($data['pno_syndic'] == 0 && $data['nesaitpassyndic'] == 1) {
                        $data['pno_syndic'] = 3;
                    }else if($data['nonsyndic'] == 1){
                        $data['pno_syndic'] = 2;
                    }else{
                        $data['pno_syndic'] = 0;
                    }

                    if ($data['pno_client'] == 1) {
                        $data['pno_client'] = 1;
                        $data['pno_montant'] = $data['pno_montant_client'];
                        $data['pno_path'] = $data['pno_path_client'];
                    } else if($data['pno_client'] == 0 && $data['nesaitpasclient'] == 1) {
                        $data['pno_client'] = 3;
                    }else if($data['nonclient'] == 1){
                        $data['pno_client'] = 2;
                    }else{
                        $data['pno_client'] = 0;
                    }                    

                    unset($data['ouicelaviegestion']);
                    unset($data['noncelaviegestion']);
                    unset($data['ouisyndic']);
                    unset($data['nonsyndic']);
                    unset($data['nesaitpassyndic']);
                    unset($data['ouiclient']);
                    unset($data['nonclient']);
                    unset($data['nesaitpasclient']);
                    unset($data['pno_montant_client']);
                    unset($data['pno_path_client']);
                    unset($data['pno_path']);

                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("pno_id" => $data['pno_id']);
                        unset($data['pno_id']);
                        unset($data['lot_id']);
                    }

                    $request = $this->Pno_m->save_data($data, $clause);
                    $data_retour['pno_id'] = ($action == "add") ? $request : $_POST['data']['pno_id']['value'];
                    $data_retour['action'] = 'edit';

                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Information enregistrée avec succès" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormAddPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $this->load->model('Pno_m');
                if ($data_post['action'] == "edit") {                    
                    $params = array(
                        'clause' => array('pno_id ' => $data_post['pno_id']),
                        'method' => "row"
                    );
                    $data['pno'] = $this->Pno_m->get($params);
                }
                $data['pno_id'] = $data_post['pno_id'];
                $data['action'] = $data_post['action'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
            }

            $this->renderComponant("Prospections/prospects/lot/pno/form-pno", $data);
        }
    }

    public function CreerPno(){
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Pno_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "lot_id" => $data['lot_id'], "action" => $action);
                    $lot_id = $data['lot_id'];
                    $prospect_id = $data['prospect_id'];
                    $data['pno_date_creation'] = date('Y-m-d H:i:s', strtotime($data['pno_date_creation']));
                    unset($data['action']);
                    $clause = array("pno_id" => $data['pno_id']);

                    if ($action == "edit") {                        
                        $data_retour['pno_id'] = $data['pno_id'];
                        unset($data['pno_id']);
                    }

                    $request = $this->Pno_m->save_data($data, $clause);
                    // $data_retour['pno_date_creation'] = ($action == "add") ? $request : $_POST['data']['pno_date_creation']['value'];
                    // $data_retour['pno_lieu_creation'] = ($action == "add") ? $request : $_POST['data']['pno_lieu_creation']['value'];

                    if (!empty($request)) {
                        $gpdf = $this->generatePdf_pno($lot_id);

                        $retour = retour(true, "success", $data_retour, array("message" => $lot_id . "-" . $gpdf));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function generatePdf_pno($lot_id){

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");
        $this->load->model('Pno_m');      
      
        $data = array();
        $params = array(
            'clause' => array('c_pno.lot_id' => $lot_id),
            'join' => array(
                'c_prospect' => 'c_prospect.prospect_id = c_pno.prospect_id',
                'c_lot_prospect' => 'c_lot_prospect.lot_id = c_pno.lot_id',
                'c_programme' => 'c_lot_prospect.progm_id = c_programme.progm_id'
            ),
            'method' => "row"

        );

        $data['pno'] = $this->Pno_m->get($params);

        $mpdf = new \Mpdf\Mpdf();

        $html = $this->load->view('Prospections/prospects/lot/pno/modele_pno/modele_pno',$data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);        
        $filename = "PNO_CLV";
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/prospects/lots/pno/" . $lot_id;
        $url_doc = "../documents/prospects/lots/pno/$lot_id/$url";
        $this->makeDirPath($url_path);

        $clause = array("lot_id" => $lot_id);
        $data = array(
            'pno_path' => str_replace('\\', '/', $url_doc),
            'pno_date_creation' => date('Y-m-d')
        );
        $this->Pno_m->save_data($data, $clause);

        $url_file = $url_path . "/" . $filename . ".pdf";

        $mpdf->Output($url_file, 'F');
        return $url_path;
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function getPathPno()
    {

        $this->load->model('Pno_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('pno_id', 'pno_path'),
                    'clause' => array('pno_id' => $data_post['id_doc'])
                );

                $request = $this->Pno_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function save_path_pno()
    {
        $this->load->model('Pno_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = $_POST;
                if (!empty($_FILES)) {
                    $target_dir = "../documents/prospects/lots/pno/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['pno_path'] = $path_filename_ext;
                }

                $pno_id = $_POST['pno_id'];
                $clause = array("pno_id" => $pno_id);

                $this->Pno_m->save_data($data, $clause);
            }
        }
    }

    public function save_path_pno_prospect()
    {
        $this->load->model('Pno_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = $_POST;
                if (!empty($_FILES)) {
                    $target_dir = "../documents/prospects/lots/pno/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['pno_path'] = $path_filename_ext;
                }

                $pno_id = $_POST['pno_id'];
                $clause = array("pno_id" => $pno_id);

                $this->Pno_m->save_data($data, $clause);
            }
        }
    }

    public function remove_file()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pno_m');

                $data_post = $_POST['data'];
                $param = array(
                    'clause' => array('lot_id' => $data_post['lot_id']),
                    'method' => 'row'
                );

                $data = $this->Pno_m->get($param);

                $data_pno = array(
                    
                    'pno_path' => NULL
                );

                $lot_id = $data_post['lot_id'];
                unset($data_post['lot_id']);

                $clause = array("lot_id" => $lot_id);

                $request = $this->Pno_m->save_data($data_pno, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "Fichier Supprimée"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function sendPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Pno_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('pno_id' => $data['pno_id']),
                    'columns' => array('pno_id'),
                    'method' => "row",
                );
                $request_get = $this->Pno_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['pno_id'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous vraiment basculer ce PNO à l'état 'Pno envoyé' ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('pno_id' => $data['pno_id']);
                    $data_update = array('etat_pno_id' => 2, 'pno_date_envoi' => date('Y-m-d'));
                    $request = $this->Pno_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['pno_id']),
                            'message' => "PNO envoyé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function SignerPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['pno_id'] = $data_post['pno_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
            }

            $this->renderComponant("Prospections/prospects/lot/pno/sign-pno", $data);
        }
    }

    public function AddSignPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pno_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("prospect_id" => $data['prospect_id'], "lot_id" => $data['lot_id']);
                $pno_id = $data['pno_id'];
                $lot_id = $data['lot_id'];
                $data['etat_pno_id'] = 3;
                $data['pno_date_signature'] = Carbon::now()->toDateTimeString();
                $data['pno_path'] = NULL;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/prospects/lots/pno/$lot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file); 
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['pno_path'] = $path_filename_ext;
                }

                $clause = array("pno_id" => $pno_id);

                $request = $this->Pno_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Signature ajoutée avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function ClorePnoCli()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Pno_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('pno_id' => $data['pno_id']),
                    'columns' => array('pno_id'),
                    'method' => "row",
                );
                $request_get = $this->Pno_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['pno_id'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous vraiment clore ce PNO ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('pno_id' => $data['pno_id']);
                    $data_update = array('etat_pno_id' => 4);
                    $request = $this->Pno_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['pno_id']),
                            'message' => "PNO envoyé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

        /********Cloturer PNO**********/

    public function cloturerPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];
                $data = array();   

                $data['pno']= $this->Pno_m->get(
                    array(
                        'clause' => array('pno_id' => $data_post['pno_id']),
                        'method' => 'row'
                    )
                );

                $data['pno_id'] = $data_post['pno_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
            }

            $this->renderComponant("Prospections/prospects/lot/pno/cloture-pno", $data);
        }
    }

    public function CloturePno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Pno_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("prospect_id" => $data['prospect_id'], "lot_id" => $data['lot_id']);
                $pno_id = $data['pno_id'];
                $lot_id = $data['lot_id'];
                $data['etat_pno_id'] = 5;
                $data['pno_fichier_cloture'] = NULL;
                $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data['pno_date_cloture'] = date('Y-m-d');

                if (!empty($_FILES)) {
                    $target_dir = "../documents/prospects/lots/pno/$lot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['pno_fichier_cloture'] = $path_filename_ext;
                }
                
                $clause = array("pno_id" => $pno_id);               

                $request = $this->Pno_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "PNO clôturé avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

}