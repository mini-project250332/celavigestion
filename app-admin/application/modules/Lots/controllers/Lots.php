<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Lots extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Lots";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/lots/lots.js",
            "js/historique/historique.js",
            "js/saisietemps/saisietemps.js"
        );

        $this->_css_personnaliser = array(
            "css/lots/lots.css",
            "css/clients/clients.css",
            "css/select2.min.css",
            "css/historique/historique.css",
            "css/loyer/loyer.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "lots";
        $this->_data['sous_module'] = "lots/contenaire-lot";

        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Taxe_m');

                $data['annee_tom'] = $this->Taxe_m->get(
                    array(
                        'columns' => array('annee'),
                        'distinct' => true,
                        'order_by_columns' => "annee DESC"
                    )
                );

                $data['utilisateur'] = $this->getListUser();
                $data['gestionnaire'] = $this->getListGestionnaire();
                $this->renderComponant("Lots/lots/header-liste", $data);
            }
        }
    }

    public function listeLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m');
                $this->load->model('Client_m');
                $data_post = $_POST['data'];
                $rows_limit = intval($data_post['row_data']);
                $pagination_data = explode('-', $data_post['pagination_page']);

                $data_lot = $this->ClientLot_m->listLot($data_post);
                $self = $this;
                $data_lot = array_map(function ($value) use ($self) {
                    $client = explode(',', $value->client_id);
                    if (count($client) > 1) {
                        $client_name = $self->Client_m->getNomClient($client);
                        $value->client = $client_name;
                    }
                    return $value;
                }, $data_lot);

                $data["countAllResult"] = count($data_lot);
                $nb_total = $data["countAllResult"];
                $data['pagination_page'] = $data_post['pagination_page'];
                $data['rows_limit'] = $rows_limit;

                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];

                $data_lot = array_slice($data_lot, $pagination_data[0], $pagination_data[1]);
                $data['lot'] = $data_lot;
                $this->renderComponant("Lots/lots/liste_lot", $data);
            }
        }
    }

    function getDossier()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                // $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('dossier_etat' => 1),
                    'columns' => array('dossier_id', 'dossier_libelle')
                );
                $request = $this->dossier->get($params);
                return $request;
            }
        }
    }

    public function getListProgram()
    {

        $this->load->model('Program_m');
        $params = array(
            'clause' => array('progm_etat' => 1),
            'columns' => array('progm_id', 'progm_nom')
        );
        $request = $this->Program_m->get($params);
        return $request;
    }

    public function Alerte_cin_Lot()
    {
        if (is_ajax()) {
            $data['nom'] = $_POST['data']['nom'];
            $this->renderComponant("Lots/lots/form-alert-cin-lots", $data);
        }
    }
}
