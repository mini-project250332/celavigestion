<input type="hidden" value="<?php echo base_url('Clients'); ?>" id="client-open-tab-url" />
<div class="row mt-2 mb-2">
    <div class="col-md-auto pt-1">
        <button class="btn btn-sm bg-light fw-semi-bold">
            Ligne
        </button>
    </div>
    <div class="col-md-auto">
        <select class="form-select row_view" id="row_view" name="row_view" width="80px">
            <?php foreach (unserialize(row_view) as $key => $value) : ?>
                <option value="<?= $value; ?>" <?= ($value == intval($rows_limit)) ? 'selected' : ''; ?>>
                    <?= $value; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="pagination_view" id="pagination_view">
    </div>
    <div class="col p-1 ">
        <div class="d-flex align-items-center position-relative">
            <div class="flex-1 pt-1">
                <h6 class="mb-0 fw-semi-bold"> Resulat(s) :
                    <?= $countAllResult ?> lots affiché(s)
                </h6>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive scrollbar">
    <table class="table table-sm table-striped fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th class="text-center">Propriétaire</th>
                <th class="text-center">Nom programme</th>
                <th class="text-center">Ville</th>
                <th class="text-center" width="10%">Lot principal</th>
                <th class="text-center">Type du Lot</th>
                <th class="text-center" width="20%">Nom gestionnaire</th>
                <th class="text-end">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($lot as $lots) { ?>
                <tr id="rowLot-<?= $lots->cliLot_id; ?>">
                    <td class="text-center">
                        <?= $lots->client ?>
                    </td>
                    <td class="text-center">
                        <?= $lots->progm_nom ?>
                    </td>
                    <td class="text-center">
                        <?= $lots->progm_ville ?>
                    </td>
                    <td class="text-center">
                        <?= $lots->cliLot_principale == 1 ? "Oui" : "Non"  ?>
                    </td>
                    <td class="text-center">
                        <?= $lots->typelot_libelle ?>
                    </td>
                    <td class="text-center">
                        <?= $lots->gestionnaire_nom ?>
                    </td>
                    <td class="text-center">
                        <div>
                            <div class="d-flex justify-content-end ">
                                <!-- <button data-id="<?= $lots->cliLot_id ?>" data-client_id="<?= $lots->cli_id ?>" class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 FicheLot <?= $lots->cin != NULL || $_SESSION['session_utilisateur']['rol_util_valeur'] == 1 ? '' : 'alert_cin' ?>" data-nom="<?= $lots->client_nom ?>" type="button"> -->
                                <button data-id="<?= $lots->cliLot_id ?>" data-client_id="<?= $lots->cli_id ?>" class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 FicheLot" data-nom="<?= $lots->client_nom ?>" type="button">
                                    <span class="fas fa-play fs-1 m-1"></span>
                                </button>
                                <button class="btn btn-outline-primary btnFicheLotNewTab mt-2" data-client_id="<?= $lots->cli_id ?>" data-id="<?= $lots->cliLot_id ?>" data-nom="<?= $lots->client_nom ?>" type="button">
                                    <span class="fas fa-plus open_tab_lot"></span>
                                </button>
                                <button data-id="<?= $lots->cliLot_id ?>" class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 btnConfirmSupLot only_visuel_gestion" type="button">
                                    <span class="bi bi-trash fs-1 m-1"></span>
                                </button>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-<?= $value; ?>" class="page-item pagination-vue-table <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<script>
    only_visuel_gestion();

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const client_tab = urlParams.get('_open@client_tab');
    if (client_tab) {
        $(`#lot_${client_id}`).trigger("click");
    }
</script>