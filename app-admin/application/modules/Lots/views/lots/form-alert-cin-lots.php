<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Information</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div>
                            <span class="alert alert-warning" style="margin-left: 20px;">
                                <p class="fs--1 text-center">
                                    Impossible d'accéder à la fiche Lot du propriétaire <?= $nom ?>. <br>
                                    Veuillez d'abord renseigner la CNI dans la fiche propriétaire de ce dernier.
                                </p>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
    </div>
</div>