<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Liste des Lots</h5>
    </div>
    <div class="d-flex flex-row justify-content-center">

    </div>
</div>
<div class="contenair-list">
    <div class="row">
        <div class="row">
            <div class="col">
                <div class="mb-2">
                    <label class="form-label">Gestionnaire de compte client</label>
                    <select class="form-select" id="charge_client">
                        <option value="">Tous</option>
                        <?php foreach ($utilisateur as $key => $value) : ?>
                            <option value="<?= $value->id ?>"><?= $value->prenom . ' ' . $value->nom ?></option>
                        <?php endforeach ?>
                        <option value="-1">Non renseigné</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label class="form-label">TOM</label>
                    <select class="form-select" id="filtre_tom">
                        <option value="Tous">Tous</option>
                        <option value="non_renseigne">TOM non renseignée pour l'année sélectionnée</option>
                        <option value="0">TOM à valider pour l'année sélectionnée</option>
                        <option value="1">TOM validée pour l'année sélectionnée</option>
                    </select>
                </div>
            </div>
            <div class="annetom col d-none">
                <div class="mb-2">
                    <label class="form-label">Année</label>
                    <select class="form-select" id="annee_tom">
                        <option value="">Toutes</option>
                        <?php foreach ($annee_tom as $key => $value_annee) : ?>
                            <option value="<?= $value_annee->annee ?>"><?= $value_annee->annee != '' ? $value_annee->annee : 'Années antérieures' ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label class="form-label">Lots</label>
                    <select class="form-select select_lot_bail_status" id="filter_bail_lot">
                        <option value="*">tous les lots </option>
                        <option value="1">bail validé</option>
                        <option value="0">bail non validé</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label class="form-label">Exploitant sur bail</label>
                    <select class="form-select" id="gestionnaire_id">
                        <option value="">tous les gestionnaires </option>
                        <option value="*">lots sans bail</option>
                        <?php foreach ($gestionnaire as $key => $value) : ?>
                            <option value="<?= $value->gestionnaire_id ?>"><?= $value->gestionnaire_nom ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label class="form-label"> Filtre statut lots</label>
                    <select class="form-select" id="filtre_lot">
                        <option value="*">Tous</option>
                        <option value="1">Actif</option>
                        <option value="2">Inactif</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="data-list_dossier">

        </div>
    </div>
</div>

<script>
    only_visuel_gestion();
    $(document).ready(function() {
        listLot();
    });

    $("#filter_bail_lot").bind("change", function(event) {
        listLot(event.target.value);
    });

    $(document).on('change', '#filtre_tom', function(e) {
        e.preventDefault();
        var filtre_tom = $(this).val();
        if (filtre_tom != "Tous") {
            $('.annetom').removeClass('d-none');
        } else {
            $('.annetom').addClass('d-none')
        }
    });
</script>