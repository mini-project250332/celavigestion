    <div class="table-responsive scrollbar mt-2">
        <table class="table table-sm fs--1 mb-0">
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Nom </th>
                    <th scope="col">Contacts</th>
                    <th scope="col">Adresses</th>
                    <th scope="col">Date création</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($list as $key => $value) : ?>
                <tr>
                    <td class="px-2"><?= $value['type']  ?></td>
                    <td class="px-2">
                        <?php if(trim($value['entreprise'])!=""):?>
                            <h6 class="mb-0 fw-semi-bold"><?=$value['entreprise'];?></h6>
                        <?php endif;?>
                        <p class="text-800 fs--1 mb-0"><?=$value['nom'].' '.$value['prenom'];?> </p>                        
                    </td>
                    <td class="px-2">
                        <div class="d-flex align-items-center position-relative">
                            <div class="flex-1">
                                <h6 class="mb-0 fw-semi-bold">
                                    <a class="text-900" href="#">
                                        <i class="fas fa-envelope fa-w-16 text-primary"></i> Email : 
                                        <span class="px-2"> <?=$value['email'];?> </span>
                                    </a>
                                </h6>
                                <p class="text-800 fs--1 mb-0">
                                    <i class="fas fa-phone-alt fa-w-16 text-primary"></i> Téléphone(s) :
                                    <span class="px-2"> <?=$value['phonemobile'];?> <?=(trim($value['phonefixe']) !="") ? ' /  '.$value['phonefixe']. '' : '';?></span>
                                </p>
                            </div>
                        </div>
                    </td>
                    <td class="px-2">
                        <div class="d-flex align-items-center position-relative">
                            <div class="flex-1">
                                <p class="text-800 fs--1 mb-0">
                                    <?=$value['adresse1'];?> <?=(trim($value['adresse2']) !="") ? ' / '.$value['adresse2'] : '';?> <?=(trim($value['adresse3']) !="") ? ' / '.$value['adresse3'] : '';?>
                                </p>  
                                <h6 class="mb-0 fw-semi-bold">
                                    <?=$value['cp'];?> <?=$value['ville'];?> , <?=$value['pays'];?>
                                </h6>                          
                            </div>
                        </div>
                    </td>
                    <td class="px-2"><?= date("d/m/Y",strtotime(str_replace('/', '-',$value['date_creation'])))  ?></td>
                    <td class="px-2">
                        <div>
                            <div class="d-flex justify-content-end ">
                                <button data-id ="<?= $value['id'] ?>" class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 <?= $value['type']=="Prospect" ? "btnFormUpdateProspect" : "btnFicheClient" ?>" type="button">
                                    <span class="fas fa-play fs-1 m-1"></span>
                                </button>                       
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>

  