<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Search extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Search";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/app.js",
            "js/clients/clients.js",
            "js/components/prospections/prospections.js",
            "js/historique/historique.js",
            "js/bail/bail.js",
            "js/loyer/loyer.js",
            "js/acte/acte.js",
            "js/immo/immo.js",
            "js/dossiers/dossiers.js",
            "js/charges/charges.js",
            "js/fiscal/fiscal.js"
        );
        $this->_css_personnaliser = array(

        );
    }

    public function index()
    {
        $this->_data['sous_module'] = "search/contenair-global";

        $this->render('contenaire');
    }


    public function listGlobal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $this->renderComponant("Search/search/liste", $data);
            }
        }
    }

    public function liste()
    {
        $this->load->model('Client_m');
        $this->load->model('Prospect_m');
        $like = array();
        $or_like = array();
        $like_pro = array();
        $or_like_pro = array();
        $data_post = $_POST['data'];

        if ($data_post['filtre_global'] != '') {
            $like_pro['prospect_nom'] = $data_post['filtre_global'];
            $or_like_pro['prospect_prenom'] = $data_post['filtre_global'];
            $or_like_pro['prospect_entreprise'] = $data_post['filtre_global'];

            $like['client_nom'] = $data_post['filtre_global'];
            $or_like['client_prenom'] = $data_post['filtre_global'];
            $or_like['client_entreprise'] = $data_post['filtre_global'];

        }


        $request_client = $this->Client_m->get(
            array(
                'clause' => array('client_etat' => 1),
                'like' => $like,
                'or_like' => $or_like,
            )
        );

        $request_prospect = $this->Prospect_m->get(
            array(
                'clause' => array('etat_prospect' => 1),
                'like' => $like_pro,
                'or_like' => $or_like_pro,
            )
        );



        $list = array_merge($request_client, $request_prospect);
        $data_liste = array();
        foreach ($list as $key => $value) {
            if (array_key_exists('client_id', $value)) {
                $array = array(
                    'type' => 'Propriétaire',
                    'id' => $value->client_id,
                    'nom' => $value->client_nom,
                    'prenom' => $value->client_prenom,
                    'adresse1' => $value->client_adresse1,
                    'adresse2' => $value->client_adresse2,
                    'adresse3' => $value->client_adresse3,
                    'phonemobile' => $value->client_phonemobile,
                    'phonefixe' => $value->client_phonefixe,
                    'cp' => $value->client_cp,
                    'ville' => $value->client_ville,
                    'pays' => $value->client_pays,
                    'email' => $value->client_email,
                    'entreprise' => $value->client_entreprise,
                    'date_creation' => $value->client_date_creation,
                );
            } else {
                $array = array(
                    'type' => 'Prospect',
                    'id' => $value->prospect_id,
                    'nom' => $value->prospect_nom,
                    'prenom' => $value->prospect_prenom,
                    'adresse1' => $value->prospect_adresse1,
                    'adresse2' => $value->prospect_adresse2,
                    'adresse3' => $value->prospect_adresse3,
                    'phonemobile' => $value->prospect_phonemobile,
                    'phonefixe' => $value->prospect_phonefixe,
                    'cp' => $value->prospect_cp,
                    'ville' => $value->prospect_ville,
                    'pays' => $value->prospect_pays,
                    'email' => $value->prospect_email,
                    'entreprise' => $value->prospect_entreprise,
                    'date_creation' => $value->prospect_date_creation,
                );
            }

            array_push($data_liste, $array);
        }
        $data["list"] = $data_liste;

        $this->renderComponant("Search/search/liste-global", $data);
    }

    public function verifyIndexation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = $_POST["data"];
                $this->load->model('IndiceLoyer_m');
                $this->load->model('IndiceValeur_m');

                $param = array(
                    'clause' => array(),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_indice_valeur_loyer.indloyer_id',
                    ),
                );

                $indices = $this->IndiceValeur_m->get($param);

                $data_retour = null;

                $date = new DateTime();
                $month = $date->format('m');
                $year = $date->format('Y');

                $roundTrimestre = ceil(intval($month) / 3);
                $trimestre = 'T' . $roundTrimestre;

                $findCurrentIndexation = false;

                foreach ($indices as $key => $indice) {
                    if ($indice->indval_annee == $year && $indice->indval_trimestre == $trimestre) {
                        $findCurrentIndexation = true;
                        break;
                    }
                }

                $data_retour["findCurrentIndexation"] = $findCurrentIndexation;

                $retour = retour(true, "success", $data_retour, array("message" => "Good"));

                echo json_encode($retour);
            }
        }
    }
}