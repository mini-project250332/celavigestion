<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Fiscal extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Fiscal";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/fiscal/fiscal.js"
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function getDocFiscal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Deficits_m');
                $this->load->model('DocumentFiscal_m');
                $data_post = $_POST['data'];
                $param = array(
                    'clause' => array("dossier_id" => $data_post['dossier_id']),
                );
                $params = array(
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_fiscal_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_fiscal.util_id')
                );
                $data['deficits_info'] = $this->Deficits_m->get($param);
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['document'] = $this->DocumentFiscal_m->get($params);
                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/list-document", $data);
            }
        }
    }

    public function getModalInitialise()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-modal-initialise", $data);
            }
        }
    }

    public function getTableDeficit()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Deficits_m');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                $deficits = $this->Deficits_m->get_data_deficite($data_post['dossier_id']);

                $liste_deficit = array();
                $total_deficit = 0;
                $total_benefice_impute = 0;
                $total_repor = 0;

                foreach ($deficits as $key => $value) {
                    $deficit_utilise =  explode(",", $value->deficit_utilise);
                    $benefices_annee =  explode(",", $value->benefices_annee);
                    $total_deficit += $value->deficite_anterieur;
                    $total_benefice_impute += array_sum($deficit_utilise);

                    $combine = array_combine($benefices_annee, $deficit_utilise);

                    $data_deficit = array(
                        'deficits_annee' => $value->deficits_annee,
                        'deficite_anterieur' => $value->deficite_anterieur,
                        'benefice_impute' => $value->deficit_utilise ? $combine : 0,
                        'deficit_repor' => $value->deficit_utilise ? ($value->deficite_anterieur - array_sum($deficit_utilise)) : $value->deficite_anterieur,
                    );

                    array_push($liste_deficit, $data_deficit);
                }

                foreach ($liste_deficit as $key => $valeur) {
                    $total_repor += $valeur['deficit_repor'];
                }

                $data['deficits'] = $liste_deficit;
                $data['total_deficit'] = $total_deficit;
                $data['total_benefice_impute'] = $total_benefice_impute;
                $data['total_repor'] = $total_repor;
                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/table-deficites", $data);
            }
        }
    }

    public function getTableBenefice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Benefices_m');
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                $benefice = $this->Benefices_m->get_data_benefices($data_post['dossier_id']);

                $liste_benfice = array();

                foreach ($benefice as $key => $value) {
                    $deficit_utilise =  explode(",", $value->deficit_utilise);
                    $deficits_annee =  explode(",", $value->deficits_annee);

                    $combine = array_combine($deficits_annee, $deficit_utilise);

                    $data_benefice = array(
                        'id_benefices' => $value->id_benefices,
                        'benefices_annee' => $value->benefices_annee,
                        'benefices' => $value->benefices,
                        'deficit_utilise' => $value->deficit_utilise ? $combine : 0,
                        'benefice_amor' => $value->deficit_utilise ? ($value->benefices - array_sum($deficit_utilise)) : $value->benefices,
                    );

                    array_push($liste_benfice, $data_benefice);
                }

                $data['benefices'] = $liste_benfice;
                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/table-benefices", $data);
            }
        }
    }

    public function getTable_2042()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];

                $this->renderComponant("Clients/clients/dossier/fiscal/2042/table-2042", $data);
            }
        }
    }

    public function initialiseFiscal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Deficits_m');

                $data_post = $_POST['data'];

                $donnee = array();
                $donnee['deficite_anterieur'] = 0;
                $donnee['dossier_id'] = $data_post['dossier_id'];
                $date_now = date('Y');

                for ($i = $data_post['deficits_annee']; $i <= intval($date_now); $i++) {
                    $donnee['deficits_annee'] = $i;
                    $this->Deficits_m->save_data($donnee);
                }

                echo json_encode($data_post);
            }
        }
    }

    public function getModalDeficit()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Deficits_m');

                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['deficits'] = $this->Deficits_m->get_data_deficite_update($data_post['dossier_id']);

                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-update-deficit", $data);
            }
        }
    }

    public function update_Deficit()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Deficits_m');
            $this->load->model('BeneficeDeficits_m');

            $data_post = $_POST['data'];

            foreach ($data_post['data_save'] as $key => $value) {
                $data['deficite_anterieur'] = $value['deficite_anterieur'];
                $clause = array(
                    'deficits_annee' => $value['deficits_annee'], 'dossier_id' => $data_post['dossier_id'],
                );
                $this->Deficits_m->save_data($data, $clause);
            }

            $deficits = $this->Deficits_m->get(array(
                'clause' => array('dossier_id' => $data_post['dossier_id']),
                'order_by_columns' => "deficits_annee  DESC",
                'limit' => 10
            ));

            foreach ($deficits as $key => $value) {
                $clause_deficite = array('id_deficits' => $value->id_deficits);
                $this->BeneficeDeficits_m->delete_data($clause_deficite);
            }

            echo json_encode($data_post);

            $this->autoIncrementDeficiteBenefice($data_post['dossier_id']);
        }
    }

    public function autoIncrementDeficiteBenefice($dossier_id)
    {
        $this->load->model('Benefices_m');
        $this->load->model('Deficits_m');
        $this->load->model('BeneficeDeficits_m');
        $benefices = $this->Benefices_m->get(
            array(
                'clause' => array('dossier_id ' => $dossier_id),
                'order_by_columns' => "benefices_annee  ASC",
            )
        );

        foreach ($benefices as $key => $value) {
            $liste_deficit = $this->getListeDeficit($dossier_id);
            $benefice_reste = '';
            foreach ($liste_deficit as $key_def => $value_def) {
                if ($value_def['deficit_repor'] != 0) {
                    if ($value->benefices >= $value_def['deficit_repor'] && $benefice_reste === '') {
                        $benefice_reste = floatval($value->benefices - $value_def['deficit_repor']);
                        $deficit_utilise = floatval($value->benefices -  $benefice_reste);
                        // var_dump('if ben > repor and reste vide', $deficit_utilise, $benefice_reste);
                        $data['id_benefices'] = $value->id_benefices;
                        $data['id_deficits'] = $value_def['id_deficits'];
                        $data['deficit_utilise'] = $deficit_utilise;
                        if ($deficit_utilise != 0) {
                            $this->BeneficeDeficits_m->save_data($data);
                        }
                    } elseif ($value->benefices < $value_def['deficit_repor'] && $benefice_reste === '') {
                        $deficit_utilise = floatval($value->benefices);
                        $benefice_reste =  0;
                        // var_dump('if ben < repor and reste vide', $deficit_utilise, $benefice_reste);
                        $data['id_benefices'] = $value->id_benefices;
                        $data['id_deficits'] = $value_def['id_deficits'];
                        $data['deficit_utilise'] = $deficit_utilise;
                        if ($deficit_utilise != 0) {
                            $this->BeneficeDeficits_m->save_data($data);
                        }
                    } elseif ($value_def['deficit_repor'] != 0 && $benefice_reste !== 0 && $benefice_reste !== '') {
                        if ($benefice_reste < $value_def['deficit_repor']) {
                            $deficit_utilise = floatval($benefice_reste);
                            $benefice_reste =  0;
                            // var_dump('else if > reste', $deficit_utilise, $benefice_reste);
                            $data['id_benefices'] = $value->id_benefices;
                            $data['id_deficits'] = $value_def['id_deficits'];
                            $data['deficit_utilise'] = $deficit_utilise;
                            if ($deficit_utilise != 0) {
                                $this->BeneficeDeficits_m->save_data($data);
                            }
                        } elseif ($benefice_reste >= $value_def['deficit_repor']) {
                            $benefice_reste = floatval($benefice_reste - $value_def['deficit_repor']);
                            $deficit_utilise = floatval($value_def['deficit_repor']);
                            // var_dump('else if < reste', $deficit_utilise, $benefice_reste);
                            $data['id_benefices'] = $value->id_benefices;
                            $data['id_deficits'] = $value_def['id_deficits'];
                            $data['deficit_utilise'] = $deficit_utilise;
                            if ($deficit_utilise != 0) {
                                $this->BeneficeDeficits_m->save_data($data);
                            }
                        }
                    } else {
                        // var_dump('tsy hay', $benefice_reste);
                    }
                }
            }
        }
    }

    public function getListeDeficit($dossier_id)
    {
        $this->load->model('Deficits_m');
        $deficits = $this->Deficits_m->get_data_deficite($dossier_id);

        $liste_deficit = array();
        foreach ($deficits as $key => $value) {
            $deficit_utilise =  explode(",", $value->deficit_utilise);
            $benefices_annee =  explode(",", $value->benefices_annee);

            $combine = array_combine($benefices_annee, $deficit_utilise);

            $data_deficit = array(
                'id_deficits' => $value->id_deficits,
                'deficits_annee' => $value->deficits_annee,
                'deficite_anterieur' => $value->deficite_anterieur,
                'benefice_impute' => $value->deficit_utilise ? $combine : 0,
                'deficit_repor' => $value->deficit_utilise ? ($value->deficite_anterieur - array_sum($deficit_utilise)) : $value->deficite_anterieur,
            );

            array_push($liste_deficit, $data_deficit);
        }

        return $liste_deficit;
    }

    public function getModaladdBenefice()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Benefices_m');
            $this->load->model('Deficits_m');

            $data_post = $_POST['data'];
            $data['dossier_id'] = $data_post['dossier_id'];

            $param = array(
                'clause' => array('dossier_id' => $data_post['dossier_id']),
                'order_by_columns' => 'deficits_annee',
            );

            $check_def = $this->Deficits_m->get($param);
            $date_liste_deficite = array();

            foreach ($check_def as $key => $value) {
                if ($value->deficite_anterieur == 0) {
                    array_push($date_liste_deficite, $value->deficits_annee);
                }
            }

            $date_courant_plus1 = date('Y') + 1;
            array_push($date_liste_deficite, $date_courant_plus1);
            $date = array();
            foreach ($date_liste_deficite as $cle => $valeur) {
                $check_benefice = $this->Benefices_m->get(
                    array(
                        'clause' => array('dossier_id' => $data_post['dossier_id'], 'benefices_annee' => (string)$valeur),
                        'method' => "row"
                    )
                );
                if (empty($check_benefice)) {
                    array_push($date, $valeur);
                }
            }

            $last_date_data = $this->Benefices_m->get(
                array(
                    'clause' => array('dossier_id' => $data_post['dossier_id']),
                    'order_by_columns' => 'benefices_annee DESC',
                    'method' => "row"
                )
            );

            if (!empty($last_date_data)) {
                foreach ($date as $cle => $val) {
                    if ($val < $last_date_data->benefices_annee) {
                        unset($date[$cle]);
                    }
                }
            }

            $data['date_liste'] = $date;

            $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-update-benefice", $data);
        }
    }

    public function addBenefice()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Benefices_m');

            $data_post = $_POST['data'];

            $this->Benefices_m->save_data($data_post);

            echo json_encode($data_post);

            $this->relationDeficiteBenefice($data_post['dossier_id']);
        }
    }

    public function relationDeficiteBenefice($dossier_id)
    {
        $this->load->model('Benefices_m');
        $this->load->model('Deficits_m');
        $this->load->model('BeneficeDeficits_m');
        $benefices = $this->Benefices_m->get(
            array(
                'clause' => array('dossier_id ' => $dossier_id),
                'order_by_columns' => "benefices_annee  DESC",
                'limit' => 1,
                'method' => 'row'
            )
        );
        $deficits = $this->Deficits_m->get_data_deficite($dossier_id);

        $liste_deficit = array();
        foreach ($deficits as $key => $value) {
            $deficit_utilise =  explode(",", $value->deficit_utilise);
            $benefices_annee =  explode(",", $value->benefices_annee);

            $combine = array_combine($benefices_annee, $deficit_utilise);

            $data_deficit = array(
                'id_deficits' => $value->id_deficits,
                'deficits_annee' => $value->deficits_annee,
                'deficite_anterieur' => $value->deficite_anterieur,
                'benefice_impute' => $value->deficit_utilise ? $combine : 0,
                'deficit_repor' => $value->deficit_utilise ? ($value->deficite_anterieur - array_sum($deficit_utilise)) : $value->deficite_anterieur,
            );

            array_push($liste_deficit, $data_deficit);
        }

        $benefice_reste = '';
        foreach ($liste_deficit as $key_def => $value_def) {
            if ($value_def['deficit_repor'] != 0) {
                if ($benefices->benefices >= $value_def['deficit_repor'] && $benefice_reste === '') {
                    $benefice_reste = floatval($benefices->benefices - $value_def['deficit_repor']);
                    $deficit_utilise = floatval($benefices->benefices -  $benefice_reste);
                    // var_dump('if ben > repor and reste vide', $deficit_utilise, $benefice_reste);
                    $data['id_benefices'] = $benefices->id_benefices;
                    $data['id_deficits'] = $value_def['id_deficits'];
                    $data['deficit_utilise'] = $deficit_utilise;
                    if ($deficit_utilise != 0) {
                        $this->BeneficeDeficits_m->save_data($data);
                    }
                } elseif ($benefices->benefices < $value_def['deficit_repor'] && $benefice_reste === '') {
                    $deficit_utilise = floatval($benefices->benefices);
                    $benefice_reste =  0;
                    // var_dump('if ben < repor and reste vide', $deficit_utilise, $benefice_reste);
                    $data['id_benefices'] = $benefices->id_benefices;
                    $data['id_deficits'] = $value_def['id_deficits'];
                    $data['deficit_utilise'] = $deficit_utilise;
                    if ($deficit_utilise != 0) {
                        $this->BeneficeDeficits_m->save_data($data);
                    }
                } elseif ($value_def['deficit_repor'] != 0 && $benefice_reste !== 0 && $benefice_reste !== '') {
                    if ($benefice_reste < $value_def['deficit_repor']) {
                        $deficit_utilise = floatval($benefice_reste);
                        $benefice_reste =  0;
                        // var_dump('else if > reste', $deficit_utilise, $benefice_reste);
                        $data['id_benefices'] = $benefices->id_benefices;
                        $data['id_deficits'] = $value_def['id_deficits'];
                        $data['deficit_utilise'] = $deficit_utilise;
                        if ($deficit_utilise != 0) {
                            $this->BeneficeDeficits_m->save_data($data);
                        }
                    } elseif ($benefice_reste >= $value_def['deficit_repor']) {
                        $benefice_reste = floatval($benefice_reste - $value_def['deficit_repor']);
                        $deficit_utilise = floatval($value_def['deficit_repor']);
                        // var_dump('else if < reste', $deficit_utilise, $benefice_reste);
                        $data['id_benefices'] = $benefices->id_benefices;
                        $data['id_deficits'] = $value_def['id_deficits'];
                        $data['deficit_utilise'] = $deficit_utilise;
                        if ($deficit_utilise != 0) {
                            $this->BeneficeDeficits_m->save_data($data);
                        }
                    }
                } else {
                    // var_dump('tsy hay', $benefice_reste);
                }
            }
        }
    }

    public function formUploadDocFiscal()
    {
        $data_post = $_POST['data'];
        $data['dossier_id'] = $data_post['dossier_id'];

        $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-upload-fiscal", $data);
    }

    public function uploadfilFiscal()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $dossier_id = $_POST['dossier_id_fiscal'];

        // Chemin du dossier
        $targetDir = "../documents/clients/dossier/Fiscal/$dossier_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_fiscal_nom' => $original_name,
                'doc_fiscal_creation' => date('Y-m-d H:i:s'),
                'doc_fiscal_path' => str_replace('\\', '/', $filePath),
                'doc_fiscal_etat' => 1,
                'util_id ' => $_SESSION['session_utilisateur']['util_id'],
                'dossier_id' => $dossier_id,
            );

            $this->load->model('DocumentFiscal_m');
            $this->DocumentFiscal_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getpathDocFiscal()
    {
        $this->load->model('DocumentFiscal_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_fiscal_id', 'doc_fiscal_path'),
                    'clause'  => array('doc_fiscal_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentFiscal_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function updateDocFiscalTraitement()
    {
        $this->load->model('DocumentFiscal_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause'  => array(
                        'doc_fiscal_id' => $data_post['doc_fiscal_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentFiscal_m->get($params);
                if ($request->doc_fiscal_traite == 1) {
                    $data['doc_fiscal_traite'] = 0;
                    $data['doc_fiscal_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_fiscal_id' => $data_post['doc_fiscal_id']);
                    $this->DocumentFiscal_m->save_data($data, $clause);
                } else {
                    $data['doc_fiscal_traite'] = 1;
                    $data['doc_fiscal_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_fiscal_id' => $data_post['doc_fiscal_id']);
                    $this->DocumentFiscal_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function modalupdateDocFiscal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['dossier_id'] = $data_post['dossier_id'];
                $this->load->model('DocumentFiscal_m');
                $data['document'] = $this->DocumentFiscal_m->get(
                    array('clause' => array("doc_fiscal_id" => $data_post['doc_fiscal_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-updatefic-fiscal", $data);
        }
    }

    public function UpdateDocumentFiscal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentFiscal_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array(
                        "dossier_id" => $data['dossier_id'],
                    );
                    $doc_fiscal_id = $data['doc_fiscal_id'];
                    unset($data['doc_fiscal_id']);

                    $clause = array("doc_fiscal_id" => $doc_fiscal_id);
                    $request = $this->DocumentFiscal_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function deleteDocFiscal()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentFiscal_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_fiscal_id' => $data['doc_fiscal_id']),
                    'columns' => array('doc_fiscal_id'),
                    'method' => "row",
                );

                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_fiscal_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_fiscal_id' => $data['doc_fiscal_id']);
                    $data_update = array('doc_fiscal_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_fiscal_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function deleteBenefice()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la suppression…",
            );

            $this->load->model('Benefices_m');
            $this->load->model('BeneficeDeficits_m');

            $data = decrypt_service($_POST["data"]);
            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'id' => $data['id_benefices'],
                        'dossier_id' => $data['dossier_id'],
                        'title' => "Confirmation de la suppression",
                        'text' => "Voulez-vous vraiment supprimer ce bénéfice ?",
                        'btnConfirm' => "Supprimer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $clause = array('id_benefices' => $data['id_benefices']);
                $request = $this->BeneficeDeficits_m->delete_data($clause);
                $request = $this->Benefices_m->delete_data($clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id_benefices' => $data['id_benefices'],
                            'dossier_id' => $data['dossier_id'],
                        ),
                        'message' => "Message supprimé",
                    );
                }
            }
            $retour['dossier_id'] = $data['dossier_id'];
            echo json_encode($retour);
        }
    }

    public function modalAlertBenefice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-alert-benefice");
            }
        }
    }

    public function alertDeficit()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Clients/clients/dossier/fiscal/deficits/form-alert-deficit");
            }
        }
    }
    /************TVA*************/

    public function getDoctva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $data_post = $_POST['data'];
                $data['dossier'] = $this->Dossier_m->get(
                    array(
                        'columns' => array('dossier_id', 'acompte_tva'),
                        'clause' => array(
                            'dossier_id' => $data_post['dossier_id']
                        ),
                        'method' => 'row'
                    )
                );
                $data['dossier_id'] = $data_post['dossier_id'];
                $this->renderComponant("Clients/clients/dossier/fiscal/tva/list-document", $data);
            }
        }
    }

    public function listeDocTva()
    {
        $this->load->model('DocumentTva_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $data = array();
                $params = array(
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_tva_annee' => $data_post['doc_tva_annee'],
                        'doc_tva_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_tva.util_id')
                );

                $data['dossier_id'] = $data_post['dossier_id'];
                $data['document'] = $this->DocumentTva_m->get($params);

                $this->renderComponant("Clients/clients/dossier/fiscal/tva/document", $data);
            }
        }
    }

    public function FormAddDocTva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentTva_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['action'] = $data_post['action'];

                if ($data_post['action'] == "edit") {
                    $params = array(
                        'clause' => array('doc_tva_id ' => $data_post['doc_tva_id']),
                        'method' => "row"
                    );
                    $data['tva'] = $this->DocumentTva_m->get($params);
                }
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['doc_tva_annee']  = $data_post['doc_tva_annee'];
            }

            $this->renderComponant("Clients/clients/dossier/fiscal/tva/form-tva", $data);
        }
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function AddDocTva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentTva_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("dossier_id" => $data['dossier_id']);
                $dossier_id = $data['dossier_id'];
                $action = $data['action'];
                $data['doc_tva_date_creation'] = Carbon::now()->toDateTimeString();
                $data['doc_tva_path'] = NULL;
                $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];

                unset($data['action']);
                $clause = null;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/client/dossiers/fiscal/tva/$dossier_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    $counter = 1;
                    while (file_exists($path_filename_ext)) {
                        $filename = $filename . '_' . $counter;
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $counter++;
                    }
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['doc_tva_path'] = $path_filename_ext;
                }

                if ($action == "edit") {
                    $clause = array("doc_tva_id" => $data['doc_tva_id']);
                    $data_retour['doc_tva_id'] = $data['doc_tva_id'];
                    if ($data['doc_tva_path'] == NULL) {
                        unset($data['doc_tva_path']);
                    }
                    unset($data['doc_tva_id']);
                    unset($data['file']);
                } else {
                    unset($data['doc_tva_nom']);
                }

                $request = $this->DocumentTva_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Document ajouté avec succès" : "Modification réussie"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function removefiletva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentTva_m');
                $data_post = $_POST['data'];
                $param = array('doc_tva_id' => $data_post['doc_tva_id']);
                $data_tva = array(
                    'cin' => NULL,
                );
                $request = $this->DocumentTva_m->save_data($data_tva, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function getpathDoctva()
    {

        $this->load->model('DocumentTva_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_tva_id', 'doc_tva_path'),
                    'clause' => array('doc_tva_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentTva_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function removeDocTva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentTva_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_tva_id' => $data['doc_tva_id']),
                    'columns' => array('doc_tva_id'),
                    'method' => "row",
                );
                $request_get = $this->DocumentTva_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_tva_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_tva_id' => $data['doc_tva_id']);
                    $data_update = array('doc_tva_etat' => 0);
                    $request = $this->DocumentTva_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_tva_id']),
                            'message' => "Document supprimé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function AddAcompteTva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("dossier_id" => $data['dossier_id']);

                    $clause = array("dossier_id" => $data['dossier_id']);
                    unset($data['doc_tva_annee']);
                    $request = $this->Dossier_m->save_data($data, $clause);

                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Ajout réussi"));
                    }
                    echo json_encode($retour);
                }
            }
        }
    }

    /**********2031***********/

    public function getDoc2031()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $data['dossier_id'] = $data_post['dossier_id'];
                $this->renderComponant("Clients/clients/dossier/fiscal/2031/list-document", $data);
            }
        }
    }

    public function listeDoc2031()
    {
        $this->load->model('Document2031_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $data = array();
                $params = array(
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_2031_annee' => $data_post['doc_2031_annee'],
                        'doc_2031_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_2031.util_id')
                );

                $data['dossier_id'] = $data_post['dossier_id'];
                $data['document'] = $this->Document2031_m->get($params);

                $this->renderComponant("Clients/clients/dossier/fiscal/2031/document", $data);
            }
        }
    }

    public function formUploadDoc2031()
    {
        $data_post = $_POST['data'];
        $data['dossier_id'] = $data_post['dossier_id'];
        $data['doc_2031_annee'] = $data_post['doc_2031_annee'];

        $this->renderComponant("Clients/clients/dossier/fiscal/2031/form_upload_2031", $data);
    }

    public function uploadfile2031()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $dossier_id = $_POST['dossier_id'];
        $doc_2031_annee = $_POST['doc_2031_annee'];

        // Chemin du dossier
        $targetDir = "../documents/client/dossiers/fiscal/2031/$dossier_id/";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_2031_nom' => $original_name,
                'doc_2031_date_creation' => date('Y-m-d H:i:s'),
                'doc_2031_path' => str_replace('\\', '/', $filePath),
                'doc_2031_etat' => 1,
                ' doc_2031_annee' => $doc_2031_annee,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'dossier_id' => $dossier_id
            );

            $this->load->model('Document2031_m', 'document');
            $this->document->save_data($data);
            echo json_encode($data);
        }
    }

    public function FormUpdateDoc2031()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Document2031_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['action'] = $data_post['action'];

                $params = array(
                    'clause' => array('doc_2031_id ' => $data_post['doc_2031_id']),
                    'method' => "row"
                );
                $data['document'] = $this->Document2031_m->get($params);
                $data['dossier_id'] = $data_post['dossier_id'];
                $data['doc_2031_id'] = $data_post['doc_2031_id'];
            }

            $this->renderComponant("Clients/clients/dossier/fiscal/2031/form-update-2031", $data);
        }
    }

    public function Updatedoc2031()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Document2031_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("dossier_id" => $data['dossier_id']);
                    $doc_2031_id = $data['doc_2031_id'];
                    unset($data['doc_2031_id']);
                    $clause = array("doc_2031_id" => $doc_2031_id);
                    $request = $this->Document2031_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDoc2031()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Document2031_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_2031_id' => $data['doc_2031_id']),
                    'columns' => array('doc_2031_id'),
                    'method' => "row",
                );
                $request_get = $this->Document2031_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_2031_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_2031_id' => $data['doc_2031_id']);
                    $data_update = array('doc_2031_etat' => 0);
                    $request = $this->Document2031_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_2031_id']),
                            'message' => "Document supprimé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getpathDoc2031()
    {
        $this->load->model('Document2031_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_2031_id', 'doc_2031_path'),
                    'clause' => array('doc_2031_id' => $data_post['id_doc'])
                );

                $request = $this->Document2031_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getDocConfidentiel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentConfidentiel_m');

                $data['dossier_id'] = $_POST['data']['dossier_id'];
                $data['document'] = $this->DocumentConfidentiel_m->get(array(
                    'clause' => array('dossier_id' => $_POST['data']['dossier_id'], 'doc_etat' => 1, 'comment_etat' => 0),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_confidentiel.util_id')
                ));

                $data['commentaireConfidentiel'] = $this->DocumentConfidentiel_m->get(array(
                    'clause' => array('dossier_id' => $_POST['data']['dossier_id'], 'doc_etat' => 1, 'comment_etat' => 1),
                    'method' => 'row'
                ));

                $this->renderComponant("Clients/clients/dossier/fiscal/doc_confidentiel/list_document", $data);
            }
        }
    }

    public function formUploadDocFiscalConfidentiels()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST['data'];

                $data['dossier_id'] = $data_post['dossier_id'];

                $this->renderComponant("Clients/clients/dossier/fiscal/doc_confidentiel/form-upload", $data);
            }
        }
    }

    public function uploadfileConfidentiel()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $dossier_id = $_POST['dossier_id_confidentiel'];

        // Chemin du dossier
        $targetDir = "../documents/clients/dossier/Fiscal/doc_confidentiel/$dossier_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_nom' => $original_name,
                'doc_date_creation' => date('Y-m-d H:i:s'),
                'doc_path' => str_replace('\\', '/', $filePath),
                'doc_etat' => 1,
                'util_id ' => $_SESSION['session_utilisateur']['util_id'],
                'dossier_id' => $dossier_id,
            );

            $this->load->model('DocumentConfidentiel_m');
            $this->DocumentConfidentiel_m->save_data($data);

            echo json_encode($data);
        }
    }

    public function deleteDocConfidentiel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentConfidentiel_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_id' => $data['doc_id']),
                    'columns' => array('doc_id'),
                    'method' => "row",
                );

                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_id' => $data['doc_id']);
                    $data_update = array('doc_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modalupdateDocConfidentiel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['dossier_id'] = $data_post['dossier_id'];
                $this->load->model('DocumentConfidentiel_m');
                $data['document'] = $this->DocumentConfidentiel_m->get(
                    array('clause' => array("doc_id" => $data_post['doc_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Clients/clients/dossier/fiscal/doc_confidentiel/form-updatefichier", $data);
        }
    }

    public function UpdateDocumentConfidentiel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentConfidentiel_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array(
                        "dossier_id" => $data['dossier_id'],
                    );
                    $doc_id = $data['doc_id'];
                    unset($data['doc_id']);

                    $clause = array("doc_id" => $doc_id);
                    $request = $this->DocumentConfidentiel_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getpathDocConfidentiel()
    {
        $this->load->model('DocumentConfidentiel_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_id', 'doc_path'),
                    'clause'  => array('doc_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentConfidentiel_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function addModifcommentaireDocConfidentiel()
    {
        $this->load->model('DocumentConfidentiel_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $retour = retour(null, null, null, array("message" => "Erreur lors de la modification."));

                $check_commentarie = $this->DocumentConfidentiel_m->get(
                    array(
                        'clause' => array('dossier_id' => $data_post['dossier_id'], 'comment_etat' => 1),
                        'method' => 'row'
                    )
                );

                $data_retour = $data_save = array(
                    'doc_etat' => 1,
                    'comment_etat' => 1,
                    'commentaire' => $data_post['commentaire'],
                    'dossier_id' => $data_post['dossier_id'],
                    'util_id' => $_SESSION['session_utilisateur']['util_id']
                );

                if (!empty($check_commentarie)) {
                    $clause = array("doc_id" => $check_commentarie->doc_id);
                    $request = $this->DocumentConfidentiel_m->save_data($data_save, $clause);
                } else {
                    $request = $this->DocumentConfidentiel_m->save_data($data_save);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement reussie."));
                }

                echo json_encode($retour);
            }
        }
    }
}
