<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Historique extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Historique";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/historique/historique.js",
        );

        $this->_css_personnaliser = array(
            "css/historique/historique.css"
        );
    }

    public function index()
    {
    }


    public function AddHistorique()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Historique_m');
                $this->load->model('HistoriqueMail_m');
                $data_post = $_POST['data'];
                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout historique"));

                $data_retour['request'] = $data_post;

                $data['util_id'] = intval($data_post['util_id']);
                $data['histo_date'] = date('Y-m-d H:i:s');
                $data['tpact_id'] = $data_post['tpact_id'] ? intval($data_post['tpact_id']) : null;
                $data['page_id'] = $data_post['page_id'] ? intval($data_post['page_id']) : null;
                $data['histo_action'] = $data_post['action'];
                $data['histo_reference_id'] = $data_post['histo_reference_id'] ? intval($data_post['histo_reference_id']) : null;

                $request = $this->Historique_m->save_data($data);
                $insert_id = $this->db->insert_id();

                if ($data_post['surplus']) {
                    $data_mail["hist_mail_emetteur"] = $data_post['surplus']['emetteur'];
                    $data_mail["hist_mail_destinataire"] = $data_post['surplus']['destinataire'];
                    $data_mail["hist_mail_destinataire_copie"] = $data_post['surplus']['destinataire_copie'];
                    $data_mail["hist_mail_objet"] = $data_post['surplus']['objet'];
                    $data_mail["hist_mail_message"] = $data_post['surplus']['message'];
                    $data_mail["hist_mail_fichiers_joints"] = $data_post['surplus']['fichiers_joints'];
                    $data_mail["histo_id"] = $insert_id;
                    $this->HistoriqueMail_m->save_data($data_mail);
                }


                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement historique réussi"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function GetHistorique()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Historique_m');
                $this->load->model('DocumentLoyer_m');
                $data_post = $_POST['data'];

                $mail_loyer = false;

                $clause = array(
                    'c_historique.page_id' => $data_post['page_id'],
                );

                $join = array(
                    'c_page' => 'c_page.page_id = c_historique.page_id',
                    'c_typeaction' => 'c_typeaction.tpact_id = c_historique.tpact_id',
                );

                if (isset($data_post['tpact_id']) && intval($data_post['tpact_id']) > 0) {
                    $clause['c_historique.tpact_id'] = $data_post['tpact_id'];
                }

                if (isset($data_post['histo_reference_id']) && intval($data_post['histo_reference_id']) > 0) {
                    $clause['c_historique.histo_reference_id'] = $data_post['histo_reference_id'];
                }

                if (isset($data_post['mail_loyer']) && $data_post['mail_loyer'] == 1) {
                    $mail_loyer = true;
                    $join['c_historique_mail'] = 'c_historique.histo_id = c_historique_mail.histo_id';
                    $join['c_facture_loyer'] = 'c_facture_loyer.fact_loyer_id = c_historique.histo_reference_id';
                }

                $params = array(
                    'clause' => $clause,
                    'columns' => array(),
                    'join' => $join,
                    'order_by_columns' => 'c_historique.histo_id DESC',
                );

                $request = $this->Historique_m->get($params);

                if (isset($data_post['histo_reference_id']) && $data_post['histo_reference_id'] == "") {
                    $request = array();
                }

                $data_retour['historiques'] = $request;
                $data_retour['containerId'] = $data_post['container_id'];
                $data_retour['request'] = $data_post;

                if ($mail_loyer) {

                    foreach ($data_retour['historiques'] as $keyHistory => $history) {
                        $array_fichier_joint = json_decode($history->hist_mail_fichiers_joints);
                        $fichiers_joint = array();
                        foreach ($array_fichier_joint as $doc_id) {
                            $paramDoc = array(
                                "clause" => array('c_document_loyer.doc_loyer_id' => $doc_id)
                            );
                            $doc = $this->DocumentLoyer_m->get($paramDoc);
                            array_push($fichiers_joint, $doc);
                        }
                        $data_retour['historiques'][$keyHistory]->hist_mail_fichiers_joints = $fichiers_joint;
                    }

                    $this->renderComponant("Clients/clients/lot/loyers/historique-mail", $data_retour);
                } else {
                    $retour = retour(true, "success", $data_retour, array("message" => "Liste des historiques"));
                    echo json_encode($retour);
                }
            }
        }
    }
}
