<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Registre des mandats</h5>
    </div>
    <div class="px-2">
        <button class="btn btn-primary" id="export_pdf_mandat"><i class="fas fa-file-pdf"></i> Exporter </button>
    </div>
</div>
<div class="contenair-list">
    <div class="row">
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Nom propriétaire/prospect</label>
                <input class="form-control mb-0" type="text" id="text_filtre_mandat" name="text_filtre_mandat">
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Etat mandat</label>
                <select class="form-select fs--1" id="etat_mandat_id" name="etat_mandat_id">
                    <option value="">Tous</option>
                    <?php foreach ($etat_mandat as $etat_mandats) : ?>
                        <option value="<?= $etat_mandats->etat_mandat_id; ?>">
                            <?= $etat_mandats->etat_mandat_libelle; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Numéro mandat</label>
                <input class="form-control mb-0" type="text" name="numero_mandat" id="numero_mandat">
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Nom programme</label>
                <input class="form-control mb-0" type="text" name="nom_programme" id="nom_programme">
            </div>
        </div>
    </div>

    <div id="data-list" class="col">

    </div>

</div>

<script>
    only_visuel_gestion();
    $(document).ready(function() {
        listMandat();
    });
</script>