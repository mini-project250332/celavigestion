<input type="hidden" value="<?php echo base_url('Clients'); ?>" id="client-open-tab-url" />
<input type="hidden" value="<?php echo base_url('Prospections'); ?>" id="prospect-open-tab-url" />
<div class="table-responsive">
    <table class="table table-sm table-striped fs--1 mb-0" id="table_mandat">
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Numéro du mandat</th>
                <th class="text-center" scope="col">Propriétaire / Prospect</th>
                <th class="text-center" scope="col">Numéro dossier</th>
                <th class="text-center" scope="col">Nom lot</th>
                <th class="text-center" scope="col">Adresses lot</th>
                <th class="text-center" scope="col">Date de création</th>
                <th class="text-center" scope="col">Date de signature</th>
                <th class="text-center" scope="col">Date de fin</th>
                <th class="text-center" scope="col">Type de mandat</th>
                <th class="text-center" scope="col">Etat</th>
                <th class="text-end" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mandat as $mandats) : ?>
                <?php
                $mandat_type = $mandats->mandat_type_id;
                if ($mandat_type == 1) {
                    $value_mandat = $mandats->data_client->client_nom . ' ' . $mandats->data_client->client_prenom;
                    $value_lot_id = $mandats->data_lot_client->cliLot_id;
                    $value_lot = $mandats->data_lot_client->progm_nom;
                    $value_adresse_lot = $mandats->data_lot_client->progm_adresse1 . ' ' . $mandats->data_lot_client->progm_cp . ' ' . $mandats->data_lot_client->progm_ville . ' ' . $mandats->data_lot_client->progm_pays;
                    $value_dossier = $mandats->data_lot_client->dossier_id;
                    $value_id = $mandats->data_client->client_id;
                } else {
                    $value_mandat = $mandats->data_prospect->prospect_nom . ' ' . $mandats->data_prospect->prospect_prenom;
                    $value_lot_id = $mandats->data_lot_prospect->lot_id;
                    $value_lot = $mandats->data_lot_prospect->progm_nom;
                    $value_adresse_lot = $mandats->data_lot_prospect->progm_adresse1 . ' ' . $mandats->data_lot_prospect->progm_cp . ' ' . $mandats->data_lot_prospect->progm_ville . ' ' . $mandats->data_lot_prospect->progm_pays;
                    $value_id = $mandats->data_prospect->prospect_id;
                }
                ?>
                <tr>
                    <td><?= str_pad($mandats->mandat_id, 4, '0', STR_PAD_LEFT) ?></td>
                    <td class="text-center"><?= $value_mandat ?></td>
                    <td class="text-center"><?= str_pad($value_dossier, 4, '0', STR_PAD_LEFT) ?></td>
                    <td class="text-center"><?= $value_lot ?></td>
                    <td class="text-center"><?= $value_adresse_lot ?></td>
                    <td class="text-center"><?= date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_creation))) ?></td>
                    <td class="text-center"><?= $mandats->mandat_date_signature != NULL ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_signature))) : "-" ?></td>
                    <td class="text-center"><?= $mandats->mandat_date_fin != NULL ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_fin))) : "-" ?></td>
                    <td class="text-center"><?= $mandats->mandat_type_libelle ?></td>
                    <td class="text-center"><?= $mandats->etat_mandat_libelle ?></td>
                    <td>
                        <div class="d-flex justify-content-end ">
                            <button class="btn btn-sm btn-outline-danger icon-btn p-0 m-1" onclick="forceDownload('<?= base_url() . $mandats->mandat_path ?>')" data-url="<?= base_url($mandats->mandat_path) ?>">
                                <span class="far fa-file-pdf fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-outline-primary icon-btn p-0 m-1 ficheMandat only_visuel_gestion" type="button" data-type="<?= $mandat_type ?>" data-proprietaire="<?= $value_id ?>" data-id="<?= $value_lot_id ?>">
                                <span class="fas fa-play fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-outline-primary btnFicheMandatNewTab" style="bottom :0px !important" data-type="<?= $mandat_type ?>" data-proprietaire="<?= $value_id ?>" data-id="<?= $value_lot_id ?>" type="button">
                                <span class="fas fa-plus open_tab_mandat"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<script>
    only_visuel_gestion();
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const client_tab = urlParams.get('_open@client_tab');
    const prospect_tab = urlParams.get('_open@prospect_tab');
    if (client_tab) {
        $(`#lot_${client_id}`).trigger("click");
    }
    if (prospect_tab) {
        $(`#lot_${client_id}`).trigger("click");
    }

    const searchName = document.getElementById('text_filtre_mandat');
    const searchProgramme = document.getElementById('nom_programme');

    const table = document.getElementById('tableMandat');

    /******Reherche prospect et propriétaire*******/
    searchName.addEventListener('keyup', function(event) {
        const searchValue = event.target.value.toLowerCase();
        const rows = table.querySelectorAll('tbody tr');

        rows.forEach(row => {
            const name = row.querySelector('td:nth-child(2)').textContent.toLowerCase();
            if (name.includes(searchValue)) {
                row.style.display = '';
            } else {
                row.style.display = 'none';
            }
        });
    });

    /******Reherche par programme*******/

    searchProgramme.addEventListener('keyup', function(event) {
        const searchValue = event.target.value.toLowerCase();
        const rows = table.querySelectorAll('tbody tr');

        rows.forEach(row => {
            const name = row.querySelector('td:nth-child(4)').textContent.toLowerCase();
            if (name.includes(searchValue)) {
                row.style.display = '';
            } else {
                row.style.display = 'none';
            }
        });
    });
</script>