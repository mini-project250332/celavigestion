<style>
    table {
        width: 150%;
        border-collapse: collapse;
    }

    th,
    td {
        border: 1px solid black;
        /* Set the border width and color */
        padding: 5px;
        text-align: center;
    }
</style>

<h2>Registre des mandats <?= date('d/m/Y') ?></h2>
<h5>REGISTRE DES MANDATS-GESTION IMMOBILIERE <br> Conforme au Décret N°72-678 du 20 juillet 1972
</h5>
<table id="table_mandat">
    <thead>
        <tr>
            <th scope="col">Numéro du mandat</th>
            <th scope="col">Propriétaire / Prospect</th>
            <th scope="col">Numéro dossier</th>
            <th scope="col">Nom lot</th>
            <th scope="col">Adresses lot</th>
            <th scope="col">Date de création</th>
            <th scope="col">Date de signature</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Type de mandat</th>
            <th scope="col">Etat</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mandat as $mandats) : ?>
            <?php
            $mandat_type = $mandats->mandat_type_id;
            if ($mandat_type == 1) {
                $value_mandat = $mandats->data_client->client_nom . ' ' . $mandats->data_client->client_prenom;
                $value_lot_id = $mandats->data_lot_client->cliLot_id;
                $value_lot = $mandats->data_lot_client->progm_nom;
                $value_adresse_lot = $mandats->data_lot_client->progm_adresse1 . ' ' . $mandats->data_lot_client->progm_cp . ' ' . $mandats->data_lot_client->progm_ville . ' ' . $mandats->data_lot_client->progm_pays;
                $value_dossier = $mandats->data_lot_client->dossier_id;
                $value_id = $mandats->data_client->client_id;
            } else {
                $value_mandat = $mandats->data_prospect->prospect_nom . ' ' . $mandats->data_prospect->prospect_prenom;
                $value_lot_id = $mandats->data_lot_prospect->lot_id;
                $value_lot = $mandats->data_lot_prospect->progm_nom;
                $value_adresse_lot = $mandats->data_lot_prospect->progm_adresse1 . ' ' . $mandats->data_lot_prospect->progm_cp . ' ' . $mandats->data_lot_prospect->progm_ville . ' ' . $mandats->data_lot_prospect->progm_pays;
                $value_id = $mandats->data_prospect->prospect_id;
            }
            ?>
            <tr>
                <td><?= str_pad($mandats->mandat_id, 4, '0', STR_PAD_LEFT) ?></td>
                <td><?= $value_mandat ?></td>
                <td><?= str_pad($value_dossier, 4, '0', STR_PAD_LEFT) ?></td>
                <td><?= $value_lot ?></td>
                <td><?= $value_adresse_lot ?></td>
                <td><?= date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_creation))) ?></td>
                <td><?= $mandats->mandat_date_signature != NULL ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_signature))) : "-" ?></td>
                <td><?= $mandats->mandat_date_fin != NULL ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_fin))) : "-" ?></td>
                <td><?= $mandats->mandat_type_libelle ?></td>
                <td><?= $mandats->etat_mandat_libelle ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>