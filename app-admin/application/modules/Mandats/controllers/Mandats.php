<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Mandats extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Mandats";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/mandats/mandats.js",
            "js/historique/historique.js",
            "js/saisietemps/saisietemps.js"
        );

        $this->_css_personnaliser = array(
            "css/lots/lots.css",
            "css/clients/clients.css",
            "css/select2.min.css",
            "css/historique/historique.css",
            "css/loyer/loyer.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "mandats";
        $this->_data['sous_module'] = "mandats/contenaire-mandat";

        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('EtatMandat_m');
                $params = array(
                    'columns' => array('etat_mandat_id', 'etat_mandat_libelle')
                );
                $data['etat_mandat'] = $this->EtatMandat_m->get($params);

                $this->renderComponant("Mandats/mandats/header-liste", $data);
            }
        }
    }

    public function listMandat()
    {
        $this->load->model('MandatNew_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $like = array();
                $or_like = array();
                $clause = array('mandat_etat' => 1);
                $data_post = $_POST['data'];

                if ($data_post['etat_mandat_id'] != '') {
                    $clause['c_etat_mandat.etat_mandat_id'] = $data_post['etat_mandat_id'];
                }

                if ($data_post['numero_mandat'] != '') {
                    $clause['c_mandat_new.mandat_id'] = $data_post['numero_mandat'];
                }

                $params = array(
                    'clause' => $clause,
                    'like' => $like,
                    'or_like' => $or_like,
                    'columns' => array(
                        'mandat_id as mandat_id', 'mandat_etat as mandat_etat', 'prospect_id', 'c_mandat_new.client_id', 'c_mandat_new.mandat_type_id', 'c_mandat_new.etat_mandat_id',
                        'etat_mandat_libelle', 'mandat_date_creation', 'mandat_date_signature', 'mandat_type_libelle', 'mandat_montant_ht', 'mandat_date_fin', 'c_mandat_new.cliLot_id',
                        'lot_id', 'mandat_path'
                    ),
                    'order_by_columns' => 'mandat_id ASC',
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_mandat_new.client_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_mandat_new.cliLot_id',
                        'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        'c_mandat_type' => 'c_mandat_type.mandat_type_id = c_mandat_new.mandat_type_id',
                    ),
                    'method' => "result",
                );

                $request = $this->MandatNew_m->get($params);
                $result = array();
                foreach ($request as $key => $value) {
                    if (intval($value->mandat_type_id) == 1) {
                        $request[$key]->data_client = $this->getData_client($value->client_id);
                        $request[$key]->data_lot_client = $this->getData_lotClient($value->cliLot_id);
                    } else {
                        $request[$key]->data_prospect = $this->getData_prospect($value->prospect_id);
                        $request[$key]->data_lot_prospect = $this->getData_lotProspect($value->lot_id);
                    }
                    $result[] = $request[$key];
                }

                $data['mandat'] = $result;

                $this->renderComponant("Mandats/mandats/liste-mandat", $data);
            }
        }
    }

    public function export_Mandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $this->load->helper("exportation");

                $clause = array('mandat_etat' => 1);

                $params = array(
                    'clause' => $clause,
                    'columns' => array(
                        'mandat_id as mandat_id', 'mandat_etat as mandat_etat', 'prospect_id', 'client_id', 'c_mandat_new.mandat_type_id', 'c_mandat_new.etat_mandat_id',
                        'etat_mandat_libelle', 'mandat_date_creation', 'mandat_date_signature', 'mandat_type_libelle', 'mandat_montant_ht', 'mandat_date_fin', 'cliLot_id',
                        'lot_id', 'mandat_path'
                    ),
                    'order_by_columns' => 'mandat_id ASC',
                    'join' => array(
                        'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        'c_mandat_type' => 'c_mandat_type.mandat_type_id = c_mandat_new.mandat_type_id',
                    ),
                    'method' => "result",
                );

                $request = $this->MandatNew_m->get($params);

                $result = array();
                foreach ($request as $key => $value) {
                    if (intval($value->mandat_type_id) == 1) {
                        $request[$key]->data_client = $this->getData_client($value->client_id);
                        $request[$key]->data_lot_client = $this->getData_lotClient($value->cliLot_id);
                    } else {
                        $request[$key]->data_prospect = $this->getData_prospect($value->prospect_id);
                        $request[$key]->data_lot_prospect = $this->getData_lotProspect($value->lot_id);
                    }
                    $result[] = $request[$key];
                }
                $view = "Mandats/mandats/liste-mandat_pdf";
                $data['mandat'] = $result;
                exportation_pdf_mandat($view, $data);
            }
        }
    }
}
