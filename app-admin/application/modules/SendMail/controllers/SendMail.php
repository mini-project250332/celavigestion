<?php
defined('BASEPATH') or exit('No direct script access allowed');


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class SendMail extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "SendMail";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/mail/mail.js",
            "js/historique/historique.js"
        );
        $this->_css_personnaliser = array();
    }

    public function EnvoiTom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('ClientLot_m', 'c_lot_client');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Taxe_m', 'c_taxe_fonciere_lot');
                $this->load->model('DocumentTaxe_m', 'c_document_lot_taxe');
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);
                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    ),
                    'join_orientation' => 'left',
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->c_lot_client->get($param);
                $param_taxe = array(
                    'clause' => array(
                        'cliLot_id  ' => $data_post['cliLot_id'],
                        'annee' => $data_post['annee']
                    ),
                    'method' => "row"
                );
                $taxe = $this->c_taxe_fonciere_lot->get($param_taxe);
                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 1
                    ),
                    'method' => "row"
                );
                $modele = $this->c_param_modelemail->get($param_modele);
                $param_document_taxe = array(
                    'clause' => array(
                        'annee ' => $data_post['annee'],
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_taxe_etat' => 1
                    ),
                );
                $data['document'] = '';
                $document_taxe = $this->c_document_lot_taxe->get($param_document_taxe);
                $data['document'] = $document_taxe;
                $object_mail = $modele->pmail_objet;
                $object_mail = str_replace('@annee', $taxe->annee, $object_mail);
                $object_mail = str_replace('@nom_client', $data_dest->client_nom, $object_mail);
                $object_mail = str_replace('@nom_programme', $data_dest->progm_nom, $object_mail);
                $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                if (!empty($taxe)) {
                    $mailMessage = str_replace('@montant_taxe', $taxe->taxe_montant, $mailMessage);
                    $mailMessage = str_replace('@annee', $taxe->annee, $mailMessage);
                    $mailMessage = str_replace('@montant_tom', $taxe->taxe_tom, $mailMessage);
                } else {
                    $mailMessage = str_replace('@montant_taxe', '', $mailMessage);
                    $mailMessage = str_replace('@annee', '', $mailMessage);
                    $mailMessage = str_replace('@montant_tom', '', $mailMessage);
                }
                $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee'] = $data_post['annee'];

                $this->renderComponant("Clients/clients/lot/taxes/formMail", $data);
            }
        }
    }

    public function EnvoiTomClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('ClientLot_m', 'c_lot_client');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Taxe_m', 'c_taxe_fonciere_lot');
                $this->load->model('DocumentTaxe_m', 'c_document_lot_taxe');
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                    ),
                    'join_orientation' => 'left',
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->c_lot_client->get($param);

                $param_taxe = array(
                    'clause' => array(
                        'cliLot_id  ' => $data_post['cliLot_id'],
                        'annee' => $data_post['annee']
                    ),
                    'method' => "row"
                );
                $taxe = $this->c_taxe_fonciere_lot->get($param_taxe);
                $pmail_id = $data_dest->client_pays == 'FRANCE' || $data_dest->client_pays == 'France' ? 12 : 14;

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' =>  $pmail_id
                    ),
                    'method' => "row"
                );
                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_taxe = array(
                    'clause' => array(
                        'annee ' => $data_post['annee'],
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_taxe_etat' => 1
                    ),
                );

                $data['document'] = '';
                // $document_taxe = $this->c_document_lot_taxe->get($param_document_taxe);
                // $data['document'] = $document_taxe;

                $object_mail = $modele->pmail_objet;
                $object_mail = str_replace('@annee', $taxe->annee, $object_mail);
                $object_mail = str_replace('@nom_client', $data_dest->client_nom, $object_mail);
                $object_mail = str_replace('@nom_programme', $data_dest->progm_nom, $object_mail);
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                if (!empty($taxe)) {
                    $mailMessage = str_replace('@montant_taxe', $taxe->taxe_montant, $mailMessage);
                    $mailMessage = str_replace('@annee', $taxe->annee, $mailMessage);
                    $mailMessage = str_replace('@montant_tom', $taxe->taxe_tom, $mailMessage);
                } else {
                    $mailMessage = str_replace('@montant_taxe', '', $mailMessage);
                    $mailMessage = str_replace('@annee', '', $mailMessage);
                    $mailMessage = str_replace('@montant_tom', '', $mailMessage);
                }
                $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee'] = $data_post['annee'];

                $this->renderComponant("Clients/clients/lot/taxes/formMailClient", $data);
            }
        }
    }

    public function declarerErreur()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('ClientLot_m', 'c_lot_client');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Taxe_m', 'c_taxe_fonciere_lot');
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);
                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    ),
                    'join_orientation' => 'left',
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->c_lot_client->get($param);
                $param_taxe = array(
                    'clause' => array(
                        'cliLot_id  ' => $data_post['cliLot_id'],
                        'annee' => $data_post['annee']
                    ),
                    'method' => "row"
                );
                $taxe = $this->c_taxe_fonciere_lot->get($param_taxe);
                $param_modele = array(
                    'clause' => array(
                        'pmail_id' => 3
                    ),
                    'method' => "row"
                );
                $modele = $this->c_param_modelemail->get($param_modele);
                $object_mail = $modele->pmail_objet;
                $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
                $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
                $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
                $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                if (!empty($taxe)) {
                    $mailMessage = str_replace('@montant_taxe', $taxe->taxe_montant, $mailMessage);
                    $mailMessage = str_replace('@annee', $taxe->annee, $mailMessage);
                    $mailMessage = str_replace('@montant_tom', $taxe->taxe_tom, $mailMessage);
                } else {
                    $mailMessage = str_replace('@montant_taxe', '', $mailMessage);
                    $mailMessage = str_replace('@annee', '', $mailMessage);
                    $mailMessage = str_replace('@montant_tom', '', $mailMessage);
                }
                $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/taxes/FormMailErreur", $data);
            }
        }
    }

    public function checkValueEmeteur()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'mess_id' => $data_post['mess_id']
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                    'method' => 'row'
                );
                $emetteur = $this->smpt_user->get($params);

                echo json_encode($emetteur);
            }
        }
    }

    public function Envoi_Mail_Taxe()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentTaxe_m', 'c_document_lot_taxe');
            $data_post = $_POST["data"];

            if (isset($data_post['list_id_doc_taxe'])) {
                $document_taxe = $this->c_document_lot_taxe->get_document($data_post['list_id_doc_taxe']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_decla']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            if (!empty($document_taxe))
                $data_post['document'] = $document_taxe;

            $this->Envoi($data_post);
        }
    }

    public function Envoi_Mail_Taxe_client()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentTaxe_m', 'c_document_lot_taxe');
            $data_post = $_POST["data"];

            if (isset($data_post['list_id_doc_taxe'])) {
                $document_taxe = $this->c_document_lot_taxe->get_document($data_post['list_id_doc_taxe']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_decla']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            if (!empty($document_taxe))
                $data_post['document'] = $document_taxe;

            $this->Envoi($data_post);
        }
    }

    public function Envoi_Mail_TaxeErreur()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $data_post = $_POST["data"];
            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_erreur']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);
            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }
            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            $this->Envoi($data_post);
        }
    }

    public function Envoi($data)
    {
        $this->load->model('Taxe_m');
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (isset($data['destinataire_copie']) && !empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_taxe_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];
            // $mail->AltBody = '../documents/signature/unnamed (5).png';
            $clause = array("taxe_id" => $data["taxe_id"]);
            $data_save = array(
                'taxe_date_envoi_mail' => date('Y-m-d : H :i:s')
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->Taxe_m->save_data($data_save, $clause);
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'taxe_id' => $data['taxe_id'],
                    'emetteur' => $data['user'],
                    'destinataire' => $data['destinataire'],
                    'objet' => $data['object'],
                    'history' => NULL
                );
                if (isset($data['cliLot_id']) && isset($data['annee'])) {
                    $retour['cliLot_id'] = $data['cliLot_id'];
                    $retour['annee'] = $data['annee'];
                }
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function modalEnvoiMailFacture()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m', 'c_lot_client');
            $this->load->model('Modelmail_m', 'c_param_modelemail');
            $this->load->model('Taxe_m', 'c_taxe_fonciere_lot');
            $this->load->model('DocumentLoyer_m', 'c_document_loyer');
            $data_post = $_POST["data"];
            $params = array(
                'clause' => array(
                    'mess_etat' => 1
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
            );
            $data['liste_emetteur'] = $this->smpt_user->get($params);
            $param = array(
                'clause' => array(
                    'cliLot_id' => $data_post['cliLot_id']
                ),
                'join' => array(
                    'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                    'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                    'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                ),
                'join_orientation' => 'left',
                'method' => "row"
            );
            $data['destinataire'] = $data_dest = $this->c_lot_client->get($param);
            $param_modele = array(
                'clause' => array(
                    'pmail_id ' => 4
                ),
                'method' => "row"
            );
            $modele = $this->c_param_modelemail->get($param_modele);
            $param_document_loyer = array(
                'clause' => array(
                    'doc_loyer_id' => $data_post['doc_loyer_id'],
                    'doc_loyer_annee' => $data_post['annee'],
                    'cliLot_id' => $data_post['cliLot_id'],
                ),
            );
            $document_loyer = $this->c_document_loyer->get($param_document_loyer);
            $data['document'] = $document_loyer;
            $object_mail = $modele->pmail_objet;
            $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
            $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
            $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
            $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
            $data['object_mail'] = $object_mail;
            $mailMessage = $modele->pmail_contenu;
            $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
            $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
            $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);

            $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);
            $data['mailMessage'] = $mailMessage;

            if ($modele->emeteur_mail == 1) {
                $util_par_default = $_SESSION['session_utilisateur']['util_id'];
            } else {
                $util_par_default = $data_dest->util_id;
            }
            $data['default_user'] = $util_par_default;

            $this->renderComponant("Clients/clients/lot/loyers/formEnvoiMail", $data);
        }
    }

    public function Envoi_Mail_Facture()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentLoyer_m', 'c_document_loyer');
            $data_post = $_POST["data"];

            if (isset($data_post['list_id_doc_loyer'])) {
                $document_loyer = $this->c_document_loyer->get_document($data_post['list_id_doc_loyer']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_facture']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            // History data
            $history['emetteur'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom . ' (' . $emetteur->mess_email . ')';
            $history['destinataire'] = $data_post['destinataire'];
            $history['destinataire_copie'] = $data_post['destinataire_copie'];
            $history['objet'] = $data_post['object'];
            $history['message'] = $data_post['message_text'];
            $history['fichiers_joints'] = isset($data_post['list_id_doc_loyer']) ? json_encode($data_post['list_id_doc_loyer']) : null;

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            if (!empty($document_loyer))
                $data_post['document'] = $document_loyer;

            $this->Envoi_Facture($data_post, $history);
        }
    }

    public function Envoi_Facture($data, $history = null)
    {
        $this->load->model('FactureLoyer_m', 'c_facture_loyer');
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_loyer_path);
                }
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $fact_loyer_id = 0;
                if (isset($data['document'])) {
                    foreach ($data['document'] as $key => $value) {
                        // Marqué la facture comme envoyé
                        $this->c_facture_loyer->updateEmailSent($value->fact_loyer_id, 1);
                        $fact_loyer_id = $value->fact_loyer_id;
                    }
                }
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'fact_loyer_id' => $fact_loyer_id,
                    'emetteur' => $data['emeteur_facture'],
                    'destinataire' => $data['destinataire'],
                    'objet' => $data['object'],
                    'history' => $history
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function modalEnvoiMailMandat()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m', 'c_lot_client');
            $this->load->model('Modelmail_m', 'c_param_modelemail');
            $this->load->model('MandatCli_m', 'c_mandat_client');
            $data_post = $_POST["data"];
            $params = array(
                'clause' => array(
                    'mess_etat' => 1
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
            );
            $data['liste_emetteur'] = $this->smpt_user->get($params);

            $param = array(
                'clause' => array(
                    'cliLot_id' => $data_post['cliLot_id']
                ),
                'join' => array(
                    'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                    'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                    'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                ),
                'join_orientation' => 'left',
                'method' => "row"
            );
            $data['destinataire'] = $data_dest = $this->c_lot_client->get($param);

            $param_modele = array(
                'clause' => array(
                    'pmail_id ' => 2
                ),
                'method' => "row"
            );
            $modele = $this->c_param_modelemail->get($param_modele);
            $param_document_mandat = array(
                'clause' => array(
                    'mandat_cli_id' => $data_post['mandat_cli_id'],
                    'cliLot_id' => $data_post['cliLot_id'],
                ),
                'join' => array(
                    'c_type_mandat' => 'c_type_mandat.type_mandat_id  = c_mandat_client.type_mandat_id',
                ),
                'method' => 'row'
            );
            $document_mandat = $this->c_mandat_client->get($param_document_mandat);
            $data['document'] = $document_mandat;

            $object_mail = $modele->pmail_objet;
            $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
            $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
            $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
            $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
            $data['object_mail'] = $object_mail;
            $mailMessage = $modele->pmail_contenu;
            $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
            $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
            $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);

            $mailMessage = str_replace('@pack_confort', $document_mandat->type_mandat_libelle, $mailMessage);
            $mailMessage = str_replace('@pack_essentiel', $document_mandat->type_mandat_libelle, $mailMessage);
            $mailMessage = str_replace('@pack_serenite', $document_mandat->type_mandat_libelle, $mailMessage);

            $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);

            $data['mailMessage'] = $mailMessage;

            if ($modele->emeteur_mail == 1) {
                $util_par_default = $_SESSION['session_utilisateur']['util_id'];
            } else {
                $util_par_default = $data_dest->util_id;
            }
            $data['default_user'] = $util_par_default;

            $this->renderComponant("Clients/clients/lot/mandats/formMailMandat", $data);
        }
    }

    public function sendMailMandat()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('MandatCli_m', 'c_mandat_client');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_mandat'])) {
                $document_mandat = $this->c_mandat_client->get_document($data_post['list_id_doc_mandat']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_mandat']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            if (!empty($document_mandat))
                $data_post['document'] = $document_mandat;

            $this->Envoi_Mail_Mandat($data_post);
        }
    }

    public function Envoi_Mail_Mandat($data)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->mandat_cli_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $retour = array('status' => 200, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function sendMailBailValidation()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m');
            $this->load->model('Bail_m');
            $this->load->model('Client_m');
            $this->load->model('Utilisateur_m');

            $data_post = $_POST["data"];

            // Bail info
            $bailId = $data_post["bail_id"];
            $paramsBail = array(
                'clause' => array(
                    'bail_id' => $bailId
                ),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($paramsBail);

            // Lot info
            $cliLotId = $data_post["cli_lot_id"];
            $paramsLot = array(
                'clause' => array('cliLot_id ' => $cliLotId),
                'method' => "row",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                    'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                    'c_client' => 'c_dossier.client_id = c_client.client_id',
                ),
            );
            $data_lot = $this->ClientLot_m->get($paramsLot);

            // Client Info
            $util_id = $data_post["util_id"];
            $paramsClient = array(
                'clause' => array('util_id  ' => $util_id),
                'method' => "row"
            );
            $data_proprietaire = $this->Utilisateur_m->get($paramsClient);

            // Administrateur
            $paramsUtil = array(
                'clause' => array('rol_util_id' => 1, 'util_etat' => 1),
            );
            $data_util = $this->Utilisateur_m->get($paramsUtil);

            $data_post['destinataire'] = $data_util[1]->util_mail;
            $data_post['destinataire_copie'] = null;

            $admins = 'BERTRAND';

            if (count($data_util) > 1) {
                $data_post['destinataire_copie'] = '';
                foreach ($data_util as $round => $utilInfo) {

                    if ($round > 1) {
                        $comma = ',';
                        if ($round <= 2) {
                            $comma = '';
                        }

                        $data_post['destinataire_copie'] .= $comma . $utilInfo->util_mail;

                        $prenom = $utilInfo->util_prenom;
                        if ($prenom == "") {
                            $prenom = $utilInfo->util_nom;
                        }
                        $admins .= ", " . $prenom;
                    }
                }
            }

            $url_lot = base_url('/Clients?_p@roprietaire=' . $data_lot->client_id . '&_lo@t_id=' . $data_lot->cliLot_id . '');

            $message = $admins . ', <br/><br/> ' . $data_proprietaire->util_prenom . ' ' . $data_proprietaire->util_nom . '
            vous demande la validation d\'un bail dans un lot : <br/>
            Propriétaire : ' . $data_lot->client_prenom . ' ' . $data_lot->client_nom . ' <br/>
            Gestionnaire : ' . $data_lot->gestionnaire_nom . ' <br/>
            Lot : ' . $data_lot->progm_nom . ' <br/>
            Dossier : ' . str_pad($data_lot->dossier_id, 4, '0', STR_PAD_LEFT) . ' <br/><br/>
            <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le bail</a>
            <br/><br/>
            <i>Remarque :  ceci est un mail automatique.</i>
            ';

            $data_post['message_text'] = $message;

            $data_post['object'] = "CELAVI Gestion : validation Bail, programme $data_lot->progm_nom";

            // Emetteur - first emetteur
            $params = array(
                'clause' => array('c_utilisateur.util_id' => $_SESSION['session_utilisateur']['util_id']),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            if (!isset($emetteur) || !$emetteur) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    le compte mail de $data_proprietaire->util_prenom $data_proprietaire->util_nom n'est pas paramétré. 
                    Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                    Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                );
                echo json_encode($retour);
                return false;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            $this->Envoi_Mail_Validation_Bail($data_post, $data_proprietaire);
        }
    }

    public function Envoi_Mail_Validation_Bail($data, $data_proprietaire)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();

        $retour = array();

        try {
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    une erreur est survenue lors de l'envoi du mail. <br/>
                    Le mail n'a donc pas été envoyé aux administrateurs. <br/>
                    Merci de les prévenir pour corriger la situation."
                );
            } else {
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'detail' => "Mail envoyé avec succès aux administrateurs. <br/>
                    Ils vont traiter votre demande dans les meilleurs délais.",
                    'data_proprietaire' => $data_proprietaire,
                    'bailId' => $data["bail_id"]
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function modalEnvoiMailCNI()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m', 'smpt_user');
            $this->load->model('Modelmail_m', 'c_param_modelemail');
            $this->load->model('Prospect_m', 'prospect');
            $data_post = $_POST["data"];
            $prospect = $this->prospect->get(array('clause' => array('prospect_id' => $data_post['prospect_id']), 'method' => 'row'));
            $id_model = $prospect->prospect_pays == 'FRANCE' || $prospect->prospect_pays == 'France' ? 5 : 18;
            $param_modele = array('clause' => array('pmail_id ' => $id_model), 'method' => "row");
            $modele = $this->c_param_modelemail->get($param_modele);

            $modele->pmail_contenu = str_replace("@prenom_client", $prospect->prospect_prenom, $modele->pmail_contenu);
            $modele->pmail_contenu = str_replace("@nom_client", $prospect->prospect_nom, $modele->pmail_contenu);

            $params = array(
                'clause' => array(
                    'c_utilisateur.util_id' => $data_post['util_id'],
                ),
                'join' => array('c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'join_orientation' => 'left',
                'method' => 'row'
            );
            $data['emetteur'] = $this->smpt_user->get($params);
            $data['util_id'] = $data_post['util_id'];
            $data['destinataire'] = $data_post['email'];
            $data['pmail_objet'] = $modele->pmail_objet;
            $data['mailMessage'] = $modele->pmail_contenu;;

            $this->renderComponant("Prospections/prospects/formMailCNI", $data);
        }
    }

    public function modalEnvoiMailCNI_Client()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m', 'smpt_user');
            $this->load->model('Modelmail_m', 'c_param_modelemail');
            $this->load->model('Client_m', 'client');
            $data_post = $_POST["data"];
            $client = $this->client->get(array('clause' => array('client_id' => $data_post['client_id']), 'method' => 'row'));
            $id_model = $client->client_pays == 'FRANCE' || $client->client_pays == 'France' ? 5 : 18;
            $param_modele = array('clause' => array('pmail_id ' => $id_model), 'method' => "row");
            $modele = $this->c_param_modelemail->get($param_modele);

            $modele->pmail_contenu = str_replace("@prenom_client", $client->client_prenom, $modele->pmail_contenu);
            $modele->pmail_contenu = str_replace("@nom_client", $client->client_nom, $modele->pmail_contenu);

            $params = array(
                'clause' => array(
                    'c_utilisateur.util_id' => $data_post['util_id'],
                ),
                'join' => array('c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'join_orientation' => 'left',
                'method' => 'row'
            );
            $data['emetteur'] = $this->smpt_user->get($params);
            $data['util_id'] = $data_post['util_id'];
            $data['destinataire'] = $data_post['email'];
            $data['pmail_objet'] = $modele->pmail_objet;
            $data['mailMessage'] = $modele->pmail_contenu;;

            $this->renderComponant("Clients/clients/formMailCNI", $data);
        }
    }
    public function demande_cni_client()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m');
            $data_post = $_POST['data'];
            $params = array(
                'clause' => array('c_utilisateur.util_id' => $data_post['util_id']),
                'join' => array(
                    'c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id',
                ),
                'method' => 'row'
            );
            $user_resposable = $this->Utilisateur_m->get($params);

            $data_email = array();
            $data_email['message_text'] = $data_post['message_text'];
            if (!empty($user_resposable->mess_signature)) {
                $signature = explode("/", $user_resposable->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_email['signature'] = $user_resposable->mess_signature;
                $data_email['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_email['sign'] . '">';
                $data_email['message_text'] .= $image;
            }
            $data_email['client_id'] = $data_post['client_id'];
            $data_email['user_label'] = $user_resposable->util_prenom . ' ' . $user_resposable->util_nom;
            $data_email['user'] = $user_resposable->mess_email;
            $data_email['pwd'] = $user_resposable->mess_motdepasse;
            $data_email['port'] = $user_resposable->mess_port;
            $data_email['auth'] = true;
            $data_email['host'] = $user_resposable->mess_adressemailserveur;
            $data_email['destinataire'] = $data_post['email'];
            $data_email['object'] = $data_post['object'];

            $this->Envoi_Demande_CNI_Client($data_email);
        }
    }

    public function Envoi_Demande_CNI_Client($data)
    {
        $mail = new PHPMailer();
        $retour = array();
        try {
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'client_id' => $data['client_id'],
                    'emetteur' => $data['user'],
                    'destinataire' => $data['destinataire'],
                    'objet' => $data['object'],
                    'history' => NULL
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function demande_cni()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m');
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('c_utilisateur.util_id' => $data_post['util_id']),
                    'join' => array(
                        'c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id',
                    ),
                    'method' => 'row'
                );
                $user_resposable = $this->Utilisateur_m->get($params);

                $data_email = array();
                $data_email['message_text'] = $data_post['message_text'];
                if (!empty($user_resposable->mess_signature)) {
                    $signature = explode("/", $user_resposable->mess_signature);
                    $string = trim($signature[3]);
                    $string = str_replace(' ', '', $string);
                    $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                    $data_email['signature'] = $user_resposable->mess_signature;
                    $data_email['sign'] = 'cid:' . $sign;
                    $image = '<br/><img src="cid:' . $data_email['sign'] . '">';
                    $data_email['message_text'] .= $image;
                }
                $data_email['prospect_id'] = $data_post['prospect_id'];
                $data_email['user_label'] = $user_resposable->util_prenom . ' ' . $user_resposable->util_nom;
                $data_email['user'] = $user_resposable->mess_email;
                $data_email['pwd'] = $user_resposable->mess_motdepasse;
                $data_email['port'] = $user_resposable->mess_port;
                $data_email['auth'] = true;
                $data_email['host'] = $user_resposable->mess_adressemailserveur;
                $data_email['destinataire'] = $data_post['email'];
                $data_email['object'] = $data_post['object'];

                $this->Envoi_Demande_CNI($data_email);
            }
        }
    }

    public function Envoi_Demande_CNI($data)
    {
        $mail = new PHPMailer();
        $retour = array();
        try {
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'prospect_id' => $data['prospect_id'],
                    'emetteur' => $data['user'],
                    'destinataire' => $data['destinataire'],
                    'objet' => $data['object'],
                    'history' => NULL
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function envoiMail()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m');
            $data_post = $_POST['data'];

            $params = array(
                'clause' => array('c_utilisateur.util_id' => $data_post['util_id']),
                'join' => array(
                    'c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id',
                ),
                'method' => 'row'
            );
            $user_resposable = $this->Utilisateur_m->get($params);

            $data_email = array();

            $data_email['message_text'] = nl2br($data_post['message_text']);

            if (!empty($user_resposable->mess_signature)) {
                $signature = explode("/", $user_resposable->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_email['signature'] = $user_resposable->mess_signature;
                $data_email['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_email['sign'] . '">';

                $data_email['message_text'] .= $image;
            }

            $data_email['user_label'] = $user_resposable->util_prenom . ' ' . $user_resposable->util_nom;
            $data_email['user'] = $user_resposable->mess_email;
            $data_email['pwd'] = $user_resposable->mess_motdepasse;
            $data_email['port'] = $user_resposable->mess_port;
            $data_email['auth'] = true;
            $data_email['host'] = $user_resposable->mess_adressemailserveur;
            $data_email['destinataire'] = $data_post['destinataire'];
            $data_email['object'] =  $data_post['object'];

            $this->Envoi_Bienvenu($data_email);
        }
    }

    public function modalformEnvoiWelcome()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m');

            $data_post = $_POST['data'];

            $params = array(
                'clause' => array('c_utilisateur.util_id' => $data_post['util_id']),
                'join' => array(
                    'c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id',
                ),
                'method' => 'row'
            );
            $data['user_resposable'] = $user_resposable = $this->Utilisateur_m->get($params);

            $this->load->model('Modelmail_m', 'c_param_modelemail');
            $data_post = $_POST["data"];
            $param_modele = array(
                'clause' => array(
                    'pmail_id ' => 6
                ),
                'method' => "row"
            );
            $data['modele'] = $modele = $this->c_param_modelemail->get($param_modele);

            $this->load->model('Prospect_m');
            $param_prospect = array(
                'clause' => array(
                    'prospect_id' => $data_post['prospect_id'],
                ),
                'method' => "row"
            );
            $data['prospect'] = $prospect =  $this->Prospect_m->get($param_prospect);

            $message_text = $modele->pmail_contenu;
            $message_text = str_replace('@nom_client', $prospect->prospect_nom, $message_text);
            $message_text = str_replace('@prenom_client', $prospect->prospect_prenom, $message_text);
            $message_text = str_replace('@nom_utilisateur', $user_resposable->util_nom, $message_text);
            $message_text = str_replace('@prenom_utilisateur', $user_resposable->util_prenom, $message_text);
            $message_text = str_replace('@email_utilisateur', $user_resposable->util_mail, $message_text);
            $message_text = str_replace('@date_envoi_mail', date('d/m/Y'), $message_text);
            $message_text = str_replace('@adresse_client', $prospect->prospect_adresse1, $message_text);
            $message_text = str_replace('@telephone_utilisateur', $user_resposable->util_phonefixe, $message_text);
            $data['message_text'] = $message_text;

            if (!empty($user_resposable->mess_signature)) {
                $data['image'] = URL_ADMIN . '/' . $user_resposable->mess_signature;
            }

            echo json_encode($data);
        }
    }

    public function Envoi_Bienvenu($data)
    {
        $mail = new PHPMailer();
        $retour = array();
        try {
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $retour = array('status' => 200, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function sendMailBailCloture()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m');
            $this->load->model('Bail_m');
            $this->load->model('Client_m');
            $this->load->model('Utilisateur_m');

            $data_post = $_POST["data"];

            // Bail info
            $bailId = $data_post["bail_id"];
            $paramsBail = array(
                'clause' => array(
                    'bail_id' => $bailId
                ),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($paramsBail);

            // Lot info
            $cliLotId = $data_post["cli_lot_id"];
            $paramsLot = array(
                'clause' => array('cliLot_id ' => $cliLotId),
                'method' => "row",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                    'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                    'c_client' => 'c_dossier.client_id = c_client.client_id',
                ),
            );
            $data_lot = $this->ClientLot_m->get($paramsLot);

            // Client Info
            $util_id = $data_post["util_id"];
            $paramsClient = array(
                'clause' => array('util_id  ' => $util_id),
                'method' => "row"
            );
            $data_proprietaire = $this->Utilisateur_m->get($paramsClient);

            // Administrateur
            $paramsUtil = array(
                'clause' => array('rol_util_id  ' => 1), // 1 : Admin
            );
            $data_util = $this->Utilisateur_m->get($paramsUtil);
            $data_post['destinataire'] = $data_util[0]->util_mail;
            $data_post['destinataire_copie'] = null;

            $prenom = $data_util[0]->util_prenom;
            if ($prenom == "") {
                $prenom = $data_util[0]->util_nom;
            }
            $admins = $prenom;

            if (count($data_util) > 1) {
                $data_post['destinataire_copie'] = '';
                foreach ($data_util as $round => $utilInfo) {
                    if ($round > 0) {
                        $comma = ',';
                        if ($round == 1)
                            $comma = '';
                        $data_post['destinataire_copie'] .= $comma . $utilInfo->util_mail;

                        $prenom = $utilInfo->util_prenom;
                        if ($prenom == "") {
                            $prenom = $utilInfo->util_nom;
                        }
                        $admins .= ", " . $prenom;
                    }
                }
            }

            $url_lot = base_url('/Clients?_p@roprietaire=' . $data_lot->client_id . '&_lo@t_id=' . $data_lot->cliLot_id . '');

            $message = '
            Bonjour ' . $admins . ', <br/><br/> ' . $data_proprietaire->util_prenom . ' ' . $data_proprietaire->util_nom . '
            souhaite demander la validation de la clôture  d\'un bail dans un lot : <br/>
            Propriétaire : ' . $data_lot->client_prenom . ' ' . $data_lot->client_nom . ' <br/>
            Gestionnaire : ' . $data_lot->gestionnaire_nom . ' <br/>
            Programme : ' . $data_lot->progm_nom . ' <br/>
            Dossier : ' . $data_lot->dossier_num . ' <br/><br/>
            <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le bail</a>
            <br/><br/>
            <i>Remarque :  ceci est un mail automatique.</i>
            ';
            $data_post['message_text'] = $message;

            $data_post['object'] = "CELAVI Gestion : validation de la clôture d'un Bail, programme $data_lot->progm_nom";

            // Emetteur - first emetteur
            $params = array(
                // 'clause' => array('c_utilisateur.util_id  ' => $util_id),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            if (!isset($emetteur) || !$emetteur) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    le compte mail de $data_proprietaire->util_prenom $data_proprietaire->util_nom n'est pas paramétré. 
                    Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                    Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                );
                echo json_encode($retour);
                return false;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            $this->Envoi_Mail_Validation_Bail($data_post, $data_proprietaire);
        }
    }

    public function sendMailBailcreerAvenant()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m');
            $this->load->model('Bail_m');
            $this->load->model('Client_m');
            $this->load->model('Utilisateur_m');

            $data_post = $_POST["data"];

            // Bail info
            $bailId = $data_post["bail_id"];
            $paramsBail = array(
                'clause' => array(
                    'bail_id' => $bailId
                ),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($paramsBail);

            // Lot info
            $cliLotId = $data_post["cli_lot_id"];
            $paramsLot = array(
                'clause' => array('cliLot_id ' => $cliLotId),
                'method' => "row",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                    'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                    'c_client' => 'c_dossier.client_id = c_client.client_id',
                ),
            );
            $data_lot = $this->ClientLot_m->get($paramsLot);

            // Client Info
            $util_id = $data_post["util_id"];
            $paramsClient = array(
                'clause' => array('util_id  ' => $util_id),
                'method' => "row"
            );
            $data_proprietaire = $this->Utilisateur_m->get($paramsClient);

            // Administrateur
            $paramsUtil = array(
                'clause' => array('rol_util_id  ' => 1), // 1 : Admin
            );
            $data_util = $this->Utilisateur_m->get($paramsUtil);
            $data_post['destinataire'] = $data_util[0]->util_mail;
            $data_post['destinataire_copie'] = null;

            $prenom = $data_util[0]->util_prenom;
            if ($prenom == "") {
                $prenom = $data_util[0]->util_nom;
            }
            $admins = $prenom;

            if (count($data_util) > 1) {
                $data_post['destinataire_copie'] = '';
                foreach ($data_util as $round => $utilInfo) {
                    if ($round > 0) {
                        $comma = ',';
                        if ($round == 1)
                            $comma = '';
                        $data_post['destinataire_copie'] .= $comma . $utilInfo->util_mail;

                        $prenom = $utilInfo->util_prenom;
                        if ($prenom == "") {
                            $prenom = $utilInfo->util_nom;
                        }
                        $admins .= ", " . $prenom;
                    }
                }
            }

            $url_lot = base_url('/Clients?_p@roprietaire=' . $data_lot->client_id . '&_lo@t_id=' . $data_lot->cliLot_id . '');

            $message = '
            Bonjour ' . $admins . ', <br/><br/> ' . $data_proprietaire->util_prenom . ' ' . $data_proprietaire->util_nom . '
            souhaite demander la validation de la création d\'un avenant dans un bail d\'un lot : <br/>
            Propriétaire : ' . $data_lot->client_prenom . ' ' . $data_lot->client_nom . ' <br/>
            Gestionnaire : ' . $data_lot->gestionnaire_nom . ' <br/>
            Programme : ' . $data_lot->progm_nom . ' <br/>
            Dossier : ' . $data_lot->dossier_num . ' <br/><br/>
            <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le bail</a>
            <br/><br/>
            <i>Remarque :  ceci est un mail automatique.</i>
            ';
            $data_post['message_text'] = $message;

            $data_post['object'] = "CELAVI Gestion : validation de la création d'un Avenant dans un Bail, programme $data_lot->progm_nom";

            // Emetteur - first emetteur
            $params = array(
                // 'clause' => array('c_utilisateur.util_id  ' => $util_id),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            if (!isset($emetteur) || !$emetteur) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    le compte mail de $data_proprietaire->util_prenom $data_proprietaire->util_nom n'est pas paramétré. 
                    Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                    Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                );
                echo json_encode($retour);
                return false;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            $this->Envoi_Mail_Validation_Bail($data_post, $data_proprietaire);
        }
    }

    public function sendMailBailprotocol()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m');
            $this->load->model('Bail_m');
            $this->load->model('Client_m');
            $this->load->model('Utilisateur_m');

            $data_post = $_POST["data"];

            // Bail info
            $bailId = $data_post["bail_id"];
            $paramsBail = array(
                'clause' => array(
                    'bail_id' => $bailId
                ),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($paramsBail);

            // Lot info
            $cliLotId = $data_post["cli_lot_id"];
            $paramsLot = array(
                'clause' => array('cliLot_id ' => $cliLotId),
                'method' => "row",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                    'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                    'c_client' => 'c_dossier.client_id = c_client.client_id',
                ),
            );
            $data_lot = $this->ClientLot_m->get($paramsLot);

            // Client Info
            $util_id = $data_post["util_id"];
            $paramsClient = array(
                'clause' => array('util_id  ' => $util_id),
                'method' => "row"
            );
            $data_proprietaire = $this->Utilisateur_m->get($paramsClient);

            // Administrateur
            $paramsUtil = array(
                'clause' => array('rol_util_id  ' => 1), // 1 : Admin
            );
            $data_util = $this->Utilisateur_m->get($paramsUtil);
            $data_post['destinataire'] = $data_util[0]->util_mail;
            $data_post['destinataire_copie'] = null;

            $prenom = $data_util[0]->util_prenom;
            if ($prenom == "") {
                $prenom = $data_util[0]->util_nom;
            }
            $admins = $prenom;

            if (count($data_util) > 1) {
                $data_post['destinataire_copie'] = '';
                foreach ($data_util as $round => $utilInfo) {
                    if ($round > 0) {
                        $comma = ',';
                        if ($round == 1)
                            $comma = '';
                        $data_post['destinataire_copie'] .= $comma . $utilInfo->util_mail;

                        $prenom = $utilInfo->util_prenom;
                        if ($prenom == "") {
                            $prenom = $utilInfo->util_nom;
                        }
                        $admins .= ", " . $prenom;
                    }
                }
            }

            $url_lot = base_url('/Clients?_p@roprietaire=' . $data_lot->client_id . '&_lo@t_id=' . $data_lot->cliLot_id . '');

            $message = '
            Bonjour ' . $admins . ', <br/><br/> ' . $data_proprietaire->util_prenom . ' ' . $data_proprietaire->util_nom . '
            souhaite demander la validation du protocol d\'un lot : <br/>
            Propriétaire : ' . $data_lot->client_prenom . ' ' . $data_lot->client_nom . ' <br/>
            Gestionnaire : ' . $data_lot->gestionnaire_nom . ' <br/>
            Programme : ' . $data_lot->progm_nom . ' <br/>
            Dossier : ' . $data_lot->dossier_num . ' <br/><br/>
            <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le bail</a>
            <br/><br/>
            <i>Remarque :  ceci est un mail automatique.</i>
            ';
            $data_post['message_text'] = $message;

            $data_post['object'] = "CELAVI Gestion : validation du protocol dans un Bail, programme $data_lot->progm_nom";

            // Emetteur - first emetteur
            $params = array(
                // 'clause' => array('c_utilisateur.util_id  ' => $util_id),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            if (!isset($emetteur) || !$emetteur) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    le compte mail de $data_proprietaire->util_prenom $data_proprietaire->util_nom n'est pas paramétré. 
                    Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                    Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                );
                echo json_encode($retour);
                return false;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;

            $this->Envoi_Mail_Validation_Bail($data_post, $data_proprietaire);
        }
    }

    function EnvoiMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->load->model('ParamMailUsers_m', 'smpt_user');
                    $this->load->model('Lot_m');
                    $this->load->model('Modelmail_m', 'c_param_modelemail');
                    $this->load->model('MandatNew_m');
                    $data_post = $_POST["data"];
                    $params = array(
                        'clause' => array(
                            'mess_etat' => 1
                        ),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                    );
                    $data['liste_emetteur'] = $this->smpt_user->get($params);

                    $param = array(
                        'clause' => array(
                            'lot_id' => $data_post['lot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id  = c_lot_prospect.progm_id',
                            'c_prospect' => 'c_prospect.prospect_id  = c_lot_prospect.prospect_id',
                            'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_prospect.gestionnaire_id',
                        ),
                        'join_orientation' => 'left',
                        'method' => "row"
                    );
                    $data['destinataire'] = $data_dest = $this->Lot_m->get($param);

                    $param_modele = array(
                        'clause' => array(
                            'pmail_id ' => 2
                        ),
                        'method' => "row"
                    );
                    $modele = $this->c_param_modelemail->get($param_modele);
                    $param_document_mandat = array(
                        'clause' => array(
                            'mandat_id' => $data_post['mandat_id'],
                            'lot_id' => $data_post['lot_id'],
                        ),
                        'join' => array(
                            'c_type_mandat' => 'c_type_mandat.type_mandat_id  = c_mandat_new.type_mandat_id',
                        ),
                        'method' => 'row'
                    );
                    $document_mandat = $this->MandatNew_m->get($param_document_mandat);
                    $data['document'] = $document_mandat;
                    $data['mandat_id'] = $data_post['mandat_id'];

                    $object_mail = $modele->pmail_objet;
                    $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
                    $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
                    $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
                    $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
                    $data['object_mail'] = $object_mail;
                    $mailMessage = $modele->pmail_contenu;
                    $mailMessage = str_replace('@nom_client', $data_dest->prospect_nom, $mailMessage);
                    $mailMessage = str_replace('@prenom_client', $data_dest->prospect_prenom, $mailMessage);
                    $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);

                    $mailMessage = str_replace('@pack_confort', $document_mandat->type_mandat_libelle, $mailMessage);
                    $mailMessage = str_replace('@pack_essentiel', $document_mandat->type_mandat_libelle, $mailMessage);
                    $mailMessage = str_replace('@pack_serenite', $document_mandat->type_mandat_libelle, $mailMessage);

                    $mailMessage = str_replace('@date_envoi_mail', date('d/m/Y'), $mailMessage);

                    $data['mailMessage'] = $mailMessage;
                    $data['lot_id'] = $data_post['lot_id'];
                    $data['prospect_id'] = $data_post['prospect_id'];

                    if ($modele->emeteur_mail == 1) {
                        $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                    } else {
                        $util_par_default = $data_dest->util_id;
                    }
                    $data['default_user'] = $util_par_default;

                    $this->renderComponant("Prospections/prospects/lot/mandats/formMail", $data);
                }
            }
        }
    }

    public function sendMailMandatProspect()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('MandatNew_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_mandat'])) {
                $document_mandat = $this->MandatNew_m->get_document($data_post['list_id_doc_mandat']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_mandat']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['mandat_id'] = $data_post['mandat_id'];

            if (!empty($document_mandat))
                $data_post['document'] = $document_mandat;

            $this->Envoi_Mail_MandatProspect($data_post);
        }
    }

    public function Envoi_Mail_MandatProspect($data)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();
        $this->load->model('MandatNew_m');



        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $lot_id = $data['lot_id'];
            $prospect_id = $data['prospect_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->mandat_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("mandat_id" => $data["mandat_id"]);
            $data = array(
                'etat_mandat_id' => 2,
                'mandat_date_envoi' => date('Y-m-d')
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->MandatNew_m->save_data($data, $clause);
                $retour = array('status' => 200, 'lot_id' => $lot_id, 'prospect_id' => $prospect_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function envoiTva()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->load->model('ParamMailUsers_m', 'smpt_user');
                    $this->load->model('Modelmail_m', 'c_param_modelemail');
                    $this->load->model('DocumentTva_m');
                    $this->load->model('Dossier_m');
                    $data_post = $_POST["data"];
                    $params = array(
                        'clause' => array(
                            'mess_etat' => 1
                        ),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                    );
                    $data['liste_emetteur'] = $this->smpt_user->get($params);

                    $param = array(
                        'clause' => array(
                            'dossier_id' => $data_post['dossier_id']
                        ),
                        'join' => array(
                            'c_client' => 'c_client.client_id  = c_dossier.client_id',
                        ),
                        'join_orientation' => 'left',
                        'method' => "row"
                    );
                    $data['destinataire'] = $data_dest = $this->Dossier_m->get($param);

                    $clause = array('pmail_id ' => 7);

                    if ($data_dest->client_pays !== "FRANCE") {
                        $clause = array('pmail_id ' => 8);
                    }

                    $param_modele = array(
                        'clause' => $clause,
                        'method' => "row"
                    );
                    $modele = $this->c_param_modelemail->get($param_modele);
                    $param_document_tva = array(
                        'clause' => array(
                            'doc_tva_id' => $data_post['doc_tva_id'],
                            'dossier_id' => $data_post['dossier_id'],
                        ),
                        'method' => 'row'
                    );
                    $document_tva = $this->DocumentTva_m->get($param_document_tva);
                    $data['document'] = $document_tva;
                    $data['doc_tva_id'] = $data_post['doc_tva_id'];

                    $object_mail = $modele->pmail_objet;
                    $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
                    $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
                    $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
                    $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
                    $data['object_mail'] = $object_mail;
                    $mailMessage = $modele->pmail_contenu;

                    $mailMessage = str_replace('@montant_tva', $document_tva->doc_tva_montant . '€', $mailMessage);


                    $data['mailMessage'] = $mailMessage;
                    $data['dossier_id'] = $data_post['dossier_id'];

                    if ($modele->emeteur_mail == 1) {
                        $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                    } else {
                        $util_par_default = $data_dest->util_id;
                    }
                    $data['default_user'] = $util_par_default;

                    $this->renderComponant("Clients/clients/dossier/fiscal/tva/formMail", $data);
                }
            }
        }
    }

    public function sendMailTva()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentTva_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_tva'])) {
                $document_tva = $this->DocumentTva_m->get_document($data_post['list_id_doc_tva']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_tva']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['doc_tva_id'] = $data_post['doc_tva_id'];

            if (!empty($document_tva))
                $data_post['document'] = $document_tva;

            $this->Envoi_Mail_Tva($data_post);
        }
    }

    public function Envoi_Mail_Tva($data)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();
        $this->load->model('DocumentTva_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $dossier_id = $data['dossier_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_tva_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_tva_id" => $data["doc_tva_id"]);
            $data = array(
                'doc_tva_date_envoi' => date('Y-m-d')
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->DocumentTva_m->save_data($data, $clause);
                $retour = array('status' => 200, 'dossier_id' => $dossier_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function envoi2031()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->load->model('ParamMailUsers_m', 'smpt_user');
                    $this->load->model('Modelmail_m', 'c_param_modelemail');
                    $this->load->model('Document2031_m');
                    $this->load->model('Dossier_m');
                    $data_post = $_POST["data"];
                    $params = array(
                        'clause' => array(
                            'mess_etat' => 1
                        ),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                    );
                    $data['liste_emetteur'] = $this->smpt_user->get($params);

                    $param = array(
                        'clause' => array(
                            'dossier_id' => $data_post['dossier_id']
                        ),
                        'join' => array(
                            'c_client' => 'c_client.client_id  = c_dossier.client_id',
                        ),
                        'join_orientation' => 'left',
                        'method' => "row"
                    );
                    $data['destinataire'] = $data_dest = $this->Dossier_m->get($param);

                    $clause = array('pmail_id ' => 9);

                    if ($data_dest->client_pays !== "FRANCE") {
                        $clause = array('pmail_id ' => 10);
                    }

                    $param_modele = array(
                        'clause' => $clause,
                        'method' => "row"
                    );
                    $modele = $this->c_param_modelemail->get($param_modele);
                    $param_document_2031 = array(
                        'clause' => array(
                            'doc_2031_id' => $data_post['doc_2031_id'],
                            'dossier_id' => $data_post['dossier_id'],
                        ),
                        'method' => 'row'
                    );
                    $document_2031 = $this->Document2031_m->get($param_document_2031);
                    $data['document'] = $document_2031;
                    $data['doc_2031_id'] = $data_post['doc_2031_id'];

                    $object_mail = $modele->pmail_objet;
                    $object_mail = str_replace('@taxe_fonciere', 'Informations taxes foncières', $object_mail);
                    $object_mail = str_replace('@proposition_mandat', 'Proposition mandat', $object_mail);
                    $object_mail = str_replace('@erreur_taxe', 'Déclaration erreur sur la taxe', $object_mail);
                    $object_mail = str_replace('@facture_Loyer', 'Envoi facture Loyer', $object_mail);
                    $data['object_mail'] = $object_mail;
                    $mailMessage = $modele->pmail_contenu;

                    $data['mailMessage'] = $mailMessage;
                    $data['dossier_id'] = $data_post['dossier_id'];

                    if ($modele->emeteur_mail == 1) {
                        $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                    } else {
                        $util_par_default = $data_dest->util_id;
                    }
                    $data['default_user'] = $util_par_default;

                    $this->renderComponant("Clients/clients/dossier/fiscal/2031/formMail", $data);
                }
            }
        }
    }

    public function sendMail2031()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('Document2031_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_2031'])) {
                $document_2031 = $this->Document2031_m->get_document($data_post['list_id_doc_2031']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_2031']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['doc_2031_id'] = $data_post['doc_2031_id'];

            if (!empty($document_2031))
                $data_post['document'] = $document_2031;

            $this->Envoi_Mail_2031($data_post);
        }
    }

    public function Envoi_Mail_2031($data)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();
        $this->load->model('Document2031_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $dossier_id = $data['dossier_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_2031_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_2031_id" => $data["doc_2031_id"]);
            $data = array(
                'doc_2031_date_envoi' => date('Y-m-d')
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->Document2031_m->save_data($data, $clause);
                $retour = array('status' => 200, 'dossier_id' => $dossier_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiExploitant()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('ClientLot_m');
                $this->load->model('DocumentLot_m');
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_lot_id'] = $data_post['doc_lot_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id  = c_lot_client.gestionnaire_id',
                        'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id  = c_gestionnaire.gestionnaire_id',
                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->ClientLot_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 11
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_exploitant = array(
                    'clause' => array(
                        'doc_lot_id' => $data_post['doc_lot_id'],
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_document_locataire.cliLot_id',
                        'c_mandat_new' => 'c_mandat_new.cliLot_id  = c_document_locataire.cliLot_id',
                    ),
                    'method' => 'row',
                    'join_orientation' => 'left'
                );

                $data['document'] = $document_locataire = $this->DocumentLot_m->get($param_document_exploitant);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/identification_lot/formMailExploitant", $data);
            }
        }
    }

    public function sendMaillocataire()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentLot_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_locataire'])) {
                $document_locataire = $this->DocumentLot_m->get_document($data_post['list_id_doc_locataire']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_locataire']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['cliLot_id'] = $data_post['cliLot_id'];

            if (!empty($document_locataire))
                $data_post['document'] = $document_locataire;

            $this->Envoi_Mail_locataire($data_post);
        }
    }

    public function Envoi_Mail_locataire($data)
    {
        // $config_server = unserialize(config_serverMail);
        $mail = new PHPMailer();
        $this->load->model('DocumentLot_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $cliLot_id = $data['cliLot_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_lot_path);
                    $mail->addAttachment($value->rib);
                    $mail->addAttachment($value->mandat_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_lot_id" => $data["doc_lot_id"]);
            $data = array(
                'doc_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->DocumentLot_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $cliLot_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiSyndic()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('ClientLot_m');
                $this->load->model('DocumentSyndic_m');
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_syndic_id'] = $data_post['doc_syndic_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_syndicat' => 'c_syndicat.syndicat_id  = c_lot_client.syndicat_id',
                        'c_contactsyndicat' => 'c_contactsyndicat.syndicat_id  = c_contactsyndicat.syndicat_id',
                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->ClientLot_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 11
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_syndic = array(
                    'clause' => array(
                        'doc_syndic_id' => $data_post['doc_syndic_id'],
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_document_syndic.cliLot_id',
                        'c_mandat_new' => 'c_mandat_new.cliLot_id  = c_document_syndic.cliLot_id',
                    ),
                    'method' => 'row',
                    'join_orientation' => 'left'
                );

                $data['document'] = $document_syndic = $this->DocumentSyndic_m->get($param_document_syndic);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/identification_lot/formMailSyndic", $data);
            }
        }
    }

    public function sendMailsyndic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentSyndic_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_syndic'])) {
                $document_syndic = $this->DocumentSyndic_m->get_document($data_post['list_id_doc_syndic']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_syndic']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['cliLot_id'] = $data_post['cliLot_id'];

            if (!empty($document_syndic))
                $data_post['document'] = $document_syndic;

            $this->Envoi_Mail_syndic($data_post);
        }
    }

    public function Envoi_Mail_syndic($data)
    {
        $mail = new PHPMailer();
        $this->load->model('DocumentSyndic_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $cliLot_id = $data['cliLot_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_syndic_path);
                    $mail->addAttachment($value->rib);
                    $mail->addAttachment($value->mandat_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_syndic_id" => $data["doc_syndic_id"]);
            $data = array(
                'doc_syndic_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->DocumentSyndic_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $cliLot_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiSie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('ClientLot_m');
                $this->load->model('DocumentSie_m');
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_sie_id'] = $data_post['doc_sie_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
                        'c_sie' => 'c_sie.sie_id  = c_dossier.sie_id',
                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->ClientLot_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 11
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_sie = array(
                    'clause' => array(
                        'doc_sie_id' => $data_post['doc_sie_id'],
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_document_sie.cliLot_id',
                        'c_mandat_new' => 'c_mandat_new.cliLot_id  = c_document_sie.cliLot_id',
                    ),
                    'method' => 'row',
                    'join_orientation' => 'left'
                );

                $data['document'] = $document_sie = $this->DocumentSie_m->get($param_document_sie);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/identification_lot/formMailSie", $data);
            }
        }
    }

    public function sendMailsie()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentSie_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_sie'])) {
                $document_sie = $this->DocumentSie_m->get_document($data_post['list_id_doc_sie']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_sie']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['cliLot_id'] = $data_post['cliLot_id'];

            if (!empty($document_sie))
                $data_post['document'] = $document_sie;

            $this->Envoi_Mail_sie($data_post);
        }
    }

    public function Envoi_Mail_sie($data)
    {
        $mail = new PHPMailer();
        $this->load->model('DocumentSie_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $cliLot_id = $data['cliLot_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_sie_path);
                    $mail->addAttachment($value->mandat_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_sie_id" => $data["doc_sie_id"]);
            $data = array(
                'doc_sie_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->DocumentSie_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $cliLot_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiSip()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('ClientLot_m');
                $this->load->model('DocumentSip_m');
                $data_post = $_POST["data"];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_sip_id'] = $data_post['doc_sip_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
                        'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
                        'c_sie' => 'c_sie.sie_id  = c_dossier.sie_id',
                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->ClientLot_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 11
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_sip = array(
                    'clause' => array(
                        'doc_sip_id' => $data_post['doc_sip_id'],
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_document_sip.cliLot_id',
                        'c_mandat_new' => 'c_mandat_new.cliLot_id  = c_document_sip.cliLot_id',
                    ),
                    'method' => 'row',
                    'join_orientation' => 'left'
                );

                $data['document'] = $document_sip = $this->DocumentSip_m->get($param_document_sip);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/identification_lot/formMailSip", $data);
            }
        }
    }

    public function sendMailsip()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('DocumentSip_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_sip'])) {
                $document_sip = $this->DocumentSip_m->get_document($data_post['list_id_doc_sip']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_sip']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['cliLot_id'] = $data_post['cliLot_id'];

            if (!empty($document_sip))
                $data_post['document'] = $document_sip;

            $this->Envoi_Mail_sip($data_post);
        }
    }

    public function Envoi_Mail_sip($data)
    {
        $mail = new PHPMailer();
        $this->load->model('DocumentSip_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $cliLot_id = $data['cliLot_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->doc_sip_path);
                    $mail->addAttachment($value->mandat_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("doc_sip_id" => $data["doc_sip_id"]);
            $data = array(
                'doc_sip_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->DocumentSip_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $cliLot_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiMailPno()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];

                $data['client_id'] = $data_post['client_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['pno_id'] = $data_post['pno_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'pno_id' => $data_post['pno_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_pno.client_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_pno.cliLot_id',
                        'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',

                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->Pno_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 13
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;

                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/pno/formMailPno", $data);
            }
        }
    }

    function EnvoiMailPnoProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];

                $data['prospect_id'] = $data_post['prospect_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['pno_id'] = $data_post['pno_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'pno_id' => $data_post['pno_id']
                    ),
                    'join' => array(
                        'c_prospect' => 'c_prospect.prospect_id  = c_pno.prospect_id',
                        'c_lot_prospect' => 'c_lot_prospect.lot_id  = c_pno.lot_id',
                        'c_programme' => 'c_programme.progm_id  = c_lot_prospect.progm_id',

                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->Pno_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 13
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;

                $mailMessage = str_replace('@nom_client', $data_dest->prospect_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->prospect_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Prospections/prospects/lot/pno/formMailPno", $data);
            }
        }
    }

    public function sendMailPno()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('Pno_m');
            $data_post = $_POST["data"];

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_pno']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['cliLot_id'] = $data_post['cliLot_id'];
            $data_post['client_id'] = $data_post['client_id'];

            $this->Envoi_Mail_pno($data_post);
        }
    }

    public function Envoi_Mail_pno($data)
    {
        $mail = new PHPMailer();
        $this->load->model('Pno_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $cliLot_id = $data['cliLot_id'];
            $client_id = $data['client_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }


            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("pno_id" => $data["pno_id"]);
            $data = array(
                'pno_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->Pno_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $cliLot_id, 'client_id' => $client_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    function EnvoiMailPnoChere()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];

                $data['client_id'] = $data_post['client_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['pno_id'] = $data_post['pno_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'pno_id' => $data_post['pno_id']
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id  = c_pno.client_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id  = c_pno.cliLot_id',
                        'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',

                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->Pno_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 15
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;

                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                $mailMessage = str_replace('@montant_pno', $data_dest->pno_montant . '€ TTC', $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/lot/pno/formMailPno", $data);
            }
        }
    }

    function EnvoiMailPnoChereProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Pno_m');
                $data_post = $_POST["data"];

                $data['prospect_id'] = $data_post['prospect_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['pno_id'] = $data_post['pno_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'pno_id' => $data_post['pno_id']
                    ),
                    'join' => array(
                        'c_prospect' => 'c_prospect.prospect_id  = c_pno.prospect_id',
                        'c_lot_prospect' => 'c_lot_prospect.lot_id  = c_pno.lot_id',
                        'c_programme' => 'c_programme.progm_id  = c_lot_prospect.progm_id',

                    ),
                    'method' => "row"
                );
                $data['destinataire'] = $data_dest = $this->Pno_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 15
                    ),
                    'method' => "row"
                );

                $modele = $this->c_param_modelemail->get($param_modele);

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;

                $mailMessage = str_replace('@nom_client', $data_dest->prospect_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->prospect_prenom, $mailMessage);
                $mailMessage = str_replace('@nom_programme', $data_dest->progm_nom, $mailMessage);
                $mailMessage = str_replace('@montant_pno', $data_dest->pno_montant . '€ TTC', $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Prospections/prospects/lot/pno/formMailPno", $data);
            }
        }
    }

    public function sendMailPnoProspect()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('Pno_m');
            $data_post = $_POST["data"];

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_pno']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['lot_id'] = $data_post['lot_id'];
            $data_post['prospect_id'] = $data_post['prospect_id'];

            $this->Envoi_Mail_pno_prospect($data_post);
        }
    }

    public function Envoi_Mail_pno_prospect($data)
    {
        $mail = new PHPMailer();
        $this->load->model('Pno_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $lot_id = $data['lot_id'];
            $prospect_id = $data['prospect_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("pno_id" => $data["pno_id"]);
            $data = array(
                'pno_date_envoi' => date('Y-m-d : H :i:s'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->Pno_m->save_data($data, $clause);
                $retour = array('status' => 200, 'cliLot_id' => $lot_id, 'client_id' => $prospect_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    private function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function testEnvoimail()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m');

            $data_post = $_POST;
            $params = array(
                'clause' => array(
                    'c_utilisateur.util_id' => $data_post['util_id'],
                ),
                'join' => array('c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'join_orientation' => 'left',
                'method' => 'row'
            );
            $emeteur = $this->Utilisateur_m->get($params);

            if ($emeteur->mess_id != '') {
                if ($emeteur->mess_signature != '') {
                    $mess_signature = $emeteur->mess_signature;
                }
            } else {
                if (!empty($_FILES)) {
                    $target_dir = "../documents/signature/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;

                    $this->makeDirPath($target_dir);

                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }

                    $mess_signature = $path_filename_ext;
                }
            }

            if (isset($mess_signature) && !empty($mess_signature)) {
                $signature = explode("/", $mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data['signature'] = $mess_signature;
                $data['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data['sign'] . '">';
                $data['message_text'] = $data_post['message_text'] . $image;
            }

            $data['user_label'] = $emeteur->util_prenom . ' ' . $emeteur->util_nom;
            $data['user'] = $data_post['email_emeteur'];
            $data['pwd'] = $data_post['mess_motdepasse'];
            $data['port'] = $data_post['mess_port'];
            $data['auth'] = true;
            $data['host'] = $data_post['mess_adressemailserveur'];
            $data['destinataire'] = $data_post['destinataire'];
            $data['object'] = $data_post['object'];
            $this->EnvoiTest($data);
        }
    }

    public function EnvoiTest($data)
    {
        $mail = new PHPMailer();

        try {
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Test d\'envoi d\'email échoué');
            } else {
                $retour = array(
                    'status' => 200,
                    'message' => 'Test d\'email réuissi et Email envoyé !',
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    function EnvoiReporting()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $this->load->model('Modelmail_m', 'c_param_modelemail');
                $this->load->model('Reporting_m');
                $data_post = $_POST["data"];

                $data['dossier_id'] = $data_post['dossier_id'];
                $data['reporting_id'] = $data_post['reporting_id'];

                $params = array(
                    'clause' => array(
                        'mess_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id')
                );
                $data['liste_emetteur'] = $this->smpt_user->get($params);

                $param = array(
                    'clause' => array(
                        'c_reporting.dossier_id' => $data['dossier_id'],
                        'c_reporting.reporting_id' => $data['reporting_id']
                    ),
                    'join' => array(
                        'c_dossier' => 'c_reporting.dossier_id = c_dossier.dossier_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_lot_client' => 'c_lot_client.dossier_id = c_dossier.dossier_id',
                        'c_gestionnaire' => 'c_lot_client.gestionnaire_id = c_gestionnaire.gestionnaire_id',
                        'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',

                    ),
                    'method' => "row",
                    'join_orientation' => 'left',
                );
                $data['destinataire'] = $data_dest = $this->Reporting_m->get($param);

                $param_modele = array(
                    'clause' => array(
                        'pmail_id ' => 16
                    ),
                    'method' => "row"
                );
                $modele = $this->c_param_modelemail->get($param_modele);

                $param_document_reporting = array(
                    'clause' => array(
                        'c_reporting.dossier_id' => $data['dossier_id'],
                        'c_reporting.reporting_id' => $data['reporting_id']
                    ),
                    'method' => 'row'
                );
                $document_reporting = $this->Reporting_m->get($param_document_reporting);

                $data['document'] = $document_reporting;

                $object_mail = $modele->pmail_objet;
                $data['object_mail'] = $object_mail;
                $mailMessage = $modele->pmail_contenu;

                $mailMessage = str_replace('@nom_client', $data_dest->client_nom, $mailMessage);
                $mailMessage = str_replace('@prenom_client', $data_dest->client_prenom, $mailMessage);
                $data['mailMessage'] = $mailMessage;

                if ($modele->emeteur_mail == 1) {
                    $util_par_default = $_SESSION['session_utilisateur']['util_id'];
                } else {
                    $util_par_default = $data_dest->util_id;
                }
                $data['default_user'] = $util_par_default;

                $this->renderComponant("Clients/clients/dossier/reporting/formMailReporting", $data);
            }
        }
    }

    public function sendMailReporting()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('Reporting_m');
            $data_post = $_POST["data"];
            if (isset($data_post['list_id_doc_reporting'])) {
                $document_reporting = $this->Reporting_m->get_document($data_post['list_id_doc_reporting']);
            }

            $params = array(
                'clause' => array(
                    'mess_id' => $data_post['emeteur_reporting']
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['dossier_id'] = $data_post['dossier_id'];

            if (!empty($document_reporting))
                $data_post['document'] = $document_reporting;

            $this->Envoi_Mail_reporting($data_post);
        }
    }

    public function Envoi_Mail_reporting($data)
    {
        $mail = new PHPMailer();
        $this->load->model('Reporting_m');

        $retour = array();
        try {
            // decommente to debug mail
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER; 
            // config mail//  
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            $dossier_id = $data['dossier_id'];

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            // $mail->addBCC('hentsilavina@gmail.com');
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            //pièce jointe
            if (isset($data['document'])) {
                foreach ($data['document'] as $key => $value) {
                    $mail->addAttachment($value->reporting_path);
                }
            }

            $mail->isHTML(true);

            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            $clause = array("reporting_id" => $data["reporting_id"]);
            $data = array(
                'reporting_date_envoi' => date('Y-m-d'),
                'util_id' => $_SESSION['session_utilisateur']['util_id']
            );

            if (!$mail->send()) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
            } else {
                $this->Reporting_m->save_data($data, $clause);
                $retour = array('status' => 200, 'dossier_id' => $dossier_id, 'message' => 'Mailer sent!');
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }

        echo json_encode($retour);
    }

    public function sendMailGestionnaire()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $this->load->model('ClientLot_m');
            $this->load->model('Bail_m');
            $this->load->model('Client_m');
            $this->load->model('Utilisateur_m');

            $data_post = $_POST["data"];

            // Bail info
            $paramsBail = array(
                'clause' => array(
                    'bail_id' => $data_post["bail_id"]
                ),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($paramsBail);

            $nature_bail = $bail->bail_avenant == 1 ? 'l\'avenant ' : 'le bail';

            // Lot info
            $paramsLot = array(
                'clause' => array('cliLot_id ' => $data_post["clilotId"]),
                'method' => "row",
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                    'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                    'c_client' => 'c_dossier.client_id = c_client.client_id',
                ),
            );
            $data_lot = $this->ClientLot_m->get($paramsLot);

            $params = array(
                'clause' => array('c_utilisateur.util_id' => $_SESSION['session_utilisateur']['util_id']),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $emetteur = $this->smpt_user->get($params);

            // Gestionnaire info
            $util_id = $data_lot->util_id;
            $paramsClient = array(
                'clause' => array('util_id  ' => $util_id),
                'method' => "row"
            );
            $data_gestionnaire = $this->Utilisateur_m->get($paramsClient);

            $url_lot = base_url('/Clients?_p@roprietaire=' . $data_lot->client_id . '&_lo@t_id=' . $data_lot->cliLot_id . '');

            $message = $data_gestionnaire->util_prenom . ', <br/><br/> ' . $emetteur->util_prenom . '
            a validé ' . $nature_bail . ' dans le lot : <br/>
            Propriétaire : ' . $data_lot->client_prenom . ' ' . $data_lot->client_nom . ' <br/>
            Gestionnaire : ' . $data_lot->gestionnaire_nom . ' <br/>
            Lot : ' . $data_lot->progm_nom . ' <br/>
            Dossier : ' . str_pad($data_lot->dossier_id, 4, '0', STR_PAD_LEFT) . ' <br/><br/>
            <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le bail</a>
            <br/><br/>
            ';

            $data_post['message_text'] = $message;
            $data_post['object'] = "CELAVI Gestion : validation Bail, programme $data_lot->progm_nom";

            // Emetteur 
            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data_post['signature'] = $emetteur->mess_signature;
                $data_post['sign'] = 'cid:' . $sign;
                $image = '<br/><img src="cid:' . $data_post['sign'] . '">';
                $data_post['message_text'] .= $image;
            }

            if (!isset($emetteur) || !$emetteur) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    le compte mail de $data_gestionnaire->util_prenom $data_gestionnaire->util_nom n'est pas paramétré. 
                    Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                    Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                );
                echo json_encode($retour);
                return false;
            }

            $data_post['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data_post['user'] = $emetteur->mess_email;
            $data_post['pwd'] = $emetteur->mess_motdepasse;
            $data_post['port'] = $emetteur->mess_port;
            $data_post['auth'] = true;
            $data_post['host'] = $emetteur->mess_adressemailserveur;
            $data_post['destinataire'] = $data_gestionnaire->util_mail;

            $this->Envoi_Mail_Validation_Bail($data_post, $data_gestionnaire);
        }
    }

    public function sendMailcloture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $this->load->model('Utilisateur_m');
                $this->load->model('ParamMailUsers_m', 'smpt_user');

                //data dossier
                $data_dossier = $this->Dossier_m->get(
                    array(
                        'clause' => array('dossier_id' => $_POST['data']['dossier_id']),
                        'method' => "row",
                        'join' => array(
                            'c_client' => 'c_dossier.client_id = c_client.client_id',
                        ),
                    )
                );

                //list admin
                $list_admin = $this->Utilisateur_m->get(
                    array(
                        'clause' => array('rol_util_id' => 1, 'util_etat' => 1)
                    )
                );

                //destinataire et destinataire en copie
                $data = array();
                $data['destinataire'] = $list_admin[1]->util_mail;
                $data['destinataire_copie'] = null;
                $admins =  $list_admin[1]->util_prenom . ' ' . $list_admin[1]->util_nom;

                foreach ($list_admin as $key => $value) {
                    if ($value->util_id == 1 || $value->util_id == 2) {
                        continue;
                    }

                    $admins .= ', ' . $value->util_prenom . ' ' . $value->util_nom;
                    $data['destinataire_copie'] .= ', ' . $value->util_mail;
                }

                $url_lot = base_url('/Clients?_p@roprietaire=' . $data_dossier->client_id . '&_do@t_id=' . $data_dossier->dossier_id . '&_nom=' . $data_dossier->client_nom);

                $message = $admins . ', <br/><br/> ' . $_SESSION['session_utilisateur']['util_prenom'] . ' ' . $_SESSION['session_utilisateur']['util_nom'] . '
                vous demande la validation de clôture d\'un dossier : <br/>
                Propriétaire : ' . $data_dossier->client_prenom . ' ' . $data_dossier->client_nom . ' <br/>
                Dossier : ' . str_pad($data_dossier->dossier_id, 4, '0', STR_PAD_LEFT) . ' <br/><br/>
                <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le dossier</a>
                <br/><br/>
                <i>Remarque :  ceci est un mail automatique.</i>
                ';

                $data['message_text'] = $message;
                $data['object'] = "CELAVI Gestion : validation de clôture du Dossier " . str_pad($data_dossier->dossier_id, 4, '0', STR_PAD_LEFT);

                // Emetteur - emetteur
                $params = array(
                    'clause' => array('c_utilisateur.util_id' => $_SESSION['session_utilisateur']['util_id']),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                    'method' => 'row'
                );
                $emetteur = $this->smpt_user->get($params);

                if (!empty($emetteur->mess_signature)) {
                    $signature = explode("/", $emetteur->mess_signature);
                    $string = trim($signature[3]);
                    $string = str_replace(' ', '', $string);
                    $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                    $data['signature'] = $emetteur->mess_signature;
                    $data['sign'] = 'cid:' . $sign;
                    $image = '<br/><img src="cid:' . $data['sign'] . '">';
                    $data['message_text'] .= $image;
                }

                if (empty($emetteur)) {
                    $params = array(
                        'clause' => array('util_id' => $_SESSION['session_utilisateur']['util_id']),
                        'method' => 'row'
                    );
                    $user = $this->Utilisateur_m->get($params);
                    $retour = array(
                        'status' => 500,
                        'message' => 'Mailer Error',
                        'detail' => "Attention, 
                        le compte mail de $user->util_prenom $user->util_nom n'est pas paramétré. 
                        Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                        Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                    );
                    echo json_encode($retour);
                    return false;
                }

                $data['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
                $data['user'] = $emetteur->mess_email;
                $data['pwd'] = $emetteur->mess_motdepasse;
                $data['port'] = $emetteur->mess_port;
                $data['auth'] = true;
                $data['host'] = $emetteur->mess_adressemailserveur;

                $this->Envoi_Mail_Validation_clotureDossier($data);
            }
        }
    }

    public function Envoi_Mail_Validation_clotureDossier($data)
    {
        $mail = new PHPMailer();
        $retour = array();
        try {
            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            $mail->addAddress(trim($data['destinataire']));

            if (!empty($data['destinataire_copie'])) {
                $searchForValue = ',';
                $found = false;
                if (strpos($data['destinataire_copie'], $searchForValue) !== false) {
                    $found = true;
                }
                if ($found == true) {
                    $liste_destinataire = explode(',', $data['destinataire_copie']);
                    foreach ($liste_destinataire as $key => $value) {
                        $mail->addCC(trim($value));
                    }
                } else {
                    $mail->addCC(trim($data['destinataire_copie']));
                }
            }
            //signature
            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader($data['object'], $mail->CharSet);
            $mail->Body = $data['message_text'];

            if (!$mail->send()) {
                $retour = array(
                    'status' => 500,
                    'message' => 'Mailer Error',
                    'detail' => "Attention, 
                    une erreur est survenue lors de l'envoi du mail. <br/>
                    Le mail n'a donc pas été envoyé aux administrateurs. <br/>
                    Merci de les prévenir pour corriger la situation."
                );
            } else {
                $retour = array(
                    'status' => 200,
                    'message' => 'Email envoyé !',
                    'detail' => "Mail envoyé avec succès aux administrateurs. <br/>
                    Ils vont traiter votre demande dans les meilleurs délais.",
                    'data_proprietaire' => $data,
                );
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }
        echo json_encode($retour);
    }

    public function sendMailclotureExpercomptable()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m');
                $this->load->model('Utilisateur_m');
                $this->load->model('ParamMailUsers_m', 'smpt_user');

                //data dossier
                $data_dossier = $this->Dossier_m->get(
                    array(
                        'clause' => array('c_cloture_annuelle.dossier_id' => $_POST['data']['dossier_id']),
                        'method' => "row",
                        'join' => array(
                            'c_client' => 'c_dossier.client_id = c_client.client_id',
                            'c_cloture_annuelle' => 'c_cloture_annuelle.dossier_id =c_dossier.dossier_id',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_cloture_annuelle.util_expert_comptable'
                        ),
                    )
                );

                //destinataire
                $data = array();
                $data['destinataire'] = $data_dossier->util_mail;

                $url_lot = base_url('/Clients?_p@roprietaire=' . $data_dossier->client_id . '&_do@t_id=' . $data_dossier->dossier_id . '&_nom=' . $data_dossier->client_nom);

                $message = 'Bonjour ' . $data_dossier->util_prenom . ' ' . $data_dossier->util_nom . ', <br/><br/> Un dossier clôturé vous est assigné : <br/>
                Propriétaire : ' . $data_dossier->client_prenom . ' ' . $data_dossier->client_nom . ' <br/>
                Dossier : ' . str_pad($data_dossier->dossier_id, 4, '0', STR_PAD_LEFT) . ' <br/><br/>
                <a style="color: white; background-color: #2569c3; padding: 7px;" href="' . $url_lot . '">Voir le dossier</a>
                <br/><br/>
                <i>Remarque :  ceci est un mail automatique.</i>
                ';

                $data['message_text'] = $message;
                $data['object'] = "CELAVI Gestion : clôture du Dossier " . str_pad($data_dossier->dossier_id, 4, '0', STR_PAD_LEFT);

                // Emetteur - emetteur
                $params = array(
                    'clause' => array('c_utilisateur.util_id' => $_SESSION['session_utilisateur']['util_id']),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                    'method' => 'row'
                );
                $emetteur = $this->smpt_user->get($params);

                if (!empty($emetteur->mess_signature)) {
                    $signature = explode("/", $emetteur->mess_signature);
                    $string = trim($signature[3]);
                    $string = str_replace(' ', '', $string);
                    $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                    $data['signature'] = $emetteur->mess_signature;
                    $data['sign'] = 'cid:' . $sign;
                    $image = '<br/><img src="cid:' . $data['sign'] . '">';
                    $data['message_text'] .= $image;
                }

                if (empty($emetteur)) {
                    $params = array(
                        'clause' => array('util_id' => $_SESSION['session_utilisateur']['util_id']),
                        'method' => 'row'
                    );
                    $user = $this->Utilisateur_m->get($params);
                    $retour = array(
                        'status' => 500,
                        'message' => 'Mailer Error',
                        'detail' => "Attention, 
                        le compte mail de $user->util_prenom $user->util_nom n'est pas paramétré. 
                        Il n'est donc pas possible d'envoyer ce mail de demande de validation. 
                        Veuillez vous rapprocher de l'administrateur du logiciel pour qu'il réalise ce paramétrage."
                    );
                    echo json_encode($retour);
                    return false;
                }

                $data['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
                $data['user'] = $emetteur->mess_email;
                $data['pwd'] = $emetteur->mess_motdepasse;
                $data['port'] = $emetteur->mess_port;
                $data['auth'] = true;
                $data['host'] = $emetteur->mess_adressemailserveur;

                $this->Envoi_Mail_Validation_clotureDossier($data);
            }
        }
    }
}
