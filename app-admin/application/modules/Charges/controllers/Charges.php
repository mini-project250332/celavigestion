<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Charges extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Charges";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/charges/charges.js",
            "js/historique/historique.js"
        );

        $this->_css_personnaliser = array();
    }

    public function index()
    {
    }

    public function getMenuCharges()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data_post = $_POST['data'];
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $request_date = $this->MandatNew_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'order_by_columns' => 'mandat_date_signature DESC',
                        'method' => 'result'
                    )
                );

                $data['date_signature'] = $request_date;

                if (!empty($data['date_signature'])) {
                    foreach ($data['date_signature'] as $key => $value) {
                        $date[] = array(
                            'annee' => $value->mandat_date_signature
                        );
                    }
                    $data['max_date'] = max($date);
                }

                $this->renderComponant("Clients/clients/lot/charges/charges-generales", $data);
            }
        }
    }

    public function getListeDocumentCharges()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TypeChargeLot_m');
                $this->load->model('TauxTVA_m');
                $this->load->model('DocumentCharges_m');
                $this->load->model('Charges_m');

                $data_post = $_POST['data'];

                $param = array(
                    'clause' => array("cliLot_id" => $data_post['cliLot_id'], "doc_charge_annee" => $data_post['annee'], "doc_charge_etat" => 1),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_charge.util_id')
                );

                $params_charge = array(
                    'clause' => array("cliLot_id" => $data_post['cliLot_id'], "annee" => $data_post['annee'], 'charge_cloture' => 0),
                );

                $param_cloture = array(
                    'clause' => array('annee' => $data_post['annee'], 'c_charge.cliLot_id' => $data_post['cliLot_id'], 'charge_cloture' => 1),
                    'method' => 'row',

                );

                $data['cloture'] = $list_charge_cloture = $this->Charges_m->get($param_cloture);

                $data['document'] = $this->DocumentCharges_m->get($param);
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee'] = $data_post['annee'];
                $data['type_charge'] = $this->TypeChargeLot_m->get(array());
                $data['tva'] = $this->TauxTVA_m->get(array());
                $data['charge'] = $this->Charges_m->get($params_charge);

                $this->renderComponant("Clients/clients/lot/charges/list-document-charge", $data);
            }
        }
    }

    public function getListCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Charges_m');
                $this->load->model('DocumentCharges_m');

                $data_post = $_POST['data'];

                // $param = array(
                //     'clause' => array('annee' => $data_post['annee'], 'cliLot_id' => $data_post['cliLot_id']),
                //     'join' => array(
                //         'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id'
                //     ),
                // );
                $param = array(
                    'clause' => array('annee' => $data_post['annee'], 'cliLot_id' => $data_post['cliLot_id'], 'charge_cloture' => 0),
                    'join' => array(
                        'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id',
                    ),
                    'join_orientation' => 'left'
                );

                $data['list_charge'] = $list_charge = $this->Charges_m->get($param);

                foreach ($list_charge as $key => $value_charge) {
                    $params_doc = array(
                        'clause' => array(
                            'annee' => $data_post['annee'],
                            'c_charge.cliLot_id' => $data_post['cliLot_id'],
                            'c_charge.charge_id' => $value_charge->charge_id,
                            'doc_charge_etat' => 1
                        ),

                        'join' => array(
                            'c_charge' => 'c_document_charge.charge_id  = c_charge.charge_id'
                        ),
                        'join_orientation' => 'left'
                    );

                    $list_charge[$key]->doc_charge = $this->DocumentCharges_m->get($params_doc);
                }

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee'] = $data_post['annee'];
                //   $data['list_charge'] = $list_charge = $this->Charges_m->get($param);





                $totat_ht = 0;
                $total_tva = 0;
                $total_ttc = 0;
                foreach ($list_charge as $key => $value) {
                    $totat_ht += $value->montant_ht;
                    $total_tva += $value->tva;
                    $total_ttc += $value->montant_ttc;
                }

                $data['totat_ht'] = $totat_ht;
                $data['total_tva'] = $total_tva;
                $data['total_ttc'] = $total_ttc;

                $this->renderComponant("Clients/clients/lot/charges/list-table-charge", $data);
            }
        }
    }

    public function addCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Charges_m');

                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout d'encaissement"));
                $data_post = $_POST['data'];

                $data_retour['request'] = $data_post;

                $data_post['charge_id'] = $charge_id = $data_post['charge_id'] ? intval($data_post['charge_id']) : null;
                $data['charge_date_saisie'] = date('Y-m-d');
                $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];

                if ($charge_id !== null) {
                    $clause = array("charge_id" => $charge_id);
                    $request = $this->Charges_m->save_data($data_post, $clause);
                    $data_retour['charge'] = $charge_id;
                } else {
                    $request = $this->Charges_m->save_data($data_post);
                    $data_retour['charge'] = $request;
                }


                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement d'encaissement reçue"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function addDocSeulCharge()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->load->model('DocumentCharges_m');
            $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout d'encaissement"));
            $insert_id = $this->db->insert_id();
            $data_post = $_POST;

            $data_retour['request'] = $data_post;

            foreach ($_FILES as $key => $value) {

                $annee = $_POST['annee'];
                $cliLot_id = $_POST['cliLot_id'];
                $targetDir = "../documents/clients/lots/depenses/$annee/$cliLot_id";
                $temp_name = $value['tmp_name'];
                $data_file['doc_charge_nom'] = $value['name'];
                $data_file['doc_charge_creation'] = date('Y-m-d H:i:s');
                $data_file['doc_charge_etat'] = 1;
                $data_file['doc_charge_traite'] = 0;
                $data_file['doc_charge_annee'] = $annee;
                $data_file['cliLot_id'] = $_POST['cliLot_id'];
                $data_file['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data_file['charge_id'] = $_POST['charge_id'] != '' ? $_POST['charge_id'] : $insert_id;
                $data_file['doc_charge_path'] = str_replace('\\', '/', $targetDir . DIRECTORY_SEPARATOR . $data_file['doc_charge_nom']);
                $this->makeDirPath($targetDir);

                $counter = 1;
                $file_info = pathinfo($data_file['doc_charge_nom']);
                while (file_exists($data_file['doc_charge_path'])) {
                    $filename = $file_info['filename'] . '_' . $counter . '.' . $file_info['extension'];
                    $data_file['doc_charge_path'] = $targetDir . '/' . $filename;
                    $counter++;
                }
                if (!file_exists($data_file['doc_charge_path'])) {
                    move_uploaded_file($temp_name, $data_file['doc_charge_path']);
                }

                $request = $this->DocumentCharges_m->save_data($data_file);
            }
        }
    }
    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function removefile()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCharges_m');
                $data_retour = array("doc_charge_id" => $_POST['data']['doc_charge_id']);
                $data = array(
                    'doc_charge_etat' => 0,
                );
                $clause = array("doc_charge_id" => $_POST['data']['doc_charge_id']);
                $request = $this->DocumentCharges_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "suppression effectuée"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function deleteCharges()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la suppression…",
            );

            $this->load->model('Charges_m');

            $data = decrypt_service($_POST["data"]);

            $params = array(
                'clause' => array('charge_id' => $data['charge_id']),
                'method' => "row",
            );
            $request_get = $this->Charges_m->get($params);

            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'charge_id' => $data['charge_id'],
                        'title' => "Confirmation de la suppression",
                        'text' => "Voulez-vous vraiment supprimer cette Dépense ?",
                        'btnConfirm' => "Supprimer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $clause = array('charge_id' => $data['charge_id']);
                $request = $this->Charges_m->delete_data($clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'charge_id' => $data['charge_id'],
                        ),
                        'message' => "Charge supprimée avec succès",
                    );
                }
            }

            $retour['charge_info'] = $request_get;
            echo json_encode($retour);
        }
    }

    public function getUpdateCharge()
    {
        $this->load->model('Charges_m');
        $this->load->model('DocumentCharges_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['charge_id'] = $data_post['charge_id'];

                $request_progrmme_charge = $this->Charges_m->get(
                    array(
                        'clause' => array('charge_id' => $data_post['charge_id']),
                        'method' => 'row',
                    )
                );

                $request_doc_charge = $this->DocumentCharges_m->get(
                    array(
                        'clause' => array('charge_id' => $data_post['charge_id'], 'doc_charge_etat' => 1),
                    )
                );
                $data_retour = $request_progrmme_charge;
                $data_retour_doc = $request_doc_charge;

                $retour = retour(true, "success", $data_retour, $data_retour_doc, array("message" => "Info Charge"));

                echo json_encode($retour);
            }
        }
    }

    public function formUploadDocCharge()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];
        $data['annee'] = $data_post['annee'];
        $this->renderComponant('Clients/clients/lot/charges/form_upload_charge', $data);
    }

    public function uploadfileCharges()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $annee = $_POST['annee'];
        $cliLot_id = $_POST['cliLot_id_charge'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/Charges/$annee/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'doc_charge_nom' => $original_name,
                'doc_charge_creation' => date('Y-m-d H:i:s'),
                'doc_charge_path' => str_replace('\\', '/', $filePath),
                'doc_charge_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id,
                'doc_charge_annee' => $annee
            );

            $this->load->model('DocumentCharges_m');
            $this->DocumentCharges_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getpathDoccharge()
    {
        $this->load->model('DocumentCharges_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_charge_id', 'doc_charge_path'),
                    'clause' => array('doc_charge_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentCharges_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function deleteDocCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentCharges_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_charge_id' => $data['doc_charge_id']),
                    'columns' => array('doc_charge_id'),
                    'method' => "row",
                );

                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_charge_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_charge_id' => $data['doc_charge_id']);
                    $data_update = array('doc_charge_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_charge_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modalupdateDocCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_charge_annee'] = $data_post['doc_charge_annee'];
                $this->load->model('DocumentCharges_m');
                $data['document'] = $this->DocumentCharges_m->get(
                    array('clause' => array("doc_charge_id" => $data_post['doc_charge_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Clients/clients/lot/charges/form_updateficCharge", $data);
        }
    }

    public function UpdateDocumentCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCharges_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array(
                        "cliLot_id" => $data['cliLot_id'],
                        "doc_charge_annee" => $data['doc_charge_annee']
                    );
                    $doc_charge_id = $data['doc_charge_id'];
                    unset($data['doc_charge_id']);

                    $clause = array("doc_charge_id" => $doc_charge_id);
                    $request = $this->DocumentCharges_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateDocChargeTraitement()
    {
        $this->load->model('DocumentCharges_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_charge_id' => $data_post['doc_charge_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentCharges_m->get($params);
                if ($request->doc_charge_traite == 1) {
                    $data['doc_charge_traite'] = 0;
                    $data['doc_charge_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_charge_id' => $data_post['doc_charge_id']);
                    $this->DocumentCharges_m->save_data($data, $clause);
                } else {
                    $data['doc_charge_traite'] = 1;
                    $data['doc_charge_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_charge_id' => $data_post['doc_charge_id']);
                    $this->DocumentCharges_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function validerDepense()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation",
                );
                $this->load->model('Charges_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('charge_id' => $data['charge_id']),
                    'columns' => array('charge_id'),
                    'method' => "row",
                );
                $request_get = $this->Charges_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['charge_id'],
                            'cliLot_id' => $data['cliLot_id'],
                            'annee' => $data['annee'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous vraiment valider cette dépense ? ",
                            'btnConfirm' => "Valider",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('charge_id' => $data['charge_id']);
                    $data_update = array(
                        'charge_date_validation' => date('Y-m-d'),
                        'charge_etat_valide' => 1
                    );
                    $request = $this->Charges_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['charge_id'],
                                'cliLot_id' => $data['cliLot_id'],
                                'annee' => $data['annee']
                            ),
                            'message' => "Validation réussie",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modaljoinCharge()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Charges_m');
                $this->load->model('DocumentCharges_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_charge_annee'] = $data_post['doc_charge_annee'];
                $data['doc_charge_id'] = $data_post['doc_charge_id'];
                $data['charge'] = $this->Charges_m->get(
                    array(
                        'columns' => array("charge_id", "type_charge_id", "tpchrg_lot_nom", "montant_ht", "date_facture"),
                        'join' => array('c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id = c_charge.type_charge_id'),
                        'method' => "result"
                    )
                );
            }

            $this->renderComponant("Clients/clients/lot/charges/formJoinCharge", $data);
        }
    }

    public function UpdateChargeDoc()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCharges_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array(
                        "cliLot_id" => $data['cliLot_id'],
                        "doc_charge_annee" => $data['doc_charge_annee']
                    );
                    $doc_charge_id = $data['doc_charge_id'];
                    unset($data['doc_charge_id']);

                    $clause = array("doc_charge_id" => $doc_charge_id);
                    $request = $this->DocumentCharges_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function annulersaisie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation",
                );
                $this->load->model('Charges_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cliLot_id' => $data['cliLot_id'], 'annee' => $data['annee']),
                    'columns' => array('charge_id'),
                    'method' => "row",
                );
                $request_get = $this->Charges_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'cliLot_id' => $data['cliLot_id'],
                            'annee' => $data['annee'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous annuler la clôture de la saisie et permettre au propriétaire de faire des saisies supplémentaires pour l'année concernée ? ",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cliLot_id' => $data['cliLot_id'], 'annee' => $data['annee'], 'charge_cloture' => 1);
                    // $data_update = array(
                    //     'charge_cloture' => 1
                    // );
                    $request = $this->Charges_m->delete_data($clause);
                    // $request = $this->Charges_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'cliLot_id' => $data['cliLot_id'],
                                'annee' => $data['annee']
                            ),
                            'message' => "Confirmation réussie",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
