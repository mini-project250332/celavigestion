<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Prospections extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Prospections";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/app.js",
            "js/historique/historique.js",
            "js/bail/bail.js",
            "js/loyer/loyer.js",
            "js/acte/acte.js",
            "js/immo/immo.js",
            "js/dossiers/dossiers.js",
            "js/charges/charges.js",
            "js/fiscal/fiscal.js",
            "js/mail/mail.js",
            "js/components/prospections/prospections.js",
            "js/components/prospections/acte.js",
            "js/components/prospections/bail.js",
            "js/saisietemps/saisietemps.js",
            "js/pno/pno_prospect.js",

        );
        $this->_css_personnaliser = array(
            "css/prospections/prospections.css",
            "css/historique/historique.css",
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "prospects";
        $this->_data['sous_module'] = "prospects/contenaire-prospect";

        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $data['originecli'] = $this->getOriginClient();
                $data['originedem'] = $this->getOriginDemande();
                $data['produit'] = $this->getlisteProduit();
                $data['parcours'] = $this->getlisteParcoursVente();
                $data['etat'] = $this->getlisteEtatProspect();

                $this->renderComponant("Prospections/prospects/header-liste", $data);
            }
        }
    }

    /*****Prospect******/

    public function formNewProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['etat'] = $this->getlisteEtatProspect();
                $data['originecli'] = $this->getOriginClient();
                $data['originedem'] = $this->getOriginDemande();
                $data['produit'] = $this->getlisteProduit();
                $data['parcours'] = $this->getlisteParcoursVente();
                $data['type'] = $this->getlisteTypeProspect();
                $data['utilisateur'] = $this->getListUser();
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();
                $data['nationalite'] = $this->getNationalite();

                $this->renderComponant("Prospections/prospects/form-prospect", $data);
            }
        }
    }

    public function listeProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $like = array();
                $or_like = array();
                $clause = array(
                    'etat_prospect' => 1,
                    'etat_prospectcli' => 1,
                    'c_etat_prospect.etat_prospect_id !=' => 5
                );

                $data_post = $_POST['data'];

                $pagination = $data_post['pagination_page'];
                $rows_limit = intval($data_post['row_data']);

                if ($data_post['origine_cli_id'] != '') {
                    $clause['c_origine_client.origine_cli_id'] = $data_post['origine_cli_id'];
                }

                if ($data_post['origine_dem_id'] != '') {
                    $clause['c_origine_demande.origine_dem_id'] = $data_post['origine_dem_id'];
                }

                if ($data_post['parcours_vente_id'] != '') {
                    $clause['c_parcours_vente.vente_id'] = $data_post['parcours_vente_id'];
                }

                if ($data_post['etat_prospect_id'] != '') {
                    $clause['c_etat_prospect.etat_prospect_id'] = $data_post['etat_prospect_id'];
                }


                $pagination_data = explode('-', $data_post['pagination_page']);

                if ($data_post['text_filtre'] != '') {
                    $like['prospect_nom'] = $data_post['text_filtre'];
                    $or_like['prospect_prenom'] = $data_post['text_filtre'];
                    $or_like['prospect_email'] = $data_post['text_filtre'];
                    $or_like['prospect_email1'] = $data_post['text_filtre'];
                    $or_like['prospect_phonemobile'] = $data_post['text_filtre'];
                    $or_like['prospect_phonefixe'] = $data_post['text_filtre'];
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['prospect_date_creation >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['prospect_date_creation <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['prospect_date_creation'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }

                $data = array();
                $this->load->model('Prospect_m');
                $params = array(
                    'clause' => $clause,
                    'like' => $like,
                    'or_like' => $or_like,
                    'columns' => array(
                        'prospect_id',
                        'prospect_nom',
                        'prospect_prenom',
                        'prospect_date_creation',
                        'prospect_email',
                        'prospect_email1',
                        'prospect_phonemobile',
                        'prospect_entreprise',
                        'prospect_phonefixe',
                        'origine_dem_libelle',
                        'util_prenom',
                        'origine_cli_libelle',
                        'vente_libelle',
                        'etat_prospect_libelle',
                        'etat_couleur',
                        'etat_prospectcli',
                        'c_origine_demande.origine_dem_id',
                        'c_parcours_vente.vente_id',
                        'c_etat_prospect.etat_prospect_id',
                        'c_utilisateur.util_id',
                        'c_paysmonde.paysmonde_id',
                        'paysmonde_indicatif',
                        'c_origine_client.origine_cli_id',
                        'c_typeprospect.type_prospect_id',
                        'type_prospect_libelle',
                        'produit_id',
                        'c_pays.pays_id',
                        'pays_indicatif',
                        'prospect_after_id'
                    ),
                    'join' => array(
                        'c_origine_demande' => 'c_origine_demande.origine_dem_id = c_prospect.origine_dem_id',
                        // 'c_produit' => 'c_produit.produit_id = c_prospect.produit_id',
                        'c_origine_client' => 'c_origine_client.origine_cli_id = c_prospect.origine_cli_id',
                        'c_parcours_vente' => 'c_parcours_vente.vente_id = c_prospect.vente_id',
                        'c_etat_prospect' => 'c_etat_prospect.etat_prospect_id = c_prospect.etat_prospect_id',
                        'c_typeprospect' => 'c_typeprospect.type_prospect_id = c_prospect.type_prospect_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id',
                        'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_prospect.paysmonde_id',
                        'c_pays' => 'c_pays.pays_id = c_prospect.pays_id'
                    ),
                    'order_by_columns' => "prospect_id DESC",
                    'limit' => array('offset' => $pagination_data[1], 'limit' => $pagination_data[0]),
                );

                $request = $this->Prospect_m->get($params);

                $filter_produit = array();
                foreach ($request as $key => $value) {
                    if (!empty($value->produit_id)) {
                        $array_produit = explode(',', $value->produit_id);
                        $produit = array();
                        foreach ($array_produit as $value_produit) {
                            array_push($produit, $this->getProduit($value_produit));
                        }
                        $request[$key]->produit = implode(' , ', $produit);
                    }
                    // fitre produit
                    if (!empty($data_post['produit_id'])) {
                        $array_produit = explode(',', $value->produit_id);
                        if (!empty($array_produit) && in_array(intval($data_post['produit_id']), $array_produit))
                            $filter_produit[] = $value;
                    } else {
                        $filter_produit[] = $value;
                    }
                }

                $data['prospect'] = $filter_produit;
                $data['mandat'] = $this->listMandatProspect();
                $nb_result = sizeof($data['prospect']);
                $data['nb_result'] = $nb_result;

                $data["countAllResult"] = $this->countAllData('Prospect_m', array('etat_prospect' => 1));
                $nb_total = $data["countAllResult"];

                $data['pagination_page'] = $data_post['pagination_page'];
                $data['rows_limit'] = $rows_limit;

                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];
                $this->renderComponant("Prospections/prospects/liste-prospects", $data);
            }
        }
    }

    public function listMandatProspect()
    {

        $this->load->model('Mandat_m');
        $mandat = $this->Mandat_m->getMandatProspect();

        return $mandat;
    }

    function getProduit($produit_id)
    {
        $this->load->model('Produit_m');
        $param = array(
            'clause' => array('produit_id' => $produit_id),
            'method' => 'row',
            'columns' => array('produit_libelle', 'produit_libelle'),
        );
        $request = $this->Produit_m->get($param);
        return (!empty($request)) ? $request->produit_libelle : '';
    }

    public function getlisteEtatProspect()
    {
        $this->load->model('EtatProspect_m');
        $params = array(
            'columns' => array('etat_prospect_id', 'etat_prospect_libelle', 'etat_couleur')
        );
        $request = $this->EtatProspect_m->get($params);
        return $request;
    }

    public function getlisteTypeProspect()
    {
        $this->load->model('TypeProspect_m');
        $params = array(
            'columns' => array('type_prospect_id', 'type_prospect_libelle')
        );
        $request = $this->TypeProspect_m->get($params);
        return $request;
    }

    function prospectAdd()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Prospect_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $data['etat_prospect'] = 1;
                    $data['prospect_date_creation'] = date_sql($data['prospect_date_creation'], '-', 'jj/MM/aaaa');
                    unset($data['undefined']);
                    $request = $this->Prospect_m->save_data($data);

                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => "Prospect enregistré avec succès"));
                    }
                }

                echo json_encode($retour);
            }
        }
    }

    public function formUpdateProspect()
    {

        $this->load->model('Prospect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $data = array();

                $data['etat'] = $this->getlisteEtatProspect();
                $data['originecli'] = $this->getOriginClient();
                $data['originedem'] = $this->getOriginDemande();
                $data['produit'] = $this->getlisteProduit();
                $data['parcours'] = $this->getlisteParcoursVente();
                $data['type'] = $this->getlisteTypeProspect();
                $data['utilisateur'] = $this->getListUser();

                // if ($data_post['action'] == "edit") {
                $data['prospect'] = $this->Prospect_m->get(
                    array('clause' => array("prospect_id" => $data_post['prospect_id']), 'method' => "row")
                );
                // }
                // $data['action'] = $data_post['action'];

                $this->renderComponant("Prospections/prospects/fiche-prospect", $data);
            }
        }
    }

    public function pageficheProspect()
    {
        $this->load->model('Prospect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('prospect_id' => $data_post['prospect_id']),
                    'method' => "row",
                    'join' => array(
                        'c_origine_demande' => 'c_origine_demande.origine_dem_id = c_prospect.origine_dem_id',
                        // 'c_produit' => 'c_produit.produit_id = c_prospect.produit_id',
                        'c_origine_client' => 'c_origine_client.origine_cli_id = c_prospect.origine_cli_id',
                        'c_parcours_vente' => 'c_parcours_vente.vente_id = c_prospect.vente_id',
                        'c_typeprospect' => 'c_typeprospect.type_prospect_id = c_prospect.type_prospect_id',
                        'c_etat_prospect' => 'c_etat_prospect.etat_prospect_id = c_prospect.etat_prospect_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id',
                        'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_prospect.paysmonde_id',
                        'c_pays' => 'c_pays.pays_id = c_prospect.pays_id'
                    )
                );


                $data['etat'] = $this->getlisteEtatProspect();
                $data['originecli'] = $this->getOriginClient();
                $data['originedem'] = $this->getOriginDemande();
                $data['produit'] = $this->getlisteProduit();
                $data['parcours'] = $this->getlisteParcoursVente();
                $data['utilisateur'] = $this->getListUser();
                $data['type'] = $this->getlisteTypeProspect();
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();
                $data['prospect'] = $this->Prospect_m->get($params);
                $data['nationalite'] = $this->getNationalite();

                $data['page'] = $data_post['page'];

                if ($data_post['page'] == "form") {
                    $this->renderComponant("Prospections/prospects/form-update-identification", $data);
                } else {
                    $this->renderComponant("Prospections/prospects/fiche-identification", $data);
                }
            }
        }
    }
    public function updateInfoProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prospect_m');
                $data = $_POST;
                $data['cin'] = NULL;
                $data['file_rib'] = NULL;

                if (!empty($_FILES)) {
                    if (!empty($_FILES['file'])) {
                        $target_dir = "../documents/Prospect_CIN/";
                        $file = $_FILES['file']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file']['tmp_name'];
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $this->makeDirPath($target_dir);
                        $counter = 1;
                        while (file_exists($path_filename_ext)) {
                            $filename = $filename . '_' . $counter;
                            $path_filename_ext = $target_dir . $filename . "." . $ext;
                            $counter++;
                        }
                        if (!file_exists($path_filename_ext)) {
                            move_uploaded_file($temp_name, $path_filename_ext);
                        }
                        $data['cin'] = $path_filename_ext;
                    }

                    if (!empty($_FILES['file_rib'])) {
                        $target_dir = "../documents/Prospect_RIB/";
                        $file = $_FILES['file_rib']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file_rib']['tmp_name'];
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $this->makeDirPath($target_dir);
                        if (!file_exists($path_filename_ext)) {
                            move_uploaded_file($temp_name, $path_filename_ext);
                        }
                        $data['file_rib'] = $path_filename_ext;
                    }
                }

                if ($data['cin'] == NULL) {
                    unset($data['cin']);
                }

                if ($data['file_rib'] == NULL) {
                    unset($data['file_rib']);
                }

                $retour = retour(false, "error", 0, array("message" => "Erreur de la modification"));
                $prospect_id = $data['prospect_id'];
                $data['prospect_iban'] = str_replace(' ', '', $data['prospect_iban']);
                unset($data['page']);
                unset($data['search_terms']);
                unset($data['prospect_id']);
                unset($data['file']);

                $clause = array("prospect_id" => $prospect_id);
                $request = $this->Prospect_m->save_data($data, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "La modification a été réussi avec succès."));
                }
                echo json_encode($retour);
            }
        }
    }

    public function removefilecin()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prospect_m');
                $data_post = $_POST['data'];
                $param = array('prospect_id' => $data_post['prospect_id']);
                $data_prospect = array(
                    'cin' => NULL,
                );
                $request = $this->Prospect_m->save_data($data_prospect, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function removefilerib()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prospect_m');
                $data_post = $_POST['data'];
                $param = array('prospect_id' => $data_post['prospect_id']);
                $data_prospect = array(
                    'file_rib' => NULL,
                );
                $request = $this->Prospect_m->save_data($data_prospect, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateParcoursProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prospect_m');

                $data = $_POST['data'];

                $prospect_id = $data['prospect_id'];
                $data['prospect_date_modification'] = Carbon::now()->toDateTimeString();
                unset($data['prospect_id']);

                $clause = array("prospect_id" => $prospect_id);
                $request = $this->Prospect_m->save_data($data, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, array("message" => "Modification reussie."));
                }

                echo json_encode($retour);
            }
        }
    }

    function removeProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Prospect_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('prospect_id' => $data['prospect_id']),
                    'columns' => array('prospect_id', 'prospect_nom', 'prospect_prenom', 'prospect_entreprise'),
                    'method' => "row",
                );
                $request_get = $this->Prospect_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['prospect_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le prospect " . ((trim($request_get->prospect_entreprise) != "") ? '[ ' . $request_get->prospect_entreprise . ' ] ' : '') . $request_get->prospect_nom . " " . $request_get->prospect_prenom . " ? ",
                            'btnConfirm' => "Supprimer le Prospect",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('prospect_id' => $data['prospect_id']);
                    $data_update = array('etat_prospect' => 0);
                    $request = $this->Prospect_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['prospect_id']),
                            'message' => "Le prospect " . ((trim($request_get->prospect_entreprise) != "") ? '[ ' . $request_get->prospect_entreprise . ' ] ' : '') . $request_get->prospect_nom . " " . $request_get->prospect_prenom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*****************Lots***********************/

    public function listLot()
    {
        $this->load->model('Lot_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array(
                        'lot_id',
                        'lot_num',
                        'lot_nom',
                        'lot_adresse1',
                        'c_gestionnaire.gestionnaire_id',
                        'gestionnaire_nom',
                        'progm_adresse1',
                        'progm_adresse2',
                        'progm_adresse3',
                        'progm_cp',
                        'progm_ville',
                        'progm_pays',
                        'c_typelot.typelot_id',
                        'typelot_libelle',
                        'lot_principale',
                        'lot_ville',
                        'c_programme.progm_id',
                        'progm_nom',
                        'c_typologielot.typologielot_id',
                        'typologielot_libelle'
                    ),

                    'clause' => array(
                        'prospect_id' => $data_post['prospect_id'],
                        'etat_lot' => 1
                    ),
                    'join' => array(
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_prospect.gestionnaire_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id',
                        'c_typelot' => 'c_typelot.typelot_id = c_lot_prospect.typelot_id',
                        'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_prospect.typologielot_id'
                    ),
                    'method' => "result",

                );

                $data['lot'] = $this->Lot_m->get($params);
                $data["prospect_id"] = $data_post['prospect_id'];

                $this->renderComponant("Prospections/prospects/lot/liste-lot", $data);
            }
        }
    }


    function cruLot()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Lot_m');
                $this->load->model('Program_m');
                $this->load->model('Mandat_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    // $data_pgm = array(
                    //     'progm_adresse1' =>  $data['progm_adresse1'],
                    //     'progm_adresse2' => $data['progm_adresse2'],
                    //     'progm_adresse3' => $data['progm_adresse3'],
                    //     'progm_cp' => $data['progm_cp'],
                    //     'progm_ville' => $data['progm_ville'],
                    //     'progm_pays' => $data['progm_pays']
                    // );

                    $data_lot = array(
                        'prospect_id' => $data['prospect_id'],
                        'gestionnaire_id' => $data['gestionnaire_id'],
                        'typelot_id' => $data['typelot_id'],
                        'progm_id' => $data['progm_id'],
                        'typologielot_id' => $data['typologielot_id'],
                        'etat_lot' => 1,
                        'lot_principale' => $data['lot_principale']
                    );

                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "action" => $action);
                    $data['etat_lot'] = 1;
                    unset($data['action']);
                    $clause = null;
                    $clause_pgm = array("progm_id" => $data['progm_id']);
                    if ($action == "edit") {
                        $clause = array("lot_id" => $data['lot_id']);
                        $data_retour['lot_id'] = $data['lot_id'];
                        unset($data['lot_id']);
                        unset($data['undefined']);

                        if ($data['lot_principale'] == 1) {
                            $claus = array('prospect_id' => $data['prospect_id']);
                            $data_update = array('lot_principale' => 2);
                            $this->Lot_m->save_data($data_update, $claus);
                        }
                    }
                    $request = $this->Lot_m->save_data($data_lot, $clause);
                    //$insert_id = $this->db->insert_id();
                    // $data_mandat = array(
                    //     'lot_id' => $insert_id,
                    //     'prospect_id' => $data['prospect_id']
                    // );
                    // $request_mandat = $this->Mandat_m->save_data($data_mandat);

                    if ($action == "add") {
                        $data_retour['lot_id'] = $request;
                    }
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Lot ajouté avec succès" : "La modification est effectuée avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function updateLot()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Lot_m');
                $this->load->model('Program_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $data_pgm = array(
                        'progm_adresse1' => $data['progm_adresse1'],
                        'progm_adresse2' => $data['progm_adresse2'],
                        'progm_adresse3' => $data['progm_adresse3'],
                        'progm_cp' => $data['progm_cp'],
                        'progm_ville' => $data['progm_ville'],
                        'progm_pays' => $data['progm_pays']
                    );

                    $data_lot = array(
                        // 'prospect_id' =>  $data['prospect_id'],
                        'gestionnaire_id' => $data['gestionnaire_id'],
                        'typelot_id' => $data['typelot_id'],
                        'progm_id' => $data['progm_id'],
                        'typologielot_id' => $data['typologielot_id'],
                        'etat_lot' => 1,
                        'lot_principale' => $data['lot_principale'],
                        'lot_num' => $data['lot_num'],
                        'lot_num_parking' => $data['lot_num_parking']

                    );

                    $lot_id = $data['lot_id'];
                    // $action = $data['action'];
                    unset($data['lot_id']);
                    unset($data['page']);

                    if ($data['lot_principale'] == 1) {

                        $data_update = array('lot_principale' => 2);
                        $this->Lot_m->save_data($data_update);
                    }

                    $clause = array("lot_id" => $lot_id);
                    $clause_pgm = array("progm_id" => $data['progm_id']);

                    $request = $this->Lot_m->save_data($data_lot, $clause);
                    $request_pgm = $this->Program_m->save_data($data_pgm, $clause_pgm);


                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "La modification est effectuée avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getListProgram()
    {

        $this->load->model('Program_m');
        $params = array(
            'clause' => array('progm_etat' => 1),
            'columns' => array(
                'progm_id',
                'progm_nom',
                'progm_adresse1',
                'progm_adresse2',
                'progm_adresse3',
                'progm_cp',
                'progm_ville',
                'progm_pays',
            ),
            'order_by_columns' => 'progm_nom ASC'
        );
        $request = $this->Program_m->get($params);
        return $request;
    }

    public function modelMandat()
    {
        $this->renderComponant("Prospections/prospects/lot/modele_mandat/pack_essentiel");
    }

    public function modelSerenite()
    {

        $this->renderComponant("Prospections/prospects/lot/modele_mandat/pack_serenite");
    }
    public function modelConfort()
    {

        $this->renderComponant("Prospections/prospects/lot/modele_mandat/pack_confort");
    }

    public function getFormLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('Lot_m');
                $data['prospect_id'] = $data_post['prospect_id'];
                if ($data_post['action'] == "edit" || $data_post['action'] == "fiche") {
                    $params = array(
                        'clause' => array('lot_id ' => $data_post['lot_id']),
                        'method' => "row",
                        'join' => array('c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id'),
                    );
                    $data['lot'] = $request = $this->Lot_m->get($params);
                }
                $data['action'] = $data_post['action'];

                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['gestionnaire'] = $this->getListGestionnaire();

                $lot_principal = array();
                $clause = array(
                    'prospect_id' => $data_post['prospect_id'],
                    'lot_principale' => 1
                );
                $lot_principal_request = $this->Lot_m->get(
                    array(
                        'columns' => ['lot_principale'],
                        'clause' => $clause
                    )
                );

                $data["lot_verify_principal"] = (!empty($lot_principal_request)) ? 2 : 1;

                $this->renderComponant("Prospections/prospects/lot/form-lot", $data);
            }
        }
    }

    public function getFicheLot()
    {
        $this->load->model('Lot_m');
        $this->load->model('Prospect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['prospect_id'] = $data_post['prospect_id'];
                $params = array(
                    'clause' => array('lot_id ' => $data_post['lot_id']),
                    'method' => "row",
                    'join' => array('c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id'),
                );
                $data['lot'] = $request = $this->Lot_m->get($params);

                $data['prospect'] = $this->Prospect_m->get(
                    array('clause' => array("prospect_id" => $data_post['prospect_id']), 'method' => "row")
                );

                $this->renderComponant("Prospections/prospects/lot/fiche-lot", $data);
            }
        }
    }

    public function FicheLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('Lot_m');

                $params = array(
                    'clause' => array('lot_id ' => $data_post['lot_id']),
                    'method' => "row",
                    'join' => array('c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id'),
                );
                $data['lot'] = $this->Lot_m->get($params);

                $data['page'] = $data_post['page'];

                $data['typelot'] = $this->getTypeLot();
                $data['typologie'] = $this->getTypologieLot();
                $data['program'] = $this->getListProgram();
                $data['gestionnaire'] = $this->getListGestionnaire();

                if ($data['page'] == "fiche") {
                    $this->renderComponant("Prospections/prospects/lot/identification_lot/fiche_identification", $data);
                } else {
                    $this->renderComponant("Prospections/prospects/lot/identification_lot/formUpdate-lot", $data);
                }
            }
        }
    }

    function removeLot()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Lot_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('lot_id' => $data['lot_id']),
                    'columns' => array('lot_id', 'lot_nom'),
                    'method' => "row",
                );
                $request_get = $this->Lot_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['lot_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le lot " . $request_get->lot_nom,
                            'btnConfirm' => "Supprimer le lot",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('lot_id' => $data['lot_id']);
                    $data_update = array('etat_lot' => 0);
                    $request = $this->Lot_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['lot_id']),
                            'message' => "Le lot " . $request_get->lot_nom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*****Mandat pour un lot*****/

    public function listeMandat()
    {
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST['data'];
                $data = array();
                $params = array(
                    'clause' => array(
                        'lot_id' => $data_post['lot_id'],
                        'mandat_etat' => 1
                    ),
                    'join' => array(
                        'c_prospect' => 'c_prospect.prospect_id = c_mandat_new.prospect_id',
                        'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id'
                    )

                );

                $data['mandat'] = $this->MandatNew_m->get($params);
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
                $data['docmandat'] = $this->getDocMandat();
                $data['mandatdoc'] = $this->getMandatDoc();

                // $paramSignature = array(
                //     'clause' => array(
                //         'lot_id' => $data_post['lot_id'],
                //     ),
                //     'columns' => array('mandat_date_signature'),
                //     'method' => 'row'
                // );
                // $data['signature'] = $this->Mandat_m->get($paramSignature);

                $this->renderComponant("Prospections/prospects/lot/mandats/list-mandat", $data);
            }
        }
    }

    public function FormMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();

                if ($data_post['action'] == "edit") {
                    $this->load->model('MandatNew_m');
                    $params = array(
                        'clause' => array('mandat_id ' => $data_post['mandat_id']),
                        'method' => "row"
                    );
                    $data['mandat'] = $this->MandatNew_m->get($params);
                }

                $data['action'] = $data_post['action'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
                $data['typemandat'] = $this->getTypeMandat();
            }

            $this->renderComponant("Prospections/prospects/lot/mandats/form-mandat", $data);
        }
    }

    public function sendMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('MandatNew_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('mandat_id' => $data['mandat_id']),
                    'columns' => array('mandat_id'),
                    'method' => "row",
                );
                $request_get = $this->MandatNew_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['mandat_id'],
                            'title' => "Confirmation",
                            'text' => "Voulez-vous vraiment basculer ce mandat à l'état 'Mandat envoyé' ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('mandat_id' => $data['mandat_id']);
                    $data_update = array('etat_mandat_id' => 2, 'mandat_date_envoi' => date('Y-m-d'));
                    $request = $this->MandatNew_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['mandat_id']),
                            'message' => "Mandat envoyé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***Signtature mandat***/

    public function SignerMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
            }

            $this->renderComponant("Prospections/prospects/lot/mandats/sign-mandat", $data);
        }
    }

    public function AddSignMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data = $_POST;
                $retour = retour(false, "error", 0, array("message" => "Error"));

                $data_retour = array("prospect_id" => $data['prospect_id'], "lot_id" => $data['lot_id']);
                $mandat_id = $data['mandat_id'];
                $lot_id = $data['lot_id'];
                $data['etat_mandat_id'] = 3;
                $data['mandat_date_signature'] = Carbon::now()->toDateTimeString();
                $data['mandat_path'] = NULL;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/prospections/lots/mandat/$lot_id";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['mandat_path'] = $path_filename_ext;
                }

                $clause = array("mandat_id" => $mandat_id);

                $request = $this->MandatNew_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Signature ajoutée avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function closMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('MandatNew_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('mandat_id' => $data['mandat_id']),
                    'columns' => array('mandat_id'),
                    'method' => "row",
                );
                $request_get = $this->MandatNew_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['mandat_id'],
                            'title' => "Confirmation de la clôture",
                            'text' => "Voulez-vous vraiment clôturer ce mandat et le mettre à l'état 'clos sans suite' ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('mandat_id' => $data['mandat_id']);
                    $data_update = array('etat_mandat_id' => 4);
                    $request = $this->MandatNew_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['mandat_id']),
                            'message' => "Mandat clôturé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    public function cloturerMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m');
                $data_post = $_POST["data"];
                $data = array();
                $data['mandat_id'] = $data_post['mandat_id'];
                $data['lot_id'] = $data_post['lot_id'];
                $data['prospect_id'] = $data_post['prospect_id'];
            }

            $this->renderComponant("Prospections/prospects/lot/mandats/cloture-mandat", $data);
        }
    }

    public function getDocMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $this->load->model('DocMandatProspect_m', 'document');

                $params = array(
                    'columns' => array('docmandat_id', 'docmandat_nom', 'docmandat_path', 'docmandat_creation', 'c_utilisateur.util_id', 'util_prenom'),
                    'clause' => array(
                        'lot_id' => $data_post['lot_id'],
                        'docmandat_etat' => 1,
                        'docmandat_type' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_mandat.util_id')
                );
                $request = $this->document->get($params);
                return $request;
            }
        }
    }

    public function getMandatDoc()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $this->load->model('DocMandatProspect_m', 'document');

                $params = array(
                    'columns' => array('docmandat_id', 'docmandat_nom', 'docmandat_path', 'docmandat_creation', 'c_utilisateur.util_id', 'util_prenom'),
                    'clause' => array(
                        'lot_id' => $data_post['lot_id'],
                        'docmandat_etat' => 1,
                        'docmandat_type' => 0
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_mandat.util_id')
                );
                $request = $this->document->get($params);
                return $request;
            }
        }
    }

    public function getPathMandat()
    {

        $this->load->model('DocMandatProspect_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('docmandat_id', 'docmandat_path'),
                    'clause' => array('docmandat_id' => $data_post['id_doc'])
                );

                $request = $this->document->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getPath()
    {

        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('mandat_id', 'mandat_path'),
                    'clause' => array('mandat_id' => $data_post['id_doc'])
                );

                $request = $this->MandatNew_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function AddMandat()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('MandatNew_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "lot_id" => $data['lot_id'], "action" => $action);
                    $lot_id = $data['lot_id'];
                    $prospect_id = $data['prospect_id'];
                    $data['mandat_date_creation'] = date('Y-m-d H:i:s', strtotime($data['mandat_date_creation']));
                    $data['mandat_etat'] = 1;
                    $data['etat_mandat_id'] = 1;
                    $data['mandat_type_id'] = 2;
                    unset($data['action']);
                    $type_mandat_id = $data['type_mandat_id'];

                    $clause = null;

                    if ($action == "edit") {
                        $clause = array("mandat_id" => $data['mandat_id']);
                        $data_retour['mandat_id'] = $data['mandat_id'];
                        unset($data['mandat_id']);
                        $request = $this->MandatNew_m->save_data($data, $clause);
                    }

                    $request = $this->MandatNew_m->save_data($data, $clause);

                    if (!empty($request)) {
                        //$retour = retour(true, "success",$clause, array("message" => "Mandat créé avec succès."));
                        $gp = $this->generatePdf_modelMandat($lot_id, $type_mandat_id);

                        $retour = retour(true, "success", $data_retour, array("message" => $lot_id . "-" . $gp));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function generatePdf_modelMandat($lot_id, $type_mandat_id)
    {
        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.1") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");
        $this->load->model('MandatNew_m');
        $data = array();
        $params = array(
            'clause' => array('c_mandat_new.lot_id' => $lot_id),
            'join' => array(
                'c_prospect' => 'c_prospect.prospect_id = c_mandat_new.prospect_id',
                'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                'c_lot_prospect' => 'c_lot_prospect.prospect_id = c_mandat_new.prospect_id',
                'c_programme' => 'c_lot_prospect.progm_id = c_programme.progm_id',
            ),
            'method' => "row"
        );

        $data['mandat'] = $this->MandatNew_m->get($params);

        $mpdf = new \Mpdf\Mpdf();
        $stylesheet = file_get_contents('./assets/css/prospections/modelMandat.css');

        if ($type_mandat_id == 1) {
            $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true);
        } else {
            $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true);
        }

        // $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true);       

        // $mpdf->WriteHTML($stylesheet,1);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);

        $filename = 'Mandat_N_' . str_pad($data['mandat']->mandat_id, 4, '0', STR_PAD_LEFT) . '_' . $data['mandat']->prospect_nom . ' ' . $data['mandat']->prospect_prenom . '_'  . $data['mandat']->progm_nom;
        $filename = str_replace(array(" ", "&"), "_", $filename);
        $url = '';
        $url .= $filename . '.pdf';

        $url_path = APPPATH . "../../documents/prospections/lots/mandat/" . $lot_id;
        $url_doc = "../documents/prospections/lots/mandat/$lot_id/$url";
        $this->makeDirPath($url_path);

        $url_file = $url_path . "/" . $url;
        if (file_exists($url_doc)) {
            $counter = 1;
            do {
                $url = $filename . '_' . $counter . '.pdf';
                $url_doc = "../documents/prospections/lots/mandat/$lot_id/$url";
                $url_file = $url_path . "/" . $url;
                $counter++;
            } while (file_exists($url_doc));
        }

        $clause = array("lot_id" => $lot_id);
        $data = array(
            'mandat_path' => str_replace('\\', '/', $url_doc),
        );

        $this->MandatNew_m->save_data($data, $clause);

        $mpdf->Output($url_file, 'F');
        return $url_path;
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function pdf_modelMandat()
    {
        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $this->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $this->load->library('mpdf-5.7-php5/mpdf');
        }

        $this->load->helper("url");
        $this->load->model('MandatNew_m');
        if ($_GET) {

            $lot_id = $_GET["lot_id"];
            $type_mandat_id = $_GET["type_mandat_id"];
            $data = array();
            $params = array(
                'clause' => array('lot_id' => $lot_id),
                'join' => array(
                    'c_prospect' => 'c_prospect.prospect_id = c_mandat_new.prospect_id',
                    'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                    'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id'
                ),
                'method' => "row"

            );

            $data['mandat'] = $this->MandatNew_m->get($params);

            $mpdf = new \Mpdf\Mpdf();
            $stylesheet = file_get_contents('./assets/css/prospections/modelMandat.css');

            if ($type_mandat_id == 1) {
                $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true);
            } else {
                $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true);
            }

            //  $html = $this->load->view("Prospections/prospects/lot/modele_mandat/pack_confort", $data, true); 
            // $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($html);

            $filename = "Mandat-CELAVIGESTION";

            // $filename = "pack_confort"; 

            $mpdf->Output($filename . '.pdf', 'D');
            exit;
        }
    }

    public function FormUploadMandat()
    {

        $data_post = $_POST['data'];
        $data['lot_id'] = $data_post['lot_id'];
        $data['prospect_id'] = $data_post['prospect_id'];
        $this->renderComponant('Prospections/prospects/lot/mandats/formulaire_upload_file', $data);
    }

    public function uploadfileMandat()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $lot_id = $_POST['lot_id'];

        // Chemin du dossier
        $targetDir = "../documents/prospections/lots/mandat/$lot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'docmandat_nom' => $original_name,
                'docmandat_creation' => date('Y-m-d H:i:s'),
                'docmandat_path' => str_replace('\\', '/', $filePath),
                'docmandat_etat' => 1,
                'docmandat_type' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'lot_id' => $lot_id
            );

            $this->load->model('DocMandatProspect_m');
            $this->DocMandatProspect_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Mandat_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('mandat_id' => $data['mandat_id']),
                    'columns' => array('mandat_id'),
                    'method' => "row",
                );
                $request_get = $this->Mandat_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['mandat_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce mandat ?",
                            'btnConfirm' => "Supprimer le mandat",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('mandat_id' => $data['mandat_id']);
                    $data_update = array('mandat_etat' => 0);
                    $request = $this->Mandat_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['mandat_id']),
                            'message' => "document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDocMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocMandatProspect_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('docmandat_id' => $data['docmandat_id']),
                    'columns' => array('docmandat_id', 'docmandat_nom'),
                    'method' => "row",
                );
                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['docmandat_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('docmandat_id' => $data['docmandat_id']);
                    $data_update = array('docmandat_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['docmandat_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*****************/
    public function FormGestionnaire()
    {
        $this->load->model('Gestionnaire_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Gestionnaire_m', 'gestionnaire');
                    $data['gestionnaire'] = $this->gestionnaire->get(
                        array('clause' => array("gestionnaire_id" => $data_post['gestionnaire_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
            }

            $this->renderComponant("Prospections/prospects/form-gestionnaire", $data);
        }
    }

    public function FormProgramme()
    {
        $this->load->model('Program_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                if ($data_post['action'] != "add") {
                    $this->load->model('Program_m');
                    $data['programme'] = $this->Program_m->get(
                        array('clause' => array("progm_id" => $data_post['progm_id']), 'method' => "row")
                    );
                }
                $data['action'] = $data_post['action'];
            }
            $this->renderComponant("Prospections/prospects/form-programme", $data);
        }
    }


    /*****************Communication****************/

    public function listCommunication()
    {
        $this->load->model('CommunicationProspect_m');
        $this->load->model('Prospect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array(
                        'comprospect_id',
                        'comprospect_texte',
                        'comprospect_date_creation',
                        'c_type_communication_prospect.type_comprospect_id',
                        'type_comprospect_libelle',
                        'c_utilisateur.util_id',
                        'util_nom',
                        'util_prenom'
                    ),
                    'clause' => array(
                        'prospect_id' => $data_post['prospect_id'],
                        'etat_com' => 1
                    ),
                    'join' => array(
                        'c_type_communication_prospect' => 'c_type_communication_prospect.type_comprospect_id = c_communication_prospect.type_comprospect_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_communication_prospect.util_id'
                    ),
                );

                $data["prospect_id"] = $data_post['prospect_id'];
                $data['com'] = $this->CommunicationProspect_m->get($params);
                $data['tache'] = $this->liste_tache();
                $data['tachevalide'] = $this->liste_tache_valide();

                $data['prospect'] = $this->Prospect_m->get(
                    array('clause' => array("prospect_id" => $data_post['prospect_id']), 'method' => "row")
                );

                $data["countAllResult"] = $this->countAllData('CommunicationProspect_m', array('etat_com' => 1, 'prospect_id' => $data_post['prospect_id']));
                $nb_total = $data["countAllResult"];

                $this->renderComponant("Prospections/prospects/liste-com", $data);
            }
        }
    }

    public function getFormCom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $data['prospect_id'] = $data_post['prospect_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('CommunicationProspect_m');
                    $params = array(
                        'clause' => array('comprospect_id ' => $data_post['comprospect_id']),
                        'method' => "row"
                    );
                    $data['com'] = $request = $this->CommunicationProspect_m->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['utilisateur'] = $this->getListUser();
                $data['typecom'] = $this->getlisteTypeCommunication();

                $this->renderComponant("Prospections/prospects/form-com", $data);
            }
        }
    }

    function cruCom()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('CommunicationProspect_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "action" => $action);
                    $data['etat_com'] = 1;
                    unset($data['action']);
                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("comprospect_id" => $data['comprospect_id']);
                        $data_retour['comprospect_id'] = $data['comprospect_id'];
                        unset($data['comprospect_id']);
                    } else {
                        $data['comprospect_date_creation'] = Carbon::now()->toDateTimeString();
                    }
                    $request = $this->CommunicationProspect_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Message ajouté" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeCom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('CommunicationProspect_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('comprospect_id' => $data['comprospect_id']),
                    'columns' => array('comprospect_id', 'comprospect_texte'),
                    'method' => "row",
                );
                $request_get = $this->CommunicationProspect_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['comprospect_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce message ?",
                            'btnConfirm' => "Supprimer le message",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('comprospect_id' => $data['comprospect_id']);
                    $data_update = array('etat_com' => 0);
                    $request = $this->CommunicationProspect_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['comprospect_id']),
                            'message' => "Message supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*******Tâche********/

    public function getFormTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prospect_m');
                $data_post = $_POST['data'];

                $data['prospect_id'] = $data_post['prospect_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('Tache_m');
                    $params = array(
                        'clause' => array('tache_id ' => $data_post['tache_id']),
                        'method' => "row"
                    );
                    $data['tache'] = $request = $this->Tache_m->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['utilisateur'] = $this->getListUser();
                $data['prospect'] = $this->getlisteProspect();
                $data['theme'] = $this->getlisteTheme();

                $data['prospects'] = $this->Prospect_m->get(
                    array('clause' => array("prospect_id" => $data_post['prospect_id']), 'method' => "row")
                );

                $this->renderComponant("Prospections/prospects/taches/form-tache", $data);
            }
        }
    }

    public function getlisteTheme()
    {
        $this->load->model('Theme_m');
        $params = array(
            'columns' => array('theme_id', 'theme_libelle')
        );
        $request = $this->Theme_m->get($params);
        return $request;
    }

    public function getlisteProspect()
    {
        $this->load->model('Prospect_m');
        $params = array(
            'clause' => array('etat_prospect' => 1),
            'columns' => array('prospect_id', 'prospect_prenom', 'prospect_nom')
        );
        $request = $this->Prospect_m->get($params);
        return $request;
    }

    public function liste_tache()
    {

        $this->load->model('Tache_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('tache_id', 'tache_nom', 'tache_date_echeance', 'tache_createur', 'tache_date_creation', 'participant', 'tache_prioritaire', 'tache_date_validationn'),
                    'clause' => array(
                        'prospect_id' => $data_post['prospect_id'],
                        'tache_etat' => 1,
                        'tache_valide' => 0
                    ),
                    'order_by_columns' => "tache_date_echeance",
                    // 'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_tache.tache_createur')
                );
                $request = $this->Tache_m->get($params);
                foreach ($request as $key => $value) {
                    if ($value->participant != "") {
                        $array_util = explode(',', $value->participant);
                        $utilisateur = array();
                        foreach ($array_util as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->utilisateur = implode(' , ', $utilisateur);
                    }
                }

                return $request;
            }
        }
    }

    public function liste_tache_valide()
    {

        $this->load->model('Tache_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('tache_id', 'tache_nom', 'tache_date_echeance', 'tache_date_creation', 'tache_createur', 'participant', 'tache_prioritaire', 'tache_date_validationn'),
                    'clause' => array(
                        'prospect_id' => $data_post['prospect_id'],
                        'tache_etat' => 1,
                        'tache_valide' => 1
                    ),
                    'order_by_columns' => "tache_date_echeance",
                    // 'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_tache.tache_createur')
                );
                $request = $this->Tache_m->get($params);
                foreach ($request as $key => $value) {
                    if ($value->participant != "") {
                        $array_util = explode(',', $value->participant);
                        $utilisateur = array();
                        foreach ($array_util as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->utilisateur = implode(' , ', $utilisateur);
                    }
                }

                return $request;
            }
        }
    }

    function getUtil($util_id)
    {
        $this->load->model('Utilisateur_m', 'util');
        $param = array(
            'clause' => array('util_id' => $util_id),
            'method' => 'row',
            'columns' => array('util_prenom'),
        );
        $request = $this->util->get($param);
        return (!empty($request)) ? $request->util_prenom : '';
    }

    function cruTache()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Tache_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "action" => $action);
                    $data['tache_etat'] = 1;
                    $data['tache_createur'] = $_SESSION['session_utilisateur']['util_prenom'];

                    $data['tache_date_creation'] = date('Y-m-d H:i:s', strtotime($data['tache_date_creation']));
                    $data['tache_date_echeance'] = date('Y-m-d H:i:s', strtotime($data['tache_date_echeance']));
                    $data['type_tache_id'] = 2;

                    unset($data['action']);
                    unset($data['search_terms']);
                    $data['participant'] = implode(',', $data['participant']);

                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("tache_id" => $data['tache_id']);
                        $data_retour['tache_id'] = $data['tache_id'];
                        unset($data['tache_id']);
                    }
                    // else{
                    //     $data['tache_date_creation'] = date_sql($data['tache_date_creation'],'-','jj/MM/aaaa');
                    // $data['tache_date_echeance'] = date_sql($data['tache_date_echeance'],'-','jj/MM/aaaa');
                    // }
                    $request = $this->Tache_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Tâche ajoutée avec succès" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Tache_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tache_id' => $data['tache_id']),
                    'columns' => array('tache_id', 'tache_nom'),
                    'method' => "row",
                );
                $request_get = $this->Tache_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tache_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce tâche ? ",
                            'btnConfirm' => "Supprimer la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tache_id' => $data['tache_id']);
                    $data_update = array('tache_etat' => 0);
                    $request = $this->Tache_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tache_id']),
                            'message' => "La tâche a bien été supprimée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function valideTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation de la tâche…",
                );
                $this->load->model('Tache_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tache_id' => $data['tache_id']),
                    'columns' => array('tache_id', 'tache_nom'),
                    'method' => "row",
                );
                $request_get = $this->Tache_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tache_id'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous vraiment valider cette tâche ? ",
                            'btnConfirm' => "Valider la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tache_id' => $data['tache_id']);
                    $data_update = array(
                        'tache_valide' => 1,
                        'tache_date_validationn' => Carbon::now()->toDateTimeString()
                    );
                    $request = $this->Tache_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tache_id']),
                            'message' => "Tâche validée avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /*************Prospect supprimé***********/

    public function prospectSupp()
    {

        $this->_data['menu_principale'] = "prospects";
        $this->_data['sous_module'] = "prospects/prospectSupp/prospect-supp";

        $this->render('prospects/prospectSupp/contenaire');
    }

    public function listeProspectSupp()
    {

        $this->_data['menu_principale'] = "prospects";
        $clause = array('etat_prospect' => 0);
        $data = array();
        $this->load->model('Prospect_m');
        $params = array(
            'clause' => $clause,
            'columns' => array(
                'prospect_id',
                'prospect_nom',
                'prospect_prenom',
                'prospect_date_creation',
                'prospect_email',
                'prospect_email1',
                'prospect_phonemobile',
                'prospect_entreprise',
                'prospect_phonefixe',
                'produit_libelle',
                'origine_dem_libelle',
                'util_prenom',
                'origine_cli_libelle',
                'vente_libelle',
                'etat_prospect_libelle',
                'etat_couleur',
                'c_origine_demande.origine_dem_id',
                'c_produit.produit_id',
                'c_parcours_vente.vente_id',
                'c_etat_prospect.etat_prospect_id',
                'c_utilisateur.util_id',
                'c_origine_client.origine_cli_id',
                'c_typeprospect.type_prospect_id',
                'type_prospect_libelle'
            ),
            'join' => array(
                'c_origine_demande' => 'c_origine_demande.origine_dem_id = c_prospect.origine_dem_id',
                'c_produit' => 'c_produit.produit_id = c_prospect.produit_id',
                'c_origine_client' => 'c_origine_client.origine_cli_id = c_prospect.origine_cli_id',
                'c_parcours_vente' => 'c_parcours_vente.vente_id = c_prospect.vente_id',
                'c_etat_prospect' => 'c_etat_prospect.etat_prospect_id = c_prospect.etat_prospect_id',
                'c_typeprospect' => 'c_typeprospect.type_prospect_id = c_prospect.type_prospect_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id'
            ),
            'order_by_columns' => "prospect_id DESC",
        );

        $data['prospect'] = $this->Prospect_m->get($params);

        $this->renderComponant('Prospections/prospects/prospectSupp/liste', $data);
    }

    public function activerProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la réactivation…",
                );
                $this->load->model('Prospect_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('prospect_id' => $data['prospect_id']),
                    'columns' => array('prospect_id', 'prospect_nom'),
                    'method' => "row",
                );
                $request_get = $this->Prospect_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['prospect_id'],
                            'title' => "Confirmation",
                            'text' => "Souhaitez-vous vraiment récupérer ce prospect ?",
                            'btnConfirm' => "Récupérer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('prospect_id' => $data['prospect_id']);
                    $data_update = array('etat_prospect' => 1);
                    $request = $this->Prospect_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['prospect_id']),
                            'message' => "Prospect récupéré avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    /***Document**/

    public function listDocument()
    {
        $this->load->model('DocumentProspect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('docp_id', 'docp_nom', 'docp_path', 'docp_creation', 'util_prenom', 'c_utilisateur.util_id'),
                    'clause' => array(
                        'prospect_id' => $data_post['prospect_id'],
                        'docp_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_prospect.util_id')
                );

                $data["prospect_id"] = $data_post['prospect_id'];
                $data['document'] = $this->DocumentProspect_m->get($params);

                $this->renderComponant("Prospections/prospects/list-document", $data);
            }
        }
    }

    public function formUpload()
    {
        $data_post = $_POST['data'];
        $data['prospect_id'] = $data_post['prospect_id'];

        $this->renderComponant('Prospections/prospects/formulaire_upload_file', $data);
    }

    public function uploadfile()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        // $cli_id = $_POST['cli_id'];
        $prospect_id = $_POST['prospect_id'];

        // Chemin du dossier
        $targetDir = "../documents/prospections/$prospect_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'docp_nom' => $original_name,
                'docp_creation' => date('Y-m-d H:i:s'),
                'docp_path' => str_replace('\\', '/', $filePath),
                'docp_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'prospect_id' => $prospect_id
            );

            $this->load->model('DocumentProspect_m');
            $this->DocumentProspect_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function getDocpath()
    {
        $this->load->model('DocumentProspect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('docp_id', 'docp_path'),
                    'clause' => array('docp_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentProspect_m->get($params);
                echo json_encode($request);
            }
        }
    }

    function removeDoc()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentProspect_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('docp_id' => $data['docp_id']),
                    'columns' => array('docp_id', 'docp_nom'),
                    'method' => "row",
                );
                $request_get = $this->DocumentProspect_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['docp_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('docp_id' => $data['docp_id']);
                    $data_update = array('docp_etat' => 0);
                    $request = $this->DocumentProspect_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['docp_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formDocument()
    {
        $this->load->model('DocumentProspect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['prospect_id'] = $data_post['prospect_id'];

                $this->load->model('DocumentProspect_m');
                $data['document'] = $this->DocumentProspect_m->get(
                    array('clause' => array("docp_id" => $data_post['docp_id']), 'method' => "row")
                );
            }

            $this->renderComponant("Prospections/prospects/form-document", $data);
        }
    }

    public function UpdateDocument()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentProspect_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("prospect_id" => $data['prospect_id']);
                    $docp_id = $data['docp_id'];
                    unset($data['docp_id']);

                    $clause = array("docp_id" => $docp_id);
                    $request = $this->DocumentProspect_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function downloadFile($docp_id)
    {

        $this->load->helper("download");
        $clause = array("docp_id" => $docp_id);
        $file = $this->DocumentProspect_m->get_methode(
            $clause,
            $clause_or = NULL,
            $like = NULL,
            $or_like = NULL,
            $method = "row_array",
            $fields = "*",
            $joinTables = NULL
        );
        download_file($file);
    }

    public function transformerClient()
    {

        $this->load->model('Prospect_m');
        $this->load->model('Client_m');
        $this->load->model('Lot_m');
        $this->load->model('ClientLot_m');
        $this->load->model('Tache_m');
        $this->load->model('MandatNew_m');

        $this->load->model('CommunicationProspect_m');
        $this->load->model('CommunicationClient_m');
        $this->load->model('Dossier_m', 'dossier');
        $this->load->model('DocumentActeProspect_m');
        $this->load->model('DocumentBailProspect_m');
        $this->load->model('DocumentActe_m');
        $this->load->model('DocumentBail_m');
        $this->load->model('Pno_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('prospect_id' => $data['prospect_id']),
                    'method' => "row",
                    'join' => array(
                        'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id'
                    )
                );
                $prospectInfo = $this->Prospect_m->get($params);
                if ($data['action'] == "demande") {

                    $hasEmptyError = false;
                    $data_error = array();
                    // verify information perso
                    if (
                        !isset($prospectInfo->prospect_nom) ||
                        !isset($prospectInfo->prospect_prenom) ||
                        !isset($prospectInfo->prospect_email) ||
                        !isset($prospectInfo->prospect_phonemobile) ||
                        !isset($prospectInfo->prospect_adresse1) ||
                        !isset($prospectInfo->prospect_ville) ||
                        !isset($prospectInfo->prospect_pays) ||
                        !isset($prospectInfo->prospect_nationalite)
                    ) {
                        array_push($data_error, "vos informations personnelles ");
                        $hasEmptyError = true;
                    }

                    if (!isset($prospectInfo->cin)) {
                        array_push($data_error, "votre carte nationale d'identité (CNI)");
                        $hasEmptyError = true;
                    }

                    // verify 1 lot and date signature
                    $paramsLot = array(
                        'clause' => array('prospect_id' => $data['prospect_id'], 'etat_lot' => 1),
                        'join' => array(
                            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_prospect.gestionnaire_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id',
                            'c_typelot' => 'c_typelot.typelot_id = c_lot_prospect.typelot_id',
                            'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_prospect.typologielot_id'
                        ),
                    );
                    $lotProspect = $this->Lot_m->get($paramsLot);
                    if (count($lotProspect) == 0) {
                        // $data_error .= "au moins un lot, ";
                        array_push($data_error, "au moins un lot");
                        $hasEmptyError = true;
                    } else {
                        foreach ($lotProspect as $lot) {
                            $paramsMandat = array(
                                'clause' => array(
                                    'lot_id' => $lot->lot_id,
                                ),
                                'columns' => array('mandat_date_signature'),
                                'method' => 'row'
                            );
                            $mandat = $this->MandatNew_m->get($paramsMandat);
                            if ($mandat && $mandat->mandat_date_signature == null) {
                                // $data_error .= "la date de signature de votre lot, ";
                                if (!in_array("la date de signature de votre mandat", $data_error))
                                    array_push($data_error, "la date de signature de votre mandat");
                                $hasEmptyError = true;
                            }

                            // verify acte, bail at least 1 document
                            $paramDoc = array(
                                'clause' => array(
                                    'lot_id' => $lot->lot_id,
                                ),
                            );

                            $paramMandats = array(
                                array('prospect_id' => $data['prospect_id'])
                            );

                            $docActe = $this->DocumentActeProspect_m->get($paramDoc);
                            $docBail = $this->DocumentBailProspect_m->get($paramDoc);
                            $Mandats =  $this->MandatNew_m->get($paramMandats);

                            if (count($docActe) == 0) {
                                // $data_error .= "au moins un document uploadé dans Acte, ";
                                if (!in_array("au moins un document dans le dossier acte du lot", $data_error))
                                    array_push($data_error, "au moins un document dans le dossier acte du lot");
                                $hasEmptyError = true;
                            }

                            if (count($docBail) == 0) {
                                // $data_error .= "au moins un document uploadé dans Bail, ";
                                if (!in_array("au moins un document dans le dossier bail du lot", $data_error))
                                    array_push($data_error, "au moins un document dans le dossier bail du lot");
                                $hasEmptyError = true;
                            }

                            if (count($Mandats) == 0) {
                                // $data_error .= "au moins un document uploadé dans Bail, ";
                                if (!in_array("au moins un mandat créé dans le lot", $data_error))
                                    array_push($data_error, "au moins un mandat créé dans le lot");
                                $hasEmptyError = true;
                            }

                            // if (count($docMandats) == 0) {
                            //     // $data_error .= "au moins un document uploadé dans Bail, ";
                            //     if (!in_array("au moins un document dans le dossier mandat du lot", $data_error))
                            //         array_push($data_error, "au moins un document dans le dossier mandat du lot");
                            //     $hasEmptyError = true;
                            // }
                        }
                    }

                    if ($hasEmptyError) {
                        $retour = array(
                            'status' => 200,
                            'action' => "error",
                            'data' => array(
                                'id' => $data['prospect_id'],
                                'title' => "Informations requises",
                                'text' => $data_error,
                                'btnConfirm' => "Confirmer",
                                'btnAnnuler' => "Annuler"
                            ),
                            'message' => $data_error,
                        );
                    } else {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['prospect_id'],
                                'title' => "Confirmation de la transformation",
                                'text' => "Voulez-vous vraiment transformer ce prospect en client ?  Ses dossiers seront suivis par : " . $prospectInfo->util_nom . " " . $prospectInfo->util_prenom,
                                'btnConfirm' => "Confirmer",
                                'btnAnnuler' => "Annuler"
                            ),
                            'message' => "",
                        );
                    }
                } else if ($data['action'] == "confirm") {
                    $paramsProspect = array(
                        'clause' => array('prospect_id' => $data['prospect_id']),
                        'method' => 'row'
                    );
                    $request_prospect = $this->Prospect_m->get($paramsProspect);

                    $params = array(
                        'clause' => array('prospect_id' => $data['prospect_id']),
                    );

                    $paramsLot = array(
                        'clause' => array('prospect_id' => $data['prospect_id'], 'etat_lot' => 1),
                        'join' => array(
                            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_prospect.gestionnaire_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_prospect.progm_id',
                            'c_typelot' => 'c_typelot.typelot_id = c_lot_prospect.typelot_id',
                            'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_prospect.typologielot_id'
                        ),
                    );

                    $request_lot = $this->Lot_m->get($paramsLot);
                    $request_tache = $this->Tache_m->get($params);
                    $request_com = $this->CommunicationProspect_m->get($params);
                    $request_pno = $this->Pno_m->get($params);

                    // change prospect state to 2 (means as client)
                    $clause = array('prospect_id' => $data['prospect_id']);
                    $data_update = array('etat_prospectcli' => 2);
                    $this->Prospect_m->save_data($data_update, $clause);

                    // Get all data into prospect => client
                    $array = array(
                        'client_nom' => $request_prospect->prospect_nom,
                        'client_prenom' => $request_prospect->prospect_prenom,
                        'client_date_creation' => Carbon::now()->toDateTimeString(),
                        'client_email' => $request_prospect->prospect_email,
                        'client_email1' => $request_prospect->prospect_email1,
                        'client_entreprise' => $request_prospect->prospect_entreprise,
                        'client_situation_matrimoniale' => $request_prospect->prospect_siutation,
                        'client_phonemobile' => $request_prospect->prospect_phonemobile,
                        'client_phonefixe' => $request_prospect->prospect_phonefixe,
                        'client_adresse1' => $request_prospect->prospect_adresse1,
                        'client_adresse2' => $request_prospect->prospect_adresse2,
                        'client_adresse3' => $request_prospect->prospect_adresse3,
                        'client_cp' => $request_prospect->prospect_cp,
                        'client_ville' => $request_prospect->prospect_ville,
                        'client_pays' => $request_prospect->prospect_pays,
                        'client_nationalite' => $request_prospect->prospect_nationalite,
                        'client_nationalite' => $request_prospect->prospect_nationalite,
                        'client_residence_fiscale' => $request_prospect->prospect_residence_fiscale,
                        'client_commentaire' => $request_prospect->prospect_commentaire,
                        'origine_cli_id' => $request_prospect->origine_cli_id,
                        'paysmonde_id' => $request_prospect->paysmonde_id,
                        'pays_id' => $request_prospect->pays_id,
                        'etat_client' => 1,
                        'util_id' => $request_prospect->util_id,
                        'cin' => $request_prospect->cin,
                    );

                    $request_client = $this->Client_m->save_data($array);
                    $client_id = $this->db->insert_id();

                    $data_update_prospect = array('prospect_after_id' => $client_id);
                    $clause_prospect = array('prospect_id' => $data['prospect_id']);
                    $this->Prospect_m->save_data($data_update_prospect, $clause_prospect);

                    $data_dossier = array(
                        'sie_id' => 1,
                        'dossier_type_adresse' => "Lot principal",
                        'dossier_d_creation' => Carbon::now()->toDateTimeString(),
                        'dossier_etat' => 1,
                        'client_id' => $client_id,
                        'util_id' => $data['util_id'],
                        'iban' => $request_prospect->prospect_iban,
                        'bic' => $request_prospect->prospect_bic,
                        'file_rib' => $request_prospect->file_rib
                    );
                    $this->dossier->save_data($data_dossier);
                    $dossier_id = $this->db->insert_id();

                    // Transfer lot and all data
                    foreach ($request_lot as $key => $value) {
                        $array_lot = array(
                            'cliLot_nom' => $value->lot_nom,
                            'cliLot_principale' => $value->lot_principale,
                            'typologielot_id' => $value->typologielot_id,
                            'dossier_id' => $dossier_id,
                            'typelot_id' => $value->typelot_id,
                            'cliLot_num' => $value->lot_num,
                            'cliLot_num_parking' => $value->lot_num_parking,
                            'progm_id' => $value->progm_id,
                            'gestionnaire_id' => $value->gestionnaire_id,
                            'produitcli_id' => 4,
                            'cliLot_etat' => 1,
                            'cli_id' => $client_id,
                            'cliLot_encaiss_loyer' => 'Compte client',
                            'cliLot_mode_paiement' => 'Virement',
                            'cliLot_iban_client' => $request_prospect->prospect_iban,
                            'cliLot_bic_client' => $request_prospect->prospect_bic,
                            'rib' => $request_prospect->file_rib
                        );

                        $request_lotcli = $this->ClientLot_m->save_data($array_lot);
                        $last_lot_id = $this->db->insert_id();

                        $this->mergeMandat($value->lot_id, $request_lotcli, $client_id);

                        // verify acte, bail at least 1 document
                        $paramDoc = array(
                            'clause' => array(
                                'lot_id' => $value->lot_id,
                            ),
                        );

                        $docActes = $this->DocumentActeProspect_m->get($paramDoc);
                        foreach ($docActes as $docActe) {
                            $array_doc_acte = array(
                                'doc_acte_nom' => $docActe->docp_acte_nom,
                                'doc_acte_path' => $docActe->docp_acte_path,
                                'doc_acte_creation' => $docActe->docp_acte_creation,
                                'doc_acte_etat' => $docActe->docp_acte_etat,
                                'util_id ' => $docActe->util_id,
                                'cliLot_id ' => $last_lot_id,
                            );
                            $this->DocumentActe_m->save_data($array_doc_acte);
                        }

                        $docBails = $this->DocumentBailProspect_m->get($paramDoc);
                        foreach ($docBails as $docBail) {
                            $array_doc_bail = array(
                                'doc_bail_nom' => $docBail->docp_bail_nom,
                                'doc_bail_path' => $docBail->docp_bail_path,
                                'doc_bail_creation' => $docBail->docp_bail_creation,
                                'doc_bail_etat' => $docBail->docp_bail_etat,
                                'util_id ' => $docBail->util_id,
                                'cliLot_id ' => $last_lot_id,
                            );
                            $this->DocumentBail_m->save_data($array_doc_bail);
                        }

                        $request_mandat = $this->MandatNew_m->get($paramDoc);

                        foreach ($request_mandat as $mandat) {
                            $array_mandat = array(
                                'client_id' => $client_id,
                                'cliLot_id' => $last_lot_id,
                                'mandat_type_id' => 1
                            );
                            $this->MandatNew_m->save_data($array_mandat, array('mandat_id' => $mandat->mandat_id));
                        }
                    }

                    foreach ($request_pno as $pno) {
                        $array_pno = array(
                            'client_id' => $client_id,
                            'cliLot_id' => $last_lot_id
                        );
                        $this->Pno_m->save_data($array_pno, array('pno_id' => $pno->pno_id));
                    }

                    foreach ($request_com as $key => $value) {
                        $array_com = array(
                            'comclient_texte' => $value->comprospect_texte,
                            'comclient_date_creation' => $value->comprospect_date_creation,
                            'etatcli_com' => 1,
                            'client_id' => $client_id,
                            'util_id' => $value->util_id,
                            'type_comprospect_id' => $value->type_comprospect_id,
                        );

                        $this->CommunicationClient_m->save_data($array_com);
                    }

                    foreach ($request_tache as $key => $value) {
                        $array_tache = array(
                            'tache_nom' => $value->tache_nom,
                            'tache_date_creation' => $value->tache_date_creation,
                            'tache_date_echeance' => $value->tache_date_echeance,
                            'tache_date_validationn' => $value->tache_date_validationn,
                            'tache_prioritaire' => $value->tache_prioritaire,
                            'tache_texte' => $value->tache_texte,
                            'tache_realise' => $value->tache_realise,
                            'tache_valide' => $value->tache_valide,
                            'tache_createur' => $value->tache_createur,
                            'client_id' => $client_id,
                            'client_id_associe' => $value->prospect_id_associe,
                            'tache_etat' => $value->tache_etat,
                            'type_tache_id' => 3,
                            'theme_id' => $value->theme_id,
                            'client_participant' => $value->prospect_participant,
                            'participant' => $value->participant,
                        );

                        $this->Tache_m->save_data($array_tache);
                    }

                    if ($request_client) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $client_id, 'url' => base_url('Clients/ficheClient?client=' . $client_id)),
                            'message' => "Prospect transformé avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function mergeMandat($lot_id, $new_lot_id, $client_id)
    {
        $this->load->model('Mandat_m');
        $this->load->model('MandatCli_m');
        // request get mandat bay lot id
        $params = array(
            'clause' => array(
                'lot_id' => $lot_id,
                'mandat_etat' => 1
            ),
        );
        $request = $this->Mandat_m->get($params);
        // if request not empty
        if (!empty($request)) {
            foreach ($request as $key => $value) {
                $data = array(
                    'mandat_cli_date_creation' => $value->mandat_date_creation,
                    'mandat_cli_lieu_creation' => $value->mandat_lieu_creation,
                    'mandat_cli_montant' => $value->mandat_montant,
                    'mandat_cli_etat' => $value->mandat_etat,
                    'mandat_cli_path' => $value->mandat_path,
                    'client_id' => $client_id,
                    'type_mandat_id' => $value->type_mandat_id,
                    'cliLot_id' => $new_lot_id,

                );
            }

            $requestcli = $this->MandatCli_m->save_data($data);
            return $requestcli;
        }

        // store data in array and caste value array['lot_id'] =>  new_lot_id
        // insert new array 
    }

    function updateDateSignatureMandat()
    {
        $this->load->model('Mandat_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la mise à jour",
                );

                $data = decrypt_service($_POST["data"]);

                $post['mandat_date_signature'] = $data['mandat_date_signature'];
                $clause = array("lot_id" => $data['lot_id']);

                $success = $this->Mandat_m->save_data($post, $clause);

                if ($success) {
                    $retour = array(
                        'status' => 200,
                        'data' => array('lot_id' => $data['lot_id']),
                        'message' => "Prospect transformé avec succès",
                    );
                }
                echo json_encode($retour);
            }
        }
    }

    public function verifier_compte()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m');
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Attention, les paramétrages SMTP (messagerie) ne sont pas réalisés pour le collaborateur sélectionné. Veuillez modifier le paramétrage SMTP de ce collaborateur avant la transformation en client ou décocher l'envoi du mail de bienvenue.",
                );
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('c_utilisateur.util_id' => $data_post['util_id']),
                    'join' => array(
                        'c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id',
                    ),
                    'method' => 'row'
                );
                $user_resposable = $this->Utilisateur_m->get($params);
                if ($user_resposable) {
                    $retour = array(
                        'status' => 200,
                        'data' => $user_resposable,
                        'data_mail' => $data_post['donnee'],
                        'message' => "Prospect transformé avec succès",
                    );
                }
                echo json_encode($retour);
            }
        }
    }

    public function alert_compte_parametre()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['message'] = $_POST['data']['message'];
                echo json_encode($data['message']);
            }
        }
    }

    function getFormMail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->renderComponant("Prospections/prospects/formMailBienvenu");
            }
        }
    }

    public function prospectConverti()
    {

        $this->_data['menu_principale'] = "prospects";
        $this->_data['sous_module'] = "prospects/prospectConverti/prospect-converti";

        $this->render('prospects/prospectSupp/contenaire');
    }

    public function listeProspectConverti()
    {

        $this->_data['menu_principale'] = "prospects";
        $clause = array('etat_prospectcli' => 2);
        $data = array();
        $this->load->model('Prospect_m');
        $params = array(
            'clause' => $clause,
            'columns' => array(
                'prospect_id',
                'prospect_nom',
                'prospect_prenom',
                'prospect_date_creation',
                'prospect_email',
                'prospect_email1',
                'prospect_phonemobile',
                'prospect_entreprise',
                'prospect_phonefixe',
                'produit_libelle',
                'origine_dem_libelle',
                'util_prenom',
                'origine_cli_libelle',
                'vente_libelle',
                'etat_prospect_libelle',
                'etat_couleur',
                'c_origine_demande.origine_dem_id',
                'c_produit.produit_id',
                'c_parcours_vente.vente_id',
                'c_etat_prospect.etat_prospect_id',
                'c_utilisateur.util_id',
                'c_origine_client.origine_cli_id',
                'c_typeprospect.type_prospect_id',
                'type_prospect_libelle'
            ),
            'join' => array(
                'c_origine_demande' => 'c_origine_demande.origine_dem_id = c_prospect.origine_dem_id',
                'c_produit' => 'c_produit.produit_id = c_prospect.produit_id',
                'c_origine_client' => 'c_origine_client.origine_cli_id = c_prospect.origine_cli_id',
                'c_parcours_vente' => 'c_parcours_vente.vente_id = c_prospect.vente_id',
                'c_etat_prospect' => 'c_etat_prospect.etat_prospect_id = c_prospect.etat_prospect_id',
                'c_typeprospect' => 'c_typeprospect.type_prospect_id = c_prospect.type_prospect_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id'
            ),
            'order_by_columns' => "prospect_id DESC",
        );

        $data['prospect'] = $this->Prospect_m->get($params);

        $this->renderComponant('Prospections/prospects/prospectConverti/liste', $data);
    }

    public function prospectPerdu()
    {

        $this->_data['menu_principale'] = "prospects";
        $this->_data['sous_module'] = "prospects/prospectPerdu/prospect-perdu";

        $this->render('prospects/prospectPerdu/contenaire');
    }

    public function listeProspectPerdu()
    {

        $this->_data['menu_principale'] = "prospects";
        $clause = array('c_prospect.etat_prospect_id' => 5);
        $data = array();
        $this->load->model('Prospect_m');
        $params = array(
            'clause' => $clause,
            'columns' => array(
                'prospect_id',
                'prospect_nom',
                'prospect_prenom',
                'prospect_date_creation',
                'prospect_email',
                'prospect_email1',
                'prospect_phonemobile',
                'prospect_entreprise',
                'prospect_phonefixe',
                'produit_libelle',
                'origine_dem_libelle',
                'util_prenom',
                'origine_cli_libelle',
                'vente_libelle',
                'etat_prospect_libelle',
                'etat_couleur',
                'c_origine_demande.origine_dem_id',
                'c_produit.produit_id',
                'c_parcours_vente.vente_id',
                'c_etat_prospect.etat_prospect_id',
                'c_utilisateur.util_id',
                'c_origine_client.origine_cli_id',
                'c_typeprospect.type_prospect_id',
                'type_prospect_libelle'
            ),
            'join' => array(
                'c_origine_demande' => 'c_origine_demande.origine_dem_id = c_prospect.origine_dem_id',
                'c_produit' => 'c_produit.produit_id = c_prospect.produit_id',
                'c_origine_client' => 'c_origine_client.origine_cli_id = c_prospect.origine_cli_id',
                'c_parcours_vente' => 'c_parcours_vente.vente_id = c_prospect.vente_id',
                'c_etat_prospect' => 'c_etat_prospect.etat_prospect_id = c_prospect.etat_prospect_id',
                'c_typeprospect' => 'c_typeprospect.type_prospect_id = c_prospect.type_prospect_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_prospect.util_id'
            ),
            'order_by_columns' => "prospect_id DESC",
        );

        $data['prospect'] = $this->Prospect_m->get($params);

        $this->renderComponant('Prospections/prospects/prospectPerdu/liste', $data);
    }
}
