<?php echo form_tag('Prospections/updateInfoProspect', array('method' => "post", 'class' => 'px-2', 'id' => 'updateInfoProspect')); ?>

<div class="contenair-title">
	<div class="px-2 menu-header">
		<h5 class="fs-0 bold" style="top:-9px;">Parcours vente : &nbsp;</h5>
		<nav>
			<ul class="nav nav-pills menu-web">
				<?php foreach ($parcours as $key => $parcour) : ?>
					<?php if ($key != 0) : ?>
						<i class="fas fa-forward mt-1" id="<?= $parcour->vente_id ?>icone_pine"></i>
					<?php endif; ?>
					<li class=" nav-item menu-item d-flex flex-column justify-content-center">
						<a href="javascript:void(0);" data-id="<?= $parcour->vente_id ?>" data-prospect="<?= $prospect->prospect_id ?>" class="m-0 w-100 rounded-1 d-flex 
									 justify-content-center<?= $parcour->vente_libelle == $prospect->vente_libelle ? " bg-primary px-2 text-white " : ""; ?>" style="font-size : 14px !important ; cursor: auto;">
							<span>
								<?= $parcour->vente_libelle; ?>
							</span>
						</a>
						<span class="fs--1" style="font-style: italic">
							<?= ($parcour->vente_libelle == $prospect->vente_libelle && !empty($prospect->prospect_date_modification)) ? date("d/m/Y", strtotime(str_replace('-', '/', $prospect->prospect_date_modification))) : ""; ?>
						</span>
					</li>
				<?php endforeach; ?>
			</ul>
		</nav>
	</div>
	<div class="px-2">
		<div class="form-group">
			<div class="col-sm-12 px-1 pt-1">
				<?php if ($prospect->etat_prospect_id == 1) {
					echo '<span class="badge badge-soft-danger fs--1 py-2">' . $prospect->etat_prospect_libelle . '</span>';
				} else if ($prospect->etat_prospect_id == 2) {
					echo '<span class="badge badge-soft-warning fs--1 py-2">' . $prospect->etat_prospect_libelle . '</span>';
				} else if ($prospect->etat_prospect_id == 3) {
					echo '<span class="badge badge-soft-secondary fs--1 py-2" style="background-color : #E4DF19">' . $prospect->etat_prospect_libelle . '</span>';
				} else
					echo '<span class="badge badge-soft-success fs--1 py-2">' . $prospect->etat_prospect_libelle . '</span>'; ?>
				<div class="help"></div>
			</div>
		</div>
		<button type="button" data-id="<?= $prospect->prospect_id; ?>" id="btn-update-prospect" class="btn btn-sm btn-primary btn-update-prospect only_visuel_gestion" style="width : 170px;">
			<span class="fas fa-edit fas me-1"></span>
			<span> Modifier </span>
		</button>
	</div>
</div>

<input type="hidden" name="prospect_id" id="prospect_id" value="<?= $prospect->prospect_id; ?>">
<div id="div-fiche-prospect" class="row m-0">
	<?php

	$phoneNumber = $prospect->prospect_phonemobile;
	$numero;
	if ($prospect->paysmonde_id == 66) {
		$array = str_split($phoneNumber, 2);
		$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
		$numero = implode(".", $array);
	} else {
		$numero = $prospect->prospect_phonemobile;
	}

	if ($prospect->prospect_phonefixe != "") {
		$phone = $prospect->prospect_phonefixe;
		if ($prospect->pays_id == 66) {
			$array = str_split($phone, 2);
			$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
			$number = implode(".", $array);
		} else {
			$number = $prospect->prospect_phonefixe;
		}
	}

	?>
	<div class="col-12 p-0 py-1">

		<div class="row m-0">

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Identité</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Nom : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_nom ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Prénom : </th>
									<th class="pe-0 text-end"><?= $prospect->prospect_prenom ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Type prospect : </th>
									<th class="pe-0 text-end">
										<?= $prospect->type_prospect_libelle ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Entreprise : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_entreprise ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Nationnalité : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_nationalite ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Résidence fiscale : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_residence_fiscale ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Situation matrimoniale : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_siutation ?>
									</th>
								</tr>
								
									<tr class="border-bottom">
										<?php $cin = explode("/", $prospect->cin) ?>
										<?php $nom_fic = $prospect->cin != NULL ? $cin[3] : 'Pas de fichier'; ?>
										<th class="ps-0">CNI : </th>
										<th class="pe-0 text-end">
											<?php if ($nom_fic == 'Pas de fichier') : ?>
												<?= $nom_fic ?>
											<?php else : ?>
												<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $prospect->cin ?>')" data-url="<?= base_url($prospect->cin) ?>" style="cursor: pointer;"><?= $nom_fic ?></span>
											<?php endif ?>
										</th>
									</tr>
									<tr class="border-bottom">
										<th class="ps-0">Date de fin de validité CNI : </th>
										<th class="pe-0 text-end">
											<?= $prospect->prospect_date_fin_validite_cni == NULL || $prospect->prospect_date_fin_validite_cni == '0000-00-00' ? "" : date("d/m/Y", strtotime(str_replace('-', '/', $prospect->prospect_date_fin_validite_cni))) ?>
										</th>
									</tr>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0">
					<div class="card-body ">
						<h5 class="fs-0">Contacts</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Email 1 : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_email ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Email 2 : </th>
									<th class="pe-0 text-end"><?= $prospect->prospect_email1 ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0" value="">Télephone 1 : </th>
									<th class="pe-0 text-end">
										<?= $prospect->paysmonde_indicatif ?> <?= $numero ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Télephone 2 : </th>
									<th class="pe-0 text-end">
										<?= (trim($prospect->prospect_phonefixe) != "") ? $prospect->pays_indicatif . ' ' . $number : ''; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<h5 class="fs-0 mt-4">Adresses</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 1 : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_adresse1 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 2 : </th>
									<th class="pe-0 text-end" <?= $prospect->prospect_adresse2 ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 3 : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_adresse3 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Code postal : </th>
									<th class="pe-0 text-end">
										<?= $prospect->prospect_cp ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Ville : </th>
									<th class="pe-0 text-end"><?= $prospect->prospect_ville ?></th>
								</tr>
								<tr>
									<th class="ps-0 pb-0">Pays : </th>
									<th class="pe-0 text-end pb-0">
										<?= $prospect->prospect_pays ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Informations de facturation</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">IBAN : </th>
									<th class="pe-0 text-end">
										<?= formatIBAN($prospect->prospect_iban) ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">BIC : </th>
									<th class="pe-0 text-end"><?= $prospect->prospect_bic ?></th>
								</tr>	
								<tr class="border-bottom">
									<?php $rib = explode("/", $prospect->file_rib) ?>
									<?php $nom_fic_rib = $prospect->file_rib != NULL ? $rib[3] : 'Pas de fichier'; ?>
									<th class="ps-0">RIB : </th>
									<th class="pe-0 text-end">
										<?php if ($nom_fic_rib == 'Pas de fichier') : ?>
											<?= $nom_fic_rib ?>
										<?php else : ?>
											<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $prospect->file_rib ?>')" data-url="<?= base_url($prospect->file_rib) ?>" style="cursor: pointer;"><?= $nom_fic_rib ?></span>
										<?php endif ?>
									</th>
								</tr>							
							</tbody>
						</table>

						<h5 class="fs-0 mt-4">Autres informations</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Origine demande : </th>
									<th class="pe-0 text-end">
										<?= $prospect->origine_dem_libelle; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Origine prospect : </th>
									<th class="pe-0 text-end"><?= $prospect->origine_cli_libelle; ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Prestation demandée : </th>
									<th class="pe-0 text-end">
										<?php
										$produit_id = explode(',', $prospect->produit_id);
										$filtre_produit = array_filter($produit, function ($produit_row) use ($produit_id) {
											return in_array($produit_row->produit_id, $produit_id);
										});
										$nom = array();
										foreach ($filtre_produit as $key => $value) {
											array_push($nom, $value->produit_libelle);
										}
										echo implode(', ', $nom);
										?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Date création : </th>
									<th class="pe-0 text-end"><?= date("d/m/Y", strtotime(str_replace('-', '/', $prospect->prospect_date_creation))); ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Suivi par : </th>
									<th class="pe-0 text-end">
										<?= $prospect->util_nom . ' ' . $prospect->util_prenom; ?>
									</th>
								</tr>
							</tbody>
						</table>

						<h5 class="fs-0 mt-4">Commentaire</h5>

						<a class="border-bottom-0 notification-unread notification rounded-0 border-x-0 border-300 mt-2">
							<div class="notification-body">
								<p class="mb-1 ">
									<?= $prospect->prospect_commentaire; ?>
								</p>
							</div>
						</a>

					</div>
				</div>

			</div>

		</div>

	</div>
</div>
<?php echo form_close(); ?>

<script>
	only_visuel_gestion();
</script>