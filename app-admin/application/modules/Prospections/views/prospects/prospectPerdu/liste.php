<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Liste des prospects perdus</h5>
    </div>
    <div class="px-2">
        <a href="<?php echo base_url('Prospections/Prospections'); ?>" class="btn btn-sm btn-secondary fs--1 "
            style="color :#ffffff; ">
            <i class="fas fa-undo me-1"></i> Retour
        </a>
    </div>
</div>
<div class="container-list p-1">
    <div class="table-responsive scrollbar">
        <table class="table table-sm table-striped fs--1 mb-0">
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Prospect (Entreprise/Nom)</th>
                    <th scope="col">Contacts</th>
                    <th scope="col">Origine demande</th>
                    <th scope="col">Prestation demandée</th>
                    <th scope="col">Date création</th>
                    <th scope="col">Parcours vente</th>
                    <th scope="col">Etat</th>
                    <th scope="col">Suivi par</th>
                </tr>
            </thead>

            <tbody id="myTable">
                <?php foreach ($prospect as $prospects): ?>
                    <tr id="rowProspect-<?= $prospects->prospect_id; ?>" class="align-middle">

                        <td>
                            <h6 class="mb-0 fw-semi-bold"><a class="stretched-link text-900"
                                    href="#"><?= $prospects->prospect_entreprise; ?></a></h6>
                            <p class="text-800 fs--1 mb-0"> <?= $prospects->prospect_nom . ' ' . $prospects->prospect_prenom; ?>
                            </p>
                        </td>
                        <td>
                            <h6 class="mb-0 fw-semi-bold">
                                <i class="fas fa-envelope fa-w-16 text-primary"></i> Email :
                                <span class="px-2"> <?= $prospects->prospect_email; ?> </span>
                            </h6>
                            <p class="text-800 fs--1 mb-0">
                                <i class="fas fa-phone-alt fa-w-16 text-primary"></i> Téléphone(s) :
                                <span class="px-2"> <?= $prospects->prospect_phonemobile; ?>
                                    <?=(trim($prospects->prospect_phonefixe) != "") ? ' / ' . $prospects->prospect_phonefixe : ''; ?></span>
                            </p>
                        </td>
                        <td><?= $prospects->origine_dem_libelle; ?></td>
                        <td><?= $prospects->produit_libelle; ?></td>
                        <td>
                            <?= date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $prospects->prospect_date_creation))); ?>
                        </td>
                        <td><?= $prospects->vente_libelle; ?></td>
                        <td>
                            <?php echo '<span class="badge badge-soft-primary fs--1">' . $prospects->etat_prospect_libelle . '</span>'; ?>
                        </td>
                        <td class="text-center"><?= $prospects->util_prenom; ?></td>                       
                    </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    </div>
</div>

<script>
    deactivate_input_gestion();

    $(document).on('click', '#etat_prospect', function (e) {
        e.preventDefault();
        var data_request = {
            action: "demande",
            prospect_id: $(this).attr('data-id')
        };
        active_prospect(data_request);
    });

    function active_prospect(data) {
        var type_request = 'POST';
        var type_output = 'json';
        var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
        var url_request = data_url.activerProspect;
        var loading = {
            type: "content",
            id_content: "contenair-supp"
        }
        fn.ajax_request(type_request, url_request, type_output, data_request, 'retourActiveProspect', loading);
    }
    function retourActiveProspect(response) {
        if (response.status == 200) {
            if (response.action == "demande") {
                Notiflix.Confirm.show(
                    response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                    () => {
                        var data = {
                            action: "confirm",
                            prospect_id: response.data.id
                        }
                        active_prospect(data);
                    },
                    () => { },
                    {
                        width: '420px',
                        borderRadius: '4px',
                        closeButton: true,
                        clickToClose: false,
                    },
                );
            } else {
                $(`tr[id=rowProspect-${response.data.id}]`).remove();
                Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            }
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    }
</script>