<div class="table-responsive scrollbar">
    <table class="table table-sm fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Nom de la tâche</th>
                <th scope="col">Prioritaire</th>
                <th scope="col">Prospect</th>
                <th scope="col">Participants</th>
                <th scope="col">Créateur</th>
                <th scope="col">Echéance</th>
                <th scope="col" id="datevalide" class="d-none">Date validation</th>
                <th class="text-end" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody id="tachenonvalide" class="tachenonvalide">
            <?php foreach ($tache as $taches): ?>
                <?php
                $type = $taches->tache_prioritaire;
                $value;
                if ($type == 1) {
                    $value = "Oui";
                } else {
                    $value = "Non";
                }

                $date_now = date("Y-m-d");

                ?>
                <tr id="rowTache-<?= $taches->tache_id; ?>" class="align-middle <?php if ($taches->tache_prioritaire == 1)
                    echo 'bold'; ?> 
                    <?php if ($date_now > $taches->tache_date_echeance)
                        echo 'greater'; ?>">
                    <td class=""><?= $taches->tache_nom; ?></td>
                    <td><?= $value; ?></td>
                    <td><?= $prospect->prospect_nom . ' ' . $prospect->prospect_prenom; ?></td>
                    <td> <?= $taches->utilisateur; ?> </td>
                    <td><?= $taches->tache_createur; ?></td>
                    <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $taches->tache_date_echeance))); ?></td>
                    <td>
                        <div class="d-flex justify-content-end ">
                            <button class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" id="updateTache"
                                data-prospect="<?= $prospect_id; ?>" data-id="<?= $taches->tache_id; ?>">
                                <span class="fas fa-edit fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-outline-success icon-btn p-0 m-1 btnValideTache only_visuel_gestion"
                                type="button" data-id="<?= $taches->tache_id; ?>">
                                <span class="fas fa-check fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 btnSuppTache only_visuel_gestion"
                                type="button" data-id="<?= $taches->tache_id; ?>">
                                <span class="bi bi-trash fs-1 m-1"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>

        <tbody id="tachevalide" class="tachevalide d-none">
            <?php foreach ($tachevalide as $tache): ?>
                <?php
                $type = $tache->tache_prioritaire;
                $value;
                if ($type == 1) {
                    $value = "Oui";
                } else {
                    $value = "Non";
                }

                $date_now = date("Y-m-d");

                ?>
                <tr id="rowTache-<?= $tache->tache_id; ?>"
                    class="align-middle <?php if ($tache->tache_prioritaire == 1)
                        echo 'bold'; ?>">
                    <td class=""><?= $tache->tache_nom; ?></td>
                    <td><?= $value; ?></td>
                    <td><?= $prospect->prospect_nom . ' ' . $prospect->prospect_prenom; ?></td>
                    <td> <?= $tache->utilisateur; ?> </td>
                    <td>
                        <?= $tache->tache_createur ?>
                    </td>
                    <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                    <td id="datevalide"><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_validationn))); ?>
                    </td>
                    <td>
                        <div class="d-flex justify-content-end ">
                            <button class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" id="updateTache"
                                data-prospect="<?= $prospect_id; ?>" data-id="<?= $tache->tache_id; ?>">
                                <span class="fas fa-edit fs-1 m-1"></span>
                            </button>
                            <!-- <button class="btn btn-outline-success icon-btn p-0 m-1 btnValideTache only_visuel_gestion" type="button" data-id="<?= $tache->tache_id; ?>">
                                <span class="fas fa-check fs-1 m-1"></span>
                            </button> -->
                            <button class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 btnSuppTache only_visuel_gestion"
                                type="button" data-id="<?= $tache->tache_id; ?>">
                                <span class="bi bi-trash fs-1 m-1"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $('#btnTacheValide').on('click', function () {
        $(".tachevalide").toggleClass('d-none');
        $(".tachenonvalide").toggleClass('d-none');
        $("#btnTacheNonValide").toggleClass('d-none');
        $("#btnFormTache").toggleClass('d-none');
        $("#datevalide").toggleClass('d-none');
        $(this).toggleClass('d-none');

    });

    $('#btnTacheNonValide').on('click', function () {
        $(".tachevalide").toggleClass('d-none');
        $(".tachenonvalide").toggleClass('d-none');
        $("#btnTacheValide").toggleClass('d-none');
        $("#datevalide").toggleClass('d-none');
        $("#btnFormTache").toggleClass('d-none');
        $(this).toggleClass('d-none');

    });
</script>