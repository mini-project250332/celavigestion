<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Fiche prospect :
			<?= $prospect->prospect_nom . ' ' . $prospect->prospect_prenom; ?>
			<?= ((trim($prospect->prospect_entreprise) != "") ? "[ " . $prospect->prospect_entreprise . " ]" : "") ?>
			<span class="badge badge-soft-primary <?= $prospect->etat_prospectcli == 2 ? "" : "d-none" ?>"> Déjà client
			</span>
		</h5>
	</div>
	<div class="px-2">
		<button id="retourListeProspect" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo me-1"></i>
			<span>Retour</span>
		</button>

		<?php if ($prospect->etat_prospectcli == 2 && $prospect->prospect_after_id) { ?>
			<button class="btn btn-sm btn-primary m-1 btn_limit_access" id="voirficheclient" data-after_id="<?= $prospect->prospect_after_id ?>" type="">
				<i class="fas fa-users"></i> Voir fiche client</button>
		<?php } ?>

		<?php if ($prospect->etat_prospectcli != 2) { ?>
			<button class="btn btn-sm btn-primary m-1 only_visuel_gestion" id="transformclient" <?= $prospect->etat_prospectcli == 2 ? "disabled" : "" ?> data-id="<?= $prospect->prospect_id ?>" type=""><i class="fas fa-users"></i> Transformer en client</button>
		<?php } ?>
	</div>
</div>

<div class="contenair-content">

	<ul class="nav nav-tabs" id="prospectTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="lot-tab" data-bs-toggle="tab" href="#tab-lot" role="tab" aria-controls="tab-lot" aria-selected="false">Lots</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="document-tab" data-bs-toggle="tab" href="#tab-document" role="tab" aria-controls="tab-document" aria-selected="false">Documents</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="communication-tab" data-bs-toggle="tab" href="#tab-communication" role="tab" aria-controls="tab-communication" aria-selected="false">Communication</a>
		</li>
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="prospectTabContent">
		<div class="tab-pane fade show active" id="tab-identification" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification'); ?>
		</div>
		<div class="tab-pane fade" id="tab-lot" role="tabpanel" aria-labelledby="lot-tab">
			<?php $this->load->view('lot/lots'); ?>
		</div>
		<div class="tab-pane fade" id="tab-document" role="tabpanel" aria-labelledby="document-tab">
			<?php $this->load->view('document'); ?>
		</div>
		<div class="tab-pane fade" id="tab-communication" role="tabpanel" aria-labelledby="communication-tab">
			<?php $this->load->view('communication'); ?>
		</div>
	</div>

</div>

<!-- modal -->
<div class="modal fade" id="modalErrorTransformClient" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal_invalid_transform">
					<i class="fas fa-warning"></i>
					Transformation en client invalide
				</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div>Veuillez compléter les informations suivantes : </div>
				<ul id="list_error_transform">

				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>

<!-- modal confirm -->

<div class="modal fade" id="modalTransformClient" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal_valid_transform">
					Confirmation de la transformation
				</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col">

					</div>
					<div class="col-10">
						<p class="text-center fs--1">Voulez-vous vraiment transformer ce prospect en client ? </p>
						<div class="form-group inline">
							<div>
								<p class="fs--1 m-0" data-width="200"> Dossier suivi par : </p>
								<select class="form-select fs--1" id="util_id" name="util_id" style="width: 70%;margin-left: 10px;">
									<?php foreach ($utilisateur as $utilisateurs) : ?>
										<option value="<?= $utilisateurs->id; ?>">
											<?= $utilisateurs->nom . ' ' . $utilisateurs->prenom; ?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-check">
							<label class="form-check-label" for="flexCheckDefault">
								Envoi du mail de bienvenue
							</label>
							<input class="form-check-input" type="checkbox" id="checkbox_bienvenu">
						</div>
					</div>
					<div class="col">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="confirmTransform" class="btn btn-success" data-bs-dismiss="modal">Confirmer</button>
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="FormModalBienvenu" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered">
		<div class="modal-content" id="modalContentBienvenu">
			<div class="modal-header" style="background-color : #636E7E !important">
				<h5 class="modal-title text-white" id="exampleModalLabel">Envoi de mail de bienvenu</h5>
			</div>
			<div class="modal-body">
				<div class="row m-0">
					<div class="col py-1">
						<div class="">
							<div class="">
								<div class="form-group inline">
									<div class="form-group">
										<label data-width="200" for="recipient-name" class="col-form-label">De :</label>
										<input type="text" class="form-control fs--1" value="" id="emetteur" disabled>
										<input type="hidden" value="" id="util_id">
										<input type="hidden" value="" id="prospect_id">
									</div>
									<div class="form-group">
										<label data-width="200" for="recipient-name" class="col-form-label">Destinataire :</label>
										<input type="text" class="form-control fs--1" id="destinataire" value="">
									</div>

									<div class="form-group">
										<label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
										<input type="text" class="form-control fs--1" id="object_mail" value="">
									</div>
									<div class="form-group">
										<label data-width="200" for="message-text" class="col-form-label">Message :</label>
										<textarea class="form-control fs--1" id="message_text" style="height : 315px;"></textarea>
									</div>
									<div class="form-group">
										<label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
										<img src="" alt="" id="image_signature" class="d-none" style="width:400px; height:130px;">
										<label data-width="200" for="recipient-name" class="col-form-label d-none" id="signature_aucun">Aucune</label>
									</div>
								</div>
								<br>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
					<button type="button" class="btn btn-primary" id="sendMailBienvenu">Envoyer</button>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="modal fade" id="modalFormAlert" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content" id="modal-main-content-alert">
			<div class="modal-header" style="background-color : #636E7E !important">
				<h5 class="modal-title text-white text-center" id="exampleModalLabel">Information</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="row m-0">
					<div class="col py-1">
						<div class="">
							<div class="">
								<div class="form-group inline">
									<div>
										<span class="alert alert-warning" style="margin-left: 20px;">
											<p class="fs--1 text-center" id="message_alert_compte">

											</p>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	access_right([1, 3, 4, 5], [1, 3, 4, 5]);
	
	$(document).on('click', '#sendMailBienvenu', function() {
		$('#FormModalBienvenu').modal('hide');

		var util_id = $('#util_id').val();
		var prospect_id = $('#prospect_id').val();
		var destinataire = $('#destinataire').val();
		var object = $('#object_mail').val();
		var message_text = $('#message_text').val();

		var data = {
			action: 'confirm',
			prospect_id: prospect_id,
			util_id: util_id
		}
		TransformClient(data);
		
		envoiMailBienvenu(util_id, prospect_id, destinataire, object, message_text);
	});
</script>