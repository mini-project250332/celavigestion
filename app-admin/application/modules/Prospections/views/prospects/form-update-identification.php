<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
<style>
	.help {
		color: red;
	}
</style>
<div class="contenair-title">
	<div class="px-2 menu-header">
		<h5 class="fs-0 bold">Parcours vente : &nbsp;</h5>
		<nav>
			<ul class="nav nav-pills menu-web">
				<?php foreach ($parcours as $key => $parcour) : ?>
					<?php if ($key != 0) : ?>
						<i class="fas fa-forward mt-1" id="<?= $parcour->vente_id ?>icone_pine"></i>
					<?php endif; ?>
					<li class=" nav-item menu-item <?= $parcour->vente_libelle == $prospect->vente_libelle ? "active" : ""; ?> ">
						<a href="javascript:void(0);" data-id="<?= $parcour->vente_id ?>" data-prospect="<?= $prospect->prospect_id ?>" class="parcours m-0" style="font-size : 14px !important ;padding : 0px !important;">
							<?= $parcour->vente_libelle; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</nav>
	</div>
	<div class=" px-2">
		<div class="form-group inline">
			<div class="col-sm-12">
				<label data-width="100"></label>
				<select class="form-select fs--1 m-1" id="etat_prospect_id" name="etat_prospect_id">
					<?php foreach ($etat as $etats) : ?>
						<option class="" style="color : #<?= $etats->etat_couleur; ?> ; background-color : #edeaea" value="<?= $etats->etat_prospect_id; ?>" <?php echo isset($prospect->etat_prospect_id) ? (($prospect->etat_prospect_id !== NULL) ? (($prospect->etat_prospect_id == $etats->etat_prospect_id) ? "selected" : " ") : "") : "" ?>>
							<?= $etats->etat_prospect_libelle; ?>
						</option>
					<?php endforeach; ?>
				</select>
				<div class="help"></div>
			</div>
		</div>
		<button type="submit" id="save-update-prospect" class="btn btn-sm btn-primary mx-1" data-id="<?= $prospect->prospect_id ?>" style="width : 140px;">
			<i class="fas fa-check-circle"></i>
			<span> Terminer </span>
		</button>
	</div>
</div>
<div id="div-fiche-prospect" class="row m-0">
	<input type="hidden" name="page" id="page" value="<?= $page ?>">
	<div class="col-12 p-0 py-1">

		<div class="row m-0">

			<div class="col-4 p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-1">Identité</h5>
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Nom *</label>
												<input type="text" class="form-control fs--1 maj" id="prospect_nom" name="prospect_nom" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= $prospect->prospect_nom ?>">
												<div class="help help_nom"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Prénom *</label>
												<input type="text" class="form-control fs--1" id="prospect_prenom" name="prospect_prenom" value="<?= $prospect->prospect_prenom ?>">
												<div class="help"> </div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Type prospect </label>
												<select class="form-select fs--1" id="type_prospect_id" name="type_prospect_id" style="margin-left : 7px;">
													<?php foreach ($type as $types) : ?>
														<option value="<?= $types->type_prospect_id; ?>" <?php echo isset($prospect->type_prospect_id) ? (($prospect->type_prospect_id !== NULL) ? (($prospect->type_prospect_id == $types->type_prospect_id) ? "selected" : " ") : "") : "" ?>>
															<?= $types->type_prospect_libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Entreprise </label>
												<input type="text" class="form-control fs--1 maj" id="prospect_entreprise" name="prospect_entreprise" value="<?= $prospect->prospect_entreprise ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Nationalité </label>
												<select class="form-select" id="prospect_nationalite" name="prospect_nationalite">
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Française") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_nationalite == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Résidence fiscale </label>
												<select class="form-select fs--1" id="prospect_residence_fiscale" name="prospect_residence_fiscale" style="margin-left : 7px;">
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Française") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_residence_fiscale == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_residence_fiscale == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>" <?php echo ($prospect->prospect_residence_fiscale == $nationalites->libelle) ? "selected" : " " ?>><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Situation matrimoniale</label>

												<select class="form-select fs--1" id="prospect_siutation" name="prospect_siutation" value="<?= $prospect->prospect_siutation ?>" style="margin-left : 7px;">
													<?php foreach (unserialize(situation) as $key => $value) : ?>
														<option value="<?= $value ?>" <?php echo ($prospect->prospect_siutation == $value) ? "selected" : " " ?>>
															<?= $value; ?>
														</option>
													<?php endforeach; ?>

												</select>
												<div class="help"> </div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									

									<tr class="border-bottom">

										<div class="form-group inline doc">
											<?php if ($prospect->cin) : ?>
												<?php $nom_fic = explode("/", $prospect->cin); ?>
												<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $prospect->cin ?>')" data-url="<?= base_url($prospect->cin) ?>" style="cursor: pointer;margin-left:35%">
													<?= $nom_fic[3] ?></span>
												<span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefilecin" data-id="<?= $prospect->prospect_id ?>" style="cursor: pointer;">.</span><br>
											<?php endif ?>
										</div>

										<div class="form-group inline p-0">
											<div class="col">
												<label data-width="300"> CNI </label>
												<input type="file" class="form-control" id="cin" name="cin">
												<div class="help"> </div>
											</div>
											<?php if (empty($prospect->cin)) : ?>
												<div class='col' id='affichage_cin'>
													<div class='row'>
														<div class='col-7'>
															<div class='fs--2 text-danger text-center pt-1'>
																<i>Attention, vous devez renseigné la CNI pour ce prospect.</i>
															</div>
														</div>
														<div class='col-2 text-end'>
															<button class='btn btn-sm btn-secondary fs--2' type='button' id='historique_mail_prospect'>Historique</button>
														</div>
														<div class='col-3 text-end'>
															<div class='text-end'>
																<button class='btn btn-sm btn-primary fs--2' type='button' title='Demander CNI' id='submit_cin' data-id="<?= $prospect->prospect_id ?>">Demander CNI</button>
															</div>
														</div>
													</div>
												</div>
											<?php else : ?>
												<div class='col' id='affichage_cin'>
													<div class='row'>
														<div class='col'>
															<button class='btn btn-sm btn-secondary fs--2' type='button' id='historique_mail_prospect'>Historique d'email</button>
														</div>
													</div>
												</div>
											<?php endif ?>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="300"> Date de fin de validité CNI </label>
												<input type="date" class="form-control fs--1" id="prospect_date_fin_validite_cni" name="prospect_date_fin_validite_cni" value="<?= $prospect->prospect_date_fin_validite_cni ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
								

									<hr class="mt-0">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0">
					<div class="card-body ">
						<h5 class="fs-1">Contacts</h5>
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Email 1</label>
												<input type="text" data-type="email" class="form-control fs--1" id="prospect_email" name="prospect_email" value="<?= $prospect->prospect_email ?>" data-formatcontrol="true" data-require="true">
												<div class="help help_email1"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Email 2</label>
												<input type="text" data-type="email" class="form-control fs--1" id="prospect_email1" name="prospect_email1" value="<?= $prospect->prospect_email1 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Téléphone 1 * </label>
												<select class="form-select select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id" name="paysmonde_id">
													<?php foreach ($pays_indicatif as $indicatif) : ?>
														<option value="<?= $indicatif->paysmonde_id ?>" <?php echo isset($prospect->paysmonde_id) ? (($prospect->paysmonde_id !== NULL) ? (($prospect->paysmonde_id == $indicatif->paysmonde_id) ? "selected" : " ") : "") : "" ?>>
															<?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
														</option>
													<?php endforeach; ?>
												</select>
												<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="prospect_phonemobile" name="prospect_phonemobile" style="height: 37px; width: 80%;left: 4px" value="<?= $prospect->prospect_phonemobile ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Télephone 2</label>
												<select class="form-select select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id" name="pays_id">
													<?php foreach ($pays as $indicatifs) : ?>
														<option value="<?= $indicatifs->pays_id ?>" <?php echo isset($prospect->pays_id) ? (($prospect->pays_id !== NULL) ? (($prospect->pays_id == $indicatifs->pays_id) ? "selected" : " ") : "") : "" ?>>
															<?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
														</option>
													<?php endforeach; ?>
												</select>
												<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="prospect_phonefixe" name="prospect_phonefixe" style="height: 37px;width: 80%; left: 4px" value="<?= $prospect->prospect_phonefixe ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
						<hr class="mt-2 mb-0 col-sm-12">
						<h5 class="fs-1 mt-4">Adresses</h5>
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 1 </label>
												<input type="text" class="form-control fs--1" id="prospect_adresse1" name="prospect_adresse1" value="<?= $prospect->prospect_adresse1 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 2</label>
												<input type="text" class="form-control fs--1" id="prospect_adresse2" name="prospect_adresse2" value="<?= $prospect->prospect_adresse2 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Adresse 3</label>
												<input type="text" class="form-control fs--1" id="prospect_adresse3" name="prospect_adresse3" value="<?= $prospect->prospect_adresse3 ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Code postal </label>
												<input type="text" class="form-control fs--1" id="prospect_cp" name="prospect_cp" value="<?= $prospect->prospect_cp ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Ville </label>
												<input v-on:keyup='input_form' type="text" class="form-control fs--1 maj" id="prospect_ville" name="prospect_ville" value="<?= $prospect->prospect_ville ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr>
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> Pays </label>
												<input type="text" class="form-control fs--1 maj" id="prospect_pays" name="prospect_pays" value="<?= $prospect->prospect_pays ?>">
												<div cclass="help fs-11 height-15 m-0"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Informations de facturation</h5>
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> IBAN </label>
												<input type="text" class="form-control fs--1" id="prospect_iban" name="prospect_iban" value="<?= $prospect->prospect_iban ?>" autocomplete="off">
												<div class="help help_iban"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> BIC </label>
												<input type="text" class="form-control fs--1" id="prospect_bic" name="prospect_bic" value="<?= $prospect->prospect_bic ?>">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">

										<div class="form-group inline doc_rib">
											<?php if ($prospect->file_rib) : ?>
												<?php $nom_fic_rib = explode("/", $prospect->file_rib); ?>
												<span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $prospect->file_rib ?>')" data-url="<?= base_url($prospect->file_rib) ?>" style="cursor: pointer;margin-left:20%">
													<?= $nom_fic_rib[3] ?></span>
												<span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefilerib" data-id="<?= $prospect->prospect_id ?>" style="cursor: pointer;">.</span><br>
											<?php endif ?>
										</div>

										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="150"> RIB </label>
												<input type="file" class="form-control fs--1" id="file_rib" name="file_rib">
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
						<hr class="mt-2 mb-0 col-sm-12">
						<h5 class="fs-1 mt-4">Autres informations</h5>
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Origine demande *</label>
												<select class="form-select fs--1" id="origine_dem_id" name="origine_dem_id">
													<?php foreach ($originedem as $originedemande) : ?>
														<option value=" <?= $originedemande->id; ?>" <?php echo isset($prospect->origine_dem_id) ? (($prospect->origine_dem_id !== NULL) ? (($prospect->origine_dem_id == $originedemande->id) ? "selected" : " ") : "") : "" ?>>
															<?= $originedemande->libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Origine client *</label>
												<select class="form-select fs--1" id="origine_cli_id" name="origine_cli_id">
													<?php foreach ($originecli as $origineclient) : ?>
														<option value="<?= $origineclient->id; ?>" <?php echo isset($prospect->origine_cli_id) ? (($prospect->origine_cli_id !== NULL) ? (($prospect->origine_cli_id == $origineclient->id) ? "selected" : " ") : "") : "" ?>>
															<?= $origineclient->libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Prestation demandée *</label>
												<select class="form-select fs--1 js-choice" id="produit_id" name="produit_id" multiple="multiple" size="1" data-require="true">
													<option value="">
													</option>
												</select>
												<div class="help help_prestation" style="margin-top: 20px;margin-right: 160px;"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Suivi par </label>
												<select class="form-select fs--1" id="util_id" name="util_id">
													<?php foreach ($utilisateur as $utilisateurs) : ?>
														<option value=" <?= $utilisateurs->id; ?>" <?php echo isset($prospect->util_id) ? (($prospect->util_id !== NULL) ? (($prospect->util_id == $utilisateurs->id) ? "selected" : " ") : "") : "" ?>>
															<?= $utilisateurs->nom . ' ' . $utilisateurs->prenom; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
						<hr class="mt-2 mb-0 col-sm-12">
						<div class="form-group inline p-0 mt-3">
							<div class="col-sm-12">
								<label data-width="150"> Commentaire</label>
								<textarea class="form-control" id="prospect_commentaire" name="prospect_commentaire" placeholder="Laissez un commentaire" style="height: 100px"><?= $prospect->prospect_commentaire; ?></textarea>
								<div class="help"></div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>


<?php
$produit_default = array();
foreach ($produit as $produits) {
	$produit_default[] = array(
		'id' => $produits->produit_id,
		'text' => $produits->produit_libelle,
	);
}

if ($page == "form") {
	$edit_exist = explode(',', $prospect->produit_id);
	$produit_array = array();
	$items = array();
	foreach ($produit_default as $key) {
		if (in_array($key['id'], $edit_exist)) {
			$produit_array[] = array(
				'id' => $key['id'],
				'text' => $key['text'],
			);
		} else {
			$items[] = array(
				'id' => $key['id'],
				'text' => $key['text'],
			);
		}
	}
	$produit_default = $items;
}
?>

<script>
	$('#prospect_nationalite').change(function() {
		var nationalite = $(this).val();
		if (nationalite === 'show') {
			$('#reply').attr('pk', '1').show();
			$('#prospect_nationalite').attr('pk', '1').hide();
		}

	});

	$(document).ready(function() {
		$('.select2').select2({
			theme: 'bootstrap-5'
		});
	});

	var produit_default = <?= json_encode($produit_default) ?>;


	var element = document.querySelector('.js-choice');
	var choices = new Choices(element, {
		removeItems: true,
		removeItemButton: true,
		choices: produit_default.map((item) => ({
			value: item.id,
			label: item.text
		}))
	});

	<?php if ($page == "form") : ?>
		var liste_produit = <?= json_encode($produit_array); ?>;
		//choices.removeActiveItems();
		choices.setValue(liste_produit.map((item) => ({
			value: item.id,
			label: item.text
		})));
	<?php endif; ?>
</script>