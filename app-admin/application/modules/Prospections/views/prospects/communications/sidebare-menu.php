<div class="sidebar-menu">
    <ul class="list-unstyled">

        <li class="item-sidebar">
            <div class="collapse <?php if(isset($menu_sidebar)) echo (in_array($menu_sidebar,['communications',''])) ? 'show' : '';?>" id="item">
                <ul class="btn-toggle-nav list-unstyled">
                    <li class="<?php if(isset($menu_sidebar)) echo ($menu_sidebar =='communications') ? 'active' : '';?>">
                        <a href="" class="">
                            <span>Gestion des utilisateurs</span>
                        </a>
                    </li>
                    <li class="<?php if(isset($menu_sidebar)) echo ($menu_sidebar =='') ? 'active' : '';?>">
                        <a href="<" class="">
                            <span>Historique d'accès</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>