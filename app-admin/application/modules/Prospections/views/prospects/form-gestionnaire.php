<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Ajouter un gestionnaire</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Setting/Gestionnaire/cruGestionnaire',array('method'=>"post", 'class'=>'px-2','id'=>'AddGestion')); ?>
 
	<?php if(isset($gestionnaire->gestionnaire_id)):?>
        <input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?=$gestionnaire->gestionnaire_id;?>">
    <?php endif;?>

	<input type="hidden" name="action" id="action" value="<?=$action?>">   
    <div class="modal-body">

        <div class="row m-0">
			<div class="col"></div>

    		<div class="col py-2">

    			<div class="card rounded-0 h-100">
	                <div class="card-body">

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Nom *</label>
		                        <input type="text" class="form-control" id="gestionnaire_nom" name="gestionnaire_nom" data-formatcontrol="true" data-require="true">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 1  </label>
		                        <input type="text" class="form-control"  id="gestionnaire_adresse1" name="gestionnaire_adresse1" >
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 2 </label>
		                        <input type="text" class="form-control" id="gestionnaire_adresse2" name="gestionnaire_adresse2">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 3 </label>
		                        <input type="text" class="form-control" id="gestionnaire_adresse3" name="gestionnaire_adresse3">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
						    <div>
						        <label data-width="200"> Code postal </label>
						        <input type="text" class="form-control" id="gestionnaire_cp" name="gestionnaire_cp">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Ville </label>
						        <input type="text" class="form-control" id="gestionnaire_ville" name="gestionnaire_ville">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Pays </label>
						        <input type="text" class="form-control"  id="gestionnaire_pays" name="gestionnaire_pays">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline switch">
	                        <div>
	                            <label data-width="250"> Statut gestionnaire</label>
	                            <div class="form-check form-switch">
	                                <span> Inactif </span>
	                                <input class="form-check-input" type="checkbox" id="gestionnaire_etat" name="gestionnaire_etat"  <?php echo isset($gestionnaire->gestionnaire_etat) ? ($gestionnaire->gestionnaire_etat=="1" ? "checked" :"") : "checked" ;?>>
	                                <span> Actif </span>
	                            </div>
	                        </div>
	                    </div>
		            </div>
		        </div>
    		</div>
    		<div class="col"></div>
    	</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </div>
    </div>
<?php echo form_close(); ?>


 
