<?php echo form_tag('Prospections/prospectAdd', array('method' => "post", 'class' => '', 'id' => 'formAddAjout')); ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

<div id="div-fiche-prospect" class="row m-0">

	<div class="col-12 p-0 py-1">
		<div class="contenair-title">
			<div class="row m-0">
				<div class="px-2">
					<h5 class="fs-1">Formulaire d'ajout d'un prospect</h5>
				</div>
			</div>
			<div class="d-flex justify-content-end">
				<button class="btn btn-sm btn-secondary m-1" id="btnAnnuler">
					<span class="bi bi-x me-1"></span><span> Annuler </span>
				</button>
				<button class="btn btn-sm btn-primary m-1" type="submit">
					<span class="fas fa-plus me-1"></span>
					<span> Enregistrer </span>
				</button>
			</div>
		</div>

		<div class="row m-0">

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-1">Identité</h5>
						<hr class="mt-0">
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Nom *</label>
												<input type="text" class="form-control maj" id="prospect_nom" name="prospect_nom" data-formatcontrol="true" data-require="true">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Prénom *</label>
												<input type="text" class="form-control" id="prospect_prenom" name="prospect_prenom" data-formatcontrol="true" data-require="true">
												<div class="help"> </div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Type prospect </label>
												<select class="form-select fs--1" id="type_prospect_id" name="type_prospect_id">
													<?php foreach ($type as $types) : ?>
														<option value="<?= $types->type_prospect_id; ?>">
															<?= $types->type_prospect_libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Entreprise </label>
												<input type="text" class="form-control maj" id="prospect_entreprise" name="prospect_entreprise">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Nationalité </label>
												<select class="form-select fs--1" id="prospect_nationalite" name="prospect_nationalite">
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Française") : ?>
															<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle == "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
													<?php foreach ($nationalite as $nationalites) :
														if ($nationalites->libelle != "Française" && $nationalites->libelle != "Britannique") : ?>
															<option value="<?= $nationalites->libelle ?>"><?= $nationalites->libelle ?></option>
													<?php endif;
													endforeach; ?>
												</select>
												<!-- <input type="text"  id="reply" class="form-control pull-right" name="prospect_nationalite" placeholder="Autre nationalité..." 
												style="display:none;"/> -->
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<hr class="mt-0">
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Situation matrimoniale</label>
												<select class="form-select fs--1" id="prospect_siutation" name="prospect_siutation">
													<?php $situation = unserialize(situation);
													foreach ($situation as $value) {
														echo '<option >' . $value . '</option>';
													}
													?>
												</select>
												<div class="help"> </div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0">
					<div class="card-body ">
						<h5 class="fs-1">Contacts</h5>
						<hr class="mt-0">
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Email 1 *</label>
												<input type="text" data-type="email" class="form-control" id="prospect_email" name="prospect_email" data-formatcontrol="true" data-require="true">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Email 2</label>
												<input type="text" data-type="email" class="form-control" id="prospect_email1" name="prospect_email1">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Télephone 1 *</label>
												<select class="form-select select2 select2-hidden-accessible" style="width: 30%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id" name="paysmonde_id" data-formatcontrol="true" data-require="true">
													<?php foreach ($pays_indicatif as $indicatif) : ?>
														<option value="<?= $indicatif->paysmonde_id ?>" title="<?= $indicatif->paysmonde_libelle ?>">
															<?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
														</option>
													<?php endforeach; ?>
												</select>
												<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="prospect_phonemobile" name="prospect_phonemobile" style="height: 37px;width: 89%;left: 4px;">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Télephone 2</label>
												<select class="form-select select2 select2-hidden-accessible" style="width: 30%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id" name="pays_id">
													<?php foreach ($pays as $indicatifs) : ?>
														<option value="<?= $indicatifs->pays_id ?>" title="<?= $indicatifs->pays_libelle ?>">
															<?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
														</option>
													<?php endforeach; ?>
												</select>
												<input v-on:keyup='input_form' type="text" class="form-control chiffre fs--1 mx-1" id="prospect_phonefixe" name="prospect_phonefixe" style="height: 37px;width: 89%;left: 4px;">
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
						<hr class="mt-2 mb-0 col-sm-12">
						<h5 class="fs-1 mt-4">Adresses</h5>
						<hr class="mt-0">
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Adresse 1 </label>
												<input type="text" class="form-control" id="prospect_adresse1" name="prospect_adresse1">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Adresse 2</label>
												<input type="text" class="form-control" id="prospect_adresse2" name="prospect_adresse2">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Adresse 3</label>
												<input type="text" class="form-control" id="prospect_adresse2" name="prospect_adresse2">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Code postal </label>
												<input type="text" class="form-control" id="prospect_cp" name="prospect_cp">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Ville </label>
												<input v-on:keyup='input_form' type="text" class="form-control maj" id="prospect_ville" name="prospect_ville">
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr>
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="200"> Pays </label>
												<input type="text" class="form-control maj" id="prospect_pays" name="prospect_pays" value="France">
												<div cclass="help fs-11 height-15 m-0"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-1">Autres informations</h5>
						<hr class="mt-0">
						<div class="mt-4">
							<table class="table table-borderless fs--1 mb-0">
								<tbody>
									<div class="form-group inline">
										<div>
											<label data-width="250"> Date de creation *</label>
											<input type="text" class="form-control calendar" id="prospect_date_creation" name="prospect_date_creation" data-formatcontrol="true" data-require="true">
											<div class="help"></div>
										</div>
									</div>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Origine demande *</label>
												<select class="form-select fs--1" id="" name="origine_dem_id">
													<?php foreach ($originedem as $originedemande) : ?>
														<option value=" <?= $originedemande->id; ?>">
															<?= $originedemande->libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline">
											<div class="col-sm-12">
												<label data-width="250"> Origine client *</label>
												<select class="form-select fs--1" id="" name="origine_cli_id">
													<?php foreach ($originecli as $origineclient) : ?>
														<option value="<?= $origineclient->id; ?>">
															<?= $origineclient->libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Prestation demandée *</label>
												<select class="form-select fs--1" id="" name="produit_id">
													<?php foreach ($produit as $produits) : ?>
														<option value=" <?= $produits->produit_id; ?>">
															<?= $produits->produit_libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Actions</label>
												<select class="form-select fs--1" id="" name="etat_prospect_id">
													<?php foreach ($etat as $etats) : ?>
														<option value="<?= $etats->etat_prospect_id; ?>">
															<?= $etats->etat_prospect_libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Parcours vente *</label>
												<select class="form-select fs--1" id="vente_id" name="vente_id">
													<?php foreach ($parcours as $parcour) : ?>
														<option value=" <?= $parcour->vente_id; ?>">
															<?= $parcour->vente_libelle; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
									<tr class="border-bottom">
										<div class="form-group inline p-0">
											<div class="col-sm-12">
												<label data-width="250"> Suivi par </label>
												<select class="form-select fs--1" id="util_id" name="util_id">
													<?php foreach ($utilisateur as $utilisateurs) : ?>
														<option value="<?= $utilisateurs->id; ?>" <?= ($utilisateurs->id == $_SESSION['session_utilisateur']['util_id']) ? 'selected' : ""; ?>>
															<?= $utilisateurs->nom . ' ' . $utilisateurs->prenom; ?>
														</option>
													<?php endforeach; ?>
												</select>
												<div class="help"></div>
											</div>
										</div>
									</tr>
								</tbody>
							</table>
						</div>
						<hr class="mt-2 mb-0 col-sm-12">
						<div class="form-group inline p-0 mt-3">
							<div class="col-sm-12">
								<label data-width="150"> Commentaire</label>
								<textarea class="form-control" id="prospect_commentaire" name="prospect_commentaire" placeholder="Laissez un commentaire" style="height: 150px"></textarea>
								<div class="help"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<?php echo form_close(); ?>

<script>
	// 	$('#prospect_nationalite').change(function () {
	// 	var nationalite = $(this).val();
	// 	if (nationalite === 'show') {
	// 	$('#reply').attr('pk','1').show();
	// 	$('#prospect_nationalite').attr('pk','1').hide();
	// }

	// });

	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y",
		defaultDate: moment().format('DD-MM-YYYY'),
	});

	$(document).ready(function() {
		$('[data-bs-toggle="tooltip"]').tooltip();
	});

	$(".select2").val(66);

	$(document).ready(function() {
		$('.select2').select2({
			theme: 'bootstrap-5',
			//placeholder: "Indicatif",
		});
	});
</script>