<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Liste des prospects</h5>
    </div>
    <div class="px-2">
        <a href="<?php echo base_url('Prospections/Prospections/prospectSupp');?>"class="btn btn-sm btn-primary fs--1 " style="color :#ffffff; ">
            <i class="fa fa-eye"></i> Voir prospects supprimés
        </a> &nbsp;
        <a href="<?php echo base_url('Prospections/Prospections/prospectConverti');?>"class="btn btn-sm btn-primary fs--1 " style="color :#ffffff; ">
            <i class="fa fa-eye"></i> Voir prospects convertis
        </a> &nbsp;
        <a href="<?php echo base_url('Prospections/Prospections/prospectPerdu');?>"class="btn btn-sm btn-primary fs--1 " style="color :#ffffff; ">
            <i class="fa fa-eye"></i> Voir prospects perdus
        </a>
    </div>
</div>

<div class="contenair-list">

    <div class="m-0">
        <div class="row m-0">
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Nom, Email, Contact</label>
                    <input class="form-control mb-0 fs--1" type="text" id="text_filtre" name="text_filtre" placeholder="Rechercher">
                </div>
            </div>
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Origine demande</label>
                    <select class="form-select fs--1" id="origine_dem_id" name="origine_dem_id">
                        <option value="">Toutes</option>
                    <?php foreach ($originedem as $originedemande ) :?>
                        <option value=" <?=$originedemande->id;?>"> 
                        <?=$originedemande->libelle;?>
                        </option>
                    <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Prestation demandée</label>
                        <select class="form-select fs--1" id="produit_id" name="produit_id">
                            <option value="">Tous</option>
                        <?php foreach ($produit as $produits ) :?>
                            <option value=" <?=$produits->produit_id;?>"> 
                            <?=$produits->produit_libelle;?>
                            </option>
                        <?php endforeach;?>
                        </select>
                </div>
            </div>
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Parcours vente</label>
                    <select class="form-select fs--1" id="parcours_vente_id" name="parcours_vente_id">
                        <option value="">Tous</option>
                        <?php foreach ($parcours as $parcour ) :?>
                            <option value=" <?=$parcour->vente_id;?>"> 
                            <?=$parcour->vente_libelle;?>
                            </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Etat</label>
                    <select class="form-select fs--1" id="etat_prospect_id" name="etat_prospect_id" >
                    <option value="">Tous</option>
                    <?php foreach($etat as $etats) :?>
                        <option value="<?=$etats->etat_prospect_id;?>">
                        <?=$etats->etat_prospect_libelle;?>
                        </option>
                    <?php endforeach;?>
                    </select>
                </div>
            </div>   
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Origine Client</label>
                    <select class="form-select fs--1" id="origine_cli_id" name="origine_cli_id">
                        <option value="">Tous</option>
                        <?php foreach ($originecli as $origineclient ) :?>
                        <option value ="<?=$origineclient->id;?>"> 
                        <?=$origineclient->libelle;?>
                        </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>         
            <div class="col p-1 pb-0">
                <div>
                    <label class="form-label">Date création</label>
                    <div class="d-flex">
                        <input type='text' class="form-control calendar" id="date_filtre" name="date_filtre" type="text"/>
                        
                        <button class="btn btn-sm btn-outline-primary d-flex justify-content-center ms-1 d-none" style="width:32px;height: 32px;" id="clear_date_filtre">
                            <i class="fas fa-times align-self-center"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0">
        <div id="data-list" class="col p-0">
            
            

        </div>
    </div>

</div>

<script type="text/javascript">
	$(document).ready(function () {
    	listProspect();
	});
    
    var date_filtre = flatpickr("#date_filtre",{
        altInput: true,
        allowInput: true,
        "mode" : "range",
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        onChange: function(){
            $('#clear_date_filtre').removeClass('d-none');
            listProspect();
        },
    });

    $(document).on('click', '#clear_date_filtre', function(e) {
        date_filtre.clear();
        $(this).toggleClass('d-none');
        listProspect();
    })

</script>
        
        