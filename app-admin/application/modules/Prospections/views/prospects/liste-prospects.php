<?php
function RemoveSpecialChar($str)
{
    $res = str_replace(
        array(
            '\'',
            '"',
            '-',
            ' ',
            '.',
            ',',
            ';',
            '<',
            '>'
        ),
        '',
        $str
    );

    return $res;
} ?>


<input type="hidden" value="<?php echo base_url('Prospections'); ?>" id="prospect-open-tab-url" />

<div class="mb-2 p-1 bg-light">
    <div class="row m-0 ">
        <div class="col-md-auto p-0">
            <button class="btn btn-sm bg-light  fw-semi-bold">
                Ligne
            </button>
        </div>
        <div class="col-md-auto p-0">
            <select class="form-select row_view" id="row_view" name="row_view" width="80px">
                <?php foreach (unserialize(row_view) as $key => $value): ?>
                    <option value="<?= $value; ?>" <?= ($value == intval($rows_limit)) ? 'selected' : ''; ?>><?= $value; ?>
                    </option>
                <?php endforeach; ?>
            </select>

            <input type="hidden" name="pagination_view" id="pagination_view">
        </div>
        <div class="col p-0 p">
            <button class="btn btn-sm bg-light  fw-semi-bold">
                Resulat(s) :
                <?= $countAllResult ?> Prospects enregistré(s)
            </button>
        </div>
        <div class="col-md-auto p-0">
            <button class="btn btn-sm btn-secondary only_visuel_gestion">
                <i class="fas fa-file-excel"></i> Exporter
            </button>

            <button id="btnFormNewProspect" class="btn btn-sm btn-primary only_visuel_gestion" type="button">
                <span class="bi bi-person-plus-fill me-1"></span>
                Nouveau prospect
            </button>
        </div>
    </div>
</div>
<div class="">
    <div class="table-responsive scrollbar">
        <table class="table table-sm table-striped fs--1 mb-0">
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Prospect (Entreprise/Nom)</th>
                    <th scope="col">Contacts</th>
                    <th scope="col">Origine demande</th>
                    <th scope="col">Prestation demandée</th>
                    <th scope="col">Date création</th>
                    <th scope="col">Parcours vente</th>
                    <th scope="col">Mandats</th>
                    <th scope="col">Etat</th>
                    <th scope="col">Suivi par</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody id="myTable">
                <?php foreach ($prospect as $prospects): ?>
                    <?php
                    $phoneNumber = RemoveSpecialChar($prospects->prospect_phonemobile);
                    $numero = $prospects->prospect_phonemobile;
                    if ($prospects->paysmonde_id == 66) {
                        if ($prospects->prospect_phonemobile != "") {
                            $array = str_split($phoneNumber, 2);
                            $array[0] = '(' . $array[0][0] . ')' . $array[0][1];
                            $numero = implode(".", $array);
                        } else {
                            $numero = $prospects->prospect_phonemobile;
                        }
                    } else {
                        $numero = $prospects->prospect_phonemobile;
                    }

                    if ($prospects->prospect_phonefixe != "") {
                        $phone = RemoveSpecialChar($prospects->prospect_phonefixe);
                        if ($prospects->pays_id == 66) {
                            $array = str_split($phone, 2);
                            $array[0] = '(' . $array[0][0] . ')' . $array[0][1];
                            $number = implode(".", $array);
                        } else {
                            $number = $prospects->prospect_phonefixe;
                        }
                    }
                    ?>
                    <tr id="rowProspect-<?= $prospects->prospect_id; ?>" class="align-middle">

                        <td>
                            <h6 class="mb-0 fw-semi-bold"><span class="stretched-link text-900">
                                    <?= $prospects->prospect_entreprise; ?>
                                </span></h6>
                            <p class="text-800 fs--1 mb-0">
                                <?= $prospects->prospect_nom . ' ' . $prospects->prospect_prenom; ?>
                                <button class="btn btn-falcon-primary me-1 mb-1 btnFicheClient" data-id="">
                                    <span
                                        class="badge badge-soft-primary <?= $prospects->etat_prospectcli == 2 ? '' : 'd-none' ?>">C</span>
                                </button>
                            </p>
                        </td>
                        <td class="col-3">
                            <h6 class="mb-0 fw-semi-bold">
                                <i class="fas fa-envelope fa-w-16 text-primary"></i> Email :
                                <span class="px-2">
                                    <?= $prospects->prospect_email; ?>
                                </span>
                            </h6>
                            <p class="text-800 fs--1 mb-0">
                                <i class="fas fa-phone-alt fa-w-16 text-primary"></i> Téléphone(s) :
                                <span class="px-2">
                                    <?= $prospects->paysmonde_indicatif; ?>
                                    <?= $numero; ?>
                                    <?= (trim($prospects->prospect_phonefixe) != "") ? ' / ' . $prospects->pays_indicatif . ' ' . $number . '' : ''; ?>
                                </span>
                            </p>
                        </td>
                        <td>
                            <?= $prospects->origine_dem_libelle; ?>
                        </td>
                        <td class="col-2">
                            <?= $prospects->produit; ?>
                        </td>
                        <td>
                            <?= date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $prospects->prospect_date_creation))); ?>
                        </td>
                        <td class="text-center">
                            <?= $prospects->vente_libelle; ?>
                        </td>
                        <?php
                        $mandat_montant = NULL;
                        if (!empty($mandat)) {
                            foreach ($mandat as $key => $value) {
                                if ($value->prospect_id == $prospects->prospect_id) {
                                    $mandat_montant = explode(",", $value->mandat_montant);
                                }
                            }
                        }
                        ?>
                        <td class="text-center">
                            <?php if ($mandat_montant != NULL):
                                foreach ($mandat_montant as $key => $value) {
                                    echo format_number_($value) . '<br>';
                                } ?>
                            <?php else: ?>
                                <?= '' ?>
                            <?php endif ?>
                        </td>
                        <td>
                            <?php if ($prospects->etat_prospect_id == 1) {
                                echo '<span class="badge badge-soft-danger fs--1">' . $prospects->etat_prospect_libelle . '</span>';
                            } else if ($prospects->etat_prospect_id == 2) {
                                echo '<span class="badge badge-soft-warning fs--1">' . $prospects->etat_prospect_libelle . '</span>';
                            } else if ($prospects->etat_prospect_id == 3) {
                                echo '<span class="badge badge-soft-secondary fs--1" style="background-color : #E4DF19">' . $prospects->etat_prospect_libelle . '</span>';
                            } else
                                echo '<span class="badge badge-soft-success fs--1">' . $prospects->etat_prospect_libelle . '</span>'; ?>
                        </td>
                        <td>
                            <?= $prospects->util_prenom; ?>
                        </td>
                        <td class="btn-action-table">
                            <div class="d-flex justify-content-end ">
                                <button data-id="<?= $prospects->prospect_id ?>"
                                    class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 btnFormUpdateProspect" id="lot_<?= $prospects->prospect_id; ?>" type="">
                                    <span class="fas fa-play fs-1 m-1"></span>
                                </button>
                                <button data-id="<?= $prospects->prospect_id ?>"
                                    class="btn btn-outline-primary btnFicheProspectNewTab" type="button">
                                    <span class="fas fa-plus open_tab_client"></span>
                                </button>
                                <button class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup only_visuel_gestion"
                                    type="button" data-id="<?= $prospects->prospect_id ?>">
                                    <span class="bi bi-trash fs-1"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<div class="d-flex justify-content-end pt-3">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>

                <?php foreach ($li_pagination as $key => $value): ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-<?= $value; ?>"
                        class="page-item pagination-vue-table <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>"
                            href="javascript:void(0);"><?= $key + 1; ?></a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<script>
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const prospect_id = urlParams.get('_p@rospect');
	if (prospect_id) $(`#lot_${prospect_id}`).trigger("click");
    const prospect_tab = urlParams.get('_open@prospect_tab');
    if (prospect_tab) {
        ficheProspect({ prospect_id: prospect_tab });
    }
</script>