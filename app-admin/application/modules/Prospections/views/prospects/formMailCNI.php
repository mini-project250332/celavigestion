<style>
    .badge {
        cursor: pointer;
    }
</style>
<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Envoi de mail (Demander CNI)</h5>
</div>
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <input type="text" class="form-control fs--1" value="<?= $emetteur->util_prenom ?> <?= $emetteur->util_nom ?> (<?= $emetteur->mess_email == NULL ? "Aucun mail dans le paramètre d'utilisateur" : $emetteur->mess_email ?>)" disabled>
                            <input type="hidden" value="<?= $util_id ?>" id="util_id">
                            <input type="hidden" value="<?= $emetteur->mess_email ?>" id="mess_email">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataire :</label>
                            <input type="text" class="form-control fs--1" id="destinataire" value="<?= $destinataire ?>">
                        </div>

                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object_CNI" value="<?= $pmail_objet ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message_text" style="height : 315px;"><?= $mailMessage ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <?php if (isset($emetteur->mess_signature) && ($emetteur->mess_signature) != NULL) : ?>
                                <img src="<?= URL_ADMIN. '/'.$emetteur->mess_signature ?>" alt="" id="image_signature" style="width:400px; height:130px;">
                            <?php else : ?>
                                <label data-width="200" for="recipient-name" class="col-form-label" id="signature_mandat">Aucune</label>
                            <?php endif ?>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary" id="sendMailCNI">Envoyer</button>
    </div>
</div>