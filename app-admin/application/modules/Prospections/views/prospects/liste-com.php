<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0">Message(s) <span class="badge badge-soft-primary rounded-pill">
                <?= $countAllResult ?>
            </span></h5>
    </div>
    <div class="px-2">
        <div class="d-flex justify-content-center">
            <button id="btnFormCom" data-id="<?= $prospect_id; ?>" class="btn btn-sm btn-primary only_visuel_gestion"
                style="margin-left : -411px;">
                <i class="fas fa-plus-circle"></i> Communication
            </button>
        </div>
    </div>
    <div class="px-2">
        <div class="d-flex justify-content-center">
            <button id="btnTacheValide" data-id="<?= $prospect_id; ?>" class="btn btn-sm btn-primary"><i
                    class="fas fa-eye"></i> Tâches validées </button> &nbsp;
            <button id="btnTacheNonValide" data-id="<?= $prospect_id; ?>" class="btn btn-sm btn-primary d-none"><i
                    class="fas fa-eye"></i> Tâches non validées </button>
            <button id="btnFormTache" data-id="<?= $prospect_id; ?>"
                class="btn btn-sm btn-primary only_visuel_gestion "><i class="fas fa-plus-circle"></i> Tâche </button>
        </div>
    </div>
</div>
<!-- <hr> -->

<div class="row m-0">

    <div class="col-5 py-2">
        <?php foreach ($com as $value) { ?>
            <div class="card rounded-0">
                <div class="card-body ">
                    <div class="table-responsive scrollbar">
                        <table class="table">
                            <thead class="fs--1">
                                <tr id="rowCom-<?= $value->comprospect_id; ?>">
                                    <th scope="col">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar avatar-xl me-2">
                                                <div class="avatar-name rounded-circle">
                                                    <span>
                                                        <?= $value->util_prenom[0]; ?>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="ms-2"><?= $value->util_prenom ?></div>
                                            <div class="ms-2">
                                                <b>
                                                    <?= date("d/m/Y H:i:s", strtotime(str_replace('-', '/', $value->comprospect_date_creation))); ?>
                                                </b>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="col" class="text-center">
                                        <label for="">
                                            <?= $value->type_comprospect_libelle ?>
                                        </label>
                                    </th>
                                    <th class="text-end only_visuel_gestion" scope="col">
                                        <div>
                                            <button class="btn p-0 ms-2" id="updateCom" data-prospect="<?= $prospect_id; ?>"
                                                data-id="<?= $value->comprospect_id; ?>">
                                                <span class="text-500 fas fa-edit"></span>
                                            </button>
                                            <button class="btn p-0 ms-2 deleteCom" data-id="<?= $value->comprospect_id; ?>"
                                                type="button" data-bs-toggle="tooltip" data-bs-placement="top"
                                                title="Delete"><span class="text-500 fas fa-trash-alt"></span></button>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="fs--1">
                                <tr id="rowCom-<?= $value->comprospect_id; ?>" class="border-bottom">
                                    <td colspan="4" id="textare">
                                        <?= nl2br($value->comprospect_texte) ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- <hr id="rowCom-<?= $value->comprospect_id; ?>"> -->
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-7 py-2">
        <?php $this->load->view("prospects/taches/tache"); ?>
    </div>
</div>

<script>only_visuel_gestion()</script>