<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Fiche Lot : <?php echo $lot->progm_nom ?>
            (Prospects : <?=$prospect->prospect_nom.' '.$prospect->prospect_prenom;?>)
        </h5>
	</div>
	<div class="px-2">
		<button id="" data-id="<?= $prospect_id ?>" class="btn btn-sm btn-secondary mx-1 retourProspect">
			<i class="fas fa-undo me-1"></i>
			<span>Fiche prospect</span>
		</button>
		<button data-prospect="<?=$lot->prospect_id;?>" id="btnRetourListeLot" class="btn btn-sm btn-primary mx-1">
            <i class="fas fa-undo fa-w-16 me-1"></i>
            <span>Liste lot</span>
        </button>
	</div>
</div>

<div class="contenair-content">
	
	<ul class="nav nav-tabs" id="lotTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification_lot" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="mandat-tab" data-bs-toggle="tab" href="#tab-mandat" role="tab" aria-controls="tab-mandat" aria-selected="false">Mandat</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pno-tab" data-bs-toggle="tab" href="#tab-pno" role="tab" aria-controls="tab-pno" aria-selected="false">PNO</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="acte-tab" data-bs-toggle="tab" href="#tab-acte" role="tab" aria-controls="tab-acte" aria-selected="false">Acte</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="bail-tab" data-bs-toggle="tab" href="#tab-bail" role="tab" aria-controls="tab-bail" aria-selected="false">Bail</a>
		</li>
		
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="lotTabContent">
		<div class="tab-pane fade show active" id="tab-identification_lot" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification_lot/identification'); ?>
		</div>
		<div class="tab-pane fade" id="tab-mandat" role="tabpanel" aria-labelledby="mandat-tab">
			<?php $this->load->view('mandats/mandat'); ?>
		</div>
		<div class="tab-pane fade" id="tab-pno" role="tabpanel" aria-labelledby="pno-tab">
			<?php $this->load->view('pno/pno'); ?>
		</div>
		<div class="tab-pane fade" id="tab-acte" role="tabpanel" aria-labelledby="acte-tab">
			<?php $this->load->view('actes/acte'); ?>
		</div>
		<div class="tab-pane fade" id="tab-bail" role="tabpanel" aria-labelledby="bail-tab">
			<?php $this->load->view('bail/bail'); ?>
		</div>
		
	</div>

</div>