<style>
    .badge {
        cursor: pointer;
    }
</style>
<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Envoi de mail (Proposition PNO)</h5>
</div>
<input type="hidden" name="lot_id" id="lot_id" value="<?=$lot_id?>">
<input type="hidden" name="prospect_id" id="prospect_id" value="<?=$prospect_id?>">
<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <select class="form-select" name="emeteur_pno" id="emeteur_pno">
                                <?php foreach ($liste_emetteur as $key => $value) : ?>
                                    <option value="<?= $value->mess_id ?>" <?= $default_user == $value->util_id ? 'selected' : '' ?>>
                                        <?= $value->util_prenom ?>&nbsp;<?= $value->util_nom ?>&nbsp;(<?= $value->mess_email ?>)
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataires :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_pno" value="<?= $destinataire->prospect_email ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataire en copie :</label>
                            <input type="text" class="form-control fs--1" id="destinataire_pno_copie" value="">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object_pno" value="<?= $object_mail ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message_text_pno" style="height : 315px;"><?= $mailMessage ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <label data-width="200" for="recipient-name" class="col-form-label d-none" id="signature_pno">Aucune</label>
                            <img src="" alt="" id="image_signature" style="width:400px; height:130px;" class="d-none">
                        </div>
                    </div>                    
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary" data-id="<?=$pno_id?>" id="sendMailPnoProspect">Envoyer</button>
    </div>
</div>