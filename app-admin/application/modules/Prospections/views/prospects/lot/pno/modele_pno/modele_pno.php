<style type="text/css">
    .img {
        text-align: center;
        height: 50px;
        margin: 0px;
    }

    .title {
        font-weight: bold;
        text-align: center;
    }

    .sous-titre {
        text-decoration: underline;
    }

    table {
        table-layout: fixed;
        border: 1px solid black;
        border-collapse: collapse;
        width: 100%;
    }
    th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .name{
        width: 20%;
        font-weight: bold;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
    }

    .th1 {
        font-weight: bold;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 10px;
    }

    .th_table2{
        font-size: 14px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .td_table2{
        padding: 13px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }

    .text {
        font-weight: bold;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 11px;
    }

    .text-2 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 10px;
    }

    .text-3 {
        font-weight: bold;
    }

    .date {
        font-weight: bold;
        font-size: 10px;
        font-family: Arial, Helvetica, sans-serif;
        text-align: center;
    }

    .box {
        border: 1px solid black;
        padding: 10px;
        text-align: center;
        flex: 1;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 13px;
    }

    .text-4 {
        font-size: 10px;
        /* width: 48%; */
        font-family: Arial, Helvetica, sans-serif;
        /* margin-left: 28%; */
        border: 1px solid black;
    }

    .text-commun {
        margin: 5px;
    }

    .text-5 {
        font-size: 10px;
        font-family: Arial, Helvetica, sans-serif;
        border: 1px solid black;
    }

    .text-bold {
        font-weight: bold;
    }

    .container {
        width: 100%;
    }

    .text-box-left {
        width: 33.33%;
        float: left;
    }

    .text-box-right {
        width: 33.33%;
        float: right;
        height: 70px;
    }

</style>

<div class="img">
    <img src="<?= base_url('../assets/img/logo_pno.jpg') ?>" alt="">
</div><br><br><br>
<div class="title">
    <span>
        CONTRAT GROUPE MULTIRISQUES PROPRIETAIRE NON OCCUPANT « PNO IMMOPLUS »
    </span><br>
    <span class="sous-titre">BULLETIN INDIVIDUEL</span>
</div> <br>
<div class="table">
    <table>
        <thead>
            <tr>
                <th colspan="2" class="th1">Je soussigné le copropriétaire bailleur</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="name">
                    Nom / Prénom : <br><br>
                    Adresse : <br>
                    Code postal/ Ville : <br>
                </td>
                <td class="name">
                 <?= $pno->prospect_nom.' '.$pno->prospect_prenom?><br><br>
                 <?= $pno->prospect_adresse1?> <br>
                 <?= $pno->prospect_cp.' '.$pno->prospect_ville ?><br>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="text">
    <p>
        Ai pris connaissance de la souscription pour mon compte par C’EST LA VIE PROPERTIES de SAINT NAZAIRE de la garantie PNO IMMOPLUS souscrit auprès la compagnie GENERALI S.A. ci-après dénommée l’Assureur.
    </p>
    <p>
        Par la présente, je mandate expressément C’EST LA VIE PROPERTIES pour agir en mon nom concernant les lots ci-dessous afin qu’il prenne en charge la gestion des relations avec l’Assureur.
    </p>
</div>

<div class="">
    <table>
        <thead>
            <tr>
                <th class="th_table2"><i>ADRESSE DES BIENS GARANTIS</i></th>
                <th class="th_table2"> <i>CODE POSTAL</i> </th>
                <th class="th_table2"> <i>VILLE</i> </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="td_table2"><?=$pno->progm_adresse1?></td>
                <td class="td_table2"><?=$pno->progm_cp?></td>
                <td class="td_table2"><?=$pno->progm_ville?></td>
            </tr>
            <tr>
                <td class="td_table2"></td>
                <td class="td_table2"></td>
                <td class="td_table2"></td>
            </tr>
        </tbody>
    </table>

</div>
<br>

<div class="text-2">
    <span>
        Le Co-propriétaire bailleur déclare :
    </span> <br>
    <span>
        - avoir reçu préalablement, la Notice d’Information « PNO ImmoPlus», en avoir pris connaissance, et les accepter. <br>
        - que le Contrat tel que décrit dans la Notice d’Information constitue une solution adaptée au regard du besoin de bénéficier des garanties en cas de sinistre survenant dans le logement, raison pour laquelle il remplit le bulletin d’adhésion ci-dessus.
    </span> <br> <br>
    <span>
        Toute réticence, omission ou déclaration inexacte expose aux sanctions prévues aux articles L113-8 et L113-9 du Code des Assurances.
    </span><br><br>
    <span>
        En ma qualité d’assuré pour compte au titre de la garantie PNO IMMOPLUS, je reconnais être seul tenu au paiement de la prime envers l’Assureur.
    </span> <br><br>
    <span>
        Montant de la cotisation :
    </span><br>
    <span>
         49,00 € TTC/an dont 20 € ttc /an au titre de frais de gestion du souscripteur agissant pour le compte du copropriétaire bailleur.
    </span><br><br>
    <span class="text-3">
        La garantie prend effet le 1er du mois qui suit la signature du présent bulletin pour une première période allant jusqu’à l’échéance du
        contrat. Elle se renouvelle ensuite par tacite reconduction annuelle.
    </span>
</div> <br><br>

<div class="date">
    <span> Fait à <?=$pno->pno_date_creation?> </span> <br><br>
    <span> Le <?=$pno->pno_lieu_creation?>.</span>
</div>
<br>
<div class="container">
    <div class="text-box-left box">
        <span>
            Cachet et signature de <br>
            l’administrateur de biens <br> <br>
            Souscripteur

        </span>
    </div>
    <div class="text-box-right box">
        <span>
            Signature du copropriétaire <br>
            bailleur

        </span>
    </div>
</div>
<br>
<div class="text-4">
    <p class="text-commun">
        Conformément à l’article L112-2-1 du code des assurances, vous bénéficiez d’un délai de renonciation de 14 jours calendaires à partir de la date de signature du contrat PNO ImmoPlus. Vous pouvez renoncer à votre adhésion sans avoir à justifier de motif, ni à payer de pénalités, tel qu’exposé à l’article 3.4 des Conditions Générales. La cotisation déjà versée me sera restituée intégralement dans les 30 jours. Dans ce cas, vous devez informer Bessé Immobilier & Construction - de votre intention de renoncer à votre adhésion au Contrat PNO ImmoPlus à l’adresse suivante : Bessé Immobilier & Construction - 46 bis rue des Hauts Pavés – BP 80205 – 44002 NANTES, CEDEX 01 avec le libellé suivant :
        « Je soussigné(e)  <?= $pno->prospect_nom.' '.$pno->prospect_prenom?> demeurant à <?= $pno->prospect_adresse1?> déclare renoncer à mon adhésion au contrat «PNO ImmoPlus» n° AT381637 concernant le bien situé <?=$mandat->progm_adresse1.' '.$pno->progm_cp.' '.$pno->progm_ville.' '.$pno->progm_pays;?> auquel
        j’ai adhéré auprès de ………………. le ……/……/…… A……le……Date et signature ».
    </p>
    <p class="text-commun">
        L’Adhérent peut demander communication, rectification et suppression de toute information le concernant qui figurerait sur tout fichier à usage de GENERALI en écrivant à l’adresse
        suivante : <br>
        GENERALI <br>
        SERVICE RELATIONS CLIENTS TSA 70100 <br>
        75309 PARIS CEDEX 09 <br>
    </p> <br>
    <p class="text-commun">
        Dans le cadre de la relation d’assurance GENERALI est amené à recueillir auprès de l’Adhérent des données personnelles protégées par la loi n° 78-17 du 6 janvier 1978 modifiée dite Informatique et Libertés. Le caractère obligatoire ou facultatif des données personnelles demandées et les éventuelles conséquences à l’égard du Bénéficiaire d’un défaut de réponse sont précisés lors de leur(s) collecte(s).
        Les responsables du traitement de ces données personnelles sont GENERALI.
    </p>
    <p class="text-commun">
        A ce titre, l’Adhérent est informé que les données personnelles le concernant peuvent être transmises :
    </p>
    <p class="text-commun">
        • aux établissements et sous-traitants liés contractuellement avec GENERALI pour l’exécution de tâches se rapportant directement aux finalités décrites précédemment ;
    </p>
    <p class="text-commun">
        • à des organismes publics afin de satisfaire aux obligations légales ou réglementaires incombant à GENERALI ;
    </p>
    <p class="text-commun">
        Enfin, toute déclaration fausse ou irrégulière pourra faire l’objet d’un traitement spécifique destiné à prévenir la fraude. L’ensemble de ces données peut donner lieu à l’exercice du droit d’accès, de rectification et d’opposition dans les conditions et limites prévues par les articles 38, 39 et 40 de la loi n° 78-17 du 6 janvier 1978 modifiée.
    </p>
</div> <br>

<div class="text-5">
    <p class="text-commun">
        Le Contrat PNO ImmoPlus est distribué par la SAS <span class="text-bold">CB.IPL</span> (commercialement dénommée « BESSÉ Immobilier & Construction »), sis 135 Boulevard Haussmann 75008 Paris,
        société de courtage en assurances, au capital de 75 450 euros - immatriculée au RCS de PARIS sous le numéro 433 869 427, et auprès de l’ORIAS (www.orias.fr)
        sous le numéro 07 019 245, et garanti par – GENERALI SA, Entreprise régie par le Code des Assurances, 313 Terrasses de l’Arche,
        Société Anonyme au capital de 214 799 030 €, immatriculée au RCS de Nanterre sous le numéro 722 057 460, intervenant en qualité d’Assureur.
    </p>
    <p class="text-commun">
        GENERALI et Bessé sont soumis au contrôle de l’Autorité de Contrôle Prudentiel et de Résolution (ACPR), 4 place de Budapest 75009 Paris. <br>
        Pour toute réclamation concernant cette offre,vous pouvez écrire à GENERALI, Service Relation Clients – TSA 70100 – 75309 PARIS CEDEX 09
    </p>
</div>