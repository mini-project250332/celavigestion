<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Clôturer un PNO</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Pnoprospect/CloturePno', array('method' => "post", 'class' => 'px-2', 'id' => 'CloturePno')); ?>
<input type="hidden" name="pno_id" id="pno_id" value="<?= $pno_id; ?>">
<input type="hidden" name="lot_id" id="lot_id" value="<?= $lot_id; ?>">
<input type="hidden" name="prospect_id" id="prospect_id" value="<?= $prospect_id; ?>">
<input type="hidden" name="pno_date_signature" id="pno_date_signature" value="<?= $pno->pno_date_signature; ?>">

<div class="modal-body">
    <div class="row m-0">
        <p class="text-center"><i>Il est possible de clôturer la PNO avec un délai d'un mois, après un an minimum de contrat</i></p>
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Date de la clôture: </label>
                    <input type="date" class="form-control" id="pno_date_cloture" name="pno_date_cloture" value="<?= date('Y-m-d') ?>">
                    <div class="text-danger help error_date"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Fichier justificatif : </label>
                    <input type="file" class="form-control" id="pno_fichier_cloture" name="pno_fichier_cloture">
                    <div class="text-danger help error_file"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
    $(document).ready(function() {
        $(document).on('keyup', '#pno_date_cloture', function(e) {
            var pno_date_signature = new Date($('#pno_date_signature').val());
            var pno_date_cloture = new Date($('#pno_date_cloture').val());
            var oneYearLater = new Date(pno_date_signature.getFullYear() + 1, pno_date_signature.getMonth(), pno_date_signature.getDate());
          
            if (pno_date_cloture <= oneYearLater) {
                $('div').children('.error_date').text("Le contrat doit avoir au moins un an d'ancienneté.");
            } else {
                $('div').children('.error_date').text("");
            }
        });
    });
</script>