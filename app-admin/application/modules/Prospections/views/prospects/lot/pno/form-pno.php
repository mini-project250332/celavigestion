<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Créer un PNO</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Pnoprospect/CreerPno',array('method'=>"post", 'class'=>'px-2','id'=>'CreerPno')); ?>

<input type="hidden" name="lot_id" id="lot_id" value="<?=$lot_id;?>">
<input type="hidden" name="prospect_id" id="prospect_id" value="<?=$prospect_id;?>">
<input type="hidden" name="action" id="action" value="<?=$action;?>">
<input type="hidden" name="pno_id" id="pno_id" value="<?=$pno_id?>">

<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Date : </label>
                    <input type="text" class="form-control calendar" id="pno_date_creation" name="pno_date_creation" 
                    value="">
                    <div class="help"></div>
                </div>
            </div>           
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Lieu : </label>
                    <input type="text" class="form-control" id="pno_lieu_creation" name="pno_lieu_creation" 
                    value="">
                    <div class="help"></div>
                </div>
            </div>           
            
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
	
	flatpickr(".calendar",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
			defaultDate: moment().format('DD-MM-YYYY'),
		}
	);

</script>