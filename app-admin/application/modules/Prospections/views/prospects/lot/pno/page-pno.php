<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Liste des PNO</h5>
        </div>

        <input type="hidden" id="lot_id" value="<?= $lot_id ?>" />
        <input type="hidden" id="prospect_id" value="<?= $prospect_id ?>" />

        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 d-none" id="ajoutPnoclient" data-pno-id="<?= isset($pno->pno_id) ? $pno->pno_id : ""; ?>" data-id="<?= $lot_id ?>" data-client="<?= $prospect_id ?>">
                    <span class="fas fa-plus me-1"></span>
                    <span> PNO </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left">
        <?php echo form_tag('Pnoprospect/AddPno', array('method' => "post", 'class' => 'px-2', 'id' => 'AddPno')); ?>
        <div class=" col d-md-flex justify-content-md-end">
            <button type="submit" class="btn btn-primary btn-sm float-right d-none"> </button>
        </div>
        <input type="hidden" name="lot_id" id="lot_id" value="<?= $lot_id; ?>">
        <input type="hidden" name="prospect_id" id="lot_id" value="<?= $prospect_id; ?>">
        <input type="hidden" id="action" name="action" value="<?= !empty($pno) ? 'edit' : 'add'; ?>" />
        <input type="hidden" id="pno_id" name="pno_id" value="<?= isset($pno->pno_id) ? $pno->pno_id : ""; ?>" />
        <div class="alert alert-secondary">
            <label for="question1">PNO souscrite par celavigestion : </label>
            <div class="row">
                <div class="col-2">
                    <div class="form-check ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="ouicelaviegestion" id="ouicelaviegestion" value="<?= isset($pno->pno_celavi) ? $pno->pno_celavi : ""; ?>">
                        <label class="form-check-label" for="ouicelaviegestion">
                            Oui
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-check  ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="noncelaviegestion" id="noncelaviegestion" value="<?= isset($pno->pno_celavi) ? $pno->pno_celavi : ""; ?>">
                        <label class="form-check-label" for="noncelaviegestion">
                            Non
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <h5 class="d-none" id="label_choix1">PNO souscrite par celavigestion</h5> <br>

        <div class="d-none" id="table_pno">
            <table class="table table-hover fs--1">
                <thead class="text-white">
                    <tr class="text-center">
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Numéro
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Créé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Envoyé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Signé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            clôturé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Etat
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-200 fs--1 <?= isset($pno->pno_date_creation) ? "" : "d-none"; ?>">
                    <tr class="apercu_Pno" id="rowdoc-<?= isset($pno->pno_id) ? $pno->pno_id : ""; ?>">
                        <td class="text-center bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= isset($pno->pno_id) ? $pno->pno_id : ""; ?>" onclick="apercuPno(<?= isset($pno->pno_id) ? $pno->pno_id : '' ?>)">
                            <?= str_pad(isset($pno->pno_id) ? $pno->pno_id : '', 4, '0', STR_PAD_LEFT) ?>
                        </td>
                        <td class="text-center"><?= date("d/m/Y", strtotime(str_replace('/', '-', $pno->pno_date_creation))) ?></td>
                        <td class="text-center"><?= isset($pno->pno_date_envoi) ? date("d/m/Y", strtotime(str_replace('/', '-', $pno->pno_date_envoi))) : '-' ?></td>
                        <td class="text-center"><?= isset($pno->pno_date_signature) ? date("d/m/Y", strtotime(str_replace('/', '-', $pno->pno_date_signature))) : '-' ?></td>
                        <td class="text-center"><?= isset($pno->pno_date_cloture) ? date("d/m/Y", strtotime(str_replace('/', '-', $pno->pno_date_cloture))) : '-' ?></td>
                        <td class="text-center"><?= $pno->etat_pno_libelle ?></td>
                        <td class="text-center">
                            <div class="btn-group fs--1">
                                <!-- <span class="btn btn-sm btn-outline-warning rounded-pill updatemandatCli <?= $pno->etat_pno_id != 1 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier">
                                    <i class="fas fa-edit"></i>
                                </span>&nbsp; -->
                                <span class="btn btn-sm btn-outline-secondary sendPnoCli rounded-pill <?= $pno->etat_pno_id != 1  ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer" data-id="<?= $pno->pno_id ?>">
                                    <i class="fas fa-file-import"></i>
                                </span>&nbsp;
                                <span class="btn btn-sm btn-outline-primary rounded-pill ">
                                    <i class="fas fa-download" onclick="forceDownload('<?= base_url() . $pno->pno_path ?>')" data-url="<?= base_url($pno->pno_path) ?>">
                                    </i>
                                </span>&nbsp;
                                <span class="btn btn-sm btn-outline-secondary rounded-pill signerPnoCli <?= $pno->etat_pno_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Signer" data-id="<?= $pno->pno_id ?>">
                                    <i class="fas fa-file-signature"></i>
                                </span>&nbsp;
                                <span class="btn btn-sm btn-outline-warning rounded-pill clos_sans_suitePno <?= $pno->etat_pno_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Clos sans suite" data-id="<?= $pno->pno_id ?>">
                                    <i class="fas fa-archive"></i>
                                </span>
                                <span class="btn btn-sm btn-outline-danger rounded-pill cloturerPno <?= $pno->etat_pno_id != 3 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Clôturer" data-id="<?= $pno->pno_id ?>">
                                    <i class="fas fa-hand-paper"></i>
                                </span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>

        <div class="alert alert-secondary d-none" id="questionnaire2">
            <label for="question1">PNO souscrite par le syndic : </label>
            <div class="row">
                <div class="col-2">
                    <div class="form-check ms-3">
                        <input class="form-check-input auto_effect" name="ouisyndic" type="checkbox" value="<?= isset($pno->pno_syndic) ? $pno->pno_syndic : ""; ?>" id="ouisyndic">
                        <label class="form-check-label" for="question1">
                            Oui
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-check  ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="nonsyndic" value="<?= isset($pno->pno_syndic) ? $pno->pno_syndic : ""; ?>" id="nonsyndic">
                        <label class="form-check-label" for="question1-2">
                            Non
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-check ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="nesaitpassyndic" value="<?= isset($pno->pno_syndic) ? $pno->pno_syndic : ""; ?>" id="nesaitpassyndic">
                        <label class="form-check-label" for="question1-2">
                            Ne sait pas
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <h5 class="d-none" id="label_choix2">PNO souscrite par le syndic</h5><br>

        <div class="d-none" id="formulaire2">
            <div class="col">
                <div class="form-group inline p-0">
                    <div class="col">
                        <br>
                        <label data-width="250">Montant TTC :</label>
                        <div class="input-group">
                            <input type="text" placeholder="0" class="form-control mb-0 chiffre pno_montant_syndic" id="pno_montant" name="pno_montant" style="text-align: right; height : 36px;" value="<?= isset($pno->pno_montant) ? $pno->pno_montant : "" ?>">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">€ TTC</span>
                            </div>
                        </div>
                        <div class="help alert_montant_syndic" style="margin: 10px;">
                            <p id="montant_syndic" class="" role="alert"><span></span> </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col" style="margin-top: 8px;">
                <div class="form-group inline p-0">
                    <?php if (!empty($pno->pno_path)) : ?>
                        <?php $nom_fic = explode("/", $pno->pno_path); ?>
                        <span class="badge rounded-pill badge-soft-secondary pno_file" onclick="forceDownload('<?= base_url() . $pno->pno_path ?>')" data-url="<?= base_url($pno->pno_path) ?>" style="cursor: pointer;margin-left: 207px;"><?= $nom_fic[5] ?></span>
                        <span class="badge rounded-pill badge-soft-light btn-close text-white pno_file" id="removefilePno" style="cursor: pointer;">.</span><br>
                    <?php endif ?>
                    <div class="col">
                        <label data-width="250">Fichier joint :</label>
                        <input type="file" id="pno_path" name="pno_path" class="form-control auto_effect pno_path">
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end d-none" id="send_mail_pno_chere_syndic">
                <button class="btn btn-outline-primary" id="send_mail_pno_chere" data-id="<?= isset($pno->pno_id) ? $pno->pno_id : "" ?>">
                    Envoyer une proposition commerciale (PNO Chère)
                </button>
            </div>
        </div>

        <div class="alert alert-secondary d-none" id="questionnaire3">
            <label for="question1">PNO souscrite par le client : </label>
            <div class="row">
                <div class="col-2">
                    <div class="form-check ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="ouiclient" value="<?= isset($pno->pno_client) ? $pno->pno_client : ""; ?>" id="ouiclient">
                        <label class="form-check-label" for="question1">
                            Oui
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-check  ms-3">
                        <input class="form-check-input auto_effect" type="checkbox" name="nonclient" value="<?= isset($pno->pno_client) ? $pno->pno_client : ""; ?>" id="nonclient">
                        <label class="form-check-label" for="question1-2">
                            Non
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <input class="form-check-input auto_effect" type="checkbox" name="nesaitpasclient" value="<?= isset($pno->pno_client) ? $pno->pno_client : ""; ?>" id="nesaitpasclient">
                    <label class="form-check-label" for="question1-2">
                        Ne sait pas
                    </label>
                </div>
            </div>
        </div>

        <h5 class="d-none mb-1" id="label_choix3">PNO souscrite par le client</h5><br>

        <div class="d-none" id="formulaire3">
            <div class="col">
                <div class="form-group inline p-0">
                    <div class="col">
                        <br>
                        <label data-width="250">Montant TTC :</label>
                        <div class="input-group">
                            <input type="text" placeholder="0" class="form-control mb-0 chiffre pno_montant_client" id="pno_montant" name="pno_montant_client" style="text-align: right; height : 36px;" value="<?= isset($pno->pno_montant) ? $pno->pno_montant : "" ?>">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">€ TTC</span>
                            </div>
                        </div>
                        <div class="help alert_montant_client" style="margin: 10px;">
                            <p id="montant_client" class="" role="alert"><span></span> </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col" style="margin-top: 8px;">
                <div class="form-group inline p-0">
                    <?php if (!empty($pno->pno_path)) : ?>
                        <?php $nom_fic = explode("/", $pno->pno_path); ?>
                        <span class="badge rounded-pill badge-soft-secondary pno_file" onclick="forceDownload('<?= base_url() . $pno->pno_path ?>')" data-url="<?= base_url($pno->pno_path) ?>" style="cursor: pointer;margin-left: 207px;"><?= $nom_fic[5] ?></span>
                        <span class="badge rounded-pill badge-soft-light btn-close text-white pno_file" id="removefilePno" style="cursor: pointer;">.</span><br>
                    <?php endif ?>
                    <div class="col">
                        <br>
                        <label data-width="250">Fichier joint :</label>
                        <input type="file" class="form-control auto_effect" id="pno_path_client" name="pno_path_client">
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end d-none" id="send_mail_pno_chere_client">
                <button class="btn btn-outline-primary" id="send_mail_pno_chere" data-id="<?= isset($pno->pno_id) ? $pno->pno_id : "" ?>">
                    Envoyer une proposition commerciale (PNO Chère)
                </button>
            </div>
        </div>

        <div class="col d-none d-flex justify-content-end" id="button_send_mail_pno">
            <button class="btn btn-outline-primary" id="send_mail_pno" data-id="<?= isset($pno->pno_id) ? $pno->pno_id : "" ?>">
                Envoyer une proposition commerciale
            </button>
        </div>

    </div>
    <?php echo form_close(); ?>

    <div class="splitter m-2">

    </div>
    <div class="col-5 panel-right">
        <div id="apercu_doc_pno">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercuPno">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>