<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Renommer un fichier</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('ActeProspect/UpdateDocumentActe_Prospect',array('method'=>"post", 'class'=>'px-2','id'=>'UpdateDocumentActe_Prospect')); ?>
 
	<?php if(isset($document->docp_acte_id)):?>
        <input type="hidden" name="docp_acte_id" id="docp_acte_id" value="<?= isset($document->docp_acte_id) ? $document->docp_acte_id : "" ;?> ">
    <?php endif;?>

    <input type="hidden" name="lot_id" id="lot_id" value="<?=$lot_id;?>">
    <div class="modal-body">

        <div class="row m-0">
    		<div class="col py-1">
    			<div class="">
	                <div class="">
		                <div class="form-group inline">
		                    <div>
		                        <label data-width="100"> Nom : </label>
		                        <input type="text" class="form-control" id="docp_acte_nom" name="docp_acte_nom" 
                                value="<?=  $document->docp_acte_nom ?> ">
		                        <div class="help"></div>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
    </div>
<?php echo form_close(); ?>


 
