<div class="uploader_file mt-4">
    
    </div>
    <div class="clearfix p-t-5 p-b-5">
        <div class="float-left">
        </div>
        <div class="d-flex justify-content-end">
            <button type="button" id="stopped-upload" class="btn btn-danger d-none btn-sm mx-2 text-white">
                <i class="fas fa-times-circle red"></i>
                Annuler
            </button>
            <button type="button" id="add-files-upload" class="btn btn-info btn-sm bold mx-1">
                <i class="fas fa-plus-circle"></i>
                Ajouter des fichiers 
            </button>
            <button type="button" id="start-files-upload" class="btn btn-success btn-sm d-none text-white">
                <i class="fas fa-cloud-upload-alt green "></i>
                Démarrer l'envoi 
            </button>
        </div>
    </div>
    
    <input id="url_upload" type="hidden" name="url_upload" value="<?php echo base_url("ActeProspect/ActeProspect/uploadfileActe");?>">
    <input type="hidden" name="lot_id" id="lot_id" value="<?=$lot_id;?>">
    
    <script type="text/javascript">
        $(document).ready(function() {
           
            var url_upload = $('#url_upload').val();
            $(".uploader_file").pluploadQueue({
                browse_button : 'add-files-upload',
                runtimes : 'html5,flash,silverlight,html4',
                chunk_size: '1mb',
                url : url_upload,
                rename: true,
                unique_names : true,
                sortable: true,
                dragdrop: true,
                flash_swf_url : '<?php echo base_url("assets/js/plupload/js/Moxie.swf");?>',
                silverlight_xap_url : '<?php echo base_url("assets/js/plupload/js/Moxie.xap");?>',
            });
    
            var uploader = $('.uploader_file').pluploadQueue();
            uploader.bind('Browse', function () {
                
            });
            uploader.bind('FilesRemoved', function () {
                if (uploader.files.length > 0) {
                    $('#start-files-upload').removeClass('d-none');
                }else{
                    $('#start-files-upload').addClass('d-none');
                    $('#stopped-upload').addClass('d-none');
                }
            });
            uploader.bind('FilesAdded', function() {
                if (uploader.files.length > 0) {
                    $('#start-files-upload').removeClass('d-none');
                }else{
                    $('#start-files-upload').addClass('d-none');
                    $('#stopped-upload').addClass('d-none');
                }
            });
            uploader.bind('UploadComplete', function() {
                // upload finish
    
            });
            uploader.bind('FileUploaded', function(response) {
                if(uploader.files.length > 0){
                    if (uploader.files.length == (uploader.total.uploaded + uploader.total.failed)) {
                        $("#modalAjoutDocActe").modal('hide');
                        var lot_id = $('#lot_id').val();
                        getLotActe(lot_id);
                    }
                }
            });
    
            $(document).on('click','#start-files-upload',function(e){
                e.preventDefault();
                var check = false;
                if (uploader.files.length > 0) {
                    var files_names = {}; 
                    var pathname_url = window.location.pathname;
                    var lot_id = $('#lot_id').val();
                    $("div.plupload_file_name").each(function(ind,elem) {
                        var content = $(this).html();  
                        var infos_file = {}; 
                        if (content != $.trim('Nom du fichier')){
                            
                            if($(this).parent().attr("id") != null){
                                infos_file.originalName = content;
                                infos_file.fileName= $(this).parent().attr("id");
                                files_names[ind] = infos_file; 
                            }
                        }
                    });
                    $('#stopped-upload').removeClass('d-none');
                    
                    uploader.settings.multipart_params = {
                        lot_id: lot_id,
                        fichiers : files_names, // Injection des noms des fichiers 
                    };
    
                    uploader.start();
                }
                
            });
    
            $(document).on('click','#stopped-upload',function(e){
                uploader.stop();
            });
        });
    </script>