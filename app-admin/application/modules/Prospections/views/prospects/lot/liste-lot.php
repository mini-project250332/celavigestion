<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <div class="d-flex justify-content-end">
            <button id="btnFormNewlot" data-id="<?= $prospect_id; ?>"
                class="btn btn-sm btn-primary only_visuel_gestion"><i class="fas fa-plus-circle"></i> Lots</button>
        </div>
    </div>
</div>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped fs--1 mb-0">
        <?php if (empty($lot)): ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun lot trouvé
                    </div>
                </div>
            </div>
        <?php else: ?>
            <thead class="bg-300 text-900">
                <tr>
                    <th scope="col">Nom programme</th>
                    <th scope="col">Ville</th>
                    <th scope="col">Lot principal</th>
                    <th scope="col">Type du Lot</th>
                    <th scope="col">Nom gestionnaire</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lot as $lots) { ?>
                    <?php
                    $type = $lots->lot_principale;
                    $value;
                    if ($type == 1) {
                        $value = "Oui";
                    } else {
                        $value = "Non";
                    }
                    ?>
                    <tr id="rowLot-<?= $lots->lot_id; ?>">
                        <td>
                            <?= $lots->progm_nom; ?>
                        </td>
                        <td>
                            <?= $lots->progm_ville ?>
                        </td>
                        <td>
                            <?= $value ?>
                        </td>
                        <td>
                            <?= $lots->typelot_libelle; ?>
                        </td>
                        <td>
                            <?= $lots->gestionnaire_nom; ?>
                        </td>
                        <td class="text-end">
                            <div>
                                <div class="d-flex justify-content-end ">
                                    <button data-prospect_id="<?= $prospect_id; ?>" data-id="<?= $lots->lot_id ?>" id="lot_btn_<?= $lots->lot_id ?>"
                                        class="btn btn-sm btn-outline-primary icon-btn p-0 m-1 btnFicheLot" type="button">
                                        <span class="fas fa-play fs-1 m-1"></span>
                                    </button>
                                    <button data-prospect_id="<?= $prospect_id; ?>" data-id="<?= $lots->lot_id ?>"
                                        class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 btnConfirmSupLot only_visuel_gestion"
                                        type="button">
                                        <span class="bi bi-trash fs-1 m-1"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        <?php endif ?>
    </table>
</div>

<script>
    only_visuel_gestion();
    const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const lot_id = urlParams.get('_lo@t_id');
    if (lot_id) $(`#lot_btn_${lot_id}`).trigger("click");
	
</script>