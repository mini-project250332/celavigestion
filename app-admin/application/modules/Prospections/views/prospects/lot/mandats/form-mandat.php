<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Créer un mandat</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Prospections/AddMandat',array('method'=>"post", 'class'=>'px-2','id'=>'AddMandat')); ?>

<input type="hidden" name="lot_id" id="lot_id" value="<?=$lot_id;?>">
<input type="hidden" name="prospect_id" id="prospect_id" value="<?=$prospect_id;?>">
<input type="hidden" name="action" id="action" value="<?=$action;?>">
<input type="hidden" name="mandat_id" id="mandat_id" value="<?=isset($mandat->mandat_id) ? $mandat->mandat_id : "";?>">
<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Date : </label>
                    <input type="text" style="background-color : #EDF2F9 !important" class="form-control calendar" id="mandat_date_creation" name="mandat_date_creation" 
                    value="<?=isset($mandat->mandat_date_creation) ? $mandat->mandat_date_creation : ""?>" disabled>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Type du mandat : </label>
                        <select class="form-select fs--1" id="type_mandat_id" name="type_mandat_id" style="left: 3px;">
                        <?php foreach ($typemandat as $key => $type) {?>
                            <option value=" <?=$type->type_mandat_id;?>" ><?=$type->type_mandat_libelle?></option>
                        <?php } ?>
                        </select>
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Lieu de facturation : </label>
                    <input type="text" class="form-control" id="mandat_lieu_facturation" name="mandat_lieu_facturation" 
                    value="<?=isset($mandat->mandat_lieu_facturation) ? $mandat->mandat_lieu_facturation : ""?>">
                    <div class="help"></div>
                </div>
            </div>            
            <div class="form-group inline">
                <div>
                    <label data-width="200"> Montant : </label>
                    <div class="input-group">
                        <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="mandat_montant_ht" name="mandat_montant_ht" style="text-align: right; height : 36px;"
                        value="<?=isset($mandat->mandat_montant_ht) ? $mandat->mandat_montant_ht : ""?>">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">€ HT</span>
                        </div>
                    </div>
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
	
	flatpickr(".calendar",
		{
			"locale": "fr",
			enableTime: false,
			dateFormat: "d-m-Y",
			defaultDate: moment().format('DD-MM-YYYY'),
		}
	);

    $(document).ready(function (){
		$(document).on('change', '#type_mandat_id', function (e) { 
			var selected_option = $('#type_mandat_id').val();
			if (selected_option == 1) {
				$("#mandat_montant_ht").val(295);
			}else if(selected_option == 2){
				$("#mandat_montant_ht").val(39);
			} else{
                $("#mandat_montant_ht").val(295);
            }
		});
	});

</script>