<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white text-center" id="exampleModalLabel">Clôturer un mandat</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Prospections/AddSignMandat', array('method' => "post", 'class' => 'px-2', 'id' => '')); ?>
<input type="hidden" name="mandat_id" id="mandat_id" value="<?= $mandat_id; ?>">
<input type="hidden" name="lot_id" id="lot_id" value="<?= $lot_id; ?>">
<input type="hidden" name="prospect_id" id="prospect_id" value="<?= $prospect_id; ?>">
<div class="modal-body">

    <div class="row m-0">
        <div class="col py-1">
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Date de la demande : </label>
                    <input type="text" class="form-control calendar" id="mandat_date_signature" name="mandat_date_signature" value="">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Date de fin présumée : </label>
                    <input type="text" class="form-control calendar" id="mandat_date_signature" name="mandat_date_signature" value="">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Fichier justificatif  : </label>
                    <input type="file" class="form-control" id="mandat_path" name="mandat_path">
                    <div class="help"></div>
                </div>
            </div>
            <div class="form-group inline">
                <div>
                    <label data-width="300"> Type de clôture : </label>
                    <select class="form-select" id="indivision" name="indivision">
                        <option value="">clôture par CELAVI (respect du préavis)</option>
                        <option value="">clôture par CELAVI (déshérence sans préavis)</option>
                        <option value="">clôture par CLIENT (respect du préavis)</option>
                        <option value="">clôture par CLIENT (Ventes préavis 3 mois)</option>
                        <option value="">suspension par CELAVI </option>
                    </select>
                    <div class="help"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
</div>

<?php echo form_close(); ?>

<script>
    flatpickr(".calendar", {
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        // defaultDate: moment().format('DD-MM-YYYY'),
    });
</script>