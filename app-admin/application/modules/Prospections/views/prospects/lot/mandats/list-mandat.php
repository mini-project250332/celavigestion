<div class="contenair-title">
    <div class="row m-0" style="width: 50%">
        <div class="col">
            <h5 class="fs-1">Liste des mandats</h5>
        </div>

        <input type="hidden" id="lot_id_mandat" value="<?= $lot_id ?>" />
        <input type="hidden" id="prospect_id" value="<?= $prospect_id ?>" />

        <div class="col">
            <div class="d-flex justify-content-end">
                <button class="btn btn-sm btn-primary m-1 ajoutMandat" data-id="<?= $lot_id ?>" data-prospect="<?= $prospect_id ?>">
                    <span class="fas fa-plus me-1"></span>
                    <span> Mandat </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row pt-3 m-0 panel-container" id="">
    <div class="col-6 panel-left">
        <?php if (empty($mandat)) : ?>
            <div class="row">
                <div class="col-12 pt-2">
                    <div class="text-center">
                        Aucun mandat créé
                    </div>
                </div>
            </div>
        <?php else : ?>
            <table class="table table-hover fs--1">
                <thead class="text-white">
                    <tr class="text-center">
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Numéro
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Créé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Envoyé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Signé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Montant (€ HT)
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Clôturé le
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Etat
                        </th>
                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-200 fs--1">
                    <?php foreach ($mandat as $mandats) : ?>
                        <tr class="apercu_docu" id="rowdoc-<?= $mandats->mandat_id ?>">
                            <td class="text-center bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $mandats->mandat_id ?>" onclick="apercuMandat(<?= $mandats->mandat_id ?>)">
                                <?= str_pad($mandats->mandat_id, 4, '0', STR_PAD_LEFT) ?>
                            </td>
                            <td class="text-center"><?= date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_creation))) ?></td>
                            <td class="text-center"><?= isset($mandats->mandat_date_envoi) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_envoi))) : '-' ?></td>
                            <td class="text-center"><?= isset($mandats->mandat_date_signature) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_signature))) : '-' ?></td>
                            <td class="text-center"><?= $mandats->mandat_montant_ht ?></td>
                            <td class="text-center"><?= isset($mandats->mandat_date_action_cloture) ? date("d/m/Y", strtotime(str_replace('/', '-', $mandats->mandat_date_action_cloture))) : '-' ?></td>
                            <td class="text-center"><?= $mandats->etat_mandat_libelle ?></td>
                            <td class="text-center">
                                <div class="btn-group fs--1">
                                    <span class="btn btn-sm btn-outline-warning rounded-pill updatemandat <?= $mandats->etat_mandat_id != 1 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier" data-mandat="<?= $mandats->mandat_id ?>" data-id="<?= $lot_id ?>" data-prospect="<?= $prospect_id ?>">
                                        <i class="fas fa-edit"></i>
                                    </span>&nbsp;
                                    <span class="btn btn-sm <?= $mandats->etat_mandat_id == 3 ? 'btn-secondary' : 'btn-danger' ?> rounded-pill">
                                        <i class="<?= $mandats->etat_mandat_id == 3 ? 'text-danger' : '' ?> far fa-file-pdf" onclick="forceDownload_mandat_new('<?= base_url() . $mandats->mandat_path ?>')" data-url="<?= base_url($mandats->mandat_path) ?>">
                                        </i>
                                    </span>&nbsp;
                                    <!-- <span class="btn btn-sm btn-outline-secondary rounded-pill envoiMandat <?= $mandats->etat_mandat_id == 1 || $mandats->etat_mandat_id == 2 ? "" : "d-none" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer"
                                        data-id="<?= $mandats->mandat_id ?>">
                                        <i class="fas fa-envelope"></i>
                                    </span>&nbsp; -->
                                    <span class="btn btn-sm btn-outline-secondary sendMandat rounded-pill <?= $mandats->etat_mandat_id != 1  ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Envoyer" data-id="<?= $mandats->mandat_id ?>">
                                        <i class="fas fa-file-import"></i>
                                    </span>&nbsp;
                                    <span class="btn btn-sm btn-outline-primary rounded-pill signerMandat <?= $mandats->etat_mandat_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Signer" data-id="<?= $mandats->mandat_id ?>">
                                        <i class="fas fa-file-signature"></i>
                                    </span>&nbsp;
                                    <span class="btn btn-sm btn-outline-warning rounded-pill clos_sans_suiteMandat <?= $mandats->etat_mandat_id != 2 ? "d-none" : "" ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Clos sans suite" data-id="<?= $mandats->mandat_id ?>">
                                        <i class="fas fa-file-archive"></i>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <div class="splitter m-2">

    </div>
    <div class="col-5 panel-right">
        <div id="apercudocMandat">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Aperçu non disponible pour ce type de
                    fichier
                </div>

            </div>
            <div id="apercu">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></i></button>
                </div>
            </div>
        </div>
    </div>
</div>