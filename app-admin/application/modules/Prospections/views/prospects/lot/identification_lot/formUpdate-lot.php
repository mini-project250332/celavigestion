<?php echo form_tag('Prospections/updateLot',array('method'=>"post", 'class'=>'px-2','id'=>'updateLot')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class=" px-2">
		<button type="submit" id="save-lot" class="btn btn-sm btn-primary mx-1">
			<i class="fas fa-check-circle"></i>
			<span> Terminer </span>
		</button>
	</div>
</div>

<?php if(isset($lot->lot_id)):?>
        <input type="hidden" name="lot_id" id="lot_id" value="<?=$lot->lot_id;?>">
    <?php endif;?>

    <!-- <input type="hidden" name="action" id="action" value="<?=$action?>"> -->
    <!-- <input type="hidden" name="prospect_id" id="prospect_id" value="<?=$prospect_id;?>"> -->
<div id="div-fiche-prospect" class="row m-0">

<div class="col-12 p-0 py-1">
    
    <div class="row m-0">

        <div class="col p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="fs-0">Identification</h5>
                    <hr class="mt-0">
                    <div class="mt-4">
                    <table class="table table-borderless fs--1 mb-0">
                        <tbody>
                            <tr class="border-bottom">
                                <div class="form-group inline">
                                    <div class="col-sm-12">
                                        <label data-width="250"> Programme </label>
                                        <select class="form-select fs--1" id="progm_id" name="progm_id" style="margin-right : 3px; width : 147%;">
                                            <?php foreach ($program as $programme ) :?>
                                                <option value="<?= $programme->progm_id ?>"
                                                <?php echo isset($lot->progm_id) ? (($lot->progm_id !== NULL) ? (($lot->progm_id==$programme->progm_id) ? "selected" : " ") : "" ) : "" ?>>
                                                <?= $programme->progm_nom ?>
                                                </option>
                                            <?php endforeach;?>
                                        </select>
                                        <div class="help"></div>
                                        <div class="">
                                            <button id="addProgramme" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Adresse 1 </label>
                                        <input type="text" class="form-control" id="progm_adresse1" name="progm_adresse1" 
                                        value ="<?php echo isset($lot->progm_adresse1) ? $lot->progm_adresse1 : "" ?>">
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Adresse 2</label>
                                        <input type="text" class="form-control" id="progm_adresse2" name="progm_adresse2" value ="<?php echo isset($lot->progm_adresse2) ? $lot->progm_adresse2 : "" ?>" >
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Adresse 3 </label>
                                        <input type="text" class="form-control" id="progm_adresse3" name="progm_adresse3" value ="<?= isset($lot->progm_adresse3) ? $lot->progm_adresse3 : "" ;?>">
                                        <div class="help"> </div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Code postal </label>
                                        <input type="text" class="form-control" id="progm_cp" name="progm_cp" value ="<?= isset($lot->progm_cp) ? $lot->progm_cp : "" ;?>">
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Ville </label>
                                        <input v-on:keyup='input_form' type="text" class="form-control maj" id="progm_ville" name="progm_ville" value ="<?= isset($lot->progm_ville) ? $lot->progm_ville : "" ;?>">
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="150"> Pays </label>
                                            <input type="text" class="form-control maj" id="progm_pays" name="progm_pays" value ="<?= isset($lot->progm_pays) ? $lot->progm_pays : "" ;?>" >
                                        <div cclass="help fs-11 height-15 m-0"></div>
                                    </div>
                                </div>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-1">
            
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="fs-0">Informations du lot</h5>
                    <hr class="mt-0">
                    <div class="mt-4">
                    <table class="table table-borderless fs--1 mb-0">
                        <tbody>
                        <tr class="border-bottom">
                                
                                <div class="form-group inline">
                                    <div class="col-sm-12">
                                        <label data-width="200"> Lot principal </label>
                                        <select class="form-select fs--1" id="lot_principale" name="lot_principale" style="width:104%;">
                                        <?php 
                                            $array_type = unserialize(lot_principal);
                                            
                                            foreach ($array_type  as $type) {
                                                echo '<option value="'.$type['value'].'"  '.(($lot->lot_principale == $type['value']) ? "selected" : "").'  >'.$type['lotprincipal'].'</option>';
                                            }
                                        ?>
                                        </select>
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"> Type de lot </label>
                                        <select class="form-select fs--1" id="typelot_id" name="typelot_id" >
                                            <?php foreach ($typelot as $typelots ) :?>
                                                <option value="<?= $typelots->typelot_id ?>" 
                                                <?php echo isset($lot->typelot_id) ? (($lot->typelot_id !== NULL) ? (($lot->typelot_id==$typelots->typelot_id) ? "selected" : " ") : "") : "" ?>>
                                                <?= $typelots->typelot_libelle ?>
                                                </option>
                                            <?php endforeach;?>
                                        </select>
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Typologie </label>
                                            <select class="form-select fs--1" id="typologielot_id" name="typologielot_id" >
                                            <?php foreach ($typologie as $typologie ) :?>
                                                <option value="<?= $typologie->typologielot_id ?>"
                                                <?php echo isset($lot->typologielot_id) ? (($lot->typologielot_id !== NULL) ? (($lot->typologielot_id==$typologie->typologielot_id) ? "selected" : " ") : "") : ""?>>
                                                <?= $typologie->typologielot_libelle ?>
                                                </option>
                                            <?php endforeach;?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"> Numéro du lot </label>
                                            <input type="text" class="form-control" id="lot_num" name="lot_num" value ="<?= isset($lot->lot_num) ? $lot->lot_num : "" ;?>" >
                                        <div cclass="help fs-11 height-15 m-0"></div>
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"> Numéro du parking </label>
                                            <input type="text" class="form-control" id="lot_num_parking" name="lot_num_parking" value ="<?= isset($lot->lot_num_parking) ? $lot->lot_num_parking : "" ;?>" >
                                        <div cclass="help fs-11 height-15 m-0"></div>
                                    </div>
                                </div>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="col p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="fs-0">Gestionnaire</h5>
                    <hr class="mt-0">
                    <div class="mt-4">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="250"> Gestionnaire </label>
                                        <select class="form-select fs--1" id="gestionnaire_id" name="gestionnaire_id" >
                                            <?php foreach ($gestionnaire as $gestionnaires ) :?>
                                                <option value="<?= $gestionnaires->gestionnaire_id ?>" 
                                                <?php echo isset($lot->gestionnaire_id) ? (($lot->gestionnaire_id !== NULL) ?(($lot->gestionnaire_id==$gestionnaires->gestionnaire_id) ? "selected" : " ") : "" ) : ""?>>
                                                <?= $gestionnaires->gestionnaire_nom ?>
                                                </option>
                                            <?php endforeach;?>
                                        </select>
                                        <div class="help"></div>&nbsp;
                                        <div class="m-0">
                                            <button id="addGestionnaire" type="button" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i></button>
                                        </div>
                                       
                                    </div>
                                </div>
                            </tr>
                            <tr class="border-bottom">
                                
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        
    </div>

</div>
</div>
<?php echo form_close(); ?>
