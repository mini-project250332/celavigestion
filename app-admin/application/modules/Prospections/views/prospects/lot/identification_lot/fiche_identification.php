<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class="px-2">

		<!-- <button data-prospect="<?= $lot->prospect_id; ?>" id="btnRetourListeLot" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo fa-w-16 me-1"></i>
			<span>Liste lot</span>
		</button> -->

		<button type="button" data-prospect="<?= $lot->prospect_id; ?>" data-id="<?= $lot->lot_id; ?>" id="btn-update-lot"
			class="only_visuel_gestion btn btn-sm btn-primary btn-update-client">
			<span class="fas fa-edit fas me-1"></span>
			<span> Modifier </span>
		</button>

	</div>
</div>

<div class="contenair-content">
	<div class="col-12 p-0 py-1">

		<div class="row m-0">

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Identification</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Programme : </th>
									<th class="pe-0 text-end">
										<?php foreach ($program as $programme): ?>
											<?php echo isset($lot->progm_id) ? (($lot->progm_id !== NULL) ? (($lot->progm_id == $programme->progm_id) ? $programme->progm_nom : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 1 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_adresse1 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 2 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_adresse2 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 3 : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_adresse3 ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Code postal : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_cp ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Ville : </th>
									<th class="pe-0 text-end">
										<?= $lot->progm_ville ?>
									</th>
								</tr>
								<tr>
									<th class="ps-0 pb-0">Pays : </th>
									<th class="pe-0 text-end pb-0">
										<?= $lot->progm_pays ?>
									</th>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col p-1">

				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Informations du lot</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<?php
									$type = $lot->lot_principale;
									$value;
									if ($type == 1) {
										$value = "Oui";
									} else {
										$value = "Non";
									}
									?>
									<th class="ps-0">Lot principal : </th>
									<th class="pe-0 text-end"><?= $value ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Type de lot : </th>
									<th class="pe-0 text-end">
										<?php foreach ($typelot as $typelots): ?>
											<?php echo isset($lot->typelot_id) ? (($lot->typelot_id !== NULL) ? (($lot->typelot_id == $typelots->typelot_id) ? $typelots->typelot_libelle : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Typologie : </th>
									<th class="pe-0 text-end">
										<?php foreach ($typologie as $typologie): ?>
											<?php echo isset($lot->typologielot_id) ? (($lot->typologielot_id !== NULL) ? (($lot->typologielot_id == $typologie->typologielot_id) ? $typologie->typologielot_libelle : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Numéro du lot : </th>
									<th class="pe-0 text-end ">
										<?= $lot->lot_num ?>
									</th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Numéro de parking : </th>
									<th class="pe-0 text-end">
										<?= $lot->lot_num_parking ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Gestionnaire</h5>

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Gestionnaire : </th>
									<th class="pe-0 text-end">
										<?php foreach ($gestionnaire as $gestionnaires): ?>
											<?php echo isset($lot->gestionnaire_id) ? (($lot->gestionnaire_id !== NULL) ? (($lot->gestionnaire_id == $gestionnaires->gestionnaire_id) ? $gestionnaires->gestionnaire_nom : " ") : "") : "" ?>
										<?php endforeach; ?>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>