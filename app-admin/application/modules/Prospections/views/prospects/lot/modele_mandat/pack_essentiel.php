<link rel="stylesheet" href="../assets/css/prospections/modelMandat.css">
<link rel="stylesheet" href="../assets/css/prospections/bootstrap.min.css">
<body style='word-wrap:break-word'>

<div class="WordSection1">
    <div class="logoclv" style='text-align:center'>
        <img src="<?php echo base_url('../assets/img/image001.jpg');?>" alt="logo">
    </div>
    
    <div class="titre">
        <img width="654" height="194" src="<?php echo base_url('../assets/img/image002.png');?>">
    </div>
        
    <br clear="ALL">

    <div class="log"> <img width="794" height="382" src="<?php echo base_url('../assets/img/image003.jpg');?>"> </div>

    <br clear="ALL">
    <br>

    <div class="inscriptions">
        <p class="MsoNormale" >
            <b><span style='color:white'>Inscription au registre des mandats : N° <?=str_pad($mandat->mandat_id, 5, '0', STR_PAD_LEFT);?></span></b>
        </p>
        <p class="MsoNormale">
            <b><span style='font-size: 10.0pt;color:white'>Un numéro de mandat par lot/en cas de cession d’un des lots un avenant au mandat sera proposé</span></b>
        </p>
        <p class="MsoNormale" style='text-align:center'>
            <i><span style='color:white'>Prévu par la loi 70-9 du 2 janvier 1970 et par l’article 65 du Décret N° 72.678 du 20 juillet 1972</span></i>
        </p>
    </div>
</div>
<br><br>
<div class="WordSection2">

    <p class="MsoNormale2">
        <b><u><span>ENTRE-LES SOUSSIGNES CI-DESSOUS,</span></u></b>
    </p>
    <p class="MsoNormale2">
        <b><u><span>LE MANDANT,</span></u></b>
    </p>
    <p class="MsoNormale2">
        <b><span>DESIGNATION DU (DES) MANDANT(S)</span></b>
    </p>
</div>

<div class="Section3" >

    <p class="MsoNormal" style='border:none;padding:0cm'>
        <b><span>Nom, prénom</span></b>
        <span > :<?=$mandat->prospect_nom.' '.$mandat->prospect_prenom;?> &nbsp;&nbsp;
        Lieu de naissance :  Lieu de naissance : <?=$mandat->prospect_lieuNais ?> &nbsp;&nbsp;
        Nationalité : <?=$mandat->prospect_nationalite ;?> &nbsp;&nbsp;</span>
    </p>

    <p class="MsoNormal">Tel personnel : <?=$mandat->prospect_phonemobile ;?> </span></p>
    <p class="MsoNormal">Email : <?=$mandat->prospect_email ;?></span></p>
    <p class="MsoNormal">Demeurant :   </span></p>
    <p class="MsoNormal"><span>Célibataire                                                  </span><span>Pacsé                                               </span><span >Marié</span></p>

</div>
<br>
<div class="section4">

    <p class="MsoNormal">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <span> Propriétaires indivis du bien ci-dessous désigné,</span>
        </div>
       
    </p>

    <p class="MsoNormal">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <span  > Propriétaire (%) – Usufruitier (&#8194; %) du bien ci-dessous désigné</span>
        </div>
        
    </p>

    <p class="MsoNormal">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <span  > La SCI &#8194;</span>
        </div>
        
    </p>

    <p class="MsoNormal">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <span  > La Sarl/SNC/EI/EURL &#8194;</span>
        </div>
        
    </p>

</div>
<br>
<div class="section5">
    <p class="MsoNormal"><b><u><span  >LE MANDATAIRE,</span></u></b></p>
    <p class="MsoNormal" style='text-align:justify'><span  >La
    Société « C’EST LA VIE PROPERTIES représentée par son Gérant, Monsieur Bertrand
    Le Mire, désignée comme le mandataire.</span></p>
    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>La Société « C’EST LA VIE PROPERTIES », S.A.R.L. au capital
    de 100 000 Euros dont le siège social est à Saint-Nazaire (44600), 39 route de
    Fondeline, Parc de Brais, immatriculée au Registre du Commerce et des Sociétés
    sous le numéro B 477 782 346 représentée par son Gérant, Monsieur
    Bertrand Le Mire, désignée comme le mandataire dans la suite des présentes,
    titulaire des cartes professionnelles CPI 4402 2018 000 035 231 et disposant
    d’une garantie financière « gestion immobilière » de 620.000 €,
    délivrée par GALIAN, 89 rue de la Boétie, 75008 Paris, N° A12924809, disposant
    également d’une assurance responsabilité civile professionnelle auprès de MMA
    IARD, police N° 120 137 405, 14 boulevard Marie et Alexandre Oyon, 72030 Le
    Mans Cedex 9, désigné comme <b><u>LE MANDATAIRE</u></b> dans la suite des
    présentes.</span></i></p>

    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>La Société « C’est La Vie PROPERTIES » ayant satisfait aux
    obligations de la Loi n°70-9 du 2 janvier 1970 et de son décret d’application
    n°72-678 du 20 juillet 1972, par : </span></i></p>

    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>1 - la possession de la carte professionnelle n°CPI 4402 2018
    000 035 231 délivrée par la CCI de Nantes, portant sur l’activité « gestion
    immobilière ». </span></i></p>

    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>2 - la souscription auprès de la Caisse de Garantie de
    l’Immobilier ‘’GALIAN’’, dont le siège social est à </span></i></p>

    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>PARIS 8° - 89 rue de La Boétie, d’une garantie financière des
    sommes et valeurs reçues au titre des activités de gestion immobilière visées
    par la Loi du 2 janvier 1970 et son décret d’application, </span></i></p>

    <p class="MsoNormal" style='text-align:justify'><i><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>3 - la souscription auprès de la Compagnie MMA IARD, dont le
    siège social est au Mans, 14 boulevard Marie et Alexandre Oyon, d’une police
    d’assurance responsabilité civile professionnelle des agents immobiliers sous
    le N° 120 137 405.</span></i></p>

</div>
<br>
<div class="section6">
    <p class="MsoNormal" style='text-align:justify'><b><u><span>Conditions particulières.</span></u></b></p>
    <p class="MsoNormal"><b><u><span  >DESIGNATION DU (DES) BIEN(S)</span></u></b></p>
    <p class="MsoNormal"><b><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>Dans la résidence dénommée :                                          Dans
    la résidence dénommée :</span></b></p>
    <p class="MsoNormal"><b><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>Située :                                                                                Située :                                                          </span></b></p>


    <table class="table table-bordered" style="width : 50%">
        <tbody>
            <tr>
                <td class="tableautr">Numéro de lot</td>
                <td></td>
            </tr>
            <tr>
                <td> Bâtiment</td>
                <td></td>
            </tr>
            <tr>
                <td>Type</td>
                <td></td>
            </tr>
            <tr>
                <td>Etage</td>
                <td></td>
            </tr>
            <tr>
                <td>Surface M2</td>
                <td></td>
            </tr>
            <tr>
                <td>Terrasse M2</td>
                <td></td>
            </tr>
            <tr>
                <td> Balcon M2</td>
                <td></td>
            </tr>
            <tr>
                <td>Parking lot</td>
                <td></td>
            </tr>
            <tr>
                <td>Numéro de lot</td>
                <td></td>
            </tr>
            <tr>
                <td>Bâtiment</td>
                <td></td>
            </tr>
            <tr>
                <td>Type</td>
                <td></td>
            </tr>
            <tr>
                <td>Etage</td>
                <td></td>
            </tr>
            <tr>
                <td>Surface M2</td>
                <td></td>
            </tr>
            <tr>
                <td>Terrasse M2</td>
                <td></td>
            </tr>
            <tr>
                <td>Balcon M2</td>
                <td></td>
            </tr>
            <tr>
                <td>Parking lot</td>
                <td></td>
            </tr>
        </tbody>
                  
    </table>

</div>

<div class="section7">

    <p class="MsoNormal" style='margin-left:70.8pt;text-indent:35.4pt'><b><u><span
     style='font-size:14.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>IL EST PREALABLEMENT EXPOSE</span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Les
    parties sont convenues que dans le cadre du bail signé avec l’exploitant pour
    les biens ci-dessus, de désigner le mandataire comme seul représentant du
    mandant vis à vis de l’exploitant-locataire afin de décharger le mandant de
    l’ensemble des tâches administratives décrites au paragraphe des prestations courantes.</span></p>

    <p class="MsoNormal"><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>Le présent mandat prendra effet à la date de
    signature des présentes.</span></p>

    <p class="MsoNormal"><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>Le Mandant déclare avoir opté, pour les biens
    objet des présentes, pour un régime fiscal spécifique notamment :</span></p>

    <p style='margin-left:54.0pt;text-indent:-18.0pt'>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="">
            <span>Scellier</span>
        </div>
    </p>
    <p style='margin-left:54.0pt;text-indent:-18.0pt'>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="">
            <span>Censi-Bouvard</span>
        </div>
    </p>
    <p style='margin-left:54.0pt;text-indent:-18.0pt'>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="">
            <span>Aucun dispositif</span>
        </div>
    </p>

    <p class="MsoNormal"><b><i><span  style='font-family:"Segoe UI",sans-serif;
    color:black;background:white'>En conséquence, le Mandant s’oblige à fournir au
    Mandataire tous les documents en sa possession relatifs à cette option fiscale.</span></i></b></p>

</div>

<div class="formule">
    <p class="MsoNormal"><span style='position:relative;z-index:251663360'><span
    style='position:absolute;left:99px;top:-2px;width:466px;height:74px'><img
    width=466 height=74 src="<?php echo base_url('../assets/img/image004.png');?>"
    alt="Formule choisie : Pack ESSENTIEL">
</div>
<br clear="ALL">

<div class="article1">

    <p class="MsoNormal"><b><u><span  >ARTICLE 1 : MISSIONS
    ET POUVOIRS DU MANDATAIRE</span></u></b></p>

    <p class="MsoNormal"><span  style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'> </span></p>

    <p class="MsoNormal"><b><u><span  style='font-size:11.0pt;font-family:
    "Segoe UI",sans-serif;color:black;background:white'>Missions et pouvoirs du
    Mandataire en matière de gestion courante :</span></u></b></p>

    <table class="table table-bordered" style="border: solid #31849B;">
        <thead class="text-center" style="border: solid #31849B; background: #31849B; color:white;" >
            <tr>
                <td><b>SUIVI DE MISSION EXPERTISE COMPTABLE</b></td>
                <td><b>MISSION</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="tableautd">- Contrôle de l’exhaustivité des documents</td>
                <td>
                    <i class="fa-solid fa-check"></i>
                </td>
            </tr>
            <tr>
                <td>- Transmission de l’ensemble des documents à l’expert-comptable</td>
                <td>
                    <p class="MsoNormal" align=center style='text-align:center'><b><span style='font-size:20.0pt;font-family:Symbol;color:black;background:white'>Ö</span></b></p>
                </td>
            </tr>
            <tr>
                <td>- Procéder à deux relances de documents par an pour la réalisation du bilan annuel. </td>
                <td>
                    <p class="MsoNormal" align=center style='text-align:center'><b><span style='font-size:20.0pt;font-family:Symbol;color:black;background:white'>Ö</span></b></p>
                </td>
            </tr>
            <tr class="text-center" style="border: solid #31849B; background: #31849B; color:white;" >
                <td><b>GESTION LOCATIVE ET ADMINISTRATIVE</b></td>
                <td><b>MISSION</b></td>
            </tr>
            <tr>
                <td>- Être l’interlocuteur du mandant vis à vis du syndic, du locataire, de l’administration fiscale : <br>
                    Création d’activité auprès du greffe <br>
                    Création de l’espace pro auprès des impôts <br>
                    Demande de devis PNO
                </td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr>
                <td>- Emettre et faire parvenir à l’exploitant les factures de loyer et autres refacturations (charges récupérables).</td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr>
                <td>- Procéder à la révision des loyers. </td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr>
                <td>- Facturer au locataire la taxe sur les ordures ménagères selon le bail*. <br>
                    <span style="font-size:9.0pt;">*Si le rôle de taxe foncière a été transmis au mandataire</span> 
                </td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr class="text-center" style="border: solid #31849B; background: #31849B; color:white;">
                <td><b>GESTION DES CONTENTIEUX</b></td>
                <td><b>MISSION</b></td>
            </tr>
            <tr>
                <td>-Procéder aux relances auprès des locataires en cas d’impayés ou retard de loyers ou de toutes charges dues par les locataires dans le cadre des obligations
                découlant de leurs baux*
                </td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr>
                <td>- Suivi du dossier</td>
                <td class="text-center"><b>Sur devis</b></td>
            </tr>
            <tr class="text-center" style="border: solid #31849B; background: #31849B; color:white;">
                <td><b>MISSIONS PARTICULIERES</b></td>
                <td><b>MISSION</b></td>
            </tr>
            <tr>
                <td>Déclaration ou modification d’activité auprès du Greffe </td>
                <td class="text-center"><b>89€ HT</b></td>
            </tr>
        </tbody>
    </table>
    <p class="MsoNormal"><i><span  style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>* coût de la LRAR non inclus 15€ TTC </span></i></p>
    <p class="MsoNormal"><i><span  style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>** frais d’huissier non compris, </span></i></p>
    <p class="MsoNormal"><i><span  style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>*** frais d’avocat non compris, sur devis</span></i></p>

</div>

<br>
<div class="article2">
    <p class="MsoNormal"><b><u><span  >ARTICLE 2 : RÉMUNÉRATION </span></u></b></p>

    <table class="table table-bordered" style="border: solid #31849B;">
        <thead class="text-center" style="border: solid #31849B; background: #31849B; color:white;" >
            <tr>
                <td><b>HONORAIRES</b></td>
                <td><b>Tarifs</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="tableautd">
                    <p class="MsoNormal"><b><u><span style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:#31849B'>Mission sans encaissement – Pack ESSENTIEL</span></u></b></p>
                    <p class="MsoNormal"><span  style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black'>Ce service vous permet de bénéficier du kit fiscal par le biais,de notre lettre de mission comptable.</span>
                    </p>
                <td class="text-center"> <b>39€ HT / an pour un lot</b></td>
            </tr>
            <tr style="border: solid #31849B; background: #31849B;">
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>

    <p class="MsoNormal" style='text-align:justify'><b><u><span>Révision des honoraires minimums :</span></u></b><u><span></span></u></p>
    <p class="MsoNormal" style='text-align:justify'><span  >Les
    honoraires fixes feront l’objet d’une révision tous les 3 ans, à la date de
    signature du présent mandat, à hauteur de 2%. </span></p>
    <p class="MsoNormal" style='text-align:justify'><b><span 
    >Conditions générales </span></b></p>
    
</div>

<div class="article3">
    <p class="MsoNormal"><b><u><span  >ARTICLE 3 : DURÉE </span></u></b></p>

    <p class="MsoNormal"><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>La date d’effet du mandat court à compter de la
    signature des présentes et ce jusqu’au 31 décembre de l’année en cours.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Ce
    contrat se renouvellera ensuite tacitement d’année en année.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >L’une ou
    l’autre des parties pourra résilier le présent mandat au terme de chaque année
    à condition d’en informer l’autre partie, par lettre recommandée avec accusé de
    réception, six mois avant la date anniversaire que représente la date
    d’échéance du contrat initial, à savoir le 1<sup>er</sup> janvier de chaque
    année. Le délai de préavis commencera à courir à compter du lendemain du jour
    de la première présentation de la lettre recommandée.  </span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Le
    Mandataire aura la possibilité de résilier le présent mandat, à tout moment,
    par lettre recommandée avec accusé de réception dans les cas suivants : </span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >- le
    Mandant refuserait ou ne donnerait pas suite à une demande formulée par le
    Mandataire. </span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >- cas de
    force majeure empêchant le Mandataire d’accomplir tout ou partie de ses
    obligations. </span></p>
    
    <p class="MsoNormal" style='text-align:justify'><span  >-à
    défaut du paiement de ses honoraires dans le délai d’un mois à réception d’une
    mise en demeure par lettre recommandée avec accusé de réception.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Cette
    résiliation prendra effet le dernier jour du mois au cours duquel la lettre
    recommandée aura été présentée. </span></p>

    <p class="MsoNormal"><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>Dans tous les cas, les comptes seront soldés selon
    la périodicité initialement prévue aux conditions particulières. </span></p>

</div>

<div class="article4">
    <p class="MsoNormal"><b><u><span  >ARTICLE 4 : PROCURATION
    DONNÉE </span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><b><span 
    style='font-family:"Segoe UI",sans-serif;color:#215868;background:white'>Le
    mandant donne procuration au mandataire aux fins de signer pour son compte les
    documents d’immatriculation (</span></b><b><span  style='font-size:9.0pt;
    font-family:"Segoe UI",sans-serif;color:#215868;background:white'>POi</span></b><b><span
     style='font-family:"Segoe UI",sans-serif;color:#215868;background:white'>)
    et tout document nécessaire au greffe et au Service des Impôts des Entreprises
    dans le cadre de l’exploitation du bien référencé.</span></b></p>

    <p class="MsoNormal"><b><u><span  >ARTICLE 5 : SUBSTITUTION -
    CESSION - FUSION </span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><span  >En cas
    de cession de son fonds de commerce par le Mandataire, en cas de fusion ou si
    celui-ci confie l’exploitation dudit fonds à un locataire gérant, le présent
    mandat se poursuivra au profit du cessionnaire ou du locataire gérant, ce que
    le mandant accepte expressément sous réserve que le successeur du mandataire
    remplisse les conditions issues de la loi du 02 janvier 1970.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Le
    mandant devra être avisé dans les meilleurs délais, et au plus tard dans les
    six mois de la substitution de la cession ou de la location gérance du fonds de
    commerce.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Le
    mandant aura la faculté de résilier le présent mandat dans le mois qui suivra
    la réception de la lettre l’avisant de l’événement. S’il use de cette faculté,
    le mandant devra faire connaitre sa décision au nouveau mandataire ou au
    mandataire substitué par lettre recommandé avec accusé de réception. </span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Il
    s’agit d’un mandat intuitu personae qui ne pourra aucunement être transmis en cas
    de décès du mandant. </span></p>
   
    <p class="MsoNormal"><b><u><span  >ARTICLE 6 : ATTRIBUTION DE
    COMPETENCE </span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><span  >En cas
    de contestation sur l’exécution du présent mandat si les parties ont la qualité
    de commerçant, le tribunal du domicile du Mandataire sera seul compétent. Le
    présent mandat est régi par les dispositions du décret N°72-768 du 20 juillet
    1972, modifié par le décret N°2005-1315 du 21 octobre 2005, pris en application
    de la loi 70-9.</span></p>
   
    <p class="MsoNormal" style='text-align:justify'><b><u><span 
    style='font-size:14.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>ARTICLE 7 : INFORMATIQUE ET LIBERTÉS </span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Les
    informations recueillies font l’objet d’un traitement automatisé ou informatisé
    destiné à la mise en œuvre du présent contrat. Conformément à la loi du 6
    Janvier 1978, le Mandant dispose d’un droit d’accès et de rectification à
    formuler auprès du Mandataire : les modalités de mise en œuvre seront fixées
    d’un commun accord. </span></p>

</div>

<div class="article8">

    <p class="MsoNormal"><b><u><span  >ARTICLE 8 : ELECTION DE
    DOMICILE </span></u></b></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Pour
    l’exécution des présentes et leurs suites, les parties font élection de
    domicile à leurs adresses respectives telles qu’indiquées en tête des
    présentes. </span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >En cas
    de modification, chacune des parties devra en informer l’autre par lettre
    recommandée avec demande accusée de réception dans les 15 jours. A défaut,
    toute notification faite à l’adresse indiquée en tête des présentes, sera
    réputée valablement faite.</span></p>

    <p class="MsoNormal" style='text-align:justify'><span  >Les
    conditions particulières annexées font partie intégrante des conditions
    générales du mandat d’administration de biens, de sorte que l’ensemble forme un
    tout indissociable et indivisible.</span></p>

</div>
<br>
<div class="signature">

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>Fait en 2 exemplaires, dont un sera remis au Mandant après
    enregistrement. </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'> </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:9.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>A  <?=$mandat->mandat_lieu_creation ;?>,    le  <?=date("d/m/Y",strtotime(str_replace('-', '/',$mandat->mandat_date_creation))) ;?></span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    > </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    > </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>Le Mandant      </span><span  style='font-size:10.0pt;
    font-family:"Arial",sans-serif'>[Signature1/]   </span><span 
    style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'>                                                             
    Le Mandataire </span><span  style='font-size:10.0pt;font-family:"Arial",sans-serif'>[Signature2/] 
     </span><span  style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>   </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'> </span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;color:black;
    background:white'> <b><i>« lu et approuvé, bon pour mandat »                                    «
    lu et approuvé, mandat accepté »</i></b></span></p>

    <p class="MsoNormal" style='border:none;padding:0cm'><span 
    > </span></p>

</div>
<br>

<div class="infos">
    <p class="MsoNormal" align=center style='text-align:center;border:none;
    padding:0cm'><b><span  style='font-size:8.0pt;font-family:"Segoe UI",sans-serif'>C’est
    La Vie ! Properties, </span></b><span  style='font-size:8.0pt;
    font-family:"Segoe UI",sans-serif'>Sarl au capital de 100000 €, Siret :
    477 782 346 00035 RCS Saint-Nazaire, Ape 6831Z, Numéro TVA
    intracommunautaire FR69477782346. Adresse : 39 route de Fondeline, parc de
    Brais, 44600 Saint-Nazaire, France. <b>Tel </b>: 0033 (0) 2 40157485, </span><span
    ><a href="mailto:info@clvproperties.com"><span style='font-size:8.0pt;
    font-family:"Segoe UI",sans-serif;color:gray'>info@clvproperties.com</span></a></span><span
     style='font-size:8.0pt;font-family:"Segoe UI",sans-serif'>  Carte
    professionnelle transaction et gestion N° CPI 4402 2018 000 035 231 délivrée
    par la CCI de Nantes, garantie transaction à hauteur de 120.000€ et garantie
    gestion à hauteur de 600.000€ souscrites auprès de GALIAN, 89 rue de la Boétie,
    75008 Paris</span></p>
    <p class="MsoNormal" align=center style='text-align:center'><b><u><span 
    style='font-size:9.0pt;font-family:"Segoe UI",sans-serif'>www.cestlavieproperties.com</span></u></b></p>

    <p class="MsoNormal" align=center style='text-align:center'><span 
    style='font-size:8.0pt;font-family:"Segoe UI",sans-serif'>2021</span></p>

    <p class="MsoNormal" align=center style='text-align:center'><span 
    style='font-family:"Segoe UI",sans-serif;color:black'><img border=0 width=169
    height=206 id="Image 2"
    src="<?php echo base_url('../assets/img//image005.jpg');?>" alt="logo"></span></p>

    <p class="MsoNormal" align=center style='text-align:center;text-autospace:none'><b><u><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>MANDAT
    DE PRELEVEMENT SEPA</span></u></b></p>
</div>

<div class="sepa">

    <p class="MsoNormal" style='text-align:justify;text-autospace:none;border:none;
    padding:0cm'><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>En signant ce formulaire de mandat, vous autorisez (A) C’EST LA
    VIE PROPERTIES à envoyer des instructions à votre banque pour débiter votre
    compte, et (B) votre banque à débiter votre compte conformément aux
    instructions de C’EST LA VIE PROPERTIES.</span></p>

    <p class="MsoNormal" style='text-align:justify;text-autospace:none;border:none;
    padding:0cm'><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>Vous bénéficiez du droit d'être remboursé par votre banque suivant
    les conditions décrites dans la convention que vous avez passée avec elle. Une
    demande de remboursement doit être présentée dans les 8 semaines suivant la
    date de débit de votre compte pour un prélèvement autorisé.</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Référence
    unique du mandat :_______________   Identifiant créancier SEPA :</span><span
     style='font-size:14.0pt;font-family:"Segoe UI",sans-serif;color:black'>
    </span><span  style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;
    color:black'>ICS: <b>FR45ZZZ532215</b></span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Débiteur
    :                                                                           Créancier
    : C’est La Vie Properties</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Votre
    Nom :                                                                       39
    route de Fondeline</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>                                                                                    
          44600 SAINT NAZAIRE</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Votre
    Adresse :                                                                  FRANCE</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Code
    postal :</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Ville :</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Pays :
    </span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>IBAN :</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>BIC :          
     </span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Paiement
    : Récurrent/Répétitif </span><span  style='font-family:"Segoe UI",sans-serif;
    color:black'>&#61694;</span><span  style='font-family:"Segoe UI",sans-serif;
    color:black'>                              </span><span 
    style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Ponctuel</span><span
     style='font-family:"Segoe UI Symbol",sans-serif;color:black'>&#9744;</span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>A
    : <?=$mandat->mandat_lieu_creation ;?>, Le : <?=date("d/m/Y",strtotime(str_replace('-', '/',$mandat->mandat_date_creation))) ;?></span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Signature
    : </span><span  style='font-size:10.0pt;font-family:"Arial",sans-serif'>[Signature1/] 
     </span><span  style='font-size:10.0pt;font-family:"Segoe UI",sans-serif;
    color:black;background:white'>       </span></p>

    <p class="MsoNormal" style='text-autospace:none;border:none;padding:0cm'><span
     style='font-size:11.0pt;font-family:"Segoe UI",sans-serif;color:black'>Nota
    : Vos droits concernant le présent mandat sont expliqués dans un document que
    vous pouvez obtenir auprès de votre banque.</span></p>

</div>
</body>

