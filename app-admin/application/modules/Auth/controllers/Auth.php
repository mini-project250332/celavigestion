<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Auth extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Auth";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/historique/historique.js",
            'js/auth/auth.js'
        );
        $this->_css_personnaliser = array(
            "css/auth/auth.css"
        );
        $this->load->model('Historiqueacces_m', 'hacces');
    }

    public function index()
    {
        redirect("Auth/login/" . (isset($this->session->userdata['token']) ? $this->session->userdata['token'] : $this->genererToken()));
    }

    public function login($token_ = null)
    {
        $token = is_array($token_) ? $token_[0] : $token_;
        if ($token != null) {
            $data['token'] = $token;
            if (isset($this->session->userdata['token'])) {
                $verify = $this->checkSession($token);
                if ($verify['status'] == true) {
                    if ($verify['db_status'] == 2) {
                        redirect("Prospections/Prospections");
                    } else {
                        $this->login_page('login');
                    }
                } else {
                    $this->session->sess_destroy();
                    redirect("Auth/login/" . $this->genererToken());
                }
            } else {
                $data = array(
                    'hacces_token' => $token,
                    'hacces_datecreate' => Carbon::now()->toDateTimeString(),
                    'hacces_ip' => $_SERVER['REMOTE_ADDR'],
                    'hacces_agent' => $this->agent->browser(),
                    'hacces_action' => 1,
                    // 1 demande // 2 login (status = 1 ok | 0 non ok)  // 3 logout (status = 1 ok | 0 non ok), // 4 expirer
                    'hacces_status' => 1
                );
                $request = $this->hacces->save_data($data);
                if (!empty($request)) {
                    $this->session->set_userdata('token', $token);
                    $this->session->set_userdata('create', $data['hacces_datecreate']);
                    $this->login_page('login');
                }
            }
        } else {
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function checkSession($token = null)
    {
        $params = array(
            'clause' => array(
                'hacces_token' => is_array($token) ? $token[0] : $token,
                'hacces_status ' => 1,
            ),
            'clause_in' => array('hacces_action' => [1, 2]),
            'method' => "row",
        );
        $retour = array("status" => false, "db_status" => 0);
        $request = $this->hacces->get($params);
        if (!empty($request)) {
            $date_creation = Carbon::createFromFormat('Y-m-d H:i:s', $request->hacces_datecreate);
            $now = Carbon::now();
            if ($now->gt($date_creation) && $date_creation->diffInHours($now) < 6) {
                $retour = array("status" => true, "db_status" => intval($request->hacces_action));
            } else {
                $data = array('hacces_action' => 4);
                $clause = array('hacces_id' => $request->hacces_id);
                $this->hacces->save_data($data, $clause);
            }
        }
        return $retour;
    }

    private function genererToken()
    {
        $token = openssl_random_pseudo_bytes(32);
        $token = bin2hex($token);
        return $token;
    }

    public function authUser()
    {
        if (is_ajax()) {
            if ($_POST) {
                $message = array();
                $_POST['data']['util_password']['value'] = trim(decrypt_service($_POST['data']['util_password']['value']));
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $clause = format_data($_POST['data']);
                    $this->load->model('Utilisateur_m', 'users');
                    $verify_ident = array(
                        'distinct' => FALSE,
                        'clause' => $clause,
                        'or_clause' => NULL,
                        'like' => NULL,
                        'or_like' => NULL,
                        'method' => "row",
                        'columns' => '*',
                        'join' => array(
                            "c_typeutilisateur" => "c_typeutilisateur.tutil_id = c_utilisateur.tutil_id",
                            "c_role_utilisateur" => "c_role_utilisateur.rol_util_id  = c_utilisateur.rol_util_id"
                        )
                    );
                    $request = $this->users->get($verify_ident);
                    $data_historique = array(
                        'hacces_action' => 2,
                        'hacces_status' => 0,
                        'hacces_datelogin' => Carbon::now()->toDateTimeString()
                    );

                    $clause_historique = array('hacces_token' => $this->session->userdata['token']);
                    if (!empty($request)) {
                        if ($request->util_status == 0) {
                            $retour = retour(true, "error", 3, array("message" => 'Ce compte a été desactivé ou n\'est pas encore activé. Veuillez informez le responsable du système.'));
                        } else {
                            $data['session_utilisateur'] = array(
                                'util_id' => $request->util_id,
                                'util_nom' => $request->util_nom,
                                'util_prenom' => $request->util_prenom,
                                'util_mail' => $request->util_mail,
                                'util_phonefixe' => $request->util_phonefixe,
                                'util_phonemobile' => $request->util_phonemobile,
                                'util_photo' => $request->util_photo,
                                'rol_util_valeur' => $request->rol_util_valeur,
                                'rol_util_libelle' => $request->rol_util_libelle
                            );
                            $data['session_compte'] = array(
                                'is_logged' => 1,
                                'profil_compte' => $request->tutil_id,
                            );
                            $data['login'] = $data_historique['hacces_datelogin'];
                            $this->session->set_userdata($data);
                            $data_historique['hacces_status'] = 1;
                            $data_historique['util_id'] = $request->util_id;
                            $retour = retour(true, "success", 1, array("message" => 'Connexion avec succès, veuillez patienter...', "userdata" => $data['session_utilisateur']));
                        }
                    } else {
                        $retour = retour(false, "error", 2, array("message" => "Identifiant ou mot de passe incorrect"));
                    }
                    $this->hacces->save_data($data_historique, $clause_historique);
                } else {
                    $retour = retour(null, null, null, $control['information']);
                }
                echo json_encode($retour);
            }
        }
    }

    public function bienvenu()
    {
        redirect("Auth/home_page/" . $this->session->userdata['token']);
    }

    public function suivi_tache()
    {
        redirect("Auth/tache_page/" . $this->session->userdata['token']);
    }

    public function home_page($token_ = null)
    {
        $token = is_array($token_) ? $token_[0] : $token_;
        if ($token != null) {
            if (isset($this->session->userdata['token']) && $this->session->userdata['token'] == $token) {
                $this->login_page('bienvenu');
            } else {
                redirect("Auth/login/" . $this->genererToken());
            }
        } else {
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function tache_page($token_ = null)
    {
        $token = is_array($token_) ? $token_[0] : $token_;
        if ($token != null) {
            if (isset($this->session->userdata['token']) && $this->session->userdata['token'] == $token) {
                $this->login_page('suivi_tache');

            } else {
                redirect("Auth/login/" . $this->genererToken());
            }
        } else {
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function getMessageContenu()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Accueil_m', 'accueil');
                $params = array(
                    'method' => 'row'
                );
                $message_accueil = $this->accueil->get($params);
                echo json_encode($message_accueil);
            }
        }
    }

    
    public function getTacheContenu()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Tache_m');
                $params = array(
                    'columns' => array('tache_id as tache_id', 'tache_nom as tache_nom','tache_date_echeance as tache_date_echeance','tache_createur as tache_createur','client_id','prospect_id',
                                        'participant as participants','tache_date_creation as tache_date_creation','c_type_tache.type_tache_id','tache_prioritaire as tache_prioritaire','tache_date_validationn as tache_date_validationn',
                                        'c_theme.theme_id','theme_libelle'),
                    'clause' => array(
                        'tache_etat' => 1,
                        'tache_valide' => 0,
                        'tache_createur' => $_SESSION['session_utilisateur']['util_prenom']
                    ),
                    'order_by_columns' => "tache_date_echeance ASC" ,
                    'join' => array(
                        'c_type_tache' => 'c_type_tache.type_tache_id = c_tache.type_tache_id',
                        'c_theme' => 'c_theme.theme_id = c_tache.theme_id',
                    ),
                );
                $data['taches'] = $taches =  $this->Tache_m->get($params);
                $this->renderComponant("Auth/acceuil_liste_tache",$data);
            }
        }
    }

    public function logout()
    {
        $data = array(
            'hacces_action' => 3,
            'hacces_datelogout' => Carbon::now()->toDateTimeString()
        );
        $clause = array('hacces_token' => $this->session->userdata['token']);
        $request = $this->hacces->save_data($data, $clause);
        if (!empty($request)) {
            $this->session->sess_destroy();
            redirect("Auth/login/" . $this->genererToken());
        }
    }
}
