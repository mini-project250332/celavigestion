<table class="table table-sm fs--1 mb-0">
    <?php if (empty($taches)) : ?>
        <div class="row">
            <div class="col-12 pt-2">
                <div class="text-center">
                    Aucune tâche non réalisée trouvée
                </div>
            </div>
        </div>
    <?php else : ?>
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Nom de la tâche</th>
                <th scope="col">Priorité</th>
                <th scope="col">Echéance</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($taches as $tache) : ?>
                <?php
                $type = $tache->tache_prioritaire;
                $value;
                if ($type == 0) {
                    $value = "Sans priorité";
                } else if ($type == 1) {
                    $value = "Urgent";
                } else {
                    $value = "Prioritaire";
                }
                ?>
                <tr>
                    <td><?= $tache->tache_nom; ?></td>
                    <td><?= $value; ?></td>
                    <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                    <td>Non realisée</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    <?php endif ?>
</table>