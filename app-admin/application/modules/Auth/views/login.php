<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta name="description" content="<?php echo description; ?>">
    <meta name="author" content="<?php echo author; ?>">
    <title>
        <?php echo title; ?>
    </title>
    <!-- Meta -->

    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />

    <?php if (isset($css_libraries)) {
        foreach ($css_libraries as $css_lib) {
            echo '<link type="text/css" href="' . base_url('../assets/' . $css_lib) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style par default com/_css -->
    <?php if (isset($css_style)) {
        foreach ($css_style as $css_s) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_s) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style personnaliser -->
    <?php if (isset($css_personnaliser)) {
        foreach ($css_personnaliser as $css_p) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_p) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Javascript constant application -->
    <script type="text/javascript">
        const const_app = {
            <?php
            if (isset($const_js)) {
                foreach ($const_js as $js_n => $val_n) {
                    echo $js_n . " : '" . $val_n . "',\n";
                }
            }
            ?>
        }
    </script>

    <!-- Lirairie JS par default "require" -->
    <?php
    if (isset($js_libraries)) {
        foreach ($js_libraries as $js_lib) {
            echo '<script src="' . base_url('../assets/' . $js_lib) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <?php
    if (isset($js_requerie)) {
        foreach ($js_requerie as $js_r) {
            echo '<script src="' . base_url('assets/' . $js_r) . '" type="text/javascript"></script>';
        }
    }
    ?>
    <style type="text/css">
        .pace .pace-progress {
            display: none !important;
        }

        .pace .pace-progress-inner {
            display: none !important;
        }

        #div-message {
            width: 350px !important;
        }
    </style>
</head>

<body>
    <div class="d-none">
        <input type="hidden" value="<?php echo base_url(); ?>" name="base_url" id="base_url" />
        <input type="hidden" value="<?php echo URL_FRONT; ?>" name="front_url" id="front_url" />
    </div>

    <div class="div-formLogin">

        <div class="login-form-box mb-3">
            <div id="div-signin-box" class="signin-box-div">
                <div class="c_logo">
                    <div class="text_logo">
                        <img src="<?php echo base_url('../assets/img/celavi.jpg'); ?>">
                    </div>
                </div>
                <hr>
                <div class="row m-0">

                    <?php echo form_tag('Auth/authUser', array('method' => "post", 'class' => 'col p-0', 'id' => 'authApp')); ?>

                    <div class="form-group">
                        <div>
                            <label> Identifiant </label>
                            <input type="text" class="form-control" id="util_login" name="util_login" placeholder="Entrer votre identifiant" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <label> Mot de passe </label>
                            <input type="password" class="form-control" id="util_password" name="util_password" placeholder="Entrer votre mot de passe" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block">
                        <span>Suivant</span>
                        <i class="fas fa-sign-in-alt"></i>
                    </button>

                    <?php echo form_close(); ?>
                </div>


                <div class="col row m-0 mt-2 p-0">
                    <div class="col p-0">
                        <div class="form-group m-t-15 m-b-0 fs-12 text-left">
                            <a class="fs--1 text-primary" href="javascript:void(0);">
                                Aide
                                <span class="icon ion-information-circled"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col p-0">
                        <div class="form-group m-t-15 m-b-0 fs-12 text-right">
                            <a class="fs--1 text-primary" href="javascript:void(0);">
                                Mot de passe oublié
                                <span class="icon ion-help-circled"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="div-message"></div>

    </div>
    <?php
    if (isset($js_script)) {
        foreach ($js_script as $js_s) {
            echo '<script src="' . base_url('/assets/' . $js_s) . '" type="text/javascript"></script>';
        }
    }
    ?>
</body>

</html>