<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta name="description" content="<?php echo description; ?>">
    <meta name="author" content="<?php echo author; ?>">
    <title>
        <?php echo title; ?>
    </title>
    <!-- Meta -->

    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />

    <?php if (isset($css_libraries)) {
        foreach ($css_libraries as $css_lib) {
            echo '<link type="text/css" href="' . base_url('../assets/' . $css_lib) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style par default com/_css -->
    <?php if (isset($css_style)) {
        foreach ($css_style as $css_s) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_s) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style personnaliser -->
    <?php if (isset($css_personnaliser)) {
        foreach ($css_personnaliser as $css_p) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_p) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Javascript constant application -->
    <script type="text/javascript">
        const const_app = {
            <?php
            if (isset($const_js)) {
                foreach ($const_js as $js_n => $val_n) {
                    echo $js_n . " : '" . $val_n . "',\n";
                }
            }
            ?>
        }
    </script>

    <!-- Lirairie JS par default "require" -->
    <?php
    if (isset($js_libraries)) {
        foreach ($js_libraries as $js_lib) {
            echo '<script src="' . base_url('../assets/' . $js_lib) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <?php
    if (isset($js_requerie)) {
        foreach ($js_requerie as $js_r) {
            echo '<script src="' . base_url('assets/' . $js_r) . '" type="text/javascript"></script>';
        }
    }
    ?>
    <style type="text/css">
        .pace .pace-progress {
            display: none !important;
        }

        .pace .pace-progress-inner {
            display: none !important;
        }

        #div-message {
            width: 350px !important;
        }
    </style>
</head>

<body>
    <div class="d-none">
        <input type="hidden" value="<?php echo base_url(); ?>" name="base_url" id="base_url" />
        <input type="hidden" value="<?php echo URL_FRONT; ?>" name="front_url" id="front_url" />
        <input type="hidden" value="<?php echo $_SESSION['session_utilisateur']['rol_util_libelle'] ?>" name="rol_util_libelle" id="rol_util_libelle">
    </div>

    <div class="div-div_home_page">
        <div class="login-form-div_home_page mb-3">
            <div class="signin-box-div_home_page">
                <div class="c_logo">
                    <div class="text_logo">
                        <img src="<?php echo base_url('../assets/img/celavi.jpg'); ?>">
                    </div>
                </div>
                <div class="row m-0">
                    <div class="contenu_bienvenu mt-5 mb-6">
                        Bonjour <?= $_SESSION['session_utilisateur']['util_prenom'] ?>,<br><br>
                        <div class="message_contenu">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <div class="row">
                                <div class="col"></div>
                                <div class="col text-end"><button class="btn btn-primary" id="btn_bienvenu">
                                        <span>Continuer</span>
                                        <i class="fas fa-sign-in-alt"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($js_script)) {
        foreach ($js_script as $js_s) {
            echo '<script src="' . base_url('/assets/' . $js_s) . '" type="text/javascript"></script>';
        }
    }
    ?>
    <script>
        $(document).ready(function() {
            getMessageContenu();
        });
    </script>
</body>

</html>