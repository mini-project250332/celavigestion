<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facturation_impaye extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function page_facture_impaye()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
                $data['mode_paiement'] = $this->mode_paiement->get(array(
                    'clause' => array('id_mode_paie !=' => 1),
                ));
                $this->renderComponant("Facturation/paiements/facture_impaye/contenu-facturation-impaye", $data);
            }
        }
    }

    public function facture_impaye()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $datapost = $_POST['data'];
                $clause['fact_annee'] = $datapost['annee'];
                $clause['fact_etatpaiement'] = 0;
                $likefacturation = array();

                $pagination_data = explode('-', $datapost['pagination_page']);
                $rows_limit = intval($datapost['row_data']);

                if ($datapost['num_dossier_facture'] != '') {
                    $likefacturation['f.dossier_id'] = $datapost['num_dossier_facture'];
                }

                if ($datapost['regleId'] != 0) {
                    $clause['f.fact_modepaiement'] = intval($datapost['regleId']);
                }

                if ($datapost['num_facture'] != '') {
                    $likefacturation['f.fact_num'] = $datapost['num_facture'];
                }

                if ($datapost['rumId'] != '') {
                    $likefacturation['ms.sepa_numero'] = $datapost['rumId'];
                }


                $like_progm = ($datapost['facture_filtre_progrm'] != '') ? ['progm_nom' => $datapost['facture_filtre_progrm']] : null;
                $like_proprietaire = ($datapost['facture_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['facture_filtre_proprietaire']] : null;

                $facture = $this->facture_annuelle->getFacture($clause, $likefacturation);
                // var_dump($facture);
                // die();
                $lotModel = $this->lot;


                foreach ($facture as $key => $value) {
                    $table_lot = explode(',', $value->cliLot_id);
                    $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                        $lot = $this->lot->get([
                            'clause' => ['cliLot_id' => $value_lot],
                            'like' => $like_progm,
                            'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                            'join_orientation' => 'left',
                            'method' => 'row'
                        ]);

                        return (!empty($lot)) ? $lot->progm_nom : null;
                    }, $table_lot);

                    $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

                    // if ($facture[$key]->progm_nom == '') {
                    //     unset($facture[$key]);
                    //     continue;
                    // }
                    $clientModel = $this->client;
                    $table_client = explode(',', $value->client_id);
                    $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                        $clientData = $this->client->get([
                            'clause' => ['client_id' => $client],
                            'like' => $like_proprietaire,
                            'method' => 'row'
                        ]);

                        return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
                    }, $table_client);

                    $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

                    if ($facture[$key]->client_nom == '') {
                        unset($facture[$key]);
                        continue;
                    }
                }

                $data["countAllResult"] = $this->countAllData('Facture_m', array('fact_annee' =>  $datapost['annee']));


                if (
                    $datapost['annee'] != '' ||
                    $datapost['facture_filtre_proprietaire'] != '' ||
                    $datapost['facture_filtre_progrm'] != '' ||
                    $datapost['num_dossier_facture'] != '' ||
                    $datapost['num_facture'] != '' ||
                    $datapost['regleId'] !=  0 ||
                    $datapost['rumId'] != ''
                ) {
                    $data["countAllResult"] = $this->countUser($clause, $likefacturation, $like_progm, $like_proprietaire, $datapost);
                }

                $nb_total = $data["countAllResult"];
                $data['pagination_page'] = $datapost['pagination_page'];
                $data['rows_limit'] = $rows_limit;
                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];
                $facture = array_slice($facture, $pagination_data[0], $pagination_data[1]);
                $data['facture'] = $facture;

                $this->renderComponant("Facturation/paiements/facture_impaye/table-facture-impaye", $data);
            }
        }
    }

    function countAllData($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }

    function countUser($clause, $likefacturation, $like_progm, $like_proprietaire, $datapost)
    {
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('ClientLot_m', 'lot');
        $this->load->model('Client_m', 'client');

        $facture = $this->facture_annuelle->getFactureCount($clause, $likefacturation);
        $lotModel = $this->lot;

        foreach ($facture as $key => $value) {
            $table_lot = explode(',', $value->cliLot_id);
            $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                $lot = $this->lot->get([
                    'clause' => ['cliLot_id' => $value_lot],
                    'like' => $like_progm,
                    'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                    'method' => 'row'
                ]);

                return (!empty($lot)) ? $lot->progm_nom : null;
            }, $table_lot);

            $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

            if ($facture[$key]->progm_nom == '') {
                unset($facture[$key]);
                continue;
            }
            $clientModel = $this->client;
            $table_client = explode(',', $value->client_id);
            $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                $clientData = $this->client->get([
                    'clause' => ['client_id' => $client],
                    'like' => $like_proprietaire,
                    'method' => 'row'
                ]);

                return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
            }, $table_client);

            $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

            if ($facture[$key]->client_nom == '') {
                unset($facture[$key]);
                continue;
            }
        }

        return !empty($facture) ? count($facture) : 0;
    }

    function pagePrelevement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Banque_m', 'banque');
                $this->load->model('Facture_m', 'facture_annuelle');
                $datapost = $_POST['data'];
                $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
                $data['facture'] = $this->facture_annuelle->getPrelevement($datapost['table_num_facture']);
                $this->renderComponant("Facturation/paiements/facture_impaye/pagePrelevement", $data);
            }
        }
    }


    public function SavePrelevement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Reglement_m', 'reglement');
                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $this->load->model('Prelevement_m', 'prelevement');
                $this->load->model('Facture_m', 'facture_annuelle');
                $datapost = $_POST['data'];
                $dateAction = $datapost['dateprelevement'];
                $dataXml = array();
                $dataPrelevement = [
                    'date_encaissement' => $dateAction,
                    'id_banque' => $datapost['banqueId'],
                    'date_action' => date('Y-m-d H:i:s')
                ];

                $idPrelevement =  $this->prelevement->save_data($dataPrelevement);
                $jsonData = $datapost['factures'];
                $factures = json_decode($jsonData, true);


                if ($factures !== null) {
                    foreach ($factures as $facture) {
                        $factId = $facture['fact_id'];
                        $dossierId = intval($facture['dossier_id']);
                        $montantPreleve = $facture['montant_preleve'];
                        $dataReglement = [
                            'mode_reglement' => 3,
                            'date_reglement' => $dateAction,
                            'id_banque' => $datapost['banqueId'],
                            'dossier_id' => $dossierId,
                            'date_action' => date('Y-m-d H:i:s'),
                            'util_id' => $_SESSION['session_utilisateur']['util_id'],
                            'idc_prelevement' => $idPrelevement,
                            'montant_total' => $montantPreleve
                        ];

                        $idReglement =  $this->reglement->save_data($dataReglement);
                        // $montantreste = $this->facture_annuelle->getResteAPayer($factId, $montantPreleve);

                        // $montantreste = $this->facture_annuelle->getResteAPayer($factId, $montantPreleve);
                        $sommmeventilationrseult = $this->ventilation->get(['columns' => ['montant'], 'clause' => ['id_facture' => $factId]]);


                        $montant = $this->facture_annuelle->get(['columns' => ['fact_montantttc'], 'clause' => ['fact_id' => $factId], 'method' => 'row']);
                        $montantreste = $montant->fact_montantttc;

                        if ($montantreste > 0) {
                            if (!empty($sommmeventilationrseult)) {
                                $sommmeventilation =  array_sum(array_column($sommmeventilationrseult, 'montant'));
                                $montantreste = round($montantreste - ($sommmeventilation + $montantPreleve), 2);
                            } else {
                                $montantreste = round($montantreste - $montantPreleve, 2);
                            }
                            $data_update['fact_etatpaiement'] = 1;
                            $clause = array('fact_id' => $factId);
                            if ($montantreste == 0) {
                                $this->facture_annuelle->save_data($data_update, $clause);
                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montantPreleve,
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                            } elseif ($montantreste < 0) {
                                $this->facture_annuelle->save_data($data_update, $clause);
                                $montant = $montantPreleve - abs($montantreste);
                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montant,
                                    'idc_reglements' => $idReglement
                                ];

                                $dataVentilationreste = [
                                    'dossier_id' => $dossierId,
                                    'montant' => abs($montantreste),
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                                $saveVentilationreste = $this->ventilation->save_data($dataVentilationreste);
                            } else {

                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montantPreleve,
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                            }
                        } else {
                            if (!empty($sommmeventilationrseult)) {
                                $sommmeventilation =  array_sum(array_column($sommmeventilationrseult, 'montant'));
                                $montantreste = round($montantreste + ($sommmeventilation + $montantPreleve), 2);
                            } else {
                                $montantreste = round($montantreste + $montantPreleve, 2);
                            }
                            $data_update['fact_etatpaiement'] = 1;
                            $clause = array('fact_id' => $factId);
                            if ($montantreste == 0) {
                                $this->facture_annuelle->save_data($data_update, $clause);
                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montantPreleve,
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                            } elseif ($montantreste > 0) {
                                $this->facture_annuelle->save_data($data_update, $clause);
                                $montant = $montantPreleve - abs($montantreste);
                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montant,
                                    'idc_reglements' => $idReglement
                                ];

                                $dataVentilationreste = [
                                    'dossier_id' => $dossierId,
                                    'montant' => abs($montantreste),
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                                $saveVentilationreste = $this->ventilation->save_data($dataVentilationreste);
                            } else {

                                $dataVentilation = [
                                    'dossier_id' => $dossierId,
                                    'id_facture' => $factId,
                                    'montant' => $montantPreleve,
                                    'idc_reglements' => $idReglement
                                ];
                                $saveVentilation = $this->ventilation->save_data($dataVentilation);
                            }
                        }



                        array_push($dataXml, $idReglement);
                    }
                }

                echo json_encode($dataXml);
            }
        }
    }

    public function pagedataxml()
    {
        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_m', 'ventilation');
                $datapost = $_POST['data'];
                $data['facture'] = $this->ventilation->getPrelevement($datapost['table_num_facture']);
                $this->renderComponant("Facturation/paiements/facture_impaye/pageSepa", $data);
            }
        }
    }

    function modifierModeReglement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
                $datapost = $_POST['data'];

                $data['facture'] = $this->facture_annuelle->get(
                    [
                        'clause' => ['fact_id' => $datapost['fact_id']],
                        'method' => 'row'
                    ]
                );


                $data['mode_paiement'] = $this->mode_paiement->get([]);

                $this->renderComponant("Facturation/paiements/facture_impaye/modifierReglement", $data);
            }
        }
    }

    function saveChangementRegle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $datapost = $_POST['data'];

                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la changement du règlement",
                );

                $facture = $this->facture_annuelle->get(
                    [
                        'clause' => ['fact_id' => $datapost['fact_id']],
                        'method' => 'row'
                    ]
                );

                $datasave = [
                    'fact_modepaiement' => $datapost['mode_paiement'],
                    'fact_commentaire' => $datapost['commentaire'],
                    'fact_reglementetat' => 1,
                    'fact_ancienetat' => $facture->fact_modepaiement
                ];

                $clause = array('fact_id' => $datapost['fact_id']);
                $request = $this->facture_annuelle->save_data($datasave, $clause);

                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'data' => array(),
                        'message' => "Changement du règlement facture effectuée",
                    );
                }

                echo json_encode($retour);
            }
        }
    }

    function exportXml()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->helper("exportation");
            $this->load->model('Reglement_m', 'reglement');
            $this->load->model('Ics_m', 'ics');
            $this->load->model('Prelevement_m', 'prelevement');
            $datapost = $_POST['data'];
            if (!empty($datapost['idprelevement']) && !empty($datapost['Idreglement'])) {
                $prelevementData = $this->prelevement->get([
                    'clause' => ['idc_prelevement' => $datapost['idprelevement']],
                    'join' => [
                        'c_banque' => 'c_banque.banque_id = c_prelevement.id_banque',
                    ],
                ]);

                $reglementData = $this->reglement->getPrelevement($datapost['Idreglement']);

                if (!empty($prelevementData) && !empty($reglementData)) {
                    $idregle = implode(',', $datapost['Idreglement']);
                    $data = [
                        'prelevement' => $prelevementData,
                        'reglement' => $reglementData,
                        'Idreglmenet' => $idregle,
                        'ics' => $this->ics->get([]),
                        'regeneration' => false,
                    ];

                    $exportXml = export_xml($data);
                    echo $exportXml;
                }
            }
        }
    }

    public function verifierFacture_avoir()
    {
        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $datapost = $_POST['data'];
                $idFacture = $datapost['idfacture'];

                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $idFacture], 'method' => 'row']);
                $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);
                $sommmeventilationrseult = $this->ventilation->get(['columns' => ['montant'], 'clause' => ['id_facture' => $idFacture]]);

                $data['last_date'] = !empty($lastfacturaton) ? date('Y-m-d', strtotime($lastfacturaton->fact_date)) : date('Y-m-d');
                $data['annee'] = $datapost['annee'];
                $data['facture'] = $facture;
                $data['title'] = "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cependant, cette facture a déjà été payée intégralement par un précédent règlement. L'avoir sera considéré comme \"impayé / non remboursé\". Il pourra être ventilé sur une autre facture de dû ou remboursé ultérieurement.";


                if ($facture->fact_etatpaiement != 1) {
                    if (!empty($sommmeventilationrseult)) {
                        $data['title'] = "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cette facture a déjà reçu un paiement partiel. L'avoir ventilera le reste à payer de la facture qui sera considéré comme \"payée\" et l'avoir sera noté comme \"non remboursé/ventilé intégralement\". Il pourra faire l'objet d'une affectation sur une autre facture ou d'un remboursement.";
                    } else {

                        $data['title'] =   "Vous allez enregistrer un avoir pour la facture " . $facture->fact_num . ". Cette facture sera considérée comme payée par l'avoir. L'avoir sera également considéré comme payé/remboursé intégralement.";
                    }
                }

                $this->renderComponant("Facturation/paiements/facture_impaye/modal-valide-factureAvoir", $data);
            }
        }
    }

    public function save_FactureAvoir()
    {

        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $this->load->model('FactureArticle_m', 'facture_article');
                $this->load->model('FacturationDossier_m', 'factu_dossier');
                $datapost = $_POST['data'];
                $idFacture = $datapost['idfacture'];

                $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $idFacture], 'method' => 'row']);


                $sommmeventilationrseult = $this->ventilation->get(['columns' => ['montant'], 'clause' => ['id_facture' => $idFacture]]);
                $data['annee'] = $anneeChoisi = intval($datapost['annee']);
                $anneeprecedent = $anneeChoisi - 1;


                $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

                if (!empty($lastfacturaton)) {
                    $numero = explode("-", $lastfacturaton->fact_num);
                    $num = intval($numero[1]) + 1;
                    $data['numero_facture'] = $anneeChoisi . '-' . str_pad($num, 5, '0', STR_PAD_LEFT);
                } else {
                    $data['numero_facture'] = "$anneeChoisi-00001";
                }


                $montantAvoir = $facture->fact_montantttc;
                $data_update['fact_etatpaiement'] = 1;
                $data_updateLot['c_facturation_status'] = 0;
                $clause = array('fact_id' => $idFacture);
                $idFactureAvoir = null;

                //Récuperer facture article 
                $lotarticle = $this->facture_article->getArticle($facture->fact_id);


                if ($facture->fact_etatpaiement != 1) {
                    if (!empty($sommmeventilationrseult)) {

                        $sommmeventilation =  array_sum(array_column($sommmeventilationrseult, 'montant'));
                        $montant = round($montantAvoir - $sommmeventilation, 2);


                        $dataFacture_Avoir = [
                            'fact_annee' => $datapost['annee'],
                            'fact_num' => $data['numero_facture'],
                            'fact_date' => $datapost['dateFactureAvoir'],
                            'fact_montantht' => -1 * $facture->fact_montantht,
                            'fact_montanttva' => -1 * $facture->fact_montanttva,
                            'fact_montantttc' => -1 * $facture->fact_montantttc,
                            'dossier_id' => $facture->dossier_id,
                            'fact_modepaiement' => 2,
                            'fact_idFactureDu' => $facture->fact_id,
                            'fact_JustificationAvoir' => $datapost['facture_justification'],

                        ];


                        $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);
                        $this->facture_annuelle->save_data($data_update, $clause);

                        foreach ($lotarticle as $key => $value) {

                            $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                            $dataSaveFactureArticle['art_id'] =  $value->art_id;
                            $dataSaveFactureArticle['lot_id'] = $value->lot_id;


                            $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                            $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                            $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;

                            $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                            $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                            $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                            $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;


                            $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                            if ($value->lot_id != null) {
                                $clauseAprepare = array(
                                    'cliLot_id' => $value->lot_id,
                                    'dossier_id' => $facture->dossier_id,
                                    'annee' => $anneeprecedent
                                );

                                $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                            }
                        }

                        $dataVentilation = [
                            'dossier_id' => $facture->dossier_id,
                            'id_facture' => $facture->fact_id,
                            'montant' => $montant,
                            'Id_factureAvoir' => $idFactureAvoir
                        ];
                        $saveVentilation = $this->ventilation->save_data($dataVentilation);

                        $dataVentilationReste = [
                            'dossier_id' => $facture->dossier_id,
                            'montant' => $sommmeventilation,
                            'id_facture' => $idFactureAvoir
                        ];
                        $saveVentilationreste = $this->ventilation->save_data($dataVentilationReste);
                    } else {
                        $dataFacture_Avoir = [
                            'fact_annee' => $datapost['annee'],
                            'fact_num' => $data['numero_facture'],
                            'fact_date' => $datapost['dateFactureAvoir'],
                            'fact_montantht' => -1 * $facture->fact_montantht,
                            'fact_montanttva' => -1 * $facture->fact_montanttva,
                            'fact_montantttc' => -1 * $facture->fact_montantttc,
                            'dossier_id' => $facture->dossier_id,
                            'fact_modepaiement' => 2,
                            'fact_etatpaiement' => 1,
                            'fact_idFactureDu' => $facture->fact_id,
                            'fact_JustificationAvoir' => $datapost['facture_justification'],

                        ];
                        $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);
                        $this->facture_annuelle->save_data($data_update, $clause);

                        foreach ($lotarticle as $key => $value) {

                            $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                            $dataSaveFactureArticle['art_id'] =  $value->art_id;
                            $dataSaveFactureArticle['lot_id'] = $value->lot_id;


                            $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                            $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                            $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;

                            $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                            $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                            $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                            $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;

                            $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                            if ($value->lot_id != null) {
                                $clauseAprepare = array(
                                    'cliLot_id' => $value->lot_id,
                                    'dossier_id' => $facture->dossier_id,
                                    'annee' => $anneeprecedent
                                );

                                $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                            }
                        }

                        $dataVentilation = [
                            'dossier_id' => $facture->dossier_id,
                            'id_facture' => $facture->fact_id,
                            'montant' => $montantAvoir,
                            'Id_factureAvoir' => $idFactureAvoir
                        ];

                        $saveVentilation = $this->ventilation->save_data($dataVentilation);
                    }
                } else {

                    $dataFacture_Avoir = [
                        'fact_annee' => $datapost['annee'],
                        'fact_num' => $data['numero_facture'],
                        'fact_date' => $datapost['dateFactureAvoir'],
                        'fact_montantht' => -1 * $facture->fact_montantht,
                        'fact_montanttva' => -1 * $facture->fact_montanttva,
                        'fact_montantttc' => -1 * $facture->fact_montantttc,
                        'dossier_id' => $facture->dossier_id,
                        'fact_modepaiement' => 2,
                        'fact_idFactureDu' => $facture->fact_id,
                        'fact_JustificationAvoir' => $datapost['facture_justification'],

                    ];

                    $idFactureAvoir =  $this->facture_annuelle->save_data($dataFacture_Avoir);

                    foreach ($lotarticle as $key => $value) {
                        $dataSaveFactureArticle['fact_id'] = $idFactureAvoir;
                        $dataSaveFactureArticle['art_id'] =  $value->art_id;
                        $dataSaveFactureArticle['lot_id'] = $value->lot_id;
                        $dataSaveFactureArticle['art_libellefr'] = $value->art_libellefr;
                        $dataSaveFactureArticle['art_libelleen'] =  $value->art_libelleen;
                        $dataSaveFactureArticle['faa_puht'] =  $value->faa_puht;
                        $dataSaveFactureArticle['faa_qte'] = -1 * $value->faa_qte;
                        $dataSaveFactureArticle['faa_prixht'] = -1 * $value->faa_prixht;
                        $dataSaveFactureArticle['faa_tauxtva'] = -1 * $value->faa_tauxtva;
                        $dataSaveFactureArticle['faa_prixttc'] = -1 * $value->faa_prixttc;

                        $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);

                        //etat à facturer
                        if ($value->lot_id != null) {
                            $clauseAprepare = array(
                                'cliLot_id' => $value->lot_id,
                                'dossier_id' => $facture->dossier_id,
                                'annee' => $anneeprecedent
                            );

                            $this->factu_dossier->save_data($data_updateLot, $clauseAprepare);
                        }
                    }
                }

                $lotarticleFactureAvoir = $this->facture_article->get(
                    array(
                        'clause' => array(
                            'c_facture_articles.fact_id' => $idFactureAvoir,
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_articles.lot_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                        ),
                    )
                );



                $this->Facturer_Avoir($idFactureAvoir, $lotarticleFactureAvoir);
            }
        }
    }




    public function Facturer_Avoir($IdfactureAvoir, $factureArticle)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('DocumentFacturation_m', 'doc_facturation');
            $this->load->model('Article_m', 'article');
            $this->load->model('Facture_m', 'facture_annuelle');
            $this->load->model('MandatSepa_m', 'mandat_sepa');
            $this->load->helper("exportation");


            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la changement du règlement",
            );

            $factureAvoir = $this->facture_annuelle->get(['clause' => ['fact_id' => $IdfactureAvoir], 'method' => 'row']);

            $factureDu = $this->facture_annuelle->get(['clause' => ['fact_id' => $factureAvoir->fact_idFactureDu], 'method' => 'row']);

            $data['annee'] = $anneeChoisi = $factureAvoir->fact_annee;
            $precedent = $anneeChoisi - 1;
            $data['article'] = $this->article->get(['clause' => ['art_id' => 1], 'method' => 'row']);
            $data['lotarticles'] = $factureArticle;


            $data['dossier'] = $this->factu_dossier->get(
                array(
                    'clause' => array('c_facturation_lot.dossier_id' => $factureAvoir->dossier_id, 'c_facturation_lot.annee' => $precedent),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                    ),
                    'method' => 'row'
                )
            );


            $data['sepa'] = $this->mandat_sepa->get(
                [
                    'clause' => ['dossier_id' => $data['dossier']->dossier_id],
                    'order_by_columns' => "sepa_id DESC",
                    'method' => 'row'
                ]
            );

            $data['lot_principale'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $factureAvoir->dossier_id, 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['numero_facture'] = $factureAvoir->fact_num;
            $data['numero_facture_du'] = $factureDu->fact_num;
            $data['factureAvoir'] = $factureAvoir;
            $data['rib'] = $this->getRibCelavi();
            $data['date_fature'] = $factureAvoir->fact_date;
            $file_name = 'facturation_annuelle_' .  $factureAvoir->fact_num;
            $view = 'Facturation/paiements/facture_impaye/export.php';

            $pdf =  exportfacture_pdf($view, $file_name, $data, $factureAvoir->fact_annee, $factureAvoir->dossier_id, $factureAvoir->fact_id);
            $this->saveIndepense($factureArticle, $factureAvoir->fact_date);
            $retour = array(
                'status' => 200,
                'data' => array(),
                'message' => "La facture d'avoir a été créée",
                'pdf' => $pdf
            );

            echo json_encode($retour);
        }
    }

    private function saveIndepense($lot, $date_fature)
    {
        $this->load->model('Charges_m', 'charge');
        $this->load->model('DocumentCharges_m', 'doc_charge');
        $this->load->model('DocumentFacturation_m', 'doc_facture');
        $data = [];

        foreach ($lot as $key => $value) {
            $data['date_facture'] = $date_fature;
            $data['cliLot_id'] = $value->lot_id;
            $data['montant_ht'] = $value->faa_prixht;
            $data['tva'] = $value->faa_tauxtva;
            $data['tva_taux'] = $value->faa_tvapourcentage;
            $data['montant_ttc'] =  $value->faa_prixttc;
            $data['annee'] = date('Y', strtotime($date_fature));
            $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
            $data['charge_date_saisie'] = date('Y-m-d');
            $data['charge_date_validation'] = date('Y-m-d');
            $data['charge_etat_valide'] = 1;
            $data['type_charge_id'] = 18;

            $request = $this->charge->save_data($data);

            if (!empty($request)) {
                $charge = $this->charge->get(['order_by_columns' => "charge_id DESC", 'method' => 'row']);
                $facture = $this->doc_facture->get(['order_by_columns' => "doc_id DESC", 'method' => 'row']);
                $data_file['doc_charge_nom'] = $facture->doc_facturation_nom;
                $data_file['doc_charge_creation'] = $facture->doc_facturation_creation;
                $data_file['doc_charge_etat'] = 1;
                $data_file['doc_charge_traite'] = 0;
                $data_file['doc_charge_annee'] = $value->annee;
                $data_file['cliLot_id'] = $value->cliLot_id;
                $data_file['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data_file['charge_id'] = $charge->charge_id;
                $data_file['doc_charge_path'] = $facture->doc_facturation_path;
                $request = $this->doc_charge->save_data($data_file);
            }
        }
    }
}
