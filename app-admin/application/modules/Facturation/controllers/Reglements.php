<?php

use phpDocumentor\Reflection\Types\This;

class Reglements extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    function pageReglement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Facture_m', 'facture_annuelle');
            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2, 4]);
            $data['facture'] = $this->facture_annuelle->getfactureAnnuelleByid($_POST['data']['fact_id']);

            $this->renderComponant("Facturation/paiements/facture_impaye/pageReglements", $data);
        }
    }

    function saveReglement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montantreglment = floatval(str_replace(",", ".", $data['montant_sur_reglement']));
                $saveReglement = [
                    'mode_reglement' => $data['mode_reglement'],
                    'date_reglement' => $data['datereglement'],
                    'id_banque' => $data['banque_celavi_reglement'],
                    'dossier_id' => $data['dossier_id_reglment'],
                    'montant_total' => $montantreglment,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_reglement']
                ];
                $request = $this->reglement->save_data($saveReglement);

                if (!empty($request)) {
                    $this->GererVentilation($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $request, $message);
                }
            }
            echo json_encode($retour);
        }
    }

    private function GererVentilation($data)
    {
        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Reglement_m', 'reglement');
        $montantreglment = round(floatval(str_replace(",", ".", $data['montant_sur_reglement'])), 2);
        $totalVentilation = floatval($this->ventilation->getTotalVentile($data['fact_id_reglement']));
        $totalVentilation = round($totalVentilation, 2);
        $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $data['fact_id_reglement']], 'method' => 'row']);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);
        $montantfacture = round(floatval($facture->fact_montantttc) - ($totalVentilation + $montantreglment), 2);


        $saveVentil = [
            'dossier_id' => $data['dossier_id_reglment'],
            'id_facture' => $facture->fact_id,
            'montant' => ($montantfacture < 0) ? round((floatval($facture->fact_montantttc) - $totalVentilation), 2) : round($montantreglment, 2),
            'idc_reglements' => $last_reglement->idc_reglements,
        ];
        $this->ventilation->save_data($saveVentil);

        if ($montantfacture <= 0) {
            $savefacture = ['fact_etatpaiement' => 1];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);

            if ($montantfacture < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossier_id_reglment'],
                    'id_facture' => NULL,
                    'montant' => abs($montantfacture),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }
}
