<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Facturation extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/facturation/facturation.js",
            "js/prepa_facturation/prepa_facturation.js",
            "js/prepa_facturation/a_facture.js",
            "js/facturation/facture.js",
            "js/facturation/dossier_incomplet.js",
            "js/facturation/prelevement.js",
            "js/paie_facturation/facturation_impaye.js",
            "js/paie_facturation/facturation_paye.js",
            "js/facturation/reglements.js",
            "js/facturation/remboursement.js",
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "facturation";
        $this->_data['sous_module'] = "facturation" . DIRECTORY_SEPARATOR . "contenaire-facturation";
        $this->render('contenaire');
    }

    public function page_facturation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Facturation/facturation/facturation");
            }
        }
    }
}
