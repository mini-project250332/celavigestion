<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facturation_paye extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function page_facture_paye()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
                $data['mode_paiement'] = $this->mode_paiement->get(array(
                    'clause' => array('id_mode_paie !=' => 1),
                ));
                $this->renderComponant("Facturation/paiements/paiement_effectue/contenu-facturation-paye", $data);
            }
        }
    }

    public function facture_paye()
    {
        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $this->load->model('Reglement_ventilation_m', 'reglement');

                // $thisload->model ('')->load->model('Reglement_m', 'reglement');
                $datapost = $_POST['data'];
                $clause['annereglement'] = $datapost['annee'];
                $dossierIDS = null;
                $like = array();

                $pagination_data = explode('-', $datapost['pagination_page']);
                $rows_limit = intval($datapost['row_data']);

                if ($datapost['num_dossier_facture'] != '') {
                    $like['f.dossier_id'] = $datapost['num_dossier_facture'];
                }

                if ($datapost['regleId'] != 0) {
                    $clause['f.fact_modepaiement'] = intval($datapost['regleId']);
                }

                if ($datapost['num_facture'] != '') {
                    $like['f.fact_num'] = $datapost['num_facture'];
                    $dossierIDS = $this->facture_annuelle->getDossierIds($like);

                    $dossierIDArray = array_map(function ($dossier) {
                        return $dossier->dossier_id;
                    }, $dossierIDS);

                    $like['facture'] = $dossierIDArray;
                }
                if ($datapost['rumId'] != '') {
                    $like['ms.sepa_numero'] = $datapost['rumId'];
                }

                if ($datapost['facture_filtre_proprietaire'] != '') {
                    $like['client'] = $datapost['facture_filtre_proprietaire'];
                }
                //Liste
                $facture = $this->reglement->listereglement($clause, $like, $pagination_data[1], $pagination_data[0]);

                $nbreglement = $this->reglement->listereglements($clause, $like);

                //Resulatats
                $nb_total = count($nbreglement);
                $data["countAllResult"] = $nb_total;
                $data['pagination_page'] = $datapost['pagination_page'];
                $data['rows_limit'] = $rows_limit;
                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];
                // $facture = array_slice($facture, $pagination_data[0], $pagination_data[1]);
                $data['facture'] = $facture;

                $this->renderComponant("Facturation/paiements/paiement_effectue/table-facture-paye", $data);
            }
        }
    }
    public function facture_payeOld()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $datapost = $_POST['data'];
                $clause['fact_annee'] = $datapost['annee'];
                $clause['fact_etatpaiement'] = 1;
                $likefacturation = array();

                $pagination_data = explode('-', $datapost['pagination_page']);
                $rows_limit = intval($datapost['row_data']);

                if ($datapost['num_dossier_facture'] != '') {
                    $likefacturation['f.dossier_id'] = $datapost['num_dossier_facture'];
                }

                if ($datapost['regleId'] != 0) {
                    $clause['f.fact_modepaiement'] = intval($datapost['regleId']);
                }

                if ($datapost['num_facture'] != '') {
                    $likefacturation['f.fact_num'] = $datapost['num_facture'];
                }

                if ($datapost['rumId'] != '') {
                    $likefacturation['ms.sepa_numero'] = $datapost['rumId'];
                }


                $like_progm = ($datapost['facture_filtre_progrm'] != '') ? ['progm_nom' => $datapost['facture_filtre_progrm']] : null;
                $like_proprietaire = ($datapost['facture_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['facture_filtre_proprietaire']] : null;

                $facture = $this->facture_annuelle->getFacture($clause, $likefacturation);

                $lotModel = $this->lot;

                foreach ($facture as $key => $value) {
                    $table_lot = explode(',', $value->cliLot_id);
                    $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                        $lot = $this->lot->get([
                            'clause' => ['cliLot_id' => $value_lot],
                            'like' => $like_progm,
                            'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                            'method' => 'row'
                        ]);

                        return (!empty($lot)) ? $lot->progm_nom : null;
                    }, $table_lot);

                    $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

                    if ($facture[$key]->progm_nom == '') {
                        unset($facture[$key]);
                        continue;
                    }

                    $clientModel = $this->client;
                    $table_client = explode(',', $value->client_id);
                    $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                        $clientData = $this->client->get([
                            'clause' => ['client_id' => $client],
                            'like' => $like_proprietaire,
                            'method' => 'row'
                        ]);

                        return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
                    }, $table_client);

                    $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

                    if ($facture[$key]->client_nom == '') {
                        unset($facture[$key]);
                        continue;
                    }
                }

                $data["countAllResult"] = $this->countAllData('Facture_m', array('fact_annee' =>  $datapost['annee']));


                if (
                    $datapost['annee'] != '' ||
                    $datapost['facture_filtre_proprietaire'] != '' ||
                    $datapost['facture_filtre_progrm'] != '' ||
                    $datapost['num_dossier_facture'] != '' ||
                    $datapost['num_facture'] != '' ||
                    $datapost['regleId'] !=  0 ||
                    $datapost['rumId'] != ''
                ) {
                    $data["countAllResult"] = $this->countUser($clause, $likefacturation, $like_progm, $like_proprietaire, $datapost);
                }

                $nb_total = $data["countAllResult"];
                $data['pagination_page'] = $datapost['pagination_page'];
                $data['rows_limit'] = $rows_limit;
                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];
                $facture = array_slice($facture, $pagination_data[0], $pagination_data[1]);
                $data['facture'] = $facture;
                $this->renderComponant("Facturation/paiements/paiement_effectue/table-facture-paye", $data);
            }
        }
    }

    function countAllData($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }

    function countUser($clause, $likefacturation, $like_progm, $like_proprietaire, $datapost)
    {
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('ClientLot_m', 'lot');
        $this->load->model('Client_m', 'client');

        $facture = $this->facture_annuelle->getFactureCount($clause, $likefacturation);
        $lotModel = $this->lot;

        foreach ($facture as $key => $value) {
            $table_lot = explode(',', $value->cliLot_id);
            $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                $lot = $this->lot->get([
                    'clause' => ['cliLot_id' => $value_lot],
                    'like' => $like_progm,
                    'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                    'method' => 'row'
                ]);

                return (!empty($lot)) ? $lot->progm_nom : null;
            }, $table_lot);

            $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

            if ($facture[$key]->progm_nom == '') {
                unset($facture[$key]);
                continue;
            }
            $clientModel = $this->client;
            $table_client = explode(',', $value->client_id);
            $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                $clientData = $this->client->get([
                    'clause' => ['client_id' => $client],
                    'like' => $like_proprietaire,
                    'method' => 'row'
                ]);

                return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
            }, $table_client);

            $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

            if ($facture[$key]->client_nom == '') {
                unset($facture[$key]);
                continue;
            }
        }

        return !empty($facture) ? count($facture) : 0;
    }

    public function getdetailReglement()
    {
        if (is_ajax()) {

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Reglement_ventilation_m', 'ventilation');
                $this->load->model('Reglement_m', 'reglement');

                $datapost = $_POST['data'];

                $regelementDetail = $data['reglement'] = $this->ventilation->get([
                    'clause' => ['idc_reglementVentilation' => $datapost['ventil_id']],
                    'join' => [
                        'c_reglements' => 'c_reglements.idc_reglements = c_reglementventilation.idc_reglements',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_reglements.util_id',
                    ],
                    'method' => 'row'
                ]);

                $this->renderComponant("Facturation/paiements/paiement_effectue/detail-regelement", $data);
            }
        }
    }
}
