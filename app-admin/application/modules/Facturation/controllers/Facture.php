<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Facture extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function facture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Facturation/facture/content-facture");
            }
        }
    }

    function getTableFacture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Facture_m', 'facture_annuelle');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $datapost = $_POST['data'];

                $clause['fact_annee'] = $datapost['annee'];
                $pagination_data = explode('-', $datapost['pagination_page']);
                $rows_limit = intval($datapost['row_data']);
                $like = null;

                $like_progm = ($datapost['facture_filtre_progrm'] != '') ? ['progm_nom' => $datapost['facture_filtre_progrm']] : null;
                $like_proprietaire = ($datapost['facture_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['facture_filtre_proprietaire']] : null;

                $facture = $this->facture_annuelle->listFacture($datapost);

                $lotModel = $this->lot;
                foreach ($facture as $key => $value) {
                    $table_lot = explode(',', $value->cliLot_id);
                    $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                        $lot = $this->lot->get([
                            'clause' => ['cliLot_id' => $value_lot],
                            'like' => $like_progm,
                            'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                            'join_orientation' => 'left',
                            'method' => 'row'
                        ]);

                        return (!empty($lot)) ? $lot->progm_nom : null;
                    }, $table_lot);

                    $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

                    // if ($facture[$key]->progm_nom == '') {
                    //     unset($facture[$key]);
                    //     continue;
                    // }

                    $clientModel = $this->client;
                    $table_client = explode(',', $value->client_id);
                    $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                        $clientData = $this->client->get([
                            'clause' => ['client_id' => $client],
                            'like' => $like_proprietaire,
                            'method' => 'row'
                        ]);

                        return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
                    }, $table_client);

                    $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

                    if ($facture[$key]->client_nom == '') {
                        unset($facture[$key]);
                        continue;
                    }
                }

                $data["countAllResult"] = $this->countAllData('Facture_m', array('fact_annee' =>  $datapost['annee']));

                if (
                    $datapost['annee'] != '' ||
                    $datapost['facture_filtre_proprietaire'] != '' ||
                    $datapost['facture_filtre_progrm'] != '' ||
                    $datapost['num_dossier_facture'] != '' ||
                    $datapost['num_facture'] != ''

                ) {
                    $data["countAllResult"] = $this->countUser($clause, $like, $like_progm, $like_proprietaire, $datapost);
                }

                $nb_total = $data["countAllResult"];

                $data['pagination_page'] = $datapost['pagination_page'];
                $data['rows_limit'] = $rows_limit;

                $nb_page = round(intval($nb_total) / intval($rows_limit), 0, PHP_ROUND_HALF_UP);
                $data["li_pagination"] = ($nb_total < (intval($rows_limit) + 1)) ? [0] : range(0, $nb_total, intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];

                $data['totalTTC'] = array_reduce($facture, function ($acc, $value) {
                    return $acc + $value->fact_montantttc;
                }, 0);
                $data['totalHT'] = array_reduce($facture, function ($acc, $value) {
                    return $acc + $value->fact_montantht;
                }, 0);

                $facture = array_slice($facture, $pagination_data[0], $pagination_data[1]);
                $data['facture'] = $facture;

                $this->renderComponant("Facturation/facture/table-facture", $data);
            }
        }
    }

    function countAllData($model, $clause)
    {
        $this->load->model($model, 'model');
        $params = array(
            'clause' => $clause,
            'columns' => array('COUNT(*) as allData'),
        );
        $request = $this->model->get($params);
        return $request[0]->allData;
    }

    function countUser($clause, $likefacturation, $like_progm, $like_proprietaire, $datapost)
    {
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('ClientLot_m', 'lot');
        $this->load->model('Client_m', 'client');

        $facture = $this->facture_annuelle->listFacturecount($datapost);

        $lotModel = $this->lot;

        foreach ($facture as $key => $value) {
            $table_lot = explode(',', $value->cliLot_id);
            $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                $lot = $this->lot->get([
                    'clause' => ['cliLot_id' => $value_lot],
                    'like' => $like_progm,
                    'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                    'method' => 'row'
                ]);

                return (!empty($lot)) ? $lot->progm_nom : null;
            }, $table_lot);

            $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

            if ($facture[$key]->progm_nom == '') {
                unset($facture[$key]);
                continue;
            }
            $clientModel = $this->client;
            $table_client = explode(',', $value->client_id);
            $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                $clientData = $this->client->get([
                    'clause' => ['client_id' => $client],
                    'like' => $like_proprietaire,
                    'method' => 'row'
                ]);

                return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
            }, $table_client);

            $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

            if ($facture[$key]->client_nom == '') {
                unset($facture[$key]);
                continue;
            }
        }

        return !empty($facture) ? count($facture) : 0;
    }

    /***** ****/

    function listExportPDf()
    {
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('ClientLot_m', 'lot');
        $this->load->model('Client_m', 'client');
        $this->load->helper("exportation");

        $datapost = $_POST['data'];

        $like = null;
        $like_progm = ($datapost['facture_filtre_progrm'] != '') ? ['progm_nom' => $datapost['facture_filtre_progrm']] : null;
        $like_proprietaire = ($datapost['facture_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['facture_filtre_proprietaire']] : null;

        $facture = $this->facture_annuelle->listFacture_exportPDF($datapost);
        $lotModel = $this->lot;
        foreach ($facture as $key => $value) {
            $table_lot = explode(',', $value->cliLot_id);
            $filteredLots = array_map(function ($value_lot) use ($datapost, $like_progm, $lotModel) {
                $lot = $this->lot->get([
                    'clause' => ['cliLot_id' => $value_lot],
                    'like' => $like_progm,
                    'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                    'method' => 'row'
                ]);

                return (!empty($lot)) ? $lot->progm_nom : null;
            }, $table_lot);

            $facture[$key]->progm_nom = implode(', ', array_filter($filteredLots));

            if ($facture[$key]->progm_nom == '') {
                unset($facture[$key]);
                continue;
            }

            $clientModel = $this->client;
            $table_client = explode(',', $value->client_id);
            $filteredClients = array_map(function ($client) use ($datapost, $like_proprietaire, $clientModel) {
                $clientData = $this->client->get([
                    'clause' => ['client_id' => $client],
                    'like' => $like_proprietaire,
                    'method' => 'row'
                ]);

                return (!empty($clientData)) ? $clientData->client_nom . ' ' . $clientData->client_prenom : null;
            }, $table_client);

            $facture[$key]->client_nom = implode(', ', array_filter($filteredClients));

            if ($facture[$key]->client_nom == '') {
                unset($facture[$key]);
                continue;
            }
        }

        $data['facture'] = $facture;
        $view = "Facturation/facture/model-pdf";
        // $this->load->view('Facturation/facture/model-pdf',$data);
        exportation_pdf_facture($view, $data);
    }
}
