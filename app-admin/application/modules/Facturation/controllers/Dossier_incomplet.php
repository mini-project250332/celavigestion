<?php class Dossier_incomplet extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    function dossier_incomplet()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Facturation/dossier_incomplet/dossier_incomplet-content");
            }
        }
    }

    function ListeDossierIncomplet()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');
                $datapost = $_POST['data'];

                $proprietaire = $datapost['proprietaire_lot_sans_mandat'] ?? null;
                $programme = $datapost['programme_lot_sans_mandat'] ?? null;
                $rows_limit = intval($datapost['row_data']);
                $pagination_data = explode('-', $datapost['pagination_page']);

                $data['lot_sans_mandat'] = $this->lot->getListlot($proprietaire, $programme, $pagination_data);
                $totalLots = $this->lot->getListlotTotal($proprietaire, $programme);
                $data['total'] = count($totalLots) > 1 ? count($totalLots) : 0;

                $data["li_pagination"] = count($totalLots) < (intval($rows_limit) + 1) ? [0] : range(0, count($totalLots), intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];

                $this->renderComponant("Facturation/dossier_incomplet/dossier_incomplet-liste", $data);
            }
        }
    }
}
