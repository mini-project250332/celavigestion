<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class A_Facturer extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function page_facture_valide()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Facturation/preparation_facture/a_facturer/contenu-facturation-valide");
            }
        }
    }

    public function filtreFactureValide()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FacturationDossier_m', 'factu_dossier');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('MandatNew_m', 'mandat');
                $this->load->model('Client_m', 'client');

                $datapost = $_POST['data'];
                $anneeChoisi = intval($datapost['annee']);
                $precedent = $anneeChoisi - 1;
                $data['anneeChoisi'] = $anneeChoisi;
                $data['anneeprecedent'] = $precedent;

                $facturation = $this->factu_dossier->obtenir_facturations_liste($precedent);

                foreach ($facturation as $key => $value) {
                    $lot_facturation = $this->factu_dossier->get(['clause' => ['dossier_id' => $value->dossier_id, 'annee' => $precedent, 'c_facturation_status' => 1]]);

                    $client_id = explode(',', $value->client_id);
                    $facturation[$key]->client_nom = '';
                    $facturation[$key]->client_prenom = '';

                    $like = ($datapost['text_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['text_filtre_proprietaire']] : [];

                    foreach ($client_id as $client) {
                        $client = $this->client->get([
                            'clause' => ['client_id' => $client],
                            'like' => $like,
                            'method' => 'row'
                        ]);

                        if (!empty($client)) {
                            $facturation[$key]->client_nom .= $facturation[$key]->client_nom == '' ? $client->client_nom . ' ' . $client->client_prenom : ', ' . $client->client_nom . ' ' . $client->client_prenom;
                        }
                    }

                    if ($facturation[$key]->client_nom == '') {
                        unset($facturation[$key]);
                        continue;
                    }

                    $facturation[$key]->lot = array_values(array_filter(array_map(function ($value_lot) use ($datapost) {
                        $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];

                        $lot = [
                            'num_lot' => $value_lot->cliLot_id,
                            'tarrif' => $value_lot->montant_ht_facture,
                            'tarrif_figurant' => $value_lot->montant_ht_figurant,
                            'tarrif_suivant' => '',
                            'tarrif_index' => '',
                            'c_facturation_commentaire' => '',
                            'id_fact_lot' => '',
                        ];

                        $identification_lot = $this->lot->get([
                            'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                            'like' => $like_progm,
                            'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                            'method' => 'row'
                        ]);

                        $lot['progm_nom'] = $identification_lot->progm_nom ?? '-';

                        $mandat = $this->mandat->get([
                            'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                            'order_by_columns' => 'mandat_id DESC',
                            'method' => 'row'
                        ]);

                        if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-') {
                            return null;
                        }

                        $lot['mandat_num'] = $mandat->mandat_id;
                        $lot['mandat_signature'] = $mandat->mandat_date_signature;
                        $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                        $lot_facturation_suivant = $this->factu_dossier->get(['clause' => ['dossier_id' => $value_lot->dossier_id, 'cliLot_id' => $value_lot->cliLot_id, 'annee' => $value_lot->annee + 1], 'method' => 'row']);

                        if (!empty($lot_facturation_suivant)) {
                            $lot['tarrif_suivant'] = $lot_facturation_suivant->montant_ht_facture;
                            $lot['tarrif_index'] = $lot_facturation_suivant->c_facturation_indexation;
                            $lot['c_facturation_commentaire'] = $lot_facturation_suivant->c_facturation_commentaire;
                            $lot['id_fact_lot'] = $lot_facturation_suivant->id_fact_lot;
                        }

                        return $lot;
                    }, $lot_facturation)));

                    if (empty($facturation[$key]->lot)) {
                        unset($facturation[$key]);
                    }
                }

                $data['facturations'] = $facturation;

                $this->renderComponant("Facturation/preparation_facture/a_facturer/contenu-valide", $data);
            }
        }
    }

    public function annulerValidation()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de l'annulation",
            );
            $data = decrypt_service($_POST["data"]);

            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'id' => $data['dossier_id'],
                        'annee' => $data['annee'],
                        'tab_lot' => $data['tab_lot'],
                        'title' => "Confirmation de l'annulation",
                        'text' => "Voulez-vous annuler la facture ?",
                        'btnConfirm' => "Confirmer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } elseif ($data['action'] == "confirm") {

                $data_update['c_facturation_status'] = 0;

                foreach ($data['tab_lot'] as $key => $value) {
                    $clause = array('dossier_id' => $data['dossier_id'], 'annee' => $data['annee'] - 1, 'cliLot_id' => $value);
                    $request = $this->factu_dossier->save_data($data_update, $clause);
                }

                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'dossier_id' => $data['dossier_id'],
                            'annee' => $data['annee'],
                        ),
                        'message' => "Annulation effectuée",
                    );
                }
            }
            echo json_encode($retour);
        }
    }

    function apercu_facture()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('Article_m', 'article');
            $this->load->model('MandatSepa_m', 'mandat_sepa');
            $datapost = $_POST['data'];

            $data['annee'] = $anneeChoisi = intval($datapost['annee']);
            $precedent = $anneeChoisi - 1;

            $data['article'] = $this->article->get(['clause' => ['art_id' => 1], 'method' => 'row']);

            $data['dossier'] = $this->factu_dossier->get(
                array(
                    'clause' => array('c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $precedent),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['sepa'] = $this->mandat_sepa->get(
                [
                    'clause' => ['dossier_id' => $data['dossier']->dossier_id],
                    'order_by_columns' => "sepa_id DESC",
                    'method' => 'row'
                ]
            );

            $data['lot'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                )
            );

            $data['lot'] = array_filter($data['lot'], function ($value) use ($datapost) {
                return in_array($value->cliLot_id, $datapost['tab_lot']);
            });

            $data['lot_principale'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['rib'] = $this->getRibCelavi();

            $this->renderComponant("Facturation/preparation_facture/a_facturer/apercu-facture", $data);
        }
    }

    function modalvalider()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Facture_m', 'facture_annuelle');
            $datapost = $_POST['data'];

            $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

            $data['last_date'] = !empty($lastfacturaton) ? date('Y-m-d', strtotime($lastfacturaton->fact_date)) : date('Y-m-d');

            $data['dossier_id'] = $datapost['dossier_id'];
            $data['annee'] = $datapost['annee'];
            $data['tab_lot'] = $datapost['tab_lot'];

            $this->renderComponant("Facturation/preparation_facture/a_facturer/modal-valide-facture", $data);
        }
    }

    function ValidationFacturer()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $datapost = $_POST['data'];

            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la création de la facture",
            );

            $data_update['c_facturation_status'] = 2;

            foreach ($datapost['tab_lot'] as $key => $value) {
                $clause = array('dossier_id' => $datapost['dossier_id'], 'annee' => $datapost['annee'] - 1, 'cliLot_id' => $value);
                $request = $this->factu_dossier->save_data($data_update, $clause);
            }

            if ($request) {
                $retour = array(
                    'status' => 200,
                    'data' => array(
                        'dossier_id' => $datapost['dossier_id'],
                        'annee' => $datapost['annee'],
                        'date_fature' => $datapost['date_fature'],
                        'tab_lot' => $datapost['tab_lot']
                    ),
                    'message' => "Création de la facture effectuée",
                );
            }

            echo json_encode($retour);
        }
    }

    function facturerDossier()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('DocumentFacturation_m', 'doc_facturation');
            $this->load->model('Article_m', 'article');
            $this->load->model('Facture_m', 'facture_annuelle');
            $this->load->model('FactureArticle_m', 'facture_article');
            $this->load->model('MandatSepa_m', 'mandat_sepa');
            $this->load->helper("exportation");

            $datapost = $_POST['data'];

            $data['annee'] = $anneeChoisi = intval($datapost['annee']);
            $precedent = $anneeChoisi - 1;

            $data['article'] = $this->article->get(['clause' => ['art_id' => 1], 'method' => 'row']);

            $data['lot'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                )
            );

            $data['lot'] = array_filter($data['lot'], function ($value) use ($datapost) {
                return in_array($value->cliLot_id, $datapost['tab_lot']);
            });

            $data['dossier'] = $this->factu_dossier->get(
                array(
                    'clause' => array('c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $precedent),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_client' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                    ),
                    'method' => 'row'
                )
            );

            $data['sepa'] = $this->mandat_sepa->get(
                [
                    'clause' => ['dossier_id' => $data['dossier']->dossier_id],
                    'order_by_columns' => "sepa_id DESC",
                    'method' => 'row'
                ]
            );

            $data['lot_principale'] = $this->factu_dossier->get(
                array(
                    'clause' => array(
                        'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                    ),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                    'method' => 'row'
                )
            );

            $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

            if (!empty($lastfacturaton)) {
                $numero = explode("-", $lastfacturaton->fact_num);
                $num = intval($numero[1]) + 1;
                $data['numero_facture'] = $anneeChoisi . '-' . str_pad($num, 5, '0', STR_PAD_LEFT);
            } else {
                $data['numero_facture'] = "$anneeChoisi-00001";
            }

            $data['rib'] = $this->getRibCelavi();
            $data['date_fature'] = $datapost['date_fature'] = $datapost['date_fature'] != NULL || $datapost['date_fature'] != '0000-00-00 00:00:00' ? $datapost['date_fature'] : date('Y-m-d');

            $data_save = [
                'fact_annee' => $anneeChoisi,
                'fact_num' => $data['numero_facture'],
                'fact_date' => $data['date_fature'],
                // 'article_id' => $data['article']->art_id,
                'dossier_id' => $datapost['dossier_id'],
                // 'cliLot_id' => implode(',', $datapost['tab_lot']),
            ];

            $totalht = 0;

            $totalht = array_sum(array_column($data['lot'], 'montant_ht_facture'));
            $data_save['fact_modepaiement'] = !empty($data['lot']) ? end($data['lot'])->mode_paiement : null;

            // Arrondir le résultat à 2 décimales si nécessaire
            $totalht = round($totalht, 2);
            $data_save['fact_montantht'] = $totalht;
            $data_save['fact_montanttva'] = $tva = round(($totalht * 20) / 100, 2);
            $data_save['fact_montantttc'] = round(($totalht + $tva), 2);


            $request = $this->facture_annuelle->save_data($data_save);

            if ($request) {
                $id_fact = $data['factureCree'] = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);
                $dataSaveFactureArticle = [];



                foreach ($data['lot'] as $key => $value) {

                    $totalhtlot = round($value->montant_ht_facture, 2);
                    $tvalot = round($totalhtlot * 0.20, 2);
                    $ttclot = round($totalhtlot + $tvalot, 2);

                    $dataSaveFactureArticle['fact_id'] = $id_fact->fact_id;
                    $dataSaveFactureArticle['art_id'] =  $data['article']->art_id;
                    $dataSaveFactureArticle['lot_id'] = $value->cliLot_id;


                    $dataSaveFactureArticle['art_libellefr'] = $data['article']->art_libelle_fr;
                    $dataSaveFactureArticle['art_libelleen'] =  $data['article']->art_libelle_en;
                    $dataSaveFactureArticle['faa_puht'] = $totalhtlot;


                    $dataSaveFactureArticle['faa_prixht'] = $totalhtlot;
                    $dataSaveFactureArticle['faa_tauxtva'] = $tvalot;
                    $dataSaveFactureArticle['faa_prixttc'] = $ttclot;

                    $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);
                }



                $file_name = 'facturation_annuelle_' .  $data['numero_facture'];
                $view = 'Facturation/preparation_facture/a_facturer/export.php';

                $data['lotarticle'] =  $this->facture_article->get(
                    array(
                        'clause' => array(
                            'c_facture_articles.fact_id' => $id_fact->fact_id
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_articles.lot_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                        ),
                    )
                );

                exportdossier_pdf($view, $file_name, $data, $datapost['annee'], $datapost['dossier_id'], $id_fact->fact_id);
                $this->saveIndepense($data['lotarticle'], $data['date_fature']);
                $this->createMailcontenu($datapost);
            }
        }
    }

    private function saveIndepense($lot, $date_fature)
    {
        $this->load->model('Charges_m', 'charge');
        $this->load->model('DocumentCharges_m', 'doc_charge');
        $this->load->model('DocumentFacturation_m', 'doc_facture');
        $data = [];

        foreach ($lot as $key => $value) {
            $data['date_facture'] = $date_fature;
            $data['cliLot_id'] = $value->lot_id;
            $data['montant_ht'] = $value->faa_puht;
            $data['tva'] = $value->faa_tauxtva;
            $data['tva_taux'] = $value->faa_tvapourcentage;
            $data['montant_ttc'] =  $value->faa_prixttc;
            $data['annee'] = date('Y', strtotime($date_fature));
            $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
            $data['charge_date_saisie'] = date('Y-m-d');
            $data['charge_date_validation'] = date('Y-m-d');
            $data['charge_etat_valide'] = 1;
            $data['type_charge_id'] = 18;

            $request = $this->charge->save_data($data);

            if (!empty($request)) {
                $charge = $this->charge->get(['order_by_columns' => "charge_id DESC", 'method' => 'row']);
                $facture = $this->doc_facture->get(['order_by_columns' => "doc_id DESC", 'method' => 'row']);

                $data_file['doc_charge_nom'] = $facture->doc_facturation_nom;
                $data_file['doc_charge_creation'] = $facture->doc_facturation_creation;
                $data_file['doc_charge_etat'] = 1;
                $data_file['doc_charge_traite'] = 0;
                $data_file['doc_charge_annee'] = $value->annee;
                $data_file['cliLot_id'] = $value->cliLot_id;
                $data_file['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                $data_file['charge_id'] = $charge->charge_id;
                $data_file['doc_charge_path'] = $facture->doc_facturation_path;

                $request = $this->doc_charge->save_data($data_file);
            }
        }
    }

    function ModaltoutSelectionFacturer()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Facture_m', 'facture_annuelle');

            $datapost = $_POST['data'];

            $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

            $data['last_date'] = !empty($lastfacturaton) ? date('Y-m-d', strtotime($lastfacturaton->fact_date)) : date('Y-m-d');
            $data['annee'] = $datapost['annee'];

            $this->renderComponant("Facturation/preparation_facture/a_facturer/modal-valide-facture-tout", $data);
        }
    }

    function toutSelectionFacturer()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $datapost = $_POST['data'];

            foreach ($datapost['tab_content'] as $key => $value) {
                $this->facturertout($datapost['annee'], $datapost['date_facture'], $value);
            }
        }
    }

    private function facturertout($annee, $date_facture, $datapost)
    {
        $this->load->model('FacturationDossier_m', 'factu_dossier');
        $this->load->model('DocumentFacturation_m', 'doc_facturation');
        $this->load->model('Article_m', 'article');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->helper("exportation");
        $this->load->model('MandatSepa_m', 'mandat_sepa');
        $this->load->model('FactureArticle_m', 'facture_article');
        $data_update['c_facturation_status'] = 2;
        $datapost['annee'] = $annee;

        foreach ($datapost['tab_lot'] as $key => $value) {
            $clause = array('dossier_id' => $datapost['dossier_id'], 'annee' => $annee - 1, 'cliLot_id' => $value);
            $request = $this->factu_dossier->save_data($data_update, $clause);
        }

        $data['annee'] = $anneeChoisi = intval($annee);
        $precedent = $anneeChoisi - 1;

        $data['article'] = $this->article->get(['clause' => ['art_id' => 1], 'method' => 'row']);

        $data['lot'] = $this->factu_dossier->get(
            array(
                'clause' => array(
                    'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi
                ),
                'join' => array(
                    'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                ),
            )
        );

        $data['lot'] = array_filter($data['lot'], function ($value) use ($datapost) {
            return in_array($value->cliLot_id, $datapost['tab_lot']);
        });

        $data['dossier'] = $this->factu_dossier->get(
            array(
                'clause' => array('c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $precedent),
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                    'c_client' => 'c_client.client_id = c_dossier.client_id',
                    'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
                ),
                'method' => 'row'
            )
        );

        $data['sepa'] = $this->mandat_sepa->get(
            [
                'clause' => ['dossier_id' => $data['dossier']->dossier_id],
                'order_by_columns' => "sepa_id DESC",
                'method' => 'row'
            ]
        );

        $data['lot_principale'] = $this->factu_dossier->get(
            array(
                'clause' => array(
                    'c_facturation_lot.dossier_id' => $datapost['dossier_id'], 'c_facturation_lot.annee' => $anneeChoisi, 'cliLot_principale' => 1
                ),
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                    'c_lot_client' => 'c_lot_client.cliLot_id = c_facturation_lot.cliLot_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                ),
                'method' => 'row'
            )
        );

        $lastfacturaton = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);

        if (!empty($lastfacturaton)) {
            $numero = explode("-", $lastfacturaton->fact_num);
            $num = intval($numero[1]) + 1;
            $data['numero_facture'] = $anneeChoisi . '-' . str_pad($num, 5, '0', STR_PAD_LEFT);
        } else {
            $data['numero_facture'] = "$anneeChoisi-00001";
        }

        $data['rib'] = $this->getRibCelavi();

        $data['date_fature'] = $date_facture = $date_facture != NULL || $date_facture != '0000-00-00 00:00:00' ? $date_facture : date('Y-m-d');


        $data_save = [
            'fact_annee' => $anneeChoisi,
            'fact_num' => $data['numero_facture'],
            'fact_date' => $data['date_fature'],
            // 'article_id' => $data['article']->art_id,
            'dossier_id' => $datapost['dossier_id'],
            // 'cliLot_id' => implode(',', $datapost['tab_lot']),
        ];

        $totalht = 0;

        $totalht = array_sum(array_column($data['lot'], 'montant_ht_facture'));
        $data_save['fact_modepaiement'] = !empty($data['lot']) ? end($data['lot'])->mode_paiement : null;

        // Arrondir le résultat à 2 décimales si nécessaire
        $totalht = round($totalht, 2);
        $data_save['fact_montantht'] = $totalht;
        $data_save['fact_montanttva'] = $tva = ($totalht * 20) / 100;
        $data_save['fact_montantttc'] = ($totalht + $tva);


        $request = $this->facture_annuelle->save_data($data_save);

        if ($request) {
            $id_fact = $data['factureCree'] = $this->facture_annuelle->get(['order_by_columns' => 'fact_id DESC', 'method' => 'row']);
            $dataSaveFactureArticle = [];



            foreach ($data['lot'] as $key => $value) {

                $totalhtlot = round($value->montant_ht_facture, 2);
                $tvalot = round($totalhtlot * 0.20, 2);
                $ttclot = round($totalhtlot + $tvalot, 2);

                $dataSaveFactureArticle['fact_id'] = $id_fact->fact_id;
                $dataSaveFactureArticle['art_id'] =  $data['article']->art_id;
                $dataSaveFactureArticle['lot_id'] = $value->cliLot_id;


                $dataSaveFactureArticle['art_libellefr'] = $data['article']->art_libelle_fr;
                $dataSaveFactureArticle['art_libelleen'] =  $data['article']->art_libelle_en;
                $dataSaveFactureArticle['faa_puht'] = $totalhtlot;


                $dataSaveFactureArticle['faa_prixht'] = $totalhtlot;
                $dataSaveFactureArticle['faa_tauxtva'] = $tvalot;
                $dataSaveFactureArticle['faa_prixttc'] = $ttclot;

                $facture_article = $this->facture_article->save_data($dataSaveFactureArticle);
            }



            $file_name = 'facturation_annuelle_' .  $data['numero_facture'];
            $view = 'Facturation/preparation_facture/a_facturer/export.php';

            $data['lotarticle'] =  $this->facture_article->get(
                array(
                    'clause' => array(
                        'c_facture_articles.fact_id' => $id_fact->fact_id
                    ),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_articles.lot_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    ),
                )
            );

            exportdossier_pdf($view, $file_name, $data, $datapost['annee'], $datapost['dossier_id'], $id_fact->fact_id);
            $this->saveIndepense($data['lotarticle'], $data['date_fature']);
            $this->createMailcontenu($datapost);
        }
    }

    private function createMailcontenu($data)
    {
        $this->load->model('Dossier_m', 'dossier');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Modelmail_m', 'c_param_modelemail');

        $get_client = $this->dossier->get([
            'clause' => ['dossier_id' => $data['dossier_id']],
            'join' => [
                'c_client' => 'c_client.client_id = c_dossier.client_id',
                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id'
            ],
            'method' => 'row'
        ]);

        $facture = $this->facture_annuelle->get(
            [
                'join' => ['c_document_facturation' => 'c_document_facturation.fact_id = c_facture.fact_id'],
                'order_by_columns' => 'c_facture.fact_id DESC',
                'method' => 'row'
            ]
        );

        switch ($facture->fact_modepaiement) {
            case 3:
                $model_id = ($get_client->client_nationalite == "Française" || $get_client->client_nationalite == "Belgique") ? 21 : 22;
                break;
            case 4:
                $model_id = ($get_client->client_nationalite == "Française" || $get_client->client_nationalite == "Belgique") ? 23 : 24;
                break;
            default:
                return;
        }

        $model_mail = $this->c_param_modelemail->get([
            'clause' => ['pmail_id' => $model_id],
            'method' => 'row'
        ]);

        $contenu = $model_mail->pmail_contenu;
        $contenu = str_replace("@prenom", $get_client->client_prenom, $contenu);
        $contenu = str_replace("@nom", $get_client->client_nom, $contenu);
        $contenu = str_replace("@annee", $data['annee'], $contenu);
        $contenu = str_replace("@chargee_de_clientele", $get_client->util_prenom . ' ' . $get_client->util_nom, $contenu);

        $subject = $model_mail->pmail_objet;
        $subject = str_replace("@facture_annuelle", 'Facture ' . $data['annee'], $subject);

        // $contenu_pdf = file_get_contents(base_url() . $facture->doc_facturation_path);
        // $base64_pdf = base64_encode($contenu_pdf);
        // $piece_jointe = [
        //     'ContentType' => "application/pdf",
        //     'Filename' => $facture->doc_facturation_nom,
        //     'Base64Content' => $base64_pdf
        // ];

        $message = [
            array(
                'From' => [
                    'Email' => "noreply@celavigestion.fr",
                    'Name' => "CELAVI Gestion"
                ],
                'To' => [
                    [
                        'Email' => trim($get_client->client_email),
                        // 'Email' => trim('ratiazafy123@gmail.com'),
                        'Name' => $get_client->client_prenom . ' ' . $get_client->client_nom
                    ]
                ],
                'Subject' =>  $subject,
                'TextPart' => $contenu,
                'HTMLPart' => nl2br(htmlspecialchars($contenu)),
                // 'Attachments' => [$piece_jointe]
            ),
        ];

        $this->sendEmailViaMailjet($message);
    }

    private function sendEmailViaMailjet($mailjetParams)
    {
        $this->load->library('MailjetService');
        $mailjet = new MailjetService();
        $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
        $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
        return $response;
    }

    /*
    public function listExportPDf()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('ClientLot_m', 'lot');
            $this->load->model('MandatNew_m', 'mandat');
            $this->load->model('Client_m', 'client');
            $this->load->helper("exportation");

            $datapost = $_POST['data']['tab_content'];
            $table_dossier = [];
            $table_dossier = array_map(function ($item) {
                return $item['dossier_id'];
            }, $datapost);

            $precedent = $_POST['data']['annee'] - 1;
            $facturation = $this->factu_dossier->obtenir_facturations($table_dossier, $precedent);

            foreach ($facturation as $key => $value) {
                $lot_facturation = $this->factu_dossier->get(['clause' => ['dossier_id' => $value->dossier_id, 'annee' => $precedent, 'c_facturation_status' => 1]]);

                $client_id = explode(',', $value->client_id);
                $facturation[$key]->client_nom = '';
                $facturation[$key]->client_prenom = '';

                foreach ($client_id as $client) {
                    $client = $this->client->get([
                        'clause' => ['client_id' => $client],
                        'method' => 'row'
                    ]);

                    if (!empty($client)) {
                        $facturation[$key]->client_nom .= $facturation[$key]->client_nom == '' ? $client->client_nom . ' ' . $client->client_prenom : ', ' . $client->client_nom . ' ' . $client->client_prenom;
                    }
                }

                if ($facturation[$key]->client_nom == '') {
                    unset($facturation[$key]);
                    continue;
                }

                $facturation[$key]->lot = array_values(array_filter(array_map(function ($value_lot) use ($datapost) {

                    $lot = [
                        'num_lot' => $value_lot->cliLot_id,
                        'tarrif' => $value_lot->montant_ht_facture,
                        'tarrif_figurant' => $value_lot->montant_ht_figurant,
                        'tarrif_suivant' => '',
                        'tarrif_index' => '',
                        'c_facturation_commentaire' => '',
                        'id_fact_lot' => '',
                    ];

                    $identification_lot = $this->lot->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                        'method' => 'row'
                    ]);

                    $lot['progm_nom'] = $identification_lot->progm_nom ?? '-';

                    $mandat = $this->mandat->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'order_by_columns' => 'mandat_id DESC',
                        'method' => 'row'
                    ]);

                    if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-') {
                        return null;
                    }

                    $lot['mandat_num'] = $mandat->mandat_id;
                    $lot['mandat_signature'] = $mandat->mandat_date_signature;
                    $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                    $lot_facturation_suivant = $this->factu_dossier->get(['clause' => ['dossier_id' => $value_lot->dossier_id, 'cliLot_id' => $value_lot->cliLot_id, 'annee' => $value_lot->annee + 1], 'method' => 'row']);

                    if (!empty($lot_facturation_suivant)) {
                        $lot['tarrif_suivant'] = $lot_facturation_suivant->montant_ht_facture;
                        $lot['tarrif_index'] = $lot_facturation_suivant->c_facturation_indexation;
                        $lot['c_facturation_commentaire'] = $lot_facturation_suivant->c_facturation_commentaire;
                        $lot['id_fact_lot'] = $lot_facturation_suivant->id_fact_lot;
                    }

                    return $lot;
                }, $lot_facturation)));

                if (empty($facturation[$key]->lot)) {
                    unset($facturation[$key]);
                }
            }
            $facturation['facturations'] = $facturation;
            $facturation['anneeChoisi'] = $precedent + 1;

            $view = "Facturation/preparation_facture/a_facturer/model-pdf";
            exportation_pdf_facturation_a_facture($view, $facturation);
        }
    }
    */

    public function listExportPDf()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('FacturationDossier_m', 'factu_dossier');
            $this->load->model('ClientLot_m', 'lot');
            $this->load->model('MandatNew_m', 'mandat');
            $this->load->model('Client_m', 'client');
            $this->load->helper("exportation");

            $datapost = $_POST['data'];
            $anneeChoisi = intval($datapost['annee']);
            $precedent = $anneeChoisi - 1;
            $data['anneeChoisi'] = $anneeChoisi;
            $data['anneeprecedent'] = $precedent;

            $facturation = $this->factu_dossier->obtenir_facturations_liste($precedent);

            foreach ($facturation as $key => $value) {
                $lot_facturation = $this->factu_dossier->get(['clause' => ['dossier_id' => $value->dossier_id, 'annee' => $precedent, 'c_facturation_status' => 1]]);

                $client_id = explode(',', $value->client_id);
                $facturation[$key]->client_nom = '';
                $facturation[$key]->client_prenom = '';

                $like = ($datapost['text_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['text_filtre_proprietaire']] : [];

                foreach ($client_id as $client) {
                    $client = $this->client->get([
                        'clause' => ['client_id' => $client],
                        'like' => $like,
                        'method' => 'row'
                    ]);

                    if (!empty($client)) {
                        $facturation[$key]->client_nom .= $facturation[$key]->client_nom == '' ? $client->client_nom . ' ' . $client->client_prenom : ', ' . $client->client_nom . ' ' . $client->client_prenom;
                    }
                }

                if ($facturation[$key]->client_nom == '') {
                    unset($facturation[$key]);
                    continue;
                }

                $facturation[$key]->lot = array_values(array_filter(array_map(function ($value_lot) use ($datapost) {
                    $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];

                    $lot = [
                        'num_lot' => $value_lot->cliLot_id,
                        'tarrif' => $value_lot->montant_ht_facture,
                        'tarrif_figurant' => $value_lot->montant_ht_figurant,
                        'tarrif_suivant' => '',
                        'tarrif_index' => '',
                        'c_facturation_commentaire' => '',
                        'id_fact_lot' => '',
                    ];

                    $identification_lot = $this->lot->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'like' => $like_progm,
                        'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                        'method' => 'row'
                    ]);

                    $lot['progm_nom'] = $identification_lot->progm_nom ?? '-';

                    $mandat = $this->mandat->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'order_by_columns' => 'mandat_id DESC',
                        'method' => 'row'
                    ]);

                    if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-') {
                        return null;
                    }

                    $lot['mandat_num'] = $mandat->mandat_id;
                    $lot['mandat_signature'] = $mandat->mandat_date_signature;
                    $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                    $lot_facturation_suivant = $this->factu_dossier->get(['clause' => ['dossier_id' => $value_lot->dossier_id, 'cliLot_id' => $value_lot->cliLot_id, 'annee' => $value_lot->annee + 1], 'method' => 'row']);

                    if (!empty($lot_facturation_suivant)) {
                        $lot['tarrif_suivant'] = $lot_facturation_suivant->montant_ht_facture;
                        $lot['tarrif_index'] = $lot_facturation_suivant->c_facturation_indexation;
                        $lot['c_facturation_commentaire'] = $lot_facturation_suivant->c_facturation_commentaire;
                        $lot['id_fact_lot'] = $lot_facturation_suivant->id_fact_lot;
                    }

                    return $lot;
                }, $lot_facturation)));

                if (empty($facturation[$key]->lot)) {
                    unset($facturation[$key]);
                }
            }

            $data['facturations'] = $facturation;
            $view = "Facturation/preparation_facture/a_facturer/model-pdf";
            exportation_pdf_facturation_a_facture($view, $data);
        }
    }
}
