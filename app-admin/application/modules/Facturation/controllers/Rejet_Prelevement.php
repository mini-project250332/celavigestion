<?php

use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Rejet_Prelevement extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    function pageRejet_Prelevement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Reglement_ventilation_m', 'ventilation');
            $this->load->model('Reglement_m', 'reglement');

            $datapost = $_POST['data'];
            $data['reglementdata']  = $this->reglement->get([
                'clause' => ['idc_reglements' => $datapost['Idreglement']],
                'method' => 'row'
            ]);

            $data['montantTroPerçus'] = floatval($this->ventilation->getTropPerçusReglement($datapost['Idreglement']));
            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2]);

            $this->renderComponant("Facturation/paiements/paiement_effectue/page-Rejet_Prelevement", $data);
        }
    }

    function saveRejet_Prelevement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montant_sur_Rejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_Prelevement'])), 2);
                $datamontantPercus = round(floatval($data['montanPercus']), 2);

                $montantReglementsDu = round(floatval($this->reglement->getTotalReglementDu($data['idreglementRembourse'])), 2);
                $resteventilReglementsDu = round(floatval($this->reglement->verifierRejet_PrelevementTropPerçus($data['idreglementRembourse'])), 2);

                $dataSaveRejet_Prelevement = [
                    'mode_reglement' => $data['mode_Rejet_Prelevement'],
                    'date_reglement' => $data['dateRejet_Prelevement'],
                    'id_banque' => $data['banque_celavi_Rejet_Prelevement'],
                    'dossier_id' => $data['dossierRejet_Prelevement'],
                    'montant_total' => $montant_sur_Rejet_Prelevement,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_Rejet_Prelevement'],
                    'reglement_type' => 1,
                    'id_reglementDu' => $data['idreglementRembourse']
                ];
                $request = $this->reglement->save_data($dataSaveRejet_Prelevement);



                if (!empty($request)) {
                    $datamontantPercus == 0 ? $this->Rejet_PrelevementReglementSansMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu) : $this->Rejet_PrelevementReglementAvecMontantPerçus($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $request, $message);
                }
            }
            echo json_encode($retour);
        }
    }



    private function Rejet_PrelevementReglementSansMontantPerçus($data, $montantReglementsDu, $resteventilReglementsDu)
    {

        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');
        $this->load->model('Facture_m', 'facture_annuelle');

        $montantRejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_Prelevement'])), 2);

        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture !=' => null],
        ]);


        $reglementData  = $this->reglement->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse']],
            'method' => 'row'
        ]);




        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);
        $montantReglement = round(floatval($reglementData->montant_total) - ($montantReglementsDu + $montantRejet_Prelevement), 2);


        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRejet_Prelevement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? round((floatval($reglementData->montant_total) - $montantReglementsDu), 2) : round($montantRejet_Prelevement, 2),
                'idc_reglements' => $last_reglement->idc_reglements,
            ];

            if ($resteventilReglementsDu > 0) {
                $saveVentil['montant'] = $montantRejet_Prelevement;
            }
            $this->ventilation->save_data($saveVentil);

            $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $value->id_facture], 'method' => 'row']);
            $savefacture = ['fact_etatpaiement' => 0];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);
        }



        if ($montantReglement <= 0) {
            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRejet_Prelevement'],
                    'id_facture' => NULL,
                    'montant' => ($resteventilReglementsDu > 0) ? $montantRejet_Prelevement : abs($montantReglement),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }


    private function Rejet_PrelevementReglementAvecMontantPerçus($data)
    {

        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Reglement_m', 'reglement');

        $montantRejet_Prelevement = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_Prelevement'])), 2);
        $ventilationReglementsdu = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture !=' => null],
        ]);


        $venitlationSupprimer  = $this->ventilation->get([
            'clause' => ['idc_reglements' => $data['idreglementRembourse'], 'id_facture' => null],
            'method' => 'row'
        ]);



        $montantperçus = round(floatval($this->ventilation->getTropPerçusReglement($data['idreglementRembourse'])), 2);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);
        $montantReglement = round(floatval($montantperçus -  $montantRejet_Prelevement), 2);


        foreach ($ventilationReglementsdu as $key => $value) {
            $saveVentil = [
                'dossier_id' => $data['dossierRejet_Prelevement'],
                'id_facture' => $value->id_facture,
                'montant' => ($montantReglement < 0) ? round(floatval($montantperçus), 2) : round($montantRejet_Prelevement, 2),
                'idc_reglements' => $last_reglement->idc_reglements,
            ];
            $this->ventilation->save_data($saveVentil);
        }


        if ($montantReglement <= 0) {

            $clause_delete = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->delete_data($clause_delete);

            if ($montantReglement < 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossierRejet_Prelevement'],
                    'id_facture' => NULL,
                    'montant' => abs($montantReglement),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        } else {

            $date_update = array('montant' => $montantReglement);
            $clauseventil = array('idc_reglementVentilation' => $venitlationSupprimer->idc_reglementVentilation);
            $this->ventilation->save_data($date_update, $clauseventil);
        }
    }


    function pageRejet_PrelevementAvoir()
    {

        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Mode_paiement_fact_m', 'mode_paiement');
            $this->load->model('Banque_m', 'banque');
            $this->load->model('Facture_m', 'facture_annuelle');
            $data['banque'] = $this->banque->get(['clause' => ['banque_etat' => 1]]);
            $data['mode_paiement'] = $this->mode_paiement->get_mode_paieReglement([2, 4]);
            $data['facture'] = $this->facture_annuelle->getfactureAnnuelleByid($_POST['data']['fact_id']);
            $this->renderComponant("Facturation/paiements/facture_impaye/pageRejet_PrelevementAvoir", $data);
        }
    }



    //Enregistrer Rejet_Prelevement Avoir
    function saveRejet_PrelevementAvoir()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Reglement_m', 'reglement');
            $control = $this->control_data($_POST['data']);
            $retour = retour(null, null, null, $control['information']);

            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $montantreglment = floatval(str_replace(",", ".", $data['montant_sur_Rejet_PrelevementsAvoir']));
                $saveReglement = [
                    'mode_reglement' => $data['mode_Rejet_PrelevementsAvoir'],
                    'date_reglement' => $data['dateRejet_PrelevementsAvoir'],
                    'id_banque' => $data['banque_celavi_Rejet_PrelevementsAvoir'],
                    'dossier_id' => $data['dossier_id_reglmentAvoir'],
                    'montant_total' => $montantreglment,
                    'date_action' => date('Y-m-d H:i:s'),
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'libelle_reglement' => $data['libelle_Rejet_PrelevementsAvoir'],
                    'reglement_type' => 1,
                ];
                $request = $this->reglement->save_data($saveReglement);

                if (!empty($request)) {
                    $this->GererVentilationAvoir($data);
                    $message = "Enregistrement réussie avec succès.";
                    $retour = retour(true, "success", $request, $message);
                }
            }
            echo json_encode($retour);
        }
    }

    private function GererVentilationAvoir($data)
    {
        $this->load->model('Reglement_ventilation_m', 'ventilation');
        $this->load->model('Facture_m', 'facture_annuelle');
        $this->load->model('Reglement_m', 'reglement');
        $montantreglment = round(floatval(str_replace(",", ".", $data['montant_sur_Rejet_PrelevementsAvoir'])), 2);
        $totalVentilation = floatval($this->ventilation->getTotalVentile($data['fact_id_Rejet_PrelevementsAvoir']));
        $totalVentilation = round($totalVentilation, 2);
        $facture = $this->facture_annuelle->get(['clause' => ['fact_id' => $data['fact_id_Rejet_PrelevementsAvoir']], 'method' => 'row']);
        $last_reglement = $this->reglement->get(['order_by_columns' => 'idc_reglements DESC', 'method' => 'row']);

        $montantfacture = round(floatval($facture->fact_montantttc) + ($totalVentilation + $montantreglment), 2);

        $saveVentil = [
            'dossier_id' => $data['dossier_id_reglmentAvoir'],
            'id_facture' => $facture->fact_id,
            'montant' => ($montantfacture > 0) ? round((floatval($facture->fact_montantttc) + $totalVentilation), 2) : round($montantreglment, 2),
            'idc_reglements' => $last_reglement->idc_reglements,
        ];
        $this->ventilation->save_data($saveVentil);

        if ($montantfacture >= 0) {
            $savefacture = ['fact_etatpaiement' => 1];
            $clause = ['fact_id' => $facture->fact_id];
            $this->facture_annuelle->save_data($savefacture, $clause);

            if ($montantfacture > 0) {
                $saveVentil = [
                    'dossier_id' => $data['dossier_id_reglmentAvoir'],
                    'id_facture' => NULL,
                    'montant' => abs($montantfacture),
                    'idc_reglements' => $last_reglement->idc_reglements,
                ];
                $this->ventilation->save_data($saveVentil);
            }
        }
    }
}
