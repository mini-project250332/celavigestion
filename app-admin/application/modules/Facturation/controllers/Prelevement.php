<?php

use phpDocumentor\Reflection\Types\This;

class Prelevement extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    function prelevement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Facturation/fichierSepa/content-fichierSepa");
            }
        }
    }

    function liste_prelevement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Prelevement_m', 'prelevement');
                $datapost = $_POST['data'];
                $rows_limit = intval($datapost['row_data']);
                $pagination_data = explode('-', $datapost['pagination_page']);
                $data['prelevement'] = $this->prelevement->getPrelevement($pagination_data);
                $totalPrelevement = $this->prelevement->getListlotTotalPrelevement();
                $data['total'] = count($totalPrelevement) > 1 ? count($totalPrelevement) : 0;
                $data["li_pagination"] = count($totalPrelevement) < (intval($rows_limit) + 1) ? [0] : range(0, count($totalPrelevement), intval($rows_limit));
                $data["active_li_pagination"] = $pagination_data[0];
                $this->renderComponant("Facturation/fichierSepa/table-prelevement", $data);
            }
        }
    }

    public function telecharger_fichier()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('DocumentPrelevement_m', 'doc_prelevement');
            $datapost = $_POST['data'];
            $doc = $this->doc_prelevement->getDocprelevement($datapost);
            $this->load->helper("exportation");
            $fileLinks = array();
            if (!empty($doc)) {
                $fileLinks = array_map(function ($value) {
                    return $value->doc_prelevement_path;
                }, $doc);
                $zip = new ZipArchive();
                $filename = "Prelevement" . date('Y-m-d');
                $url_path = APPPATH . "../../documents/Archive_xml/";
                $zipchemin = $url_path . "/" . $filename . '.zip';
                $url_doc = "../documents/Archive_xml/" . $filename . '.zip';

                if (!makeDirPath($url_path)) {
                    error_log("Failed to create directory: " . $url_path);
                    return false;
                }

                if ($zip->open($zipchemin, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
                    foreach ($fileLinks as $file_url) {
                        $file_name = basename($file_url);
                        $file_content = file_get_contents($file_url);

                        if ($file_content === FALSE) {
                            $error_message = error_get_last();
                            echo "Failed to retrieve content for file $file_url. Error: " . $error_message['message'];
                            return;
                        }
                        $zip->addFromString($file_name, $file_content);
                    }
                    $zip->close();

                    $url = str_replace('\\', '/', $url_doc);
                    echo $url;
                }
            }
        }
    }

    function detailsPrelevement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Prelevement_m', 'prelevement');

            $datapost = $_POST['data'];
            $data['prelevement'] = $prelevement = $this->prelevement->get([
                'clause' => ['idc_prelevement' => $datapost['idc_prelevement']],
                'join' => [
                    'c_banque' => 'c_banque.banque_id = c_prelevement.id_banque',
                ],
                'method' => 'row'
            ]);

            $data['facture'] = $this->prelevement->getPrelevementFacturebyId($prelevement->idc_prelevement);

            $this->renderComponant("Facturation/fichierSepa/details-prelevement", $data);
        }
    }

    function regenererFichierXml()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la régéneration",
                );
                $this->load->helper("exportation");
                $this->load->model('Reglement_m', 'reglement');
                $this->load->model('Ics_m', 'ics');
                $this->load->model('Prelevement_m', 'prelevement');
                $this->load->model('DocumentPrelevement_m', 'docprelevement');

                $regeneration = false;

                $datapost = decrypt_service($_POST["data"]);
                $Infoprelevement = $this->prelevement->getPrelevementAvecId($datapost['prelevement_id']);

                $Infoprelevement = $this->prelevement->getPrelevementAvecId($datapost['prelevement_id']);

                if ($datapost['action'] == "demande") {
                    $text = ($Infoprelevement && isset($Infoprelevement[0]->doc_prelevement_nom))
                        ? "Voulez-vous vraiment régénérer le fichier " . $Infoprelevement[0]->doc_prelevement_nom . ".xml ? "
                        : "Le fichier n'a pas encore été généré.";

                    $retour = array(
                        'status' => 200,
                        'action' => $datapost['action'],
                        'data' => array(
                            'idprelevement' => $datapost['prelevement_id'],
                            'title' => "Regénération du fichier Xml",
                            'text' => $text,
                            'btnConfirm' => "Régénérer le fichier",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($datapost['action'] == "confirm") {

                    $prelevementData = $this->prelevement->get([
                        'clause' => ['idc_prelevement' => $datapost['prelevement_id']],
                        'join' => [
                            'c_banque' => 'c_banque.banque_id = c_prelevement.id_banque',
                        ],
                    ]);

                    $checkDocPrelevement =  $this->docprelevement->get([
                        'clause' => ['doc_prelevement_prelevementID' => $datapost['prelevement_id']],
                        'method' => 'row'
                    ]);


                    if (!empty($checkDocPrelevement)) {
                        $regeneration = true;
                    }


                    $reglementData = $this->reglement->getPrelevementParIdPrelevement($datapost['prelevement_id']);


                    if (!empty($prelevementData) && !empty($reglementData)) {



                        $data = [
                            'prelevement' => $prelevementData,
                            'reglement' => $reglementData,
                            'Idreglmenet' => "",
                            'ics' => $this->ics->get([]),
                            'regeneration' => $regeneration,
                            'docprelevement' => $checkDocPrelevement ?? '',
                        ];




                        $exportXml = export_xml($data);
                        $retour = array(
                            'status' => 200,
                            'action' => $datapost['action'],
                            'data' => array('id' => $datapost['prelevement_id']),
                            'message' => "",
                            'url' => $exportXml,
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
