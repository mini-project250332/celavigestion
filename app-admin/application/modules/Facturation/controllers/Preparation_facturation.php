<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class Preparation_facturation extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Facturation";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function page_preparation_facture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FacturationDossier_m', 'factu_dossier');
                $this->load->model('Program_m', 'programme');
                $programmes2 = $this->programme->liste_lotplus_cinqlot();
                $data['programmes'] = $programmes2;
                $this->renderComponant("Facturation/preparation_facture/preparation/contenu-prepa-facturation", $data);
            }
        }
    }

    public function filtre_preparation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FacturationDossier_m', 'factu_dossier');
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('MandatNew_m', 'mandat');
                $this->load->model('Client_m', 'client');

                $datapost = $_POST['data'];
                $anneeChoisi = intval($datapost['annee']);
                $precedent = $anneeChoisi - 1;
                $data['anneeChoisi'] = $anneeChoisi;
                $data['anneeprecedent'] = $precedent;

                $clause['c_facturation_lot.annee'] = $precedent;
                $clause['c_facturation_lot.c_facturation_status'] = 0;

                $dossier = $this->dossier->get(
                    array(
                        'columns' => array('dossier_id', 'client_id'),
                        'clause' => array(
                            'dossier_etat' => 1
                        )
                    )
                );

                $facturation = $this->factu_dossier->get(
                    array(
                        'columns' => array(
                            'c_utilisateur.util_prenom',
                            'c_utilisateur.util_nom',
                            'c_facturation_lot.dossier_id',
                            'c_facturation_lot.mode_paiement',
                            'c_mode_paiement_facturation.libelle',
                            'c_mode_paiement_facturation.libelle',
                            'SUM(c_facturation_lot.montant_ht_facture) AS sommeht',
                            'c_dossier.client_id'
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                            'c_mode_paiement_facturation' => 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        ),
                        "group_by_columns" => "c_dossier.dossier_id",
                        'join_orientation' => 'left'
                    )
                );

                $indexedArray1  = [];
                $indexedArray2 = [];

                foreach ($dossier as $item) {
                    $indexedArray1[$item->dossier_id] = $item;
                }

                foreach ($facturation as $item) {
                    $indexedArray2[$item->dossier_id] = $item;
                }

                $resultArray = [];

                foreach ($indexedArray1 as $dossier_id => $item1) {
                    $mergedItem = new stdClass();
                    $mergedItem->client_id = $item1->client_id;
                    $mergedItem->dossier_id = $dossier_id;
                    $mergedItem->mode_paiement = $indexedArray2[$dossier_id]->mode_paiement ?? null;
                    $mergedItem->libelle = $indexedArray2[$dossier_id]->libelle ?? "Non renseigné";
                    $mergedItem->sommeht = $indexedArray2[$dossier_id]->sommeht ?? null;
                    $mergedItem->util_prenom = $indexedArray2[$dossier_id]->util_prenom ?? null;
                    $mergedItem->util_nom = $indexedArray2[$dossier_id]->util_nom ?? null;
                    $resultArray[] = $mergedItem;
                }

                $facturation = $resultArray;
                foreach ($facturation as $key => $value) {
                    $lot_facturation = $this->factu_dossier->get(['clause' => ['dossier_id' => $value->dossier_id, 'annee' => $precedent, 'c_facturation_status' => 0]]);

                    $client_id = explode(',', $value->client_id);
                    $facturation[$key]->client_nom = '';
                    $facturation[$key]->client_prenom = '';

                    $like = ($datapost['text_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['text_filtre_proprietaire']] : [];


                    foreach ($client_id as $client) {
                        $client = $this->client->get([
                            'clause' => ['client_id' => $client],
                            'like' => $like,
                            'method' => 'row'
                        ]);

                        if (!empty($client)) {
                            $facturation[$key]->client_nom .= $facturation[$key]->client_nom == '' ? $client->client_nom . ' ' . $client->client_prenom : ', ' . $client->client_nom . ' ' . $client->client_prenom;
                        }
                    }

                    if (!empty($client_id)) {
                        $facturation[$key]->client_id = $client_id[0];
                    }

                    if ($facturation[$key]->client_nom == '') {
                        unset($facturation[$key]);
                        continue;
                    }

                    if (!empty($lot_facturation)) {
                        $facturation[$key]->lot = [];

                        foreach ($lot_facturation as $value_lot) {
                            $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];
                            $clauseprogramme = ['cliLot_id' => $value_lot->cliLot_id];

                            if ($datapost['program_id'] != 0) {
                                $clauseprogramme['c_programme.progm_id'] = $datapost['program_id'];
                            }

                            $lot = [
                                'num_lot' => $value_lot->cliLot_id,
                                'tarrif' => $value_lot->montant_ht_facture,
                                'tarrif_figurant' => $value_lot->montant_ht_figurant,
                                'tarrif_suivant' => '',
                                'tarrif_index' => '',
                                'c_facturation_commentaire' => '',
                                'id_fact_lot' => '',
                            ];

                            $identification_lot = $this->lot->get([
                                'clause' => $clauseprogramme,
                                'like' => $like_progm,
                                'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                                'method' => 'row'
                            ]);

                            $lot['progm_nom'] = $identification_lot->progm_nom ?? '-';
                            $lot['progm_id'] = $identification_lot->progm_id ?? '-';

                            $mandat = $this->mandat->get([
                                'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                                'order_by_columns' => 'mandat_id DESC',
                                'method' => 'row'
                            ]);

                            if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-' || $mandat->etat_mandat_id == 5 || $mandat->etat_mandat_id == 7) {
                                continue;
                            }

                            if ($mandat->mandat_date_fin != NULL && date('Y-m-d', strtotime($mandat->mandat_date_fin)) < date(('Y-m-d'))) {
                                continue;
                            }

                            $lot['mandat_num'] = $mandat->mandat_id;
                            $lot['mandat_signature'] = $mandat->mandat_date_signature;
                            $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                            $lot_facturation_suivant = $this->factu_dossier->get([
                                'clause' => ['dossier_id' => $value_lot->dossier_id, 'cliLot_id' => $value_lot->cliLot_id, 'annee' => $value_lot->annee + 1],
                                'method' => 'row'
                            ]);

                            if (!empty($lot_facturation_suivant)) {
                                $lot['tarrif_suivant'] = $lot_facturation_suivant->montant_ht_facture;
                                $lot['tarrif_index'] = $lot_facturation_suivant->c_facturation_indexation;
                                $lot['c_facturation_commentaire'] = $lot_facturation_suivant->c_facturation_commentaire;
                                $lot['id_fact_lot'] = $lot_facturation_suivant->id_fact_lot;
                            }

                            $facturation[$key]->lot[] = $lot;
                        }
                    } else {
                        $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];
                        $clauseprogramme2 = ['dossier_id' => $value->dossier_id];

                        if ($datapost['program_id'] != 0) {
                            $clauseprogramme2['c_programme.progm_id'] = $datapost['program_id'];
                        }

                        $identification_lot = $this->lot->get([
                            'clause' => $clauseprogramme2,
                            'like' => $like_progm,
                            'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                        ]);

                        if (empty($identification_lot)) {
                            unset($facturation[$key]);
                            continue;
                        }

                        foreach ($identification_lot as $key_lot => $value_lot) {
                            $lot = [
                                'num_lot' => $value_lot->cliLot_id,
                                'tarrif' => 0,
                                'tarrif_figurant' => 0,
                                'tarrif_suivant' => 0,
                                'tarrif_index' => 0,
                                'c_facturation_commentaire' => '',
                                'id_fact_lot' => '',
                            ];

                            $lot['progm_nom'] = $value_lot->progm_nom ?? '-';
                            $lot['progm_id'] = $value_lot->progm_id ?? '-';

                            $mandat = $this->mandat->get([
                                'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                                'order_by_columns' => 'mandat_id DESC',
                                'method' => 'row'
                            ]);

                            if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-' || $mandat->etat_mandat_id == 5 || $mandat->etat_mandat_id == 7) {
                                continue;
                            }

                            if ($mandat->mandat_date_fin != NULL && date('Y-m-d', strtotime($mandat->mandat_date_fin)) < date(('Y-m-d'))) {
                                continue;
                            }

                            $lot['mandat_num'] = $mandat->mandat_id;
                            $lot['mandat_signature'] = $mandat->mandat_date_signature;
                            $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                            $facturation[$key]->lot[] = $lot;
                        }
                    }

                    if (empty($facturation[$key]->lot)) {
                        unset($facturation[$key]);
                    }
                }

                $facturation_lot = $this->factu_dossier->getallstatus($precedent);

                $facturation = array_filter($facturation, function ($item) use ($facturation_lot) {
                    $item->lot = array_filter($item->lot, function ($lot) use ($facturation_lot) {
                        return !in_array($lot['num_lot'], array_column($facturation_lot, 'cliLot_id'));
                    });
                    return !empty($item->lot);
                });

                $facturation = array_map(function ($item) {
                    $item->lot = array_values($item->lot);
                    return $item;
                }, $facturation);

                $data['facturations'] = $facturation;

                $this->renderComponant("Facturation/preparation_facture/preparation/contenu-filtre-facturation", $data);
            }
        }
    }




    public function save_facturation()
    {
        if (!is_ajax() || $_SERVER['REQUEST_METHOD'] !== 'POST') {
            return;
        }

        $datapost = $_POST['data'];
        $this->load->model('FacturationDossier_m', 'factu_dossier');

        $dossier_id = $this->getIdDossier($datapost['cliLot_id']);
        $mode_paiement = $this->getIdPaiementr($datapost['cliLot_id']);
        $anneeprecedent = intval($datapost['annee']) - 1;

        $checkfacturationPrecedent = $this->checkfacturation($datapost['cliLot_id'], $anneeprecedent, $dossier_id);

        if ($checkfacturationPrecedent) {
            $data = [
                'cliLot_id' => $datapost['cliLot_id'],
                'mode_paiement' => $mode_paiement,
                'montant_ht_facture' => 0,
                'montant_ht_figurant' => 0,
                'dossier_id' => $dossier_id,
                'annee' => $anneeprecedent,
                'c_facturation_indexation' => 0,
                'c_facturation_commentaire' => ''
            ];

            $this->factu_dossier->save_data($data);
        }

        if ($datapost['validation'] == true) {
            $clause_fac = [
                'cliLot_id' => $datapost['cliLot_id'],
                'dossier_id' => $dossier_id,
                'annee' => $anneeprecedent
            ];
            $data = [
                'c_facturation_status' => $datapost['c_facturation_status']
            ];

            $this->factu_dossier->save_data($data, $clause_fac);
        }

        $checkfacturation = $this->checkfacturation($datapost['cliLot_id'], $datapost['annee'], $dossier_id);
        $data = [
            'cliLot_id' => $datapost['cliLot_id'],
            'mode_paiement' => $mode_paiement,
            'montant_ht_facture' => $datapost['tarrif'],
            'montant_ht_figurant' => $datapost['tarrif'],
            'dossier_id' => $dossier_id,
            'annee' => $datapost['annee'],
            'c_facturation_indexation' => $datapost['indexation'],
            'c_facturation_commentaire' => $datapost['commentaire']
        ];

        if ($checkfacturation) {
            $this->factu_dossier->save_data($data);
            echo json_encode(['message' => 'Enregistrement effectué']);
        } else {
            $clause_fac = [
                'cliLot_id' => $datapost['cliLot_id'],
                'dossier_id' => $dossier_id,
                'annee' => $datapost['annee']
            ];

            $this->factu_dossier->save_data($data, $clause_fac);
            echo json_encode(['message' => 'Enregistrement effectué']);
        }
    }

    function checkfacturation($cliloId, $annee, $dossierId)
    {

        $this->load->model('FacturationDossier_m', 'factu_dossier');
        $facturation = $this->factu_dossier->get(
            array(
                'clause' => array('cliLot_id' => $cliloId, 'annee' => $annee, 'dossier_id' => $dossierId),
            )
        );

        return (empty($facturation)) ? true : false;
    }

    function getIdDossier($idlot)
    {
        $this->load->model('ClientLot_m', 'lotclient');

        $Idossier = $this->lotclient->get(
            array(
                'clause' => array('cliLot_id' => $idlot),
            )
        );

        return $Idossier[0]->dossier_id;
    }

    function getIdPaiementr($idlot)
    {
        $this->load->model('FacturationDossier_m', 'facture_dossier');
        $paie = $this->facture_dossier->get(
            array(
                'clause' => array('cliLot_id' => $idlot),
            )
        );
        return (empty($paie)) ? 1 : $paie[0]->mode_paiement;
    }


    /***** ****/


    function listExportPDf()
    {
        $this->load->model('FacturationDossier_m', 'factu_dossier');
        $this->load->model('Dossier_m', 'dossier');
        $this->load->model('ClientLot_m', 'lot');
        $this->load->model('MandatNew_m', 'mandat');
        $this->load->model('Client_m', 'client');
        $this->load->helper("exportation");

        $datapost = $_POST['data'];
        $anneeChoisi = intval($datapost['annee']);
        $precedent = $anneeChoisi - 1;
        $data['anneeChoisi'] = $anneeChoisi;
        $data['anneeprecedent'] = $precedent;

        $clause['c_facturation_lot.annee'] = $precedent;
        $clause['c_facturation_lot.c_facturation_status'] = 0;

        $dossier = $this->dossier->get(
            array(
                'columns' => array('dossier_id', 'client_id'),
                'clause' => array(
                    'dossier_etat' => 1
                )
            )
        );

        $facturation = $this->factu_dossier->get(
            array(
                'columns' => array(
                    'c_facturation_lot.dossier_id',
                    'c_facturation_lot.mode_paiement',
                    'c_mode_paiement_facturation.libelle',
                    'c_mode_paiement_facturation.libelle',
                    'SUM(c_facturation_lot.montant_ht_facture) AS sommeht',
                    'c_dossier.client_id'
                ),
                'clause' => $clause,
                'join' => array(
                    'c_dossier' => 'c_dossier.dossier_id = c_facturation_lot.dossier_id',
                    'c_mode_paiement_facturation' => 'c_mode_paiement_facturation.id_mode_paie = c_facturation_lot.mode_paiement'
                ),
                "group_by_columns" => "c_dossier.dossier_id",
            )
        );

        $indexedArray1  = [];
        $indexedArray2 = [];

        foreach ($dossier as $item) {
            $indexedArray1[$item->dossier_id] = $item;
        }

        foreach ($facturation as $item) {
            $indexedArray2[$item->dossier_id] = $item;
        }

        $resultArray = [];

        foreach ($indexedArray1 as $dossier_id => $item1) {
            $mergedItem = new stdClass();
            $mergedItem->client_id = $item1->client_id;
            $mergedItem->dossier_id = $dossier_id;
            $mergedItem->mode_paiement = $indexedArray2[$dossier_id]->mode_paiement ?? null;
            $mergedItem->libelle = $indexedArray2[$dossier_id]->libelle ?? "Non renseigné";
            $mergedItem->sommeht = $indexedArray2[$dossier_id]->sommeht ?? null;

            $resultArray[] = $mergedItem;
        }

        $facturation = $resultArray;

        foreach ($facturation as $key => $value) {
            $lot_facturation = $this->factu_dossier->get(['clause' => ['dossier_id' => $value->dossier_id, 'annee' => $precedent, 'c_facturation_status' => 0]]);

            $client_id = explode(',', $value->client_id);
            $facturation[$key]->client_nom = '';
            $facturation[$key]->client_prenom = '';

            $like = ($datapost['text_filtre_proprietaire'] != '') ? ['client_nom' => $datapost['text_filtre_proprietaire']] : [];


            foreach ($client_id as $client) {
                $client = $this->client->get([
                    'clause' => ['client_id' => $client],
                    'like' => $like,
                    'method' => 'row'
                ]);

                if (!empty($client)) {
                    $facturation[$key]->client_nom .= $facturation[$key]->client_nom == '' ? $client->client_nom . ' ' . $client->client_prenom : ', ' . $client->client_nom . ' ' . $client->client_prenom;
                }
            }

            if (!empty($client_id)) {
                $facturation[$key]->client_id = $client_id[0];
            }

            if ($facturation[$key]->client_nom == '') {
                unset($facturation[$key]);
                continue;
            }

            if (!empty($lot_facturation)) {
                $facturation[$key]->lot = [];

                foreach ($lot_facturation as $value_lot) {
                    $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];
                    $clauseprogramme = ['cliLot_id' => $value_lot->cliLot_id];

                    if ($datapost['program_id'] != 0) {
                        $clauseprogramme['c_programme.progm_id'] = $datapost['program_id'];
                    }

                    $lot = [
                        'num_lot' => $value_lot->cliLot_id,
                        'tarrif' => $value_lot->montant_ht_facture,
                        'tarrif_figurant' => $value_lot->montant_ht_figurant,
                        'tarrif_suivant' => '',
                        'tarrif_index' => '',
                        'c_facturation_commentaire' => '',
                        'id_fact_lot' => '',
                    ];

                    $identification_lot = $this->lot->get([
                        'clause' => $clauseprogramme,
                        'like' => $like_progm,
                        'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                        'method' => 'row'
                    ]);

                    $lot['progm_nom'] = $identification_lot->progm_nom ?? '-';
                    $lot['progm_id'] = $identification_lot->progm_id ?? '-';

                    $mandat = $this->mandat->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'order_by_columns' => 'mandat_id DESC',
                        'method' => 'row'
                    ]);

                    if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-' || $mandat->etat_mandat_id == 5 || $mandat->etat_mandat_id == 7) {
                        continue;
                    }

                    if ($mandat->mandat_date_fin != NULL && date('Y-m-d', strtotime($mandat->mandat_date_fin)) < date(('Y-m-d'))) {
                        continue;
                    }

                    $lot['mandat_num'] = $mandat->mandat_id;
                    $lot['mandat_signature'] = $mandat->mandat_date_signature;
                    $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                    $lot_facturation_suivant = $this->factu_dossier->get([
                        'clause' => ['dossier_id' => $value_lot->dossier_id, 'cliLot_id' => $value_lot->cliLot_id, 'annee' => $value_lot->annee + 1],
                        'method' => 'row'
                    ]);

                    if (!empty($lot_facturation_suivant)) {
                        $lot['tarrif_suivant'] = $lot_facturation_suivant->montant_ht_facture;
                        $lot['tarrif_index'] = $lot_facturation_suivant->c_facturation_indexation;
                        $lot['c_facturation_commentaire'] = $lot_facturation_suivant->c_facturation_commentaire;
                        $lot['id_fact_lot'] = $lot_facturation_suivant->id_fact_lot;
                    }

                    $facturation[$key]->lot[] = $lot;
                }
            } else {
                $like_progm = ($datapost['text_filtre_programme'] != '') ? ['progm_nom' => $datapost['text_filtre_programme']] : [];
                $clauseprogramme2 = ['dossier_id' => $value->dossier_id];

                if ($datapost['program_id'] != 0) {
                    $clauseprogramme2['c_programme.progm_id'] = $datapost['program_id'];
                }

                $identification_lot = $this->lot->get([
                    'clause' => $clauseprogramme2,
                    'like' => $like_progm,
                    'join' => ['c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'],
                ]);

                if (empty($identification_lot)) {
                    unset($facturation[$key]);
                    continue;
                }

                foreach ($identification_lot as $key_lot => $value_lot) {
                    $lot = [
                        'num_lot' => $value_lot->cliLot_id,
                        'tarrif' => 0,
                        'tarrif_figurant' => 0,
                        'tarrif_suivant' => 0,
                        'tarrif_index' => 0,
                        'c_facturation_commentaire' => '',
                        'id_fact_lot' => '',
                    ];

                    $lot['progm_nom'] = $value_lot->progm_nom ?? '-';
                    $lot['progm_id'] = $value_lot->progm_id ?? '-';

                    $mandat = $this->mandat->get([
                        'clause' => ['cliLot_id' => $value_lot->cliLot_id],
                        'order_by_columns' => 'mandat_id DESC',
                        'method' => 'row'
                    ]);

                    if (empty($mandat) || $mandat->mandat_date_signature === null || $lot['progm_nom'] == '-' || $mandat->etat_mandat_id == 5 || $mandat->etat_mandat_id == 7) {
                        continue;
                    }

                    if ($mandat->mandat_date_fin != NULL && date('Y-m-d', strtotime($mandat->mandat_date_fin)) < date(('Y-m-d'))) {
                        continue;
                    }

                    $lot['mandat_num'] = $mandat->mandat_id;
                    $lot['mandat_signature'] = $mandat->mandat_date_signature;
                    $lot['mandat_montant_ht'] = $mandat->mandat_montant_ht;

                    $facturation[$key]->lot[] = $lot;
                }
            }

            if (empty($facturation[$key]->lot)) {
                unset($facturation[$key]);
            }
        }

        $facturation_lot = $this->factu_dossier->getallstatus($precedent);

        $facturation = array_filter($facturation, function ($item) use ($facturation_lot) {
            $item->lot = array_filter($item->lot, function ($lot) use ($facturation_lot) {
                return !in_array($lot['num_lot'], array_column($facturation_lot, 'cliLot_id'));
            });
            return !empty($item->lot);
        });

        $facturation = array_map(function ($item) {
            $item->lot = array_values($item->lot);
            return $item;
        }, $facturation);

        $data['facturations'] = $facturation;
        $view = "Facturation/preparation_facture/preparation/model-pdf";
        exportation_pdf_facturation_a_preparer($view, $data);
    }
}
