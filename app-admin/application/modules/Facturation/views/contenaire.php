<div class="container-app vue_limit_access">
    <?php $this->load->view($sous_module); ?>
</div>

<div class="modal fade" id="myModalApercufacture" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content" id="modalMainApercu">

        </div>
    </div>
</div>

<div class="modal fade" id="myModalValideFacture" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-md  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="modalMain">

        </div>
    </div>
</div>

<div class="modal fade" id="myModalmodereglement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-md  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="modalMainRegle">

        </div>
    </div>
</div>

<div class="modal fade" id="myModaldetailsPrelevement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="details-prelevement">

        </div>
    </div>
</div>


<div class="modal fade" id="myModalValiderFactureAvoir" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="modalMainFactureAvoir">

        </div>
    </div>
</div>


<div class="modal fade" id="myModaldetailsReglement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content" id="details-reglement">

        </div>
    </div>
</div>

<script>
    access_right([1, 3, 4, 5], "*");
</script>