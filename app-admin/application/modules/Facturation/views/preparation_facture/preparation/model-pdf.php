<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
	    @page {
	        margin: 2%;
	    }

	    table {
	        width: 100%;
	        border-collapse: collapse;
	    }

	    th,
	    td {
	        border: 1px solid black;
	        padding: 5px;
	        text-align: center;
	        font-size: 10px;
	    }

	    .nowrap {
	        white-space: nowrap;
	    }

	    h2 {
	        text-align: center;
	    }

	    .title-pdf h1{
	    	text-align: center;
	    	font-size: 14px;
	    	max-resolution: 10px;
	    }
	</style>
</head>
<body>

	<?php
		function tarrif($index, $tarrifdefault){
		    $pourcentage  = ($tarrifdefault * $index) / 100;
		    $total = $tarrifdefault + $pourcentage;
		    return $total;
		} 
	?>

	<div class="title-pdf">
		<h1> Factures à préparer </h1>
	</div>

	<table >
	    <thead>
	        <tr>
	            <th rowspan="2">Dossier</th>
	            <th rowspan="2">Propriétaire</th>
	            <th rowspan="2">Mode de paiements</th>
	            <th colspan="8">Lot</th>
	            <th rowspan="2">Total HT</th>
	            <th rowspan="2">Commentaire interne</th>
	        </tr>
	        <tr class="text-center">
	            <th>Programme</th>
	            <th>N°Mandat</th>
	            <th>Date signature Mandat</th>
	            <th>Tarif HT figurant sur Mandat</th>

	            <th> Facturé HT <?= $anneeprecedent ?></th>
	            <th>Ecart Mandat - Facturé HT <?= $anneeprecedent ?></th>
	            <th>Indexation</th>
	            <th >Proposition HT <?= $anneeChoisi ?> </th>
	        </tr>
	    </thead>

	    <tbody>
	        <?php
		        $somme_tarif_lot = 0;
		        $somme_mandat_lot = 0;
		        $somme_tarrif_suivant = 0;

		        $somme_proposition = 0;
		        $somme_total = 0;
	        ?>
	        <?php foreach ($facturations as $key => $facturation) : ?>
	            
	            <?php $countsLots = count($facturation->lot); ?>
	            
	            <?php if ($countsLots > 1) : ?>
	                
	                <?php $lots = array_values($facturation->lot); ?>

	                <tr class="dossier_<?= $facturation->dossier_id ?>">

	                    <td rowspan="<?= $countsLots; ?>">
	                    	<?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?>
	                    </td>
	                    <td rowspan="<?= $countsLots; ?>">
	                    	<?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?>
	                    </td>
	                    <td rowspan="<?= $countsLots; ?>">
	                        <?= $facturation->libelle ?>
	                    </td>
	                    <?php 
	                    	$ht_total = array_reduce($lots, function ($carry, $lot) {
	                    		$tarif = ((2 * floatval($lot['tarrif_figurant'])) / 100) + floatval($lot['tarrif_figurant']);
	                    		$calcul = ($lot['tarrif_suivant'] == '') ?  $tarif : floatval($lot['tarrif_suivant']);
							    return $carry + floatval($calcul);
							}, 0);

		        			$somme_total += $ht_total;
	                    ?>

	                    <?php foreach ($lots as $key => $lot) : ?>

	                    	<?php if ($key !== 0) : ?>
	                            <tr>
                            <?php endif; ?>

	                        <td>
	                            <?= $lot['progm_nom'] ?>
	                        </td>
	                        <td><?= $lot['mandat_num'] ?></td>
	                        <td ><?= date('d/m/Y', strtotime($lot['mandat_signature'])) ?></td>
	                        <td ><?= format_number2_($lot['tarrif_figurant']) ?> €</td>
	                        <td><?= format_number2_($lot['tarrif']) ?> €</td>
	                        <td ><?= format_number2_($lot['tarrif_figurant'] - $lot['tarrif']) ?> €</td>
	                        <td>
	                        	<?= ($lot['tarrif_index'] == '') ? format_number2_(2) : format_number2_($lot['tarrif_index'])  ?> %
	                        </td>

	                        <td>

	                            <?php
	                            	$proposition = ($lot['tarrif_suivant'] == '') ? tarrif(2, $lot['tarrif_figurant']) : $lot['tarrif_suivant'];
	                            	$somme_proposition += floatval($proposition);

	                            	echo format_number2_($proposition);
	                            ?> 
	                            €
	                        </td>

	                        <?php if ($key === 0) : ?>
		                            <td rowspan="<?= $countsLots; ?>">
		                            	<?php echo format_number2_($ht_total);?> €
		                            </td>
		                            <td>
			                            <?= $lot['c_facturation_commentaire'] ?>
			                        </td>
		                        </tr>
                            <?php endif; ?>

	                	<?php if ($key !== 0) : ?>
	                        <td>
	                            <?= $lot['c_facturation_commentaire'] ?>
	                        </td>
			            </tr>
                        <?php endif; ?>
	   
		                <?php
	                        $somme_mandat_lot += intval($lot['tarrif_figurant']);
	                        $somme_tarif_lot += intval($lot['tarrif']);
	                        $somme_tarrif_suivant += ($lot['tarrif_suivant'] == '') ? tarrif(2, $lot['tarrif']) : $lot['tarrif_suivant'];
		                ?>
	            <?php endforeach; ?>

	        <?php else : ?>
	            <tr>
	                <td ><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
	                <td ><?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?></td>
	                <td >
	                    <?= $facturation->libelle ?>
	                </td>
	                <td><?= $facturation->lot[0]['progm_nom'] ?></span></td>
	                <td><?= $facturation->lot[0]['mandat_num'] ?></td>
	                <td><?= date('d/m/Y', strtotime($facturation->lot[0]['mandat_signature'])) ?></td>
	                <td><?= format_number2_($facturation->lot[0]['tarrif_figurant']) ?> €</td>
	                <td><?= format_number2_($facturation->lot[0]['tarrif']) ?> €</td>
	                <td><?= format_number2_($facturation->lot[0]['tarrif_figurant'] - $facturation->lot[0]['tarrif']) ?> €</td>

	                <td>
	                    <?= ($facturation->lot[0]['tarrif_index'] == '') ? format_number2_(2) : format_number2_($facturation->lot[0]['tarrif_index']) ?> %
	                </td>

	                <td>
	                    <?php 
	                    	$proposition = ($facturation->lot[0]['tarrif_suivant'] == '') ? tarrif(2, $facturation->lot[0]['tarrif_figurant']) : $facturation->lot[0]['tarrif_suivant'];
							$somme_proposition += floatval($proposition);

							echo format_number2_($proposition);

	                     ?> €
	                </td>

	                <td style="text-align: right;">
	                    <?php 

	                    	$total_calcul = ($facturation->lot[0]['tarrif_suivant'] == '') ? tarrif(2, $facturation->lot[0]['tarrif_figurant']) : $facturation->lot[0]['tarrif_suivant'];
	                    	$somme_total += floatval($total_calcul);
	                    	echo format_number2_($total_calcul);
	                	?> 

						€

	                </td>
	                <td>                    
	                	<?= ($facturation->lot[0]['c_facturation_commentaire'] == '') ? '' : $facturation->lot[0]['c_facturation_commentaire'] ?>
	                </td>

	                <?php
		                $somme_tarif_lot += intval($facturation->lot[0]['tarrif']);
		                $somme_mandat_lot += intval($facturation->lot[0]['tarrif_figurant']);
		                $somme_tarrif_suivant += ($facturation->lot[0]['tarrif_suivant'] == '') ? tarrif(2, $facturation->lot[0]['tarrif']) : $facturation->lot[0]['tarrif_suivant'];
	                ?>
	            </tr>

	        <?php endif; ?>
	    <?php endforeach;?>

        <tr>
            <td colspan="6">TOTAL</td>
            <td>
            	<?= format_number_($somme_mandat_lot) ?> €
            </td>
            <td >
            	<?= format_number_($somme_tarif_lot) ?> € 
            </td>
            <td></td>
            <td></td>
            <td style="white-space: nowrap;">
            	<?php echo format_number2_($somme_total); ?>
            </td>
            <td style="white-space: nowrap;">
            	<?php echo format_number2_($somme_total); ?>
            </td>
            <td></td>
        </tr>
	    </tbody>
	</table>

</body>
</html>
