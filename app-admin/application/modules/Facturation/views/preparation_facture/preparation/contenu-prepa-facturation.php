<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Préparation des factures de renouvellement annuelle</h4>
    </div>
    <div class="px-3">
        <select name="annee_preparation" id="annee_preparation" class="form-select">
            <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")); $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="container-list">
    <div class="row m-0">
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Propriétaire</label>
                <input class="form-control mb-0" type="text" id="preparation_filtre_proprietaire" name="preparation_filtre_proprietaire">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" id="preparation_filtre_programme" name="preparation_filtre_programme">
            </div>
        </div>

        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme plus de 5 lots</label>
                <select name="programmeId" id="programmeId" class="form-select">
                <option value="0">
                    Tous
                </option> 
                    <?php foreach($programmes as $key => $programme ) : ?>
                    <option value="<?=$programme->progm_id ?>"><?=$programme->progm_nom ?> (<?=$programme->nbr ?>)</option>
                   
                    <?php endforeach ?>
                          
        </select>
            </div>
        </div>
        
    </div>
</div>

<div class="w-100" id="filtre-prepa-content">

</div>

<script>
    getfiltrePrepaFacture(<?= date('Y') ?>);
</script>