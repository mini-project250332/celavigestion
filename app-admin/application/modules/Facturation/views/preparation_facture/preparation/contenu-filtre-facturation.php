<style>
    thead {
        background: #2569C3;
    }

    table {
        width: 100%;

        color: white;
    }

    th {
        border-right: 1px solid #ffff;
        color: white;
        text-align: center;
    }

    td {
        border: 1px bold grey;
        border-style: solid;
        border-width: 1px;
    }

    .fixed-header {
        position: sticky;
        top: 0;
        z-index: 1;
    }

    .fixed-footer {
        position: sticky;
        bottom: 0;
        background-color: #f2f2f2;
        z-index: 1;
    }

    .espace {
        margin-left: 3px;
        /* Vous pouvez ajuster la valeur selon vos préférences */
    }

    .custom-input {
        margin-bottom: 1px;
    }
</style>

<?php

$facturationsArray = [];

function extraireNomClient($inputString)
{
    $noms = explode(', ', $inputString);
    $premierNom = $noms[0];
    $parts = preg_split('/\s+|-/', $premierNom, 3);
    if (count($parts) == 3) {
        $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
    } else {
        $nomExtrait = trim($parts[0]);
    }

    return $nomExtrait;
}
?>
<?php
function tarrif($index, $tarrifdefault)
{
    $pourcentage  = ($tarrifdefault * $index) / 100;
    $total = $tarrifdefault + $pourcentage;

    return $total;
} ?>

<div class="w-100 row mt-2 mb-2">
    <div class="col mt-2 ms-3 ">
        Résultats : <span id="nbr_dossier_preparation_base">0</span> dossier(s) pour <span id="nbr_lot_preparation_base">0</span> lot(s)
    </div>

    <div class="col mt-2 ms-3 ">
        <span id="nbr_dossier_preparation">0</span> dossier(s) sélectionné(s) pour <span id="nbr_lot_preparation">0</span> lot(s)
    </div>
    <div class="col text-end">

        <button id="exportPdf_apreparer" type="button" class="btn btn-success">
            Exporter en PDF
        </button>

        <button type="button" class="btn btn-primary tout_valider" disabled>
            Valider le(s) dossier(s) sélectionné(s)
        </button>

    </div>
</div>


<div class="row m-0 mb-2">
    <div class="col">
        <div class="table-container">
            <table class="table table-hover table-sm fs--1 mb-0 sticky-header" id="#votreTableau">
                <thead class="text-900 fixed-header">
                    <tr class="text-center">
                        <td rowspan="2" class="bg-white" width="1%"> <input type="checkbox" class="form-check-input" id="tout_cocher_valider" name="tout_cocher_valider"></td>
                        <th class="p-3 " rowspan="2">
                            <div class="form-group" style="display: flex;">
                                <span class="tri-dossier" style="cursor:pointer;">
                                    <i class="fas fa-sort-amount-down-alt"></i>
                                </span>
                                <!-- Ajout de l'espace -->
                                <span class="espace"></span>
                                Dossier
                            </div>
                        </th>
                        <th class="p-3" rowspan="2">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span> Propriétaire
                        </th>
                        <th style="white-space: nowrap;" class="p-3" rowspan="2">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span> Mode de paiements
                        </th>
                        <th colspan="8">Lot</th>
                        <th class="p-3 " rowspan="2">Total HT</th>
                        <th class="p-3 " rowspan="2">Commentaire interne</th>
                        <th class="p-3 " rowspan="2">Action</th>
                    </tr>
                    <tr class="text-center">
                        <th>Programme</th>
                        <th style="white-space: nowrap;">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span>N°Mandat
                        </th>
                        <th>Date signature Mandat</th>

                        <th>
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span>Tarif HT figurant sur Mandat
                        </th>

                        <th style="white-space: nowrap;"> <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span>
                            Facturé HT <?= $anneeprecedent ?></th>
                        <th>Ecart Mandat - Facturé HT <?= $anneeprecedent ?></th>
                        <th style="width : 100px;">
                            Indexation
                            <div class="input-group">
                                <input type="text" class="form-control custom-input" id="indexation_global" disabled style="width: 40px; text-align: right; height: 25px; padding-left: 0.3rem; padding-right: 0.3rem;" aria-label="Percentage" aria-describedby="basic-addon1">
                                <div class="input-group-append" disabled style="cursor: pointer;" id="validation_indexation">
                                    <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 25px; font-size: smaller; padding : 2px;"><i class="fas fa-check"></i></span>
                                </div>
                            </div>
                        </th>
                        <th style="width: 6%;">Proposition HT <?= $anneeChoisi ?> </th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $somme_tarif_lot = 0;
                    $somme_mandat_lot = 0;
                    $somme_tarrif_suivant = 0;
                    ?>
                    <?php foreach ($facturations as $key => $facturation) : ?>
                        <?php $facturationsArray[] = [
                            'dossier_id' => $facturation->dossier_id,
                        ]; ?>
                        <?php $countsLots = count($facturation->lot); ?>
                        <?php if ($countsLots > 1) : ?>
                            <?php $lots = array_values($facturation->lot); ?>

                            <tr class="dossier_<?= $facturation->dossier_id ?>">
                                <td rowspan="<?= $countsLots; ?>"><input type="checkbox" <?php if ($facturation->libelle == 'Non renseigné') echo 'disabled'; ?> class="form-check-input  checkbox_preparation dossier_<?= $facturation->dossier_id ?>" id="" name="" data-nbrelot="<?= count($facturation->lot) ?>" data-dossierid="<?= $facturation->dossier_id ?>"></td>
                                <?php $user = $facturation->util_prenom != null && $facturation->util_nom != null ?  $facturation->util_prenom . ' ' . $facturation->util_nom : 'Non renseigné' ?>
                                <td style="text-align: center;cursor:pointer;" rowspan="<?= $countsLots; ?>" title="Chargé de clientèle : <?= $user ?>"><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td rowspan="<?= $countsLots; ?>"><?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?> </td>
                                <td id="paiementId_<?= $facturation->mode_paiement ?>" rowspan="<?= $countsLots; ?>" style="text-align: center;">
                                    <?php if ($facturation->libelle == 'Non renseigné') : ?>
                                        <span class="badge badge-soft-danger fs--1">
                                        <?php endif; ?>
                                        <?= $facturation->libelle ?>
                                </td>
                                <?php foreach ($lots as $key => $lot) : ?>
                                    <td class="dossier_<?= $facturation->dossier_id ?> ps-1">

                                        <input type="hidden" class="factureApresparer_dossierID_<?= $facturation->dossier_id ?>" value="<?= $lot['num_lot']; ?>">

                                        <?= $lot['progm_nom'] ?>

                                    </td>
                                    <td style="text-align: center;" class="dossier_<?= $facturation->dossier_id ?>"><?= $lot['mandat_num'] ?></td>
                                    <td style="text-align: center;" class="dossier_<?= $facturation->dossier_id ?>"><?= date('d/m/Y', strtotime($lot['mandat_signature'])) ?></td>
                                    <td style="text-align: right;" class="dossier_<?= $facturation->dossier_id ?>" id="tarrifPrecedent_<?= $lot['num_lot'] ?>"><?= format_number2_($lot['tarrif_figurant']) ?> €</td>

                                    <td class="dossier_<?= $facturation->dossier_id ?>" id="tarrifPrecedentHt_<?= $lot['num_lot'] ?>" style="text-align: right;"><?= format_number2_($lot['tarrif']) ?> €</td>
                                    <td style="text-align: right;" class="dossier_<?= $facturation->dossier_id ?>"><?= format_number2_($lot['tarrif_figurant'] - $lot['tarrif']) ?> €</td>
                                    <td class="dossier_<?= $facturation->dossier_id ?>">
                                        <div class="input-group">
                                            <input type="hidden" value="<?= $lot['num_lot'] ?>" class="idlot_<?= $facturation->dossier_id ?>" id="">
                                            <input type="text" id="indexation_<?= $lot['num_lot'] ?>" oninput="delaySaveFacturation(<?= $lot['num_lot'] ?>, 'indexation', <?= $facturation->dossier_id ?> )" class="form-control custom-input index-value" placeholder="" aria-label="Percentage" aria-describedby="basic-addon1" value="<?= ($lot['tarrif_index'] == '') ? format_number2_(2) : format_number2_($lot['tarrif_index'])  ?>" style="width: 40px;text-align:right; padding-left: 0.3rem; padding-right: 0.3rem;">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 32.5px; padding : 3px;">%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="dossier_<?= $facturation->dossier_id ?>">
                                        <div class="input-group p-2">
                                            <input type="text" id="tarrif_<?= $lot['num_lot'] ?>" class="form-control custom-input indexation-input" data-dossier-id="<?= $facturation->dossier_id ?>" oninput="delaySaveFacturation(<?= $lot['num_lot'] ?>, 'tarrif', <?= $facturation->dossier_id ?>)" placeholder="" aria-label="Amount" aria-describedby="basic-addon1" value="<?= ($lot['tarrif_suivant'] == '') ? format_number2_(tarrif(2, $lot['tarrif_figurant'])) : format_number2_($lot['tarrif_suivant'])  ?>" style="width: 40px;text-align:right; padding-left: 0.3rem; padding-right: 0.3rem;">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 32.5px; padding : 3px;">€</span>
                                            </div>
                                        </div>
                                    </td>

                                    <?php if ($key === 0) : ?>
                                        <td style="text-align: right;" class="tarrifTotal" id="totatlTarif_<?= $facturation->dossier_id ?>" rowspan="<?= $countsLots; ?>">

                                        </td>
                                    <?php endif; ?>

                                    <td class="dossier_<?= $facturation->dossier_id ?> pe-2">
                                        <textarea name="" style="padding-left: 0.3rem; padding-right: 0.3rem;" oninput="delaySaveFacturation(<?= $lot['num_lot'] ?>, 'comment', <?= $facturation->dossier_id ?>)" id="commentaire_<?= $lot['num_lot'] ?>" cols="30" rows="10" class="form-control custom-input"><?= $lot['c_facturation_commentaire'] ?></textarea>
                                    </td>
                                    <?php if ($key === 0) : ?>
                                        <td style="text-align: center; white-space: nowrap;" rowspan="<?= $countsLots; ?>">
                                            <button class="btn btn-sm btn-primary" onclick="validerFacturation(this)" data-dossier-id="<?= $facturation->dossier_id ?>" type="button" id="validerButton_<?= $facturation->dossier_id ?>" <?php if ($facturation->libelle == 'Non renseigné') echo 'disabled'; ?>>
                                                Valider
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-primary fiche-dossier" data-client_id="<?= intval($facturation->client_id) ?>" data-dossier_id="<?= $facturation->dossier_id ?>" data-nom="<?= extraireNomClient($facturation->client_nom) ?>"><span class="fas fa-play"></span></button>
                                        </td>
                                    <?php endif; ?>
                            </tr>
                            <?php
                                    $somme_mandat_lot += intval($lot['tarrif_figurant']);
                                    $somme_tarif_lot += intval($lot['tarrif']);
                                    $somme_tarrif_suivant += ($lot['tarrif_suivant'] == '') ? tarrif(2, $lot['tarrif']) : $lot['tarrif_suivant'];
                            ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr class="dossier_<?= $facturation->dossier_id ?>">
                            <td><input type="checkbox" <?php if ($facturation->libelle == 'Non renseigné') echo 'disabled'; ?> class="form-check-input me-2 checkbox_preparation" id="" name="" data-nbrelot="<?= count($facturation->lot) ?>" data-dossierid="<?= $facturation->dossier_id ?>"></td>
                            <?php $user = $facturation->util_prenom != null && $facturation->util_nom != null ?  $facturation->util_prenom . ' ' . $facturation->util_nom : 'Non renseigné' ?>
                            <td style="text-align: center;cursor:pointer;" title="Chargé de clientèle : <?= $user ?>"><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                            <td style="text-align: left;"><?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?></td>
                            <td style="text-align: center;">
                                <?php if ($facturation->libelle == 'Non renseigné') : ?>
                                    <span class="badge badge-soft-danger fs--1">
                                    <?php endif; ?>
                                    <?= $facturation->libelle ?>
                            </td>
                            <td><?= $facturation->lot[0]['progm_nom'] ?></span></td>
                            <td style="text-align: center;"><?= $facturation->lot[0]['mandat_num'] ?></td>
                            <td style="text-align: center;"><?= date('d/m/Y', strtotime($facturation->lot[0]['mandat_signature'])) ?></td>
                            <td id="tarrifPrecedent_<?= $facturation->lot[0]['num_lot'] ?>" style="text-align: right;"><?= format_number2_($facturation->lot[0]['tarrif_figurant']) ?> €</td>
                            <td id="tarrifPrecedentHt_<?= $facturation->lot[0]['num_lot'] ?>" style="text-align: right;"><?= format_number2_($facturation->lot[0]['tarrif']) ?> €</td>
                            <td style="text-align: right;"><?= format_number2_($facturation->lot[0]['tarrif_figurant'] - $facturation->lot[0]['tarrif']) ?> €</td>

                            <td>
                                <div class="input-group">
                                    <input type="hidden" class="idlot_<?= $facturation->dossier_id ?>" value="<?= $facturation->lot[0]['num_lot'] ?>">
                                    <input type="text" class="form-control custom-input index-value" id="indexation_<?= $facturation->lot[0]['num_lot'] ?>" onfocusout="save_facturation(<?= $facturation->lot[0]['num_lot'] ?>, 'indexation', <?= $facturation->dossier_id ?>);" placeholder="" aria-label="Percentage" aria-describedby="basic-addon1" value="<?= ($facturation->lot[0]['tarrif_index'] == '') ? format_number2_(2) : format_number2_($facturation->lot[0]['tarrif_index']) ?>" style="width: 40px; text-align: right; padding-left: 0.3rem; padding-right: 0.3rem;">

                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 32.5px; font-size: smaller; padding : 3px;">%</span>
                                    </div>
                                </div>
                            </td>

                            <td>
                                <div class="input-group">
                                    <input type="text" id="tarrif_<?= $facturation->lot[0]['num_lot'] ?>" class="form-control custom-input indexation-input" data-dossier-id="<?= $facturation->dossier_id ?>" onfocusout="save_facturation(<?= $facturation->lot[0]['num_lot'] ?>, 'tarrif', <?= $facturation->dossier_id ?>);" placeholder="" aria-label="Amount" aria-describedby="basic-addon1" value="<?= ($facturation->lot[0]['tarrif_suivant'] == '') ? format_number2_(tarrif(2, $facturation->lot[0]['tarrif_figurant'])) : format_number2_($facturation->lot[0]['tarrif_suivant']) ?>" style="width: 40px;text-align:right;padding-left: 0.3rem; padding-right: 0.3rem;">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 32.5px; padding : 3px;">€</span>
                                    </div>
                                </div>
                            </td>
                            <td style="text-align: right;" class="tarrifTotal" id="totatlTarif_<?= $facturation->dossier_id ?>">
                                €
                            </td>
                            <td>
                                <textarea name="" style="padding-left: 0.3rem; padding-right: 0.3rem;" oninput="delaySaveFacturation(<?= $facturation->lot[0]['num_lot'] ?>, 'comment', <?= $facturation->dossier_id ?>);" id="commentaire_<?= $facturation->lot[0]['num_lot'] ?>" cols="30" rows="10" class="form-control custom-input"><?= ($facturation->lot[0]['c_facturation_commentaire'] == '') ? '' : $facturation->lot[0]['c_facturation_commentaire'] ?></textarea>
                            </td>
                            <td style="text-align: center; white-space: nowrap;">
                                <button class="btn btn-sm btn-primary" onclick="validerFacturation(this)" data-dossier-id="<?= $facturation->dossier_id ?>" type="button" id="validerButton_<?= $facturation->dossier_id ?>" <?php if ($facturation->libelle == 'Non renseigné') echo 'disabled'; ?>>
                                    Valider
                                </button>
                                <button type="button" class="btn btn-sm btn-outline-primary fiche-dossier" data-client_id="<?= intval($facturation->client_id) ?>" data-dossier_id="<?= $facturation->dossier_id ?>" data-nom="<?= extraireNomClient($facturation->client_nom) ?>"><span class="fas fa-play"></span></button>
                            </td>

                            <?php
                            $somme_tarif_lot += intval($facturation->lot[0]['tarrif']);
                            $somme_mandat_lot += intval($facturation->lot[0]['tarrif_figurant']);
                            $somme_tarrif_suivant += ($facturation->lot[0]['tarrif_suivant'] == '') ? tarrif(2, $facturation->lot[0]['tarrif']) : $facturation->lot[0]['tarrif_suivant'];
                            ?>
                        </tr>

                    <?php endif; ?>
                <?php endforeach;
                    $facturationsJson = json_encode($facturationsArray);
                ?>
                </tbody>
                <tfoot class="fixed-footer">
                    <tr>
                        <td colspan="7" class="text-center"><b> TOTAL </b></td>
                        <td style="white-space: nowrap;"><b id="somme_mandatlot"><?= format_number_($somme_mandat_lot) ?> € </b></td>
                        <td style="white-space: nowrap;"><b id="somme_ht"><?= format_number_($somme_tarif_lot) ?> € </b> </td>
                        <td></td>
                        <td></td>
                        <td style="white-space: nowrap;"><b id="total_proposition"></b></td>
                        <td style="white-space: nowrap;"><b id="total_ht"> </b></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        var sommefacturation = 0;
        var facturations = <?php echo $facturationsJson; ?>;
        facturations.forEach(function(facturation) {
            var sommeIndexation = updateSommeTarrif(facturation.dossier_id);
            $('#totatlTarif_' + facturation.dossier_id).text(sommeIndexation + ' €');
            sommefacturation += sommeIndexation;
        });

        countAfficherPreparation();
        var total = TotalTarif();

        $('#total_proposition').text(customFormatNumber(total) + ' €');
        $('#total_ht').text(customFormatNumber(total) + ' €');
    });

    function updateSommeTarrif(dossierId) {
        var sommeIndexation = 0;

        $('.indexation-input[data-dossier-id="' + dossierId + '"]').each(function() {
            var value = $(this).val();
            value = value.replace(/\s/g, '');
            value = value.replace(',', '.');
            var indexationValue = parseFloat(value) || 0;
            sommeIndexation += indexationValue;
        });


        return formatNumber(sommeIndexation);
    }

    function customFormatNumber(number) {
        let strNumber = number.toFixed(2).toString();
        let [integerPart, decimalPart] = strNumber.split('.');
        let formattedIntegerPart = '';
        for (let i = integerPart.length - 1, count = 0; i >= 0; i--) {
            formattedIntegerPart = integerPart[i] + formattedIntegerPart;
            count++;
            if (count % 3 === 0 && i !== 0) {
                formattedIntegerPart = '.' + formattedIntegerPart;
            }
        }
        let formattedNumber = formattedIntegerPart + ',' + decimalPart;

        return formattedNumber;
    }




    function formatNumber(number, negatif = false) {

        const options = {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        };
        if (Math.abs(number) >= 1000) {
            options.minimumFractionDigits = 2;
            options.maximumFractionDigits = 2;
        }
        const result = new Intl.NumberFormat('fr-FR', options).format(number);
        if (negatif && parseFloat(number) > 0) {
            return "- " + result;
        }

        return result;
    }
</script>