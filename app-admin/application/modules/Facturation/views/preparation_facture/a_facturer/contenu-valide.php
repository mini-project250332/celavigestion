<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    .fixed-header {
        position: sticky;
        top: 0;
        z-index: 1;
    }

    .fixed-footer {
        position: sticky;
        bottom: 0;
        background-color: #f2f2f2;
        z-index: 1;
    }
</style>

<script>
    function updateSommeTarrif(dossierId) {
        var sommeIndexation = 0;
        $('.indexation-input[data-dossier-id="' + dossierId + '"]').each(function() {
            var indexationValue = parseFloat($(this).val()) || 0;
            sommeIndexation += indexationValue;
        });

        // Affichez la somme dans la console (vous pouvez faire autre chose avec la somme ici)
        return sommeIndexation;
    }

    $(document).ready(function() {
        <?php foreach ($facturations as $facturation) : ?>
            var sommeIndexation<?= $facturation->dossier_id ?> = updateSommeTarrif(<?= $facturation->dossier_id ?>);
            $('#totatlTarif_<?= $facturation->dossier_id ?>').text(sommeIndexation<?= $facturation->dossier_id ?> + ' €');
        <?php endforeach; ?>
    });

    function tarrif(index, tarrifdefault) {
        var pourcentage = (tarrifdefault * index) / 100;
        var total = tarrifdefault + pourcentage;
        return total;
    }
</script>

<?php
$alltotalht = 0;
$alltotaltva = 0;
$alltotalttc = 0;
function tarrif($index, $tarrifdefault)
{
    $pourcentage  = ($tarrifdefault * $index) / 100;
    $total = $tarrifdefault + $pourcentage;

    return $total;
}

function extraireNomClient($inputString)
{
    $noms = explode(', ', $inputString);
    $premierNom = $noms[0];
    $parts = preg_split('/\s+|-/', $premierNom, 3);
    if (count($parts) == 3) {
        $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
    } else {
        $nomExtrait = trim($parts[0]);
    }

    return $nomExtrait;
}
?>

<div class="w-100 row mt-2 mb-2">
    <div class="col col mt-2 ms-3">
        <span id="nbr_dossier_affiche">0</span> dossier(s) affiché(s) pour <span id="nbr_lot_affiche">0</span> lot(s)
    </div>
    <div class="col mt-2 ms-3">
        <span id="nbr_dossier">0</span> dossier(s) sélectionné(s) pour <span id="nbr_lot">0</span> lot(s)
    </div>

    <div class="col-5 text-end">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-success tout_export">
                    Exporter en PDF
                </button> 
                <button type="button" class="btn btn-primary text-nowrap tout_facturer_pdf" disabled>
                    Facturer le(s) dossier(s) selectionné(s)
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row m-0 mb-2">
    <div class="col">
        <div class="table-wrapper">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <td rowspan="2" class="bg-white" width="1%"> <input type="checkbox" class="form-check-input me-2" id="tout_cocher_facturer" name="tout_cocher_facturer"></td>
                        <th class="p-3" rowspan="2" width="6.5%">
                            <div class="form-group" style="display: flex;">
                                <span class="tri-" style="cursor:pointer;">
                                    <i class="fas fa-sort-amount-down-alt"></i>
                                </span> &nbsp;
                                &nbsp;Dossier
                            </div>
                        </th>
                        <th class="p-3" rowspan="2" width="10%">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span> Propriétaire
                        </th>
                        <th style="white-space: nowrap;" class="p-3" rowspan="2" width="8%">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span> Mode de paiements
                        </th>
                        <th colspan="4">Lot</th>
                        <th class="p-3 " rowspan="2">Total HT</th>
                        <th class="p-3 " rowspan="2">TVA</th>
                        <th class="p-3 " rowspan="2">TTC</th>
                        <th class="p-3 " rowspan="2" width="15%">Commentaire interne</th>
                        <th class="p-3 " rowspan="2">Action</th>
                    </tr>
                    <tr class="text-center">
                        <th>Programme</th>
                        <th style="white-space: nowrap;">
                            <span class="tri-" style="cursor:pointer;">
                                <i class="fas fa-sort-amount-down-alt"></i>
                            </span>N°Mandat
                        </th>
                        <th width="8%">Date signature Mandat</th>
                        <th>Proposition <br> HT <?= $anneeChoisi ?> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($facturations as $key => $facturation) : ?>
                        <?php $countsLots = count($facturation->lot); ?>
                        <?php if ($countsLots > 1) : ?>
                            <?php $lots = array_values($facturation->lot); ?>
                            <?php $somme_tarif_lot = 0; ?>
                            <tr class="row_dossier_<?= $facturation->dossier_id ?>">
                                <td rowspan="<?= $countsLots; ?>">
                                    <?php if (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) : ?>
                                        <input type="checkbox" class="form-check-input me-2 checkbox_facturer" data-dossier_id="<?= $facturation->dossier_id ?>" data-nbrelot="<?= count($facturation->lot) ?>">
                                    <?php else : ?>
                                        <span style="color: red; font-weight: bold; font-size: 1.2em;"><i class="fas fa-exclamation-circle"></i></span>
                                    <?php endif ?>
                                </td>
                                <td class="text-center" rowspan="<?= $countsLots; ?>" style="cursor: pointer;" title="Chargé de clientèle : <?= $facturation->user != null ? $facturation->user : 'Non renseigné' ?>"><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td rowspan="<?= $countsLots; ?>"><?= $facturation->client_nom ?></td>
                                <td id="paiementId_<?= $facturation->mode_paiement ?>" rowspan="<?= $countsLots; ?>" class="text-center">
                                    <?php if (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) : ?>
                                        <?= $facturation->libelle ?>
                                    <?php else : ?>
                                        <?= $facturation->libelle ?> <br>
                                        <b class="text-danger fw-bold">Infos SEPA manquantes !</b>
                                    <?php endif ?>
                                </td>
                                <?php foreach ($lots as $key => $lot) : ?>
                                    <?php $somme_tarif_lot = 0; ?>
                                    <?php $somme_tarif_lot += intval($lot['tarrif']); ?>
                                    <input type="hidden" class="lot_a_facture_<?= $facturation->dossier_id ?>" value="<?= $lot['num_lot'] ?>">
                                    <td class="p-2 row_dossier_<?= $facturation->dossier_id ?>"><?= $lot['progm_nom'] ?></td>
                                    <td class="text-center row_dossier_<?= $facturation->dossier_id ?>"><?= $lot['mandat_num'] ?></td>
                                    <td class="text-center row_dossier_<?= $facturation->dossier_id ?>"><?= date('d/m/Y', strtotime($lot['mandat_signature'])) ?></td>
                                    <td class="text-end row_dossier_<?= $facturation->dossier_id ?>">
                                        <?= ($lot['tarrif_suivant'] == '') ? format_number_(tarrif(2, $lot['tarrif'])) : format_number_($lot['tarrif_suivant'])  ?> €
                                    </td>
                                    <?php if ($key === 0) : ?>
                                        <td class="text-end" rowspan="<?= $countsLots; ?>">
                                            <?php $totalht = array_sum(array_column($lots, 'tarrif_suivant'));
                                            $alltotalht += $totalht; ?>
                                            <?= format_number_($totalht) ?> €
                                        </td>

                                        <td class="text-end" rowspan="<?= $countsLots; ?>">
                                            <?php $tva = round(($totalht * 20 / 100), 2);
                                            $alltotaltva += $tva; ?>
                                            <?= format_number_($tva) ?> €
                                        </td>
                                        <td class="text-end" rowspan="<?= $countsLots; ?>">
                                            <?php $ttc = round(($totalht + $tva), 2);
                                            $alltotalttc += $ttc; ?>
                                            <?= format_number_($ttc) ?> €
                                        </td>
                                    <?php endif; ?>
                                    <td class="row_dossier_<?= $facturation->dossier_id ?>">
                                        <?= ($lot['c_facturation_commentaire'] == '') ? '' : $lot['c_facturation_commentaire']  ?>
                                    </td>
                                    <?php if ($key === 0) : ?>
                                        <td class="text-center" style="white-space: nowrap" rowspan="<?= $countsLots; ?>">
                                            <button type="button" class="btn btn btn-sm btn-outline-primary fiche-dossier" data-dossier_id="<?= $facturation->dossier_id ?>" data-client_id="<?= intval($facturation->client_id) ?>" data-nom="<?= extraireNomClient($facturation->client_nom) ?>"><i class="fas fa-play"></i></button>
                                            <button type="button" class="btn btn btn-sm btn-outline-danger annuler_validation" title="annuler la validation" data-dossier_id="<?= $facturation->dossier_id ?>">
                                                <span class="fas fa-reply-all"> </span>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary apercu_facture" title="aperçu de la facture" data-dossier_id="<?= $facturation->dossier_id ?>"><span class="fas fa-eye"></span></button>
                                            <button type="button" class="btn btn btn-sm btn-outline-danger facturer_dossier" title="créer la facture" data-dossier_id="<?= $facturation->dossier_id ?>" <?= (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) ? '' : 'disabled' ?>><i class="fas fa-file-invoice-dollar"></i> <i class="fas fa-plus-circle" style="height: 10px; width : 10px; margin-left: -6px; margin-bottom: 6px;"></i></button>

                                        </td>
                                    <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr class="row_dossier_<?= $facturation->dossier_id ?>">
                            <td>
                                <?php if (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) : ?>
                                    <input type="checkbox" class="form-check-input me-2 checkbox_facturer" data-dossier_id="<?= $facturation->dossier_id ?>" data-nbrelot="<?= count($facturation->lot) ?>">
                                <?php else : ?>
                                    <span style="color: red; font-weight: bold; font-size: 1.2em;"><i class="fas fa-exclamation-circle"></i></span>
                                <?php endif ?>
                            </td>
                            <td class="text-center" style="cursor: pointer;" title="Chargé de clientèle : <?= $facturation->user != null ? $facturation->user : 'Non renseigné' ?>"><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                            <td style="text-align: left;"><?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?></td>
                            <td class="text-center">
                                <?php if (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) : ?>
                                    <?= $facturation->libelle ?>
                                <?php else : ?>
                                    <?= $facturation->libelle ?> <br>
                                    <b class="text-danger fw-bold">Infos SEPA manquantes !</b>
                                <?php endif ?>
                            </td>
                            <input type="hidden" class="lot_a_facture_<?= $facturation->dossier_id ?>" value="<?= $facturation->lot[0]['num_lot'] ?>">
                            <td><?= $facturation->lot[0]['progm_nom'] ?></td>
                            <td class="text-center"><?= $facturation->lot[0]['mandat_num'] ?></td>
                            <td class="text-center"><?= date('d/m/Y', strtotime($facturation->lot[0]['mandat_signature'])) ?></td>
                            <td class="text-end">
                                <?= ($facturation->lot[0]['tarrif_suivant'] == '') ? format_number_(tarrif(2, $facturation->lot[0]['tarrif'])) : format_number_($facturation->lot[0]['tarrif_suivant']) ?> €
                            </td>
                            <td class="text-end">
                                <?php $totalht = ($facturation->lot[0]['tarrif_suivant'] == '') ? tarrif(2, $facturation->lot[0]['tarrif']) : $facturation->lot[0]['tarrif_suivant'];
                                $alltotalht += $totalht; ?>
                                <?= format_number_($totalht) ?> €
                            </td>
                            <td class="text-end">
                                <?php $tva = round(($totalht * 20 / 100), 2);
                                $alltotaltva += $tva; ?>
                                <?= format_number_($tva) ?> €
                            </td>
                            <td class="text-end">
                                <?php $ttc = round(($totalht + $tva), 2);
                                $alltotalttc += $ttc; ?>
                                <?= format_number_($ttc) ?> €
                            </td>
                            <td>
                                <?= ($facturation->lot[0]['c_facturation_commentaire'] == '') ? '' : $facturation->lot[0]['c_facturation_commentaire'] ?>
                            </td>
                            <td class="text-center" style="white-space: nowrap">
                                <button type="button" class="btn btn btn-sm btn-outline-primary fiche-dossier" data-dossier_id="<?= $facturation->dossier_id ?>" data-client_id="<?= intval($facturation->client_id) ?>" data-nom="<?= extraireNomClient($facturation->client_nom) ?>"><i class="fas fa-play"></i></button>
                                <button type="button" class="btn btn btn-sm btn-outline-danger annuler_validation" title="annuler la validation" data-dossier_id="<?= $facturation->dossier_id ?>">
                                    <span class="fas fa-reply-all"> </span>
                                </button>
                                <button type="button" class="btn btn btn-sm btn-outline-secondary apercu_facture" title="aperçu de la facture" data-dossier_id="<?= $facturation->dossier_id ?>"><span class="fas fa-eye"></span></button>
                                <button type="button" class="btn btn btn-sm btn-outline-danger facturer_dossier" title="créer la facture" data-dossier_id="<?= $facturation->dossier_id ?>" <?= (!empty($facturation->sepa_numero) && $facturation->sepa_etat == 1 && verifyIBAN($facturation->sepa_iban)) ? '' : 'disabled' ?>><i class="fas fa-file-invoice-dollar"></i> <i class="fas fa-plus-circle" style="height: 10px; width : 10px; margin-left: -6px; margin-bottom: 6px;"></i></button>

                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
                <tfoot class="fixed-footer">
                    <td class="text-center" colspan="8"><b>TOTAL</b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($alltotalht) ?> €</b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($alltotaltva) ?> €</b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($alltotalttc) ?> €</b></td>
                    <td colspan="3"></td>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        countAfficher();
    });
</script>