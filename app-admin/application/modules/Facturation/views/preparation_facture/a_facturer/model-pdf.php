<style>
    @page {
        margin: 2%;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        border: 1px solid black;
        padding: 5px;
        text-align: center;
        font-size: 10px;
    }

    .nowrap {
        white-space: nowrap;
    }

    h2 {
        text-align: center;
    }

    .title-pdf h1{
        text-align: center;
        font-size: 14px;
        max-resolution: 10px;
    }
</style>
<?php
$alltotalht = 0;
$alltotaltva = 0;
$alltotalttc = 0;

function tarrif($index, $tarrifdefault)
{
    $pourcentage  = ($tarrifdefault * $index) / 100;
    $total = $tarrifdefault + $pourcentage;

    return $total;
}
?>
<div class="title-pdf">
    <h1> Factures à valider </h1>
</div>

<table id="table_mandat">
    <thead>
        <tr>
            <th rowspan="2"> Dossier </th>
            <th rowspan="2"> Propriétaire </th>
            <th rowspan="2"> Mode de paiements </th>
            <th colspan="4">Lot</th>
            <th rowspan="2">Total HT</th>
            <th rowspan="2">TVA</th>
            <th rowspan="2">TTC</th>
            <th rowspan="2">Commentaire interne</th>
        </tr>
        <tr>
            <th>Programme</th>
            <th> N°Mandat </th>
            <th>Date signature Mandat</th>
            <th>Proposition <br> HT <?= $anneeChoisi ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($facturations as $key => $facturation) : ?>
            <?php $countsLots = count($facturation->lot); ?>
            <tr>
                <td rowspan="<?= $countsLots ?>"><?= str_pad($facturation->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                <td rowspan="<?= $countsLots ?>"><?= $facturation->client_nom . ' ' . $facturation->client_prenom  ?></td>
                <td rowspan="<?= $countsLots ?>"><?= $facturation->libelle_mode_paiement ?></td>
                <td><?= $facturation->lot[0]['progm_nom'] ?></td>
                <td><?= $facturation->lot[0]['mandat_num'] ?></td>
                <td><?= date('d/m/Y', strtotime($facturation->lot[0]['mandat_signature'])) ?></td>
                <td><?= ($facturation->lot[0]['tarrif_suivant'] == '') ? format_number_(tarrif(2, $facturation->lot[0]['tarrif'])) : format_number_($facturation->lot[0]['tarrif_suivant']) . ' €' ?> </td>
                <td rowspan="<?= $countsLots ?>" class="nowrap">
                    <?php $totalht = array_column($facturation->lot, 'tarrif_suivant');
                    $totalht = array_sum($totalht);
                    $alltotalht += $totalht;
                    echo format_number_($totalht) . ' €' ?>
                </td>
                <td rowspan="<?= $countsLots ?>" class="nowrap">
                    <?php $tva = round(($totalht * 20 / 100), 2);
                    $alltotaltva += $tva; ?>
                    <?= format_number_($tva) . ' €' ?>
                </td>
                <td rowspan="<?= $countsLots ?>" class="nowrap">
                    <?php $ttc = round(($totalht + $tva), 2);
                    $alltotalttc += $ttc; ?>
                    <?= format_number_($ttc) . ' €' ?>
                </td>
                <td><?= ($facturation->lot[0]['c_facturation_commentaire'] == '') ? '' : $facturation->lot[0]['c_facturation_commentaire'] ?></td>
            </tr>
            <?php if ($countsLots > 1) : foreach ($facturation->lot as $key_lot => $fact_lot) : if ($key_lot != 0) : ?>
                        <tr>
                            <td><?= $fact_lot['progm_nom'] ?></td>
                            <td><?= $fact_lot['mandat_num'] ?></td>
                            <td><?= date('d/m/Y', strtotime($fact_lot['mandat_signature'])) ?></td>
                            <td class="nowrap"><?= ($fact_lot['tarrif_suivant'] == '') ? format_number_(tarrif(2, $fact_lot['tarrif'])) . ' €' : format_number_($fact_lot['tarrif_suivant']) . ' €' ?></td>
                            <td><?= ($fact_lot['c_facturation_commentaire'] == '') ? '' : $fact_lot['c_facturation_commentaire'] ?></td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
            <?php endif ?>
        <?php endforeach ?>
        <tr>
            <td colspan="7"><b>TOTAL</b></td>
            <td class="nowrap"><b><?= format_number_($alltotalht) . ' €' ?> </b></td>
            <td class="nowrap"><b><?= format_number_($alltotaltva) . ' €' ?> </b></td>
            <td class="nowrap"><b><?= format_number_($alltotalttc) . ' €' ?> </b></td>
        </tr>
    </tbody>
</table>