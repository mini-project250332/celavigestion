<style>
    .celavilogohouse {
        width: 90px;
    }

    .celaviword {
        width: 225px;
    }

    .numero_facture {
        font-size: 25px;
        font-weight: bold;
    }

    .first_table th {
        color: black;
        border-left: none;
        border-right: none;
    }

    .first_table td {
        color: black;
        text-align: center;
        border: none;
    }

    .border_foot {
        border: 2px solid black;
    }
</style>

<?php
$totalht = 0;
$mode_virement = array(
    1 => 'NON RENSEIGNER',
    2 => 'VIREMENT',
    3 => 'PRELEVEMENT',
    4 => 'SUR LOYERS ENCAISSES'
);
?>

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalToggleLabel">Aperçu de la facture</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <div class="card rounded-0 h-100">
        <div class="row mt-3 mb-4">
            <div class="col"></div>
            <div class="col text-center">
                <img class="celavilogohouse" src="<?php echo base_url('../assets/img/celavilogohouse.png'); ?>"> <br>
                <img class="celaviword" src="<?php echo base_url('../assets/img/celaviword.png'); ?>">
            </div>
            <div class="col"></div>
        </div>

        <div class="row ms-2 mb-4">
            <div class="col-4">
                Parc de Brais 39 route de Fondeline <br>
                44600 SAINT NAZAIRE <br>
                <?= $dossier->util_prenom . ' ' . $dossier->util_nom ?> <br>
                <?= $dossier->util_mail  ?> <br>
                <?= formaterNumeroTelephone($dossier->util_phonefixe)  ?>
            </div>
            <div class="col-4"> </div>
            <div class="col-4"> </div>
        </div>

        <div class="row ms-2 mb-4">
            <div class="col">
                <span class="numero_facture">
                    Facture N° -
                </span>
            </div>
            <div class="col-2"></div>
            <div class="col">
                <?= $dossier->client_nom . ' ' . $dossier->client_prenom ?> <br>
                <?= $lot_principale->progm_nom ?> <br>
                <?= $lot_principale->progm_cp . ' ' . $lot_principale->progm_ville ?> <br>
            </div>
        </div>

        <div class="row ms-2 me-2 mb-4">
            <div class="col">
                <table class="table first_table">
                    <thead>
                        <tr class="table-secondary">
                            <th>Numero</th>
                            <th>Date</th>
                            <th>Dossier</th>
                            <th>Date échéance</th>
                            <th>Mode de réglement</th>
                            <th>Num TVA intracom</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>-</td>
                            <td><?= date('d/m/Y', strtotime(date('Y-m-d'))) ?></td>
                            <td><?= str_pad($dossier->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                            <td><?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?></td>
                            <td><?= array_key_exists($dossier->mode_paiement, $mode_virement) ? $mode_virement[$dossier->mode_paiement] : '' ?></td>
                            <td><?= $dossier->dossier_tva_intracommunautaire ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row ms-2 me-2 mb-4">
            <div class="col">
                <table class="table first_table">
                    <thead>
                        <tr class="table-secondary">
                            <th>Article</th>
                            <th>Lot</th>
                            <th>Qt</th>
                            <th>Prix Uni. HT</th>
                            <th>Tva</th>
                            <th>Total HT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($lot as $lots) : ?>
                            <?php $totalht += $lots->montant_ht_facture  ?>
                            <tr>
                                <td>
                                    <?= str_replace("@annee", $annee, $article->art_libelle_fr); ?><br>
                                    <?= str_replace("@annee", $annee, $article->art_libelle_en); ?>
                                </td>
                                <td><?= $lots->progm_nom ?></td>
                                <td>1.00</td>
                                <td><?= format_number_($lots->montant_ht_facture) ?> €</td>
                                <td>20.00 %</td>
                                <td><?= format_number_($lots->montant_ht_facture) ?> €</td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                    <?php $tva = ($totalht * 20) / 100;
                    $ttc = ($totalht + $tva); ?>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td class="text-end" colspan="2">
                                <b>Total HT :</b> <br>
                                <b>TVA :</b> <br>
                                <b>Total TTC :</b>
                            </td>
                            <td class="text-end" colspan="2">
                                <b><?= format_number_($totalht) ?> €</b> <br>
                                <b><?= format_number_($tva) ?> €</b> <br>
                                <b><?= format_number_($ttc) ?> €</b>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="row ms-2 me-2 mb-4">
            <div class="col">
                <table class="table first_table">
                    <thead>
                        <tr class="table-secondary">
                            <th>
                                <?= array_key_exists($dossier->mode_paiement, $mode_virement) ? $mode_virement[$dossier->mode_paiement] : '' ?> <br><br>
                                Echéance de paiement / Deadline : <?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?>
                            </th>
                            <th>
                                <br> <br>
                                A PAYER :
                            </th>
                            <th>
                                <br> <br>
                                <?= format_number_($ttc) ?> €
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="row ms-2 me-2 mb-7">
            <div class="col">
                <table class="table first_table">
                    <?php if ($dossier->mode_paiement == 2) : ?>
                        <thead>
                            <tr class="table-secondary">
                                <th>
                                    Le virement doit être effectué avant le <?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?> avec la référence [Num facture] sur le compte : / The transfer must be made before <?= date('d/m/Y', strtotime(date('Y-m-d') . ' +10 days')) ?> with the reference [Invoice number] to the following account :
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b> IBAN</b>: <?= formaterIBAN($rib->rib_iban) ?> - <b> BIC</b>: <?= $rib->rib_bic ?>
                                </td>
                            </tr>
                        </tbody>
                    <?php elseif ($dossier->mode_paiement == 3) : ?>
                        <thead>
                            <tr class="table-secondary">
                                <th>
                                    Prélevé automatiquement sur votre compte bancaire : / Automatically debited from your bank account :
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b> IBAN</b>: <?= formaterIBAN($lot_principale->iban) ?> - <b> BIC</b>: <?= $lot_principale->bic ?>
                                </td>
                            </tr>
                        </tbody>
                    <?php elseif ($dossier->mode_paiement == 4) : ?>
                        <thead>
                            <tr class="table-secondary">
                                <th>
                                    Prélevé automatiquement sur vos loyers / Automatically deducted from your rent
                                </th>
                            </tr>
                        </thead>
                    <?php endif ?>
                </table>
            </div>
        </div>

        <div class="row ms-5 me-5 mb-4 border_foot">
            <div class="col text-center mb-2">
                <b>SASU CELAVI GESTION - Parc de Brais 39 route de Fondeline 44600 SAINT NAZAIRE</b> <br>
                SASU au Capital de 100000 Euros - RCS SAINT NAZAIRE B 477 782 346 - Siret : 477 782 346 00035 - APE : 6831Z <br>
                Numéro de TVA intracommunautaire : FR69477782346 <br>
                <br>
                En cas de retard de paiement, une pénalité égale à 3 fois le taux d’intérêt légal sera exigible (Article L441-10, alinéa 12 du Code du Commerce). Pour tout professionnel, en sus des indemnités de retard, toute somme, y compris l’acompte, non payée à sa date d’exigibilité produira de plein droit le paiement d’une indemnité forfaitaire de 40 euros due au titre des frais de recouvrement (Article 441-6, I al. 12 et D.441-5 du Code du Commerce).
                <br> <br>
                Pas d’escompte pour règlement anticipé.
                <br> <br>
                Carte professionnelle gestion N° CPI 4402 2018 000 035 231 délivrée par la CCI de Nantes SAINT NAZAIRE le 1er juillet 2021, garantie gestion à
                hauteur de 620.000€ souscrites auprès de GALIAN, 89 rue de la Boétie 75008 PARIS <br>
            </div>
        </div>

        <div class="row ms-3 me-3 mb-4">
            <div class="col">
                Mentions légales pour les règlements par prélévement SEPA -> ICS FR45ZZZ532215 - RUM <?= $sepa->sepa_numero ?>
            </div>
        </div>
    </div>
</div>
<br>
<div class="modal-footer">
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>