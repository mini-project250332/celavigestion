<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Facturation de renouvellement annuelle</h4>
    </div>
    <div class="px-3">
        <select name="annee_facture_valide" id="annee_facture_valide" class="form-select">
            <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")); $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="container-list">
    <div class="row m-0">
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Propriétaire</label>
                <input class="form-control mb-0" type="text" id="text_filtre_proprietaire" name="text_filtre_proprietaire">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" id="text_filtre_programme" name="text_filtre_programme">
            </div>
        </div>
    </div>
</div>

<div class="w-100" id="filtre-facture-valide">

</div>

<script>
    getfiltreFactureValide(<?= date('Y') ?>);
</script>