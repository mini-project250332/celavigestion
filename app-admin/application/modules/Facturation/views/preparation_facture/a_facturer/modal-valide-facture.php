<div class="modal-header bg-success">
    <h5 class="modal-title text-white" id="exampleModalToggleLabel">Confirmation de la creation de facture</h5>
    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-4">
            <label for="exampleInput">Date de la facture :</label>
        </div>
        <div class="col-8">
            <input type="hidden" id="dossier_id_facture" value="<?= $dossier_id ?>">
            <input type="hidden" id="annee_facture" value="<?= $annee ?>">
            <?php foreach ($tab_lot as $tab) :  ?>
                <input type="hidden" class="modal_lot_id_facture" value="<?= $tab ?>">
            <?php endforeach ?>
            <input type="date" class="form-control" id="date_facture" value="<?= $last_date ?>" data-date="<?= $last_date ?>">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="text-danger text-center fs--1"><i class="text_alert"></i></div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" id="confirmer_creation">Confirmer</button>
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>