<div class="h-100">
    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="sous_preparation-tab" data-bs-toggle="tab" href="#tab-sous_preparation" role="tab" aria-controls="tab-sous_preparation" aria-selected="true">A préparer</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="validation" data-bs-toggle="tab" href="#tab-validation" role="tab" aria-controls="tab-validation" aria-selected="false">A facturer</a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-2" id="lotTabContent">
        <div class="tab-pane fade show active" id="tab-sous_preparation" role="tabpanel" aria-labelledby="sous_preparation-tab">
            <?php $this->load->view('preparation_facture/preparation/page_sous_preparation'); ?>
        </div>
        <div class="tab-pane fade" id="tab-validation" role="tabpanel" aria-labelledby="validation-tab">
            <?php $this->load->view('preparation_facture/a_facturer/page_facture'); ?>
        </div>
    </div>
</div>