<div class="h-100">
    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="dossier_incomplets-tab" data-bs-toggle="tab" href="#tab-dossier_incomplets" role="tab" aria-controls="tab-dossier_incomplets" aria-selected="true">Lots sans mandat</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="devis" data-bs-toggle="tab" href="#tab-devis" role="tab" aria-controls="tab-devis" aria-selected="false">Devis</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="preparation_facture" data-bs-toggle="tab" href="#tab-preparation_facture" role="tab" aria-controls="tab-preparation_facture" aria-selected="false">Préparation des factures</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="facture" data-bs-toggle="tab" href="#tab-facture" role="tab" aria-controls="tab-facture" aria-selected="false">Factures</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="paiements" data-bs-toggle="tab" href="#tab-paiements" role="tab" aria-controls="tab-paiements" aria-selected="false">Paiements</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fichierSepa" data-bs-toggle="tab" href="#tab-fichierSepa" role="tab" aria-controls="tab-fichierSepa" aria-selected="false">Fichiers XML Prélèvement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="relance_impaye" data-bs-toggle="tab" href="#tab-relance_impaye" role="tab" aria-controls="tab-relance_impaye" aria-selected="false">Relances impayés</a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-2" id="lotTabContent">
        <div class="tab-pane fade show active" id="tab-dossier_incomplets" role="tabpanel" aria-labelledby="dossier_incomplets-tab">
            <?php $this->load->view('dossier_incomplet/dossier_incomplet'); ?>
        </div>
        <div class="tab-pane fade" id="tab-devis" role="tabpanel" aria-labelledby="devis-tab">
            <div class="alert alert-primary text-center" role="alert">
                En cours de développement...
            </div>
        </div>
        <div class="tab-pane fade" id="tab-preparation_facture" role="tabpanel" aria-labelledby="preparation_facture-tab">
            <?php $this->load->view('preparation_facture/page_preparation'); ?>
        </div>
        <div class="tab-pane fade" id="tab-facture" role="tabpanel" aria-labelledby="facture-tab">
            <?php $this->load->view('facture/facture'); ?>
        </div>
        <div class="tab-pane fade" id="tab-paiements" role="tabpanel" aria-labelledby="paiements-tab">
            <?php $this->load->view('paiements/page_paiement'); ?>
        </div>
        <div class="tab-pane fade" id="tab-fichierSepa" role="tabpanel" aria-labelledby="fichierSepa-tab">
            <?php $this->load->view('fichierSepa/fichierSepa'); ?>
        </div>
        <div class="tab-pane fade" id="tab-relance_impaye" role="tabpanel" aria-labelledby="relance_impaye-tab">
            <div class="alert alert-primary text-center" role="alert">
                En cours de développement...
            </div>
        </div>
    </div>
</div>