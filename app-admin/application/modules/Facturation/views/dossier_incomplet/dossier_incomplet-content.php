<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Liste des lots sans mandat</h4>
    </div>
    <div class="px-3">

    </div>
</div>
<br>
<div class="container-list">
    <div class="row m-0">
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Propriétaire</label>
                <input class="form-control mb-0" type="text" id="proprietaire_lot_sans_mandat" name="proprietaire_lot_sans_mandat">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" id="programme_lot_sans_mandat" name="programme_lot_sans_mandat">
            </div>
        </div>
    </div>
</div>

<div class="w-100" id="content-table-dossier_incomplet">

</div>

<script>
    getTableDossier_incomplet();
</script>