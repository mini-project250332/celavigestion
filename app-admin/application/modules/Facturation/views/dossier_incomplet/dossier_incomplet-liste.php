<?php
$tab_etat = [
    1 => 'Crée',
    2 => 'Envoyé',
    5 => 'Rétracté',
    6 => 'Cloturé',
    7 => 'Suspendu'
]
?>

<div class="row m-0 mb-2 mt-2">
    <b>Resultat(s) : <?= $total ?> enregistré(s)</b>
</div>
<input type="hidden" name="pagination_view" id="pagination_view">
<div class="row m-0 mb-2 mt-2">
    <div class="col">
        <div class="table">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <th>Numéro du dossier</th>
                        <th>Nom propriétaires</th>
                        <th>Num lot concerné</th>
                        <th>Nom programme lot concerné</th>
                        <th>Etat </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($lot_sans_mandat)) : ?>
                        <?php foreach ($lot_sans_mandat as $key => $value) : ?>
                            <tr>
                                <td class="text-center" style="cursor: pointer;" title="Chargé de clientèle : <?= $value->user != null ? $value->user : 'Non renseigné' ?>"><?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td><?= $value->client_nom . ' ' . $value->client_prenom ?></td>
                                <td class=" text-center"> <?= $value->cliLot_id ?> </td>
                                <td><?= $value->progm_nom ?></td>
                                <td class="text-center"><?= array_key_exists($value->etat_mandat_id, $tab_etat) ? $tab_etat[$value->etat_mandat_id] : 'Sans mandat'; ?></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn btn-outline-primary ficheLot" data-id="<?= $value->cliLot_id ?>" data-client_id="<?= $value->client_id ?>" data-nom="<?= $value->client_nom ?>">
                                        <span class="fas fa-play"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-<?= $value; ?>" class="page-item pagination-vue-table <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>