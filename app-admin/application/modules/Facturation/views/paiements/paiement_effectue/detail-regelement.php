<?php
function formatHeure($date)
{
    $heureMin = date('H:i', strtotime($date)); // Récupère l'heure et les minutes
    // Vérifie si l'heure est minuit (00:00:00)
    if ($heureMin == '00:00') {
        return '';
    } else {
        list($heures, $minutes) = explode(':', $heureMin);
        return sprintf('à %02dh : %02dmin', $heures, $minutes);
    }
}
?>

<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Détails du réglement numéro <?= $reglement->idc_reglements ?> </h5>
</div>
<div class="modal-body">
    <div class="row mb-3">
        <div class="col">
            Règlement saisi par <?= $reglement->util_nom . ' ' . $reglement->util_prenom ?> le <?= date('d/m/Y', strtotime($reglement->date_action)) ?> <?= formatHeure($reglement->date_action) ?>
        </div>

    </div>

    <div class="row mb-3">
        <div class="col">
            <b>Information :</b> <br>
            Libellé du règlement : <?= ($reglement->mode_reglement == 3) ? 'Prélèvements honoraires CELAVIGestion du ' . date('d/m/Y', strtotime($reglement->date_reglement)) : $reglement->libelle_reglement ?>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>