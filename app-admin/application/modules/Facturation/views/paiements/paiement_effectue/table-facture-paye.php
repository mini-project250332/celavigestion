<?php
function extraireNomClient($inputString)
{
    $noms = explode(',', $inputString);
    $premierNom = $noms[0];
    $parts = preg_split('/\s+|-/', $premierNom, 3);
    if (count($parts) == 3) {
        $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
    } else {
        $nomExtrait = trim($parts[0]);
    }

    return $nomExtrait;
} ?>

<?php
$regls_type = array(
    0 => 'REGLEMENT',
    1 => 'REMBOURSEMENT',
    2 => 'REJET DE PRELEVEMENT'
);
?>

<?php
$classeBtn = array(
    0 => 'success',
    1 => 'danger',
    2 => 'warning'
);
?>
<div class="w-100 row mt-3 mb-2">
    <div class="col col mt-2 ms-3">
        <input type="hidden" name="pagination_view_facturePaye" id="pagination_view_facturePaye">
        <input type="hidden" name="row_view_facturePaye" id="row_view_facturePaye" value="100">
        <h6 class="mb-0 fw-semi-bold"> Resulat(s) :
            <?= $countAllResult ?> facture(s) payée(s)
        </h6>
    </div>


</div>

<div class="row  w-100 m-0 mb-2 mt-2">
    <div class="col">
        <div class="table-wrapper">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">

                        <th>Numéro du règlement </th>
                        <th>Nom propriétaires</th>
                        <th>Num dossier</th>
                        <th>Factures concernées</th>
                        <th>Type</th>
                        <th>Mode de paiement</th>
                        <th width="10%">Date de règlement</th>
                        <th>Montant règlement</th>
                        <th>Montant en trop perçu</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($facture)) :
                        foreach ($facture as $key => $value) :
                            $ventilations = json_decode($value->reglement_ventilations);
                            $countsVentilation = count($ventilations);     ?>
                            <tr>
                                <td class="text-center"><?= $value->idc_reglements ?></td>
                                <td><?= $ventilations[0]->clients ?></td>
                                <?php $user = $value->util_prenom != null && $value->util_nom != null ?  $value->util_prenom . ' ' . $value->util_nom : 'Non renseigné' ?>
                                <td class="text-center" style="cursor: pointer;" title="Chargé de clientèle : <?= $user ?>"><?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td class="text-center">
                                    <?php foreach ($ventilations as $ventile) : if ($ventile->fact_num != null) : ?>
                                            <button type="button" style="font-size: small;" class="btn btn-link btn-sm" onclick="forceDownload('<?= addslashes(base_url() . $ventile->docpath) ?>')"><?= $ventile->fact_num ?></button> <br>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td class="text-center"><span class="badge badge-soft-<?= isset($classeBtn[$value->reglement_type]) ? $classeBtn[$value->reglement_type] : '' ?> fs--1"><?= isset($regls_type[$value->reglement_type]) ? $regls_type[$value->reglement_type] : '' ?></span></td>
                                <td class="text-center text-nowrap"><?= $value->libelle ?> &nbsp;</td>
                                <td class="text-center"><?= date('d/m/Y', strtotime($value->date_reglement)) ?></td>
                                <td class="text-end"><?= format_number_($value->montant_total) ?> €</td>
                                <td class="text-end">
                                    <?php foreach ($ventilations as $ventile) : if ($ventile->fact_num == null) : ?>
                                            <?= format_number_($ventile->montant) ?> €
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td class="text-center" style="white-space: nowrap;">
                                    <button class="btn btn-sm btn-outline-primary fiche-dossier" title="Accéder au dossier" data-client_id="<?= intval(extraireNomClient($ventilations[0]->clientsId)) ?>" data-dossier_id="<?= $value->dossier_id ?>" data-nom="<?= extraireNomClient($ventilations[0]->clients) ?>" type="button">
                                        <i class="fas fa-play"></i>
                                    </button>

                                    <button type="button" class="btn btn-sm btn-outline-primary detailreglement" title="Afficher les détails du règlement" data-reglementId="<?= $ventilations[0]->idc_reglementVentilation ?>">
                                        <i class="fas fa-info-circle"></i>
                                    </button>
                                    <!-- 
                                    <button type="button" title="Enregistrer un remboursement" <?= $value->reglement_type == 0 ? '' : 'disabled'  ?> class="btn btn-sm btn-outline-danger saisir_Remboursements" data-reglementId="<?= $value->idc_reglements ?>">
                                        <i class="fas fa-money-bill"></i> <i class="fas fa-minus-circle" style="height: 10px; width: 10px; margin-left: -6px; margin-bottom: 6px; padding: 0.5px;"></i>
                                    </button> -->
                                </td>
                            </tr>
                    <?php endforeach;
                    endif;
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul-facturePaye" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview-FacturePaye">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-facturePaye-<?= $value; ?>" class="page-item pagination-vue-table-facturePaye <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
                <li class="page-item" id="pagination-vue-next-factureiPaye">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>