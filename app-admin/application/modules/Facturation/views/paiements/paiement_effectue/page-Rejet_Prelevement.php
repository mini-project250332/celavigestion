<?php echo form_tag('Facturation/Rejet_Prelevement/saveRejet_Prelevement', array('method' => "post", 'class' => '', 'id' => 'saveRejet_Prelevement')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Enregistrement d'un Rejet_Prelevement pour le règlement <b><?= $reglementdata->idc_reglements ?></b> </h4>
    </div>
    <div class="px-2">
        <button type="button" class="btn btn-sm btn-secondary" id="annulerRejet_Prelevement"> Annuler</button> &nbsp;
        <button type="submit" class="btn btn-sm btn-primary" id="enregistrerRejet_Prelevement"> Enregister</button>&nbsp;
    </div>
</div>

<input type="hidden" name="idreglementRembourse" id="idreglementRembourse" value="<?= $reglementdata->idc_reglements ?>">
<input type="hidden" name="dossierRejet_Prelevement" id="dossierRejet_Prelevement" value="<?= $reglementdata->dossier_id ?>">
<input type="hidden" name="montanPercus" id="montanPercus" value="<?= $montantTroPerçus ?>">

<div class="row w-100 m-0 mt-4">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="mode_Rejet_Prelevement" class="form-label">Mode de règlement * : </label>
                <select name="mode_Rejet_Prelevement" id="mode_Rejet_Prelevement" class="form-select">
                    <?php foreach ($mode_paiement as $mode) : ?>
                        <option value="<?= $mode->id_mode_paie ?>"><?= $mode->libelle  ?></option>
                    <?php endforeach ?>
                </select>
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="banque_celavi" class="form-label">Choix de la banque CELAVI * : </label>
                <select name="banque_celavi_Rejet_Prelevement" id="banque_celavi_Rejet_Prelevement" class="form-select">
                    <?php foreach ($banque as $banques) : ?>
                        <option value="<?= $banques->banque_id ?>"><?= 'IBAN: ' . $banques->banque_iban . ' -BIC: ' . $banques->banque_bic  ?></option>
                    <?php endforeach ?>
                </select>
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="dateRejet_Prelevement" class="form-label">Date de Rejet_Prelevement * :</label>
                <input type="date" class="form-control" name="dateRejet_Prelevement" id="dateRejet_Prelevement" data-require="true" value="<?= date('Y-m-d') ?>">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="montant_sur_Rejet_Prelevement" class="form-label">Montant * :</label>
                <input type="text" class="form-control text-end chiffre" name="montant_sur_Rejet_Prelevement" id="montant_sur_Rejet_Prelevement" data-require="true" min="0" value="<?= $montantTroPerçus == 0 ? format_number_($reglementdata->montant_total) : format_number_($montantTroPerçus) ?>">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>

<div class="row w-100 m-0 mb-4">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="form-group inline">
            <div>
                <label data-width="400" for="libelle" class="form-label">Libellé du Rejet_Prelevement :</label>
                <input type="text" class="form-control" name="libelle_Rejet_Prelevement" id="libelle_Rejet_Prelevement">
                <div class="help"></div>
            </div>
        </div>
    </div>
    <div class="col-3"> </div>
</div>
<?php echo form_close(); ?>