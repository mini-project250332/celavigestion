<div>
    <ul class="nav nav-tabs" id="lotTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="facturation_impaye-tab" data-bs-toggle="tab" href="#tab-facturation_impaye" role="tab" aria-controls="tab-facturation_impaye" aria-selected="true">Factures impayées</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="paiement_effectue" data-bs-toggle="tab" href="#tab-paiement_effectue" role="tab" aria-controls="tab-paiement_effectue" aria-selected="false">Paiements effectués </a>
        </li>
    </ul>

    <div class="tab-content border-x border-bottom p-2" id="lotTabContent">
        <div class="tab-pane fade show active" id="tab-facturation_impaye" role="tabpanel" aria-labelledby="facturation_impaye-tab" style="overflow-y: hidden !important;">
            <?php $this->load->view('paiements/facture_impaye/page_facture_impaye'); ?>
        </div>
        <div class="tab-pane fade" id="tab-paiement_effectue" role="tabpanel" aria-labelledby="paiement_effectue-tab" style="overflow-y: hidden !important;">
            <div class="tab-pane fade show active" id="tab-paiement_effectue" role="tabpanel" aria-labelledby="paiement_effectue-tab" style="overflow-y: hidden !important;">
                <?php $this->load->view('paiements/paiement_effectue/page_facture_paye'); ?>
            </div>
        </div>
    </div>
</div>