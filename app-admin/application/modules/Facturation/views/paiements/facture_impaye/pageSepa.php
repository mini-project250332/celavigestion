<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Enregistrement fichier sepa</h4>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-primary" id="retourFactureImpaye" disabled name="retourFactureImpaye"> <span>
                <i class="fas fa-undo"></i>
            </span> Factures impayées</button>&nbsp;
        <button class="btn btn-sm btn-secondary" id="export_xml" name="export_xml"> Genérer fichier Xml</button>&nbsp;
    </div>
</div>




<div class="row contenair-title w-100 m-0 mb-2 mt-2">
    <div class="col">
        <h4> Liste des prélèvements effectués</h4>
    </div>
</div>

<div class="row w-100 m-0 mb-2 mt-3">
    <div class="table-wrapper">
        <table class="table table-hover table-sm fs--1 mb-0">
            <thead class="text-900 fixed-header text-center">
                <tr class="text-center">
                    <th>Dossier</th>
                    <th>Propriétaires</th>
                    <th>RUM </th>
                    <th>IBAN</th>
                    <th>BIC</th>
                    <th>Numéro de facture</th>
                    <th>Montant Prélevé</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total_montant = 0;
                $total_reste_paye = 0;
                foreach ($facture as $fact) :

                ?>
                    <tr class="lignePrelevement" data-reglement_id="<?= $fact->idc_reglements ?>">
                        <td class="text-center" id="dosssier_<?= $fact->fact_id ?>"><?= str_pad($fact->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                        <td><?= $fact->clients ?></td>
                        <input type="hidden" class="idprelevemnt" value="<?= $fact->idc_prelevement ?>">
                        <td class="text-center"><?= $fact->sepa_numero ?></td>
                        <td class="text-center"><?= $fact->iban ?></td>
                        <td class="text-center"><?= $fact->bic ?></td>
                        <td class="text-center"><?= $fact->fact_num ?></td>
                        <td class="text-end"><?= format_number_($fact->montant_total) ?></td>
                    </tr>
                    <?php
                    $total_montant += $fact->montant_total;
                    ?>
                <?php endforeach ?>
            </tbody>
            <tfoot class="bg-200">
                <tr>
                    <td colspan="6" class="text-center"><b> TOTAL </b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($total_montant) ?> €</b></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>