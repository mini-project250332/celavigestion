<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Liste des factures impayés</h4>
    </div>
    <div class="px-3">
        <select name="annee_facturationImpaye" id="annee_facturationImpaye" class="form-select">
            <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")); $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="container-list">
    <div class="row m-0">
    <div class="col mb-0">
        <div class="mb-2">
                <label class="form-label">Nom propriétaires</label>
                <input class="form-control mb-0" type="text" id="factureImpaye_filtre_proprietaire" name="factureImpaye_filtre_proprietaire">
            </div>
        </div>

        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Numéro de facture</label>
                <input class="form-control mb-0" type="text" id="factureImpaye_filtre_facture" name="factureImpaye_filtre_facture">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" id="factureImpaye_filtre_progrm" name="factureImpaye_filtre_progrm">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Num dossier</label>
                <input class="form-control mb-0" type="text" id="num_dossier_factureImpaye" name="num_dossier_factureImpaye">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Mode de règlement</label>
                <select name="regleId" id="regleId" class="form-select">
                    <option value="0">
                        Tous
                    </option>
                    <?php foreach ($mode_paiement as $key => $paie) : ?>
                        <option value="<?= $paie->id_mode_paie ?>"><?= $paie->libelle ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>

        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">RUM (référence unique de mandat)</label>
                <input class="form-control mb-0" type="text" id="num_rum_factureImpaye" name="num_rum_factureImpaye">
            </div>
        </div>
    </div>
</div>

<div class="w-100" id="filtre-impaye-content">

</div>

<script>
    getfiltreFacturationImpaye(<?= date('Y') ?>);
</script>