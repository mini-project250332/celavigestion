<?php

use phpDocumentor\Reflection\Types\Null_;

function extraireNomClient($inputString)
{
    $noms = explode(', ', $inputString);
    $premierNom = $noms[0];
    $parts = preg_split('/\s+|-/', $premierNom, 3);
    if (count($parts) == 3) {
        $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
    } else {
        $nomExtrait = trim($parts[0]);
    }

    return $nomExtrait;
} ?>

<div class="w-100 row mt-3 mb-2">
    <div class="col col mt-2 ms-3">
        <input type="hidden" name="pagination_view_factureimpaye" id="pagination_view_factureimpaye">
        <input type="hidden" name="row_view_factureimpaye" id="row_view_factureimpaye" value="100">
        <h6 class="mb-0 fw-semi-bold"> Resulat(s) :
            <?= $countAllResult ?> facture(s) impayée(s)
        </h6>
    </div>

    <div class="col text-end">
        <button class="btn btn-sm btn-primary" id="enregistrer_prelevement" disabled>
            Enregistrer un prélèvement
        </button>
    </div>
</div>

<div class="row  w-100 m-0 mb-2 mt-2">
    <div class="col">
        <div class="table-wrapper">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <td class="bg-white text-center" width="1%"> <input type="checkbox" class="form-check-input" id="tous_cocher_impaye" name=""></td>
                        <th>Numéro de facture</th>
                        <th>Nom propriétaires</th>
                        <th>Num dossier</th>
                        <th>Num lots concerné</th>
                        <th>Nom programme concernés lots</th>
                        <th>date facture</th>
                        <th width="10%">mode de règlement</th>
                        <th>Montant HT</th>
                        <th>Montant TVA</th>
                        <th>Montant TTC</th>
                        <th> reste à payer</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($facture)) : ?>
                        <?php foreach ($facture as $key => $value) : ?>
                            <tr>
                                <td class="bg-white" width="1%">
                                    <?php if (($value->sepa_id == NULL || $value->sepa_date_signature == Null) && $value->id_mode_paie == 3) : ?>
                                        <span style="color: red; font-weight: bold; font-size: 1.2em;"><i class="fas fa-exclamation-circle"></i></span>
                                    <?php else : ?>
                                        <input type="checkbox" <?php if ($value->id_mode_paie != 3 || $value->fact_montantttc < 0) echo 'disabled'; ?> class="form-check-input facture_impaye" data-fact_id="<?= $value->fact_id ?>">
                                    <?php endif; ?>
                                </td>
                                <td class="text-center"><?= $value->fact_num ?></td>
                                <td><?= $value->client_nom ?></td>
                                <td class="text-center" style="cursor: pointer;" title="Chargé de clientèle : <?= $value->user != null ? $value->user : 'Non renseigné' ?>"><?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td class="text-center"><?= $value->cliLot_id ?></td>
                                <td><?= $value->progm_nom ?></td>
                                <td class="text-center"><?= date('d/m/Y', strtotime($value->fact_date)) ?></td>
                                <td class="text-nowrap">
                                    <?php if ($value->fact_montantttc < 0) : ?>
                                        <div class="text-center" style="color: orange; font-weight: bold;">Avoir</div>
                                    <?php else : ?>
                                        <div class="text-end">
                                            <?= $value->libelle ?> &nbsp;
                                            <button type="button" class="btn btn-sm <?= $value->fact_reglementetat == 0 ? 'btn-primary' : 'btn-success' ?> modifier_reglement" data-fact_id="<?= $value->fact_id ?>">
                                                <i class="fas fa-pen"></i>
                                            </button>
                                            <?php if (($value->sepa_id == NULL || $value->sepa_date_signature == Null) && $value->id_mode_paie == 3) : ?>
                                                <div style="color: red; font-weight: bold;">Infos SEPA manquantes !</div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </td>
                                <td class="text-end text-nowrap"><?= format_number_($value->fact_montantht) ?> €</td>
                                <td class="text-end text-nowrap"><?= format_number_($value->fact_montanttva) ?> €</td>
                                <td class="text-end text-nowrap"><?= format_number_($value->fact_montantttc) ?> €</td>
                                <?php $reste_a_payer = $value->reste_a_payer != NULL ? abs($value->reste_a_payer) : $value->fact_montantttc ?>
                                <td class="text-end text-nowrap"><?= format_number_($reste_a_payer) ?> €</td>
                                <td class="text-center" style="white-space: nowrap;">
                                    <button class="btn btn-outline-primary icon-btn p-0 m-1 align-items-center justify-content-center fiche-dossier" title="Accéder au dossier" data-client_id="<?= intval($value->client_id) ?>" data-dossier_id="<?= $value->dossier_id ?>" data-nom="<?= extraireNomClient($value->client_nom) ?>" type="button" style="height:30px;width: 30px;">
                                        <i class="fas fa-play"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-outline-success" title="Télécharger la facture" onclick="forceDownload('<?= addslashes(base_url() . $value->doc_facturation_path) ?>')"><span class="fas fa-download"></span></button>
                                    <button type="button" class="btn btn-sm btn-outline-primary saisir_reglements" title="Enregistrer un règlement" data-fact_id="<?= $value->fact_id ?>" <?= $value->libelle  == 'Prélèvement' || $value->fact_montantttc < 0  ? 'disabled' : '' ?>>
                                        <i class="fas fa-money-bill"></i>

                                    </button>
                                    </button>
                                    <button type="button" title="créer un avoir" class="btn btn-sm btn-outline-danger creerAvoir" data-fact_id="<?= $value->fact_id ?>" <?= $value->fact_montantttc < 0  ? 'disabled' : '' ?>>
                                        <i class="fas fa-file-invoice-dollar"></i> <i class="fas fa-minus-circle" style="height: 10px; width : 10px; margin-left: -6px; margin-bottom: 6px;"></i>
                                    </button>
                                    <button type="button" title="Enregistrer un remboursement" <?= $value->fact_idFactureDu == null ? 'disabled' : ''  ?> class="btn btn-sm btn-outline-danger saisir_RemboursementsAvoir" data-fact_id="<?= $value->fact_id ?>">
                                        <i class="fas fa-money-bill"></i> <i class="fas fa-minus-circle" style="height: 10px; width: 10px; margin-left: -6px; margin-bottom: 6px; padding: 0.5px;"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-outline-primary" title="Envoyer un e-mail" data-dossier_id="">
                                        <i class="fas fa-envelope"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul-factureimpaye" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview-Factureimpaye">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-factureImpaye-<?= $value; ?>" class="page-item pagination-vue-table-factureImpaye <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next-factureimpaye">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>