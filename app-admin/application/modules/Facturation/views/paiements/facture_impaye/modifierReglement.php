<div class="modal-header bg-primary">
    <h5 class="modal-title text-white" id="exampleModalToggleLabel">Modification du mode de règlement</h5>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-4">
            <label for="exampleInput">Mode de règlement :</label>
        </div>
        <div class="col-8">
            <input type="hidden" id="reglement_fact_id" value="<?= $facture->fact_id ?>">
            <select class="form-select" name="nouveau_mode_reglement" id="nouveau_mode_reglement">
                <?php foreach ($mode_paiement as $mode) : ?>
                    <?php if ($mode->id_mode_paie != 1) : ?>
                        <option value="<?= $mode->id_mode_paie ?>" <?= $facture->fact_modepaiement == $mode->id_mode_paie ? 'selected' : '' ?>><?= $mode->libelle ?></option>
                    <?php endif  ?>
                <?php endforeach  ?>
            </select>
        </div>
    </div> <br>
    <div class="row">
        <div class="col-4">
            <label for="exampleInput">Commentaire :</label>
        </div>
        <div class="col-8">
            <textarea class="form-control" name="nouveau_commentaire" id="nouveau_commentaire" style="height: 100px;"><?= $facture->fact_commentaire ?></textarea>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="confirmerChangementregl">Confirmer</button>
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>