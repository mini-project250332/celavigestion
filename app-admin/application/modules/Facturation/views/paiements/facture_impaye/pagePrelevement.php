<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Enregistrement d'un prélèvement</h4>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-secondary" id="annulerPrelevement"> Annuler</button> &nbsp;
        <button class="btn btn-sm btn-primary" id="Saveprelevement"> Enregister</button>&nbsp;
    </div>
</div>

<div class="row w-100 m-0 mb-2 mt-3">
    <div class="col-5">
        <div class="row">
            <div class="col-4">
                <label for="banque_celavi" class="form-label">Choix de la banque CELAVI (*) : </label>
            </div>
            <div class="col">
                <select name="banque_celavi" id="banque_celavi" class="form-select">
                    <?php foreach ($banque as $banques) : ?>
                        <option value="<?= $banques->banque_id ?>"><?= 'IBAN: ' . $banques->banque_iban . ' -BIC: ' . $banques->banque_bic  ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-7"> </div>
</div>

<div class="row w-100 m-0 mb-2 mt-3">
    <div class="col-5">
        <div class="row">
            <div class="col-4">
                <label for="dateprelevement" class="form-label">Date du prélèvement à effectuer :</label>
            </div>
            <div class="col">
                <input type="date" class="form-control" name="dateprelevement" id="dateprelevement">
                <div id="dateError" style="color: red; font-size: 14px;"></div>
            </div>
        </div>
    </div>
    <div class="col-7"> </div>
</div>

<div class="row contenair-title w-100 m-0 mb-2 mt-2">
    <div class="col">
        <h4 class="fw-bold">Prélèvements à effectuer</h4>
    </div>
</div>

<div class="row w-100 m-0 mb-2 mt-3">
    <div class="table-wrapper">
        <table class="table table-hover table-sm fs--1 mb-0">
            <thead class="text-900 fixed-header text-center">
                <tr class="text-center">
                    <th>Dossier</th>
                    <th>Propriétaires</th>
                    <th>RUM </th>
                    <th>IBAN</th>
                    <th>BIC</th>
                    <th>Numéro de facture</th>
                    <th>Montant Facturé</th>
                    <th>Reste à payer</th>
                    <th width="12%">à prélever</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total_montant_ttc = 0;
                $total_reste_paye = 0;
                foreach ($facture as $fact) :

                ?>
                    <tr class="lignefacture" data-facture_id="<?= $fact->fact_id ?>">
                        <td class="text-center" id="dosssier_<?= $fact->fact_id ?>"><?= str_pad($fact->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                        <td><?= $fact->clients ?></td>
                        <td class="text-center"><?= $fact->sepa_numero ?></td>
                        <td>

                            <?= $fact->iban ?>
                            <?php if (!verifyIBAN($fact->iban)) : ?>
                                <div style="color: red; font-weight: bold;">IBAN est incorrect.</div>
                            <?php endif; ?>

                        </td>
                        <td class="text-center"><?= $fact->bic ?></td>
                        <td class="text-center"><?= $fact->fact_num ?></td>
                        <td class="text-end"><?= format_number_($fact->fact_montantttc) ?></td>
                        <?php $reste_a_payer = $fact->reste_a_payer != NULL ? abs($fact->reste_a_payer) : $fact->fact_montantttc ?>
                        <td class="text-end"><?= format_number_($reste_a_payer) ?></td>
                        <td>
                            <div class="input-group">
                                <input type="text" onfocusout="verifierValeur(this);" id="montantreglement_<?= $fact->fact_id ?>" class="form-control montantAPrelever custom-input" placeholder="" aria-label="Amount" aria-describedby="basic-addon1" value="<?= format_number_($reste_a_payer) ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-index" id="basic-addon1" style="height: 32.5px; padding : 3px;">€</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $total_montant_ttc += $fact->fact_montantttc;
                    $total_reste_paye += $reste_a_payer;
                    ?>
                <?php endforeach ?>
            </tbody>
            <tfoot class="bg-200">
                <tr>
                    <td colspan="6" class="text-center"><b> TOTAL </b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($total_montant_ttc) ?> €</b></td>
                    <td class="text-end text-nowrap"><b><?= format_number_($total_reste_paye) ?> €</b></td>
                    <td class="text-end text-nowrap"><b id="montantTotal_preleve"><?= format_number_($total_reste_paye) ?> €</b></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dateprelevement').val(getDefaultDate());
    });
</script>