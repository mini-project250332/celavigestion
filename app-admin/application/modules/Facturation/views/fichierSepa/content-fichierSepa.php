<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Liste des prélèvements</h4>
    </div>
    <div class="px-3">
        <select name="" id="" class="form-select">
            <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")); $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>
<br>
<div class="container-list">
    <div class="row m-0">
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Date de prélèvement</label>
                <input class="form-control mb-0" type="date" id="date_prelevement" name="date_prelevement">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Numéro prélèvement</label>
                <input class="form-control mb-0" type="text" id="num_prelevement" name="num_prelevement">
            </div>
        </div>
    </div>
</div>

<div class="w-100" id="content-table-prelevement">

</div>

<script>
    listePrelevement();
</script>