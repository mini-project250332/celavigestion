<div class="row m-0 mb-2 mt-2">
    <div class="col col mt-2 ms-3">
        <b>Resultat(s) : <?= $total ?> enregistré(s)</b>
    </div>

    <div class="col text-end">
        <button class="btn btn-sm btn-primary" id="telechargertous_prelevement" disabled>
            Télécharger prélèvement(s)
        </button>
    </div>
</div>

<input type="hidden" name="pagination_view_prelev" id="pagination_view_prelev">
<div class="row m-0 mb-2 mt-2">
    <div class="col">
        <div class="table">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <td class="bg-white text-center" width="1%"> <input type="checkbox" class="form-check-input" id="tous_cocher_prelevement" name=""></td>
                        <th>Numéro prélèvement</th>
                        <th>Date prélèvement</th>
                        <th>Libellé prélèvement</th>
                        <th>BIC CELAVI</th>
                        <th>IBAN CELAVI</th>
                        <th>Montant Total prélèvement </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($prelevement)) : ?>
                        <?php foreach ($prelevement as $key => $value) : ?>
                            <tr>
                                <td class="bg-white" width="1%">
                                    <input type="checkbox" class="form-check-input facture_prelevement" data-doc_prelevement_id="<?= $value->doc_prelevement_id ?>">
                                </td>
                                <td class="text-center"><?= $value->idc_prelevement ?></td>
                                <td class="text-center"><?= date('d/m/Y', strtotime($value->date_encaissement)) ?></td>
                                <td class="text-center">Prélèvements honoraires CELAVIGestion du <?= date('d/m/Y', strtotime($value->date_encaissement)) ?></td>
                                <td class="text-center"><?= $value->banque_bic ?></td>
                                <td class="text-center"><?= $value->banque_iban ?></td>
                                <td class="text-end"><?= format_number_($value->total_reglements) ?> €</td>
                                <td class="text-center">
                                    <button type="button" title="Télécharger fichier xml" class="btn btn btn-outline-success" <?php if ($value->doc_prelevement_path == null) echo 'disabled' ?> onclick="forceDownload('<?= addslashes(base_url() . $value->doc_prelevement_path) ?>')">
                                        <span class="fas fa-download"></span>
                                    </button>
                                    <button type="button" title="détails prélèvement" class="btn btn btn-outline-primary info_prelevement" data-idc_prelevement="<?= $value->idc_prelevement ?>">
                                        <span class="fas fa-info-circle"></span>
                                    </button>
                                    <button type="button" title="Régénérer fichier xml" class="btn btn btn-outline-secondary generationFichierXml" data-idprelevement="<?= $value->idc_prelevement ?>">
                                        <span class="fas fa-file-export"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-previewPrelevement">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-tablePrelevement-<?= $value; ?>" class="page-item pagination-vue-tablePrelevement <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-nextPrelevement">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>