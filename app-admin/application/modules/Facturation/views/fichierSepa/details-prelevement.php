<div class="modal-header bg-primary">
    <h5 class="modal-title text-white">Détails du prélèvement numéro <?= $prelevement->idc_prelevement ?> </h5>
</div>
<div class="modal-body">
    <div class="row mb-3">
        <div class="col">
            Banque CELAVI : IBAN-<?= $prelevement->banque_iban ?> BIC-<?= $prelevement->banque_bic ?>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col">
            Date du prélèvement : <?= date('d/m/Y', strtotime($prelevement->date_encaissement)) ?>
        </div>
    </div>

    <div class="row">
        <div class="table-wrapper">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <th>Dossiers</th>
                        <th>Propriétaires</th>
                        <th>RUM </th>
                        <th>IBAN</th>
                        <th>BIC</th>
                        <th>Numéro facture</th>
                        <!-- <th>Montant Facturé</th> -->
                        <!-- <th>Reste à payer</th> -->
                        <th>Montant prélevé</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_montant_ttc = 0;
                    $total_reste_paye = 0;
                    $total_prelever = 0;
                    foreach ($facture as $fact) :
                    ?>
                        <tr data-facture_id="<?= $fact->fact_id ?>">
                            <td class="text-center" id="dosssier_<?= $fact->fact_id ?>"><?= str_pad($fact->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                            <td><?= $fact->clients ?></td>
                            <td class="text-center"><?= $fact->sepa_numero ?></td>
                            <td><?= $fact->iban ?></td>
                            <td class="text-center"><?= $fact->bic ?></td>
                            <td class="text-center"><?= $fact->fact_num ?></td>
                            <!-- <td class="text-end"><?= format_number_($fact->fact_montantttc) ?></td> -->
                            <!-- <?php $reste_a_payer = $fact->reste_a_payer != NULL ? $fact->reste_a_payer : $fact->fact_montantttc ?>
                            <td class="text-end"><?= format_number_($reste_a_payer) ?></td> -->
                            <!-- <td class="text-end"></td> -->
                            <td class="text-end">
                                <?= format_number_($fact->montant_total) ?>
                            </td>
                        </tr>
                        <?php
                        $total_montant_ttc += $fact->fact_montantttc;
                        $total_reste_paye += $reste_a_payer;
                        $total_prelever += $fact->montant_total
                        ?>
                    <?php endforeach ?>
                </tbody>
                <tfoot class="bg-200">
                    <tr>
                        <td colspan="6" class="text-center"><b> TOTAL </b></td>
                        <!-- <td class="text-end text-nowrap"><b><?= format_number_($total_montant_ttc) ?> €</b></td> -->
                        <!-- <td class="text-end text-nowrap"><b><?= format_number_($total_reste_paye) ?> €</b></td> -->
                        <td class="text-end text-nowrap"><b id="montantTotal_preleve"><?= format_number_($total_prelever) ?> €</b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>