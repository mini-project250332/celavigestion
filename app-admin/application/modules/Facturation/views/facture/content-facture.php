<div class="contenair-title">
    <div class="px-2">
        <h4 class="ms-2 fs-1">Liste des factures annuelle</h4>
    </div>
    <div class="px-3">
        <select name="tri_annee_facture" id="tri_annee_facture" class="form-select">
            <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")); $i--) { ?>
                <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                    <?= $i ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>
<br>
<div class="container-list">
    <div class="row m-0">
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Propriétaire</label>
                <input class="form-control mb-0" type="text" id="facture_filtre_proprietaire" name="facture_filtre_proprietaire">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Programme</label>
                <input class="form-control mb-0" type="text" id="facture_filtre_progrm" name="facture_filtre_progrm">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Numéro dossier</label>
                <input class="form-control mb-0" type="text" id="num_dossier_facture" name="num_dossier_facture">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Numéro facture</label>
                <input class="form-control mb-0" type="text" id="num_facture" name="num_facture">
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Paiements : </label>
                <select name="paiementEtat" id="paiementEtat" class="form-select">
                    <option value="">Tous</option> <!-- Modifier la valeur de l'option pour être vide -->
                    <option value="0">Factures impayées</option>
                    <option value="1">Factures payées</option>
                </select>
            </div>
        </div>
        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Date début</label>
                <input class="form-control mb-0" type="date" id="date_debut" name="date_debut">
            </div>
        </div>

        <div class="col mb-0">
            <div class="mb-2">
                <label class="form-label">Date fin</label>
                <input class="form-control mb-0" type="date" id="date_fin" name="date_fin">
            </div>
        </div>
    </div>
</div>

<div class="w-100" id="content-table-facture">

</div>

<script>
    getTableFacture(<?= date('Y') ?>);
</script>