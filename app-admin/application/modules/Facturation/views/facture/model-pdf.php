<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>

    <style>
        @page {
            margin: 2%;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid black;
            padding: 5px;
            text-align: center;
            font-size: 10px;
        }

        .nowrap {
            white-space: nowrap;
        }

        h2 {
            text-align: center;
        }

        .title-pdf h1{
            text-align: center;
            font-size: 14px;
            max-resolution: 10px;
        }
    </style>
</head>
<body>


<?php
    function extraireNomClient($inputString){
        $noms = explode(', ', $inputString);
        $premierNom = $noms[0];
        $parts = preg_split('/\s+|-/', $premierNom, 3);
        if (count($parts) == 3) {
            $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
        } else {
            $nomExtrait = trim($parts[0]);
        }

        return $nomExtrait;
    } 
?>

<div class="title-pdf">
    <h1> Factures définitives </h1>
</div>

<table class="table table-hover table-sm fs--1 mb-0">
    <thead class="text-900 fixed-header text-center">
        <tr class="text-center">
            <th>Numéro de facture</th>
            <th>Nom propriétaires</th>
            <th>Num dossier</th>
            <th>Num lots concerné</th>
            <th>Nom programme concernés lots</th>
            <th>date facture</th>
            <th>Montant HT</th>
            <th>Montant TVA</th>
            <th>Montant TTC</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($facture)) : ?>
            <?php foreach ($facture as $key => $value) : ?>
                <tr>
                    <td class="text-center"><?= $value->fact_num ?></td>
                    <td><?= $value->client_nom ?></td>
                    <td class="text-center"><?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                    <td class="text-center"><?= $value->cliLot_id ?></td>
                    <td><?= $value->progm_nom ?></td>
                    <td class="text-center"><?= date('d/m/Y', strtotime($value->fact_date)) ?></td>
                    <td class="text-end"><?= format_number_($value->fact_montantht) ?> €</td>
                    <td class="text-end"><?= format_number_($value->fact_montanttva) ?> €</td>
                    <td class="text-end"><?= format_number_($value->fact_montantttc) ?> €</td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>

</body>
</html>