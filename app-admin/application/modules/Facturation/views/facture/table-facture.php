<?php
function extraireNomClient($inputString)
{
    $noms = explode(', ', $inputString);
    $premierNom = $noms[0];
    $parts = preg_split('/\s+|-/', $premierNom, 3);
    if (count($parts) == 3) {
        $nomExtrait = trim($parts[0] . ' ' . $parts[1]);
    } else {
        $nomExtrait = trim($parts[0]);
    }

    return $nomExtrait;
} ?>

<div class="w-100 row mt-3 mb-2">
    <div class="col col mt-2 ms-3">
        <input type="hidden" name="pagination_view_NouvelleFacture" id="pagination_view_NouvelleFacture">
        <input type="hidden" name="row_view_NouvelleFacture" id="row_view_NouvelleFacture" value="100">
        <h6 class="mb-0 fw-semi-bold"> Resulat(s) :
            <?= $countAllResult ?> facture(s), &nbsp; <?= format_number_($totalHT) ?> € HT, <?= format_number_($totalTTC) ?> € TTC
        </h6>
    </div>

    <div class="col text-end">

        <button id="exportPdf_factures" type="button" class="btn btn-success">
            Exporter en PDF
        </button>

    </div>
</div>

<div class="row m-0 mb-2 mt-2">
    <div class="col">
        <div class="table-wrapper">
            <table class="table table-hover table-sm fs--1 mb-0">
                <thead class="text-900 fixed-header text-center">
                    <tr class="text-center">
                        <th>Numéro de facture</th>
                        <th>Nom propriétaires</th>
                        <th>Num dossier</th>
                        <th>Num lots concerné</th>
                        <th>Nom programme concernés lots</th>
                        <th>date facture</th>
                        <th>Montant HT</th>
                        <th>Montant TVA</th>
                        <th>Montant TTC</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($facture)) : ?>
                        <?php foreach ($facture as $key => $value) : ?>
                            <tr>
                                <td class="text-center"><?= $value->fact_num ?></td>
                                <td><?= $value->client_nom ?></td>
                                <td class="text-center" style="cursor: pointer;" title="Chargé de clientèle : <?= $value->user != null ? $value->user : 'Non renseigné' ?>"><?= str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT); ?></td>
                                <td class="text-center"><?= $value->cliLot_id ?></td>
                                <td><?= $value->progm_nom ?></td>
                                <td class="text-center"><?= date('d/m/Y', strtotime($value->fact_date)) ?></td>
                                <td class="text-end"><?= format_number_($value->fact_montantht) ?> €</td>
                                <td class="text-end"><?= format_number_($value->fact_montanttva) ?> €</td>
                                <td class="text-end"><?= format_number_($value->fact_montantttc) ?> €</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn btn-outline-success" onclick="forceDownload('<?= addslashes(base_url() . $value->doc_facturation_path) ?>')"><span class="fas fa-download"></span></button>
                                    <button type="button" class="btn btn btn-outline-primary fiche-dossier" data-client_id="<?= intval($value->client_id) ?>" data-dossier_id="<?= $value->dossier_id ?>" data-nom="<?= extraireNomClient($value->client_nom) ?>"><span class="fas fa-play"></span></button>
                                    <button type="button" title="créer un avoir" class="btn btn-sm btn-outline-danger creerAvoir" data-fact_id="<?= $value->fact_id ?>" <?= $value->fact_montantttc < 0  ? 'disabled' : '' ?>>
                                        <i class="fas fa-file-invoice-dollar"></i> <i class="fas fa-minus-circle" style="height: 10px; width : 10px; margin-left: -6px; margin-bottom: 6px;"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="d-flex justify-content-end pt-2">
    <div class="d-flex align-items-center p-0">
        <nav aria-label="Page navigation example">
            <ul id="pagination-ul-facturenouvelle" class="pagination pagination-sm mb-0">
                <li class="page-item" id="pagination-vue-preview-Facturenouvelle">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Previous">
                        Précédent
                    </a>
                </li>
                <?php foreach ($li_pagination as $key => $value) : ?>
                    <li data-offset="<?= $value; ?>" id="pagination-vue-table-facturenouvelle-<?= $value; ?>" class="page-item pagination-vue-table-facturenouvelle <?= (intval($active_li_pagination) == $value) ? 'active' : 'text-900'; ?>">
                        <a class="page-link - px-3 <?= (intval($active_li_pagination) == $value) ? '' : 'text-900'; ?>" href="javascript:void(0);">
                            <?= $key + 1; ?>
                        </a>
                    </li>
                <?php endforeach; ?>

                <li class="page-item" id="pagination-vue-next-facturenouvelle">
                    <a class="page-link text-900" href="javascript:void(0);" aria-label="Next">
                        Suivant
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>