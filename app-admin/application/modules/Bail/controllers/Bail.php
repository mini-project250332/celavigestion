<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bail extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Bail";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/bail/bail.js",
            "js/historique/historique.js",
            "js/loyer/loyer.js"
        );

        $this->_css_personnaliser = array(
            "css/bail/bail.css"
        );
    }

    public function index()
    {
    }

    public function AddBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Bail_m');
                $this->load->model('IndexationLoyer_m');
                $this->load->model('IndiceValeurLoyer_m');
                $this->load->model('MandatCli_m');

                $data_post = $_POST['data'];
                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout de bail"));

                $data_retour['request'] = $data_post;
                $data_retour['last_bail_id'] = null;
                $default_indloyer_id = $data_post['indloyer_id'];
                $paramIndLoyer = array(
                    'clause' => array(
                        'c_indice_valeur_loyer.indloyer_id' => $default_indloyer_id,
                    ),
                    'method' => 'row'
                );
                $valeurs = $this->IndiceValeurLoyer_m->get($paramIndLoyer);

                $data['bail_debut'] = $data_post['bail_debut'];
                $data['bail_fin'] = $data_post['bail_fin'];
                $data['bail_resiliation_triennale'] = $data_post['bail_resiliation_triennale'];
                $data['bail_loyer_variable'] = $data_post['bail_loyer_variable'];
                $data['bail_forfait_charge_indexer'] = $data_post['bail_forfait_charge_indexer'];
                $data['bail_remboursement_tom'] = $data_post['bail_remboursement_tom'];
                $data['bail_facturation_loyer_celaviegestion'] = $data_post['bail_facturation_loyer_celaviegestion'];
                $data['bail_charge_recuperable'] = $data_post['bail_charge_recuperable'];
                $data['bail_date_premiere_indexation'] = $data_post['bail_date_premiere_indexation'];
                $data['bail_date_prochaine_indexation'] = $data_post['bail_date_prochaine_indexation'];
                $data['bail_date_echeance'] = isset($data_post['bail_date_echeance']) ? $data_post['bail_date_echeance'] : NULL;
                $data['bail_commentaire'] = $data_post['bail_commentaire'];
                $data['pdl_id'] = $data_post['pdl_id'];
                $data['natef_id'] = $data_post['natef_id'];
                $data['tpba_id'] = $data_post['tpba_id'];
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['gestionnaire_id'] = $data_post['gestionnaire_id'];
                $data['preneur_id'] = $data_post['preneur_id'];
                $data['tpech_id'] = $data_post['tpech_id'];
                $data['bail_art_605'] = $data_post['bail_art_605'];
                $data['bail_art_606'] = $data_post['bail_art_606'];
                $data['indloyer_id'] = $data_post['indloyer_id'];
                $data['indval_id'] = isset($data_post['indval_id']) ? $data_post['indval_id'] : $valeurs->indval_id;
                $data['momfac_id'] = $data_post['momfac_id'];
                $data['prdind_id'] = $data_post['prdind_id'];
                $data['bail_annee'] = $data_post['bail_annee'];
                $data['bail_mois'] = $data_post['bail_mois'];

                $data['bail_var_capee'] = $data_post['bail_var_capee'];
                $data['bail_var_plafonnee'] = $data_post['bail_var_plafonnee'];
                $data['bail_var_min'] = $data_post['bail_var_min'];
                $data['bail_var_appliquee'] = $data_post['bail_var_appliquee'];
                $data['bail_mode_calcul'] = $data_post['bail_mode_calcul'];
                $data['indice_valeur_plafonnement'] = $data_post['indice_valeur_plafonnement'];

                if (!isset($data_post['bail_id']) || $data_post['bail_id'] === "") {
                    $data['bail_avenant'] = $data_post['bail_avenant'];
                    $data['bail_avenant_id_origin'] = $data_post['bail_avenant_id_origin'];
                }
                $data['premier_mois_trimes'] = $data_post['premier_mois_trimes'];

                //Date debut mission
                $mandat = $this->MandatCli_m->get(
                    array(
                        'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                        'columns' => array("mandat_cli_id", "mandat_cli_date_signature"),
                        'method' => 'row'
                    )
                );

                // $data['bail_date_debut_mission'] = $data_post['bail_date_debut_mission'] == null ? $mandat->mandat_cli_date_signature : $data_post['bail_date_debut_mission'];

                // verify bail origin if avenant, if avenant add origin as same origin
                if ($data_post['bail_avenant_id_origin']) {
                    $paramBailOrigin = array(
                        'clause' => array(
                            'bail_id' => $data_post['bail_avenant_id_origin'],
                        ),
                        'method' => 'row'
                    );
                    $bailOriginInfo = $this->Bail_m->get($paramBailOrigin);
                    if ($bailOriginInfo && $bailOriginInfo->bail_avenant == 1) {
                        $data['bail_avenant_id_origin'] = $bailOriginInfo->bail_avenant_id_origin;
                    }
                }

                $bail_id = $data_post['bail_id'] ? intval($data_post['bail_id']) : null;

                $clause = array("bail_id" => $bail_id);

                if ($bail_id == NULL) {
                    $request = $this->Bail_m->save_data($data);
                    $last_bail_id = $this->db->insert_id();

                    $data_retour['last_bail_id'] = $last_bail_id;

                    // Add empty indexation
                    $dataIndexation['indlo_debut'] = $data_post['bail_debut'];
                    $dataIndexation['indlo_fin'] = $data_post['bail_fin'];

                    // Default indice de base

                    $dataIndexation['indlo_indice_base'] = $valeurs->indval_id;
                    $dataIndexation['indlo_indice_reference'] = null;

                    $dataIndexation['indlo_appartement_ht'] = 0;
                    $dataIndexation['indlo_appartement_tva'] = 10;
                    $dataIndexation['indlo_appartement_ttc'] = 0;

                    $dataIndexation['indlo_parking_ht'] = 0;
                    $dataIndexation['indlo_parking_tva'] = 20;
                    $dataIndexation['indlo_parking_ttc'] = 0;

                    $dataIndexation['bail_id'] = $last_bail_id;

                    $dataIndexation['indlo_charge_ht'] = 0;
                    $dataIndexation['indlo_charge_tva'] = 20;
                    $dataIndexation['indlo_charge_ttc'] = 0;

                    $dataIndexation['indlo_autre_prod_ht'] = 0;
                    $dataIndexation['indlo_autre_prod_tva'] = 0;
                    $dataIndexation['indlo_autre_prod_ttc'] = 0;

                    $dataIndexation['indlo_variation_plafonnee'] = 0;
                    $dataIndexation['indlo_variation_capee'] = 0;
                    $dataIndexation['indlo_variation_loyer'] = 0;
                    $dataIndexation['indlo_variation_appliquee'] = 0;

                    $dataIndexation['indlo_ref_loyer'] = 1;

                    $this->IndexationLoyer_m->save_data($dataIndexation);
                } else {

                    // Verify if bail is validate before update
                    $paramBail = array(
                        'clause' => array(
                            'bail_id' => $bail_id,
                        ),
                        'method' => 'row'
                    );

                    $bailInfo = $this->Bail_m->get($paramBail);

                    // Save only if bail is still invalid
                    if (isset($bailInfo) && $bailInfo->bail_valide == 0) {
                        $request = $this->Bail_m->save_data($data, $clause);
                    }
                }

                $isBail = 'bail';

                if ($data_post['bail_avenant'] == 1) {
                    $isBail = 'avenant';
                    // $clause = array('bail_id' => $data['bail_avenant_id_origin']);
                    // $data_update = array('bail_cloture' => 1, 'bail_date_cloture' => date("Y-m-d"));
                    // $this->Bail_m->save_data($data_update, $clause);
                }

                $data_retour['isBail'] = $isBail;

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement de ${isBail} réussi"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function GetBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m');
                $this->load->model('IndexationLoyer_m');

                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array('c_bail.cliLot_id' => $data_post['cliLot_id'], 'c_bail.bail_cloture' => 0, 'c_bail.bail_avenant' => 0),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                        'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                        'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                        'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                        'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                        'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                        'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'join_orientation' => 'left',
                );

                $baux = $this->Bail_m->get($params);
                // If no bail en cours 
                if (count($baux) == 0) {
                    $params = array(
                        'clause' => array(
                            'c_bail.cliLot_id' => $data_post['cliLot_id'],
                            'c_bail.bail_cloture' => 0,
                            'c_bail.bail_avenant' => 1
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                            'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                            'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                            'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                            'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                            'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                            'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                            'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                            'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                        ),
                        'join_orientation' => 'left',
                    );
                    $baux = $this->Bail_m->get($params);

                    if (count($baux) == 0) {
                        $params = array(
                            'clause' => array(
                                'c_bail.cliLot_id' => $data_post['cliLot_id'],
                                'c_bail.bail_cloture' => 1,
                            ),
                            'join' => array(
                                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                                'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                                'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                                'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                                'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                                'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                                'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                                'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                                'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                                'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                            ),
                            'join_orientation' => 'left',
                        );
                        $baux = $this->Bail_m->get($params);
                    }
                } else {
                    $params = array(
                        'clause' => array(
                            'c_bail.cliLot_id' => $data_post['cliLot_id'], 'c_bail.bail_cloture' => 0,
                            'c_bail.bail_avenant' => 1, 'c_bail.bail_avenant_id_origin' => $baux[0]->bail_id
                        ),
                        'join' => array(
                            'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                            'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                            'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                            'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                            'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                            'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                            'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                            'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                            'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                        ),
                        'join_orientation' => 'left',
                        'orderBy' => 'c_bail.bail_id ASC'
                    );

                    $listAvenants = $this->Bail_m->get($params);
                    if (count($listAvenants) > 0) {
                        $count_baux = count($listAvenants);
                        $baux = array($listAvenants[$count_baux - 1]);
                    }
                }

                $data_retour = $baux;

                if (count($baux) > 0) {
                    $indexationParams = array(
                        'clause' => array(
                            'c_indexation_loyer.bail_id' => $baux[0]->bail_id,
                            'c_indexation_loyer.etat_indexation' => 1,
                        ),
                        'join' => array(
                            'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                            'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                        ),
                        'orderBy' => "c_indexation_loyer.indlo_id DESC"
                    );
                    $indexations = $this->IndexationLoyer_m->get($indexationParams);
                    $data_retour['indexations'] = $indexations;
                }

                $retour = retour(true, "success", $data_retour, array("message" => "Liste des baux", 'c_bail.cliLot_id' => $data_post['cliLot_id']));
                echo json_encode($retour);
            }
        }
    }

    public function AddIndexationLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('IndexationLoyer_m');
                $this->load->model('Bail_m', "bail");

                $data_post = $_POST['data'];
                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout de'indexation du loyer"));

                $data_retour['request'] = $data_post;

                $data['indlo_debut'] = $data_post['indlo_debut'];
                $data['indlo_fin'] = $data_post['indlo_fin'];
                $data['indlo_appartement_ht'] = $data_post['indlo_appartement_ht'];
                $data['indlo_appartement_tva'] = $data_post['indlo_appartement_tva'];
                $data['indlo_appartement_ttc'] = $data_post['indlo_appartement_ttc'];
                $data['indlo_parking_ht'] = $data_post['indlo_parking_ht'];
                $data['indlo_parking_tva'] = $data_post['indlo_parking_tva'];
                $data['indlo_parking_ttc'] = $data_post['indlo_parking_ttc'];
                $data['bail_id'] = $data_post['bail_id'];
                $data['indlo_charge_ht'] = $data_post['indlo_charge_ht'];
                $data['indlo_charge_tva'] = $data_post['indlo_charge_tva'];
                $data['indlo_charge_ttc'] = $data_post['indlo_charge_ttc'];
                $data['indlo_autre_prod_ht'] = $data_post['indlo_autre_prod_ht'];
                $data['indlo_autre_prod_tva'] = $data_post['indlo_autre_prod_tva'];
                $data['indlo_autre_prod_ttc'] = $data_post['indlo_autre_prod_ttc'];
                $data['indlo_indice_base'] = $data_post['indlo_indice_base'];

                if ($data_post['indlo_indice_reference']) {
                    $data['indlo_indice_reference'] = $data_post['indlo_indice_reference'];
                }

                $data['indlo_variation_plafonnee'] = $data_post['indlo_variation_plafonnee'];
                $data['indlo_variation_capee'] = $data_post['indlo_variation_capee'];
                $data['indlo_variation_loyer'] = $data_post['indlo_variation_loyer'];
                $data['indlo_variation_appliquee'] = $data_post['indlo_variation_appliquee'];
                $indlo_id = $data_post['indlo_id'] ? intval($data_post['indlo_id']) : null;

                $clause = array("indlo_id" => $indlo_id);

                if ($indlo_id == null) {
                    $request = $this->IndexationLoyer_m->save_data($data);
                } else {
                    $request = $this->IndexationLoyer_m->save_data($data, $clause);
                }

                $param = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'method' => "row"
                );

                $data_retour['bail_info'] = $this->bail->get($param);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement d'indexation du bail réussi"));
                }

                echo json_encode($retour);
            }
        }
    }


    public function GetIndexationLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndexationLoyer_m');
                $this->load->model('IndiceValeurLoyer_m');

                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array(
                        'c_indexation_loyer.bail_id' => $data_post['bail_id'],
                        'c_indexation_loyer.etat_indexation' => 1
                    ),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                        'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                        'c_indice_loyer' => 'c_indice_valeur_loyer.indloyer_id  = c_indice_loyer.indloyer_id',
                    ),
                );

                $data['indexations'] = $this->IndexationLoyer_m->get($params);
                $data['indices'] = $this->IndiceValeurLoyer_m->get(array());

                $this->renderComponant("Clients/clients/lot/bails/indexation-loyer", $data);
            }
        }
    }

    public function GetLoyerReference()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndexationLoyer_m');
                $this->load->model('IndiceValeurLoyer_m');
                $this->load->model('TauxTVA_m');

                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array(
                        'c_indexation_loyer.bail_id' => $data_post['bail_id'],
                        'c_indexation_loyer.etat_indexation' => 1
                    ),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                        'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                    ),
                );

                $data['indexations'] = $this->IndexationLoyer_m->get($params);
                $data['indices'] = $this->IndiceValeurLoyer_m->get(array());
                $data['tva'] = $this->TauxTVA_m->get(array());

                $paramIndexation = array(
                    'clause' => array(
                        'bail_id ' => $data_post['bail_id'],
                        'indlo_ref_loyer' => 1
                    ),
                    'method' => "row"
                );

                $data['indexation_ref'] = $this->IndexationLoyer_m->get($paramIndexation);
                $this->renderComponant("Clients/clients/lot/bails/loyer-reference", $data);
            }
        }
    }

    public function UpdateLoyerReference()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndexationLoyer_m');
                $data_post = $_POST['data'];
                $data_retour['request'] = $data_post;
                $clause = array("indlo_id" => $data_post['indlo_id']);
                $request = $this->IndexationLoyer_m->save_data($data_post, $clause);

                // Get bail Info
                $params = array(
                    'clause' => array(
                        'indlo_id' => $data_post['indlo_id']
                    ),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id'
                    ),
                    'method' => 'row'
                );

                $data_retour['details'] = $this->IndexationLoyer_m->get($params);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement de franchise loyer du bail réussi"));
                }

                echo json_encode($retour);
            }
        }
    }


    public function AddFranciseLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('FranchiseLoyer_m');
                $data_post = $_POST['data'];

                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout de'indexation du loyer"));

                $data_retour['request'] = $data_post;

                $data['franc_debut'] = $data_post['franc_debut'];
                $data['franc_fin'] = $data_post['franc_fin'];
                $data['franc_explication'] = $data_post['franc_explication'];
                $data['bail_id'] = $data_post['bail_id'];
                $data['type_deduction'] = $data_post['type_deduction'];
                $data['montant_ht'] = $data_post['montant_ht_deduit'];
                $data['tva'] = $data_post['taux_tva_franchise'];
                $data['montant_ttc_deduit'] = $data_post['montant_ttc_deduit'];
                $data['franchise_mode_calcul'] = $data_post['franchise_mode_calcul'];
                $data['franchise_pourcentage'] = $data_post['franchise_pourcentage'];
                if ($data['type_deduction'] == 1) {
                    $data['montant_ttc_deduit'] = NULL;
                    $data['montant_ht'] = NULL;
                    $data['tva'] = 0;
                    $data['franchise_pourcentage'] = NULL;
                }

                if ($data['franchise_mode_calcul'] == 0) {
                    $data['franchise_pourcentage'] = NULL;
                } else {
                    $data['montant_ttc_deduit'] = NULL;
                    $data['montant_ht'] = NULL;
                }

                $franc_id = $data_post['franc_id'] ? intval($data_post['franc_id']) : null;

                $clause = array("franc_id" => $franc_id);

                if ($franc_id == null) {
                    $request = $this->FranchiseLoyer_m->save_data($data);
                } else {
                    $request = $this->FranchiseLoyer_m->save_data($data, $clause);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement de franchise loyer du bail réussi"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function GetFranciseLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FranchiseLoyer_m');

                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array('c_franchise_loyer.bail_id' => $data_post['bail_id'], 'c_franchise_loyer.etat_franchise' => 1),
                    'join' => array(
                        'c_bail' => 'c_bail.bail_id = c_franchise_loyer.bail_id'
                    ),
                    'order_by_columns' => "c_franchise_loyer.franc_debut ASC",
                );

                $data['franchises'] = $this->FranchiseLoyer_m->get($params);
                $this->renderComponant("Clients/clients/lot/bails/franchise-bail", $data);
            }
        }
    }

    public function removeFranchise()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la suppression…",
            );

            $this->load->model('FranchiseLoyer_m');

            $data = decrypt_service($_POST["data"]);
            $params = array(
                'clause' => array('franc_id' => $data['franc_id']),
                'columns' => array('franc_id'),
                'method' => "row",
            );
            $request_get = $this->FranchiseLoyer_m->get($params);
            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'id' => $data['franc_id'],
                        'title' => "Confirmation de la suppression",
                        'text' => "Voulez-vous vraiment supprimer cette franchise de loyer ?",
                        'btnConfirm' => "Supprimer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $clause = array('franc_id' => $data['franc_id']);
                $data_update = array('etat_franchise' => 0);
                $request = $this->FranchiseLoyer_m->save_data($data_update, $clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array('id' => $data['franc_id']),
                        'message' => "Franchise de loyer supprimé",
                    );
                }
            }
            echo json_encode($retour);
        }
    }

    public function removeIndexation()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la suppression…",
            );

            $this->load->model('IndexationLoyer_m');
            $this->load->model('Bail_m', 'bail');

            $data = decrypt_service($_POST["data"]);
            $params = array(
                'clause' => array('indlo_id' => $data['indlo_id']),
                'method' => "row",
            );
            $request_get = $this->IndexationLoyer_m->get($params);

            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'id' => $data['indlo_id'],
                        'title' => "Confirmation de la suppression",
                        'text' => "Voulez-vous vraiment supprimer cette Indexation de loyer ?",
                        'btnConfirm' => "Supprimer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $clause = array('indlo_id' => $data['indlo_id']);
                $data_update = array('etat_indexation' => 0);
                $request = $this->IndexationLoyer_m->save_data($data_update, $clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array('id' => $data['indlo_id']),
                        'message' => "Indexation supprimée",
                    );
                }
            }
            $param = array(
                'clause' => array('bail_id ' => $request_get->bail_id),
                'method' => "row"
            );

            $retour['bail_info'] = $this->bail->get($param);
            echo json_encode($retour);
        }
    }

    public function formUploadDocBail()
    {
        $data_post = $_POST['data'];
        $data['cliLot_id'] = $data_post['cliLot_id'];

        $this->renderComponant('Clients/clients/lot/bails/form_upload_bail', $data);
    }

    public function uploadfileBail()
    {

        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $cliLot_id = $_POST['cliLot_id'];

        // Chemin du dossier
        $targetDir = "../documents/clients/lots/bail/$cliLot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_bail_nom' => $original_name,
                'doc_bail_creation' => date('Y-m-d H:i:s'),
                'doc_bail_path' => str_replace('\\', '/', $filePath),
                'doc_bail_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'cliLot_id' => $cliLot_id
            );

            $this->load->model('DocumentBail_m');
            $this->DocumentBail_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function updateDocBailTraitement()
    {
        $this->load->model('DocumentBail_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array(
                        'doc_bail_id' => $data_post['doc_bail_id']
                    ),
                    'method' => "row"
                );
                $request = $this->DocumentBail_m->get($params);
                if ($request->doc_bail_traite == 1) {
                    $data['doc_bail_traite'] = 0;
                    $data['doc_bail_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_bail_id' => $data_post['doc_bail_id']);
                    $this->DocumentBail_m->save_data($data, $clause);
                } else {
                    $data['doc_bail_traite'] = 1;
                    $data['doc_bail_traitement'] = date("Y-m-d H:i:s");
                    $data['prenom_util'] = $_SESSION['session_utilisateur']['util_prenom'];
                    $clause = array('doc_bail_id' => $data_post['doc_bail_id']);
                    $this->DocumentBail_m->save_data($data, $clause);
                }

                echo json_encode($request);
            }
        }
    }

    public function FormUpdateBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['cliLot_id'] = $data_post['cliLot_id'];

                $this->load->model('DocumentBail_m');
                $data['document'] = $this->DocumentBail_m->get(
                    array('clause' => array("doc_bail_id" => $data_post['doc_bail_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Clients/clients/lot/bails/formUpdateBail", $data);
        }
    }

    public function UpdateDocumentBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentBail_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);
                    $doc_bail_id = $data['doc_bail_id'];
                    unset($data['doc_bail_id']);

                    $clause = array("doc_bail_id" => $doc_bail_id);
                    $request = $this->DocumentBail_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeDocBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentBail_m', 'document');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_bail_id' => $data['doc_bail_id']),
                    'columns' => array('doc_bail_id'),
                    'method' => "row",
                );
                $request_get = $this->document->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_bail_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_bail_id' => $data['doc_bail_id']);
                    $data_update = array('doc_bail_etat' => 0);
                    $request = $this->document->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_bail_id']),
                            'message' => "Document supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function validationBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation",
                );
                $this->load->model('Bail_m', 'bail');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('bail_id' => $data['bail_id']),
                    'method' => "row",
                );
                $request_get = $this->bail->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['bail_id'],
                            'bail_id' => $data['bail_id'],
                            'bail_valide' => $data['bail_valide'],
                            'title' => "Confirmation de la validation",
                            'text' => $data['bail_valide'] == 0 ? "Voulez-vous annuler la validation de ce bail ?" : "Voulez-vous valider ce bail ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('bail_id' => $data['bail_id']);
                    $data_update = array('bail_valide' => $data['bail_valide']);
                    if ($data['bail_valide'] == 0) {
                        $data_update['bail_facture'] = 0;
                    }
                    $request = $this->bail->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['bail_id'],
                                'bail_id' => $data['bail_id'],
                                'clilotId' => $request_get->cliLot_id,
                                'bail_valide' => $data['bail_valide']
                            ),
                            'message' => $data['bail_valide'] == 0 ? "Annulation effectuée" : "Validation effectuée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function genererFacture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la géneration des factures",
                );
                $this->load->model('Bail_m', 'bail');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('bail_id' => $data['bail_id']),
                    'method' => "row",
                );
                $request_get = $this->bail->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['bail_id'],
                            'bail_id' => $data['bail_id'],
                            'title' => "Génération des factures",
                            'text' =>  "Voulez-vous confirmer la génération des factures?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('bail_id' => $data['bail_id']);
                    $data_update = array('bail_facture' => 1);
                    $request = $this->bail->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['bail_id'],
                                'bail_id' => $data['bail_id'],
                                'clilotId' => $request_get->cliLot_id,
                            ),
                            'message' => "Génération des factures effectuée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function automatique_Bail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndexationLoyer_m');
                $this->load->model('Bail_m');
                $this->load->model('ClientLot_m');
                $this->load->model('FactureLoyer_m');
                $this->load->model('PeriodeAnnuel_m');
                $this->load->model('FranchiseLoyer_m');
                $this->load->helper("loyer");
                $this->load->model('Acte_m');

                $data_post = $_POST['data'];
                $data_retour['request'] = $data_post;

                // Obtenir les informations du bail
                $bailparam = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                        'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                        'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                        'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                        'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                        'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                        'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                        'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
                    ),
                    'method' => 'row'
                );
                $bail = $this->Bail_m->get($bailparam);

                $periode_indexation = $bail->prdind_id;
                $lot_id = $bail->cliLot_id;
                $pdl_id = $bail->pdl_id;

                //obtenir Acte
                $getacte = $this->Acte_m->get(array(
                    'clause' => array('cliLot_id ' => $lot_id),
                    'method' => 'row'
                ));

                // Obtenir l'adresse du gestionnaire
                $param_index = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'join' => array('c_gestionnaire' => 'c_gestionnaire.gestionnaire_id  = c_bail.preneur_id'),
                    'method' => 'row'
                );
                $addresse_gestionnaire = $this->Bail_m->get($param_index);

                // Obtenir les informations sur le lot
                $paramLot = array(
                    'clause' => array('cliLot_id ' => $lot_id),
                    'method' => 'row'
                );
                $lot_facture = $this->ClientLot_m->get($paramLot);

                $cliLot_mode_paiement = "";
                $cliLot_encaiss_loyer = "";
                $cliLot_iban_client = "";

                // Obtenir les informations sur l'indexation du loyer
                if (isset($lot_facture)) {
                    $cliLot_mode_paiement = $lot_facture->cliLot_mode_paiement;
                    $cliLot_encaiss_loyer = " sur " . $lot_facture->cliLot_encaiss_loyer;
                    $cliLot_iban_client = $lot_facture->cliLot_iban_client;
                }

                $indexationLoyerParam = array(
                    'clause' => array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
                );
                $indexation_loyer = $this->IndexationLoyer_m->get($indexationLoyerParam);

                $indexationLoyerParam_no_indice = array(
                    'clause' => array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0, 'indlo_indince_non_publie' => 1),
                    'method' => 'row'
                );
                $indexation_loyer_no_indice = $this->IndexationLoyer_m->get($indexationLoyerParam_no_indice);

                // Obtenir l'adresse de l'émetteur
                $addresse_emetteur = $this->get_emetteur($data_post['bail_id']);

                $liste_annee = array();

                if ($periode_indexation == 1) {
                    foreach ($indexation_loyer as $key => $value) {
                        $indlo_debut = date('Y', strtotime($value->indlo_debut));
                        $indlo_fin = date('Y', strtotime($value->indlo_fin));
                        if (!(in_array($indlo_debut, $liste_annee))) {
                            array_push($liste_annee, $indlo_debut);
                        }
                        if (!(in_array($indlo_fin, $liste_annee))) {
                            array_push($liste_annee, $indlo_fin);
                        }
                    }
                    if (!empty($indexation_loyer_no_indice)) {
                        $liste_annee = array();
                        for ($i = intval($indexation_loyer_no_indice->indlo_debut); $i <= intval($indlo_fin); $i++) {
                            array_push($liste_annee, $i);
                        }
                    }
                } else {
                    $countIndex = count($indexation_loyer);
                    $indlo_debut = date('Y', strtotime($indexation_loyer[0]->indlo_debut));
                    $indlo_fin = date('Y', strtotime($indexation_loyer[$countIndex - 1]->indlo_fin));

                    for ($i = intval($indlo_debut); $i <= intval($indlo_fin); $i++) {
                        array_push($liste_annee, $i);
                    }
                }

                //Periodicité mensuel
                if ($pdl_id == 1) {
                    foreach ($liste_annee as $key => $value) {
                        $list_full_mois = genererListeMoisDansAnnee($value, true);

                        $indexation = array();
                        foreach ($list_full_mois as $key => $value) {
                            $indexation[] = array(
                                'debut' => $value['debut'],
                                'fin' => $value['fin'],
                                'nom' => $value['nom'],
                                'indexation' => $this->getindexation($value['fin'], $data_post['bail_id']),
                            );
                        }

                        foreach ($indexation as $index) {
                            if (!empty($index['indexation'])) {
                                $data = array();
                                foreach ($index['indexation'] as $table_indexation) {
                                    $bail_date_echeance = new DateTime($bail->bail_date_echeance);
                                    $bail_debut_bail = new DateTime($bail->bail_debut);

                                    $date_prorata = '';

                                    if ($bail_date_echeance >= $bail_debut_bail) {
                                        $debut_facture = $bail_date_echeance;
                                        if (date('m', strtotime($bail->bail_date_echeance)) == date('m', strtotime($bail->bail_debut)) && date('Y', strtotime($bail->bail_date_echeance)) == date('Y', strtotime($bail->bail_debut))) {
                                            $date_prorata = $bail->bail_debut;
                                        } else {
                                            $date_prorata = $indexation_loyer[0]->indlo_debut;
                                        }
                                    } else {
                                        $debut_facture = $bail_debut_bail;
                                        $date_prorata = $bail->bail_debut;
                                    }

                                    $indexation_loyer_date = new DateTime($indexation_loyer[0]->indlo_debut);
                                    if ($debut_facture < $indexation_loyer_date) {
                                        $date_prorata = $indexation_loyer[0]->indlo_debut;
                                    }

                                    if (!empty($getacte->acte_date_notarie) && $date_prorata < $getacte->acte_date_notarie) {
                                        $date_prorata = $getacte->acte_date_notarie;
                                        $debut_facture = new DateTime($getacte->acte_date_notarie);
                                    }

                                    $date_debut =  date('m-Y', strtotime($index['debut']));
                                    $debut_indexation = date('m-Y', strtotime($date_prorata));
                                    $date1 = new DateTime($date_prorata);
                                    $date2 = new DateTime($index['fin']);

                                    $indlo_appartement_ht = $table_indexation->indlo_appartement_ht;
                                    $indlo_appartement_tva = $table_indexation->indlo_appartement_tva;
                                    $indlo_appartement_ttc = $table_indexation->indlo_appartement_ttc;

                                    $indlo_parking_ht = $table_indexation->indlo_parking_ht;
                                    $indlo_parking_tva = $table_indexation->indlo_parking_tva;
                                    $indlo_parking_ttc = $table_indexation->indlo_parking_ttc;

                                    $indlo_charge_ht = $table_indexation->indlo_charge_ht;
                                    $indlo_charge_tva = $table_indexation->indlo_charge_tva;
                                    $indlo_charge_ttc = $table_indexation->indlo_charge_ttc;

                                    if ($date_debut === $debut_indexation) {
                                        $interval = date_diff($date1, $date2);

                                        $diff = (intval($interval->format(' %d '))) + 1;
                                        $end_month = (date("d", strtotime($index['fin'])));

                                        if ($diff - 1 == $end_month) {
                                            $diff = $end_month;
                                        }

                                        $indlo_appartement_ht = round((($table_indexation->indlo_appartement_ht * $diff) / $end_month), 2);
                                        $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                        $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                        $indlo_parking_ht = round((($table_indexation->indlo_parking_ht * $diff) / $end_month), 2);
                                        $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                        $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                        $indlo_charge_ht = round((($table_indexation->indlo_charge_ht * $diff) / $end_month), 2);
                                        $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                        $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                        $data['fact_prorata'] = date('d/m/Y', strtotime($date_prorata)) . ' - ' . date('d/m/Y', strtotime($index['fin']));
                                    }

                                    $franchise = $this->FranchiseLoyer_m->get(array(
                                        'clause' => array('bail_id' => $bail->bail_id, 'etat_franchise' => 1),
                                    ));

                                    if (!empty($franchise)) {
                                        $start_timestamp = strtotime($index['debut']);
                                        $end_timestamp = strtotime($index['fin']);

                                        foreach ($franchise as $key_franchise => $value_franchise) {
                                            $date_to_check_timestamp_debut = strtotime($value_franchise->franc_debut);
                                            $date_to_check_timestamp_fin = strtotime($value_franchise->franc_fin);

                                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff) / $end_month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff) / $end_month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff) / $end_month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff) / $end_month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff) / $end_month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut === $debut_indexation) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff) / $end_month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                    $data['bail_id'] = $bail->bail_id;
                                    $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad($this->get_numero_factu($data_post['bail_id']), 5, '0', STR_PAD_LEFT);
                                    $data['periode_mens_id'] = intval(date('m', strtotime($index['fin'])));
                                    $data['facture_nombre'] = 1;
                                    $data['facture_annee'] = date('Y', strtotime($index['fin']));

                                    $data['dossier_id'] = $bail->dossier_id;
                                    $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                                    $data['emet_adress1'] = $addresse_emetteur['adress1'];
                                    $data['emet_adress2'] = $addresse_emetteur['adress2'];
                                    $data['emet_adress3'] = $addresse_emetteur['adress3'];
                                    $data['emet_cp'] = $addresse_emetteur['cp'];
                                    $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                                    $data['emet_ville'] = $addresse_emetteur['ville'];
                                    $data['dest_nom'] = $addresse_gestionnaire->gestionnaire_nom;
                                    $data['dest_adresse1'] = $addresse_gestionnaire->gestionnaire_adresse1;
                                    $data['dest_adresse2'] = $addresse_gestionnaire->gestionnaire_adresse2;
                                    $data['dest_adresse3'] = $addresse_gestionnaire->gestionnaire_adresse3;
                                    $data['dest_cp'] = $addresse_gestionnaire->gestionnaire_cp;
                                    $data['dest_pays'] = $addresse_gestionnaire->gestionnaire_pays;
                                    $data['dest_ville'] = $addresse_gestionnaire->gestionnaire_ville;
                                    $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;
                                    if (($bail->momfac_id  == 1)) {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    } else {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) - 1, 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    }

                                    // Voir si la date de facture depasse le date du jour
                                    if ($data['fact_loyer_date'] > date('Y-m-d')) {
                                        break;
                                    }

                                    $date_fin_foreact_index = new DateTime($index['fin']);
                                    $date_debut_facture = $debut_facture;

                                    if ($date_debut_facture <= $date_fin_foreact_index || !empty($indexation_loyer_no_indice) || $periode_indexation == 3) {
                                        $this->FactureLoyer_m->save_data($data);
                                        $request_id = array(
                                            'order_by_columns' => "fact_loyer_id DESC",
                                            'join' => array('c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'),
                                            'limit' => 1,
                                            'method' => 'row'
                                        );
                                        $last_facture_loyer = $this->FactureLoyer_m->get($request_id);

                                        $this->generate_facture($last_facture_loyer);
                                    }
                                }
                            }
                        }
                    }
                }

                //Periodicité trimestriel
                elseif ($pdl_id == 2) {
                    $listTimestre = array(
                        array("nom" => "T1", "value" => "Mars"),
                        array("nom" => "T2", "value" => "Juin"),
                        array("nom" => "T3", "value" => "Septembre"),
                        array("nom" => "T4", "value" => "Décembre"),
                    );
                    foreach ($liste_annee as $key_annee => $value) {
                        $list_full_mois = genererListeMoisDansAnnee($value, true);
                        $indexation = array();
                        foreach ($list_full_mois as $key => $value) {
                            if (($key + 1) % 3 === 0) {
                                $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                                $indexation[] = array(
                                    'debut' => $debut,
                                    'fin' => $value['fin'],
                                    'nom' => $value['nom'],
                                    'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                                );
                            }
                        }

                        foreach ($indexation as $index) {
                            $periode_timestre_id = "";
                            $month = array();
                            foreach ($listTimestre as $key_listTimestre => $value_listTimestre) {
                                if ($value_listTimestre['value'] == $index['nom']) {
                                    if ($value_listTimestre['nom'] == "T1") {
                                        $periode_timestre_id = 1;
                                        $month = [1, 2, 3];
                                    }
                                    if ($value_listTimestre['nom'] == "T2") {
                                        $periode_timestre_id = 2;
                                        $month = [4, 5, 6];
                                    }
                                    if ($value_listTimestre['nom'] == "T3") {
                                        $periode_timestre_id = 3;
                                        $month = [7, 8, 9];
                                    }
                                    if ($value_listTimestre['nom'] == "T4") {
                                        $periode_timestre_id = 4;
                                        $month = [10, 11, 12];
                                    }
                                }
                            }

                            if (!empty($index['indexation'])) {
                                $data = array();
                                foreach ($index['indexation'] as $table_indexation) {
                                    $bail_date_echeance = new DateTime($bail->bail_date_echeance);
                                    $bail_debut_bail = new DateTime($bail->bail_debut);

                                    $date_prorata = '';
                                    if ($bail_date_echeance >= $bail_debut_bail) {
                                        $debut_facture = $bail_date_echeance;
                                        if (
                                            in_array(intval(date('m', strtotime($bail->bail_date_echeance))), $month) &&
                                            in_array(intval(date('m', strtotime($bail->bail_debut))), $month) &&
                                            date('Y', strtotime($bail->bail_date_echeance)) == date('Y', strtotime($bail->bail_debut))
                                        ) {
                                            $date_prorata = $bail->bail_debut;
                                        } else {
                                            $date_prorata = $indexation_loyer[0]->indlo_debut;
                                        }
                                    } else {
                                        $debut_facture = $bail_debut_bail;
                                        $date_prorata = $bail->bail_debut;
                                    }

                                    $indexation_loyer_date = new DateTime($indexation_loyer[0]->indlo_debut);
                                    if ($debut_facture < $indexation_loyer_date) {
                                        $date_prorata = $indexation_loyer[0]->indlo_debut;
                                    }

                                    if (!empty($getacte->acte_date_notarie) && $date_prorata < $getacte->acte_date_notarie) {
                                        $date_prorata = $getacte->acte_date_notarie;
                                        $debut_facture = new DateTime($getacte->acte_date_notarie);
                                    }

                                    $date_debut_year =  date('Y', strtotime($index['debut']));
                                    $date_debut_month =  date('m', strtotime($index['debut']));

                                    $debut_indexation_year = date('Y', strtotime($date_prorata));
                                    $debut_indexation_month = date('m', strtotime($date_prorata));

                                    $indlo_appartement_ht = $table_indexation->indlo_appartement_ht;
                                    $indlo_appartement_tva = $table_indexation->indlo_appartement_tva;
                                    $indlo_appartement_ttc = $table_indexation->indlo_appartement_ttc;
                                    $indlo_parking_ht = $table_indexation->indlo_parking_ht;
                                    $indlo_parking_tva = $table_indexation->indlo_parking_tva;
                                    $indlo_parking_ttc = $table_indexation->indlo_parking_ttc;

                                    $indlo_charge_ht = $table_indexation->indlo_charge_ht;
                                    $indlo_charge_tva = $table_indexation->indlo_charge_tva;
                                    $indlo_charge_ttc = $table_indexation->indlo_charge_ttc;

                                    if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                        $date1 = $date_prorata;
                                        $date2 = $index['fin'];

                                        $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                        $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                        $date11 = $index['debut'];
                                        $date22 = $index['fin'];

                                        $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                        $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                        if ($diff_in_days - 1 ==  $days_3month) {
                                            $diff_in_days =  $days_3month;
                                        }

                                        $indlo_appartement_ht = round((($table_indexation->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                        $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                        $indlo_parking_ht = round((($table_indexation->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                        $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                        $indlo_charge_ht = round((($table_indexation->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                        $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                        $data['fact_prorata'] = date('d/m/Y', strtotime($date_prorata)) . ' - ' . date('d/m/Y', strtotime($index['fin']));
                                    }

                                    $franchise = $this->FranchiseLoyer_m->get(array(
                                        'clause' => array('bail_id' => $bail->bail_id, 'etat_franchise' => 1),
                                    ));

                                    if (!empty($franchise)) {
                                        $start_timestamp = strtotime($index['debut']);
                                        $end_timestamp = strtotime($index['fin']);

                                        foreach ($franchise as $key_franchise => $value_franchise) {
                                            $date_to_check_timestamp_debut = strtotime($value_franchise->franc_debut);
                                            $date_to_check_timestamp_fin = strtotime($value_franchise->franc_fin);

                                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                    $data['bail_id'] = $bail->bail_id;
                                    $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad($this->get_numero_factu($data_post['bail_id']), 5, '0', STR_PAD_LEFT);
                                    $data['periode_timestre_id '] = $periode_timestre_id;
                                    $data['facture_nombre'] = 1;
                                    $data['facture_annee'] = date('Y', strtotime($index['fin']));
                                    $data['dossier_id'] = $bail->dossier_id;
                                    $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                                    $data['emet_adress1'] = $addresse_emetteur['adress1'];
                                    $data['emet_adress2'] = $addresse_emetteur['adress2'];
                                    $data['emet_adress3'] = $addresse_emetteur['adress3'];
                                    $data['emet_cp'] = $addresse_emetteur['cp'];
                                    $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                                    $data['emet_ville'] = $addresse_emetteur['ville'];
                                    $data['dest_nom'] = $addresse_gestionnaire->gestionnaire_nom;
                                    $data['dest_adresse1'] = $addresse_gestionnaire->gestionnaire_adresse1;
                                    $data['dest_adresse2'] = $addresse_gestionnaire->gestionnaire_adresse2;
                                    $data['dest_adresse3'] = $addresse_gestionnaire->gestionnaire_adresse3;
                                    $data['dest_cp'] = $addresse_gestionnaire->gestionnaire_cp;
                                    $data['dest_pays'] = $addresse_gestionnaire->gestionnaire_pays;
                                    $data['dest_ville'] = $addresse_gestionnaire->gestionnaire_ville;
                                    $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                                    if (($bail->momfac_id  == 1)) {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    } else {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    }

                                    // Voir si la date de facture depasse le date du jour
                                    if ($data['fact_loyer_date'] > date('Y-m-d')) {
                                        break;
                                    }

                                    $date_fin_foreact_index = new DateTime($index['fin']);
                                    $date_debut_facture = $debut_facture;

                                    if ($date_debut_facture <= $date_fin_foreact_index || !empty($indexation_loyer_no_indice) || $periode_indexation == 3) {
                                        $this->FactureLoyer_m->save_data($data);
                                        $request_id = array(
                                            'order_by_columns' => "fact_loyer_id DESC",
                                            'join' => array('c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'),
                                            'limit' => 1,
                                            'method' => 'row'
                                        );
                                        $last_facture_loyer = $this->FactureLoyer_m->get($request_id);

                                        $this->generate_facture($last_facture_loyer);
                                    }
                                }
                            }
                        }
                    }
                }
                //Periodicité trimestriel decallée
                elseif ($pdl_id == 5) {
                    foreach ($liste_annee as $key => $value) {
                        $list_full_mois = genererListeMoisDansAnnee($value, true);
                        $key_value = new DateTime($value . '-' . $bail->premier_mois_trimes . '-1');

                        //re-order Trimestre
                        $less_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                            $debut = new DateTime($d['debut']);
                            return $debut < $key_value;
                        });
                        $greater_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                            $debut = new DateTime($d['debut']);
                            return $debut >= $key_value;
                        });
                        $reordered_data = array_merge($greater_than_data, $less_than_data);

                        $indexation = array();
                        foreach ($reordered_data as $key => $value) {
                            if (($key + 1) % 3 === 0) {
                                $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                                $indexation[] = array(
                                    'debut' => $debut,
                                    'fin' => $value['fin'],
                                    'nom' => $value['nom'],
                                    'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                                );
                            }
                        }

                        $listTimestre = array(
                            array("nom" => "T1", "value" => $indexation[0]['nom'], "debut" => $indexation[0]['debut'], "fin" => $indexation[0]['fin']),
                            array("nom" => "T2", "value" => $indexation[1]['nom'], "debut" => $indexation[1]['debut'], "fin" => $indexation[1]['fin']),
                            array("nom" => "T3", "value" => $indexation[2]['nom'], "debut" => $indexation[2]['debut'], "fin" => $indexation[2]['fin']),
                            array("nom" => "T4", "value" => $indexation[3]['nom'], "debut" => $indexation[3]['debut'], "fin" => $indexation[3]['fin']),
                        );

                        //re-order Header
                        $less_than_data_listTimestre = array_filter($listTimestre, function ($t) use ($key_value) {
                            $debut = new DateTime($t['debut']);
                            return $debut < $key_value;
                        });
                        $greater_than_data_listTimestre  = array_filter($listTimestre, function ($t) use ($key_value) {
                            $debut = new DateTime($t['debut']);
                            return $debut >= $key_value;
                        });
                        $data_listTimestre = array_merge($less_than_data_listTimestre, $greater_than_data_listTimestre);

                        //re-order indexations
                        $less_than_data_indexation = array_filter($indexation, function ($i) use ($key_value) {
                            $debut = new DateTime($i['debut']);
                            return $debut < $key_value;
                        });
                        $greater_than_data_indexation  = array_filter($indexation, function ($i) use ($key_value) {
                            $debut = new DateTime($i['debut']);
                            return $debut >= $key_value;
                        });

                        $data_indexation = array_merge($less_than_data_indexation, $greater_than_data_indexation);

                        foreach ($data_indexation as $index) {
                            $periode_timestre_id = "";
                            $month = array();
                            foreach ($data_listTimestre as $key_listTimestre => $value_listTimestre) {
                                $date_debut_trim =  intval(date('m', strtotime($value_listTimestre['debut'])));
                                $date_fin_trim =  intval(date('m', strtotime($value_listTimestre['fin'])));
                                if ($value_listTimestre['value'] == $index['nom']) {
                                    if ($value_listTimestre['nom'] == "T1") {
                                        $periode_timestre_id = 1;
                                        $month[0] = $date_debut_trim;
                                        $month[2] = $date_fin_trim;
                                        if ($month[0] == 12) {
                                            $month[1] = 1;
                                        } else {
                                            $month[1] = $month[0] + 1;
                                        }
                                    }
                                    if ($value_listTimestre['nom'] == "T2") {
                                        $periode_timestre_id = 2;
                                        $month[0] = $date_debut_trim;
                                        $month[2] = $date_fin_trim;
                                        if ($month[0] == 12) {
                                            $month[1] = 1;
                                        } else {
                                            $month[1] = $month[0] + 1;
                                        }
                                    }
                                    if ($value_listTimestre['nom'] == "T3") {
                                        $periode_timestre_id = 3;
                                        $month[0] = $date_debut_trim;
                                        $month[2] = $date_fin_trim;
                                        if ($month[0] == 12) {
                                            $month[1] = 1;
                                        } else {
                                            $month[1] = $month[0] + 1;
                                        }
                                    }
                                    if ($value_listTimestre['nom'] == "T4") {
                                        $periode_timestre_id = 4;
                                        $month[0] = $date_debut_trim;
                                        $month[2] = $date_fin_trim;
                                        if ($month[0] == 12) {
                                            $month[1] = 1;
                                        } else {
                                            $month[1] = $month[0] + 1;
                                        }
                                    }
                                }
                            }
                            if (!empty($index['indexation'])) {
                                $data = array();
                                foreach ($index['indexation'] as $table_indexation) {
                                    $bail_date_echeance = new DateTime($bail->bail_date_echeance);
                                    $bail_debut_bail = new DateTime($bail->bail_debut);

                                    $date_prorata = '';

                                    if ($bail_date_echeance >= $bail_debut_bail) {
                                        $debut_facture = $bail_date_echeance;
                                        if (
                                            in_array(intval(date('m', strtotime($bail->bail_date_echeance))), $month) &&
                                            in_array(intval(date('m', strtotime($bail->bail_debut))), $month) &&
                                            date('Y', strtotime($bail->bail_date_echeance)) == date('Y', strtotime($bail->bail_debut))
                                        ) {
                                            $date_prorata = $bail->bail_debut;
                                        } else {
                                            $date_prorata = $indexation_loyer[0]->indlo_debut;
                                        }
                                    } else {
                                        $debut_facture = $bail_debut_bail;
                                        $date_prorata = $bail->bail_debut;
                                    }

                                    $indexation_loyer_date = new DateTime($indexation_loyer[0]->indlo_debut);
                                    if ($debut_facture < $indexation_loyer_date) {
                                        $date_prorata = $indexation_loyer[0]->indlo_debut;
                                    }

                                    if (!empty($getacte->acte_date_notarie) && $date_prorata < $getacte->acte_date_notarie) {
                                        $date_prorata = $getacte->acte_date_notarie;
                                        $debut_facture = new DateTime($getacte->acte_date_notarie);
                                    }

                                    $date_debut_year =  date('Y', strtotime($index['debut']));
                                    $date_debut_month =  date('m', strtotime($index['debut']));

                                    $debut_indexation_year = date('Y', strtotime($date_prorata));
                                    $debut_indexation_month = date('m', strtotime($date_prorata));

                                    $indlo_appartement_ht = $table_indexation->indlo_appartement_ht;
                                    $indlo_appartement_tva = $table_indexation->indlo_appartement_tva;
                                    $indlo_appartement_ttc = $table_indexation->indlo_appartement_ttc;
                                    $indlo_parking_ht = $table_indexation->indlo_parking_ht;
                                    $indlo_parking_tva = $table_indexation->indlo_parking_tva;
                                    $indlo_parking_ttc = $table_indexation->indlo_parking_ttc;

                                    $indlo_charge_ht = $table_indexation->indlo_charge_ht;
                                    $indlo_charge_tva = $table_indexation->indlo_charge_tva;
                                    $indlo_charge_ttc = $table_indexation->indlo_charge_ttc;

                                    if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                        $date1 = $date_prorata;
                                        $date2 = $index['fin'];

                                        $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                        $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                        $date11 = $index['debut'];
                                        $date22 = $index['fin'];

                                        $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                        $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                        if ($diff_in_days - 1 ==  $days_3month) {
                                            $diff_in_days =  $days_3month;
                                        }

                                        $indlo_appartement_ht = round((($table_indexation->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                        $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                        $indlo_parking_ht = round((($table_indexation->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                        $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                        $indlo_charge_ht = round((($table_indexation->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                        $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                        $data['fact_prorata'] = date('d/m/Y', strtotime($date_prorata)) . ' - ' . date('d/m/Y', strtotime($index['fin']));
                                    }

                                    $franchise = $this->FranchiseLoyer_m->get(array(
                                        'clause' => array('bail_id' => $bail->bail_id, 'etat_franchise' => 1),
                                    ));

                                    if (!empty($franchise)) {
                                        $start_timestamp = strtotime($index['debut']);
                                        $end_timestamp = strtotime($index['fin']);

                                        foreach ($franchise as $key_franchise => $value_franchise) {
                                            $date_to_check_timestamp_debut = strtotime($value_franchise->franc_debut);
                                            $date_to_check_timestamp_fin = strtotime($value_franchise->franc_fin);

                                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                    $data['bail_id'] = $bail->bail_id;
                                    $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad($this->get_numero_factu($data_post['bail_id']), 5, '0', STR_PAD_LEFT);
                                    $data['periode_timestre_id '] = $periode_timestre_id;
                                    $data['facture_nombre'] = 1;
                                    if (($bail->momfac_id  == 1)) {
                                        $data['facture_annee'] = date('Y', strtotime($index['fin']));
                                    } else {
                                        $data['facture_annee'] = date('Y', strtotime($index['debut']));
                                    }

                                    $data['dossier_id'] = $bail->dossier_id;
                                    $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                                    $data['emet_adress1'] = $addresse_emetteur['adress1'];
                                    $data['emet_adress2'] = $addresse_emetteur['adress2'];
                                    $data['emet_adress3'] = $addresse_emetteur['adress3'];
                                    $data['emet_cp'] = $addresse_emetteur['cp'];
                                    $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                                    $data['emet_ville'] = $addresse_emetteur['ville'];
                                    $data['dest_nom'] = $addresse_gestionnaire->gestionnaire_nom;
                                    $data['dest_adresse1'] = $addresse_gestionnaire->gestionnaire_adresse1;
                                    $data['dest_adresse2'] = $addresse_gestionnaire->gestionnaire_adresse2;
                                    $data['dest_adresse3'] = $addresse_gestionnaire->gestionnaire_adresse3;
                                    $data['dest_cp'] = $addresse_gestionnaire->gestionnaire_cp;
                                    $data['dest_pays'] = $addresse_gestionnaire->gestionnaire_pays;
                                    $data['dest_ville'] = $addresse_gestionnaire->gestionnaire_ville;
                                    $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                                    if (($bail->momfac_id  == 1)) {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    } else {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    }

                                    // Voir si la date de facture depasse le date du jour
                                    if ($data['fact_loyer_date'] > date('Y-m-d')) {
                                        break;
                                    }

                                    $date_fin_foreact_index = new DateTime($index['fin']);
                                    $date_debut_facture = $debut_facture;

                                    if ($date_debut_facture <= $date_fin_foreact_index || !empty($indexation_loyer_no_indice) || $periode_indexation == 3) {
                                        $this->FactureLoyer_m->save_data($data);
                                        $request_id = array(
                                            'order_by_columns' => "fact_loyer_id DESC",
                                            'join' => array('c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'),
                                            'limit' => 1,
                                            'method' => 'row'
                                        );
                                        $last_facture_loyer = $this->FactureLoyer_m->get($request_id);

                                        $this->generate_facture($last_facture_loyer);
                                    }
                                }
                            }
                        }
                    }
                }
                //Periodicité semestriel
                elseif ($pdl_id == 3) {
                    $listSemestre = array(
                        array("nom" => "S1", "value" => "Juin"),
                        array("nom" => "S2", "value" => "Décembre"),
                    );
                    foreach ($liste_annee as $key => $value) {
                        $list_full_mois = genererListeMoisDansAnnee($value, true);
                        $indexation = array();
                        foreach ($list_full_mois as $key => $value) {
                            if (($key + 1) % 6 === 0) {
                                $debut = date('Y-m-d', strtotime('-5 months', strtotime($value['debut'])));
                                $indexation[] = array(
                                    'debut' => $debut,
                                    'fin' => $value['fin'],
                                    'nom' => $value['nom'],
                                    'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                                );
                            }
                        }

                        foreach ($indexation as $index) {
                            $periode_semestre_id = "";
                            $month = array();
                            foreach ($listSemestre as $key_listSemestriel => $value_listSemestriel) {
                                if ($value_listSemestriel['value'] == $index['nom']) {
                                    if ($value_listSemestriel['nom'] == "S1") {
                                        $periode_semestre_id = 1;
                                        $month = [1, 2, 3, 4, 5, 6];
                                    }
                                    if ($value_listSemestriel['nom'] == "S2") {
                                        $periode_semestre_id = 2;
                                        $month = [7, 8, 9, 10, 11, 12];
                                    }
                                }
                            }
                            if (!empty($index['indexation'])) {
                                $data = array();
                                foreach ($index['indexation'] as $table_indexation) {
                                    $bail_date_echeance = new DateTime($bail->bail_date_echeance);
                                    $bail_debut_bail = new DateTime($bail->bail_debut);

                                    $date_prorata = '';

                                    if ($bail_date_echeance >= $bail_debut_bail) {
                                        $debut_facture = $bail_date_echeance;
                                        if (
                                            in_array(intval(date('m', strtotime($bail->bail_date_echeance))), $month) &&
                                            in_array(intval(date('m', strtotime($bail->bail_debut))), $month) &&
                                            date('Y', strtotime($bail->bail_date_echeance)) == date('Y', strtotime($bail->bail_debut))
                                        ) {
                                            $date_prorata = $bail->bail_debut;
                                        } else {
                                            $date_prorata = $indexation_loyer[0]->indlo_debut;
                                        }
                                    } else {
                                        $debut_facture = $bail_debut_bail;
                                        $date_prorata = $bail->bail_debut;
                                    }

                                    $indexation_loyer_date = new DateTime($indexation_loyer[0]->indlo_debut);
                                    if ($debut_facture < $indexation_loyer_date) {
                                        $date_prorata = $indexation_loyer[0]->indlo_debut;
                                    }

                                    if (!empty($getacte->acte_date_notarie) && $date_prorata < $getacte->acte_date_notarie) {
                                        $date_prorata = $getacte->acte_date_notarie;
                                        $debut_facture = new DateTime($getacte->acte_date_notarie);
                                    }

                                    $date_debut_year =  date('Y', strtotime($index['debut']));
                                    $date_debut_month =  date('m', strtotime($index['debut']));

                                    $debut_indexation_year = date('Y', strtotime($date_prorata));
                                    $debut_indexation_month = date('m', strtotime($date_prorata));

                                    $indlo_appartement_ht = $table_indexation->indlo_appartement_ht;
                                    $indlo_appartement_tva = $table_indexation->indlo_appartement_tva;
                                    $indlo_appartement_ttc = $table_indexation->indlo_appartement_ttc;
                                    $indlo_parking_ht = $table_indexation->indlo_parking_ht;
                                    $indlo_parking_tva = $table_indexation->indlo_parking_tva;
                                    $indlo_parking_ttc = $table_indexation->indlo_parking_ttc;

                                    $indlo_charge_ht = $table_indexation->indlo_charge_ht;
                                    $indlo_charge_tva = $table_indexation->indlo_charge_tva;
                                    $indlo_charge_ttc = $table_indexation->indlo_charge_ttc;

                                    if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                        $date1 = $date_prorata;
                                        $date2 = $index['fin'];

                                        $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                        $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                        $date11 = $index['debut'];
                                        $date22 = $index['fin'];

                                        $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                        $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                        if ($diff_in_days - 1 ==  $days_3month) {
                                            $diff_in_days =  $days_3month;
                                        }

                                        $indlo_appartement_ht = round((($table_indexation->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                        $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                        $indlo_parking_ht = round((($table_indexation->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                        $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                        $indlo_charge_ht = round((($table_indexation->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                        $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                        $data['fact_prorata'] = date('d/m/Y', strtotime($date_prorata)) . ' - ' . date('d/m/Y', strtotime($index['fin']));
                                    }

                                    $franchise = $this->FranchiseLoyer_m->get(array(
                                        'clause' => array('bail_id' => $bail->bail_id, 'etat_franchise' => 1),
                                    ));

                                    if (!empty($franchise)) {
                                        $start_timestamp = strtotime($index['debut']);
                                        $end_timestamp = strtotime($index['fin']);

                                        foreach ($franchise as $key_franchise => $value_franchise) {
                                            $date_to_check_timestamp_debut = strtotime($value_franchise->franc_debut);
                                            $date_to_check_timestamp_fin = strtotime($value_franchise->franc_fin);

                                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                    $data['bail_id'] = $bail->bail_id;
                                    $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad($this->get_numero_factu($data_post['bail_id']), 5, '0', STR_PAD_LEFT);
                                    $data['periode_semestre_id'] = $periode_semestre_id;
                                    $data['facture_nombre'] = 1;
                                    $data['facture_annee'] = date('Y', strtotime($index['fin']));
                                    $data['dossier_id'] = $bail->dossier_id;
                                    $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                                    $data['emet_adress1'] = $addresse_emetteur['adress1'];
                                    $data['emet_adress2'] = $addresse_emetteur['adress2'];
                                    $data['emet_adress3'] = $addresse_emetteur['adress3'];
                                    $data['emet_cp'] = $addresse_emetteur['cp'];
                                    $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                                    $data['emet_ville'] = $addresse_emetteur['ville'];
                                    $data['dest_nom'] = $addresse_gestionnaire->gestionnaire_nom;
                                    $data['dest_adresse1'] = $addresse_gestionnaire->gestionnaire_adresse1;
                                    $data['dest_adresse2'] = $addresse_gestionnaire->gestionnaire_adresse2;
                                    $data['dest_adresse3'] = $addresse_gestionnaire->gestionnaire_adresse3;
                                    $data['dest_cp'] = $addresse_gestionnaire->gestionnaire_cp;
                                    $data['dest_pays'] = $addresse_gestionnaire->gestionnaire_pays;
                                    $data['dest_ville'] = $addresse_gestionnaire->gestionnaire_ville;
                                    $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                                    if (($bail->momfac_id  == 1)) {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    } else {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    }

                                    // Voir si la date de facture depasse le date du jour
                                    if ($data['fact_loyer_date'] > date('Y-m-d')) {
                                        break;
                                    }

                                    $date_fin_foreact_index = new DateTime($index['fin']);
                                    $date_debut_facture = $debut_facture;

                                    if ($date_debut_facture <= $date_fin_foreact_index || !empty($indexation_loyer_no_indice) || $periode_indexation == 3) {
                                        $this->FactureLoyer_m->save_data($data);
                                        $request_id = array(
                                            'order_by_columns' => "fact_loyer_id DESC",
                                            'join' => array('c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'),
                                            'limit' => 1,
                                            'method' => 'row'
                                        );
                                        $last_facture_loyer = $this->FactureLoyer_m->get($request_id);

                                        $this->generate_facture($last_facture_loyer);
                                    }
                                }
                            }
                        }
                    }
                }
                //Periodicité annuel
                elseif ($pdl_id == 4) {
                    $periode_annee = $this->PeriodeAnnuel_m->get(array());
                    foreach ($liste_annee as $key => $value) {
                        $list_full_mois = genererListeMoisDansAnnee($value, true);
                        $indexation = array();
                        foreach ($list_full_mois as $key => $value) {
                            if (($key + 1) % 12 === 0) {
                                $debut = date('Y-m-d', strtotime('-11 months', strtotime($value['debut'])));
                                $indexation[] = array(
                                    'debut' => $debut,
                                    'fin' => $value['fin'],
                                    'nom' => $value['nom'],
                                    'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                                );
                            }
                        }
                        foreach ($indexation as $index) {
                            $periode_annee_id = "";
                            foreach ($periode_annee as $key_periode_annee => $value_periode_annee) {
                                if (intval($value_periode_annee->periode_annee_libelle) == date('Y', strtotime($index['fin']))) {
                                    $periode_annee_id = $value_periode_annee->periode_annee_id;
                                }
                            }

                            $month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                            if (!empty($index['indexation'])) {
                                $data = array();
                                foreach ($index['indexation'] as $table_indexation) {
                                    $bail_date_echeance = new DateTime($bail->bail_date_echeance);
                                    $bail_debut_bail = new DateTime($bail->bail_debut);

                                    $date_prorata = '';

                                    if ($bail_date_echeance >= $bail_debut_bail) {
                                        $debut_facture = $bail_date_echeance;
                                        if (
                                            in_array(intval(date('m', strtotime($bail->bail_date_echeance))), $month) &&
                                            in_array(intval(date('m', strtotime($bail->bail_debut))), $month) &&
                                            date('Y', strtotime($bail->bail_date_echeance)) == date('Y', strtotime($bail->bail_debut))
                                        ) {
                                            $date_prorata = $bail->bail_debut;
                                        } else {
                                            $date_prorata = $indexation_loyer[0]->indlo_debut;
                                        }
                                    } else {
                                        $debut_facture = $bail_debut_bail;
                                        $date_prorata = $bail->bail_debut;
                                    }

                                    $indexation_loyer_date = new DateTime($indexation_loyer[0]->indlo_debut);
                                    if ($debut_facture < $indexation_loyer_date) {
                                        $date_prorata = $indexation_loyer[0]->indlo_debut;
                                    }

                                    if (!empty($getacte->acte_date_notarie) && $date_prorata < $getacte->acte_date_notarie) {
                                        $date_prorata = $getacte->acte_date_notarie;
                                        $debut_facture = new DateTime($getacte->acte_date_notarie);
                                    }

                                    $date_debut_year =  date('Y', strtotime($index['debut']));
                                    $date_debut_month =  date('m', strtotime($index['debut']));

                                    $debut_indexation_year = date('Y', strtotime($date_prorata));
                                    $debut_indexation_month = date('m', strtotime($date_prorata));

                                    $indlo_appartement_ht = $table_indexation->indlo_appartement_ht;
                                    $indlo_appartement_tva = $table_indexation->indlo_appartement_tva;
                                    $indlo_appartement_ttc = $table_indexation->indlo_appartement_ttc;
                                    $indlo_parking_ht = $table_indexation->indlo_parking_ht;
                                    $indlo_parking_tva = $table_indexation->indlo_parking_tva;
                                    $indlo_parking_ttc = $table_indexation->indlo_parking_ttc;

                                    $indlo_charge_ht = $table_indexation->indlo_charge_ht;
                                    $indlo_charge_tva = $table_indexation->indlo_charge_tva;
                                    $indlo_charge_ttc = $table_indexation->indlo_charge_ttc;

                                    if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                        $date1 = $date_prorata;
                                        $date2 = $index['fin'];

                                        $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                        $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                        $date11 = $index['debut'];
                                        $date22 = $index['fin'];

                                        $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                        $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                        if ($diff_in_days - 1 ==  $days_3month) {
                                            $diff_in_days =  $days_3month;
                                        }

                                        $indlo_appartement_ht = round((($table_indexation->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                        $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                        $indlo_parking_ht = round((($table_indexation->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                        $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                        $indlo_charge_ht = round((($table_indexation->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                                        $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                        $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                        $data['fact_prorata'] = date('d/m/Y', strtotime($date_prorata)) . ' - ' . date('d/m/Y', strtotime($index['fin']));
                                    }

                                    $franchise = $this->FranchiseLoyer_m->get(array(
                                        'clause' => array('bail_id' => $bail->bail_id, 'etat_franchise' => 1),
                                    ));

                                    if (!empty($franchise)) {
                                        $start_timestamp = strtotime($index['debut']);
                                        $end_timestamp = strtotime($index['fin']);

                                        foreach ($franchise as $key_franchise => $value_franchise) {
                                            $date_to_check_timestamp_debut = strtotime($value_franchise->franc_debut);
                                            $date_to_check_timestamp_fin = strtotime($value_franchise->franc_fin);

                                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                                if ($value_franchise->type_deduction == 1) {
                                                    $data['montant_deduction'] = $indlo_appartement_ht;
                                                    $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                    $data['tva_deduit'] = $indlo_appartement_tva;
                                                    $indlo_appartement_tva = $indlo_appartement_tva;
                                                    $indlo_appartement_ttc = 0;
                                                } else {
                                                    if ($value_franchise->franchise_mode_calcul == 0) {
                                                        $data['montant_deduction'] = $value_franchise->montant_ht;
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        $montant_ttc_deduit = $value_franchise->montant_ttc_deduit;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $ht = round((($value_franchise->montant_ht * $diff_in_days) / $days_3month), 2);
                                                            $tva = $ht * $value_franchise->tva / 100;
                                                            $montant_ttc_deduit = $ht + $tva;
                                                        }

                                                        $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                                    } else {
                                                        $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise->franchise_pourcentage / 100), 2);
                                                        $data['libelle_facture_deduit'] = $value_franchise->franc_explication;
                                                        $data['tva_deduit'] = $value_franchise->tva;

                                                        if ($date_debut_year == $debut_indexation_year && in_array(intval($date_debut_month), $month) && in_array(intval($debut_indexation_month), $month)) {
                                                            $data['montant_deduction'] = $montant_deduction = round((($montant_deduction * $diff_in_days) / $days_3month), 2);
                                                        }

                                                        $indlo_appartement_ttc = $value_franchise->tva != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise->tva / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                    $data['bail_id'] = $bail->bail_id;
                                    $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad($this->get_numero_factu($data_post['bail_id']), 5, '0', STR_PAD_LEFT);
                                    $data['periode_annee_id'] = $periode_annee_id;
                                    $data['facture_nombre'] = 1;
                                    $data['facture_annee'] = date('Y', strtotime($index['fin']));
                                    $data['dossier_id'] = $bail->dossier_id;
                                    $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                                    $data['emet_adress1'] = $addresse_emetteur['adress1'];
                                    $data['emet_adress2'] = $addresse_emetteur['adress2'];
                                    $data['emet_adress3'] = $addresse_emetteur['adress3'];
                                    $data['emet_cp'] = $addresse_emetteur['cp'];
                                    $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                                    $data['emet_ville'] = $addresse_emetteur['ville'];
                                    $data['dest_nom'] = $addresse_gestionnaire->gestionnaire_nom;
                                    $data['dest_adresse1'] = $addresse_gestionnaire->gestionnaire_adresse1;
                                    $data['dest_adresse2'] = $addresse_gestionnaire->gestionnaire_adresse2;
                                    $data['dest_adresse3'] = $addresse_gestionnaire->gestionnaire_adresse3;
                                    $data['dest_cp'] = $addresse_gestionnaire->gestionnaire_cp;
                                    $data['dest_pays'] = $addresse_gestionnaire->gestionnaire_pays;
                                    $data['dest_ville'] = $addresse_gestionnaire->gestionnaire_ville;
                                    $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                                    if (($bail->momfac_id  == 1)) {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    } else {
                                        $date = date_create();
                                        $fact_loyer_date = date_create();
                                        date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail->tpech_valeur);
                                        date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                        $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date_format($date, "d/m/Y");
                                        $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                        if (intval($bail->tpech_valeur) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                            $data['echeance_paiement'] = $bail->tpech_valeur . ' du mois (' . $bail->momfact_nom . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                        }
                                    }

                                    // Voir si la date de facture depasse le date du jour
                                    if ($data['fact_loyer_date'] > date('Y-m-d')) {
                                        break;
                                    }

                                    $date_fin_foreact_index = new DateTime($index['fin']);
                                    $date_debut_facture = $debut_facture;

                                    if ($date_debut_facture <= $date_fin_foreact_index || !empty($indexation_loyer_no_indice) || $periode_indexation == 3) {
                                        $this->FactureLoyer_m->save_data($data);
                                        $request_id = array(
                                            'order_by_columns' => "fact_loyer_id DESC",
                                            'join' => array('c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'),
                                            'limit' => 1,
                                            'method' => 'row'
                                        );
                                        $last_facture_loyer = $this->FactureLoyer_m->get($request_id);

                                        $this->generate_facture($last_facture_loyer);
                                    }
                                }
                            }
                        }
                    }
                }
                echo json_encode(
                    array(
                        'cliLot_id' => $bail->cliLot_id,
                        'bail_id' => $bail->bail_id,
                        'pdl_id' => $pdl_id
                    )
                );
            }
        }
    }

    public function NouveauBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la création de nouveau bail",
                );
                $this->load->model('Bail_m');
                $data = decrypt_service($_POST["data"]);
                $paramBail = array(
                    'clause' => array('bail_id' => $data['bail_id']),
                    'method' => "row",
                );
                $bail = $this->Bail_m->get($paramBail);

                if ($data['action'] == "demande") {
                    $message = "Voulez-vous créer un nouveau bail ? Cela cloturera automatique le bail en cours";
                    $confirmMessage = "Confirmer";
                    $titleMessage = "Confirmation du nouveau bail";

                    if ($bail->bail_cloture == 0) {
                        $message = "Un bail est encore ouvert, veuillez cloturer le bail avant de créer un nouveau. Voulez-vous créer le bail ?";
                        $confirmMessage = "Créer le bail";
                        $titleMessage = "Attention";
                    }

                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['bail_id'],
                            'bail_id' => $data['bail_id'],
                            'title' => $titleMessage,
                            'text' => $message,
                            'btnConfirm' => $confirmMessage,
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {

                    // cloturer le bail avant
                    if ($bail->bail_cloture == 0) {
                        $clause = array('bail_id' => $data['bail_id']);
                        $data_update = array('bail_cloture' => 1, 'bail_date_cloture' => date("Y-m-d"));
                        $this->Bail_m->save_data($data_update, $clause);
                    }

                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array('id' => $data['bail_id']),
                        'message' => "Nouveau bail crée",
                    );
                }
                echo json_encode($retour);
            }
        }
    }

    private function generate_facture($get_facture)
    {
        $this->load->helper("exportation");
        $this->load->model('FactureLoyer_m', 'Facture');
        $this->load->model('Rib_m');
        if (!is_null($get_facture->periode_mens_id)) {
            $_data['facture'] = $facture = $this->get_mensuel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_mens_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/export_facture_mensuel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } elseif (!is_null($get_facture->periode_timestre_id)) {
            $_data['facture'] = $facture = $this->get_trimestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $donnee = explode("(", $facture->echeance_paiement);
            $donnee_final = explode(")", $donnee[1]);
            $date = explode("=>", $facture->echeance_paiement);
            $moment_factu = $donnee_final[0];
            if ($moment_factu == "échu") {
                $date_str = trim($date[1]);
                $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
                $new_date_str = $date_obj->format('Y-m-d');
                $debut_trim = date('m-Y', strtotime('-3 months', strtotime($new_date_str)));
                $fin_trim = date('m-Y', strtotime('-1 months', strtotime($new_date_str)));
                $_data['debut_trim'] = $debut_trim;
                $_data['fin_trim'] = $fin_trim;
            } else {
                $date_str = trim($date[1]);
                $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
                $new_date_str = $date_obj->format('Y-m-d');
                $debut_trim = date('m-Y', strtotime('+0 months', strtotime($new_date_str)));
                $fin_trim = date('m-Y', strtotime('+2 months', strtotime($new_date_str)));
                $_data['debut_trim'] = $debut_trim;
                $_data['fin_trim'] = $fin_trim;
            }
            $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_trimesrte_libelle . '_' . $facture->emetteur_nom;
            $view = $get_facture->pdl_id == 2 ? "Bail/export_facture_trimestre" : "Bail/export_facture_trimestre_decale";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } elseif (!is_null($get_facture->periode_semestre_id)) {
            $_data['facture'] = $facture = $this->get_semestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_semestre_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/export_facture_semestriel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } else {
            $_data['facture'] = $facture = $this->get_annuel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_annee_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/export_facture_annuel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        }

        $param_rib = array(
            'clause' => array(),
            'order_by_columns' => "rib_id  DESC",
            'method' => "row"
        );
        $_data['rib'] = $this->Rib_m->get($param_rib);
        exporation_pdf($view, $file_name, $_data, $cliLot_id, $annee, $get_facture->fact_loyer_id);
    }

    private function getindexation($date, $bail_id)
    {
        $this->load->model('IndexationLoyer_m', 'index');
        $param_index = array(
            'clause' => array('bail_id ' => $bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );
        $data_indexation = $this->index->get($param_index);

        $interval = array();
        foreach ($data_indexation as $key => $value) {
            $interval[] = array(
                'id' => $value->indlo_id,
                'debut' => $value->indlo_debut,
                'fin' => $value->indlo_fin
            );
        }
        $value_index = null;
        foreach ($interval as $key => $value) {
            if ($this->verify_dateBetween($date, $value['debut'], $value['fin'])) {
                $currentIndexation = $data_indexation[$key];
                $value_index = array();
                array_push($value_index, $currentIndexation);
            }
        }
        return $value_index;
    }

    private function verify_dateBetween($post, $date_debut, $date_fin)
    {
        $post = date('Y-m-d', strtotime($post));
        $contractDateBegin = date('Y-m-d', strtotime($date_debut));
        $contractDateEnd = date('Y-m-d', strtotime($date_fin));
        return (($post >= $contractDateBegin) && ($post <= $contractDateEnd)) ? true : false;
    }

    private function getindexation_month($date_debut, $date_fin, $bail_id)
    {
        $this->load->model('IndexationLoyer_m', 'index');
        $param_index = array(
            'clause' => array('bail_id ' => $bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );
        $data_indexation = $this->index->get($param_index);

        $interval = array();
        foreach ($data_indexation as $key => $value) {
            $interval[] = array(
                'id' => $value->indlo_id,
                'debut' => $value->indlo_debut,
                'fin' => $value->indlo_fin
            );
        }
        $value_index = null;
        foreach ($interval as $key => $value) {
            if ($this->verify_dateBetween_month($date_debut, $date_fin, $value['debut'], $value['fin'])) {
                $currentIndexation = $data_indexation[$key];
                $value_index = array();
                array_push($value_index, $currentIndexation);
            }
        }
        return $value_index;
    }

    private function verify_dateBetween_month($post_debut, $post_fin, $date_debut, $date_fin)
    {
        $post_debut = date('Y-m-d', strtotime($post_debut));
        $post_fin = date('Y-m-d', strtotime($post_fin));
        $contractDateBegin = date('Y-m-d', strtotime($date_debut));
        $contractDateEnd = date('Y-m-d', strtotime($date_fin));

        if (($post_debut >= $contractDateBegin) && ($post_fin <= $contractDateEnd)) return true;
        if (($contractDateBegin <= $post_fin) && ($post_fin <= $contractDateEnd)) return true;
        // if (($contractDateEnd >= $post_debut) && ($contractDateEnd <= $post_fin)) return true;

        return false;
    }

    private function get_numero_factu($bail_id)
    {
        $this->load->model('FactureLoyer_m', 'facture');
        $this->load->model('Bail_m', 'bail');

        $param = array(
            'clause' => array('bail_id' => $bail_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            ),
            'method' => 'row'
        );

        $res = $this->bail->get($param);

        $params_get_dossier = array(
            'clause' => array('dossier_id' => $res->dossier_id),
            'order_by_columns' => "fact_loyer_id  DESC",
            'limit' => 1,
            'method' => 'row'
        );

        $facture_dossier = $this->facture->get($params_get_dossier);

        if (empty($facture_dossier)) {
            $facture_num_suivant = 1;
        } else {
            $num_factu_precedent = explode("-", $facture_dossier->fact_loyer_num);
            $facture_num_suivant = intval($num_factu_precedent[1]) + 1;
        }
        return $facture_num_suivant;
    }

    private function get_emetteur($bail_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('Program_m', 'program');
        $this->load->model('Client_m', 'client');
        $param = array(
            'clause' => array('c_bail.bail_id' => $bail_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            ),
            'method' => 'row'
        );
        $get_emet = $this->bail->get($param);
        $array_client = explode(',', $get_emet->client_id);
        $data['emetteur'] = NULL;
        foreach ($array_client as $key => $value) {
            $client = $this->client->get(
                array(
                    'columns' => array('client_id', 'client_nom', 'client_prenom'),
                    'clause' => array('client_id' => $value),
                    'method' => 'row'
                )
            );
            if (!empty($client)) {
                if (!empty($data['emetteur'])) {
                    $data['emetteur'] .= ' & ';
                }
                $data['emetteur'] .= $client->client_nom . ' ' . $client->client_prenom;
            }
        }
        $data['adress1'] = $get_emet->dossier_adresse1;
        $data['adress2'] = $get_emet->dossier_adresse2;
        $data['adress3'] = $get_emet->dossier_adresse3;
        $data['cp'] = $get_emet->dossier_cp;
        $data['ville'] = $get_emet->dossier_ville;
        $data['pays'] = $get_emet->dossier_pays;
        if ($get_emet->dossier_type_adresse == 'Lot principal') {
            $params = array(
                'clause' => array('progm_id' => $get_emet->progm_id),
                'method' => 'row'
            );
            $get_program = $this->program->get($params);
            if ($get_program->progm_id == 309) {
                $data['adress1'] = $get_emet->cliLot_adresse1;
                $data['adress2'] = $get_emet->cliLot_adresse2;
                $data['adress3'] = $get_emet->cliLot_adresse3;
                $data['cp'] = $get_emet->cliLot_cp;
                $data['ville'] = $get_emet->cliLot_ville;
                $data['pays'] = $get_emet->cliLot_pays;
            } else {
                $data['adress1'] = $get_program->progm_adresse1;
                $data['adress2'] = $get_program->progm_adresse2;
                $data['adress3'] = $get_program->progm_adresse3;
                $data['cp'] = $get_program->progm_cp;
                $data['ville'] = $get_program->progm_ville;
                $data['pays'] = $get_program->progm_pays;
            }
        }

        return $data;
    }

    private function get_mensuel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');
        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_mensuel' => 'c_periode_mensuel.periode_mens_id  = c_facture_loyer.periode_mens_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id',
                'c_utilisateur' => 'c_utilisateur.util_id  = c_dossier.util_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );
        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_semestriel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');
        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_semestre' => 'c_periode_semestre.periode_semestre_id = c_facture_loyer.periode_semestre_id ',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id',
                'c_utilisateur' => 'c_utilisateur.util_id  = c_dossier.util_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );
        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_trimestriel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');
        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_trimestre' => 'c_periode_trimestre.periode_timestre_id  = c_facture_loyer.periode_timestre_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id',
                'c_utilisateur' => 'c_utilisateur.util_id  = c_dossier.util_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );
        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    private function get_annuel($bail_id, $fact_loyer_id)
    {
        $this->load->model('FactureLoyer_m', 'Facture');

        $param = array(
            'clause' => array('c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.bail_id ' => $bail_id, 'c_facture_loyer.fact_loyer_id' => $fact_loyer_id),
            'join' => array(
                'c_bail' => 'c_bail.bail_id = c_facture_loyer.bail_id',
                'c_periode_annee' => 'c_periode_annee.periode_annee_id = c_facture_loyer.periode_annee_id',
                'c_lot_client' => 'c_lot_client.cliLot_id  = c_bail.cliLot_id',
                'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id',
                'c_utilisateur' => 'c_utilisateur.util_id  = c_dossier.util_id'
            ),
            'limit' => 1,
            'method' => 'row'
        );

        $get_facture = $this->Facture->get($param);
        return $get_facture;
    }

    public function supprimer_facture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FactureLoyer_m');
                $this->load->model('Bail_m');
                $this->load->model('DocumentLoyer_m');
                $data_post = $_POST['data'];
                $clause_bail = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'method' => 'row'
                );
                $bail = $this->Bail_m->get($clause_bail);
                $clause_facture = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                );
                $get_factureloyer = $this->FactureLoyer_m->get($clause_facture);
                $table_id_facture = array();
                foreach ($get_factureloyer as $key => $value) {
                    array_push($table_id_facture, $value->fact_loyer_id);
                }
                $clause_facture = array(
                    'clause' => array('cliLot_id' => $bail->cliLot_id),
                );
                $doc_loyer = $this->DocumentLoyer_m->get($clause_facture);
                foreach ($doc_loyer as $key_doc_loyer => $value_doc_loyer) {
                    if (in_array($value_doc_loyer->fact_loyer_id, $table_id_facture)) {
                        $clause_doc_loyer = array('fact_loyer_id' => $value_doc_loyer->fact_loyer_id);
                        $this->DocumentLoyer_m->delete_data($clause_doc_loyer);
                    }
                }
                $clause = array('bail_id' => $data_post['bail_id']);
                $this->FactureLoyer_m->delete_data($clause);
            }
        }
    }

    public function NouveauAvenant()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Bail_m');
                $data_post = $_POST['data'];
                $paramBail = array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => "row",
                );
                $bail = $this->Bail_m->get($paramBail);

                echo json_encode($bail);
            }
        }
    }

    public function cloturerBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('Bail_m');
                $data_post = $_POST['data'];
                $paramBail = array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => "row",
                );
                $bail = $this->Bail_m->get($paramBail);

                echo json_encode($bail);
            }
        }
    }

    public function validerCloture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m', 'bail');
                $data_post = $_POST['data'];

                $params = array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => "row",
                );
                $bail = $this->bail->get($params);

                $clause = array('bail_id' => $data_post['bail_id']);
                $data_update = array(
                    'bail_cloture' => 1,
                    'bail_date_cloture' => $data_post['date_cloture'],
                    'bail_util_id_cloture' => $_SESSION['session_utilisateur']['util_id']
                );
                $request = $this->bail->save_data($data_update, $clause);

                if ($bail->bail_avenant_id_origin) {
                    $clause_bail = array('bail_id' => $bail->bail_avenant_id_origin);
                    $clause_avenants = array('bail_avenant_id_origin' => $bail->bail_avenant_id_origin);
                    $data_update_avenants = array(
                        'bail_cloture' => 1,
                        'bail_date_cloture' => $data_post['date_cloture'],
                        'bail_util_id_cloture' => $_SESSION['session_utilisateur']['util_id']
                    );
                    $request = $this->bail->save_data($data_update_avenants, $clause_bail);
                    $request = $this->bail->save_data($data_update_avenants, $clause_avenants);
                }
                $this->gererAvoirAndFactures_cloture_bail($data_post['bail_id'], $data_post['date_cloture']);
                echo json_encode($data_post);
            }
        }
    }

    private function gererAvoirAndFactures_cloture_bail($bail_id, $date_cloture)
    {
        $this->load->model('IndexationLoyer_m');
        $this->load->model('FactureLoyer_m');
        $this->load->model('DocumentLoyer_m');
        $this->load->model('Bail_m');

        $param_bail = array(
            'clause' => array('bail_id' => $bail_id),
            'method' => 'row'
        );

        $bail = $this->Bail_m->get($param_bail);

        $pdl_id = $bail->pdl_id;
        $moment_factu = $bail->momfac_id;

        $clause = array(
            'clause' => array('bail_id' => $bail->bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );

        $liste_indexation = $this->IndexationLoyer_m->get($clause);

        if (!empty($liste_indexation)) {
            foreach ($liste_indexation as $key => $value) {
                $start_date = $value->indlo_debut;
                $end_date = $value->indlo_fin;

                $start_timestamp = strtotime($start_date);
                $end_timestamp = strtotime($end_date);
                $date_to_check_timestamp = strtotime($date_cloture);
                $indlo_id = $value->indlo_id;

                if ($date_to_check_timestamp <  $start_timestamp) {
                    $clause_delete = array('bail_id' => $bail->bail_id, 'indlo_id' => $indlo_id);

                    $this->IndexationLoyer_m->delete_data($clause_delete);
                } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {
                    $clause = array("indlo_id" => $indlo_id);
                    $data = array(
                        'indlo_fin' => $date_cloture
                    );

                    $request = $this->IndexationLoyer_m->save_data($data, $clause);
                }
            }

            $params_factures = array(
                'clause' => array('bail_id' => $bail->bail_id),
            );

            $liste_factures = $this->FactureLoyer_m->get($params_factures);

            if (!empty($liste_factures)) {
                foreach ($liste_factures as $key => $value) {
                    switch ($pdl_id) {
                        case 2:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);
                            $date_to_check_timestamp = strtotime($date_cloture);
                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp <  $start_timestamp) {

                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

                                $date1 = $start_date;
                                $date2 = $date_cloture;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            }

                            break;
                        case 3:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+5 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-6 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);
                            $date_to_check_timestamp = strtotime($date_cloture);
                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp <  $start_timestamp) {

                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);
                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

                                $date1 = $start_date;
                                $date2 = $date_cloture;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            }
                            break;
                        case 4:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+11 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-12 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);
                            $date_to_check_timestamp = strtotime($date_cloture);
                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp <  $start_timestamp) {

                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);
                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

                                $date1 = $start_date;
                                $date2 = $date_cloture;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            }
                            break;
                        case 5:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);
                            $date_to_check_timestamp = strtotime($date_cloture);
                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp <  $start_timestamp) {

                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);
                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

                                $date1 = $start_date;
                                $date2 = $date_cloture;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            }
                            break;

                        default:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime($start_date));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-1 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime($start_date));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);
                            $date_to_check_timestamp = strtotime($date_cloture);
                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp <  $start_timestamp) {

                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {
                                $date1 = new DateTime($start_date);
                                $date2 = new DateTime($date_cloture);

                                $interval = date_diff($date1, $date2);
                                $diff = (intval($interval->format(' %d '))) + 1;

                                $end_month = (date("d", strtotime($end_date))) + 1;

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff) / $end_month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff) / $end_month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff) / $end_month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            }

                            break;
                    }
                }
            }
        }
    }

    public function generer_indexation_fixe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m');
                $this->load->model('IndiceLoyer_m');
                $this->load->model('IndexationLoyer_m');
                $this->load->model('IndiceValeur_m');
                $this->load->model('IndiceValeurLoyer_m');

                $data_post = $_POST['data'];
                $retour['data_post'] = $data_post;

                $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);
                $this->IndexationLoyer_m->delete_data($clause_delete);

                $bail = $this->Bail_m->get(array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'method' => 'row'
                ));

                $last_indexation_infirst = $this->getLastIndexation($bail->bail_id);

                $indloyer_id =  $bail->indloyer_id;

                $maxIndice = $this->IndiceValeur_m->get(
                    array(
                        'columns' => array('indval_id', 'indval_annee'),
                        'clause' => array(
                            'indloyer_id' => $indloyer_id
                        ),
                        'order_by_columns' => "indval_annee DESC",
                        'limit' => 1,
                        'method' => 'row'
                    )
                );

                $periode_valeur = $bail->prdind_id;

                if ($bail->bail_debut < $last_indexation_infirst->indlo_debut) {
                    $start_year = intval(date('Y', strtotime($bail->bail_debut)));
                } else {
                    $start_year = intval(date('Y', strtotime($last_indexation_infirst->indlo_debut)));
                }

                $end_year = intval($maxIndice->indval_annee);
                //si endYear inférieur à startYear
                $end_year = intval($maxIndice->indval_annee) < $start_year ? $start_year + $periode_valeur : $end_year;

                $clause_indice = array(
                    'clause' => array('indloyer_id' => $indloyer_id),
                    'method' => 'row'
                );

                if ($periode_valeur == 2) {
                    $debut = date('Y', strtotime($last_indexation_infirst->indlo_debut));
                    $fin = $end_year;
                    $pas = 3;
                    $tableauAnnees = array();

                    for ($annee = $debut; $annee <= $fin; $annee += $pas) {
                        $tableauAnnees[] = $annee;
                    }
                }

                $indice = $this->IndiceLoyer_m->get($clause_indice);

                $tab = array();

                for ($annee = $start_year; $annee <= $end_year; $annee++) {

                    $checkIndicefixe = $this->IndexationLoyer_m->get(array(
                        'clause' => array('bail_id' => $bail->bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
                    ));

                    $checkIndicefixe = !empty($checkIndicefixe) ? count($checkIndicefixe) : 0;

                    $indice_base =  $this->IndiceValeurLoyer_m->get(
                        array(
                            'clause' => array('indval_annee' => $annee, 'indloyer_id' => $indloyer_id, 'indval_valide' => 1),
                            'method' => 'row'
                        )
                    );

                    $new_indice_reference =  $this->getIndiceReferenceAnnuelle($indloyer_id, ($annee));

                    $last_index = $this->getLastIndexation($bail->bail_id);

                    $data_index = array();
                    if (!empty($new_indice_reference)) {
                        $data_index['bail_id'] = $bail->bail_id;
                        $data_index['etat_indexation'] = 1;
                        $data_index['indlo_indice_base'] = $indice_base->indval_id;
                        $data_index['indlo_indice_reference'] = $new_indice_reference->indval_id;
                        $data_index['indlo_ref_loyer'] = 0;

                        $array_variation = array();

                        if ($indice->mode_calcul == 1) {
                            $variation_loyer = (($new_indice_reference->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100;
                        } else {
                            $variation_loyer = $checkIndicefixe >= 1 ? $new_indice_reference->indval_valeur : 0;
                        }

                        array_push($array_variation, $variation_loyer);

                        if ($bail->bail_var_plafonnee != 0) {
                            $variation_plafonnee = (floatval($bail->bail_var_plafonnee) * $variation_loyer) / 100;
                            array_push($array_variation, $variation_plafonnee);
                        }
                        if ($bail->bail_var_capee != 0) {
                            $variation_capee = floatval($bail->bail_var_capee);
                            array_push($array_variation, $variation_capee);
                        }

                        $variation_appliquee = round(min($array_variation), 2);

                        if (floatval($bail->bail_var_min) != 0 && floatval($bail->bail_var_min) > floatval($variation_appliquee)) {
                            $variation_appliquee = floatval($bail->bail_var_min);
                        }

                        if ($checkIndicefixe == 1) {
                            $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                            $day = date('d', strtotime($last_indexation_infirst->indlo_debut));
                            $first_debut_indexation = date('Y-m-d', strtotime($annee - 1 . '-' . $month . '-' . $day));
                            $bail_debut = $bail->bail_debut;
                            $first_fin_indexation = date('Y-m-d', strtotime(($annee) . '-' . $month . '-' . ($day - 1)));

                            $date1 = new DateTime($bail_debut);
                            $date2 = new DateTime($first_fin_indexation);
                            $interval = $date1->diff($date2);
                            $diffEnJours_prorata = $interval->days + 1;

                            $dateindex1 = new DateTime($first_debut_indexation);
                            $dateindex2 = new DateTime($first_fin_indexation);
                            $interval = $dateindex1->diff($dateindex2);
                            $diffEnJours_index = $interval->days + 1;

                            $variation_appliquee = (round($diffEnJours_prorata / $diffEnJours_index, 10)) * $new_indice_reference->indval_valeur;
                        }

                        $data_index['indlo_variation_appliquee'] = $variation_appliquee;

                        $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                        $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index->indlo_appartement_ht + (($last_index->indlo_appartement_ht * $variation_appliquee) / 100), 2);
                        $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index->indlo_parking_ht + (($last_index->indlo_parking_ht * $variation_appliquee) / 100), 2);
                        $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index->indlo_charge_ht + (($last_index->indlo_charge_ht * $variation_appliquee) / 100), 2);

                        $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index->indlo_appartement_tva;
                        $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index->indlo_parking_tva;
                        $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index->indlo_charge_tva;

                        $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                        $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                        $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                        $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                        $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                        $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                        $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                        $day = date('d', strtotime($last_indexation_infirst->indlo_debut));

                        $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                        $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                        if ($periode_valeur == 2) {
                            if (in_array($annee, $tableauAnnees)) {
                                $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 3) . '-' . $month . '-' . ($day - 1)));
                            } else {
                                array_push($tab, $data_index['indlo_debut']);
                            }
                        }

                        $this->IndexationLoyer_m->save_data($data_index);
                    }
                }

                $this->IndexationLoyer_m->update_where_date($last_indexation_infirst->indlo_debut, $bail->bail_id);

                if ($periode_valeur == 2) {
                    foreach ($tab as $key => $value) {
                        if ($last_indexation_infirst->indlo_debut < $value) {
                            $this->IndexationLoyer_m->delete_where_index_date($value, $bail->bail_id);
                        }
                    }
                }
            }
        }
    }

    public function generer_indexation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m');
                $this->load->model('IndiceLoyer_m');
                $this->load->model('IndexationLoyer_m');
                $this->load->model('IndiceValeur_m');
                $this->load->model('IndiceValeurLoyer_m');

                $data_post = $_POST['data'];
                $retour['data_post'] = $data_post;
                $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);
                $this->IndexationLoyer_m->delete_data($clause_delete);

                $clause_bail = array(
                    'clause' => array('bail_id ' => $data_post['bail_id']),
                    'method' => 'row'
                );
                $bail = $this->Bail_m->get($clause_bail);

                $last_indexation_infirst = $this->getLastIndexation($bail->bail_id);

                $indloyer_id =  $bail->indloyer_id;

                $maxIndice = $this->IndiceValeur_m->get(
                    array(
                        'columns' => array('indval_id', 'indval_annee'),
                        'clause' => array(
                            'indloyer_id' => $indloyer_id
                        ),
                        'order_by_columns' => "indval_annee DESC",
                        'limit' => 1,
                        'method' => 'row'
                    )
                );

                $periode_valeur = $bail->prdind_id;

                if ($bail->bail_debut < $last_indexation_infirst->indlo_debut) {
                    $start_year = intval(date('Y', strtotime($bail->bail_debut)));
                } else {
                    $start_year = intval(date('Y', strtotime($last_indexation_infirst->indlo_debut)));
                }

                $month_of_indexation = intval(date('m', strtotime($last_indexation_infirst->indlo_debut)));
                $end_year = intval($maxIndice->indval_annee);
                //si endYear inférieur à startYear
                $end_year = intval($maxIndice->indval_annee) < $start_year ? $start_year + $periode_valeur : $end_year;

                $clause_indice = array(
                    'clause' => array('indloyer_id' => $indloyer_id),
                    'method' => 'row'
                );

                $indice = $this->IndiceLoyer_m->get($clause_indice);

                if ($periode_valeur == 1) {
                    for ($annee = $start_year; $annee <= $end_year; $annee++) {
                        $indice_base = $this->getIndiceBase($bail->bail_id);

                        if ($indice->indloyer_periode == "Annuelle") {
                            $new_indice_reference =  $this->getIndiceReferenceAnnuelle($indloyer_id, ($annee));
                        } else {
                            $new_indice_reference =  $this->getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base->indval_trimestre);
                        }

                        $last_index = $this->getLastIndexation($bail->bail_id);
                        $data_index = array();

                        if (!empty($new_indice_reference) && date('Y', strtotime($bail->bail_date_premiere_indexation)) <= $annee) {
                            $data_index['bail_id'] = $bail->bail_id;
                            $data_index['etat_indexation'] = 1;
                            $data_index['indlo_indice_base'] = $indice_base->indval_id;
                            $data_index['indlo_indice_reference'] = $new_indice_reference->indval_id;
                            $data_index['indlo_ref_loyer'] = 0;

                            $array_variation = array();

                            if ($indice->mode_calcul == 1) {
                                $variation_loyer = (($new_indice_reference->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100;
                            } else {
                                $variation_loyer = $new_indice_reference->indval_valeur;
                            }

                            array_push($array_variation, $variation_loyer);

                            if ($bail->bail_var_plafonnee != 0) {
                                $variation_plafonnee = (floatval($bail->bail_var_plafonnee) * $variation_loyer) / 100;
                                array_push($array_variation, $variation_plafonnee);
                            }
                            if ($bail->bail_var_capee != 0) {
                                $variation_capee = floatval($bail->bail_var_capee);
                                array_push($array_variation, $variation_capee);
                            }

                            $variation_appliquee = round(min($array_variation), 2);

                            if (floatval($bail->bail_var_min) != 0 && floatval($bail->bail_var_min) > floatval($variation_appliquee)) {
                                $variation_appliquee = floatval($bail->bail_var_min);
                            }

                            if ($bail->indice_valeur_plafonnement != 0) {
                                $indice_plafonne = $this->IndiceLoyer_m->get(array(
                                    'clause' => array('indloyer_id' => $bail->indice_valeur_plafonnement),
                                    'method' => 'row'
                                ));

                                if ($indice_plafonne->indloyer_periode == "Annuelle") {
                                    $clauseBase = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $indice_base->indval_annee, 'indval_valide' => 1);
                                    $clauseReference = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $annee, 'indval_valide' => 1);
                                } else {
                                    $clauseBase = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $indice_base->indval_annee, 'indval_trimestre' => $indice_base->indval_trimestre, 'indval_valide' => 1);
                                    $clauseReference = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $new_indice_reference->indval_annee, 'indval_trimestre' => $new_indice_reference->indval_trimestre, 'indval_valide' => 1);
                                }

                                $indice_base_plafonne = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => $clauseBase,
                                    'method' => 'row'
                                ));

                                $new_indice_reference_plafonne = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => $clauseReference,
                                    'method' => 'row'
                                ));

                                if ($indice_plafonne->mode_calcul == 1) {
                                    $variation_loyer_plafonnee = round(((($new_indice_reference_plafonne->indval_valeur - $indice_base_plafonne->indval_valeur) / $indice_base_plafonne->indval_valeur) * 100), 2);
                                } else {
                                    $variation_loyer_plafonnee = $new_indice_reference_plafonne->indval_valeur;
                                }

                                $variation_appliquee = min($variation_loyer_plafonnee, $variation_appliquee);
                            }

                            $data_index['indlo_variation_appliquee'] = $variation_appliquee;
                            $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                            $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index->indlo_appartement_ht + (($last_index->indlo_appartement_ht * $variation_appliquee) / 100), 2);
                            $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index->indlo_parking_ht + (($last_index->indlo_parking_ht * $variation_appliquee) / 100), 2);
                            $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index->indlo_charge_ht + (($last_index->indlo_charge_ht * $variation_appliquee) / 100), 2);

                            $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index->indlo_appartement_tva;
                            $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index->indlo_parking_tva;
                            $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index->indlo_charge_tva;

                            $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                            $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                            $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                            $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                            $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                            $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                            $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                            $day = date('d', strtotime($last_indexation_infirst->indlo_debut));
                            $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                            $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                            $this->IndexationLoyer_m->save_data($data_index);
                        } else {
                            if ($annee <= date('Y', strtotime($bail->bail_date_premiere_indexation))) {

                                $indice_base = $this->getIndiceBase($bail->bail_id);

                                if ($indice->indloyer_periode == "Annuelle") {
                                    $new_indice_reference =  $this->getIndiceReferenceAnnuelle($indloyer_id, $indice_base->indval_annee + 1);
                                } else {
                                    $new_indice_reference =  $this->getIndiceReferenceTrimestrielle($indloyer_id, $indice_base->indval_annee + 1, $month_of_indexation, $indice_base->indval_trimestre);
                                }

                                $data = $this->IndexationLoyer_m->get(
                                    array(
                                        'clause' => array(
                                            'bail_id' => $data_post['bail_id'],
                                            'indlo_ref_loyer' => 1
                                        ),
                                        'method' => 'row'
                                    )
                                );

                                $data->indlo_indice_base = $indice_base->indval_id;
                                $data->indlo_indice_reference = $new_indice_reference->indval_id;

                                $array_variation = array();

                                if ($indice->mode_calcul == 1) {
                                    $variation_loyer = (($new_indice_reference->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100;
                                } else {
                                    $variation_loyer = $new_indice_reference->indval_valeur;
                                }

                                array_push($array_variation, $variation_loyer);

                                if ($bail->bail_var_plafonnee != 0) {
                                    $variation_plafonnee = (floatval($bail->bail_var_plafonnee) * $variation_loyer) / 100;
                                    array_push($array_variation, $variation_plafonnee);
                                }
                                if ($bail->bail_var_capee != 0) {
                                    $variation_capee = floatval($bail->bail_var_capee);
                                    array_push($array_variation, $variation_capee);
                                }
                                $variation_appliquee = round(min($array_variation), 2);

                                if (floatval($bail->bail_var_min) != 0 && floatval($bail->bail_var_min) > floatval($variation_appliquee)) {
                                    $variation_appliquee = floatval($bail->bail_var_min);
                                }

                                $data->indlo_variation_appliquee = $variation_appliquee;
                                $data->indlo_variation_loyer = round($variation_loyer, 2);

                                $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                                $day = date('d', strtotime($last_indexation_infirst->indlo_debut));

                                unset($data->indlo_id);
                                $data->indlo_debut = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                                $data->indlo_fin = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                                $data->indlo_ref_loyer = 0;
                                $data->indlo_indince_non_publie = 1;

                                $request = $this->IndexationLoyer_m->save_data($data);
                            }
                        }
                    }
                } else {
                    for ($annee = $start_year; $annee <= $end_year; $annee++) {
                        $indice_base = $this->getIndiceBase($bail->bail_id);

                        if ($indice->indloyer_periode == "Annuelle") {
                            $new_indice_reference =  $this->getIndiceReferenceAnnuelle($indloyer_id, ($annee));
                        } else {
                            $new_indice_reference =  $this->getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base->indval_trimestre);
                        }

                        $last_index = $this->getLastIndexation($bail->bail_id);
                        if (!empty($new_indice_reference) && date('Y', strtotime($bail->bail_date_premiere_indexation)) <= $annee) {
                            $data_index = array();
                            $data_index['bail_id'] = $bail->bail_id;
                            $data_index['etat_indexation'] = 1;
                            $data_index['indlo_indice_base'] = $indice_base->indval_id;
                            $data_index['indlo_indice_reference'] = $new_indice_reference->indval_id;
                            $data_index['indlo_ref_loyer'] = 0;

                            $array_variation = array();

                            if ($bail->bail_mode_calcul == 0) {
                                if ($indice->mode_calcul == 1) {
                                    $variation_loyer = (($new_indice_reference->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100;
                                } else {
                                    $variation_loyer =  $new_indice_reference->indval_valeur;
                                }
                            } else {
                                $new_indice_reference1 = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => array('indloyer_id' => $indloyer_id, 'indval_annee' => $indice_base->indval_annee + 1, 'indval_trimestre' => $indice_base->indval_trimestre, 'indval_valide' => 1),
                                    'method' => 'row'
                                ));

                                if (empty($new_indice_reference1)) {
                                    break;
                                }

                                $new_indice_reference2 = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => array('indloyer_id' => $indloyer_id, 'indval_annee' => $new_indice_reference1->indval_annee + 1, 'indval_trimestre' => $new_indice_reference1->indval_trimestre, 'indval_valide' => 1),
                                    'method' => 'row'
                                ));

                                if (empty($new_indice_reference2)) {
                                    break;
                                }

                                if ($indice->mode_calcul == 1) {
                                    $first_year_variation = round(((($new_indice_reference1->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100), 2);
                                    $second_year_variation = round(((($new_indice_reference2->indval_valeur - $new_indice_reference1->indval_valeur) / $new_indice_reference1->indval_valeur) * 100), 2);
                                    $third_year_variation = round(((($new_indice_reference->indval_valeur - $new_indice_reference2->indval_valeur) / $new_indice_reference2->indval_valeur) * 100), 2);
                                } else {
                                    $first_year_variation = $new_indice_reference1->indval_valeur;
                                    $second_year_variation = $new_indice_reference2->indval_valeur;
                                    $third_year_variation = $new_indice_reference->indval_valeur;
                                }

                                $variation_loyer = round((($first_year_variation + $second_year_variation + $third_year_variation) / 3), 2);
                            }

                            array_push($array_variation, $variation_loyer);

                            if ($bail->bail_var_plafonnee != 0) {
                                $variation_plafonnee = (floatval($bail->bail_var_plafonnee) * $variation_loyer) / 100;
                                array_push($array_variation, $variation_plafonnee);
                            }
                            if ($bail->bail_var_capee != 0) {
                                $variation_capee = floatval($bail->bail_var_capee);
                                array_push($array_variation, $variation_capee);
                            }

                            $variation_appliquee = round(min($array_variation), 2);

                            if (floatval($bail->bail_var_min) != 0 && floatval($bail->bail_var_min) > floatval($variation_appliquee)) {
                                $variation_appliquee = floatval($bail->bail_var_min);
                            }

                            if ($bail->indice_valeur_plafonnement != 0) {
                                $indice_plafonne = $this->IndiceLoyer_m->get(array(
                                    'clause' => array('indloyer_id' => $bail->indice_valeur_plafonnement),
                                    'method' => 'row'
                                ));

                                if ($indice_plafonne->indloyer_periode == "Annuelle") {
                                    $clauseBase = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $indice_base->indval_annee, 'indval_valide' => 1);
                                    $clauseReference = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $annee, 'indval_valide' => 1);
                                } else {
                                    $clauseBase = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $indice_base->indval_annee, 'indval_trimestre' => $indice_base->indval_trimestre, 'indval_valide' => 1);
                                    $clauseReference = array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $new_indice_reference->indval_annee, 'indval_trimestre' => $new_indice_reference->indval_trimestre, 'indval_valide' => 1);
                                }

                                $indice_base_plafonne = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => $clauseBase,
                                    'method' => 'row'
                                ));

                                $new_indice_reference_plafonne = $this->IndiceValeurLoyer_m->get(array(
                                    'clause' => $clauseReference,
                                    'method' => 'row'
                                ));

                                if ($bail->bail_mode_calcul == 0) {
                                    if ($indice_plafonne->mode_calcul == 1) {
                                        $variation_loyer_plafonnee = round(((($new_indice_reference_plafonne->indval_valeur - $indice_base_plafonne->indval_valeur) / $indice_base_plafonne->indval_valeur) * 100), 2);
                                    } else {
                                        $variation_loyer_plafonnee = $new_indice_reference_plafonne->indval_valeur;
                                    }
                                } else {
                                    $new_indice_reference1 = $this->IndiceValeurLoyer_m->get(array(
                                        'clause' => array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $indice_base_plafonne->indval_annee + 1, 'indval_trimestre' => $indice_base_plafonne->indval_trimestre, 'indval_valide' => 1),
                                        'method' => 'row'
                                    ));

                                    if (empty($new_indice_reference1)) {
                                        break;
                                    }

                                    $new_indice_reference2 = $this->IndiceValeurLoyer_m->get(array(
                                        'clause' => array('indloyer_id' => $bail->indice_valeur_plafonnement, 'indval_annee' => $new_indice_reference1->indval_annee + 1, 'indval_trimestre' => $new_indice_reference1->indval_trimestre, 'indval_valide' => 1),
                                        'method' => 'row'
                                    ));

                                    if (empty($new_indice_reference2)) {
                                        break;
                                    }

                                    if ($indice_plafonne->mode_calcul == 1) {
                                        $first_year_variation = round(((($new_indice_reference1->indval_valeur - $indice_base_plafonne->indval_valeur) / $indice_base_plafonne->indval_valeur) * 100), 2);
                                        $second_year_variation = round(((($new_indice_reference2->indval_valeur - $new_indice_reference1->indval_valeur) / $new_indice_reference1->indval_valeur) * 100), 2);
                                        $third_year_variation = round(((($new_indice_reference_plafonne->indval_valeur - $new_indice_reference2->indval_valeur) / $new_indice_reference2->indval_valeur) * 100), 2);
                                    } else {
                                        $first_year_variation = $new_indice_reference1->indval_valeur;
                                        $second_year_variation = $new_indice_reference2->indval_valeur;
                                        $third_year_variation = $new_indice_reference_plafonne->indval_valeur;
                                    }

                                    $variation_loyer_plafonnee = round((($first_year_variation + $second_year_variation + $third_year_variation) / 3), 2);
                                }

                                $variation_appliquee = min($variation_loyer_plafonnee, $variation_appliquee);
                            }

                            $data_index['indlo_variation_appliquee'] = $variation_appliquee;
                            $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                            $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index->indlo_appartement_ht + (($last_index->indlo_appartement_ht * $variation_appliquee) / 100), 2);
                            $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index->indlo_parking_ht + (($last_index->indlo_parking_ht * $variation_appliquee) / 100), 2);
                            $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index->indlo_charge_ht + (($last_index->indlo_charge_ht * $variation_appliquee) / 100), 2);

                            $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index->indlo_appartement_tva;
                            $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index->indlo_parking_tva;
                            $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index->indlo_charge_tva;

                            $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                            $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                            $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                            $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                            $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                            $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                            $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                            $day = date('d', strtotime($last_indexation_infirst->indlo_debut));
                            $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                            $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 3) . '-' . $month . '-' . ($day - 1)));

                            $this->IndexationLoyer_m->save_data($data_index);

                            $annee += 2;
                        } else {
                            if ($annee <= date('Y', strtotime($bail->bail_date_premiere_indexation))) {

                                $indice_base = $this->getIndiceBase($bail->bail_id);

                                if ($indice->indloyer_periode == "Annuelle") {
                                    $new_indice_reference =  $this->getIndiceReferenceAnnuelle($indloyer_id, $indice_base->indval_annee + 1);
                                } else {
                                    $new_indice_reference =  $this->getIndiceReferenceTrimestrielle($indloyer_id, $indice_base->indval_annee + 1, $month_of_indexation, $indice_base->indval_trimestre);
                                }

                                $data = $this->IndexationLoyer_m->get(
                                    array(
                                        'clause' => array(
                                            'bail_id' => $data_post['bail_id'],
                                            'indlo_ref_loyer' => 1
                                        ),
                                        'method' => 'row'
                                    )
                                );

                                $data->indlo_indice_base = $indice_base->indval_id;
                                $data->indlo_indice_reference = $new_indice_reference->indval_id;

                                $array_variation = array();

                                if ($indice->mode_calcul == 1) {
                                    $variation_loyer = (($new_indice_reference->indval_valeur - $indice_base->indval_valeur) / $indice_base->indval_valeur) * 100;
                                } else {
                                    $variation_loyer = $new_indice_reference->indval_valeur;
                                }

                                array_push($array_variation, $variation_loyer);

                                if ($bail->bail_var_plafonnee != 0) {
                                    $variation_plafonnee = (floatval($bail->bail_var_plafonnee) * $variation_loyer) / 100;
                                    array_push($array_variation, $variation_plafonnee);
                                }
                                if ($bail->bail_var_capee != 0) {
                                    $variation_capee = floatval($bail->bail_var_capee);
                                    array_push($array_variation, $variation_capee);
                                }
                                $variation_appliquee = round(min($array_variation), 2);

                                if (floatval($bail->bail_var_min) != 0 && floatval($bail->bail_var_min) > floatval($variation_appliquee)) {
                                    $variation_appliquee = floatval($bail->bail_var_min);
                                }

                                $data->indlo_variation_appliquee = $variation_appliquee;
                                $data->indlo_variation_loyer = round($variation_loyer, 2);

                                $month = date('m', strtotime($last_indexation_infirst->indlo_debut));
                                $day = date('d', strtotime($last_indexation_infirst->indlo_debut));

                                unset($data->indlo_id);
                                $data->indlo_debut = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                                $data->indlo_fin = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                                $data->indlo_ref_loyer = 0;
                                $data->indlo_indince_non_publie = 1;

                                $request = $this->IndexationLoyer_m->save_data($data);
                            }
                        }
                    }
                }
            }
            echo json_encode($retour);
        }
    }

    private function getIndiceReferenceAnnuelle($indloyer_id, $annee)
    {
        $this->load->model('IndiceValeurLoyer_m');
        $params_indice = array(
            'clause' => array('indloyer_id' => $indloyer_id, 'indval_annee' => $annee, 'indval_valide' => 1),
            'method' => 'row'
        );
        $indice_reference = $this->IndiceValeurLoyer_m->get($params_indice);
        return $indice_reference;
    }

    private function getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month, $indval_trimestre)
    {
        $this->load->model('IndiceValeurLoyer_m');

        $table_trimestriel = array(
            'T1' => array(1, 2, 3),
            'T2' => array(4, 5, 6),
            'T3' => array(7, 8, 9),
            'T4' => array(10, 11, 12),
        );

        $result = array_map(function ($key, $trimester) use ($month) {
            if (in_array($month, $trimester)) {
                return $key;
            }
        }, array_keys($table_trimestriel), $table_trimestriel);

        $matchingMonth = array_filter($result);
        $matchingMonth = implode(', ', $matchingMonth);

        if ($matchingMonth > $indval_trimestre) {
            $params_indice = array(
                'clause' => array('indloyer_id' => $indloyer_id, 'indval_annee' => $annee, 'indval_trimestre' => $indval_trimestre, 'indval_valide' => 1),
                'method' => 'row'
            );
        } else {
            $params_indice = array(
                'clause' => array('indloyer_id' => $indloyer_id, 'indval_annee' => $annee - 1, 'indval_trimestre' => $indval_trimestre, 'indval_valide' => 1),
                'method' => 'row'
            );
        }
        $indice_reference = $this->IndiceValeurLoyer_m->get($params_indice);

        return $indice_reference;
    }

    private function getLastIndexation($bail_id)
    {
        $this->load->model('IndexationLoyer_m');
        $params = array(
            'clause' => array('bail_id' => $bail_id, 'etat_indexation' => 1),
            'order_by_columns' => 'indlo_id DESC',
            'method' => 'row'
        );
        $last = $this->IndexationLoyer_m->get($params);

        return $last;
    }

    private function getIndiceBase($bail_id)
    {
        $this->load->model('IndiceValeurLoyer_m');
        $this->load->model('IndexationLoyer_m');
        $params = array(
            'clause' => array('bail_id' => $bail_id, 'etat_indexation' => 1, 'indlo_indince_non_publie' => 0),
            'order_by_columns' => 'indlo_id ASC',
        );
        $indexation_loyer = $this->IndexationLoyer_m->get($params);
        $indice_base = $indexation_loyer[0]->indlo_indice_base;
        if (count($indexation_loyer) > 1) {
            $indice_base = $indexation_loyer[count($indexation_loyer) - 1]->indlo_indice_reference;
        }

        $params_indice = array(
            'clause' => array('indval_id' => $indice_base, 'indval_valide' => 1),
            'method' => 'row'
        );
        $indice_base = $this->IndiceValeurLoyer_m->get($params_indice);

        return $indice_base;
    }

    public function indexationExist()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $retour = $_POST['data'];
                echo json_encode($retour);
            }
        }
    }

    public function DeleteIndexation()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $this->load->model('IndexationLoyer_m');
                $data_post = $_POST['data'];
                $retour['data_post'] = $data_post;
                $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);
                $this->IndexationLoyer_m->delete_data($clause_delete);
                echo json_encode($retour);
            }
        }
    }

    // public function gererAvoirAndFactures()
    // {
    //     if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //         if (is_ajax()) {
    //             $this->load->model('IndexationLoyer_m');
    //             $this->load->model('FactureLoyer_m');
    //             $this->load->model('DocumentLoyer_m');
    //             $this->load->model('Bail_m');
    //             $data_post = $_POST['data'];

    //             $param_bail = array(
    //                 'clause' => array('bail_id' => $data_post['bail_id']),
    //                 'method' => 'row'
    //             );

    //             $bail = $this->Bail_m->get($param_bail);

    //             $params_get_allbail = array(
    //                 'clause' => array('cliLot_id' => $bail->cliLot_id, 'bail_cloture' => 0),
    //             );
    //             $all_bail_parents = $this->Bail_m->get($params_get_allbail);
    //             $count_baux = count($all_bail_parents);
    //             $bail_origin_avenant = $all_bail_parents[$count_baux - 2];

    //             $pdl_id = $bail_origin_avenant->pdl_id;
    //             $moment_factu = $bail_origin_avenant->momfac_id;

    //             $clause = array(
    //                 'clause' => array('bail_id' => $bail_origin_avenant->bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
    //             );

    //             $liste_indexation = $this->IndexationLoyer_m->get($clause);

    //             if (!empty($liste_indexation)) {
    //                 foreach ($liste_indexation as $key => $value) {
    //                     $start_date = $value->indlo_debut;
    //                     $end_date = $value->indlo_fin;

    //                     $start_timestamp = strtotime($start_date);
    //                     $end_timestamp = strtotime($end_date);
    //                     $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                     $indlo_id = $value->indlo_id;

    //                     if ($date_to_check_timestamp <  $start_timestamp) {
    //                         $clause_delete = array('bail_id' => $bail_origin_avenant->bail_id, 'indlo_id' => $indlo_id);

    //                         $this->IndexationLoyer_m->delete_data($clause_delete);
    //                     } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {
    //                         $clause = array("indlo_id" => $indlo_id);
    //                         $data = array(
    //                             'indlo_fin' => $data_post['date_prise_en_charge']
    //                         );

    //                         $request = $this->IndexationLoyer_m->save_data($data, $clause);
    //                     }
    //                 }

    //                 $params_factures = array(
    //                     'clause' => array('bail_id' => $bail_origin_avenant->bail_id),
    //                 );

    //                 $liste_factures = $this->FactureLoyer_m->get($params_factures);

    //                 if (!empty($liste_factures)) {
    //                     foreach ($liste_factures as $key => $value) {
    //                         switch ($pdl_id) {
    //                             case 2:
    //                                 if ($moment_factu == 2) {
    //                                     $start_date = $value->fact_loyer_date;
    //                                     $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
    //                                 } else {
    //                                     $date = $value->fact_loyer_date;
    //                                     $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
    //                                     $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
    //                                 }

    //                                 $start_timestamp = strtotime($start_date);
    //                                 $end_timestamp = strtotime($end_date);
    //                                 $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                                 $fact_loyer_id = $value->fact_loyer_id;
    //                                 $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

    //                                 if ($date_to_check_timestamp <  $start_timestamp) {
    //                                     $data_fac = array(
    //                                         'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail_origin_avenant->bail_id), 5, '0', STR_PAD_LEFT),
    //                                         'fact_loyer_date' => $value->fact_loyer_date,
    //                                         'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
    //                                         'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
    //                                         'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
    //                                         'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
    //                                         'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
    //                                         'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
    //                                         'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
    //                                         'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
    //                                         'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
    //                                         "bail_id" => $value->bail_id,
    //                                         "periode_semestre_id" => $value->periode_semestre_id,
    //                                         "periode_timestre_id" => $value->periode_timestre_id,
    //                                         "periode_annee_id" => $value->periode_annee_id,
    //                                         "periode_mens_id" => $value->periode_mens_id,
    //                                         "facture_nombre" => $value->facture_nombre,
    //                                         "facture_annee" => $value->facture_annee,
    //                                         "dossier_id" => $value->dossier_id,
    //                                         "facture_commentaire" => $value->facture_commentaire,
    //                                         "mode_paiement" => $value->mode_paiement,
    //                                         "echeance_paiement" => $value->echeance_paiement,
    //                                         "emetteur_nom" => $value->emetteur_nom,
    //                                         "emet_adress1" => $value->emet_adress1,
    //                                         "emet_adress2" => $value->emet_adress2,
    //                                         "emet_adress3" => $value->emet_adress3,
    //                                         "emet_cp" => $value->emet_cp,
    //                                         "emet_pays" => $value->emet_pays,
    //                                         "emet_ville" => $value->emet_ville,
    //                                         "dest_nom" => $value->dest_nom,
    //                                         "dest_adresse1" => $value->dest_adresse1,
    //                                         "dest_adresse2" => $value->dest_adresse2,
    //                                         "dest_adresse3" => $value->dest_adresse3,
    //                                         "dest_cp" => $value->dest_cp,
    //                                         "dest_pays" => $value->dest_pays,
    //                                         "dest_ville" => $value->dest_ville,
    //                                         "fact_email_sent" => $value->fact_email_sent,
    //                                     );
    //                                     $request = $this->FactureLoyer_m->save_data($data_fac);

    //                                     $request_id = array(
    //                                         'clause' => array('c_facture_loyer.bail_id' => $bail_origin_avenant->bail_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'order_by_columns' => 'fact_loyer_id DESC',
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture_avoir($facture_loyer);
    //                                 } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

    //                                     $date1 = $start_date;
    //                                     $date2 = $data_post['date_prise_en_charge'];

    //                                     $diff_in_seconds = strtotime($date2) - strtotime($date1);
    //                                     $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

    //                                     $date11 = $start_date;
    //                                     $date22 = $end_date;

    //                                     $diff_in_seconds = strtotime($date22) - strtotime($date11);
    //                                     $days_3month = ($diff_in_seconds / (60 * 60 * 24));

    //  if ($diff_in_days - 1 ==  $days_3month) {
    //                                       $diff_in_days =  $days_3month;
    //                                  }

    //                                     $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_appartement_tva = $value->fact_loyer_app_tva;
    //                                     $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
    //                                     $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

    //                                     $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_parking_tva = $value->fact_loyer_park_tva;
    //                                     $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
    //                                     $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

    //                                     $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_charge_tva = $value->fact_loyer_charge_tva;
    //                                     $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
    //                                     $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

    //                                     $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
    //                                     $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
    //                                     $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
    //                                     $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
    //                                     $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
    //                                     $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;
    //                                     $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
    //                                     $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
    //                                     $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

    //                                     $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

    //                                     $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
    //                                     $this->DocumentLoyer_m->delete_data($clause_delete);

    //                                     $request_id = array(
    //                                         'clause' => array('fact_loyer_id' => $fact_loyer_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture($facture_loyer);
    //                                 }

    //                                 break;
    //                             case 3:
    //                                 if ($moment_factu == 2) {
    //                                     $start_date = $value->fact_loyer_date;
    //                                     $end_date = date('Y-m-t', strtotime('+5 month', strtotime($start_date)));
    //                                 } else {
    //                                     $date = $value->fact_loyer_date;
    //                                     $start_date = date('Y-m-d', strtotime('-6 month', strtotime($date)));
    //                                     $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
    //                                 }

    //                                 $start_timestamp = strtotime($start_date);
    //                                 $end_timestamp = strtotime($end_date);
    //                                 $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                                 $fact_loyer_id = $value->fact_loyer_id;
    //                                 $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

    //                                 if ($date_to_check_timestamp <  $start_timestamp) {

    //                                     $data_fac = array(
    //                                         'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail_origin_avenant->bail_id), 5, '0', STR_PAD_LEFT),
    //                                         'fact_loyer_date' => $value->fact_loyer_date,
    //                                         'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
    //                                         'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
    //                                         'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
    //                                         'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
    //                                         'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
    //                                         'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
    //                                         'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
    //                                         'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
    //                                         'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
    //                                         "bail_id" => $value->bail_id,
    //                                         "periode_semestre_id" => $value->periode_semestre_id,
    //                                         "periode_timestre_id" => $value->periode_timestre_id,
    //                                         "periode_annee_id" => $value->periode_annee_id,
    //                                         "periode_mens_id" => $value->periode_mens_id,
    //                                         "facture_nombre" => $value->facture_nombre,
    //                                         "facture_annee" => $value->facture_annee,
    //                                         "dossier_id" => $value->dossier_id,
    //                                         "facture_commentaire" => $value->facture_commentaire,
    //                                         "mode_paiement" => $value->mode_paiement,
    //                                         "echeance_paiement" => $value->echeance_paiement,
    //                                         "emetteur_nom" => $value->emetteur_nom,
    //                                         "emet_adress1" => $value->emet_adress1,
    //                                         "emet_adress2" => $value->emet_adress2,
    //                                         "emet_adress3" => $value->emet_adress3,
    //                                         "emet_cp" => $value->emet_cp,
    //                                         "emet_pays" => $value->emet_pays,
    //                                         "emet_ville" => $value->emet_ville,
    //                                         "dest_nom" => $value->dest_nom,
    //                                         "dest_adresse1" => $value->dest_adresse1,
    //                                         "dest_adresse2" => $value->dest_adresse2,
    //                                         "dest_adresse3" => $value->dest_adresse3,
    //                                         "dest_cp" => $value->dest_cp,
    //                                         "dest_pays" => $value->dest_pays,
    //                                         "dest_ville" => $value->dest_ville,
    //                                         "fact_email_sent" => $value->fact_email_sent,
    //                                     );
    //                                     $request = $this->FactureLoyer_m->save_data($data_fac);
    //                                     $request_id = array(
    //                                         'clause' => array('c_facture_loyer.bail_id' => $bail_origin_avenant->bail_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'order_by_columns' => 'fact_loyer_id DESC',
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture_avoir($facture_loyer);
    //                                 } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

    //                                     $date1 = $start_date;
    //                                     $date2 = $data_post['date_prise_en_charge'];

    //                                     $diff_in_seconds = strtotime($date2) - strtotime($date1);
    //                                     $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

    //                                     $date11 = $start_date;
    //                                     $date22 = $end_date;

    //                                     $diff_in_seconds = strtotime($date22) - strtotime($date11);
    //                                     $days_3month = ($diff_in_seconds / (60 * 60 * 24));

    //    if ($diff_in_days - 1 ==  $days_3month) {
    //                                         $diff_in_days =  $days_3month;
    //                                     }

    //                                     $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_appartement_tva = $value->fact_loyer_app_tva;
    //                                     $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
    //                                     $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

    //                                     $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_parking_tva = $value->fact_loyer_park_tva;
    //                                     $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
    //                                     $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

    //                                     $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_charge_tva = $value->fact_loyer_charge_tva;
    //                                     $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
    //                                     $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

    //                                     $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
    //                                     $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
    //                                     $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
    //                                     $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
    //                                     $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
    //                                     $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;
    //                                     $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
    //                                     $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
    //                                     $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

    //                                     $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

    //                                     $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
    //                                     $this->DocumentLoyer_m->delete_data($clause_delete);

    //                                     $request_id = array(
    //                                         'clause' => array('fact_loyer_id' => $fact_loyer_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture($facture_loyer);
    //                                 }
    //                                 break;
    //                             case 4:
    //                                 if ($moment_factu == 2) {
    //                                     $start_date = $value->fact_loyer_date;
    //                                     $end_date = date('Y-m-t', strtotime('+11 month', strtotime($start_date)));
    //                                 } else {
    //                                     $date = $value->fact_loyer_date;
    //                                     $start_date = date('Y-m-d', strtotime('-12 month', strtotime($date)));
    //                                     $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
    //                                 }

    //                                 $start_timestamp = strtotime($start_date);
    //                                 $end_timestamp = strtotime($end_date);
    //                                 $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                                 $fact_loyer_id = $value->fact_loyer_id;
    //                                 $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

    //                                 if ($date_to_check_timestamp <  $start_timestamp) {

    //                                     $data_fac = array(
    //                                         'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail_origin_avenant->bail_id), 5, '0', STR_PAD_LEFT),
    //                                         'fact_loyer_date' => $value->fact_loyer_date,
    //                                         'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
    //                                         'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
    //                                         'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
    //                                         'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
    //                                         'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
    //                                         'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
    //                                         'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
    //                                         'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
    //                                         'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
    //                                         "bail_id" => $value->bail_id,
    //                                         "periode_semestre_id" => $value->periode_semestre_id,
    //                                         "periode_timestre_id" => $value->periode_timestre_id,
    //                                         "periode_annee_id" => $value->periode_annee_id,
    //                                         "periode_mens_id" => $value->periode_mens_id,
    //                                         "facture_nombre" => $value->facture_nombre,
    //                                         "facture_annee" => $value->facture_annee,
    //                                         "dossier_id" => $value->dossier_id,
    //                                         "facture_commentaire" => $value->facture_commentaire,
    //                                         "mode_paiement" => $value->mode_paiement,
    //                                         "echeance_paiement" => $value->echeance_paiement,
    //                                         "emetteur_nom" => $value->emetteur_nom,
    //                                         "emet_adress1" => $value->emet_adress1,
    //                                         "emet_adress2" => $value->emet_adress2,
    //                                         "emet_adress3" => $value->emet_adress3,
    //                                         "emet_cp" => $value->emet_cp,
    //                                         "emet_pays" => $value->emet_pays,
    //                                         "emet_ville" => $value->emet_ville,
    //                                         "dest_nom" => $value->dest_nom,
    //                                         "dest_adresse1" => $value->dest_adresse1,
    //                                         "dest_adresse2" => $value->dest_adresse2,
    //                                         "dest_adresse3" => $value->dest_adresse3,
    //                                         "dest_cp" => $value->dest_cp,
    //                                         "dest_pays" => $value->dest_pays,
    //                                         "dest_ville" => $value->dest_ville,
    //                                         "fact_email_sent" => $value->fact_email_sent,
    //                                     );
    //                                     $request = $this->FactureLoyer_m->save_data($data_fac);
    //                                     $request_id = array(
    //                                         'clause' => array('c_facture_loyer.bail_id' => $bail_origin_avenant->bail_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'order_by_columns' => 'fact_loyer_id DESC',
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture_avoir($facture_loyer);
    //                                 } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

    //                                     $date1 = $start_date;
    //                                     $date2 = $data_post['date_prise_en_charge'];

    //                                     $diff_in_seconds = strtotime($date2) - strtotime($date1);
    //                                     $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

    //                                     $date11 = $start_date;
    //                                     $date22 = $end_date;

    //                                     $diff_in_seconds = strtotime($date22) - strtotime($date11);
    //                                     $days_3month = ($diff_in_seconds / (60 * 60 * 24));

    //    if ($diff_in_days - 1 ==  $days_3month) {
    //                                         $diff_in_days =  $days_3month;
    //                                     }

    //                                     $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_appartement_tva = $value->fact_loyer_app_tva;
    //                                     $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
    //                                     $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

    //                                     $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_parking_tva = $value->fact_loyer_park_tva;
    //                                     $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
    //                                     $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

    //                                     $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_charge_tva = $value->fact_loyer_charge_tva;
    //                                     $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
    //                                     $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

    //                                     $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
    //                                     $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
    //                                     $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
    //                                     $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
    //                                     $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
    //                                     $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;
    //                                     $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
    //                                     $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
    //                                     $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

    //                                     $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

    //                                     $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
    //                                     $this->DocumentLoyer_m->delete_data($clause_delete);

    //                                     $request_id = array(
    //                                         'clause' => array('fact_loyer_id' => $fact_loyer_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture($facture_loyer);
    //                                 }
    //                                 break;
    //                             case 5:
    //                                 if ($moment_factu == 2) {
    //                                     $start_date = $value->fact_loyer_date;
    //                                     $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
    //                                 } else {
    //                                     $date = $value->fact_loyer_date;
    //                                     $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
    //                                     $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
    //                                 }

    //                                 $start_timestamp = strtotime($start_date);
    //                                 $end_timestamp = strtotime($end_date);
    //                                 $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                                 $fact_loyer_id = $value->fact_loyer_id;
    //                                 $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

    //                                 if ($date_to_check_timestamp <  $start_timestamp) {

    //                                     $data_fac = array(
    //                                         'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail_origin_avenant->bail_id), 5, '0', STR_PAD_LEFT),
    //                                         'fact_loyer_date' => $value->fact_loyer_date,
    //                                         'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
    //                                         'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
    //                                         'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
    //                                         'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
    //                                         'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
    //                                         'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
    //                                         'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
    //                                         'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
    //                                         'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
    //                                         "bail_id" => $value->bail_id,
    //                                         "periode_semestre_id" => $value->periode_semestre_id,
    //                                         "periode_timestre_id" => $value->periode_timestre_id,
    //                                         "periode_annee_id" => $value->periode_annee_id,
    //                                         "periode_mens_id" => $value->periode_mens_id,
    //                                         "facture_nombre" => $value->facture_nombre,
    //                                         "facture_annee" => $value->facture_annee,
    //                                         "dossier_id" => $value->dossier_id,
    //                                         "facture_commentaire" => $value->facture_commentaire,
    //                                         "mode_paiement" => $value->mode_paiement,
    //                                         "echeance_paiement" => $value->echeance_paiement,
    //                                         "emetteur_nom" => $value->emetteur_nom,
    //                                         "emet_adress1" => $value->emet_adress1,
    //                                         "emet_adress2" => $value->emet_adress2,
    //                                         "emet_adress3" => $value->emet_adress3,
    //                                         "emet_cp" => $value->emet_cp,
    //                                         "emet_pays" => $value->emet_pays,
    //                                         "emet_ville" => $value->emet_ville,
    //                                         "dest_nom" => $value->dest_nom,
    //                                         "dest_adresse1" => $value->dest_adresse1,
    //                                         "dest_adresse2" => $value->dest_adresse2,
    //                                         "dest_adresse3" => $value->dest_adresse3,
    //                                         "dest_cp" => $value->dest_cp,
    //                                         "dest_pays" => $value->dest_pays,
    //                                         "dest_ville" => $value->dest_ville,
    //                                         "fact_email_sent" => $value->fact_email_sent,
    //                                     );
    //                                     $request = $this->FactureLoyer_m->save_data($data_fac);
    //                                     $request_id = array(
    //                                         'clause' => array('c_facture_loyer.bail_id' => $bail_origin_avenant->bail_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'order_by_columns' => 'fact_loyer_id DESC',
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture_avoir($facture_loyer);
    //                                 } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {

    //                                     $date1 = $start_date;
    //                                     $date2 = $data_post['date_prise_en_charge'];

    //                                     $diff_in_seconds = strtotime($date2) - strtotime($date1);
    //                                     $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

    //                                     $date11 = $start_date;
    //                                     $date22 = $end_date;

    //                                     $diff_in_seconds = strtotime($date22) - strtotime($date11);
    //                                     $days_3month = ($diff_in_seconds / (60 * 60 * 24));

    //    if ($diff_in_days - 1 ==  $days_3month) {
    //                                         $diff_in_days =  $days_3month;
    //                                     }

    //                                     $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_appartement_tva = $value->fact_loyer_app_tva;
    //                                     $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
    //                                     $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

    //                                     $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_parking_tva = $value->fact_loyer_park_tva;
    //                                     $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
    //                                     $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

    //                                     $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
    //                                     $indlo_charge_tva = $value->fact_loyer_charge_tva;
    //                                     $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
    //                                     $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

    //                                     $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
    //                                     $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
    //                                     $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
    //                                     $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
    //                                     $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
    //                                     $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;
    //                                     $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
    //                                     $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
    //                                     $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

    //                                     $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

    //                                     $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
    //                                     $this->DocumentLoyer_m->delete_data($clause_delete);

    //                                     $request_id = array(
    //                                         'clause' => array('fact_loyer_id' => $fact_loyer_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture($facture_loyer);
    //                                 }
    //                                 break;

    //                             default:
    //                                 if ($moment_factu == 2) {
    //                                     $start_date = $value->fact_loyer_date;
    //                                     $end_date = date('Y-m-t', strtotime($start_date));
    //                                 } else {
    //                                     $date = $value->fact_loyer_date;
    //                                     $start_date = date('Y-m-d', strtotime('-1 month', strtotime($date)));
    //                                     $end_date = date('Y-m-t', strtotime($start_date));
    //                                 }

    //                                 $start_timestamp = strtotime($start_date);
    //                                 $end_timestamp = strtotime($end_date);
    //                                 $date_to_check_timestamp = strtotime($data_post['date_prise_en_charge']);
    //                                 $fact_loyer_id = $value->fact_loyer_id;
    //                                 $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

    //                                 if ($date_to_check_timestamp <  $start_timestamp) {

    //                                     $data_fac = array(
    //                                         'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail_origin_avenant->bail_id), 5, '0', STR_PAD_LEFT),
    //                                         'fact_loyer_date' => $value->fact_loyer_date,
    //                                         'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
    //                                         'fact_loyer_app_tva' =>  $value->fact_loyer_app_tva,
    //                                         'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
    //                                         'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
    //                                         'fact_loyer_park_tva' =>  $value->fact_loyer_park_tva,
    //                                         'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
    //                                         'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
    //                                         'fact_loyer_charge_tva' =>  $value->fact_loyer_charge_tva,
    //                                         'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
    //                                         "bail_id" => $value->bail_id,
    //                                         "periode_semestre_id" => $value->periode_semestre_id,
    //                                         "periode_timestre_id" => $value->periode_timestre_id,
    //                                         "periode_annee_id" => $value->periode_annee_id,
    //                                         "periode_mens_id" => $value->periode_mens_id,
    //                                         "facture_nombre" => $value->facture_nombre,
    //                                         "facture_annee" => $value->facture_annee,
    //                                         "dossier_id" => $value->dossier_id,
    //                                         "facture_commentaire" => $value->facture_commentaire,
    //                                         "mode_paiement" => $value->mode_paiement,
    //                                         "echeance_paiement" => $value->echeance_paiement,
    //                                         "emetteur_nom" => $value->emetteur_nom,
    //                                         "emet_adress1" => $value->emet_adress1,
    //                                         "emet_adress2" => $value->emet_adress2,
    //                                         "emet_adress3" => $value->emet_adress3,
    //                                         "emet_cp" => $value->emet_cp,
    //                                         "emet_pays" => $value->emet_pays,
    //                                         "emet_ville" => $value->emet_ville,
    //                                         "dest_nom" => $value->dest_nom,
    //                                         "dest_adresse1" => $value->dest_adresse1,
    //                                         "dest_adresse2" => $value->dest_adresse2,
    //                                         "dest_adresse3" => $value->dest_adresse3,
    //                                         "dest_cp" => $value->dest_cp,
    //                                         "dest_pays" => $value->dest_pays,
    //                                         "dest_ville" => $value->dest_ville,
    //                                         "fact_email_sent" => $value->fact_email_sent,
    //                                     );
    //                                     $request = $this->FactureLoyer_m->save_data($data_fac);

    //                                     $request_id = array(
    //                                         'clause' => array('c_facture_loyer.bail_id' => $bail_origin_avenant->bail_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'order_by_columns' => 'fact_loyer_id DESC',
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture_avoir($facture_loyer);
    //                                 } elseif ($date_to_check_timestamp >= $start_timestamp && $date_to_check_timestamp <= $end_timestamp) {
    //                                     $date1 = new DateTime($start_date);
    //                                     $date2 = new DateTime($data_post['date_prise_en_charge']);

    //                                     $interval = date_diff($date1, $date2);
    //                                     $diff = (intval($interval->format(' %d '))) + 1;

    //                                     $end_month = (date("d", strtotime($end_date))) + 1;

    //                                     $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff) / $end_month), 2);
    //                                     $indlo_appartement_tva = $value->fact_loyer_app_tva;
    //                                     $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
    //                                     $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

    //                                     $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff) / $end_month), 2);
    //                                     $indlo_parking_tva = $value->fact_loyer_park_tva;
    //                                     $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
    //                                     $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

    //                                     $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff) / $end_month), 2);
    //                                     $indlo_charge_tva = $value->fact_loyer_charge_tva;
    //                                     $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
    //                                     $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

    //                                     $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
    //                                     $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
    //                                     $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
    //                                     $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
    //                                     $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
    //                                     $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;
    //                                     $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
    //                                     $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
    //                                     $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

    //                                     $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

    //                                     $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
    //                                     $this->DocumentLoyer_m->delete_data($clause_delete);

    //                                     $request_id = array(
    //                                         'clause' => array('fact_loyer_id' => $fact_loyer_id),
    //                                         'join' => array(
    //                                             'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
    //                                         ),
    //                                         'limit' => 1,
    //                                         'method' => 'row'
    //                                     );
    //                                     $facture_loyer = $this->FactureLoyer_m->get($request_id);
    //                                     $this->generate_facture($facture_loyer);
    //                                 }

    //                                 break;
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    private function generate_facture_avoir($get_facture)
    {
        $this->load->helper("exportation");
        $this->load->model('FactureLoyer_m', 'Facture');
        $this->load->model('Rib_m');
        if (!is_null($get_facture->periode_mens_id)) {
            $_data['facture'] = $facture = $this->get_mensuel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = 'Avoir_' . $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_mens_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/avoir/export_facture_mensuel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } elseif (!is_null($get_facture->periode_timestre_id)) {
            $_data['facture'] = $facture = $this->get_trimestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $donnee = explode("(", $facture->echeance_paiement);
            $donnee_final = explode(")", $donnee[1]);
            $date = explode("=>", $facture->echeance_paiement);
            $moment_factu = $donnee_final[0];
            if ($moment_factu == "échu") {
                $date_str = trim($date[1]);
                $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
                $new_date_str = $date_obj->format('Y-m-d');
                $debut_trim = date('m-Y', strtotime('-3 months', strtotime($new_date_str)));
                $fin_trim = date('m-Y', strtotime('-1 months', strtotime($new_date_str)));
                $_data['debut_trim'] = $debut_trim;
                $_data['fin_trim'] = $fin_trim;
            } else {
                $date_str = trim($date[1]);
                $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
                $new_date_str = $date_obj->format('Y-m-d');
                $debut_trim = date('m-Y', strtotime('+0 months', strtotime($new_date_str)));
                $fin_trim = date('m-Y', strtotime('+2 months', strtotime($new_date_str)));
                $_data['debut_trim'] = $debut_trim;
                $_data['fin_trim'] = $fin_trim;
            }
            $file_name = 'Avoir_' . $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_trimesrte_libelle . '_' . $facture->emetteur_nom;
            $view = $get_facture->pdl_id == 2 ? "Bail/avoir/export_facture_trimestre" : "Bail/avoir/export_facture_trimestre_decale";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } elseif (!is_null($get_facture->periode_semestre_id)) {
            $_data['facture'] = $facture = $this->get_semestriel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = 'Avoir_' . $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_semestre_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/avoir/export_facture_semestriel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        } else {
            $_data['facture'] = $facture = $this->get_annuel($get_facture->bail_id, $get_facture->fact_loyer_id);
            $file_name = 'Avoir_' . $facture->fact_loyer_date . '_' . $facture->fact_loyer_num . '_' . $facture->periode_annee_libelle . '_' . $facture->emetteur_nom;
            $view = "Bail/avoir/export_facture_annuel";
            $cliLot_id = $facture->cliLot_id;
            $annee = $facture->facture_annee;
        }

        $param_rib = array(
            'clause' => array(),
            'order_by_columns' => "rib_id  DESC",
            'method' => "row"
        );
        $_data['rib'] = $this->Rib_m->get($param_rib);
        exporation_pdf($view, $file_name, $_data, $cliLot_id, $annee, $get_facture->fact_loyer_id);
    }

    public function alert_Siret_TVa()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $data_post = $_POST['data'];
                $data['titre'] = $data_post['titre'];
                $data['contenu'] = $data_post['contenu'];
                $this->renderComponant("Clients/clients/lot/bails/formAlert_Siret_tva", $data);
            }
        }
    }

    public function modal_protocole()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $data['titre_modal'] = 'Confirmation de l\'enregistrement du protocole';
                $this->renderComponant("Clients/clients/lot/bails/modalvalidation_protocole", $data);
            }
        }
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function saveProtocole()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $this->load->model('Protocol_m');
                $this->load->model('DocumentBail_m');
                $data_post = json_decode($_POST['data']);

                if (!empty($_FILES['file'])) {
                    $target_dir = "../documents/clients/lots/bail/$data_post->cliLot_id/";
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;
                    $this->makeDirPath($target_dir);

                    $counter = 1;
                    while (file_exists($path_filename_ext)) {
                        $filename = $filename . '_' . $counter;
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $counter++;
                    }
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }

                    $data_file = array(
                        'doc_bail_nom' => $_FILES['file']['name'],
                        'doc_bail_creation' => date('Y-m-d H:i:s'),
                        'doc_bail_path' => str_replace('\\', '/', $target_dir . '' . $_FILES['file']['name']),
                        'doc_bail_etat' => 1,
                        'util_id' => $_SESSION['session_utilisateur']['util_id'],
                        'cliLot_id' => $data_post->cliLot_id,
                        'doc_protocole' => 1
                    );

                    $data['doc_path'] = str_replace('\\', '/', $target_dir . '' . $_FILES['file']['name']);
                    $data['doc_nom'] = $_FILES['file']['name'];
                }

                $data = array(
                    'bail_id' => $data_post->bail_id,
                    'date_sign_protocole' => $data_post->date_sign_protocole,
                    'type_abandon' => $data_post->type_abandon,
                    'pourcentage_protocol' => $data_post->pourcentage_protocol,
                    'info_complementaire' => $data_post->info_complementaire,
                    'date_debut_protocole' => $data_post->date_debut_protocole,
                    'date_fin_protocole' => $data_post->date_fin_protocole == "" ? NULL : $data_post->date_fin_protocole,
                );

                if ($data_post->id_protocol != '') {
                    $clause = array('id_protocol ' => $data_post->id_protocol);
                    $request = $this->Protocol_m->save_data($data, $clause);
                } else {
                    $request = $this->Protocol_m->save_data($data);
                }

                if (!empty($_FILES['file'])) {

                    $clause_delete = array('id_protocol' => $data_post->id_protocol);
                    $this->DocumentBail_m->delete_data($clause_delete);

                    $param = array(
                        'order_by_columns' => 'id_protocol DESC',
                        'method' => 'row'
                    );
                    $last_protocol = $this->Protocol_m->get($param);

                    $data_file['id_protocol'] = $last_protocol->id_protocol;

                    $this->DocumentBail_m->save_data($data_file);
                }

                echo json_encode($data_post);
            }
        }
    }

    private function gererAvoirAndFactures_AbandonPartiel($bail_id, $date_debut, $datefin)
    {
        $this->load->model('IndexationLoyer_m');
        $this->load->model('FactureLoyer_m');
        $this->load->model('DocumentLoyer_m');
        $this->load->model('Bail_m');
        $this->load->model('Protocol_m', 'protocol');

        $param_bail = array(
            'clause' => array('c_bail.bail_id' => $bail_id),
            'join' => array(
                'c_protocol' => 'c_bail.bail_id  = c_protocol.bail_id'
            ),
            'method' => 'row'
        );

        $bail = $this->Bail_m->get($param_bail);

        $pdl_id = $bail->pdl_id;
        $moment_factu = $bail->momfac_id;
        $pourcentage_protocol = $bail->pourcentage_protocol;
        $date_sign_protocole = $bail->date_sign_protocole;

        $clause = array(
            'clause' => array('bail_id' => $bail->bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );

        $liste_indexation = $this->IndexationLoyer_m->get($clause);

        if (!empty($liste_indexation)) {

            $params_factures = array(
                'clause' => array('bail_id' => $bail->bail_id),
            );

            $liste_factures = $this->FactureLoyer_m->get($params_factures);

            if (!empty($liste_factures)) {
                foreach ($liste_factures as $key => $value) {
                    switch ($pdl_id) {
                        case 2:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);

                            $date_to_check_timestamp_debut = strtotime($date_debut);
                            $date_to_check_timestamp_fin = strtotime($datefin);

                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  -$value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  -$value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  -$value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $date1 = $start_date;
                                $date2 = $date_debut;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {

                                $date1 = $datefin;
                                $date2 = $end_date;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;
                                }

                                $data_fac_proto['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac_proto['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac_proto['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac_proto['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac_proto['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac_proto['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                $data_fac_proto['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac_proto['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac_proto['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                $data_fac_proto['periode_timestre_id'] = $value->periode_timestre_id;

                                $data_fac_proto['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                $request = $this->FactureLoyer_m->save_data($data_fac_proto, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin < $start_timestamp && $date_to_check_timestamp_fin < $end_timestamp) {

                                $indlo_appartement_ht = round(($value->fact_loyer_app_ht), 2);
                                $indlo_appartement_tva = round(($value->fact_loyer_app_tva), 2);
                                $indlo_appartement_ttc = round(($value->fact_loyer_app_ttc), 2);
                                $indlo_parking_ht = round(($value->fact_loyer_park_ht), 2);
                                $indlo_parking_tva = round(($value->fact_loyer_park_tva), 2);
                                $indlo_parking_ttc = round(($value->fact_loyer_park_ttc), 2);
                                $indlo_charge_ht = round(($value->fact_loyer_charge_ht), 2);
                                $indlo_charge_tva = round(($value->fact_loyer_charge_tva), 2);
                                $indlo_charge_ttc = round(($value->fact_loyer_charge_ttc), 2);

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                    $data_fac_proto_pourcent['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data_fac_proto_pourcent['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data_fac_proto_pourcent['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data_fac_proto_pourcent['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data_fac_proto_pourcent['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data_fac_proto_pourcent['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                    $data_fac_proto_pourcent['periode_timestre_id'] = $value->periode_timestre_id;
                                    $data_fac_proto_pourcent['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                    $request = $this->FactureLoyer_m->save_data($data_fac_proto_pourcent, $clause_facture);

                                    $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                    $this->DocumentLoyer_m->delete_data($clause_delete);

                                    $request_id = array(
                                        'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                        'join' => array(
                                            'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                        ),
                                        'limit' => 1,
                                        'method' => 'row'
                                    );
                                    $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                    $this->generate_facture($facture_loyer);
                                }
                            }
                            break;
                        case 3:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+5 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-6 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);

                            $date_to_check_timestamp_debut = strtotime($date_debut);
                            $date_to_check_timestamp_fin = strtotime($datefin);

                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp_debut <  $start_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  -$value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  -$value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  -$value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $date1 = $start_date;
                                $date2 = $date_debut;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {

                                $date1 = $datefin;
                                $date2 = $end_date;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;
                                }

                                $data_fac_proto['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac_proto['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac_proto['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac_proto['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac_proto['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac_proto['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                $data_fac_proto['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac_proto['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac_proto['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                $data_fac_proto['periode_semestre_id'] = $value->periode_semestre_id;
                                $data_fac_proto['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                $request = $this->FactureLoyer_m->save_data($data_fac_proto, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin < $start_timestamp && $date_to_check_timestamp_fin < $end_timestamp) {

                                $indlo_appartement_ht = round(($value->fact_loyer_app_ht), 2);
                                $indlo_appartement_tva = round(($value->fact_loyer_app_tva), 2);
                                $indlo_appartement_ttc = round(($value->fact_loyer_app_ttc), 2);
                                $indlo_parking_ht = round(($value->fact_loyer_park_ht), 2);
                                $indlo_parking_tva = round(($value->fact_loyer_park_tva), 2);
                                $indlo_parking_ttc = round(($value->fact_loyer_park_ttc), 2);
                                $indlo_charge_ht = round(($value->fact_loyer_charge_ht), 2);
                                $indlo_charge_tva = round(($value->fact_loyer_charge_tva), 2);
                                $indlo_charge_ttc = round(($value->fact_loyer_charge_ttc), 2);

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                    $data_fac_proto_pourcent['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data_fac_proto_pourcent['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data_fac_proto_pourcent['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data_fac_proto_pourcent['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data_fac_proto_pourcent['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data_fac_proto_pourcent['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                    $data_fac_proto_pourcent['periode_semestre_id'] = $value->periode_semestre_id;
                                    $data_fac_proto_pourcent['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                    $request = $this->FactureLoyer_m->save_data($data_fac_proto_pourcent, $clause_facture);

                                    $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                    $this->DocumentLoyer_m->delete_data($clause_delete);

                                    $request_id = array(
                                        'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                        'join' => array(
                                            'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                        ),
                                        'limit' => 1,
                                        'method' => 'row'
                                    );
                                    $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                    $this->generate_facture($facture_loyer);
                                }
                            }
                            break;
                        case 4:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+11 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-12 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);

                            $date_to_check_timestamp_debut = strtotime($date_debut);
                            $date_to_check_timestamp_fin = strtotime($datefin);

                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp_debut <  $start_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  -$value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  -$value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  -$value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $date1 = $start_date;
                                $date2 = $date_debut;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {

                                $date1 = $datefin;
                                $date2 = $end_date;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;
                                }

                                $data_fac_proto['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac_proto['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac_proto['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac_proto['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac_proto['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac_proto['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                $data_fac_proto['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac_proto['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac_proto['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                $data_fac_proto['periode_annee_id'] = $value->periode_annee_id;
                                $data_fac_proto['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                $request = $this->FactureLoyer_m->save_data($data_fac_proto, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin < $start_timestamp && $date_to_check_timestamp_fin < $end_timestamp) {

                                $indlo_appartement_ht = round(($value->fact_loyer_app_ht), 2);
                                $indlo_appartement_tva = round(($value->fact_loyer_app_tva), 2);
                                $indlo_appartement_ttc = round(($value->fact_loyer_app_ttc), 2);
                                $indlo_parking_ht = round(($value->fact_loyer_park_ht), 2);
                                $indlo_parking_tva = round(($value->fact_loyer_park_tva), 2);
                                $indlo_parking_ttc = round(($value->fact_loyer_park_ttc), 2);
                                $indlo_charge_ht = round(($value->fact_loyer_charge_ht), 2);
                                $indlo_charge_tva = round(($value->fact_loyer_charge_tva), 2);
                                $indlo_charge_ttc = round(($value->fact_loyer_charge_ttc), 2);

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                    $data_fac_proto_pourcent['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data_fac_proto_pourcent['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data_fac_proto_pourcent['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data_fac_proto_pourcent['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data_fac_proto_pourcent['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data_fac_proto_pourcent['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                    $data_fac_proto_pourcent['periode_annee_id'] = $value->periode_annee_id;
                                    $data_fac_proto_pourcent['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                    $request = $this->FactureLoyer_m->save_data($data_fac_proto_pourcent, $clause_facture);

                                    $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                    $this->DocumentLoyer_m->delete_data($clause_delete);

                                    $request_id = array(
                                        'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                        'join' => array(
                                            'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                        ),
                                        'limit' => 1,
                                        'method' => 'row'
                                    );
                                    $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                    $this->generate_facture($facture_loyer);
                                }
                            }
                            break;
                        case 5:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime('+2 month', strtotime($start_date)));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-3 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime('-1 month', strtotime($date)));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);

                            $date_to_check_timestamp_debut = strtotime($date_debut);
                            $date_to_check_timestamp_fin = strtotime($datefin);

                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp_debut <  $start_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  -$value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  -$value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  -$value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $date1 = $start_date;
                                $date2 = $date_debut;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {

                                $date1 = $datefin;
                                $date2 = $end_date;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;
                                }

                                $data_fac_proto['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac_proto['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac_proto['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac_proto['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac_proto['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac_proto['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                $data_fac_proto['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac_proto['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac_proto['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                $data_fac_proto['periode_timestre_id'] = $value->periode_timestre_id;
                                $data_fac_proto['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                $request = $this->FactureLoyer_m->save_data($data_fac_proto, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin < $start_timestamp && $date_to_check_timestamp_fin < $end_timestamp) {

                                $indlo_appartement_ht = round(($value->fact_loyer_app_ht), 2);
                                $indlo_appartement_tva = round(($value->fact_loyer_app_tva), 2);
                                $indlo_appartement_ttc = round(($value->fact_loyer_app_ttc), 2);
                                $indlo_parking_ht = round(($value->fact_loyer_park_ht), 2);
                                $indlo_parking_tva = round(($value->fact_loyer_park_tva), 2);
                                $indlo_parking_ttc = round(($value->fact_loyer_park_ttc), 2);
                                $indlo_charge_ht = round(($value->fact_loyer_charge_ht), 2);
                                $indlo_charge_tva = round(($value->fact_loyer_charge_tva), 2);
                                $indlo_charge_ttc = round(($value->fact_loyer_charge_ttc), 2);

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                    $data_fac_proto_pourcent['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data_fac_proto_pourcent['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data_fac_proto_pourcent['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data_fac_proto_pourcent['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data_fac_proto_pourcent['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data_fac_proto_pourcent['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                    $data_fac_proto_pourcent['periode_timestre_id'] = $value->periode_timestre_id;
                                    $data_fac_proto_pourcent['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                    $request = $this->FactureLoyer_m->save_data($data_fac_proto_pourcent, $clause_facture);

                                    $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                    $this->DocumentLoyer_m->delete_data($clause_delete);

                                    $request_id = array(
                                        'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                        'join' => array(
                                            'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                        ),
                                        'limit' => 1,
                                        'method' => 'row'
                                    );
                                    $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                    $this->generate_facture($facture_loyer);
                                }
                            }
                            break;

                        default:
                            if ($moment_factu == 2) {
                                $start_date = $value->fact_loyer_date;
                                $end_date = date('Y-m-t', strtotime($start_date));
                            } else {
                                $date = $value->fact_loyer_date;
                                $start_date = date('Y-m-d', strtotime('-1 month', strtotime($date)));
                                $end_date = date('Y-m-t', strtotime($start_date));
                            }

                            $start_timestamp = strtotime($start_date);
                            $end_timestamp = strtotime($end_date);

                            $date_to_check_timestamp_debut = strtotime($date_debut);
                            $date_to_check_timestamp_fin = strtotime($datefin);

                            $fact_loyer_id = $value->fact_loyer_id;
                            $clause_facture = array("fact_loyer_id" => $fact_loyer_id);

                            if ($date_to_check_timestamp_debut <  $start_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $data_fac = array(
                                    'fact_loyer_num' => $value->facture_annee . '-' . str_pad($this->get_numero_factu($bail->bail_id), 5, '0', STR_PAD_LEFT),
                                    'fact_loyer_date' => $value->fact_loyer_date,
                                    'fact_loyer_app_ht' =>  -$value->fact_loyer_app_ht,
                                    'fact_loyer_app_tva' =>  -$value->fact_loyer_app_tva,
                                    'fact_loyer_app_ttc' =>  -$value->fact_loyer_app_ttc,
                                    'fact_loyer_park_ht' =>  -$value->fact_loyer_park_ht,
                                    'fact_loyer_park_tva' =>  -$value->fact_loyer_park_tva,
                                    'fact_loyer_park_ttc' =>  -$value->fact_loyer_park_ttc,
                                    'fact_loyer_charge_ht' =>  -$value->fact_loyer_charge_ht,
                                    'fact_loyer_charge_tva' =>  -$value->fact_loyer_charge_tva,
                                    'fact_loyer_charge_ttc' =>  -$value->fact_loyer_charge_ttc,
                                    "bail_id" => $value->bail_id,
                                    "periode_semestre_id" => $value->periode_semestre_id,
                                    "periode_timestre_id" => $value->periode_timestre_id,
                                    "periode_annee_id" => $value->periode_annee_id,
                                    "periode_mens_id" => $value->periode_mens_id,
                                    "facture_nombre" => $value->facture_nombre,
                                    "facture_annee" => $value->facture_annee,
                                    "dossier_id" => $value->dossier_id,
                                    "facture_commentaire" => $value->facture_commentaire,
                                    "mode_paiement" => $value->mode_paiement,
                                    "echeance_paiement" => $value->echeance_paiement,
                                    "emetteur_nom" => $value->emetteur_nom,
                                    "emet_adress1" => $value->emet_adress1,
                                    "emet_adress2" => $value->emet_adress2,
                                    "emet_adress3" => $value->emet_adress3,
                                    "emet_cp" => $value->emet_cp,
                                    "emet_pays" => $value->emet_pays,
                                    "emet_ville" => $value->emet_ville,
                                    "dest_nom" => $value->dest_nom,
                                    "dest_adresse1" => $value->dest_adresse1,
                                    "dest_adresse2" => $value->dest_adresse2,
                                    "dest_adresse3" => $value->dest_adresse3,
                                    "dest_cp" => $value->dest_cp,
                                    "dest_pays" => $value->dest_pays,
                                    "dest_ville" => $value->dest_ville,
                                    "fact_email_sent" => $value->fact_email_sent,
                                );
                                $request = $this->FactureLoyer_m->save_data($data_fac);

                                $request_id = array(
                                    'clause' => array('c_facture_loyer.bail_id' => $bail->bail_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'order_by_columns' => 'fact_loyer_id DESC',
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture_avoir($facture_loyer);
                            } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                $date1 = $start_date;
                                $date2 = $date_debut;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                $data_fac['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                                $data_fac['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac['fact_loyer_charge_ttc'] = $indlo_charge_ttc;

                                $request = $this->FactureLoyer_m->save_data($data_fac, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {

                                $date1 = $datefin;
                                $date2 = $end_date;

                                $diff_in_seconds = strtotime($date2) - strtotime($date1);
                                $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                                $date11 = $start_date;
                                $date22 = $end_date;

                                $diff_in_seconds = strtotime($date22) - strtotime($date11);
                                $days_3month = ($diff_in_seconds / (60 * 60 * 24));

                                if ($diff_in_days - 1 ==  $days_3month) {
                                    $diff_in_days =  $days_3month;
                                }

                                $indlo_appartement_ht = round((($value->fact_loyer_app_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_appartement_tva = $value->fact_loyer_app_tva;
                                $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                $indlo_parking_ht = round((($value->fact_loyer_park_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_parking_tva = $value->fact_loyer_park_tva;
                                $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                $indlo_charge_ht = round((($value->fact_loyer_charge_ht * $diff_in_days) / $days_3month), 2);
                                $indlo_charge_tva = $value->fact_loyer_charge_tva;
                                $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;
                                }

                                $data_fac_proto['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                $data_fac_proto['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                $data_fac_proto['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                $data_fac_proto['fact_loyer_park_ht'] = $indlo_parking_ht;
                                $data_fac_proto['fact_loyer_park_tva'] = $indlo_parking_tva;
                                $data_fac_proto['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                $data_fac_proto['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                $data_fac_proto['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                $data_fac_proto['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                $data_fac_proto['periode_mens_id'] = $value->periode_mens_id;
                                $data_fac_proto['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                $request = $this->FactureLoyer_m->save_data($data_fac_proto, $clause_facture);

                                $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                $this->DocumentLoyer_m->delete_data($clause_delete);

                                $request_id = array(
                                    'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                    'join' => array(
                                        'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                    ),
                                    'limit' => 1,
                                    'method' => 'row'
                                );
                                $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                $this->generate_facture($facture_loyer);
                            } elseif ($date_to_check_timestamp_fin < $start_timestamp && $date_to_check_timestamp_fin < $end_timestamp) {

                                $indlo_appartement_ht = round(($value->fact_loyer_app_ht), 2);
                                $indlo_appartement_tva = round(($value->fact_loyer_app_tva), 2);
                                $indlo_appartement_ttc = round(($value->fact_loyer_app_ttc), 2);
                                $indlo_parking_ht = round(($value->fact_loyer_park_ht), 2);
                                $indlo_parking_tva = round(($value->fact_loyer_park_tva), 2);
                                $indlo_parking_ttc = round(($value->fact_loyer_park_ttc), 2);
                                $indlo_charge_ht = round(($value->fact_loyer_charge_ht), 2);
                                $indlo_charge_tva = round(($value->fact_loyer_charge_tva), 2);
                                $indlo_charge_ttc = round(($value->fact_loyer_charge_ttc), 2);

                                if ($pourcentage_protocol != 0) {
                                    $indlo_appartement_ht =  $indlo_appartement_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_appartement_tva =  $indlo_appartement_tva;
                                    $tva_appartement = $indlo_appartement_ht * $indlo_appartement_tva / 100;
                                    $indlo_appartement_ttc = $indlo_appartement_ht + $tva_appartement;

                                    $indlo_parking_ht =  $indlo_parking_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_parking_tva =  $indlo_parking_tva;
                                    $tva_parking = $indlo_parking_ht * $indlo_parking_tva / 100;
                                    $indlo_parking_ttc = $indlo_parking_ht + $tva_parking;

                                    $indlo_charge_ht =  $indlo_charge_ht * floatval($pourcentage_protocol) / 100;
                                    $indlo_charge_tva =  $indlo_charge_tva;
                                    $tva_charge = $indlo_charge_ht * $indlo_charge_tva / 100;
                                    $indlo_charge_ttc = $indlo_charge_ht + $tva_charge;

                                    $data_fac_proto_pourcent['fact_loyer_app_ht'] = $indlo_appartement_ht;
                                    $data_fac_proto_pourcent['fact_loyer_app_tva'] = $indlo_appartement_tva;
                                    $data_fac_proto_pourcent['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_park_ht'] = $indlo_parking_ht;
                                    $data_fac_proto_pourcent['fact_loyer_park_tva'] = $indlo_parking_tva;
                                    $data_fac_proto_pourcent['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ht'] = $indlo_charge_ht;
                                    $data_fac_proto_pourcent['fact_loyer_charge_tva'] = $indlo_charge_tva;
                                    $data_fac_proto_pourcent['fact_loyer_charge_ttc'] = $indlo_charge_ttc;
                                    $data_fac_proto_pourcent['periode_mens_id'] = $value->periode_mens_id;
                                    $data_fac_proto_pourcent['facture_commentaire'] = 'Protocole signé le ' . date('d/m/Y', strtotime($date_sign_protocole)) . ' : ' . $pourcentage_protocol . '% de prise en charge du loyer.';

                                    $request = $this->FactureLoyer_m->save_data($data_fac_proto_pourcent, $clause_facture);

                                    $clause_delete = array('fact_loyer_id' => $fact_loyer_id);
                                    $this->DocumentLoyer_m->delete_data($clause_delete);

                                    $request_id = array(
                                        'clause' => array('fact_loyer_id' => $fact_loyer_id),
                                        'join' => array(
                                            'c_bail' => 'c_bail.bail_id  = c_facture_loyer.bail_id'
                                        ),
                                        'limit' => 1,
                                        'method' => 'row'
                                    );
                                    $facture_loyer = $this->FactureLoyer_m->get($request_id);
                                    $this->generate_facture($facture_loyer);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    public function getProtocoleBail()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $this->load->model('Protocol_m');
                $this->load->model('Bail_m');
                $data_post = $_POST['data'];

                $paramsBail = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'order_by_columns' => 'bail_id DESC',
                    'method' => 'row'
                );

                $bail = $this->Bail_m->get($paramsBail);

                $data['protocol'] = NULL;

                $data['etat_button_protocol'] = 1;

                if (!empty($bail)) {
                    $param_protocole = array(
                        'clause' => array('bail_id' => $bail->bail_id),
                    );

                    $protocol = $this->Protocol_m->get($param_protocole);

                    if (!empty($protocol)) {
                        $data['protocol'] = $protocol;
                        $count_protocol = count($protocol);

                        if ($protocol[$count_protocol - 1]->etat_protocol == 1) {
                            $data['etat_button_protocol'] = 1;
                        } else {
                            $data['etat_button_protocol'] = 0;
                        }
                    }
                }

                $this->renderComponant("Clients/clients/lot/bails/table_protocole", $data);
            }
        }
    }

    public function validerProtocol()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation",
                );
                $this->load->model('Protocol_m', 'protocol');
                $data = decrypt_service($_POST["data"]);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['id_protocol'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous valider ce protocole ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $params = array(
                        'clause' => array('id_protocol' => $data['id_protocol']),
                        'join' => array(
                            'c_bail' => 'c_bail.bail_id  = c_protocol.bail_id'
                        ),
                        'order_by_columns' => 'id_protocol DESC',
                        'method' => 'row'
                    );

                    $protocol = $this->protocol->get($params);

                    $clause = array('id_protocol' => $data['id_protocol']);
                    $data_update = array('etat_protocol' => 1);
                    $this->protocol->save_data($data_update, $clause);

                    if ($protocol->type_abandon == 0) {
                        $this->gererAvoirAndFactures_cloture_bail($protocol->bail_id, $protocol->date_debut_protocole);
                    } else {
                        $this->gererAvoirAndFactures_AbandonPartiel($protocol->bail_id, $protocol->date_debut_protocole, $protocol->date_fin_protocole);
                    }
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['id_protocol'],
                            'cliLot_id' => $protocol->cliLot_id
                        ),
                        'message' =>  "Validation effectuée",
                    );
                }
                echo json_encode($retour);
            }
        }
    }

    public function consulterProtocol()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $this->load->model('Protocol_m');

                $data_post = $_POST['data'];
                $data['titre_modal'] = 'Consultation du protocole';

                $params = array(
                    'clause' => array('id_protocol' => $data_post['id_protocol']),
                    'method' => 'row'
                );

                $data['protocol'] = $this->Protocol_m->get($params);

                $this->renderComponant("Clients/clients/lot/bails/modalvalidation_protocole", $data);
            }
        }
    }

    public function modifierProtocol()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (is_ajax()) {
                $this->load->model('Protocol_m');

                $data_post = $_POST['data'];
                $data['titre_modal'] = 'Modification du protocole';

                $params = array(
                    'clause' => array('id_protocol' => $data_post['id_protocol']),
                    'method' => 'row'
                );

                $data['protocol'] = $this->Protocol_m->get($params);

                $this->renderComponant("Clients/clients/lot/bails/modalvalidation_protocole", $data);
            }
        }
    }

    public function generer_indexation_en_attente()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndexationLoyer_m');
            $this->load->model('Bail_m');
            $data_post = $_POST['data'];

            $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);

            $this->IndexationLoyer_m->delete_data($clause_delete);

            $bail = $this->Bail_m->get(
                array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => 'row'
                )
            );

            $data = $this->IndexationLoyer_m->get(
                array(
                    'clause' => array(
                        'bail_id' => $data_post['bail_id'],
                        'indlo_ref_loyer' => 1
                    ),
                    'method' => 'row'
                )
            );

            $date_fin = date('Y-m-d', strtotime('-1 day', strtotime($data->indlo_debut)));

            unset($data->indlo_id);
            $data->indlo_ref_loyer = 0;
            $data->indlo_indince_non_publie = 1;
            $data->indlo_debut = $bail->bail_debut;
            $data->indlo_fin = $date_fin;

            $retour = array('status' => 500, 'message' => "Echec");

            $request = $this->IndexationLoyer_m->save_data($data);

            if (!empty($request)) {
                $retour = array('status' => 200, 'message' => "Générattion réussi");
            }

            echo json_encode($retour);
        }
    }

    public function trie_indice_reference()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceValeurLoyer_m');
            $data_post = $_POST['data'];
            $clause = array(
                'clause' => array('indloyer_id' => $data_post['indloyer_id']),
                'order_by_columns' => 'indval_annee ASC, indval_trimestre ASC'
            );
            $indice_valeur_loyer = $this->IndiceValeurLoyer_m->get($clause);
            $data_retour['indloyer_id'] = $data_post['indloyer_id'];
            $data_retour['indval_id_selected'] = $data_post['indval_id'];
            $data_retour['indice_valeur_loyer'] = $data_post['indloyer_id'] != 7 ? $indice_valeur_loyer : '';

            echo json_encode($data_retour);
        }
    }

    public function tri_indice_plafonnement()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceLoyer_m');
            $indice_loyer = $this->IndiceLoyer_m->get(array());

            $data_retour['indice_loyer'] = $indice_loyer;
            $data_retour['indval_id_selected'] = $_POST['data']['indice_valeur_plafonnement'];

            echo json_encode($data_retour);
        }
    }

    public function alert_dateSigne_mandat()
    {
        if (is_ajax()) {
            $data['onglet'] = $_POST['data']['onglet'];
            $this->renderComponant("Clients/clients/lot/bails/form-alert", $data);
        }
    }

    public function create_indexationZero()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndexationLoyer_m');
            $this->load->model('Bail_m');
            $data_post = $_POST['data'];

            $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);
            $this->IndexationLoyer_m->delete_data($clause_delete);

            $bail = $this->Bail_m->get(
                array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => 'row'
                )
            );

            $data = $this->IndexationLoyer_m->get(
                array(
                    'clause' => array(
                        'bail_id' => $data_post['bail_id'],
                        'indlo_ref_loyer' => 1
                    ),
                    'method' => 'row'
                )
            );

            unset($data->indlo_id);
            $data->indlo_ref_loyer = 0;
            $data->indlo_indince_non_publie = 2;
            $data->indlo_debut = $bail->bail_debut;
            $data->indlo_fin = $bail->bail_fin;

            $retour = array('status' => 500, 'message' => "Echec");

            $request = $this->IndexationLoyer_m->save_data($data);

            if (!empty($request)) {
                $retour = array('status' => 200, 'message' => "Générattion réussi");
            }

            echo json_encode($retour);
        }
    }

    public function delete_indexationZero()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndexationLoyer_m');
            $this->load->model('Bail_m');
            $data_post = $_POST['data'];
            $retour = array('status' => 500, 'message' => "Echec");
            $clause_delete = array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1, 'indlo_ref_loyer' => 0);

            $request = $this->IndexationLoyer_m->delete_data($clause_delete);

            if (!empty($request)) {
                $retour = array('status' => 200, 'message' => "Supprimer réussi");
            }
            echo json_encode($retour);
        }
    }
}
