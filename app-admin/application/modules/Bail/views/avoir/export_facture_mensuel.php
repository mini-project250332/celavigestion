<style type="text/css">
    @page {
        margin: 3%;
    }

    .column {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
    }

    .left {
        width: 60%;
    }

    .right {
        width: 37%;
    }

    .col {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
    }

    .column_left {
        width: 60%;
    }

    .column_right {
        width: 34%;
    }

    .espace {
        width: 3%;
    }

    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    .facture {
        font-size: 17px;
        font-weight: bold;
    }

    .sous_tilte {
        font-size: 13px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .contact {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 100%;
        font-family: Arial, Helvetica, sans-serif;
    }

    th {
        height: 40px;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    td {
        height: 40px;
    }

    .th_designation {
        width: 70%;
        border-left: 1px solid black;
    }

    .th_tva {
        text-align: right;
    }

    .th_total {
        text-align: right;
        border-right: 1px solid black;
    }

    .td_designation {
        font-weight: bold;
    }

    .td_tva {
        text-align: right;
    }

    .td_ht {
        text-align: right;
    }

    .tfoot_designation {
        text-align: center;
        font-weight: bold;
        border-left: 1px solid black;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .tfoot_tva {
        text-align: right;
        font-weight: bold;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .tfoot_ht {
        text-align: right;
        font-weight: bold;
        border-right: 1px solid black;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .commentaire {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 11px;
    }

    .footer_exp {
        font-family: Arial, Helvetica, sans-serif;
        border: 1px solid black;
        text-align: center;
        width: 55%;
        font-size: 13px;
        margin-left: 23%;
    }

    .foot_bien {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
    }
</style>

<div class="row">
    <div class="column left">
        <span><?= $facture->emetteur_nom ?></span><br>

        <?php if (!empty($facture->emet_adress1)) : ?>
            <span>
                <?= $facture->emet_adress1 ?>
            </span><br>
        <?php endif ?>

        <?php if (!empty($facture->emet_adress2)) : ?>
            <span>
                <?= $facture->emet_adress2 ?>
            </span><br>
        <?php endif ?>

        <?php if (!empty($facture->emet_adress3)) : ?>
            <span>
                <?= $facture->emet_adress3 ?>
            </span><br>
        <?php endif ?>

        <span><?= $facture->emet_cp ?>&nbsp;<?= $facture->emet_ville ?></span><br>
        <span>Siret : <?= $facture->dossier_siret ?> - APE : <?= $facture->dossier_ape ?></span><br>
        <span>Numéro de TVA intracommunautaire :</span><br>
        <span><?= $facture->dossier_tva_intracommunautaire ?></span>
    </div>

    <div class="column right">
        <br><br><br>
        <span class="facture">Facture d'avoir N°&nbsp;<?= $facture->fact_loyer_num ?></span>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col column_left">
        <span class="sous_tilte">Adresse du bien : </span><br><br>
        <span><?= $facture->progm_nom ?></span><br>
        <span>
            <?= $facture->emet_adress1 ?>
        </span><br>
        <span>
            <?= $facture->emet_cp ?>&nbsp;
            <?= $facture->emet_ville ?>
        </span>
    </div>
    <div class="col espace"></div>
    <div class="col column_right">
        <span class="sous_tilte" style="text-align: center;">Adresse Facturation :</span><br><br>
        <span><?= $facture->dest_nom ?></span><br>
        <?php if (!empty($facture->dest_adresse1)) : ?>
            <span>
                <?= $facture->dest_adresse1 ?>
            </span><br>
        <?php endif ?>
        <?php if (!empty($facture->dest_adresse2)) : ?>
            <span>
                <?= $facture->dest_adresse2 ?>
            </span><br>
        <?php endif ?>
        <?php if (!empty($facture->dest_adresse3)) : ?>
            <span>
                <?= $facture->dest_adresse3 ?>
            </span><br>
        <?php endif ?>
        <span><?= $facture->dest_cp ?>&nbsp;<?= $facture->dest_ville ?></span><br>
        <br><br>
        <span><?= $facture->emet_ville ?> , le <?= date("d/m/Y", strtotime($facture->fact_loyer_date)); ?></span>
    </div>
</div>

<br><br><br>

<div class="contact">
    <span><i><b><u>Votre contact</u>&nbsp;:&nbsp;SARL CELAVIGESTION</b></i></span><br>
    <span><i><b>Parc de Brais 39 route de Fondeline 44600 Saint-Nazaire</b></i></span><br>
    <span><i><b>Tel : <?= $facture->util_phonemobile ?> - Email: <?= $facture->util_mail ?> </b></i></span>
</div>

<br>
<?php if (!empty($facture->facture_commentaire)) : ?>
    <div class="commentaire">
        <?= $facture->facture_commentaire ?>
    </div>
<?php endif ?>
<br>

<table>
    <thead>
        <tr>
            <th class="th_designation">Désignation</th>
            <th class="th_tva">TVA</th>
            <th class="th_total">Total HT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="td_designation">LOYER APPARTEMENT HT&nbsp;<?= $facture->periode_mens_libelle ?>&nbsp;<?= $facture->facture_annee ?></td>
            <td class="td_tva"><?= number_format(-$facture->fact_loyer_app_tva, 2, ",", ".") ?> %</td>
            <td class="td_ht"><?=  number_format($facture->fact_loyer_app_ht, 2, ",", ".") ?> €</td>
        </tr>
        <?php if (!empty($facture->fact_loyer_park_ht)) : ?>
            <tr>
                <td class="td_designation">LOYER PARKING HT&nbsp;<?= $facture->periode_mens_libelle ?>&nbsp;<?= $facture->facture_annee ?></td>
                <td class="td_tva"><?= number_format(-$facture->fact_loyer_park_tva, 2, ",", ".") ?> %</td>
                <td class="td_ht"><?=  number_format($facture->fact_loyer_park_ht, 2, ",", ".") ?> €</td>
            </tr>
        <?php endif ?>
        <?php if (!empty($facture->fact_loyer_charge_ht)) : ?>
            <tr>
                <td class="td_designation">FORFAIT CHARGES HT&nbsp;<?= $facture->periode_mens_libelle ?>&nbsp;<?= $facture->facture_annee ?></td>
                <td class="td_tva"><?= number_format(-$facture->fact_loyer_charge_tva, 2, ",", ".") ?> %</td>
                <td class="td_ht"><?= number_format(-$facture->fact_loyer_charge_ht, 2, ",", ".") ?> €</td>
            </tr>
        <?php endif ?>
    </tbody>
    <tfoot>
        <tr>
            <?php $date = explode("=>", $facture->echeance_paiement); ?>
            <td class="tfoot_designation">Echéance de paiement : <?= $date[1] ?> </td>
            <td class="tfoot_tva">
                Total HT :<br>
                TVA : <br>
                Total TTC : </td>
            <td class="tfoot_ht">
                <?php $total_ht = $facture->fact_loyer_app_ht + $facture->fact_loyer_park_ht - $facture->fact_loyer_charge_ht;
                $total_ttc = $facture->fact_loyer_app_ttc + $facture->fact_loyer_park_ttc - $facture->fact_loyer_charge_ttc;
                $total_tva =  ($facture->fact_loyer_app_ht * $facture->fact_loyer_app_tva / 100) + ($facture->fact_loyer_park_ht * $facture->fact_loyer_park_tva / 100) - ($facture->fact_loyer_charge_ht * $facture->fact_loyer_charge_tva / 100) ?>
                <?=  number_format($total_ht, 2, ",", ".") ?> € <br>
                <?=  number_format(-$total_tva, 2, ",", ".") ?> €<br>
                <?=  number_format($total_ttc, 2, ",", ".") ?> € <br>
            </td>
        </tr>
    </tfoot>
</table>

<br>

<div class="commentaire">
    <?php if ($facture->cliLot_encaiss_loyer == "Compte client") : ?>
        <span>IBAN : <?= $facture->cliLot_iban_client ?>&nbsp;&emsp;BIC : <?= $facture->cliLot_bic_client ?></span>
    <?php else : ?>
        <span>IBAN : <?= $rib->rib_iban ?>&nbsp;&emsp;BIC : <?= $rib->rib_bic ?></span>
    <?php endif ?>
</div>

<br>

<div class="commentaire">
    <?php $date = explode("=>", $facture->echeance_paiement); ?>
    Tout règlement non parvenu avant le <?= $date[1] ?> sera majoré des pénalités de retard légales <br> Escompte pour paiement anticipé : néant <br>
    Passé ce délai, sans obligation d'envoi d'une relance, conformément à l’article L441-6 du code de commerce, il sera appliqué une pénalité calculée à un taux
    annuel de 20% sans que ce taux soit inférieur à 3 fois le taux d'intérêt légal.<br>
    Une indemnité forfaitaire pour frais de recouvrement de 40€ sera aussi exigible.
</div>

<br><br><br><br><br><br>

<div class="row">
    <div class="footer_exp">
        <span class="foot_bien"><?= $facture->emetteur_nom ?>&nbsp;<?= $facture->emet_cp ?>&nbsp;<?= $facture->emet_ville ?></span> <br>
        <span class="foot_bien">
            <?php if (!empty($facture->emet_adress1)) : ?>
                <?= $facture->emet_adress1 ?>&nbsp;
            <?php endif ?>

            <?php if (!empty($facture->emet_adress2)) : ?>
                <?= $facture->emet_adress2 ?>&nbsp;
            <?php endif ?>

            <?php if (!empty($facture->emet_adress3)) : ?>
                <?= $facture->emet_adress3 ?>&nbsp;
            <?php endif ?>
        </span><br>
        <span>Siret : <?= $facture->dossier_siret ?> - APE : <?= $facture->dossier_ape ?></span><br>
        <span>Numéro de TVA intracommunautaire : <?= $facture->dossier_tva_intracommunautaire ?></span>
    </div>
</div>