<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Accueil_Intranet extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/accueil_intranet.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-accueil-intranet";
        $this->_data['sous_module'] = "accueil_intranet" . DIRECTORY_SEPARATOR . "contenaire-accueil-intranet";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $this->renderComponant("Setting/accueil_intranet/header-accueil-intranet", $data);
            }
        }
    }

    public function getMessage()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('AccueilIntranet_m', 'accueil_intranet');
                $params = array();
                $data['accueil'] = $this->accueil_intranet->get($params);
                $this->renderComponant("Setting/accueil_intranet/liste-accueil-intranet", $data);
            }
        }
    }

    public function updateMessage()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('AccueilIntranet_m', 'accueil_intranet');
                $data_post = $_POST["data"];
                $data['model_accueil'] = $this->accueil_intranet->get(
                    array('clause' => array("mess_id" => $data_post['mess_id']), 'method' => "row")
                );
                $this->renderComponant("Setting/accueil_intranet/form-model-intranet", $data);
            }
        }
    }

    public function saveUpdateModel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('AccueilIntranet_m', 'accueil_intranet');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $mess_id = $data['mess_id'];
                    unset($data['mess_id']);

                    $clause = array("mess_id" => $mess_id);
                    $request = $this->accueil_intranet->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
