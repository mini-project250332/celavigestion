<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sie extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/sie.js',
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-sie";
        $this->_data['sous_module'] = "sie" . DIRECTORY_SEPARATOR . "contenaire-sie";
        $this->render('contenaire');
    }

    // get liste sie

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/sie/header-sie", $data);
            }
        }
    }

    public function getListSie()
    {
        if (is_ajax()) {
            $this->load->model('Sie_m');
            $this->load->model('Dossier_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('sie_etat' => 1),
                    'order_by_columns' => 'sie_libelle ASC'
                );
                $data['sie'] = $this->Sie_m->get($params);
                $data['dossier'] = $this->Dossier_m->get(
                    array('clause' => array("dossier_id" => 1), 'method' => "result")
                );
                $this->renderComponant("Setting/sie/liste-sie", $data);
            }
        }
    }

    public function getformSie()
    {
        if (is_ajax()) {
            $this->load->model('Sie_m');
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                if ($post['action'] == "edit") {
                    $data['sie'] = $this->Sie_m->get(
                        array('clause' => array("sie_id" => $post['sie_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/sie/form-sie", $data);
            }
        }
    }

    private function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function cruSie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Sie_m');

                $data = $_POST;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/RIB SIE/";
                    $file = $_FILES['file_0']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file_0']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;

                    $this->makeDirPath($target_dir);

                    $counter = 1;
                    while (file_exists($path_filename_ext)) {
                        $filename = $filename . '_' . $counter;
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $counter++;
                    }
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }
                    $data['file_rib_sie'] = $path_filename_ext;
                }

                $data['date_maj'] = date('Y-m-d H:i:s');
                $action = $data['action'];

                $data['sie_rib'] = str_replace(' ', '', $data['sie_rib']);

                if ($data['action'] == "edit") {
                    $clause = array("sie_id" => $data['sie_id']);
                    unset($data['sie_id']);
                    unset($data['action']);
                    $request = $this->Sie_m->save_data($data, $clause);
                } else {
                    $data['sie_etat'] = 1;
                    unset($data['action']);
                    $request = $this->Sie_m->save_data($data);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $request, array("message" => ($action == "add") ? "L'information du nouveau sie a été enregistrée avec succès" : "La modification réussie."));
                }

                echo json_encode($retour);
            }
        }
    }

    public function removeSie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Sie_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('sie_id' => $data['sie_id']),
                    'columns' => array('sie_id', 'sie_libelle'),
                    'method' => "row",
                );
                $request_get = $this->Sie_m->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['sie_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le " . $request_get->sie_libelle . " " . "?",
                            'btnConfirm' => "Supprimer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('sie_id' => $data['sie_id']);
                    $data_update = array('sie_etat' => 0);
                    $request = $this->Sie_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['sie_id']),
                            'message' => "Le sie " . $request_get->sie_libelle . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formAlertSuppSie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->renderComponant("Setting/sie/form-alert");
            }
        }
    }

    public function removefilesie()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Sie_m');
                $data_post = $_POST['data'];
                $param = array('sie_id' => $data_post['sie_id']);
                $data_sie = array(
                    'file_rib_sie' => NULL,
                );
                $request = $this->Sie_m->save_data($data_sie, $param);
                if (!empty($request)) {
                    $retour = retour(true, "success", $param, array("message" => "Fichier Supprimée"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function alert_rib_Iban()
    {
        if (is_ajax()) {
            $this->renderComponant("Setting/sie/form-alert_iban");
        }
    }
}
