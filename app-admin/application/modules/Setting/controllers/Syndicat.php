<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Syndicat extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/syndicat.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-syndicat";
        $this->_data['sous_module'] = "syndicat" . DIRECTORY_SEPARATOR . "contenaire-syndicat";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $this->renderComponant("Setting/syndicat/header-syndicat", $data);
            }
        }
    }

    public function getListSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Syndicat_m', 'syndicat');
                $params = array(
                    'clause' => array('syndicat_etat' => 1),
                    'order_by_columns' => "syndicat_nom ASC",
                );
                $data['syndicat'] = $this->syndicat->get($params);
                $this->renderComponant("Setting/syndicat/liste-syndicat", $data);
            }
        }
    }

    public function getFormSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                $this->load->model('TypeContactSyndicat_m', 'type_contact');
                if ($post['action'] != "add") {
                    $this->load->model('Syndicat_m', 'syndicat');
                    $data['syndicat'] = $this->syndicat->get(
                        array(
                            'clause' => array("syndicat_id" => $post['syndicat_id']),
                            'method' => "row"
                        )
                    );
                }
                $data['action'] = $post['action'];
                $data['type_contact'] = $this->type_contact->get(array());
                $this->renderComponant("Setting/syndicat/form-syndicat", $data);
            }
        }
    }

    public function cruSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Syndicat_m', 'syndicat');
                $this->load->model('ContactSyndicat_m', 'contact');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_gestionnaire = null;
                    $clause = null;
                    if ($data['action'] == "edit") {
                        $clause = array("syndicat_id" => $data['syndicat_id']);
                        unset($data['syndicat_id']);
                    } else {
                        $data['syndicat_date_creation'] = Carbon::now()->toDateTimeString();
                        $data['syndicat_etat'] = 1;
                        $data_gestionnaire['syndicat_date_creation'] = Carbon::now()->toDateTimeString();
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    // Data syndicat to save
                    $data_gestionnaire["syndicat_nom"] = $data['syndicat_nom'];
                    $data_gestionnaire["syndicat_adresse1"] = $data['syndicat_adresse1'];
                    $data_gestionnaire["syndicat_adresse2"] = $data['syndicat_adresse2'];
                    $data_gestionnaire["syndicat_adresse3"] = $data['syndicat_adresse3'];
                    $data_gestionnaire["syndicat_cp"] = $data['syndicat_cp'];
                    $data_gestionnaire["syndicat_ville"] = $data['syndicat_ville'];
                    $data_gestionnaire["syndicat_pays"] = $data['syndicat_pays'];
                    $data_gestionnaire['syndicat_etat'] = $data['syndicat_etat'];

                    // Data contact to save
                    $clause_data_contact = null;
                    $data_contact["cnctSyndicat_nom"] = $data['cnctSyndicat_nom'];
                    $data_contact["cnctSyndicat_prenom"] = $data['cnctSyndicat_prenom'];
                    $data_contact["cnctSyndicat_fonction"] = $data['cnctSyndicat_fonction'];
                    $data_contact["cnctSyndicat_service"] = $data['cnctSyndicat_service'];
                    $data_contact["cnctSyndicat_email1"] = $data['cnctSyndicat_email1'];
                    $data_contact["cnctSyndicat_email2"] = $data['cnctSyndicat_email2'];
                    $data_contact["cnctSyndicat_tel1"] = $data['cnctSyndicat_tel1'];
                    $data_contact["cnctSyndicat_tel2"] = $data['cnctSyndicat_tel2'];
                    $data_contact["type_cont_id"] = $data['type_cont_id'];
                    $data_contact["etat_contact"] = 1;

                    $request = $this->syndicat->save_data($data_gestionnaire, $clause);
                    $insert_id = $this->db->insert_id();
                    $data_contact["syndicat_id"] = $insert_id;
                    $this->contact->save_data($data_contact, $clause_data_contact);
                    if (!empty($request)) {
                        $message = array(
                            "message" => ($action == "add") ?
                                "L'information du nouveau syndicat a été enregistrée avec succès" : "La modification a été réussi avec succès."
                        );
                        $message['liste'] = array(
                            'id' => $request,
                            'libelle' => $data['syndicat_nom']
                        );
                        $retour = retour(true, "success", $request, $message);
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formUpdateSyndicat()
    {
        $this->load->model('Syndicat_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['syndicat'] = $this->Syndicat_m->get(
                    array('clause' => array("syndicat_id" => $data_post['syndicat_id']), 'method' => "row")
                );
                $this->renderComponant("Setting/syndicat/fiche-syndicat", $data);
            }
        }
    }

    public function pageficheSyndicat()
    {
        $this->load->model('Syndicat_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('syndicat_id' => $data_post['syndicat_id']),
                    'method' => "row"
                );
                $data['syndicat'] = $this->Syndicat_m->get($params);
                if ($data_post['page'] == "form") {
                    $this->renderComponant("Setting/syndicat/form-update-identification", $data);
                } else {
                    $this->renderComponant("Setting/syndicat/fiche-identification", $data);
                }
            }
        }
    }

    public function removeSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Syndicat_m', 'syndicat');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('syndicat_id' => $data['syndicat_id']),
                    'columns' => array('syndicat_id', 'syndicat_nom'),
                    'method' => "row",
                );
                $request_get = $this->syndicat->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['syndicat_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le syndicat " . $request_get->syndicat_nom,
                            'btnConfirm' => "Supprimer le syndicat",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('syndicat_id' => $data['syndicat_id']);
                    $data_update = array('syndicat_etat' => 0);
                    $request = $this->syndicat->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['syndicat_id']),
                            'message' => "Le syndicat " . $request_get->syndicat_nom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    public function updateInfoSyndicat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Syndicat_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $syndicat_id = $data['syndicat_id'];
                    //    $data['client_date_creation'] = date_sql($data['client_date_creation'],'-','jj/MM/aaaa');
                    unset($data['prospect_id']);

                    $clause = array("syndicat_id" => $syndicat_id);
                    $request = $this->Syndicat_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function listeContact()
    {
        $this->load->model('ContactSyndicat_m', 'contact');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'syndicat_id' => $data_post['syndicat_id'],
                        'etat_contact' => 1
                    ),
                    'join' => array(
                        'c_type_contact_syndicat' => 'c_type_contact_syndicat.type_cont_id = c_contactsyndicat.type_cont_id'
                    )
                );
                $data["syndicat_id"] = $data_post['syndicat_id'];
                $data['contact'] = $this->contact->get($params);

                $this->renderComponant("Setting/syndicat/liste-contact", $data);
            }
        }
    }

    public function getFormContact()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('TypeContactSyndicat_m', 'type_contact');
                $data['syndicat_id'] = $data_post['syndicat_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('ContactSyndicat_m', 'contact');
                    $params = array(
                        'clause' => array('cnctSyndicat_id ' => $data_post['cnctSyndicat_id']),
                        'method' => "row",
                        'join' => array(
                            'c_type_contact_syndicat' => 'c_type_contact_syndicat.type_cont_id = c_contactsyndicat.type_cont_id'
                        )
                    );
                    $data['contact'] = $request = $this->contact->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['type_contact'] = $this->type_contact->get(array());
                $this->renderComponant("Setting/syndicat/form-contact", $data);
            }
        }
    }

    public function cruContact()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('ContactSyndicat_m', 'contact');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("syndicat_id" => $data['syndicat_id'], "action" => $action);
                    $data['etat_contact'] = 1;
                    unset($data['action']);
                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("cnctSyndicat_id" => $data['cnctSyndicat_id']);
                        $data_retour['cnctSyndicat_id'] = $data['cnctSyndicat_id'];
                        unset($data['cnctSyndicat_id']);
                    }

                    $request = $this->contact->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Contact ajouté avec succès" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeContact()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('ContactSyndicat_m', 'contact');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cnctSyndicat_id' => $data['cnctSyndicat_id']),
                    'columns' => array('cnctSyndicat_id', 'cnctSyndicat_nom', 'cnctSyndicat_prenom'),
                    'method' => "row",
                );
                $request_get = $this->contact->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cnctSyndicat_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce contact ?",
                            'btnConfirm' => "Supprimer le contact",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cnctSyndicat_id' => $data['cnctSyndicat_id']);
                    $data_update = array('etat_contact' => 0);
                    $request = $this->contact->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cnctSyndicat_id']),
                            'message' => "Contact supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
