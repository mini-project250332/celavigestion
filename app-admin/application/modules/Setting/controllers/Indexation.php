<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;

class Indexation extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/indexation.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-indexation";
        $this->_data['sous_module'] = "indexation" . DIRECTORY_SEPARATOR . "contenaire-indexation";
        $this->render('contenaire');
    }

    // get liste indexation

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $this->renderComponant("Setting/indexation/header-indexation", $data);
            }
        }
    }

    public function getListIndexation()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndiceLoyer_m', 'indice');

                $params = array(
                    'columns' => array('indloyer_id', 'indloyer_description', 'indloyer_valide', 'indloyer_periode'),
                    'order_by_columns' => "indloyer_id ASC"
                );
                $data['indice'] = $this->indice->get($params);
                $this->renderComponant("Setting/indexation/liste-indexation", $data);
            }
        }
    }

    public function getFormIndice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('IndiceLoyer_m', 'indice');
                    $data['indice'] = $this->indice->get(
                        array('clause' => array("indloyer_id" => $post['indloyer_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/indexation/form-indexation", $data);
            }
        }
    }

    public function AjouterIndice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndiceLoyer_m', 'indice');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;

                    $postdataAnnuel = array(
                        "indloyer_description" => $data["indloyer_description"],
                        "indloyer_periode" => $data["indloyer_periode"],
                        "indloyer_date_saisie_an_jour" => $data["indloyer_date_saisie_an_jour"],
                        "indloyer_date_saisie_an_mois" => $data["indloyer_date_saisie_an_mois"],
                    );

                    $postdataTrimestre = array(
                        "indloyer_description" => $data["indloyer_description"],
                        "indloyer_periode" => $data["indloyer_periode"],
                        "indloyer_date_saisie_t1_jour" => $data["indloyer_date_saisie_t1_jour"],
                        "indloyer_date_saisie_t1_mois" => $data["indloyer_date_saisie_t1_mois"],
                        "indloyer_date_saisie_t2_jour" => $data["indloyer_date_saisie_t2_jour"],
                        "indloyer_date_saisie_t2_mois" => $data["indloyer_date_saisie_t2_mois"],
                        "indloyer_date_saisie_t3_jour" => $data["indloyer_date_saisie_t3_jour"],
                        "indloyer_date_saisie_an_mois" => $data["indloyer_date_saisie_t3_mois"],
                        "indloyer_date_saisie_an_jour" => $data["indloyer_date_saisie_t4_jour"],
                        "indloyer_date_saisie_an_mois" => $data["indloyer_date_saisie_t4_mois"],
                    );

                    if ($data['action'] == "edit") {
                        $clause = array("indloyer_id" => $data['indloyer_id']);
                        unset($data['indloyer_id']);
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $controlRequest = $data['indloyer_periode'] == 'Annuelle' ? 'Annuelle' : 'Trimestrielle';

                    $request = ($controlRequest == "Annuelle") ? $this->indice->save_data($postdataAnnuel, $clause) : $this->indice->save_data($postdataTrimestre, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action == "add") ? "Enregistrement effectué" : "La modification réussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function formUpdateIndice()
    {
        $this->load->model('IndiceLoyer_m', 'indice');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $data = array();

                $data['indice'] = $this->indice->get(
                    array('clause' => array("indloyer_id" => $data_post['indloyer_id']), 'method' => "row")
                );

                $this->renderComponant("Setting/indexation/fiche-indexation", $data);
            }
        }
    }

    public function validerIndice()
    {
        if (is_ajax()) {

            $data_post = $_POST["data"];
            $data = array();

            $data['indval_id'] = $data_post['indval_id'];
            $data['indloyer_id'] = $data_post['indloyer_id'];

            $this->renderComponant("Setting/indexation/form-valide_indice", $data);
        }
    }

    public function AlertValidationValeur()
    {
        if (is_ajax()) {

            $data_post = $_POST["data"];
            $data = array();

            $data['indval_id'] = $data_post['indval_id'];
            $data['indloyer_id'] = $data_post['indloyer_id'];

            $this->renderComponant("Setting/indexation/alert-valide-valeur", $data);
        }
    }


    public function validerValeur()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndiceValeur_m', 'indice');
                $this->load->model('Cron_reindexation_m');
                $data_post = $_POST["data"];
                $data = array();

                $data['indval_id'] = $data_post['indval_id']['value'];
                $data['indloyer_id'] = $data_post['indloyer_id']['value'];

                $clause = array('indval_id' => $data['indval_id']);
                $data_update = array(
                    'indval_valide' => 1,
                    'util_prenom_valide' => $_SESSION['session_utilisateur']['util_prenom'],
                    'indval_date_generation' => date('Y-m-d'),
                    'indval_date_validation' => date('Y-m-d'),
                );

                $request = $this->indice->save_data($data_update, $clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'data' => array(
                            'id' => $data['indloyer_id']
                        ),
                        'message' => "Indexation validée avec succès",
                    );

                    $param = array(
                        'clause' => array(
                            'indloyer_id' => $data['indloyer_id'], 'etat_reindex' => 0
                        ),
                        'method' => 'row'
                    );

                    $exist_reindex = $this->Cron_reindexation_m->get($param);

                    $data_recentCron = array(
                        'indloyer_id' => $data["indloyer_id"],
                        'date_valid_indice' => date('Y-m-d:H:i:s')
                    );
                    if (empty($exist_reindex)) {
                        $this->Cron_reindexation_m->save_data($data_recentCron);
                    } else {
                        $clause_reindex = array('cron_index_id ' => $exist_reindex->cron_index_id);
                        $this->Cron_reindexation_m->save_data($data_recentCron, $clause_reindex);
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function pageficheIndice()
    {
        $this->load->model('IndiceLoyer_m', 'indice');
        $this->load->model('IndiceValeur_m', 'valeur');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('indloyer_id' => $data_post['indloyer_id']),
                    'method' => "row"
                );

                $data['valeur'] = $this->valeur->get(
                    array('clause' => array("indloyer_id" => $data_post['indloyer_id']))
                );

                $data['indice'] = $this->indice->get($params);
                $data['indloyer_id'] = $data_post['indloyer_id'];

                if ($data_post['page'] == "form") {
                    $this->renderComponant("Setting/indexation/form-update-indice", $data);
                } else {
                    $this->renderComponant("Setting/indexation/fiche-identification", $data);
                }
            }
        }
    }

    public function removeIndice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('IndiceLoyer_m', 'indice');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('indloyer_id' => $data['indloyer_id']),
                    'columns' => array('indloyer_id', 'indloyer_description'),
                    'method' => "row",
                );
                $request_get = $this->indice->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['indloyer_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer l'indice " . $request_get->indloyer_description . "?",
                            'btnConfirm' => "Supprimer l'indice",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('indloyer_id' => $data['indloyer_id']);
                    $request = $this->indice->delete_data($clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['indloyer_id']),
                            'message' => "L'indice " . $request_get->indloyer_description . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function listeValeurIndice()
    {
        $this->load->model('IndiceLoyer_m', 'indice');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $data = array();

                $data['indice'] = $this->indice->get(
                    array('clause' => array("indloyer_id" => $data_post['indloyer_id']), 'method' => "row")
                );

                $this->renderComponant("Setting/indexation/liste-valeur-indice", $data);
            }
        }
    }

    public function listeValeur()
    {
        $this->load->model('IndiceValeur_m', 'indice');
        $this->load->model('LotIndexe_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array('indloyer_id' => $data_post['indloyer_id']),
                    'order_by_columns' => "indval_annee ASC",
                );

                $request_id = array(
                    'clause' => array('indloyer_id' => $data_post['indloyer_id']),
                );

                $indval_id_table = $this->indice->get($request_id);

                $filterCallback = function ($indice_valeur) {
                    return $indice_valeur->indval_valide != 1;
                };
                $filteredArray = array_filter($indval_id_table, $filterCallback);

                $data['first_indval_id'] = NULL;

                if (!empty($filteredArray)) {
                    $reorderedArray = array_values($filteredArray);
                    $data['first_indval_id'] = $reorderedArray[0]->indval_id;
                }

                $data["indloyer_id"] = $data_post['indloyer_id'];
                $valeur = $this->indice->get($params);

                $valeur_indice = array();
                foreach ($valeur as $key => $value) {
                    $params_lot_index = array(
                        'clause' => array('indval_id' => $value->indval_id),
                    );
                    $lot_index = $this->LotIndexe_m->get($params_lot_index);
                    $countlot_index = !empty($lot_index) ? count($lot_index) : 0;
                    $array_valeur = array(
                        'indval_id' => $value->indval_id,
                        'indval_libelle' => $value->indval_libelle,
                        'indval_valeur' => $value->indval_valeur,
                        'indval_annee' => $value->indval_annee,
                        'indval_trimestre' => $value->indval_trimestre,
                        'indval_date_parution' => $value->indval_date_parution,
                        'indval_valide' => $value->indval_valide,
                        'util_prenom' => $value->util_prenom,
                        'indval_date_registre' => $value->indval_date_registre,
                        'util_prenom_valide' => $value->util_prenom_valide,
                        'indval_date_validation' => $value->indval_date_validation,
                        'util_prenom_publie' => $value->util_prenom_publie,
                        'indval_date_publie' => $value->indval_date_publie,
                        'indval_date_generation' => $value->indval_date_generation,
                        'count_indexed' => $countlot_index,
                    );
                    array_push($valeur_indice, $array_valeur);
                }
                $data['valeur'] = $valeur_indice;

                $this->renderComponant("Setting/indexation/liste-valeur", $data);
            }
        }
    }

    public function AlertAjoutValeur()
    {
        if (is_ajax()) {
            $this->renderComponant("Setting/indexation/form-alert");
        }
    }

    public function getFormValeur()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndiceValeur_m', 'indice');
                $this->load->model('IndiceLoyer_m');
                $data_post = $_POST['data'];

                $data['indloyer_id'] = $data_post['indloyer_id'];

                $request_indice = array(
                    'columns' => array('indloyer_id', 'indloyer_periode'),
                    'clause' => array(
                        'indloyer_id' => $data_post['indloyer_id'],
                    ),
                    'method' => 'row'
                );

                $data['indice'] = $this->IndiceLoyer_m->get($request_indice);

                /***Valeur max***/

                $request_valeur = array(
                    'columns' => array('indval_id', 'indval_annee', 'indval_trimestre'),
                    'clause' => array(
                        'indloyer_id' => $data_post['indloyer_id'],
                    ),
                    'order_by_columns' => "indval_annee DESC",
                    // 'limit' => 1,
                    'method' => 'result'
                );

                $data['valeur_annee'] = $this->indice->get($request_valeur);

                $items = array();

                if (!empty($data['valeur_annee'])) {
                    foreach ($data['valeur_annee'] as $key => $value) {
                        $items[] = array(
                            'annee' => $value->indval_annee,
                            'trimestre' => $value->indval_trimestre
                        );
                    }

                    $data['valeur_max'] = max($items);
                }

                /********Valeur dernière ajoutée ********/

                $request_value = array(
                    'clause' => array(
                        'c_indice_valeur_loyer.indloyer_id' => $data_post['indloyer_id'], 'indval_valide' => 1
                    ),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_indice_valeur_loyer.indloyer_id'
                    ),
                    'order_by_columns' => "indval_annee, indval_libelle ASC",
                );

                $data_indice = $this->indice->get($request_value);

                if (!empty($data_indice)) {
                    $count_data_indice = count($data_indice);
                    $data['last_value'] = $data_indice[$count_data_indice - 1];
                }

                /*****Update*****/

                if ($data_post['action'] == "edit") {
                    $this->load->model('IndiceValeur_m', 'indice');
                    $params = array(
                        'clause' => array('indval_id ' => $data_post['indval_id']),
                        'method' => "row"
                    );
                    $data['valeur'] = $this->indice->get($params);
                    $data['indval_id'] = $data_post['indval_id'];
                }
                $data['action'] = $data_post['action'];

                $this->renderComponant("Setting/indexation/form-valeur", $data);
            }
        }
    }

    function cruValeur()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceValeur_m');
            $this->load->model('IndiceLoyer_m');
            $retour = retour(false, "error", 0, array("message" => "Error"));
            $data = format_data($_POST['data']);
            $action = $data['action'];

            $clause_indice = array(
                'clause' => array('indloyer_id' => $data["indloyer_id"]),
                'method' => 'row'
            );

            $indice = $this->IndiceLoyer_m->get($clause_indice);

            $postdataAnnuel = array(
                "indval_libelle" => $indice->indloyer_description,
                "indval_valeur" => $data["indval_valeur"],
                "indval_annee" => $data["indval_annee"],
                "indval_date_parution" => $data["indval_date_parution"],
                "util_prenom_publie" => $_SESSION['session_utilisateur']['util_prenom'],
                "indval_date_publie" => date('Y-m-d'),
                "indloyer_id" => $data["indloyer_id"],
                "indval_valide" => 3
            );

            $postdataTrimestre = array(
                "indval_libelle" => $data["indval_trimestre"] . ' ' . $data["indval_annee"],
                "indval_valeur" => $data["indval_valeur"],
                "indval_annee" => $data["indval_annee"],
                "indval_trimestre" => $data["indval_trimestre"],
                "indval_date_parution" => $data["indval_date_parution"],
                "util_prenom_publie" => $_SESSION['session_utilisateur']['util_prenom'],
                "indval_date_publie" => date('Y-m-d'),
                "indloyer_id" => $data["indloyer_id"],
                "indval_valide" => 3
            );

            if ($data["indval_date_parution"] === "" || $data['indval_valeur'] == 0) {
                $retour = retour(false, "error", 0, array("message" => "Vous devez renseigner la valeur et la date de parution"));
                echo json_encode($retour);
                return false;
            }

            $clause = array("indloyer_id" => $data["indloyer_id"]);
            $id = null;

            if ($data['action'] === 'edit') {
                $clause = array("indloyer_id" => $data["indloyer_id"], "indval_id" => $data["indval_id"]);
                $id = array("indval_id" => $data["indval_id"]);
                //$request = $this->IndiceValeur_m->save_data($postdata, $id);
            }

            $controlRequest = (intval($data['type_periode']) == 1) ? 'annuel' : 'trimestre';

            $request = ($controlRequest == "annuel") ? $this->IndiceValeur_m->save_data($postdataAnnuel, $id) : $this->IndiceValeur_m->save_data($postdataTrimestre, $id);

            if (!empty($request)) {
                $retour = retour(true, "success", $clause, array("message" => ($action == "add") ? "Ajout avec succès" : "Modification réussie"));
            }

            echo json_encode($retour);
        }
    }

    public function removeValeurIndice()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('IndiceValeur_m', 'indice');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('indval_id' => $data['indval_id']),
                    'columns' => array(
                        'indval_id',
                        'indval_libelle',
                        'indval_valeur',
                        'indval_annee',
                        'indval_trimestre',
                        'indval_date_parution',
                        'indloyer_id'
                    ),
                    'method' => "row",
                );
                $request_get = $this->indice->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['indval_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer la valeur de l'indice de libellé " . $request_get->indval_libelle . "?",
                            'btnConfirm' => "Supprimer la valeur",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('indval_id' => $data['indval_id']);
                    $request = $this->indice->delete_data($clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['indval_id']),
                            'message' => "La valeur de l'indice " . $request_get->indval_libelle . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function updateIndice()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceLoyer_m');
            $retour = retour(false, "error", 0, array("message" => "Error"));
            $data = format_data($_POST['data']);

            // $postdata = array(
            //     "indloyer_description" => $data["indloyer_description"],
            // );

            $postdataAnnuel = array(
                "indloyer_description" => $data["indloyer_description"],
                "indloyer_periode" => $data["indloyer_periode"],
                "indloyer_date_saisie_an_jour" => $data["indloyer_date_saisie_an_jour"],
                "indloyer_date_saisie_an_mois" => $data["indloyer_date_saisie_an_mois"],
            );

            $postdataTrimestre = array(
                "indloyer_description" => $data["indloyer_description"],
                "indloyer_periode" => $data["indloyer_periode"],
                "indloyer_date_saisie_t1_jour" => $data["indloyer_date_saisie_t1_jour"],
                "indloyer_date_saisie_t1_mois" => $data["indloyer_date_saisie_t1_mois"],
                "indloyer_date_saisie_t2_jour" => $data["indloyer_date_saisie_t2_jour"],
                "indloyer_date_saisie_t2_mois" => $data["indloyer_date_saisie_t2_mois"],
                "indloyer_date_saisie_t3_jour" => $data["indloyer_date_saisie_t3_jour"],
                "indloyer_date_saisie_t3_mois" => $data["indloyer_date_saisie_t3_mois"],
                "indloyer_date_saisie_t4_jour" => $data["indloyer_date_saisie_t4_jour"],
                "indloyer_date_saisie_t4_mois" => $data["indloyer_date_saisie_t4_mois"],
            );

            $clause = array("indloyer_id" => $data["indloyer_id"]);
            $id = array("indloyer_id" => $data["indloyer_id"]);
            $controlRequest = $data['indloyer_periode'] == 'Annuelle' ? 'Annuelle' : 'Trimestrielle';

            $request = ($controlRequest == "Annuelle") ? $this->IndiceLoyer_m->save_data($postdataAnnuel, $clause) : $this->IndiceLoyer_m->save_data($postdataTrimestre, $clause);
            if (!empty($request)) {
                $retour = retour(true, "success", $clause, array("message" => "modification réussie avec succès."));
            }
            echo json_encode($retour);
        }
    }

    public function ListLot()
    {
        if (is_ajax()) {

            $this->load->model('LotIndexe_m');
            $data_post = $_POST['data'];

            $params_lot_index = array(
                'clause' => array('c_lot_indexe.indval_id' => $data_post['indval_id']),
                'join' => array(
                    'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id = c_lot_indexe.indval_id',
                    'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_indice_valeur_loyer.indloyer_id',
                    'c_bail' => 'c_bail.bail_id = c_lot_indexe.bail_id',
                    'c_lot_client' => 'c_bail.cliLot_id = c_lot_client.cliLot_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_client' => 'c_client.client_id = c_lot_client.cli_id'
                ),
            );

            $data['lot_index'] = $this->LotIndexe_m->get($params_lot_index);

            $this->renderComponant("Setting/indexation/form-list-lot", $data);
        }
    }

    public function PopupNewValeur()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceValeur_m');
            $this->load->model('IndiceLoyer_m');
            $data_post = $_POST['data'];

            $clause_indice = array(
                'clause' => array('c_indice_loyer.indloyer_id' => $data_post["indloyer_id"]),
                'method' => 'row'
            );

            $indice = $this->IndiceLoyer_m->get($clause_indice);
            $non_indice = $indice->indloyer_description;
            $data['indloyer_id'] = $data_post["indloyer_id"];

            if ($indice->indloyer_periode == 'Trimestrielle') {
                $data['message'] = 'Vous allez créer 4 nouvelles valeurs d\'indice pour les 4 prochains trimestre pour le type "' . $non_indice . '", ces indices seront dans l\'état "ajouté", c\'est-à-dire "non publié". Voulez-vous continuer ?';
            } else {
                $data['message'] = 'Vous allez créer une nouvelle valeur d\'indice annuel pour le type "' . $non_indice . '", cet indice sera dans l\'état "ajouté", c\'est-à-dire "non publié", voulez-vous continuer ?';
            }
            $this->renderComponant("Setting/indexation/form-popup-indice_valeur", $data);
        }
    }

    public function addIndiceVleur()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('IndiceValeur_m');
            $this->load->model('IndiceLoyer_m');
            $data_post = $_POST['data'];
            $data_retour = $data_post;

            $clause_indice = array(
                'clause' => array('c_indice_loyer.indloyer_id' => $data_post["indloyer_id"]),
                'method' => 'row'
            );

            $indice = $this->IndiceLoyer_m->get($clause_indice);

            // Trimestrielle
            if ($indice->indloyer_periode == 'Trimestrielle') {
                $clause_indice_valeur = array(
                    'clause' => array('c_indice_valeur_loyer.indloyer_id' => $data_post["indloyer_id"]),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_valeur_loyer.indloyer_id = c_indice_loyer.indloyer_id'
                    ),
                );

                $indice_valeur = $this->IndiceValeur_m->get($clause_indice_valeur);

                $maxValue = null;

                foreach ($indice_valeur as $row) {
                    $value = $row->indval_annee;
                    if ($value >= $maxValue) {
                        $maxValue = $value;
                        $maxRow = $row;
                    }
                }

                switch ($maxRow->indval_trimestre) {
                    case 'T1':
                        $indval_trimestre_table = ['T2', 'T3', 'T4'];

                        foreach ($indval_trimestre_table as $key => $value) {
                            $data = array(
                                'indval_libelle' => $value . ' ' . ($maxRow->indval_annee),
                                'indval_annee' => ($maxRow->indval_annee),
                                'indval_trimestre' => $value,
                                'indval_valide' => 2,
                                'indloyer_id' => $indice->indloyer_id,
                                'indval_date_registre' => date('Y-m-d'),
                                'util_prenom' => $_SESSION['session_utilisateur']['util_prenom']
                            );
                            $request = $this->IndiceValeur_m->save_data($data);
                        }
                        break;

                    case 'T2':
                        $indval_trimestre_table = ['T3', 'T4'];

                        foreach ($indval_trimestre_table as $key => $value) {
                            $data = array(
                                'indval_libelle' => $value . ' ' . ($maxRow->indval_annee),
                                'indval_annee' => ($maxRow->indval_annee),
                                'indval_trimestre' => $value,
                                'indval_valide' => 2,
                                'indloyer_id' => $indice->indloyer_id,
                                'indval_date_registre' => date('Y-m-d'),
                                'util_prenom' => $_SESSION['session_utilisateur']['util_prenom']
                            );
                            $request = $this->IndiceValeur_m->save_data($data);
                        }
                        break;

                    case 'T3':
                        $indval_trimestre_table = ['T4'];

                        foreach ($indval_trimestre_table as $key => $value) {
                            $data = array(
                                'indval_libelle' => $value . ' ' . ($maxRow->indval_annee),
                                'indval_annee' => ($maxRow->indval_annee),
                                'indval_trimestre' => $value,
                                'indval_valide' => 2,
                                'indloyer_id' => $indice->indloyer_id,
                                'indval_date_registre' => date('Y-m-d'),
                                'util_prenom' => $_SESSION['session_utilisateur']['util_prenom']
                            );
                            $request = $this->IndiceValeur_m->save_data($data);
                        }
                        break;
                    default:
                        $indval_trimestre_table = ['T1', 'T2', 'T3', 'T4'];

                        foreach ($indval_trimestre_table as $key => $value) {
                            $annee_suivant =  intval($maxRow->indval_annee) + 1;
                            $data = array(
                                'indval_libelle' => $value . ' ' . $annee_suivant,
                                'indval_annee' => $annee_suivant,
                                'indval_trimestre' => $value,
                                'indval_valide' => 2,
                                'indloyer_id' => $indice->indloyer_id,
                                'indval_date_registre' => date('Y-m-d'),
                                'util_prenom' => $_SESSION['session_utilisateur']['util_prenom']
                            );
                            $request = $this->IndiceValeur_m->save_data($data);
                        }
                        break;
                }
                $retour = retour(false, "error", array("message" => "Echec de l'enregistrement", 'data' => $data_retour));
                if (!empty($request)) {
                    $retour = retour(true, "success", array("message" => "Enregistrement réussie", 'data' => $data_retour));
                }
                echo json_encode($retour);
            }
            // Annuelle 
            else {
                $clause_indice_valeur = array(
                    'clause' => array('c_indice_valeur_loyer.indloyer_id' => $data_post["indloyer_id"]),
                    'join' => array(
                        'c_indice_loyer' => 'c_indice_valeur_loyer.indloyer_id = c_indice_loyer.indloyer_id'
                    ),
                );

                $indice_valeur = $this->IndiceValeur_m->get($clause_indice_valeur);

                $maxValue = null;

                foreach ($indice_valeur as $row) {
                    $value = $row->indval_annee;
                    if ($value >= $maxValue) {
                        $maxValue = $value;
                        $maxRow = $row;
                    }
                }
                $data = array(
                    'indval_libelle' => $indice->indloyer_description,
                    'indval_annee' => ($maxRow->indval_annee) + 1,
                    'indval_valide' => 2,
                    'indloyer_id' => $indice->indloyer_id,
                    'indval_date_registre' => date('Y-m-d'),
                    'util_prenom' => $_SESSION['session_utilisateur']['util_prenom']
                );

                $request = $this->IndiceValeur_m->save_data($data);

                $retour = retour(false, "error", array("message" => "Echec de l'enregistrement", 'data' => $data_retour));
                if (!empty($request)) {
                    $retour = retour(true, "success", array("message" => "Enregistrement réussie", 'data' => $data_retour));
                }
                echo json_encode($retour);
            }
        }
    }
}
