<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Programme extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/programme.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-programme";
        $this->_data['sous_module'] = "programme" . DIRECTORY_SEPARATOR . "contenaire-programme";
        $this->render('contenaire');
    }

    // get liste programme

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/programme/header-programme", $data);
            }
        }
    }

    public function getListProgramme()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('progm_etat' => 1),
                    'order_by_columns' => "progm_nom ASC",
                );
                $this->load->model('Program_m', 'programme');
                $data['programme'] = $this->programme->get($params);
                $this->renderComponant("Setting/programme/liste-programme", $data);
            }
        }
    }

    public function getFormProgramme()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentProgm_m');
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Program_m', 'programme');
                    $data['programme'] = $this->programme->get(
                        array('clause' => array("progm_id" => $post['progm_id']), 'method' => "row")
                    );
                    $data['document'] = $this->DocumentProgm_m->get(array(
                        'clause' => array("progm_id" => $post['progm_id'], "doc_progm_etat" => 1),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_prgm.util_id')
                    ));
                } else {
                    $this->load->model('Program_m', 'programme');

                    $last_id = $this->programme->get(
                        array(
                            'clause' => array(),
                            'order_by_columns' => "progm_id  DESC",
                            'method' => "row"
                        )
                    );

                    $data['last_id'] = $last_id->progm_id;
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/programme/form-programme", $data);
            }
        }
    }

    public function cruProgram()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Program_m', 'programme');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    $data['progm_pourcentage_evaluation'] = str_replace(',', '.', ($data['progm_pourcentage_evaluation']));

                    if ($data['action'] == "edit") {
                        $clause = array("progm_id" => $data['progm_id']);
                        unset($data['progm_id']);
                    } else {
                        $data['progm_date_creation'] = Carbon::now()->toDateTimeString();
                        $data['progm_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->programme->save_data($data, $clause);
                    if (!empty($request)) {
                        $message = array("message" => ($action == "add") ?
                            "Programme enregistré avec succès" : "La modification réussie avec succès.");
                        $message['liste'] = array(
                            'id' => $request,
                            'libelle' => $data['progm_nom']
                        );
                        $retour = retour(true, "success", $request, $message);
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeProgramme()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Program_m', 'programme');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('progm_id' => $data['progm_id']),
                    'columns' => array('progm_id', 'progm_num', 'progm_nom'),
                    'method' => "row",
                );
                $request_get = $this->programme->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['progm_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le programme " . $request_get->progm_nom . " " . ((trim($request_get->progm_num) != "") ? "[ N°: " . $request_get->progm_num . " ]" : ''),
                            'btnConfirm' => "Supprimer le programme",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('progm_id' => $data['progm_id']);
                    $data_update = array('progm_etat' => 0);
                    $request = $this->programme->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['progm_id']),
                            'message' => "Le programme " . $request_get->progm_nom . " " . ((trim($request_get->progm_num) != "") ? "[ N°: " . $request_get->progm_num . " ]" : '') . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
    public function verifierNom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Program_m', 'programme');
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('progm_nom' => $data_post['nom']),
                );
                $check = $this->programme->get($params);
                echo json_encode($check);
            }
        }
    }

    public function formUploadProgrm()
    {
        $data_post = $_POST['data'];

        $data['progm_id'] = $data_post['progm_id'];
        $this->renderComponant("Setting/programme/formulaire_upload_file", $data);
    }

    public function uploadfileProgm()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $progm_id = $_POST['progm_id'];

        // Chemin du dossier
        $targetDir = "../documents/settings/programme/$progm_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_progm_nom' => $original_name,
                'doc_progm_creation' => date('Y-m-d H:i:s'),
                'doc_progm_path' => str_replace('\\', '/', $filePath),
                'doc_progm_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'progm_id' => $progm_id,
            );

            $this->load->model('DocumentProgm_m');
            $this->DocumentProgm_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function delete_docprogm()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentProgm_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_progm_id' => $data['doc_progm_id']),
                    'method' => "row",
                );
                $request_get = $this->DocumentProgm_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_progm_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_progm_id' => $data['doc_progm_id']);
                    $data_update = array('doc_progm_etat' => 0);
                    $request = $this->DocumentProgm_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_progm_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modalupdateDocProgm()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $this->load->model('DocumentProgm_m');
                $data['document'] = $this->DocumentProgm_m->get(
                    array('clause' => array("doc_progm_id" => $data_post['doc_progm_id']), 'method' => "row")
                );

                $this->renderComponant("Setting/programme/formulaire_update", $data);
            }
        }
    }

    public function UpdateDocumentProgm()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentProgm_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("doc_progm_id" => $data['doc_progm_id']);

                    $doc_progm_id = $data['doc_progm_id'];
                    unset($data['doc_progm_id']);

                    $clause = array("doc_progm_id" => $doc_progm_id);
                    $request = $this->DocumentProgm_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
