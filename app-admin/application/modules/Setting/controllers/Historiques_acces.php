<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
class Historiques_acces extends ADM_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        
	}
}