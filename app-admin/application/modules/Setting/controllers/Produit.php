<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
class Produit extends ADM_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/produit.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-produit";
        $this->_data['sous_module'] = "produit".DIRECTORY_SEPARATOR."contenaire-produit";
        $this->render('contenaire');
	}

     // get liste produit

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/produit/header-produit",$data);

            }
        }
    }

    public function getListProduit(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('produit_etat' => 1)
                );
                $this->load->model('Produit_m','produit');
                $data['produit'] = $this->produit->get($params);
                $this->renderComponant("Setting/produit/liste-produit",$data);

            }
        }
    }

    public function getFormProduit(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Produit_m','produit');
                    $data['produit'] = $this->produit->get(
                        array('clause' => array("produit_id" => $post['produit_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/produit/form-produit",$data);
            }
        }
    }

    public function cruProduit(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Produit_m','produit');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("produit_id" => $data['produit_id']);
                        unset($data['produit_id']);
                    }else{
                        $data['produit_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->produit->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action=="add") ? "L'information du nouveau produit a été enregistrée avec succès" : "La modification a été réussi avec succès."));
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeProduit(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…", 
                );
                $this->load->model('Produit_m','produit');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('produit_id' => $data['produit_id']),
                    'columns' => array('produit_id','produit_libelle'),
                    'method' => "row",
                );
                $request_get = $this->produit->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['produit_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le produit ".$request_get->produit_libelle,
                            'btnConfirm' => "Supprimer le produit",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('produit_id' => $data['produit_id']);
                    $data_update = array('produit_etat' => 0);
                    $request = $this->produit->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['produit_id']),
                            'message' => "Le produit ".$request_get->produit_libelle." a bien été supprimé",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}