<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cgp extends ADM_Controller {
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/cgp.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-cgp";
        $this->_data['sous_module'] = "cgp".DIRECTORY_SEPARATOR."contenaire-cgp";
        $this->render('contenaire');
	}

     // get liste cgp

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/cgp/header-cgp",$data);

            }
        }
    }

    public function getListCgp(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('cgp_etat' => 1)
                );
                $this->load->model('Cgp_m');
                $data['cgp'] = $this->Cgp_m->get($params);
                $this->renderComponant("Setting/cgp/liste-cgp",$data);

            }
        }
    }

    public function getFormCgp(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Cgp_m');
                    $data['cgp'] = $this->Cgp_m->get(
                        array('clause' => array("cgp_id" => $post['cgp_id']), 'method' => "row")
                    );
                }else{
                    $this->load->model('Cgp_m');
                   $last_id = $this->Cgp_m->get(
                        array(
                            'clause' => array(),
                            'order_by_columns' => "cgp_id  DESC", 
                            'method' => "row"
                          )
                    );

                    $data['last_id'] = $last_id->cgp_id;
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/cgp/form-cgp",$data);
            }
        }
    }

    public function cruCgp(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Cgp_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("cgp_id" => $data['cgp_id']);
                        unset($data['cgp_id']);
                    }else{
                        $data['cgp_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->Cgp_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $message = array("message" => ($action=="add") ? 
                        "CGP enregistré avec succès" : "La modification réussie avec succès.");
                        $message['liste'] = array(
                            'id' => $request,
                            'libelle' => $data['cgp_nom'],
                            'email' =>$data['cgp_email'],
                            'tel' =>$data['cgp_tel'],
                            'adresse1' =>$data['cgp_adresse1'],
                            'adresse2' =>$data['cgp_adresse2'],
                            'adresse3' =>$data['cgp_adresse3'],
                            'cp' =>$data['cgp_cp'],
                            'ville' =>$data['cgp_ville'],
                            'pays' =>$data['cgp_pays']
                        );
                        $retour = retour(true, "success", $request,$message);
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeCgp(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…", 
                );
                $this->load->model('Cgp_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cgp_id' => $data['cgp_id']),
                    'columns' => array('cgp_id','cgp_nom'),
                    'method' => "row",
                );
                $request_get = $this->Cgp_m->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cgp_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le CGP ".$request_get->cgp_nom." "."?",
                            'btnConfirm' => "Supprimer le CGP",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('cgp_id' => $data['cgp_id']);
                    $data_update = array('cgp_etat' => 0);
                    $request = $this->Cgp_m->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cgp_id']),
                            'message' => "Le CGP ".$request_get->cgp_nom." "."a bien été supprimé",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}