<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Gestionnaire extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/gestionnaire.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-gestionnaire";
        $this->_data['sous_module'] = "gestionnaire" . DIRECTORY_SEPARATOR . "contenaire-gestionnaire";
        $this->render('contenaire');
    }

    // get liste gestionnaire

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/gestionnaire/header-gestionnaire", $data);
            }
        }
    }

    public function getListGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('gestionnaire_etat' => 1),
                    'order_by_columns' => "gestionnaire_nom ASC",
                );
                $this->load->model('Gestionnaire_m', 'gestionnaire');
                $data['gestionnaire'] = $this->gestionnaire->get($params);
                $this->renderComponant("Setting/gestionnaire/liste-gestionnaire", $data);
            }
        }
    }

    public function getFormGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();

                $this->load->model('TypeContactGestionnaire_m', 'type_contact');


                if ($post['action'] != "add") {
                    $this->load->model('Gestionnaire_m', 'gestionnaire');
                    $data['gestionnaire'] = $this->gestionnaire->get(
                        array('clause' => array("gestionnaire_id" => $post['gestionnaire_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $data['type_contact'] = $this->type_contact->get(array());


                $this->renderComponant("Setting/gestionnaire/form-gestionnaire", $data);
            }
        }
    }

    public function formUpdateGestionnaire()
    {
        $this->load->model('Gestionnaire_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];

                $data['gestionnaire'] = $this->Gestionnaire_m->get(
                    array('clause' => array("gestionnaire_id" => $data_post['gestionnaire_id']), 'method' => "row")
                );

                $this->renderComponant("Setting/gestionnaire/fiche-gestionnaire", $data);
            }
        }
    }

    public function pageficheGestionnaire()
    {
        $this->load->model('Gestionnaire_m');
        $this->load->model('DocumentGestionnaire_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array('gestionnaire_id' => $data_post['gestionnaire_id']),
                    'method' => "row"
                );

                $data['gestionnaire'] = $this->Gestionnaire_m->get($params);

                $data['document'] = $this->DocumentGestionnaire_m->get(
                    array(
                        'clause' => array("gestionnaire_id" => $data_post['gestionnaire_id'], "doc_gest_etat" => 1),
                        'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_gestionnaire.util_id')
                    )
                );

                if ($data_post['page'] == "form") {
                    $this->renderComponant("Setting/gestionnaire/form-update-identification", $data);
                } else {
                    $this->renderComponant("Setting/gestionnaire/fiche-identification", $data);
                }
            }
        }
    }

    public function cruGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Gestionnaire_m', 'gestionnaire');
                $this->load->model('ContactGestionnaire_m', 'contact');

                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_gestionnaire = null;
                    $clause = null;
                    if ($data['action'] == "edit") {
                        $clause = array("gestionnaire_id" => $data['gestionnaire_id']);
                        unset($data['gestionnaire_id']);
                    } else {
                        $data['gestionnaire_date_creation'] = Carbon::now()->toDateTimeString();
                        $data['gestionnaire_etat'] = 1;
                        $data_gestionnaire['gestionnaire_date_creation'] = Carbon::now()->toDateTimeString();
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    // Data gestionnaire to save
                    $data_gestionnaire["gestionnaire_nom"] = $data['gestionnaire_nom'];
                    $data_gestionnaire["gestionnaire_adresse1"] = $data['gestionnaire_adresse1'];
                    $data_gestionnaire["gestionnaire_adresse2"] = $data['gestionnaire_adresse2'];
                    $data_gestionnaire["gestionnaire_adresse3"] = $data['gestionnaire_adresse3'];
                    $data_gestionnaire["gestionnaire_cp"] = $data['gestionnaire_cp'];
                    $data_gestionnaire["gestionnaire_ville"] = $data['gestionnaire_ville'];
                    $data_gestionnaire["gestionnaire_pays"] = $data['gestionnaire_pays'];
                    $data_gestionnaire['gestionnaire_etat'] = $data['gestionnaire_etat'];


                    // Data contact to save
                    $clause_data_contact = null;
                    $data_contact["cnctGestionanire_nom"] = $data['cnctGestionanire_nom'];
                    $data_contact["cnctGestionnaire_prenom"] = $data['cnctGestionnaire_prenom'];
                    $data_contact["cnctGestionnaire_fonction"] = $data['cnctGestionnaire_fonction'];
                    $data_contact["cnctGestionnaire_service"] = $data['cnctGestionnaire_service'];
                    $data_contact["cnctGestionnaire_email1"] = $data['cnctGestionnaire_email1'];
                    $data_contact["cnctGestionnaire_email2"] = $data['cnctGestionnaire_email2'];
                    $data_contact["cnctGestionnaire_tel1"] = $data['cnctGestionnaire_tel1'];
                    $data_contact["cnctGestionnaire_tel2"] = $data['cnctGestionnaire_tel2'];
                    $data_contact["type_cont_id"] = $data['type_cont_id'];
                    $data_contact["etat_contact"] = 1;

                    $request = $this->gestionnaire->save_data($data_gestionnaire, $clause);
                    $insert_id = $this->db->insert_id();

                    $data_contact["gestionnaire_id"] = $insert_id;
                    $this->contact->save_data($data_contact, $clause_data_contact);

                    if (!empty($request)) {
                        $message = array(
                            "message" => ($action == "add") ?
                                "L'information du nouveau gestionnaire a été enregistrée avec succès" : "La modification a été réussi avec succès."
                        );
                        $message['liste'] = array(
                            'id' => $request,
                            'libelle' => $data['gestionnaire_nom']
                        );
                        $retour = retour(true, "success", $request, $message);
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function updateInfoGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Gestionnaire_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $gestionnaire_id = $data['gestionnaire_id'];
                    //    $data['client_date_creation'] = date_sql($data['client_date_creation'],'-','jj/MM/aaaa');
                    unset($data['prospect_id']);

                    $clause = array("gestionnaire_id" => $gestionnaire_id);
                    $request = $this->Gestionnaire_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeGestionnaire()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Gestionnaire_m', 'gestionnaire');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('gestionnaire_id' => $data['gestionnaire_id']),
                    'columns' => array('gestionnaire_id', 'gestionnaire_nom'),
                    'method' => "row",
                );
                $request_get = $this->gestionnaire->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['gestionnaire_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le gestionnaire " . $request_get->gestionnaire_nom,
                            'btnConfirm' => "Supprimer le gestionnaire",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('gestionnaire_id' => $data['gestionnaire_id']);
                    $data_update = array('gestionnaire_etat' => 0);
                    $request = $this->gestionnaire->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['gestionnaire_id']),
                            'message' => "Le gestionnaire " . $request_get->gestionnaire_nom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function listeContact()
    {
        $this->load->model('ContactGestionnaire_m', 'contact');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'clause' => array(
                        'gestionnaire_id' => $data_post['gestionnaire_id'],
                        'etat_contact' => 1
                    ),
                    'join' => array(
                        'c_type_contact_gestionnaire' => 'c_type_contact_gestionnaire.type_cont_id = c_contactgestionnaire.type_cont_id'
                    )
                );

                $data["gestionnaire_id"] = $data_post['gestionnaire_id'];
                $data['contact'] = $this->contact->get($params);

                $this->renderComponant("Setting/gestionnaire/liste-contact", $data);
            }
        }
    }

    public function getFormContact()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $this->load->model('TypeContactGestionnaire_m', 'type_contact');

                $data['gestionnaire_id'] = $data_post['gestionnaire_id'];
                if ($data_post['action'] == "edit") {
                    $this->load->model('ContactGestionnaire_m', 'contact');
                    $params = array(
                        'clause' => array('cnctGestionnaire_id ' => $data_post['cnctGestionnaire_id']),
                        'method' => "row",
                        'join' => array(
                            'c_type_contact_gestionnaire' => 'c_type_contact_gestionnaire.type_cont_id = c_contactgestionnaire.type_cont_id'
                        )
                    );
                    $data['contact'] = $request = $this->contact->get($params);
                }
                $data['action'] = $data_post['action'];

                $data['type_contact'] = $this->type_contact->get(array());

                $this->renderComponant("Setting/gestionnaire/form-contact", $data);
            }
        }
    }

    function cruContact()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('ContactGestionnaire_m', 'contact');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("gestionnaire_id" => $data['gestionnaire_id'], "action" => $action);
                    $data['etat_contact'] = 1;
                    unset($data['action']);
                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("cnctGestionnaire_id" => $data['cnctGestionnaire_id']);
                        $data_retour['cnctGestionnaire_id'] = $data['cnctGestionnaire_id'];
                        unset($data['cnctGestionnaire_id']);
                    }

                    $request = $this->contact->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Contact ajouté avec succès" : "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeContact()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('ContactGestionnaire_m', 'contact');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cnctGestionnaire_id' => $data['cnctGestionnaire_id']),
                    'columns' => array('cnctGestionnaire_id', 'cnctGestionanire_nom', 'cnctGestionnaire_prenom'),
                    'method' => "row",
                );
                $request_get = $this->contact->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cnctGestionnaire_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce contact ?",
                            'btnConfirm' => "Supprimer le contact",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cnctGestionnaire_id' => $data['cnctGestionnaire_id']);
                    $data_update = array('etat_contact' => 0);
                    $request = $this->contact->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['cnctGestionnaire_id']),
                            'message' => "Contact supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function listePourcentageProgm()
    {
        $this->load->model('ClientLot_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('c_programme.progm_id', 'progm_nom', 'progm_pourcentage_evaluation'),
                    'clause' => array(
                        'c_gestionnaire.gestionnaire_id' => $data_post['gestionnaire_id'],
                        'c_lot_client.cliLot_etat' => 1
                    ),
                    'distinct' => TRUE,
                    'join' => array(
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    )
                );

                $data['programme'] = $this->ClientLot_m->get($params);
                $data["gestionnaire_id"] = $data_post['gestionnaire_id'];

                $this->renderComponant("Setting/gestionnaire/list-pourcentagePgm", $data);
            }
        }
    }

    public function FormPourcentageProgm()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Program_m');
                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'progm_id' => $data_post['progm_id'],
                    ),
                    'method' => 'row'
                );

                $data['programme'] = $this->Program_m->get($params);
                $data['gestionnaire_id'] = $data_post['gestionnaire_id'];

                $this->renderComponant("Setting/gestionnaire/form-update-pourcentagePgm", $data);
            }
        }
    }

    public function UpdatePourcentage()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Program_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("gestionnaire_id" => $data['gestionnaire_id']);

                    $data['progm_pourcentage_evaluation'] = str_replace(',', '.', ($data['progm_pourcentage_evaluation']));

                    $clause = array("progm_id" => $data['progm_id']);
                    unset($data['progm_id']);
                    unset($data['gestionnaire_id']);

                    $request = $this->Program_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification réussie"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateAllPourcentage()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Program_m');
                $data_post = $_POST["data"];
                $data['gestionnaire_id'] = $data_post['gestionnaire_id'];
                $this->renderComponant("Setting/gestionnaire/form-updateAll-pourcentage", $data);
            }
        }
    }

    public function UpdateDataPourcentage()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Program_m');
                $retour = retour(null, null, null, array("message" => "Erreur de modification"));
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('c_programme.progm_id','progm_nom','progm_pourcentage_evaluation'),
                    'clause' => array(
                        'c_gestionnaire.gestionnaire_id' => $data_post['gestionnaire_id'],
                        'c_lot_client.cliLot_etat' => 1
                    ),
                    'distinct' => TRUE,
                    'join' => array(
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id'
                    )
                );

                $this->load->model('ClientLot_m');
                $request = $this->ClientLot_m->get($params);
                $action = $data_post['action'];

                if(intval($action) == 1){
                    $request = array_filter($request, function($item) use ($action) {
                        return $item->progm_pourcentage_evaluation == null;
                    });
                }

                $idProgrammes = array_map(function($item){
                    return $item->progm_id;
                }, $request);

                $data = array();
                foreach ($idProgrammes as $value) {
                    $data[] = array(
                        'progm_id' => $value,
                        'progm_pourcentage_evaluation' => $data_post['progm_pourcentage_evaluation']
                    );
                }

                $data_retour = array("gestionnaire_id" => $data_post['gestionnaire_id']);
                if(!empty($data)){
                    $requestUpdate = $this->Program_m->multi_update_data($data);
                    if (!empty($requestUpdate)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification réussie"));
                    }
                }else{
                    $retour = retour(true, "success", $data_retour, array("message" => "Tous les programme de ce gestionnaire ont tous déjà de valeur de pourcentage"));
                }
                echo json_encode($retour);
            }
        }
    }

    public function formUploadGestionnaire()
    {
        $data_post = $_POST['data'];

        $data['gestionnaire_id'] = $data_post['gestionnaire_id'];
        $this->renderComponant("Setting/gestionnaire/formulaire_upload_file", $data);
    }

    public function uploadfileGestionnaire()
    {
        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $gestionnaire_id = $_POST['gestionnaire_id'];

        // Chemin du dossier
        $targetDir = "../documents/settings/gestionnaire/$gestionnaire_id";

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            $data = array(
                'doc_gest_nom' => $original_name,
                'doc_gest_creation' => date('Y-m-d H:i:s'),
                'doc_gest_path' => str_replace('\\', '/', $filePath),
                'doc_gest_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'gestionnaire_id' => $gestionnaire_id,
            );

            $this->load->model('DocumentGestionnaire_m');
            $this->DocumentGestionnaire_m->save_data($data);
            echo json_encode($data);
        }
    }

    public function delete_docGest()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentGestionnaire_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('doc_gest_id' => $data['doc_gest_id']),
                    'method' => "row",
                );
                $request_get = $this->DocumentGestionnaire_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_gest_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_gest_id' => $data['doc_gest_id']);
                    $data_update = array('doc_gest_etat' => 0);
                    $request = $this->DocumentGestionnaire_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['doc_gest_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function modalupdateDocgest()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $this->load->model('DocumentGestionnaire_m');
                $data['document'] = $this->DocumentGestionnaire_m->get(
                    array('clause' => array("doc_gest_id" => $data_post['doc_gest_id']), 'method' => "row")
                );

                $this->renderComponant("Setting/gestionnaire/formulaire_update", $data);
            }
        }
    }

    public function UpdateDocumentGest()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentGestionnaire_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("doc_gest_id" => $data['doc_gest_id']);

                    $doc_gest_id = $data['doc_gest_id'];
                    unset($data['doc_gest_id']);

                    $clause = array("doc_gest_id" => $doc_gest_id);
                    $request = $this->DocumentGestionnaire_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
