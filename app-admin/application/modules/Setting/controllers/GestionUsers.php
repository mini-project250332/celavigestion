<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class GestionUsers extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/users.js',
            'js/mail/mail.js'
        );
        $this->_css_personnaliser = array(
            "css/setting/setting.css"
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "gestion-users";
        $this->_data['sous_module'] = "users" . DIRECTORY_SEPARATOR . "contenaire-users";
        $this->render('contenaire');
    }

    // get liste utilisateur

    public function getListUser()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m', 'users');
                $params = array(
                    'clause' => array('util_etat' => 1),
                    'columns' => array(
                        'util_id as id',
                        'util_nom as nom',
                        'util_prenom as prenom',
                        'util_phonemobile as mobile',
                        'util_phonefixe as fixe',
                        'util_mail as mail',
                        'util_adresse as adresse',
                        'util_cp as cp',
                        'util_ville as ville',
                        'util_photo as photo',
                        'util_status as status',
                        'util_datecreate as create',
                        'util_stamp as modifer',
                        'c_typeutilisateur.tutil_libelle as type_user',
                        'c_role_utilisateur.rol_util_libelle as rol_util_libelle'
                    ),
                    'join' => array(
                        'c_typeutilisateur' => 'c_typeutilisateur.tutil_id = c_utilisateur.tutil_id',
                        'c_role_utilisateur' => 'c_role_utilisateur.rol_util_id  = c_utilisateur.rol_util_id',
                    ),
                );
                $request = $this->users->get($params);
                $data["listUsers"] = $request;
                $this->renderComponant("Setting/users/liste-users", $data);
            }
        }
    }

    // get form utilisateur

    public function getFormUser()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Utilisateur_m', 'users');
                    $data['user'] = $this->users->get(
                        array('clause' => array("util_id" => $post['util_id']), 'method' => "row")
                    );
                }
                $params_typeUser = array(
                    'columns' => array('tutil_id as id', 'tutil_libelle as libelle', 'tutil_sigle as sigle')
                );

                // $this->load->model('Typeutilisateur_m', 'type_users');
                // $data['type_users'] = $this->type_users->get($params_typeUser);

                $this->load->model('RoleUtilisateur_m', 'role');
                $data['roles'] = $this->role->get(array());


                $data['action'] = $post['action'];
                $this->renderComponant("Setting/users/form-user", $data);
            }
        }
    }

    // Create and Update utitisateur function

    public function cruUser()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Utilisateur_m', 'users');
                $control = $this->control_data($_POST['data']);

                if ($_POST['data']['action']['value'] == "add") {
                    if (!isset($control['information']['util_login'])) {
                        $params_login = array('clause' => array("util_login" => strtolower(trim($_POST['data']['util_login']['value']))));
                        $request_login = $this->users->get($params_login);
                        if (!empty($request_login)) {
                            $control['return'] = false;
                            if (gettype($control['information']) == "array") {
                                $control['information']['util_login']['format'] = "Identifiant dèjà utilisé";
                            } else {
                                $control['information'] = array('util_login' => array('format' => "Identifiant dèjà utilisé"));
                            }
                        }
                    }
                }

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action'] == "edit") {

                        $clause = array("util_id" => $data['util_id']);
                        unset($data['util_id']);
                        if (isset($data['util_new_password']))
                            unset($data['util_new_password']);
                        if (isset($data['util_password'])) {
                            $data['util_password'] = hash('sha512', $data['util_password']);
                        }
                    } else {
                        $data['util_datecreate'] = Carbon::now()->toDateTimeString();
                        $data['util_password'] = hash('sha512', $data['util_password']);
                        $data['util_etat'] = 1;
                        $data['tutil_id'] = 1;
                    }
                    $data['util_datenaissance'] = date_sql($data['util_datenaissance'], '-', 'jj/MM/aaaa');
                    $action = $data['action'];
                    unset($data['action']);
                    $request = $this->users->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action == "add") ? "L'information du nouvel utilisateur a été enregistrée avec succès" : "La modification a été réussi avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getgeneratorPW()
    {
        $return = array(
            'status' => 200,
            'data' => encrypt_service(generatePassword()),
            'message' => ''
        );
        echo json_encode($return);
    }

    public function verifyLogin()
    {
        $this->load->model('Utilisateur_m', 'users');
        $login_post = decrypt_service($_POST["data"]);
        $params = array(
            'clause' => array("util_login" => strtolower(trim($login_post))),
        );
        $request = $this->users->get($params);
        $return = array(
            'status' => 200,
            'data' => encrypt_service(
                array(
                    "request" => !empty($request) ? false : true,
                    "mail" => trim($login_post),
                )
            ),
            'message' => !empty($request) ? 'Identifiant déjà utilisé' : ''
        );
        echo json_encode($return);
    }

    public function removeUser()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Utilisateur_m', 'users');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('util_id' => $data['util_id']),
                    'columns' => array('util_id', 'util_nom', 'util_prenom'),
                    'method' => "row",
                );
                $request_get = $this->users->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['util_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer le compte " . $request_get->util_nom . " " . $request_get->util_prenom,
                            'btnConfirm' => "Supprimer le compte",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('util_id' => $data['util_id']);
                    $data_update = array('util_etat' => 0);
                    $request = $this->users->save_data($data_update, $clause);
                    //$request = $this->users->delete_data($clause);;
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['util_id']),
                            'message' => "L'utilisateur " . $request_get->util_nom . " " . $request_get->util_prenom . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function test($variable = null)
    {
        echo $variable;
    }

    public function FormUserParamSmtp()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m', 'smpt_user');

                $data_post = $_POST["data"];

                $data['param'] = $this->smpt_user->get(
                    array('clause' => array("util_id" => $data_post['util_id']), 'method' => "row")
                );
                $data['util_id'] = $data_post['util_id'];
                $this->renderComponant("Setting/users/form-users-smtp", $data);
            }
        }
    }

    public function updateMailParam()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMailUsers_m');

                $data = $_POST;

                $data['mess_signature'] = NULL;

                if (!empty($_FILES)) {
                    $target_dir = "../documents/signature/";
                    $file = $_FILES['file_0']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file_0']['tmp_name'];
                    $path_filename_ext = $target_dir . $filename . "." . $ext;

                    $this->makeDirPath($target_dir);

                    $counter = 1;
                    while (file_exists($path_filename_ext)) {
                        $filename = $filename . '_' . $counter;
                        $path_filename_ext = $target_dir . $filename . "." . $ext;
                        $counter++;
                    }
                    if (!file_exists($path_filename_ext)) {
                        move_uploaded_file($temp_name, $path_filename_ext);
                    }

                    $data['mess_signature'] = $path_filename_ext;
                }

                if (!isset($data['mess_id'])) {
                    $data['mess_etat'] = 1;
                    $request = $this->ParamMailUsers_m->save_data($data);
                    $message = array("message" => "Enregistrement réussie avec succès.");
                } else {
                    $data['mess_etat'] = (isset($_POST['mess_etat']) && $_POST['mess_etat'] == 'on') ? 1 : 0;
                    $check_fic = $this->ParamMailUsers_m->get(array('clause' => array('mess_id' => $data['mess_id']), 'method' => 'row'));
                    if (empty($_FILES) && $check_fic->mess_signature != NULL) {
                        $data['mess_signature'] = $check_fic->mess_signature;
                    } elseif (!empty($_FILES)) {
                        $data['mess_signature'] = $path_filename_ext;
                    }
                    $mess_id = $data['mess_id'];
                    unset($data['mess_id']);
                    $clause = array("mess_id" => $mess_id);
                    $request = $this->ParamMailUsers_m->save_data($data, $clause);
                    $message = array("message" => "Modification réussie avec succès.");
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $clause, $message);
                }

                echo json_encode($retour);
            }
        }
    }

    private function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function test_mail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m');
                $data_post = $_POST;

                $params = array(
                    'clause' => array(
                        'c_utilisateur.util_id' => $data_post['util_id'],
                    ),
                    'join' => array('c_messagerie_paramserveur_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                    'join_orientation' => 'left',
                    'method' => 'row'
                );
                $emeteur = $this->Utilisateur_m->get($params);

                $data['emeteur_email'] = $data_post['email'];

                $data['emeteur'] = $emeteur->util_prenom . ' ' . $emeteur->util_nom . ' (' . $data_post['email'] . ')';

                $data['object'] = 'Test de la messagerie ' . $emeteur->util_prenom . ' ' . $emeteur->util_nom;
                $message = "Bonjour,\r\n\r\nCeci est un message de test à partir de CELAVIGestion, compte utilisateur " . $emeteur->util_prenom . ' ' . $emeteur->util_nom . ".";

                $data['message'] = strip_tags($message);

                if ($emeteur->mess_id != '') {
                    if ($emeteur->mess_signature != '') {
                        $data['mess_signature'] = $emeteur->mess_signature;
                    }
                } else {
                    if (!empty($_FILES)) {
                        $target_dir = "../documents/signature/";
                        $file = $_FILES['file']['name'];
                        $path = pathinfo($file);
                        $filename = $path['filename'];
                        $ext = $path['extension'];
                        $temp_name = $_FILES['file']['tmp_name'];
                        $path_filename_ext = $target_dir . $filename . "." . $ext;

                        $this->makeDirPath($target_dir);

                        if (!file_exists($path_filename_ext)) {
                            move_uploaded_file($temp_name, $path_filename_ext);
                        }

                        $data['mess_signature'] = $path_filename_ext;
                    }
                }

                $this->renderComponant("Setting/users/form-testmail", $data);
            }
        }
    }
}
