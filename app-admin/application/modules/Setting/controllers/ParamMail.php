<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
class ParamMail extends ADM_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/paramMail.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-mail";
        $this->_data['sous_module'] = "paramMail".DIRECTORY_SEPARATOR."contenaire-paramMail";
        $this->render('contenaire');
	}


    public function getListParamMail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                
                $this->load->model('ParamMail_m');
                $data = array();
                $param = array(
                    'clause' => array('mess_etat' =>1),
                    'columns' => array('mess_id','mess_libelle','mess_protocole','mess_description','mess_etat'),
                );
                $data['param'] = $this->ParamMail_m->get($param);

                $this->renderComponant("Setting/paramMail/liste-paramMail",$data);
            
            }

        }
    }

    public function formUpdate(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMail_m');

                $data_post = $_POST["data"];
                $data['param'] = $this->ParamMail_m->get(
                    array('clause' => array("mess_id" => $data_post['mess_id']), 'method' => "row")
                );
                $this->renderComponant("Setting/paramMail/form-param",$data);
            }
        }
    }

    function updateMailParam(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ParamMail_m');
 
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
 
                    $mess_id = $data['mess_id'];
                    unset($data['mess_id']);
                   
                    $clause = array("mess_id" => $mess_id);
                    $request = $this->ParamMail_m->save_data($data,$clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}