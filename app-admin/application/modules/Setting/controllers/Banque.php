<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banque extends ADM_Controller {
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/banque.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "banque";
        $this->_data['sous_module'] = "banque".DIRECTORY_SEPARATOR."contenaire-banque";
        $this->render('contenaire');
	}

     // get liste banque

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/banque/header-banque",$data);

            }
        }
    }

    public function listBanque(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('banque_etat' => 1)
                );
                $this->load->model('Banque_m');
                $data['banque'] = $this->Banque_m->get($params);
                $this->renderComponant("Setting/banque/liste-banque",$data);

            }
        }
    }

    public function getFormBanque(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Banque_m');
                    $data['banque'] = $this->Banque_m->get(
                        array('clause' => array("banque_id" => $post['banque_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/banque/form-banque",$data);
            }
        }
    }

    public function cruBanque(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Banque_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("banque_id" => $data['banque_id']);
                        unset($data['banque_id']);
                    }else{
                        $data['banque_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->Banque_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action=="add") ? "Enregistrement réussi" : "Modification réussie."));
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeBanque(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…", 
                );
                $this->load->model('Banque_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('banque_id' => $data['banque_id']),
                    'columns' => array('banque_id','banque_nom'),
                    'method' => "row",
                );
                $request_get = $this->Banque_m->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['banque_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer la banque ".$request_get->banque_nom." "."?",
                            'btnConfirm' => "Supprimer la banque",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('banque_id' => $data['banque_id']);
                    $data_update = array('banque_etat' => 0);
                    $request = $this->Banque_m->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['banque_id']),
                            'message' => "La Banque ".$request_get->banque_nom." "."a bien été supprimée",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

}