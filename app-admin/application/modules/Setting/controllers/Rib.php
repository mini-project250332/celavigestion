<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rib extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/rib.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-rib";
        $this->_data['sous_module'] = "rib" . DIRECTORY_SEPARATOR . "contenaire-rib";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/rib/header-rib", $data);
            }
        }
    }

    public function getListRib()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('rib_etat' => 1)
                );
                $this->load->model('Rib_m');
                $data['rib'] = $this->Rib_m->get($params);
                $this->renderComponant("Setting/rib/liste-rib", $data);
            }
        }
    }

    public function getFormRib()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('Rib_m');
                    $data['rib'] = $this->Rib_m->get(
                        array('clause' => array("rib_id" => $post['rib_id']), 'method' => "row")
                    );
                } else {
                    $this->load->model('Rib_m');
                    $last_id = $this->Rib_m->get(
                        array(
                            'clause' => array(),
                            'order_by_columns' => "rib_id  DESC",
                            'method' => "row"
                        )
                    );

                    $data['last_id'] = $last_id->rib_id;
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/rib/form-rib", $data);
            }
        }
    }

    public function cruRib()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Rib_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    $data['rib_iban'] = str_replace(' ', '', $data['rib_iban']);
                    if ($data['action'] == "edit") {
                        $clause = array("rib_id" => $data['rib_id']);
                        unset($data['rib_id']);
                    } else {
                        $data['rib_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->Rib_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $message = array("message" => ($action == "add") ?
                            "RIB enregistré avec succès" : "La modification réussie avec succès.");
                        $message['liste'] = array(
                            'id' => $request,
                            'rib_iban' => $data['rib_iban'],
                            'rib_bic' => $data['rib_bic'],
                        );
                        $retour = retour(true, "success", $request, $message);
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeRib(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…", 
                );
                $this->load->model('Rib_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('rib_id' => $data['rib_id']),
                    'columns' => array('rib_id'),
                    'method' => "row",
                );
                $request_get = $this->Rib_m->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['rib_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce RIB ?",
                            'btnConfirm' => "Supprimer le RIB",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('rib_id' => $data['rib_id']);
                    $data_update = array('rib_etat' => 0);
                    $request = $this->Rib_m->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['rib_id']),
                            'message' => "Le RIB a bien été supprimé",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function alert_rib_Iban()
    {
        if (is_ajax()) {
            $this->renderComponant("Setting/rib/form-alert_iban");
        }
    }
}
