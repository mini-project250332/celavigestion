<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Accueil extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/accueil.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-accueil";
        $this->_data['sous_module'] = "accueil" . DIRECTORY_SEPARATOR . "contenaire-accueil";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $this->renderComponant("Setting/accueil/header-accueil", $data);
            }
        }
    }

    public function getMessage()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Accueil_m', 'accueil');
                $params = array(
                    'method' => 'row'
                );
                $data['accueil'] = $this->accueil->get($params);
                $this->renderComponant("Setting/accueil/liste-accueil", $data);
            }
        }
    }

    public function updateMessage()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Accueil_m', 'accueil');
                $data_post = $_POST["data"];
                $data['model_accueil'] = $this->accueil->get(
                    array('clause' => array("mess_id" => $data_post['mess_id']), 'method' => "row")
                );
                $this->renderComponant("Setting/accueil/form-model", $data);
            }
        }
    }

    public function saveUpdateModel(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Accueil_m', 'accueil');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
 
                    $mess_id = $data['mess_id'];
                    unset($data['mess_id']);
                   
                    $clause = array("mess_id" => $mess_id);
                    $request = $this->accueil->save_data($data,$clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
