<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ics extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/ics.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-ics";
        $this->_data['sous_module'] = "ics" . DIRECTORY_SEPARATOR . "contenaire-ics";
        $this->render('contenaire');
    }

    function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/ics/header-ics", $data);
            }
        }
    }

    function getListIcs()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Ics_m');
                $data['ics'] = $this->Ics_m->get([]);

                $this->renderComponant("Setting/ics/liste-ics", $data);
            }
        }
    }

    function getFormIcs()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Ics_m');
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $data['ics'] = $this->Ics_m->get(
                        array('clause' => array("ics_id" => $post['ics_id']), 'method' => "row")
                    );
                } else {
                    $last_id = $this->Ics_m->get(
                        array(
                            'clause' => array(),
                            'order_by_columns' => "ics_id  DESC",
                            'method' => "row"
                        )
                    );

                    $data['last_id'] = $last_id->ics_id;
                }
                $data['action'] = $post['action'];

                $this->renderComponant("Setting/ics/form-ics", $data);
            }
        }
    }

    public function cruIcs()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Ics_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;

                    if ($data['action'] == "edit") {
                        $clause = array("ics_id" => $data['ics_id']);
                        unset($data['ics_id']);
                    }

                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->Ics_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $message = array("message" => ($action == "add") ?
                            "RIB enregistré avec succès" : "La modification réussie avec succès.");
                        $message['liste'] = array(
                            'id' => $request,
                        );
                        $retour = retour(true, "success", $request, $message);
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
