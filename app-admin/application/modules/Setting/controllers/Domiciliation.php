<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Domiciliation extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/domiciliation.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "domiciliation";
        $this->_data['sous_module'] = "domiciliation" . DIRECTORY_SEPARATOR . "contenaire-domiciliation";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Entreprise_m');
                $data_post = $_POST['data'];
                $data = array();

                $data['entreprise'] = $this->Entreprise_m->get(
                    array(
                        'method' => 'row'
                    )
                );

                if ($data_post['visuel'] == 'fiche') {
                    $this->renderComponant("Setting/domiciliation/fiche-domiciliation", $data);
                } else {
                    $this->renderComponant("Setting/domiciliation/form-domiciliation", $data);
                }
            }
        }
    }

    function updateEntreprise()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Entreprise_m');
            $control = $this->control_data($_POST['data']);

            $retour = retour(null, null, null, $control['information']);
            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $clause = null;

                $data_id_entreprise = $this->Entreprise_m->get(
                    array(
                        'method' => 'row'
                    )
                );
                
                if (!empty($data_id_entreprise)) {
                    $clause = array("id_entreprise" => $data_id_entreprise->id_entreprise);
                }
                $request = $this->Entreprise_m->save_data($data, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $request, array("message" => "Enregistrement réussi"));
                }
            }

            echo json_encode($retour);
        }
    }
}
