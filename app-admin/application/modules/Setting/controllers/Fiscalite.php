<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fiscalite extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/fiscalite.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "fiscalite";
        $this->_data['sous_module'] = "fiscalite" . DIRECTORY_SEPARATOR . "contenaire-fiscalite";
        $this->render('contenaire');
    }

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Entreprise_m');
                $data_post = $_POST['data'];
                $data = array();

                $data['entreprise'] = $this->Entreprise_m->get(
                    array(
                        'method' => 'row'
                    )
                );

                if ($data_post['visuel'] == 'fiche') {
                    $this->renderComponant("Setting/fiscalite/fiche-fiscalite", $data);
                } else {
                    $this->renderComponant("Setting/fiscalite/form-fiscalite", $data);
                }
            }
        }
    }

    function updateEntreprise_fiscale()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Entreprise_m');
            $control = $this->control_data($_POST['data']);

            $retour = retour(null, null, null, $control['information']);
            if ($control['return']) {
                $retour = retour(false, "error", 0, array("message" => "Error"));
                $data = format_data($_POST['data']);
                $clause = null;

                $data_id_entreprise = $this->Entreprise_m->get(
                    array(
                        'method' => 'row'
                    )
                );
                
                if (!empty($data_id_entreprise)) {
                    $clause = array("id_entreprise" => $data_id_entreprise->id_entreprise);
                }
                $request = $this->Entreprise_m->save_data($data, $clause);
                if (!empty($request)) {
                    $retour = retour(true, "success", $request, array("message" => "Enregistrement réussi"));
                }
            }

            echo json_encode($retour);
        }
    }
}