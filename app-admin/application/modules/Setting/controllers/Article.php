<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/article.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "article";
        $this->_data['sous_module'] = "article" . DIRECTORY_SEPARATOR . "contenaire-article";
        $this->render('contenaire');
    }

    // get liste article

    public function getHeaderListe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/article/header-article", $data);
            }
        }
    }

    public function listArticle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'columns' => array('*'),
                    'join' => array(
                        'c_famille_article' => 'c_famille_article.fam_id = c_article.fam_id',
                        'c_type_article_facturation' => 'c_type_article_facturation.type_fact_id = c_article.type_fact_id'
                    )
                );
                $this->load->model('Article_m');
                $data['article'] = $this->Article_m->get($params);
                $this->renderComponant("Setting/article/liste-article", $data);
            }
        }
    }

    public function getFormArticle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                $this->load->model('Article_m');
                $this->load->model('FamilleArticle_m', 'famille');
                $this->load->model('TypeArticle_m', 'type');

                if ($post['action'] != "add") {
                    $data['article'] = $this->Article_m->get(
                        array('clause' => array("art_id" => $post['art_id']), 'method' => "row")
                    );
                }

                $data['action'] = $post['action'];

                $data['type_article'] = $this->type->get(
                    array(
                        'columns' => array('*')
                    )
                );

                $data['famille'] = $this->famille->get(
                    array('columns' => array('*'))
                );

                $this->renderComponant("Setting/article/form-article", $data);
            }
        }
    }

    public function cruArticle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Article_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action'] == "edit") {
                        $clause = array("art_id" => $data['art_id']);
                        unset($data['art_id']);
                    } else {
                        $data['art_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->Article_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action == "add") ? "Enregistrement réussi" : "Modification réussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function removeArticle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Article_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('art_id' => $data['art_id']),
                    'columns' => array('art_id'),
                    'method' => "row",
                );
                $request_get = $this->Article_m->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['art_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment désactiver cet article ?",
                            'btnConfirm' => "Désactiver l'article",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('art_id' => $data['art_id']);
                    $data_update = array('art_etat' => 0);
                    $request = $this->Article_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['art_id']),
                            'message' => "Suppression réussie",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
