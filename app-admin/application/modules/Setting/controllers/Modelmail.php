<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Modelmail extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/setting/modelMail.js'
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-model";
        $this->_data['sous_module'] = "modelMail" . DIRECTORY_SEPARATOR . "contenaire-modelMail";
        $this->render('contenaire');
    }


    public function getListModelMail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Modelmail_m');
                $data = array();
                $param = array(
                    'columns' => array('pmail_id', 'pmail_libelle', 'pmail_description', 'pmail_objet', 'pmail_contenu', 'pmail_langue'),
                    'order_by_columns' => 'pmail_libelle ASC'
                );
                $data['model'] = $this->Modelmail_m->get($param);

                $this->renderComponant("Setting/modelMail/liste-modelMail", $data);
            }
        }
    }

    public function formUpdate()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Modelmail_m');
                $data_post = $_POST["data"];
                $data['model'] = $this->Modelmail_m->get(
                    array('clause' => array("pmail_id" => $data_post['pmail_id']), 'method' => "row")
                );
                $this->renderComponant("Setting/modelMail/form-model", $data);
            }
        }
    }

    function updateModel()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Modelmail_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);

                    $pmail_id = $data['pmail_id'];
                    unset($data['pmail_id']);

                    $clause = array("pmail_id" => $pmail_id);
                    $request = $this->Modelmail_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $clause, array("message" => "Modification réussie avec succès."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
