<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
class OrigineClient extends ADM_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/origine-client.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-origine-client";
        $this->_data['sous_module'] = "origine_clients".DIRECTORY_SEPARATOR."contenaire-origineClient";
        $this->render('contenaire');
	}

     // get list'origine client

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/origine_clients/header-origineClient",$data);

            }
        }
    }

    public function getListOrigineClient(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('origine_cli_etat' => 1)
                );
                $this->load->model('OrigineClient_m','origine_cli');
                $data['origine_cli'] = $this->origine_cli->get($params);
                $this->renderComponant("Setting/origine_clients/liste-origineClient",$data);

            }
        }
    }

    public function getFormOrigineClient(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('OrigineClient_m','origine_cli');
                    $data['origine_cli'] = $this->origine_cli->get(
                        array('clause' => array("origine_cli_id" => $post['origine_cli_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/origine_clients/form-origineClient",$data);
            }
        }
    }

    public function cruOrigineClient(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('OrigineClient_m','origine_cli');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("origine_cli_id" => $data['origine_cli_id']);
                        unset($data['origine_cli_id']);
                    }else{
                        $data['origine_cli_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->origine_cli->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action=="add") ? "L'information du nouveau origine client a été enregistrée avec succès" : "La modification a été réussi avec succès."));
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeOrigineClient(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…", 
                );
                $this->load->model('OrigineClient_m','origine_cli');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('origine_cli_id' => $data['origine_cli_id']),
                    'columns' => array('origine_cli_id','origine_cli_libelle'),
                    'method' => "row",
                );
                $request_get = $this->origine_cli->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['origine_cli_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer l'origine client ".$request_get->origine_cli_libelle,
                            'btnConfirm' => "Supprimer l'origine client",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('origine_cli_id' => $data['origine_cli_id']);
                    $data_update = array('origine_cli_etat' => 0);
                    $request = $this->origine_cli->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['origine_cli_id']),
                            'message' => "L'origine client ".$request_get->origine_cli_libelle." a bien été supprimé",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}