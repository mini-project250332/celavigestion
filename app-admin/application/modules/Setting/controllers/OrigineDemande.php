<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
class OrigineDemande extends ADM_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/origine-demande.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-origine-demande";
        $this->_data['sous_module'] = "origine_demande".DIRECTORY_SEPARATOR."contenaire-origineDemande";
        $this->render('contenaire');
	}

     // get list'origine demande

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/origine_demande/header-origineDemande",$data);

            }
        }
    }

    public function getListOrigineDemande(){   
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('origine_dem_etat' => 1)
                );
                $this->load->model('OrigineDemande_m','origine_dem');
                $data['origine_dem'] = $this->origine_dem->get($params);
                $this->renderComponant("Setting/origine_demande/liste-origineDemande",$data);

            }
        }
    }

    public function getFormOrigineDemande(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('OrigineDemande_m','origine_dem');
                    $data['origine_dem'] = $this->origine_dem->get(
                        array('clause' => array("origine_dem_id" => $post['origine_dem_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/origine_demande/form-origineDemande",$data);
            }
        }
    }

    public function cruOrigineDemande(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('OrigineDemande_m','origine_dem');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("origine_dem_id" => $data['origine_dem_id']);
                        unset($data['origine_dem_id']);
                    }else{
                        $data['origine_dem_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->origine_dem->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action=="add") ? "L'information du nouveau origine client a été enregistrée avec succès" : "La modification a été réussi avec succès."));
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeOrigineDemande(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…", 
                );
                $this->load->model('OrigineDemande_m','origine_dem');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('origine_dem_id' => $data['origine_dem_id']),
                    'columns' => array('origine_dem_id','origine_dem_libelle'),
                    'method' => "row",
                );
                $request_get = $this->origine_dem->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['origine_dem_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer l'origine client ".$request_get->origine_dem_libelle,
                            'btnConfirm' => "Supprimer l'origine client",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('origine_dem_id' => $data['origine_dem_id']);
                    $data_update = array('origine_dem_etat' => 0);
                    $request = $this->origine_dem->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['origine_dem_id']),
                            'message' => "L'origine client ".$request_get->origine_dem_libelle." a bien été supprimé",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}