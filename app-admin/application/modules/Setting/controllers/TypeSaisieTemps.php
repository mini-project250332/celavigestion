<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TypeSaisieTemps extends ADM_Controller {
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Setting";
    protected $_const_js = array(
    	"timer" => 10
    );

	public function __construct(){
		parent::__construct();
        $this->_script = array(
             'js/setting/saisietemps.js'
        );
        $this->_css_personnaliser = array();
    }
	
	public function index(){
        $this->_data['menu_principale'] = "setting";
        $this->_data['menu_sidebar'] = "data-temps";
        $this->_data['sous_module'] = "saisieTemps".DIRECTORY_SEPARATOR."contenaire-temps";
        $this->render('contenaire');
	}

     // get liste cgp

    public function getHeaderListe(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = array();
                $this->renderComponant("Setting/saisieTemps/header-saisieTemps",$data);

            }
        }
    }

    public function getListSaisieTemps(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array('type_travail_etat' => 1)
                );
                $this->load->model('TypeTravail_m');
                $data['type'] = $this->TypeTravail_m->get($params);
                $this->renderComponant("Setting/saisieTemps/list-typeTemps",$data);

            }
        }
    }

    
    public function getFormTypeTravail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $post = $_POST["data"];
                $data = array();
                if ($post['action'] != "add") {
                    $this->load->model('TypeTravail_m');
                    $data['type_travail_id'] = $this->TypeTravail_m->get(
                        array('clause' => array("type_travail_id" => $post['type_travail_id']), 'method' => "row")
                    );
                }
                $data['action'] = $post['action'];
                $this->renderComponant("Setting/saisieTemps/form-typeTravail",$data);
            }
        }
    }

    public function cruTypeTravail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('TypeTravail_m');
                $control = $this->control_data($_POST['data']);

                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $clause = null;
                    if ($data['action']=="edit") {
                        $clause = array("type_travail_id" => $data['type_travail_id']);
                        unset($data['type_travail_id']);
                    }else{
                        $data['type_travail_etat'] = 1;
                    }
                    $action = $data['action'];
                    unset($data['action']);

                    $request = $this->TypeTravail_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $request, array("message" => ($action=="add") ? "Enregistrement réussi" : "Modification réussie."));
                    }
                }
                 echo json_encode($retour);
            }
        }
    }

    public function removeTypeTravail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…", 
                );
                $this->load->model('TypeTravail_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('type_travail_id' => $data['type_travail_id']),
                    'columns' => array('type_travail_id','type_travail_libelle'),
                    'method' => "row",
                );
                $request_get = $this->TypeTravail_m->get($params);

                if($data['action'] == "demande"){
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['type_travail_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce type de saisi ?",
                            'btnConfirm' => "Supprimer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",  
                    );
                }else if($data['action'] == "confirm"){
                    $clause = array('type_travail_id' => $data['type_travail_id']);
                    $data_update = array('type_travail_etat' => 0);
                    $request = $this->TypeTravail_m->save_data($data_update, $clause);
                    if($request){
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['type_travail_id']),
                            'message' => "Suppression réussie",  
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}