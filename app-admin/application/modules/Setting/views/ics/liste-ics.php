<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th>Nom émetteur </th>
					<th>ICS</th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($ics as $key => $value) : ?>
						<tr id="rowIcs-<?= $value->ics_id; ?>" class="align-middle">
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<?= $value->nom_emeteur; ?>
									</div>
								</div>
							</td>
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fs--1 mb-0">
											<?= $value->ics; ?>
										</p>
									</div>
								</div>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->ics_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateIcs" type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>