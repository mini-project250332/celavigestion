<?php echo form_tag('Setting/ICS/cruIcs', array('method' => "post", 'class' => '', 'id' => 'cruIcs')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire d'ajout d'un ICS</h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnulerICS" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($ics->ics_id)) : ?>
	<input type="hidden" name="ics_id" id="ics_id" value="<?= $ics->ics_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<br>
<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<div class="form-group inline">
						<div>
							<label data-width="200"> Nom émetteur </label>
							<input type="text" class="form-control" value="<?= isset($ics->nom_emeteur) ? $ics->nom_emeteur : ""; ?>" id="nom_emeteur" name="nom_emeteur" data-require="true" autocomplete="off">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> ICS </label>
							<input type="text" class="form-control" value="<?= isset($ics->ics) ? $ics->ics : ""; ?>" id="ics" name="ics" data-require="true">
							<div class="help"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>