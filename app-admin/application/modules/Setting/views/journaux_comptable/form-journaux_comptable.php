<?php echo form_tag('Setting/Journaux_comptable/updateEntreprise_Journaux_comptable', array('method' => "post", 'class' => '', 'id' => 'updateEntreprise_Journaux_comptable')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Journaux comptables </h5>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-primary" type="submit">
            <span class="fas fa-check me-1"></span>
            Terminée
        </button>
    </div>
</div>

<div class="col-12 p-0 py-1">
    <div class="row m-0">
        <div class="col-2"></div>
        <div class="col p-4">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Journal de ventes :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->journal_ventes : '' ?>" name="journal_ventes" id="journal_ventes">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Journal des OD :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->journal_od : '' ?>" name="journal_od" id="journal_od">
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<?php echo form_close(); ?>