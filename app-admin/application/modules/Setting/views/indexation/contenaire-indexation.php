<div id="contenaire-indexation" class="contenair-main contenair-main-sidebar vue_limit_access">


</div>

<div class="modal fade" id="modalValeurAlert" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-main-content">

        </div>
    </div>
</div>

<div class="modal fade" id="modalNewValeur" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-content-valeur">

        </div>
    </div>
</div>

<div class="modal fade" id="modalValidationValeur" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="contentIndice">

        </div>
    </div>
</div>

<div class="modal fade" id="ModalFormLot" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content" id="modalContentLot">

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        pgListeIndexation();
        verifyIndexation();
    });

    access_right([1], [1]);
</script>