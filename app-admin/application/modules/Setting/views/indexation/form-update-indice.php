<?php echo form_tag('Setting/Indexation/updateIndice', array('method' => "post", 'class' => '', 'id' => 'updateIndice')); ?>
<div class="contenair-title">
    <div class="px-2">

    </div>
    <div class="px-2">

        <button type="button" id="annulerUpdateIndice" data-id="<?= $indloyer_id ?>" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler </span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary">
            <span class="fas fa-plus me-1"></span>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<input type="hidden" name="indloyer_id" id="indloyer_id" value="<?= $indloyer_id; ?>">

<div class="contenair-content">
    <div class="row m-0">
        <div class="col"></div>
        <div class="col py-2">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Nom de l'indice </label>
                            <input type="text" autocomplete="off" class="form-control" value="<?php if (isset($indice)) echo $indice->indloyer_description; ?>" name="indloyer_description" id="indloyer_description" required>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Mode de calcul </label>
                            <select class="form-select fs--1" id="mode_calcul" name="mode_calcul" <?= !empty($valeur) ? 'disabled' : ''?>>
                                <option value="1" <?php echo ($indice->mode_calcul == 1) ? "selected" : " " ?>>Variation d'indice</option>
                                <option value="2" <?php echo ($indice->mode_calcul == 2) ? "selected" : " " ?>>Pourcentage</option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Périodicité </label>
                            <select class="form-select fs--1" id="indloyer_periode" name="indloyer_periode" <?= !empty($valeur) ? 'disabled' : ''?>>
                                <option value="Annuelle" <?php echo ($indice->indloyer_periode == "Annuelle") ? "selected" : " " ?>>Annuelle</option>
                                <option value="Trimestrielle" <?php echo ($indice->indloyer_periode == "Trimestrielle") ? "selected" : " " ?>>Trimestrielle</option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div style="margin-top: -19px;margin-bottom: -18px;">
                            <label style="width: 137px !important;margin-left: 205px !important;"> Jour </label>
                            <label style="width: 120px !important;"> Mois </label>
                        </div>
                    </div>
                    <div class="form-group inline annuelle">
                        <div>
                            <label data-width="200"> Date limite de saisie </label>
                            <select class="form-select fs--1" id="indloyer_date_saisie_an_jour" name="indloyer_date_saisie_an_jour" style="width: 30%;margin-left: -32px;margin-right: -1px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03"<?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "12") ? "selected" : " ") : "" ?>>12 </option>
                                <option value="13" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "13") ? "selected" : " ") : "" ?>>13 </option>
                                <option value="14" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "14") ? "selected" : " ") : "" ?>>14 </option>
                                <option value="15" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "15") ? "selected" : " ") : "" ?>>15 </option>
                                <option value="16" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "16") ? "selected" : " ") : "" ?>>16 </option>
                                <option value="17" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "17") ? "selected" : " ") : "" ?>>17 </option>
                                <option value="18" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "18") ? "selected" : " ") : "" ?>>18 </option>
                                <option value="19" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "19") ? "selected" : " ") : "" ?>>19 </option>
                                <option value="20" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "20") ? "selected" : " ") : "" ?>>20 </option>
                                <option value="21" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "21") ? "selected" : " ") : "" ?>>21 </option>
                                <option value="22" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "22") ? "selected" : " ") : "" ?>>22 </option>
                                <option value="23" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "23") ? "selected" : " ") : "" ?>>23 </option>
                                <option value="24" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "24") ? "selected" : " ") : "" ?>>24 </option>
                                <option value="25" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "25") ? "selected" : " ") : "" ?>>25 </option>
                                <option value="26" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "26") ? "selected" : " ") : "" ?>>26 </option>
                                <option value="27" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "27") ? "selected" : " ") : "" ?>>27 </option>
                                <option value="28" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "28") ? "selected" : " ") : "" ?>>28 </option>
                                <option value="29" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "29") ? "selected" : " ") : "" ?>>29 </option>
                                <option value="30" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "30") ? "selected" : " ") : "" ?>>30 </option>
                                <option value="31" <?php echo isset($indice->indloyer_date_saisie_an_jour) ? (($indice->indloyer_date_saisie_an_jour == "31") ? "selected" : " ") : "" ?>>31 </option>
                            </select>
                            <select class="form-select fs--1" id="indloyer_date_saisie_an_mois" name="indloyer_date_saisie_an_mois" style="width: 32%;margin-left: 5px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_an_mois) ? (($indice->indloyer_date_saisie_an_mois == "12") ? "selected" : " ") : "" ?>>12 </option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline trimestrielle">
                        <div>
                            <label data-width="200"> Date limite de saisie T1 </label>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t1_jour" name="indloyer_date_saisie_t1_jour" style="width: 30%;margin-left: -32px;margin-right: -1px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03"<?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "12") ? "selected" : " ") : "" ?>>12 </option>
                                <option value="13" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "13") ? "selected" : " ") : "" ?>>13 </option>
                                <option value="14" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "14") ? "selected" : " ") : "" ?>>14 </option>
                                <option value="15" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "15") ? "selected" : " ") : "" ?>>15 </option>
                                <option value="16" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "16") ? "selected" : " ") : "" ?>>16 </option>
                                <option value="17" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "17") ? "selected" : " ") : "" ?>>17 </option>
                                <option value="18" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "18") ? "selected" : " ") : "" ?>>18 </option>
                                <option value="19" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "19") ? "selected" : " ") : "" ?>>19 </option>
                                <option value="20" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "20") ? "selected" : " ") : "" ?>>20 </option>
                                <option value="21" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "21") ? "selected" : " ") : "" ?>>21 </option>
                                <option value="22" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "22") ? "selected" : " ") : "" ?>>22 </option>
                                <option value="23" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "23") ? "selected" : " ") : "" ?>>23 </option>
                                <option value="24" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "24") ? "selected" : " ") : "" ?>>24 </option>
                                <option value="25" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "25") ? "selected" : " ") : "" ?>>25 </option>
                                <option value="26" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "26") ? "selected" : " ") : "" ?>>26 </option>
                                <option value="27" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "27") ? "selected" : " ") : "" ?>>27 </option>
                                <option value="28" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "28") ? "selected" : " ") : "" ?>>28 </option>
                                <option value="29" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "29") ? "selected" : " ") : "" ?>>29 </option>
                                <option value="30" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "30") ? "selected" : " ") : "" ?>>30 </option>
                                <option value="31" <?php echo isset($indice->indloyer_date_saisie_t1_jour) ? (($indice->indloyer_date_saisie_t1_jour == "31") ? "selected" : " ") : "" ?>>31 </option>
                            </select>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t1_mois" name="indloyer_date_saisie_t1_mois" style="width: 32%;margin-left: 5px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "12") ? "selected" : " ") : "" ?>>12 </option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline trimestrielle">
                        <div>
                            <label data-width="200"> Date limite de saisie T2 </label>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t2_jour" name="indloyer_date_saisie_t2_jour" style="width: 30%;margin-left: -32px;margin-right: -1px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03"<?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "12") ? "selected" : " ") : "" ?>>12 </option>
                                <option value="13" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "13") ? "selected" : " ") : "" ?>>13 </option>
                                <option value="14" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "14") ? "selected" : " ") : "" ?>>14 </option>
                                <option value="15" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "15") ? "selected" : " ") : "" ?>>15 </option>
                                <option value="16" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "16") ? "selected" : " ") : "" ?>>16 </option>
                                <option value="17" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "17") ? "selected" : " ") : "" ?>>17 </option>
                                <option value="18" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "18") ? "selected" : " ") : "" ?>>18 </option>
                                <option value="19" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "19") ? "selected" : " ") : "" ?>>19 </option>
                                <option value="20" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "20") ? "selected" : " ") : "" ?>>20 </option>
                                <option value="21" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "21") ? "selected" : " ") : "" ?>>21 </option>
                                <option value="22" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "22") ? "selected" : " ") : "" ?>>22 </option>
                                <option value="23" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "23") ? "selected" : " ") : "" ?>>23 </option>
                                <option value="24" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "24") ? "selected" : " ") : "" ?>>24 </option>
                                <option value="25" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "25") ? "selected" : " ") : "" ?>>25 </option>
                                <option value="26" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "26") ? "selected" : " ") : "" ?>>26 </option>
                                <option value="27" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "27") ? "selected" : " ") : "" ?>>27 </option>
                                <option value="28" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "28") ? "selected" : " ") : "" ?>>28 </option>
                                <option value="29" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "29") ? "selected" : " ") : "" ?>>29 </option>
                                <option value="30" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "30") ? "selected" : " ") : "" ?>>30 </option>
                                <option value="31" <?php echo isset($indice->indloyer_date_saisie_t2_jour) ? (($indice->indloyer_date_saisie_t2_jour == "31") ? "selected" : " ") : "" ?>>31 </option>
                            </select>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t2_mois" name="indloyer_date_saisie_t2_mois" style="width: 32%;margin-left: 5px;">
                            <option value="02" <?php echo isset($indice->indloyer_date_saisie_t1_mois) ? (($indice->indloyer_date_saisie_t1_mois == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t2_mois) ? (($indice->indloyer_date_saisie_t2_mois == "12") ? "selected" : " ") : "" ?>>12 </option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline trimestrielle">
                        <div>
                            <label data-width="200"> Date limite de saisie T3</label>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t3_jour" name="indloyer_date_saisie_t3_jour" style="width: 30%;margin-left: -32px;margin-right: -1px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03"<?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "12") ? "selected" : " ") : "" ?>>12 </option>
                                <option value="13" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "13") ? "selected" : " ") : "" ?>>13 </option>
                                <option value="14" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "14") ? "selected" : " ") : "" ?>>14 </option>
                                <option value="15" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "15") ? "selected" : " ") : "" ?>>15 </option>
                                <option value="16" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "16") ? "selected" : " ") : "" ?>>16 </option>
                                <option value="17" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "17") ? "selected" : " ") : "" ?>>17 </option>
                                <option value="18" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "18") ? "selected" : " ") : "" ?>>18 </option>
                                <option value="19" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "19") ? "selected" : " ") : "" ?>>19 </option>
                                <option value="20" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "20") ? "selected" : " ") : "" ?>>20 </option>
                                <option value="21" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "21") ? "selected" : " ") : "" ?>>21 </option>
                                <option value="22" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "22") ? "selected" : " ") : "" ?>>22 </option>
                                <option value="23" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "23") ? "selected" : " ") : "" ?>>23 </option>
                                <option value="24" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "24") ? "selected" : " ") : "" ?>>24 </option>
                                <option value="25" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "25") ? "selected" : " ") : "" ?>>25 </option>
                                <option value="26" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "26") ? "selected" : " ") : "" ?>>26 </option>
                                <option value="27" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "27") ? "selected" : " ") : "" ?>>27 </option>
                                <option value="28" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "28") ? "selected" : " ") : "" ?>>28 </option>
                                <option value="29" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "29") ? "selected" : " ") : "" ?>>29 </option>
                                <option value="30" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "30") ? "selected" : " ") : "" ?>>30 </option>
                                <option value="31" <?php echo isset($indice->indloyer_date_saisie_t3_jour) ? (($indice->indloyer_date_saisie_t3_jour == "31") ? "selected" : " ") : "" ?>>31 </option>
                            </select>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t3_mois" name="indloyer_date_saisie_t3_mois" style="width: 32%;margin-left: 5px;">
                                <option value="03" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t3_mois) ? (($indice->indloyer_date_saisie_t3_mois == "12") ? "selected" : " ") : "" ?>>12 </option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline trimestrielle">
                        <div>
                            <label data-width="200"> Date limite de saisie T4</label>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t4_jour" name="indloyer_date_saisie_t4_jour" style="width: 30%;margin-left: -32px;margin-right: -1px;">
                                <option value="01" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "01") ? "selected" : " ") : "" ?>>01 </option>
                                <option value="02" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "02") ? "selected" : " ") : "" ?>>02 </option>
                                <option value="03"<?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "12") ? "selected" : " ") : "" ?>>12 </option>
                                <option value="13" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "13") ? "selected" : " ") : "" ?>>13 </option>
                                <option value="14" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "14") ? "selected" : " ") : "" ?>>14 </option>
                                <option value="15" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "15") ? "selected" : " ") : "" ?>>15 </option>
                                <option value="16" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "16") ? "selected" : " ") : "" ?>>16 </option>
                                <option value="17" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "17") ? "selected" : " ") : "" ?>>17 </option>
                                <option value="18" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "18") ? "selected" : " ") : "" ?>>18 </option>
                                <option value="19" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "19") ? "selected" : " ") : "" ?>>19 </option>
                                <option value="20" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "20") ? "selected" : " ") : "" ?>>20 </option>
                                <option value="21" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "21") ? "selected" : " ") : "" ?>>21 </option>
                                <option value="22" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "22") ? "selected" : " ") : "" ?>>22 </option>
                                <option value="23" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "23") ? "selected" : " ") : "" ?>>23 </option>
                                <option value="24" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "24") ? "selected" : " ") : "" ?>>24 </option>
                                <option value="25" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "25") ? "selected" : " ") : "" ?>>25 </option>
                                <option value="26" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "26") ? "selected" : " ") : "" ?>>26 </option>
                                <option value="27" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "27") ? "selected" : " ") : "" ?>>27 </option>
                                <option value="28" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "28") ? "selected" : " ") : "" ?>>28 </option>
                                <option value="29" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "29") ? "selected" : " ") : "" ?>>29 </option>
                                <option value="30" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "30") ? "selected" : " ") : "" ?>>30 </option>
                                <option value="31" <?php echo isset($indice->indloyer_date_saisie_t4_jour) ? (($indice->indloyer_date_saisie_t4_jour == "31") ? "selected" : " ") : "" ?>>31 </option>
                            </select>
                            <select class="form-select fs--1" id="indloyer_date_saisie_t4_mois" name="indloyer_date_saisie_t4_mois" style="width: 32%;margin-left: 5px;">
                                <option value="03" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "03") ? "selected" : " ") : "" ?>>03 </option>
                                <option value="04" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "04") ? "selected" : " ") : "" ?>>04 </option>
                                <option value="05" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "05") ? "selected" : " ") : "" ?>>05 </option>
                                <option value="06" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "06") ? "selected" : " ") : "" ?>>06 </option>
                                <option value="07" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "07") ? "selected" : " ") : "" ?>>07 </option>
                                <option value="08" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "08") ? "selected" : " ") : "" ?>>08 </option>
                                <option value="09" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "09") ? "selected" : " ") : "" ?>>09 </option>
                                <option value="10" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "10") ? "selected" : " ") : "" ?>>10 </option>
                                <option value="11" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "11") ? "selected" : " ") : "" ?>>11 </option>
                                <option value="12" <?php echo isset($indice->indloyer_date_saisie_t4_mois) ? (($indice->indloyer_date_saisie_t4_mois == "12") ? "selected" : " ") : "" ?>>12 </option>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('change', '#indloyer_periode', function(e) {
            var selected_option = $('#indloyer_periode').val();
            if (selected_option == "Trimestrielle") {
                $(".annuelle").hide();
                $(".trimestrielle").show();
            } else {
                $(".annuelle").show();
                $(".trimestrielle").hide();
            }
        });

    });
</script>