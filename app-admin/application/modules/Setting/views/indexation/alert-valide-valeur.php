<div class="modal-header">
    <h5 class="modal-title" id="title_modal_valid_transform">
        Validation impossible
    </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<?php echo form_tag('Setting/Indexation/validerValeur', array('method' => "post", 'class' => '', 'id' => 'validerValeur')); ?>
<div class="modal-body">
    <div class="row">
        <div class="col">

        </div>
        <input type="hidden" name="indval_id" id="indval_id" value="<?=$indval_id?>">
        <input type="hidden" name="indloyer_id" id="indloyer_id" value="<?=$indloyer_id?>">

        <div class="col-10">
            <p class="text-center fs--1 m-0">
               La validation de cette indexation doit être effectuée par un autre administrateur.
            </p>   
        </div>
        <div class="col">

        </div>
    </div>
</div>
<div class="modal-footer">
    <!-- <button type="submit" id="" class="btn btn-success" data-bs-dismiss="modal">Demander la validation</button> -->
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>

<?php echo form_close(); ?>