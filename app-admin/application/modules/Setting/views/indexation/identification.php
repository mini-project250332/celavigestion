<div class="contenair-title" id="title-identification-list">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <button type="submit" id="save-update-indice" class="btn btn-sm btn-primary mx-1 d-none">
            <i class="fas fa-check-circle"></i>
            <span> Terminer </span>
        </button>

        <button type="button" data-id="<?= $indice->indloyer_id; ?>" id="btn-update-indice" class="btn btn-sm btn-primary">
            <span class="fas fa-edit fas me-1"></span>
            <span> Modifier </span>
        </button>
    </div>
</div>


<input type="hidden" name="indloyer_id" id="indloyer_id" value="<?= $indice->indloyer_id; ?>">

<div id="fiche-identifiant-indexation">

</div>


<script type="text/javascript">
    $(document).ready(function() {
        getFicheIdentificationIndice(<?= $indice->indloyer_id; ?>, 'fiche');
    });

    $(document).on('click', '#btn-update-indice', function(e) {
        $("#title-identification-list").hide();
        getFicheIdentificationIndice(<?= $indice->indloyer_id; ?>, 'form');
    });
</script>