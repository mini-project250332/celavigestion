<div class="modal-header">
    <h5 class="modal-title text-center" id="exampleModalLabel">Liste des lots </h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Nom propriétaire
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Nom programme
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Indice
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Informations
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Facture réalisée
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Date de la création de la facture
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Nombre des factures réalisées
                </th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                    Action
                </th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <?php if (empty($lot_index)) : ?>
                <tr>
                    <td class="text-center" colspan="6">Aucun lot indexé</td>
                </tr>
            <?php else : ?>
                <?php foreach ($lot_index as $key => $value) : ?>
                    <tr>
                        <td class="text-center"><?= $value->client_nom . ' ' . $value->client_prenom ?></td>
                        <td class="text-center"><?= $value->progm_nom ?></td>
                        <td class="text-center"><?= $value->indloyer_description ?></td>
                        <td class="text-center">indexé le <?= date("d/m/Y", strtotime(str_replace('/', '-', $value->indval_date_generation))) ?></td>
                        <td class="text-center"><?= $value->etat_facture == 1 ? 'Oui' : 'Non' ?></td>
                        <td class="text-center">
                            <?php if ($value->date_creation_facture == NULL || $value->date_creation_facture == '0000-00-00') : ?>
                                -
                            <?php else : ?>
                                facturée le <?= date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $value->date_creation_facture))) ?>
                            <?php endif ?>
                        </td>
                        <td class="text-center"><?= $value->nmbr_facture > 0 ? $value->nmbr_facture : '-' ?></td>
                        <td class="text-center">
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-sm btn-primary fiche_Lot_indexé" data-cliLot_id="<?= $value->cliLot_id ?>" data-client_id="<?= $value->client_id ?>" data-nom="<?= $value->client_nom ?>">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
</div>