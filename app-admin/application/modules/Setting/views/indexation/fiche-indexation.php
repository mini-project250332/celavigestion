<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Fiche indexation : <?=$indice->indloyer_description ?> </h5>
	</div>
	<div class="px-2">
		<button id="retourListeIndice" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo me-1"></i>
			<span>Retour</span>
		</button>
	</div>
</div>

<div class="contenair-content">
	
	<ul class="nav nav-tabs" id="indiceTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
		</li>
		<!-- <li class="nav-item">
			<a class="nav-link" id="valeur-tab" data-bs-toggle="tab" href="#tab-valeur" role="tab" aria-controls="tab-valeur" aria-selected="false">Valeur</a>
		</li> -->
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="prospectTabContent">
		<div class="tab-pane fade show active" id="tab-identification" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification'); ?>
		</div>
		<!-- <div class="tab-pane fade" id="tab-valeur" role="tabpanel" aria-labelledby="lot-tab">
			<?php $this->load->view('valeur'); ?>
		</div> -->
	</div>

</div>
