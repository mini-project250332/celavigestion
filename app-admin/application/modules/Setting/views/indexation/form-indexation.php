<?php echo form_tag('Setting/Indexation/AjouterIndice', array('method' => "post", 'class' => '', 'id' => 'AddIndice')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire </h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnulerIndice" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>
<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<div class="form-group inline">
						<div>
							<label data-width="300"> Nom de l'indice </label>
							<input type="text" class="form-control" name="indloyer_description">
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline">
						<div>
							<label data-width="200"> Périodicité de l'indexation </label>
							<select class="form-select fs--1" id="indloyer_periode" name="indloyer_periode">
								<option value="Annuelle">Annuelle</option>
								<option value="Trimestrielle">Trimestrielle</option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline">
						<div>
							<label data-width="300"> Mode de calcul </label>
							<select class="form-select fs--1" id="mode_calcul" name="mode_calcul">
								<option value="1">Variation d'indice</option>
								<option value="2">Pourcentage</option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline">
						<div style="margin-top: -19px;margin-bottom: -18px;">
							<label style="width: 137px !important;margin-left: 231px !important;"> Jour </label>
							<label style="width: 120px !important;"> Mois </label>
						</div>
					</div>
					<div class="form-group inline annuelle">
						<div>
							<label data-width="200"> Date limite de saisie </label>
							<select class="form-select fs--1" id="indloyer_date_saisie_an_jour" name="indloyer_date_saisie_an_jour" style="width: 30%;margin-left: 11px; margin-right: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
								<option value="13">13 </option>
								<option value="14">14 </option>
								<option value="15">15 </option>
								<option value="16">16 </option>
								<option value="17">17 </option>
								<option value="18">18 </option>
								<option value="19">19 </option>
								<option value="20">20 </option>
								<option value="21">21 </option>
								<option value="22">22 </option>
								<option value="23">23 </option>
								<option value="24">24 </option>
								<option value="25">25 </option>
								<option value="26">26 </option>
								<option value="27">27 </option>
								<option value="28">28 </option>
								<option value="29">29 </option>
								<option value="30">30 </option>
								<option value="31">31 </option>
							</select>
							<select class="form-select fs--1" id="indloyer_date_saisie_an_mois" name="indloyer_date_saisie_an_mois" style="width: 32%;margin-left: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline trimestrielle">
						<div>
							<label data-width="200"> Date limite de saisie T1 </label>
							<select class="form-select fs--1" id="indloyer_date_saisie_t1_jour" name="indloyer_date_saisie_t1_jour" style="width: 30%;margin-left: 11px; margin-right: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
								<option value="13">13 </option>
								<option value="14">14 </option>
								<option value="15">15 </option>
								<option value="16">16 </option>
								<option value="17">17 </option>
								<option value="18">18 </option>
								<option value="19">19 </option>
								<option value="20">20 </option>
								<option value="21">21 </option>
								<option value="22">22 </option>
								<option value="23">23 </option>
								<option value="24">24 </option>
								<option value="25">25 </option>
								<option value="26">26 </option>
								<option value="27">27 </option>
								<option value="28">28 </option>
								<option value="29">29 </option>
								<option value="30">30 </option>
								<option value="31">31 </option>
							</select>
							<select class="form-select fs--1" id="indloyer_date_saisie_t1_mois" name="indloyer_date_saisie_t1_mois" style="width: 32%;margin-left: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline trimestrielle">
						<div>
							<label data-width="200"> Date limite de saisie T2 </label>
							<select class="form-select fs--1" id="indloyer_date_saisie_t2_jour" name="indloyer_date_saisie_t2_jour" style="width: 30%;margin-left: 11px; margin-right: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
								<option value="13">13 </option>
								<option value="14">14 </option>
								<option value="15">15 </option>
								<option value="16">16 </option>
								<option value="17">17 </option>
								<option value="18">18 </option>
								<option value="19">19 </option>
								<option value="20">20 </option>
								<option value="21">21 </option>
								<option value="22">22 </option>
								<option value="23">23 </option>
								<option value="24">24 </option>
								<option value="25">25 </option>
								<option value="26">26 </option>
								<option value="27">27 </option>
								<option value="28">28 </option>
								<option value="29">29 </option>
								<option value="30">30 </option>
								<option value="31">31 </option>
							</select>
							<select class="form-select fs--1" id="indloyer_date_saisie_t2_mois" name="indloyer_date_saisie_t2_mois" style="width: 32%;margin-left: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline trimestrielle">
						<div>
							<label data-width="200"> Date limite de saisie T3</label>
							<select class="form-select fs--1" id="indloyer_date_saisie_t3_jour" name="indloyer_date_saisie_t3_jour" style="width: 30%;margin-left: 11px; margin-right: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
								<option value="13">13 </option>
								<option value="14">14 </option>
								<option value="15">15 </option>
								<option value="16">16 </option>
								<option value="17">17 </option>
								<option value="18">18 </option>
								<option value="19">19 </option>
								<option value="20">20 </option>
								<option value="21">21 </option>
								<option value="22">22 </option>
								<option value="23">23 </option>
								<option value="24">24 </option>
								<option value="25">25 </option>
								<option value="26">26 </option>
								<option value="27">27 </option>
								<option value="28">28 </option>
								<option value="29">29 </option>
								<option value="30">30 </option>
								<option value="31">31 </option>
							</select>
							<select class="form-select fs--1" id="indloyer_date_saisie_t3_mois" name="indloyer_date_saisie_t3_mois" style="width: 32%;margin-left: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
							</select>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline trimestrielle">
						<div>
							<label data-width="200"> Date limite de saisie T4</label>
							<select class="form-select fs--1" id="indloyer_date_saisie_t4_jour" name="indloyer_date_saisie_t4_jour" style="width: 30%;margin-left: 11px; margin-right: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
								<option value="13">13 </option>
								<option value="14">14 </option>
								<option value="15">15 </option>
								<option value="16">16 </option>
								<option value="17">17 </option>
								<option value="18">18 </option>
								<option value="19">19 </option>
								<option value="20">20 </option>
								<option value="21">21 </option>
								<option value="22">22 </option>
								<option value="23">23 </option>
								<option value="24">24 </option>
								<option value="25">25 </option>
								<option value="26">26 </option>
								<option value="27">27 </option>
								<option value="28">28 </option>
								<option value="29">29 </option>
								<option value="30">30 </option>
								<option value="31">31 </option>
							</select>
							<select class="form-select fs--1" id="indloyer_date_saisie_t4_mois" name="indloyer_date_saisie_t4_mois" style="width: 32%;margin-left: 5px;">
								<option value="01">01 </option>
								<option value="02">02 </option>
								<option value="03">03 </option>
								<option value="04">04 </option>
								<option value="05">05 </option>
								<option value="06">06 </option>
								<option value="07">07 </option>
								<option value="08">08 </option>
								<option value="09">09 </option>
								<option value="10">10 </option>
								<option value="11">11 </option>
								<option value="12">12 </option>
							</select>
							<div class="help"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('change', '#indloyer_periode', function(e) {
			var selected_option = $('#indloyer_periode').val();
			if (selected_option == "Trimestrielle") {
				$(".annuelle").hide();
				$(".trimestrielle").show();
			} else {
				$(".annuelle").show();
				$(".trimestrielle").hide();
			}
		});

	});
</script>