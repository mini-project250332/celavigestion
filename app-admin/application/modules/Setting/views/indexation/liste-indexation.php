<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th scope="col">Nom de l'indice</th>
					<th scope="col">Périodicité</th>					
					<th class="text-end">Actions</th>
				</thead>
				<tbody>
					<?php foreach ($indice as $key => $value) : ?>						
						<tr id="rowIndice-<?= $value->indloyer_id; ?>" class="align-middle">
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">
											<?= $value->indloyer_description; ?>
										</h6>
									</div>
								</div>
							</td>
							<td><?= isset($value->indloyer_periode) ? $value->indloyer_periode : ''; ?></td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->indloyer_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateIndice" type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
									<button data-id="<?= $value->indloyer_id; ?>"class="btn btn-sm btn-outline-primary icon-btn btnlisteValeurIndice" type="button">
										<span class="fas fa-play fs-1"></span>
									</button>
									<!-- <button class="btn btn-outline-success icon-btn p-0 m-1 btnValideIndice only_visuel_gestion" type="button" data-id="<?= $value->indloyer_id; ?>">
										<span class="fas fa-check fs-1 m-1"></span>
									</button> -->
									<button data-id="<?= $value->indloyer_id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup only_visuel_gestion" type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>