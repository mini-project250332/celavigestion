<?php echo form_tag('Setting/Indexation/cruValeur', array('method' => "post", 'class' => '', 'id' => 'cruValeur')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">
			<?php if ($action === "add") { ?>
				Ajout des valeurs d'indexation
			<?php } ?>

			<?php if ($action === "edit") { ?>
				Modification des valeurs d'indexation
			<?php } ?>
		</h5>
	</div>
	<div class="px-2">

		<button type="button" id="annulerAddValeur" data-id="<?= $indloyer_id ?>" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<input type="hidden" name="indloyer_id" id="indloyer_id" value="<?= $indloyer_id; ?>">
<?php if (isset($last_value)) : ?>
	<input type="hidden" name="last_valeur" id="last_valeur" value="<?= !empty($last_value) ? $last_value->indval_valeur : ""; ?>">
<?php endif ?>

<?php if (isset($indval_id)) { ?>
	<input type="hidden" name="indval_id" id="indval_id" value="<?= $indval_id; ?>">
<?php } ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">

					<!-- <div class="form-group inline">
						<div>
							<label data-width="200"> Libellé </label>
							<input type="text" autocomplete="off" class="form-control" value="<?php if (isset($valeur)) echo $valeur->indval_libelle; ?>" name="indval_libelle" id="indval_libelle" required>
							<div class="help"></div>
						</div>
					</div> -->
					<div class="form-group inline">
						<div>
							<label data-width="200"> Type de périodicité </label>
							<select class="form-select fs--1" id="type_periode" name="type_periode" disabled>
								<option value="<?= $indice->indloyer_periode == 'Trimestrielle' ? '2' : '1' ?>" <?= (isset($valeur) && $valeur->indval_trimestre === null) ? "selected" : '' ?>>
									<?= (isset($valeur) && $valeur->indval_trimestre === null) ? "Annuel" : $indice->indloyer_periode; ?>
								</option>
								<option value="2" <?php if (isset($valeur) && $valeur->indval_trimestre !== null) echo "selected"; ?>>Trimestriel</option>
							</select>
							<div class="help"></div>
						</div>
					</div>

					<?php if ($action == 'edit') : ?>
						<div class="form-group inline">
							<div>
								<?php
								$indval_annee = isset($valeur_annee->indval_annee) ? $valeur_annee->indval_annee : "";
								?>
								<label data-width="200"> Année</label>
								<input type="number" required disabled class="form-control maj" <?= isset($valeur_annee->indval_id) ? '' : '' ?> value="<?php echo isset($valeur) ? $valeur->indval_annee : $indval_annee  ?>" maxlength="4" minlength="4" autocomplete="off" id="indval_annee" name="indval_annee" data-require="true">
								<div class="help"></div>
							</div>
						</div>
					<?php else : ?>
						<div class="form-group inline">
							<div>
								<?php
								if (isset($valeur_max)) {
									$annee = $valeur_max['annee'];
								}

								if (isset($valeur_max) && $valeur_max['trimestre'] == 'T4') {
									$annee = $valeur_max['annee'] + 1;
								}
								?>
								<label data-width="200"> Année</label>
								<input type="number" required class="form-control maj" value="<?= isset($valeur_max) ? (($indice->indloyer_periode == 'Trimestrielle') ?  $annee : $annee + 1) : ""  ?>" maxlength="4" minlength="4" autocomplete="off" id="indval_annee" name="indval_annee" data-require="true" <?= !empty($valeur_annee) ? 'disabled' : '' ?>>
								<div class="help"></div>
							</div>
						</div>
					<?php endif ?>
					<?php if ($action == 'edit') : ?>
						<div class="form-group inline Trimestre" style="display : none;">
							<div>
								<label data-width="200"> Trimestre </label>
								<select class="form-select fs--1" id="indval_trimestre" name="indval_trimestre" data-require="true" required disabled>
									<option value="T1" <?php echo isset($valeur->indval_trimestre) ? (($valeur->indval_trimestre == "T1") ? "selected" : " ") : " " ?>>T1</option>
									<option value="T2" <?php echo isset($valeur->indval_trimestre) ? (($valeur->indval_trimestre == "T2") ? "selected" : " ") : " " ?>>T2</option>
									<option value="T3" <?php echo isset($valeur->indval_trimestre) ? (($valeur->indval_trimestre == "T3") ? "selected" : " ") : " " ?>>T3</option>
									<option value="T4" <?php echo isset($valeur->indval_trimestre) ? (($valeur->indval_trimestre == "T4") ? "selected" : " ") : " " ?>>T4</option>
								</select>
								<div class="help"></div>
							</div>
						</div>
					<?php else : ?>
						<div class="form-group inline Trimestre" style="display : none;">
							<div>
								<label data-width="200"> Trimestre </label>
								<select class="form-select fs--1" id="indval_trimestre" name="indval_trimestre" data-require="true" required <?= !empty($valeur_annee) ? 'disabled' : '' ?>>
									<option value="T1" <?= isset($valeur_max) && $valeur_max['trimestre'] == 'T4' ? 'selected' : '' ?>>T1</option>
									<option value="T2" <?= isset($valeur_max) && $valeur_max['trimestre'] == 'T1' ? 'selected' : '' ?>>T2</option>
									<option value="T3" <?= isset($valeur_max) && $valeur_max['trimestre'] == 'T2' ? 'selected' : '' ?>>T3</option>
									<option value="T4" <?= isset($valeur_max) && $valeur_max['trimestre'] == 'T3' ? 'selected' : '' ?>>T4</option>
								</select>
								<div class="help"></div>
							</div>
						</div>
					<?php endif ?>
					<div class="form-group inline">
						<div>
							<label data-width="200"> Valeur </label>
							<input required type="number" step="0.01" autocomplete="off" class="form-control" value="<?php if (isset($valeur)) echo $valeur->indval_valeur; ?>" id="indval_valeur" name="indval_valeur" data-require="true">
							<div class="help alert-message pourcentage">
								<p id="percent" class="" role="alert">Indice précédent <?= !empty($last_value) ? $last_value->indval_valeur : ""; ?> <span></span> </p>
							</div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Date de parution </label>
							<input required type="text" class="form-control calendar" id="indval_date_parution" name="indval_date_parution" data-require="true" value="<?php if (isset($valeur)) echo $valeur->indval_date_parution; ?>">
							<div class="help"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y",
		defaultDate: null,
	});

	$(document).ready(function() {
		$(document).on('change', '#type_periode', function(e) {
			var selected_option = $('#type_periode').val();
			if (selected_option == "2") {
				$(".Trimestre").show();
			} else {
				$(".Trimestre").hide();
			}
		});
		const type_periode = $("#type_periode").val();

		// If trimmestriel
		if (type_periode === "2") {
			$(".Trimestre").show();
		} else {
			$(".Trimestre").hide();
		}
	});

	$(document).on('keyup', '#indval_valeur', function(e) {
		var last_value = parseFloat($("#last_valeur").val());
		var new_value = parseFloat($("#indval_valeur").val());
		var ecart = new_value - last_value;

		var percent = (ecart / last_value) * 100;

		$("#percent").removeClass('text text-danger');

		if (percent < -5 || percent > 5) {
			$("#percent").addClass('text text-danger');
		}
		$(".pourcentage span").html(':' + ' ' + percent.toFixed(2) + '%');

		if (timer != null) {
			clearTimeout(timer);
			timer = null;
		}
		timer = setTimeout(() => {
			//$(".alert-message").toggleClass('d-none');
		}, 2500);
	});
</script>

<style>
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	/* Firefox */
	input[type=number] {
		-moz-appearance: textfield;
	}
</style>