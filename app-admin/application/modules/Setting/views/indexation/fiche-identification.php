<div id="div-fiche-indice" class="row m-0">

	<div class="col-12 p-0 py-1">

		<div class="row m-0">
			<div class="col-3 p-1">

			</div>

			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<!-- <h5 class="fs-0">Identité</h5> -->

						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Nom de l'indice: </th>
									<th class="pe-0 text-end"><?= $indice->indloyer_description ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Périodicité: </th>
									<th class="pe-0 text-end"><?= !isset($indice->indloyer_periode) ? '' : $indice->indloyer_periode ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Mode de calcul: </th>
									<?php $mod_calcul = isset($indice->mode_calcul) && $indice->mode_calcul == 1 ? 'Variation d\'indice' : 'Pourcentage' ?>
									<th class="pe-0 text-end"><?= !isset($indice->mode_calcul) ? '' : $mod_calcul ?></th>
								</tr>
								<tr class="border-bottom <?= $indice->indloyer_periode == 'Trimestrielle' ? 'd-none' : '' ?>">
									<th class="ps-0">Date limite de saisie annuelle: </th>
									<th class="pe-0 text-end">
										<?php
										$indloyer_date_saisie_an_jour = !isset($indice->indloyer_date_saisie_an_jour) ? '' : $indice->indloyer_date_saisie_an_jour . '/';
										$indloyer_date_saisie_an_mois = !isset($indice->indloyer_date_saisie_an_mois) ? '' : $indice->indloyer_date_saisie_an_mois;
										?>
										<?= $indloyer_date_saisie_an_jour . $indloyer_date_saisie_an_mois ?>
									</th>
								</tr>
								<tr class="border-bottom <?= $indice->indloyer_periode == 'Trimestrielle' ? '' : 'd-none' ?>">
									<th class="ps-0">Date limite de saisie T1: </th>
									<th class="pe-0 text-end">
										<?php
										$indloyer_date_saisie_t1_jour = !isset($indice->indloyer_date_saisie_t1_jour) ? '' : $indice->indloyer_date_saisie_t1_jour . '/';
										$indloyer_date_saisie_t1_mois = !isset($indice->indloyer_date_saisie_t1_mois) ? '' : $indice->indloyer_date_saisie_t1_mois;
										?>
										<?= $indloyer_date_saisie_t1_jour . $indloyer_date_saisie_t1_mois ?>
									</th>
								</tr>
								<tr class="border-bottom <?= $indice->indloyer_periode == 'Trimestrielle' ? '' : 'd-none' ?>">
									<th class="ps-0">Date limite de saisie T2: </th>
									<th class="pe-0 text-end">
										<?php
										$indloyer_date_saisie_t2_jour = !isset($indice->indloyer_date_saisie_t2_jour) ? '' : $indice->indloyer_date_saisie_t2_jour . '/';
										$indloyer_date_saisie_t2_mois = !isset($indice->indloyer_date_saisie_t2_mois) ? '' : $indice->indloyer_date_saisie_t2_mois;
										?>
										<?= $indloyer_date_saisie_t2_jour . $indloyer_date_saisie_t2_mois ?>
									</th>
								</tr>
								<tr class="border-bottom <?= $indice->indloyer_periode == 'Trimestrielle' ? '' : 'd-none' ?>">
									<th class="ps-0">Date limite de saisie T3: </th>
									<th class="pe-0 text-end">
										<?php
										$indloyer_date_saisie_t3_jour = !isset($indice->indloyer_date_saisie_t3_jour) ? '' : $indice->indloyer_date_saisie_t3_jour . '/';
										$indloyer_date_saisie_t3_mois = !isset($indice->indloyer_date_saisie_t3_mois) ? '' : $indice->indloyer_date_saisie_t3_mois;
										?>
										<?= $indloyer_date_saisie_t3_jour . $indloyer_date_saisie_t3_mois ?>
									</th>
								</tr>
								<tr class="border-bottom <?= $indice->indloyer_periode == 'Trimestrielle' ? '' : 'd-none' ?>">
									<th class="ps-0">Date limite de saisie T4: </th>
									<th class="pe-0 text-end">
										<?php
										$indloyer_date_saisie_t4_jour = !isset($indice->indloyer_date_saisie_t4_jour) ? '' : $indice->indloyer_date_saisie_t4_jour . '/';
										$indloyer_date_saisie_t4_mois = !isset($indice->indloyer_date_saisie_t4_mois) ? '' : $indice->indloyer_date_saisie_t4_mois;
										?>
										<?= $indloyer_date_saisie_t4_jour . $indloyer_date_saisie_t4_mois ?>
									</th>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-3 p-1">

			</div>
		</div>

	</div>
</div>