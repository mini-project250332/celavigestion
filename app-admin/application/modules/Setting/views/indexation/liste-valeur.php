<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <div class="d-flex justify-content-end">
            <button id="btnFormNewValeur" data-id="<?= $indloyer_id; ?>" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Valeur indexation</button>
        </div>
    </div>
</div>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped text-center fs--1 mb-0">
        <thead class="bg-300 text-900 border">
            <tr>
                <th scope="col">Année </th>
                <th scope="col">Trimestre</th>
                <th scope="col">Valeur</th>
                <th scope="col"> Date de parution </th>
                <th scope="col"> Etat </th>
                <th scope="col"> Informations </th>
                <th class="" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($valeur)) { ?>
                <?php foreach ($valeur as $key => $value) : ?>
                    <?php
                    $button = $value['util_prenom'] == $_SESSION['session_utilisateur']['util_prenom'] ? 'alertValide' : 'btnValideIndice';
                    $valeur_valide = $value['indval_valide'];
                    $value;
                    if ($valeur_valide == 1) {
                        $value_indice = "Validé";
                    } else if ($valeur_valide == 2) {
                        $value_indice = "Ajouté non publié";
                    } else if ($valeur_valide == 3) {
                        $value_indice = "Publié non validé";
                    } else {
                        $value_indice = "Non Validé";
                    }
                    ?>
                    <tr id="rowValeurIndice-<?= $value['indval_id']; ?>" class="align-middle text-center">
                        <td class="m-0 p-0"><?= $value['indval_annee']; ?></td>
                        <td class="m-0 p-0"><?= $value['indval_trimestre']; ?></td>
                        <td class="m-0 p-0"><?= $value['indval_valeur'] != 0 ? $value['indval_valeur'] : ''; ?></td>
                        <td class="m-0 p-0"><?= $value['indval_date_parution']; ?></td>
                        <td class="m-0 p-0"><?= isset($value['indval_date_generation']) ? 'Générée' : $value_indice; ?></td>
                        <td class="m-0 p-0">
                            <?= isset($value['util_prenom']) ? 'Ajouté par' . ' ' . $value['util_prenom'] . ' ' . 'le' . ' ' . date("d/m/Y", strtotime(str_replace('/', '-', $value['indval_date_registre']))) : ''; ?> <br>
                            <?= isset($value['util_prenom_publie']) ? 'Publié par' . ' ' . $value['util_prenom_publie'] . ' ' . 'le' . ' ' . date("d/m/Y", strtotime(str_replace('/', '-', $value['indval_date_publie']))) : ''; ?> <br>
                            <?= isset($value['util_prenom_valide']) ? 'Validé par' . ' ' . $value['util_prenom_valide'] . ' ' . 'le' . ' ' . date("d/m/Y", strtotime(str_replace('/', '-', $value['indval_date_validation']))) : ''; ?> <br>
                            <?= isset($value['indval_date_generation']) ? 'Génération des indexations réalisée pour 
                            <span type="button" data-id=' . $value['indval_id'] . ' class="text text-primary fw-bold list-lot"> (' . $value['count_indexed'] . ') </span> 
                            lot(s) le ' . date("d/m/Y", strtotime(str_replace('/', '-', $value['indval_date_generation']))) . ' ' : '' ?>
                        </td>
                        <td class="btn-action-table m-0 p-0">
                            <?php if ($value['indval_id'] == $first_indval_id && ($value['indval_valide'] == 0 || $value['indval_valide'] == 2 || $value['indval_valide'] == 3)) {
                                echo '
                            <div class="d-flex justify-content-center">
                                <button data-id-loyer=' . $indloyer_id . ' data-id=' . $value['indval_id'] . ' class="btn btn-sm btn-outline-warning icon-btn btnUpdateValeurIndice" type="button">
                                    <span class="bi bi-pencil-square fs-1"></span>
                                </button>
                                <button data-id-loyer=' . $indloyer_id . ' class="btn btn-outline-success icon-btn p-0 m-1 ' . $button . ' only_visuel_gestion" type="button" data-id=' . $value['indval_id'] . '>
                                    <span class="fas fa-check fs-1 m-1"></span>
                                </button>
                            </div> 
                            ';
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php } ?>

        </tbody>
    </table>
</div>