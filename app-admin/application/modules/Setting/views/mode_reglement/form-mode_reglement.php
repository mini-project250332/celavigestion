<?php echo form_tag('Setting/Mode_reglement/updateEntreprise_mode_reglement', array('method' => "post", 'class' => '', 'id' => 'updateEntreprise_mode_reglement')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Mode de règlement</h5>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-primary" type="submit">
            <span class="fas fa-check me-1"></span>
            Terminée
        </button>
    </div>
</div>

<div class="col-12 p-0 py-1">
    <div class="row m-0">
        <div class="col-2"></div>
        <div class="col p-4">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="form-group inline">
                        <div>
                            <label data-width="300"> Mode de paiement en prélèvement :</label>
                            <textarea class="form-control" name="mode_prelevement" id="mode_prelevement" style="height: 75px;"><?= !empty($entreprise) ? $entreprise->mode_prelevement : '' ?></textarea>

                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="300"> Mode de paiement en virement :</label>
                            <textarea class="form-control" name="mode_virement" id="mode_virement" style="height: 75px;"><?= !empty($entreprise) ? $entreprise->mode_virement : '' ?></textarea>

                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="300"> Mode de paiement en chèque :</label>
                            <textarea class="form-control" name="mode_cheque" id="mode_cheque" style="height: 75px;"><?= !empty($entreprise) ? $entreprise->mode_cheque : '' ?></textarea>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<?php echo form_close(); ?>