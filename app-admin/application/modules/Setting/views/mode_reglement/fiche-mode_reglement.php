<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1"> Mode de règlement </h5>
	</div>
	<div class="px-2">
		<button id="btnFormodifier" class="btn btn-sm btn-primary" type="button">
			<span class="fas fa-edit me-1"></span>
			Modifier
		</button>
	</div>
</div>

<div class="col-12 p-0 py-1">
	<div class="row m-0">
		<div class="col-1"></div>
		<div class="col p-4">
			<div class="card rounded-0 h-100">
				<div class="card-body ">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom border-top">
								<th class="ps-0" width ='30%'>Mode de paiement en prélèvement :</th>
								<th class="pe-0"><?= !empty($entreprise) ? $entreprise->mode_prelevement : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0" width ='30%'>Mode de paiement en virement :</th>
								<th class="pe-0"><?= !empty($entreprise) ? $entreprise->mode_virement : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Mode de paiement en chèque :</th>
								<th class="pe-0"><?= !empty($entreprise) ? $entreprise->mode_cheque : '' ?></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-1"></div>
	</div>
</div>