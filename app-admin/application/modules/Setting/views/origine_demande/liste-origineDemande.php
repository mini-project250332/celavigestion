<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th scope="col">Libelle </th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($origine_dem as $key => $value): ?>
						<tr id="rowOrigineDemande-<?= $value->origine_dem_id; ?>" class="align-middle">
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">
											<a class="stretched-link text-900"
												href="#"><?= $value->origine_dem_libelle; ?></a>
										</h6>
									</div>
								</div>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->origine_dem_id; ?>"
										class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateOrigineDemande"
										type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
									<button data-id="<?= $value->origine_dem_id; ?>"
										class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup only_visuel_gestion"
										type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>