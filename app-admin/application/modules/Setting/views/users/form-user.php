<div class="container-users form-user pt-0">

	<?php echo form_tag('Setting/GestionUsers/cruUser', array('method' => "post", 'class' => '', 'id' => 'cruUser')); ?>

	<div class="contenair-title">
		<div class="">
			<?php if ($action == "add") : ?>
				<h5 class="fs-1">Formulaire ajout nouvel utilisateur</h5>
			<?php elseif ($action == "edit") : ?>
				<h5 class="fs-1">Formulaire modification utilisateur</h5>
			<?php else : ?>
				<h5 class="fs-1">Fiche utilisateur</h5>
			<?php endif; ?>
		</div>
		<div class="px-2">

			<?php if ($action != "detail") : ?>
				<button id="btnAnnulerForm" type="button" class="btn btn-sm btn-secondary mx-1">
					<span class="bi bi-x me-1"></span>
					<span> Annuler </span>
				</button>
			<?php else : ?>
				<button id="btnAnnulerForm" type="button" class="btn btn-sm btn-secondary mx-1">
					<span class="fas fa-arrow-left me-1"></span>
					<span> Retour </span>
				</button>
			<?php endif; ?>

			<?php if ($action != "detail") : ?>
				<button type="submit" class="btn btn-sm btn-primary">
					<i class="fas fa-user-plus me-1"></i>
					<span> Enregistrer </span>
				</button>
			<?php endif; ?>
		</div>
	</div>

	<?php if ($action == "edit") : ?>
		<input type="hidden" id="util_id" name="util_id" value="<?php echo $user->util_id; ?>">
	<?php endif; ?>
	<input type="hidden" id="action" name="action" value="<?= $action; ?>">

	<div class="container-form-user">
		<div class="row m-0">
			<div class="col-6">
				<div class="py-3">
					<h5 class="fs-1"> Informations personnelles </h5>
				</div>

				<div class="form-group inline">
					<div>
						<label data-width="150"> Nom *</label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_nom" name="util_nom" data-formatcontrol="true" data-require="true" value="<?= isset($user->util_nom) ? $user->util_nom : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Prénom </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_prenom" name="util_prenom" value="<?= isset($user->util_prenom) ? $user->util_prenom : ""; ?>">
						<div class="help"> </div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Date naissance </label>
						<input data-type="date/fr" type="text" class="form-control <?= $action == 'detail' ? 'detail' : 'calendar'; ?>" id="util_datenaissance" name="util_datenaissance" value="<?= isset($user->util_datenaissance) ? date("d-m-Y", strtotime(str_replace('/', '-', $user->util_datenaissance))) : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Email *</label>
						<input data-type="email" type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_mail" name="util_mail" data-formatcontrol="true" data-require="true" value="<?= isset($user->util_mail) ? $user->util_mail : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Téléphone 1 </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_phonefixe" name="util_phonefixe" value="<?= isset($user->util_phonefixe) ? $user->util_phonefixe : ""; ?>">
						<div class="help"> </div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Téléphone 2 </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_phonemobile" name="util_phonemobile" value="<?= isset($user->util_phonemobile) ? $user->util_phonemobile : ""; ?>">
						<div class="help"></div>
					</div>
				</div>

				<hr class="mt-0">

				<div class="form-group inline mt-3">
					<div>
						<label data-width="150"> Adresse </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_adresse" name="util_adresse" value="<?= isset($user->util_adresse) ? $user->util_adresse : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Code postal </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_cp" name="util_cp" value="<?= isset($user->util_cp) ? $user->util_cp : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
				<div class="form-group inline">
					<div>
						<label data-width="150"> Ville </label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_ville" name="util_ville" value="<?= isset($user->util_ville) ? $user->util_ville : ""; ?>">
						<div class="help"></div>
					</div>
				</div>
			</div>


			<div class="col-6">
				<div class="py-3">
					<h5 class="fs-1"> Informations compte utilisateur </h5>
				</div>

				<!-- <div class="form-group inline">
					<div>
						<label data-width="250"> Profil utilisateur *</label>
						<select class="form-select <?= $action == 'detail' ? 'detail' : ''; ?>" id="tutil_id"
							name="tutil_id" data-require="true">
							<?php foreach ($type_users as $key => $value) : ?>
																													<option value="<?= $value->id; ?>" <?= isset($user->tutil_id) ? ($user->tutil_id == $value->id ? "selected" : "") : ""; ?>>
																														<?= $value->libelle; ?>
																													</option>
							<?php endforeach; ?>
						</select>
						<div class="help"></div>
					</div>
				</div> -->

				<input type="hidden" name="tutil_id" value="1" />

				<div class="form-group inline">
					<div>
						<label data-width="250"> Profil utilisateur *</label>
						<select class="form-select <?= $action == 'detail' ? 'detail' : ''; ?>" id="rol_util_id" name="rol_util_id" data-require="true">
							<?php foreach ($roles as $key_role => $role) : ?>
								<option value="<?= $role->rol_util_id; ?>" <?= isset($user->rol_util_id) ? ($user->rol_util_id == $role->rol_util_id ? "selected" : "") : ""; ?>>
									<?= $role->rol_util_libelle; ?>
								</option>
							<?php endforeach; ?>
						</select>
						<div class="help"></div>
					</div>
				</div>

				<div class="form-group inline">
					<div>
						<label data-width="250"> Identifiant *</label>
						<input type="text" class="form-control <?= $action == 'detail' ? 'detail' : ''; ?>" id="util_login" name="util_login" data-formatcontrol="true" data-require="true" value="<?= isset($user->util_login) ? $user->util_login : ""; ?>">
						<div class="help"></div>
					</div>
				</div>

				<?php if ($action == "edit") : ?>

					<div class="form-group inline">
						<div>
							<label data-width="250"> Mot de passe *</label>
							<input type="text" class="form-control" id="util_new_password" value="" data-formatcontrol="true" name="util_new_password" placeholder="<mot de passe>" autocomplete="off">
							<div class="help">
								<i>laisser vide pour ne pas modifier le mot de passe</i>
							</div>
							<div class="help">
								<span class="alert-new-password">
									Attention, si vous modifiez ce champ le mot de passe dans la base de données changera
									aussi.
								</span>
							</div>
						</div>
					</div>

				<?php endif; ?>

				<?php if ($action == "add") : ?>
					<div class="form-group inline">
						<div>
							<label data-width="250"> Mot de passe *</label>
							<input type="text" class="form-control" id="util_password" data-formatcontrol="true" data-require="true" name="util_password">
							<div class="help"></div>
						</div>
					</div>

					<div class="d-flex flex-row-reverse mb-3">
						<button id="generer_pw" type="button" class="btn btn-sm btn-outline-primary">
							<i class="fas fa-sync-alt"></i>
							<span> Générer mot de passe </span>
						</button>
					</div>

				<?php endif; ?>

				<div class="form-group inline switch" style="margin-top: 15px;">
					<div>
						<label data-width="250"> Statut </label>
						<div class="form-check form-switch">
							<span> Inactif </span>
							<input class="form-check-input" type="checkbox" id="util_status" name="util_status" <?php echo isset($user->util_status) ? ($user->util_status == "1" ? "checked" : "") : "checked"; ?>>
							<span> Actif </span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<script type="text/javascript">
	flatpickr(".calendar", {
		"locale": "fr",
		enableTime: false,
		dateFormat: "d-m-Y"
	});

	$("#util_new_password").bind("keyup", function(event) {
		var value = event.target.value;
		if (value != "") {
			$(this).attr('name', 'util_password');
			$(".alert-new-password").show();
		} else {
			$(this).attr('name', 'util_new_password');
			$(".alert-new-password").hide();
		}
	});
</script>

<style>
	.alert-new-password {
		color: red;
		padding-top: 15px;
		font-size: 11px;
		font-weight: 500;
		display: none;
	}
</style>