<style>
    .badge {
        cursor: pointer;
    }
</style>
<div class="modal-header" style="background-color : #636E7E !important">
    <h5 class="modal-title text-white" id="exampleModalLabel">Test d'envoi d'email</h5>
</div>

<div class="modal-body">
    <div class="row m-0">
        <div class="col py-1">
            <div class="">
                <div class="">
                    <div class="form-group inline">
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">De :</label>
                            <input type="text" class="form-control fs--1" id="emeteur_id" value="<?= $emeteur ?>">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Destinataire :</label>
                            <input type="text" class="form-control fs--1" id="destinataire" value="">
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Objet :</label>
                            <input type="text" class="form-control fs--1" id="object" value="<?= $object ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="message-text" class="col-form-label">Message :</label>
                            <textarea class="form-control fs--1" id="message_text" style="height : 315px;"><?= $message ?></textarea>
                        </div>
                        <div class="form-group">
                            <label data-width="200" for="recipient-name" class="col-form-label">Signature :</label>
                            <?php if (!empty($mess_signature)) : ?>
                                <img src="<?= base_url($mess_signature)  ?>" alt="" id="image_signature" style="width:400px; height:130px;">
                            <?php else : ?>
                                <label data-width="200" for="recipient-name" class="col-form-label" id="signature_decla">Aucune</label>
                            <?php endif ?>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
        <button type="button" class="btn btn-primary" id="testerEnvoiMail">Envoyer</button>
    </div>
</div>