<style>
    .form-check {
        top: 5px;
    }
</style>
<?php echo form_tag('Setting/GestionUsers/updateMailParam', array('method' => "post", 'class' => 'px-2', 'id' => 'updateMailParam')); ?>
<div class="contenair-title bg-300">
    <div class="px-2">
        <i class="fa fa-file fs-1" aria-hidden="true"></i> &nbsp; <h5 class="fs-1">Modification du paramètre messagerie</h5>
    </div>
    <div class="px-2">
        <button type="button" id="annulerUpdateParam_Smtp" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler</span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary mx-1">
            <i class="fas fa-plus me-1"></i>
            <span> Enregistrer </span>
        </button>
    </div>
</div>
<?php if (isset($param->mess_id)) : ?>
    <input type="hidden" name="mess_id" id="mess_id" value="<?= $param->mess_id; ?>">
<?php endif; ?>
<input type="hidden" name="util_id" id="util_id" value="<?= $util_id; ?>">
<div class="col-12 p-0 py-1">
    <div class="row m-0">
        <div class="col p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="fs-1">Informations générales</h5>
                    <hr class="mt-2 mb-0 col-sm-12">
                    <div class="mt-4">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Libellé *</label>
                                            <input type="text" class="form-control fs--1 " id="mess_libelle" name="mess_libelle" autocomplete="off" data-formatcontrol="true" data-require="true" required value="<?= isset($param->mess_libelle) ? $param->mess_libelle : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Description</label>
                                            <textarea class="form-control" id="mess_description" name="mess_description" style="height: 100px"><?= isset($param->mess_description) ? $param->mess_description : ""; ?></textarea>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <br>
                                <?php if (isset($param->mess_signature) && $param->mess_signature != NULL) : ?>
                                    <?php $fic_name = explode("/", $param->mess_signature); ?>
                                    <tr class="border-bottom">
                                        <div class="form-group inline p-0">
                                            <div class="col-sm-12">
                                                <label data-width="150"></label>
                                                <label><i class="bi bi-file-earmark-image"></i> <?= $fic_name[3] ?></label>
                                            </div>
                                        </div>
                                    </tr>
                                <?php endif ?>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Signature</label>
                                            <input class="form-control fs--1" id="mess_signature" name="mess_signature" type="file" value="<?= isset($param->mess_signature) ? $param->mess_signature : ""; ?>">
                                            <div class="help"> </div>
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-1">

            <div class="card rounded-0">
                <div class="card-body ">
                    <h5 class="fs-1">Paramètres de la messagerie</h5>
                    <hr class="mt-2 mb-0 col-sm-12">
                    <div class="mt-4">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Protocole *</label>
                                            <select class="form-select fs--1" id="mess_protocole" name="mess_protocole">
                                                <?php $protocol = unserialize(PROTOCOL);
                                                foreach ($protocol  as $prot) {
                                                    $selected = ($prot == $param->mess_protocole) ? ' selected' : "";
                                                    echo '<option value="' . $prot . '" ' . $selected . '>' . $prot . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Type du serveur *</label>
                                            <select class="form-select fs--1" id="mess_type" name="mess_type">
                                                <?php $typeServeur = unserialize(MAIL_SERVEUR);
                                                foreach ($typeServeur  as $typeServ) {
                                                    $selected = ($typeServ == $param->mess_type) ? ' selected' : "";
                                                    echo '<option value="' . $typeServ . '" ' . $selected . '>' . $typeServ . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Serveur du messagerie * </label>
                                            <input type="text" class="form-control fs--1" id="mess_adressemailserveur" name="mess_adressemailserveur" data-require="true" required value="<?= isset($param->mess_adressemailserveur) ? $param->mess_adressemailserveur : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Type d'authentification * </label>
                                            <select class="form-select fs--1" id="mess_typeauthentification" name="mess_typeauthentification">
                                                <?php $typeAuth = unserialize(TYPE_dechiffrement_MAIL);
                                                foreach ($typeAuth  as $auth) {
                                                    $selected = ($auth == $param->mess_typeauthentification) ? ' selected' : "";
                                                    echo '<option value="' . $auth . '" ' . $selected . ' >' . $auth . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Port * </label>
                                            <select class="form-select fs--1" id="mess_port" name="mess_port">
                                                <?php $port = unserialize(PORT_MAIL_SERVEUR);
                                                foreach ($port  as $port_mail => $port_type) {
                                                    foreach ($port_type  as $port_type_index => $value_port) {
                                                        $selected = ($value_port == $param->mess_port) ? ' selected' : "";
                                                        echo "<option data-protocol=" . $port_mail . " value='" . $value_port . "' " . $selected . ">" . $value_port . " </option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="mt-2 mb-0 col-sm-12">
                    <!-- <h5 class="fs-1 mt-4">Nom de la source</h5> -->
                    <div class="mt-4">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Nom de la source * </label>
                                            <input type="text" class="form-control fs--1" id="mess_nomsource" name="mess_nomsource" required data-require="true" value="<?= isset($param->mess_nomsource) ? $param->mess_nomsource : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Utilisateur * </label>
                                            <input type="text" class="form-control fs--1" id="mess_email" name="mess_email" data-require="true" required value="<?= isset($param->mess_email) ? $param->mess_email : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Mot de passe *</label>
                                            <input type="password" class="form-control fs--1" id="mess_motdepasse_crypte" required name="mess_motdepasse_crypte" data-require="true" value="<?= isset($param->mess_motdepasse) ? $param->mess_motdepasse : ""; ?>">
                                            <input type="hidden" id="mess_motdepasse" name="mess_motdepasse" value="<?= isset($param->mess_motdepasse) ? $param->mess_motdepasse : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Compte retour des mails </label>
                                            <input type="text" class="form-control fs--1" id="mess_compteretourmail" name="mess_compteretourmail" value="<?= isset($param->mess_compteretourmail) ? $param->mess_compteretourmail : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="fs-1">Autres paramètres</h5>
                    <hr class="mt-2 mb-0 col-sm-12">
                    <div class="mt-4">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="250"> Priorité </label>
                                            <select class="form-select fs--1" id="mess_priorite" name="mess_priorite">
                                                <?php $priorite = unserialize(PRIORITE_PARAMETRE);
                                                foreach ($priorite  as $priorite_param) {
                                                    $selected = ($priorite_param == $param->mess_priorite) ? ' selected' : "";
                                                    echo '<option value="' . $priorite_param . '" ' . $selected . ' >' . $priorite_param . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="250"> Encodage (Charset)</label>
                                            <select class="form-select fs--1" id="mess_charset" name="mess_charset">
                                                <?php $encodage = unserialize(CHARSET_CODAGE);
                                                foreach ($encodage  as $charset_codage) {
                                                    $selected = ($charset_codage == $param->mess_charset) ? ' selected' : "";
                                                    echo '<option value="' . $charset_codage . '" ' . $selected . '>' . $charset_codage . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline switch p-0">
                                        <div class="col-sm-12">
                                            <label data-width="150"> Status </label>
                                            <div class="form-check form-switch">
                                                <span> Inactif </span>
                                                <input class="form-check-input" id="mess_etat" name="mess_etat" type="checkbox" <?php echo !empty($param) ? ($param->mess_etat == "1" ? "checked" : "") : "disabled" ?> />
                                                <span> Actif </span>
                                            </div>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline switch p-0">
                                        <div class="col-sm-12 text-end">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-6">
                                                <button type="button" class="btn btn-sm btn-outline-primary" id="test_mail">Faire un test d'envoi</button>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
    // $(document).ready(function() {
    //     filter_port_protocol($('#mess_protocole').val());
    //     $('#mess_protocole').change(function() {
    //         var protocol = $(this).val();
    //         filter_port_protocol(protocol);
    //     });

    // });

    // function filter_port_protocol(protocol) {
    //     var list_port = $("#mess_port option");
    //     $(list_port).hide();
    //     $("#mess_port").find("option[data-protocol*=" + protocol + "]").each(function(i) {
    //         $(this).show();
    //         $("#mess_port").find("option[data-protocol*=" + protocol + "]:eq(0)").prop('selected', true);
    //     });
    // }
</script>