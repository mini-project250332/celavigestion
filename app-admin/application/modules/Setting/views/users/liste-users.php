<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Liste des utitisateurs</h5>
	</div>
	<div class="px-2">

		<button id="btnFormAjout" class="btn btn-sm btn-primary" type="button">
			<span class="bi bi-person-plus-fill me-1"></span>
			Nouvel utilisateur
		</button>

	</div>
</div>
<div class="contenair-list">

	<div class="table-responsive scrollbar">

		<table class="table table-sm table-striped overflow-hidden">
			<thead class="bg-300 text-900">
				<tr>
					<th scope="col">Utilisateur</th>
					<th scope="col">Email</th>
					<th scope="col" style="min-width: 100px">Téléphone 1</th>
					<th scope="col" style="min-width: 100px">Téléphone 2</th>
					<th scope="col">Adresse</th>
					<th scope="col">Profil</th>
					<th scope="col">Status</th>
					<th scope="col">Date creation</th>
					<th scope="">Action</th>
				</tr>
			</thead>
			<tbody>

				<?php foreach ($listUsers as $key => $value): ?>

					<tr id="rowUser-<?= $value->id; ?>" class="align-middle">
						<td class="text-nowrap">
							<div class="d-flex align-items-center">
								<?php if ($value->photo == null): ?>
									<div class="avatar avatar-xl me-2">
										<div class="avatar-name rounded-circle"><span>
												<?= $value->nom[0]; ?>
											</span></div>
									</div>
								<?php else: ?>
									<div class="avatar avatar-xl">
										<img class="rounded-circle" src="../../assets/img/team/4.jpg" alt="" />
									</div>
								<?php endif; ?>
								<div class="ms-2">
									<?= $value->nom . ' ' . $value->prenom; ?>
								</div>
							</div>
						</td>
						<td>
							<?= $value->mail; ?>
						</td>
						<td>
							<?= $value->fixe; ?>
						</td>
						<td>
							<?= $value->mobile; ?>
						</td>
						<td>
							<?= $value->ville . ' ' . $value->cp . ' ' . $value->adresse; ?>
						</td>
						<td>
							<?= $value->rol_util_libelle; ?>
						</td>
						<td>
							<div class="form-check form-switch">
								<input data-id="<?= $value->id; ?>" class="form-check-input"
									id="status-user-<?= $value->id; ?>" type="checkbox" <?php echo isset($value->status) ? ($value->status == "1" ? "checked" : "") : "checked"; ?> />
							</div>
						</td>
						<td>
							<?= $value->create; ?>
						</td>
						<td class="btn-action-table">
							<div>
								<button data-id="<?= $value->id; ?>"
									class="btn btn-sm btn-outline-primary icon-btn btnFormDetail" type="button">
									<span class="bi bi-eye-fill fs-1"></span>
								</button>
								<button data-id="<?= $value->id; ?>"
									class="btn btn-sm btn-outline-warning icon-btn btnFormUpdate" type="button">
									<span class="bi bi-pencil-square fs-1"></span>
								</button>
								<button data-id="<?= $value->id; ?>"
									class="btn btn-sm btn-outline-info icon-btn btnFormParam" type="button">
									<span class="bi bi bi-envelope fs-1"></span>
								</button>
								<button data-id="<?= $value->id; ?>"
									class="btn btn-sm btn-outline-danger icon-btn deleteUser" type="button">
									<span class="bi bi-trash fs-1"></span>
								</button>
							</div>
						</td>
					</tr>

				<?php endforeach; ?>

			</tbody>
		</table>
	</div>
</div>