<div class="contenair-title">
	<div class="px-2">
        <i class="fa fa-list fs-2" aria-hidden="true"></i> &nbsp; <h5 class="fs-1">Paramètres du modèle de l'envoi du mail</h5>
	</div>
</div>

<div class="card" style="height: 155px">
  <div class="card-body">
    <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead class="bg-300 text-900">
                    <th class="px-2">Libellé</th>
                    <th scope="col">Protocole</th>
                    <th scope="col">Description</th>
                    <th width="10%" class="text-center">Etat</th>
                    <th class="text-end">Action</th>
                </thead>
                <tbody class="bg-200">
                    <?php foreach ($param as $key => $value) {?>
                       <tr>
                            <td class="p-t-2 p-b-2 align-middle"><?=$value->mess_libelle ?></td>
                            <td class="p-t-2 p-b-2 align-middle"><?=$value->mess_protocole ?></td>
                            <td class="p-t-2 p-b-2 align-middle"><?=$value->mess_description ?> </td>
                            <td class="text-center align-middle p-t-2 p-b-2">
                                <div class="form-group inline switch">
                                    <div class="form-check form-switch">
                                        <span> Inactif </span>
                                            <input class="form-check-input" type="checkbox" id="mess_etat" name="mess_etat" disabled <?php echo isset($value->mess_etat) ? ($value->mess_etat=="1" ? "checked" :"") : "checked" ;?>>
                                        <span> Actif </span>
                                    </div>
                                </div>
                            </td>
                            <td class="d-flex justify-content-end">
                                <button class="btn btn-sm btn-primary icon-btn btnUpdateParam" type="button" data-id="<?= $value->mess_id ?>">
                                    <span class="bi bi-pencil-square"></span>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
            </table>
		</div>
    </div>
</div>