<?php echo form_tag('Setting/Modelmail/updateModel', array('method' => "post", 'class' => 'px-2', 'id' => 'updateModel')); ?>
<div class="contenair-title bg-300">
    <div class="px-2">
        <i class="fa fa-file fs-1" aria-hidden="true"></i> &nbsp; <h5 class="fs-1">Modification du modèle d'envoi du mail</h5>
    </div>
    <div class="px-2">
        <button type="button" id="annulerUpdateModel" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler</span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary mx-1">
            <i class="fas fa-plus me-1"></i>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<input value="<?= isset($model->pmail_id) ? $model->pmail_id : ""; ?>" name="pmail_id" id="pmail_id" type="hidden" class="form-control form-control-sm">
<div class="col-12 p-0 py-1">
    <div class="row m-0" style="width: 109%;">
        <div class="col-7 p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="mt-2">
                        <table class="table table-borderless fs--1 mb-0">
                            <tbody>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Nom du modèle * :</label>
                                            <input type="text" class="form-control fs--1 " id="pmail_libelle" name="pmail_libelle" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= isset($model->pmail_libelle) ? $model->pmail_libelle : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Description * :</label>
                                            <textarea class="form-control fs--1" id="pmail_description" name="pmail_description" style="height: 100px"><?= isset($model->pmail_description) ? $model->pmail_description : ""; ?></textarea>
                                            <div class="help"> </div>
                                        </div>
                                    </div>
                                </tr>
                                <br>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200">Objet du mail * :</label>
                                            <input type="text" class="form-control fs--1 " id="pmail_objet" name="pmail_objet" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= isset($model->pmail_objet) ? $model->pmail_objet : ""; ?>">
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="200"> Contenu du mail * :</label>
                                            <textarea class="form-control fs--1" id="pmail_contenu" name="pmail_contenu" style="height: 300px"><?= isset($model->pmail_contenu) ? $model->pmail_contenu : ""; ?></textarea>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <br>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Emetteur du mail par défaut :</label>
                                            <select class="form-select fs--1" id="emeteur_mail" name="emeteur_mail">
                                                <option value="1" <?= isset($model->emeteur_mail) && $model->emeteur_mail == 1 ? "selected" : ""; ?>>Utilisateur qui réalise l'action</option>
                                                <option value="2" <?= isset($model->emeteur_mail) && $model->emeteur_mail == 2 ? "selected" : ""; ?>>Utilisateur qui est le responsable client du dossier concerné</option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="300"> Signature :</label>
                                            <select class="form-select fs--1" id="signature" name="signature">
                                                <option value="1" <?= isset($model->signature) && $model->signature == 1 ? "selected" : ""; ?>>Signature image de l'utilisateur qui est l'émetteur du mail</option>
                                                <option value="2" <?= isset($model->signature) && $model->signature == 2 ? "selected" : ""; ?>>Pas de signature</option>
                                            </select>
                                            <div class="help"></div>
                                        </div>
                                    </div>
                                </tr>
                                <tr class="border-bottom">
                                    <div class="form-group inline p-0">
                                        <div class="col-sm-12">
                                            <label data-width="250"> Fichiers joints :</label>
                                            <label data-width="250"> <?= $model->fichier_joint ?></label>
                                            <!-- <label data-width="300"> Facture du loyer concernée en pièce jointe</label> -->
                                        </div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4 p-1">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="mt-1">
                        <div class="tasks">
                            <div class="task-header">
                                <h3 class="task-title mr-auto">Cliquez ici pour ajouter des champs de fusion </h3>
                            </div>
                            <div class="task-body">
                                <div class="task-issue ">
                                    <label>Informations sur le gestionnaire :</label>
                                    <div class="card m-b-5 g-box-shadow-none" style="height: 46px;">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7">
                                            <div class="" style="top : -10px;">
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@nom_gestionnaire ');" class="badge bg-primary cursor-pointer">@Nom</span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@prenom_gestionnaire ');" class="badge bg-primary cursor-pointer">@Prénom</span>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Informations sur le programme :</label>
                                    <div class="card m-b-5 g-box-shadow-none" style="height: 46px;">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7">
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@nom_programme');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Nom du programme
                                            </span>
                                        </div>
                                    </div>
                                    <label>Informations sur le client et prospect :</label>
                                    <div class="card m-b-5 g-box-shadow-none">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7" style="height: 68px;">
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@nom_client ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Nom client
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@prenom_client ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Prénom client
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@nom_prospect ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Nom prospect
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@prenom_prospect ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Prénom prospect
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@adresse_client ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Adresse client
                                            </span>
                                        </div>
                                    </div>
                                    <label>Informations sur l'utilisateur :</label>
                                    <div class="card m-b-5 g-box-shadow-none" style="height: 68px;">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7" style="height: 46px;">
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@nom_utilisateur ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Nom utilisateur
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@prenom_utilisateur ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Prénom utilisateur
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@adresse_utilisateur ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Adresse utilisateur
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@email_utilisateur ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Email utilisateur
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@telephone_utilisateur ');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Téléphone utilisateur
                                            </span>
                                        </div>
                                    </div>
                                    <label>Informations sur la taxe foncière :</label>
                                    <div class="card m-b-5 g-box-shadow-none">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7" style="height: 60px;">
                                            <div style="top : -13px;">
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@montant_taxe');" class="badge bg-primary cursor-pointer">@Montant taxe foncière </span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@montant_tom');" class="badge bg-primary cursor-pointer">@Montant de la TOM </span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@annee');" class="badge bg-primary cursor-pointer">@Année </span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@montant_tva');" class="badge bg-primary cursor-pointer">@Montant_tva </span>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Type de mandat :</label>
                                    <div class="card m-b-5 g-box-shadow-none">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7" style="height: 30px;">
                                            <div style="top : -13px;">
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@pack_confort');" class="badge bg-primary cursor-pointer">@Pack confort </span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@pack_essentiel');" class="badge bg-primary cursor-pointer">@Pack essentiel </span>
                                                <span onclick="insert_character_to_caracter('pmail_contenu','@pack_serenite');" class="badge bg-primary cursor-pointer">@Pack serénité </span>
                                            </div>
                                        </div>
                                    </div>
                                    <label>Objet du mail :</label>
                                    <div class="card m-b-5 g-box-shadow-none" style="height: 46px;">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7">
                                            <span onclick="insert_character_to_caracter('pmail_objet','@taxe_fonciere');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Taxe foncière
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_objet','@proposition_mandat');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Proposition mandat
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_objet','@erreur_taxe');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Erreur taxe
                                            </span>
                                            <span onclick="insert_character_to_caracter('pmail_objet','@facture_Loyer');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Facture Loyer
                                            </span>
                                        </div>
                                    </div>
                                    <label>Autres informations :</label>
                                    <div class="card m-b-5 g-box-shadow-none" style="height: 46px;">
                                        <div class="card-body p-l-5 p-t-5 p-r-5 p-b-7">
                                            <span onclick="insert_character_to_caracter('pmail_contenu','@date_envoi_mail');" class="badge bg-primary cursor-pointer" style="top : -10px;">
                                                @Date d'envoi du mail
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo form_close(); ?>