<style>
    #table_overflow {
        height: 773px;
        overflow-y: auto !important;
    }
</style>

<div class="contenair-title">
    <div class="px-2">
        <i class="fa fa-list fs-2" aria-hidden="true"></i> &nbsp; <h5 class="fs-1">Paramètres du modèle de l'envoi du mail</h5>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="table-responsive scrollbar" id="table_overflow">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead class="bg-300 text-900">
                    <th class="px-2">Nom du libellé</th>
                    <th scope="col-3">Description</th>
                    <th scope="col-3">Objet du mail</th>
                    <th scope="col-3">Langue</th>
                    <th class="col-1 text-end">Action</th>
                </thead>
                <tbody class="bg-200">
                    <?php foreach ($model as $key => $value) { ?>
                        <tr>
                            <td class="align-middle p-2"><?= $value->pmail_libelle ?></td>
                            <td class="align-middle p-2"><?= $value->pmail_description ?></td>
                            <td class="align-middle p-2"><?= $value->pmail_objet ?></td>
                            <td class="p-2"><?= $value->pmail_langue == 0 ? 'Français' : 'Anglais' ?></td>
                            <td class="text-end p-2">
                                <button class="btn btn-sm btn-primary icon-btn btnUpdateModel" type="button" data-id="<?= $value->pmail_id ?>">
                                    <span class="bi bi-pencil-square"></span>
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>