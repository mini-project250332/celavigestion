<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1 ms-3 mt-2">Progammme</h5>
	</div>
	<div class="px-2">
		<button type="button" id="btnAnnulerProgramme" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo me-1"></i>
			<span> Retour </span>
		</button>
	</div>
</div>

<div class="row">
	<div class="col-4">
		<div class="card rounded-6 h-100 ms-3 mt-3">
			<?php echo form_tag('Setting/Programme/cruProgram', array('method' => "post", 'class' => '', 'id' => 'formProgram')); ?>
			<div class="contenair-title">
				<div class="px-2">
					<h5 class="fs-1 mx-2">Formulaire </h5>
				</div>
				<div class="px-2">
					<button type="submit" class="btn btn-sm btn-primary">
						<span class="fas fa-check me-1"></span>
						<span> Enregistrer </span>
					</button>
				</div>
			</div>

			<?php if (isset($programme->progm_id)) : ?>
				<input type="hidden" name="progm_id" id="progm_id" value="<?= $programme->progm_id; ?>">
			<?php endif; ?>

			<input type="hidden" name="action" id="action" value="<?= $action ?>">

			<div class="contenair-content">
				<div class="row m-0">
					<div class="col"></div>
					<div class="col py-2">
						<div class="card-body">
							<div class="form-group inline">
								<div>
									<label data-width="200"> Numero </label>
									<?php if ($action == 'add') : ?>
										<input type="text" class="form-control" value="<?= isset($last_id) ? str_pad(($last_id + 1), 5, "0", STR_PAD_LEFT)  : ""; ?>" id="" name="" disabled>
									<?php else : ?>
										<input type="text" class="form-control" value="<?= isset($programme->progm_id) ? str_pad($programme->progm_id, 5, "0", STR_PAD_LEFT)  : ""; ?>" id="" name="" disabled>
									<?php endif ?>
									<input type="hidden" class="form-control" value="<?= isset($programme->progm_num) ? $programme->progm_num : ""; ?>" id="progm_num" name="progm_num">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Nom *</label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_nom) ? $programme->progm_nom : ""; ?>" id="progm_nom" name="progm_nom" data-formatcontrol="true" data-require="true">
									<div class="help" id="error_name"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 1 </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_adresse1) ? $programme->progm_adresse1 : ""; ?>" id="progm_adresse1" name="progm_adresse1">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 2 </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_adresse2) ? $programme->progm_adresse2 : ""; ?>" id="progm_adresse2" name="progm_adresse2">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Adresse 3 </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_adresse3) ? $programme->progm_adresse3 : ""; ?>" id="progm_adresse3" name="progm_adresse3">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Code postal </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_cp) ? $programme->progm_cp : ""; ?>" id="progm_cp" name="progm_cp">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Ville </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_ville) ? $programme->progm_ville : ""; ?>" id="progm_ville" name="progm_ville">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Pays </label>
									<input type="text" class="form-control maj" value="<?= isset($programme->progm_pays) ? $programme->progm_pays : ""; ?>" id="progm_pays" name="progm_pays">
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline">
								<div>
									<label data-width="200"> Pourcentage </label>
									<div class="input-group">
										<input type="text" placeholder="0" class="form-control mb-0 chiffre" id="progm_pourcentage_evaluation" name="progm_pourcentage_evaluation" style="text-align: right; height : 36px;" value="<?= isset($programme->progm_pourcentage_evaluation) ? $programme->progm_pourcentage_evaluation : ""; ?>">
										<div class="input-group-append">
											<span class="input-group-text" id="basic-addon2">%</span>
										</div>
									</div>
									<div class="help"></div>
								</div>
							</div>

							<div class="form-group inline switch">
								<div>
									<label data-width="250"> Statut programme</label>
									<div class="form-check form-switch">
										<span> Inactif </span>
										<input class="form-check-input" type="checkbox" id="progm_etat" name="progm_etat" <?php echo isset($programme->progm_etat) ? ($programme->progm_etat == "1" ? "checked" : "") : "checked"; ?>>
										<span> Actif </span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col"></div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>

	<div class="col">
		<div class="card rounded-6 h-100 mt-3 mx-3">
			<div class="contenair-title">
				<div class="px-2">
					<h5 class="fs-1 mx-2">Documents </h5>
				</div>
				<div class="px-2">
					<button type="submit" class="btn btn-sm btn-primary" id="ajoutdocProgramme" data-id="<?= $programme->progm_id; ?>">
						<span class="fas fa-plus me-1"></span>
						<span> Fichier </span>
					</button>
				</div>
			</div>

			<div class="row m-0">
				<div class="col-4 bold">
					<label for="">Nom</label>
				</div>
				<div class="col-2 bold text-center">
					<label for="">Dépôt</label>
				</div>
				<div class="col-2 bold text-center">
					<label for="">Création</label>
				</div>
				<div class="col-4 bold text-center">
					<label for="">Actions</label>
				</div>
			</div>
			<hr class="my-0">
			<div class="row pt-3 m-0" style=" height: calc(73vh - 100px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
				<div class="col-12">
					<?php foreach ($document as $documents) { ?>
						<div class="row apercu_docu" id="rowdoc-<?= $documents->doc_progm_id ?>">
							<div class="col-4 pt-2">
								<div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_progm_id ?>"> <?= $documents->doc_progm_nom ?></div>
							</div>
							<div class="col-2 text-center">
								<label for=""><i class="fas fa-user"></i> <?= $documents->util_prenom ?></label>
							</div>
							<div class="col-2 text-center">
								<label for=""><i class="fas fa-calendar"></i> <?= $documents->doc_progm_creation ?></label>
							</div>
							<div class="col-4 text-center">
								<div class="btn-group fs--1">
									<span class="btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocprogm" data-id="<?= $documents->doc_progm_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
										<i class="far fa-edit"></i>
									</span>&nbsp;
									<span class="btn btn-sm btn-outline-primary rounded-pill">
										<i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_progm_path ?>','<?= $documents->doc_progm_nom ?>')" data-url="<?= base_url($documents->doc_progm_path) ?>">
										</i>
									</span>&nbsp;
									<span class="btn btn-sm btn-outline-danger rounded-pill btnSuppDocprogm" data-id="<?= $documents->doc_progm_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
										<i class="far fa-trash-alt"></i>
									</span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>