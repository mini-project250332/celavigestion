<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0" id="table_programme">
				<thead>
					<!-- <th class="px-2">Numero</th> -->
					<th scope="col">Nom programme</th>
					<th scope="col">Adresse</th>
					<th scope="col">Pourcentage de calucl pour le reporting</th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($programme as $key => $value) : ?>
						<tr id="rowProgramme-<?= $value->progm_id; ?>" class="align-middle">
							<!-- <td>
										<div class="d-flex align-items-center position-relative">
											<div class="flex-1">
												<h6 class="mb-0 fw-semi-bold">
													<a class="stretched-link text-900" href="#"><?= $value->progm_num; ?></a>
												</h6>
											   </div>
										   </div>
									</td> -->
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">
											<a class="stretched-link text-900" href="#">
												<?= $value->progm_nom; ?>
											</a>
										</h6>
									</div>
								</div>
							</td>
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fs--1 mb-0">
											<?= $value->progm_adresse1 . ' ' . ((trim($value->progm_adresse2) != "") ? " / " . $value->progm_adresse2 : "") . ' ' . ((trim($value->progm_adresse3) != "") ? " / " . $value->progm_adresse3 : ""); ?>


											<span class="mb-0 fw-semi-bold">
												<?= $value->progm_cp . ' ' . $value->progm_ville . ' ' . $value->progm_pays; ?>
											</span>
										</p>

									</div>
								</div>
							</td>
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fw-semi-bold">
											<?= isset($value->progm_pourcentage_evaluation) ?  $value->progm_pourcentage_evaluation.'%' : ""; ?>
										</p>
									</div>
								</div>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->progm_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateProgramme" type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
									<button data-id="<?= $value->progm_id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup no_account_manager" type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
	only_visuel_gestion(["1"], "no_account_manager");
	const searchProgramme = document.getElementById('text_filtre_programme');
	const table = document.getElementById('table_programme');

	searchProgramme.addEventListener('keyup', function(event) {
		const searchValue = event.target.value.toLowerCase();
		const rows = table.querySelectorAll('tbody tr');

		rows.forEach(row => {
			const name = row.querySelector('td:first-child').textContent.toLowerCase();
			if (name.includes(searchValue)) {
				row.style.display = '';
			} else {
				row.style.display = 'none';
			}
		});
	});
</script>