<?php echo form_tag('Setting/Gestionnaire/UpdatePourcentage', array('method' => "post", 'class' => '', 'id' => 'UpdatePourcentage')); ?>

<?php if (isset($programme->progm_id)) : ?>
    <input type="hidden" name="progm_id" id="progm_id" value="<?= $programme->progm_id; ?>">
<?php endif; ?>

<input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?= $gestionnaire_id; ?>">


<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class=" px-2">
		<button type="button" data-id="<?=$gestionnaire_id?>" id="annulerUpdatePgm"
			class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" id="save-pourcentage" class="btn btn-sm btn-primary mx-1">
			<i class="fas fa-check me-1"></i>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<div class="contenair-content">
    <div class="row m-0">
        <div class="col"></div>
        <div class="col py-2">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200">Pourcentage : </label>
                            <div class="input-group">
                                <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="progm_pourcentage_evaluation" name="progm_pourcentage_evaluation" style="text-align: right; height : 36px;" value="<?= $programme->progm_pourcentage_evaluation ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                            </div>
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
</div>
<?php echo form_close(); ?>