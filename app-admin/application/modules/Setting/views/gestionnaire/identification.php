<?php echo form_tag('Setting/Gestionnaire/updateInfoGestionnaire',array('method'=>"post", 'class'=>'px-2','id'=>'updateInfoGestionnaire')); ?>
    <div class="contenair-title">
        <div class="px-2">
            <h5 class="fs-0"></h5>
        </div>
        <div class=" px-2">
            <button type="submit" id="save-update-gestionnaire" class="btn btn-sm btn-primary mx-1 d-none">
                <i class="fas fa-check-circle"></i>
                <span> Terminer </span>
            </button>
                    
            <button type="button" data-id="<?=$gestionnaire->gestionnaire_id;?>" id="btn-update-gestionnaire" class="btn btn-sm btn-primary btn-update-gestionnaire">
                <span class="fas fa-edit fas me-1"></span>          
                <span> Modifier </span>
            </button>
        </div>
    </div>

    <input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?=$gestionnaire->gestionnaire_id;?>">

    <div id="fiche-identifiant-gestionnaire">
        
    </div>
<?php echo form_close(); ?>


<script type="text/javascript">
    $(document).ready(function(){
        getFicheIdentificationGestionnaire(<?=$gestionnaire->gestionnaire_id;?>,'fiche');
    });
</script>