<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class=" px-2">
		<div class="d-flex justify-content-end">
			<button id="btnModifyAll" data-id="<?=$gestionnaire_id?>" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Modifier le pourcentage de tous les programmes</button>
		</div>
	</div>
</div>

<div class="table-responsive scrollbar" id="tableLot">
	<table class="table table-sm table-striped fs--1 mb-0">
		<?php if (empty($programme)) : ?>
			<div class="row">
				<div class="col-12 pt-2">
					<div class="text-center">
						Aucun programme n'est lié à cet gestionnaire
					</div>
				</div>
			</div>
		<?php else : ?>
			<thead class="bg-300 text-900">
				<tr>
					<th scope="col">Nom programme</th>
					<th scope="col" style="min-width: 80px;">Pourcentage</th>
					<th class="text-end" scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($programme as $key => $value) : ?>
					<tr id="">
						<td><?= $value->progm_nom ?></td>
						<td><?= isset($value->progm_pourcentage_evaluation) ? $value->progm_pourcentage_evaluation.'%' : "" ?></td>
						<td class="text-end">
							<div>
								<div class="d-flex justify-content-end ">
									<button data-gestionnaire ="<?=$gestionnaire_id?>" data-id="<?= $value->progm_id ?>" class="btn btn-sm btn-outline-warning icon-btn updatePourcentagePgm" type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
								</div>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		<?php endif ?>
	</table>
</div>

<script>
	only_visuel_gestion(["1"], "no_account_manager");
</script>