<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <div class="d-flex justify-content-end">
            <button id="btnFormNewContact" data-id="<?=$gestionnaire_id;?>" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Contact</button>
        </div>
    </div>
</div>

<div class="table-responsive scrollbar" id="tableLot">
    <table class="table table-sm table-striped fs--1 mb-0">
        <thead class="bg-300 text-900">
        <tr>
            <th scope="col">Nom</th>
            <th scope="col" style="min-width: 80px;">Fonction</th>
            <th scope="col" style="min-width: 80px;">Service</th>
            <th scope="col" style="min-width: 80px;">Type contact</th>
            <th scope="col">Téléphones </th>
            <th scope="col">Email </th>
            <th class="text-end" scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($contact as $contacts) {?>
        <tr id="rowContact-<?=$contacts->cnctGestionnaire_id;?>">
            <td> 
                <p class="text-800 fs--1 mb-0"> <?=$contacts->cnctGestionanire_nom.' '.$contacts->cnctGestionnaire_prenom;?> </p>
            </td>
            <td><?= $contacts->cnctGestionnaire_fonction ?></td>
            <td><?= $contacts->cnctGestionnaire_service ?></td>
            <td>
                <?= $contacts->type_cont_libelle ?>
            </td>
            <td>
                <p class="text-800 fs--1 mb-0">
                    <i class="fas fa-phone-alt fa-w-16 text-primary"></i>
                    <span class="px-2"> <?=$contacts->cnctGestionnaire_tel1;?> <?=(trim($contacts->cnctGestionnaire_tel2) !="") ? ' / '.$contacts->cnctGestionnaire_tel2 : '';?></span>
                </p>
                
            </td>
            <td>
                <h6 class="mb-0 fw-semi-bold">
                    <i class="fas fa-envelope fa-w-16 text-primary"></i>
                    <span class="px-2"> <?=$contacts->cnctGestionnaire_email1;?> </span>
                </h6>
            </td>
            <td class="text-end">
                <div>
                    <div class="d-flex justify-content-end ">
                        <button id="updateContact" data-gestionnaire_id="<?=$gestionnaire_id;?>" data-id ="<?= $contacts->cnctGestionnaire_id ?>" class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" type="button">
                            <span class="fas fa-edit fs-1 m-1"></span>
                        </button>
                        <button data-gestionnaire_id="<?=$gestionnaire_id;?>" data-id ="<?= $contacts->cnctGestionnaire_id ?>" class="no_account_manager btn btn-sm btn-outline-danger icon-btn p-0 m-1 deleteContact" type="button">
                            <span class="bi bi-trash fs-1 m-1"></span>
                        </button>
                    </div>
                </div>
            </td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
	only_visuel_gestion(["1"], "no_account_manager");
</script>