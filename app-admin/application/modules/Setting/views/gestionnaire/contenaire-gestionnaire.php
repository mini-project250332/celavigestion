<div id="contenaire-gestionnaire" class="contenair-main contenair-main-sidebar">


</div>

<div class="modal fade" id="modalAjoutDoc" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id=""><i class="fas fa-cloud-upload-alt"></i> Envoi des fichiers</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body" id="contenaireUpload">

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalForm" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content" id="modal-main-content" style="width:600px !important;">

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		pgListeGestionnaire();
	});
</script>