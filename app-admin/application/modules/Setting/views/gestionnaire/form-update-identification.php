<style>
	.contenair-content {
		overflow-x: hidden !important;
	}
</style>
<?php if (isset($gestionnaire->gestionnaire_id)) : ?>
	<input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?= $gestionnaire->gestionnaire_id; ?>">
<?php endif; ?>

<!-- <input type="hidden" name="action" id="action" value="<?= $action ?>"> -->

<div class="contenair-content">
	<div class="row">
		<div class="col-4">
			<div class="col py-2">
				<div class="card rounded-6 h-100">
					<div class="card-body">
						<div class="contenair-title">
							<div class="px-2">
								<h5 class="fs-1 mx-2">Formulaire </h5>
							</div>
							<div class="px-2">

							</div>
						</div>
						<br>
						<div class="form-group inline">
							<div>
								<label data-width="200"> Nom *</label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_nom) ? $gestionnaire->gestionnaire_nom : ""; ?>" id="gestionnaire_nom" name="gestionnaire_nom" data-formatcontrol="true" data-require="true">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Adresse 1 </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_adresse1) ? $gestionnaire->gestionnaire_adresse1 : ""; ?>" id="gestionnaire_adresse1" name="gestionnaire_adresse1">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Adresse 2 </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_adresse2) ? $gestionnaire->gestionnaire_adresse2 : ""; ?>" id="gestionnaire_adresse2" name="gestionnaire_adresse2">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Adresse 3 </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_adresse3) ? $gestionnaire->gestionnaire_adresse3 : ""; ?>" id="gestionnaire_adresse3" name="gestionnaire_adresse3">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Code postal </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_cp) ? $gestionnaire->gestionnaire_cp : ""; ?>" id="gestionnaire_cp" name="gestionnaire_cp">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Ville </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_ville) ? $gestionnaire->gestionnaire_ville : ""; ?>" id="gestionnaire_ville" name="gestionnaire_ville">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline">
							<div>
								<label data-width="200"> Pays </label>
								<input type="text" class="form-control maj" value="<?= isset($gestionnaire->gestionnaire_pays) ? $gestionnaire->gestionnaire_pays : ""; ?>" id="gestionnaire_pays" name="gestionnaire_pays">
								<div class="help"></div>
							</div>
						</div>

						<div class="form-group inline switch">
							<div>
								<label data-width="250"> Statut gestionnaire</label>
								<div class="form-check form-switch">
									<span> Inactif </span>
									<input class="form-check-input" type="checkbox" id="gestionnaire_etat" name="gestionnaire_etat" <?php echo isset($gestionnaire->gestionnaire_etat) ? ($gestionnaire->gestionnaire_etat == "1" ? "checked" : "") : "checked"; ?>>
									<span> Actif </span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col">
			<div class="col py-2">
				<div class="card rounded-6 h-100">
					<div class="card-body">
						<div class="contenair-title">
							<div class="px-2">
								<h5 class="fs-1 mx-2">Documents </h5>
							</div>
							<div class="px-2">
								<button type="button" class="btn btn-sm btn-primary" id="ajoutdocGestionnaire" data-id="<?= $gestionnaire->gestionnaire_id ?>">
									<span class="fas fa-plus me-1"></span>
									<span> Fichier </span>
								</button>
							</div>
						</div>

						<div class="row m-0">
							<div class="col-4 bold">
								<label for="">Nom</label>
							</div>
							<div class="col-2 bold text-center">
								<label for="">Dépôt</label>
							</div>
							<div class="col-2 bold text-center">
								<label for="">Création</label>
							</div>
							<div class="col-4 bold text-center">
								<label for="">Actions</label>
							</div>
						</div>
						<hr class="my-0">
						<div class="row pt-3 m-0" style=" height: calc(73vh - 260px);overflow-y: auto;overflow-x: hidden; text-align:justify;">
							<div class="col-12">
								<?php foreach ($document as $documents) { ?>
									<div class="row apercu_docu" id="rowdoc-<?= $documents->doc_gest_id ?>">
										<div class="col-4 pt-2">
											<div class="fs--1 bold" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor:pointer;color:#2569C3" id="<?= $documents->doc_gest_id ?>"> <?= $documents->doc_gest_nom ?></div>
										</div>
										<div class="col-2 text-center">
											<label for=""><i class="fas fa-user"></i> <?= $documents->util_prenom ?></label>
										</div>
										<div class="col-2 text-center">
											<label for=""><i class="fas fa-calendar"></i> <?= $documents->doc_gest_creation ?></label>
										</div>
										<div class="col-4 text-center">
											<div class="btn-group fs--1">
												<span class="btn btn-sm btn-outline-warning rounded-pill btnUpdtadeDocgest" data-id="<?= $documents->doc_gest_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Renommer">
													<i class="far fa-edit"></i>
												</span>&nbsp;
												<span class="btn btn-sm btn-outline-primary rounded-pill">
													<i class="fas fa-download" onclick="forceDownload('<?= base_url() . $documents->doc_gest_path ?>','<?= $documents->doc_gest_nom ?>')" data-url="<?= base_url($documents->doc_gest_path) ?>">
													</i>
												</span>&nbsp;
												<span class="btn btn-sm btn-outline-danger rounded-pill btnSuppDocgest" data-id="<?= $documents->doc_gest_id ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer">
													<i class="far fa-trash-alt"></i>
												</span>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>