<?php echo form_tag('Setting/Gestionnaire/cruContact', array('method' => "post", 'class' => 'px-2', 'id' => 'cruContact')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class=" px-2">
		<button type="button" data-id="<?= $gestionnaire_id; ?>" id="annulerAddContact"
			class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" id="save-contact" class="btn btn-sm btn-primary mx-1">
			<i class="fas fa-plus me-1"></i>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($contact->cnctGestionnaire_id)): ?>
	<input type="hidden" name="cnctGestionnaire_id" id="cnctGestionnaire_id" value="<?= $contact->cnctGestionnaire_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<input type="hidden" name="gestionnaire_id" id="gestionnaire_id" value="<?= $gestionnaire_id; ?>">

<div class="row m-0">

	<div class="col p-1">
		<div class="card rounded-0 h-100">
			<div class="card-body ">
				<h5 class="fs-1">Identité</h5>
				<hr class="mt-0">
				<div class="mt-4">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom">
								<div class="form-group inline">
									<div class="col-sm-12">
										<label data-width="200"> Nom </label>
										<input type="text" class="form-control maj"
											value="<?= isset($contact->cnctGestionanire_nom) ? $contact->cnctGestionanire_nom : ""; ?>"
											id="cnctGestionanire_nom" name="cnctGestionanire_nom">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Prénom </label>
										<input type="text" class="form-control"
											value="<?= isset($contact->cnctGestionnaire_prenom) ? $contact->cnctGestionnaire_prenom : ""; ?>"
											id="cnctGestionnaire_prenom" name="cnctGestionnaire_prenom">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Fonction </label>
										<input type="text" class="form-control maj"
											value="<?= isset($contact->cnctGestionnaire_fonction) ? $contact->cnctGestionnaire_fonction : ""; ?>"
											id="cnctGestionnaire_fonction" name="cnctGestionnaire_fonction">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Service </label>
										<input type="text" class="form-control maj"
											value="<?= isset($contact->cnctGestionnaire_service) ? $contact->cnctGestionnaire_service : ""; ?>"
											id="cnctGestionnaire_service" name="cnctGestionnaire_service">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="col p-1">

		<div class="card rounded-0 h-100">
			<div class="card-body ">
				<h5 class="fs-1">Contacts</h5>
				<hr class="mt-0">
				<div class="mt-4">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Email 1 </label>
										<input type="text" class="form-control"
											value="<?= isset($contact->cnctGestionnaire_email1) ? $contact->cnctGestionnaire_email1 : ""; ?>"
											id="cnctGestionnaire_email1" name="cnctGestionnaire_email1">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Email 2</label>
										<input type="text" class="form-control"
											value="<?= isset($contact->cnctGestionnaire_email2) ? $contact->cnctGestionnaire_email2 : ""; ?>"
											id="cnctGestionnaire_email2" name="cnctGestionnaire_email2">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Téléphone 1 </label>
										<input v-on:keyup='input_form' type="text" class="form-control"
											value="<?= isset($contact->cnctGestionnaire_tel1) ? $contact->cnctGestionnaire_tel1 : ""; ?>"
											id="cnctGestionnaire_tel1" name="cnctGestionnaire_tel1">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Téléphone 2 </label>
										<input type="text" class="form-control"
											value="<?= isset($contact->cnctGestionnaire_tel2) ? $contact->cnctGestionnaire_tel2 : ""; ?>"
											id="cnctGestionnaire_tel2" name="cnctGestionnaire_tel2">
										<div class="help"></div>
									</div>
								</div>
							</tr>

						</tbody>
					</table>
				</div>
				<hr class="mt-2 mb-0 col-sm-12">

				<div class="form-group inline pt-1">
					<div class="col-sm-12">
						<label data-width="150"> Type </label>
						<select class="form-control form-select fs--1" id="type_cont_id" name="type_cont_id"
							value="<?= isset($contact->type_cont_id) ? $contact->type_cont_id : ""; ?>">
							<?php foreach ($type_contact as $type) { ?>
								<option value="<?= $type->type_cont_id; ?>" <?php if (isset($contact->type_cont_id) && $type->type_cont_id == $contact->type_cont_id)
									  echo "selected"; ?>>
									<?php echo $type->type_cont_libelle; ?>
								</option>
							<?php } ?>
						</select>
						<div class="help"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>