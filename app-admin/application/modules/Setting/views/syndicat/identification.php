<?php echo form_tag('Setting/Syndicat/updateInfoSyndicat',array('method'=>"post", 'class'=>'px-2','id'=>'updateInfoSyndicat')); ?>
    <div class="contenair-title">
        <div class="px-2">
            <h5 class="fs-0"></h5>
        </div>
        <div class=" px-2">
            <button type="submit" id="save-update-syndicat" class="btn btn-sm btn-primary mx-1 d-none">
                <i class="fas fa-check-circle"></i>
                <span> Terminer </span>
            </button>
                    
            <button type="button" data-id="<?=$syndicat->syndicat_id;?>" id="btn-update-syndicat" class="btn btn-sm btn-primary btn-update-syndicat">
                <span class="fas fa-edit fas me-1"></span>          
                <span> Modifier </span>
            </button>
        </div>
    </div>

    <input type="hidden" name="syndicat_id" id="syndicat_id" value="<?=$syndicat->syndicat_id;?>">

    <div id="fiche-identifiant-syndicat">
        
    </div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function(){
        getFicheIdentificationSyndicat(<?=$syndicat->syndicat_id;?>,'fiche');
    });
</script>