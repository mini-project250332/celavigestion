<div class="row m-0">
    <div class="col p-0">
        <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead>
                    <th scope="col">Nom Syndicat</th>
                    <th scope="col">Adresse</th>
                    <th class="text-end">Action</th>
                </thead>
                <tbody>
                    <?php foreach ($syndicat as $key => $value) : ?>
                        <tr id="rowSyndicat-<?= $value->syndicat_id; ?>" class="align-middle">
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <h6 class="mb-0 fw-semi-bold">
                                            <a class="stretched-link text-900" href="#">
                                                <?= $value->syndicat_nom; ?>
                                            </a>
                                        </h6>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->syndicat_adresse1 . ' ' . ((trim($value->syndicat_adresse2) != "") ? " / " . $value->syndicat_adresse2 : "") . ' ' . ((trim($value->syndicat_adresse3) != "") ? " / " . $value->syndicat_adresse3 : ""); ?>
                                            <span class="mb-0 fw-semi-bold">
                                                <?= $value->syndicat_cp . ' ' . $value->syndicat_ville . ' ' . $value->syndicat_pays; ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td class="btn-action-table">
                                <div class="d-flex justify-content-end">
                                    <button data-id="<?= $value->syndicat_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateSyndicat" type="button">
                                        <span class="bi bi-pencil-square fs-1"></span>
                                    </button>
                                    <button data-id="<?= $value->syndicat_id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnConfirmSup no_account_manager" type="button">
                                        <span class="bi bi-trash fs-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    only_visuel_gestion(["1"], "no_account_manager");
</script>