<?php echo form_tag('Setting/Syndicat/cruContact', array('method' => "post", 'class' => 'px-2', 'id' => 'cruContact')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-0"></h5>
	</div>
	<div class=" px-2">
		<button type="button" data-id="<?= $syndicat_id; ?>" id="annulerAddContact" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" id="save-contact" class="btn btn-sm btn-primary mx-1">
			<i class="fas fa-plus me-1"></i>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($contact->cnctSyndicat_id)) : ?>
	<input type="hidden" name="cnctSyndicat_id" id="cnctSyndicat_id" value="<?= $contact->cnctSyndicat_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<input type="hidden" name="syndicat_id" id="syndicat_id" value="<?= $syndicat_id; ?>">

<div class="row m-0">

	<div class="col p-1">
		<div class="card rounded-0 h-100">
			<div class="card-body ">
				<h5 class="fs-1">Identité</h5>
				<hr class="mt-0">
				<div class="mt-4">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom">
								<div class="form-group inline">
									<div class="col-sm-12">
										<label data-width="200"> Nom </label>
										<input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_nom) ? $contact->cnctSyndicat_nom : ""; ?>" id="cnctSyndicat_nom" name="cnctSyndicat_nom">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Prénom </label>
										<input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_prenom) ? $contact->cnctSyndicat_prenom : ""; ?>" id="cnctSyndicat_prenom" name="cnctSyndicat_prenom">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Fonction </label>
										<input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_fonction) ? $contact->cnctSyndicat_fonction : ""; ?>" id="cnctSyndicat_fonction" name="cnctSyndicat_fonction">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="200"> Service </label>
										<input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_service) ? $contact->cnctSyndicat_service : ""; ?>" id="cnctSyndicat_service" name="cnctSyndicat_service">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<hr class="mt-0">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="col p-1">

		<div class="card rounded-0 h-100">
			<div class="card-body ">
				<h5 class="fs-1">Contacts</h5>
				<hr class="mt-0">
				<div class="mt-4">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Email 1 </label>
										<input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_email1) ? $contact->cnctSyndicat_email1 : ""; ?>" id="cnctSyndicat_email1" name="cnctSyndicat_email1">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Email 2</label>
										<input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_email2) ? $contact->cnctSyndicat_email2 : ""; ?>" id="cnctSyndicat_email2" name="cnctSyndicat_email2">
										<div class="help"></div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Téléphone 1 </label>
										<input v-on:keyup='input_form' type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_tel1) ? $contact->cnctSyndicat_tel1 : ""; ?>" id="cnctSyndicat_tel1" name="cnctSyndicat_tel1">
										<div class="help"> </div>
									</div>
								</div>
							</tr>
							<tr class="border-bottom">
								<div class="form-group inline p-0">
									<div class="col-sm-12">
										<label data-width="150"> Téléphone 2 </label>
										<input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_tel2) ? $contact->cnctSyndicat_tel2 : ""; ?>" id="cnctSyndicat_tel2" name="cnctSyndicat_tel2">
										<div class="help"></div>
									</div>
								</div>
							</tr>

						</tbody>
					</table>
				</div>
				<hr class="mt-2 mb-0 col-sm-12">

				<div class="form-group inline pt-1">
					<div class="col-sm-12">
						<label data-width="150"> Type </label>
						<select class="form-control form-select fs--1" id="type_cont_id" name="type_cont_id" value="<?= isset($contact->type_cont_id) ? $contact->type_cont_id : ""; ?>">
							<?php foreach ($type_contact as $type) { ?>
								<option value="<?= $type->type_cont_id; ?>" <?php if (isset($contact->type_cont_id) && $type->type_cont_id == $contact->type_cont_id)
																				echo "selected"; ?>>
									<?php echo $type->type_cont_libelle; ?>
								</option>
							<?php } ?>
						</select>
						<div class="help"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>