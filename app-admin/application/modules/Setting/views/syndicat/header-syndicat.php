<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Liste des Syndics</h5>
	</div>
	<div class="px-2">

		<button id="btnFormAddSyndicat" class="btn btn-sm btn-primary" type="button">
				<span class="bi bi-person-plus-fill me-1"></span>
				Nouveau syndic
		</button>
		
	</div>
</div>

<div id="contenair-list-syndicat" class="contenair-list-syndicat">


</div>

<script type="text/javascript">
	$(document).ready(function () {
    	listeSyndicats();
	});
</script>