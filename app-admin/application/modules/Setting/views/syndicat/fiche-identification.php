<div id="div-fiche-prospect" class="row m-0">
	<div class="col-12 p-0 py-1">
		<div class="row m-0">
			<div class="col-2 p-1">

			</div>
			<div class="col p-1">
				<div class="card rounded-0 h-100">
					<div class="card-body ">
						<h5 class="fs-0">Identité</h5>
						<table class="table table-borderless fs--1 mb-0">
							<tbody>
								<tr class="border-bottom">
									<th class="ps-0">Nom : </th>
									<th class="pe-0 text-end"><?= $syndicat->syndicat_nom ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 1 : </th>
									<th class="pe-0 text-end"><?= $syndicat->syndicat_adresse1 ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 2 : </th>
									<th class="pe-0 text-end" <?= $syndicat->syndicat_adresse2 ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Adresse 3 : </th>
									<th class="pe-0 text-end"><?= $syndicat->syndicat_adresse3 ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Code postal : </th>
									<th class="pe-0 text-end"><?= $syndicat->syndicat_cp ?></th>
								</tr>
								<tr class="border-bottom">
									<th class="ps-0">Ville : </th>
									<th class="pe-0 text-end"><?= $syndicat->syndicat_ville ?></th>
								</tr>
								<tr>
									<th class="ps-0 pb-0">Pays : </th>
									<th class="pe-0 text-end pb-0"><?= $syndicat->syndicat_pays ?></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-2 p-1">

			</div>
		</div>
	</div>
</div>