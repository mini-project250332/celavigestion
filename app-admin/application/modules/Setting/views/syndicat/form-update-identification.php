<?php echo form_tag('Setting/Syndicat/cruSyndicat',array('method'=>"post", 'class'=>'','id'=>'formSyndicat')); ?>
	<?php if(isset($syndicat->syndicat_id)):?>
        <input type="hidden" name="syndicat_id" id="syndicat_id" value="<?=$syndicat->syndicat_id;?>">
    <?php endif;?>
    <!-- <input type="hidden" name="action" id="action" value="<?=$action?>"> -->
	<div class="contenair-content">
		<div class="row m-0">
			<div class="col"></div>

    		<div class="col py-2">

    			<div class="card rounded-0 h-100">
	                <div class="card-body">

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Nom *</label>
		                        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_nom) ? $syndicat->syndicat_nom : "" ;?>" id="syndicat_nom" name="syndicat_nom" data-formatcontrol="true" data-require="true">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 1 </label>
		                        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_adresse1) ? $syndicat->syndicat_adresse1 : "" ;?>" id="syndicat_adresse1" name="syndicat_adresse1">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 2 </label>
		                        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_adresse2) ? $syndicat->syndicat_adresse2 : "" ;?>" id="syndicat_adresse2" name="syndicat_adresse2">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Adresse 3 </label>
		                        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_adresse3) ? $syndicat->syndicat_adresse3 : "" ;?>" id="syndicat_adresse3" name="syndicat_adresse3">
		                        <div class="help"></div>
		                    </div>
		                </div>

		                <div class="form-group inline">
						    <div>
						        <label data-width="200"> Code postal </label>
						        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_cp) ? $syndicat->syndicat_cp : "" ;?>" id="syndicat_cp" name="syndicat_cp">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Ville </label>
						        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_ville) ? $syndicat->syndicat_ville : "" ;?>" id="syndicat_ville" name="syndicat_ville">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline">
						    <div>
						        <label data-width="200"> Pays </label>
						        <input type="text" class="form-control maj" value ="<?= isset($syndicat->syndicat_pays) ? $syndicat->syndicat_pays : "" ;?>" id="syndicat_pays" name="syndicat_pays">
						        <div class="help"></div>
						    </div>
						</div>

						<div class="form-group inline switch">
	                        <div>
	                            <label data-width="250"> Statut syndic</label>
	                            <div class="form-check form-switch">
	                                <span> Inactif </span>
	                                <input class="form-check-input" type="checkbox" id="syndicat_etat" name="syndicat_etat"  <?php echo isset($syndicat->syndicat_etat) ? ($syndicat->syndicat_etat=="1" ? "checked" :"") : "checked" ;?>>
	                                <span> Actif </span>
	                            </div>
	                        </div>
	                    </div>


		            </div>
		        </div>
    		</div>
    		<div class="col"></div>
    	</div>
    </div>
<?php echo form_close(); ?>