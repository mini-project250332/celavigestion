<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Fiche Syndicat : <?=$syndicat->syndicat_nom ?> </h5>
	</div>
	<div class="px-2">
		<button id="retourListeSyndicat" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-undo me-1"></i>
			<span>Retour</span>
		</button>
	</div>
</div>

<div class="contenair-content">
	
	<ul class="nav nav-tabs" id="prospectTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="identification-tab" data-bs-toggle="tab" href="#tab-identification" role="tab" aria-controls="tab-identification" aria-selected="true">Identification</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#tab-contact" role="tab" aria-controls="tab-contact" aria-selected="false">Contact</a>
		</li>
	</ul>

	<div class="tab-content border-x border-bottom p-1" id="prospectTabContent">
		<div class="tab-pane fade show active" id="tab-identification" role="tabpanel" aria-labelledby="identification-tab">
			<?php $this->load->view('identification'); ?>
		</div>
		<div class="tab-pane fade" id="tab-contact" role="tabpanel" aria-labelledby="lot-tab">
			<?php $this->load->view('contact'); ?>
		</div>
	</div>

</div>