<?php echo form_tag('Setting/Syndicat/cruSyndicat', array('method' => "post", 'class' => '', 'id' => 'formSyndicat')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Formulaire </h5>
    </div>
    <div class="px-2">

        <button type="button" id="btnAnnulerSyndicat" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler </span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary">
            <span class="fas fa-plus me-1"></span>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<?php if (isset($syndicat->syndicat_id)) : ?>
    <input type="hidden" name="syndicat_id" id="syndicat_id" value="<?= $syndicat->syndicat_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
    <!-- With contact form -->
    <div class="row">
        <div class="col">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <h5 class="fs-1 text-center pb-2">Informations du syndic</h5>
                    <hr class="mt-0">

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Nom *</label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_nom) ? $syndicat->syndicat_nom : ""; ?>" id="syndicat_nom" name="syndicat_nom" data-formatcontrol="true" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 1 </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_adresse1) ? $syndicat->syndicat_adresse1 : ""; ?>" id="syndicat_adresse1" name="syndicat_adresse1">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 2 </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_adresse2) ? $syndicat->syndicat_adresse2 : ""; ?>" id="syndicat_adresse2" name="syndicat_adresse2">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 3 </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_adresse3) ? $syndicat->syndicat_adresse3 : ""; ?>" id="syndicat_adresse3" name="syndicat_adresse3">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Code postal </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_cp) ? $syndicat->syndicat_cp : ""; ?>" id="syndicat_cp" name="syndicat_cp">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Ville </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_ville) ? $syndicat->syndicat_ville : ""; ?>" id="syndicat_ville" name="syndicat_ville">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Pays </label>
                            <input type="text" class="form-control maj" value="<?= isset($syndicat->syndicat_pays) ? $syndicat->syndicat_pays : ""; ?>" id="syndicat_pays" name="syndicat_pays">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline switch">
                        <div>
                            <label data-width="250"> Statut syndic</label>
                            <div class="form-check form-switch">
                                <span> Inactif </span>
                                <input class="form-check-input" type="checkbox" id="syndicat_etat" name="syndicat_etat" <?php echo isset($syndicat->syndicat_etat) ? ($syndicat->syndicat_etat == "1" ? "checked" : "") : "checked"; ?>>
                                <span> Actif </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Identité contact -->
        <div class="col">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <h5 class="fs-1 text-center pb-2">Informations du contact</h5>
                    <hr class="mt-0">

                    <div class="form-group inline">
                        <div class="col-sm-12">
                            <label data-width="200"> Nom * </label>
                            <input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_nom) ? $contact->cnctSyndicat_nom : ""; ?>" id="cnctSyndicat_nom" name="cnctSyndicat_nom" data-formatcontrol="true" data-require="true">
                            <div class="help"></div>
                        </div>

                        <div class="form-group inline p-0">
                            <div class="col-sm-12">
                                <label data-width="200"> Prénom </label>
                                <input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_prenom) ? $contact->cnctSyndicat_prenom : ""; ?>" id="cnctSyndicat_prenom" name="cnctSyndicat_prenom">
                                <div class="help"> </div>
                            </div>
                        </div>

                        <div class="form-group inline p-0">
                            <div class="col-sm-12">
                                <label data-width="200"> Fonction </label>
                                <input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_fonction) ? $contact->cnctSyndicat_fonction : ""; ?>" id="cnctSyndicat_fonction" name="cnctSyndicat_fonction">
                                <div class="help"> </div>
                            </div>
                        </div>

                        <div class="form-group inline p-0">
                            <div class="col-sm-12">
                                <label data-width="200"> Service </label>
                                <input type="text" class="form-control maj" value="<?= isset($contact->cnctSyndicat_service) ? $contact->cnctSyndicat_service : ""; ?>" id="cnctSyndicat_service" name="cnctSyndicat_service">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <!-- Info contact -->
        <div class="col">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <h5 class="fs-1 text-center pb-2">Détails du contact</h5>
                    <hr class="mt-0">

                    <div class="form-group inline p-0">
                        <div class="col-sm-12">
                            <label data-width="150"> Email 1 * </label>
                            <input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_email1) ? $contact->cnctSyndicat_email1 : ""; ?>" id="cnctSyndicat_email1" name="cnctSyndicat_email1" data-formatcontrol="true" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline p-0">
                        <div class="col-sm-12">
                            <label data-width="150"> Email 2</label>
                            <input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_email2) ? $contact->cnctSyndicat_email2 : ""; ?>" id="cnctSyndicat_email2" name="cnctSyndicat_email2">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline p-0">
                        <div class="col-sm-12">
                            <label data-width="150"> Téléphone 1 </label>
                            <input v-on:keyup='input_form' type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_tel1) ? $contact->cnctSyndicat_tel1 : ""; ?>" id="cnctSyndicat_tel1" name="cnctSyndicat_tel1">
                            <div class="help"> </div>
                        </div>
                    </div>

                    <div class="form-group inline p-0">
                        <div class="col-sm-12">
                            <label data-width="150"> Téléphone 2 </label>
                            <input type="text" class="form-control" value="<?= isset($contact->cnctSyndicat_tel2) ? $contact->cnctSyndicat_tel2 : ""; ?>" id="cnctSyndicat_tel2" name="cnctSyndicat_tel2">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline p-0">
                        <div class="col-sm-12">
                            <label data-width="150"> Type </label>
                            <select class="form-control form-select fs--1" id="type_cont_id" name="type_cont_id" value="<?= isset($contact->type_cont_id) ? $contact->type_cont_id : ""; ?>">
                                <?php foreach ($type_contact as $type) { ?>
                                    <option value="<?= $type->type_cont_id; ?>" <?php if (isset($contact->type_cont_id) && $type->type_cont_id == $contact->type_cont_id)
                                                                                    echo "selected"; ?>>
                                        <?php echo $type->type_cont_libelle; ?>
                                    </option>
                                <?php } ?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>