<div class="row m-0">
    <div class="col p-0">
        <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead>
                    <th scope="col">Nom Banque</th>
                    <th scope="col">Numéro compte</th>
                    <th scope="col">Iban</th>
                    <th scope="col">BIC</th>
                    <th scope="col">Numéro compte comptable</th>
                    <th scope="col">Journal</th>
                    <th scope="col">Status</th>
                    <th class="text-end">Actions</th>
                </thead>
                <tbody>
                    <?php foreach ($banque as $key => $value) : ?>
                        <tr id="rowBanque-<?= $value->banque_id; ?>" class="align-middle">
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <h6 class="mb-0 fw-semi-bold">
                                            <?= $value->banque_nom; ?>
                                        </h6>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->banque_num_compte; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->banque_iban; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->banque_bic; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->banque_cpt_comptable; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->banque_code_journal; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= ($value->banque_etat == 1) ? "Actif" : "Inactif"; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td class="btn-action-table">
                                <div class="d-flex justify-content-end">
                                    <button data-id="<?= $value->banque_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateBanque" type="button">
                                        <span class="bi bi-pencil-square fs-1"></span>
                                    </button>
                                    <button data-id="<?= $value->banque_id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnSupBanque" type="button">
                                        <span class="bi bi-trash fs-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>