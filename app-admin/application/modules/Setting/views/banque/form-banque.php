<?php echo form_tag('Setting/Banque/cruBanque', array('method' => "post", 'class' => '', 'id' => 'cruBanque')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire </h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnulerBanque" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-check me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($banque->banque_id)) : ?>
	<input type="hidden" name="banque_id" id="banque_id" value="<?= $banque->banque_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">			
					<div class="form-group inline">
						<div>
							<label data-width="200"> Nom *</label>
							<input type="text" class="form-control maj" value="<?= isset($banque->banque_nom) ? $banque->banque_nom : ""; ?>" id="banque_nom" name="banque_nom" data-formatcontrol="true" data-require="true">
							<div class="help" id="error_name"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Numéro de compte </label>
							<input type="text" class="form-control" value="<?= isset($banque->banque_num_compte) ? $banque->banque_num_compte : ""; ?>" id="banque_num_compte" name="banque_num_compte">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> IBAN </label>
							<input type="text" class="form-control" value="<?= isset($banque->banque_iban) ? $banque->banque_iban : ""; ?>" id="banque_iban" name="banque_iban">
							<div class="help error_file text-danger"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> BIC </label>
							<input type="text" class="form-control" value="<?= isset($banque->banque_bic) ? $banque->banque_bic : ""; ?>" id="banque_bic" name="banque_bic">
							<div class="help"></div>
						</div>
					</div>

                    <div class="form-group inline">
						<div>
							<label data-width="200"> Compte comptable </label>
							<input type="text" class="form-control" value="<?= isset($banque->banque_cpt_comptable) ? $banque->banque_cpt_comptable : ""; ?>" id="banque_cpt_comptable" name="banque_cpt_comptable">
							<div class="help"></div>
						</div>
					</div>	

					<div class="form-group inline">
						<div>
							<label data-width="200"> Code journal </label>
							<input type="text" class="form-control" value="<?= isset($banque->banque_code_journal) ? $banque->banque_code_journal : ""; ?>" id="banque_code_journal" name="banque_code_journal">
							<div class="help"></div>
						</div>
					</div>				

					<div class="form-group inline switch">
						<div>
							<label data-width="250"> Statut banque</label>
							<div class="form-check form-switch">
								<span> Inactif </span>
								<input class="form-check-input" type="checkbox" id="banque_etat" name="banque_etat" <?php echo isset($banque->banque_etat) ? ($banque->banque_etat == "1" ? "checked" : "") : "checked"; ?>>
								<span> Actif </span>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>