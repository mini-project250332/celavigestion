<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Adresse de CELAVI Gestion</h5>
	</div>
	<div class="px-2">
		<button id="btnFormodifier" class="btn btn-sm btn-primary" type="button">
			<span class="fas fa-edit me-1"></span>
			Modifier
		</button>
	</div>
</div>

<div class="col-12 p-0 py-1">
	<div class="row m-0">
		<div class="col-2"></div>
		<div class="col p-4">
			<div class="card rounded-0 h-100">
				<div class="card-body ">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom border-top">
								<th class="ps-0">Nom entreprise :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->nom_entreprise : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Adresse 1 :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->adresse1 : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Adresse 2 :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->adresse2 : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Adresse 3 :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->adresse3 : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Code postal :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->cp : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Ville :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->ville : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Pays :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->pays : '' ?></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-2"></div>
	</div>
</div>