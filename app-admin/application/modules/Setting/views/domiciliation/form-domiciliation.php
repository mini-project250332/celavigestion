<?php echo form_tag('Setting/Domiciliation/updateEntreprise', array('method' => "post", 'class' => '', 'id' => 'updateEntreprise')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Adresse de CELAVI Gestion</h5>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-primary" type="submit">
            <span class="fas fa-check me-1"></span>
            Terminée
        </button>
    </div>
</div>

<div class="col-12 p-0 py-1">
    <div class="row m-0">
        <div class="col-2"></div>
        <div class="col p-4">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Nom de l'entreprise * :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) && $entreprise->nom_entreprise != '' ? $entreprise->nom_entreprise : 'CELAVI Gestion' ?>" name="nom_entreprise" id="nom_entreprise" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 1 * :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->adresse1 : '' ?>" name="adresse1" id="adresse1" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 2 :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->adresse2 : '' ?>" name="adresse2" id="adresse2">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Adresse 3 :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->adresse3 : '' ?>" name="adresse3" id="adresse3">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Code Postal * :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->cp : '' ?>" name="cp" id="cp" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Ville :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->ville : '' ?>" name="ville" id="ville">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Pays * :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->pays : '' ?>" name="pays" id="pays" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<?php echo form_close(); ?>