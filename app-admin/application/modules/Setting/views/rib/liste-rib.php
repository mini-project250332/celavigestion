<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th>IBAN</th>
					<th>BIC</th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($rib as $key => $value) : ?>
						<tr id="rowRib-<?= $value->rib_id; ?>" class="align-middle">
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<?= formatIBAN($value->rib_iban); ?>
									</div>
								</div>
							</td>
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fs--1 mb-0">
											<?= $value->rib_bic; ?>
										</p>
									</div>
								</div>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->rib_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateRib" type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
