<?php echo form_tag('Setting/Rib/cruRib', array('method' => "post", 'class' => '', 'id' => 'cruRib')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire d'ajout d'un RIB CELAVI</h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnulerRib" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($rib->rib_id)) : ?>
	<input type="hidden" name="rib_id" id="rib_id" value="<?= $rib->rib_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<br>
<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<div class="form-group inline">
						<div>
							<label data-width="200"> IBAN </label>
							<input type="text" class="form-control maj" value="<?= isset($rib->rib_iban) ? $rib->rib_iban : ""; ?>" id="rib_iban" name="rib_iban" autocomplete="off">
							<div class="help help_iban"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> BIC </label>
							<input type="text" class="form-control maj" value="<?= isset($rib->rib_bic) ? $rib->rib_bic : ""; ?>" id="rib_bic" name="rib_bic">
							<div class="help help_bic"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>