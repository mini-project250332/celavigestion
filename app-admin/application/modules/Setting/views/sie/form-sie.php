<?php echo form_tag('Setting/Sie/cruSie', array('method' => "post", 'class' => '', 'id' => 'cruSie')); ?>
<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Formulaire d'ajout d'un SIE</h5>
	</div>
	<div class="px-2">

		<button type="button" id="btnAnnulerSie" class="btn btn-sm btn-secondary mx-1">
			<i class="fas fa-times me-1"></i>
			<span> Annuler </span>
		</button>

		<button type="submit" class="btn btn-sm btn-primary">
			<span class="fas fa-plus me-1"></span>
			<span> Enregistrer </span>
		</button>
	</div>
</div>

<?php if (isset($sie->sie_id)) : ?>
	<input type="hidden" name="sie_id" id="sie_id" value="<?= $sie->sie_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
	<div class="row m-0">
		<div class="col"></div>
		<div class="col py-2">
			<div class="card rounded-0 h-100">
				<div class="card-body">
					<div class="form-group inline">
						<div>
							<label data-width="200"> Libellé *</label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_libelle) ? $sie->sie_libelle : ""; ?>" id="sie_libelle" name="sie_libelle" required>
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline">
						<div>
							<label data-width="200"> Email </label>
							<input type="text" class="form-control" value="<?= isset($sie->sie_email) ? $sie->sie_email : ""; ?>" id="sie_email" name="sie_email">
							<div class="help"></div>
						</div>
					</div>
					<div class="form-group inline">
						<div>
							<label data-width="200"> Téléphone </label>
							<input type="text" class="form-control " value="<?= isset($sie->sie_tel) ? $sie->sie_tel : ""; ?>" id="sie_tel" name="sie_tel">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 1 *</label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_adresse1) ? $sie->sie_adresse1 : ""; ?>" id="sie_adresse1" name="sie_adresse1">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 2 </label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_adresse2) ? $sie->sie_adresse2 : ""; ?>" id="sie_adresse2" name="sie_adresse2">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Adresse 3 </label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_adresse3) ? $sie->sie_adresse3 : ""; ?>" id="sie_adresse3" name="sie_adresse3">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Code postal *</label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_cp) ? $sie->sie_cp : ""; ?>" id="sie_cp" name="sie_cp" required>
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Ville *</label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_ville) ? $sie->sie_ville : ""; ?>" id="sie_ville" name="sie_ville" required>
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> Pays </label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_pays) ? $sie->sie_pays : ""; ?>" id="sie_pays" name="sie_pays">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<div>
							<label data-width="200"> RIB / IBAN </label>
							<input type="text" class="form-control maj" value="<?= isset($sie->sie_rib) ? $sie->sie_rib : ""; ?>" id="sie_rib" name="sie_rib" autocomplete="off">
							<div class="help"></div>
						</div>
					</div>

					<div class="form-group inline">
						<?php if (isset($sie->file_rib_sie) && $sie->file_rib_sie != NULL) :
							$file_rib = explode("/", $sie->file_rib_sie);
						?>
							<div class="doc_sie" style="margin-bottom: -15px;">
								<span class="badge rounded-pill badge-soft-secondary ms-9" onclick="forceDownload('<?= base_url() . $sie->file_rib_sie ?>')" data-url="<?= base_url($sie->file_rib_sie) ?>" style="cursor: pointer; height: 21px;"><?= $file_rib[3] ?></span>
								<span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefilesie" style="cursor: pointer;">.</span>
							</div>
						<?php endif ?>
						<div>
							<label data-width="200">Fichier RIB </label>
							<input type="file" class="form-control maj" id="sie_file_rib" name="sie_file_rib">
							<div class="help"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col"></div>
	</div>
</div>
<?php echo form_close(); ?>

<script>
	$(document).ready(function() {
		$('#sie_rib').keyup(function() {
			$(this).val($(this).val().toUpperCase());

			if ($(this).val() && verifyIBAN($(this).val()) == false) {
				if (timer != null) {
					clearTimeout(timer);
					timer = null;
				}
				timer = setTimeout(() => {
					alert_rib_Iban();
				}, 1000);
			}
		});
	});
</script>