<div class="row mt-3 ms-1 me-2 mb-0">

	<div class="col-8">
		<h5 class="fs-1">Liste des SIE</h5>
	</div>
	<div class="col">
		<div class="row align-input-button">
			<div class="col">
				<input type="text" class="form-control" id="recherche_sie" placeholder="Recherche">
			</div>
			<div class="col-auto">
				<button id="btnFormAddSie" class="btn btn-sm btn-primary" type="button">
					<span class="bi bi-person-plus-fill me-1"></span>
					Nouveau SIE
				</button>
			</div>
		</div>
	</div>
	<hr>
</div>

<div id="contenair-list" class="contenair-list" style="height: 89%">

</div>

<script type="text/javascript">
	$(document).ready(function() {
		listeSie();
	});
</script>