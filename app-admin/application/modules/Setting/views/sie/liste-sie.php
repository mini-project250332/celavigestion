<div class="row m-0">
    <div class="col p-0">
        <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0" id="tableSie">
                <thead>
                    <th scope="col">Libellé</th>
                    <th>Contact</th>
                    <th scope="col">Adresse</th>
                    <th scope="col" width="15%">Date de la modification</th>
                    <th class="text-end">Action</th>
                </thead>
                <tbody>
                    <?php foreach ($sie as $key => $value) : ?>
                        <tr id="rowSie-<?= $value->sie_id; ?>" class="align-middle">
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <h6 class="mb-0 fw-semi-bold">
                                            <?= $value->sie_libelle ?>
                                        </h6>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->sie_tel . ' ' . ((trim($value->sie_email) != "") ? " / " . $value->sie_email : ""); ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->sie_adresse1 . ' ' . ((trim($value->sie_adresse2) != "") ? " / " . $value->sie_adresse2 : "") . ' ' . ((trim($value->sie_adresse3) != "") ? " / " . $value->sie_adresse3 : ""); ?>
                                            <span class="mb-0 fw-semi-bold">
                                                <?= $value->sie_cp . ' ' . $value->sie_ville . ' ' . $value->sie_pays; ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?php if ($value->date_maj != NULL) : ?>
                                                <?= date('d/m/Y H:i:s',strtotime($value->date_maj)) ?>
                                            <?php endif ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <?php
                            $dossier_id = "";
                            if (!empty($dossier)) {
                                foreach ($dossier as $key => $value_dossier) {
                                    $dossier_id = $value_dossier->sie_id;
                                }
                            }
                            ?>
                            <td class="btn-action-table">
                                <div class="d-flex justify-content-end">
                                    <button data-id="<?= $value->sie_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateSie" type="button">
                                        <span class="bi bi-pencil-square fs-1"></span>
                                    </button>
                                    <button data-id="<?= $value->sie_id; ?>" class="btn btn-sm btn-outline-danger icon-btn <?= $dossier_id == $value->sie_id ? 'alertSuppSie' : 'btnSuppSie' ?> " type="button">
                                        <span class="bi bi-trash fs-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        const searchName = document.getElementById('recherche_sie');
        const table = document.getElementById('tableSie');
        searchName.addEventListener('keyup', function(event) {
            const searchValue = event.target.value.toLowerCase();
            const rows = table.querySelectorAll('tbody tr');

            rows.forEach(row => {
                const name = row.querySelector('td:first-child').textContent.toLowerCase();
                if (name.includes(searchValue)) {
                    row.style.display = '';
                } else {
                    row.style.display = 'none';
                }
            });
        });
    });
</script>