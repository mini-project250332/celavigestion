<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th scope="col">Nom CGP</th>
                    <th>Contact</th>
					<th scope="col">Adresse</th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($cgp as $key => $value): ?>
						<tr id="rowCgp-<?= $value->cgp_id; ?>" class="align-middle">							
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">											
                                            <?= $value->cgp_nom .' '.$value->cgp_prenom; ?>											
										</h6>
									</div>
								</div>
							</td>
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fs--1 mb-0">
											<?= $value->cgp_tel . ' ' . ((trim($value->cgp_email) != "") ? " / " . $value->cgp_email : ""); ?>											
										</p>
									</div>
								</div>
							</td>
                            <td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<p class="text-800 fs--1 mb-0">
											<?= $value->cgp_adresse1 . ' ' . ((trim($value->cgp_adresse2) != "") ? " / " . $value->cgp_adresse2 : "") . ' ' . ((trim($value->cgp_adresse3) != "") ? " / " . $value->cgp_adresse3 : ""); ?>
											<span class="mb-0 fw-semi-bold">
												<?= $value->cgp_cp . ' ' . $value->cgp_ville . ' ' . $value->cgp_pays; ?>
											</span>
										</p>
									</div>
								</div>
							</td>
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->cgp_id; ?>"
										class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateCgp"
										type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
									<button data-id="<?= $value->cgp_id; ?>"
										class="btn btn-sm btn-outline-danger icon-btn btnSupCgp"
										type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- <script>
	only_visuel_gestion(["1"], "no_account_manager");
</script> -->