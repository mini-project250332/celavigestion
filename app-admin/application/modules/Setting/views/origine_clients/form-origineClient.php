<?php echo form_tag('Setting/OrigineClient/cruOrigineClient',array('method'=>"post", 'class'=>'px-2','id'=>'formOrigineClient')); ?>

    <div class="contenair-title">
        <div class="px-2">
            <h5 class="fs-0"></h5>
        </div>
        <div class="px-2">
            <button type="button" id="btnAnnulerOrigineClient" class="btn btn-sm btn-secondary mx-1">
                <i class="fas fa-times me-1"></i>
                <span> Annuler</span>
            </button>

            <button type="submit" class="btn btn-sm btn-primary mx-1">
                <i class="fas fa-plus me-1"></i>
                <span> Enregistrer </span>
            </button>
        </div>
    </div>

    <?php if(isset($origine_cli->origine_cli_id)):?>
        <input type="hidden" name="origine_cli_id" id="origine_cli_id" value="<?=$origine_cli->origine_cli_id;?>">
    <?php endif;?>

    <input type="hidden" name="action" id="action" value="<?=$action?>">

    <div class="row m-0">
        <div class="col">
            

        </div>
        <div class="col py-2">

            <div class="card rounded-0 h-100">
                <div class="card-body">

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Libelle  * </label>
                            <input type="text" class="form-control maj" value ="<?= isset($origine_cli->origine_cli_libelle) ? $origine_cli->origine_cli_libelle : "" ;?>" id="origine_cli_libelle" name="origine_cli_libelle" data-formatcontrol="true" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>


                    <div class="form-group inline switch">
                        <div>
                            <label data-width="250"> Statut </label>
                            <div class="form-check form-switch">
                                <span> Inactif </span>
                                <input class="form-check-input" type="checkbox" id="origine_cli_etat" name="origine_cli_etat"  <?php echo isset($origine_cli->origine_cli_etat) ? ($origine_cli->origine_cli_etat=="1" ? "checked" :"") : "checked" ;?>>
                                <span> Actif </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col">
            

        </div>
    </div>
<?php echo form_close(); ?>


