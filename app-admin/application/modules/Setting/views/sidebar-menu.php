<div class="sidebar-menu">
    <ul class="list-unstyled">
        <li class="item-sidebar btn_limit_access">
            <a class="item-collapsed" data-bs-toggle="collapse" data-bs-target="#item-entreprise" aria-expanded="<?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['domiciliation', 'fiscalite', 'data-rib', 'data-temps'])) ? 'true' : 'false'; ?>">
                <i class="bi bi-building"></i>
                <span> Entreprise</span>
            </a>
            <div class="collapse <?php if (isset($menu_sidebar))
                                        echo (in_array($menu_sidebar, ['domiciliation', 'fiscalite', 'data-rib', 'data-temps'])) ? 'show' : ''; ?>" id="item-entreprise">
                <ul class="btn-toggle-nav list-unstyled">
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'domiciliation') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Domiciliation'); ?>" class="">
                            <span>Domiciliation</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'fiscalite') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Fiscalite'); ?>" class="">
                            <span>Fiscalité</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-rib') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Rib'); ?>" class="">
                            <span>RIB CELAVI</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-temps') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/TypeSaisieTemps'); ?>" class="">
                            <span>Type temps saisis</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="item-sidebar">
            <a class="item-collapsed" data-bs-toggle="collapse" data-bs-target="#item-data" aria-expanded="<?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['data-indexation', 'data-cgp', 'data-sie', 'data-programme', 'data-gestionnaire', 'data-origine-client', 'data-origine-demande', 'data-produit'])) ? 'true' : 'false'; ?>">
                <i class="bi bi-server fs-2"></i>
                <span> Gestion</span>
            </a>
            <div class="collapse <?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['data-syndicat', 'data-indexation', 'data-cgp', 'data-sie', 'data-programme', 'data-gestionnaire', 'data-origine-client', 'data-origine-demande', 'data-produit'])) ? 'show' : ''; ?>" id="item-data">
                <ul class="btn-toggle-nav list-unstyled">

                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-programme') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Programme'); ?>" class="">
                            <span>Programmes</span>
                        </a>
                    </li>

                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-gestionnaire') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Gestionnaire'); ?>" class="">
                            <span>Gestionnaires</span>
                        </a>
                    </li>

                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-syndicat') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Syndicat'); ?>" class="">
                            <span>Syndic de copropriété</span>
                        </a>
                    </li>

                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-cgp') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Cgp'); ?>" class="">
                            <span>CGP</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-sie') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Sie'); ?>" class="">
                            <span>SIE</span>
                        </a>
                    </li>
                    <li class="btn_limit_access <?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-origine-client') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/OrigineClient'); ?>" class="">
                            <span>Origines clients</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-origine-demande') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/OrigineDemande'); ?>" class="">
                            <span>Origines demandes</span>
                        </a>
                    </li>

                    <li class="btn_limit_access <?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-produit') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Produit'); ?>" class="">
                            <span>Produits / Packs</span>
                        </a>
                    </li>
                    <li class="btn_limit_access <?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-indexation') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Indexation'); ?>" class="">
                            <span>Indexation</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="item-sidebar">
            <a class="item-collapsed" data-bs-toggle="collapse" data-bs-target="#item-messagerie" aria-expanded="<?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['data-mail', 'data-model', 'data-accueil', 'data-sms', 'data-accueil-intranet'])) ? 'true' : 'false'; ?>">
                <i class="fa fa-envelope fs-2" aria-hidden="true"></i> &nbsp;
                <span> Communication </span>
            </a>
            <div class="collapse <?php if (isset($menu_sidebar))
                                        echo (in_array($menu_sidebar, ['data-mail', 'data-model', 'data-accueil', 'data-sms', 'data-accueil-intranet'])) ? 'show' : ''; ?>" id="item-messagerie">
                <ul class="btn-toggle-nav list-unstyled">
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-accueil') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Accueil'); ?>" class="">
                            <span>Message d'accueil CELAVI</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-accueil-intranet') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Accueil_Intranet'); ?>" class="">
                            <span>Message d'accueil Intranet</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'data-model') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Modelmail'); ?>" class="">
                            <span>Modèles de mail</span>
                        </a>
                    </li>
                    <li class="btn_limit_access <?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'data-sms') ? 'active' : ''; ?>">
                        <a href=" " class="">
                            <span>Modèle de SMS</span>
                        </a>
                    </li>
                    <!-- <li class="<?php if (isset($menu_sidebar))
                                        echo ($menu_sidebar == 'data-mail') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/ParamMail'); ?>" class="">
                            <span>Paramètre du compte mail</span>
                        </a>
                    </li> -->
                </ul>
            </div>
        </li>

        <li class="item-sidebar btn_limit_access access_comptable_true">
            <a class="item-collapsed" data-bs-toggle="collapse" data-bs-target="#item-comptable" aria-expanded="<?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['articles', 'banque', 'mode-reglement', 'mode-reglement', 'journaux-comptable', 'data-ics'])) ? 'true' : 'false'; ?>">
                <i class="bi bi-bank"></i>
                <span> Comptabilité</span>
            </a>
            <div class="collapse <?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['article', 'banque', 'mode-reglement', 'journaux-comptable', 'data-ics'])) ? 'show' : ''; ?>" id="item-comptable">
                <ul class="btn-toggle-nav list-unstyled">
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'article') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Article'); ?>" class="">
                            <span>Articles</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar)) echo ($menu_sidebar == 'banque') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Banque'); ?>" class="">
                            <span>Banques </span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'data-ics') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Ics'); ?>" class="">
                            <span>ICS</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'mode-reglement') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Mode_reglement'); ?>" class="">
                            <span>Modes de règlement</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'journaux-comptable') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/Journaux_comptable'); ?>" class="">
                            <span>Journaux comptables</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="item-sidebar">
            <a class="item-collapsed" data-bs-toggle="collapse" data-bs-target="#item-utlilsateur" aria-expanded="<?php if (isset($menu_sidebar)) echo (in_array($menu_sidebar, ['gestion-users', 'historique-acces'])) ? 'true' : 'false'; ?>">
                <i class="bi bi-people"></i>
                <span> Utilisateurs</span>
            </a>
            <div class="collapse <?php if (isset($menu_sidebar))
                                        echo (in_array($menu_sidebar, ['gestion-users', 'historique-acces'])) ? 'show' : ''; ?>" id="item-utlilsateur">
                <ul class="btn-toggle-nav list-unstyled">
                    <li class="btn_limit_access <?php if (isset($menu_sidebar))
                                                    echo ($menu_sidebar == 'gestion-users') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting/GestionUsers'); ?>" class="">
                            <span>Gestion des utilisateurs</span>
                        </a>
                    </li>
                    <li class="<?php if (isset($menu_sidebar))
                                    echo ($menu_sidebar == 'historique-acces') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('Setting'); ?>" class="">
                            <span>Historique d'accès</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>

<script>
    acces_comptable_only();
</script>