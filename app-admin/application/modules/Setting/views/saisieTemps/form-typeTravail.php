<?php echo form_tag('Setting/TypeSaisieTemps/cruTypeTravail',array('method'=>"post", 'class'=>'px-2','id'=>'cruTypeTravail')); ?>

    <div class="contenair-title">
        <div class="px-2">
            <h5 class="fs-0"></h5>
        </div>
        <div class="px-2">
            <button type="button" id="annulerAjoutTravail" class="btn btn-sm btn-secondary mx-1">
                <i class="fas fa-times me-1"></i>
                <span> Annuler</span>
            </button>

            <button type="submit" class="btn btn-sm btn-primary mx-1">
                <i class="fas fa-plus me-1"></i>
                <span> Enregistrer </span>
            </button>
        </div>
    </div>

    <?php if(isset($type_travail_id->type_travail_id)):?>
        <input type="hidden" name="type_travail_id" id="type_travail_id" value="<?=$type_travail_id->type_travail_id;?>">
    <?php endif;?>

    <input type="hidden" name="action" id="action" value="<?=$action?>">

    <div class="row m-0">
        <div class="col">
            

        </div>
        <div class="col py-2">

            <div class="card rounded-0 h-100">
                <div class="card-body">

                    <div class="form-group inline">
                        <div>
                            <label data-width="250"> Libelle  * </label>
                            <input type="text" class="form-control" value ="<?= isset($type_travail_id->type_travail_libelle) ? $type_travail_id->type_travail_libelle : "" ;?>" id="type_travail_libelle" name="type_travail_libelle" data-formatcontrol="true" data-require="true">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Texte sur le reporting </label>
                            <textarea class="form-control" id="type_traivail_texte" name="type_traivail_texte" style="height: 100px"><?= isset($type_travail_id->type_traivail_texte) ? $type_travail_id->type_traivail_texte : ""; ?></textarea>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline switch">
                        <div>
                            <label data-width="250"> Statut </label>
                            <div class="form-check form-switch">
                                <span> Inactif </span>
                                <input class="form-check-input" type="checkbox" id="type_travail_etat" name="type_travail_etat"  <?php echo isset($type_travail_id->type_travail_etat) ? ($type_travail_id->type_travail_etat=="1" ? "checked" :"") : "checked" ;?>>
                                <span> Actif </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col">
            

        </div>
    </div>
<?php echo form_close(); ?>


