<div class="row m-0">
	<div class="col p-0">
		<div class="table-responsive scrollbar">
			<table class="table table-sm table-striped fs--1 mb-0">
				<thead>
					<th scope="col">Libellé</th>
					<th scope="col">Texte à mettre sur le reporting</th>
					<th class="text-end">Action</th>
				</thead>
				<tbody>
					<?php foreach ($type as $key => $value): ?>
						<tr id="rowtype-<?= $value->type_travail_id; ?>" class="align-middle">							
							<td>
								<div class="d-flex align-items-center position-relative">
									<div class="flex-1">
										<h6 class="mb-0 fw-semi-bold">											
                                            <?= $value->type_travail_libelle; ?>											
										</h6>
									</div>
								</div>
							</td>
							<td> <?= $value->type_traivail_texte; ?></td>							
							<td class="btn-action-table">
								<div class="d-flex justify-content-end">
									<button data-id="<?= $value->type_travail_id; ?>"
										class="btn btn-sm btn-outline-warning icon-btn updateTypeTravail"
										type="button">
										<span class="bi bi-pencil-square fs-1"></span>
									</button>
									<button data-id="<?= $value->type_travail_id; ?>"
										class="btn btn-sm btn-outline-danger icon-btn deleteTypeTravail"
										type="button">
										<span class="bi bi-trash fs-1"></span>
									</button>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
