<?php echo form_tag('Setting/Fiscalite/updateEntreprise_fiscale', array('method' => "post", 'class' => '', 'id' => 'updateEntreprise_fiscale')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Fiscalité</h5>
    </div>
    <div class="px-2">
        <button class="btn btn-sm btn-primary" type="submit">
            <span class="fas fa-check me-1"></span>
            Terminée
        </button>
    </div>
</div>

<div class="col-12 p-0 py-1">
    <div class="row m-0">
        <div class="col-2"></div>
        <div class="col p-4">
            <div class="card rounded-0 h-100">
                <div class="card-body ">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Capital :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->capital : '' ?>" name="capital" id="capital">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> RCS :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->rcs : '' ?>" name="rcs" id="rcs">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> SIRET :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->siret : '' ?>" name="siret" id="siret">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> APE :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->ape : '' ?>" name="ape" id="ape">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> TVA Intracommunautaire :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->tva_intra : '' ?>" name="tva_intra" id="tva_intra">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Carte professionnelle :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->carte_pro : '' ?>" name="carte_pro" id="carte_pro">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Libellé :</label>
                            <input type="text" class="form-control" value="<?= !empty($entreprise) ? $entreprise->libelle : '' ?>" name="libelle" id="libelle">
                            <div class="help"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<?php echo form_close(); ?>