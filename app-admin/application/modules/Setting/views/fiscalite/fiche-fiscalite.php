<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Fiscalité</h5>
	</div>
	<div class="px-2">
		<button id="btnFormodifier" class="btn btn-sm btn-primary" type="button">
			<span class="fas fa-edit me-1"></span>
			Modifier
		</button>
	</div>
</div>

<div class="col-12 p-0 py-1">
	<div class="row m-0">
		<div class="col-2"></div>
		<div class="col p-4">
			<div class="card rounded-0 h-100">
				<div class="card-body ">
					<table class="table table-borderless fs--1 mb-0">
						<tbody>
							<tr class="border-bottom border-top">
								<th class="ps-0">Capital :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->capital : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">RCS :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->rcs : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">SIRET :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->siret : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">APE :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->ape : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">TVA Intracommunautaire :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->tva_intra : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Carte professionnelle :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->carte_pro : '' ?></th>
							</tr>
							<tr class="border-bottom">
								<th class="ps-0">Libellé  :</th>
								<th class="pe-0 text-end"><?= !empty($entreprise) ? $entreprise->libelle : '' ?></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-2"></div>
	</div>
</div>