<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Liste des produits</h5>
	</div>
	<div class="px-2">

		<button id="btnFormAddProduit" class="btn btn-sm btn-primary" type="button">
				<span class="bi bi-person-plus-fill me-1"></span>
				Nouveau produit
		</button>
		
	</div>
</div>

<div id="contenair-list" class="contenair-list">


</div>

<script type="text/javascript">
	$(document).ready(function () {
    	listeProduit();
	});
</script>