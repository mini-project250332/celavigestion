<?php echo form_tag('Setting/Produit/cruProduit',array('method'=>"post", 'class'=>'','id'=>'formProduit')); ?>
	<div class="contenair-title">
		<div class="px-2">
			<h5 class="fs-1">Formulaire </h5>
		</div>
		<div class="px-2">
					
			<button type="button" id="btnAnnulerProduit" class="btn btn-sm btn-secondary mx-1">
				<i class="fas fa-times me-1"></i>
				<span> Annuler </span>
			</button>
					
			<button type="submit" class="btn btn-sm btn-primary">
				<span class="fas fa-plus me-1"></span>			
				<span> Enregistrer </span>
			</button>
		</div>
	</div>

	<?php if(isset($produit->produit_id)):?>
        <input type="hidden" name="produit_id" id="produit_id" value="<?=$produit->produit_id;?>">
    <?php endif;?>

    <input type="hidden" name="action" id="action" value="<?=$action?>">

	<div class="contenair-content">
		<div class="row m-0">
			<div class="col"></div>
    		<div class="col py-2">
    			<div class="card rounded-0 h-100">
	                <div class="card-body">

		    			<div class="form-group inline">
		                    <div>
		                        <label data-width="200"> Libelle </label>
		                        <input type="text" class="form-control maj" value ="<?= isset($produit->produit_libelle) ? $produit->produit_libelle : "" ;?>" id="produit_libelle" name="produit_libelle">
		                        <div class="help"></div>
		                    </div>
		                </div>

						<div class="form-group inline switch">
	                        <div>
	                            <label data-width="250"> Statut </label>
	                            <div class="form-check form-switch">
	                                <span> Inactif </span>
	                                <input class="form-check-input" type="checkbox" id="produit_etat" name="produit_etat"  <?php echo isset($produit->produit_etat) ? ($produit->produit_etat=="1" ? "checked" :"") : "checked" ;?>>
	                                <span> Actif </span>
	                            </div>
	                        </div>
	                    </div>


		            </div>
		        </div>
			</div>
			<div class="col"></div>
		</div>
	</div>
<?php echo form_close(); ?>
