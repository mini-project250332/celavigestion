<?php echo form_tag('Setting/Accueil/saveUpdateModel', array('method' => "post", 'class' => 'px-2', 'id' => 'saveUpdateModel')); ?>
<div class="contenair-title bg-300">
    <div class="px-2">
        <i class="fa fa-file fs-1" aria-hidden="true"></i> &nbsp; <h5 class="fs-1">Modification du modèle d'envoi du mail</h5>
    </div>
    <div class="px-2">
        <button type="button" id="annulerUpdateMessage" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler</span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary mx-1">
            <i class="fas fa-plus me-1"></i>
            <span> Enregistrer </span>
        </button>
    </div>
</div>
<div class="col-12">
    <div class="row">
        <div class="card rounded-0 h-100">
            <div class="card-body ">
                <div class="mt-2">
                    <table class="table table-borderless fs--1 mb-0">
                        <input type="hidden" id="mess_id" name="mess_id" value="<?= $model_accueil->mess_id ?>">
                        <tbody>
                            <tr class="border-bottom">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"> Message d'accueil * :</label>
                                        <textarea class="form-control fs--1 mb-6" id="message" name="message" style="height: 300px;" data-require="true"><?= isset($model_accueil->message) ? $model_accueil->message : ""; ?></textarea>
                                        <div class="help" style="top:99%"></div>
                                    </div>
                                </div>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>