<div class="card">
    <div class="card-body">
        <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead class="bg-300 text-900">
                    <th scope="col-8">Message d'accueil</th>
                    <th class="col-1 text-center">Action</th>
                </thead>
                <tbody class="bg-200">
                    <tr>
                        <td class="align-middle p-2"><?= nl2br($accueil->message) ?></td>
                        <td class="text-center p-2">
                            <button class="btn btn-sm btn-primary icon-btn btnUpdateMessage" type="button" data-id="<?= $accueil->mess_id ?>">
                                <span class="bi bi-pencil-square"></span>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>