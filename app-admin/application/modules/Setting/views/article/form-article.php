<?php echo form_tag('Setting/Article/cruArticle', array('method' => "post", 'class' => '', 'id' => 'cruArticle')); ?>
<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Formulaire </h5>
    </div>
    <div class="px-2">

        <button type="button" id="btnAnnulerArticle" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler </span>
        </button>

        <button type="submit" class="btn btn-sm btn-primary">
            <span class="fas fa-check me-1"></span>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<?php if (isset($article->art_id)) : ?>
    <input type="hidden" name="art_id" id="art_id" value="<?= $article->art_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">

<div class="contenair-content">
    <div class="row m-0">
        <div class="col"></div>
        <div class="col-6 py-2">
            <div class="card rounded-0 h-100">
                <div class="card-body">
                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Code *</label>
                            <input type="text" class="form-control" value="<?= isset($article->art_code) ? $article->art_code : ""; ?>" id="art_code" name="art_code" data-formatcontrol="true" data-require="true">
                            <div class="help" id="error_name"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Libellé FR </label>
                            <input type="text" class="form-control" value="<?= isset($article->art_libelle_fr) ? $article->art_libelle_fr : ""; ?>" id="art_libelle_fr" name="art_libelle_fr">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Libellé EN </label>
                            <input type="text" class="form-control" value="<?= isset($article->art_libelle_en) ? $article->art_libelle_en : ""; ?>" id="art_libelle_en" name="art_libelle_en">
                            <div class="help error_file text-danger"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Compte comptable </label>
                            <input type="text" class="form-control" value="<?= isset($article->art_cpt_comptable) ? $article->art_cpt_comptable : ""; ?>" id="art_cpt_comptable" name="art_cpt_comptable">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Code TVA </label>
                            <input type="text" class="form-control" value="<?= isset($article->art_code_tva) ? $article->art_code_tva : ""; ?>" id="art_code_tva" name="art_code_tva">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Pourcentage TVA </label>
                            <div class="input-group">
                                <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="art_pourcentage_tva" name="art_pourcentage_tva" style="text-align: right; height : 36px;" value="<?= isset($article->art_pourcentage_tva) ? $article->art_pourcentage_tva : ""; ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                            </div>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Compte comptable TVA </label>
                            <input type="text" class="form-control" value="<?= isset($article->art_compte_comptable) ? $article->art_compte_comptable : ""; ?>" id="art_compte_comptable" name="art_compte_comptable">
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Famille </label>
                            <select class="form-select fs--1" id="fam_id" name="fam_id">
                                <?php foreach ($famille as $key => $value_famille) : ?>
                                    <option value="<?= $value_famille->fam_id ?>" <?php echo isset($article->fam_id) ? (($article->fam_id !== NULL) ? (($article->fam_id == $value_famille->fam_id) ? "selected" : " ") : "") : "" ?>>
                                        <?= $value_famille->fam_libelle ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Type </label>
                            <select class="form-select fs--1" id="type_fact_id" name="type_fact_id">
                                <?php foreach ($type_article as $key => $value_type) : ?>
                                    <option value="<?= $value_type->type_fact_id ?>" <?php echo isset($article->type_fact_id) ? (($article->type_fact_id !== NULL) ? (($article->type_fact_id == $value_type->type_fact_id) ? "selected" : " ") : "") : "" ?>>
                                        <?= $value_type->type_fact_libelle ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline">
                        <div>
                            <label data-width="200"> Montant </label>
                            <div class="input-group">
                                <input type="text" placeholder="0" class="form-control mb-0 chiffre" id="art_tarif" name="art_tarif" style="text-align: right; height : 36px;" value="<?= isset($article->art_tarif) ? $article->art_tarif : ""; ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">HT</span>
                                </div>
                            </div>
                            <div class="help"></div>
                        </div>
                    </div>

                    <div class="form-group inline switch">
                        <div>
                            <label data-width="250"> Statut</label>
                            <div class="form-check form-switch">
                                <span> Inactif </span>
                                <input class="form-check-input" type="checkbox" id="art_etat" name="art_etat" <?php echo isset($article->art_etat) ? ($article->art_etat == "1" ? "checked" : "") : "checked"; ?>>
                                <span> Actif </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
</div>
<?php echo form_close(); ?>