<div class="contenair-title">
	<div class="px-2">
		<h5 class="fs-1">Liste des articles</h5>
	</div>
	<div class="px-2">
		<button id="btnFormAddArticle" class="btn btn-sm btn-primary" type="button">
			<span class="fas fa-plus me-1"></span>
			Nouveau article
		</button>		
	</div>
</div>

<div id="contenair-list" class="contenair-list">

</div>

<script type="text/javascript">
	$(document).ready(function () {
    	listeArticle();
	});
</script>