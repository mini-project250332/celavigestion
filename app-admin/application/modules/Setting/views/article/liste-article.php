<div class="row m-0">
    <div class="col p-0">
        <div class="table-responsive scrollbar">
            <table class="table table-sm table-striped fs--1 mb-0">
                <thead>
                    <th scope="col">Code article</th>
                    <th scope="col">Libellé FR</th>
                    <th scope="col">Libellé EN</th>
                    <th scope="col">Montant(HT)</th>
                    <th scope="col">Numéro compte comptable</th>
                    <th scope="col">Famille</th>
                    <th scope="col">Type</th>
                    <th scope="col">Etat</th>
                    <th class="text-end">Actions</th>
                </thead>
                <tbody>
                    <?php foreach ($article as $key => $value) : ?>
                        <tr id="rowArticle-<?= $value->art_id; ?>" class="align-middle">
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <h6 class="mb-0 fw-semi-bold">
                                            <?= $value->art_code; ?>
                                        </h6>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->art_libelle_fr; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->art_libelle_en; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->art_tarif; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->art_cpt_comptable; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->fam_libelle; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= $value->type_fact_libelle ; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center position-relative">
                                    <div class="flex-1">
                                        <p class="text-800 fs--1 mb-0">
                                            <?= ($value->art_etat == 1) ? "Actif" : "Inactif"; ?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td class="btn-action-table">
                                <div class="d-flex justify-content-end">
                                    <button data-id="<?= $value->art_id; ?>" class="btn btn-sm btn-outline-warning icon-btn btnFormUpdateArticle" type="button">
                                        <span class="bi bi-pencil-square fs-1"></span>
                                    </button>
                                    <button data-id="<?= $value->art_id; ?>" class="btn btn-sm btn-outline-danger icon-btn btnSupArticle" type="button">
                                        <span class="bi bi-trash fs-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>