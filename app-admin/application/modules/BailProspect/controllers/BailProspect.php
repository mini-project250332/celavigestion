<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class BailProspect extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "BailProspect";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/components/prospections/bail.js",
        );

        $this->_css_personnaliser = array();
    }

    public function listeDocBail()
    {
        $this->load->model('DocumentBailProspect_m', 'document');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data_post = $_POST["data"];
                $params = array(
                    'columns' => array('docp_bail_id', 'docp_bail_nom', 'docp_bail_path', 'docp_bail_creation', 'util_prenom', 'c_utilisateur.util_id', 'lot_id'),
                    'clause' => array(
                        'lot_id' => $data_post['lot_id'],
                        'docp_bail_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_prospect_bail.util_id')
                );

                $data['lot_id'] = $data_post['lot_id'];
                $data['document'] = $this->document->get($params);

                $this->renderComponant("Prospections/prospects/lot/bail/list-document", $data);
            }
        }
    }

    public function formUploadDocBail()
    {

        $data_post = $_POST['data'];
        $data['lot_id'] = $data_post['lot_id'];

        $this->renderComponant('Prospections/prospects/lot/bail/form_upload_bail', $data);
    }

    public function uploadfileBail()
    {


        @set_time_limit(5 * 60);

        $fichiers = $_POST['fichiers'];
        $lot_id = $_POST['lot_id'];

        // Chemin du dossier
        $targetDir = "../documents/prospections/lots/bail/$lot_id";


        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        if (!file_exists($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $res = array();
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        $original_name = "";

        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de chemin temporail echouée',
                    "error" => 100,
                    "extention" => ''
                );
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                $tmp_name_file = explode('.', $_POST['name'])[0];
                foreach ($fichiers as $key => $value) {
                    if ($value['fileName'] == $tmp_name_file) {
                        $original_name = str_replace(array('<span>', '</span>'), "", $value['originalName']);
                    }
                }
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            $res = array(
                "jsonrpc" => "2.0",
                'message' => 'Ouverture de output stream echoué',
                "error" => 102,
                "extention" => ''
            );
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Telechargerment fichier echoué',
                    "error" => 103,
                    "extention" => ''
                );
            }
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $res = array(
                    "jsonrpc" => "2.0",
                    'message' => 'Ouverture de input stream echoué',
                    "error" => 104,
                    "extention" => ''
                );
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            // var_dump($original_name);

            $data = array(
                'docp_bail_nom' => $original_name,
                'docp_bail_creation' => date('Y-m-d H:i:s'),
                'docp_bail_path' => str_replace('\\', '/', $filePath),
                'docp_bail_etat' => 1,
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'lot_id' => $lot_id
            );

            $this->load->model('DocumentBailProspect_m', 'document');
            $this->document->save_data($data);
            echo json_encode($data);
        }
    }

    public function removeDocBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentBailProspect_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('docp_bail_id' => $data['docp_bail_id']),
                    'columns' => array('docp_bail_id'),
                    'method' => "row",
                );
                $request_get = $this->DocumentBailProspect_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['docp_bail_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document ? ",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('docp_bail_id' => $data['docp_bail_id']);
                    $data_update = array('docp_bail_etat' => 0);
                    $request = $this->DocumentBailProspect_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['docp_bail_id']),
                            'message' => "Le document a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['lot_id'] = $data_post['lot_id'];
                $this->load->model('DocumentBailProspect_m');
                $data['document'] = $this->DocumentBailProspect_m->get(
                    array('clause' => array("docp_bail_id" => $data_post['docp_bail_id']), 'method' => "row")
                );
            }
            $this->renderComponant("Prospections/prospects/lot/bail/formUpdateBail", $data);
        }
    }

    public function UpdateDocumentBail_Prospect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentBailProspect_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("lot_id" => $data['lot_id']);
                    $docp_bail_id = $data['docp_bail_id'];
                    unset($data['docp_bail_id']);
                    $clause = array("docp_bail_id" => $docp_bail_id);
                    $request = $this->DocumentBailProspect_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getDocpath()
    {
        $this->load->model('DocumentBailProspect_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('docp_bail_id', 'docp_bail_path'),
                    'clause' => array('docp_bail_id' => $data_post['docp_bail_id'])
                );
                $request = $this->DocumentBailProspect_m->get($params);
                echo json_encode($request);
            }
        }
    }
}