<div class="table-responsive scrollbar mt-2">
    <table class="table table-sm fs--1 mb-0">
        <thead class="bg-300 text-900">
            <tr>
                <th scope="col">Nom de la tâche</th>
                <th scope="col">Prioritaire</th>
                <th scope="col">Prospect/Client</th>
                <th scope="col">Participants</th>
                <th scope="col">Créateur</th>
                <th scope="col">Echéance</th>
                <th scope="col">Date validation</th>
                <th class="text-end" scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($taches as $tache): ?>

                <?php

                $type = $tache->tache_prioritaire;
                $value;
                if ($type == 1) {
                    $value = "Oui";
                } else {
                    $value = "Non";
                }

                $date_now = date("Y-m-d");

                $type_tache = $tache->type_tache_id;
                $value_tache;
                if ($type_tache == 3) {
                    $value_tache = $tache->data_client->client_nom . ' ' . $tache->data_client->client_prenom;
                } else if ($type_tache == 2) {
                    $value_tache = $tache->data_prospect->prospect_nom . ' ' . $tache->data_prospect->prospect_prenom;
                } else {
                    $value_tache = "";
                }

                ?>
                <tr id="rowTache-<?= $tache->tache_id; ?>"
                    class="align-middle <?php if ($tache->tache_prioritaire == 1)
                        echo 'bold'; ?>">
                    <td class=""><?= $tache->tache_nom; ?></td>
                    <td><?= $value; ?></td>
                    <td>
                        <?= $value_tache; ?>
                    </td>
                    <td><?= $tache->participants; ?></td>
                    <td> <?= $tache->tache_createur; ?> </td>
                    <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                    <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_validationn))); ?></td>
                    <td>
                        <div class="d-flex justify-content-end ">
                            <button class="btn btn-sm btn-outline-primary icon-btn p-0 m-1" id="updateTache"
                                data-id="<?= $tache->tache_id; ?>">
                                <span class="fas fa-edit fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-outline-success icon-btn p-0 m-1 btnValideTache only_visuel_gestion"
                                type="button" data-id="<?= $tache->tache_id; ?>">
                                <span class="fas fa-check fs-1 m-1"></span>
                            </button>
                            <button class="btn btn-sm btn-outline-danger icon-btn p-0 m-1 btnSuppTache only_visuel_gestion"
                                type="button" data-id="<?= $tache->tache_id; ?>">
                                <span class="bi bi-trash fs-1 m-1"></span>
                            </button>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>