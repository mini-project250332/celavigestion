<input type="hidden" value="<?php echo base_url('Clients'); ?>" id="client-open-tab-url" />
<input type="hidden" value="<?php echo base_url('Prospections'); ?>" id="prospect-open-tab-url" />
<div class="card rounded-0 ">
    <!-- <div class="card-header rounded bg-300">
        <h5 class="fs-0">            
           Tâches prioritaires
        </h5>
    </div> -->
    
    <div class="card-body p-0">
        <div class="table-responsive scrollbar mt-2">
            <table class="table table-hover table-striped overflow-hidden fs--1 mb-0">                
                <thead class=" text-900">
                    <tr class="btn-reveal-trigger">
                        <th scope="col">Nom de la tâche</th>
                        <th scope="col">Thème</th>
                        <th scope="col">Responsable </th>
                        <th scope="col">Priorité</th>
                        <th scope="col">Prospect/Client</th>
                        <th scope="col">Echéance</th>
                        <th class="text-end" scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($taches as $tache) : ?>
                        <?php
                        $type = $tache->tache_prioritaire;
                        $value;
                        if ($type == 0) {
                            $value = "Sans priorité";
                        } else if ($type == 1) {
                            $value = "Urgent";
                        } else {
                            $value = "Prioritaire";
                        }

                        $date_now = date("Y-m-d");

                        $type_tache = $tache->type_tache_id;
                        $value_tache;
                        if ($type_tache == 3) {
                            $value_tache = $tache->data_client->client_nom . ' ' . $tache->data_client->client_prenom;
                        } else if ($type_tache == 2) {
                            $value_tache = $tache->data_prospect->prospect_nom . ' ' . $tache->data_prospect->prospect_prenom;
                        } else {
                            $value_tache = "";
                        }

                        $value_id;

                        if ($type_tache == 3) {
                            $value_id = $tache->data_client->client_id;
                        } else if ($type_tache == 2) {
                            $value_id = $tache->data_prospect->prospect_id;
                        } else {
                            $value_id = "";
                        }

                        ?>

                        <tr id="rowTache-<?= $tache->tache_id; ?>" class="align-middle btn-reveal-trigger <?php if ($tache->tache_prioritaire == 1) echo 'bold'; ?> 
                    <?php if ($date_now > $tache->tache_date_echeance) echo 'greater'; ?>">
                            <td><?= $tache->tache_nom; ?></td>
                            <td><?= $tache->theme_libelle; ?></td>
                            <td> <?= $tache->tache_createur; ?> </td>
                            <td>
                                <span class="badge badge rounded-pill badge-soft-warning"><?= $value; ?></span></span>
                            </td>
                            <td class="<?= $type_tache == 3 ? 'FicheClientNewTab' : ($type_tache == 2 ? 'FicheProspectNewTab' : '') ?> mt-2 link-primary" data-id="<?= $value_id ?>" style="cursor:pointer; text-decoration:underline">
                                <?= $value_tache; ?>
                            </td>
                            <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                            <td>
                                <div class="d-flex justify-content-end ">
                                    <button class="btn btn-sm btn-outline-primary p-0 m-1" id="updateTache" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-edit fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-outline-success p-0 m-1 btnValideTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-check fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-sm btn-outline-danger p-0 m-1 btnSuppTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="bi bi-trash fs-1 m-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- <div class="card rounded-0 ">
    <div class="card-header rounded bg-300">
        <h5 class="fs-0">            
           Tâches urgents
        </h5>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive scrollbar mt-2">
            <table class="table table-hover table-striped overflow-hidden fs--1 mb-0">                
                <thead class=" text-900">
                    <tr class="btn-reveal-trigger">
                        <th scope="col">Nom de la tâche</th>
                        <th scope="col">Thème</th>
                        <th scope="col">Responsable </th>
                        <th scope="col">Priorité</th>
                        <th scope="col">Prospect/Client</th>
                        <th scope="col">Echéance</th>
                        <th class="text-end" scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($taches as $tache) : 
                        if ($tache->tache_prioritaire == 1) :?>
                        <?php
                        $type = $tache->tache_prioritaire;
                        $value;
                        if ($type == 0) {
                            $value = "Sans priorité";
                        } else if ($type == 1) {
                            $value = "Urgent";
                        } else {
                            $value = "Prioritaire";
                        }

                        $date_now = date("Y-m-d");

                        $type_tache = $tache->type_tache_id;
                        $value_tache;
                        if ($type_tache == 3) {
                            $value_tache = $tache->data_client->client_nom . ' ' . $tache->data_client->client_prenom;
                        } else if ($type_tache == 2) {
                            $value_tache = $tache->data_prospect->prospect_nom . ' ' . $tache->data_prospect->prospect_prenom;
                        } else {
                            $value_tache = "";
                        }

                        $value_id;

                        if ($type_tache == 3) {
                            $value_id = $tache->data_client->client_id;
                        } else if ($type_tache == 2) {
                            $value_id = $tache->data_prospect->prospect_id;
                        } else {
                            $value_id = "";
                        }

                        ?>

                        <tr id="rowTache-<?= $tache->tache_id; ?>" class="align-middle btn-reveal-trigger <?php if ($tache->tache_prioritaire == 1) echo 'bold'; ?> 
                    <?php if ($date_now > $tache->tache_date_echeance) echo 'greater'; ?>">
                            <td><?= $tache->tache_nom; ?></td>
                            <td><?= $tache->theme_libelle; ?></td>
                            <td> <?= $tache->tache_createur; ?> </td>
                            <td>
                                <span class="badge badge rounded-pill badge-soft-warning"><?= $value; ?></span></span>
                            </td>
                            <td class="<?= $type_tache == 3 ? 'FicheClientNewTab' : ($type_tache == 2 ? 'FicheProspectNewTab' : '') ?> mt-2 link-primary" data-id="<?= $value_id ?>" style="cursor:pointer; text-decoration:underline">
                                <?= $value_tache; ?>
                            </td>
                            <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                            <td>
                                <div class="d-flex justify-content-end ">
                                    <button class="btn btn-sm btn-outline-primary p-0 m-1" id="updateTache" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-edit fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-outline-success p-0 m-1 btnValideTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-check fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-sm btn-outline-danger p-0 m-1 btnSuppTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="bi bi-trash fs-1 m-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endif; endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="card rounded-0">
    <div class="card-header rounded bg-300">
        <h4 class="fs-0">            
           Tâches sans prioritaires
        </h4>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive scrollbar mt-2">
            <table class="table table-hover table-striped overflow-hidden fs--1 mb-0">                
                <thead class="text-900">
                    <tr class="btn-reveal-trigger">
                        <th scope="col">Nom de la tâche</th>
                        <th scope="col">Thème</th>
                        <th scope="col">Responsable </th>
                        <th scope="col">Priorité</th>
                        <th scope="col">Prospect/Client</th>
                        <th scope="col">Echéance</th>
                        <th class="text-end" scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($taches as $tache) :
                        if($tache->tache_prioritaire == 0) : ?>
                        <?php
                        $type = $tache->tache_prioritaire;
                        $value;
                        if ($type == 0) {
                            $value = "Sans priorité";
                        } else if ($type == 1) {
                            $value = "Urgent";
                        } else {
                            $value = "Prioritaire";
                        }

                        $date_now = date("Y-m-d");

                        $type_tache = $tache->type_tache_id;
                        $value_tache;
                        if ($type_tache == 3) {
                            $value_tache = $tache->data_client->client_nom . ' ' . $tache->data_client->client_prenom;
                        } else if ($type_tache == 2) {
                            $value_tache = $tache->data_prospect->prospect_nom . ' ' . $tache->data_prospect->prospect_prenom;
                        } else {
                            $value_tache = "";
                        }

                        $value_id;

                        if ($type_tache == 3) {
                            $value_id = $tache->data_client->client_id;
                        } else if ($type_tache == 2) {
                            $value_id = $tache->data_prospect->prospect_id;
                        } else {
                            $value_id = "";
                        }

                        ?>

                        <tr id="rowTache-<?= $tache->tache_id; ?>" class="align-middle btn-reveal-trigger <?php if ($tache->tache_prioritaire == 1) echo 'bold'; ?> 
                    <?php if ($date_now > $tache->tache_date_echeance) echo 'greater'; ?>">
                            <td><?= $tache->tache_nom; ?></td>
                            <td><?= $tache->theme_libelle; ?></td>
                            <td> <?= $tache->tache_createur; ?> </td>
                            <td>
                                <span class="badge badge rounded-pill badge-soft-secondary"><?= $value; ?></span></span>
                            </td>
                            <td class="<?= $type_tache == 3 ? 'FicheClientNewTab' : ($type_tache == 2 ? 'FicheProspectNewTab' : '') ?> mt-2 link-primary" data-id="<?= $value_id ?>" style="cursor:pointer; text-decoration:underline">
                                <?= $value_tache; ?>
                            </td>
                            <td><?= date("d/m/Y", strtotime(str_replace('-', '/', $tache->tache_date_echeance))); ?></td>
                            <td>
                                <div class="d-flex justify-content-end ">
                                    <button class="btn btn-sm btn-outline-primary p-0 m-1" id="updateTache" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-edit fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-outline-success p-0 m-1 btnValideTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="fas fa-check fs-1 m-1"></span>
                                    </button>
                                    <button class="btn btn-sm btn-outline-danger p-0 m-1 btnSuppTache" type="button" data-id="<?= $tache->tache_id; ?>">
                                        <span class="bi bi-trash fs-1 m-1"></span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endif;endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div> -->
<script>
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const client_id = urlParams.get('_p@roprietaire');
    const prospect_id = urlParams.get('_p@rospect');
    const client_tab = urlParams.get('_open@client_tab');
    const prospect_tab = urlParams.get('_open@prospect_tab');
    if (client_tab) {
        ficheClient({
            client_id: client_tab
        });
    }
    if (prospect_tab) {
        ficheProspect({
            prospect_id: prospect_tab
        });
    }
</script>