<?php echo form_tag('Taches/cruTache', array('method' => "post", 'class' => 'px-2', 'id' => 'cruTache')); ?>
<style>
    .choices__inner {

        min-height: 0px !important;
        margin-right: 200px !important;
        height: 35px !important;
    }
</style>

<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-0"></h5>
    </div>
    <div class=" px-2">
        <button type="button" id="annulerAddTache" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-times me-1"></i>
            <span> Annuler </span>
        </button>

        <button type="submit" id="save-tache" class="btn btn-sm btn-primary mx-1 only_visuel_gestion">
            <i class="fas fa-plus me-1"></i>
            <span> Enregistrer </span>
        </button>
    </div>
</div>

<?php if (isset($tache->tache_id)) : ?>
    <input type="hidden" name="tache_id" id="tache_id" value="<?= $tache->tache_id; ?>">
<?php endif; ?>

<input type="hidden" name="action" id="action" value="<?= $action ?>">
<div class="card rounded-0  ">
    <div class="card-body">
        <div class="row m-0">
            <div class="col-2">

            </div>
            <div class="col py-2">
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="300"> Date création *</label>
                        <input type="text" class="form-control calendar fs--1" id="tache_date_creation" name="tache_date_creation" disabled data-formatcontrol="true" data-require="true" value="<?= (isset($tache->tache_date_creation)) ? date("d-m-Y", strtotime(str_replace('/', '-', $tache->tache_date_creation))) : ""; ?>">
                        <div class="help"></div>
                    </div>
                </div>
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="300"> Date d'échéance *</label>
                        <input type="text" class="form-control calendar fs--1" id="tache_date_echeance" name="tache_date_echeance" data-formatcontrol="true" data-require="true" value="<?= (isset($tache->tache_date_echeance)) ? date("d-m-Y", strtotime(str_replace('/', '-', $tache->tache_date_echeance))) : ""; ?>">
                        <div class="help"></div>
                    </div>
                </div>
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="300"> Nom de la tâche *</label>
                        <input type="text" class="form-control fs--1" id="tache_nom" name="tache_nom" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= isset($tache->tache_nom) ? $tache->tache_nom : ""; ?>">
                        <div class="help"></div>
                    </div>
                </div>
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="300"> Prioritaire </label>
                        <select class="form-select fs--1" id="tache_prioritaire" name="tache_prioritaire" style="width:97%;">
                            <?php
                            $array_type = unserialize(tache_prioritaire);

                            foreach ($array_type  as $type) {
                                echo '<option value="' . $type['value'] . '" ' . (($tache->tache_prioritaire == $type['value']) ? "selected" : "") . '>
                                    ' . $type['tacheprioritaire'] . '</option>';
                            }
                            ?>
                        </select>
                        <div class="help"></div>
                    </div>
                </div>
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="300">Thème</label>
                        <select class="form-select fs--1 m-0" id="theme_id" name="theme_id" style="width:97%;">
                            <?php foreach ($theme as $themes) : ?>
                                <option class="" value="<?= $themes->theme_id; ?>" <?php echo isset($tache->theme_id) ? (($tache->theme_id !== NULL) ? (($tache->theme_id == $themes->theme_id) ? "selected" : " ") : "") : "" ?>>
                                    <?= $themes->theme_libelle; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <div class="help"></div>
                    </div>
                </div>
            </div>
            <div class="col py-2">
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="250"> Créateur *</label>
                        <input type="text" class="form-control fs--1" id="" name="" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= $_SESSION['session_utilisateur']['util_prenom'] ?>" disabled>
                        <div class="help"></div>
                    </div>
                </div>
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label style="min-width : 11.25rem"> Participants *</label>
                        <select class="form-select fs--1 js-choice" id="participant" name="participant" multiple="multiple" size="1" data-require="true">
                            <option value=""></option>
                        </select>

                        <div class="help"></div>
                    </div>
                </div>

                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="250"> Association prospect/client </label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" id="client" name="checkclient" type="checkbox" onclick="showinputclient()" />
                            <label id="clientlabel" class="form-check-label">Client</label>
                        </div>

                        <div class="" id="client_div">
                            <select class="select2" id="client_id" name="client_id">
                                <?php foreach ($client as $clients) : ?>
                                    <option class="" value="<?= $clients->client_id; ?>" <?php echo isset($tache->client_id) ? (($tache->client_id !== NULL) ? (($tache->client_id == $clients->client_id) ? "selected" : " ") : "") : "" ?>>
                                        <?= $clients->client_nom . ' ' . $clients->client_prenom; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" id="prospect" name="checkprospect" type="checkbox" onclick="showinputprospect()" />
                            <label id="prospectlabel" class="form-check-label">Prospect</label>
                        </div>

                        <div class="" id="prospect_div">
                            <select class="select2" id="prospect_id" name="prospect_id">
                                <?php foreach ($prospect as $prospects) : ?>
                                    <option class="" value="<?= $prospects->prospect_id; ?>" <?php echo isset($tache->prospect_id) ? (($tache->prospect_id !== NULL) ? (($tache->prospect_id == $prospects->prospect_id) ? "selected" : " ") : "") : "" ?>>
                                        <?= $prospects->prospect_nom . ' ' . $prospects->prospect_prenom; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="help"></div>
                    </div>
                </div>

                <div class="form-group inline switch mt-3">
                    <div>
                        <label data-width="250"> Statut </label>
                        <div class="form-check form-switch fs--1">
                            <span> Non réalisé </span>
                            <input class="form-check-input" type="checkbox" id="tache_valide" name="tache_valide" <?php echo isset($tache->tache_valide) ? ($tache->tache_valide == "1" ? "checked" : "") : ""; ?>>
                            <span> Réalisé </span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-2">

            </div>
        </div>
        <div class="row">
            <div class="col-2">

            </div>
            <div class="col">
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <textarea class="form-control" id="tache_texte" name="tache_texte" placeholder="Laissez un commentaire" style="height: 100px"><?= isset($tache->tache_texte) ? $tache->tache_texte : ""; ?></textarea>
                        <div class="help"></div>
                    </div>
                </div>
            </div>
            <div class="col-2">

            </div>
        </div>
    </div>
</div>

<?php echo form_close(); ?>

<?php
$util_default = array();
foreach ($utilisateur as $utilisateurs) {
    $util_default[] = array(
        'id' => $utilisateurs->id,
        'text' => $utilisateurs->prenom,
    );
}

$session[] = array(
    'id' => $_SESSION['session_utilisateur']['util_id'],
    'text' => $_SESSION['session_utilisateur']['util_prenom'],
);


if ($action == "edit") {
    $edit_exist = explode(',', $tache->participant);
    $util_array = array();
    $items = array();
    foreach ($util_default as $key) {
        if (in_array($key['id'], $edit_exist)) {
            $util_array[] = array(
                'id' => $key['id'],
                'text' => $key['text'],
            );
        } else {
            $items[] = array(
                'id' => $key['id'],
                'text' => $key['text'],
            );
        }
    }
    $util_default = $items;
}
?>
<script>
    deactivate_input_gestion();
    only_visuel_gestion();
    var util_default = <?= json_encode($util_default) ?>;
    flatpickr("#tache_date_creation", {
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        defaultDate: moment().format('DD-MM-YYYY')
    });

    $(document).ready(function() {
        $('.select2').select2({
            theme: 'bootstrap-5',
        });
        $("#client_div").hide();
        $("#prospect_div").hide();
    });

    flatpickr("#tache_date_echeance", {
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
    });

    var element = document.querySelector('.js-choice');
    var choices = new Choices(element, {
        removeItems: true,
        removeItemButton: true,
        choices: util_default.map((item) => ({
            value: item.id,
            label: item.text
        }))
    });

    <?php if ($action == "add") : ?>
        var liste = <?= json_encode($session); ?>;
        console.log(liste)

        choices.setValue(liste.map((item) => ({
            value: item.id,
            label: item.text
        })));

    <?php endif; ?>

    <?php if ($action == "edit") : ?>
        var liste_util = <?= json_encode($util_array); ?>;
        //choices.removeActiveItems();
        choices.setValue(liste_util.map((item) => ({
            value: item.id,
            label: item.text
        })));
    <?php endif; ?>
</script>