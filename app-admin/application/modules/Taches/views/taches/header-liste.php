<div class="contenair-title">
    <div class="px-2">
        <h5 class="fs-1">Liste des tâches</h5>
    </div>
    <div class="px-2">
        <div class="d-flex justify-content-center">
            <button id="btnTacheValide" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Tâches validées
            </button> &nbsp;
            <button id="btnTacheNonValide" class="btn btn-sm btn-primary d-none"><i class="fas fa-eye"></i> Tâches non
                validées </button>
            <button id="btnFormTache" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Tâche </button>
        </div>
    </div>
</div>
<div class="contenair-list">
    <div class="row m-0">
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Responsables (Créateur de la Tâche)</label>
                <select class="form-select fs--1" id="createur" name="createur" style="width: 100%">
                    <option value="">Tous</option>
                    <?php foreach ($utilisateur as $key => $value) : ?>
                        <option value="<?= $value->prenom; ?>" <?= ($value->prenom == $_SESSION['session_utilisateur']['util_prenom']) ? 'selected' : ''; ?>><?= $value->prenom; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Participants</label>
                <select class="form-select fs--1" id="participant" name="participant" style="width: 100%">
                    <option value="">Tous</option>
                    <?php foreach ($utilisateur as $key => $value) : ?>
                        <option value="<?= $value->id; ?>" <?= ($value->id == $_SESSION['session_utilisateur']['util_id']) ? 'selected' : ''; ?>><?= $value->prenom; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Thèmes</label>
                <select class="form-select fs--1" id="theme_id" name="theme_id" style="width: 100%">
                    <option value="">Toutes</option>
                    <?php foreach ($theme as $themes) : ?>
                        <option value="<?= $themes->theme_id; ?>" <?php echo isset($tache->theme_id) ? (($tache->theme_id !== NULL) ? (($tache->theme_id == $themes->theme_id) ? "selected" : " ") : "") : "" ?>>
                            <?= $themes->theme_libelle; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="mb-2">
                <label class="form-label">Dates échéances</label>
                <div class="d-flex">
                    <input class="form-control mb-0 calendar" id="date_filtre" name="date_filtre" type="text">
                    <button class="btn btn-sm btn-outline-primary d-flex justify-content-center ms-1 d-none" style="width:32px;height: 30px;" id="clear_date_filtre">
                        <i class="fas fa-times align-self-center"></i>
                    </button>
                </div>
            </div>
        </div>
        <div id="data-list">

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        listTache();
    });

    var date_filtre = flatpickr("#date_filtre", {
        altInput: true,
        allowInput: true,
        "mode": "range",
        "locale": "fr",
        enableTime: false,
        dateFormat: "d-m-Y",
        onChange: function() {
            $('#clear_date_filtre').removeClass('d-none');
            listTache();
        },
    });

    $(document).on('click', '#clear_date_filtre', function(e) {
        date_filtre.clear();
        $(this).toggleClass('d-none');
        listTache();
    })
</script>