<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Taches extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Taches";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/app.js",
            "js/taches/taches.js",
            "js/saisietemps/saisietemps.js"
        );

        $this->_css_personnaliser = array(
            "css/tache/tache.css",
            "css/select2.min.css",
        );
    }

    public function index()
    {
        $this->_data['menu_principale'] = "taches";
        $this->_data['sous_module'] = "taches/contenaire-tache";

        $this->render('contenaire');
    }

    public function getHeaderList()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data = array();
                $data['utilisateur'] = $this->getListUser();
                $data['theme'] = $this->getlisteTheme();

                $this->renderComponant("Taches/taches/header-liste", $data);
            }
        }
    }

    public function listeTache()
    {

        $this->load->model('Tache_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $clause = array(
                    'tache_etat' => 1,
                    'tache_valide' => 0,
                );

                if ($data_post['createur'] != '') {
                    $clause['c_tache.tache_createur'] = $data_post['createur'];
                }

                if ($data_post['theme_id'] != '') {
                    $clause['c_tache.theme_id'] = $data_post['theme_id'];
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['tache_date_echeance >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['tache_date_echeance <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['tache_date_echeance'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }

                $params = array(
                    'columns' => array(
                        'tache_id as tache_id', 'tache_nom as tache_nom', 'tache_date_echeance as tache_date_echeance', 'tache_createur as tache_createur', 'client_id', 'prospect_id',
                        'participant as participants', 'tache_date_creation as tache_date_creation', 'c_type_tache.type_tache_id', 'tache_prioritaire as tache_prioritaire', 'tache_date_validationn as tache_date_validationn',
                        'c_theme.theme_id', 'theme_libelle'
                    ),
                    'clause' => $clause,
                    'order_by_columns' => "tache_date_echeance ASC",
                    'join' => array(
                        'c_type_tache' => 'c_type_tache.type_tache_id = c_tache.type_tache_id',
                        'c_theme' => 'c_theme.theme_id = c_tache.theme_id',
                    ),
                );

                $request = $this->Tache_m->get($params);

                $result = array();
                foreach ($request as $key => $value) {
                    $participants_id = (!empty($value->participants)) ?  explode(',', $value->participants) : NULL;
                    if (intval($value->type_tache_id) == 3) {
                        $request[$key]->data_client = $this->getData_client($value->client_id);
                        //$request[$key]->data_client_associe = "";
                    } else if (intval($value->type_tache_id) == 2) {
                        $request[$key]->data_prospect = $this->getData_prospect($value->prospect_id);
                        //$request[$key]->data_prospect_associes = "";
                    }
                    if (!empty($participants_id)) {
                        $utilisateur = array();
                        foreach ($participants_id as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->participants = implode(' , ', $utilisateur);
                    }
                    if (!empty($data_post['participant'])) {
                        if (!empty($participants_id) && in_array(intval($data_post['participant']), $participants_id))
                            $result[] = $request[$key];
                    } else {
                        $result[] = $request[$key];
                    }
                }

                $data['taches'] = $result;
                $data['utilisateur'] = $this->getListUser();
                $this->renderComponant("Taches/taches/liste_tache", $data);
            }
        }
    }

    public function getParticipant($participants)
    {
        $this->load->model('Utilisateur_m');
        $Query = " Select util_id,util_nom,util_prenom from c_utilisateur where util_id IN ($participants)";
        $request = $this->Utilisateur_m->request_query($Query);
        return $request;
    }

    public function listTacheValide()
    {
        $this->load->model('Tache_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $like = array();
                $or_like = array();
                $clause = array(
                    'tache_etat' => 1,
                    'tache_valide' => 1
                );
                $data_post = $_POST['data'];

                if ($data_post['createur'] != '') {
                    $clause['c_tache.tache_createur'] = $data_post['createur'];
                }

                if ($data_post['theme_id'] != '') {
                    $clause['c_tache.theme_id'] = $data_post['theme_id'];
                }

                if ($data_post['date_filtre'] != '') {
                    $data_date = $data_post['date_filtre'];
                    $explode = explode("au", $data_date);
                    if (count($explode) == 2) {
                        $clause['tache_date_echeance >='] = str_replace(' ', '', date_sql($explode[0], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                        $clause['tache_date_echeance <='] = str_replace(' ', '', date_sql($explode[1], '-', 'jj/MM/aaaa')) . ' 00:00:00';
                    } else {
                        $clause['tache_date_echeance'] = date_sql($data_post['date_filtre'], '-', 'jj/MM/aaaa');
                    }
                }

                $params = array(
                    'columns' => array(
                        'tache_id as tache_id', 'tache_nom as tache_nom', 'tache_date_echeance as tache_date_echeance', 'tache_createur as tache_createur', 'client_id', 'prospect_id',
                        'participant as participants', 'tache_date_creation as tache_date_creation', 'c_type_tache.type_tache_id', 'tache_prioritaire as tache_prioritaire', 'tache_date_validationn as tache_date_validationn',
                        'c_theme.theme_id', 'theme_libelle'
                    ),
                    'clause' => $clause,
                    'order_by_columns' => "tache_date_echeance ASC",
                    'join' => array(
                        'c_type_tache' => 'c_type_tache.type_tache_id = c_tache.type_tache_id',
                        'c_theme' => 'c_theme.theme_id = c_tache.theme_id',
                    ),
                );

                $request = $this->Tache_m->get($params);
                $result = array();
                foreach ($request as $key => $value) {
                    $participants_id = (!empty($value->participants)) ?  explode(',', $value->participants) : NULL;
                    if (intval($value->type_tache_id) == 3) {
                        $request[$key]->data_client = $this->getData_client($value->client_id);
                        //$request[$key]->data_client_associe = "";
                    } else if (intval($value->type_tache_id) == 2) {
                        $request[$key]->data_prospect = $this->getData_prospect($value->prospect_id);
                        //$request[$key]->data_prospect_associes = "";
                    }
                    if (!empty($participants_id)) {
                        $utilisateur = array();
                        foreach ($participants_id as $value_util) {
                            array_push($utilisateur, $this->getUtil($value_util));
                        }
                        $request[$key]->participants = implode(' , ', $utilisateur);
                    }
                    if (!empty($data_post['participant'])) {
                        if (!empty($participants_id) && in_array(intval($data_post['participant']), $participants_id))
                            $result[] = $request[$key];
                    } else {
                        $result[] = $request[$key];
                    }
                }
                $data['taches'] = $result;
                $data['utilisateur'] = $this->getListUser();

                $this->renderComponant("Taches/taches/liste_tacheValide", $data);
            }
        }
    }

    function getUtil($util_id)
    {
        $this->load->model('Utilisateur_m', 'util');
        $param = array(
            'clause' => array('util_id' => $util_id),
            'method' => 'row',
            'columns' => array('util_prenom'),
        );
        $request = $this->util->get($param);
        return (!empty($request)) ? $request->util_prenom  : '';
    }

    public function getlisteTheme()
    {
        $this->load->model('Theme_m');
        $params = array(
            'columns' => array('theme_id', 'theme_libelle')
        );
        $request = $this->Theme_m->get($params);
        return $request;
    }


    public function getFormTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                if ($data_post['action'] == "edit") {
                    $this->load->model('Tache_m');
                    $params = array(
                        'clause' => array('tache_id' => $data_post['tache_id']),
                        'method' => "row"
                    );
                    $data['tache'] = $request = $this->Tache_m->get($params);
                }
                $data['action'] = $data_post['action'];
                $data['utilisateur'] = $this->getListUser();
                $data['theme'] = $this->getlisteTheme();
                $data['prospect'] = $this->listProspect();
                $data['client'] = $this->listClient();


                $this->renderComponant("Taches/taches/form-tache", $data);
            }
        }
    }

    function listProspect()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $clause = array('etat_prospect' => 1);
                $data = array();
                $this->load->model('Prospect_m');
                $params = array(
                    'clause' => $clause,
                    'columns' => array('prospect_id', 'prospect_nom', 'prospect_prenom'),
                    'order_by_columns' => "prospect_nom ASC",
                );

                $request = $this->Prospect_m->get($params);

                return $request;
            }
        }
    }

    function listClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $clause = array('client_etat' => 1);
                $data = array();
                $this->load->model('Client_m');
                $params = array(
                    'clause' => $clause,
                    'columns' => array('client_id', 'client_nom', 'client_prenom'),
                    'order_by_columns' => "client_nom ASC",
                );

                $request = $this->Client_m->get($params);

                return $request;
            }
        }
    }

    public function cruTache()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Tache_m');
                $this->load->model('TacheClient_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = $data['action'];
                    $data_retour = array("prospect_id" => $data['prospect_id'], "action" => $action);
                    $data['tache_etat'] = 1;
                    $data['tache_createur'] = $_SESSION['session_utilisateur']['util_prenom'];

                    $data['tache_date_creation'] = date('Y-m-d H:i:s', strtotime($data['tache_date_creation']));
                    $data['tache_date_echeance'] = date('Y-m-d H:i:s', strtotime($data['tache_date_echeance']));
                    $data['type_tache_id'] = 2;

                    $data_client = array(
                        'tache_nom' =>  $data['tache_nom'],
                        'tache_createur' => $_SESSION['session_utilisateur']['util_prenom'],
                        'tache_date_creation' => $data['tache_date_creation'],
                        'tache_date_echeance' => $data['tache_date_echeance'],
                        'tache_prioritaire' => $data['tache_prioritaire'],
                        'theme_id' => $data['theme_id'],
                        'tache_texte' => $data['tache_texte'],
                        'client_id' => $data['client_id'],
                        'tache_etat' => 1,
                        'type_tache_id' => 3,
                        'participant' => implode(',', $data['participant'])

                    );

                    $data_tache = array(
                        'tache_nom' =>  $data['tache_nom'],
                        'tache_createur' => $_SESSION['session_utilisateur']['util_prenom'],
                        'tache_date_creation' => $data['tache_date_creation'],
                        'tache_date_echeance' => $data['tache_date_echeance'],
                        'tache_prioritaire' => $data['tache_prioritaire'],
                        'theme_id' => $data['theme_id'],
                        'tache_texte' => $data['tache_texte'],
                        'tache_etat' => 1,
                        'type_tache_id' => 1,
                        'participant' => implode(',', $data['participant'])

                    );

                    unset($data['action']);
                    unset($data['undefined']);
                    unset($data['client_id']);
                    unset($data['search_terms']);
                    $data['participant'] = implode(',', $data['participant']);

                    $clause = null;
                    if ($action == "edit") {
                        $clause = array("tache_id" => $data['tache_id']);
                        $data_retour['tache_id'] = $data['tache_id'];
                        unset($data['tache_id']);
                    }
                    // else{
                    //     $data['tache_date_creation'] = date_sql($data['tache_date_creation'],'-','jj/MM/aaaa');
                    // $data['tache_date_echeance'] = date_sql($data['tache_date_echeance'],'-','jj/MM/aaaa');
                    // }

                    //var_dump($data);die;

                    $controlRequest = (intval($data['checkclient']) == 1) ? 'client' : ((intval($data['checkprospect']) == 1) ? 'prospect' : NULL);
                    if ($controlRequest != NULL) {
                        unset($data['checkclient']);
                        unset($data['checkprospect']);
                        $request = ($controlRequest == "client") ? $this->Tache_m->save_data($data_client, $clause) : $this->Tache_m->save_data($data, $clause);
                        if (!empty($request)) {
                            $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Tâche ajoutée avec succès" : "Modification réussie"));
                        }
                    } else if ($controlRequest == NULL) {
                        $request = $this->Tache_m->save_data($data_tache, $clause);
                        if (!empty($request)) {
                            $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Tâche ajoutée avec succès" : "Modification réussie"));
                        }
                    } else {
                        $retour = array(
                            'data_retour' => null,
                            'form_validation' => array(
                                'select_type' => ""
                            ),
                            'status' => false,
                            'type' => "error"
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function removeTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('Tache_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tache_id' => $data['tache_id']),
                    'columns' => array('tache_id', 'tache_nom'),
                    'method' => "row",
                );
                $request_get = $this->Tache_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tache_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce tâche ? ",
                            'btnConfirm' => "Supprimer la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tache_id' => $data['tache_id']);
                    $data_update = array('tache_etat' => 0);
                    $request = $this->Tache_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tache_id']),
                            'message' => "La tâche a bien été supprimée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function valideTache()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation de la tâche…",
                );
                $this->load->model('Tache_m');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('tache_id' => $data['tache_id']),
                    'columns' => array('tache_id', 'tache_nom'),
                    'method' => "row",
                );
                $request_get = $this->Tache_m->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['tache_id'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous vraiment valider cette tâche ? ",
                            'btnConfirm' => "Valider la tâche",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('tache_id' => $data['tache_id']);
                    $data_update = array(
                        'tache_valide' => 1,
                        'tache_date_validationn' => Carbon::now()->toDateTimeString()
                    );
                    $request = $this->Tache_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['tache_id']),
                            'message' => "Tâche validée avec succès",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
