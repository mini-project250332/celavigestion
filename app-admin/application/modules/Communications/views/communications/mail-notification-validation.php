<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="margin: 0; padding: 0; font-family: Arial, sans-serif; background-color: #ffffff;">

    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: center;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                    <tr>
                        <td style="padding: 40px 0 20px 0;">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align: left; padding: 0 30px; color: #222; font-size: 14px; line-height: 1.5;">
                                        
                                        <p>
                                            Chers Administrateurs,
                                        </p>

                                        <?php 
                                            $createur_id = $communications->util_id;
                                            $createur = array_filter($utilisateurs,function($util) use ($createur_id){
                                                return $util->id == $createur_id;
                                            });
                                            $createur = array_values($createur);

                                            $utilisateur_id = $communications->util_validate;
                                            $validateur = array_filter($utilisateurs,function($util) use ($utilisateur_id){
                                                return $util->id == $utilisateur_id;
                                            });
                                            $validateur = array_values($validateur);
                                        ;?>

                                        <p>
                                            Nous avons le plaisir de vous informer qu'une communication importante a été validée 
                                            <?=(!empty($validateur)) ? ' par M. '.$validateur[0]->prenom.' '.$validateur[0]->nom : '';?> 
                                            sur la plateforme "Celavigestion.fr". Les détails de cette validation sont les suivants :
                                        </p>

                                        <p style="margin-bottom:10px">
                                            <ul>
                                                <li>
                                                    <span style="font-weight: bold;"> Nom de la Communication : </span><?=$communications->coms_libelle;?></li>
                                                <li>
                                                    <span style="font-weight: bold;"> Communication crée par : </span>
                                                    <?=(!empty($createur)) ? $createur[0]->prenom.' '.$createur[0]->nom : '';?></li>
                                                <li>
                                                    <span style="font-weight: bold;"> Date et heure de création : </span> <?=$communications->coms_date_creation;?></li>
                                            </ul>
                                        </p>

                                        <?php if(!empty($validateur)):?>
                                            <p>
                                                <ul>
                                                    <li>
                                                        <span style="font-weight: bold;">Communication validée par : </span> M. <?=(!empty($validateur)) ? $validateur[0]->prenom.' '.$validateur[0]->nom : '';?></li>
                                                    <li>
                                                        <span style="font-weight: bold;"> Date et Heure de la validation : </span> <?=(isset($communications->coms_date_validation) && !empty($communications->coms_date_validation)) ? dateFr_textCarbone($communications->coms_date_validation) : '';?>
                                                    </li>
                                                </ul>
                                            </p>
                                        <?php endif;?>

                                        <p>
                                            Cette communication validée est maintenant prête à être diffusée ou mise en œuvre conformément aux procédures établies. Nous vous encourageons à prendre les mesures appropriées pour vous assurer que cette communication soit correctement déployée.
                                        </p>

                                        <p>
                                            Si vous avez des questions supplémentaires ou avez besoin de plus amples informations concernant cette validation, n'hésitez pas à contacter notre équipe de support.
                                        </p>

                                        <p>
                                            Nous vous remercions de votre engagement en tant qu'administrateurs et de votre contribution à son succès continu.
                                        </p>

                                        <p>Cordialement,</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; padding: 0 30px;">
                                    <?php if (isset($signature) && !empty($signature)): ?>
                                        <img src="<?= base_url($signature); ?>" alt="Signature" style="max-width: 450px; display: block;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            </td>
        </tr>
    </table>

</body>
</html>
