<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="margin: 0; padding: 0; font-family: Arial, sans-serif; background-color: #ffffff;">

<table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="text-align: center;">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                <tr>
                    <td style="padding: 40px 0 20px 0;">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; padding: 0 30px; color: #222; font-size: 14px; line-height: 1.5;">
                                    <?= nl2br(htmlspecialchars($contenu)); ?>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; padding: 0 30px;">
                                    <?php if (!empty($signature)): ?>
                                        <img src="<?= base_url($signature); ?>" alt="Signature" style="max-width: 450px; display: block;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <?php if(!empty($attachments)): ?>
                    <tr>
                        <td>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align: left; padding: 0 30px; color: #222; font-size: 14px; line-height: 1.5;">
                                        <p style="margin-top: 20px;"> 
                                            <?=count($attachments) > 1 ? 'Pièces jointes / Attached documents :' : 'Pièce jointe / Attached document :';?>
                                        </p>
                                        <ul style="padding-top: 0px;">
                                            <?php foreach ($attachments as $attachment): ?>
                                                <li>
                                                    <span style="margin-right:10px;"> <strong><?=$attachment->coms_pjoin_libelle;?></strong></span>
                                                    <a href="<?=base_url($attachment->coms_pjoin_paths);?>" target="_blank">
                                                        Télécharger / Download
                                                    </a>
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
