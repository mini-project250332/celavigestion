<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="margin: 0; padding: 0; font-family: Arial, sans-serif; background-color: #ffffff;">

    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: center;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                    <tr>
                        <td style="padding: 40px 0 20px 0;">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align: left; padding: 0 30px; color: #222; font-size: 14px; line-height: 1.5;">
                                        
                                        <p>
                                            Chers Administrateurs,
                                        </p>

                                        <?php 
                                            $createur_id = $communications->util_id;
                                            $createur = array_filter($utilisateurs,function($util) use ($createur_id){
                                                return $util->id == $createur_id;
                                            });
                                            $createur = array_values($createur);

                                            $demandeur_id = $_SESSION['session_utilisateur']['util_id'];
                                            $demandeur = array_filter($utilisateurs,function($util) use ($demandeur_id){
                                                return $util->id == $demandeur_id;
                                            });
                                            $demandeur= array_values($demandeur);
                                        ;?>

                                        <p>
                                            Nous avons le plaisir de vous informer que Mr/Mm <?=(!empty($demandeur)) ? $demandeur[0]->prenom.' '.$demandeur[0]->nom : '';?> a soumis une demande de validation pour une communication sur la plateforme celavigestion.fr. Les détails de cette demande sont les suivants :
                                        </p>

                                        <p style="margin-bottom:10px">
                                            <ul>
                                                <li>
                                                    <span style="font-weight: bold;"> Nom de la Communication : </span><?=$communications->coms_libelle;?>
                                                </li>
                                                <li>
                                                    <span style="font-weight: bold;"> Communication crée par : </span>
                                                    <?=(!empty($createur)) ? $createur[0]->prenom.' '.$createur[0]->nom : '';?>
                                                </li>
                                                <li>
                                                    <span style="font-weight: bold;"> Date et heure de création : </span> <?=$communications->coms_date_creation;?>
                                                </li>
                                            </ul>
                                        </p>

                                        <p>
                                            Pour valider la communication, il faut aller sur la communication concernée dans le logiciel.
                                        </p>
                                        
                                        <p>Cordialement,</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; padding: 0 30px;">
                                    <?php if (isset($signature) && !empty($signature)): ?>
                                        <img src="<?= base_url($signature); ?>" alt="Signature" style="max-width: 450px; display: block;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            </td>
        </tr>
    </table>

</body>
</html>
