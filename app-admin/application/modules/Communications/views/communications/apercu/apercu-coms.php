<div class="contenair-title">
    <div class="col-2"></div>
    <div class="col px-2 flex-row-reverse">

    	<?php 
    		$verify_data = true;
    		if(empty($coms_con))
    			$verify_data = false;

    		if(!isset($coms_con->coms_con_contenu) || empty($coms_con->coms_con_contenu))
    			$verify_data = false;

    		if(!isset($coms_con->coms_con_objet) || empty($coms_con->coms_con_objet))
    			$verify_data = false;

    		if(empty($list_destinataire))
    			$verify_data = false;

    		$valider_envoie = true;
    		if(!$verify_data || empty($communications->coms_date_validation))
    			$valider_envoie = false;
    	?>

    	<?php if($verify_data && !empty($communications->coms_date_validation)): ?>
    		<?php if($_SESSION['session_utilisateur']['rol_util_valeur'] == 1 && $communications->coms_status !=2): ?>
		        
		        <button style="cursor:pointer" type="button" data-id="<?= $coms_id ?>" id="valider-envoie-coms" class="btn btn-success mx-1" <?=(!$valider_envoie) ? "disabled='true'" : "";?>  <?=($communications->coms_status==2) ? "disabled='true'" : "";?>>
		            <span class="fas fa-paper-plane me-1"></span>
		            <span> Valider l'envoi du mail </span>
		        </button>

		    <?php endif; ?>
        <?php endif; ?>

        <?php if($_SESSION['session_utilisateur']['rol_util_valeur'] == 1 && empty($communications->coms_date_validation)): ?>

	        <button style="cursor:pointer" type="button" data-id="<?= $coms_id ?>" id="valider-coms" class="btn btn-primary mx-1" <?=(!$verify_data) ? "disabled='true'" : "";?> >
	            <span class="fas fa-check-circle"></span>
	            <span> Valider l'opération </span>
	        </button>

	    <?php endif; ?>


	    <?php if($_SESSION['session_utilisateur']['rol_util_valeur'] != 1 && empty($communications->coms_date_demande)): ?>

	        <button style="cursor:pointer" type="button" data-id="<?= $coms_id ?>" id="demande-valided-coms" class="btn btn-primary mx-1" <?=(!$verify_data) ? "disabled='true'" : "";?>>
	            <span class="fas fa-paper-plane"></span>
	            <span> Envoie une demande de validation aux l'administrateur</span>
	        </button>

	    <?php endif; ?>

	    <?php if($communications->coms_status == 3 && !empty($communications->coms_date_demande)): ?>
		    <div class="me-2 px-2 d-flex">
		    	<div>
		            <h6 class="mb-0 ">
		              	<span class="fw-semi-bold">Date d'envoie du demande</span> 
		              	<span class="fw-normal"> : <?=dateFr_textCarbone($communications->coms_date_demande);?></span> 
		            </h6>
		            <div>
		              	<span class="me-2 fs--1 fw-semi-bold">Status: </span>
		                <div class="badge rounded-pill badge-soft-primary fs--2">
		                	En cours de validation
		                	<span class="fas fa-check"></span>
		                </div>
		            </div>
		      	</div>

		      	<div class="ps-3">
		      		<div class="d-flex align-items-center position-relative">
					    <div class="avatar avatar-xl">
					        <span class="fas fa-user-circle fa-w-16 fs-4 me-2"></span>
					    </div>
					    <div class="ms-2">
					        <h6 class="mb-1 fw-semi-bold">
					            <?php
					                $util_demande_id = $communications->util_demande;
					                $util_demande = array_filter($emetteur,function($util) use ($util_demande_id){
					                    return $util->id == $util_demande_id;
					                });

					                $util_demande = array_values($util_demande);
						            echo $util_demande[0]->prenom.' '.$util_demande[0]->nom;
						        ?>
					        </h6>
					        <p class="text-500 fs--2 mb-0"><?=$util_demande[0]->rol_util_libelle;?></p>
					    </div>
					</div>
		      	</div>
			</div>
		<?php endif; ?>

	    <?php if($communications->coms_status == 2 && !empty($communications->coms_date_envoi)): ?>
		    <div class="me-2 px-2 d-flex">
		    	<div>
		            <h6 class="mb-0 ">
		              	<span class="fw-semi-bold">Date d'envoie </span> 
		              	<span class="fw-normal"> : <?=dateFr_textCarbone($communications->coms_date_envoi);?></span> 
		            </h6>
		            <div>
		              	<span class="me-2 fs--1 fw-semi-bold">Status: </span>
		                <div class="badge rounded-pill badge-soft-success fs--2">
		                	Envoyé 
		                	<span class="fas fa-check"></span>
		                </div>
		            </div>
		      	</div>

		      	<div class="ps-3">
		      		<div class="d-flex align-items-center position-relative">
					    <div class="avatar avatar-xl">
					        <span class="fas fa-user-circle fa-w-16 fs-4 me-2"></span>
					    </div>
					    <div class="ms-2">
					        <h6 class="mb-1 fw-semi-bold">
					            <?php
					                $utilEnvoi_id = $communications->util_envoi;
					                $utilEnvoi = array_filter($emetteur,function($util) use ($utilEnvoi_id){
					                    return $util->id == $utilEnvoi_id;
					                });

					                $utilEnvoi = array_values($utilEnvoi);
						            echo $utilEnvoi[0]->prenom.' '.$utilEnvoi[0]->nom;
						        ?>
					        </h6>
					        <p class="text-500 fs--2 mb-0"><?=$utilEnvoi[0]->rol_util_libelle;?></p>
					    </div>
					</div>
		      	</div>
			</div>
		<?php endif; ?>

		<?php if($communications->coms_status == 1 && !empty($communications->coms_date_validation)): ?>
		    <div class="me-2 px-2 d-flex align-items-center">
		    	<div>
		            <h6 class="mb-0 ">
		              	<span class="fw-semi-bold">Date de validation </span> 
		              	<span class="fw-normal"> : <?=dateFr_textCarbone($communications->coms_date_validation);?> </span> 
		            </h6>
		            <div>
		              	<span class="me-2 fs--1 fw-semi-bold">Status: </span>
		                <div class="badge badge rounded-pill badge-soft-warning fs--2">
		                	Validé
		                	<span class="fas fa-check"></span>
		                </div>
		            </div>
		      	</div>

		      	<div class="px-3">
		      		<div class="d-flex align-items-center position-relative">
					    <div class="avatar avatar-xl">
					        <span class="fas fa-user-circle fa-w-16 fs-4 me-2"></span>
					    </div>
					    <div class="ms-2">
					        <h6 class="mb-1 fw-semi-bold">
					        	<?php
					                $validateur_id = $communications->util_validate;
					                $validateur = array_filter($emetteur,function($util) use ($validateur_id){
					                    return $util->id == $validateur_id;
					                });

					                $validateur = array_values($validateur);
						            echo $validateur[0]->prenom.' '.$validateur[0]->nom;
						        ?>
					        </h6>
					        <p class="text-500 fs--2 mb-0"><?=$validateur[0]->rol_util_libelle;?></p>
					    </div>
					</div>
		      	</div>

		      	<div style="width:2px;height: 32px;background: #b6c1d2;"></div>
			</div>
		<?php endif; ?>



    </div>
    <div class="col-2"></div>
</div>

<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col pb-5">
        
        <div class="bg-primary rounded py-2 ps-4 pe-6">
            <h5 class="mb-1 fs-0 text-white">Aperçu sur la communication</h5>
        </div>

        <div class="py-2 pt-3">

        	<?php if(empty($list_destinataire)):?>
				<div class="alert alert-warning border-2 d-flex align-items-center" role="alert">
				  	<div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
					 <p class="mb-0 flex-1">
					  	La liste des destinataires est actuellement vide. Veuillez ajouter des destinataires à cette communication.
					</p>
				</div>
			<?php endif; ?>
            
            <?php if(!empty($coms_con)):?>

            	<div class="p-4 border border-3 rounded-4">

		        	<div class="row m-0 py-2">
	            		<div class="col-2 ps-0">
	            			<span class="text-800 fs-0 fw-semi-bold">Objet du mail :</span>
	            		</div>
	            		<div class="col">
	            			<h5 class="mb-0"> <?= isset($coms_con->coms_con_objet) ? $coms_con->coms_con_objet : "" ?> </h5>
	            		</div>
	            	</div>

					<hr class="my-1">
	            	<div class="row m-0 py-2">
	            		<div class="col-2 ps-0">
	            			<span class="text-800 fs-0 fw-semi-bold">
	            				Destinataires du mail :
	            			</span>
	            		</div>
	            		<div class="col">
	            			
	            			<?php if(count($list_destinataire) < 10): ?>
	                            <?php   foreach ($list_destinataire as $key => $value) : ?>
	                                <span class="badge <?=(!check_isEmailValid($value->client_email))? 'badge-soft-danger' : 'badge-soft-info'?>"><?=$value->client_email;?></span>
	                            <?php endforeach;?>
	                        <?php else: ?>
	                            <?php $i = 0;?>
	                            <?php foreach (array_slice($list_destinataire, 0,10) as $value):?>
	                                <span class="badge <?=(!check_isEmailValid($value->client_email))? 'badge-soft-danger' : 'badge-soft-info'?>"><?=$value->client_email;?></span>
	                                <?php $i++;?>
	                            <?php endforeach;?>

	                            <span style="cursor: pointer;" class="badge badge-soft-primary" data-bs-toggle="modal" data-bs-target="#liste-destinataire"> + <?=count($list_destinataire) - $i;?> autres</span>

	                            <div class="modal fade" id="liste-destinataire" data-bs-keyboard="false" data-bs-backdrop="static" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	                                <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
	                                    <div class="modal-content border-0">
	                                      
	                                        <div class="modal-header">
	                                        	<div class="rounded-top-lg ps-4 pe-6">
	                                                <h5 class="mb-1" id="staticBackdropLabel">
	                                                   Tous les mails destinataires (<?=count($list_destinataire);?>)
	                                                </h5>
	                                            </div>
	                                        </div>

	                                        <div class="modal-body p-0 pb-3">
	                                            <div class="px-4">
	                                                <div class="row">
	                                                    <div class="col-12">
	                                                		<table class="table table-hover table-sm fs--1 mb-0">
											                    <thead class="text-900">
											                        <tr>
											                            <th class="p-2" scope="col">
											                                Propriétaires
											                            </th>
											                            <th class="p-2" scope="col">Adresse mail</th>
											                            <th class="p-2" scope="col">Nom programme</th>
											                            <th class="p-2" scope="col">
											                                <span style="white-space: nowrap;">N° mandat</span>
											                            </th>
											                            <th class="p-2" scope="col">
											                                <span style="white-space: nowrap;">Etat mandat</span>
											                            </th>
											                            <th class="p-2" scope="col">
											                                <span style="white-space: nowrap;">Suivi par</span>
											                            </th>
											                        </tr>
											                    </thead>
											                    <tbody>
											                        <?php foreach ($list_destinataire as $key => $value) : ?>
											                            <tr style="cursor:pointer;" >
											                                <td class="ps-2">
											                                    <?= $value->client_nom . ' ' . $value->client_prenom ?>
											                                </td>
											                                <td>
											                                	<span class="<?=(!check_isEmailValid($value->client_email))? 'text-danger' : ''?>"><?= $value->client_email ?></span>
											                                	<?php if(!empty($value->client_email1)): ?>
												                                	<span class="px-2">|</span> <span class="<?=(!check_isEmailValid($value->client_email1))? 'text-danger' : ''?>"><?=$value->client_email1;?></span>
												                                <?php endif;?>
											                                </td>
											                                <td><?= $value->progm_nom ?></td>
											                                <td class="text-center"><?= $value->mandat_id ?></td>
											                                <td class="pe-1 ">
											                                    <span style="white-space: nowrap;"><?= $value->etat_mandat_libelle ?></span>
											                                </td>
											                                <td class="pe-1 ">
											                                    <span style="white-space: nowrap;"><?= $value->util_prenom ?></span>
											                                </td>
											                            </tr>
											                        <?php endforeach; ?>
											                    </tbody>
											                </table>

	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>

	                                        <div class="modal-footer py-2">
	                                        	<button type="button" class="btn btn-sm btn-primary d-flex" data-bs-dismiss="modal">Fermer</button>
	                                        </div>

	                                    </div>
	                                </div>
	                            </div>
	                        <?php endif;?>
	            		</div>
	            	</div>
					                
	            	<hr class="my-1">
	            	<div class="row m-0 py-2">
	            		<div class="col-2 ps-0">
	            			<span class="text-800 fs-0 fw-semi-bold">Emetteur du mail :</span>
	            		</div>
	            		<div class="col">
	            			<span class="text-800 fw-semi-bold">
		            			<?php 
                                    if(isset($coms_con->coms_util_emetteur)){
                                        $emetteur_id = $coms_con->coms_util_emetteur;
                                        $filtre_emetteur = array_filter($emetteur,function($util) use ($emetteur_id){
                                            return $util->id == $emetteur_id;
                                        });

                                        $filtre_emetteur = array_values($filtre_emetteur);
                                        echo $filtre_emetteur[0]->prenom.' '.$filtre_emetteur[0]->nom;
                                    }
                                ?>
					        </span>
	            		</div>
	            	</div>

	            	<hr class="my-1">
	            	<div class="text-800 fw-semi-bold py-2">
	                   <span> Contenu du mail :  </span>
	                </div>
	                <div class="flex-1 mt-2 p-4 bg-light">
	                    <?= isset($coms_con->coms_con_contenu) ? nl2br($coms_con->coms_con_contenu) : "" ?>
	                </div>

	                <hr class="my-1">       	   
					<div class="row m-0 py-2">
	            		<div class="col-2 ps-0">
	            			<span class="text-800 fs-0 fw-semi-bold">Fichier(s) joint(s) :</span>
	            		</div>
	            		<div class="col">
	            			<div class="d-flex flex-wrap">
	            				<?php if(!empty($coms_pjoin)): ?>
		                            <?php foreach ($coms_pjoin as $key => $value): ?>
		                                <span class="border px-2 py-1 rounded-3 d-flex flex-between-center bg-white m-1 fs--1">
		                                    <span class="ms-2">
		                                        <?=$value->coms_pjoin_libelle;?> 
		                                        (<?=formatSize(intval($value->coms_pjoin_size));?>)
		                                    </span>
		                                </span>
		                            <?php endforeach; ?>
		                        <?php endif;?>
	                        </div>
	            		</div>
	            	</div>

	                <hr class="my-1">      	   
					<div class="row m-0 py-2">
	            		<div class="col-2 ps-0">
	            			<span class="text-800 fs-0 fw-semi-bold">Signature :</span>
	            		</div>
	            		<div class="col">
	            			<?php if(!empty($signature)):?>
	            				<div>
	            					<?php if (!empty($signature)): ?>
                                        <img src="<?= base_url($signature); ?>" alt="Signature" style="max-width: 400px; display: block;">
                                    <?php endif; ?>
	            				</div>
							<?php else: ?>
								<span class="text-800 fw-semi-bold">Aucun signature</span>
	            			<?php endif;?>
					        
	            		</div>
	            	</div>

	            	<?php if(!empty($communications->coms_date_validation) && $communications->coms_status == 2): ?>
	            		<hr class="my-1">
		            	<div class="row m-0 py-2">
		            		<div class="col-2 ps-0">
		            			<span class="text-800 fs-0 fw-semi-bold">Date validation : </span>
		            		</div>
		            		<div class="col">
		            			<span class="text-800 fw-semi-bold">
			            			<?=dateFr_textCarbone($communications->coms_date_validation);?>
						        </span>
		            		</div>
		            	</div>

		            	<hr class="my-1">
		            	<div class="row m-0 py-2">
		            		<div class="col-2 ps-0">
		            			<span class="text-800 fs-0 fw-semi-bold">Validé par : </span>
		            		</div>
		            		<div class="col">
		            			<span class="text-800 fw-semi-bold">
			            			<?php
						                $validateur_id = $communications->util_validate;
						                $validateur = array_filter($emetteur,function($util) use ($validateur_id){
						                    return $util->id == $validateur_id;
						                });

						                $validateur = array_values($validateur);
							            echo $validateur[0]->prenom.' '.$validateur[0]->nom;
							        ?>
						        </span>
		            		</div>
		            	</div>
		            <?php endif;?>
		        </div>
					
			<?php else: ?>

				<div class="alert alert-warning border-2 d-flex align-items-center" role="alert">
				  	<div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
					 <p class="mb-0 flex-1">
					  	Le contenu du courrier électronique de cette communication est vide. Veuillez prendre un moment pour rédiger le contenu de cette communication.
					</p>
				</div>

			<?php endif; ?>

        </div>
            
    </div>
    <div class="col-2"></div>
</div>