<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="margin: 0; padding: 0; font-family: Arial, sans-serif; background-color: #ffffff;">

    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="text-align: center;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                    <tr>
                        <td style="padding: 40px 0 20px 0;">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align: left; padding: 0 30px; color: #222; font-size: 14px; line-height: 1.5;">
                                        
                                        
                                        <?php 
                                            $createur_id = $communications->util_id;
                                            $createur = array_filter($utilisateurs,function($util) use ($createur_id){
                                                return $util->id == $createur_id;
                                            });
                                            $createur = array_values($createur);

                                            $validateur_id = $communications->util_validate;
                                            $validateur = array_filter($utilisateurs,function($util) use ($validateur_id){
                                                return $util->id == $validateur_id;
                                            });
                                            $validateur = array_values($validateur);

                                            $util_envoi_id = $communications->util_envoi;
                                            $util_envoi = array_filter($utilisateurs,function($util) use ($util_envoi_id){
                                                return $util->id == $util_envoi_id;
                                            });
                                            $util_envoi = array_values($util_envoi);
                                        ;?>

                                        <p>
                                            Bonjour <?=(!empty($createur)) ? $createur[0]->prenom.' '.$createur[0]->nom : '';?>,
                                        </p>


                                        <p>
                                            Nous avons le plaisir de vous informer que <?=(!empty($util_envoi)) ? $util_envoi[0]->prenom.' '.$util_envoi[0]->nom : '';?> a envoyé la communication " <?=$communications->coms_libelle;?> " sur la plateforme celavigestion.fr le <?=(isset($communications->coms_date_envoi) && !empty($communications->coms_date_envoi)) ? dateFr_textCarbone($communications->coms_date_envoi) : '';?>.
                                        </p>

                                        
                                        <p>Cordialement,</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="800" style="max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; padding: 0 30px;">
                                    <?php if (isset($signature) && !empty($signature)): ?>
                                        <img src="<?= base_url($signature); ?>" alt="Signature" style="max-width: 450px; display: block;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            </td>
        </tr>
    </table>

</body>
</html>
