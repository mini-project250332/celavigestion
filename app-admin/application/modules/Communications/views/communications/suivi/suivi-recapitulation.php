<div class="contenair-title">
    <div class="col-2"></div>
    <div class="col px-2 flex-row-reverse"></div>
    <div class="col-2"></div>
</div>


<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col">
        
        <div class="bg-primary rounded-2 py-2 ps-4 pe-4 d-flex align-items-center justify-content-between">
            <div>
                <h5 class="mb-1 fs-0 text-white" >Suivi et récapitulation de l'envoi des mails : </h5>
            </div>
            <div>
                <button onclick="getContenutNavCom('suivi');" class="btn btn-sm btn-outline-secondary border mx-1 py-0 fw-normal d-flex align-items-center" style="height: 32px;">
                    <span class="text-white me-1 fas fa-sync-alt"></span>
                    <span class="text-white">Actualiser</span>
                </button>
            </div>
        </div>

        <?php if(isset($communications) && !empty($communications->data_coms)) : ?>
            
            <div class="pb-4 pt-2">
                <div class="row g-0">

                    <div class="col-6 col-md-3 border-200 border-md-200 border-md-start border-md-end border-md-top pb-3 pt-3 ps-3">
                        <p class="font-sans-serif lh-1 mb-1 fs-2"><?=count($list_destinataire);?></p>
                        <div class="d-flex align-items-center">
                            <h6 class="fs--1 mb-0 ">Adresses (Emails envoyés)</h6>
                            <h6 class="fs--2 ps-3 mb-0 text-primary"> </h6>
                        </div>
                    </div>

                    <div class="col-6 col-md-3 border-200 border-md-200 border-md-start border-md-end border-md-top pb-3 pt-3 ps-3">
                        <?php 
                            $filteredData_Livree = array_filter($list_destinataire, function ($item) {
                                return isset($item->mailjetInfo) && $item->mailjetInfo && $item->mailjetInfo['success'];
                            });
                        ?>
                        <p class="font-sans-serif lh-1 mb-1 fs-2">
                            <?php 
                                $resultfilteredData_Livree = (count($filteredData_Livree) * 100) / count($list_destinataire);
                                $result_filteredData_Livree = ($resultfilteredData_Livree == 0) ? '0' : number_format($resultfilteredData_Livree, 2);
                                echo $result_filteredData_Livree.'%';
                            ?>
                        </p>
                        <div class="d-flex align-items-center">
                            <h6 class="fs--1 mb-0 text-success">Livrés : </h6>
                            <h6 class="fs--2 ps-3 mb-0 text-primary"><?=count($filteredData_Livree).' Email'.(count($filteredData_Livree)>1 ? 's' : '');?></h6>
                        </div>
                    </div>

                    <div class="col-6 col-md-2 border-200 border-md-200  border-md-end border-md-top pb-3 pt-3 ps-3">
                        <?php 
                            $filteredData_Ouverts = array_filter($list_destinataire, function ($item) {
                                return isset($item->mailjetInfo) && $item->mailjetInfo && $item->mailjetInfo['success'] && $item->mailjetInfo['data'][0]['Status'] === "opened";
                            });
                        ?>
                        <p class="font-sans-serif lh-1 mb-1 fs-2">
                            <?php 
                                $resultfilteredData_Ouverts = (count($filteredData_Ouverts) * 100) / count($list_destinataire);
                                $result_filteredData_Livree = ($resultfilteredData_Ouverts == 0) ? '0' : number_format($resultfilteredData_Ouverts, 2);
                                echo $result_filteredData_Livree.'%';
                            ?>
                        </p>
                        <div class="d-flex align-items-center">
                            <h6 class="fs--1 mb-0 text-success">Ouverts : </h6>
                            <h6 class="fs--2 ps-3 mb-0 text-primary"> <?=count($filteredData_Ouverts).' Email'.(count($filteredData_Ouverts)>1 ? 's' : '');?></h6>
                        </div>
                    </div>

                    <div class="col-6 col-md-2 border-200 border-md-200  border-md-end border-md-top pb-3 pt-3 ps-3">
                        <?php 
                            $filteredData_Spam = array_filter($list_destinataire, function ($item) {
                                return isset($item->mailjetInfo) && $item->mailjetInfo && $item->mailjetInfo['success'] && $item->mailjetInfo['data'][0]['Status'] === "spam";
                            });
                        ?>
                        <p class="font-sans-serif lh-1 mb-1 fs-2">
                            <?php 
                                $resultfilteredData_Spam = (count($filteredData_Spam) * 100) / count($list_destinataire);
                                $result_filteredData_Spam = ($resultfilteredData_Spam == 0) ? '0' : number_format($resultfilteredData_Spam, 2);
                                echo $result_filteredData_Spam.'%';
                            ?>
                        </p>
                        <div class="d-flex align-items-center">
                            <h6 class="fs--1 text-warning mb-0">Spam : </h6>
                            <h6 class="fs--2 ps-3 mb-0 text-primary"><?=count($filteredData_Spam).' Email'.(count($filteredData_Spam)>1 ? 's' : '');?></h6>
                        </div>
                    </div>

                    <div class="col-6 col-md-2 border-200 border-md-200  border-md-end border-md-top pb-3 pt-3 ps-3">
                        <?php 
                            $filteredData_Bloques = array_filter($list_destinataire, function ($item) {
                                return isset($item->mailjetInfo) && $item->mailjetInfo && $item->mailjetInfo['success'] && $item->mailjetInfo['data'][0]['Status'] === "blocked";
                            });
                        ?>
                        <p class="font-sans-serif lh-1 mb-1 fs-2">
                            <?php 
                                $resultfilteredData_Bloques = (count($filteredData_Bloques) * 100) / count($list_destinataire);
                                $result_filteredData_Bloques = ($resultfilteredData_Bloques == 0) ? '0' : number_format($resultfilteredData_Bloques, 2);
                                echo $result_filteredData_Bloques.'%';
                            ?>
                        </p>
                        <div class="d-flex align-items-center">
                            <h6 class="fs--1 mb-0 text-danger">Bloqués : </h6>
                            <h6 class="fs--2 ps-3 mb-0 text-primary"><?=count($filteredData_Bloques).' Email'.(count($filteredData_Bloques)>1 ? 's' : '');?></h6>
                        </div>
                    </div>

                    <hr class="">
                </div>
                
                <div class="row m-0">
                    <div class="col"></div>
                    <div class="col-auto pt-2 pb-1 px-0 justify-content-end">
                            <select class="form-select form-select-sm d-none" id="filtre-status-message">
                                <option value="0">Tous les status</option>
                                <option value="En attente"> En attente </option>
                                <option value="Livrés"> Livrés </option>
                                <option value="Ouverts"> Ouverts </option>
                                <option value="Cliqués"> Cliqués </option>
                                <option value="Marqués comme spam"> Marqués comme spam </option>
                                <option value="Bloqués"> Bloqués </option>
                            </select>
                    </div>
                    <!--
                    <div class="col pt-2 pb-1 px-0 d-flex ">
                         

                        <button class="btn btn-sm border mx-1 d-flex justify-content-center align-items-center py-0" style="height: 32px;">
                            <span>Télécharger la liste au format CSV</span>
                        </button>
                    </div> -->
                </div>

                <div id="destinataire-table-suivi" class="d-flex flex-column">
                    
                    <table class="table table-hover table-sm fs--1 mb-0">
                        <thead class="text-900">
                            <tr>
                                <th class="p-2 sort" data-sort="dest_nom" >Destinataire</th>
                                <th class="p-2 sort" data-sort="dest_mail">Mail</th>
                                <th class="p-2 sort" data-sort="date">
                                    <span style="white-space: nowrap;">Date de réception</span>
                                </th>
                                <th class="p-2 sort" data-sort="status">
                                    <span style="white-space: nowrap;">Status</span>
                                </th>
                                <th class="p-2 pe-4 d-flex justify-content-end">
                                    <span>Action</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php foreach($list_destinataire as $dest): ?>
                                <tr class="align-middle">
                                    <td class="ps-2 dest_nom"> <?=$dest->client_nom.' '.$dest->client_prenom; ?></td>
                                    <td class="dest_mail">  <?=$dest->client_email;?></td>
                                    <td class="pe-1 date">
                                        <?php if(!empty($dest->mailjetInfo) && $dest->mailjetInfo['success'] === true):?>
                                            <span style="white-space: nowrap;">
                                                <?php 
                                                    $ArrivedAt = date_create($dest->mailjetInfo['data'][0]['ArrivedAt']);
                                                    echo date_format($ArrivedAt, "d/m/Y H:i");
                                                    //echo dateFr_textCarbone(date_format($ArrivedAt, "Y-m-d H:i"));
                                                ?>
                                            </span>
                                        <?php else: ?>
                                            <span style="white-space: nowrap;">---</span>
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td class="pe-1 status">
                                        <?php if(!empty($dest->mailjetInfo) && $dest->mailjetInfo['success'] === true):?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'unknown'): ?>
                                                <span class="badge badge-soft-warning fs--1">Inconnu</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'queued'): ?>
                                                <span class="badge badge-soft-secondary fs--1">En attente</span>
                                            <?php endif; ?> 

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'sent'): ?>
                                                <span class="badge badge-soft-success fs--1">Livrés</span>
                                            <?php endif; ?> 

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'opened'): ?>
                                                <span class="badge badge-soft-success fs--1">Ouvert</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'clicked'): ?>
                                                <span class="badge badge-soft-success fs--1">Cliqués</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'unsub'): ?>
                                                <span class="badge badge-soft-danger fs--1">Désabonnements</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'blocked'): ?>
                                                <span class="badge badge-soft-danger fs--1">Bloqué</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'spam'): ?>
                                                <span class="badge badge-soft-warning fs--1">Spam</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'softbounced'): ?>
                                                <span class="badge badge-soft-warning fs--1">Rebonds temporaires</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'hardbounced'): ?>
                                                <span class="badge badge-soft-warning fs--1">Rebonds permanents</span>
                                            <?php endif; ?>

                                            <?php if ($dest->mailjetInfo['data'][0]['Status'] == 'deferred'): ?>
                                                <span class="badge badge-soft-warning fs--1">Différé</span>
                                            <?php endif; ?>

                                        <?php else: ?>
                                            <span style="white-space: nowrap;">---</span>
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td class="pe-2 d-flex justify-content-end">
                                        <button data-mail="<?=$dest->client_email;?>" data-messageID="<?=(!empty($dest->mailjetInfo)) ? $dest->mailjetInfo['MessageID'] : "" ;?>" data-client_id="<?=$dest->client_id;?>" class="btn btn-sm border mx-1 border-primary py-0 fw-normal text-primary detail-envoie" style="height: 25px;">
                                            <span>Détail</span>
                                        </button>
                                    </td>
                                </tr>
                             <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end pt-2">
                        <div class="d-flex align-items-center p-0">
                            <div class="pagination-list-js px-2">
                                <ul  class="pagination pagination-sm mb-0" style=""></ul>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    var options = {
                        valueNames: [ 'dest_nom', 'dest_mail', 'date' , 'status'],
                        page: 20,
                        pagination: true
                    };
                    var list_destinataire = new List('destinataire-table-suivi', options);

                    const filtre_status = document.getElementById("filtre-status-message");
                    filtre_status.addEventListener("change", (e) => {
                        // console.log(e.target.value);
                    });

                </script>
            </div>
        <?php else: ?>
            
            <div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                <div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                 <p class="mb-0 flex-1">
                    Pas de récapitulatif pour cette communication car la fonctionnalité de suivi a été mise en service après sa création, c'est à dire à partir du 19 octobre 2023. Le suivi n'est disponible que pour les communications créées à partir du 19 octobre 2023.
                </p>
            </div>

        <?php endif; ?>
    </div>
    <div class="col-2"></div>
</div>


<div class="modal fade" id="modalDetail_envoi" data-bs-keyboard="false" data-bs-backdrop="static" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl mt-6" role="document">
        <div class="modal-content border-0">
            <!-- 
                <div class="position-absolute top-0 end-0 mt-3 me-3 z-index-1">
                    <button id="close_modalDetail_envoi" class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"></button>
                </div> 
            -->
            <div class="modal-body p-0">
                <div class="bg-light rounded-top-lg py-3 ps-4 pe-6">
                    <h4 class="mb-1" id="staticBackdropLabel">Informations détaillées de l'envoi</h4>
                </div>
                <div id="div-information-envoie" class="p-4"></div>
            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button id="close_modalDetail_envoi" class="btn btn-secondary" type="button" >Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var modal_detailMail = new bootstrap.Modal(document.getElementById('modalDetail_envoi'), {
        keyboard: false
    });
</script>

