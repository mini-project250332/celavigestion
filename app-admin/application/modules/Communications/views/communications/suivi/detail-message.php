<div class="row">
    <div class="col">
        <div class="d-flex">
            <span class="fa-stack ms-n1 me-3">
                <i class="fas fa-circle  text-200" style="height:2em;width:2em"></i>
                <i class="fa-inverse fa-stack-1x text-primary fas fa-info" data-fa-transform="shrink-2"></i>
            </span>

            <div class="flex-1">
                <ul class="nav flex-lg-column fs--1">
                    <li class="nav-item me-2 me-lg-0">
                        <a class="nav-link px-0" href="#!">
                            <span class="pe-1 text-primary"> Objet : </span>
                            <span class="text-800 fw-medium "> <?=$objet;?> </span>
                        </a>
                    </li>
                    <li class="nav-item me-2 me-lg-0">
                        <a class="nav-link px-0" href="#!">
                            <span class="pe-1 text-primary"> Emetteur : </span>
                            <span class="text-800 fw-medium "> 
                                <?php 
                                    if(isset($coms_con->coms_util_emetteur)){
                                        $emetteur_id = $coms_con->coms_util_emetteur;
                                        $filtre_emetteur = array_filter($emetteur,function($util) use ($emetteur_id){
                                            return $util->id == $emetteur_id;
                                        });

                                        $filtre_emetteur = array_values($filtre_emetteur);
                                        echo $filtre_emetteur[0]->prenom.' '.$filtre_emetteur[0]->nom;
                                    }
                                ?>
                                <span class="badge badge-soft-info"><?=$filtre_emetteur[0]->email;?></span>
                            </span>
                        </a>
                    </li>
                    <li class="nav-item me-2 me-lg-0">
                        <a class="nav-link px-0" href="#!">
                            <span class="pe-1 text-primary"> Destinataires : </span>
                            <span class="text-800 fw-medium "> <?=$clients_destinataire->client_nom.' '.$clients_destinataire->client_prenom;?>  
                                <span class="badge badge-soft-info"><?=$mail;?></span>
                            </span>
                        </a>
                    </li>
                </ul>

                <hr class="my-2" />

                <ul class="nav flex-lg-column fs--1">
                    
                    <li class="nav-item me-2 me-lg-0">
                        <div class="me-2 d-flex">
                            <div>
                                <h6 class="mb-0 fs--1">
                                    <span class="fw-semi-bold text-primary">Date d'envoi </span> 
                                    <span class="fw-normal"> : <?=dateFr_textCarbone($communications->coms_date_envoi);?></span> 
                                </h6>
                            </div>

                            <div class="ps-3">
                                <div class="d-flex align-items-center position-relative">
                                    <div class="ms-2">
                                        <h6 class="mb-1 fw-semi-bold fs--1">
                                            <?php
                                                $utilEnvoi_id = $communications->util_envoi;
                                                $utilEnvoi = array_filter($emetteur,function($util) use ($utilEnvoi_id){
                                                    return $util->id == $utilEnvoi_id;
                                                });

                                                $utilEnvoi = array_values($utilEnvoi);
                                                echo $utilEnvoi[0]->prenom.' '.$utilEnvoi[0]->nom;
                                            ?>

                                            <span class="text-500 fs--2 mb-0 px-2">[ <?=$utilEnvoi[0]->rol_util_libelle;?> ]</span>
                                        </h6>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <?php if(isset($info_mailjet)): ?>
                        <?php if($info_mailjet['success']): ?>
                            <li class="nav-item me-2 me-lg-0">
                                <a class="nav-link px-0" href="javascript:void(0);">
                                    <span class="pe-1 text-primary"> Date d'arrivée au destinataire : </span>
                                    <span class="text-800 fw-medium "> 
                                        <?php 
                                            $ArrivedAt = date_create($info_mailjet['data'][0]['ArrivedAt']);
                                            echo dateFr_textCarbone(date_format($ArrivedAt, "Y-m-d H:i"));
                                        ?>
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item me-2 me-lg-0">
                                <a class="nav-link px-0" href="javascript:void(0);">
                                    <span class="pe-1 text-primary"> Status : </span>
                                    <span class="text-800 fw-medium "> 
                                        <?php if ($info_mailjet['data'][0]['Status'] == 'opened'): ?>
                                            <span class="badge badge-soft-success fs--1">Ouvert</span>
                                        <?php endif; ?>
                                        
                                        <?php if ($info_mailjet['data'][0]['Status'] == 'spam'): ?>
                                            <span class="badge badge-soft-warning fs--1">Spam</span>
                                        <?php endif; ?>

                                        <?php if ($info_mailjet['data'][0]['Status'] == 'blocked'): ?>
                                            <span class="badge badge-soft-error fs--1">Bloqué</span>
                                        <?php endif; ?>

                                        <?php if ($info_mailjet['data'][0]['Status'] == 'sent'): ?>
                                            <span class="badge badge-soft-secondary fs--1">En attente</span>
                                        <?php endif; ?>
                                    </span>
                                </a>
                            </li>

                        <?php endif; ?>
                    <?php endif; ?>

                </ul>
                <hr class="my-2" />
            </div>
        </div>

        <div class="d-flex mt-2">
            <span class="fa-stack ms-n1 me-3">
                <i class="fas fa-circle  text-200" style="height:2em;width:2em"></i>
                <i class="fa-inverse fa-stack-1x text-primary fas fa-align-left" data-fa-transform="shrink-2"></i>
            </span>

            <div class="flex-1">
                <h5 class="mb-2 fs-0">Contenu</h5>
                <p class="text-word-break fs-0">
                    <?=$contenu["HTMLPart"];?>
                </p>

                <hr class="my-2" />
            </div>
        </div>

        <div class="d-flex">

            <span class="fa-stack ms-n1 me-3">
                <i class="fas fa-circle  text-200" style="height:2em;width:2em"></i>
                <i class="fa-inverse fa-stack-1x text-primary fas fa-paperclip" data-fa-transform="shrink-2"></i>
            </span>

            <div class="flex-1">
                <h5 class="mb-2 fs-0">Pièce(s) jointe(s)</h5>
                <ul class="nav flex-lg-column fs--1">
                    <?php if(!empty($coms_pjoin)): ?>
                        <?php foreach ($coms_pjoin as $key => $value): ?>
                            <li class="nav-item me-2 rounded-2 border me-lg-0 mb-1">
                                <a class="nav-link nav-link-card-details mb-0 text-700" href="javascript:void(0);">
                                    <?=$value->coms_pjoin_libelle;?> 
                                    (<?=formatSize(intval($value->coms_pjoin_size));?>)
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>