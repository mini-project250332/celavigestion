<?php echo form_tag('Communications/cruContenuComs', array('method' => "post", 'class' => '', 'id' => 'cruContenuComs')); ?>

    <div class="contenair-title">
        <div class="col-1"></div>
        <div class="col-auto ps-3">
            <h5 class="mb-1 fs-0"> Formulaire pour contenu du mail</h5>
        </div>
        <div class="col px-2 flex-row-reverse">
            
            <button type="submit" data-id="<?= $coms_id ?>" id="terminer-contenu-coms" class="btn btn-sm btn-primary mx-1">
                <span class="fas fa-edit fas me-1"></span>
                <span> Enregistrer </span>
            </button>

            <button type="button" data-id="<?= $coms_id ?>" id="annuler-contenu-coms" class="btn btn-sm btn-secondary">
                <span class="fas fa-undo fas me-1"></span>
                <span> Annuler </span>
            </button>

        </div>
        <div class="col-1"></div>
    </div>

    <input type="hidden" name="coms_id" value="<?=$coms_id?>">

    <?php if(isset($coms_con->coms_con_id)):?>
        <input type="hidden" name="coms_con_id" value="<?=$coms_con->coms_con_id;?>">
    <?php endif; ?>

    <div class="row py-2 m-0">
        <div class="col-1"></div>
        <div class="col">
  
            <div class="px-2 pt-2">
                
                <div class="row m-0">
                    <div class="col ps-0">

                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label"> Objet du mail * :</label>
                                <input type="text" class="form-control fs--1 " id="coms_objet" name="coms_objet" autocomplete="off" data-formatcontrol="true" data-require="true" value="<?= isset($coms_con->coms_con_objet) ? $coms_con->coms_con_objet : "" ?>">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group p-0">
                            <div>
                                <label class="form-label">Contenu du mail * :</label>
                                <textarea class="form-control fs--1" id="coms_contenu" name="coms_contenu" style="height: 406px" data-require="true"><?= isset($coms_con->coms_con_contenu) ? htmlspecialchars($coms_con->coms_con_contenu) : "";?></textarea>
                                <div class="help"> </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-3 px-2 pe-0">
                        <div class="form-group bg-light rounded-top-lg h-100">
                            <label class="px-2">Cliquez ici pour ajouter des champs de fusion </label>
                            <hr class="mt-0 mb-1">
                            <div class="pb-2 px-2">
                                <div class="pb-2">
                                    <span class="fs--1 fw-medium">Objet du mail :</span>
                                    <div class="m-b-2">
                                        <span onclick="insert_character_to_caracter('coms_objet','@nom_programme');" class="badge bg-primary cursor-pointer">
                                            @Nom du programme
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@nom_client ');" class="badge bg-primary cursor-pointer">
                                            @Nom client
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@prenom_client ');" class="badge bg-primary cursor-pointer">
                                            @Prénom client
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@nom_utilisateur ');" class="badge bg-primary cursor-pointer mb-1">
                                            @Nom utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@prenom_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Prénom utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@adresse_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Adresse utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@email_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Email utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_objet','@telephone_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Téléphone utilisateur
                                        </span>
                                    </div>
                                </div>

                                <span class="fs--1 fw-medium">Contenu du mail</span>
                                <hr class="mt-0 mb-1">

                                <div class="pb-2">
                                    <span class="fs--1 fw-medium">Informations sur le programme :</span>
                                    <div class="m-b-2">
                                        <span onclick="insert_character_to_caracter('coms_contenu','@nom_programme');" class="badge bg-primary cursor-pointer">
                                            @Nom du programme
                                        </span>
                                    </div>
                                </div>

                                <div class="pb-2">
                                    <span class="fs--1 fw-medium">Informations sur le client</span>
                                    <div class="m-b-2">
                                        <span onclick="insert_character_to_caracter('coms_contenu','@nom_client ');" class="badge bg-primary cursor-pointer">
                                            @Nom client
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@prenom_client ');" class="badge bg-primary cursor-pointer">
                                            @Prénom client
                                        </span>
                                    </div>
                                </div>

                                <div>
                                    <span class="fs--1 fw-medium">Informations sur le suivi clientèle :</span>
                                    <div class="m-b-2">
                                        <span onclick="insert_character_to_caracter('coms_contenu','@nom_utilisateur ');" class="badge bg-primary cursor-pointer mb-1">
                                            @Nom utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@prenom_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Prénom utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@adresse_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Adresse utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@email_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Email utilisateur
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@telephone_utilisateur ');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Téléphone utilisateur
                                        </span>
                                    </div>
                                </div>

                                <hr class="mt-0 mb-1">
                                <div>
                                    <span class="fs--1 fw-medium">Informations sur accès </span>
                                    <div class="m-b-2">
                                        <span onclick="insert_character_to_caracter('coms_contenu','@identifiant_intranet');" class="badge bg-primary cursor-pointer mb-1">
                                            @Identifiant Intranet
                                        </span>
                                        <span onclick="insert_character_to_caracter('coms_contenu','@mot_passe_intranet');" class="badge bg-primary cursor-pointer  mb-1">
                                            @Mot de passe Intranet
                                        </span>
                                        
                                        <!-- 
                                            <span onclick="insert_character_to_caracter('coms_contenu','@identifiant_location_meublee');" class="badge bg-primary cursor-pointer  mb-1">
                                                @Identifiant Location Meublée
                                            </span>
                                            <span onclick="insert_character_to_caracter('coms_contenu','@mot_passe_location_meublee');" class="badge bg-primary cursor-pointer  mb-1">
                                                @Mot de passe location Meublée
                                            </span>
                                        -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row m-0 mt-2">
                    <div class="col ps-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label"> Emetteur du mail :</label>
                                <select class="form-select fs--1" id="coms_util_emetteur" name="coms_util_emetteur">
                                    <?php foreach ($emetteur as $key => $value) : ?>
                                        <option value="<?= $value->id ?>" <?php echo isset($coms_con->coms_util_emetteur) ? (($coms_con->coms_util_emetteur !== NULL) ? (($coms_con->coms_util_emetteur == $value->id) ? "selected" : " ") : "") : "" ?>>
                                            <?= $value->prenom ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col ps-1 pe-0">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label"> Signature :</label>
                                <select class="form-select fs--1" id="coms_signature" name="coms_signature">
                                        <option value="2" <?= isset($coms_con->coms_con_signature) && $coms_con->coms_con_signature == 2 ? "selected" : ""; ?>>Signature image de l'émetteur du mail</option>
                                        <option value="0" <?= !isset($coms_con->coms_con_signature) || $coms_con->coms_con_signature != 2 ? "selected" : ""; ?>>
                                            Aucun signature
                                        </option>
                                    </select>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="form-group p-0 my-2">
                    <div>
                        <label class="form-label"> Fichiers joints : </label>
                        <input class="form-control fs--1" type="file" id="coms_fichier" name="coms_fichier[]" multiple="multiple" style="line-height: 1.5!important;">
                        <div class="help"></div>
                    </div>
                </div>

                <?php if(!empty($coms_pjoin)) :?>
                    <div class="bg-light py-2">
                        <div class="pb-2 px-2">Liste des pieces joints du contenu:</div>
                        <hr class="m-0">
                        <div class="pt-2 px-2 w-100 d-flex flex-wrap">
                            <?php foreach ($coms_pjoin as $key => $value): ?>
                                
                                <span id="piece-joint-<?=$value->coms_pjoin_id;?>" class="border px-2 py-1 rounded-3 d-flex flex-between-center bg-white m-1 fs--1">
                                    <span class="ms-2">
                                        <?=$value->coms_pjoin_libelle;?> 
                                        (<?=formatSize(intval($value->coms_pjoin_size));?>)
                                    </span>

                                    <button data-id="<?=$value->coms_pjoin_id;?>"   type="button" class="btn bg-500 rounded-circle text-300 text-white ms-6 p-0 d-flex justify-content-center align-items-center delete-piecejoin" data-bs-toggle="tooltip" data-bs-placement="right" title="" data-bs-original-title="Supprimer" aria-label="Supprimer" style="width: 25px; height: 25px;">
                                        <span class="fas fa-times fa-w-11"></span>
                                    </button>
                                </span>
                                
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>


            </div>
               
        </div>
        <div class="col-1"></div>
    </div>

<?php echo form_close(); ?>



                