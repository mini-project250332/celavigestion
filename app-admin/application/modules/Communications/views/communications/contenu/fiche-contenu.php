<div class="contenair-title">
    <div class="col-2"></div>
    <div class="col px-2 flex-row-reverse">
        <button type="button" data-id="<?= $coms_id ?>" id="update-contenu-coms" class="btn btn-sm btn-primary" <?=($communications->coms_status==2) ? "disabled='true'" : "";?>>
            <span class="fas fa-edit fas me-1"></span>
            <span> <?= (!empty($coms_con)) ? 'Mettre à jour' : 'Créer le contenu du mail';?> </span>
        </button>
    </div>
    <div class="col-2"></div>
</div>

<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col">
        
        <div class="bg-primary rounded-2 py-2 ps-4 pe-6">
            <h5 class="mb-1 fs-0 text-white" >Contenu de l'envoi</h5></h5>
        </div>

        <?php if(!empty($coms_con)) : ?>

            <div class="p-4 border border-3 rounded-4 mt-2">
                
                <div class="px-3">

                    <div class="d-flex m-0">
                        <div style="width:160px">
                            Objet du mail :
                        </div>
                        <div class="flex-grow-1">
                            <span class="text-800 fw-semi-bold">
                                <?= isset($coms_con->coms_con_objet) ? $coms_con->coms_con_objet : "" ?>
                            </span>
                        </div>
                    </div>
                    <hr>

                    <div class="text-800 fw-semi-bold px-1">
                       <span> Contenu du mail :  </span>
                    </div>
                    <div class="flex-1 mt-2 p-4 bg-light">
                        <?= isset($coms_con->coms_con_contenu) ? nl2br($coms_con->coms_con_contenu) : "" ?>
                    </div>
                    
                    <div class="px-2">
                        <table class="table table-borderless  mb-0 mt-3">
                            <tbody>
                                <tr class="border-bottom">
                                    <td class="ps-0" style="width:160px">Emetteur du mail :</td>
                                    <td class="pe-0">
                                        <span class="text-800 fw-semi-bold">
                                            <?php 
                                                if(isset($coms_con->coms_util_emetteur)){
                                                    $emetteur_id = $coms_con->coms_util_emetteur;
                                                    $filtre_emetteur = array_filter($emetteur,function($util) use ($emetteur_id){
                                                        return $util->id == $emetteur_id;
                                                    });
                                                    $filtre_emetteur = array_values($filtre_emetteur);
                                                    echo $filtre_emetteur[0]->prenom.' '.$filtre_emetteur[0]->nom;
                                                }
                                            ?>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="ps-0">Signature : </td>
                                    <td class="pe-0">
                                        <span class="text-800 fw-semi-bold">
                                        <?= isset($coms_con->coms_con_signature) ? ($coms_con->coms_con_signature ==  2 ? "Signature image de l'émetteur du mail" : "Aucun signature") : "Aucun signature" ?>
                                        </span>
                                    </td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="ps-0">Fichier(s) joint(s) : </td>
                                    <td class="pe-0">
                                        <div class="d-flex flex-column">
                                            
                                            <?php foreach ($coms_pjoin as $key => $value): ?>
                                                <div class="d-flex align-items-center">
                                                    <span class="border px-2 py-1 rounded-3 d-flex flex-between-center bg-white m-1 fs--1">
                                                        <span class="ms-2">
                                                            <?=$value->coms_pjoin_libelle;?> 
                                                            (<?=formatSize(intval($value->coms_pjoin_size));?>)
                                                        </span>
                                                    </span>
                                                    <span class="px-2 d-flex align-items-center">
                                                        <a class="text-primary" href="javascript:window.open('<?=base_url($value->coms_pjoin_paths);?>', '_blank');"><small><?=base_url($value->coms_pjoin_paths);?></small></a>
                                                    </span>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="py-2"></div>
                </div>

            </div>

        <?php else: ?>

            <div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                <div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                 <p class="mb-0 flex-1">
                    Le contenu du courrier électronique de cette communication est vide. Veuillez prendre un moment pour rédiger le contenu de cette communication.
                </p>
            </div>

        <?php endif; ?>
    </div>
    <div class="col-2"></div>
</div>
