<div class="row mt-3 ms-1 me-2 mb-0">
    <div class="col-8">
        <h5 class="fs-1">Communications clients</h5>
    </div>
    <div class="col">
        <div class="row align-input-button">
            <div class="col">
                <div class="form-group inline p-0">
                    <div class="col-sm-12">
                        <label data-width="100"> Status :</label>
                        <select class="form-select fs--1" id="statut-com" name="statut-com">
                            <option value="">Tous</option>
                            <option value="0">En édition</option>
                            <option value="3">En cours de validation</option>
                            <option value="1">Validé</option>
                            <option value="2">Envoyé</option>
                        </select>
                        <div class="help"></div>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <button class="btn btn-primary fs--1" id="add-coms"><i class="fas fa-plus"></i> Ajouter une communication </button>
            </div>
        </div>
    </div>
    <hr>
</div>

<div class="contenair-list">
    <div class="row m-0">
        <div id="data-list" class="col p-0"></div>
    </div>
</div>

<script>
    only_visuel_gestion();
    $(document).ready(function() {
        listCommunications();
    });
</script>