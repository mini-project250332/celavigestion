<?php echo form_tag('Communications/cruCommunications', array('method' => "post", 'class' => '', 'id' => 'updateCommunications')); ?>

<div class="contenair-title">
    <div class="col-2"></div>
    <div class="px-2 col">
        <h5 class="fs-0"></h5>
    </div>
    <div class="px-2">
        <button type="button" id="annuler-update-coms" class="btn btn-secondary fs--1 mx-2">
            <i class="fas fa-times me-1"></i>Annuler
        </button>

        <button type="submit" class="btn btn-sm btn-primary">
            <span class="fas fa-check fas me-1"></span>
            <span> Enregistrer </span>
        </button>
    </div>
    <div class="col-2"></div>
</div>

<?php if (isset($coms_id)) : ?>
    <input type="hidden" name="coms_id" id="coms_id" value="<?= $coms_id; ?>">
<?php endif; ?>

<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col">
        
        <div class="bg-light rounded py-2 ps-2 pe-6">
            <h5 class="mb-1" id="staticBackdropLabel">
               <h5 class="fs-0">Modifier les détails de la Communication</h5>
            </h5>
        </div>
        <div class="px-2">
            <div class="mb-3">
                <div class="form-group p-0 mt-4">
                    <div>
                        <label class="form-label"> Nom de la communication :</label>
                        <input type="text" class="form-control fs--1 " id="coms_libelle" name="coms_libelle" autocomplete="off" data-formatcontrol="true" data-require="true" <?=(isset($communications->coms_libelle)) ? 'value="'.$communications->coms_libelle.'"' : "";?> >
                        <div class="help"></div>
                    </div>
                </div>
            </div>

            <div class="form-group p-0">
                <div>
                    <label> Description :</label>
                    <textarea class="form-control fs--1" id="coms_description" name="coms_description" style="height: 150px" ><?= isset($communications->coms_description) ? htmlspecialchars($communications->coms_description) : "" ?></textarea>
                    <div class="help"> </div>
                </div>
            </div>

        </div>
            
    </div>
    <div class="col-2"></div>
</div>

<?php echo form_close(); ?>