<div class="contenair-title" style="padding-bottom: 10px !important;">
    <div class="px-2">
        <input type="hidden" value="<?= $coms_id ?>" id="coms_id_nav">
        <span class="px-2" style="font-size : 1.5em;">
            <i class="fas fa-envelope"></i>
        </span>
        <h5 class="fs-1 px-1">
            Fiche communication : <span> <?= $communications->coms_libelle ?> </span>
            <!--
                Status
                <span class="badge bg-secondary cursor-pointer fs--1">
                    Mail 
                </span>
            -->
        </h5>
    </div>
    <div class="px-2">
        <button id="retourListeCom" class="btn btn-sm btn-secondary mx-1">
            <i class="fas fa-undo me-1"></i>
            <span>Retour</span>
        </button>
    </div>
</div>

<div class="contenair-content px-2">
    <!-- justify-content-center -->
    <ul class="nav nav-tabs bg-light" id="comTab" role="tablist">
        
        <li class="nav-item" onclick="getContenutNavCom('communication');">
            <a class="nav-link active" id="communication-tab" data-bs-toggle="tab" href="#tab-communication" role="tab" aria-controls="tab-communication" aria-selected="true">Informations générales</a>
        </li>

        <li class="nav-item" onclick="getContenutNavCom('contenu');">
            <a class="nav-link" id="contenu-tab" data-bs-toggle="tab" href="#tab-contenu" role="tab" aria-controls="tab-contenu" aria-selected="false">Contenu de l'envoi</a>
        </li>

        <li class="nav-item" onclick="getContenutNavCom('destinataire');">
            <a class="nav-link" id="destinataire-tab" data-bs-toggle="tab" href="#tab-destinataire" role="tab" aria-controls="tab-destinataire" aria-selected="false">Destinataire(s) du mail</a>
        </li>

        <li class="nav-item" onclick="getContenutNavCom('apercu');">
            <a class="nav-link" id="envoi-tab" data-bs-toggle="tab" href="#tab-apercu" role="tab" aria-controls="tab-apercu" aria-selected="false">Aperçu et validation</a>
        </li>


        <li class="nav-item" onclick="getContenutNavCom('suivi')">
            <a class="nav-link" id="suivi-tab" data-bs-toggle="tab" href="#tab-suivi" role="tab" aria-controls="tab-suivi" aria-selected="false">Suivi et récapitulation</a>
        </li>

    </ul>

    <div class="tab-content p-1" id="comTabContent">
        <div class="tab-pane fade show active" id="tab-communication" role="tabpanel" aria-labelledby="communication-tab"></div>
        <div class="tab-pane fade" id="tab-contenu" role="tabpanel" aria-labelledby="contenu-tab"></div>
        <div class="tab-pane fade" id="tab-destinataire" role="tabpanel" aria-labelledby="destinataire-tab"></div>
        <div class="tab-pane fade" id="tab-apercu" role="tabpanel" aria-labelledby="envoi-tab"></div>
        <div class="tab-pane fade" id="tab-suivi" role="tabpanel" aria-labelledby="suivi-tab"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        get_informationComs('fiche');
    });
</script>