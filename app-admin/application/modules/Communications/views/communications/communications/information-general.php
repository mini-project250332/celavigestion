<div class="contenair-title">
    <div class="col-2"></div>
    <div class="col px-2 flex-row-reverse">
        <button type="button" data-id="<?= $coms_id ?>" id="update-creation-coms" class="btn btn-sm btn-primary">
            <span class="fas fa-edit fas me-1"></span>
            <span> Modifier </span>
        </button>
    </div>
    <div class="col-2"></div>
</div>

<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col ">
        
        <div class="bg-primary rounded-top-lg py-2 ps-4 pe-6">
            <h5 class="mb-1 fs-0 text-white">
               Informations sur la communication
            </h5>
        </div>


        <div class="p-4 border border-3 rounded-4 mt-2 ">
            <div class="px-1">
                <div class="d-flex m-0">
                    <div>
                        Nom de la communication : 
                    </div>
                    <div class="flex-grow-1">
                        <span class="text-800 fw-semi-bold">
                            &nbsp;<?=isset($communications->coms_libelle) ? $communications->coms_libelle : "" ?>
                        </span>
                    </div>
                </div>
                <hr>

                <div class="d-flex m-0">
                    <div class="pe-2" style="white-space: nowrap;">
                        Description :
                    </div>
                    <div class="flex-grow-1">
                        <span class="text-800 fw-semi-bold">
                            <?= isset($communications->coms_description) ? htmlspecialchars($communications->coms_description) : "" ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
            
    </div>
    <div class="col-2"></div>
</div>
