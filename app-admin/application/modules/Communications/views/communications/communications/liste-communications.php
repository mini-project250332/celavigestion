<input type="hidden" value="<?php echo base_url('Communication'); ?>" id="com-open-tab-url" />
<?php if (empty($communications)) : ?>
    <div class="row">
        <div class="col-12 pt-2">
            <div class="text-center">
                Aucune communication enregistrée
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="table-responsive scrollbar pt-2">
        <table class="table table-sm table-striped fs--1 mb-0">
            <thead class="bg-300 text-900">
                <tr>
                    <th class="text-start" scope="col">Nom de la Communication</th>
                    <th class="text-start" scope="col">Creation</th>
                    <th class="text-start" scope="col">Validation</th>
                    <th class="text-start" scope="col">Expédition</th>
                    <th class="text-center" scope="col">Nombre de destinataires</th>
                    <th class="text-center" scope="col">Etat</th>
                    <th class="text-end" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($communications as $key => $value) : ?>
                    <tr id="rowComm-<?= $value->coms_id ?>">
                        <td class="text-start align-middle">
                            <div class="flex-1">
                                <h6 class="mb-0 fw-semi-bold">
                                    <span class="stretched-link text-900" href="pages/user/profile.html">
                                        <?= $value->coms_libelle ?>
                                    </span>
                                </h6>
                                <p class="text-700 fs--2 mb-0"></p>
                            </div>
                        </td>
                        <td class="text-start align-middle">
                            <div class="flex-1">
                                <h6 class="mb-0 fw-semi-bold">
                                    <span class="stretched-link text-900" href="pages/user/profile.html">
                                        Créer le <?= date("d/m/Y", strtotime(str_replace('/', '-', $value->coms_date_creation)));?>
                                    </span>
                                </h6>
                                <p class="text-700 fs--2 mb-0">
                                    par <?= $value->util_prenom.' '.$value->util_nom ?> 
                                </p>
                            </div>
                        </td>
                        <td class="text-start align-middle">
                            <?php if(isset($value->coms_date_validation)) :?>
                                <div class="flex-1">
                                    <h6 class="mb-0 fw-semi-bold">
                                        <span class="stretched-link text-900" href="pages/user/profile.html">
                                            Valider le <?= date("d/m/Y", strtotime(str_replace('/', '-', $value->coms_date_validation)));?>
                                        </span>
                                    </h6>
                                    <p class="text-700 fs--2 mb-0">
                                        par <?php 
                                                if(isset($value->coms_date_validation)){
                                                    $validateur_id = $value->util_validate;
                                                    $validateur = array_filter($utilisateur,function($util) use ($validateur_id){
                                                        return $util->id == $validateur_id;
                                                    });
                                                    $validateur = array_values($validateur);
                                                    echo $validateur[0]->prenom.' '.$validateur[0]->nom;
                                                }
                                            ?> 
                                    </p>
                                </div>
                            <?php else: ?>
                                <span> - </span>
                            <?php endif; ?>
                        </td>
                        <td class="text-start align-middle">
                            <?php if(isset($value->coms_date_envoi)) :?>
                                <div class="flex-1">
                                    <h6 class="mb-0 fw-semi-bold">
                                        <span class="stretched-link text-900" href="pages/user/profile.html">
                                            Expedié le <?= date("d/m/Y", strtotime(str_replace('/', '-', $value->coms_date_envoi)));?>
                                        </span>
                                    </h6>
                                    <p class="text-700 fs--2 mb-0">
                                        par <?php 
                                                if(isset($value->coms_date_envoi)){
                                                    $expediteur_id = $value->util_envoi;
                                                    $expediteur = array_filter($utilisateur,function($util) use ($expediteur_id){
                                                        return $util->id == $expediteur_id;
                                                    });
                                                    $expediteur = array_values($expediteur);
                                                    echo $expediteur[0]->prenom.' '.$expediteur[0]->nom;
                                                }
                                            ?> 
                                    </p>
                                </div>
                            <?php else: ?>
                                <span> - </span>
                            <?php endif; ?>
                        </td>
                        <td class="text-center align-middle">
                            <?php if(isset($value->destinataire)) : ?>
                                <?php if(!empty($value->destinataire)): ?>
                                    <span>
                                        <?php
                                            $destinataire = unserialize($value->destinataire->client_destinataire_id);
                                            echo count($destinataire);
                                        ?>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-center align-middle">
                            <?php if(isset($value->coms_status)) : ?>
                                <?php if($value->coms_status == 0) : ?>
                                    <span class="badge badge rounded-pill badge-soft-secondary">
                                        En édition
                                        <span class="fas fa-edit fa-w-16 ms-1"></span>
                                    </span>
                                <?php endif; ?>

                                <?php if($value->coms_status == 1) : ?>
                                    <span class="badge badge rounded-pill badge-soft-warning">
                                        Validé
                                        <span class="fas fa-check fa-w-16 ms-1"></span>
                                    </span>
                                <?php endif; ?>
                                <?php if($value->coms_status == 2) : ?>
                                    <span class="badge badge rounded-pill badge-soft-success">
                                        Envoyé
                                        <span class="fas fa-check fa-w-16 ms-1"></span>
                                    </span>
                                <?php endif; ?>

                                <?php if($value->coms_status == 3) : ?>
                                    <span class="badge badge rounded-pill badge-soft-primary">
                                        En cours de validation
                                        <span class="fas fa-check fa-w-16 ms-1"></span>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-end align-middle">
                            <div class="d-flex justify-content-end ">
                                <button data-id="<?= $value->coms_id ?>" id="" class="btn btn-outline-primary icon-btn p-0 m-1 d-flex align-items-center justify-content-center btnFicheCom" type="button" style="height:30px;width: 30px;">
                                    <span class="fas fa-play fs-0 m-1"></span>
                                </button>

                                <button data-id="<?= $value->coms_id ?>" class="btn d-flex align-items-center justify-content-center btn-outline-danger icon-btn p-0 m-1 only_visuel_gestion btn-supprimer-coms" style="height:30px;width: 30px;" type="button" <?=($_SESSION['session_utilisateur']['rol_util_valeur'] != 1) ? "disabled" : ""; ?>>
                                    <span class="bi bi-trash fs-1 m-1"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>
   