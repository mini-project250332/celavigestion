<?php
    $destinataire_selectionner = [];
    if(isset($destinataire) && !empty($destinataire))
        $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);
?>

<table class="table table-hover table-sm fs--1 mb-0" id="proprietaire-destinataire">
    <thead class="text-900">
        <tr>
            <th class="p-2" scope="col">
                <div class="form-check mb-0 d-flex align-items-center">
                    <input class="form-check-input me-2" type="checkbox" id="check-resultat">
                    <label class="form-check-label mb-0">
                        Propriétaires
                    </label>
                </div>
            </th>
            <th class="p-2" scope="col">Pays</th>
            <th class="p-2" scope="col">Nom programme</th>
            <th class="p-2" scope="col">
                <span style="white-space: nowrap;">Etat mandat</span>
            </th>
            <th class="p-2 text-center" scope="col" style="display:table-cell;">
                <span style="white-space: nowrap;">Suivi par</span>
            </th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($clients as $key => $value) : ?>
            <?php if(!in_array($value->client_id, $destinataire_selectionner)):?>
                <tr 
                    data-pays="<?=(!empty($value->client_pays)) ? preg_replace('/\s+/','-', strtolower($value->client_pays)) : 'non-renseigne';?>" 
                    data-etatMandat="<?=$value->etat_mandat_id;?>" 
                    data-clientEtat="<?=$value->client_etat;?>" 
                    data-program="<?=$value->progm_id;?>" 
                    data-suiviClient="<?= is_null($value->util_id) ? '-1' : $value->util_id ;?>" 
                    data-gestionnaireLot="<?= (isset($value->gestionnaires) && !empty($value->gestionnaires)) ? implode(',',$value->gestionnaires) : -1;?>" 
                class="row-proprietaire" style="cursor:pointer;">
                    <td class="ps-2">
                        <div class="form-check mb-0 d-flex align-items-center">
                            <input class="form-check-input client_row" type="checkbox" name="client_row" id="checkbox_<?= $value->client_id ?>" value="<?= $value->client_id ?>">
                            
                            <label for="checkbox_<?= $value->client_id ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" class="form-check-label mb-0 ms-2 ">
                                <span class="d-flex flex-column lh-sm">
                                    <span> <?= $value->client_nom . ' ' . $value->client_prenom;?></span>
                                    <small class="fw-light fs--1">
                                        <?= $value->client_email;?> 
                                        <?php if(!empty(trim($value->client_email1))): ?>
                                            <br><?=$value->client_email1;?>
                                        <?php endif;?>
                                    </small>
                                </span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <span><?= (!empty($value->client_pays)) ? $value->client_pays : 'Non renseigné';?></span>
                    </td>
                    <td>
                        <span><?= $value->progm_nom ?></span>
                    </td>
                    <td class="pe-1">
                        <span style="white-space: nowrap;"><?= $value->etat_mandat_libelle ?></span>
                    </td>
                    <td class="pe-1 text-center">
                        <span style="white-space: nowrap;">
                             <?= (!empty($value->util_prenom)) ? $value->util_prenom : "---" ?>
                        </span>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>


<script type="text/javascript">
    $(document).ready(function () {
        filtrerTableau();
    });
</script>