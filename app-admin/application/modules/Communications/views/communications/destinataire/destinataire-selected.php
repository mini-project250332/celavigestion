<div class="d-flex flex-column pb-1">
    <div class="w-100">
        <div class="row m-0">

            <div class="col-auto ps-0 pe-2">
                <div class="form-group p-0 my-2">
                    <div class="ps-2">
                        <label class="form-label" style="height:21px"></label>
                        <div class="d-flex align-items-center fw-normal " style="height:32pxfont-size: 0.875rem;font-weight: 500 !important;">
                            <span>Propriétaires séléctionnés pour la communication : </span> 
                            <span class="px-1" id="nombre-resultats-selectionner"><?= (!empty($list_destinataire)) ? count($list_destinataire) : "Aucun";?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-auto ps-0 pe-2">
                <div class="form-group p-0 my-2">
                    <div>
                        <label class="form-label" style="height:21px"></label>
                        <div style="height:32px">
                            <button type="button" id="retirer-selection-destinataire-mail" class="btn btn-warning btn-sm mx-1" <?= (!empty($list_destinataire)) ? "" : "disabled";?> >
                                <i class="fas fa-minus-square"></i>
                                Retirer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 px-1 pe-2">
        <div class="row m-0 border">
            <div class="col px-0">
                <table class="table table-hover table-sm fs--1 mb-0" id="proprietaire-selection-destinataire">
                    <thead class="text-900">
                        <tr>
                            <th class="p-2" scope="col">
                                <div class="form-check mb-0 d-flex align-items-center">
                                    <input class="form-check-input me-2" type="checkbox" id="check-selection-resultat">
                                    <label class="form-check-label mb-0">
                                        Propriétaires
                                    </label>
                                </div>
                            </th>
                            <th class="p-2" scope="col">Pays</th>
                            <th class="p-2" scope="col">Nom programme</th>
                            <th class="p-2" scope="col">
                                <span style="white-space: nowrap;">Etat mandat</span>
                            </th>
                            <th class="p-2 text-center" scope="col" style="display:table-cell;">
                                <span style="white-space: nowrap;">Suivi par</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if(!empty($list_destinataire)) : ?>
                            <?php foreach ($list_destinataire as $key => $value) : ?>
                                <tr data-etatMandat="<?=$value->etat_mandat_id;?>" data-clientEtat="<?=$value->client_etat;?>" data-program="<?=$value->progm_id;?>" data-suiviClient="<?= $value->util_id;?>" class="row-proprietaire-selectionne" style="cursor:pointer;" >
                                    <td class="ps-2">
                                        <div class="form-check mb-0 d-flex align-items-center">
                                            <input class="form-check-input client_selectionne_row" type="checkbox" name="client_row" id="checkbox_<?= $value->client_id ?>" value="<?= $value->client_id ?>">
                                            
                                            <label for="checkbox_<?= $value->client_id ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" class="form-check-label mb-0 ms-2 ">
                                                <span class="d-flex flex-column lh-sm">
                                                    <span><?= $value->client_nom . ' ' . $value->client_prenom;?></span>
                                                    <small class="fw-light fs--1">
                                                        <?= $value->client_email;?>
                                                         <?php if(!empty(trim($value->client_email1))): ?>
                                                            <br><?=$value->client_email1;?>
                                                        <?php endif;?>   
                                                    </small>
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <span><?= (!empty($value->client_pays)) ? $value->client_pays : 'Non renseigné';?></span>
                                    </td>
                                    <td>
                                        <span><?= $value->progm_nom ?></span>
                                    </td>
                                    <td class="pe-1 ">
                                        <span style="white-space: nowrap;"><?= $value->etat_mandat_libelle ?></span>
                                    </td>
                                    <td class="pe-1 text-center">
                                        <span style="white-space: nowrap;">
                                            <?= (!empty($value->util_prenom)) ? $value->util_prenom : "---" ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr class=""> 
                                <td colspan="4">
                                    <span class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                                        Aucun destinataire n'a été ajouté. Vous pouvez sélectionner dans la liste à côté, puis cliquer sur le bouton " Ajouter la sélection " pour les y ajouter.
                                    </span>
                                </td>
                            </tr>
                        <?php endif;?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>