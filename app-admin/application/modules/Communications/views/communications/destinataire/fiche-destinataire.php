<div class="contenair-title">
    <div class="col-2"></div>
    <div class="col">
        <h5 class="mb-1 fs-0">Destinataires du mail 
            <?php if(!empty($list_destinataire)):?>
                <span class="px-2">:</span> <span class="badge badge-soft-primary fs-0 px-2 py-1"><?=count($list_destinataire);?></span>
            <?php endif;?>
        </h5>
    </div>
    <div class="col px-2 flex-row-reverse">
        <button type="button" data-id="<?= $coms_id ?>" id="btn-formDestinataire-coms" class="btn btn-sm btn-primary" <?=($communications->coms_status==2) ? "disabled='true'" : "";?>>
            <span class="bi bi-person-plus-fill me-1"></span>
            <span> <?= (!empty($destinataire)) ? 'Mettre à jour' : 'Ajouter destinataire';?> </span>
        </button>
    </div>
    <div class="col-2"></div>
</div>

<div class="row py-2 m-0">
    <div class="col-2"></div>
    <div class="col px-0">

        <div class="pb-4">
            <?php if(!empty($list_destinataire)):?>
                <table class="table table-hover table-sm fs--1 mb-0">
                    <thead class="text-900">
                        <tr>
                            <th class="p-2" scope="col">
                                Propriétaires
                            </th>
                            <th class="p-2" scope="col">Adresse mail</th>
                            <th class="p-2" scope="col">Nom programme</th>
                            <th class="p-2" scope="col">
                                <span style="white-space: nowrap;">N° mandat</span>
                            </th>
                            <th class="p-2" scope="col">
                                <span style="white-space: nowrap;">Etat mandat</span>
                            </th>
                            <th class="p-2" scope="col">
                                <span style="white-space: nowrap;">Suivi par</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($list_destinataire as $key => $value) : ?>
                            <tr style="cursor:pointer;" >
                                <td class="ps-2">
                                    <?= $value->client_nom . ' ' . $value->client_prenom ?>
                                </td>
                                <td>
                                    <span class="<?=(!check_isEmailValid($value->client_email))? 'text-danger' : ''?>">
                                        <?= $value->client_email ?>
                                    </span>
                                    <?php if(!empty(trim($value->client_email1))): ?>
                                        &nbsp;|&nbsp; <span class="<?=(!check_isEmailValid($value->client_email1))? 'text-danger' : ''?>">
                                            <?=$value->client_email1;?>
                                         </span>
                                    <?php endif;?>
                                </td>
                                <td><?= $value->progm_nom ?></td>
                                <td class="text-center"><?= $value->mandat_id ?></td>
                                <td class="pe-1 ">
                                    <span style="white-space: nowrap;"><?= $value->etat_mandat_libelle ?></span>
                                </td>
                                <td class="pe-1 ">
                                    <span style="white-space: nowrap;"><?= $value->util_prenom ?></span>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>

                <div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                    <div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                     <p class="mb-0 flex-1">
                        Actuellement, aucun destinataire n'a été ajouté. Vous pouvez ajouter des destinataires en cliquant sur le bouton ci-dessus.
                    </p>
                </div>
                
            <?php endif;?>
        </div>
    </div>
    <div class="col-2"></div>
</div>