<div class="contenair-title pt-1 pb-1">
    <div class="px-2">
        <h5 class="fs-0">Formulaire pour les destinataires du mail</h5>
    </div>
    <div class="px-2">
        <div class="d-flex flex-row-reverse">
            <button type="button" id="fermer-form-destinataire" class="btn btn-primary btn-sm mx-1">Terminer</button>
        </div>
    </div>
</div>

<div class="d-flex m-0">
    
    <div class="col-7 px-1">
        
        <div class="d-flex flex-column pb-1">
            <div class="w-100">
                <div class="row m-0">

                    <div class="col-auto ps-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label pe-2">Résultats : </label>
                                <div class="d-flex align-items-center fw-normal ps-2" style="height:32px">
                                    <span id="nombre-resultats">0</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                   
                    <div class="col px-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Propriétaires :</label>
                                <select class="form-select form-select-sm" id="filtre-propretiaire-des">
                                    <option value="0">Tous les propriétaires</option>
                                    <option value="1">Propriétaires actifs</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col px-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Pays :</label>

                                <div id="div-dropdown-pays" class="dropdown">
                                    <button class="btn fw-normal border rounded-0 text-black d-flex justify-content-center dropdown-toggle p-0 px-2 w-100" type="button" id="dropdown-pays" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div id="dropdown-checkbox-pays" class="dropdown-menu" aria-labelledby="dropdown-pays" style="max-height:230px;overflow-y: auto;">

                                        <?php 
                                            array_splice($list_pays, 2, 0, "Non renseigné");
                                            function supprimerAccents($valeur) {
                                                $accents = array("à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "ÿ", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "Ÿ", "ş", "Ş", "ı", "İ", "ğ", "Ğ");
                                                $sansAccents = array("a", "a", "a", "a", "a", "a", "ae", "c", "e", "e", "e", "e", "i", "i", "i", "i", "d", "n", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "y", "y", "A", "A", "A", "A", "A", "A", "AE", "C", "E", "E", "E", "E", "I", "I", "I", "I", "D", "N", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "Y", "Y", "s", "S", "i", "I", "g", "G");
                                                return strtolower(str_replace($accents, $sansAccents, $valeur));
                                            }
                                        ?>
                                        <?php foreach ($list_pays as $key => $value) : ?>
                                            <div class="form-check px-1 d-flex align-items-center">
                                                <input style="margin-left:0em!important;" id="checkbox-pays-<?=$key;?>" class="form-check-input m-0 mx-1" type="checkbox" value="<?= preg_replace('/\s+/', '-',supprimerAccents($value)); ?>" id="checkbox-pays-<?=$key;?>">
                                                <label style="font-size: .8rem;font-weight: 300 !important;" class="form-check-label m-0 text-black" for="checkbox-pays-<?=$key;?>"><?= ucwords($value); ?></label>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col px-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Programmes :</label>
                                <select class="form-select form-select-sm" id="filtre-programme-des">
                                    <option value="0">Tous</option>
                                    <?php foreach ($program as $key => $value) : ?>
                                        <option value="<?= $value->progm_id ?>">
                                            <?= $value->progm_nom ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col px-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Chargé de clientèle :</label>
                                <select class="form-select form-select-sm" id="filtre-utilisateur-des">
                                    <option value="0">Tous</option>
                                    <?php foreach ($emetteur as $key => $value) : ?>
                                        <option value="<?= $value->id ?>">
                                            <?= $value->prenom ?>
                                        </option>
                                    <?php endforeach; ?>
                                    <option value="-1">Sans chargé de clientèle</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col px-0 pe-1">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Gestionnaire :</label>
                                <select class="form-select form-select-sm" id="filtre-gestionnaire">
                                    <option value="0">Tous</option>
                                    <?php foreach ($gestionnaires as $key => $value) : ?>
                                        <option value="<?= $value->gestionnaire_id ?>">
                                            <?= $value->gestionnaire_nom ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col px-0">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label">Etat mandat :</label>
                                <select class="form-select form-select-sm" id="filtre-mandat-des">
                                    <option value="0">Tous</option>
                                    <?php foreach ($etat_mandat as $key => $value) : ?>
                                        <option value="<?= $value->etat_mandat_id ?>">
                                            <?= $value->etat_mandat_libelle ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-auto ps-0 pe-2">
                        <div class="form-group p-0 my-2">
                            <div>
                                <label class="form-label" style="height:21px"></label>
                                <div style="height:32px">
                                    <button type="button" id="add-destinataire-mail" class="btn btn-primary btn-sm mx-1">
                                        <i class="fas fa-plus-square"></i>
                                        Ajouter
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-100">
                <div class="row m-0 border">
                    <div id="list-destinataire" class="col px-0"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col" id="list-destinataire-selectionne">
        
        
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        getlistDestNSelected();
        getlistDestSelected();
    });
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });
    

    // Fonction pour mettre à jour le texte du bouton en fonction des checkboxes sélectionnées
    function updateButtonText() {
        const checkboxes = document.querySelectorAll('#div-dropdown-pays .dropdown-menu input[type="checkbox"]');
        const selectedCheckboxes = Array.from(checkboxes).filter(checkbox => checkbox.checked);
        const button = document.querySelector('#div-dropdown-pays button');
        if (selectedCheckboxes.length === 0) {
            button.textContent = 'Tous';
        } else if (selectedCheckboxes.length === 1) {
            const selectedLabel = selectedCheckboxes[0].nextElementSibling.textContent;
            button.textContent = selectedLabel.trim();
        } else {
            button.textContent = `${selectedCheckboxes.length} Pays`;
        }
    }
    // Écouter les événements de changement des checkboxes
    const checkboxes = document.querySelectorAll('#div-dropdown-pays .dropdown-menu input[type="checkbox"]');
    checkboxes.forEach(checkbox => {
        checkbox.addEventListener('change', () => {
            updateButtonText();
        });
    });
    // Appel initial pour mettre à jour le texte du bouton
    updateButtonText();


    $(document).on('change', '#dropdown-checkbox-pays input[type="checkbox"]', function () {
        filtrerTableau();
    });
</script>