<?php
defined('BASEPATH') or exit('No direct script access allowed');

error_reporting(E_ALL);
ini_set('display_errors', '1');

use Carbon\Carbon;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Communications extends ADM_Controller
{

    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Communications";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/communications/communications.js",
            "js/saisietemps/saisietemps.js"
        );

        $this->_css_personnaliser = array(
            "css/clients/clients.css",
            "css/select2.min.css",
        );
      
    }

    public function index()
    {
        $this->_data['menu_principale'] = "communication";
        $this->_data['sous_module'] = "communications/contenaire-communications";
        $this->render('contenaire');
    }

    public function getHeaderList(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Communications/communications/header-liste");
            }
        }
    }

    public function listCommunications(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $clause = array('coms_etat' => 1);

                if($data_post['statut_com'] != "")
                    $clause['coms_status'] = intval($data_post['statut_com']);

                $params =  array(
                    'clause' => $clause,
                    'columns' => array('*'),
                    'join' => array(
                        'c_utilisateur' => 'c_utilisateur.util_id = c_communications.util_id'
                    ),
                    'order_by_columns' => 'coms_date_creation DESC',
                    'method' => 'result',
                );

                $this->load->model('Communications_m');
                $data['communications'] = $communications = $this->Communications_m->get($params); 

                $this->load->model('Communication_destinataire_m');
                foreach ($communications as $key => $value) {
                    $request = $this->Communication_destinataire_m->get(
                        array('clause' => array('coms_id' => $value->coms_id),'method' => 'row'));
                    $communications[$key]->destinataire = $request;
                }

                $this->load->model('Communication_contenu_m');
                $contenu_coms = $this->Communication_contenu_m->get(array('method' => 'result')); 

                $data['utilisateur'] = $this->getListUser();
                $this->renderComponant("Communications/communications/communications/liste-communications", $data);
            }
        }
    }

    public function getFormNewCommunications(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->renderComponant("Communications/communications/communications/form-communications");
            }
        }
    }

    public function cruCommunications(){
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Communications_m');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $action = !isset($data['coms_id']) ? 'add' : 'edit';
                    $data_retour = array("coms_id" => null, "action" => $action);
                    $clause = null;

                    if ($action == "edit") {
                        $clause = array("coms_id" => $data['coms_id']);
                        $data_retour['coms_id'] = $data['coms_id'];
                        unset($data['coms_id']);
                    } else {
                        $data['coms_date_creation'] = Carbon::now()->toDateTimeString();
                        $data['util_id'] = $_SESSION['session_utilisateur']['util_id'];
                        $data['coms_etat'] = 1;
                        $data['coms_status'] = 0;
                    }

                    $request = $this->Communications_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $data_retour['coms_id'] = ($action === "add") ? $request : $clause['coms_id'];
                        $retour = retour(true, "success", $data_retour, array("message" => ($action == "add") ? "Communication enregistrée avec succès" : "Modification réussie"));
                    }

                }
                echo json_encode($retour);
            }
        }
    }

    function deleteCommunications(){
        if (is_ajax()) {
            if ($_POST) {
                $data_post = $_POST["data"];
                $data_retour = array("coms_id" => $data_post['coms_id'], "action" => $data_post['action']);

                if ($data_post['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data_post['action'],
                        'data' => array(
                            'id' => $data_post['coms_id'],
                            'title' => "Confirmation la suppression du mail",
                            'text' => "Voulez-vous vraiment supprimer cette communication ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data_post['action'] == "confirm") {
                    $retour = retour(false, "success",$data_retour, array("message" => "Prolème de l'action supprimer"));
                    $this->load->model('Communications_m');
                    $clause = array('coms_id' => $data_post['coms_id']);
                    $retour = retour(500, "error", $clause, array("message" =>  "Erreur sur l'action suppr"));
                    $data['coms_etat'] = 0;
                    $request = $this->Communications_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(200, "success", $data_retour, array("message" =>  "Communication a été supprimer avec succès"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }


    /****** *****/ 

    function getFicheCommunications(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Communications_m');
                $data_post = $_POST["data"];
                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );
                $data['communications'] = $this->Communications_m->get($params);
                $data['coms_id'] = $data_post['coms_id'];
                $this->renderComponant("Communications/communications/communications/fiche-communications", $data);
            }
        }
    }

    /***** *****/

    function getInformationComs(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Communications_m');
                $data_post = $_POST["data"];
                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );
                $data['communications'] = $this->Communications_m->get($params);
                $data['coms_id'] = $data_post['coms_id'];

                if($data_post['page'] === 'form' )
                    $this->renderComponant("Communications/communications/communications/form-update-communications", $data);
                else
                    $this->renderComponant("Communications/communications/communications/information-general", $data);
            }
        }
    }

    /**** ****/

    function getContenuComs(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Communications_m');
                $this->load->model('Communication_contenu_m');
                $this->load->model('Communication_pjoin_m');
                
                $data_post = $_POST["data"];
                $clause = array('coms_id' => $data_post['coms_id']);
                $params =  array(
                    'clause' => $clause,
                    'method' => 'row',
                );

                $data['coms_id'] = $data_post['coms_id'];
                $data['emetteur'] = $this->getListUser();
                $data['communications'] = $this->Communications_m->get($params);

                /**** modele mail ***/
                $this->load->model('Modelmail_m');
                $param = array(
                    'columns' => array('pmail_id', 'pmail_libelle', 'pmail_description', 'pmail_objet', 'pmail_contenu'),
                );
                $data['modelMail'] = $this->Modelmail_m->get($param);

                /**** get ****/
                $data['coms_con'] = $this->Communication_contenu_m->get($params);
                $data['coms_pjoin'] = $this->Communication_pjoin_m->get(array('clause' => $clause));

                if($data_post['page'] === 'form')
                    $this->renderComponant("Communications/communications/contenu/form-contenu", $data);
                else
                    $this->renderComponant("Communications/communications/contenu/fiche-contenu", $data);

            }
        }
    }

    function getModelMail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = format_data($_POST['data']);
                $this->load->model('Modelmail_m');
                $params =  array(
                    'clause' => array('pmail_id' => $data_post['modelContenuMail']),
                    'method' => 'row',
                );
                $request = $this->Modelmail_m->get($params);
                echo json_encode($request);
            }
        }
    }

    function cruContenuComs(){
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'data_retour' => null,
                'status' => false,
                'type' => 'error',
                'form_validation' => array()
            );

            $coms_objet = $_POST['coms_objet'] ?? '';
            $coms_contenu = $_POST['coms_contenu'] ?? '';

            if (empty($coms_objet) || empty($coms_contenu)) {
                if (empty($coms_objet)) {
                    $retour['form_validation']['coms_objet'] = array('required' => 'Ce champ ne doit pas être vide.');
                }
                if (empty($coms_contenu)) {
                    $retour['form_validation']['coms_contenu'] = array('required' => 'Ce champ ne doit pas être vide.');
                }
            } else {

                $this->load->model('Communication_contenu_m');
                $this->load->model('Communication_pjoin_m');

                $data_coms_contenu = array(
                    'coms_con_objet' => $coms_objet,
                    'coms_con_contenu' => $coms_contenu,
                    'coms_con_type_contenu' => 2,
                    'coms_util_emetteur' => $_POST['coms_util_emetteur'],
                    'coms_con_signature' => ($_POST['coms_signature'] == 0) ? null : $_POST['coms_signature'],
                    'util_id' => $_SESSION['session_utilisateur']['util_id'],
                    'coms_id' => $_POST['coms_id'],
                );

                $clause = (isset($_POST['coms_con_id'])) ? array('coms_con_id' => $_POST['coms_con_id']) : null;
                $request_contenu = $this->Communication_contenu_m->save_data($data_coms_contenu, $clause);

                if (!empty($request_contenu)) {
                    $clause_pjoin = $clause ?? ['coms_con_id' => $request_contenu];
                    $get_pjoin = $this->Communication_pjoin_m->get(array('clause' => $clause_pjoin));
                    $retour['form_validation']['message'] = is_null($clause) ? "Contenu enregistré avec succès " : "Le contenu a été mis à jour avec succès";

                    if (isset($_FILES['coms_fichier']) && is_array($_FILES['coms_fichier']['name']) && $_FILES['coms_fichier']['size'][0] != 0) {
                        try {
                            $fileData = $this->processUploadedFiles($_FILES['coms_fichier'], $get_pjoin, $clause_pjoin['coms_con_id'], $_POST['coms_id']);
                            $retour['form_validation']['message'] .= $this->getUploadMessage($fileData);
                        } catch (Exception $e) {
                            $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                        }
                    }

                    $retour['data_retour'] = $_POST['coms_id'];
                    $retour['status'] = true;
                    $retour['type'] = "succes";
                } else {
                    $retour['form_validation']['message'] = "Erreur enregistrement contenu.";
                }
            }
            echo json_encode($retour);
        }
    }

    function processUploadedFiles($files, $get_pjoin, $coms_con_id, $coms_id) {
        $fileData = array();
        $uploadDir = "uploads/piece-jointe/$coms_id/";

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        $fichier_security = "uploads/piece-jointe/index.html";
        if(!file_exists($uploadDir.'index.html')){
            copy($fichier_security, $uploadDir.'index.html');
        }

        foreach ($files['tmp_name'] as $key => $tempFile) {
            $originalFilename = $_FILES['coms_fichier']['name'][$key];
            $filename = $this->sanitizeFileName($originalFilename);
            // $targetFile = $uploadDir . uniqid() . '_' . basename($filename); // Renommer le fichier de manière aléatoire
            $targetFile = $uploadDir . basename($filename);
            $fileSize = $_FILES['coms_fichier']['size'][$key];
            $contentType = $_FILES['coms_fichier']['type'][$key];

            $verify_pjoin = array_filter($get_pjoin, function ($pjoin) use ($originalFilename, $filename, $targetFile, $contentType, $fileSize) {
                return $pjoin->coms_pjoin_libelle === $originalFilename && $pjoin->coms_pjoin_paths === $targetFile && $pjoin->coms_pjoin_type === $contentType && $pjoin->coms_pjoin_size === $fileSize;
            });

            if (empty($verify_pjoin)) {
                if (move_uploaded_file($tempFile, $targetFile)) {
                    $fichier = array(
                        'coms_pjoin_type' => $contentType,
                        'coms_pjoin_libelle' => $filename,
                        'coms_pjoin_paths' => $targetFile,
                        'coms_pjoin_size' => $fileSize,
                        'coms_con_id' => $coms_con_id,
                        'coms_id' => $coms_id
                    );
                    $fileData[] = $fichier;
                } else {
                    throw new Exception("Erreur upload fichier $originalFilename");
                }
            }
        }
        return $fileData;
    }


    function sanitizeFileName($filename) {
        // Convertir les caractères accentués en caractères non accentués
        $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
        // Supprimer les caractères spéciaux et remplacer les espaces par des tirets
        $filename = str_replace(" ", "-", $filename);
        $filename = preg_replace("/[^a-zA-Z0-9\-_\.]/", "", $filename);
        
        return $filename;
    }

    // Reste du code inchangé...


    function getUploadMessage($fileData) {
        if (!empty($fileData)) {
            $this->load->model('Communication_pjoin_m');
            $request_pjoin = $this->Communication_pjoin_m->multi_insert_data($fileData);
            if($request_pjoin)
                return " <br> Fichiers téléchargés avec succès ";
            else
                return " <br> Erreur enregistrement fichier dans base. ";
        } else {
            return " <br> Aucun fichier téléchargé, ils ont tous déjà été téléchargés ou utilisés dans un autre contenu de communication auparavant. Veuillez les renommer";
        }
    }

    function deletePjoin(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $clause = array("coms_pjoin_id" => $data_post['id']);

                $retour = retour(false, "success",$clause, array("message" => "Prolème de l'action supprimer"));
                $this->load->model('Communication_pjoin_m');
                $request = $this->Communication_pjoin_m->delete_data($clause);
                if (!empty($request))
                    $retour = retour(true, "success", $clause, array("message" => "Suppression avec succès"));
                
                echo json_encode($retour);
            }
        }
    }


    /***** ******/

    function getDestinataireComs(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['coms_id'] = $data_post['coms_id'];

                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $this->load->model('Communication_contenu_m');
                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $this->load->model('Communications_m');
                $data['communications'] = $this->Communications_m->get($params);

                $data['coms_con'] = $this->Communication_contenu_m->get($params);
                $this->load->model('Communication_destinataire_m');
                $data['destinataire'] = $destinataire = $this->Communication_destinataire_m->get(
                    array( 
                        'clause' => array('c_communications_destinataire.coms_id' => $data_post['coms_id']),                     
                        'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'
                    ),
                        'join' => array(
                            'c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id',
                        ),
                        'method' => 'row'
                    )
                );

                $this->load->model('Client_m');
                $clause = array(
                    'c_client.client_etat' => 1,
                    'c_dossier.dossier_etat' => 1
                );
                
                $clients = $this->Client_m->get(
                    array(
                        'columns' => array(
                            'c_dossier.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom',
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        ),
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );

                $pays = array_map(function($client_pays){
                    return strtolower(trim($client_pays->client_pays));
                },$clients);

                $data['list_pays'] = array_filter(array_unique(array_values($pays)));
                $data['list_pays']  = $this->customSortData($data['list_pays']);

                $destinataire_selectionner = [];
                if(isset($destinataire) && !empty($destinataire))
                    $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

                $data['list_destinataire'] = array_filter($clients,function($client) use ($destinataire_selectionner){
                    return in_array($client->client_id, $destinataire_selectionner);
                });

                if($data_post['page'] == "fiche"){
                    $this->renderComponant("Communications/communications/destinataire/fiche-destinataire", $data);
                }else{

                    $this->load->model('EtatMandat_m');
                    $data['etat_mandat'] = $this->EtatMandat_m->get(array(
                        'columns' => array('etat_mandat_id', 'etat_mandat_libelle')
                    ));

                    $data['emetteur'] = $this->getListUser();

                    $this->load->model('Program_m');
                    $data['program'] = $this->Program_m->get(
                        array(
                            'clause' => array('progm_etat' => 1),
                            'columns' => array('progm_id', 'progm_nom'),
                            'order_by_columns' => 'progm_nom ASC'
                        )
                    );

                     /*** gestionnaire ***/
                    $this->load->model('Gestionnaire_m');
                    $clause_gest = array(
                        'gestionnaire_etat' => 1
                    );

                    $data['gestionnaires'] = $gestionnaires = $this->Gestionnaire_m->get(
                        array(
                            'clause' => $clause_gest,
                            'columns' => array('gestionnaire_id','gestionnaire_nom'),
                            //'order_by_columns' => 'gestionnaire_nom ASC'
                        )
                    );

                    $this->renderComponant("Communications/communications/destinataire/form-destinataire", $data);
                }
            }
        }
    }

    function customSortData($data) {
        usort($data, function ($a, $b) {
            if ($a === "france" || $a === "royaume uni") {
                return -1;
            } elseif ($b === "france" || $b === "royaume uni") {
                return 1;
            }
            return strcmp($a, $b);
        });
        return $data;
    }

    function get_listDestNSelected(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['coms_id'] = $data_post['coms_id'];

                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $this->load->model('Communication_contenu_m');
                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $data['coms_con'] = $this->Communication_contenu_m->get($params);
                $this->load->model('Communication_destinataire_m');
                $destinataire = $this->Communication_destinataire_m->get(
                    array( 
                        'clause' => array('c_communications_destinataire.coms_id' => $data_post['coms_id']),                     
                        'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'
                    ),
                        'join' => array(
                            'c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id',
                        ),
                        'method' => 'row'
                    )
                );

                /*** gestionnaire ***/
                $this->load->model('Gestionnaire_m');
                $clause_gest = array(
                    'gestionnaire_etat' => 1
                );
                $data['gestionnaires'] = $gestionnaires = $this->Gestionnaire_m->get(
                    array(
                        'clause' => $clause_gest,
                        'columns' => array('gestionnaire_id','gestionnaire_nom'),
                        'order_by_columns' => 'gestionnaire_nom ASC'
                    )
                );

                $this->load->model('Client_m');
                $clause = array(
                    'c_client.client_etat' => 1,
                    'c_dossier.dossier_etat' => 1
                );
                
                $clients = $this->Client_m->get(
                    array(
                        'columns' => array(
                            'c_dossier.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom',
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        ),
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );

                $destinataire_selectionner = [];
                if(isset($destinataire) && !empty($destinataire))
                    $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

                $this->load->model('ClientLot_m');
                $clause_cliLot = array('cliLot_etat' => 1);
                $lots_clients = $this->ClientLot_m->get(
                    array(
                        'columns' => array('dossier_id','gestionnaire_id'),
                        'clause' => $clause_cliLot
                    )
                );

                $data['clients'] =  array_map(function ($client) use ($lots_clients) {
                    $dossier_id = $client->dossier_id;
                    $gestionnaire_filtre = array_map(function($gestionnaire){
                        return intval($gestionnaire->gestionnaire_id);
                    },
                    array_filter(
                        $lots_clients,
                        function ($lots) use ($dossier_id) {
                            return $dossier_id == $lots->dossier_id;
                        }
                    ));
                    $gest_lot = array_values(array_unique($gestionnaire_filtre));
                    $client->gestionnaires = !empty($gest_lot) ? $gest_lot : null;
                    return $client;
                },array_filter($clients,function($client) use ($destinataire_selectionner){
                    return !in_array($client->client_id, $destinataire_selectionner);
                }));

                // $data['clients'] = array_filter($clients,function($client) use ($destinataire_selectionner){
                //     return !in_array($client->client_id, $destinataire_selectionner);
                // });

                $this->renderComponant("Communications/communications/destinataire/destinataire-nselected", $data);
            }
        }
    }

    function get_listDestSelected(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['coms_id'] = $data_post['coms_id'];

                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $this->load->model('Communication_contenu_m');
                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );

                $data['coms_con'] = $this->Communication_contenu_m->get($params);
                $this->load->model('Communication_destinataire_m');
                $destinataire = $this->Communication_destinataire_m->get(
                    array( 
                        'clause' => array('c_communications_destinataire.coms_id' => $data_post['coms_id']),                     
                        'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'
                    ),
                        'join' => array(
                            'c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id',
                        ),
                        'method' => 'row'
                    )
                );

                $this->load->model('Client_m');
                $clause = array(
                    'c_client.client_etat' => 1,
                    'c_dossier.dossier_etat' => 1
                );
                $clients = $this->Client_m->get(
                    array(
                        'columns' => array(
                            'c_dossier.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom',
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        ),
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );

                $destinataire_selectionner = [];
                if(isset($destinataire) && !empty($destinataire))
                    $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

                $data['list_destinataire'] = array_filter($clients,function($client) use ($destinataire_selectionner){
                    return in_array($client->client_id, $destinataire_selectionner);
                });

                $this->renderComponant("Communications/communications/destinataire/destinataire-selected", $data);
            }
        }
    }

    function getFormDestinataireComs(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['coms_id'] = $data_post['coms_id'];

                $this->load->model('Communication_destinataire_m');
                $data['destinataire'] = $this->Communication_destinataire_m->get(
                    array('clause' => array('coms_id' => $data_post['coms_id']),'method'=>'row'));

                $this->load->model('Client_m');
                $clause = array('c_client.client_etat' => 1);
                $data['client'] = $client = $this->Client_m->get(
                    array(
                        'columns' => array(
                            'c_utilisateur.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email', 'c_client.client_email1','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom'
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_utilisateur' => 'c_utilisateur.util_id = c_client.util_id',
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        ),
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );

                $this->load->model('EtatMandat_m');
                $data['etat_mandat'] = $this->EtatMandat_m->get(array(
                    'columns' => array('etat_mandat_id', 'etat_mandat_libelle')
                ));

                $data['emetteur'] = $this->getListUser();

                $this->load->model('Program_m');
                $data['program'] = $this->Program_m->get(
                    array(
                        'clause' => array('progm_etat' => 1),
                        'columns' => array('progm_id', 'progm_nom'),
                        'order_by_columns' => 'progm_nom ASC'
                    )
                );
                $this->renderComponant("Communications/communications/destinataire/form-destinataire", $data);
            }
        }
    }

    function loadDestinataire(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $retour = retour(false, "error", $data_post['coms_id'], array("message" => "Erreur"));
                if(empty(json_decode($data_post['destinataire'], true))){
                    $retour = retour(false, "warning", $data_post['coms_id'], array("message" => "Aucun destinataire sélectionner"));
                    echo json_encode($retour);
                    return;
                }

                $clause = array('coms_id' => $data_post['coms_id']);
                $this->load->model('Communication_destinataire_m');
                $verify_destComs = $this->Communication_destinataire_m->get(array('clause' => $clause,"method"=>"row"));
                
                if(empty($verify_destComs)){
                    $clause = null;
                    $data['coms_id'] = $data_post['coms_id'];
                    $data['client_destinataire_id'] = serialize(json_decode($data_post['destinataire'], true));
                }else{
                    $dest_existe = unserialize($verify_destComs->client_destinataire_id);
                    $data_post['destinataire'] = json_decode($data_post['destinataire'], true);

                    if($data_post['action'] == "adding")
                        $data['client_destinataire_id'] = serialize(array_merge($dest_existe, $data_post['destinataire']));
                    else
                        $data['client_destinataire_id'] = serialize(array_diff($dest_existe, $data_post['destinataire']));
                }

                $request = $this->Communication_destinataire_m->save_data($data, $clause);
                if(!empty($request))
                    $retour = retour(true, "success", $data_post['coms_id'], array("message" => ($clause === null) ? "Destinataire mail enregistrée avec succès" : "Destinataire a été mis à jour avec succès"));

                echo json_encode($retour);
            }
        }
    }

    /****** *****/

    function getApercuComs(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['coms_id'] = $data_post['coms_id'];

                $params =  array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                );
                $this->load->model('Communications_m');

                /**** get communication information ****/
                $data['communications'] = $this->Communications_m->get($params);
                $data['emetteur'] = $this->getListUser();

                $this->load->model('Communication_destinataire_m');
                $data['destinataire'] = $destinataire = $this->Communication_destinataire_m->get(
                    array( 
                        'clause' => array('c_communications_destinataire.coms_id' => $data_post['coms_id']),                     
                        'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'
                    ),
                        'join' => array(
                            'c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id',
                        ),
                        'method' => 'row'
                    )
                );

                /**** get contenu ****/
                $this->load->model('Communication_contenu_m');
                $data['coms_con'] = $this->Communication_contenu_m->get(array(
                    'clause' => array('coms_id' => $data_post['coms_id']),
                    'method' => 'row',
                ));

                $data['signature'] = null;
                if(!empty($data['coms_con'])){
                    if($data['coms_con']->coms_con_signature == 2){
                        $this->load->model('ParamMailUsers_m');
                        $params =  array(
                            'clause' => array('util_id' => $data['coms_con']->coms_util_emetteur),
                            'columns' => array('*'),                   
                            'method' => 'row'
                        );
                        $request_signature = $this->ParamMailUsers_m->get($params);
                        $data['signature'] = (!empty($request_signature)) ? $request_signature->mess_signature : null;
                    }
                }

                /**** get piece joint ****/
                $this->load->model('Communication_pjoin_m'); 
                $data['coms_pjoin'] = $this->Communication_pjoin_m->get(array('clause' => array('coms_id' => $data_post['coms_id'])));

                /**** get destinataire ****/
                $this->load->model('Client_m');
                $clause = array(
                    'c_client.client_etat' => 1,
                    'c_dossier.dossier_etat' => 1
                );
                
                $clients = $this->Client_m->get(
                    array(
                        'columns' => array(
                            'c_dossier.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom',
                        ),
                        'clause' => $clause,
                        'join' => array(
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        ),
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );

                $destinataire_selectionner = [];
                if(isset($destinataire) && !empty($destinataire))
                    $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

                $data['list_destinataire'] = array_filter($clients,function($client) use ($destinataire_selectionner){
                    return in_array($client->client_id, $destinataire_selectionner);
                });

                $this->renderComponant("Communications/communications/apercu/apercu-coms", $data);
            }
        }
    }

    function demande_validation(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $retour = retour(false, "error", $data_post['coms_id'], array("message" => "Erreur de demande"));
            
                $clause = array('coms_id' => $data_post['coms_id']);
                $data['coms_date_demande'] = date('Y-m-d H:i:s');
                $data['coms_status'] = 3;
                $data['util_demande'] = $_SESSION['session_utilisateur']['util_id'];

                $this->load->model('Communications_m');
                $request = $this->Communications_m->save_data($data, $clause);

                if(!empty($request)){
                    $notification = $this->mail_demandeValidation($data_post['coms_id'],$_SESSION['session_utilisateur']['util_id']);
                    $retour = retour(false, "success", $data_post['coms_id'], array("message" => $notification['message']));
                    if($notification['status'] == 200)
                        $retour = retour(true, "success", $data_post['coms_id'], array("message" => $notification['message']));
                }
                echo json_encode($retour);
            }
        }
    }


    function mail_demandeValidation($coms_id,$util_id){
        $mail = new PHPMailer();
        $retour = array();
        try {
            $params = array(
                'clause' => array(
                    'c_utilisateur.util_id' => $util_id
                ),
                'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                'method' => 'row'
            );
            $this->load->model('ParamMailUsers_m', 'smpt_user');
            $emetteur = $this->smpt_user->get($params);
            
            if (!empty($emetteur->mess_signature)) {
                $signature = explode("/", $emetteur->mess_signature);
                $string = trim($signature[3]);
                $string = str_replace(' ', '', $string);
                $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $data['signature'] = $emetteur->mess_signature;
                $data['sign'] = 'cid:' . $sign;
                $image = '<img alt="Signature" style="max-width: 450px; display: block;" src="cid:' . $data['sign'] . '"/>';
                $data_contenu['signature'] =  $image;
            }

            $data['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
            $data['user'] = $emetteur->mess_email;
            $data['pwd'] = $emetteur->mess_motdepasse;
            $data['port'] = $emetteur->mess_port;
            $data['auth'] = true;
            $data['host'] = $emetteur->mess_adressemailserveur;

            $mail->isSMTP();
            $mail->Host = $data['host'];
            $mail->SMTPAuth = $data['auth'];
            $mail->Username = $data['user'];
            $mail->Password = $data['pwd'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = $data['port'];
            $mail->CharSet = 'UTF-8';
            $mail->setFrom($data['user'], $data['user_label']);
            
            $data_contenu['utilisateurs'] = $this->getListUser();
            $administrateurs = array_filter($data_contenu['utilisateurs'],function($util){return $util->rol_util_id == 1;});
            
            $premierDestinataire = reset($administrateurs);
            $mail->addAddress($premierDestinataire->email, $premierDestinataire->nom . ' ' . $premierDestinataire->prenom);
            foreach ($administrateurs as $destinataire) {
                if ($destinataire !== $premierDestinataire) {
                    $mail->addCC($destinataire->email, $destinataire->nom. ' ' .$destinataire->prenom);
                }
            }

            $this->load->model('Communications_m');
            $data_contenu['communications'] = $communications = $this->Communications_m->get(array('coms_id' => $coms_id,'method'=>'row'));
            $view_mail = $this->load->view("Communications/communications/demande-validation", $data_contenu,TRUE);

            if (isset($data['signature'])) {
                $mail->AddEmbeddedImage($data['signature'], $data['sign']);
            }

            $mail->isHTML(true);
            $mail->Subject = mb_encode_mimeheader('Demande de validation de la communication " '.$communications->coms_libelle.' "', $mail->CharSet);
            $mail->Body = $view_mail;

            $retour = array('status' => 500, 'message' => " Mailer Error sur l'envoie de demande pour les administrateurs");
            if ($mail->send())
                $retour = array('status' => 200, 'message' => " Demande de validation a été bien envoyer à tous les administrateurs");

        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }   
        return $retour;
    }


    function valided_operationMail(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $retour = retour(false, "error", $data_post['coms_id'], array("message" => "Erreur de validation"));
            
                $clause = array('coms_id' => $data_post['coms_id']);
                $data['coms_date_validation'] = date('Y-m-d H:i:s');
                $data['coms_status'] = 1;
                $data['util_validate'] = $_SESSION['session_utilisateur']['util_id'];

                $this->load->model('Communications_m');
                $request = $this->Communications_m->save_data($data, $clause);
                
                if(!empty($request)){
                    $retour = retour(true, "success", $data_post['coms_id'], array("message" => "La validation a été effectuée avec succès."));
                }
                echo json_encode($retour);
            }
        }
    }

    /******* *******/

    function fetchRecipients($comsId) {
        $this->load->model('Communication_destinataire_m');
        $destinataire = $this->Communication_destinataire_m->get(
            array( 
                'clause' => array('c_communications_destinataire.coms_id' => $comsId),                     
                'columns' => array('coms_des_id', 'c_communications_destinataire.coms_id', 'client_destinataire_id'),
                'join' => array('c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id'),
                'method' => 'row'
            )
        );

        if (!empty($destinataire)) {
            $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

            $list_destinataire = array();
            $this->load->model('Client_m');
            foreach ($destinataire_selectionner as $key => $value) {
                $list_destinataire[] = $this->Client_m->get(
                    array(
                        'clause' => array(
                            'c_client.client_etat' => 1,
                            'c_client.client_id' => $value,
                        ),
                        'join' => array(
                            'c_dossier' => 'c_client.client_id  = c_dossier.client_id',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                            'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',

                            'c_access_proprietaire_client' => 'c_access_proprietaire_client.client_id = c_client.client_id',
                            'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire_client.mtpfr_id',
                            //'c_access_proprietaire' => 'c_access_proprietaire.client_id = c_client.client_id',
                        ),
                        'method' => 'row',
                        'join_orientation' => 'left',
                        'group_by_columns' => "c_client.client_id",
                    )
                );
            }
           
            // $clients = $this->Client_m->get(
            //     array(
            //         'clause' => array('c_client.client_etat' => 1),
            //         'join' => array(
            //             'c_dossier' => 'c_client.client_id  = c_dossier.client_id',
            //             'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
            //             'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
            //             'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
            //             'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
            //             'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',

            //             'c_access_proprietaire_client' => 'c_access_proprietaire_client.client_id = c_client.client_id',
            //             'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire_client.mtpfr_id',
            //             //'c_access_proprietaire' => 'c_access_proprietaire.client_id = c_client.client_id',
            //         ),
            //         'join_orientation' => 'left',
            //         'group_by_columns' => "c_client.client_id",
            //     )
            // );

            // $list_destinataire = array_filter($clients, function ($client) use ($destinataire_selectionner) {
            //     return in_array($client->client_id, array_values($destinataire_selectionner));
            // });
            return $list_destinataire;
        }

        return array();
    }

    function fetchCommunicationContent($comsId) {
        $this->load->model('Communication_contenu_m');
        $contenu = $this->Communication_contenu_m->get(
            array(
                'clause' => array('coms_id' => $comsId),
                'columns' => array('*'),
                'method' => 'row'
            )
        );
        return $contenu;
    }

    function fetchSenderEmail($content) {
        $emetteur = $this->getListUser();
        $emetteur_id = $content->coms_util_emetteur;
        $filtre_emetteur = array_filter($emetteur,function($util) use ($emetteur_id){
            return $util->id == $emetteur_id;
        });
        $filtre_emetteur = array_values($filtre_emetteur);
        return $filtre_emetteur[0];
    }

    function fetchAttachments_link($comsId){
        $this->load->model('Communication_pjoin_m');
        return $this->Communication_pjoin_m->get(array('clause' => array('coms_id' => $comsId)));
    }

    function fetchAttachments($comsId) {
        $this->load->model('Communication_pjoin_m');
        $attachments = $this->Communication_pjoin_m->get(array('clause' => array('coms_id' => $comsId)));
        $formattedAttachments = [];
        foreach ($attachments as $attachment) {
            $formattedAttachment = [
                'ContentType' => $attachment->coms_pjoin_type,
                'Filename' => $attachment->coms_pjoin_libelle,
                'Base64Content' => base64_encode(file_get_contents($attachment->coms_pjoin_paths)),
            ];
            $formattedAttachments[] = $formattedAttachment;
        }
        return $formattedAttachments;
    }

    function isEmailValid($email = '') {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /*
        function exportToJSON($data,$fichier) {
            // Convertir le tableau en format JSON
            $jsonData = json_encode($data, JSON_PRETTY_PRINT);
            // Spécifier le chemin du répertoire
            $directoryPath = APPPATH . '../documents/communications/historique/';
            // Créer le répertoire s'il n'existe pas
            if (!is_dir($directoryPath)) {
                mkdir($directoryPath, 0777, true);
            }
            // Spécifier le chemin du fichier JSON
            $jsonFilePath = $directoryPath ."$fichier.json";

            // Écrire le contenu JSON dans le fichier
            if (write_file($jsonFilePath, $jsonData)) {
                //echo 'Les données ont été exportées avec succès vers ' . $jsonFilePath;
            } else {
                //echo 'Une erreur s\'est produite lors de l\'exportation des données vers ' . $jsonFilePath;
            }
        }
    */

    function exportToJSON($data, $fichier) {
        $directoryPath = APPPATH . '../documents/communications/historique/';
        // chmod -R 777 /var/www/cestlavie/cestlavie/app-admin/documents/communications/historique/
        if (!is_dir($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }
        $jsonFilePath = $directoryPath . "$fichier.json";

        // Utiliser un flux pour l'écriture
        $fileHandle = fopen($jsonFilePath, 'w');
        if ($fileHandle !== false) {
            foreach ($data as $item) {
                // Convertir chaque élément en format JSON
                $jsonChunk = json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                // Écrire le contenu JSON dans le fichier
                fwrite($fileHandle, $jsonChunk . PHP_EOL);
            }
            fclose($fileHandle);
            //echo 'Les données ont été exportées avec succès vers ' . $jsonFilePath;
        } else {
            //echo 'Une erreur s\'est produite lors de l\'exportation des données vers ' . $jsonFilePath;
        }
    }


    function prepareMailjetParameters($comsId,$senderEmail, $recipients, $content, $attachments) {
        $mailjetParams = [];
        $recipients_error_mail = [];

        foreach ($recipients as $recipient) {
            $array_adresseMaildestinataire = [];
            $recipients_error = array(
                'client_id' => null,
                'mail_error' => []
            );
            
            if($this->isEmailValid($recipient->client_email)){
                $adresse_mail = [
                    'Email' => $recipient->client_email,
                    'Name' => $recipient->util_prenom . ' ' . $recipient->client_nom,
                ];
                $array_adresseMaildestinataire[] = $adresse_mail;
            }else{
                $recipients_error['mail_error'][] = $recipient->client_email;
            }

            if($this->isEmailValid($recipient->client_email1)){
                if (!empty($recipient->client_email1)) {
                    $adresse_mail1 = [
                        'Email' => $recipient->client_email1,
                        'Name' => $recipient->util_prenom . ' ' . $recipient->client_nom,
                    ];
                    $array_adresseMaildestinataire[] = $adresse_mail1;
                }
            }else{
                $recipients_error['mail_error'][] = $recipient->client_email1;
            }

            $param = [
                'From' => [
                    'Email' => $senderEmail->email,
                    'Name' => $senderEmail->prenom . ' ' . $senderEmail->nom,
                ],
                'To' => $array_adresseMaildestinataire,
                'Subject' => mb_encode_mimeheader($this->objet_mailReplace($content->coms_con_objet,$recipient), 'UTF-8', 'B')
            ];

            $content_mail = $this->contenu_mail($content,$recipient, $attachments);

            $param['HTMLPart'] = $content_mail['HTMLPart'];
            $param['TextPart'] = $content_mail['TextPart'];

            /*
                if (!empty($attachments))
                    $param['Attachments'] = $attachments;
            */

            if(!empty($array_adresseMaildestinataire)){
                $mailjetParams[] = $param;
            }

            if(!empty($recipients_error['mail_error'])){
                $recipients_error['client_id'] = $recipient->client_id;
                $recipients_error_mail[] = $recipients_error;
            }
        }

        if(!empty($recipients_error_mail)){
            $data['client_destinataire_error'] = serialize($recipients_error_mail);
            $clause = array('coms_id' => $comsId);
            $this->load->model('Communication_destinataire_m');
            $this->Communication_destinataire_m->save_data($data, $clause);
        }

        $this->exportToJSON($mailjetParams,'communication-'.date('Y-m-d'));
        return $mailjetParams;
    }

    function objet_mailReplace($objet,$dest){
        $objet = str_replace('@nom_client' , $dest->client_nom, $objet); 
        $objet = str_replace('@prenom_client', $dest->client_prenom, $objet); 
        $objet = str_replace('@adresse_client', $dest->client_adresse1, $objet); 
        
        $objet = str_replace('@nom_utilisateur' , ((!empty($dest->util_nom)) ? $dest->util_nom : ''), $objet);
        $objet = str_replace('@prenom_utilisateur' , ((!empty($dest->util_prenom)) ? $dest->util_prenom : ''), $objet); 
        $objet = str_replace('@adresse_utilisateur' , ((!empty($dest->util_adresse)) ? $dest->util_adresse : ''), $objet); 
        $objet = str_replace('@email_utilisateur' , ((!empty($dest->util_mail)) ? $dest->util_mail : ''), $objet); 
        $objet = str_replace('@telephone_utilisateur', ((!empty($dest->util_phonefixe)) ? $dest->util_phonefixe : ''), $objet); 

        $objet = str_replace('@nom_programme', $dest->progm_nom, $objet);
        return $objet; 
    }

    function contenu_mail($content,$recipient, $attachments = null){
        $data = array();
        $mailMessage = $content->coms_con_contenu;
        $mailMessage = str_replace('@nom_client' , $recipient->client_nom, $mailMessage); 
        $mailMessage = str_replace('@prenom_client', $recipient->client_prenom, $mailMessage); 
        $mailMessage = str_replace('@adresse_client', $recipient->client_adresse1, $mailMessage); 
        $mailMessage = str_replace('@nom_utilisateur' , $recipient->util_nom, $mailMessage); 
        $mailMessage = str_replace('@prenom_utilisateur' , $recipient->util_prenom, $mailMessage); 
        $mailMessage = str_replace('@adresse_utilisateur' , $recipient->util_adresse, $mailMessage); 
        $mailMessage = str_replace('@email_utilisateur' , $recipient->util_mail, $mailMessage); 
        $mailMessage = str_replace('@telephone_utilisateur', $recipient->util_phonefixe, $mailMessage); 
        $mailMessage = str_replace('@nom_programme', $recipient->progm_nom, $mailMessage);

        $mailMessage = str_replace('@identifiant_intranet' , $recipient->accp_login, $mailMessage); 
        $mailMessage = str_replace('@mot_passe_intranet' , $recipient->mtpfr_nom, $mailMessage); 
        
        // $mailMessage = str_replace('@identifiant_location_meublee', $dest->util_phonefixe, $mailMessage); 
        // $mailMessage = str_replace('@mot_passe_location_meublee', $dest->progm_nom, $mailMessage);

        $data['signature'] = null;
        if($content->coms_con_signature == 2){
            $this->load->model('ParamMailUsers_m');
            $params =  array(
                'clause' => array('util_id' => $content->coms_util_emetteur),
                'columns' => array('*'),                   
                'method' => 'row'
            );
            $request_signature = $this->ParamMailUsers_m->get($params);
            $data['signature'] = (!empty($request_signature)) ? $request_signature->mess_signature : null;
        }
        $data['contenu'] = $mailMessage;
        $data['attachments'] = $attachments;

        $mailjet_contenu = array(
            'HTMLPart' => $this->load->view("Communications/communications/template-contenu", $data,TRUE),
            'TextPart' => $mailMessage,
        );
        return $mailjet_contenu;
    }

    function sendEmailViaMailjet($mailjetParams) {
        $this->load->library('MailjetService');
        $mailjet = new MailjetService();
        $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
        $response = $mailjet->sendEmailBody(['Messages'=>$mailjetParams]);
        return $response;
    }

    function prepareRequestData($requestData) {
        return array(
            'status' => 200,
            'action' => $requestData['action'],
            'data' => array(
                'id' => $requestData['coms_id'],
                'title' => "Confirmation d'envoi du mail",
                'text' => "Voulez-vous vraiment envoyer cette communication ?",
                'btnConfirm' => "Confirmer",
                'btnAnnuler' => "Annuler"
            ),
            'message' => "",
        );
    }

    function processCommunicationRequest() {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $postData = $_POST['data'];
            if ($postData['action'] == "demande") {
                $response = $this->prepareRequestData($postData);
            }

            if($postData['action'] == "confirm") {
                $response = $this->sendEmail($postData);
            }
            echo json_encode($response);
        }
    }

    function segmenterDonnees($data) {
        $totalObjets = count($data);
        
        $objetsParSegment = 25;
        $nombreSegments = ceil($totalObjets / $objetsParSegment);

        $dataSegments = [];
        for ($i = 0; $i < $nombreSegments; $i++) {
            $debut = $i * $objetsParSegment;
            $fin = ($i + 1) * $objetsParSegment - 1;
            $segment = array_slice($data, $debut, $objetsParSegment);
            $dataSegments[] = $segment;
        }
        return $dataSegments;
    }

    // processus send mail
    function sendEmail($requestData) {
        $comsId = $requestData['coms_id'];
        $recipients = $this->fetchRecipients($comsId);
        $content = $this->fetchCommunicationContent($comsId);
        $senderEmail = $this->fetchSenderEmail($content);
        //$attachments = $this->fetchAttachments($comsId);
        $attachments = $this->fetchAttachments_link($comsId);

        $mailjetParams = $this->prepareMailjetParameters($comsId,$senderEmail, $recipients, $content, $attachments);
        //$this->exportToJSON($mailjetParams,'retour-communication-'.date('Y-m-d'));

        $segments_data = $this->segmenterDonnees($mailjetParams);

        $retourMailjet = array("Messages" => []);
        foreach ($segments_data as $key => $segment) {
            $sendEmail_segment = $this->sendEmailViaMailjet($segment);
            if (!empty($sendEmail_segment) && isset($sendEmail_segment['success']) && $sendEmail_segment['success'] == true) {
                $retourMailjet["Messages"] = array_merge($retourMailjet['Messages'],$sendEmail_segment['data']['Messages']);
            }else{
                $this->exportToJSON($segment,"erreur-com-segment_".$key."-".date('Y-m-d'));
            }
        }

        if (!empty($retourMailjet['Messages'])) {

            $data_update = array(
                'coms_date_envoi' => date('Y-m-d H:i:s'),
                'util_envoi' => $_SESSION['session_utilisateur']['util_id'],
                'coms_status' => 2,
                'data_coms' => serialize($retourMailjet),
            );
            
            $clause_update = array('coms_id' => $comsId);
            $this->load->model('Communications_m');
            $resquest_update = $this->Communications_m->save_data($data_update, $clause_update);

            $retour = retour(true, "success", count($segments_data), array("message" => "Mail envoyé mais la mise à jour données communication a un erreur"));
            if(!empty($resquest_update)){

                $message = "La communication bien envoyé. ";
                $notification = $this->mail_notificationEnvoie($comsId);
                if(!empty($notification)){
                    $message .= $notification['message'];
                }
                $retour = retour(true, "success", count($segments_data), array("message" => $message));
            }
        }else{
            $retour = retour(false, "success", count($segments_data), array("message" => "Prolème d'envoye en connection avec mailjet"));
        }
        return $retour;
    }

    // 

    function mail_notificationEnvoie($comsId){
        $mail = new PHPMailer();
        $retour = array();
        try {
            $params_coms =  array(
                'clause' => array('coms_id' => $comsId),
                'method' => 'row',
            );
            $this->load->model('Communications_m');
            $data_contenu['communications'] = $communications = $this->Communications_m->get($params_coms);
            if(!empty($communications->util_demande)){

                $params = array(
                    'clause' => array('c_utilisateur.util_id' => $_SESSION['session_utilisateur']['util_id']),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id'),
                    'method' => 'row'
                );

                $this->load->model('ParamMailUsers_m', 'smpt_user');
                $emetteur = $this->smpt_user->get($params);
                
                if (!empty($emetteur->mess_signature)) {
                    $signature = explode("/", $emetteur->mess_signature);
                    $string = trim($signature[3]);
                    $string = str_replace(' ', '', $string);
                    $sign = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                    $data['signature'] = $emetteur->mess_signature;
                    $data['sign'] = 'cid:' . $sign;
                    $image = '<img alt="Signature" style="max-width: 450px; display: block;" src="cid:' . $data['sign'] . '"/>';
                    $data_contenu['signature'] =  $image;
                }

                $data['user_label'] = $emetteur->util_prenom . ' ' . $emetteur->util_nom;
                $data['user'] = $emetteur->mess_email;
                $data['pwd'] = $emetteur->mess_motdepasse;
                $data['port'] = $emetteur->mess_port;
                $data['auth'] = true;
                $data['host'] = $emetteur->mess_adressemailserveur;

                //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();
                $mail->Host = $data['host'];
                $mail->SMTPAuth = $data['auth'];
                $mail->Username = $data['user'];
                $mail->Password = $data['pwd'];
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->Port = $data['port'];
                $mail->CharSet = 'UTF-8';
                $mail->setFrom($data['user'], $data['user_label']);

                $data_contenu['utilisateurs'] = $this->getListUser();
                $demandeur_util = $communications->util_demande;
                $destinataire = array_filter($data_contenu['utilisateurs'],function($util) use ($demandeur_util){return $util->id == $demandeur_util;});

                $destinataire = reset($destinataire);
                $mail->addAddress($destinataire->email, $destinataire->nom . ' ' . $destinataire->prenom);
                
                $view_mail = $this->load->view("Communications/communications/mail-confirmation-envoie", $data_contenu,TRUE);
                if (isset($data['signature'])) {
                    $mail->AddEmbeddedImage($data['signature'], $data['sign']);
                }

                $mail->isHTML(true);
                $mail->Subject = mb_encode_mimeheader(' Envoi de la communication " '.$communications->coms_libelle.' "', $mail->CharSet);
                $mail->Body = $view_mail;

                $retour = array('status' => 500, 'message' => " Mailer Error sur l'envoie des notifications pour les administrateurs");
                if ($mail->send())
                    $retour = array('status' => 200, 'message' => "La notification de validation et d'envoie a été envoyer à utilisateur qui envoie la demande");
            }
        } catch (Exception $e) {
            $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
        }   
        return $retour;
    }

    // suivi et evaluation

    function getSuiviComs(){
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $data_post = $_POST["data"];
            $params =  array(
                'clause' => array('coms_id' => $data_post['coms_id']),
                'method' => 'row',
            );
            $this->load->model('Communications_m');
            $data['communications'] = $communications = $this->Communications_m->get($params);

            /***** ******/
            $this->load->model('Communication_destinataire_m');
            $data['destinataire'] = $destinataire = $this->Communication_destinataire_m->get(
                array( 
                    'clause' => array('c_communications_destinataire.coms_id' => $data_post['coms_id']),                     
                    'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'),
                    'join' => array('c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id'),
                    'method' => 'row'
                )
            );

            $this->load->model('Client_m');
            $clause = array(
                'c_client.client_etat' => 1,
                'c_dossier.dossier_etat' => 1
            );
            
            $clients = $this->Client_m->get(
                array(
                    'columns' => array(
                        'c_dossier.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays','c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom',
                    ),
                    'clause' => $clause,
                    'join' => array(
                        'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                    ),
                    'join_orientation' => 'left',
                    'group_by_columns' => "c_client.client_id",
                )
            );

            $destinataire_selectionner = [];
            if(isset($destinataire) && !empty($destinataire))
                $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

            $data['list_destinataire'] = $list_destinataire = array_filter($clients,function($client) use ($destinataire_selectionner){
                return in_array($client->client_id, $destinataire_selectionner);
            });

            $reformat_listDestinataire = [];
            foreach ($list_destinataire as $item) {
                $reformattedItem = $item;
                if (!empty($item->client_email1)) {
                    $reformattedItem2 = clone $reformattedItem;
                    $reformattedItem2->client_email = $item->client_email1;
                    unset($reformattedItem2->client_email1);
                    $reformat_listDestinataire[] = $reformattedItem2;
                }
                unset($reformattedItem->client_email1);
                $reformat_listDestinataire[] = $reformattedItem;
            }
            $data['list_destinataire'] = $reformat_listDestinataire;

            $retour_dataComs = array();
            if(!empty($communications->data_coms)){
                $data_coms = unserialize($communications->data_coms);
                foreach ($data_coms['Messages'] as $message) {
                    if ($message['Status'] === 'success' && isset($message['To']))
                        $retour_dataComs = array_merge($retour_dataComs, $message['To']);
                }

                foreach ($retour_dataComs as $key => $value) {
                    $retour_dataComs[$key] = array_merge($retour_dataComs[$key], $this->get_messagesInfo($value['MessageID']));
                }
            }

            $indexed_retour = array_column($retour_dataComs, null, 'Email');
            foreach ($reformat_listDestinataire as &$element) {
                $email = $element->client_email;
                $element->mailjetInfo = (!isset($indexed_retour[$email])) ? null : $indexed_retour[$email]; 
            }

            /***** *****/
            $this->load->view("Communications/communications/suivi/suivi-recapitulation", $data);
        }
    }

    function get_messagesInfo($messageID){
        $this->load->library('MailjetService');
        $mailjet = new MailjetService();
        $mailjet->init(API_KEY_MAILJET, API_SECRET_MAILJET);
        $response = $mailjet->getMailResourcesBy_id($messageID);
        return $response;
    }

    function get_detailMail(){
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $data_post = $_POST["data"];
            $data['mail'] = $data_post['mail'];
            $params =  array(
                'clause' => array('coms_id' => $data_post['coms_id']),
                'method' => 'row',
            );
            $this->load->model('Communications_m');
            $data['communications'] = $communications = $this->Communications_m->get($params);

            /**** get communication information ****/
            $data['emetteur'] = $this->getListUser();

            /**** get contenu ****/
            $this->load->model('Communication_contenu_m');
            $data['coms_con'] = $contenu_coms = $this->Communication_contenu_m->get(array(
                'clause' => array('coms_id' => $data_post['coms_id']),
                'method' => 'row',
            ));

            /**** get piece joint ****/
            $this->load->model('Communication_pjoin_m'); 
            $data['coms_pjoin'] = $this->Communication_pjoin_m->get(array('clause' => array('coms_id' => $data_post['coms_id'])));

            /**** ****/

            $this->load->model('Client_m');
            $data['clients_destinataire'] = $clients = $this->Client_m->get(
                array(
                    'columns' => '*',
                    'clause' => array(
                        'c_client.client_etat' => 1,
                        'c_client.client_id' => $data_post['client_id']
                    ),
                    'join' => array(
                        'c_dossier' => 'c_client.client_id  = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        'c_access_proprietaire_client' => 'c_access_proprietaire_client.client_id = c_client.client_id',
                        'c_mot_de_passe_fruit' => 'c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire_client.mtpfr_id',
                        //'c_access_proprietaire' => 'c_access_proprietaire.client_id = c_client.client_id',
                    ),
                    'join_orientation' => 'left',
                    'group_by_columns' => "c_client.client_id",
                    'method' => "row"
                )
            );

            $data['contenu'] = $this->contenu_mail($contenu_coms,$clients);
            $data['objet'] = $this->objet_mailReplace($contenu_coms->coms_con_objet,$clients);

            if(!empty($data_post['messageID'])){
                $data['info_mailjet'] = $this->get_messagesInfo($data_post['messageID']);
            }

            $this->load->view("Communications/communications/suivi/detail-message", $data);
        }
    }


    /**** ****/

    function getSuiviComs_test(){
        $params =  array(
            'clause' => array('coms_id' => 8),
            'method' => 'row',
        );
        $this->load->model('Communications_m');
        $communications = $this->Communications_m->get($params);


        $params__8 =  array(
            'clause' => array('coms_id' => 10),
            'method' => 'row',
        );
        $communications_8 = $this->Communications_m->get($params__8);


        // $toEmails = [];
        // if(!empty($communications->data_coms)){
        //     $data_coms = unserialize($communications->data_coms);
            
        //     foreach ($data_coms['Messages'] as $message) {
        //         if ($message['Status'] === 'success' && isset($message['To'])) {
        //             $toEmails = array_merge($toEmails, $message['To']);
        //         }
        //     }
        // }

        // foreach ($toEmails as $key => $value) {
        //     //$toEmails[$key]['getInfo'] = $this->get_messagesInfo($value['MessageID']);
        //     $toEmails[$key] = array_merge($toEmails[$key], $this->get_messagesInfo($value['MessageID']));
        // }

        $data_coms = unserialize($communications->data_coms);
        $data_coms_8 = unserialize($communications_8->data_coms);
        $data_retour = array("Messages" => []);

        $data_retour["Messages"] = array_merge($data_retour["Messages"], $data_coms["Messages"], $data_coms_8["Messages"]);

        // echo json_encode(
        //     array( 
        //         'all' => count($data_retour),
        //         '1' => count($data_coms["Messages"]),
        //         '2' => count($data_coms_8["Messages"]),
        //     )
        // );

        echo json_encode($data_retour);
    }

    function getDonnees(){

        $params =  array(
            'clause' => array('coms_id' => 3),
            'method' => 'row',
        );
        $this->load->model('Communications_m');
        $data['communications'] = $communications = $this->Communications_m->get($params);
    
        $this->load->model('Communication_destinataire_m');
        $data['destinataire'] = $destinataire = $this->Communication_destinataire_m->get(
            array( 
                'clause' => array('c_communications_destinataire.coms_id' => 3),                     
                'columns' => array('coms_des_id','c_communications_destinataire.coms_id', ' client_destinataire_id'
            ),
                'join' => array(
                    'c_communications' => 'c_communications_destinataire.coms_id = c_communications.coms_id',
                ),
                'method' => 'row'
            )
        );

        $this->load->model('Client_m');
        $clause = array('c_client.client_etat' => 1);
        $data['clients'] = $clients = $this->Client_m->get(
            array(
                'columns' => array(
                    'c_utilisateur.util_id','c_client.client_id', 'c_client.client_nom', 'c_client.client_prenom', 'c_client.client_email','c_client.client_email1','c_client.client_pays', 'c_dossier.dossier_id','progm_nom', 'mandat_id', 'etat_mandat_libelle' , 'c_etat_mandat.etat_mandat_id' , 'c_programme.progm_id', 'c_client.client_etat' , 'c_utilisateur.util_prenom'
                ),
                'clause' => $clause,
                'join' => array(
                    'c_utilisateur' => 'c_utilisateur.util_id = c_client.util_id',
                    'c_dossier' => 'c_client.client_id  = c_dossier.client_id ',
                    'c_lot_client' => 'c_client.client_id = c_lot_client.cli_id',
                    'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    'c_mandat_new' => 'c_mandat_new.client_id = c_client.client_id',
                    'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                ),
                'join_orientation' => 'left',
                'group_by_columns' => "c_client.client_id",
            )
        );

        $destinataire_selectionner = [];
        if(isset($destinataire) && !empty($destinataire))
            $destinataire_selectionner = unserialize($destinataire->client_destinataire_id);

        $data['list_destinataire'] = $list_destinataire = array_filter($clients,function($client) use ($destinataire_selectionner){
            return in_array($client->client_id, $destinataire_selectionner);
        });

        $reformattedData = [];
        foreach ($list_destinataire as $item) {
            $reformattedItem = $item;

            if (!empty($item->client_email1)) {
                $reformattedItem2 = clone $reformattedItem;
                $reformattedItem2->client_email = $item->client_email1;
                unset($reformattedItem2->client_email1);
                $reformattedData[] = $reformattedItem2;
            }
            unset($reformattedItem->client_email1);
            $reformattedData[] = $reformattedItem;
        }

        $retour_dataComs = array();
        if(!empty($communications->data_coms)){
            $data_coms = unserialize($communications->data_coms);
            foreach ($data_coms['Messages'] as $message) {
                if ($message['Status'] === 'success' && isset($message['To']))
                    $retour_dataComs = array_merge($retour_dataComs, $message['To']);
            }

            foreach ($retour_dataComs as $key => $value) {
                $retour_dataComs[$key] = array_merge($retour_dataComs[$key], $this->get_messagesInfo($value['MessageID']));
            }
        }

        $data['list_destinataire'] = $reformattedData;

        $indexed_liste_2 = array_column($retour_dataComs, null, 'Email');
        foreach ($reformattedData as &$element) {
            $email = $element->client_email;
            $element->mailjetInfo = (!isset($indexed_liste_2[$email])) ? null : $indexed_liste_2[$email]; 
        }

        echo json_encode($reformattedData);
    }
}