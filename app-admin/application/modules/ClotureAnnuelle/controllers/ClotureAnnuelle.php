<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class ClotureAnnuelle extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "ClotureAnnuelle";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/cloture_annuelle/cloture_annuelle.js"
        );

        $this->_css_personnaliser = array();
    }

    public function listCloture_Annuelle()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClotureAnnuelle_m', 'cloture_annuelle');

                $data['etat_array'] = array(
                    0 => 'non clôturé',
                    1 => 'validation en cours',
                    2 => 'clôturé'
                );

                $data_save = array(
                    'dossier_id' => $_POST['data']['dossier_id'],
                    'annee' => date('Y')
                );

                $data['liste_cloture_annuelle'] = $this->cloture_annuelle->getListclotureAnnuelle($_POST['data']['dossier_id']);

                //Create new row if last data is less than current year or if list data is empty
                if (!empty($data['liste_cloture_annuelle'])) {
                    if ($data['liste_cloture_annuelle'][0]->annee < intval(date('Y'))) {
                        $request = $this->cloture_annuelle->save_data($data_save);
                        $data['liste_cloture_annuelle'] = $this->cloture_annuelle->getListclotureAnnuelle($_POST['data']['dossier_id']);
                    }
                } else {
                    $request = $this->cloture_annuelle->save_data($data_save);
                    $data['liste_cloture_annuelle'] = $this->cloture_annuelle->getListclotureAnnuelle($_POST['data']['dossier_id']);
                }

                $data['last_cloture'] = $this->cloture_annuelle->get(
                    array(
                        'columns' => array('annee'),
                        'clause' => array('dossier_id' => $_POST['data']['dossier_id'], 'etat' => 2),
                        'method' => 'row',
                        'order_by_columns' => "annee DESC",
                    )
                );

                if (empty($data['last_cloture'])) {
                    $data['last_annee'] = $this->cloture_annuelle->get(
                        array(
                            'columns' => array('annee'),
                            'clause' => array('dossier_id' => $_POST['data']['dossier_id']),
                            'method' => 'row',
                            'order_by_columns' => "annee ASC",
                        )
                    );
                }

                $this->renderComponant("Clients/clients/dossier/cloture_annuelle/list_cloture_annuelle", $data);
            }
        }
    }

    public function valider_cloture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la validation",
                );
                $this->load->model('ClotureAnnuelle_m', 'cloture_annuelle');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cloture_id' => $data['cloture_id']),
                    'method' => "row",
                );
                $request_get = $this->cloture_annuelle->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cloture_id'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous vraiment valider la clôture ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cloture_id' => $data['cloture_id']);
                    $data_update = array(
                        'etat' => 2,
                        'util_validation' => $_SESSION['session_utilisateur']['util_id'],
                        'date_validation' => date('Y-m-d')
                    );

                    $request = $this->cloture_annuelle->save_data($data_update, $clause);
                    $cloture = $this->cloture_annuelle->get(
                        array(
                            'clause' => array(
                                'cloture_id' => $data['cloture_id']
                            ),
                            'method' => 'row'
                        )
                    );
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['cloture_id'],
                                'cloture_id' => $data['cloture_id'],
                                'dossier_id' => $cloture->dossier_id
                            ),
                            'message' =>  "Validation effectuée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function annuler_cloture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'annulation",
                );
                $this->load->model('ClotureAnnuelle_m', 'cloture_annuelle');
                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('cloture_id' => $data['cloture_id']),
                    'method' => "row",
                );
                $request_get = $this->cloture_annuelle->get($params);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['cloture_id'],
                            'title' => "Confirmation de la validation",
                            'text' => "Voulez-vous annuler la clôture ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('cloture_id' => $data['cloture_id']);
                    $data_update = array(
                        'etat' => 0,
                        'date_demande_validation' => NULL,
                        'util_demande_validation' => NULL,
                        'date_validation' => NULL,
                        'util_validation' => NULL,
                        'util_expert_comptable' => NULL
                    );

                    $request = $this->cloture_annuelle->save_data($data_update, $clause);

                    $cloture = $this->cloture_annuelle->get(
                        array(
                            'clause' => array(
                                'cloture_id' => $data['cloture_id']
                            ),
                            'method' => 'row'
                        )
                    );
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array(
                                'id' => $data['cloture_id'],
                                'cloture_id' => $data['cloture_id'],
                                'dossier_id' => $cloture->dossier_id
                            ),
                            'message' =>  "Annulation effectuée",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    function demander_cloture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m', 'users');

                $data['util_expert'] = $this->users->get(
                    array(
                        'clause' => array('rol_util_id' => 6, 'util_etat' => 1)
                    )
                );
                $data['cloture_id'] = $_POST['data']['cloture_id'];

                $this->renderComponant("Clients/clients/dossier/cloture_annuelle/form_validation_cloture", $data);
            }
        }
    }

    function confirmDemande()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClotureAnnuelle_m', 'cloture_annuelle');

                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'annulation",
                );

                $clause = array('cloture_id' => $_POST['data']['cloture_id']);
                $data_update = array(
                    'etat' => 1,
                    'util_demande_validation' => $_SESSION['session_utilisateur']['util_id'],
                    'date_demande_validation' => date('Y-m-d'),
                    'util_expert_comptable' => $_POST['data']['util_expert_comptable'],
                );
                $request = $this->cloture_annuelle->save_data($data_update, $clause);

                $cloture = $this->cloture_annuelle->get(
                    array(
                        'clause' => array(
                            'cloture_id' => $_POST['data']['cloture_id']
                        ),
                        'method' => 'row'
                    )
                );

                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'data' => array(
                            'cloture_id' => $cloture->cloture_id,
                            'dossier_id' => $cloture->dossier_id
                        ),
                        'message' =>  "Demande effectuée",
                    );
                }
                echo json_encode($retour);
            }
        }
    }
}
