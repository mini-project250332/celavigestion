<?php
defined('BASEPATH') or exit('No direct script access allowed');


use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;

class ExpertsComptables extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "ExpertsComptables";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/experts_comptable/experts_comptables.js",
        );
        $this->_css_personnaliser = array();
    }

    public function index()
    {
       
    }

    public function data_affectationExpertComptable(){

        $data_post = $_POST['data'];
        $data['dossier_id'] = $dossier_id = $data_post['dossier_id'];

        //Expertcomptable_mission_dossier_m
        //Expertcomptable_assiger_dossier_m

        $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
        $params = array(
            'clause' => array('dossier_id' => $dossier_id),
            'join' => array(
                'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
            ),
            'columns' => array(
                'expert_MD_id',
                'c_expertcomptable_assiger_dossier.assiger_ECD_id',
                'date_assigner',
                'expert_id',
                'util_nom as expert_nom',
                'util_prenom as expert_prenom',
                'date_creation as date_creation_mission',
                'date_debut_mission',
                'date_resiliation_mission',
                'fichier_mission',
                'fichier_resiliation',
                'dossier_id',
            ),
            'order_by_columns' => "date_debut_mission ASC",
            //'group_by_columns' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id',
        );

        $data['data_missionsEC'] = $this->mission->get($params);
        $this->renderComponant("ExpertsComptables/assigner_expertC/tableau_affectation_expertC", $data);
    }

    public function affectationExpertComptable(){
        $data_post = $_POST['data'];
        $data['dossier_id'] = $dossier_id = $data_post['dossier_id'];
        $data['action'] = $action = $data_post['dossier_id'];

        $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
        $params = array(
            'clause' => array('dossier_id' => $dossier_id),
            'join' => array(
                'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
            ),
            'columns' => array(
                'expert_MD_id',
                'c_expertcomptable_assiger_dossier.assiger_ECD_id',
                'date_assigner',
                'expert_id',
                'util_nom as expert_nom',
                'util_prenom as expert_prenom',
                'date_creation as date_creation_mission',
                'date_debut_mission',
                'date_resiliation_mission',
                'fichier_mission',
                'fichier_resiliation',
                'dossier_id',
            ),
            'order_by_columns' => "date_debut_mission ASC",
        );
        $data['data_missionsEC'] = $this->mission->get($params);

        $this->load->model('Utilisateur_m', 'users');
        $data['util_expert'] = $this->users->get(
            array('clause' => array('rol_util_id' => 6, 'util_etat' => 1))
        );

        $this->renderComponant("ExpertsComptables/assigner_expertC/form_affectation_expertC", $data);
    }


    public function assigner_expertC_mission(){
        $retour = array(
            'data_retour' => null,
            'status' => false,
            'type' => 'error',
            'form_validation' => array()
        );

        $date_debut_mission = $_POST['date_debut_mission'] ?? '';
        $expert_id = $_POST['expert_id'] ?? '';
        $dossier_id = $_POST['dossier_id'] ?? '';

        if (empty($date_debut_mission)) {
            $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ ne doit pas être vide.');
        }

        if (empty($expert_id)) {
            $retour['form_validation']['expert_id'] = array('required' => 'Ce champ ne doit pas être vide.');
        }

        if(isset($_POST['preview_date_resiliation']) && !empty($date_debut_mission)){
            $date_debut_mission_ = new DateTime($date_debut_mission);
            $preview_date_resiliation = new DateTime($_POST['preview_date_resiliation']);
        
            if($preview_date_resiliation > $date_debut_mission_){
                $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ doit être postérieur à la date de resiliation de la mission précédente.');
            }
        }

        $clause = null;
        if(empty($retour['form_validation'])){
            $data_mission = array(
                'date_debut_mission' => date('Y-m-d',strtotime($_POST['date_debut_mission'])),
                'util_id' => $_SESSION['session_utilisateur']['util_id'],
                'date_creation' => Carbon::now()->toDateTimeString(),
            );

            $prosses_insere_mission = false;
            if(isset($_POST['preview_expert_id']) && $_POST['preview_expert_id'] == $_POST['expert_id']){
                $data_mission['assiger_ECD_id'] = $_POST['preview_assiger_ECD_id'];
                $prosses_insere_mission = true;
            }else{
                // assigner expert C avec le dossier
                $data_assigner_ED = array( 
                    'date_assigner' => Carbon::now()->toDateTimeString(),
                    'expert_id' =>  $_POST['expert_id'],
                    'dossier_id' => $dossier_id,
                    'util_id' => $_SESSION['session_utilisateur']['util_id']
                );
            
                $this->load->model('Expertcomptable_assiger_dossier_m', 'assigner');
                $request_assignerEC = $this->assigner->save_data($data_assigner_ED, $clause);

                if($request_assignerEC){
                    $data_mission['assiger_ECD_id'] = $request_assignerEC;
                    $prosses_insere_mission = true;
                }else{
                    $retour['form_validation']['message'] = "Erreur lors du processus d'affectation de l'expert-comptable à ce dossier.";
                }
            }

            if($prosses_insere_mission){
                $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
                $request = $this->mission->save_data($data_mission, $clause);
                if ($request) {
                    $retour['form_validation']['message'] = "L'affectation et la création de la mission ont été réalisées avec succès.";
                    $retour['data_retour'] = $dossier_id;
                    $retour['status'] = true;
                    $retour['type'] = "succes";

                    if (isset($_FILES['fichier_mission']) && $_FILES['fichier_mission']['size'] != 0) {
                        try {
                           $uploadFile = $this->processUploadedFile($_FILES['fichier_mission'],$request,$expert_id,'Accord');
                           $data_url['fichier_mission'] = $uploadFile['url'];
                           $this->mission->save_data($data_url, array('expert_MD_id'=>$request));
                        } catch (Exception $e) {
                            $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                        }
                    }
                }
            }
        }
        echo json_encode($retour);
    }

    /*-*** *****/

    function form_modif_affectationMission(){
        $data_post = $_POST['data'];
        $data['dossier_id'] = $dossier_id = $data_post['dossier_id'];
        $data['mission_id'] = $mission_id = $data_post['mission_id'];
        $data['action'] = $action = $data_post['action'];

        $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
        $params = array(
            'clause' => array(
                'dossier_id' => $dossier_id,
                'c_expertcomptable_mission_dossier.expert_MD_id' => $mission_id,
            ),
            'join' => array(
                'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
            ),
            'columns' => array(
                'expert_MD_id',
                'c_expertcomptable_assiger_dossier.assiger_ECD_id',
                'date_assigner',
                'expert_id',
                'util_nom as expert_nom',
                'util_prenom as expert_prenom',
                'date_creation as date_creation_mission',
                'date_debut_mission',
                'date_resiliation_mission',
                'fichier_mission',
                'fichier_resiliation',
                'dossier_id',
            ),
            'method' => "row",
        );
        $data['data_missionsEC'] = $this->mission->get($params);

        $this->load->model('Utilisateur_m', 'users');
        $data['util_expert'] = $this->users->get(
            array('clause' => array('rol_util_id' => 6, 'util_etat' => 1))
        );

        // data mission 
        $params_allMission = array(
            'clause' => array('dossier_id' => $dossier_id),
            'join' => array(
                'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
            ),
            'columns' => array(
                'expert_MD_id',
                'expert_id',
                'date_debut_mission',
                'date_resiliation_mission'
            ),
            'order_by_columns' => "expert_MD_id ASC",
        );

        $data_missionsEC = $this->mission->get($params_allMission);
        // Trouver l'index de l'objet "S1" avec l'expert_MD_id spécifié
        $index_currentMission = array_search($mission_id, array_column($data_missionsEC, 'expert_MD_id'));
        
        // Trouver l'index next mission
        $index_nextMission = $index_currentMission + 1;
        $nextMission = ($index_nextMission < count($data_missionsEC)) ? $data_missionsEC[$index_nextMission] : null;
        $data['nextMission'] = $nextMission;

        $this->renderComponant("ExpertsComptables/assigner_expertC/form_editMission", $data);
    }


    function saveModify_missionExpertC(){
        $retour = array(
            'data_retour' => null,
            'status' => false,
            'type' => 'error',
            'form_validation' => array()
        );

        $expert_MD_id = $_POST['expert_MD_id'];
        $assiger_ECD_id = $_POST['assiger_ECD_id'];
        $dossier_id = $_POST['dossier_id'];

        if (empty($_POST['date_debut_mission'])) {
            $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ ne doit pas être vide.');
        }

        if (empty($_POST['expert_id'])) {
            $retour['form_validation']['expert_id'] = array('required' => 'Ce champ ne doit pas être vide.');
        }

        if(isset($_POST['date_resiliation_mission']) && !empty($_POST['date_resiliation_mission'])){
            $date_debut_mission = new DateTime($_POST['date_debut_mission']);
            $date_resiliation_mission = new DateTime($_POST['date_resiliation_mission']);
        
            if($date_resiliation_mission < $date_debut_mission){
                $retour['form_validation']['date_resiliation_mission'] = array('required' => 'Ce champ doit être postérieur à la date de debut de la mission ('.date('d/m/Y',strtotime($_POST['date_debut_mission'])).').');

                $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ doit être anterieur à la date de fin de la mission ('.date('d/m/Y',strtotime($_POST['date_resiliation_mission'])).').');
            }
        }

        if(empty($retour['form_validation'])){

            $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
            $params = array(
                'clause' => array('dossier_id' => $dossier_id),
                'join' => array(
                    'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                    'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
                ),
                'columns' => array(
                    'expert_MD_id',
                    'expert_id',
                    'date_debut_mission',
                    'date_resiliation_mission'
                ),
                'order_by_columns' => "expert_MD_id ASC",
            );

            $data_missionsEC = $this->mission->get($params);
            // Trouver l'index de l'objet "S1" avec l'expert_MD_id spécifié
            $index_currentMission = array_search($expert_MD_id, array_column($data_missionsEC, 'expert_MD_id'));
            // Trouver l'index preview mission
            $index_previewMission = $index_currentMission - 1;
            $previewMission = ($index_previewMission >= 0) ? $data_missionsEC[$index_previewMission] : null;
            
            

            $date_debut_mission = new DateTime($_POST['date_debut_mission']);
            if(!empty($previewMission)){

                // Date de resiliation de precedent mission
                $date_resiliation_mission_preview = new DateTime($previewMission->date_resiliation_mission);

                if($date_resiliation_mission_preview > $date_debut_mission){
                    $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ doit être postérieur à la date de résiliation de la mission précédente ('.date('d/m/Y',strtotime($previewMission->date_resiliation_mission)).').');
                }
            }

            // Trouver l'index next mission
            $index_nextMission = $index_currentMission + 1;
            $nextMission = ($index_nextMission < count($data_missionsEC)) ? $data_missionsEC[$index_nextMission] : null;

            if(!empty($nextMission)){

                // Date debut prochaine mission
                $date_debut_mission_next = new DateTime($nextMission->date_debut_mission);

                if( ($date_debut_mission > $date_debut_mission_next)) {
                    $retour['form_validation']['date_debut_mission'] = array('required' => 'Ce champ doit être anterieur à la date de debut de la prochaine mission ('.date('d/m/Y',strtotime($nextMission->date_debut_mission)).').');
                }  
            }

            if(empty($retour['form_validation'])){

                $data_mission = array(
                    'date_debut_mission' => date('Y-m-d',strtotime($_POST['date_debut_mission'])),
                );

                // traiter d'abord  information assiger expert comptable s'il modifier l'expert comptable
                
                $this->load->model('Expertcomptable_assiger_dossier_m', 'assigner');
                if($_POST['expert_id'] != $_POST['current_expert_id']) {
                    //$clause_assigner_ED = array('assiger_ECD_id' => $assiger_ECD_id);
                    $clause_assigner_ED = null;
                    $data_assigner_ED = array( 
                        'expert_id' =>  $_POST['expert_id'],
                        'date_assigner' => Carbon::now()->toDateTimeString(),
                        'dossier_id' => $_POST['dossier_id'],
                        'util_id' => $_SESSION['session_utilisateur']['util_id']
                    );
                    $request_assignerEC = $this->assigner->save_data($data_assigner_ED, $clause_assigner_ED);

                    if(empty($request_assignerEC)){
                        $retour['form_validation']['message'] = "Erreur système, la modification de l'identifiant d'expert comptable assigner à ce dossier n'a pas fonctionné.";
                    }else{
                        $data_mission['assiger_ECD_id'] = $request_assignerEC;
                    }
                }

                $clause_mission_EC = array('expert_MD_id' => $expert_MD_id);
                if(isset($_POST['date_resiliation_mission']) && !empty($_POST['date_resiliation_mission'])){
                    $data_mission['date_resiliation_mission'] = date('Y-m-d',strtotime($_POST['date_resiliation_mission']));
                }

                if (isset($_FILES['fichier_mission']) && $_FILES['fichier_mission']['size'] != 0) {
                    try {
                       $uploadFile_a = $this->processUploadedFile($_FILES['fichier_mission'],$expert_MD_id,$_POST['expert_id'],'Accord');
                       $data_mission['fichier_mission'] = $uploadFile_a['url'];
                    } catch (Exception $e) {
                        $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                    }
                }

                if (isset($_FILES['fichier_resiliation']) && $_FILES['fichier_resiliation']['size'] != 0) {
                    try {
                       $uploadFile_b = $this->processUploadedFile($_FILES['fichier_resiliation'],$expert_MD_id,$_POST['expert_id'],'resiliation');
                       $data_mission['fichier_resiliation'] = $uploadFile_b['url'];
                    } catch (Exception $e) {
                        $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                    }
                } 

                $request_mission = $this->mission->save_data($data_mission, $clause_mission_EC);
                if ($request_mission) {
                    $retour['form_validation']['message'] = "Modifier les détails relatifs à cette mission a été effectuée avec succès.";
                    $retour['data_retour'] = $dossier_id;
                    $retour['status'] = true;
                    $retour['type'] = "succes";
                }
            }

        }
        echo json_encode($retour);
    }

    /***** *****/

    function delete_fileMission(){
        $retour = array(
            'data_retour' => null,
            'status' => false,
            'type' => 'error',
            'form_validation' => array()
        );
        $retour['form_validation']['message'] = "Erreur de suppression : La tentative de suppression du fichier a échoué.";

        $data_post = $_POST['data'];
        $data['fichier'] = $fichier = $data_post['fichier'];
        $data['mission_id'] = $mission_id = $data_post['mission_id'];

        $clause = array(
            "expert_MD_id" => $mission_id
        );

        $data_update = ($fichier == "mission") ? array('fichier_mission' => null) :  array('fichier_resiliation' => null);

        $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
        $request_mission = $this->mission->save_data($data_update, $clause);
        if ($request_mission) {
            $retour['form_validation']['message'] = "Suppression fichier a été effectuée avec succès.";
            $retour['data_retour'] = ($fichier == "mission") ? 1 :  2;
            $retour['status'] = true;
            $retour['type'] = "succes";
        }
        echo json_encode($retour);
    }


    /***** resiler mission expert comptable ****/


    function form_resilier_missionExpertC(){
        $data_post = $_POST['data'];
        $data['dossier_id'] = $dossier_id = $data_post['dossier_id'];
        $data['mission_id'] = $mission_id = $data_post['mission_id'];

        $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
        $params = array(
            'clause' => array(
                'dossier_id' => $dossier_id,
                'c_expertcomptable_mission_dossier.expert_MD_id' => $mission_id,
            ),
            'join' => array(
                'c_expertcomptable_assiger_dossier' => 'c_expertcomptable_assiger_dossier.assiger_ECD_id = c_expertcomptable_mission_dossier.assiger_ECD_id',
                'c_utilisateur' => 'c_expertcomptable_assiger_dossier.expert_id = c_utilisateur.util_id'
            ),
            'columns' => array(
                'expert_MD_id',
                'c_expertcomptable_assiger_dossier.assiger_ECD_id',
                'date_assigner',
                'expert_id',
                'util_nom as expert_nom',
                'util_prenom as expert_prenom',
                'date_creation as date_creation_mission',
                'date_debut_mission',
                'date_resiliation_mission',
                'fichier_mission',
                'fichier_resiliation',
                'dossier_id',
            ),
            'method' => "row",
        );
        $data['data_missionsEC'] = $this->mission->get($params);
        $this->renderComponant("ExpertsComptables/assigner_expertC/form_resilierMission", $data);
    }

    function resilier_missionExpertC(){
        $retour = array(
            'data_retour' => null,
            'status' => false,
            'type' => 'error',
            'form_validation' => array()
        );

        $date_resiliation_mission = $_POST['date_resiliation_mission'] ?? '';
        
        $dossier_id = $_POST['dossier_id'] ?? '';
        $mission_id = $_POST['mission_id'] ?? '';
        $expert_id = $_POST['expert_id'] ?? '';

        if (empty($date_resiliation_mission)) {
            $retour['form_validation']['date_resiliation_mission'] = array('required' => 'Ce champ ne doit pas être vide.');
        }

        if (!empty($date_resiliation_mission)) {
            $debut_mission = new DateTime($_POST['debut_mission']);
            $date_resiliation_mission = new DateTime($date_resiliation_mission);
        
            if($date_resiliation_mission > $debut_mission){

                $this->load->model('Expertcomptable_mission_dossier_m', 'mission');
                $clause =  array('expert_MD_id' => $_POST['mission_id']);
                $data = array(
                    'date_resiliation_mission' => date('Y-m-d',strtotime($_POST['date_resiliation_mission']))
                );

                if (isset($_FILES['fichier_resiliation']) && $_FILES['fichier_resiliation']['size'] != 0) {
                    try {
                       $uploadFile = $this->processUploadedFile($_FILES['fichier_resiliation'],$mission_id,$expert_id,'resiliation');
                       $data['fichier_resiliation'] = $uploadFile['url'];
                    } catch (Exception $e) {
                        $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                    }
                }   

                $request = $this->mission->save_data($data, $clause);
                if ($request) {
                    $retour['form_validation']['message'] = "La résiliation a été effectuée avec succès.";
                    $retour['data_retour'] = $dossier_id;
                    $retour['status'] = true;
                    $retour['type'] = "succes";
                }
                
            }else{
                $retour['form_validation']['date_resiliation_mission'] = array('required' => 'Ce champ doit être postérieur à la date de début de la mission ('.date('d/m/Y',strtotime($_POST['debut_mission'])).').');
            }
        }
        echo json_encode($retour);
    }


    function processUploadedFile($file, $mission_id, $expert_id, $type) {
        $fileData = array('status' => false, 'url' => '');
        $uploadDir = "../documents/dossiers/mission_experts/$mission_id/$expert_id/$type/";

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        $fichier_security = "../documents/dossiers/index.html";

        if (!file_exists($uploadDir . 'index.html')) {
            copy($fichier_security, $uploadDir . 'index.html');
        }

        if (!file_exists("../documents/dossiers/mission_experts/$mission_id/$expert_id/"."index.html")) {
            copy($fichier_security,"../documents/dossiers/mission_experts/$mission_id/$expert_id/". 'index.html');
        }
        if (!file_exists("../documents/dossiers/mission_experts/$mission_id/". 'index.html')) {
            copy($fichier_security,"../documents/dossiers/mission_experts/$mission_id/"."index.html");
        }
        if (!file_exists("../documents/dossiers/mission_experts/". 'index.html')) {
            copy($fichier_security,"../documents/dossiers/mission_experts/"."index.html");
        }

        // $originalFilename = $file['name'];
        $originalFilename = $this->sanitizeFileName($file['name']);
        $pathInfo = pathinfo($originalFilename);
        $filenameWithoutExtension = $pathInfo['filename'];

        // Renommer le fichier
        // $filename = $type . "_missionID" . $mission_id . "_expertID" . $expert_id . "." . pathinfo($originalFilename, PATHINFO_EXTENSION);
        $filename = $filenameWithoutExtension.".".pathinfo($originalFilename, PATHINFO_EXTENSION);
    
        $targetFile = $uploadDir . basename($filename);

        // Vérifier si le fichier existe déjà
        $counter = 1;
        while (file_exists($targetFile)) {
            // Ajouter un préfixe de compteur pour éviter les doublons
            //$filename = $type . "_missionID" . $mission_id . "_expertID" . $expert_id . "_" . $counter . "." . pathinfo($originalFilename, PATHINFO_EXTENSION);

            $filename = $filenameWithoutExtension."_".$counter.".".pathinfo($originalFilename, PATHINFO_EXTENSION);
            $targetFile = $uploadDir . basename($filename);
            $counter++;
        }

        if (move_uploaded_file($file['tmp_name'], $targetFile)) {
            $fileData['status'] = true;
            $fileData['url'] = $targetFile;
        }
        return $fileData;
    }


    function sanitizeFileName($filename) {
        $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
        $filename = str_replace(" ", "-", $filename);
        $filename = preg_replace("/[^a-zA-Z0-9\-_\.]/", "", $filename);
        return $filename;
    }
}
