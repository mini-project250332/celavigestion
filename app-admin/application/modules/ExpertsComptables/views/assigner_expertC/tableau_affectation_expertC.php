<?php 
    $style_disabled = "cursor: not-allowed !important; opacity : .6;";
?>

<div class="modal-header d-flex align-items-center justify-content-center py-2">
        <h5 class="modal-title mb-1">
        Liste des missions et experts comptables du dossier
    </h5>
</div>
<div class="modal-body">

    <div class="row m-0">
        <div class="col"></div>
        <div class="col-auto align-input-button">
            <button class="btn btn-primary fs--1" data-dossier_id="<?=$dossier_id;?>" id="add-newAffectation-ExpertComptable">
                <i class="fas fa-plus"></i> Ajouter
            </button>
        </div>
        
    </div>
        
    <div class="row m-0">
        <div id="liste-mission" class="col p-0">
            
            <div class="table-responsive scrollbar pt-2">
                <table class="table table-sm table-striped fs--1 mb-0">
                    <thead class="bg-300 text-900">
                        <tr>
                            <th class="text-start py-2" scope="col">Date de début de la mission</th>
                            <th class="text-start py-2" scope="col">Date de fin de la mission</th>
                            <th class="text-start py-2" scope="col">Expert comptable</th>
                            <th class="text-start py-2" scope="col">Fichier mission</th>
                            <th class="text-center py-2" scope="col">Fichier résiliation</th>
                            <th class="text-center py-2" scope="col">Etat</th>
                            <th class="text-center py-2" scope="col">Actions</th>
                            <th class="d-none"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if(!empty($data_missionsEC)): ?>

                            <?php foreach ($data_missionsEC as $key => $value):?> 

                                <tr id="rowComm-26">
                                    <td class="text-start align-middle">
                                        <div class="flex-1">
                                            <span class="mb-0 fw-semi-bold">
                                                <span class="stretched-link text-900">
                                                    <?=date('d/m/Y',strtotime($value->date_debut_mission));?>
                                                </span>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-start align-middle">
                                        <div class="flex-1">
                                            <span class="mb-0 fw-semi-bold">
                                                <span class="stretched-link text-900">
                                                    <?php if(!empty($value->date_resiliation_mission)): ?>
                                                        <?=date('d/m/Y',strtotime($value->date_resiliation_mission));?>
                                                    <?php else: ?>
                                                        - 
                                                    <?php endif;?>                                   
                                                </span>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-start align-middle">
                                        <span>
                                            <?=$value->expert_nom.' '.$value->expert_prenom;?>
                                        </span>
                                    </td>
                                    <td class="text-start align-middle">

                                        <?php if(!empty($value->fichier_mission)): ?>
                                            
                                            <div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
                                                <span class="fs--1 px-1" style="width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
                                                    <?php $pathInfo_fichier_mission = pathinfo($value->fichier_mission); ?>
                                                    <?=$pathInfo_fichier_mission['basename']; ?>
                                                </span>
                                                <div>
                                                    <button onclick="forceDownload('<?=base_url($value->fichier_mission);?>')" data-url="<?=base_url($value->fichier_mission); ?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center" type="button" style="height:25px;width: 25px;cursor: pointer">
                                                        <span class="fas fa-download fs-0 m-1"></span>
                                                    </button>
                                                </div>
                                            </div>

                                        <?php else: ?>
                                            <span> - </span>
                                        <?php endif;?>  

                                    </td>
                                    <td class="text-start align-middle">
                                        
                                        <?php if(!empty($value->fichier_resiliation)): ?>
                                            
                                            <div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
                                                <span class="fs--1 px-1" style="width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
                                                    <?php $pathInfo_fichier_resiliation = pathinfo($value->fichier_resiliation); ?>
                                                    <?=$pathInfo_fichier_resiliation['basename']; ?>
                                                </span>
                                                <div>
                                                    <button onclick="forceDownload('<?=base_url($value->fichier_resiliation);?>')" data-url="<?=base_url($value->fichier_resiliation); ?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center" type="button" style="height:25px;width: 25px;cursor: pointer">
                                                        <span class="fas fa-download fs-0 m-1"></span>
                                                    </button>
                                                </div>
                                            </div>

                                        <?php else: ?>
                                            <span> - </span>
                                        <?php endif;?> 

                                    </td>
                                    <td class="text-center align-middle">
                                        <?php $status_mission = ''; ?>
                                        <?php if (!empty($value->date_resiliation_mission)):?>
                                            <?php 
                                                $resiliationDate = new DateTime($value->date_resiliation_mission);
                                                $currentDate = new DateTime();
                                            ?>
                                            <?php  if ($resiliationDate < $currentDate): ?>
                                                <?php $status_mission = 'resilie'; ?>
                                                <span style="min-width:127px" class="badge rounded-0 badge-soft-danger fs--1 text-center">Résilié</span>
                                            <?php else: ?>
                                                <?php 
                                                    $debutDate = new DateTime($value->date_debut_mission);
                                                ?>
                                                <?php  if ($debutDate > $currentDate): ?>
                                                    <?php $status_mission = 'a_venir'; ?>
                                                    <span style="min-width:127px" class="badge rounded-0 badge-soft-info fs--1 text-center">
                                                        À venir
                                                    </span>
                                                <?php else: ?>
                                                    <?php $status_mission = 'en_cours'; ?>
                                                    <span style="min-width:127px" class="badge rounded-0 badge-soft-success fs--1 text-center">
                                                        En cours
                                                    </span>
                                                <?php endif;?>
                                             <?php endif;?>
                                        <?php else: ?>

                                            <?php 
                                                $debutDate = new DateTime($value->date_debut_mission);
                                                $currentDate = new DateTime();
                                            ?>
                                            <?php  if ($debutDate > $currentDate): ?>
                                                <?php $status_mission = 'a_venir'; ?>
                                                <span style="min-width:127px" class="badge rounded-0 badge-soft-info fs--1 text-center">
                                                    À venir
                                                </span>
                                            <?php else: ?>
                                                <?php $status_mission = 'en_cours'; ?>
                                                <span style="min-width:127px" class="badge rounded-0 badge-soft-success fs--1 text-center">
                                                    En cours
                                                </span>
                                            <?php endif;?>
                                        <?php endif;?>
                  
                                    </td>
                                    <td class="text-end align-middle">

                                        <div class="d-flex justify-content-center ">

                                            <?php 
                                                $btn_resiliation_cache = (!empty($value->date_resiliation_mission)) ? true : false;

                                                $btn_modification_cache = true;
                                                $index_nextMission = $key + 1;
                                                $nextMission = ($index_nextMission < count($data_missionsEC)) ? $data_missionsEC[$index_nextMission] : null;

                                                if(!empty($nextMission) && $status_mission=="resilie"){
                                                    $btn_modification_cache = false;
                                                }

                                            ?>

                                             <!-- <button data-status_mission="<?=$status_mission?>" data-dossier_id="<?=$dossier_id;?>" data-mission_id="<?=$value->expert_MD_id;?>" id="editMission_expertC-<?=$value->expert_MD_id;?>" class="btn btn-primary icon-btn p-0 m-1 d-flex align-items-center justify-content-center <?=($btn_modification_cache) ? '' : 'btn-editMission_expertC';?> " type="button" style="height:30px;width: 30px;<?=($btn_modification_cache) ? $style_disabled : '';?>">
                                                <span class="fas fa-pen fs-0 m-1"></span>
                                            </button> -->

                                            <button data-dossier_id="<?=$dossier_id;?>" data-mission_id="<?=$value->expert_MD_id;?>" id="editMission_expertC-<?=$value->expert_MD_id;?>" class="btn btn-primary icon-btn p-0 m-1 d-flex align-items-center justify-content-center <?=($btn_modification_cache) ? 'btn-editMission_expertC' : '';?> " type="button" style="height:30px;width: 30px;<?=($btn_modification_cache) ? '' : $style_disabled;?> ">
                                                <span class="fas fa-pen fs-0 m-1"></span>
                                            </button>

                                            <!-- 
                                                <button data-dossier_id="<?=$dossier_id;?>"  data-mission_id="<?=$value->expert_MD_id;?>" id="resilierMission_expertC-<?=$value->expert_MD_id;?>" class="btn d-flex align-items-center justify-content-center btn-danger icon-btn p-0 m-1 only_visuel_gestion <?=($btn_resiliation_cache) ? 'btn-update-resilierMission_expertC' : 'btn-resilierMission_expertC';?>" style="height:30px;width: 30px;" type="button">
                                                    <span class="fas fa-ban fs-0 m-1"></span>
                                                </button> 
                                            -->

                                            <button data-dossier_id="<?=$dossier_id;?>"  data-mission_id="<?=$value->expert_MD_id;?>" id="resilierMission_expertC-<?=$value->expert_MD_id;?>" class="btn d-flex align-items-center justify-content-center btn-danger icon-btn p-0 m-1 only_visuel_gestion <?=($btn_resiliation_cache) ? '' : 'btn-resilierMission_expertC';?>" style="height:30px;width: 30px;<?=($btn_resiliation_cache) ? $style_disabled : '';?>" type="button">
                                                <span class="fas fa-ban fs-0 m-1"></span>
                                            </button>
                                            
                                        </div>
                                    </td>
                                </tr>

                            <?php endforeach;?>

                        <?php  else: ?> 
                            <tr>
                                <td colspan="7">

                                    <span class="alert alert-warning border-1 d-flex justify-content-center align-items-center mt-2 py-2 px-3" role="alert">
                                        <span class="rounded-circle border-1 border-warning">
                                            <span class="fas fa-exclamation-circle text-warning fs-1"></span>
                                        </span>
                                        <span class="mb-0 flex-1 ps-2">
                                            Aucune mission d'expert comptable n'est renseignée pour ce dossier. Pour en ajouter une, cliquez sur le bouton "Ajouter".                                
                                        </span>
                                    </span>

                                </td>
                            </tr>
                        <?php  endif;?> 

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="closeModal_tableau_assignerExpertComptable">Fermer</button>
    </div>
</div>
<?php echo form_close(); ?>
