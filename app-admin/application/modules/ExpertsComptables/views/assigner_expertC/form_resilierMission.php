
<?php echo form_tag('ExpertsComptables/resilier_missionExpertC',array('method'=>"post", 'class'=>'px-2','id'=>'resilier_missionExpertC')); ?>
    
    <input type="hidden" id="dossier_id" name="dossier_id" value="<?=$dossier_id;?>">
    <input type="hidden" id="mission_id" name="mission_id" value="<?=$mission_id;?>">
    <input type="hidden" id="debut_mission" name="debut_mission" value="<?=$data_missionsEC->date_debut_mission;?>">
    <input type="hidden" id="expert_id" name="expert_id" value="<?=$data_missionsEC->expert_id;?>">

    <div class="modal-header">
        <h5 class="modal-title">
            Résiliation de la mission "<?=$data_missionsEC->expert_nom.' '.$data_missionsEC->expert_prenom;?>" du "<?=date('d/m/Y',strtotime($data_missionsEC->date_debut_mission));?>" (date de début de la mission).
        </h5>
    </div>

    <div class="modal-body">
        <div class="row m-0">
            <div class="col-12">
                <div class="alert alert-info border-1 d-flex align-items-center mt-2 py-2 px-3" role="alert">
                    <div class="rounded-circle border-1 border-info">
                        <span class="fas fa-info-circle text-info fs-1"></span>
                    </div>
                    <p class="mb-0 flex-1 ps-2">
                        Vous allez résilier la mission de cet expert comptable. Il ne pourra plus accéder aux documents de ce dossier pour les prochaines années                           
                    </p>
                </div>
            </div>

            <div class="col-12 pt-2">

                <div class="form-group p-0 pb-2">
                    <div>
                        <label class="form-label"> Date de fin de mission * :</label>
                        <input type="date" class="form-control" id="date_resiliation_mission" name="date_resiliation_mission" data-require="true">
                        <div class="help"></div>
                    </div>
                </div>

                <div class="form-group p-0 pb-2">
                    <div>
                        <label class="form-label"> Fichier justificatif de la résiliation :</label>
                        <input class="form-control fs--1" type="file" id="fichier_resiliation" name="fichier_resiliation" style="line-height: 1.5!important;">
                        <div class="help"></div>
                    </div>
                </div>
                
            </div>
        </div> 
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Confirmer</button>
        <button type="button" class="btn btn-secondary" data-dossier_id = "<?=$dossier_id;?>" id="closeModal_resilier_missionExpertC">Annuler</button>
    </div>

<?php echo form_close(); ?>
