<?php echo form_tag('ExpertsComptables/assigner_expertC_mission',array('method'=>"post", 'class'=>'px-2','id'=>'assigner_expertC_mission')); ?>
    
    <input type="hidden" id="dossier_id" name="dossier_id" value="<?=$dossier_id;?>">
    <input type="hidden" id="action" name="action" value="<?=$action;?>">

    <div class="modal-header d-flex align-items-center justify-content-center py-2">
        <h5 class="modal-title mb-1">
            Assignation d'un expert-comptable et création de mission
        </h5>
    </div>

    <div class="modal-body">
            
        <?php if(!empty($data_missionsEC)): ?>

            <?php $dernier_mission_expertC = $data_missionsEC[count($data_missionsEC) - 1];?>

            <?php if(empty($dernier_mission_expertC->date_resiliation_mission)): ?> 
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning border-1 d-flex align-items-center mt-2 py-2 px-3" role="alert">
                            <div class="rounded-circle border-1 border-info">
                                <span class="fas fa-exclamation-circle text-warning fs-3"></span>
                            </div>
                            <p class="mb-0 flex-1 ps-2">
                                Vous ne pouvez pas ajouter de nouvelle mission tant que la mission précédente n'est pas encore résiliée. Veuillez "résilier" les missions précédentes avant d'ajouter une nouvelle mission.                           
                            </p>
                        </div>
                    </div>
                </div>
            <?php endif;?>

           <?php if(!empty($dernier_mission_expertC->date_resiliation_mission)): ?>

                <input type="hidden" id="preview_expert_id" name="preview_expert_id" value="<?=$dernier_mission_expertC->expert_id;?>">
                <input type="hidden" id="preview_assiger_ECD_id" name="preview_assiger_ECD_id" value="<?=$dernier_mission_expertC->assiger_ECD_id;?>">
                <input type="hidden" id="preview_expert_MD_id" name="preview_expert_MD_id" value="<?=$dernier_mission_expertC->expert_MD_id;?>">
                <input type="hidden" id="preview_date_resiliation" name="preview_date_resiliation" value="<?=$dernier_mission_expertC->date_resiliation_mission;?>">

            <?php endif;?>

        <?php endif;?>

        <?php if( empty($data_missionsEC) || ( isset($dernier_mission_expertC) && !empty($dernier_mission_expertC->date_resiliation_mission))): ?>
            <div class="row m-0">
                <div class="col-12 pt-3">

                    <div class="mb-3">
                        <div class="form-group p-0 mt-2 pb-2">
                            <div>
                                <label class="form-label"> Date de début de la mission * :</label>
                                <input type="date" class="form-control" id="date_debut_mission" name="date_debut_mission" data-require="true">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group p-0 pb-2">
                            <div>
                                <label class="form-label"> Expert Comptable * :</label>
                                <select class="form-select fs--1" id="expert_id" name="expert_id" data-require="true">
                                    <option value=""> Choisir un expert-comptable </option>
                                    <?php foreach ($util_expert as $utilisateurs) : ?>
                                        <option value="<?= $utilisateurs->util_id; ?>" <?=(isset($dernier_mission_expertC->expert_id) && $utilisateurs->util_id==$dernier_mission_expertC->expert_id) ? 'selected' : ''?>>
                                            <?= $utilisateurs->util_prenom . ' ' . $utilisateurs->util_nom; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help"></div>
                            </div>
                        </div>

                        <?php if(!empty($data_missionsEC)): ?>

                            <div class="">
                                <span class="text-mutedmt-1" style="font-size: .7rem !important;">
                                    <span class="text-decoration-underline">Remarque :</span>
                                    <span class="fw-lighter">La valeur par défaut est l'expert-comptable de la mission précédente.</span>
                                </span>
                            </div>

                         <?php endif;?>

                    </div>

                    <div class="mb-3">
                        <div class="form-group p-0 pb-2">
                            <div>
                                <label class="form-label"> Fichier pour la lettre de mission : </label>
                                <input class="form-control fs--1" type="file" id="fichier_mission" name="fichier_mission" style="line-height: 1.5!important;">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div> 
        <?php endif;?>

    </div>
    <div class="modal-footer">
        <div class="row m-0">
            <div class="col">
                <?php if(!empty($data_missionsEC) && empty($dernier_mission_expertC->date_resiliation_mission)): ?>
                    <button type="button" class="btn btn-secondary" data-dossier_id = "<?=$dossier_id;?>" id="closeModal_assignerExpertComptable">
                        <i class="fas fa-undo me-1"></i>
                        <span>Retour</span>
                    </button>
                <?php else: ?>
                    <button type="submit" class="btn btn-success">Confirmer</button>
                    <button type="button" class="btn btn-secondary" data-dossier_id = "<?=$dossier_id;?>" id="closeModal_assignerExpertComptable">Annuler</button>
                <?php endif;?>
            </div>
        </div>
    </div>

<?php echo form_close(); ?>