<?php echo form_tag('ExpertsComptables/saveModify_missionExpertC',array('method'=>"post", 'class'=>'px-2','id'=>'saveModify_missionExpertC')); ?>
    
    <?php 
        $style_disabled = "cursor: not-allowed !important; opacity : .6;";
    ?>

    <input type="hidden" id="dossier_id" name="dossier_id" value="<?=$dossier_id;?>">
    <input type="hidden" id="expert_MD_id" name="expert_MD_id" value="<?=$data_missionsEC->expert_MD_id;?>">
    <input type="hidden" id="assiger_ECD_id" name="assiger_ECD_id" value="<?=$data_missionsEC->assiger_ECD_id;?>">
    <input type="hidden" id="current_expert_id" name="current_expert_id" value="<?=$data_missionsEC->expert_id;?>">

    <div class="modal-header d-flex align-items-center justify-content-center py-2">
        <h5 class="modal-title mb-1">
            <?php if($action=="update-resilier-mission"):?>
                Modifier l'information de résiliation de cette mission.
            <?php endif;?>

            <?php if($action=="update-detail-mission"):?>
                Modifier les détails relatifs à cette mission.
            <?php endif;?>
        </h5>
    </div>

    <div class="modal-body">

        <div class="row m-0">
            
            <div class="col-12 pt-3 ">

                <!-- forme pour modification info mission -->
                
                <div class="<?=($action == "update-detail-mission") ? '' : 'd-none';?>">

                    <div class="mb-3">
                        <div class="form-group p-0 mt-2 pb-2">
                            <div>
                                <label class="form-label"> Date de début de la mission * :</label>
                                <input type="date" class="form-control" id="date_debut_mission" name="date_debut_mission" value ="<?=date("Y-m-d", strtotime($data_missionsEC->date_debut_mission))?>" data-require="true">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group p-0 pb-2">
                            <div>
                                <label class="form-label"> Expert Comptable * :</label>
                                <select class="form-select fs--1" id="expert_id" name="expert_id" data-require="true">
                                    <option value=""> Choisir un expert-comptable </option>
                                    <?php foreach ($util_expert as $utilisateurs) : ?>
                                        <option value="<?= $utilisateurs->util_id; ?>" <?=($utilisateurs->util_id==$data_missionsEC->expert_id) ? 'selected' : ''?>>
                                            <?= $utilisateurs->util_prenom . ' ' . $utilisateurs->util_nom; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <div id="champ_fichier_mission" class="mb-3 <?=(!empty($data_missionsEC->fichier_mission)) ? 'd-none' : '';?>" >
                        <div class="form-group p-0 mt-2 pb-2">
                            <div>
                                <label class="form-label">Fichier pour la lettre de mission :</label>
                                <input class="form-control fs--1" type="file" id="fichier_mission" name="fichier_mission" style="line-height: 1.5!important;">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    

                    <?php if(!empty($data_missionsEC->fichier_mission)): ?>
                        <div id="div_affichage_fichier_mission">
                            <label class="form-label">Fichier pour la lettre de mission :</label>
                            <div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
                                <span class="fs--1 px-1" style="width: calc(100% - 30px);max-width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
                                    <?php $pathInfo_fichier_mission = pathinfo($data_missionsEC->fichier_mission); ?>
                                    <?=$pathInfo_fichier_mission['basename']; ?>
                                </span>
                                <div>
                                    <button data-fichier="mission" data-id="<?=$data_missionsEC->expert_MD_id;?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center btn-supprimerFileMission" type="button" style="height:25px;width: 25px;cursor: pointer">
                                        <i class="fas fa-times fs-0 m-1"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php endif;?> 

                </div>

                

                <!-- forme pour modification info resiliation de mission -->


                <div class="<?=($action=="update-resilier-mission") ? '' : 'd-none';?>">

                    <div class="form-group p-0 pb-2" style="<?=(!empty($nextMission)) ? $style_disabled : '';?>">
                        <div style="<?=(!empty($nextMission)) ? $style_disabled : '';?>">
                            <label class="form-label" style="<?=(!empty($nextMission)) ? $style_disabled : '';?>"> Date de fin de mission :</label>
                            
                            <input style="<?=(!empty($nextMission)) ? $style_disabled : '';?>" type="date" class="form-control" id="date_resiliation_mission" name="date_resiliation_mission" data-require="true"  <?=(!empty($data_missionsEC->date_resiliation_mission)) ? 'value ="'.date("Y-m-d", strtotime($data_missionsEC->date_resiliation_mission)).'"' : ''?> <?=(!empty($nextMission)) ? 'disabled' : '';?>>

                            <div class="help"></div>
                        </div>
                    </div>

                    <div id="champ_fichier_resiliation" class="mb-3 <?=(!empty($data_missionsEC->fichier_resiliation)) ? 'd-none' : '';?>" >
                        <div class="form-group p-0 pb-2">
                            <div>
                                <label class="form-label"> Fichier justificatif de la résiliation :</label>
                                <input class="form-control fs--1" type="file" id="fichier_resiliation" name="fichier_resiliation" style="line-height: 1.5!important;">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <?php if(!empty($data_missionsEC->fichier_resiliation)): ?>
                        <div id="div_affichage_fichier_resiliation">
                            <label class="form-label">Fichier justificatif de la résiliation :</label>
                            <div class="w-100 d-flex align-items-center justify-content-between mb-1 border rounded-3 px-1 py-1 bg-white">
                                <span class="fs--1 px-1" style="width: calc(100% - 30px);max-width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;color:#2569C3;font-weight: 700;">
                                    <?php $pathInfo_fichier_resiliation = pathinfo($data_missionsEC->fichier_resiliation); ?>
                                    <?=$pathInfo_fichier_resiliation['basename']; ?>
                                </span>
                                <div>
                                    <button data-fichier="resiliation" data-id="<?=$data_missionsEC->expert_MD_id;?>" class="btn btn-secondary icon-btn p-0 d-flex align-items-center justify-content-center btn-supprimerFileMission" type="button" style="height:25px;width: 25px;cursor: pointer">
                                        <i class="fas fa-times fs-0 m-1"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php endif;?> 
                </div>

            </div>
        </div> 
           
    </div>
    <div class="modal-footer">
        <div class="row m-0">
            <div class="col">
    
                <button type="submit" class="btn btn-success">Confirmer</button>
                <button type="button" class="btn btn-secondary" data-dossier_id = "<?=$dossier_id;?>" id="closeModal_modif_affectationMission">Annuler</button>

            </div>
        </div>
    </div>

<?php echo form_close(); ?>
