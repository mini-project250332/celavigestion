<?php

use phpDocumentor\Reflection\DocBlock\Tags\Var_;

defined('BASEPATH') or exit('No direct script access allowed');

class Loyer extends ADM_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Loyer";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();

        $this->_script = array(
            "js/bail/bail.js",
            "js/loyer/loyer.js",
            "js/historique/historique.js"
        );

        $this->_css_personnaliser = array(
            "css/loyer/loyer.css"
        );
    }

    public function index()
    {
    }

    public function getLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m');
                $this->load->model('DocumentLoyer_m');

                $data_post = $_POST['data'];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['year'] = $data_post['year'];
                $bails = $this->Bail_m->GetbyYear($data_post['cliLot_id']);

                $data['document'] = $this->DocumentLoyer_m->GetbyYear($data_post['cliLot_id'], $data_post['year']);
                $data['bail'] = $bails;

                $this->renderComponant("Clients/clients/lot/loyers/loyer-facture", $data);
            }
        }
    }

    public function verifyLoyerValide()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Bail_m');
                $this->load->model('DocumentLoyer_m');
                $this->load->model('ClientLot_m');

                $data_post = $_POST['data'];

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['year'] = $data_post['year'];
                $bails = $this->Bail_m->GetbyYear($data_post['cliLot_id']);
                $data['document'] = $this->DocumentLoyer_m->GetbyYear($data_post['cliLot_id'], $data_post['year']);
                $data['bail'] = $bails;

                $isAllInvalid = true;
                $isFactureValide = true;

                $data['nombreBail'] = $nombreBail = count($bails);

                if ($bails[$nombreBail - 1]->bail_valide == 1) {
                    $isAllInvalid = false;
                }

                if ($isAllInvalid == false) {
                    if ($bails[$nombreBail - 1]->bail_facture == 1) {
                        $isFactureValide = false;
                    }
                }

                $params_check_siret_tva = array(
                    'clause' => array('cliLot_id ' => $data_post['cliLot_id']),
                    'join' => array(
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                    ),
                    'method' => "row",
                );
                $data['check_siret_tva'] = $this->ClientLot_m->get($params_check_siret_tva);

                if ($isAllInvalid) {
                    $this->renderComponant("Clients/clients/lot/loyers/bail-invalide", $data);
                } elseif ($isFactureValide) {
                    $this->renderComponant("Clients/clients/lot/loyers/bail-none-facture", $data);
                } else {
                    echo null;
                }
            }
        }
    }

    function verify_dateBetween($post, $date_debut, $date_fin)
    {
        $post = date('Y-m-d', strtotime($post));
        $contractDateBegin = date('Y-m-d', strtotime($date_debut));
        $contractDateEnd = date('Y-m-d', strtotime($date_fin));
        return (($post >= $contractDateBegin) && ($post <= $contractDateEnd)) ? true : false;
    }

    public function getindexation($date, $bail_id)
    {
        $this->load->model('IndexationLoyer_m', 'index');

        $param_index = array(
            'clause' => array('bail_id ' => $bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );

        $data_indexation = $this->index->get($param_index);

        $interval = array();

        foreach ($data_indexation as $key => $value) {
            $interval[] = array(
                'id' => $value->indlo_id,
                'debut' => $value->indlo_debut,
                'fin' => $value->indlo_fin
            );
        }

        $value_index = null;

        foreach ($interval as $key => $value) {
            if ($this->verify_dateBetween($date, $value['debut'], $value['fin'])) {
                $currentIndexation = $data_indexation[$key];
                $value_index = array();
                array_push($value_index, $currentIndexation);
            }
        }
        return $value_index;
    }

    private function get_emetteur($bail_id)
    {
        $this->load->model('Bail_m', 'bail');
        $this->load->model('Program_m', 'program');

        $param = array(
            'clause' => array('c_bail.bail_id' => $bail_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
                'c_client' => 'c_client.client_id = c_lot_client.cli_id'
            ),
        );

        $get_emet = $this->bail->get($param);

        $data = array(
            'emetteur' => $get_emet[0]->client_nom,
            'adress1' => $get_emet[0]->dossier_adresse1,
            'adress2' => $get_emet[0]->dossier_adresse2,
            'adress3' => $get_emet[0]->dossier_adresse3,
            'cp' => $get_emet[0]->dossier_cp,
            'ville' => $get_emet[0]->dossier_ville,
            'pays' => $get_emet[0]->dossier_pays
        );

        if ($get_emet[0]->dossier_type_adresse == 'Lot principal') {
            $params = array(
                'clause' => array('progm_id' => $get_emet[0]->progm_id),
            );

            $get_program = $this->program->get($params);

            $data = array(
                'emetteur' => $get_emet[0]->client_nom,
                'adress1' => $get_program[0]->progm_adresse1,
                'adress2' => $get_program[0]->progm_adresse2,
                'adress3' => $get_program[0]->progm_adresse3,
                'cp' => $get_program[0]->progm_cp,
                'ville' => $get_program[0]->progm_ville,
                'pays' => $get_program[0]->progm_pays
            );
        }

        return $data;
    }

    private function get_numero_factu($bail_id)
    {
        $this->load->model('FactureLoyer_m', 'facture');
        $this->load->model('Bail_m', 'bail');

        $param = array(
            'clause' => array('bail_id' => $bail_id),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            ),
            'method' => 'row'
        );

        $res = $this->bail->get($param);

        $params_get_dossier = array(
            'clause' => array('dossier_id' => $res->dossier_id),
            'order_by_columns' => "fact_loyer_id  DESC",
            'limit' => 1,
            'method' => 'row'
        );

        $facture_dossier = $this->facture->get($params_get_dossier);

        if (empty($facture_dossier)) {
            $facture_num_suivant = 1;
        } else {
            $num_factu_precedent = explode("-", $facture_dossier->fact_loyer_num);
            $facture_num_suivant = intval($num_factu_precedent[1]) + 1;
        }
        return $facture_num_suivant;
    }

    private function getindexation_month($date_debut, $date_fin, $bail_id)
    {
        $this->load->model('IndexationLoyer_m', 'index');
        $param_index = array(
            'clause' => array('bail_id ' => $bail_id, 'etat_indexation' => 1, 'indlo_ref_loyer' => 0),
        );
        $data_indexation = $this->index->get($param_index);

        $interval = array();
        foreach ($data_indexation as $key => $value) {
            $interval[] = array(
                'id' => $value->indlo_id,
                'debut' => $value->indlo_debut,
                'fin' => $value->indlo_fin
            );
        }
        $value_index = null;
        foreach ($interval as $key => $value) {
            if ($this->verify_dateBetween_month($date_debut, $date_fin, $value['debut'], $value['fin'])) {
                $currentIndexation = $data_indexation[$key];
                $value_index = array();
                array_push($value_index, $currentIndexation);
            }
        }
        return $value_index;
    }

    private function verify_dateBetween_month($post_debut, $post_fin, $date_debut, $date_fin)
    {
        $post_debut = date('Y-m-d', strtotime($post_debut));
        $post_fin = date('Y-m-d', strtotime($post_fin));
        $contractDateBegin = date('Y-m-d', strtotime($date_debut));
        $contractDateEnd = date('Y-m-d', strtotime($date_fin));

        if (($post_debut >= $contractDateBegin) && ($post_fin <= $contractDateEnd)) return true;
        if (($contractDateBegin <= $post_fin) && ($post_fin <= $contractDateEnd)) return true;
        // if (($contractDateEnd >= $post_debut) && ($contractDateEnd <= $post_fin)) return true;

        return false;
    }

    public function GetTableFacture()
    {
        $this->load->model('Acte_m');
        $this->load->model('Bail_m');
        $this->load->model('IndexationLoyer_m');
        $this->load->model('Encaissement_m');
        $this->load->model('FactureLoyer_m');
        $this->load->model('TauxTVA_m');
        $this->load->model('PeriodeAnnuel_m');
        $this->load->model('ClientLot_m');
        $this->load->model('TypeProduitLotEncaissement_m');
        $this->load->model('TypeChargeDeductibleLotEncaissement_m');

        $data_post = $_POST['data'];

        $bail_prise_effet = $this->Bail_m->get(
            array(
                'columns' => array('bail_debut'),
                'clause' => array('bail_id ' => $data_post['bail_id']),
                'method' => 'row'
            )
        );

        $this->load->helper("loyer");
        $list_mois = genererListeMoisDansAnnee($data_post['year'], false);
        $list_full_mois = genererListeMoisDansAnnee($data_post['year'], true);
        $lot_id = $data_post['lot_id'];

        //obtenir Acte
        $getacte = $this->Acte_m->get(array(
            'clause' => array('cliLot_id ' => $lot_id),
            'method' => 'row'
        ));

        $encaissementParams = array(
            'clause' => array(
                'DATE_FORMAT(enc_date,"%Y")' => $data_post['year'],
                'clilot_id' => $lot_id,
                "enc_etat" => 1
            )
        );
        $encaissements = $this->Encaissement_m->get($encaissementParams);

        $param_index = array(
            'clause' => array('bail_id ' => $data_post['bail_id']),
            'join' => array('c_gestionnaire' => 'c_gestionnaire.gestionnaire_id  = c_bail.gestionnaire_id '),
        );

        $paramLot = array(
            'clause' => array('cliLot_id ' => $lot_id),
            'method' => 'row'
        );

        $data['lot_facture'] = $this->ClientLot_m->get($paramLot);

        $data['addresse_gestionnaire'] = $this->Bail_m->get($param_index);

        $data['addresse_emetteur'] = $this->get_emetteur($data_post['bail_id']);
        $data['numero_facture'] = $this->get_numero_factu($data_post['bail_id']);

        // trimestriel
        if ($data_post['pdl_id'] == 2) {
            $indexation = array();

            foreach ($list_mois as $key => $value) {
                $debut = $list_mois[0]['debut'];
                if (($key + 1) % 3 === 0) {
                    $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                    $indexation[] = array(
                        'debut' => $debut,
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                    );
                }
            }

            if (!empty($getacte) && $getacte->acte_date_notarie != NULL &&  date('Y-m-d', strtotime($bail_prise_effet->bail_debut) < date('Y-m-d', strtotime($getacte->acte_date_notarie)))) {
                $date_limite = $getacte->acte_date_notarie;
            } else {
                $date_limite = $bail_prise_effet->bail_debut;
            }

            foreach ($indexation as &$value) {
                $dateFin = new DateTime($value['fin']);
                $dateLimiteObj = new DateTime($date_limite);

                if ($dateLimiteObj > $dateFin) {
                    $value['indexation'] = array();
                }
            }
            unset($value);

            $arrayFiltre = array_filter($indexation, function ($element) use ($bail_prise_effet) {
                $dateDebut = new DateTime($element['debut']);
                $dateFin = new DateTime($element['fin']);
                $dateLimiteObj = new DateTime($bail_prise_effet->bail_debut);
                return ($dateLimiteObj > $dateDebut && $dateLimiteObj <= $dateFin);
            });

            if (!empty($arrayFiltre)) {
                foreach ($arrayFiltre as $key => $index) {
                    $date1 = $bail_prise_effet->bail_debut;
                    $date2 = $index['fin'];

                    $diff_in_seconds = strtotime($date2) - strtotime($date1);
                    $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $date11 = $index['debut'];
                    $date22 = $index['fin'];

                    $diff_in_seconds = strtotime($date22) - strtotime($date11);
                    $days_3month = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $end_month = (date("d", strtotime($index['fin']))) + 1;
                    foreach ($index['indexation'] as $key => $value) {
                        $value->indlo_appartement_ht = round((($value->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                        $tva_appartement = $value->indlo_appartement_ht * $value->indlo_appartement_tva / 100;
                        $value->indlo_appartement_ttc = $value->indlo_appartement_ht + $tva_appartement;

                        $value->indlo_parking_ht = round((($value->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                        $tva_parking = $value->indlo_parking_ht * $value->indlo_parking_tva / 100;
                        $value->indlo_parking_ttc = $value->indlo_parking_ht + $tva_parking;

                        $value->indlo_charge_ht = round((($value->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                        $tva_charge = $value->indlo_charge_ht * $value->indlo_charge_tva / 100;
                        $value->indlo_charge_ttc = $value->indlo_charge_ht + $tva_charge;
                    }
                }

                $combinedArray = [];
                $indexedArray2 = [];

                foreach ($arrayFiltre as $item) {
                    $indexedArray2[$item['nom']] = $item;
                }

                foreach ($indexation as $item1) {
                    if (isset($indexedArray2[$item1['nom']])) {
                        $combinedArray[] = $indexedArray2[$item1['nom']];
                    } else {
                        $combinedArray[] = $item1;
                    }
                }
                $indexation = $combinedArray;
            }

            $indlo_appartement_ht = 0;
            $indlo_appartement_tva = 0;
            $indlo_appartement_ttc = 0;
            $indlo_parking_ht = 0;
            $indlo_parking_tva = 0;
            $indlo_parking_ttc = 0;
            $indlo_charge_ht = 0;
            $indlo_charge_tva = 0;
            $indlo_charge_ttc = 0;
            $data_total_ttc_theorique = array();

            foreach ($indexation as $d) {
                if (!empty($d['indexation'])) {
                    foreach ($d['indexation'] as $index) {
                        $indlo_appartement_ht += $index->indlo_appartement_ht;
                        $indlo_appartement_tva += $index->indlo_appartement_tva;
                        $indlo_appartement_ttc += $index->indlo_appartement_ttc;
                        $indlo_parking_ht += $index->indlo_parking_ht;
                        $indlo_parking_tva += $index->indlo_parking_tva;
                        $indlo_parking_ttc += $index->indlo_parking_ttc;
                        $indlo_charge_ht += $index->indlo_charge_ht;
                        $indlo_charge_tva += $index->indlo_charge_tva;
                        $indlo_charge_ttc += $index->indlo_charge_ttc;
                        $total_ttc_theorique = $index->indlo_appartement_ttc + $index->indlo_parking_ttc - $index->indlo_charge_ttc;
                    }
                } else {
                    $total_ttc_theorique = 0;
                }
                array_push($data_total_ttc_theorique, $total_ttc_theorique);
            }

            $ligne = array(
                "nom" => 'Total',
                'indlo_appartement_ht' => $indlo_appartement_ht,
                'indlo_appartement_tva' => $indlo_appartement_tva,
                'indlo_appartement_ttc' => $indlo_appartement_ttc,
                'indlo_parking_ht' => $indlo_parking_ht,
                'indlo_parking_tva' => $indlo_parking_tva,
                'indlo_parking_ttc' => $indlo_parking_ttc,
                'indlo_charge_ht' => $indlo_charge_ht,
                'indlo_charge_tva' => $indlo_charge_tva,
                'indlo_charge_ttc' => $indlo_charge_ttc,
            );
            $data['indexation_trimestriel_total_ttc_theorique_par_colonne'] = $data_total_ttc_theorique;
            $data['indexation_trimestriel_total_ttc_theorique'] = array_sum($data_total_ttc_theorique);
            $data['indexation_trimestriel_total'] = $ligne;
            $data['indexation_trimestriel'] = $indexation;
        }
        //trimestriel decalée 
        elseif ($data_post['pdl_id'] == 5) {
            $param_bail = array(
                'clause' => array('bail_id ' => $data_post['bail_id']),
                'method' => 'row'
            );
            $bail = $this->Bail_m->get($param_bail);

            $key_value = new DateTime($data_post['year'] . '-' . $bail->premier_mois_trimes . '-1');
            //re-order Trimestre

            $greater_than_data = array_filter($list_mois, function ($d) use ($key_value) {
                $debut = new DateTime($d['debut']);
                return $debut >= $key_value;
            });

            $less_than_data = array_filter($list_mois, function ($d) use ($key_value) {
                $debut = new DateTime($d['debut']);
                return $debut < $key_value;
            });

            $reordered_data = array_merge($greater_than_data, $less_than_data);

            $indexation = array();
            foreach ($reordered_data as $key => $value) {
                $debut = $list_mois[0]['debut'];
                if (($key + 1) % 3 === 0) {
                    $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                    $indexation[] = array(
                        'debut' => $debut,
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                    );
                }
            }

            if ($bail->momfac_id == 1) {

                $listTimestre = array(
                    array("nom" => "T1", "value" => $indexation[0]['nom'], "debut" => $indexation[0]['debut'], "fin" => $indexation[0]['fin'], 'periode' => 1),
                    array("nom" => "T2", "value" => $indexation[1]['nom'], "debut" => $indexation[1]['debut'], "fin" => $indexation[1]['fin'], 'periode' => 2),
                    array("nom" => "T3", "value" => $indexation[2]['nom'], "debut" => $indexation[2]['debut'], "fin" => $indexation[2]['fin'], 'periode' => 3),
                    array("nom" => "T4", "value" => $indexation[3]['nom'], "debut" => $indexation[3]['debut'], "fin" => $indexation[3]['fin'], 'periode' => 4),
                );
                //re-order Header
                $less_than_data_listTimestre = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_listTimestre  = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut >= $key_value;
                });
                $data['listTimestre'] = array_merge($less_than_data_listTimestre, $greater_than_data_listTimestre);

                //re-order indexations
                $less_than_data_indexation = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_indexation  = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut >= $key_value;
                });

                $data_indexation = array_merge($less_than_data_indexation, $greater_than_data_indexation);
            } else {
                $year = $data_post['year'];

                $indexation_data_great = array_filter($indexation, function ($t) use ($year) {
                    $year_index_debut = date('Y', strtotime($t['debut']));
                    $year_index_fin = date('Y', strtotime($t['fin']));
                    return  $year == $year_index_debut && $year == $year_index_fin;
                });

                $indexation_data_less = array_filter($indexation, function ($t) use ($year) {
                    $year_index_debut = date('Y', strtotime($t['debut']));
                    $year_index_fin = date('Y', strtotime($t['fin']));
                    return  $year > $year_index_debut && $year == $year_index_fin;
                });

                $indexation_data_less_chnged = array();
                foreach ($indexation_data_less as $key => $value) {
                    $debut = date('Y-m-d', strtotime('+1 year', strtotime($value['debut'])));
                    $fin = date('Y-m-d', strtotime('+1 year', strtotime($value['fin'])));
                    $indexation_data_less_chnged[] = array(
                        'debut' => $debut,
                        'fin' => $fin,
                        'nom' => $value['nom'],
                        'indexation' => $this->getindexation_month($debut, $fin, $data_post['bail_id']),
                    );
                }

                $indexation = array();
                $indexation = array_merge($indexation_data_great, $indexation_data_less_chnged);

                //re-order indexations
                $less_than_data_indexation = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut < $key_value;
                });

                $greater_than_data_indexation  = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut >= $key_value;
                });

                $data_indexation_before_final = array_merge($greater_than_data_indexation, $less_than_data_indexation);

                function compareDateDebut($a, $b)
                {
                    return strcmp($a['debut'], $b['debut']);
                }

                usort($data_indexation_before_final, 'compareDateDebut');


                //re-order indexations final
                $less_than_data_indexation_final = array_filter($data_indexation_before_final, function ($final) use ($key_value) {
                    $debut = new DateTime($final['debut']);
                    return $debut < $key_value;
                });


                $greater_than_data_indexation_final  = array_filter($data_indexation_before_final, function ($final) use ($key_value) {
                    $debut = new DateTime($final['debut']);
                    return $debut >= $key_value;
                });

                $data_indexation = array_merge($greater_than_data_indexation_final, $less_than_data_indexation_final);

                $listTimestre = array(
                    array("nom" => "T1", "value" => $data_indexation[0]['nom'], "debut" => $data_indexation[0]['debut'], "fin" => $data_indexation[0]['fin'], 'periode' => 1),
                    array("nom" => "T2", "value" => $data_indexation[1]['nom'], "debut" => $data_indexation[1]['debut'], "fin" => $data_indexation[1]['fin'], 'periode' => 2),
                    array("nom" => "T3", "value" => $data_indexation[2]['nom'], "debut" => $data_indexation[2]['debut'], "fin" => $data_indexation[2]['fin'], 'periode' => 3),
                    array("nom" => "T4", "value" => $data_indexation[3]['nom'], "debut" => $data_indexation[3]['debut'], "fin" => $data_indexation[3]['fin'], 'periode' => 4),
                );

                //re-order Header
                $less_than_data_listTimestre = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_listTimestre  = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut >= $key_value;
                });
                $data['listTimestre'] = array_merge($less_than_data_listTimestre, $greater_than_data_listTimestre);
            }

            if (!empty($getacte) && $getacte->acte_date_notarie != NULL &&  date('Y-m-d', strtotime($bail_prise_effet->bail_debut) < date('Y-m-d', strtotime($getacte->acte_date_notarie)))) {
                $date_limite = $getacte->acte_date_notarie;
            } else {
                $date_limite = $bail_prise_effet->bail_debut;
            }

            foreach ($data_indexation as &$value) {
                $dateFin = new DateTime($value['fin']);
                $dateLimiteObj = new DateTime($date_limite);

                if ($dateLimiteObj > $dateFin) {
                    $value['indexation'] = array();
                }
            }
            unset($value);

            $arrayFiltre = array_filter($indexation, function ($element) use ($bail_prise_effet) {
                $dateDebut = new DateTime($element['debut']);
                $dateFin = new DateTime($element['fin']);
                $dateLimiteObj = new DateTime($bail_prise_effet->bail_debut);
                return ($dateLimiteObj > $dateDebut && $dateLimiteObj <= $dateFin);
            });

            if (!empty($arrayFiltre)) {
                foreach ($arrayFiltre as $key => $index) {
                    $date1 = $bail_prise_effet->bail_debut;
                    $date2 = $index['fin'];

                    $diff_in_seconds = strtotime($date2) - strtotime($date1);
                    $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $date11 = $index['debut'];
                    $date22 = $index['fin'];

                    $diff_in_seconds = strtotime($date22) - strtotime($date11);
                    $days_3month = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $end_month = (date("d", strtotime($index['fin']))) + 1;
                    foreach ($index['indexation'] as $key => $value) {
                        $value->indlo_appartement_ht = round((($value->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                        $tva_appartement = $value->indlo_appartement_ht * $value->indlo_appartement_tva / 100;
                        $value->indlo_appartement_ttc = $value->indlo_appartement_ht + $tva_appartement;

                        $value->indlo_parking_ht = round((($value->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                        $tva_parking = $value->indlo_parking_ht * $value->indlo_parking_tva / 100;
                        $value->indlo_parking_ttc = $value->indlo_parking_ht + $tva_parking;

                        $value->indlo_charge_ht = round((($value->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                        $tva_charge = $value->indlo_charge_ht * $value->indlo_charge_tva / 100;
                        $value->indlo_charge_ttc = $value->indlo_charge_ht + $tva_charge;
                    }
                }

                $combinedArray = [];
                $indexedArray2 = [];

                foreach ($arrayFiltre as $item) {
                    $indexedArray2[$item['nom']] = $item;
                }

                foreach ($data_indexation as $item1) {
                    if (isset($indexedArray2[$item1['nom']])) {
                        $combinedArray[] = $indexedArray2[$item1['nom']];
                    } else {
                        $combinedArray[] = $item1;
                    }
                }
                $indexation = $combinedArray;
            }

            $indlo_appartement_ht = 0;
            $indlo_appartement_tva = 0;
            $indlo_appartement_ttc = 0;
            $indlo_parking_ht = 0;
            $indlo_parking_tva = 0;
            $indlo_parking_ttc = 0;
            $indlo_charge_ht = 0;
            $indlo_charge_tva = 0;
            $indlo_charge_ttc = 0;
            $data_total_ttc_theorique = array();

            foreach ($data_indexation as $d) {
                if (!empty($d['indexation'])) {
                    foreach ($d['indexation'] as $index) {
                        $indlo_appartement_ht += $index->indlo_appartement_ht;
                        $indlo_appartement_tva += $index->indlo_appartement_tva;
                        $indlo_appartement_ttc += $index->indlo_appartement_ttc;
                        $indlo_parking_ht += $index->indlo_parking_ht;
                        $indlo_parking_tva += $index->indlo_parking_tva;
                        $indlo_parking_ttc += $index->indlo_parking_ttc;
                        $indlo_charge_ht += $index->indlo_charge_ht;
                        $indlo_charge_tva += $index->indlo_charge_tva;
                        $indlo_charge_ttc += $index->indlo_charge_ttc;
                        $total_ttc_theorique = $index->indlo_appartement_ttc + $index->indlo_parking_ttc - $index->indlo_charge_ttc;
                    }
                } else {
                    $total_ttc_theorique = 0;
                }
                array_push($data_total_ttc_theorique, $total_ttc_theorique);
            }

            $ligne = array(
                "nom" => 'Total',
                'indlo_appartement_ht' => $indlo_appartement_ht,
                'indlo_appartement_tva' => $indlo_appartement_tva,
                'indlo_appartement_ttc' => $indlo_appartement_ttc,
                'indlo_parking_ht' => $indlo_parking_ht,
                'indlo_parking_tva' => $indlo_parking_tva,
                'indlo_parking_ttc' => $indlo_parking_ttc,
                'indlo_charge_ht' => $indlo_charge_ht,
                'indlo_charge_tva' => $indlo_charge_tva,
                'indlo_charge_ttc' => $indlo_charge_ttc,
            );
            $data['indexation_trimestriel_total_ttc_theorique_par_colonne'] = $data_total_ttc_theorique;
            $data['indexation_trimestriel_total_ttc_theorique'] = array_sum($data_total_ttc_theorique);
            $data['indexation_trimestriel_total'] = $ligne;
            $data['indexation_trimestriel'] = $indexation;
        }
        // semestriel
        elseif ($data_post['pdl_id'] == 3) {
            $indexation = array();

            foreach ($list_mois as $key => $value) {
                if (($key + 1) % 6 === 0) {
                    $debut = date('Y-m-d', strtotime('-5 months', strtotime($value['debut'])));
                    $indexation[] = array(
                        'debut' => $debut,
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                    );
                }
            }

            if (!empty($getacte) && $getacte->acte_date_notarie != NULL &&  date('Y-m-d', strtotime($bail_prise_effet->bail_debut) < date('Y-m-d', strtotime($getacte->acte_date_notarie)))) {
                $date_limite = $getacte->acte_date_notarie;
            } else {
                $date_limite = $bail_prise_effet->bail_debut;
            }

            foreach ($indexation as &$value) {
                $dateFin = new DateTime($value['fin']);
                $dateLimiteObj = new DateTime($date_limite);

                if ($dateLimiteObj > $dateFin) {
                    $value['indexation'] = array();
                }
            }
            unset($value);

            $arrayFiltre = array_filter($indexation, function ($element) use ($bail_prise_effet) {
                $dateDebut = new DateTime($element['debut']);
                $dateFin = new DateTime($element['fin']);
                $dateLimiteObj = new DateTime($bail_prise_effet->bail_debut);
                return ($dateLimiteObj > $dateDebut && $dateLimiteObj <= $dateFin);
            });

            if (!empty($arrayFiltre)) {
                foreach ($arrayFiltre as $key => $index) {
                    $date1 = $bail_prise_effet->bail_debut;
                    $date2 = $index['fin'];

                    $diff_in_seconds = strtotime($date2) - strtotime($date1);
                    $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $date11 = $index['debut'];
                    $date22 = $index['fin'];

                    $diff_in_seconds = strtotime($date22) - strtotime($date11);
                    $days_3month = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $end_month = (date("d", strtotime($index['fin']))) + 1;
                    foreach ($index['indexation'] as $key => $value) {
                        $value->indlo_appartement_ht = round((($value->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                        $tva_appartement = $value->indlo_appartement_ht * $value->indlo_appartement_tva / 100;
                        $value->indlo_appartement_ttc = $value->indlo_appartement_ht + $tva_appartement;

                        $value->indlo_parking_ht = round((($value->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                        $tva_parking = $value->indlo_parking_ht * $value->indlo_parking_tva / 100;
                        $value->indlo_parking_ttc = $value->indlo_parking_ht + $tva_parking;

                        $value->indlo_charge_ht = round((($value->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                        $tva_charge = $value->indlo_charge_ht * $value->indlo_charge_tva / 100;
                        $value->indlo_charge_ttc = $value->indlo_charge_ht + $tva_charge;
                    }
                }

                $combinedArray = [];
                $indexedArray2 = [];

                foreach ($arrayFiltre as $item) {
                    $indexedArray2[$item['nom']] = $item;
                }

                foreach ($indexation as $item1) {
                    if (isset($indexedArray2[$item1['nom']])) {
                        $combinedArray[] = $indexedArray2[$item1['nom']];
                    } else {
                        $combinedArray[] = $item1;
                    }
                }
                $indexation = $combinedArray;
            }

            $indlo_appartement_ht = 0;
            $indlo_appartement_tva = 0;
            $indlo_appartement_ttc = 0;
            $indlo_parking_ht = 0;
            $indlo_parking_tva = 0;
            $indlo_parking_ttc = 0;
            $indlo_charge_ht = 0;
            $indlo_charge_tva = 0;
            $indlo_charge_ttc = 0;
            $data_total_ttc_theorique = array();

            foreach ($indexation as $d) {
                if (!empty($d['indexation'])) {
                    foreach ($d['indexation'] as $index) {
                        $indlo_appartement_ht += $index->indlo_appartement_ht;
                        $indlo_appartement_tva += $index->indlo_appartement_tva;
                        $indlo_appartement_ttc += $index->indlo_appartement_ttc;
                        $indlo_parking_ht += $index->indlo_parking_ht;
                        $indlo_parking_tva += $index->indlo_parking_tva;
                        $indlo_parking_ttc += $index->indlo_parking_ttc;
                        $indlo_charge_ht += $index->indlo_charge_ht;
                        $indlo_charge_tva += $index->indlo_charge_tva;
                        $indlo_charge_ttc += $index->indlo_charge_ttc;
                        $total_ttc_theorique = $index->indlo_appartement_ttc + $index->indlo_parking_ttc - $index->indlo_charge_ttc;
                    }
                } else {
                    $total_ttc_theorique = 0;
                }
                array_push($data_total_ttc_theorique, $total_ttc_theorique);
            }

            $ligne = array(
                "nom" => 'Total',
                'indlo_appartement_ht' => $indlo_appartement_ht,
                'indlo_appartement_tva' => $indlo_appartement_tva,
                'indlo_appartement_ttc' => $indlo_appartement_ttc,
                'indlo_parking_ht' => $indlo_parking_ht,
                'indlo_parking_tva' => $indlo_parking_tva,
                'indlo_parking_ttc' => $indlo_parking_ttc,
                'indlo_charge_ht' => $indlo_charge_ht,
                'indlo_charge_tva' => $indlo_charge_tva,
                'indlo_charge_ttc' => $indlo_charge_ttc,

            );

            $data['indexation_semestriel_total_ttc_theorique_par_colonne'] = $data_total_ttc_theorique;
            $data['indexation_semestriel_total_ttc_theorique'] = array_sum($data_total_ttc_theorique);
            $data['indexation_semestriel_total'] = $ligne;
            $data['indexation_semestriel'] = $indexation;
        }
        //annuel 
        elseif ($data_post['pdl_id'] == 4) {
            $indexation = array();

            foreach ($list_mois as $key => $value) {

                if (($key + 1) % 12 === 0) {
                    $debut = date('Y-m-d', strtotime('-11 months', strtotime($value['debut'])));
                    $indexation[] = array(
                        'debut' => $debut,
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => $this->getindexation_month($debut, $value['fin'], $data_post['bail_id']),
                    );
                }
            }

            if (!empty($getacte) && $getacte->acte_date_notarie != NULL &&  date('Y-m-d', strtotime($bail_prise_effet->bail_debut) < date('Y-m-d', strtotime($getacte->acte_date_notarie)))) {
                $date_limite = $getacte->acte_date_notarie;
            } else {
                $date_limite = $bail_prise_effet->bail_debut;
            }

            foreach ($indexation as &$value) {
                $dateFin = new DateTime($value['fin']);
                $dateLimiteObj = new DateTime($date_limite);

                if ($dateLimiteObj > $dateFin) {
                    $value['indexation'] = array();
                }
            }
            unset($value);

            $arrayFiltre = array_filter($indexation, function ($element) use ($bail_prise_effet) {
                $dateDebut = new DateTime($element['debut']);
                $dateFin = new DateTime($element['fin']);
                $dateLimiteObj = new DateTime($bail_prise_effet->bail_debut);
                return ($dateLimiteObj > $dateDebut && $dateLimiteObj <= $dateFin);
            });

            if (!empty($arrayFiltre)) {
                foreach ($arrayFiltre as $key => $index) {
                    $date1 = $bail_prise_effet->bail_debut;
                    $date2 = $index['fin'];

                    $diff_in_seconds = strtotime($date2) - strtotime($date1);
                    $diff_in_days = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $date11 = $index['debut'];
                    $date22 = $index['fin'];

                    $diff_in_seconds = strtotime($date22) - strtotime($date11);
                    $days_3month = ($diff_in_seconds / (60 * 60 * 24)) + 1;

                    $end_month = (date("d", strtotime($index['fin']))) + 1;
                    foreach ($index['indexation'] as $key => $value) {
                        $value->indlo_appartement_ht = round((($value->indlo_appartement_ht * $diff_in_days) / $days_3month), 2);
                        $tva_appartement = $value->indlo_appartement_ht * $value->indlo_appartement_tva / 100;
                        $value->indlo_appartement_ttc = $value->indlo_appartement_ht + $tva_appartement;

                        $value->indlo_parking_ht = round((($value->indlo_parking_ht * $diff_in_days) / $days_3month), 2);
                        $tva_parking = $value->indlo_parking_ht * $value->indlo_parking_tva / 100;
                        $value->indlo_parking_ttc = $value->indlo_parking_ht + $tva_parking;

                        $value->indlo_charge_ht = round((($value->indlo_charge_ht * $diff_in_days) / $days_3month), 2);
                        $tva_charge = $value->indlo_charge_ht * $value->indlo_charge_tva / 100;
                        $value->indlo_charge_ttc = $value->indlo_charge_ht + $tva_charge;
                    }
                }

                $combinedArray = [];
                $indexedArray2 = [];

                foreach ($arrayFiltre as $item) {
                    $indexedArray2[$item['nom']] = $item;
                }

                foreach ($indexation as $item1) {
                    if (isset($indexedArray2[$item1['nom']])) {
                        $combinedArray[] = $indexedArray2[$item1['nom']];
                    } else {
                        $combinedArray[] = $item1;
                    }
                }
                $indexation = $combinedArray;
            }

            $indlo_appartement_ht = 0;
            $indlo_appartement_tva = 0;
            $indlo_appartement_ttc = 0;
            $indlo_parking_ht = 0;
            $indlo_parking_tva = 0;
            $indlo_parking_ttc = 0;
            $indlo_charge_ht = 0;
            $indlo_charge_tva = 0;
            $indlo_charge_ttc = 0;
            $data_total_ttc_theorique = array();

            foreach ($indexation as $d) {
                if (!empty($d['indexation'])) {
                    foreach ($d['indexation'] as $index) {
                        $indlo_appartement_ht += $index->indlo_appartement_ht;
                        $indlo_appartement_tva += $index->indlo_appartement_tva;
                        $indlo_appartement_ttc += $index->indlo_appartement_ttc;
                        $indlo_parking_ht += $index->indlo_parking_ht;
                        $indlo_parking_tva += $index->indlo_parking_tva;
                        $indlo_parking_ttc += $index->indlo_parking_ttc;
                        $indlo_charge_ht += $index->indlo_charge_ht;
                        $indlo_charge_tva += $index->indlo_charge_tva;
                        $indlo_charge_ttc += $index->indlo_charge_ttc;
                        $total_ttc_theorique = $index->indlo_appartement_ttc + $index->indlo_parking_ttc - $index->indlo_charge_ttc;
                    }
                } else {
                    $total_ttc_theorique = 0;
                }
                array_push($data_total_ttc_theorique, $total_ttc_theorique);
            }

            $ligne = array(
                "nom" => 'Total',
                'indlo_appartement_ht' => $indlo_appartement_ht,
                'indlo_appartement_tva' => $indlo_appartement_tva,
                'indlo_appartement_ttc' => $indlo_appartement_ttc,
                'indlo_parking_ht' => $indlo_parking_ht,
                'indlo_parking_tva' => $indlo_parking_tva,
                'indlo_parking_ttc' => $indlo_parking_ttc,
                'indlo_charge_ht' => $indlo_charge_ht,
                'indlo_charge_tva' => $indlo_charge_tva,
                'indlo_charge_ttc' => $indlo_charge_ttc,

            );
            $data['indexation_annuel_total_ttc_theorique_par_colonne'] = $data_total_ttc_theorique;
            $data['indexation_annuel_total_ttc_theorique'] = array_sum($data_total_ttc_theorique);
            $data['indexation_annuel_total'] = $ligne;
            $data['indexation_annuel'] = $indexation;
            $data['annee'] = $data_post['year'];
        }
        // mensuel
        else {
            $indexation = array();

            foreach ($list_mois as $key => $value) {
                $indexation[] = array(
                    'debut' => $value['debut'],
                    'fin' => $value['fin'],
                    'nom' => $value['nom'],
                    'indexation' => $this->getindexation($value['fin'], $data_post['bail_id']),
                );
            }

            if (!empty($getacte) && $getacte->acte_date_notarie != NULL &&  date('Y-m-d', strtotime($bail_prise_effet->bail_debut) < date('Y-m-d', strtotime($getacte->acte_date_notarie)))) {
                $date_limite = $getacte->acte_date_notarie;
            } else {
                $date_limite = $bail_prise_effet->bail_debut;
            }

            foreach ($indexation as &$value) {
                $dateFin = new DateTime($value['fin']);
                $dateLimiteObj = new DateTime($date_limite);

                if ($dateLimiteObj > $dateFin) {
                    $value['indexation'] = array();
                }
            }
            unset($value);

            $arrayFiltre = array_filter($indexation, function ($element) use ($bail_prise_effet) {
                $dateDebut = new DateTime($element['debut']);
                $dateFin = new DateTime($element['fin']);
                $dateLimiteObj = new DateTime($bail_prise_effet->bail_debut);
                return ($dateLimiteObj > $dateDebut && $dateLimiteObj <= $dateFin);
            });

            if (!empty($arrayFiltre)) {
                foreach ($arrayFiltre as $key => $index) {
                    $date1 = new DateTime($bail_prise_effet->bail_debut);
                    $date2 = new DateTime($index['fin']);

                    $interval = date_diff($date1, $date2);
                    $diff = (intval($interval->format(' %d '))) + 1;
                    $end_month = (date("d", strtotime($index['fin']))) + 1;
                    foreach ($index['indexation'] as $key => $value) {
                        $value->indlo_appartement_ht = round((($value->indlo_appartement_ht * $diff) / $end_month), 2);
                        $tva_appartement = $value->indlo_appartement_ht * $value->indlo_appartement_tva / 100;
                        $value->indlo_appartement_ttc = $value->indlo_appartement_ht + $tva_appartement;

                        $value->indlo_parking_ht = round((($value->indlo_parking_ht * $diff) / $end_month), 2);
                        $tva_parking = $value->indlo_parking_ht * $value->indlo_parking_tva / 100;
                        $value->indlo_parking_ttc = $value->indlo_parking_ht + $tva_parking;

                        $value->indlo_charge_ht = round((($value->indlo_charge_ht * $diff) / $end_month), 2);
                        $tva_charge = $value->indlo_charge_ht * $value->indlo_charge_tva / 100;
                        $value->indlo_charge_ttc = $value->indlo_charge_ht + $tva_charge;
                    }
                }

                $combinedArray = [];
                $indexedArray2 = [];

                foreach ($arrayFiltre as $item) {
                    $indexedArray2[$item['nom']] = $item;
                }

                foreach ($indexation as $item1) {
                    if (isset($indexedArray2[$item1['nom']])) {
                        $combinedArray[] = $indexedArray2[$item1['nom']];
                    } else {
                        $combinedArray[] = $item1;
                    }
                }
                $indexation = $combinedArray;
            }

            $data['indexation_mensuel'] = $indexation;

            $indlo_appartement_ht = 0;
            $indlo_appartement_tva = 0;
            $indlo_appartement_ttc = 0;
            $indlo_parking_ht = 0;
            $indlo_parking_tva = 0;
            $indlo_parking_ttc = 0;
            $indlo_charge_ht = 0;
            $indlo_charge_tva = 0;
            $indlo_charge_ttc = 0;
            $data_total_ttc_theorique = array();

            foreach ($indexation as $d) {
                if (!empty($d['indexation'])) {
                    foreach ($d['indexation'] as $index) {
                        $indlo_appartement_ht += $index->indlo_appartement_ht;
                        $indlo_appartement_tva += $index->indlo_appartement_tva;
                        $indlo_appartement_ttc += $index->indlo_appartement_ttc;
                        $indlo_parking_ht += $index->indlo_parking_ht;
                        $indlo_parking_tva += $index->indlo_parking_tva;
                        $indlo_parking_ttc += $index->indlo_parking_ttc;
                        $indlo_charge_ht += $index->indlo_charge_ht;
                        $indlo_charge_tva += $index->indlo_charge_tva;
                        $indlo_charge_ttc += $index->indlo_charge_ttc;
                        $total_ttc_theorique = $index->indlo_appartement_ttc + $index->indlo_parking_ttc - $index->indlo_charge_ttc;
                    }
                } else {
                    $total_ttc_theorique = 0;
                }
                array_push($data_total_ttc_theorique, $total_ttc_theorique);
            }

            $ligne = array(
                "nom" => 'Total',
                'indlo_appartement_ht' => $indlo_appartement_ht,
                'indlo_appartement_tva' => $indlo_appartement_tva,
                'indlo_appartement_ttc' => $indlo_appartement_ttc,
                'indlo_parking_ht' => $indlo_parking_ht,
                'indlo_parking_tva' => $indlo_parking_tva,
                'indlo_parking_ttc' => $indlo_parking_ttc,
                'indlo_charge_ht' => $indlo_charge_ht,
                'indlo_charge_tva' => $indlo_charge_tva,
                'indlo_charge_ttc' => $indlo_charge_ttc,
            );

            $data['indexation_mensuel_total_ttc_theorique_par_colonne'] = $data_total_ttc_theorique;
            $data['indexation_mensuel_total_ttc_theorique'] = array_sum($data_total_ttc_theorique);
            $data['indexation_mensuel_total'] = $ligne;

            $data['factu_mensuel'] = $this->FactureLoyer_m->get_mensuel($data_post['bail_id'], $data_post['year']);
        }

        $data['annuel'] = $this->getPeriodeAnnuel();
        $data['mensuel'] = $this->getPeriodeMensuel();
        $data['trimestriel'] = $this->getPeriodeTrimestriel();
        $data['semestriel'] = $this->getPeriodeSemestriel();

        $params = array(
            'clause' => array('c_bail.bail_id ' => $data_post['bail_id']),
            'join' => array(
                'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                'c_periodicite_loyer' => 'c_periodicite_loyer.pdl_id = c_bail.pdl_id',
                'c_nature_prise_effet' => 'c_nature_prise_effet.natef_id = c_bail.natef_id',
                'c_type_bail' => 'c_type_bail.tpba_id = c_bail.tpba_id',
                'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id',
                'c_type_echeance' => 'c_type_echeance.tpech_id = c_bail.tpech_id',
                'c_bail_art' => 'c_bail_art.bart_id = c_bail.bail_art_605',
                // 'c_indice_loyer' => 'c_indice_loyer.indloyer_id = c_bail.indloyer_id',
                'c_moment_facturation_bail' => 'c_moment_facturation_bail.momfac_id = c_bail.momfac_id',
                'c_periode_indexation' => 'c_periode_indexation.prdind_id = c_bail.prdind_id',
            ),
        );

        $this->load->model('IndexationLoyer_m', 'index');

        $indexationLoyerParam = array(
            'clause' => array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1),
        );

        $indexations = $this->IndexationLoyer_m->get($indexationLoyerParam);

        $data['bail_loyer'] = $this->Bail_m->get($params);

        $data['list_full_mois'] = $list_full_mois;
        $data['list_mois'] = $list_mois;
        $data['encaissements'] = $encaissements;
        $data['annee_courant'] = $data_post['year'];
        $data['tva'] = $this->TauxTVA_m->get(array());
        $data['typeProduitLotEncaissement'] = $this->TypeProduitLotEncaissement_m->get(array());
        $data['typeChargeDeductibleLotEncaissement'] = $this->TypeChargeDeductibleLotEncaissement_m->get(array());
        $data['indexations'] = $indexations;

        $data['listAnnuel'] = $this->PeriodeAnnuel_m->get(array());

        $paramFactures = array(
            'clause' => array('c_facture_loyer.bail_id' => $data_post['bail_id'], 'c_facture_loyer.facture_annee' => $data_post['year']),
            'join' => array(
                'c_dossier' => 'c_dossier.dossier_id  = c_facture_loyer.dossier_id'
            ),
        );
        $data['factures'] = $this->FactureLoyer_m->get($paramFactures);

        $bail_facturation_loyer_celaviegestion = NULL;

        foreach ($data['bail_loyer'] as $key => $bail_val) {
            $bail_facturation_loyer_celaviegestion = $bail_val->bail_facturation_loyer_celaviegestion;
        }

        /**** *****/

        // Données de votre exemple
        $data['sommes_encaissements'] = array_map(function ($interval) use ($encaissements) {
            $debut = strtotime($interval["debut"]);
            $fin = strtotime($interval["fin"]);
            $sum = array_reduce($encaissements, function ($carry, $item) use ($debut, $fin) {
                $enc_date = strtotime($item->enc_date);
                $enc_montant = $item->enc_montant;
                if ($enc_date >= $debut && $enc_date <= $fin) {
                    return $carry + $enc_montant;
                }
                return $carry;
            }, 0);

            $interval['somme'] = $sum;
            return $interval;
        }, $indexation);

        echo $bail_facturation_loyer_celaviegestion . "@@@@@@@";
        $this->renderComponant("Clients/clients/lot/loyers/table-facture_encaissement", $data);
    }

    public function getPeriodeMensuel()
    {
        $this->load->model('PeriodeMensuel_m');
        $params = array(
            'columns' => array('periode_mens_id', 'periode_mens_libelle')
        );
        $request = $this->PeriodeMensuel_m->get($params);
        return $request;
    }

    public function getPeriodeAnnuel()
    {
        $this->load->model('PeriodeAnnuel_m');
        $params = array(
            'columns' => array('periode_annee_id', 'periode_annee_libelle')
        );
        $request = $this->PeriodeAnnuel_m->get($params);
        return $request;
    }

    public function getPeriodeSemestriel()
    {
        $this->load->model('PeriodeSemestriel_m');
        $params = array(
            'columns' => array('periode_semestre_id', 'periode_semestre_libelle')
        );
        $request = $this->PeriodeSemestriel_m->get($params);
        return $request;
    }

    public function getPeriodeTrimestriel()
    {
        $this->load->model('PeriodeTrimestriel_m');
        $params = array(
            'columns' => array('periode_timestre_id', 'periode_trimesrte_libelle')
        );
        $request = $this->PeriodeTrimestriel_m->get($params);
        return $request;
    }

    public function AddFactureLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FactureLoyer_m');
                $this->load->model('DocumentLoyer_m');
                $this->load->model('Bail_m', 'bail');

                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout d'indexation du loyer"));

                $data_post = $_POST['data'];

                $param_check_dossier = array(
                    'clause' => array('c_bail.bail_id' => $data_post['bail_id']),
                    'join' => array(
                        'c_lot_client' => 'c_lot_client.cliLot_id = c_bail.cliLot_id',
                        'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id'
                    ),
                );

                $data_bail = $this->bail->get($param_check_dossier);

                $data_retour['request'] = $data_post;

                $data['facture_annee'] = $data_post['fact_annee'];
                $data['fact_loyer_num'] = $data_post['fact_loyer_num'];
                $data['fact_loyer_date'] = $data_post['fact_loyer_date'];
                $data['fact_loyer_app_ht'] = $data_post['fact_loyer_app_ht'];
                $data['fact_loyer_app_tva'] = $data_post['fact_loyer_app_tva'];
                $data['fact_loyer_app_ttc'] = $data_post['fact_loyer_app_ttc'];
                $data['fact_loyer_park_ht'] = $data_post['fact_loyer_park_ht'];
                $data['fact_loyer_park_tva'] = $data_post['fact_loyer_park_tva'];
                $data['fact_loyer_park_ttc'] = $data_post['fact_loyer_park_ttc'];
                $data['fact_loyer_charge_ht'] = $data_post['fact_loyer_charge_ht'];
                $data['fact_loyer_charge_tva'] = $data_post['fact_loyer_charge_tva'];
                $data['fact_loyer_charge_ttc'] = $data_post['fact_loyer_charge_ttc'];
                $data['facture_commentaire'] = $data_post['facture_commentaire'];
                $data['mode_paiement'] = $data_post['mode_paiement'];
                $data['echeance_paiement'] = $data_post['echeance_paiement'];
                $data['emetteur_nom'] = $data_post['emetteur_nom'];
                $data['emet_adress1'] = isset($data_post['emetteur_nom']) ? $data_post['emetteur_nom'] : NULL;
                $data['emet_adress2'] = isset($data_post['emet_adress2']) ? $data_post['emet_adress2'] : NULL;
                $data['emet_adress3'] = isset($data_post['emet_adress3']) ? $data_post['emet_adress3'] : NULL;
                $data['emet_cp'] = isset($data_post['emet_cp']) ? $data_post['emet_cp'] : NULL;
                $data['emet_pays'] = isset($data_post['emet_pays']) ? $data_post['emet_pays'] : NULL;
                $data['emet_ville'] = isset($data_post['emet_ville']) ? $data_post['emet_ville'] : NULL;
                $data['dest_nom'] = $data_post['dest_nom'];
                $data['dest_adresse1'] = isset($data_post['dest_adresse1']) ? $data_post['dest_adresse1'] : NULL;
                $data['dest_adresse2'] = isset($data_post['dest_adresse2']) ? $data_post['dest_adresse2'] : NULL;
                $data['dest_adresse3'] = isset($data_post['dest_adresse3']) ? $data_post['dest_adresse3'] : NULL;
                $data['dest_cp'] = isset($data_post['dest_cp']) ? $data_post['dest_cp'] : NULL;
                $data['dest_pays'] = isset($data_post['dest_pays']) ? $data_post['dest_pays'] : NULL;
                $data['dest_ville'] = isset($data_post['dest_ville']) ? $data_post['dest_ville'] : NULL;
                $data['dossier_id'] = $data_bail[0]->dossier_id;
                $data['facture_nombre'] = 1;
                $data['bail_id'] = $data_post['bail_id'];

                if (isset($data_post['periode_timestre_id'])) {
                    $data['periode_timestre_id'] = $data_post['periode_timestre_id'];
                    unset($data['periode_semestre_id']);
                    unset($data['periode_annee_id']);
                    unset($data['periode_mens_id ']);
                } elseif (isset($data_post['periode_semestre_id'])) {
                    $data['periode_semestre_id'] = $data_post['periode_semestre_id'];
                    unset($data['periode_timestre_id']);
                    unset($data['periode_annee_id']);
                    unset($data['periode_mens_id ']);
                } elseif (isset($data_post['periode_annee_id'])) {
                    $data['periode_annee_id'] = $data_post['periode_annee_id'];
                    unset($data['periode_timestre_id']);
                    unset($data['periode_semestre_id']);
                    unset($data['periode_mens_id ']);
                } elseif (isset($data_post['periode_mens_id'])) {
                    $data['periode_mens_id'] = $data_post['periode_mens_id'];
                    unset($data['periode_timestre_id']);
                    unset($data['periode_semestre_id']);
                    unset($data['periode_annee_id ']);
                }

                if (isset($data_post['fact_loyer_id']) && $data_post['fact_loyer_id'] !== null && $data_post['fact_loyer_id'] !== "") {
                    $clause = array("fact_loyer_id" => $data_post['fact_loyer_id']);
                    $request = $this->FactureLoyer_m->save_data($data, $clause);
                    $clause = array('fact_loyer_id ' => $data_post['fact_loyer_id']);
                    $this->DocumentLoyer_m->delete_data($clause);
                } else {
                    $request = $this->FactureLoyer_m->save_data($data);
                    $data_retour['fact_loyer_id'] = $this->FactureLoyer_m->getInsertID();
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement de franchise loyer du bail réussi"));
                }

                echo json_encode($retour);
            }
        }
    }


    public function AddEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Encaissement_m');

                $retour = retour(false, "error", 0, array("message" => "Erreur dans ajout d'encaissement"));

                $data_post = $_POST['data'];

                $enc_id = null;

                $data_retour['request'] = $data_post;

                $data['enc_date'] = $data_post['enc_date'];
                $data['enc_periode'] = $data_post['enc_periode'];
                $data['enc_annee'] = $data_post['enc_annee'];
                $data['enc_montant'] = $data_post['enc_montant'];
                $data['enc_mode'] = $data_post['enc_mode'];
                $data['bail_id'] = $data_post['bail_id'];
                $enc_id = $data_post['enc_id'] ? intval($data_post['enc_id']) : null;
                $data['enc_app_ttc'] = $data_post['enc_app_ttc'];
                $data['enc_park_ttc'] = $data_post['enc_park_ttc'];
                $data['enc_charge_ttc'] = $data_post['enc_charge_ttc'];
                $data['enc_charge_deductible_ttc'] = $data_post['enc_charge_deductible_ttc'];
                $data['enc_produit_ttc'] = $data_post['enc_produit_ttc'];
                $data['enc_total_ttc'] = $data_post['enc_total_ttc'];
                $data['enc_app_tva'] = $data_post['enc_app_tva'];
                $data['enc_park_tva'] = $data_post['enc_park_tva'];
                $data['enc_charge_tva'] = $data_post['enc_charge_tva'];
                $data['enc_charge_deductible_tva'] = $data_post['enc_charge_deductible_tva'];
                $data['enc_produit_tva'] = $data_post['enc_produit_tva'];
                $data['enc_app_montant_tva'] = $data_post['enc_app_montant_tva'];
                $data['enc_park_montant_tva'] = $data_post['enc_park_montant_tva'];
                $data['enc_charge_montant_tva'] = $data_post['enc_charge_montant_tva'];
                $data['enc_charge_deductible_montant_tva'] = $data_post['enc_charge_deductible_montant_tva'];
                $data['enc_produit_montant_tva'] = $data_post['enc_produit_montant_tva'];
                $data['enc_total_montant_tva'] = $data_post['enc_total_montant_tva'];
                $data['enc_app_ht'] = $data_post['enc_app_ht'];
                $data['enc_park_ht'] = $data_post['enc_park_ht'];
                $data['enc_charge_ht'] = $data_post['enc_charge_ht'];
                $data['enc_charge_deductible_ht'] = $data_post['enc_charge_deductible_ht'];
                $data['enc_produit_ht'] = $data_post['enc_produit_ht'];
                $data['enc_total_ht'] = $data_post['enc_total_ht'];
                $data['enc_commentaire'] = $data_post['enc_commentaire'];
                $data['tpchrg_deduct_id'] = $data_post['tpchrg_deduct_id'];
                $data['tpchrg_prod_id'] = $data_post['tpchrg_prod_id'];

                if ($enc_id !== null) {
                    $clause = array("enc_id" => $enc_id);
                    $request = $this->Encaissement_m->save_data($data, $clause);
                } else {
                    $request = $this->Encaissement_m->save_data($data);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => "Enregistrement d'encaissement reçue"));
                }

                echo json_encode($retour);
            }
        }
    }

    public function GetDetailEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Encaissement_m');
                $this->load->model('Bail_m');
                $this->load->helper("loyer");

                $data_post = $_POST['data'];

                $encaissementParams = array(
                    'clause' => array(
                        'bail_id' => $data_post['bail_id'],
                        'enc_annee' => $data_post['enc_annee'],
                        'enc_periode' => $data_post['enc_periode']
                    ),
                    'join' => array(
                        'c_type_produit_lot_encaissement' => 'c_type_produit_lot_encaissement.tpchrg_prod_id = c_encaissement.tpchrg_prod_id',
                        'c_type_charge_deductible_lot_encaissement' => 'c_type_charge_deductible_lot_encaissement.tpchrg_deduct_id  = c_encaissement.tpchrg_deduct_id'
                    ),
                );
                $detail_encaissement = $this->Encaissement_m->get($encaissementParams);

                $bailParam = array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => 'row'
                );

                $bail = $this->Bail_m->get($bailParam);

                $data['detail_encaissement'] = $detail_encaissement;
                $data['bail'] = $bail;
                $data['enc_annee'] = $data_post['enc_annee'];
                $data['enc_periode'] = $data_post['enc_periode'];

                $list_full_mois = genererListeMoisDansAnnee($data_post['enc_annee'], true);
                $data['list_full_mois'] = $list_full_mois;

                $this->renderComponant("Clients/clients/lot/loyers/list-detail-encaissement", $data);
            }
        }
    }

    public function GetDetailFacture()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('FactureLoyer_m');
                $this->load->model('Bail_m');
                $data_post = $_POST['data'];
                $params = array();

                $periode = $data_post['periode'];
                $params = array(
                    'clause' => array(
                        'bail_id' => $data_post['bail_id'],
                        'facture_annee' => $data_post['facture_annee'],
                        $periode => $data_post['periode_id']
                    )
                );

                $bailParam = array(
                    'clause' => array('bail_id' => $data_post['bail_id']),
                    'method' => 'row'
                );

                $bail = $this->Bail_m->get($bailParam);

                $factures = $this->FactureLoyer_m->get($params);

                $data['factures'] = $factures;
                $data['requests'] = $data_post;
                $data['bail'] = $bail;

                $this->renderComponant("Clients/clients/lot/loyers/list-detail-facture", $data);
            }
        }
    }

    public function removeEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('Encaissement_m');

                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('enc_id' => $data['enc_id']),
                    'method' => "row",
                );

                $request_get = $this->Encaissement_m->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['enc_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer l'encaissement " . $request_get->enc_periode . " " . $request_get->enc_annee . "?",
                            'btnConfirm' => "Supprimer l'encaissement",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('enc_id' => $data['enc_id']);
                    $request = $this->Encaissement_m->delete_data($clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['enc_id']),
                            'message' => "L'encaissement " . $request_get->enc_periode . " " . $request_get->enc_annee . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function DeleteFactureLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de l'suppression…",
                );
                $this->load->model('FactureLoyer_m');
                $this->load->model('DocumentLoyer_m');

                $data = decrypt_service($_POST["data"]);
                $params = array(
                    'clause' => array('fact_loyer_id' => $data['fact_id']),
                    'method' => "row",
                );

                $request_get = $this->FactureLoyer_m->get($params);

                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['fact_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer la facture " . $request_get->fact_loyer_num . " ?",
                            'btnConfirm' => "Supprimer la facture",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('fact_loyer_id ' => $data['fact_id']);
                    $this->DocumentLoyer_m->delete_data($clause);
                    $request = $this->FactureLoyer_m->delete_data($clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('id' => $data['fact_id']),
                            'message' => "La facture " . $request_get->fact_loyer_num . " a bien été supprimé",
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }



    public function getIndexationValue()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('IndexationLoyer_m');

                $data_post = $_POST['data'];
                $data_retour['request'] = $data_post;
                $indexationLoyerParam = array(
                    'clause' => array('bail_id ' => $data_post['bail_id'], 'etat_indexation' => 1),
                );
                $indexations = $this->IndexationLoyer_m->get($indexationLoyerParam);
                $data_retour['indexations'] = $indexations;
                $retour = retour(true, "success", $data_retour, array("message" => "Get indexation value"));
                echo json_encode($retour);
            }
        }
    }

    public function deleteDocumentLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => "Des erreurs ont été détectées lors de la suppression…",
                );
                $this->load->model('DocumentLoyer_m');
                $data = decrypt_service($_POST["data"]);
                if ($data['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'id' => $data['doc_loyer_id'],
                            'title' => "Confirmation de la suppression",
                            'text' => "Voulez-vous vraiment supprimer ce document du loyer ?",
                            'btnConfirm' => "Supprimer le document",
                            'btnAnnuler' => "Annuler",
                            'doc_loyer_id' => $data['doc_loyer_id']
                        ),
                        'message' => "",
                    );
                } else if ($data['action'] == "confirm") {
                    $clause = array('doc_loyer_id' => $data['doc_loyer_id']);
                    $data_update = array('doc_loyer_etat' => 0);
                    $request = $this->DocumentLoyer_m->save_data($data_update, $clause);
                    if ($request) {
                        $retour = array(
                            'status' => 200,
                            'action' => $data['action'],
                            'data' => array('doc_loyer_id' => $data['doc_loyer_id']),
                            'message' => "document supprimé"
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function FormUpdateDocLoyer()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data = array();
                $data['doc_loyer_id'] = $data_post['doc_loyer_id'];

                $this->load->model('DocumentLoyer_m');
                $data['document'] = $this->DocumentLoyer_m->get(
                    array('clause' => array("doc_loyer_id" => $data_post['doc_loyer_id']), 'method' => "row")
                );

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee_loyer'] = $data_post['annee_loyer'];
            }

            $this->renderComponant("Clients/clients/lot/loyers/formUpdateDocument", $data);
        }
    }

    public function UpdateDocument()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentLoyer_m');

                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("cliLot_id" => $data['cliLot_id']);

                    $clause = array("doc_loyer_id" => $data['doc_loyer_id']);
                    $request = $this->DocumentLoyer_m->save_data($data, $clause);
                    if (!empty($request)) {
                        $retour = retour(true, "success", $data_retour, array("message" => "Modification reussie."));
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
