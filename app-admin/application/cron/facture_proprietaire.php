<?php 

require_once  __DIR__ .'/inc/db.php';

/***** *****/

require_once  __DIR__ .'/libraries/DbConnector.php';
require_once  __DIR__ .'/libraries/DbRequest.php';
require_once  __DIR__ .'/libraries/MailjetService.php';

$databaseConnection = new DbConnector(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
$dbRequest = new DbRequest($databaseConnection);

getFacture_proprietaire($dbRequest);

function getFacture_proprietaire($dbRequest)
{
    $params_lot = array(
        'distinct' => true,
        'clause' => array('c_facture_automatique.etat_proprietaire' => 0),
        'join' => array(
            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_facture_automatique.gestionaire_id',
            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_automatique.cliLot_id',
            'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
            'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
            'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_gestionnaire.gestionnaire_id',
        ),
        'columns' =>  array(
            'fact_auto_id',
            'c_facture_automatique.cliLot_id',
            'c_facture_automatique.gestionaire_id',
            'gestionnaire_nom',
            'cnctGestionnaire_email1',
            'cnctGestionnaire_email2',
            'cliLot_num',
            'cliLot_mode_paiement',
            'cliLot_encaiss_loyer',
            'cliLot_iban_client',
            'cliLot_bic_client',
            'c_dossier.dossier_id',
            'c_dossier.util_id',
            'client_nom',
            'client_prenom',
            'client_email',
            'client_email1',
            'progm_nom',
            'progm_adresse1',
            'progm_ville',
            'progm_cp',
            'progm_pays',
        )
    );
    // liste lot
    $request =  $dbRequest->select('c_facture_automatique', $params_lot);

    // all document loyer (facture)
    $params_fac = array(
        'clause' => array(
            'c_facture_automatique.etat_proprietaire' => 0,
        ),
        'join' => array(
            'c_document_loyer' => 'c_document_loyer.fact_loyer_id = c_facture_automatique.fact_loyer_id',
        ),
        'columns' =>  array(
            'c_facture_automatique.cliLot_id',
            'c_document_loyer.fact_loyer_id',
            'doc_loyer_path',
            'doc_loyer_nom',
            'doc_loyer_annee',
        )
    );
    $allfac =  $dbRequest->select('c_facture_automatique', $params_fac);

    // charger clientelle
    $allChargeClient = getUtilisateur($dbRequest);
    foreach ($request as &$data_fac) {
        $util_id = (!empty($data_fac['util_id'])) ? $data_fac['util_id'] : 2;
        $filtre_chargeClientel = array_filter($allChargeClient, function ($clientel) use ($util_id) {
            return $clientel['util_id'] == $util_id;
        });
        $chargeClientel = array_values($filtre_chargeClientel);
        $data_fac['charge_clientel'] = $chargeClientel[0];

        $cliLot_id = $data_fac['cliLot_id'];
        $filter_doc_loyer = array_filter($allfac, function ($doc) use ($cliLot_id) {
            return $doc['cliLot_id'] == $cliLot_id;
        });
        $data_fac['documents_facture'] = array_values($filter_doc_loyer);
    }

    // contenu 
    $contenu = getMailContent($dbRequest);
    // preparer donner

    $formatData = prepareMailjetParameters($request, $contenu);
    $segments_data = segmenterDonnees($formatData);

    foreach ($segments_data as $key => $segment) {
        $valeursFactAutoId = [];
        $nouveauSegment = array_map(function ($item) use (&$valeursFactAutoId) {
            $valeursFactAutoId[] = $item["fact_auto_id"];
            unset($item["fact_auto_id"]);
            return $item;
        }, $segment);

        $data_exportJson = array(
            'segment' => $key,
            'fact_auto_id' => $valeursFactAutoId,
        );

        $sendEmail_segment = sendEmailViaMailjet($nouveauSegment);
        if (!empty($sendEmail_segment) && isset($sendEmail_segment['success']) && $sendEmail_segment['success'] == true) {
            $request_statusFactAuto = updateData_factAuto($valeursFactAutoId, $dbRequest);
            if (!$request_statusFactAuto){
                $data_exportJson['mailjet'] = "L'envoi des données vers Mailjet a été accompli avec succès.";
                $data_exportJson['bd'] = 'Update status etat dans table fact_automatique error';
            }
        }else{
            $data_exportJson['mailjet'] = "L'envoi des données vers Mailjet a rencontré un problème.";
            $data_exportJson['bd'] = 'Update status etat dans table fact_automatique pas encore executé';
        }
        exportToJSON($data_exportJson,"Log_mailProprietaire_Send_Facture_".date('Y-m-d')."_segement_".$key);
    }
}

function updateData_factAuto($Array_factAutoId,$dbRequest){
    $factAutoId_update = [];
    foreach ($Array_factAutoId as $value) {
        $factAutoId_update[] = ' fact_auto_id  = ' . $value . ' THEN 1 ';
    }
    $clause_set = implode(' WHEN ', $factAutoId_update);
    $clause_where = '(' . implode(',', $Array_factAutoId) . ')';
    $query = "UPDATE c_facture_automatique SET etat_proprietaire = CASE WHEN $clause_set ELSE etat_proprietaire END WHERE fact_auto_id IN  $clause_where;";
    return $dbRequest->_executeRequest($query);
}

function segmenterDonnees($data) {
    $totalObjets = count($data);
    $objetsParSegment = 25;
    $nombreSegments = ceil($totalObjets / $objetsParSegment);
    $dataSegments = [];
    for ($i = 0; $i < $nombreSegments; $i++) {
        $debut = $i * $objetsParSegment;
        $fin = ($i + 1) * $objetsParSegment - 1;
        $segment = array_slice($data, $debut, $objetsParSegment);
        $dataSegments[] = $segment;
    }
    return $dataSegments;
}

function exportToJSON($data,$fichier) {
    $directoryPath = './logs/';
    if (!is_dir($directoryPath)) {
        mkdir($directoryPath, 0777, true);
    }
    $jsonFilePath = $directoryPath . "$fichier.json";
    $fileHandle = fopen($jsonFilePath, 'w');
    if ($fileHandle !== false) {
        $jsonChunk = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        fwrite($fileHandle, $jsonChunk . PHP_EOL);
        fclose($fileHandle);
    }
}


/***********  *********/

function getUtilisateur($dbRequest)
{
    $request =  $dbRequest->select(
        'c_utilisateur',
        array(
            'columns' => array('util_nom', 'util_prenom', 'util_mail', 'util_id')
        )
    );
    return $request;
}

/***********  *********/

function getMailContent($dbRequest)
{
    $params_get = array(
        'clause' => array('pmail_id ' => 20),
        'method' => "row"
    );
    $request =  $dbRequest->select('c_param_modelemail', $params_get);
    return $request;
}

/********* ********/

function sendEmailViaMailjet($mailjetParams)
{
    $mailjet = new MailjetService();
    $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
    $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
    return $response;
}

function prepareMailjetParameters($data, $content)
{
    $mailjetParams = [];
    foreach ($data as $recipient) {
        $array_adresseMaildestinataire = [];
        
        if(isEmailValid(trim($recipient['client_email']))){
            $adresse_mail = [
                'Email' => trim($recipient['client_email']),
                'Name' => $recipient['client_nom'].' '.$recipient['client_prenom']
            ];
            $array_adresseMaildestinataire[] = $adresse_mail;
        }

        if(isEmailValid(trim($recipient['cnctGestionnaire_email2']))){
            if (!empty(trim($recipient['cnctGestionnaire_email2']))) {
                $adresse_mail1 = [
                    'Email' => trim($recipient['client_email1']),
                    'Name' => $recipient['client_nom'].' '.$recipient['client_prenom']
                ];
                $array_adresseMaildestinataire[] = $adresse_mail1;
            }
        }

        if(!empty($array_adresseMaildestinataire)){
            $param = [
                'From' => [
                    'Email' => $recipient['charge_clientel']['util_mail'],
                    'Name' => $recipient['charge_clientel']['util_nom'] . ' ' . $recipient['charge_clientel']['util_prenom'],
                ],
                'To' => $array_adresseMaildestinataire,
                'Subject' => $content['pmail_libelle'] . ' de la résidence ' . $recipient['progm_nom']
            ];

            $contenu = $content['pmail_contenu'];
            $contenu = str_replace('@nom_client', $recipient['client_nom'], $contenu);
            $contenu = str_replace('@prenom_client', $recipient['client_prenom'], $contenu);
            $contenu = str_replace('@nom_programme', $recipient['progm_nom'], $contenu);
            $contenu = str_replace('@date_envoi_mail', date('d/m/Y'), $contenu);

            $param['HTMLPart'] = '<p>' . $contenu . '</p>';
            $param['TextPart'] = $contenu;

            $formattedAttachments = [];
            if (isset($recipient['documents_facture']) && !empty($recipient['documents_facture'])) {
                foreach ($recipient['documents_facture'] as $attachment) {
                    $file_path = __DIR__.'/../../' . $attachment['doc_loyer_path'];
                    if (file_exists($file_path)) {
                        if (ini_get('allow_url_fopen')) {
                            $formattedAttachment = [
                                'ContentType' => mime_content_type($file_path),
                                'Filename' => basename($file_path),
                                'Base64Content' => base64_encode(file_get_contents($file_path)),
                            ];
                            $formattedAttachments[] = $formattedAttachment;
                        }
                    }
                }
            }

            $param['fact_auto_id'] =  $recipient['fact_auto_id'];
            $param['Attachments'] = $formattedAttachments;
            $mailjetParams[] = $param;
        }
    }
    return $mailjetParams;
}


function isEmailValid($email = '') {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}