<?php
include './inc/db.php';
include './func/model_clos_sans_suite.php';

if (function_exists('get_mandat_envoyer')) {
    $mandat_envoyer = executeQuery(get_mandat_envoyer(), $pdo);

    if (!empty($mandat_envoyer)) {
        foreach ($mandat_envoyer as $key => $value) {
            $date1 = $value['mandat_date_envoi'];
            $date2 = date('Y-m-d');
            $datetime1 = new DateTime($date1);
            $datetime2 = new DateTime($date2);

            $interval = $datetime1->diff($datetime2);

            $days = $interval->days;

            if ($days > 21) {
                updatetQuery(updateMandat($value['mandat_id']), $pdo);
            }
        }
    }
}
