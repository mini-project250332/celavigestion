<?php 

require_once  __DIR__ .'/inc/db.php';
require_once  __DIR__ .'/func/facturLoyer.php';
require_once  __DIR__ .'/mpdf/Mpdf.php';

require_once  __DIR__ .'/libraries/DbConnector.php';
require_once  __DIR__ .'/libraries/DbRequest.php';
require_once  __DIR__ .'/libraries/MailjetService.php';


function sendEmailViaMailjet($mailjetParams)
{
    $mailjet = new MailjetService();
    $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
    $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
    return $response;
}


function segmenterDonnees($data) {
    $totalObjets = count($data);
    
    $objetsParSegment = 25;
    $nombreSegments = ceil($totalObjets / $objetsParSegment);

    $dataSegments = [];
    for ($i = 0; $i < $nombreSegments; $i++) {
        $debut = $i * $objetsParSegment;
        $fin = ($i + 1) * $objetsParSegment - 1;
        $segment = array_slice($data, $debut, $objetsParSegment);
        $dataSegments[] = $segment;
    }
    return $dataSegments;
}

