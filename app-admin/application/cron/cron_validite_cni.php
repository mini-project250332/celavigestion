<?php

include './inc/db.php';
include './func/validite_cni.php';

require_once '../third_party/PHPMailer/PHPMailer.php';
require_once '../third_party/PHPMailer/SMTP.php';
require_once '../third_party/PHPMailer/Exception.php';

if (function_exists('getClient')) {
    $client = executeQuery(getClient(), $pdo);
}

if (!empty($client)) {
    foreach ($client as $key => $value) {
        $numModelMail = $value['client_pays'] == 'FRANCE' || $value['client_pays'] == 'France' || $value['client_pays'] == 'france' ? 17 : 18;
        $modelMail  = executeQuery(getModel($numModelMail), $pdo);

        $check_dossier = executeQuery(verifierDossier($value['client_id']), $pdo);
        $check_lot_mandat = executeQuery(verifierLotMandat($value['client_id']), $pdo);

        if (!empty($check_dossier) && !empty($check_lot_mandat)) {
            $contenu = str_replace('@nom_client', $value['client_nom'], $modelMail[0]['pmail_contenu']);
            $contenu = str_replace('@prenom_client', $value['client_prenom'], $contenu);
            $contenu = str_replace('@charge_client', $value['util_nom'] . ' ' . $value['util_prenom'], $contenu);


            $data['client_id'] = $value['client_id'];
            $data['util_id'] = $value['util_id'];
            $data['comclient_texte'] = $contenu;

            $data['contenu_mail'] = nl2br($contenu);
            $data['object_mail'] = $modelMail[0]['pmail_objet'];
            $data['destinataire'] = trim($value['client_email']);

            $data['user_label'] = $value['util_nom'] . ' ' . $value['util_prenom'];
            $data['user'] = $value['mess_email'];
            $data['pwd'] = $value['mess_motdepasse'];
            $data['port'] = $value['mess_port'];
            $data['auth'] = true;
            $data['host'] = $value['mess_adressemailserveur'];

            EnvoiMail($data, $pdo);
        }
    }
}

function EnvoiMail($data, $pdo)
{
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    try {
        $mail->isSMTP();
        $mail->Host = $data['host'];
        $mail->SMTPAuth = $data['auth'];
        $mail->Username = $data['user'];
        $mail->Password = $data['pwd'];
        $mail->SMTPSecure = 'tls';
        $mail->Port = $data['port'];
        $mail->CharSet = 'UTF-8';
        $mail->setFrom($data['user'], $data['user_label']);
        $mail->addAddress(trim($data['destinataire']));

        $mail->isHTML(true);
        $mail->Subject = $data['object_mail'];
        $mail->Body = $data['contenu_mail'];

        if (!$mail->send()) {
            echo 'Error: ' . $mail->ErrorInfo;
        } else {
            insertQuery(insertcommunication($data), $pdo);
        }
        return false;
    } catch (Exception $e) {
        echo 'Error: ' . $e;
    }
}
