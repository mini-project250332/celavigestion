<?php
// database configuration
define('DB_HOST', 'localhost');
define('DB_NAME', 'c_estlaviedb');
define('DB_USER', 'appuser');
define('DB_PASSWORD', '2G3Uq-ywV[}8');
// define('DB_USER', 'root');
// define('DB_PASSWORD', '');

define('API_KEY_MAILJET', '6b1498a8e62030bbdc29e64b3b6aab09');
define('API_SECRET_MAILJET', 'd00f453b664904ab07bd66a98f948bd7');

// create PDO instance
try {
    $pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

function executeQuery($query, $pdo)
{
    try {
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage() . ' executer ' . $query);
    }
}

function deleteQuery($query, $pdo)
{
    try {
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        return 'success';
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage() . 'delete ' . $query);
    }
}

function insertQuery($query, $pdo)
{
    try {
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        return 'success';
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage() . ' insert ' . $query);
    }
}

function updatetQuery($query, $pdo)
{
    try {
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        return 'success';
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage() . ' update ' . $query);
    }
}
