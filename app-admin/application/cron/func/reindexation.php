<?php

function get_Indice_valide()
{
    $query = "SELECT * FROM c_cron_reindex_indice WHERE etat_reindex = 0";
    return $query;
}

function  getListeBailActif($indloyer_id)
{
    $query = "SELECT * FROM c_lot_client 
    INNER JOIN c_bail ON c_lot_client.cliLot_id = c_bail.cliLot_id
    INNER JOIN c_dossier ON c_dossier.dossier_id = c_lot_client.dossier_id
    WHERE c_bail.indloyer_id = $indloyer_id 
    AND c_lot_client.cliLot_etat = 1 
    AND c_dossier.dossier_etat = 1
    AND c_bail.bail_cloture = 0";
    return $query;
}

function getLast_indice_valeur($indloyer_id)
{
    $query = "SELECT * FROM c_indice_loyer
    INNER JOIN c_indice_valeur_loyer ON c_indice_valeur_loyer.indloyer_id = c_indice_loyer.indloyer_id
    WHERE c_indice_loyer.indloyer_id = $indloyer_id AND c_indice_valeur_loyer.indval_valide = 1
    ORDER BY c_indice_valeur_loyer.indval_annee DESC , c_indice_valeur_loyer.indval_trimestre DESC
    LIMIT 1";
    return $query;
}

function getIndexationLoyerQuery($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id AND indlo_ref_loyer = 0
    ORDER BY indlo_id DESC
    LIMIT 1";
    return $query;
}

function insertlotIndexe($data)
{
    $bail_id = $data['bail_id'];
    $indval_id = $data['indval_id'];
    $query = "INSERT INTO c_lot_indexe (bail_id, indval_id) VALUES ($bail_id, $indval_id)";
    return $query;
}

function delete_indexation($bail_id)
{
    $query = "DELETE FROM c_indexation_loyer WHERE bail_id = $bail_id AND etat_indexation = 1 AND indlo_ref_loyer = 0";
    return $query;
}

function getBail($bail_id)
{
    $query = "SELECT * FROM c_bail 
    WHERE bail_id = $bail_id";
    return $query;
}

function getLastIndexationQuery($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id 
    AND etat_indexation = 1
    ORDER BY indlo_id DESC
    LIMIT 1";
    return $query;
}

function getIndice($indloyer_id)
{
    $query = "SELECT * FROM c_indice_loyer WHERE indloyer_id = $indloyer_id";
    return $query;
}

function getIndexationLoyerById($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id
    AND etat_indexation = 1
    AND indlo_indince_non_publie = 0
    ORDER BY indlo_id ASC";
    return $query;
}

function checkIndicefixe($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id 
    AND etat_indexation = 1
    AND indlo_ref_loyer = 0";
    return $query;
}

function fixeindicebase($annee, $indloyer_id)
{
    $query = "SELECT * FROM c_indice_valeur_loyer 
    WHERE indval_valide = 1 
    AND indloyer_id = $indloyer_id
    AND indval_annee = $annee
    LIMIT 1";
    return $query;
}

function getIndiceValeurBail($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    INNER JOIN c_indice_valeur_loyer ON c_indexation_loyer.indlo_indice_base = c_indice_valeur_loyer.indval_id
    WHERE c_indexation_loyer.bail_id = $bail_id
    AND c_indexation_loyer.indlo_ref_loyer = 1
    LIMIT 1";
    return $query;
}

function getIndiceValuer($indval_id)
{
    $query = "SELECT * FROM c_indice_valeur_loyer 
    WHERE indval_id = $indval_id
    AND indval_valide = 1";
    return $query;
}

function getIndiceReferenceAnnuelleQuery($indloyer_id, $annee)
{
    $query = "SELECT * FROM c_indice_valeur_loyer 
    WHERE indloyer_id = $indloyer_id
    AND indval_annee = $annee
    AND indval_valide = 1";
    return $query;
}

function getIndiceReferenceTrimestrielleQuery($indloyer_id, $annee, $indval_trimestre)
{
    $query = "SELECT * FROM c_indice_valeur_loyer 
    WHERE indloyer_id = $indloyer_id
    AND indval_annee = $annee
    AND indval_valide = 1
    AND indval_trimestre = '" . $indval_trimestre . "'";
    return $query;
}

function getSingleIndexation($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id
    AND indlo_ref_loyer = 1";
    return $query;
}

function getNewreference($indloyer_id, $indval_annee, $indval_trimestre)
{
    $query = "SELECT * FROM `c_indice_valeur_loyer` 
    WHERE indloyer_id = $indloyer_id 
    AND indval_annee = ($indval_annee + 1) 
    AND indval_valide = 1
    AND indval_trimestre = '" . $indval_trimestre . "'";
    return $query;
}

function getIndiceValeurPlafonne($indloyer_id, $indval_annee, $indval_trimestre = null)
{
    if ($indval_trimestre == null) {
        $query = "SELECT * FROM `c_indice_valeur_loyer` 
        WHERE indloyer_id = $indloyer_id 
        AND indval_annee = $indval_annee 
        AND indval_valide = 1";
    } else {
        $query = "SELECT * FROM `c_indice_valeur_loyer` 
        WHERE indloyer_id = $indloyer_id 
        AND indval_annee = $indval_annee 
        AND indval_valide = 1
        AND indval_trimestre = '" . $indval_trimestre . "'";
    }

    return $query;
}

function updateCron_indexe($data, $cron_index_id)
{
    $etat_reindex = $data['etat_reindex'];
    $date_reindex = $data['date_reindex'];
    $query = "UPDATE c_cron_reindex_indice SET etat_reindex = $etat_reindex , date_reindex = '" . $date_reindex . "' WHERE cron_index_id = $cron_index_id";
    return $query;
}

function maxIndice($indloyer_id)
{
    $query = "SELECT * FROM `c_indice_valeur_loyer` 
    WHERE indloyer_id = $indloyer_id 
    ORDER BY indval_annee 
    DESC LIMIT 1";
    return $query;
}

function maxIndiceFixe($indloyer_id)
{
    $query = "SELECT * FROM `c_indice_valeur_loyer` 
    WHERE indloyer_id = $indloyer_id 
    ORDER BY indval_annee DESC
    LIMIT 1";
    return $query;
}

function update_where_date($indlo_debut, $bail_id)
{
    $query = "UPDATE c_indexation_loyer
    SET indlo_indince_non_publie = 2
    WHERE bail_id = $bail_id AND indlo_debut < '" . $indlo_debut . "' ;";
    return $query;
}

function delete_where_index_date($indlo_debut, $bail_id)
{
    $query = "DELETE FROM c_indexation_loyer WHERE bail_id = $bail_id AND indlo_debut = '" . $indlo_debut . "'";
    return $query;
}

function getclientQuery($client_id)
{
    $query = "SELECT * FROM `c_client` 
    WHERE client_id = $client_id";
    return $query;
}

function insertIndexatons($data)
{
    $bail_id = $data['bail_id'];
    $etat_indexation = $data['etat_indexation'];
    $indlo_indice_base = $data['indlo_indice_base'];
    $indlo_indice_reference = $data['indlo_indice_reference'];
    $indlo_ref_loyer = $data['indlo_ref_loyer'];
    $indlo_variation_appliquee = $data['indlo_variation_appliquee'];
    $indlo_variation_loyer = $data['indlo_variation_loyer'];
    $indlo_appartement_ht = $data['indlo_appartement_ht'];
    $indlo_parking_ht = $data['indlo_parking_ht'];
    $indlo_charge_ht = $data['indlo_charge_ht'];
    $indlo_appartement_tva = $data['indlo_appartement_tva'];
    $indlo_parking_tva = $data['indlo_parking_tva'];
    $indlo_charge_tva = $data['indlo_charge_tva'];
    $indlo_appartement_ttc = $data['indlo_appartement_ttc'];
    $indlo_parking_ttc = $data['indlo_parking_ttc'];
    $indlo_charge_ttc = $data['indlo_charge_ttc'];
    $indlo_debut = $data['indlo_debut'];
    $indlo_fin = $data['indlo_fin'];
    $query = "INSERT INTO c_indexation_loyer (bail_id, etat_indexation, indlo_indice_base, indlo_indice_reference, indlo_ref_loyer, indlo_variation_appliquee, indlo_variation_loyer, indlo_appartement_ht, indlo_parking_ht, indlo_charge_ht, indlo_appartement_tva, indlo_parking_tva, indlo_charge_tva, indlo_appartement_ttc, indlo_parking_ttc, indlo_charge_ttc, indlo_debut, indlo_fin) 
    VALUES ($bail_id, $etat_indexation, $indlo_indice_base, $indlo_indice_reference, $indlo_ref_loyer, $indlo_variation_appliquee, $indlo_variation_loyer, $indlo_appartement_ht, $indlo_parking_ht, $indlo_charge_ht, $indlo_appartement_tva, $indlo_parking_tva, $indlo_charge_tva, $indlo_appartement_ttc, $indlo_parking_ttc, $indlo_charge_ttc, '" . $indlo_debut . "', '" . $indlo_fin . "')";
    return $query;
}

function getFactureBybail($bail_id)
{
    $query = "SELECT * FROM c_bail
    INNER JOIN c_facture_loyer ON c_facture_loyer.bail_id = c_bail.bail_id
    WHERE c_bail.bail_id = $bail_id";
    return $query;
}

function getBailwithJoin($bail_id)
{
    $query = "SELECT * FROM c_bail
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id = c_bail.cliLot_id
    INNER JOIN c_periodicite_loyer ON c_periodicite_loyer.pdl_id = c_bail.pdl_id
    INNER JOIN c_nature_prise_effet ON c_nature_prise_effet.natef_id = c_bail.natef_id
    INNER JOIN c_type_bail ON c_type_bail.tpba_id = c_bail.tpba_id
    INNER JOIN c_gestionnaire ON c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id
    INNER JOIN c_type_echeance ON c_type_echeance.tpech_id = c_bail.tpech_id
    INNER JOIN c_bail_art ON c_bail_art.bart_id = c_bail.bail_art_605
    -- INNER JOIN c_indice_loyer ON c_indice_loyer.indloyer_id = c_bail.indloyer_id
    INNER JOIN c_moment_facturation_bail ON c_moment_facturation_bail.momfac_id = c_bail.momfac_id
    INNER JOIN c_periode_indexation ON c_periode_indexation.prdind_id = c_bail.prdind_id
    WHERE c_bail.bail_id = $bail_id";
    return $query;
}

function getGestionnaireByBAil($bail_id)
{
    $query = "SELECT * FROM c_bail
    INNER JOIN c_gestionnaire ON c_gestionnaire.gestionnaire_id  = c_bail.preneur_id
    WHERE c_bail.bail_id = $bail_id";
    return $query;
}

function getLotBail($lot_id)
{
    $query = "SELECT * FROM c_lot_client
    WHERE cliLot_id = $lot_id";
    return $query;
}

function getIndexationReference($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer
    WHERE bail_id = $bail_id
    AND etat_indexation = 1 
    AND indlo_ref_loyer = 0
    ORDER BY indlo_id DESC
    LIMIT 1";
    return $query;
}

function getBailEmeteur($bail_id)
{
    $query = "SELECT * FROM c_bail
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id = c_bail.cliLot_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_lot_client.dossier_id
    WHERE c_bail.bail_id = $bail_id";
    return $query;
}

function getProgram($progm_id)
{
    $query = "SELECT * FROM c_programme
    WHERE progm_id = $progm_id";
    return $query;
}

function getFactureBAil($bail_id, $annee)
{
    $query = "SELECT * FROM c_facture_loyer
    WHERE bail_id = $bail_id
    AND facture_annee = $annee";
    return $query;
}

function getDocLoyer($cliLot_id)
{
    $query = "SELECT * FROM c_document_loyer
    WHERE cliLot_id = $cliLot_id";
    return $query;
}

function delete_docById($fact_loyer_id)
{
    $query = "DELETE FROM c_document_loyer WHERE fact_loyer_id = $fact_loyer_id";
    return $query;
}

function delete_factureById($bail_id, $annee)
{
    $query = "DELETE FROM c_facture_loyer WHERE bail_id = $bail_id AND facture_annee = $annee";
    return $query;
}

function getIndexationOther($bail_id)
{
    $query = "SELECT * FROM c_indexation_loyer 
    WHERE bail_id = $bail_id AND etat_indexation = 1 AND indlo_ref_loyer = 0";
    return $query;
}

function getBailWithDossier($bail_id)
{
    $query = "SELECT * FROM c_bail
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id = c_bail.cliLot_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_lot_client.dossier_id
    WHERE c_bail.bail_id = $bail_id";
    return $query;
}

function getDossierFacture($dossier_id)
{
    $query = "SELECT * FROM c_facture_loyer
    WHERE dossier_id = $dossier_id
    ORDER BY fact_loyer_id DESC
    LIMIT 1";
    return $query;
}

function insertFacture($data)
{
    $fact_loyer_app_ht = $data['fact_loyer_app_ht'];
    $fact_loyer_app_tva = $data['fact_loyer_app_tva'];
    $fact_loyer_app_ttc = $data['fact_loyer_app_ttc'];
    $fact_loyer_park_ht = $data['fact_loyer_park_ht'];
    $fact_loyer_park_tva = $data['fact_loyer_park_tva'];
    $fact_loyer_park_ttc = $data['fact_loyer_park_ttc'];
    $fact_loyer_charge_ht = $data['fact_loyer_charge_ht'];
    $fact_loyer_charge_tva = $data['fact_loyer_charge_tva'];
    $fact_loyer_charge_ttc = $data['fact_loyer_charge_ttc'];
    $bail_id = $data['bail_id'];
    $fact_loyer_num = $data['fact_loyer_num'];
    $periode_mens_id = $data['periode_mens_id'];
    $facture_nombre = $data['facture_nombre'];
    $facture_annee = $data['facture_annee'];
    $dossier_id = $data['dossier_id'];
    $emetteur_nom = addslashes($data['emetteur_nom']);
    $emet_adress1 = addslashes($data['emet_adress1']);
    $emet_adress2 = addslashes($data['emet_adress2']);
    $emet_adress3 = addslashes($data['emet_adress3']);
    $emet_cp = $data['emet_cp'];
    $emet_pays = addslashes($data['emet_pays']);
    $emet_ville = addslashes($data['emet_ville']);
    $dest_nom = addslashes($data['dest_nom']);
    $dest_adresse1 = addslashes($data['dest_adresse1']);
    $dest_adresse2 = addslashes($data['dest_adresse2']);
    $dest_adresse3 = addslashes($data['dest_adresse3']);
    $dest_cp = $data['dest_cp'];
    $dest_pays = addslashes($data['dest_pays']);
    $dest_ville = addslashes($data['dest_ville']);
    $mode_paiement = addslashes($data['mode_paiement']);
    $echeance_paiement = $data['echeance_paiement'];
    $fact_loyer_date = $data['fact_loyer_date'];
    $montant_deduction = $data['montant_deduction'];
    $libelle_facture_deduit = $data['libelle_facture_deduit'];

    if ($data['montant_deduction'] == '') {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_mens_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date) 
    VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_mens_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "')";
    } else {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_mens_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date, montant_deduction, libelle_facture_deduit) 
    VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_mens_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "',  $montant_deduction, '" . addslashes($libelle_facture_deduit) . "')";
    }

    return $query;
}

function insertFactureTrimestriel($data)
{
    $fact_loyer_app_ht = $data['fact_loyer_app_ht'];
    $fact_loyer_app_tva = $data['fact_loyer_app_tva'];
    $fact_loyer_app_ttc = $data['fact_loyer_app_ttc'];
    $fact_loyer_park_ht = $data['fact_loyer_park_ht'];
    $fact_loyer_park_tva = $data['fact_loyer_park_tva'];
    $fact_loyer_park_ttc = $data['fact_loyer_park_ttc'];
    $fact_loyer_charge_ht = $data['fact_loyer_charge_ht'];
    $fact_loyer_charge_tva = $data['fact_loyer_charge_tva'];
    $fact_loyer_charge_ttc = $data['fact_loyer_charge_ttc'];
    $bail_id = $data['bail_id'];
    $fact_loyer_num = $data['fact_loyer_num'];
    $periode_timestre_id = $data['periode_timestre_id'];
    $facture_nombre = $data['facture_nombre'];
    $facture_annee = $data['facture_annee'];
    $dossier_id = $data['dossier_id'];
    $emetteur_nom = addslashes($data['emetteur_nom']);
    $emet_adress1 = addslashes($data['emet_adress1']);
    $emet_adress2 = addslashes($data['emet_adress2']);
    $emet_adress3 = addslashes($data['emet_adress3']);
    $emet_cp = $data['emet_cp'];
    $emet_pays = addslashes($data['emet_pays']);
    $emet_ville = addslashes($data['emet_ville']);
    $dest_nom = addslashes($data['dest_nom']);
    $dest_adresse1 = addslashes($data['dest_adresse1']);
    $dest_adresse2 = addslashes($data['dest_adresse2']);
    $dest_adresse3 = addslashes($data['dest_adresse3']);
    $dest_cp = $data['dest_cp'];
    $dest_pays = addslashes($data['dest_pays']);
    $dest_ville = addslashes($data['dest_ville']);
    $mode_paiement = addslashes($data['mode_paiement']);
    $echeance_paiement = $data['echeance_paiement'];
    $fact_loyer_date = $data['fact_loyer_date'];
    $montant_deduction = $data['montant_deduction'];
    $libelle_facture_deduit = $data['libelle_facture_deduit'];

    if ($data['montant_deduction'] == '') {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_timestre_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_timestre_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "')";
    } else {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_timestre_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date, montant_deduction, libelle_facture_deduit) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_timestre_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "',  $montant_deduction, '" . addslashes($libelle_facture_deduit) . "')";
    }
    return $query;
}

function insertFactureSemestriel($data)
{
    $fact_loyer_app_ht = $data['fact_loyer_app_ht'];
    $fact_loyer_app_tva = $data['fact_loyer_app_tva'];
    $fact_loyer_app_ttc = $data['fact_loyer_app_ttc'];
    $fact_loyer_park_ht = $data['fact_loyer_park_ht'];
    $fact_loyer_park_tva = $data['fact_loyer_park_tva'];
    $fact_loyer_park_ttc = $data['fact_loyer_park_ttc'];
    $fact_loyer_charge_ht = $data['fact_loyer_charge_ht'];
    $fact_loyer_charge_tva = $data['fact_loyer_charge_tva'];
    $fact_loyer_charge_ttc = $data['fact_loyer_charge_ttc'];
    $bail_id = $data['bail_id'];
    $fact_loyer_num = $data['fact_loyer_num'];
    $periode_semestre_id = $data['periode_semestre_id'];
    $facture_nombre = $data['facture_nombre'];
    $facture_annee = $data['facture_annee'];
    $dossier_id = $data['dossier_id'];
    $emetteur_nom = addslashes($data['emetteur_nom']);
    $emet_adress1 = addslashes($data['emet_adress1']);
    $emet_adress2 = addslashes($data['emet_adress2']);
    $emet_adress3 = addslashes($data['emet_adress3']);
    $emet_cp = $data['emet_cp'];
    $emet_pays = addslashes($data['emet_pays']);
    $emet_ville = addslashes($data['emet_ville']);
    $dest_nom = addslashes($data['dest_nom']);
    $dest_adresse1 = addslashes($data['dest_adresse1']);
    $dest_adresse2 = addslashes($data['dest_adresse2']);
    $dest_adresse3 = addslashes($data['dest_adresse3']);
    $dest_cp = $data['dest_cp'];
    $dest_pays = addslashes($data['dest_pays']);
    $dest_ville = addslashes($data['dest_ville']);
    $mode_paiement = addslashes($data['mode_paiement']);
    $echeance_paiement = $data['echeance_paiement'];
    $fact_loyer_date = $data['fact_loyer_date'];
    $montant_deduction = $data['montant_deduction'];
    $libelle_facture_deduit = $data['libelle_facture_deduit'];

    if ($data['montant_deduction'] == '') {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_semestre_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_semestre_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "')";
    } else {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_semestre_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date, , montant_deduction, libelle_facture_deduit) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_semestre_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "',  $montant_deduction, '" . addslashes($libelle_facture_deduit) . "')";
    }
    return $query;
}

function getPeriodeAnuelle()
{
    $query = "SELECT * FROM c_periode_annee";
    return $query;
}

function getLastFacture($bail_id)
{
    $query = "SELECT * FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    WHERE c_facture_loyer.bail_id = $bail_id
    ORDER BY fact_loyer_id DESC
    LIMIT 1";
    return $query;
}

function insertFactureAnnuelle($data)
{
    $fact_loyer_app_ht = $data['fact_loyer_app_ht'];
    $fact_loyer_app_tva = $data['fact_loyer_app_tva'];
    $fact_loyer_app_ttc = $data['fact_loyer_app_ttc'];
    $fact_loyer_park_ht = $data['fact_loyer_park_ht'];
    $fact_loyer_park_tva = $data['fact_loyer_park_tva'];
    $fact_loyer_park_ttc = $data['fact_loyer_park_ttc'];
    $fact_loyer_charge_ht = $data['fact_loyer_charge_ht'];
    $fact_loyer_charge_tva = $data['fact_loyer_charge_tva'];
    $fact_loyer_charge_ttc = $data['fact_loyer_charge_ttc'];
    $bail_id = $data['bail_id'];
    $fact_loyer_num = $data['fact_loyer_num'];
    $periode_annee_id = $data['periode_annee_id'];
    $facture_nombre = $data['facture_nombre'];
    $facture_annee = $data['facture_annee'];
    $dossier_id = $data['dossier_id'];
    $emetteur_nom = addslashes($data['emetteur_nom']);
    $emet_adress1 = addslashes($data['emet_adress1']);
    $emet_adress2 = addslashes($data['emet_adress2']);
    $emet_adress3 = addslashes($data['emet_adress3']);
    $emet_cp = $data['emet_cp'];
    $emet_pays = addslashes($data['emet_pays']);
    $emet_ville = addslashes($data['emet_ville']);
    $dest_nom = addslashes($data['dest_nom']);
    $dest_adresse1 = addslashes($data['dest_adresse1']);
    $dest_adresse2 = addslashes($data['dest_adresse2']);
    $dest_adresse3 = addslashes($data['dest_adresse3']);
    $dest_cp = $data['dest_cp'];
    $dest_pays = addslashes($data['dest_pays']);
    $dest_ville = addslashes($data['dest_ville']);
    $mode_paiement = addslashes($data['mode_paiement']);
    $echeance_paiement = $data['echeance_paiement'];
    $fact_loyer_date = $data['fact_loyer_date'];
    $montant_deduction = $data['montant_deduction'];
    $libelle_facture_deduit = $data['libelle_facture_deduit'];

    if ($data['montant_deduction'] == '') {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_annee_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_annee_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "')";
    } else {
        $query = "INSERT INTO c_facture_loyer (fact_loyer_app_ht, fact_loyer_app_tva, fact_loyer_app_ttc, fact_loyer_park_ht, fact_loyer_park_tva, fact_loyer_park_ttc, fact_loyer_charge_ht, fact_loyer_charge_tva, fact_loyer_charge_ttc, bail_id, fact_loyer_num, periode_annee_id, facture_nombre, facture_annee, dossier_id, emetteur_nom, emet_adress1, emet_adress2, emet_adress3, emet_cp, emet_pays, emet_ville, dest_nom, dest_adresse1, dest_adresse2, dest_adresse3, dest_cp, dest_pays, dest_ville, mode_paiement, echeance_paiement, fact_loyer_date, , montant_deduction, libelle_facture_deduit) 
        VALUES ($fact_loyer_app_ht, $fact_loyer_app_tva, $fact_loyer_app_ttc, $fact_loyer_park_ht, $fact_loyer_park_tva, $fact_loyer_park_ttc, $fact_loyer_charge_ht, $fact_loyer_charge_tva, $fact_loyer_charge_ttc, $bail_id, '" . $fact_loyer_num . "', $periode_annee_id, $facture_nombre, $facture_annee, $dossier_id, '" . $emetteur_nom . "', '" . $emet_adress1 . "', '" . $emet_adress2 . "', '" . $emet_adress3 . "', '" . $emet_cp . "', '" . $emet_pays . "','" . $emet_ville . "', '" . $dest_nom . "','" . $dest_adresse1 . "', '" . $dest_adresse2 . "', '" . $dest_adresse3 . "', '" . $dest_cp . "', '" . $dest_pays . "', '" . $dest_ville . "', '" . $mode_paiement . "', '" . $echeance_paiement . "', '" . $fact_loyer_date . "',  $montant_deduction, '" . addslashes($libelle_facture_deduit) . "')";
    }
    return $query;
}

function getLastLotIndex($bail_id)
{
    $query = "SELECT * FROM c_lot_indexe
    WHERE bail_id = $bail_id
    ORDER BY lot_index_id  DESC
    LIMIT 1";
    return $query;
}

function updateLotIndexe($lot_index_id, $data)
{
    $etat_facture = $data['etat_facture'];
    $date_creation_facture = $data['date_creation_facture'];
    $nmbr_facture = $data['nmbr_facture'];

    $query = "UPDATE c_lot_indexe SET etat_facture = $etat_facture , date_creation_facture = '" . $date_creation_facture . "' , nmbr_facture = $nmbr_facture WHERE lot_index_id = $lot_index_id";
    return $query;
}

function getFactureMensAdded($bail_id, $fact_loyer_id)
{
    $query = "SELECT * FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    INNER JOIN c_periode_mensuel ON c_periode_mensuel.periode_mens_id  = c_facture_loyer.periode_mens_id
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id  = c_bail.cliLot_id
    INNER JOIN c_programme ON c_programme.progm_id  = c_lot_client.progm_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_facture_loyer.dossier_id
    INNER JOIN c_utilisateur ON c_utilisateur.util_id  = c_dossier.util_id
    WHERE c_facture_loyer.bail_id = $bail_id AND c_facture_loyer.fact_loyer_id = $fact_loyer_id
    ORDER BY fact_loyer_id   DESC
    LIMIT 1";
    return $query;
}

function getFactureTrimAdded($bail_id, $fact_loyer_id)
{
    $query = "SELECT * FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    INNER JOIN c_periode_trimestre ON c_periode_trimestre.periode_timestre_id  = c_facture_loyer.periode_timestre_id
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id  = c_bail.cliLot_id
    INNER JOIN c_programme ON c_programme.progm_id  = c_lot_client.progm_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_facture_loyer.dossier_id
    INNER JOIN c_utilisateur ON c_utilisateur.util_id  = c_dossier.util_id
    WHERE c_facture_loyer.bail_id = $bail_id AND c_facture_loyer.fact_loyer_id = $fact_loyer_id
    ORDER BY fact_loyer_id   DESC
    LIMIT 1";
    return $query;
}

function getFactureSemAdded($bail_id, $fact_loyer_id)
{
    $query = "SELECT * FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    INNER JOIN c_periode_semestre ON c_periode_semestre.periode_semestre_id = c_facture_loyer.periode_semestre_id
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id  = c_bail.cliLot_id
    INNER JOIN c_programme ON c_programme.progm_id  = c_lot_client.progm_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_facture_loyer.dossier_id
    INNER JOIN c_utilisateur ON c_utilisateur.util_id  = c_dossier.util_id
    WHERE c_facture_loyer.bail_id = $bail_id AND c_facture_loyer.fact_loyer_id = $fact_loyer_id
    ORDER BY fact_loyer_id   DESC
    LIMIT 1";
    return $query;
}

function getFactureAnnuelleAdded($bail_id, $fact_loyer_id)
{
    $query = "SELECT * FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    INNER JOIN c_periode_annee ON c_periode_annee.periode_annee_id = c_facture_loyer.periode_annee_id
    INNER JOIN c_lot_client ON c_lot_client.cliLot_id  = c_bail.cliLot_id
    INNER JOIN c_programme ON c_programme.progm_id  = c_lot_client.progm_id
    INNER JOIN c_dossier ON c_dossier.dossier_id  = c_facture_loyer.dossier_id
    INNER JOIN c_utilisateur ON c_utilisateur.util_id  = c_dossier.util_id
    WHERE c_facture_loyer.bail_id = $bail_id AND c_facture_loyer.fact_loyer_id = $fact_loyer_id
    ORDER BY fact_loyer_id   DESC
    LIMIT 1";
    return $query;
}

function getRib()
{
    $query = "SELECT * FROM c_rib
    ORDER BY rib_id DESC
    LIMIT 1";
    return $query;
}

function insertPDf($data)
{
    $doc_loyer_nom = addslashes($data['doc_loyer_nom']);
    $doc_loyer_path = $data['doc_loyer_path'];
    $doc_loyer_creation = $data['doc_loyer_creation'];
    $doc_loyer_etat = $data['doc_loyer_etat'];
    $doc_loyer_annee = $data['doc_loyer_annee'];
    $cliLot_id = $data['cliLot_id'];
    $util_id = $data['util_id'];
    $fact_loyer_id = $data['fact_loyer_id'];

    $query = "INSERT INTO c_document_loyer (doc_loyer_nom, doc_loyer_path,doc_loyer_creation,doc_loyer_etat,doc_loyer_annee,cliLot_id,util_id,fact_loyer_id) 
    VALUES ('" . $doc_loyer_nom . "', '" . $doc_loyer_path . "', '" . $doc_loyer_creation . "',$doc_loyer_etat,$doc_loyer_annee,$cliLot_id,$util_id,$fact_loyer_id)";
    return $query;
}

function getFranchiseBail($bail_id)
{
    $query = "SELECT * FROM `c_franchise_loyer` 
    WHERE bail_id = $bail_id AND etat_franchise = 1";
    return $query;
}


function getDonnefactureAuto($bail_id)
{
    $query = "SELECT c_facture_loyer.fact_loyer_date AS fact_loyer_date, 
    c_lot_client.gestionnaire_id AS gestionnaire_id, 
    c_bail.cliLot_id AS cliLot_id,
    c_facture_loyer.fact_loyer_id AS fact_loyer_id
    FROM c_facture_loyer
    INNER JOIN c_bail ON c_bail.bail_id = c_facture_loyer.bail_id
    INNER JOIN c_lot_client ON  c_lot_client.cliLot_id = c_bail.cliLot_id
    WHERE c_facture_loyer.bail_id = $bail_id
    ORDER BY c_facture_loyer.fact_loyer_id DESC
    LIMIT 1";
    return $query;
}

function insertfactureAuto($data)
{
    $cliLot_id = $data['cliLot_id'];
    $fact_loyer_id = $data['fact_loyer_id'];
    $date_facture = $data['date_facture'];
    $gestionaire_id = $data['gestionaire_id'];
    $etat = $data['etat'];

    $query = "INSERT INTO c_facture_automatique (cliLot_id, fact_loyer_id, date_facture, gestionaire_id, etat) 
    VALUES ($cliLot_id, $fact_loyer_id,'" . $date_facture . "', $gestionaire_id, $etat)";
    return $query;
}

function check_trimestre($data)
{
    $facture_annee = $data['facture_annee'];
    $periode_timestre_id = $data['periode_timestre_id'];
    $bail_id = $data['bail_id'];

    $query = "SELECT * FROM c_facture_loyer 
    WHERE facture_annee = $facture_annee AND bail_id = $bail_id
    ORDER BY periode_timestre_id DESC LIMIT 1";

    return $query;
}
