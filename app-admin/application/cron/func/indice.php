<?php

$queryIndice = "SELECT * FROM c_indice_loyer";

function getIndiceValeurMaxAnnee($indloyer_id)
{
    $query = "SELECT MAX(indval_annee) AS indval_annee FROM c_indice_valeur_loyer
    WHERE indloyer_id = $indloyer_id";
    return $query;
}

function getIndiceValeur($indloyer_id, $indval_annee)
{
    $query = "SELECT *, MAX(indval_trimestre) AS indval_trimestre FROM c_indice_valeur_loyer
    WHERE indloyer_id = $indloyer_id AND indval_annee = $indval_annee";
    return $query;
}

function getUserDefault()
{
    $query = "SELECT * FROM `c_utilisateur` AS u 
    INNER JOIN `c_messagerie_paramserveur_utilisateur` AS m_p 
    ON  m_p.util_id = u.util_id
    WHERE u.util_id = 1";
    return $query;
}