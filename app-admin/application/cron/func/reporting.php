<?php

function getReporting($dossier_id,$reporting_id){

    $query = "SELECT *, `c_utilisateur`.`util_prenom`, `c_utilisateur`.`util_id`
    FROM `c_reporting`
    LEFT JOIN `c_dossier` ON `c_reporting`.`dossier_id` = `c_dossier`.`dossier_id`
    LEFT JOIN `c_utilisateur` ON `c_utilisateur`.`util_id` = `c_dossier`.`util_id`
    LEFT JOIN `c_client` ON `c_client`.`client_id` = `c_dossier`.`client_id`
    LEFT JOIN `c_lot_client` ON `c_lot_client`.`dossier_id` = `c_dossier`.`dossier_id`
    LEFT JOIN `c_programme` ON `c_lot_client`.`progm_id` = `c_programme`.`progm_id`
    LEFT JOIN `c_gestionnaire` ON `c_lot_client`.`gestionnaire_id` = `c_gestionnaire`.`gestionnaire_id`
    LEFT JOIN `c_typologielot` ON `c_lot_client`.`typologielot_id` = `c_typologielot`.`typologielot_id`
    LEFT JOIN `c_acte` ON `c_lot_client`.`cliLot_id` = `c_acte`.`cliLot_id`
    LEFT JOIN `c_pno` ON `c_lot_client`.`cliLot_id` = `c_pno`.`cliLot_id`
    LEFT JOIN `c_pourcentage_evaluation` ON `c_pourcentage_evaluation`.`gestionnaire_id` = `c_gestionnaire`.`gestionnaire_id`
    WHERE `c_reporting`.`dossier_id` = $dossier_id
    AND `c_reporting`.`reporting_id` = $reporting_id
    AND `c_lot_client`.`cliLot_etat` = 1";
    return $query;

}

function getTaxefonciere($reporting_annee, $cliLot_id){

    $query = "SELECT *
    FROM `c_taxe_fonciere_lot`
    JOIN `c_lot_client` ON `c_lot_client`.`cliLot_id` = `c_taxe_fonciere_lot`.`cliLot_id`
    WHERE `annee` = $reporting_annee
    AND `c_lot_client`.`cliLot_id` = $cliLot_id";
    return $query;
}


function getInfoBail($cliLot_id){

    $query = "SELECT *, `c_bail`.`bail_id`
    FROM `c_bail`
    LEFT JOIN `c_lot_client` ON `c_lot_client`.`cliLot_id` = `c_bail`.`cliLot_id`
    LEFT JOIN `c_periodicite_loyer` ON `c_bail`.`pdl_id` = `c_periodicite_loyer`.`pdl_id`
    LEFT JOIN `c_periode_indexation` ON `c_bail`.`prdind_id` = `c_periode_indexation`.`prdind_id`
    LEFT JOIN `c_indice_loyer` ON `c_bail`.`indloyer_id` = `c_indice_loyer`.`indloyer_id`
    LEFT JOIN `c_moment_facturation_bail` ON `c_bail`.`momfac_id` = `c_moment_facturation_bail`.`momfac_id`
    LEFT JOIN `c_type_echeance` ON `c_bail`.`tpech_id` = `c_type_echeance`.`tpech_id`
    LEFT JOIN `c_facture_loyer` ON `c_bail`.`bail_id` = `c_facture_loyer`.`bail_id`
    LEFT JOIN `c_type_bail` ON `c_bail`.`tpba_id` = `c_type_bail`.`tpba_id`
    WHERE `c_lot_client`.`cliLot_id` = $cliLot_id
    ORDER BY `c_bail`.`bail_id` DESC";
    return $query;

}

function getLoyerFacture($bail_id, $reporting_annee, $periode_timestre_id){

    $query= "SELECT *
    FROM `c_facture_loyer`
    WHERE `bail_id` = $bail_id
    AND `periode_timestre_id` = $periode_timestre_id
    AND `facture_annee` = $reporting_annee";
    return $query;

}

function updatePdfReporting($data,$reporting_id)
{ 
    $reporting_path = $data['reporting_path'];
    $reporting_date_generation = $data['reporting_date_generation'];
    
    $query = "UPDATE `c_reporting` SET `reporting_path` = $reporting_path, `reporting_date_generation` =  $reporting_date_generation, `reporting_etat` = 'Généré'
    WHERE `reporting_id` = $reporting_id";
    return $query;
}
