<?php

function getClient()
{
    $query = "SELECT *
    FROM c_client
    INNER JOIN c_utilisateur ON c_utilisateur.util_id = c_client.util_id
    INNER JOIN c_messagerie_paramserveur_utilisateur ON c_utilisateur.util_id = c_messagerie_paramserveur_utilisateur.util_id
    WHERE c_client.client_date_fin_validite_cni IS NOT NULL
    AND DATE_SUB(c_client.client_date_fin_validite_cni, INTERVAL 3 MONTH) <= CURRENT_DATE";
    return $query;
}

function getModel($id)
{
    $query = "SELECT * FROM c_param_modelemail
    WHERE pmail_id = $id";
    return $query;
}

function verifierDossier($id)
{
    $query = "SELECT * FROM c_dossier
    WHERE client_id = $id 
    AND dossier_etat = 1";
    return $query;
}

function verifierLotMandat($id)
{
    $query = "SELECT * FROM c_lot_client
    INNER JOIN c_mandat_new 
    ON c_mandat_new.cliLot_id = c_lot_client.cliLot_id
    WHERE c_lot_client.cli_id = $id 
    AND c_lot_client.cliLot_etat = 1 
    AND c_mandat_new.mandat_date_signature IS NOT NULL";
    return $query;
}

function insertcommunication($data)
{
    $comclient_texte = $data['comclient_texte'];
    $comclient_date_creation = date('Y-m-d H:i:s');
    $etatcli_com = 1;
    $client_id = $data['client_id'];
    $util_id = $data['util_id'];
    $type_comprospect_id = 2;
    $query = "INSERT INTO c_communication_client (comclient_texte, comclient_date_creation,etatcli_com,client_id,util_id,type_comprospect_id) 
    VALUES ('" . $comclient_texte . "','" . $comclient_date_creation . "', $etatcli_com, $client_id, $util_id, $type_comprospect_id)";
    return $query;
}
