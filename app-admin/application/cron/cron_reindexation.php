<?php

require_once  __DIR__ . '/inc/db.php';
require_once  __DIR__ . '/func/reindexation.php';
require_once  __DIR__ . '/mpdf/Mpdf.php';

require_once  __DIR__ . '/libraries/DbConnector.php';
require_once  __DIR__ . '/libraries/DbRequest.php';
require_once  __DIR__ . '/libraries/MailjetService.php';

if (function_exists('get_Indice_valide')) {
    $Cron_a_faire = executeQuery(get_Indice_valide(), $pdo);
}

if (!empty($Cron_a_faire)) {
    foreach ($Cron_a_faire as $key => $value) {
        if (function_exists('reIndexIndiceValeur')) {
            $cron_index_id = $value['cron_index_id'];

            $data_cron = array(
                'date_reindex' => date('Y-m-d H:i:s'),
                'etat_reindex' => 1
            );

            $req = updatetQuery(updateCron_indexe($data_cron, $cron_index_id), $pdo);

            reIndexIndiceValeur($value['indloyer_id'], $pdo);
        }
    }
}

function reIndexIndiceValeur($indloyer_id, $pdo)
{
    if (function_exists('getListeBailActif')) {
        $liste_bail = executeQuery(getListeBailActif($indloyer_id), $pdo);
    }

    if (function_exists('getLast_indice_valeur')) {
        $last_indice_valeur = executeQuery(getLast_indice_valeur($indloyer_id), $pdo);
    }

    if (!empty($liste_bail)) {
        foreach ($liste_bail as $key => $value) {
            if (function_exists('getIndexationLoyer')) {
                $indexation = getIndexationLoyer($value['bail_id'], $pdo);
            }

            if (!empty($indexation)) {
                $date_fin = date('Y', strtotime($indexation[0]['indlo_fin']));

                if ($date_fin <= $last_indice_valeur[0]['indval_annee']) {

                    $data_lot_index = array(
                        'bail_id' => $value['bail_id'],
                        'indval_id' => $last_indice_valeur[0]['indval_id']
                    );

                    if (function_exists('insertlotIndexe')) {

                        $req = insertQuery(insertlotIndexe($data_lot_index), $pdo);

                        if (function_exists('reIndexation')) {
                            reIndexation($value['bail_id'], $pdo);
                        }
                    }
                }
            }
        }
    }
}

function getIndexationLoyer($bail_id, $pdo)
{
    if (function_exists('getIndexationLoyerQuery')) {
        $indexation = executeQuery(getIndexationLoyerQuery($bail_id), $pdo);
    }
    return $indexation;
}

function reIndexation($bail_id, $pdo)
{
    if (function_exists('getLastIndexation')) {
        $last_indexation_before_reindex = getLastIndexation($bail_id, $pdo);
    }

    $indiceValeurBail = executeQuery(getIndiceValeurBail($bail_id), $pdo);

    if (function_exists('delete_indexation') && $indiceValeurBail[0]['indval_valide'] == 1) {
        $req = deleteQuery(delete_indexation($bail_id), $pdo);
    }

    if (function_exists('getBail')) {
        $bail = executeQuery(getBail($bail_id), $pdo);
    }

    if (function_exists('getLastIndexation')) {
        $last_indexation_infirst = getLastIndexation($bail_id, $pdo);
    }

    $indloyer_id =  $bail[0]['indloyer_id'];
    $periode_valeur = $bail[0]['prdind_id'];

    if (function_exists('maxIndice')) {
        $maxIndice = executeQuery(maxIndice($indloyer_id), $pdo);
    }

    if ($bail[0]['bail_debut'] < $last_indexation_infirst['indlo_debut']) {
        $start_year = intval(date('Y', strtotime($bail[0]['bail_debut'])));
    } else {
        $start_year = intval(date('Y', strtotime($last_indexation_infirst['indlo_debut'])));
    }

    $month_of_indexation = intval(date('m', strtotime($last_indexation_infirst['indlo_debut'])));
    $end_year = intval(date('Y', strtotime($maxIndice[0]['indval_annee'])));
    //si endYear inférieur à startYear
    $end_year = intval($maxIndice[0]['indval_annee']) < $start_year ? $start_year + $periode_valeur : $end_year;

    if (function_exists('getIndice')) {
        $indice = executeQuery(getIndice($indloyer_id), $pdo);
    }

    if ($indiceValeurBail[0]['indval_valide'] != 1) {
    } elseif ($indice[0]['indloyer_id'] == 7) {
        $last_indexation_infirst = getLastIndexation($bail[0]['bail_id'], $pdo);

        $indloyer_id =  $bail[0]['indloyer_id'];

        $maxIndice = executeQuery(maxIndiceFixe($indloyer_id), $pdo);

        $periode_valeur = $bail[0]['prdind_id'];

        if ($bail[0]['bail_debut'] < $last_indexation_infirst['indlo_debut']) {
            $start_year = intval(date('Y', strtotime($bail[0]['bail_debut'])));
        } else {
            $start_year = intval(date('Y', strtotime($last_indexation_infirst['indlo_debut'])));
        }

        $end_year = intval($maxIndice[0]['indval_annee']);
        $end_year = intval($maxIndice[0]['indval_annee']) < $start_year ? $start_year + $periode_valeur : $end_year;

        if ($periode_valeur == 2) {
            $debut = date('Y', strtotime($last_indexation_infirst['indlo_debut']));
            $fin = $end_year;
            $pas = 3;
            $tableauAnnees = array();

            for ($annee = $debut; $annee <= $fin; $annee += $pas) {
                $tableauAnnees[] = $annee;
            }
        }

        $tab = array();

        for ($annee = $start_year; $annee <= $end_year; $annee++) {
            $checkIndicefixe = executeQuery(checkIndicefixe($bail[0]['bail_id']), $pdo);

            $checkIndicefixe = !empty($checkIndicefixe) ? count($checkIndicefixe) : 0;

            $indice_base = executeQuery(fixeindicebase($annee, $indloyer_id), $pdo);

            $new_indice_reference = $new_indice_reference = getIndiceReferenceAnnuelle($indloyer_id, ($annee), $pdo);

            $last_index = getLastIndexation($bail[0]['bail_id'], $pdo);

            $data_index = array();

            if (!empty($new_indice_reference)) {
                $data_index['bail_id'] = $bail[0]['bail_id'];
                $data_index['etat_indexation'] = 1;
                $data_index['indlo_indice_base'] = $indice_base[0]['indval_id'];
                $data_index['indlo_indice_reference'] = $new_indice_reference['indval_id'];
                $data_index['indlo_ref_loyer'] = 0;

                $array_variation = array();

                if ($indice->mode_calcul == 1) {
                    $variation_loyer = (($new_indice_reference['indval_valeur'] - $indice_base[0]['indval_valeur']) / $indice_base[0]['indval_valeur']) * 100;
                } else {
                    $variation_loyer = $checkIndicefixe >= 1 ? $new_indice_reference['indval_valeur'] : 0;
                }

                array_push($array_variation, $variation_loyer);

                if ($bail[0]['bail_var_plafonnee'] != 0) {
                    $variation_plafonnee = (floatval($bail[0]['bail_var_plafonnee']) * $variation_loyer) / 100;
                    array_push($array_variation, $variation_plafonnee);
                }
                if ($bail[0]['bail_var_capee'] != 0) {
                    $variation_capee = floatval($bail[0]['bail_var_capee']);
                    array_push($array_variation, $variation_capee);
                }

                $variation_appliquee = round(min($array_variation), 2);

                if (floatval($bail[0]['bail_var_min']) != 0 && floatval($bail[0]['bail_var_min']) > floatval($variation_appliquee)) {
                    $variation_appliquee = floatval($bail[0]['bail_var_min']);
                }

                if ($checkIndicefixe == 1) {
                    $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                    $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));
                    $first_debut_indexation = date('Y-m-d', strtotime($annee - 1 . '-' . $month . '-' . $day));
                    $bail_debut = $bail[0]['bail_debut'];
                    $first_fin_indexation = date('Y-m-d', strtotime(($annee) . '-' . $month . '-' . ($day - 1)));

                    $date1 = new DateTime($bail_debut);
                    $date2 = new DateTime($first_fin_indexation);
                    $interval = $date1->diff($date2);
                    $diffEnJours_prorata = $interval->days + 1;

                    $dateindex1 = new DateTime($first_debut_indexation);
                    $dateindex2 = new DateTime($first_fin_indexation);
                    $interval = $dateindex1->diff($dateindex2);
                    $diffEnJours_index = $interval->days + 1;

                    $variation_appliquee = (round($diffEnJours_prorata / $diffEnJours_index, 10)) * $new_indice_reference['indval_valeur'];
                }

                $data_index['indlo_variation_appliquee'] = $variation_appliquee;

                $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index['indlo_appartement_ht'] + (($last_index['indlo_appartement_ht'] * $variation_appliquee) / 100), 2);
                $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index['indlo_parking_ht'] + (($last_index['indlo_parking_ht'] * $variation_appliquee) / 100), 2);
                $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index['indlo_charge_ht'] + (($last_index['indlo_charge_ht'] * $variation_appliquee) / 100), 2);

                $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index['indlo_appartement_tva'];
                $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index['indlo_parking_tva'];
                $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index['indlo_charge_tva'];

                $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));

                $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                if ($periode_valeur == 2) {
                    if (in_array($annee, $tableauAnnees)) {
                        $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 3) . '-' . $month . '-' . ($day - 1)));
                    } else {
                        array_push($tab, $data_index['indlo_debut']);
                    }
                }

                $request = insertQuery(insertIndexatons($data_index), $pdo);
            }
        }

        updatetQuery(update_where_date($last_indexation_infirst['indlo_debut'], $bail[0]['bail_id']), $pdo);

        if ($periode_valeur == 2) {
            foreach ($tab as $key => $value) {
                if ($last_indexation_infirst['indlo_debut'] < $value) {
                    deleteQuery(delete_where_index_date($value, $bail[0]['bail_id']), $pdo);
                }
            }
        }
    } else {
        if ($periode_valeur == 1) {
            for ($annee = $start_year; $annee <= $end_year; $annee++) {
                $indice_base = getIndiceBase($bail[0]['bail_id'], $pdo);

                if ($indice[0]['indloyer_periode'] == "Annuelle") {
                    $new_indice_reference = getIndiceReferenceAnnuelle($indloyer_id, ($annee), $pdo);
                } else {
                    $new_indice_reference = getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base['indval_trimestre'], $pdo);
                }

                $last_index = getLastIndexation($bail[0]['bail_id'], $pdo);
                $data_index = array();
                if (!empty($new_indice_reference) && date('Y', strtotime($bail[0]['bail_date_premiere_indexation'])) <= $annee) {
                    $data_index['bail_id'] = $bail[0]['bail_id'];
                    $data_index['etat_indexation'] = 1;
                    $data_index['indlo_indice_base'] = $indice_base['indval_id'];
                    $data_index['indlo_indice_reference'] = $new_indice_reference['indval_id'];
                    $data_index['indlo_ref_loyer'] = 0;

                    $array_variation = array();

                    if ($indice[0]['mode_calcul'] == 1) {
                        $variation_loyer = round(((($new_indice_reference['indval_valeur'] - $indice_base['indval_valeur']) / $indice_base['indval_valeur']) * 100), 2);
                    } else {
                        $variation_loyer = $new_indice_reference['indval_valeur'];
                    }

                    array_push($array_variation, $variation_loyer);

                    if ($bail[0]['bail_var_plafonnee'] != 0) {
                        $variation_plafonnee = (floatval($bail[0]['bail_var_plafonnee']) * $variation_loyer) / 100;
                        array_push($array_variation, $variation_plafonnee);
                    }
                    if ($bail[0]['bail_var_capee'] != 0) {
                        $variation_capee = floatval($bail[0]['bail_var_capee']);
                        array_push($array_variation, $variation_capee);
                    }

                    $variation_appliquee = round(min($array_variation), 2);

                    if (floatval($bail[0]['bail_var_min']) != 0 && floatval($bail[0]['bail_var_min']) > floatval($variation_appliquee)) {
                        $variation_appliquee = floatval($bail[0]['bail_var_min']);
                    }

                    if ($bail[0]['indice_valeur_plafonnement'] != 0) {
                        $indice_plafonne = executeQuery(getIndice($bail[0]['indice_valeur_plafonnement']), $pdo);

                        if ($indice_plafonne[0]['indloyer_periode'] == "Annuelle") {
                            $indice_base_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $indice_base['indval_annee']), $pdo);
                            $new_indice_reference_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $annee), $pdo);
                        } else {
                            $indice_base_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $indice_base['indval_annee'], $indice_base['indval_trimestre']), $pdo);
                            $new_indice_reference_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $new_indice_reference['indval_annee'], $new_indice_reference['indval_trimestre']), $pdo);
                        }

                        if ($indice_plafonne[0]['mode_calcul'] == 1) {
                            $variation_loyer_plafonnee = round(((($new_indice_reference_plafonne[0]['indval_valeur'] - $indice_base_plafonne[0]['indval_valeur']) / $indice_base_plafonne[0]['indval_valeur']) * 100), 2);
                        } else {
                            $variation_loyer_plafonnee = $new_indice_reference_plafonne[0]['indval_valeur'];
                        }

                        $variation_appliquee = round(min($variation_loyer_plafonnee, $variation_appliquee), 2);
                    }

                    $data_index['indlo_variation_appliquee'] = $variation_appliquee;
                    $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                    $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index['indlo_appartement_ht'] + (($last_index['indlo_appartement_ht'] * $variation_appliquee) / 100), 2);
                    $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index['indlo_parking_ht'] + (($last_index['indlo_parking_ht'] * $variation_appliquee) / 100), 2);
                    $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index['indlo_charge_ht'] + (($last_index['indlo_charge_ht'] * $variation_appliquee) / 100), 2);

                    $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index['indlo_appartement_tva'];
                    $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index['indlo_parking_tva'];
                    $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index['indlo_charge_tva'];

                    $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                    $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                    $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                    $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                    $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                    $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                    $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                    $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));
                    $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                    $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                    $request = insertQuery(insertIndexatons($data_index), $pdo);
                } else {
                    if ($annee <= date('Y', strtotime($bail->bail_date_premiere_indexation))) {
                        $indice_base = getIndiceBase($bail[0]['bail_id'], $pdo);

                        if ($indice[0]['indloyer_periode'] == "Annuelle") {
                            $new_indice_reference = getIndiceReferenceAnnuelle($indloyer_id, ($annee), $pdo);
                        } else {
                            $new_indice_reference = getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base['indval_trimestre'], $pdo);
                        }

                        $data = executeQuery(getSingleIndexation($bail[0]['bail_id']), $pdo);
                        $data = $data[0];

                        $data['indlo_indice_base'] = $indice_base['indval_id'];
                        $data['indlo_indice_reference'] = $new_indice_reference['indval_id'];

                        $array_variation = array();

                        if ($indice[0]['mode_calcul'] == 1) {
                            $variation_loyer = round(((($new_indice_reference['indval_valeur'] - $indice_base['indval_valeur']) / $indice_base['indval_valeur']) * 100), 2);
                        } else {
                            $variation_loyer = $new_indice_reference['indval_valeur'];
                        }

                        array_push($array_variation, $variation_loyer);

                        if ($bail[0]['bail_var_plafonnee'] != 0) {
                            $variation_plafonnee = (floatval($bail[0]['bail_var_plafonnee']) * $variation_loyer) / 100;
                            array_push($array_variation, $variation_plafonnee);
                        }
                        if ($bail[0]['bail_var_capee'] != 0) {
                            $variation_capee = floatval($bail[0]['bail_var_capee']);
                            array_push($array_variation, $variation_capee);
                        }

                        $variation_appliquee = round(min($array_variation), 2);

                        if (floatval($bail[0]['bail_var_min']) != 0 && floatval($bail[0]['bail_var_min']) > floatval($variation_appliquee)) {
                            $variation_appliquee = floatval($bail[0]['bail_var_min']);
                        }

                        $data['indlo_variation_appliquee'] = $variation_appliquee;
                        $data['indlo_variation_loyer'] = round($variation_loyer, 2);

                        $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                        $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));

                        unset($data['indlo_id']);
                        $data['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                        $data['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                        $data['indlo_ref_loyer'] = 0;
                        $data['indlo_indince_non_publie'] = 1;

                        $request = insertQuery(insertIndexatons($data), $pdo);
                    }
                }
            }
        } else {
            for ($annee = $start_year; $annee <= $end_year; $annee++) {
                $indice_base = getIndiceBase($bail[0]['bail_id'], $pdo);

                if ($indice[0]['indloyer_periode'] == "Annuelle") {
                    $new_indice_reference = getIndiceReferenceAnnuelle($indloyer_id, ($annee), $pdo);
                } else {
                    $new_indice_reference = getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base['indval_trimestre'], $pdo);
                }

                $last_index = getLastIndexation($bail[0]['bail_id'], $pdo);
                $data_index = array();

                if (!empty($new_indice_reference) && date('Y', strtotime($bail[0]['bail_date_premiere_indexation'])) <= $annee) {
                    $data_index['bail_id'] = $bail[0]['bail_id'];
                    $data_index['etat_indexation'] = 1;
                    $data_index['indlo_indice_base'] = $indice_base['indval_id'];
                    $data_index['indlo_indice_reference'] = $new_indice_reference['indval_id'];
                    $data_index['indlo_ref_loyer'] = 0;

                    $array_variation = array();

                    if ($bail[0]['bail_mode_calcul'] == 0) {
                        if ($indice[0]['mode_calcul'] == 1) {
                            $variation_loyer = round(((($new_indice_reference['indval_valeur'] - $indice_base['indval_valeur']) / $indice_base['indval_valeur']) * 100), 2);
                        } else {
                            $variation_loyer = $new_indice_reference['indval_valeur'];
                        }
                    } else {
                        $new_indice_reference1 = executeQuery(getNewreference($indloyer_id, $indice_base['indval_annee'], $indice_base['indval_trimestre']), $pdo);

                        if (empty($new_indice_reference1)) {
                            break;
                        }

                        $new_indice_reference2 = executeQuery(getNewreference($indloyer_id, $new_indice_reference1[0]['indval_annee'], $new_indice_reference1[0]['indval_trimestre']), $pdo);

                        if (empty($new_indice_reference2)) {
                            break;
                        }

                        if ($indice[0]['mode_calcul'] == 1) {
                            $first_year_variation = round(((($new_indice_reference1[0]['indval_valeur'] - $indice_base['indval_valeur']) / $indice_base['indval_valeur']) * 100), 2);
                            $second_year_variation = round(((($new_indice_reference2[0]['indval_valeur'] - $new_indice_reference1[0]['indval_valeur']) / $new_indice_reference1[0]['indval_valeur']) * 100), 2);
                            $third_year_variation = round(((($new_indice_reference['indval_valeur'] - $new_indice_reference2[0]['indval_valeur']) / $new_indice_reference2[0]['indval_valeur']) * 100), 2);
                        } else {
                            $first_year_variation = $new_indice_reference1[0]['indval_valeur'];
                            $second_year_variation = $new_indice_reference2[0]['indval_valeur'];
                            $third_year_variation = $new_indice_reference['indval_valeur'];
                        }

                        $variation_loyer = round((($first_year_variation + $second_year_variation + $third_year_variation) / 3), 2);
                    }

                    array_push($array_variation, $variation_loyer);
                    if ($bail[0]['bail_var_plafonnee'] != 0) {
                        $variation_plafonnee = (floatval($bail[0]['bail_var_plafonnee']) * $variation_loyer) / 100;
                        array_push($array_variation, $variation_plafonnee);
                    }
                    if ($bail[0]['bail_var_capee'] != 0) {
                        $variation_capee = floatval($bail[0]['bail_var_capee']);
                        array_push($array_variation, $variation_capee);
                    }

                    $variation_appliquee = round(min($array_variation), 2);

                    if (floatval($bail[0]['bail_var_min']) != 0 && floatval($bail[0]['bail_var_min']) > floatval($variation_appliquee)) {
                        $variation_appliquee = floatval($bail[0]['bail_var_min']);
                    }

                    if ($bail[0]['indice_valeur_plafonnement'] != 0) {
                        $indice_plafonne = executeQuery(getIndice($bail[0]['indice_valeur_plafonnement']), $pdo);

                        if ($indice_plafonne[0]['indloyer_periode'] == "Annuelle") {
                            $indice_base_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $indice_base['indval_annee']), $pdo);
                            $new_indice_reference_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $annee), $pdo);
                        } else {
                            $indice_base_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $indice_base['indval_annee'], $indice_base['indval_trimestre']), $pdo);
                            $new_indice_reference_plafonne = executeQuery(getIndiceValeurPlafonne($bail[0]['indice_valeur_plafonnement'], $new_indice_reference['indval_annee'], $new_indice_reference['indval_trimestre']), $pdo);
                        }

                        if ($bail[0]['bail_mode_calcul'] == 0) {
                            if ($indice_plafonne[0]['mode_calcul'] == 1) {
                                $variation_loyer_plafonnee = round(((($new_indice_reference_plafonne[0]['indval_valeur'] - $indice_base_plafonne[0]['indval_valeur']) / $indice_base_plafonne[0]['indval_valeur']) * 100), 2);
                            } else {
                                $variation_loyer_plafonnee = $new_indice_reference_plafonne[0]['indval_valeur'];
                            }
                        } else {
                            $new_indice_reference1 = executeQuery(getNewreference($bail[0]['indice_valeur_plafonnement'], $indice_plafonne[0]['indval_annee'], $indice_plafonne[0]['indval_trimestre']), $pdo);

                            if (empty($new_indice_reference1)) {
                                break;
                            }

                            $new_indice_reference2 = executeQuery(getNewreference($bail[0]['indice_valeur_plafonnement'], $new_indice_reference1[0]['indval_annee'], $new_indice_reference1[0]['indval_trimestre']), $pdo);

                            if (empty($new_indice_reference2)) {
                                break;
                            }

                            if ($indice_plafonne[0]['mode_calcul'] == 1) {
                                $first_year_variation = round(((($new_indice_reference1[0]['indval_valeur'] - $indice_base_plafonne[0]['indval_valeur']) / $indice_base_plafonne[0]['indval_valeur']) * 100), 2);
                                $second_year_variation = round(((($new_indice_reference2[0]['indval_valeur'] - $new_indice_reference1[0]['indval_valeur']) / $new_indice_reference1[0]['indval_valeur']) * 100), 2);
                                $third_year_variation = round(((($new_indice_reference_plafonne[0]['indval_valeur'] - $new_indice_reference2[0]['indval_valeur']) / $new_indice_reference2[0]['indval_valeur']) * 100), 2);
                            } else {
                                $first_year_variation = $new_indice_reference_plafonne[0]['indval_valeur'];
                                $second_year_variation = $new_indice_reference1[0]['indval_valeur'];
                                $third_year_variation = $new_indice_reference2[0]['indval_valeur'];
                            }

                            $variation_loyer = round((($first_year_variation + $second_year_variation + $third_year_variation) / 3), 2);
                        }

                        $variation_appliquee = min($variation_loyer_plafonnee, $variation_appliquee);
                    }

                    $data_index['indlo_variation_appliquee'] = $variation_appliquee;
                    $data_index['indlo_variation_loyer'] = round($variation_loyer, 2);

                    $data_index['indlo_appartement_ht'] = round($indlo_appartement_ht =  $last_index['indlo_appartement_ht'] + (($last_index['indlo_appartement_ht'] * $variation_appliquee) / 100), 2);
                    $data_index['indlo_parking_ht'] = round($indlo_parking_ht = $last_index['indlo_parking_ht'] + (($last_index['indlo_parking_ht'] * $variation_appliquee) / 100), 2);
                    $data_index['indlo_charge_ht'] = round($indlo_charge_ht = $last_index['indlo_charge_ht'] + (($last_index['indlo_charge_ht'] * $variation_appliquee) / 100), 2);

                    $data_index['indlo_appartement_tva'] = $indlo_appartement_tva = $last_index['indlo_appartement_tva'];
                    $data_index['indlo_parking_tva'] = $indlo_parking_tva = $last_index['indlo_parking_tva'];
                    $data_index['indlo_charge_tva'] = $indlo_charge_tva = $last_index['indlo_charge_tva'];

                    $app_tva = ($indlo_appartement_ht * $indlo_appartement_tva) / 100;
                    $park_tva = ($indlo_parking_ht * $indlo_parking_tva) / 100;
                    $charge_tva = ($indlo_charge_ht * $indlo_charge_tva) / 100;

                    $data_index['indlo_appartement_ttc'] =  round($indlo_appartement_ht, 2) + round($app_tva, 2);
                    $data_index['indlo_parking_ttc'] = round($indlo_parking_ht, 2) + round($park_tva, 2);
                    $data_index['indlo_charge_ttc'] = round($indlo_charge_ht, 2) + round($charge_tva, 2);

                    $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                    $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));
                    $data_index['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                    $data_index['indlo_fin'] = date('Y-m-d', strtotime(($annee + 3) . '-' . $month . '-' . ($day - 1)));

                    $request = insertQuery(insertIndexatons($data_index), $pdo);

                    $annee += 2;
                } else {
                    if ($annee <= date('Y', strtotime($bail->bail_date_premiere_indexation))) {

                        $indice_base = getIndiceBase($bail[0]['bail_id'], $pdo);

                        if ($indice[0]['indloyer_periode'] == "Annuelle") {
                            $new_indice_reference = getIndiceReferenceAnnuelle($indloyer_id, ($annee), $pdo);
                        } else {
                            $new_indice_reference = getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month_of_indexation, $indice_base['indval_trimestre'], $pdo);
                        }

                        $data = executeQuery(getSingleIndexation($bail[0]['bail_id']), $pdo);
                        $data = $data[0];

                        $data['indlo_indice_base'] = $indice_base['indval_id'];
                        $data['indlo_indice_reference'] = $new_indice_reference['indval_id'];

                        $array_variation = array();

                        if ($indice[0]['mode_calcul'] == 1) {
                            $variation_loyer = round(((($new_indice_reference['indval_valeur'] - $indice_base['indval_valeur']) / $indice_base['indval_valeur']) * 100), 2);
                        } else {
                            $variation_loyer = $new_indice_reference['indval_valeur'];
                        }

                        array_push($array_variation, $variation_loyer);

                        if ($bail[0]['bail_var_plafonnee'] != 0) {
                            $variation_plafonnee = (floatval($bail[0]['bail_var_plafonnee']) * $variation_loyer) / 100;
                            array_push($array_variation, $variation_plafonnee);
                        }
                        if ($bail[0]['bail_var_capee'] != 0) {
                            $variation_capee = floatval($bail[0]['bail_var_capee']);
                            array_push($array_variation, $variation_capee);
                        }

                        $variation_appliquee = round(min($array_variation), 2);

                        if (floatval($bail[0]['bail_var_min']) != 0 && floatval($bail[0]['bail_var_min']) > floatval($variation_appliquee)) {
                            $variation_appliquee = floatval($bail[0]['bail_var_min']);
                        }

                        $data['indlo_variation_appliquee'] = $variation_appliquee;
                        $data['indlo_variation_loyer'] = round($variation_loyer, 2);

                        $month = date('m', strtotime($last_indexation_infirst['indlo_debut']));
                        $day = date('d', strtotime($last_indexation_infirst['indlo_debut']));

                        unset($data['indlo_id']);
                        $data['indlo_debut'] = date('Y-m-d', strtotime($annee . '-' . $month . '-' . $day));
                        $data['indlo_fin'] = date('Y-m-d', strtotime(($annee + 1) . '-' . $month . '-' . ($day - 1)));

                        $data['indlo_ref_loyer'] = 0;
                        $data['indlo_indince_non_publie'] = 1;

                        $request = insertQuery(insertIndexatons($data), $pdo);
                    }
                }
            }
        }
    }

    if (function_exists('getLastIndexation')) {
        $last_indexation_before_reindex2 = getLastIndexation($bail_id, $pdo);
    }

    if (!empty($request)) {
        $countfacture = verifierfacture($bail_id, $pdo);

        if ($countfacture > 0 && $last_indexation_before_reindex['indlo_debut'] != $last_indexation_before_reindex2['indlo_debut']) {
            factureAuto($bail_id, $pdo);
        }
    }
}

function verifierfacture($bail_id, $pdo)
{
    if (function_exists(('getFactureBybail'))) {
        $facture = executeQuery(getFactureBybail($bail_id), $pdo);
    }

    $countfac = 0;

    if (!empty($facture)) {
        $countfac = count($facture);
    }

    return $countfac;
}

function getIndiceReferenceTrimestrielle($indloyer_id, $annee, $month, $indval_trimestre, $pdo)
{
    $table_trimestriel = array(
        'T1' => array(1, 2, 3),
        'T2' => array(4, 5, 6),
        'T3' => array(7, 8, 9),
        'T4' => array(10, 11, 12),
    );

    $result = array_map(function ($key, $trimester) use ($month) {
        if (in_array($month, $trimester)) {
            return $key;
        }
    }, array_keys($table_trimestriel), $table_trimestriel);

    $matchingMonth = array_filter($result);
    $matchingMonth = implode(', ', $matchingMonth);

    if ($matchingMonth > $indval_trimestre) {
        $annee = $annee;
    } else {
        $annee = $annee - 1;
    }

    $indice_reference = executeQuery(getIndiceReferenceTrimestrielleQuery($indloyer_id, $annee, $indval_trimestre), $pdo);
    if (empty($indice_reference)) {
        $indice_reference = $indice_reference;
    } else {
        $indice_reference = $indice_reference[0];
    }
    return $indice_reference;
}

function getIndiceReferenceAnnuelle($indloyer_id, $annee, $pdo)
{
    $indice_reference = executeQuery(getIndiceReferenceAnnuelleQuery($indloyer_id, $annee), $pdo);
    if (empty($indice_reference)) {
        $indice_reference = $indice_reference;
    } else {
        $indice_reference = $indice_reference[0];
    }
    return $indice_reference;
}

function getLastIndexation($bail_id, $pdo)
{
    $last = executeQuery(getLastIndexationQuery($bail_id), $pdo);
    return $last[0];
}

function getIndiceBase($bail_id, $pdo)
{
    $indexation_loyer = executeQuery(getIndexationLoyerById($bail_id), $pdo);
    $indice_base = $indexation_loyer[0]['indlo_indice_base'];

    if (count($indexation_loyer) > 1) {
        $indice_base = $indexation_loyer[count($indexation_loyer) - 1]['indlo_indice_reference'];
    }
    if (empty($indice_base)) {
        $indice_base = $indexation_loyer[0]['indlo_indice_base'];
    }

    $indice = executeQuery(getIndiceValuer($indice_base), $pdo);
    return $indice[0];
}

function factureAuto($bail_id, $pdo)
{
    if (function_exists('getBailwithJoin')) {
        $bail = executeQuery(getBailwithJoin($bail_id), $pdo);
    }

    if (!empty($bail)) {
        $lot_id = $bail[0]['cliLot_id'];
        $pdl_id = $bail[0]['pdl_id'];

        $last_facture = executeQuery(getLastFacture($bail_id), $pdo);
        $addresse_gestionnaire = executeQuery(getGestionnaireByBAil($bail_id), $pdo);
        $lot_facture = executeQuery(getLotBail($lot_id), $pdo);

        $cliLot_mode_paiement = "";
        $cliLot_encaiss_loyer = "";
        $cliLot_iban_client = "";

        if (isset($lot_facture) && !empty($lot_facture)) {
            $cliLot_mode_paiement = $lot_facture[0]['cliLot_mode_paiement'];
            $cliLot_encaiss_loyer = " sur " . $lot_facture[0]['cliLot_encaiss_loyer'];
            $cliLot_iban_client = $lot_facture[0]['cliLot_iban_client'];
        }

        $indexation_loyer = executeQuery(getIndexationReference($bail_id), $pdo);
        $addresse_emetteur = get_emetteur($bail_id, $pdo);
        $liste_annee = array();
        $indlo_debut = (!empty($last_facture[0]['facture_annee'])) ? $last_facture[0]['facture_annee'] : date('Y');
        $indlo_fin = (!empty($indexation_loyer[0]['indlo_fin']) && (intval($indlo_debut) < intval($indexation_loyer[0]['indlo_fin']))) ? date('Y', strtotime($indexation_loyer[0]['indlo_fin'])) : date('Y');

        for ($i = intval($indlo_debut); $i <= intval($indlo_fin); $i++) {
            array_push($liste_annee, $i);
        }

        $countNmbrFact = 0;

        // Tableau mensuel
        if ($pdl_id == 1) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);

                $indexation = array();

                foreach ($list_full_mois as $key => $value) {
                    $indexation[] = array(
                        'debut' => $value['debut'],
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => getindexation($value['fin'], $bail_id, $pdo),
                    );
                }

                foreach ($indexation as $index) {
                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $date_debut =  date('m-Y', strtotime($index['debut']));
                            $debut_indexation = date('m-Y', strtotime($indexation_loyer[0]['indlo_debut']));
                            $date1 = new DateTime($indexation_loyer[0]['indlo_debut']);
                            $date2 = new DateTime($index['fin']);

                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            if ($date_debut === $debut_indexation) {
                                $interval = date_diff($date1, $date2);
                                $diff = intval($interval->format(' %d '));
                                $end_month = date("d", strtotime($index['fin']));
                                $indlo_appartement_ht = round((($table_indexation['indlo_appartement_ht'] * $diff) / $end_month), 2);
                                $indlo_appartement_tva = round((($table_indexation['indlo_appartement_tva'] * $diff) / $end_month), 2);
                                $indlo_appartement_ttc = round((($table_indexation['indlo_appartement_ttc'] * $diff) / $end_month), 2);
                                $indlo_parking_ht = round((($table_indexation['indlo_parking_ht'] * $diff) / $end_month), 2);
                                $indlo_parking_tva = round((($table_indexation['indlo_parking_tva'] * $diff) / $end_month), 2);
                                $indlo_parking_ttc = round((($table_indexation['indlo_parking_ttc'] * $diff) / $end_month), 2);
                            }

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_mens_id'] = intval(date('m', strtotime($index['fin'])));
                            $data['facture_nombre'] = 1;
                            $data['facture_annee'] = date('Y', strtotime($index['fin']));

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFacture($data), $pdo);
                            }

                            if ($request == "success") {
                                $countNmbrFact += 1;
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau trimestriel
        elseif ($pdl_id == 2) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 3 === 0) {
                        $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    $periode_timestre_id = "";

                    $lastFacture = executeQuery(getLastFacture($bail_id), $pdo);

                    if ($lastFacture[0]['periode_timestre_id'] == 4) {
                        $periode_timestre_id = 1;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 1) {
                        $periode_timestre_id = 2;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 2) {
                        $periode_timestre_id = 3;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 3) {
                        $periode_timestre_id = 4;
                    }

                    if (empty($lastFacture))
                        continue;

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_timestre_id'] = $periode_timestre_id;
                            $data['facture_nombre'] = 1;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            $mois = intval(date("m", strtotime($data['fact_loyer_date'])) - 1);
                            $moislast = intval(date('m', strtotime($last_facture[0]['fact_loyer_date'])));

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date'] && $mois != $moislast) {
                                $request = insertQuery(insertFactureTrimestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $countNmbrFact += 1;
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau trimestriel decallée
        elseif ($pdl_id == 5) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $key_value = new DateTime($value . '-' . $bail[0]['premier_mois_trimes'] . '-1');

                //re-order Trimestre
                $less_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                    $debut = new DateTime($d['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                    $debut = new DateTime($d['debut']);
                    return $debut >= $key_value;
                });
                $reordered_data = array_merge($greater_than_data, $less_than_data);

                $indexation = array();
                foreach ($reordered_data as $key => $value) {
                    if (($key + 1) % 3 === 0) {
                        $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                $listTimestre = array(
                    array("nom" => "T1", "value" => $indexation[0]['nom'], "debut" => $indexation[0]['debut'], "fin" => $indexation[0]['fin']),
                    array("nom" => "T2", "value" => $indexation[1]['nom'], "debut" => $indexation[1]['debut'], "fin" => $indexation[1]['fin']),
                    array("nom" => "T3", "value" => $indexation[2]['nom'], "debut" => $indexation[2]['debut'], "fin" => $indexation[2]['fin']),
                    array("nom" => "T4", "value" => $indexation[3]['nom'], "debut" => $indexation[3]['debut'], "fin" => $indexation[3]['fin']),
                );

                //re-order Header
                $less_than_data_listTimestre = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_listTimestre  = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut >= $key_value;
                });
                $data_listTimestre = array_merge($less_than_data_listTimestre, $greater_than_data_listTimestre);

                //re-order indexations
                $less_than_data_indexation = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_indexation  = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut >= $key_value;
                });

                $data_indexation = array_merge($less_than_data_indexation, $greater_than_data_indexation);

                foreach ($data_indexation as $index) {
                    $periode_timestre_id = "";
                    foreach ($data_listTimestre as $key_listTimestre => $value_listTimestre) {
                        if ($value_listTimestre['value'] == $index['nom']) {
                            if ($value_listTimestre['nom'] == "T1") {
                                $periode_timestre_id = 1;
                            }
                            if ($value_listTimestre['nom'] == "T2") {
                                $periode_timestre_id = 2;
                            }
                            if ($value_listTimestre['nom'] == "T3") {
                                $periode_timestre_id = 3;
                            }
                            if ($value_listTimestre['nom'] == "T4") {
                                $periode_timestre_id = 4;
                            }
                        }
                    }

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_timestre_id'] = $periode_timestre_id;
                            $data['facture_nombre'] = 1;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            $mois = intval(date("m", strtotime($data['fact_loyer_date'])) - 1);
                            $moislast = intval(date('m', strtotime($last_facture[0]['fact_loyer_date'])));

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date'] && $mois != $moislast) {
                                $request = insertQuery(insertFactureTrimestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $countNmbrFact += 1;
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau semestriel
        elseif ($pdl_id == 3) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 6 === 0) {
                        $debut = date('Y-m-d', strtotime('-5 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    $periode_semestre_id = "";

                    $lastFacture = executeQuery(getLastFacture($bail_id), $pdo);

                    if ($lastFacture[0]['periode_semestre_id'] == 1) {
                        $periode_semestre_id = 2;
                    }

                    if ($lastFacture[0]['periode_semestre_id'] == 2) {
                        $periode_semestre_id = 1;
                    }

                    // if (empty($lastFacture)) {
                    //     $periode_semestre_id = 1;
                    // }

                    if (empty($lastFacture))
                        continue;

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];
                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_semestre_id'] = $periode_semestre_id;
                            $data['facture_nombre'] = 1;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFactureSemestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $countNmbrFact += 1;
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau annuel
        elseif ($pdl_id == 4) {
            $periode_annee = executeQuery(getPeriodeAnuelle(), $pdo);

            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 12 === 0) {
                        $debut = date('Y-m-d', strtotime('-11 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    foreach ($periode_annee as $key_periode_annee => $value_periode_annee) {
                        if (intval($value_periode_annee['periode_annee_libelle']) == date('Y', strtotime($index['fin']))) {
                            $periode_annee_id = $value_periode_annee['periode_annee_id'];
                        }
                    }

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];
                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_annee_id'] = $periode_annee_id;
                            $data['facture_nombre'] = 1;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFactureAnnuelle($data), $pdo);
                            }

                            if ($request == "success") {
                                $countNmbrFact += 1;
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        if ($countNmbrFact > 0) {
            $last_lot_index = executeQuery(getLastLotIndex($bail_id), $pdo);
            $last_lot_index_id = $last_lot_index[0]['lot_index_id'];
            $data_lot_index = array(
                'etat_facture' => 1,
                'date_creation_facture' => date('Y-m-d:H:i:s'),
                'nmbr_facture' => $countNmbrFact
            );
            $req = updatetQuery(updateLotIndexe($last_lot_index_id, $data_lot_index), $pdo);
        }
    }
}

function generer_facture($get_facture, $pdo)
{
    if (!is_null($get_facture[0]['periode_mens_id'])) {
        $_data['facture'] = $facture = get_mensuel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_mens_libelle'] . '_' . $facture['emetteur_nom'];
        $view = "/views_pdf/views/export_facture_mensuel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } elseif (!is_null($get_facture[0]['periode_timestre_id'])) {
        $_data['facture'] = $facture = get_trimestriel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $donnee = explode("(", $facture['echeance_paiement']);
        $donnee_final = explode(")", $donnee[1]);
        $date = explode("=>", $facture['echeance_paiement']);
        $moment_factu = $donnee_final[0];
        if ($moment_factu == "échu") {
            $date_str = trim($date[1]);
            $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
            $new_date_str = $date_obj->format('Y-m-d');
            $debut_trim = date('m-Y', strtotime('-3 months', strtotime($new_date_str)));
            $fin_trim = date('m-Y', strtotime('-1 months', strtotime($new_date_str)));
            $_data['debut_trim'] = $debut_trim;
            $_data['fin_trim'] = $fin_trim;
        } else {
            $date_str = trim($date[1]);
            $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
            $new_date_str = $date_obj->format('Y-m-d');
            $debut_trim = date('m-Y', strtotime('+0 months', strtotime($new_date_str)));
            $fin_trim = date('m-Y', strtotime('+2 months', strtotime($new_date_str)));
            $_data['debut_trim'] = $debut_trim;
            $_data['fin_trim'] = $fin_trim;
        }
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_trimesrte_libelle'] . '_' . $facture['emetteur_nom'];

        $view = $get_facture[0]['pdl_id'] == 2 ? "/views_pdf/views/export_facture_trimestre.php" : "/views_pdf/views/export_facture_trimestre_decale.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } elseif (!is_null($get_facture[0]['periode_semestre_id'])) {
        $_data['facture'] = $facture = get_semestriel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_semestre_libelle'] . '_' . $facture['emetteur_nom'];
        $view = "/views_pdf/views/export_facture_semestriel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } else {
        $_data['facture'] = $facture = get_annuel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_annee_libelle'] . '_' . $facture['emetteur_nom'];

        $view = "/views_pdf/views/export_facture_annuel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    }

    $rib = executeQuery(getRib(), $pdo);
    $_data['rib'] = $rib[0];

    exporation_pdf_cron($view, $file_name, $_data, $cliLot_id, $annee, $get_facture[0]['fact_loyer_id'], $pdo);
}

function exporation_pdf_cron($view_file, $filename, $data_ = null, $cliLot_id, $annee, $fact_loyer_id = null, $pdo)
{
    $mpdf = new Mpdf\Mpdf();
    $data = $data_;

    ob_start();
    include(__DIR__ . $view_file);
    $html = ob_get_clean();

    // $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');

    $mpdf->WriteHTML($html);

    $string = str_replace('-', '_', $filename);
    $pdf_save_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    $url = $pdf_save_name . '.pdf';

    $url_path = "/../../../documents/clients/lots/Loyer/$annee/$cliLot_id";
    $url_doc = "../documents/clients/lots/Loyer/$annee/$cliLot_id/$url";

    makeDirPath(__DIR__ . $url_path);

    $data = array(
        'doc_loyer_nom' => $filename,
        'doc_loyer_path' => str_replace('\\', '/', $url_doc),
        'doc_loyer_creation' => date('Y-m-d H:i:s'),
        'doc_loyer_etat' => 1,
        'doc_loyer_annee' => $annee,
        'cliLot_id' => $cliLot_id,
        'util_id' => 1,
        'fact_loyer_id' => $fact_loyer_id
    );

    $req = insertQuery(insertPDf($data), $pdo);

    $url_file = $url_path . "/" . $pdf_save_name . ".pdf";

    try {
        $mpdf->Output(__DIR__ . $url_file, "F");
    } catch (Mpdf\MpdfException $e) {
        echo 'Erreur mPDF : ' . $e->getMessage();
        echo $e->getTraceAsString();
    }
}

function makeDirPath($path)
{
    return file_exists($path) || mkdir($path, 0777, true);
}

function get_mensuel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureMensAdded($bail_id, $fact_loyer_id), $pdo);

    return $get_facture[0];
}

function get_trimestriel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureTrimAdded($bail_id, $fact_loyer_id), $pdo);

    return $get_facture[0];
}

function get_semestriel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureSemAdded($bail_id, $fact_loyer_id), $pdo);
    return $get_facture[0];
}

function get_annuel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureAnnuelleAdded($bail_id, $fact_loyer_id), $pdo);
    return $get_facture[0];
}

function getindexation_month($date_debut, $date_fin, $bail_id, $pdo)
{
    $data_indexation = executeQuery(getIndexationOther($bail_id), $pdo);

    $interval = array();
    foreach ($data_indexation as $key => $value) {
        $interval[] = array(
            'id' => $value['indlo_id'],
            'debut' => $value['indlo_debut'],
            'fin' => $value['indlo_fin']
        );
    }

    $value_index = null;
    foreach ($interval as $key => $value) {
        if (verify_dateBetween_month($date_debut, $date_fin, $value['debut'], $value['fin'])) {
            $currentIndexation = $data_indexation[$key];
            $value_index = array();
            array_push($value_index, $currentIndexation);
        }
    }
    return $value_index;
}

function verify_dateBetween_month($post_debut, $post_fin, $date_debut, $date_fin)
{
    $post_debut = date('Y-m-d', strtotime($post_debut));
    $post_fin = date('Y-m-d', strtotime($post_fin));
    $contractDateBegin = date('Y-m-d', strtotime($date_debut));
    $contractDateEnd = date('Y-m-d', strtotime($date_fin));

    if (($post_debut >= $contractDateBegin) && ($post_fin <= $contractDateEnd)) return true;
    if (($contractDateBegin <= $post_fin) && ($post_fin <= $contractDateEnd)) return true;

    return false;
}

function get_numero_factu($bail_id, $pdo)
{
    $res = executeQuery(getBailWithDossier($bail_id), $pdo);
    $facture_dossier = executeQuery(getDossierFacture($res[0]['dossier_id']), $pdo);

    if (empty($facture_dossier)) {
        $facture_num_suivant = 1;
    } else {
        $num_factu_precedent = explode("-", $facture_dossier[0]['fact_loyer_num']);
        $facture_num_suivant = intval($num_factu_precedent[1]) + 1;
    }
    return $facture_num_suivant;
}

function getindexation($date, $bail_id, $pdo)
{
    $data_indexation = executeQuery(getIndexationOther($bail_id), $pdo);

    $interval = array();
    foreach ($data_indexation as $key => $value) {
        $interval[] = array(
            'id' => $value['indlo_id'],
            'debut' => $value['indlo_debut'],
            'fin' => $value['indlo_fin']
        );
    }

    $value_index = null;

    foreach ($interval as $key => $value) {
        if (verify_dateBetween($date, $value['debut'], $value['fin'])) {
            $currentIndexation = $data_indexation[$key];
            $value_index = array();
            array_push($value_index, $currentIndexation);
        }
    }
    return $value_index;
}

function verify_dateBetween($post, $date_debut, $date_fin)
{
    $post = date('Y-m-d', strtotime($post));
    $contractDateBegin = date('Y-m-d', strtotime($date_debut));
    $contractDateEnd = date('Y-m-d', strtotime($date_fin));
    return (($post >= $contractDateBegin) && ($post <= $contractDateEnd)) ? true : false;
}

function premierJourMois($annee, $mois)
{
    return date("Y-m-d", strtotime($mois . '/01/' . $annee . ' 00:00:00'));
}

function dernierJourMois($annee, $mois)
{
    return date("Y-m-d", strtotime('-1 second', strtotime('+1 month', strtotime($mois . '/01/' . $annee . ' 00:00:00'))));
}

function nomMois($mois, $full = false)
{
    $months = ["", "Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sept", "Oct", "Nov", "Déc"];
    $fullMonths = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
    $listeMois = null;
    if ($full) {
        $listeMois = $fullMonths;
    } else {
        $listeMois = $months;
    }

    $index = (intval($mois)) ? intval($mois) : 0;
    return $listeMois[$index];
}

function genererListeMoisDansAnnee_cron($annee, $full = false)
{
    $listeMois = array();

    if (!intval($annee)) {
        return null;
    }

    for ($numeroMois = 1; $numeroMois < 13; $numeroMois++) {
        $mois["debut"] = premierJourMois($annee, $numeroMois);
        $mois["fin"] = dernierJourMois($annee, $numeroMois);
        $mois["nom"] = nomMois($numeroMois, $full);

        array_push($listeMois, $mois);
    }

    return $listeMois;
}

function get_emetteur($bail_id, $pdo)
{
    $get_emet =  executeQuery(getBailEmeteur($bail_id), $pdo);

    $array_client = explode(',', $get_emet[0]['client_id']);
    $data['emetteur'] = NULL;

    foreach ($array_client as $key => $value) {
        $client = executeQuery(getclientQuery($value), $pdo);

        if (!empty($client)) {

            if (!empty($data['emetteur'])) {
                $data['emetteur'] .= ' & ';
            }
            $data['emetteur'] .= $client[0]['client_nom'] . ' ' . $client[0]['client_prenom'];
        }
    }

    $data['adress1'] = $get_emet[0]['dossier_adresse1'];
    $data['adress2'] = $get_emet[0]['dossier_adresse2'];
    $data['adress3'] = $get_emet[0]['dossier_adresse3'];
    $data['cp'] = $get_emet[0]['dossier_cp'];
    $data['ville'] = $get_emet[0]['dossier_ville'];
    $data['pays'] = $get_emet[0]['dossier_pays'];

    if ($get_emet[0]['dossier_type_adresse'] == 'Lot principal') {
        $get_program = executeQuery(getProgram($get_emet[0]['progm_id']), $pdo);

        if ($get_program[0]['progm_id'] == 309) {
            $data['adress1'] = $get_emet[0]['cliLot_adresse1'];
            $data['adress2'] = $get_emet[0]['cliLot_adresse2'];
            $data['adress3'] = $get_emet[0]['cliLot_adresse3'];
            $data['cp'] = $get_emet[0]['cliLot_cp'];
            $data['ville'] = $get_emet[0]['cliLot_ville'];
            $data['pays'] = $get_emet[0]['cliLot_pays'];
        } else {
            $data['adress1'] = $get_program[0]['progm_adresse1'];
            $data['adress2'] = $get_program[0]['progm_adresse2'];
            $data['adress3'] = $get_program[0]['progm_adresse3'];
            $data['cp'] = $get_program[0]['progm_cp'];
            $data['ville'] = $get_program[0]['progm_ville'];
            $data['pays'] = $get_program[0]['progm_pays'];
        }
    }

    return $data;
}

/********** *************/

function getFacture($dbRequest)
{
    $params_lot = array(
        'distinct' => true,
        'clause' => array('c_facture_automatique.etat' => 0),
        'join' => array(
            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_facture_automatique.gestionaire_id',
            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_automatique.cliLot_id',
            'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
            'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
            'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_gestionnaire.gestionnaire_id',
        ),
        'columns' =>  array(
            'fact_auto_id',
            'c_facture_automatique.cliLot_id',
            'c_facture_automatique.gestionaire_id',
            'gestionnaire_nom',
            'cnctGestionnaire_email1',
            'cnctGestionnaire_email2',
            'cliLot_num',
            'cliLot_mode_paiement',
            'cliLot_encaiss_loyer',
            'cliLot_iban_client',
            'cliLot_bic_client',
            'c_dossier.dossier_id',
            'c_dossier.util_id',
            'client_nom',
            'client_prenom',
            'client_email',
            'progm_nom',
            'progm_adresse1',
            'progm_ville',
            'progm_cp',
            'progm_pays',
        )
    );
    // liste lot
    $request =  $dbRequest->select('c_facture_automatique', $params_lot);
    $IdFacture_auto = array_map(function ($row) {
        return intval($row['fact_auto_id']);
    }, $request);

    // all document loyer (facture)
    $params_fac = array(
        'clause' => array(
            'c_facture_automatique.etat' => 0,
        ),
        'join' => array(
            'c_document_loyer' => 'c_document_loyer.fact_loyer_id = c_facture_automatique.fact_loyer_id',
        ),
        'columns' =>  array(
            'c_facture_automatique.cliLot_id',
            'c_document_loyer.fact_loyer_id',
            'doc_loyer_path',
            'doc_loyer_nom',
            'doc_loyer_annee',
        )
    );
    $allfac =  $dbRequest->select('c_facture_automatique', $params_fac);

    // charger clientelle
    $allChargeClient = getUtilisateur($dbRequest);
    foreach ($request as &$data_fac) {
        $util_id = (!empty($data_fac['util_id'])) ? $data_fac['util_id'] : 2;
        $filtre_chargeClientel = array_filter($allChargeClient, function ($clientel) use ($util_id) {
            return $clientel['util_id'] == $util_id;
        });
        $chargeClientel = array_values($filtre_chargeClientel);
        $data_fac['charge_clientel'] = $chargeClientel[0];

        $cliLot_id = $data_fac['cliLot_id'];
        $filter_doc_loyer = array_filter($allfac, function ($doc) use ($cliLot_id) {
            return $doc['cliLot_id'] == $cliLot_id;
        });
        $data_fac['documents_facture'] = array_values($filter_doc_loyer);
    }

    // contenu 
    $contenu = getMailContent($dbRequest);
    // preparer donner
    $formatData = prepareMailjetParameters($request, $contenu);
    // envoyer mail
    $emailResult = sendEmailViaMailjet($formatData);

    if (!empty($emailResult) && isset($emailResult['success']) && $emailResult['success'] == true) {
        if (!empty($IdFacture_auto)) {
            $value_update = [];
            foreach ($IdFacture_auto as $value) {
                $value_update[] = ' fact_auto_id  = ' . $value . ' THEN 1 ';
            }
            $value_update = implode(' WHEN ', $value_update);
            $clauseSet = '(' . implode(',', $IdFacture_auto) . ')';
            $query = "UPDATE c_facture_automatique SET etat = CASE WHEN $value_update ELSE etat END WHERE fact_auto_id IN  $clauseSet;";
            $request_update = $dbRequest->_executeRequest($query);
            echo 'Succes send mailjet';
            if ($request_update) {
                echo 'Succes send mailjet';
            } else {
                echo 'Error update db';
            }
        }
    } else {
        echo 'Error send mailjet';
    }
}

/***********  *********/

function getUtilisateur($dbRequest)
{
    $request =  $dbRequest->select(
        'c_utilisateur',
        array(
            'columns' => array('util_nom', 'util_prenom', 'util_mail', 'util_id')
        )
    );
    return $request;
}

/***********  *********/

function getMailContent($dbRequest)
{
    $params_get = array(
        'clause' => array('pmail_id ' => 19),
        'method' => "row"
    );
    $request =  $dbRequest->select('c_param_modelemail', $params_get);
    return $request;
}

/********* ********/


function sendEmailViaMailjet($mailjetParams)
{
    $mailjet = new MailjetService();
    $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
    $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
    return $response;
}

function prepareMailjetParameters($data, $content)
{
    $mailjetParams = [];
    foreach ($data as $recipient) {
        $array_adresseMaildestinataire = [];

        if (isEmailValid($recipient['client_email'])) {
            $adresse_mail = [
                'Email' => $recipient['client_email'],
                'Name' => $recipient['client_nom'] . ' ' . $recipient['client_prenom']
            ];
            $array_adresseMaildestinataire[] = $adresse_mail;
        }

        if (isEmailValid($recipient['cnctGestionnaire_email2'])) {
            if (!empty($recipient['cnctGestionnaire_email2'])) {
                $adresse_mail1 = [
                    'Email' => $recipient['client_email1'],
                    'Name' => $recipient['client_nom'] . ' ' . $recipient['client_prenom']
                ];
                $array_adresseMaildestinataire[] = $adresse_mail1;
            }
        }

        if (!empty($array_adresseMaildestinataire)) {
            $param = [
                'From' => [
                    'Email' => $recipient['charge_clientel']['util_mail'],
                    'Name' => $recipient['charge_clientel']['util_nom'] . ' ' . $recipient['charge_clientel']['util_prenom'],
                ],
                'To' => $array_adresseMaildestinataire,
                'Subject' => $content['pmail_libelle'] . ' de la résidence ' . $recipient['progm_nom']
            ];

            $contenu = $content['pmail_contenu'];
            $contenu = str_replace('@nom_client', $recipient['client_nom'], $contenu);
            $contenu = str_replace('@prenom_client', $recipient['client_prenom'], $contenu);
            $contenu = str_replace('@nom_programme', $recipient['progm_nom'], $contenu);
            $contenu = str_replace('@date_envoi_mail', date('d/m/Y'), $contenu);

            $param['HTMLPart'] = '<p>' . $contenu . '</p>';
            $param['TextPart'] = $contenu;

            $formattedAttachments = [];
            if (isset($recipient['documents_facture']) && !empty($recipient['documents_facture'])) {
                foreach ($recipient['documents_facture'] as $attachment) {
                    $file_path = __DIR__ . '/../../' . $attachment['doc_loyer_path'];
                    if (file_exists($file_path)) {
                        if (ini_get('allow_url_fopen')) {
                            $formattedAttachment = [
                                'ContentType' => mime_content_type($file_path),
                                'Filename' => basename($file_path),
                                'Base64Content' => base64_encode(file_get_contents($file_path)),
                            ];
                            $formattedAttachments[] = $formattedAttachment;
                        }
                    }
                }
            }
            $param['Attachments'] = $formattedAttachments;
            $mailjetParams[] = $param;
        }
    }
    return $mailjetParams;
}

function isEmailValid($email = '')
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}
