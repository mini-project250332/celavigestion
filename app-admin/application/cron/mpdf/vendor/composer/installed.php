<?php return array(
    'root' => array(
        'name' => 'kbs1/mpdf-5.7-php7',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'b8cf90dc4444330478c0c2347754a823675a697e',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'kbs1/mpdf-5.7-php7' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'b8cf90dc4444330478c0c2347754a823675a697e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
