<?php 

class DbRequest {
    private $dbConnection;

    public function __construct(DbConnector $dbConnection) {
        $this->dbConnection = $dbConnection;
    }

    public function insert($table, $data) {
        $columns = implode(", ", array_keys($data));
        $values = ":" . implode(", :", array_keys($data));

        $query = "INSERT INTO $table ($columns) VALUES ($values)";

        try {
            $statement = $this->dbConnection->getConnection()->prepare($query);
            $statement->execute($data);
            //echo "Record inserted successfully";
        } catch (PDOException $e) {
            die("Insert failed: " . $e->getMessage());
        }
    }

    /*
        $params_get = array(
            'distinct' => FALSE,
            'clause' => NULL,
            'or_clause' => NULL,
            'like' => NULL,
            'or_like' => NULL,
            'method' => "result",
            'columns' => "*" ,
            'join' => NULL,
            'order_by_columns' => NULL,
            'limit' => NULL, // array('offset' => Int , 'limit' => Int) // Int
            'join_orientation' => NULL 
        );
    */

    public function executeQuery($query,$method = null){
        try {
            $method = isset($params_get['method']) ? $params_get['method'] : "result";
            $statement = $this->dbConnection->getConnection()->query($query);

            switch ($method) {
                case 'result':
                    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
                    return $result;
                case 'row':
                    $result = $statement->fetch(PDO::FETCH_ASSOC);
                    return $result;
                case 'count':
                    $result = $statement->rowCount();
                    return $result;
                default:
                    throw new InvalidArgumentException("Invalid method specified in params_get.");
            }
        } catch (PDOException $e) {
            die("Select failed: " . $e->getMessage());
        }
    }

    public function select($table, $params_get = null) {
        //$columns = isset($params_get['columns']) ?  $params_get['columns'] : "*";

        $_columns = "*";
        if ( isset($params_get['columns']) ){ 
            $columns = $params_get['columns'];
            $_columns = ""; 
            if ( is_array( $columns ) ){
                $count_columns = count( $columns );
                $i = 0;
                foreach( $columns as $col ) {
                    $_columns .= $col;
                    $i++;
                    if( $i < $count_columns ) $_columns .= ","; 
                }
            } else {
                $_columns = $columns;
            }
        }

        $distinct = isset($params_get['distinct']) && $params_get['distinct'] ? "DISTINCT" : "";

        $query = "SELECT $distinct $_columns FROM $table";

        // Join tables
        if (isset($params_get['join'])) {
            $_join_tables = $params_get['join'];
            if (isset($params_get['join_orientation'])) {
                $_join_orient = $params_get['join_orientation'];
            } else {
                $_join_orient = NULL;
            }
            if (is_array($_join_tables)) {
                foreach ($_join_tables as $joinTable => $onCondition) {
                    if (!is_null($_join_orient)) {
                        $query .= " INNER JOIN $joinTable ON $onCondition $_join_orient";
                    } else {
                        $query .= " INNER JOIN $joinTable ON $onCondition";
                    }
                }
            }
        }

        // Where clause
        if (isset($params_get['clause']) && is_array($params_get['clause'])) {
            $query .= " WHERE ";
            $query .= $this->buildWhereClause($params_get['clause']);
        }

        // OR Where clause
        if (isset($params_get['or_clause']) && is_array($params_get['or_clause'])) {
            $query .= isset($params_get['clause']) ? " OR " : " WHERE ";
            $query .= $this->buildWhereClause($params_get['or_clause']);
        }

        // LIKE
        if (isset($params_get['like']) && is_array($params_get['like'])) {
            $query .= isset($params_get['clause']) || isset($params_get['or_clause']) ? " AND " : " WHERE ";
            $query .= $this->buildLikeClause($params_get['like']);
        }

        // OR LIKE
        if (isset($params_get['or_like']) && is_array($params_get['or_like'])) {
            $query .= isset($params_get['clause']) || isset($params_get['or_clause']) || isset($params_get['like']) ? " OR " : " WHERE ";
            $query .= $this->buildLikeClause($params_get['or_like']);
        }

        // Order By
        if (isset($params_get['order_by_columns'])) {
            $query .= " ORDER BY " . $params_get['order_by_columns'];
        }

        // Limit
        if (isset($params_get['limit'])) {
            if (is_array($params_get['limit'])) {
                $offset = isset($params_get['limit']['offset']) ? $params_get['limit']['offset'] : 0;
                $limit = isset($params_get['limit']['limit']) ? $params_get['limit']['limit'] : 10;
                $query .= " LIMIT $offset, $limit";
            } else {
                $query .= " LIMIT " . $params_get['limit'];
            }
        }

        // echo $query;die;

        try {
            $method = isset($params_get['method']) ? $params_get['method'] : "result";
            $statement = $this->dbConnection->getConnection()->query($query);

            switch ($method) {
                case 'result':
                    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
                    return $result;
                case 'row':
                    $result = $statement->fetch(PDO::FETCH_ASSOC);
                    return $result;
                case 'count':
                    $result = $statement->rowCount();
                    return $result;
                default:
                    throw new InvalidArgumentException("Invalid method specified in params_get.");
            }
        } catch (PDOException $e) {
            die("Select failed: " . $e->getMessage());
        }
    }

    private function buildWhereClause($conditions) {
        $clause = "";
        foreach ($conditions as $field => $value) {
            if ($clause !== "") {
                $clause .= " AND ";
            }
            $clause .= "$field = '$value'";
        }
        return $clause;
    }

    private function buildLikeClause($conditions) {
        $clause = "";
        foreach ($conditions as $field => $value) {
            if ($clause !== "") {
                $clause .= " AND ";
            }
            $clause .= "$field LIKE '%$value%'";
        }
        return $clause;
    }

    /*** ****/

    public function update($table, $data, $condition) {
        $set = [];
        foreach ($data as $key => $value) {
            $set[] = "$key = :$key";
        }

        $setClause = implode(", ", $set);

        foreach ($condition as $key => $value) {
            $set[] = "$key = :$key";
        }

        $query = "UPDATE $table SET $setClause WHERE $condition";

        try {
            $statement = $this->dbConnection->getConnection()->prepare($query);
            $statement->execute($data);
            echo "Record updated successfully";
        } catch (PDOException $e) {
            die("Update failed: " . $e->getMessage());
        }
    }

    public function _executeRequest($query) {
        try {
            $statement = $this->dbConnection->getConnection()->prepare($query);
            $result = $statement->execute();
            return $result;
        } catch (PDOException $e) {
            die("Update failed: " . $e->getMessage());
        }
    }


    public function delete($table, $condition) {
        $query = "DELETE FROM $table WHERE $condition";

        try {
            $statement = $this->dbConnection->getConnection()->exec($query);
            echo "Record deleted successfully";
        } catch (PDOException $e) {
            die("Delete failed: " . $e->getMessage());
        }
    }
}