<?php

class DbConnector {
    private $host;
    private $dbname;
    private $username;
    private $password;
    private $connection;

    public function __construct($host, $dbname, $username, $password) {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->username = $username;
        $this->password = $password;

        $this->connect();
    }

    private function connect() {
        $dsn = "mysql:host={$this->host};dbname={$this->dbname}";
        try {
            $this->connection = new PDO($dsn, $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->exec("SET NAMES 'utf8'");
            //echo "Connected to the database successfully";
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    public function getConnection() {
        return $this->connection;
    }

    public function closeConnection() {
        $this->connection = null;
    }
}


/*
    // Example of usage
    $host = "localhost";
    $dbname = "c_estlaviedb";
    $username = "root";
    $password = "";

    $databaseConnection = new DbConnector($host, $dbname, $username, $password);

    // To get the PDO connection object
    $pdoConnection = $databaseConnection->getConnection();

    // Perform database operations using $pdoConnection

    // Don't forget to close the connection when done
    $databaseConnection->closeConnection();
*/

?>