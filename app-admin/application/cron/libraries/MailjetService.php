<?php 
// Chemin vers le fichier d'autoloading de Composer
require_once __DIR__ . '/../../../../vendor/autoload.php';
use \Mailjet\Resources;

class MailjetService {

	private $api_Key;
	private $api_Secret;
	private $mj;

	public function __construct() {}

    public function init($apiKey, $apiSecret){
    	$this->api_Key = $apiKey;
		$this->api_Secret = $apiSecret;
		// init mailjet 
        $this->mj = new \Mailjet\Client($apiKey, $apiSecret,true,['version' => 'v3']);
    }
 
    /****** function send mail param body *******/

    public function sendEmailBody($body){
        try {
            $response = $this->mj->post(Resources::$Email, ['body' => $body]);
            return ['success' => $response->success(), 'data' => $response->getData()];
        } catch (\Exception $e) {
            // Gérer les erreurs ici
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**** ******/

    public function getMailResources($params = array()){
        /* 
            Exemple : $params
            -----------------
            $filters = [
                'Limit'=>40,  // default is 10, max is 1000
                'Offset'=>20,
                'Sort'=>'ArrivedAt DESC',
                'Contact'=>$contact->ID,
                'showSubject'=>true
            ];
        */
        try {
            $response = $this->mj->get(Resources::$Message, $params);
            return ['success' => $response->success(), 'data' => $response->getData()];
        } catch (\Exception $e) {
            // Gérer les erreurs ici
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    public function getMailResourcesBy_id($id){
        $response = $this->mj->get(Resources::$Message, ['id' => $id]);
        return ['success' => $response->success(), 'data' => $response->getData()]; 
    }

    /***** version 3.1 ******/
    public function init_v3_1($apiKey, $apiSecret){
        $this->api_Key = $apiKey;
        $this->api_Secret = $apiSecret;
        $this->mj = new \Mailjet\Client($apiKey, $apiSecret,true,['version' => 'v3.1']);
    }
    
}