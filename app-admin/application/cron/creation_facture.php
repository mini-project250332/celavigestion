<?php

// error_reporting(E_ALL);
// ini_set('display_errors', '1');

require_once  __DIR__ . '/inc/db.php';
require_once  __DIR__ . '/func/facturLoyer.php';
require_once  __DIR__ . '/mpdf/Mpdf.php';

require_once  __DIR__ . '/libraries/DbConnector.php';
require_once  __DIR__ . '/libraries/DbRequest.php';
require_once  __DIR__ . '/libraries/MailjetService.php';


if (function_exists('getListeLotfacture')) {
    $listeLot = executeQuery(getListeLotfacture(), $pdo);
}

$listbail = array();

foreach ($listeLot as $key => $value) {
    $bail = executeQuery(getBail_id($value['cliLot_id']), $pdo);

    if (!empty($bail[0])) {
        array_push($listbail, $bail[0]);
    }
}

if (!empty($listbail)) {

    foreach ($listbail as $key => $value) {
        factureAuto($value['bail_id'], $pdo);
    }

    $databaseConnection = new DbConnector(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    $dbRequest = new DbRequest($databaseConnection);

    getFacture($dbRequest);
}


function factureAuto($bail_id, $pdo)
{
    if (function_exists('getBailwithJoin')) {
        $bail = executeQuery(getBailwithJoin($bail_id), $pdo);
    }

    if (!empty($bail)) {
        $lot_id = $bail[0]['cliLot_id'];
        $pdl_id = $bail[0]['pdl_id'];

        $last_facture = executeQuery(getLastFacture($bail_id), $pdo);

        $addresse_gestionnaire = executeQuery(getGestionnaireByBAil($bail_id), $pdo);

        $lot_facture = executeQuery(getLotBail($lot_id), $pdo);

        $cliLot_mode_paiement = "";
        $cliLot_encaiss_loyer = "";
        $cliLot_iban_client = "";

        if (isset($lot_facture) && !empty($lot_facture)) {
            $cliLot_mode_paiement = $lot_facture[0]['cliLot_mode_paiement'];
            $cliLot_encaiss_loyer = " sur " . $lot_facture[0]['cliLot_encaiss_loyer'];
            $cliLot_iban_client = $lot_facture[0]['cliLot_iban_client'];
        }

        $indexation_loyer = executeQuery(getIndexationReference($bail_id), $pdo);

        $addresse_emetteur = get_emetteur($bail_id, $pdo);

        $liste_annee = array();

        $indlo_debut = (!empty($last_facture[0]['facture_annee'])) ? $last_facture[0]['facture_annee'] : date('Y');
        // $indlo_debut = '2023';
        $indlo_fin = (!empty($indexation_loyer[0]['indlo_fin']) && (intval($indlo_debut) < intval($indexation_loyer[0]['indlo_fin']))) ? date('Y', strtotime($indexation_loyer[0]['indlo_fin'])) : date('Y');
        // $indlo_fin = '2024';

        for ($i = intval($indlo_debut); $i <= intval($indlo_fin); $i++) {
            array_push($liste_annee, $i);
        }

        //Tableau mensuel
        if ($pdl_id == 1) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);

                $indexation = array();

                foreach ($list_full_mois as $key => $value) {
                    $indexation[] = array(
                        'debut' => $value['debut'],
                        'fin' => $value['fin'],
                        'nom' => $value['nom'],
                        'indexation' => getindexation($value['fin'], $bail_id, $pdo),
                    );
                }

                foreach ($indexation as $index) {
                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $date_debut =  date('m-Y', strtotime($index['debut']));
                            $debut_indexation = date('m-Y', strtotime($indexation_loyer[0]['indlo_debut']));
                            $date1 = new DateTime($indexation_loyer[0]['indlo_debut']);
                            $date2 = new DateTime($index['fin']);

                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            if ($date_debut === $debut_indexation) {
                                $interval = date_diff($date1, $date2);
                                $diff = intval($interval->format(' %d '));
                                $end_month = date("d", strtotime($index['fin']));
                                $indlo_appartement_ht = round((($table_indexation['indlo_appartement_ht'] * $diff) / $end_month), 2);
                                $indlo_appartement_tva = round((($table_indexation['indlo_appartement_tva'] * $diff) / $end_month), 2);
                                $indlo_appartement_ttc = round((($table_indexation['indlo_appartement_ttc'] * $diff) / $end_month), 2);
                                $indlo_parking_ht = round((($table_indexation['indlo_parking_ht'] * $diff) / $end_month), 2);
                                $indlo_parking_tva = round((($table_indexation['indlo_parking_tva'] * $diff) / $end_month), 2);
                                $indlo_parking_ttc = round((($table_indexation['indlo_parking_ttc'] * $diff) / $end_month), 2);
                            }

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;
                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_mens_id'] = intval(date('m', strtotime($index['fin'])));
                            $data['facture_nombre'] = 1;
                            $data['facture_annee'] = date('Y', strtotime($index['fin']));

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['fin']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFacture($data), $pdo);
                            }

                            if ($request == "success") {
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                $last_facture_auto = executeQuery(getDonnefactureAuto($bail_id), $pdo);

                                $data_facture_auto = array(
                                    'cliLot_id' => $last_facture_auto[0]['cliLot_id'],
                                    'fact_loyer_id' => $last_facture_auto[0]['fact_loyer_id'],
                                    'date_facture' => $data['fact_loyer_date'],
                                    'gestionaire_id' => $last_facture_auto[0]['gestionnaire_id'],
                                    'etat' => $bail[0]['envoi_loye_gest'] == 1 ? 0 : 1,
                                    'etat_proprietaire' => 0
                                );

                                insertQuery(insertfactureAuto($data_facture_auto), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau trimestriel
        elseif ($pdl_id == 2) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 3 === 0) {
                        $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    $periode_timestre_id = "";

                    $lastFacture = executeQuery(getLastFacture($bail_id), $pdo);

                    if ($lastFacture[0]['periode_timestre_id'] == 4) {
                        $periode_timestre_id = 1;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 1) {
                        $periode_timestre_id = 2;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 2) {
                        $periode_timestre_id = 3;
                    } elseif ($lastFacture[0]['periode_timestre_id'] == 3) {
                        $periode_timestre_id = 4;
                    }

                    if (empty($lastFacture))
                        continue;

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_timestre_id'] = $periode_timestre_id;
                            $data['facture_nombre'] = 1;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            $mois = intval(date("m", strtotime($data['fact_loyer_date'])) - 1);
                            $moislast = intval(date('m', strtotime($last_facture[0]['fact_loyer_date'])));

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date'] && $mois != $moislast) {
                                $request = insertQuery(insertFactureTrimestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                $last_facture_auto = executeQuery(getDonnefactureAuto($bail_id), $pdo);

                                $data_facture_auto = array(
                                    'cliLot_id' => $last_facture_auto[0]['cliLot_id'],
                                    'fact_loyer_id' => $last_facture_auto[0]['fact_loyer_id'],
                                    'date_facture' => $data['fact_loyer_date'],
                                    'gestionaire_id' => $last_facture_auto[0]['gestionnaire_id'],
                                    'etat' => $bail[0]['envoi_loye_gest'] == 1 ? 0 : 1,
                                    'etat_proprietaire' => 0
                                );

                                insertQuery(insertfactureAuto($data_facture_auto), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau trimestriel decallée
        elseif ($pdl_id == 5) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $key_value = new DateTime($value . '-' . $bail[0]['premier_mois_trimes'] . '-1');

                //re-order Trimestre
                $less_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                    $debut = new DateTime($d['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data = array_filter($list_full_mois, function ($d) use ($key_value) {
                    $debut = new DateTime($d['debut']);
                    return $debut >= $key_value;
                });
                $reordered_data = array_merge($greater_than_data, $less_than_data);

                $indexation = array();
                foreach ($reordered_data as $key => $value) {
                    if (($key + 1) % 3 === 0) {
                        $debut = date('Y-m-d', strtotime('-2 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                $listTimestre = array(
                    array("nom" => "T1", "value" => $indexation[0]['nom'], "debut" => $indexation[0]['debut'], "fin" => $indexation[0]['fin']),
                    array("nom" => "T2", "value" => $indexation[1]['nom'], "debut" => $indexation[1]['debut'], "fin" => $indexation[1]['fin']),
                    array("nom" => "T3", "value" => $indexation[2]['nom'], "debut" => $indexation[2]['debut'], "fin" => $indexation[2]['fin']),
                    array("nom" => "T4", "value" => $indexation[3]['nom'], "debut" => $indexation[3]['debut'], "fin" => $indexation[3]['fin']),
                );

                //re-order Header
                $less_than_data_listTimestre = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_listTimestre  = array_filter($listTimestre, function ($t) use ($key_value) {
                    $debut = new DateTime($t['debut']);
                    return $debut >= $key_value;
                });
                $data_listTimestre = array_merge($less_than_data_listTimestre, $greater_than_data_listTimestre);

                //re-order indexations
                $less_than_data_indexation = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut < $key_value;
                });
                $greater_than_data_indexation  = array_filter($indexation, function ($i) use ($key_value) {
                    $debut = new DateTime($i['debut']);
                    return $debut >= $key_value;
                });

                $data_indexation = array_merge($less_than_data_indexation, $greater_than_data_indexation);

                foreach ($data_indexation as $index) {
                    $periode_timestre_id = "";
                    foreach ($data_listTimestre as $key_listTimestre => $value_listTimestre) {
                        if ($value_listTimestre['value'] == $index['nom']) {
                            if ($value_listTimestre['nom'] == "T1") {
                                $periode_timestre_id = 1;
                            }
                            if ($value_listTimestre['nom'] == "T2") {
                                $periode_timestre_id = 2;
                            }
                            if ($value_listTimestre['nom'] == "T3") {
                                $periode_timestre_id = 3;
                            }
                            if ($value_listTimestre['nom'] == "T4") {
                                $periode_timestre_id = 4;
                            }
                        }
                    }

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];

                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_timestre_id'] = $periode_timestre_id;
                            $data['facture_nombre'] = 1;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            $mois = intval(date("m", strtotime($data['fact_loyer_date'])) - 1);
                            $moislast = intval(date('m', strtotime($last_facture[0]['fact_loyer_date'])));

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date'] && $mois != $moislast) {
                                $request = insertQuery(insertFactureTrimestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                $last_facture_auto = executeQuery(getDonnefactureAuto($bail_id), $pdo);

                                $data_facture_auto = array(
                                    'cliLot_id' => $last_facture_auto[0]['cliLot_id'],
                                    'fact_loyer_id' => $last_facture_auto[0]['fact_loyer_id'],
                                    'date_facture' => $data['fact_loyer_date'],
                                    'gestionaire_id' => $last_facture_auto[0]['gestionnaire_id'],
                                    'etat' => $bail[0]['envoi_loye_gest'] == 1 ? 0 : 1,
                                    'etat_proprietaire' => 0
                                );

                                insertQuery(insertfactureAuto($data_facture_auto), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau semestriel
        elseif ($pdl_id == 3) {
            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 6 === 0) {
                        $debut = date('Y-m-d', strtotime('-5 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    $periode_semestre_id = "";

                    $lastFacture = executeQuery(getLastFacture($bail_id), $pdo);

                    if ($lastFacture[0]['periode_semestre_id'] == 1) {
                        $periode_semestre_id = 2;
                    }

                    if ($lastFacture[0]['periode_semestre_id'] == 2) {
                        $periode_semestre_id = 1;
                    }

                    // if (empty($lastFacture)) {
                    //     $periode_semestre_id = 1;
                    // }

                    if (empty($lastFacture))
                        continue;

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];
                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_semestre_id'] = $periode_semestre_id;
                            $data['facture_nombre'] = 1;

                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFactureSemestriel($data), $pdo);
                            }

                            if ($request == "success") {
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                $last_facture_auto = executeQuery(getDonnefactureAuto($bail_id), $pdo);

                                $data_facture_auto = array(
                                    'cliLot_id' => $last_facture_auto[0]['cliLot_id'],
                                    'fact_loyer_id' => $last_facture_auto[0]['fact_loyer_id'],
                                    'date_facture' => $data['fact_loyer_date'],
                                    'gestionaire_id' => $last_facture_auto[0]['gestionnaire_id'],
                                    'etat' => $bail[0]['envoi_loye_gest'] == 1 ? 0 : 1,
                                    'etat_proprietaire' => 0
                                );

                                insertQuery(insertfactureAuto($data_facture_auto), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }

        // Tableau annuel
        elseif ($pdl_id == 4) {
            $periode_annee = executeQuery(getPeriodeAnuelle(), $pdo);

            foreach ($liste_annee as $key => $value) {
                $list_full_mois = genererListeMoisDansAnnee_cron($value, true);
                $indexation = array();
                foreach ($list_full_mois as $key => $value) {
                    if (($key + 1) % 12 === 0) {
                        $debut = date('Y-m-d', strtotime('-11 months', strtotime($value['debut'])));
                        $indexation[] = array(
                            'debut' => $debut,
                            'fin' => $value['fin'],
                            'nom' => $value['nom'],
                            'indexation' => getindexation_month($debut, $value['fin'], $bail_id, $pdo),
                        );
                    }
                }

                foreach ($indexation as $index) {
                    foreach ($periode_annee as $key_periode_annee => $value_periode_annee) {
                        if (intval($value_periode_annee['periode_annee_libelle']) == date('Y', strtotime($index['fin']))) {
                            $periode_annee_id = $value_periode_annee['periode_annee_id'];
                        }
                    }

                    if (!empty($index['indexation'])) {
                        $data = array();
                        foreach ($index['indexation'] as $table_indexation) {
                            $indlo_appartement_ht = $table_indexation['indlo_appartement_ht'];
                            $indlo_appartement_tva = $table_indexation['indlo_appartement_tva'];
                            $indlo_appartement_ttc = $table_indexation['indlo_appartement_ttc'];
                            $indlo_parking_ht = $table_indexation['indlo_parking_ht'];
                            $indlo_parking_tva = $table_indexation['indlo_parking_tva'];
                            $indlo_parking_ttc = $table_indexation['indlo_parking_ttc'];

                            $franchise = executeQuery(getFranchiseBail($bail_id), $pdo);

                            $data['montant_deduction'] = '';
                            $data['libelle_facture_deduit'] = '';

                            if (!empty($franchise)) {
                                $start_timestamp = strtotime($index['debut']);
                                $end_timestamp = strtotime($index['fin']);

                                foreach ($franchise as $key_franchise => $value_franchise) {
                                    $date_to_check_timestamp_debut = strtotime($value_franchise['franc_debut']);
                                    $date_to_check_timestamp_fin = strtotime($value_franchise['franc_fin']);

                                    if ($date_to_check_timestamp_debut <=  $start_timestamp && $date_to_check_timestamp_fin >= $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_debut >= $start_timestamp && $date_to_check_timestamp_debut <= $end_timestamp && $date_to_check_timestamp_fin > $end_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    } elseif ($date_to_check_timestamp_fin <= $end_timestamp && $date_to_check_timestamp_fin >= $start_timestamp) {
                                        if ($value_franchise['type_deduction'] == 1) {
                                            $data['montant_deduction'] = $indlo_appartement_ht;
                                            $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                            $data['tva_deduit'] = $indlo_appartement_tva;
                                            $indlo_appartement_tva = $indlo_appartement_tva;
                                            $indlo_appartement_ttc = 0;
                                        } else {
                                            if ($value_franchise['franchise_mode_calcul'] == 0) {
                                                $data['montant_deduction'] = $value_franchise['montant_ht'];
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $montant_ttc_deduit = $value_franchise['montant_ttc_deduit'];

                                                $indlo_appartement_ttc = $indlo_appartement_ttc - $montant_ttc_deduit;
                                            } else {
                                                $data['montant_deduction'] = $montant_deduction = round(($indlo_appartement_ht * $value_franchise['franchise_pourcentage'] / 100), 2);
                                                $data['libelle_facture_deduit'] = $value_franchise['franc_explication'];
                                                $data['tva_deduit'] = $value_franchise['tva'];

                                                $indlo_appartement_ttc = $value_franchise['tva'] != 0 ? $indlo_appartement_ttc - (round($montant_deduction + ($montant_deduction * $value_franchise['tva'] / 100), 2)) : $indlo_appartement_ttc - $montant_deduction;
                                            }
                                        }
                                    }
                                }
                            }

                            $data['fact_loyer_app_ht'] = $indlo_appartement_ht;
                            $data['fact_loyer_app_tva'] = $indlo_appartement_tva;
                            $data['fact_loyer_app_ttc'] = $indlo_appartement_ttc;
                            $data['fact_loyer_park_ht'] = $indlo_parking_ht;
                            $data['fact_loyer_park_tva'] = $indlo_parking_tva;
                            $data['fact_loyer_park_ttc'] = $indlo_parking_ttc;

                            $data['fact_loyer_charge_ht'] = $table_indexation['indlo_charge_ht'];
                            $data['fact_loyer_charge_tva'] = $table_indexation['indlo_charge_tva'];
                            $data['fact_loyer_charge_ttc'] = $table_indexation['indlo_charge_ttc'];
                            $data['bail_id'] = $bail[0]['bail_id'];
                            $data['fact_loyer_num'] = date('Y', strtotime($index['fin'])) . '-' . str_pad(get_numero_factu($bail_id, $pdo), 5, '0', STR_PAD_LEFT);
                            $data['periode_annee_id'] = $periode_annee_id;
                            $data['facture_nombre'] = 1;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $data['facture_annee'] = date('Y', strtotime($index['fin']));
                            } else {
                                $data['facture_annee'] = date('Y', strtotime($index['debut']));
                            }

                            $data['dossier_id'] = $bail[0]['dossier_id'];
                            $data['emetteur_nom'] = $addresse_emetteur['emetteur'];
                            $data['emet_adress1'] = $addresse_emetteur['adress1'];
                            $data['emet_adress2'] = $addresse_emetteur['adress2'];
                            $data['emet_adress3'] = $addresse_emetteur['adress3'];
                            $data['emet_cp'] = $addresse_emetteur['cp'];
                            $data['emet_pays'] = !empty($addresse_emetteur['pays'] ? $addresse_emetteur['pays'] : NULL);
                            $data['emet_ville'] = $addresse_emetteur['ville'];

                            $data['dest_nom'] = $addresse_gestionnaire[0]['gestionnaire_nom'];
                            $data['dest_adresse1'] = $addresse_gestionnaire[0]['gestionnaire_adresse1'];
                            $data['dest_adresse2'] = $addresse_gestionnaire[0]['gestionnaire_adresse2'];
                            $data['dest_adresse3'] = $addresse_gestionnaire[0]['gestionnaire_adresse3'];
                            $data['dest_cp'] = $addresse_gestionnaire[0]['gestionnaire_cp'];
                            $data['dest_pays'] = $addresse_gestionnaire[0]['gestionnaire_pays'];
                            $data['dest_ville'] = $addresse_gestionnaire[0]['gestionnaire_ville'];
                            $data['mode_paiement'] = $cliLot_mode_paiement . ' ' . $cliLot_encaiss_loyer . ' ' . $cliLot_iban_client;
                            if (($bail[0]['momfac_id']  == 1)) {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])) + 1, $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['fin'])), date('m', strtotime($index['fin'])), 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            } else {
                                $date = date_create();
                                $fact_loyer_date = date_create();
                                date_date_set($date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])), $bail[0]['tpech_valeur']);
                                date_date_set($fact_loyer_date, date('Y', strtotime($index['debut'])), date('m', strtotime($index['debut'])) - 1, 1);
                                $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date_format($date, "d/m/Y");
                                $data['fact_loyer_date'] = date_format($fact_loyer_date, "Y-m-d");
                                if (intval($bail[0]['tpech_valeur']) == 30 && intval(date('m', strtotime($index['debut']))) == 2) {
                                    $data['echeance_paiement'] = $bail[0]['tpech_valeur'] . ' du mois (' . $bail[0]['momfact_nom'] . ') => ' . date("d/m/Y", strtotime($index['fin']));
                                }
                            }

                            $request = "";

                            // Voir si la date de facture depasse le date du jour
                            if (date('Y-m-d', strtotime($data['fact_loyer_date'])) > date('Y-m-d', strtotime('last day of this month'))) {
                                break;
                            }

                            if ($data['fact_loyer_date'] > $last_facture[0]['fact_loyer_date']) {
                                $request = insertQuery(insertFactureAnnuelle($data), $pdo);
                            }

                            if ($request == "success") {
                                $last_facture_loyer = executeQuery(getLastFacture($bail_id), $pdo);

                                $last_facture_auto = executeQuery(getDonnefactureAuto($bail_id), $pdo);

                                $data_facture_auto = array(
                                    'cliLot_id' => $last_facture_auto[0]['cliLot_id'],
                                    'fact_loyer_id' => $last_facture_auto[0]['fact_loyer_id'],
                                    'date_facture' => $data['fact_loyer_date'],
                                    'gestionaire_id' => $last_facture_auto[0]['gestionnaire_id'],
                                    'etat' => $bail[0]['envoi_loye_gest'] == 1 ? 0 : 1,
                                    'etat_proprietaire' => 0
                                );

                                insertQuery(insertfactureAuto($data_facture_auto), $pdo);

                                generer_facture($last_facture_loyer, $pdo);
                            }
                        }
                    }
                }
            }
        }
    }
}

function generer_facture($get_facture, $pdo)
{
    if (!is_null($get_facture[0]['periode_mens_id'])) {
        $_data['facture'] = $facture = get_mensuel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_mens_libelle'] . '_' . $facture['emetteur_nom'];
        //$view = "./views_pdf/views/export_facture_mensuel.php";
        $view = "/views_pdf/views/export_facture_mensuel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } elseif (!is_null($get_facture[0]['periode_timestre_id'])) {
        $_data['facture'] = $facture = get_trimestriel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $donnee = explode("(", $facture['echeance_paiement']);
        $donnee_final = explode(")", $donnee[1]);
        $date = explode("=>", $facture['echeance_paiement']);
        $moment_factu = $donnee_final[0];
        if ($moment_factu == "échu") {
            $date_str = trim($date[1]);
            $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
            $new_date_str = $date_obj->format('Y-m-d');
            $debut_trim = date('m-Y', strtotime('-3 months', strtotime($new_date_str)));
            $fin_trim = date('m-Y', strtotime('-1 months', strtotime($new_date_str)));
            $_data['debut_trim'] = $debut_trim;
            $_data['fin_trim'] = $fin_trim;
        } else {
            $date_str = trim($date[1]);
            $date_obj = DateTime::createFromFormat('d/m/Y', $date_str);
            $new_date_str = $date_obj->format('Y-m-d');
            $debut_trim = date('m-Y', strtotime('+0 months', strtotime($new_date_str)));
            $fin_trim = date('m-Y', strtotime('+2 months', strtotime($new_date_str)));
            $_data['debut_trim'] = $debut_trim;
            $_data['fin_trim'] = $fin_trim;
        }
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_trimesrte_libelle'] . '_' . $facture['emetteur_nom'];
        //$view = $get_facture[0]['pdl_id'] == 2 ? "./views_pdf/views/export_facture_trimestre.php" : "./views_pdf/views/export_facture_trimestre_decale.php";

        $view = $get_facture[0]['pdl_id'] == 2 ? "/views_pdf/views/export_facture_trimestre.php" : "/views_pdf/views/export_facture_trimestre_decale.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } elseif (!is_null($get_facture[0]['periode_semestre_id'])) {
        $_data['facture'] = $facture = get_semestriel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_semestre_libelle'] . '_' . $facture['emetteur_nom'];
        //$view = "./views_pdf/views/export_facture_semestriel.php";
        $view = "/views_pdf/views/export_facture_semestriel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    } else {
        $_data['facture'] = $facture = get_annuel($get_facture[0]['bail_id'], $get_facture[0]['fact_loyer_id'], $pdo);
        $file_name = $facture['fact_loyer_date'] . '_' . $facture['fact_loyer_num'] . '_' . $facture['periode_annee_libelle'] . '_' . $facture['emetteur_nom'];
        //$view = "./views_pdf/views/export_facture_annuel.php";

        $view = "/views_pdf/views/export_facture_annuel.php";

        $cliLot_id = $facture['cliLot_id'];
        $annee = $facture['facture_annee'];
    }

    $rib = executeQuery(getRib(), $pdo);
    $_data['rib'] = $rib[0];

    exporation_pdf_cron($view, $file_name, $_data, $cliLot_id, $annee, $get_facture[0]['fact_loyer_id'], $pdo);
}

function exporation_pdf_cron($view_file, $filename, $data_ = null, $cliLot_id, $annee, $fact_loyer_id = null, $pdo)
{
    $mpdf = new Mpdf\Mpdf();
    $data = $data_;

    ob_start();
    include(__DIR__ . $view_file);
    $html = ob_get_clean();
    // $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
    $mpdf->WriteHTML($html);

    $string = str_replace('-', '_', $filename);
    $pdf_save_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    $url = $pdf_save_name . '.pdf';

    $url_path = "/../../../documents/clients/lots/Loyer/$annee/$cliLot_id";
    $url_doc = "../documents/clients/lots/Loyer/$annee/$cliLot_id/$url";

    makeDirPath(__DIR__ . $url_path);

    $data = array(
        'doc_loyer_nom' => $filename,
        'doc_loyer_path' => str_replace('\\', '/', $url_doc),
        'doc_loyer_creation' => date('Y-m-d H:i:s'),
        'doc_loyer_etat' => 1,
        'doc_loyer_annee' => $annee,
        'cliLot_id' => $cliLot_id,
        'util_id' => 1,
        'fact_loyer_id' => $fact_loyer_id
    );

    $req = insertQuery(insertPDf($data), $pdo);

    $url_file = $url_path . "/" . $pdf_save_name . ".pdf";

    //$mpdf->Output(__DIR__ .$url_file, "F");

    try {
        $mpdf->Output(__DIR__ . $url_file, "F");
    } catch (Mpdf\MpdfException $e) {
        echo 'Erreur mPDF : ' . $e->getMessage();
    }
}

function makeDirPath($path)
{
    return file_exists($path) || mkdir($path, 0777, true);
}

function get_mensuel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureMensAdded($bail_id, $fact_loyer_id), $pdo);

    return $get_facture[0];
}

function get_trimestriel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureTrimAdded($bail_id, $fact_loyer_id), $pdo);

    return $get_facture[0];
}

function get_semestriel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureSemAdded($bail_id, $fact_loyer_id), $pdo);
    return $get_facture[0];
}

function get_annuel($bail_id, $fact_loyer_id, $pdo)
{
    $get_facture = executeQuery(getFactureAnnuelleAdded($bail_id, $fact_loyer_id), $pdo);
    return $get_facture[0];
}

function getindexation_month($date_debut, $date_fin, $bail_id, $pdo)
{
    $data_indexation = executeQuery(getIndexationOther($bail_id), $pdo);

    $interval = array();
    foreach ($data_indexation as $key => $value) {
        $interval[] = array(
            'id' => $value['indlo_id'],
            'debut' => $value['indlo_debut'],
            'fin' => $value['indlo_fin']
        );
    }

    $value_index = null;
    foreach ($interval as $key => $value) {
        if (verify_dateBetween_month($date_debut, $date_fin, $value['debut'], $value['fin'])) {
            $currentIndexation = $data_indexation[$key];
            $value_index = array();
            array_push($value_index, $currentIndexation);
        }
    }
    return $value_index;
}

function verify_dateBetween_month($post_debut, $post_fin, $date_debut, $date_fin)
{
    $post_debut = date('Y-m-d', strtotime($post_debut));
    $post_fin = date('Y-m-d', strtotime($post_fin));
    $contractDateBegin = date('Y-m-d', strtotime($date_debut));
    $contractDateEnd = date('Y-m-d', strtotime($date_fin));

    if (($post_debut >= $contractDateBegin) && ($post_fin <= $contractDateEnd)) return true;
    if (($contractDateBegin <= $post_fin) && ($post_fin <= $contractDateEnd)) return true;

    return false;
}

function get_numero_factu($bail_id, $pdo)
{
    $res = executeQuery(getBailWithDossier($bail_id), $pdo);
    $facture_dossier = executeQuery(getDossierFacture($res[0]['dossier_id']), $pdo);

    if (empty($facture_dossier)) {
        $facture_num_suivant = 1;
    } else {
        $num_factu_precedent = explode("-", $facture_dossier[0]['fact_loyer_num']);
        $facture_num_suivant = intval($num_factu_precedent[1]) + 1;
    }
    return $facture_num_suivant;
}

function getindexation($date, $bail_id, $pdo)
{
    $data_indexation = executeQuery(getIndexationOther($bail_id), $pdo);

    $interval = array();
    foreach ($data_indexation as $key => $value) {
        $interval[] = array(
            'id' => $value['indlo_id'],
            'debut' => $value['indlo_debut'],
            'fin' => $value['indlo_fin']
        );
    }

    $value_index = null;

    foreach ($interval as $key => $value) {
        if (verify_dateBetween($date, $value['debut'], $value['fin'])) {
            $currentIndexation = $data_indexation[$key];
            $value_index = array();
            array_push($value_index, $currentIndexation);
        }
    }
    return $value_index;
}

function verify_dateBetween($post, $date_debut, $date_fin)
{
    $post = date('Y-m-d', strtotime($post));
    $contractDateBegin = date('Y-m-d', strtotime($date_debut));
    $contractDateEnd = date('Y-m-d', strtotime($date_fin));
    return (($post >= $contractDateBegin) && ($post <= $contractDateEnd)) ? true : false;
}

function premierJourMois($annee, $mois)
{
    return date("Y-m-d", strtotime($mois . '/01/' . $annee . ' 00:00:00'));
}

function dernierJourMois($annee, $mois)
{
    return date("Y-m-d", strtotime('-1 second', strtotime('+1 month', strtotime($mois . '/01/' . $annee . ' 00:00:00'))));
}

function nomMois($mois, $full = false)
{
    $months = ["", "Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sept", "Oct", "Nov", "Déc"];
    $fullMonths = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
    $listeMois = null;
    if ($full) {
        $listeMois = $fullMonths;
    } else {
        $listeMois = $months;
    }

    $index = (intval($mois)) ? intval($mois) : 0;
    return $listeMois[$index];
}

function genererListeMoisDansAnnee_cron($annee, $full = false)
{
    $listeMois = array();

    if (!intval($annee)) {
        return null;
    }

    for ($numeroMois = 1; $numeroMois < 13; $numeroMois++) {
        $mois["debut"] = premierJourMois($annee, $numeroMois);
        $mois["fin"] = dernierJourMois($annee, $numeroMois);
        $mois["nom"] = nomMois($numeroMois, $full);

        array_push($listeMois, $mois);
    }

    return $listeMois;
}

function get_emetteur($bail_id, $pdo)
{
    $get_emet =  executeQuery(getBailEmeteur($bail_id), $pdo);

    $array_client = explode(',', $get_emet[0]['client_id']);
    $data['emetteur'] = NULL;

    foreach ($array_client as $key => $value) {
        $client = executeQuery(getclientQuery($value), $pdo);

        if (!empty($client)) {

            if (!empty($data['emetteur'])) {
                $data['emetteur'] .= ' & ';
            }
            $data['emetteur'] .= $client[0]['client_nom'] . ' ' . $client[0]['client_prenom'];
        }
    }

    $data['adress1'] = $get_emet[0]['dossier_adresse1'];
    $data['adress2'] = $get_emet[0]['dossier_adresse2'];
    $data['adress3'] = $get_emet[0]['dossier_adresse3'];
    $data['cp'] = $get_emet[0]['dossier_cp'];
    $data['ville'] = $get_emet[0]['dossier_ville'];
    $data['pays'] = $get_emet[0]['dossier_pays'];

    if ($get_emet[0]['dossier_type_adresse'] == 'Lot principal') {
        $get_program = executeQuery(getProgram($get_emet[0]['progm_id']), $pdo);

        if ($get_program[0]['progm_id'] == 309) {
            $data['adress1'] = $get_emet[0]['cliLot_adresse1'];
            $data['adress2'] = $get_emet[0]['cliLot_adresse2'];
            $data['adress3'] = $get_emet[0]['cliLot_adresse3'];
            $data['cp'] = $get_emet[0]['cliLot_cp'];
            $data['ville'] = $get_emet[0]['cliLot_ville'];
            $data['pays'] = $get_emet[0]['cliLot_pays'];
        } else {
            $data['adress1'] = $get_program[0]['progm_adresse1'];
            $data['adress2'] = $get_program[0]['progm_adresse2'];
            $data['adress3'] = $get_program[0]['progm_adresse3'];
            $data['cp'] = $get_program[0]['progm_cp'];
            $data['ville'] = $get_program[0]['progm_ville'];
            $data['pays'] = $get_program[0]['progm_pays'];
        }
    }

    return $data;
}

/********** *************/

function getFacture($dbRequest)
{
    $params_lot = array(
        'distinct' => true,
        'clause' => array('c_facture_automatique.etat' => 0),
        'join' => array(
            'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_facture_automatique.gestionaire_id',
            'c_lot_client' => 'c_lot_client.cliLot_id = c_facture_automatique.cliLot_id',
            'c_client' => 'c_client.client_id  = c_lot_client.cli_id',
            'c_programme' => 'c_programme.progm_id  = c_lot_client.progm_id',
            'c_dossier' => 'c_dossier.dossier_id  = c_lot_client.dossier_id',
            'c_contactgestionnaire' => 'c_contactgestionnaire.gestionnaire_id = c_gestionnaire.gestionnaire_id',
        ),
        'columns' =>  array(
            'fact_auto_id',
            'c_facture_automatique.cliLot_id',
            'c_facture_automatique.gestionaire_id',
            'gestionnaire_nom',
            'cnctGestionnaire_email1',
            'cnctGestionnaire_email2',
            'cliLot_num',
            'cliLot_mode_paiement',
            'cliLot_encaiss_loyer',
            'cliLot_iban_client',
            'cliLot_bic_client',
            'c_dossier.dossier_id',
            'c_dossier.util_id',
            'client_nom',
            'client_prenom',
            'client_email',
            'progm_nom',
            'progm_adresse1',
            'progm_ville',
            'progm_cp',
            'progm_pays',
        )
    );
    // liste lot
    $request =  $dbRequest->select('c_facture_automatique', $params_lot);
    $IdFacture_auto = array_map(function ($row) {
        return intval($row['fact_auto_id']);
    }, $request);

    // all document loyer (facture)
    $params_fac = array(
        'clause' => array(
            'c_facture_automatique.etat' => 0,
        ),
        'join' => array(
            'c_document_loyer' => 'c_document_loyer.fact_loyer_id = c_facture_automatique.fact_loyer_id',
        ),
        'columns' =>  array(
            'c_facture_automatique.cliLot_id',
            'c_document_loyer.fact_loyer_id',
            'doc_loyer_path',
            'doc_loyer_nom',
            'doc_loyer_annee',
        )
    );
    $allfac =  $dbRequest->select('c_facture_automatique', $params_fac);

    // charger clientelle
    $allChargeClient = getUtilisateur($dbRequest);
    foreach ($request as &$data_fac) {
        $util_id = (!empty($data_fac['util_id'])) ? $data_fac['util_id'] : 2;
        $filtre_chargeClientel = array_filter($allChargeClient, function ($clientel) use ($util_id) {
            return $clientel['util_id'] == $util_id;
        });
        $chargeClientel = array_values($filtre_chargeClientel);
        $data_fac['charge_clientel'] = $chargeClientel[0];

        $cliLot_id = $data_fac['cliLot_id'];
        $filter_doc_loyer = array_filter($allfac, function ($doc) use ($cliLot_id) {
            return $doc['cliLot_id'] == $cliLot_id;
        });
        $data_fac['documents_facture'] = array_values($filter_doc_loyer);
    }

    // contenu 
    $contenu = getMailContent($dbRequest);
    // preparer donner
    $formatData = prepareMailjetParameters($request, $contenu);
    $segments_data = segmenterDonnees($formatData);

    foreach ($segments_data as $key => $segment) {
        $valeursFactAutoId = [];
        $nouveauSegment = array_map(function ($item) use (&$valeursFactAutoId) {
            $valeursFactAutoId[] = $item["fact_auto_id"];
            unset($item["fact_auto_id"]);
            return $item;
        }, $segment);

        $data_exportJson = array(
            'segment' => $key,
            'fact_auto_id' => $valeursFactAutoId,
        );

        $sendEmail_segment = sendEmailViaMailjet($nouveauSegment);
        if (!empty($sendEmail_segment) && isset($sendEmail_segment['success']) && $sendEmail_segment['success'] == true) {
            $request_statusFactAuto = updateData_factAuto($valeursFactAutoId, $dbRequest);
            if (!$request_statusFactAuto) {
                $data_exportJson['mailjet'] = "L'envoi des données vers Mailjet a été accompli avec succès.";
                $data_exportJson['bd'] = 'Update status etat dans table fact_automatique error';
            }
        } else {
            $data_exportJson['mailjet'] = "L'envoi des données vers Mailjet a rencontré un problème.";
            $data_exportJson['bd'] = 'Update status etat dans table fact_automatique pas encore executé';
        }
        exportToJSON($data_exportJson, "Log_mailGestionnaire_Send_Facture_" . date('Y-m-d') . "_segement_" . $key);
    }
}

function updateData_factAuto($Array_factAutoId, $dbRequest)
{
    $factAutoId_update = [];
    foreach ($Array_factAutoId as $value) {
        $factAutoId_update[] = ' fact_auto_id  = ' . $value . ' THEN 1 ';
    }
    $clause_set = implode(' WHEN ', $factAutoId_update);
    $clause_where = '(' . implode(',', $Array_factAutoId) . ')';
    $query = "UPDATE c_facture_automatique SET etat = CASE WHEN $clause_set ELSE etat END WHERE fact_auto_id IN  $clause_where;";
    return $dbRequest->_executeRequest($query);
}

function segmenterDonnees($data)
{
    $totalObjets = count($data);
    $objetsParSegment = 25;
    $nombreSegments = ceil($totalObjets / $objetsParSegment);
    $dataSegments = [];
    for ($i = 0; $i < $nombreSegments; $i++) {
        $debut = $i * $objetsParSegment;
        $fin = ($i + 1) * $objetsParSegment - 1;
        $segment = array_slice($data, $debut, $objetsParSegment);
        $dataSegments[] = $segment;
    }
    return $dataSegments;
}

function exportToJSON($data, $fichier)
{
    $directoryPath = './logs/';
    if (!is_dir($directoryPath)) {
        mkdir($directoryPath, 0777, true);
    }
    $jsonFilePath = $directoryPath . "$fichier.json";
    $fileHandle = fopen($jsonFilePath, 'w');
    if ($fileHandle !== false) {
        $jsonChunk = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        fwrite($fileHandle, $jsonChunk . PHP_EOL);
        fclose($fileHandle);
    }
}

/***********  *********/

function getUtilisateur($dbRequest)
{
    $request =  $dbRequest->select(
        'c_utilisateur',
        array(
            'columns' => array('util_nom', 'util_prenom', 'util_mail', 'util_id')
        )
    );
    return $request;
}

/***********  *********/

function getMailContent($dbRequest)
{
    $params_get = array(
        'clause' => array('pmail_id ' => 19),
        'method' => "row"
    );
    $request =  $dbRequest->select('c_param_modelemail', $params_get);
    return $request;
}

/********* ********/


function sendEmailViaMailjet($mailjetParams)
{
    $mailjet = new MailjetService();
    $mailjet->init_v3_1(API_KEY_MAILJET, API_SECRET_MAILJET);
    $response = $mailjet->sendEmailBody(['Messages' => $mailjetParams]);
    return $response;
}

function prepareMailjetParameters($data, $content)
{
    $mailjetParams = [];
    foreach ($data as $recipient) {
        $array_adresseMaildestinataire = [];

        if (isEmailValid(trim($recipient['cnctGestionnaire_email1']))) {
            $adresse_mail = [
                'Email' => trim($recipient['cnctGestionnaire_email1']),
                'Name' => $recipient['gestionnaire_nom']
            ];
            $array_adresseMaildestinataire[] = $adresse_mail;
        }

        if (isEmailValid(trim($recipient['cnctGestionnaire_email2']))) {
            if (!empty(trim($recipient['cnctGestionnaire_email2']))) {
                $adresse_mail1 = [
                    'Email' => trim($recipient['cnctGestionnaire_email2']),
                    'Name' => $recipient['gestionnaire_nom']
                ];
                $array_adresseMaildestinataire[] = $adresse_mail1;
            }
        }

        if (!empty($array_adresseMaildestinataire)) {
            $param = [
                'From' => [
                    'Email' => $recipient['charge_clientel']['util_mail'],
                    'Name' => $recipient['charge_clientel']['util_nom'] . ' ' . $recipient['charge_clientel']['util_prenom'],
                ],
                'To' => $array_adresseMaildestinataire,
                'Subject' => $content['pmail_libelle'] . ' de la résidence ' . $recipient['progm_nom']
            ];

            $contenu = $content['pmail_contenu'];
            $contenu = str_replace('@nom_client', $recipient['client_nom'], $contenu);
            $contenu = str_replace('@prenom_client', $recipient['client_prenom'], $contenu);
            $contenu = str_replace('@nom_programme', $recipient['progm_nom'], $contenu);
            $contenu = str_replace('@date_envoi_mail', date('d/m/Y'), $contenu);

            $param['HTMLPart'] = '<p>' . $contenu . '</p>';
            $param['TextPart'] = $contenu;

            $formattedAttachments = [];
            if (isset($recipient['documents_facture']) && !empty($recipient['documents_facture'])) {
                foreach ($recipient['documents_facture'] as $attachment) {
                    $file_path = __DIR__ . '/../../' . $attachment['doc_loyer_path'];
                    if (file_exists($file_path)) {
                        if (ini_get('allow_url_fopen')) {
                            $formattedAttachment = [
                                'ContentType' => mime_content_type($file_path),
                                'Filename' => basename($file_path),
                                'Base64Content' => base64_encode(file_get_contents($file_path)),
                            ];
                            $formattedAttachments[] = $formattedAttachment;
                        }
                    }
                }
            }

            $param['fact_auto_id'] =  $recipient['fact_auto_id'];
            $param['Attachments'] = $formattedAttachments;
            $mailjetParams[] = $param;
        }
    }
    return $mailjetParams;
}

function isEmailValid($email = '')
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}
