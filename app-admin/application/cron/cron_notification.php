<?php

include './inc/db.php';
include './func/indice.php';

require_once '../third_party/PHPMailer/PHPMailer.php';
require_once '../third_party/PHPMailer/SMTP.php';
require_once '../third_party/PHPMailer/Exception.php';

if (function_exists('executeQuery')) {
    $indice = executeQuery($queryIndice, $pdo);
}

if (!empty($indice)) {

    $countIndice = count($indice);
    $data_table_mail = array();

    for ($i = 0; $i < $countIndice; $i++) {

        $indiceperiode = $indice[$i]['indloyer_periode'];

        if ($indiceperiode == "Trimestrielle") {

            $indloyer_id = $indice[$i]['indloyer_id'];

            $date_saisie_t1_jour = $indice[$i]['indloyer_date_saisie_t1_jour'];
            $date_saisie_t1_mois = $indice[$i]['indloyer_date_saisie_t1_mois'];
            $date_saisie_t2_jour = $indice[$i]['indloyer_date_saisie_t2_jour'];
            $date_saisie_t2_mois = $indice[$i]['indloyer_date_saisie_t2_mois'];
            $date_saisie_t3_jour = $indice[$i]['indloyer_date_saisie_t3_jour'];
            $date_saisie_t3_mois = $indice[$i]['indloyer_date_saisie_t3_mois'];
            $date_saisie_t4_jour = $indice[$i]['indloyer_date_saisie_t4_jour'];
            $date_saisie_t4_mois = $indice[$i]['indloyer_date_saisie_t4_mois'];

            $indiceValeurMaxAnnee = executeQuery(getIndiceValeurMaxAnnee($indloyer_id), $pdo);

            if ($indiceValeurMaxAnnee[0]['indval_annee'] != NULL) {
                $Last_indiceValeur = executeQuery(getIndiceValeur($indloyer_id, $indiceValeurMaxAnnee[0]['indval_annee']), $pdo);
                $Last_indiceValeur = $Last_indiceValeur[0];

                $date_now = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . date('m') . '-' . date('d')));

                switch ($Last_indiceValeur['indval_trimestre']) {
                    case 'T2':
                        $date_limite = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . $date_saisie_t3_mois . '-' . $date_saisie_t3_jour));

                        if ($date_limite < $date_now) {
                            array_push($data_table_mail, array(
                                'description' =>  $indice[$i]['indloyer_description'],
                                'trimestre' => 'T3',
                                'annee' => intval($Last_indiceValeur['indval_annee'])
                            ));
                        }
                        break;

                    case 'T3':
                        $date_limite = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . $date_saisie_t4_mois . '-' . $date_saisie_t4_jour));

                        if ($date_limite < $date_now) {
                            array_push($data_table_mail, array(
                                'description' =>  $indice[$i]['indloyer_description'],
                                'trimestre' => 'T4',
                                'annee' => intval($Last_indiceValeur['indval_annee'])
                            ));
                        }
                        break;
                    case 'T4':
                        $date_limite = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) + 1 . '-' . $date_saisie_t1_mois . '-' . $date_saisie_t1_jour));
                        $date_now = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) + 1 . '-' . date('m') . '-' . date('d')));
                        if ($date_limite < $date_now) {
                            array_push($data_table_mail, array(
                                'description' =>  $indice[$i]['indloyer_description'],
                                'trimestre' => 'T1',
                                'annee' => intval($Last_indiceValeur['indval_annee']) + 1
                            ));
                        }
                        break;

                    default:
                        $date_limite = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . $date_saisie_t2_mois . '-' . $date_saisie_t2_jour));

                        if ($date_limite < $date_now) {
                            array_push($data_table_mail, array(
                                'description' =>  $indice[$i]['indloyer_description'],
                                'trimestre' => 'T2',
                                'annee' => intval($Last_indiceValeur['indval_annee'])
                            ));
                        }
                        break;
                }
            }
        } else {
            $indloyer_id = $indice[$i]['indloyer_id'];

            $date_saisie_an_jour = $indice[$i]['indloyer_date_saisie_an_jour'];
            $date_saisie_an_mois = $indice[$i]['indloyer_date_saisie_an_mois'];

            $indiceValeurMaxAnnee = executeQuery(getIndiceValeurMaxAnnee($indloyer_id), $pdo);

            if ($indiceValeurMaxAnnee[0]['indval_annee'] != NULL) {
                $Last_indiceValeur = executeQuery(getIndiceValeur($indloyer_id, $indiceValeurMaxAnnee[0]['indval_annee']), $pdo);
                $Last_indiceValeur = $Last_indiceValeur[0];

                $date_now = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . date('m') . '-' . date('d')));

                $date_limite_an = date('Y-m-d', strtotime(intval($Last_indiceValeur['indval_annee']) . '-' . $date_saisie_an_mois . '-' . $date_saisie_an_jour));

                if ($date_limite_an < $date_now) {
                    array_push($data_table_mail, array(
                        'description' =>  $indice[$i]['indloyer_description'],
                        'trimestre' => NULL,
                        'annee' => intval($Last_indiceValeur['indval_annee'])
                    ));
                }
            }
        }
    }

    trierDonnee($data_table_mail, $pdo);
}

function trierDonnee($data_indice, $pdo)
{
    if (!empty($data_indice)) {
        $data = array();

        $lettre_trimestrielle = 'Un nouvel indice @Nom_indice a dû être publié, merci de saisir au plus vite la période @periode , année : @annee.<br>';
        $lettre_annuelle =  'Un nouvel indice @Nom_indice a dû être publié, merci de saisir au plus vite l\' année : @annee.<br>';

        $cordialement = '<br>L\'Administration du CELAVigestion';

        $message = 'Bonjour,<br><br>';

        foreach ($data_indice as $key => $value) {
            if ($value['trimestre'] == NULL) {
                $contenu = str_replace('@Nom_indice', $value['description'], $lettre_annuelle);
                $contenu = str_replace('@annee', $value['annee'], $contenu);
                $message .= $contenu;
            } else {
                $contenu = str_replace('@Nom_indice', $value['description'], $lettre_trimestrielle);
                $contenu = str_replace('@annee', $value['annee'], $contenu);
                $contenu = str_replace('@periode', $value['trimestre'], $contenu);
                $message .= $contenu;
            }
        }

        $message .= $cordialement;

        $data['object_mail'] = 'C\'est la Vie - Saisi d\'un nouvel indice';
        $data['contenu_mail'] = $message;

        $data['destinataire'] = "bertrand@celavigestion.fr";
        $data['destinataire_en_copie'] = "anne@celavigestion.fr";

        $defaultUser =  executeQuery(getUserDefault(), $pdo);
        $defaultUser = $defaultUser[0];

        $data['user_label'] = $defaultUser['util_prenom'] . ' ' . $defaultUser['util_nom'];
        $data['user'] = $defaultUser['mess_email'];
        $data['pwd'] = $defaultUser['mess_motdepasse'];
        $data['port'] = $defaultUser['mess_port'];
        $data['auth'] = true;
        $data['host'] = $defaultUser['mess_adressemailserveur'];

        EnvoiMail($data);
    }
}

function EnvoiMail($data)
{
    $mail = new PHPMailer\PHPMailer\PHPMailer();

    try {
        $mail->isSMTP();
        $mail->Host = $data['host'];
        $mail->SMTPAuth = $data['auth'];
        $mail->Username = $data['user'];
        $mail->Password = $data['pwd'];
        $mail->SMTPSecure = 'tls';
        $mail->Port = $data['port'];
        $mail->CharSet = 'UTF-8';
        $mail->setFrom($data['user'], $data['user_label']);
        $mail->addAddress(trim($data['destinataire']));
        $mail->addBCC(trim($data['destinataire_en_copie']));

        if (isset($data['signature'])) {
            $mail->AddEmbeddedImage($data['signature'], $data['sign']);
        }

        $mail->isHTML(true);
        $mail->Subject = mb_encode_mimeheader($data['object_mail'], $mail->CharSet);
        $mail->Body = $data['contenu_mail'];

        if (!$mail->send()) {
            echo 'Error: ' . $mail->ErrorInfo;
        }
    } catch (Exception $e) {
        echo 'Error: ' . $e;
    }
}
