<style type="text/css">
    @page {
        margin: 3%;
    }

    .column {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
    }

    .left {
        width: 60%;
    }

    .right {
        width: 37%;
    }

    .col {
        font-family: Arial, Helvetica, sans-serif;
        float: left;
        padding: 3px;
    }

    .column_left {
        width: 60%;
    }

    .column_right {
        width: 34%;
    }

    .espace {
        width: 3%;
    }

    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    .facture {
        font-size: 19px;
        font-weight: bold;
    }

    .sous_tilte {
        font-size: 13px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .contact {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 100%;
        font-family: Arial, Helvetica, sans-serif;
    }

    th {
        height: 40px;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    td {
        height: 40px;
    }

    .th_designation {
        width: 70%;
        border-left: 1px solid black;
    }

    .th_tva {
        text-align: right;
    }

    .th_total {
        text-align: right;
        border-right: 1px solid black;
    }

    .td_designation {
        font-weight: bold;
    }

    .td_tva {
        text-align: right;
    }

    .td_ht {
        text-align: right;
    }

    .tfoot_designation {
        text-align: center;
        font-weight: bold;
        border-left: 1px solid black;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .tfoot_tva {
        text-align: right;
        font-weight: bold;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .tfoot_ht {
        text-align: right;
        font-weight: bold;
        border-right: 1px solid black;
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        background-color: #Eaeaea;
    }

    .commentaire {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 11px;
    }

    .footer_exp {
        font-family: Arial, Helvetica, sans-serif;
        border: 1px solid black;
        text-align: center;
        width: 70%;
        font-size: 13px;
        margin-left: 15%;
        padding: 1%;
    }

    .foot_bien {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
    }
</style>

<div class="row">
    <div class="column left">
        <span><?= $data['facture']['emetteur_nom'] ?></span><br>
        <span>
            <?= $data['facture']['emet_adress1'] ?>
        </span><br>

        <?php if (!empty($data['facture']['emet_adress2'])) : ?>
            <span>
                <?= $data['facture']['emet_adress2'] ?>
            </span><br>
        <?php endif ?>

        <?php if (!empty($data['facture']['emet_adress3'])) : ?>
            <span>
                <?= $data['facture']['emet_adress3'] ?>
            </span><br>
        <?php endif ?>
        <span><?= $data['facture']['emet_cp'] ?>&nbsp;<?= $data['facture']['emet_ville'] ?></span><br>
        <span>Siret : <?= $data['facture']['dossier_siret'] ?> - APE : <?= $data['facture']['dossier_ape'] ?></span><br>
        <span>Numéro de TVA intracommunautaire :</span><br>
        <span><?= $data['facture']['dossier_tva_intracommunautaire'] ?></span>
    </div>

    <div class="column right">
        <br><br><br>
        <span class="facture">Facture N°&nbsp;<?= $data['facture']['fact_loyer_num'] ?></span>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col column_left">
        <span class="sous_tilte">Adresse du bien : </span><br><br>
        <span><?= $data['facture']['progm_nom'] ?></span><br>
        <span><?= $data['facture']['emet_adress1'] ?></span><br>
        <span><?= $data['facture']['emet_cp'] ?>&nbsp;<?= $data['facture']['emet_ville'] ?></span>
    </div>
    <div class="col espace"></div>
    <div class="col column_right">
        <span class="sous_tilte" style="text-align: center;">Adresse Facturation :</span><br><br>
        <span><?= $data['facture']['dest_nom'] ?></span><br>
        <?php if (!empty($data['facture']['dest_adresse1'])) : ?>
            <span>
                <?= $data['facture']['dest_adresse1'] ?>
            </span><br>
        <?php endif ?>
        <?php if (!empty($data['facture']['dest_adresse2'])) : ?>
            <span>
                <?= $data['facture']['dest_adresse2'] ?>
            </span><br>
        <?php endif ?>
        <?php if (!empty($data['facture']['dest_adresse3'])) : ?>
            <span>
                <?= $data['facture']['dest_adresse3'] ?>
            </span><br>
        <?php endif ?>
        <span><?= $data['facture']['dest_cp'] ?>&nbsp;<?= $data['facture']['dest_ville'] ?></span><br><br><br>
        <span><?= $data['facture']['emet_ville'] ?> , le <?= date("d/m/Y", strtotime($data['facture']['fact_loyer_date'])); ?></span>
    </div>
</div>

<br><br><br>

<div class="contact">
    <span><i><b><u>Votre contact</u>&nbsp;:&nbsp;SARL CELAVIGESTION</b></i></span><br>
    <span><i><b>Parc de Brais 39 route de Fondeline 44600 Saint-Nazaire</b></i></span><br>
    <span><i><b>Tel : <?= $data['facture']['util_phonemobile'] ?> - Email: <?= $data['facture']['util_mail'] ?> </b></i></span>
</div>

<br>
<?php if (!empty($data['facture']['facture_commentaire'])) : ?>
    <div class="commentaire">
        <?= $data['facture']['facture_commentaire'] ?>
    </div>
<?php endif ?>
<br>

<table>
    <thead>
        <tr>
            <th class="th_designation">Désignation</th>
            <th class="th_tva">TVA</th>
            <th class="th_total">Total HT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php
            if ($data['facture']['periode_trimesrte_libelle'] == "T1")
                $periode = "TRIMESTRE 1";
            if ($data['facture']['periode_trimesrte_libelle'] == "T2")
                $periode = "TRIMESTRE 2";
            if ($data['facture']['periode_trimesrte_libelle'] == "T3")
                $periode = "TRIMESTRE 3";
            if ($data['facture']['periode_trimesrte_libelle'] == "T4")
                $periode = "TRIMESTRE 4";
            if ($data['facture']['periode_trimesrte_libelle'] == "Variable")
                $periode = "Variable"; ?>
            <td class="td_designation">LOYER APPARTEMENT HT&nbsp;
                <?= $periode ?>&nbsp;
                <?= $data['facture']['facture_annee'] ?>
            </td>
            <td class="td_tva">
                <?= number_format($data['facture']['fact_loyer_app_tva'], 2, ",", ".") ?> %
            </td>
            <td class="td_ht">
                <?= number_format($data['facture']['fact_loyer_app_ht'], 2, ",", ".") ?> €
            </td>
        </tr>
        <?php if (!empty($data['facture']['fact_loyer_park_ht'])) : ?>
            <tr>
                <?php
                if ($data['facture']['periode_trimesrte_libelle'] == "T1")
                    $periode = "TRIMESTRE 1";
                if ($data['facture']['periode_trimesrte_libelle'] == "T2")
                    $periode = "TRIMESTRE 2";
                if ($data['facture']['periode_trimesrte_libelle'] == "T3")
                    $periode = "TRIMESTRE 3";
                if ($data['facture']['periode_trimesrte_libelle'] == "T4")
                    $periode = "TRIMESTRE 4";
                if ($data['facture']['periode_trimesrte_libelle'] == "Variable")
                    $periode = "Variable"; ?>
                <td class="td_designation">LOYER PARKING HT&nbsp;
                    <?= $periode ?>&nbsp;
                    <?= $data['facture']['facture_annee'] ?>
                </td>
                <td class="td_tva">
                    <?= number_format($data['facture']['fact_loyer_park_tva'], 2, ",", ".") ?> %
                </td>
                <td class="td_ht">
                    <?= number_format($data['facture']['fact_loyer_park_ht'], 2, ",", ".") ?> €
                </td>
            </tr>
        <?php endif ?>
        <?php if (!empty($data['facture']['fact_loyer_charge_ht'])) : ?>
            <tr>
                <?php
                if ($data['facture']['periode_trimesrte_libelle'] == "T1")
                    $periode = "TRIMESTRE 1";
                if ($data['facture']['periode_trimesrte_libelle'] == "T2")
                    $periode = "TRIMESTRE 2";
                if ($data['facture']['periode_trimesrte_libelle'] == "T3")
                    $periode = "TRIMESTRE 3";
                if ($data['facture']['periode_trimesrte_libelle'] == "T4")
                    $periode = "TRIMESTRE 4";
                if ($data['facture']['periode_trimesrte_libelle'] == "Variable")
                    $periode = "Variable"; ?>
                <td class="td_designation">FORFAIT CHARGES HT&nbsp;
                    <?= $periode ?>&nbsp;
                    <?= $data['facture']['facture_annee'] ?>
                </td>
                <td class="td_tva">
                    <?= number_format($data['facture']['fact_loyer_charge_tva'], 2, ",", ".") ?> %
                </td>
                <td class="td_ht">
                    <?= '- ' . number_format($data['facture']['fact_loyer_charge_ht'], 2, ",", ".") ?> €
                </td>
            </tr>
        <?php endif ?>
        <?php if ($data['facture']['montant_deduction'] != NULL) { ?>
            <tr>
                <td class="td_designation"><?= $data['facture']['libelle_facture_deduit'] ?>&nbsp;
                </td>
                <td class="td_tva"><?= $data['facture']['tva_deduit'] != NULL ? number_format($data['facture']['tva_deduit'], 2, ",", ".") . ' %' : '' ?></td>
                <td class="td_ht"><?= '- ' . number_format($data['facture']['montant_deduction'], 2, ",", ".") ?> €</td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <?php $date = explode("=>", $data['facture']['echeance_paiement']); ?>
            <td class="tfoot_designation">Echéance de paiement :
                <?= $date[1] ?>
            </td>
            <td class="tfoot_tva">
                Total HT :<br>
                TVA : <br>
                Total TTC : </td>
            <td class="tfoot_ht">
                <?php if ($data['facture']['montant_deduction'] != NULL) { ?>
                    <?php $total_ht = $data['facture']['fact_loyer_app_ht'] + $data['facture']['fact_loyer_park_ht'] - $data['facture']['fact_loyer_charge_ht'] - $data['facture']['montant_deduction'];
                    $total_ttc = $data['facture']['fact_loyer_app_ttc'] + $data['facture']['fact_loyer_park_ttc'] - $data['facture']['fact_loyer_charge_ttc'];
                    $total_tva = ($data['facture']['fact_loyer_app_ht'] * $data['facture']['fact_loyer_app_tva'] / 100) + ($data['facture']['fact_loyer_park_ht'] * $data['facture']['fact_loyer_park_tva'] / 100) - ($data['facture']['fact_loyer_charge_ht'] * $data['facture']['fact_loyer_charge_tva'] / 100) - ($data['facture']['montant_deduction'] * $data['facture']['tva_deduit']) ?>
                    <?= number_format($total_ht, 2, ",", ".") ?> € <br>
                    <?= number_format($total_tva, 2, ",", ".") ?> €<br>
                    <?= number_format($total_ttc, 2, ",", ".") ?> € <br>
                <?php } else { ?>
                    <?php $total_ht = $data['facture']['fact_loyer_app_ht'] + $data['facture']['fact_loyer_park_ht'] - $data['facture']['fact_loyer_charge_ht'];
                    $total_ttc = $data['facture']['fact_loyer_app_ttc'] + $data['facture']['fact_loyer_park_ttc'] - $data['facture']['fact_loyer_charge_ttc'];
                    $total_tva = ($data['facture']['fact_loyer_app_ht'] * $data['facture']['fact_loyer_app_tva'] / 100) + ($data['facture']['fact_loyer_park_ht'] * $data['facture']['fact_loyer_park_tva'] / 100) - ($data['facture']['fact_loyer_charge_ht'] * $data['facture']['fact_loyer_charge_tva'] / 100) ?>
                    <?= number_format($total_ht, 2, ",", ".") ?> € <br>
                    <?= number_format($total_tva, 2, ",", ".") ?> €<br>
                    <?= number_format($total_ttc, 2, ",", ".") ?> € <br>
                <?php  } ?>
            </td>
        </tr>
    </tfoot>
</table>

<br>

<div class="commentaire">
    <?php if ($data['facture']['cliLot_encaiss_loyer'] == "Compte client") : ?>
        <span>IBAN :
            <?= $data['facture']['cliLot_iban_client'] ?>&nbsp;&emsp;BIC :
            <?= $data['facture']['cliLot_bic_client'] ?>
        </span>
    <?php else : ?>
        <span>IBAN :
            <?= $data['rib']['rib_iban'] ?>&nbsp;&emsp;BIC :
            <?= $data['rib']['rib_bic'] ?>
        </span>
    <?php endif ?>
</div>

<br>

<div class="commentaire">
    <?php $date = explode("=>", $data['facture']['echeance_paiement']); ?>
    Tout règlement non parvenu avant le
    <?= $date[1] ?> sera majoré des pénalités de retard légales <br>
    Escompte pour paiement anticipé : néant <br>
    Passé ce délai, sans obligation d'envoi d'une relance, conformément à l’article L441-6 du code de commerce, il sera
    appliqué une pénalité calculée à un taux
    annuel de 20% sans que ce taux soit inférieur à 3 fois le taux d'intérêt légal.<br>
    Une indemnité forfaitaire pour frais de recouvrement de 40€ sera aussi exigible.
</div>

<br><br><br><br><br><br>

<div class="row">
    <div class="footer_exp">
        <span class="foot_bien"><?= $data['facture']['emetteur_nom'] ?>&nbsp;</span> <br>
        <span class="foot_bien">
            <?php if (!empty($data['facture']['emet_adress1'])) : ?>
                <?= $data['facture']['emet_adress1'] ?>&nbsp;
            <?php endif ?>

            <?php if (!empty($data['facture']['emet_adress2'])) : ?>
                <?= $data['facture']['emet_adress2'] ?>&nbsp;
            <?php endif ?>

            <?php if (!empty($data['facture']['emet_adress3'])) : ?>
                <?= $data['facture']['emet_adress3'] ?>&nbsp;
            <?php endif ?>
        </span><br>
        <span class="foot_bien"><?= $data['facture']['emet_cp'] ?>&nbsp;<?= $data['facture']['emet_ville'] ?></span><br>
        <span>Siret : <?= $data['facture']['dossier_siret'] ?> - APE : <?= $data['facture']['dossier_ape'] ?></span><br>
        <span>Numéro de TVA intracommunautaire : <?= $data['facture']['dossier_tva_intracommunautaire'] ?></span>
    </div>
</div>