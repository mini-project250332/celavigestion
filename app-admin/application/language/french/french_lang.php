<?php 

/* list d'encaissement */
$lang['list_encaissement'] = 'Liste des encaissements';
$lang['plus_encaissement'] = 'Ajouter un encaissement';
$lang['date_encaissement'] = 'Date d\'encaissement';
$lang['type_encaissement'] = 'Type d\'encaissement';
$lang['montant_encaissement'] = 'Montant';
$lang['fichier_encaissement'] = 'Fichiers';
$lang['montant_encaissements'] = ' Montant de l\'encaissement ';


$lang['titre-action'] = 'Action';
$lang['title_formulaire_modif_encaissement'] = 'Modification d\'un encaissement';
$lang['title_formulaire_encaissement'] = 'Ajout d\'un encaissement';
$lang['title_formulaire_cin'] = 'Ajout une nouvelle pièce d\'identité';

//Session expirer
$lang['session_expirer'] = '<p> Cher utilisateur, votre session a expiré. Veuillez vous reconnecter pour continuer.</p>';