<?php
require realpath('../vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (!function_exists("exporation_pdf")) {
    function exporation_pdf($view_file, $filename, $data = null, $lot_id, $annee, $fact_loyer_id = null)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');
        $CI->load->helper("url");
        $CI->load->model('DocumentLoyer_m');

        $mpdf = new \Mpdf\Mpdf();

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");

        $mpdf->WriteHTML($html);

        $string = str_replace('-', '_', $filename);
        $pdf_save_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $url = $pdf_save_name . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/Loyer/$annee/$lot_id";
        $url_doc = "../documents/clients/lots/Loyer/$annee/$lot_id/$url";

        makeDirPath($url_path);

        $data = array(
            'doc_loyer_nom' => $filename,
            'doc_loyer_path' => str_replace('\\', '/', $url_doc),
            'doc_loyer_creation' => date('Y-m-d H:i:s'),
            'doc_loyer_etat' => 1,
            'doc_loyer_annee' => $annee,
            'cliLot_id ' => $lot_id,
            'util_id' => $_SESSION['session_utilisateur']['util_id'],
            'fact_loyer_id' => $fact_loyer_id
        );

        $CI->load->DocumentLoyer_m->save_data($data);

        $url_file = $url_path . "/" . $pdf_save_name . ".pdf";
        $mpdf->Output($url_file, "F");
    }
}

if (!function_exists("makeDirPath")) {
    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }
}

if (!function_exists("exportation_pdf_mandat")) {
    function exportation_pdf_mandat($view_file, $data = null)
    {
        $CI = get_instance();

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $CI->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $CI->load->library('mpdf-5.7-php5/mpdf', 'mpdf');
        }

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->SetFooter('{PAGENO}' . '/{nb}');

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");

        $mpdf->WriteHTML($html);

        $url_path = APPPATH . "../../documents/registre_mandat/";

        makeDirPath($url_path);

        $pdf_save_name = 'registre_mandat_' . date('Y-m-d') . '.pdf';
        $url_file = $url_path . "/" . $pdf_save_name;

        $mpdf->Output($url_file, "F");

        $url_doc = "../documents/registre_mandat/" . $pdf_save_name;

        echo str_replace('\\', '/', $url_doc);
    }
}

if (!function_exists("exportation_csv_compta")) {
    function exportation_csv_compta($data = NULL, $transcomp_id)
    {
        $CI = get_instance();
        $CI->load->helper("url");
        $CI->load->model('Transfer_comptable_m');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($data as $rowIndex => $rowData) {
            // Loop through each cell in the row and set its value
            foreach ($rowData as $columnIndex => $value) {
                $sheet->setCellValueByColumnAndRow($columnIndex + 1, $rowIndex + 1, $value);
            }
        }

        $writer = new PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);
        $writer->setDelimiter(';');
        $writer->setEnclosure('');
        $writer->setLineEnding("\r\n");

        $url_path = APPPATH . "../../documents/Registre_Export_comptable/";
        makeDirPath($url_path);
        $excel_save_name = 'export_comptable_' . $transcomp_id . '.csv';
        $url_file = $url_path . "/" . $excel_save_name;
        $url_doc = "../documents/Registre_Export_comptable/" . $excel_save_name;

        $clause = ['transcomp_id' => $transcomp_id];
        $save = ['transcomp_fichier' => str_replace('\\', '/', $url_doc)];
        $CI->load->Transfer_comptable_m->save_data($save, $clause);

        $writer->save($url_file);
    }
}

if (!function_exists("exportation_excel_dossier")) {
    function exportation_excel_dossier($listDossier)
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // Ajouter la première ligne avec les en-têtes
        $headers = array("Numéro", "Noms Propriétaires", "Date de Création", "Conseiller clientèle", "Status");
        $col = 1;
        foreach ($headers as $header) {
            $sheet->setCellValueByColumnAndRow($col, 1, $header);

            // Ajuster la largeur de la colonne
            $sheet->getColumnDimensionByColumn($col)->setWidth(strlen($header) + 5); // Ajuster la largeur en fonction de la longueur du texte

            $col++;
        }

        // Ajouter les données à partir de la deuxième ligne
        $row = 2;  // Commencer à la deuxième ligne
        foreach ($listDossier as $value) {
            $dossier_etat = $value->dossier_etat;
            $etat = ($dossier_etat == 1) ? "Actif" : "Inactif";
            $col = 1;
            $cellValue = strval(str_pad($value->dossier_id, 4, '0', STR_PAD_LEFT));
            $sheet->setCellValueByColumnAndRow($col, $row, $cellValue);
            // Définir le format de la cellule comme texte
            $sheet->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

            // Continuer avec les autres colonnes
            $col++;
            $sheet->setCellValueByColumnAndRow($col++, $row, $value->nom_prenom);
            $sheet->getColumnDimensionByColumn($col - 1)->setAutoSize(true); // Ajuster automatiquement la largeur
            $sheet->setCellValueByColumnAndRow($col++, $row, date("d/m/Y", strtotime(str_replace('-', '/', $value->dossier_d_creation))));
            $sheet->setCellValueByColumnAndRow($col++, $row, !empty($value->utilNom) || !empty($value->utilNutilPrenomom) ? $value->utilNom . ' ' . $value->utilPrenom : 'Non renseigné');
            $sheet->setCellValueByColumnAndRow($col++, $row, $etat);

            $row++;
        }
        // Enregistrer la feuille de calcul au format Excel
        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $url_path = APPPATH . "../../documents/Registre_Dossier_/";
        makeDirPath($url_path);

        $excel_save_name = 'Registre_Dossier_' . date('Y-m-d') . '.xlsx';
        $url_file = $url_path . "/" . $excel_save_name;

        $writer->save($url_file);

        $url_doc = "../documents/Registre_Dossier_/" . $excel_save_name;

        return str_replace('\\', '/', $url_doc);
    }
}

if (!function_exists("exportdossier_pdf")) {
    function exportdossier_pdf($view_file, $filename, $data = null, $annee, $dossier_id, $id_fact)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');
        $CI->load->helper("url");
        $CI->load->model('DocumentFacturation_m');
        $mpdf = new \Mpdf\Mpdf();
        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $url = $filename . '.pdf';
        $url_path = APPPATH . "../../documents/facturation/$dossier_id/$annee";
        $url_doc = "../documents/facturation/$dossier_id/$annee/$url";
        makeDirPath($url_path);
        $data = array(
            'doc_facturation_nom' => $filename,
            'doc_facturation_path' => str_replace('\\', '/', $url_doc),
            'doc_facturation_creation' => date('Y-m-d H:i:s'),
            'doc_facturation_etat' => 1,
            'doc_facturation_annee' => $annee,
            'fact_id ' => $id_fact,
            'util_id' => $_SESSION['session_utilisateur']['util_id'],
            'doc_facturation_annee' => $annee
        );

        $CI->load->DocumentFacturation_m->save_data($data);
        $data['dossier_id'] = $dossier_id;
        $data['annee'] = $annee;

        $url_file = $url_path . "/" . $filename . ".pdf";
        $mpdf->Output($url_file, "F");
        echo json_encode($data);
    }
}

if (!function_exists("exportfacture_pdf")) {
    function exportfacture_pdf($view_file, $filename, $data = null, $annee, $dossier_id, $id_fact)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');
        $CI->load->helper("url");
        $CI->load->model('DocumentFacturation_m');
        $mpdf = new \Mpdf\Mpdf();
        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);
        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->WriteHTML($html);
        $url = $filename . '.pdf';
        $url_path = APPPATH . "../../documents/facturation/$dossier_id/$annee";
        $url_doc = "../documents/facturation/$dossier_id/$annee/$url";
        makeDirPath($url_path);
        $data = array(
            'doc_facturation_nom' => $filename,
            'doc_facturation_path' => str_replace('\\', '/', $url_doc),
            'doc_facturation_creation' => date('Y-m-d H:i:s'),
            'doc_facturation_etat' => 1,
            'doc_facturation_annee' => $annee,
            'fact_id ' => $id_fact,
            'util_id' => $_SESSION['session_utilisateur']['util_id'],
            'doc_facturation_annee' => $annee
        );

        $CI->load->DocumentFacturation_m->save_data($data);
        $data['dossier_id'] = $dossier_id;
        $data['annee'] = $annee;

        $url_file = $url_path . "/" . $filename . ".pdf";
        $mpdf->Output($url_file, "F");
        return $data;
    }
}


if (!function_exists("export_xml")) {

    function export_xml($data)
    {
        $CI = get_instance();

        if (!isset($data['prelevement'], $data['reglement'], $data['ics'], $data['Idreglmenet'])) {
            error_log("Il faut vérifier donnee de prelevement ou reglement ou ics");
            return false;
        }

        $prelevement = $data['prelevement'];
        $reglements = $data['reglement'];
        $ics = $data['ics'];
        $idsReglement = $data['Idreglmenet'];
        $regeneration = $data['regeneration'];

        if (!$CI->load->model('DocumentPrelevement_m')) {
            error_log("Failed to load DocumentPrelevement_m model");
            return false;
        }
        $url_path = APPPATH . "../../documents/Registre_sepa_xml/" . $prelevement[0]->idc_prelevement;
        if (!makeDirPath($url_path)) {
            error_log("Failed to create directory: " . $url_path);
            return false;
        }
        $filename = $prelevement[0]->idc_prelevement . ' Prelevement ' . date('Y-m-d');
        $xml_content = generate_xml_content($prelevement, $reglements, $ics);
        $xml_file_path = $url_path . "/" . $filename . '.xml';
        $url_doc = "../documents/Registre_sepa_xml/" . $prelevement[0]->idc_prelevement . "/" . $filename . '.xml';

        if (file_put_contents($xml_file_path, $xml_content) !== false) {

            if (!$regeneration) {
                $datadoc = array(
                    'doc_prelevement_nom' => $filename,
                    'doc_prelevement_path' => str_replace('\\', '/', $url_doc),
                    'doc_prelevement_creation' => date('Y-m-d H:i:s'),
                    'doc_prelevement_etat' => 1,
                    'doc_prelevement_annee' => date('Y'),
                    'doc_prelevement_util_id' => isset($_SESSION['session_utilisateur']['util_id']) ? $_SESSION['session_utilisateur']['util_id'] : null,
                    'doc_prelevement_regleID' => $idsReglement,
                    'doc_prelevement_prelevementID' => $prelevement[0]->idc_prelevement,
                );

                $CI->DocumentPrelevement_m->save_data($datadoc);
            } else {

                $documentPrelevement = $data['docprelevement'];
                $idDoc = $documentPrelevement->doc_prelevement_id;
                $clause = ['doc_prelevement_id' => $idDoc];
                $datadoc = array(
                    'doc_prelevement_nom' => $filename,
                    'doc_prelevement_path' => str_replace('\\', '/', $url_doc),
                    'doc_prelevement_creation' => date('Y-m-d H:i:s'),
                    'doc_prelevement_etat' => 1,
                    'doc_prelevement_annee' => date('Y'),
                    'doc_prelevement_util_id' => isset($_SESSION['session_utilisateur']['util_id']) ? $_SESSION['session_utilisateur']['util_id'] : null,
                    'doc_prelevement_prelevementID' => $prelevement[0]->idc_prelevement,
                );

                $CI->DocumentPrelevement_m->save_data($datadoc, $clause);
            }

            return str_replace('\\', '/', $url_doc);
        } else {
            error_log("Failed to write XML file: " . $xml_file_path);
        }

        return false;
    }
}
function generate_xml_content($prelevement, $reglements, $ics)
{

    if (empty($prelevement) || empty($reglements) || empty($ics)) {
        error_log("Donnee non complet pour generation fichier xml");
        return false;
    }

    $nombreligne = count($reglements);
    $sommeMontantTotalArrondie = round(array_sum(array_column($reglements, 'montant_total')), 2);
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Document xsi:schemaLocation="urn:iso:std:iso:20022:tech:xsd:pain.008.001.02 pain.008.001.02.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:iso:std:iso:20022:tech:xsd:pain.008.001.02"></Document>');
    $cstmrDrctDbtInitn = $xml->addChild('CstmrDrctDbtInitn');
    $grpHdr = $cstmrDrctDbtInitn->addChild('GrpHdr');
    $grpHdr->addChild('MsgId', $prelevement[0]->idc_prelevement . ' Prelevement ' . date('Y-m-d'));
    $grpHdr->addChild('CreDtTm', date('Y-m-d\TH:i:s'));
    $grpHdr->addChild('NbOfTxs', $nombreligne);
    $grpHdr->addChild('CtrlSum', $sommeMontantTotalArrondie);
    $initgPty = $grpHdr->addChild('InitgPty');
    $initgPty->addChild('Nm', 'CELAVI gestion');
    $pmtInf = $cstmrDrctDbtInitn->addChild('PmtInf');
    $pmtInf->addChild('PmtInfId',  $prelevement[0]->idc_prelevement . ' Prelevement ' . date('Y-m-d') . ' RCUR');
    $pmtInf->addChild('PmtMtd', 'DD');
    $pmtInf->addChild('NbOfTxs', $nombreligne);
    $pmtInf->addChild('CtrlSum', $sommeMontantTotalArrondie);
    $pmtTpInf = $pmtInf->addChild('PmtTpInf');
    $svcLvl = $pmtTpInf->addChild('SvcLvl');
    $svcLvl->addChild('Cd', 'SEPA');
    $lclInstrm = $pmtTpInf->addChild('LclInstrm');
    $lclInstrm->addChild('Cd', 'CORE');
    $pmtTpInf->addChild('SeqTp', 'RCUR');
    $pmtInf->addChild('ReqdColltnDt', $prelevement[0]->date_encaissement);
    $cdtr = $pmtInf->addChild('Cdtr');
    $cdtr->addChild('Nm', 'CELAVI gestion');
    $cdtrAcct = $pmtInf->addChild('CdtrAcct');
    $cdtrAcct->addChild('Id')->addChild('IBAN', $prelevement[0]->banque_iban);
    $cdtrAgt = $pmtInf->addChild('CdtrAgt');
    $cdtrAgt->addChild('FinInstnId')->addChild('BIC', $prelevement[0]->banque_bic);
    $cdtrSchmeId = $pmtInf->addChild('CdtrSchmeId');
    $id = $cdtrSchmeId->addChild('Id');
    $prvtId = $id->addChild('PrvtId');
    $othr = $prvtId->addChild('Othr');
    $othr->addChild('Id', $ics[0]->ics);
    $schmeNm = $othr->addChild('SchmeNm');
    $schmeNm->addChild('Prtry', 'SEPA');
    foreach ($reglements as $item) {
        $drctDbtTxInf = $pmtInf->addChild('DrctDbtTxInf');
        $pmtId = $drctDbtTxInf->addChild('PmtId');
        $pmtId->addChild('InstrId', $item->fact_num);
        $pmtId->addChild('EndToEndId', $item->fact_num);
        $instdAmt = $drctDbtTxInf->addChild('InstdAmt', $item->montant_total);
        $instdAmt->addAttribute('Ccy', 'EUR');
        $drctDbtTx = $drctDbtTxInf->addChild('DrctDbtTx');
        $mndtRltdInf = $drctDbtTx->addChild('MndtRltdInf');
        $mndtRltdInf->addChild('MndtId', $item->sepa_numero);
        $mndtRltdInf->addChild('DtOfSgntr', ($item->sepa_date_signature == null) ? '-' : $item->sepa_date_signature);
        $dbtrAgt = $drctDbtTxInf->addChild('DbtrAgt');
        $dbtrAgt->addChild('FinInstnId')->addChild('BIC', $item->bic);
        $dbtr = $drctDbtTxInf->addChild('Dbtr');
        $dbtr->addChild('Nm', htmlspecialchars($item->clients));
        $dbtrAcct = $drctDbtTxInf->addChild('DbtrAcct');
        $dbtrAcct->addChild('Id')->addChild('IBAN', $item->iban);
    }

    return $xml->asXML();
}

if (!function_exists("exportation_pdf_facturation_a_facture")) {
    function exportation_pdf_facturation_a_facture($view_file, $data = null)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->SetFooter('{PAGENO}' . '/{nb}');

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);

        $url_path = APPPATH . "../../documents/download_facturation/";

        makeDirPath($url_path);

        $pdf_save_name = 'liste_a_facture' . date('Y-m-d') . '.pdf';
        $url_file = $url_path . "/" . $pdf_save_name;

        $mpdf->Output($url_file, "F");

        $url_doc = "../documents/download_facturation/" . $pdf_save_name;

        echo str_replace('\\', '/', $url_doc);
    }
}


if (!function_exists("exportation_pdf_facturation_a_preparer")) {
    function exportation_pdf_facturation_a_preparer($view_file, $data = null)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->SetFooter('{PAGENO}' . '/{nb}');

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);

        $url_path = APPPATH . "../../documents/download_facturation/";

        makeDirPath($url_path);

        $pdf_save_name = 'liste_a_preparer' . date('Y-m-d') . '.pdf';
        $url_file = $url_path . "/" . $pdf_save_name;

        $mpdf->Output($url_file, "F");

        $url_doc = "../documents/download_facturation/" . $pdf_save_name;

        echo str_replace('\\', '/', $url_doc);
    }
}


if (!function_exists("exportation_pdf_facture")) {
    function exportation_pdf_facture($view_file, $data = null)
    {
        $CI = get_instance();

        $CI->load->library('mpdf-5.7-php7/Mpdf');

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->SetFooter('{PAGENO}' . '/{nb}');

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");
        $mpdf->AddPage();
        $mpdf->WriteHTML($html);

        $url_path = APPPATH . "../../documents/download_facturation/";

        makeDirPath($url_path);

        $pdf_save_name = 'liste_facture_exporter_' . date('Y-m-d') . '.pdf';
        $url_file = $url_path . "/" . $pdf_save_name;

        $mpdf->Output($url_file, "F");

        $url_doc = "../documents/download_facturation/" . $pdf_save_name;

        echo str_replace('\\', '/', $url_doc);
    }
}
