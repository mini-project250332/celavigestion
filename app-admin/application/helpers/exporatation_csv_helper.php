<?php

if (!function_exists("exportation_excel")) {
    function exportation_excel($view_file, $filename, $data = null, $lot_id, $annee, $fact_loyer_id = null)
    {
        $CI = get_instance();

        // Charger la bibliothèque PhpSpreadsheet
        $CI->load->library('PhpSpreadsheet/Spreadsheet');
        $CI->load->library('PhpSpreadsheet/Writer/Xlsx');

        $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Charger le modèle ou obtenir les données d'une autre manière
        if (!is_null($data)) {
            // Ajouter les données à la feuille de calcul
            $row = 1;
            foreach ($data as $row_data) {
                $col = 1;
                foreach ($row_data as $value) {
                    $sheet->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $row++;
            }
        }

        // Enregistrer la feuille de calcul au format Excel
        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $string = str_replace('-', '_', $filename);
        $excel_save_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $url = $excel_save_name . '.xlsx';

        $url_path = APPPATH . "../../documents/clients/lots/Loyer/$annee/$lot_id";
        $url_doc = "../documents/clients/lots/Loyer/$annee/$lot_id/$url";

        makeDirPath($url_path);

        $data = array(
            'doc_loyer_nom' => $filename,
            'doc_loyer_path' => str_replace('\\', '/', $url_doc),
            'doc_loyer_creation' => date('Y-m-d H:i:s'),
            'doc_loyer_etat' => 1,
            'doc_loyer_annee' => $annee,
            'cliLot_id ' => $lot_id,
            'util_id' => $_SESSION['session_utilisateur']['util_id'],
            'fact_loyer_id' => $fact_loyer_id
        );

        $CI->load->DocumentLoyer_m->save_data($data);

        $url_file = $url_path . "/" . $excel_save_name . ".xlsx";
        $writer->save($url_file);
    }
}

if (!function_exists("exportation_excel_mandat")) {
    function exportation_excel_mandat($view_file, $data = null)
    {
        $CI = get_instance();

        // Charger la bibliothèque PhpSpreadsheet
        $CI->load->library('PhpSpreadsheet/Spreadsheet');
        $CI->load->library('PhpSpreadsheet/Writer/Xlsx');
        

        $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Charger le modèle ou obtenir les données d'une autre manière
        if (!is_null($data)) {
            // Ajouter les données à la feuille de calcul
            $row = 1;
            foreach ($data as $row_data) {
                $col = 1;
                foreach ($row_data as $value) {
                    $sheet->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $row++;
            }
        }

        // Enregistrer la feuille de calcul au format Excel
        $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $url_path = APPPATH . "../../documents/registre_mandat/";

        makeDirPath($url_path);

        $excel_save_name = 'registre_mandat_' . date('Y-m-d') . '.xlsx';
        $url_file = $url_path . "/" . $excel_save_name;

        $writer->save($url_file);

        $url_doc = "../documents/registre_mandat/" . $excel_save_name;

        return str_replace('\\', '/', $url_doc);
    }
}


