<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('URL_ADMIN', 'http://pp-cestlavie.manao.eu/app-admin');
define('URL_CLIENT', 'http://pp-cestlavie.manao.eu/app-customer');
define('URL_FRONT', 'http://pp-cestlavie.manao.eu');

// APP
define('title', 'Celavigestion');
define('author', 'Manao logiciel');
define('description', 'Application ');

//BD
define('DB_HOST', '172.18.0.1');
define('DB_USER', 'phpmyadmin');
define('DB_PASSWORD', 'Pr3prodVM_!');

define('CODE_CRYPT', '12345');

define(
    "situation",
    serialize(
        array(
            'Célibataire',
            'Pacsé',
            'Marié'
        )
    )
);

define(
    "row_view",
    serialize(
        array(
            10,
            25,
            50,
            100,
            150,
            200
        )
    )
);

define(
    "lot_principal",
    serialize(
        array(
            "oui" => array(
                "value" => 1,
                "lotprincipal" => "Oui"
            ),
            "non" => array(
                "value" => 2,
                "lotprincipal" => "Non"
            )
        )
    )
);

define(
    "tache_prioritaire",
    serialize(
        array(
            "sans priorite" => array(
                "value" => 0,
                "tacheprioritaire" => "Sans priorité"
            ),
            "urgent" => array(
                "value" => 1,
                "tacheprioritaire" => "Urgent"
            ),
            "prioritaire " => array(
                "value" => 2,
                "tacheprioritaire" => "Prioritaire "
            )
        )
    )
);

define(
    "clilotprincipale",
    serialize(
        array(
            "oui" => array(
                "value" => 1,
                "clilotprincipal" => "Oui"
            ),
            "non" => array(
                "value" => 2,
                "clilotprincipal" => "Non"
            )
        )
    )
);

define(
    "tachecli_prioritaire",
    serialize(
        array(
            "oui" => array(
                "value" => 1,
                "tachecli_prioritaire" => "Oui"
            ),
            "non" => array(
                "value" => 2,
                "tachecli_prioritaire" => "Non"
            )
        )
    )
);

define('StartYear', '2021');

/***
 *  Constant valeur par defaut
 */

define("PROTOCOL", serialize(array("SMTP", "POP3", "IMAP")));
define("MAIL_SERVEUR", serialize(array("GMAIL", "ORANGE", "YAHOO", "Outlook", "OVH", "SFR", "Autre")));
define("TYPE_dechiffrement_MAIL", serialize(array("SSL", "TLS", "STARTTLS")));
define(
    "PORT_MAIL_SERVEUR",
    serialize(
        array(
            "SMTP" => array(25, 587, 465),
            "POP3" => array(110, 995),
            "IMAP" => array(143, 993),
        )
    )
);
define("PRIORITE_PARAMETRE", serialize(array("Important", "Normal", "Peu normal")));
define("CHARSET_CODAGE", serialize(array("UTF-8", "ISO-8859-1", "ISO-8859-15")));

define("ACCESS_WRITE_GESTION", serialize(array("1", "2", "3")));
define("ACCESS_READ_SETTING", serialize(array("1", "3")));

define('API_ACCOUNT_ID', '53089');
define('API_MAILING_KEY', '212d0d68-ca88-44c6-981b-6cd88a3d13f3');

define('VERSION_SCRIPT', '?' . uniqid("v="));


// Mailjet
define('API_KEY_MAILJET', '6b1498a8e62030bbdc29e64b3b6aab09');
define('API_SECRET_MAILJET', 'd00f453b664904ab07bd66a98f948bd7');
