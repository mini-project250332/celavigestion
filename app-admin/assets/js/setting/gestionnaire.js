var data_url = {
    headerList: `${const_app.base_url}Setting/Gestionnaire/getHeaderListe`,
    list: `${const_app.base_url}Setting/Gestionnaire/getListGestionnaire`,
    form: `${const_app.base_url}Setting/Gestionnaire/getFormGestionnaire`,
    formUpdate: `${const_app.base_url}Setting/Gestionnaire/formUpdateGestionnaire`,
    ficheGestionnaire_page: `${const_app.base_url}Setting/Gestionnaire/pageficheGestionnaire`,
    deleteGestionnaire: `${const_app.base_url}Setting/Gestionnaire/removeGestionnaire`,
    listeContact: `${const_app.base_url}Setting/Gestionnaire/listeContact`,
    getFormContact: `${const_app.base_url}Setting/Gestionnaire/getFormContact`,
    deleteContact: `${const_app.base_url}Setting/Gestionnaire/removeContact`,
    listePourcentageProgm: `${const_app.base_url}Setting/Gestionnaire/listePourcentageProgm`,
    FormPourcentageProgm: `${const_app.base_url}Setting/Gestionnaire/FormPourcentageProgm`,
    FormUpdateAllPourcentage: `${const_app.base_url}Setting/Gestionnaire/FormUpdateAllPourcentage`,
    formUploadGestionnaire: `${const_app.base_url}Setting/Gestionnaire/formUploadGestionnaire`,
    delete_docGest: `${const_app.base_url}Setting/Gestionnaire/delete_docGest`,
    modalupdateDocgest: `${const_app.base_url}Setting/Gestionnaire/modalupdateDocgest`,
    UpdateDataPourcentage : `${const_app.base_url}Setting/Gestionnaire/UpdateDataPourcentage`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgListeGestionnaire() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeGestionnaire');
}
function load_pgListeGestionnaire(response) {
    $('#contenaire-gestionnaire').html(response)
}

$(document).on('click', '#retourListeGestionnaire', function (e) {
    e.preventDefault();
    pgListeGestionnaire();
});

function listeGestionnaire() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeGestionnaire', loading);
}
function load_listeGestionnaire(response) {
    $('#contenair-list').html(response)
}


function geFormGestionnaire(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormGestionnaire', loading);
}
function returnFormGestionnaire(response) {
    $('#contenaire-gestionnaire').html(response)
}

$(document).on('click', '.btnFormUpdateGestionnaire', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        gestionnaire_id: $(this).attr('data-id')
    };
    ficheGestionnaire(data_request);
});

function ficheGestionnaire(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdate', loading);
}
function load_formUpdate(response) {
    $('#contenaire-gestionnaire').html(response);
}

function getFicheIdentificationGestionnaire(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'gestionnaire_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.ficheGestionnaire_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheGestionnaire', loading);
}
function retourFicheGestionnaire(response) {
    $('#fiche-identifiant-gestionnaire').html(response);
}

$(document).on('click', '#btnFormAddGestionnaire', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
        //util_id : ""
    };
    geFormGestionnaire(data_request);
});

$(document).on('click', '#btnAnnulerGestionnaire', function (e) {
    e.preventDefault();
    pgListeGestionnaire();
});

$(document).on('click', '#btn-update-gestionnaire', function (e) {
    e.preventDefault();
    $("#save-update-gestionnaire").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationGestionnaire($(this).attr('data-id'), 'form');
});


$(document).on('submit', '#formGestionnaire', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var code_postal = $('#gestionnaire_cp').val();

    if ($('#gestionnaire_nom').val()) {
        if (($('#gestionnaire_adresse1').val() || $('#gestionnaire_adresse2').val() || $('#gestionnaire_adresse3').val()) && $('#gestionnaire_cp').val() && $('#gestionnaire_ville').val()) {

            if ($('#gestionnaire_cp').val() && code_postal.length > 5) {
                Notiflix.Notify.failure('Le code postal doit contenir 5 caractères ou moins', { position: 'left-bottom', timeout: 3000 });
            }
            else {
                fn.ajax_form_data_custom(form, type_output, "retourCruGestionnaire", loading);
            }
        }
        else {
            Notiflix.Notify.failure('Il manque l’adresse ou code postal ou ville', { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        fn.ajax_form_data_custom(form, type_output, "retourCruGestionnaire", loading);
    }
});

function retourCruGestionnaire(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            gestionnaire_id: response.data_retour
        };
        ficheGestionnaire(data_request);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#gestionnaire_nom', function () {
    var nom = $(this).val().replace(/'/g, ' ');
    $(this).val(nom);
});

/***** update real time ****/

$(document).on('submit', '#updateInfoGestionnaire', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdate", loading);
});
function retourUpdate(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationGestionnaire(response.data_retour.gestionnaire_id, 'fiche');
        // $("#annulerUpdateProspect").toggleClass('d-none');
        $("#save-update-gestionnaire").toggleClass('d-none');
        $("#btn-update-gestionnaire").toggleClass('d-none');
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#updateInfoGestionnaire input, #updateInfoGestionnaire textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 800);
});
$(document).on('change', '#updateInfoGestionnaire select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 800);
});

function submitUpdateIdentification() {
    var form = $("#updateInfoGestionnaire");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output, "retourUpdateRealtime");
}
function retourUpdateRealtime(response) {
    if (response.status == true) {
        Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}


/******* *****/

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        gestionnaire_id: $(this).attr('data-id'),
    }
    deleteGestionnaire(data);
});
function deleteGestionnaire(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteGestionnaire;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        gestionnaire_id: response.data.id
                    }
                    deleteGestionnaire(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowGestionnaire-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function getContact(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'gestionnaire_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    var url_request = data_url.listeContact;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeContact', loading);
}

function loadListeContact(response) {
    $('#contenair-contact').html(response);
}

function getFormContact(gestionnaire_id, action, cnctGestionnaire_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'gestionnaire_id': gestionnaire_id,
        'cnctGestionnaire_id': cnctGestionnaire_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    var url_request = data_url.getFormContact;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormContact', loading);
}
function retourFormContact(response) {
    $('#contenair-contact').html(response);
}

$(document).on('click', '#btnFormNewContact', function (e) {
    e.preventDefault();
    getFormContact($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerAddContact', function (e) {
    e.preventDefault();
    getContact($(this).attr('data-id'));
});

$(document).on('click', '#updateContact', function (e) {
    e.preventDefault();
    getFormContact($(this).attr('data-gestionnaire_id'), 'edit', $(this).attr('data-id'));
});

$(document).on('submit', '#cruContact', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddContact", loading);
});

function retourAddContact(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getContact(response.data_retour.gestionnaire_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.deleteContact', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cnctGestionnaire_id: $(this).attr('data-id')
    };
    delete_Contact(data_request);
});

function delete_Contact(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteContact;
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteContact', loading);
}
function retourDeleteContact(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cnctGestionnaire_id: response.data.id
                    }
                    delete_Contact(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowContact-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/***Pourcentage de calcul**/

function getRegleCalculGestionnaire(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'gestionnaire_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.listePourcentageProgm;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlistePourcentage', loading);
}

function retourlistePourcentage(response) {
    $('#contenair-pourcentage').html(response);
}

$(document).on('click', '.updatePourcentagePgm', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'progm_id': $(this).attr('data-id'),
        'gestionnaire_id': $(this).attr('data-gestionnaire')
    };
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.FormPourcentageProgm;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlistePourcentage', loading);
});

$(document).on('click', '#btnModifyAll', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'gestionnaire_id': $(this).attr('data-id'),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.FormUpdateAllPourcentage;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlistePourcentage', loading);
});


$(document).on('click', '#annulerUpdatePgm', function (e) {
    e.preventDefault();
    getRegleCalculGestionnaire($(this).attr('data-id'));
});

$(document).on('submit', '#UpdatePourcentage', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-gestionnaire"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdatePourcentage", loading);
});

function retourUpdatePourcentage(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getRegleCalculGestionnaire(response.data_retour.gestionnaire_id);
    } else {
        fn.response_control_champ(response);
    }
}


$(document).on('click', '#update-withoutPercentage', function(e) {
    e.preventDefault();
    var action = 1;
    updateMultiDataPourcetageGestionnaire(action)
});

$(document).on('click', '#update-percentageForAll', function(e) {
    e.preventDefault();
    var action = 2;
    updateMultiDataPourcetageGestionnaire(action)
});


function updateMultiDataPourcetageGestionnaire(action){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'action' : action,
        'gestionnaire_id' : $("#gestionnaire_id").val(),
        'progm_pourcentage_evaluation' : $("#progm_pourcentage_evaluation").val(),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-gestionnaire"
    }
    var url_request = data_url.UpdateDataPourcentage;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourUpdatePourcentage',loading);
}

$(document).on('click', '#ajoutdocGestionnaire', function (e) {
    e.preventDefault();
    ajoutdocGestionnaire($(this).attr('data-id'), 'add');
});

function ajoutdocGestionnaire(gestionnaire_id, action) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'gestionnaire_id': gestionnaire_id,
    };
    var url_request = data_url.formUploadGestionnaire;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');

    $("#modalAjoutDoc").modal('show');
}

function chargercontentModal(response) {
    $('#contenaireUpload').html(response);
}

$(document).on('click', '.btnSuppDocgest', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_gest_id: $(this).attr('data-id')
    };
    delete_docgest(data_request);
});

function delete_docgest(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_docGest;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourdelete_docgest');
}

function retourdelete_docgest(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_gest_id: response.data.id
                    }
                    delete_docgest(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

$(document).on('click', '.btnUpdtadeDocgest', function (e) {
    e.preventDefault();
    var doc_gest_id = $(this).attr("data-id");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_gest_id: doc_gest_id };
    var url_request = data_url.modalupdateDocgest;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargepopup_callback');
});

function chargepopup_callback(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentGest', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateDocumentGest", "null_fn", param);
});
function retourUpdateDocumentGest(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var gestionnaire_id = $('#gestionnaire_id').val();
        getFicheIdentificationGestionnaire(gestionnaire_id, 'form');
    } else {
        fn.response_control_champ(response);
    }
}
