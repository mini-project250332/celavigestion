var journaux_comptable = {
    headerList: `${const_app.base_url}Setting/Journaux_comptable/getHeaderListe`,
}

var timer = null;

function pgListeJournaux_comptable(visuel) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        visuel: visuel
    };
    var url_request = journaux_comptable.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeJournaux_comptable');
}
function loadpgListeJournaux_comptable(response) {
    $('#contenaire-journaux_comptable').html(response);
}

$(document).on('click', '#btnFormodifier', function () {
    pgListeJournaux_comptable('form');
});

$(document).on('submit', '#updateEntreprise_Journaux_comptable', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-journaux_comptable"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_Journaux_comptable", loading);
});

function retourUpdateEntreprise_Journaux_comptable(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeJournaux_comptable('fiche');
    } else {
        fn.response_control_champ(response);
    }
}

var debounceTimer;

$(document).on('keyup', '#updateEntreprise_Journaux_comptable', function () {
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-journaux_comptable"
    }

    clearTimeout(debounceTimer);
    
    debounceTimer = setTimeout(function () {
        fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_keyup", loading);
    }, 2000);
});

function retourUpdateEntreprise_keyup(response) {
    if (response.status == true) {
        // Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}