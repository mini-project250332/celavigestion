var data_url = {
    headerList : `${const_app.base_url}Setting/OrigineClient/getHeaderListe`,
    list : `${const_app.base_url}Setting/OrigineClient/getListOrigineClient`,
    form : `${const_app.base_url}Setting/OrigineClient/getFormOrigineClient`,
    deleteOrigineClient : `${const_app.base_url}Setting/OrigineClient/removeOrigineClient`,
}

var timer = null;

$(document).ready(function () {
   
});

function chargeUrl(){
    
}

function pgListeOrigineClient(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeOrigineClient');
}
function load_pgListeOrigineClient(response) {
    $('#contenaire-origine-client').html(response)
}

function listeOrigineClient(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-origine-client"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeOrigineClient',loading);
}
function load_listeOrigineClient(response) {
    $('#contenair-list').html(response)
}


function geFormOrigineClient(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-origine-client"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormOrigineClient',loading);
}
function returnFormOrigineClient(response){
    $('#contenaire-origine-client').html(response)
}

$(document).on('click', '.btnFormUpdateOrigineClient', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       origine_cli_id : $(this).attr('data-id')
    };
    geFormOrigineClient(data_request);
});

$(document).on('click', '#btnFormAddOrigineClient', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add",
       //util_id : ""
    };
    geFormOrigineClient(data_request);
});

$(document).on('click', '#btnAnnulerOrigineClient', function(e) {
    e.preventDefault();
    pgListeOrigineClient();
});


$(document).on('submit', '#formOrigineClient', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-origine-client"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCruOrigineClient",loading);
});
function retourCruOrigineClient(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeOrigineClient();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnConfirmSup', function(e) {
    e.preventDefault();
    var data = {
        action : "demande",
        origine_cli_id : $(this).attr('data-id'),
    }
    deleteOrigineClient(data);
});
function deleteOrigineClient(data){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data,const_app.code_crypt);
    var url_request = data_url.deleteOrigineClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response){
    if(response.status == 200){
        if(response.action=="demande"){
            Notiflix.Confirm.show(
                response.data.title,response.data.text,response.data.btnConfirm,response.data.btnAnnuler,
                () => {
                    var data = {
                        action : "confirm",
                        origine_cli_id : response.data.id
                    }
                    deleteOrigineClient(data);
                },
                ()=> {},
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        }else{
            $(`tr[id=rowOrigineClient-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
        }  
    }else{
        Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
    }
}