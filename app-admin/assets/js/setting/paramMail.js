var data_url = {
    list : `${const_app.base_url}Setting/ParamMail/getListParamMail`,
    formUpdate : `${const_app.base_url}Setting/ParamMail/formUpdate`
}

var timer = null;

$(document).ready(function () {
   
});

function chargeUrl(){
    
}

function pgListeParamMail(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeParamMail');
}
function load_pgListeParamMail(response) {
    $('#contenaire-paramMail').html(response)
}

$(document).on('click', '.btnUpdateParam', function(e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action : "edit",
        mess_id : $(this).attr('data-id')
    };
    var loading = {
        type: "content",
        id_content: "contenaire-paramMail"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourParam',loading);
});

function retourParam(response){
    $('#contenaire-paramMail').html(response)
}

$(document).on('click', '#annulerUpdateParam', function(e) {
    e.preventDefault();
    pgListeParamMail();
});

$(document).on('submit', '#updateMailParam', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-paramMail"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdate",loading);
});

function retourUpdate(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeParamMail();
    }else{
        fn.response_control_champ(response);
    }
}

