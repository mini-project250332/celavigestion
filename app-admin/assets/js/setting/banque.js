var banque_url = {
    headerList: `${const_app.base_url}Setting/Banque/getHeaderListe`,
    listBanque: `${const_app.base_url}Setting/Banque/listBanque`,
    form: `${const_app.base_url}Setting/Banque/getFormBanque`,
    deleteBanque: `${const_app.base_url}Setting/Banque/removeBanque`,
}

var timer = null;

$(document).ready(function () {

});


function pgListeBanque() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = banque_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeBanque');
}

function loadpgListeBanque(response) {
    $('#contenaire-banque').html(response)
}

function listeBanque() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-banque"
    }
    var url_request = banque_url.listBanque;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeBanque', loading);
}

function load_listeBanque(response) {
    $('#contenair-list').html(response)
}

$(document).on('click', '#btnFormAddBanque', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add"
    };
    getFormBanque(data_request);
});

$(document).on('click', '.btnFormUpdateBanque', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       banque_id : $(this).attr('data-id')
    };
    getFormBanque(data_request);
});

$(document).on('click', '#btnAnnulerBanque', function(e) {
    e.preventDefault();
    pgListeBanque();
});


function getFormBanque(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-banque"
    }
    var url_request = banque_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormBanque',loading);
}
function returnFormBanque(response){
    $('#contenaire-banque').html(response)
}

$(document).on('submit', '#cruBanque', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-banque"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCrubanque",loading);
});

function retourCrubanque(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeBanque();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#banque_iban', function (e) {
    e.preventDefault();
    if (verifyIBAN($('#banque_iban').val()) == false) {
        $('div').children('.error_file').text("Format iban incorrect");
        $('.error_file').removeClass("d-none");
    }else{
        $('.error_file').addClass("d-none");
    } 
});

function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

$(document).on('click', '.btnSupBanque', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        banque_id: $(this).attr('data-id'),
    }
    deleteBanque(data);
});

function deleteBanque(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = banque_url.deleteBanque;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDeleteBanque');
}

function returnDeleteBanque(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        banque_id: response.data.id
                    }
                    deleteBanque(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowBanque-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

