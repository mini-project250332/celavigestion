var data_url = {
    list : `${const_app.base_url}Setting/Modelmail/getListModelMail`,
    formUpdate : `${const_app.base_url}Setting/Modelmail/formUpdate`
}

var timer = null;

$(document).ready(function () {
   
});

function chargeUrl(){
    
}

function pgListeModelmail(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeModelmail');
}
function load_pgListeModelmail(response) {
    $('#contenaire-modelMail').html(response)
}

$(document).on('click', '.btnUpdateModel', function(e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action : "edit",
        pmail_id : $(this).attr('data-id')
    };
    var loading = {
        type: "content",
        id_content: "contenaire-modelMail"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourModel',loading);
});

function retourModel(response){
    $('#contenaire-modelMail').html(response)
}

$(document).on('click', '#annulerUpdateModel', function(e) {
    e.preventDefault();
    pgListeModelmail();
});

$(document).on('submit', '#updateModel', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-modelMail"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdate",loading);
});

function retourUpdate(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeModelmail();
    }else{
        fn.response_control_champ(response);
    }
}