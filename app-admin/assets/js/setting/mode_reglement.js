var mode_reglement_url = {
    headerList: `${const_app.base_url}Setting/Mode_reglement/getHeaderListe`,
}

var timer = null;

function pgListeMode_reglement(visuel) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        visuel: visuel
    };
    var url_request = mode_reglement_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeFiscalite');
}
function loadpgListeFiscalite(response) {
    $('#contenaire-mode_reglement').html(response);
}

$(document).on('click', '#btnFormodifier', function () {
    pgListeMode_reglement('form');
});

$(document).on('submit', '#updateEntreprise_mode_reglement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-mode_reglement"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_mode_reglement", loading);
});

function retourUpdateEntreprise_mode_reglement(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeMode_reglement('fiche');
    } else {
        fn.response_control_champ(response);
    }
}

var debounceTimer;

$(document).on('keyup', '#updateEntreprise_mode_reglement', function () {
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-mode_reglement"
    }

    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(function () {
        fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_keyup", loading);
    }, 2000);
});

function retourUpdateEntreprise_keyup(response) {
    if (response.status == true) {
        // Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}