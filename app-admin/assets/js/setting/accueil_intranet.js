var data_url = {
    headerList: `${const_app.base_url}Setting/Accueil_Intranet/getHeaderListe`,
    list: `${const_app.base_url}Setting/Accueil_Intranet/getMessage`,
    updateMessage: `${const_app.base_url}Setting/Accueil_Intranet/updateMessage`,
}

var timer = null;

function pgAccueil() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgAccueil');
}
function load_pgAccueil(response) {
    $('#contenaire-accueil').html(response)
}

function messageAcceuil() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-accueil"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_messageAcceuil', loading);
}
function load_messageAcceuil(response) {
    $('#contenair-list').html(response);
}

$(document).on('click', '.btnUpdateMessage', function () {
    modifierMessage($(this).attr('data-id'));
});
function modifierMessage(mess_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'mess_id': mess_id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-list"
    }
    var url_request = data_url.updateMessage;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modifierMessage_Callback', loading);
}
function modifierMessage_Callback(response) {
    $('#contenair-list').html(response);
}

$(document).on('click','#annulerUpdateMessage',function(){
    messageAcceuil();
});

$(document).on('submit', '#saveUpdateModel', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-list"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdate",loading);
});

function retourUpdate(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        messageAcceuil();
    }else{
        fn.response_control_champ(response);
    }
}