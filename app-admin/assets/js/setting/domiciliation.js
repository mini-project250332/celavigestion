var domiciliation_url = {
    headerList: `${const_app.base_url}Setting/Domiciliation/getHeaderListe`,
}

var timer = null;

function pgListeDomiciliation(visuel) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        visuel: visuel
    };
    var url_request = domiciliation_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeDomiciliation');
}
function loadpgListeDomiciliation(response) {
    $('#contenaire-domiciliation').html(response);
}

$(document).on('click', '#btnFormodifier', function () {
    pgListeDomiciliation('form');
});

$(document).on('submit', '#updateEntreprise', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-domiciliation"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise", loading);
});

function retourUpdateEntreprise(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeDomiciliation('fiche');
    } else {
        fn.response_control_champ(response);
    }
}

var debounceTimer;

$(document).on('keyup', '#updateEntreprise', function () {
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-domiciliation"
    }

    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(function () {
        fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_keyup", loading);
    }, 2000);
});

function retourUpdateEntreprise_keyup(response) {
    if (response.status == true) {
        // Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}