var article_url = {
    headerList: `${const_app.base_url}Setting/Article/getHeaderListe`,
    listArticle: `${const_app.base_url}Setting/Article/listArticle`,
    form: `${const_app.base_url}Setting/Article/getFormArticle`,
    deleteArticle: `${const_app.base_url}Setting/Article/removeArticle`,
}

var timer = null;

$(document).ready(function () {

});


function pgListeArticle() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = article_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeArticle');
}

function loadpgListeArticle(response) {
    $('#contenaire-article').html(response)
}

function listeArticle() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-article"
    }
    var url_request = article_url.listArticle;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeArticle', loading);
}

function load_listeArticle(response) {
    $('#contenair-list').html(response)
}

$(document).on('click', '#btnFormAddArticle', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add"
    };
    getFormArticle(data_request);
});

$(document).on('click', '.btnFormUpdateArticle', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       art_id : $(this).attr('data-id')
    };
    getFormArticle(data_request);
});

$(document).on('click', '#btnAnnulerArticle', function(e) {
    e.preventDefault();
    pgListeArticle();
});


function getFormArticle(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-article"
    }
    var url_request = article_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormArticle',loading);
}
function returnFormArticle(response){
    $('#contenaire-article').html(response)
}

$(document).on('submit', '#cruArticle', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-article"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCruarticle",loading);
});

function retourCruarticle(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeArticle();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSupArticle', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        art_id: $(this).attr('data-id'),
    }
    deleteArticle(data);
});

function deleteArticle(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = article_url.deleteArticle;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDeleteArticle');
}

function returnDeleteArticle(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        art_id: response.data.id
                    }
                    deleteArticle(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            // $(`tr[id=rowArticle-${response.data.id}]`).remove();
            pgListeArticle();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

