var data_url = {
    headerList: `${const_app.base_url}Setting/Programme/getHeaderListe`,
    list: `${const_app.base_url}Setting/Programme/getListProgramme`,
    form: `${const_app.base_url}Setting/Programme/getFormProgramme`,
    deleteProgramme: `${const_app.base_url}Setting/Programme/removeProgramme`,
    verifierNom: `${const_app.base_url}Setting/Programme/verifierNom`,
    formUploadProgrm: `${const_app.base_url}Setting/Programme/formUploadProgrm`,
    delete_docprogm: `${const_app.base_url}Setting/Programme/delete_docprogm`,
    modalupdateDocProgm: `${const_app.base_url}Setting/Programme/modalupdateDocProgm`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgListeProgramme() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeProgramme');
}
function load_pgListeProgramme(response) {
    $('#contenaire-programme').html(response)
}

function listeProgramme() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-programme"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeProgramme', loading);
}
function load_listeProgramme(response) {
    $('#contenair-list').html(response)
}

function geFormProgramme(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-programme"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormProgramme', loading);
}
function returnFormProgramme(response) {
    $('#contenaire-programme').html(response)
}

$(document).on('click', '.btnFormUpdateProgramme', function (e) {
    e.preventDefault();
    var data_request = {
        action: "edit",
        progm_id: $(this).attr('data-id')
    };
    geFormProgramme(data_request);
});

$(document).on('click', '#btnFormAddProgramme', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
        //util_id : ""
    };
    geFormProgramme(data_request);
});

$(document).on('click', '#btnAnnulerProgramme', function (e) {
    e.preventDefault();
    pgListeProgramme();
});

$(document).on('submit', '#formProgram', function (e) {
    e.preventDefault();
    var code_postal = $('#progm_cp').val();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-programme"
    }

    if ($('#progm_nom').val()) {
        if (($('#progm_adresse1').val() || $('#progm_adresse2').val() || $('#progm_adresse3').val()) && $('#progm_cp').val() && $('#progm_ville').val()) {

            if ($('#progm_cp').val() && code_postal.length > 5) {
                Notiflix.Notify.failure('Le code postal doit contenir 5 caractères ou moins', { position: 'left-bottom', timeout: 3000 });
            }
            else {
                fn.ajax_form_data_custom(form, type_output, "retourCruProgramme", loading);
            }
        }
        else {
            Notiflix.Notify.failure('Il manque l’adresse ou code postal ou ville', { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        fn.ajax_form_data_custom(form, type_output, "retourCruProgramme", loading);
    }
});

function retourCruProgramme(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeProgramme();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#progm_nom', function () {
    var nom = $(this).val().replace(/'/g, ' ');
    $(this).val(nom);
});

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        progm_id: $(this).attr('data-id'),
    }
    deleteProgramme(data);
});
function deleteProgramme(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteProgramme;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        progm_id: response.data.id
                    }
                    deleteProgramme(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowProgramme-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('keyup', '#progm_nom', function () {
    var Nom = $(this).val();
    verifierNom(Nom);
});

function verifierNom(nom) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        nom: nom
    };

    var url_request = data_url.verifierNom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourverifierNom');
}
function retourverifierNom(response) {
    const obj = JSON.parse(response);
    if (obj[0] != undefined) {
        $('#error_name').text('Attention programme déjà créé');
        $('#error_name').addClass('text-danger');
    }
    else {
        $('#error_name').empty();
    }
}

$(document).on('click', '#ajoutdocProgramme', function (e) {
    e.preventDefault();
    ajoutDocProgramme($(this).attr('data-id'), 'add');
});

function ajoutDocProgramme(progm_id, action) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'progm_id': progm_id,
    };
    var url_request = data_url.formUploadProgrm;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');

    $("#modalAjoutDoc").modal('show');
}

function chargercontentModal(response) {
    $('#contenaireUpload').html(response);
}

$(document).on('click', '.btnSuppDocprogm', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_progm_id: $(this).attr('data-id')
    };
    delete_docprogm(data_request);
});

function delete_docprogm(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_docprogm;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocprogm');
}

function retourDeleteDocprogm(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_progm_id: response.data.id
                    }
                    delete_docprogm(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

$(document).on('click', '.btnUpdtadeDocprogm', function (e) {
    e.preventDefault();
    var doc_progm_id = $(this).attr("data-id");

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_progm_id: doc_progm_id };
    var url_request = data_url.modalupdateDocProgm;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargepopup_callback');
});

function chargepopup_callback(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentProgm', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedoctProgm", "null_fn", param);
});
function retourupdatedoctProgm(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            action: "edit",
            progm_id: $('#progm_id').val()
        };
        geFormProgramme(data_request);
    } else {
        fn.response_control_champ(response);
    }
}