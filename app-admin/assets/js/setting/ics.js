var ics_url = {
    headerList: `${const_app.base_url}Setting/Ics/getHeaderListe`,
    list: `${const_app.base_url}Setting/Ics/getListIcs`,
    form: `${const_app.base_url}Setting/Ics/getFormIcs`,
}

var timer = null;

function pgIcs() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = ics_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgIcs');
}
function loadpgIcs(response) {
    $('#contenaire-ics').html(response);
}

function listeIcs() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-ics"
    }
    var url_request = ics_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeIcs', loading);
}
function load_listeIcs(response) {
    $('#contenair-list').html(response);
}

$(document).on('click', '.btnFormUpdateIcs', function (e) {
    e.preventDefault();
    var data_request = {
        action: "edit",
        ics_id: $(this).attr('data-id')
    };
    geFormIcs(data_request);
});

function geFormIcs(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-ics"
    }
    var url_request = ics_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormIcs', loading);
}

function returnFormIcs(response) {
    $('#contenaire-ics').html(response)
}

$(document).on('submit', '#cruIcs', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-ics"
    }
    fn.ajax_form_data_custom(form, type_output, "retourcruIcs", loading);
});
function retourcruIcs(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgIcs();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#btnAnnulerICS', function () {
    pgIcs();
});