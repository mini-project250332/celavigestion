var data_url = {
    headerList : `${const_app.base_url}Setting/OrigineDemande/getHeaderListe`,
    list : `${const_app.base_url}Setting/OrigineDemande/getListOrigineDemande`,
    form : `${const_app.base_url}Setting/OrigineDemande/getFormOrigineDemande`,
    deleteOrigineDemande : `${const_app.base_url}Setting/OrigineDemande/removeOrigineDemande`,
}

var timer = null;

$(document).ready(function () {
   
});

function chargeUrl(){
    
}

function pgListeOrigineDemande(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeOrigineDemande');
}
function load_pgListeOrigineDemande(response) {
    $('#contenaire-origine-demande').html(response)
}

function listeOrigineDemande(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-origine-demande"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeOrigineDemande',loading);
}
function load_listeOrigineDemande(response) {
    $('#contenair-list').html(response)
}


function geFormOrigineDemande(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-origine-demande"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormOrigineDemande',loading);
}
function returnFormOrigineDemande(response){
    $('#contenaire-origine-demande').html(response)
}

$(document).on('click', '.btnFormUpdateOrigineDemande', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       origine_dem_id : $(this).attr('data-id')
    };
    geFormOrigineDemande(data_request);
});

$(document).on('click', '#btnFormAddOrigineDemande', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add",
       //util_id : ""
    };
    geFormOrigineDemande(data_request);
});

$(document).on('click', '#btnAnnulerOrigineDemande', function(e) {
    e.preventDefault();
    pgListeOrigineDemande();
});


$(document).on('submit', '#formOrigineDemande', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-origine-demande"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCruOrigineDemande",loading);
});
function retourCruOrigineDemande(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeOrigineDemande();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnConfirmSup', function(e) {
    e.preventDefault();
    var data = {
        action : "demande",
        origine_dem_id : $(this).attr('data-id'),
    }
    deleteOrigineDemande(data);
});
function deleteOrigineDemande(data){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data,const_app.code_crypt);
    var url_request = data_url.deleteOrigineDemande;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response){
    if(response.status == 200){
        if(response.action=="demande"){
            Notiflix.Confirm.show(
                response.data.title,response.data.text,response.data.btnConfirm,response.data.btnAnnuler,
                () => {
                    var data = {
                        action : "confirm",
                        origine_dem_id : response.data.id
                    }
                    deleteOrigineDemande(data);
                },
                ()=> {},
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        }else{
            $(`tr[id=rowOrigineDemande-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
        }  
    }else{
        Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
    }
}