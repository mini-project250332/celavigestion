var rib_url = {
    headerList: `${const_app.base_url}Setting/Rib/getHeaderListe`,
    list: `${const_app.base_url}Setting/Rib/getListRib`,
    form: `${const_app.base_url}Setting/Rib/getFormRib`,
    deleteRib: `${const_app.base_url}Setting/Rib/removeRib`,
    alert_rib_Iban: `${const_app.base_url}Setting/Rib/alert_rib_Iban`,
}

var timer = null;

function pgListeRib() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = rib_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeRib');
}
function loadpgListeRib(response) {
    $('#contenaire-rib').html(response);
}

function listeRib() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-rib"
    }
    var url_request = rib_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeRib', loading);
}
function load_listeRib(response) {
    $('#contenair-list').html(response);
}

$(document).on('click', '#btnFormAddRib', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
    };
    geFormRib(data_request);
});

function geFormRib(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-rib"
    }
    var url_request = rib_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormRib', loading);
}

function returnFormRib(response) {
    $('#contenaire-rib').html(response)
}

$(document).on('click', '#btnAnnulerRib', function (e) {
    e.preventDefault();
    pgListeRib();
});

$(document).on('submit', '#cruRib', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-rib"
    }
    $('.help_iban').text("");
    $('.help_bic').text("");

    if ($('#rib_iban').val() && $('#rib_bic').val()) {
      

        if (verifyIBAN($('#rib_iban').val()) == true) {
            fn.ajax_form_data_custom(form, type_output, "retourCruRib", loading);
        } else {
            alert_rib_Iban();
        }
    }
    else {

        if (!$('#rib_iban').val()) {
            $('.help_iban').text("Ce champ ne doit pas être vide.");
            $('.help_iban').css('color', 'red');
        }

        if (!$('#rib_bic').val()) {
            $('.help_bic').text("Ce champ ne doit pas être vide.");
            $('.help_bic').css('color', 'red');
        }
    }
});
function retourCruRib(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeRib();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSupRib', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        rib_id: $(this).attr('data-id'),
    }
    deleteRib(data);
});

function deleteRib(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = rib_url.deleteRib;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDeleteRib');
}
function returnDeleteRib(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        rib_id: response.data.id
                    }
                    deleteRib(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowRib-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnFormUpdateRib', function (e) {
    e.preventDefault();
    var data_request = {
        action: "edit",
        rib_id: $(this).attr('data-id')
    };
    geFormRib(data_request);
});


function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function alert_rib_Iban() {
    var data_request = {};
    var type_request = 'POST';
    var url_request = rib_url.alert_rib_Iban;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_rib_Iban_Callback');
}

function alert_rib_Iban_Callback(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}