var sie_url = {
    headerList: `${const_app.base_url}Setting/Sie/getHeaderListe`,
    list: `${const_app.base_url}Setting/Sie/getListSie`,
    formSie: `${const_app.base_url}Setting/Sie/getformSie`,
    deleteSie: `${const_app.base_url}Setting/Sie/removeSie`,
    formAlertSuppSie: `${const_app.base_url}Setting/Sie/formAlertSuppSie`,
    removefilesie: `${const_app.base_url}Setting/Sie/removefilesie`,
    alert_rib_Iban: `${const_app.base_url}Setting/Sie/alert_rib_Iban`,
}

var timer = null;

var timeout;

$(document).ready(function () {

});

function pgListeSie() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = sie_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeSie');
}
function loadpgListeSie(response) {
    $('#contenaire-sie').html(response)
}

function listeSie() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-sie"
    }
    var url_request = sie_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeSie', loading);
}

function load_listeSie(response) {
    $('#contenair-list').html(response)
}

$(document).on('click', '#btnFormAddSie', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
    };
    geFormSie(data_request);
});

$(document).on('click', '.btnFormUpdateSie', function (e) {
    e.preventDefault();
    var data_request = {
        action: "edit",
        sie_id: $(this).attr('data-id')
    };
    geFormSie(data_request);
});

$(document).on('click', '#btnAnnulerSie', function (e) {
    e.preventDefault();
    pgListeSie();
});

function geFormSie(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-sie"
    }
    var url_request = sie_url.formSie;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'FormSieCallback', loading);
}

function FormSieCallback(response) {
    $('#contenaire-sie').html(response)
}

function validateRIB(rib) {
    rib = rib.replace(/\s/g, '');
    if (rib.length !== 27) {
        return false;
    }

    return true;
}

function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

$(document).on('submit', '#cruSie', function (e) {
    e.preventDefault();

    var code_postal = $('#sie_cp').val();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-sie"
    }

    var fd = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    for (var i = 0; i < file_data.length; i++) {
        fd.append("file_" + i, file_data[i]);
    }
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });

    if ($('#sie_libelle').val()) {
        if (($('#sie_adresse1').val() || $('#sie_adresse2').val() || $('#sie_adresse3').val()) && $('#sie_cp').val() && $('#sie_ville').val()) {

            if ($('#sie_cp').val() && code_postal.length > 5) {
                Notiflix.Notify.failure('Le code postal doit contenir 5 caractères ou moins', { position: 'left-bottom', timeout: 3000 });
            }
            else if ($('#sie_rib').val() && verifyIBAN($('#sie_rib').val()) == false) {
                alert_rib_Iban();
            }

            else {
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        pgListeSie();
                    }
                });

            }
        }
        else {
            Notiflix.Notify.failure('Il manque l’adresse ou code postal ou ville', { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure('Il manque le libellé du SIE', { position: 'left-bottom', timeout: 3000 });
    }
});

$(document).on('click', '.btnSuppSie', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        sie_id: $(this).attr('data-id'),
    }
    deleteSie(data);
});

function deleteSie(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = sie_url.deleteSie;
    var loading = {
        type: "content",
        id_content: "contenaire-sie"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDeleteSie', loading);
}
function returnDeleteSie(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        sie_id: response.data.id
                    }
                    deleteSie(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowSie-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var myModalalertSie = new bootstrap.Modal(document.getElementById("modalAlertSuppSie"), {
    keyboard: false
});

$(document).on('click', '.alertSuppSie', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = sie_url.formAlertSuppSie;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerContentModalSie');
});

function chargerContentModalSie(response) {
    $("#modal-main-content-sie").html(response);
    myModalalertSie.toggle();
}

$(document).on('click', '#removefilesie', function () {
    $('.doc_sie').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        sie_id: $('#sie_id').val()
    };
    var url_request = sie_url.removefilesie;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilesie');
});

function retour_removefilesie(response) {
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function alert_rib_Iban() {
    var data_request = {};
    var type_request = 'POST';
    var url_request = sie_url.alert_rib_Iban;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_rib_Iban_Callback');
}

function alert_rib_Iban_Callback(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}