var data_url = {
    headerList: `${const_app.base_url}Setting/Cgp/getHeaderListe`,
    list: `${const_app.base_url}Setting/Cgp/getListCgp`,
    form: `${const_app.base_url}Setting/Cgp/getFormCgp`,
    deleteCgp: `${const_app.base_url}Setting/Cgp/removeCgp`,
}

var timer = null;

$(document).ready(function () {

});


function pgListecgp() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeCgp');
}
function loadpgListeCgp(response) {
    $('#contenaire-cgp').html(response)
}

function listeCgp() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-cgp"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeCgp', loading);
}

function load_listeCgp(response) {
    $('#contenair-list').html(response)
}

$(document).on('click', '#btnFormAddCgp', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
    };
    geFormCgp(data_request);
});

$(document).on('click', '#btnAnnulerCgp', function (e) {
    e.preventDefault();
    pgListecgp();
});

function geFormCgp(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-cgp"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormCgp', loading);
}

function returnFormCgp(response) {
    $('#contenaire-cgp').html(response)
}

$(document).on('submit', '#cruCgp', function (e) {
    e.preventDefault();
    var code_postal = $('#cgp_cp').val();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-cgp"
    }

    if ($('#cgp_nom').val()) {
        if (($('#cgp_adresse1').val() || $('#cgp_adresse2').val() || $('#cgp_adresse3').val()) && $('#cgp_cp').val() && $('#cgp_ville').val()) {

            if ($('#cgp_cp').val() && code_postal.length > 5) {
                Notiflix.Notify.failure('Le code postal doit contenir 5 caractères ou moins', { position: 'left-bottom', timeout: 3000 });
            }
            else {
                fn.ajax_form_data_custom(form, type_output, "retourCruCgp", loading);
            }
        }
        else {
            Notiflix.Notify.failure('Il manque l’adresse ou code postal ou ville', { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        fn.ajax_form_data_custom(form, type_output, "retourCruCgp", loading);
    }
});

function retourCruCgp(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListecgp();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#cgp_nom', function () {
    var nom = $(this).val().replace(/'/g, ' ');
    $(this).val(nom);
});

$(document).on('click', '.btnFormUpdateCgp', function (e) {
    e.preventDefault();
    var data_request = {
        action: "edit",
        cgp_id: $(this).attr('data-id')
    };
    geFormCgp(data_request);
});

$(document).on('click', '.btnSupCgp', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        cgp_id: $(this).attr('data-id'),
    }
    deleteCgp(data);
});

function deleteCgp(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteCgp;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDeleteCgp');
}
function returnDeleteCgp(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cgp_id: response.data.id
                    }
                    deleteCgp(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowCgp-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}
