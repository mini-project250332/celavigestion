var data_url = {
    headerList : `${const_app.base_url}Setting/Produit/getHeaderListe`,
    list : `${const_app.base_url}Setting/Produit/getListProduit`,
    form : `${const_app.base_url}Setting/Produit/getFormProduit`,
    deleteProduit : `${const_app.base_url}Setting/Produit/removeProduit`,
}

var timer = null;

$(document).ready(function () {
   
});

function chargeUrl(){
    
}

function pgListeProduit(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeProduit');
}
function load_pgListeProduit(response) {
    $('#contenaire-produit').html(response)
}

function listeProduit(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-produit"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeProduit',loading);
}
function load_listeProduit(response) {
    $('#contenair-list').html(response)
}


function geFormProduit(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-produit"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormProduit',loading);
}
function returnFormProduit(response){
    $('#contenaire-produit').html(response)
}

$(document).on('click', '.btnFormUpdateProduit', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       produit_id : $(this).attr('data-id')
    };
    geFormProduit(data_request);
});

$(document).on('click', '#btnFormAddProduit', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add",
       //util_id : ""
    };
    geFormProduit(data_request);
});

$(document).on('click', '#btnAnnulerProduit', function(e) {
    e.preventDefault();
    pgListeProduit();
});


$(document).on('submit', '#formProduit', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-produit"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCruProduit",loading);
});
function retourCruProduit(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeProduit();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnConfirmSup', function(e) {
    e.preventDefault();
    var data = {
        action : "demande",
        produit_id : $(this).attr('data-id'),
    }
    deleteProduit(data);
});
function deleteProduit(data){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data,const_app.code_crypt);
    var url_request = data_url.deleteProduit;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response){
    if(response.status == 200){
        if(response.action=="demande"){
            Notiflix.Confirm.show(
                response.data.title,response.data.text,response.data.btnConfirm,response.data.btnAnnuler,
                () => {
                    var data = {
                        action : "confirm",
                        produit_id : response.data.id
                    }
                    deleteProduit(data);
                },
                ()=> {},
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        }else{
            $(`tr[id=rowProduit-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
        }  
    }else{
        Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
    }
}