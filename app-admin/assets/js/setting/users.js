var data_url = {
    list: `${const_app.base_url}Setting/GestionUsers/getListUser`,
    form: `${const_app.base_url}Setting/GestionUsers/getFormUser`,
    generer_pw: `${const_app.base_url}Setting/GestionUsers/getgeneratorPW`,
    verify_lg: `${const_app.base_url}Setting/GestionUsers/verifyLogin`,
    deleteUser: `${const_app.base_url}Setting/GestionUsers/removeUser`,
    formUpdateParamSmtp: `${const_app.base_url}Setting/GestionUsers/FormUserParamSmtp`,
    test_mail: `${const_app.base_url}Setting/GestionUsers/test_mail`,
    testEnvoimail: `${const_app.base_url}SendMail/testEnvoimail`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

var Modal = new bootstrap.Modal(document.getElementById("FormModalTestmail"), {
    keyboard: false
});

function pgUser() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        data: ""
    };
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgUser', loading);
}
function load_pgUser(response) {
    $('#contenair-user').html(response)
}

$(document).on('click', '.btnFormUpdate', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        util_id: $(this).attr('data-id')
    };
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUser', loading);
});
$(document).on('click', '.btnFormDetail', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "detail",
        util_id: $(this).attr('data-id')
    };
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUser', loading);
});

$(document).on('click', '#btnFormAjout', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add"
    };
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUser', loading);
});
function load_formUser(response) {
    $('#contenair-user').html(response)
}

$(document).on('click', '#btnAnnulerForm', function (e) {
    e.preventDefault();
    pgUser();
});

$(document).on('submit', '#cruUser', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCru", loading);
});
function retourCru(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgUser();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#generer_pw', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {};
    var url_request = data_url.generer_pw;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourGenerPW');
});
function retourGenerPW(response) {
    if (response.status == 200) {
        $('input[name=util_password]').val(CryptoJSAesJson.decrypt(response.data, const_app.code_crypt));
    }
}

$(document).on('keyup', 'input[name=util_mail]', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        mail = e.target.value;
        if (mail.trim() != "") {
            if (_validationEmail(mail)) {
                _verifyLogin(mail.trim());
            } else {
                $(`#${e.target.getAttribute('id')}`).parent().parent('.form-group').addClass('has-error');
                $(`#${e.target.getAttribute('id')}`).parent('div').children('.help').text("Email invalide");
            }
        }
    }, 800);
});
function _validationEmail(mail = null) {
    var regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex_email.test(mail);
}
function _verifyLogin(mail = null) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(mail, const_app.code_crypt);
    var url_request = data_url.verify_lg;
    fn.ajax_request(type_request, url_request, type_output, data_request, '_retourVerifyLogin');
}
function _retourVerifyLogin(response) {
    if (response.status == 200) {
        var retour = CryptoJSAesJson.decrypt(response.data, const_app.code_crypt);
        $('input[name=util_login]').val(retour.mail);
        if (!retour.request) {
            $('input[name=util_login]').parent().parent('.form-group').addClass('has-error');
            $('input[name=util_login]').parent('div').children('.help').text(response.message);
        }
    }
}
$(document).on('keyup', 'input[name=util_login]', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        login = e.target.value;
        if (mail.trim() != "") {
            _verifyLogin(login);
        }
    }, 800);
});


$(document).on('click', '.deleteUser', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        util_id: $(this).attr('data-id'),
    }
    deleteUser(data);
});
function deleteUser(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteUser;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        util_id: response.data.id
                    }
                    deleteUser(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowUser-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnFormParam', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        util_id: $(this).attr('data-id')
    };
    var loading = {
        type: "content",
        id_content: "contenair-user"
    }
    var url_request = data_url.formUpdateParamSmtp;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUser', loading);
});

$(document).on('click', '#annulerUpdateParam_Smtp', function () {
    pgUser();
});

$(document).on('submit', '#updateMailParam', function (e) {
    e.preventDefault();
    var form = $(this);

    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-user"
    }

    var fd = new FormData();
    var file_data = $('input[type="file"]')[0].files; // for multiple files
    for (var i = 0; i < file_data.length; i++) {
        fd.append("file_" + i, file_data[i]);
    }
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: fd,
        contentType: false,
        processData: false,
        success: function (data) {
            pgUser();
        }
    });
});


$(document).on('keyup', '#mess_motdepasse_crypte', function () {
    $('#mess_motdepasse').val($(this).val());
});

$(document).on('click', '#test_mail', function () {
    var formData = new FormData();
    formData.append('email', $('#mess_email').val());
    formData.append('mess_id', $('#mess_id').val());
    formData.append('util_id', $('#util_id').val());
    formData.append('file', $('#mess_signature')[0].files[0]);
    if ($('#mess_email').val() && $('#mess_motdepasse').val() && $('#mess_adressemailserveur').val()) {
        $.ajax({
            url: data_url.test_mail,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                Modal.toggle();
                $("#modalContentTest").html(response);
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
    else {
        Notiflix.Notify.failure("Veuillez remplir les champs pour tester l'envoi de mail.", { position: 'left-bottom', timeout: 4000 });
    }
});

$(document).on('click', '#testerEnvoiMail', function () {
    var formData = new FormData();
    var message_text = $('#message_text').val().replace(/\n/g, "<br/>");
    formData.append('util_id', $('#util_id').val());
    formData.append('email_emeteur', $('#mess_email').val());
    formData.append('file', $('#mess_signature')[0].files[0]);
    formData.append('mess_adressemailserveur', $('#mess_adressemailserveur').val());
    formData.append('mess_port', $('#mess_port').val());
    formData.append('mess_typeauthentification', $('#mess_typeauthentification').val());
    formData.append('mess_protocole', $('#mess_protocole').val());
    formData.append('mess_port', $('#mess_port').val());
    formData.append('mess_motdepasse', $('#mess_motdepasse').val());
    formData.append('message_text', message_text);
    formData.append('destinataire', $('#destinataire').val());
    formData.append('object', $('#object').val());
    if ($('#destinataire').val()) {
        Modal.toggle();
        $.ajax({
            url: data_url.testEnvoimail,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == 200) {
                    Notiflix.Notify.success(obj.message, { position: 'left-bottom', timeout: 3000 });
                }
                else {
                    Notiflix.Notify.failure(obj.message, { position: 'left-bottom', timeout: 4000 });
                }
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
    else {
        Notiflix.Notify.failure("Veuillez renseigner le destinataire de l'email.", { position: 'left-bottom', timeout: 4000 });
    }
});