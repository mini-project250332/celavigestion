var syndicat_url = {
    headerList: `${const_app.base_url}Setting/Syndicat/getHeaderListe`,
    list: `${const_app.base_url}Setting/Syndicat/getListSyndicat`,
    form: `${const_app.base_url}Setting/Syndicat/getFormSyndicat`,
    formUpdate: `${const_app.base_url}Setting/Syndicat/formUpdateSyndicat`,
    ficheSyndicat_page: `${const_app.base_url}Setting/Syndicat/pageficheSyndicat`,
    listeContact: `${const_app.base_url}Setting/Syndicat/listeContact`,
    deleteSyndicat: `${const_app.base_url}Setting/Syndicat/removeSyndicat`,
    getFormContact: `${const_app.base_url}Setting/Syndicat/getFormContact`,
    deleteContact: `${const_app.base_url}Setting/Syndicat/removeContact`,
}

var timer = null;

function Syndicats() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = syndicat_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'Syndicats_callback');
}
function Syndicats_callback(response) {
    $('#contenaire-syndicat').html(response);
}

function listeSyndicats() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    var url_request = syndicat_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeSyndicat', loading);
}
function load_listeSyndicat(response) {
    $('#contenair-list-syndicat').html(response)
}

$(document).on('click', '#btnAnnulerSyndicat', function (e) {
    e.preventDefault();
    Syndicats();
});

$(document).on('click', '#btnFormAddSyndicat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
    };
    geFormSyndicat(data_request);
});

function geFormSyndicat(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    var url_request = syndicat_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormSyndicat', loading);
}
function returnFormSyndicat(response) {
    $('#contenaire-syndicat').html(response)
}

$(document).on('submit', '#formSyndicat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    var code_postal = $('#syndicat_cp').val();

    if ($('#syndicat_nom').val()) {
        if (($('#syndicat_adresse1').val() || $('#syndicat_adresse2').val() || $('#syndicat_adresse3').val()) && $('#syndicat_cp').val() && $('#syndicat_ville').val()) {

            if ($('#syndicat_cp').val() && code_postal.length > 5) {
                Notiflix.Notify.failure('Le code postal doit contenir 5 caractères ou moins', { position: 'left-bottom', timeout: 3000 });
            }
            else {
                fn.ajax_form_data_custom(form, type_output, "retourCruSyndicat", loading);
            }
        }
        else {
            Notiflix.Notify.failure('Il manque l’adresse ou code postal ou ville', { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        fn.ajax_form_data_custom(form, type_output, "retourCruSyndicat", loading);
    }
});

function retourCruSyndicat(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            syndicat_id: response.data_retour
        };
        ficheSyndicat(data_request);
    } else {
        fn.response_control_champ(response);
    }
}

function ficheSyndicat(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    var url_request = syndicat_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdate', loading);
}
function load_formUpdate(response) {
    $('#contenaire-syndicat').html(response);
}

$(document).on('click', '#retourListeSyndicat', function (e) {
    e.preventDefault();
    Syndicats();
});

$(document).on('click', '#btn-update-syndicat', function (e) {
    e.preventDefault();
    $("#save-update-syndicat").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationSyndicat($(this).attr('data-id'), 'form');
});

$(document).on('click', '.btnFormUpdateSyndicat', function (e) {
    e.preventDefault();
    var data_request = {
        syndicat_id: $(this).attr('data-id')
    };
    ficheSyndicat(data_request);
});

function getFicheIdentificationSyndicat(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'syndicat_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    var url_request = syndicat_url.ficheSyndicat_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheSyndicat', loading);
}
function retourFicheSyndicat(response) {
    $('#fiche-identifiant-syndicat').html(response);
}

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        syndicat_id: $(this).attr('data-id'),
    }
    deleteSyndicat(data);
});
function deleteSyndicat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = syndicat_url.deleteSyndicat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDelete');
}
function retourDelete(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        syndicat_id: response.data.id
                    }
                    deleteSyndicat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowSyndicat-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('submit', '#updateInfoSyndicat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-syndicat"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdate", loading);
});
function retourUpdate(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationSyndicat(response.data_retour.syndicat_id, 'fiche');
        // $("#annulerUpdateProspect").toggleClass('d-none');
        $("#save-update-syndicat").toggleClass('d-none');
        $("#btn-update-syndicat").toggleClass('d-none');
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#updateInfoSyndicat input, #updateInfoSyndicat textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 800);
});
$(document).on('change', '#updateInfoSyndicat select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 800);
});

function submitUpdateIdentification() {
    var form = $("#updateInfoSyndicat");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output, "retourUpdateRealtime");
}
function retourUpdateRealtime(response) {
    if (response.status == true) {
        Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}

function getContact(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'syndicat_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    var url_request = syndicat_url.listeContact;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeContact', loading);
}

function loadListeContact(response) {
    $('#contenair-contact').html(response);
}

$(document).on('click', '#btnFormNewContact', function (e) {
    e.preventDefault();
    getFormContact($(this).attr('data-id'), 'add', null);
});

function getFormContact(syndicat_id, action, cnctSyndicat_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'syndicat_id': syndicat_id,
        'cnctSyndicat_id': cnctSyndicat_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    var url_request = syndicat_url.getFormContact;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormContact', loading);
}
function retourFormContact(response) {
    $('#contenair-contact').html(response);
}

$(document).on('click', '#annulerAddContact', function (e) {
    e.preventDefault();
    getContact($(this).attr('data-id'));
});

$(document).on('submit', '#cruContact', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddContact", loading);
});

function retourAddContact(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getContact(response.data_retour.syndicat_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#updateContact', function (e) {
    e.preventDefault();
    getFormContact($(this).attr('data-syndicat_id'), 'edit', $(this).attr('data-id'));
});

$(document).on('click', '.deleteContact', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cnctSyndicat_id: $(this).attr('data-id')
    };
    delete_Contact(data_request);
});

function delete_Contact(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = syndicat_url.deleteContact;
    var loading = {
        type: "content",
        id_content: "contenair-contact"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteContact', loading);
}
function retourDeleteContact(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cnctSyndicat_id: response.data.id
                    }
                    delete_Contact(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowContact-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}