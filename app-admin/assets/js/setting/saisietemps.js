var data_url = {
    headerList: `${const_app.base_url}Setting/TypeSaisieTemps/getHeaderListe`,
    list: `${const_app.base_url}Setting/TypeSaisieTemps/getListSaisieTemps`,
    form : `${const_app.base_url}Setting/TypeSaisieTemps/getFormTypeTravail`,
    deleteTypeTravail: `${const_app.base_url}Setting/TypeSaisieTemps/removeTypeTravail`,
}

var timer = null;

$(document).ready(function () {

});

function pgListeSaisieTemps() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeSaisieTemps');
}

function loadpgListeSaisieTemps(response) {
    $('#contenaire-temps').html(response)
}

function listeTypeTempsSaisie() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-temps"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeSaisieTemps', loading);
}

function load_listeSaisieTemps(response) {
    $('#contenair-list').html(response)
}

function getFormTypeTravail(data){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-temps"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormTypeTravail',loading);
}
function returnFormTypeTravail(response){
    $('#contenaire-temps').html(response)
}

$(document).on('click', '#AddTypeTravail', function(e) {
    e.preventDefault();
    var data_request = {
       action : "add",
    };
    getFormTypeTravail(data_request);
});

$(document).on('click', '#annulerAjoutTravail', function(e) {
    e.preventDefault();
    pgListeSaisieTemps();
});


$(document).on('click', '.updateTypeTravail', function(e) {
    e.preventDefault();
    var data_request = {
       action : "edit",
       type_travail_id : $(this).attr('data-id')
    };
    getFormTypeTravail(data_request);
});

$(document).on('submit', '#cruTypeTravail', function(e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-temps"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCruTypeTravail",loading);
});

function retourCruTypeTravail(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message,{position: 'left-bottom',timeout: 3000});
        pgListeSaisieTemps();
    }else{
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.deleteTypeTravail', function(e) {
    e.preventDefault();
    var data = {
        action : "demande",
        type_travail_id : $(this).attr('data-id'),
    }
    deleteTypeTravail(data);
});

function deleteTypeTravail(data){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data,const_app.code_crypt);
    var url_request = data_url.deleteTypeTravail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteTypeTravail');
}

function retourDeleteTypeTravail(response){
    if(response.status == 200){
        if(response.action=="demande"){
            Notiflix.Confirm.show(
                response.data.title,response.data.text,response.data.btnConfirm,response.data.btnAnnuler,
                () => {
                    var data = {
                        action : "confirm",
                        type_travail_id : response.data.id
                    }
                    deleteTypeTravail(data);
                },
                ()=> {},
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        }else{
            $(`tr[id=rowtype-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
        }  
    }else{
        Notiflix.Notify.success(response.message,{position: 'left-bottom',timeout: 3000});
    }
}