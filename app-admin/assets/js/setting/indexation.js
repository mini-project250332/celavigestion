var data_url = {
    headerList: `${const_app.base_url}Setting/Indexation/getHeaderListe`,
    list: `${const_app.base_url}Setting/Indexation/getListIndexation`,
    form: `${const_app.base_url}Setting/Indexation/getFormIndice`,
    deleteIndice: `${const_app.base_url}Setting/Indexation/removeIndice`,
    deleteValeurIndice: `${const_app.base_url}Setting/Indexation/removeValeurIndice`,
    formUpdate: `${const_app.base_url}Setting/Indexation/formUpdateIndice`,
    ficheindexationPage: `${const_app.base_url}Setting/Indexation/pageficheIndice`,
    listeValeur: `${const_app.base_url}Setting/Indexation/listeValeur`,
    listeValeurIndice: `${const_app.base_url}Setting/Indexation/listeValeurIndice`,
    getFormValeur: `${const_app.base_url}Setting/Indexation/getFormValeur`,
    AlertAjoutValeur: `${const_app.base_url}Setting/Indexation/AlertAjoutValeur`,
    validerIndice: `${const_app.base_url}Setting/Indexation/validerIndice`,
    alertValide: `${const_app.base_url}Setting/Indexation/AlertValidationValeur`,
    listLot: `${const_app.base_url}Setting/Indexation/ListLot`,
    reIndexIndiceValeur: `${const_app.base_url}Setting/Indexation/reIndexIndiceValeur`,
    PopupNewValeur: `${const_app.base_url}Setting/Indexation/PopupNewValeur`,
    addIndiceVleur: `${const_app.base_url}Setting/Indexation/addIndiceVleur`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgListeIndexation() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_pgListeIndex');
}
function load_pgListeIndex(response) {
    $('#contenaire-indexation').html(response)
}

function listeIndexation() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    var url_request = data_url.list;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeIndex', loading);
}
function load_listeIndex(response) {
    $('#contenair-list').html(response)
}

function getFormIndice(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    var url_request = data_url.form;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFormIndexation', loading);
}
function returnFormIndexation(response) {
    $('#contenaire-indexation').html(response);
    $('#indloyer_periode').trigger('change');
}

$(document).on('click', '#btnFormAddIndice', function (e) {
    e.preventDefault();
    var data_request = {
        action: "add",
    };
    getFormIndice(data_request);
});

$(document).on('click', '#btnAnnulerIndice', function (e) {
    e.preventDefault();
    pgListeIndexation();
});

$(document).on('click', '#annulerUpdateIndice', function (e) {
    e.preventDefault();
    pgListeIndexation();
});

$(document).on('submit', '#AddIndice', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutIndice", loading);
});

function retourAjoutIndice(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeIndexation();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnFormUpdateIndice', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        indloyer_id: $(this).attr('data-id')
    };
    ficheIndice(data_request);
});

function ficheIndice(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdate', loading);
}
function load_formUpdate(response) {
    $('#contenaire-indexation').html(response);
}

$(document).on('click', '.btnlisteValeurIndice', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        indloyer_id: $(this).attr('data-id')
    };
    listeValeurIndice(data_request);
});

function listeValeurIndice(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    var url_request = data_url.listeValeurIndice;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeValeur', loading);
}
function load_listeValeur(response) {
    $('#contenaire-indexation').html(response);
}

function getFicheIdentificationIndice(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'indloyer_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    var url_request = data_url.ficheindexationPage;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheIndice', loading);
}
function retourFicheIndice(response) {
    $('#fiche-identifiant-indexation').html(response);
    $('#indloyer_periode').trigger('change');

}

$(document).on('click', '#retourListeIndice', function (e) {
    e.preventDefault();
    pgListeIndexation();
});

/********Supprimer indice**********/

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        indloyer_id: $(this).attr('data-id')
    };
    deleteIndice(data_request);
});
function deleteIndice(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteIndice;
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteIndice', loading);
}
function retourDeleteIndice(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        indloyer_id: response.data.id
                    }
                    deleteIndice(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowIndice-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function getValeur(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'indloyer_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    var url_request = data_url.listeValeur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeValeur', loading);
}

function loadListeValeur(response) {
    $('#contenair-valeur').html(response);
}

function getFormValeur(indloyer_id, action, indval_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'indloyer_id': indloyer_id,
        'indval_id': indval_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    var url_request = data_url.getFormValeur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormValeur', loading);
}
function retourFormValeur(response) {
    $('#contenair-valeur').html(response);
}

$(document).on('click', '#btnFormNewValeur', function (e) {
    e.preventDefault();
    // getFormValeur($(this).attr('data-id'), 'add', null);

    PopupNewValeur($(this).attr('data-id'));
});

var myModal_valeur = new bootstrap.Modal(document.getElementById("modalNewValeur"), {
    keyboard: false
});

function PopupNewValeur(indloyer_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'indloyer_id': indloyer_id,
    };
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    var url_request = data_url.PopupNewValeur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourPopupNewValeur', loading);
}

function retourPopupNewValeur(response) {
    $("#modal-main-content-valeur").html(response);
    myModal_valeur.toggle();
}

$(document).on('click', '#Creervaleur', function (e) {
    e.preventDefault();
    addIndiceVleur($(this).attr('data-id'));
});

function addIndiceVleur(indloyer_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'indloyer_id': indloyer_id,
    };
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    var url_request = data_url.addIndiceVleur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retouraddIndiceVleur', loading);
}

function retouraddIndiceVleur(response) {
    var jsonObj = JSON.parse(response);
    myModal_valeur.toggle();
    if (jsonObj.status == true) {
        getValeur(jsonObj.data_retour.data.indloyer_id);
        Notiflix.Notify.success(jsonObj.data_retour.message, { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure(jsonObj.data_retour.message, {
            position: 'left-bottom',
            timeout: 3000
        });
    }
}

$(document).on('click', '#annulerAddValeur', function (e) {
    e.preventDefault();
    getValeur($(this).attr('data-id'));
});

$(document).on('click', '.btnUpdateValeurIndice', function (e) {
    e.preventDefault();
    getFormValeur($(this).attr('data-id-loyer'), 'edit', $(this).attr('data-id'));
});

/******** Ajout  indice valeur **********/
$(document).on('submit', '#cruValeur', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutValeurIndice", loading);
});

function retourAjoutValeurIndice(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getValeur(response.data_retour.indloyer_id);
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        fn.response_control_champ(response);
    }
}


/********Supprimer indice valeur **********/

$(document).on('click', '.btnDeleteValeurIndice', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        indval_id: $(this).attr('data-id')
    };
    deleteValeurIndice(data_request);
});
function deleteValeurIndice(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteValeurIndice;
    var loading = {
        type: "content",
        id_content: "contenair-valeur"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValeurDeleteIndice', loading);
}
function retourValeurDeleteIndice(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        indval_id: response.data.id
                    }
                    deleteValeurIndice(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowValeurIndice-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/******** mise à jour  indice  **********/
$(document).on('submit', '#updateIndice', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateIndice", loading);
});

function retourUpdateIndice(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeIndexation();
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        fn.response_control_champ(response);
    }
}


/*****alert ajout valeur indexation non validé****/

$(document).on('click', '#btnFormValeurAlert', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.AlertAjoutValeur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

var myModal = new bootstrap.Modal(document.getElementById("modalValeurAlert"), {
    keyboard: false
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

/*********Valider indice*********/

$(document).on('click', '.btnValideIndice', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        indval_id: $(this).attr('data-id'),
        indloyer_id: $(this).attr('data-id-loyer')
    };
    var url_request = data_url.validerIndice;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');
});

var ModalIndice = new bootstrap.Modal(document.getElementById("modalValidationValeur"), {
    keyboard: false
});

function chargercontentModal(response) {
    $("#contentIndice").html(response);
    ModalIndice.toggle();
}

$(document).on('click', '.alertValide', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        indval_id: $(this).attr('data-id'),
        indloyer_id: $(this).attr('data-id-loyer')
    };
    var url_request = data_url.alertValide;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');
});

$(document).on('submit', '#validerValeur', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-indexation"
    }
    fn.ajax_form_data_custom(form, type_output, "retourValiderIndexation", loading);
});

function retourValiderIndexation(response) {
    if (response.status == 200) {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        setTimeout(() => {
            getValeur(response.data.id);
        }, 500);
        // reIndexIndiceValeur(response.data.id);
    } else {
        Notiflix.Notify.failure(response.message, { position: 'left-bottom', timeout: 3000 });
        fn.response_control_champ(response);
    }
}

function reIndexIndiceValeur(indloyer_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        indloyer_id: indloyer_id,
    };
    var url_request = data_url.reIndexIndiceValeur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'reIndexIndiceValeur_Callback');
}
function reIndexIndiceValeur_Callback(response) {

}

$(document).on('click', '.list-lot', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        indval_id: $(this).attr('data-id')
    };
    var url_request = data_url.listLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'callbackModalLot');
});

var ModalLot = new bootstrap.Modal(document.getElementById("ModalFormLot"), {
    keyboard: false
});

function callbackModalLot(response) {
    $("#modalContentLot").html(response);
    ModalLot.toggle();
}

$(document).on('click', '.fiche_Lot_indexé', function () {
    var lot_id = $(this).attr('data-cliLot_id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');

    var href = $('#proprietaire-menu-web').attr('href');
    var newLocation = href + `?_p@roprietaire=${client_id}&_lo@t_id=${lot_id}&_nom=${nom}`;
    window.open(newLocation, '_blank');
});

