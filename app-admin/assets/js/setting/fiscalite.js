var fiscalite_url = {
    headerList: `${const_app.base_url}Setting/Fiscalite/getHeaderListe`,
}

var timer = null;

function pgListeFiscalite(visuel) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        visuel: visuel
    };
    var url_request = fiscalite_url.headerList;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadpgListeFiscalite');
}
function loadpgListeFiscalite(response) {
    $('#contenaire-fiscalite').html(response);
}

$(document).on('click', '#btnFormodifier', function () {
    pgListeFiscalite('form');
});

$(document).on('submit', '#updateEntreprise_fiscale', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-fiscalite"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_fiscale", loading);
});

function retourUpdateEntreprise_fiscale(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgListeFiscalite('fiche');
    } else {
        fn.response_control_champ(response);
    }
}

var debounceTimer;

$(document).on('keyup', '#updateEntreprise_fiscale', function () {
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-fiscalite"
    }

    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(function () {
        fn.ajax_form_data_custom(form, type_output, "retourUpdateEntreprise_keyup", loading);
    }, 2000);
});

function retourUpdateEntreprise_keyup(response) {
    if (response.status == true) {
        // Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        fn.response_control_champ(response);
    }
}