var fiscal_url = {
    getDocFiscal: `${const_app.base_url}Fiscal/getDocFiscal`,
    getModalInitialise: `${const_app.base_url}Fiscal/getModalInitialise`,
    getTableDeficit: `${const_app.base_url}Fiscal/getTableDeficit`,
    getTableBenefice: `${const_app.base_url}Fiscal/getTableBenefice`,
    getTable_2042: `${const_app.base_url}Fiscal/getTable_2042`,
    initialiseFiscal: `${const_app.base_url}Fiscal/initialiseFiscal`,
    getModalDeficit: `${const_app.base_url}Fiscal/getModalDeficit`,
    update_Deficit: `${const_app.base_url}Fiscal/update_Deficit`,
    getModaladdBenefice: `${const_app.base_url}Fiscal/getModaladdBenefice`,
    addBenefice: `${const_app.base_url}Fiscal/addBenefice`,
    getModalaction: `${const_app.base_url}Fiscal/getModalaction`,
    formUploadDocFiscal: `${const_app.base_url}Fiscal/formUploadDocFiscal`,
    getpathDocFiscal: `${const_app.base_url}Fiscal/getpathDocFiscal`,
    updateDocFiscalTraitement: `${const_app.base_url}Fiscal/updateDocFiscalTraitement`,
    modalupdateDocFiscal: `${const_app.base_url}Fiscal/modalupdateDocFiscal`,
    deleteDocFiscal: `${const_app.base_url}Fiscal/deleteDocFiscal`,
    deleteBenefice: `${const_app.base_url}Fiscal/deleteBenefice`,
    modalAlertBenefice: `${const_app.base_url}Fiscal/modalAlertBenefice`,
    alertDeficit: `${const_app.base_url}Fiscal/alertDeficit`,
    /********Tva*/
    getDoctva: `${const_app.base_url}Fiscal/getDoctva`,
    listeDocTva: `${const_app.base_url}Fiscal/listeDocTva`,
    FormAddDocTva: `${const_app.base_url}Fiscal/FormAddDocTva`,
    getpathDoctva: `${const_app.base_url}Fiscal/getpathDoctva`,
    deleteDocTva: `${const_app.base_url}Fiscal/removeDocTva`,
    removefiletva: `${const_app.base_url}Fiscal/removefiletva`,
    envoiTva: `${const_app.base_url}SendMail/envoiTva`,
    /*******2031**********/
    getDoc2031: `${const_app.base_url}Fiscal/getDoc2031`,
    listeDoc2031: `${const_app.base_url}Fiscal/listeDoc2031`,
    formUploadDoc2031: `${const_app.base_url}Fiscal/formUploadDoc2031`,
    FormUpdateDoc2031: `${const_app.base_url}Fiscal/FormUpdateDoc2031`,
    deleteDoc2031: `${const_app.base_url}Fiscal/removeDoc2031`,
    getpathDoc2031: `${const_app.base_url}Fiscal/getpathDoc2031`,
    envoi2031: `${const_app.base_url}SendMail/envoi2031`,
    getDocConfidentiel: `${const_app.base_url}Fiscal/getDocConfidentiel`,
    formUploadDocFiscalConfidentiels: `${const_app.base_url}Fiscal/formUploadDocFiscalConfidentiels`,
    deleteDocConfidentiel: `${const_app.base_url}Fiscal/deleteDocConfidentiel`,
    modalupdateDocConfidentiel: `${const_app.base_url}Fiscal/modalupdateDocConfidentiel`,
    getpathDocConfidentiel: `${const_app.base_url}Fiscal/getpathDocConfidentiel`,
    addModifcommentaireDocConfidentiel: `${const_app.base_url}Fiscal/addModifcommentaireDocConfidentiel`,
}

var timer;

function getDocFiscal(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }
    var type_request = 'POST';
    var url_request = fiscal_url.getDocFiscal;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-fiscal-deficits"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDocFiscalCallback', loading);
}

function getDocFiscalCallback(response) {
    $('#contenair-fiscal-deficits').html(response);
}

$(document).on('click', '#btn_initialise_fiscal', function () {
    getModalInitialise($(this).attr('data-id'));
});

function getModalInitialise(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getModalInitialise;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getModalInitialiseCallback');
    $('#modalFormFiscal').modal('show');
}

function getModalInitialiseCallback(response) {
    $('#modal-main-content-fiscal').html(response);
}

function getTableDeficit(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getTableDeficit;
    var type_output = 'html';
    var loading = {
        type: "content",
        id_content: "container-table-deficits"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTableDeficitCallback', loading);
}

function getTableDeficitCallback(response) {
    $('#container-table-deficits').html(response);
}

function getTableBenefice(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getTableBenefice;
    var type_output = 'html';
    var loading = {
        type: "content",
        id_content: "container-table-benefices"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTableBeneficeCallback', loading);
}

function getTableBeneficeCallback(response) {
    $('#container-table-benefices').html(response);
}

function getTable_2042(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getTable_2042;
    var type_output = 'html';
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-2042"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTable_2042Callback', loading);
}

function getTable_2042Callback(response) {
    $('#contenair-fiscal-2042').html(response);
}

$(document).on('click', '.initialyse', function () {
    initialiseFiscal($('#dossier_fiscal').val(), $('#annee_initial').val());
});

function initialiseFiscal(dossier_id, deficits_annee) {
    var data_request = {
        'deficits_annee': deficits_annee,
        'dossier_id': dossier_id
    }

    var type_request = 'POST';
    var url_request = fiscal_url.initialiseFiscal;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'initialiseFiscalCallback');
    $('#modalFormFiscal').modal('hide');
}

function initialiseFiscalCallback(response) {
    const obj = JSON.parse(response);
    getDocFiscal(obj.dossier_id);
}

$(document).on('click', '.ModifDeficits', function () {
    getModalDeficit($('#dossier_fiscal').val());
});

function getModalDeficit(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getModalDeficit;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getModalDeficitCallback');
    $('#miniModalFormFiscal').modal('show');
}

function getModalDeficitCallback(response) {
    $('#modal-main-content-fiscal-mini').html(response);
}

$(document).on('click', '.enregistrer_deficite', function () {
    var data = new Array();
    $('.deficite').each(function () {
        var deficits = $(this).val() ? $(this).val() : 0;
        var sous_data = {
            'deficits_annee': $(this).attr('data-annee'),
            'deficite_anterieur': deficits,
        };
        data.push(sous_data);
    });
    update_Deficit($("#dossier_id_modal_modif").val(), data)
});

function update_Deficit(dossier_id, data_save) {
    var data_request = {
        'dossier_id': dossier_id,
        'data_save': data_save
    }

    var type_request = 'POST';
    var url_request = fiscal_url.update_Deficit;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'update_DeficitCallback');
    $('#miniModalFormFiscal').modal('hide');
}

function update_DeficitCallback(response) {
    const obj = JSON.parse(response);
    getTableDeficit(obj.dossier_id);
    getTableBenefice(obj.dossier_id);
}

$(document).on('click', '#addBenefice', function () {
    getModaladdBenefice($('#dossier_fiscal').val());
});

function getModaladdBenefice(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }

    var type_request = 'POST';
    var url_request = fiscal_url.getModaladdBenefice;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getModaladdBeneficeCallback');
    $('#miniModalFormFiscal').modal('show');
}

function getModaladdBeneficeCallback(response) {
    $('#modal-main-content-fiscal-mini').html(response);
}

$(document).on('click', '.enregistrer_benefice', function () {
    if ($('#benefices').val()) {
        addBenefice(
            $('#cliLot_id_modal_ajout').val(),
            $('#benefices_annee').val(),
            $('#benefices').val()
        );
    }
    else {
        Notiflix.Notify.failure("Veuillez remplir le champ bénéfice.", {
            position: 'left-bottom',
            timeout: 3000
        });
    }
});

function addBenefice(dossier_id, benefices_annee, benefices) {
    var data_request = {
        'dossier_id': dossier_id,
        'benefices_annee': benefices_annee,
        'benefices': benefices
    }

    var type_request = 'POST';
    var url_request = fiscal_url.addBenefice;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addBeneficeCallback');
    $('#miniModalFormFiscal').modal('hide');
}

function addBeneficeCallback(response) {
    const obj = JSON.parse(response);
    getTableBenefice(obj.dossier_id);
    getTableDeficit(obj.dossier_id);
}

$(document).on('click', '#ajoutDocFiscal', function () {
    ajoutDocFiscal($(this).attr('data-id'), 'add', null);
});

function ajoutDocFiscal(dossier_id, action, doc_fiscal_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'dossier_id': dossier_id,
        'doc_fiscal_id': doc_fiscal_id
    };

    var url_request = fiscal_url.formUploadDocFiscal;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentFiscal');

    $("#modalAjoutFiscal").modal('show');
}

function chargercontentFiscal(response) {
    $('#UploadFiscal').html(response);
}

function apercuDocumentFiscal(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = fiscal_url.getpathDocFiscal;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathfiscal');
}

function responsepathfiscal(response) {
    var el_apercu = document.getElementById('apercuFiscaldoc')
    var dataPath = response[0].doc_fiscal_path
    var id_document = response[0].doc_fiscal_id
    apercuFiscal(dataPath, el_apercu, id_document);
}

function apercuFiscal(data, el, id_doc) {
    viderApercuFiscal();
    var doc_selectionner = document.getElementById("docrowfiscal-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuFiscal').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuFiscal').appendChild(element);
    }
}

function viderApercuFiscal() {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercudocBail") != undefined) {
        if (document.getElementById("apercudocBail").querySelector("#view_apercu") != null) {
            document.getElementById("apercudocBail").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudocBail").querySelector("#view_apercu").remove();
        }
    }
}


$(document).on('click', '.btnTraiteFiscal', function () {
    updateTraitementFiscal($(this).attr('data-id'));
});

function updateTraitementFiscal(doc_fiscal_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_fiscal_id': doc_fiscal_id,
    };

    var url_request = fiscal_url.updateDocFiscalTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementFiscalCallback');
}

function updateTraitementFiscalCallback(response) {
    getDocFiscal(response.dossier_id);
}

$(document).on('click', '.btnUpdtadeDocfiscal', function (e) {
    e.preventDefault();
    var doc_fiscal_id = $(this).attr("data-id");
    var dossier_id = $(this).attr("data-dossier");

    var type_request = 'POST';
    var type_output = 'html';

    var data_request = {
        action: "edit",
        doc_fiscal_id: doc_fiscal_id,
        dossier_id: dossier_id,
    };
    var url_request = fiscal_url.modalupdateDocFiscal;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);

    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentFiscal', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocFiscal", "null_fn", param);
});

function retourupdatedocFiscal(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocFiscal(response.data_retour.dossier_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSuppDocFiscal', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_fiscal_id: $(this).attr('data-id')
    };

    deleteDocFiscal(data_request);
});

function deleteDocFiscal(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = fiscal_url.deleteDocFiscal;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-deficits"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocFiscal', loading);
}

function retourDeleteDocFiscal(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_fiscal_id: response.data.id
                    }
                    deleteDocFiscal(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowfiscal-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.SupprimerBenefice', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        id_benefices: $(this).attr('data-id'),
        dossier_id: $('#dossier_fiscal').val()
    };
    deleteBenefice(data_request);
});

function deleteBenefice(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = fiscal_url.deleteBenefice;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-deficits"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteBenefice', loading);
}

function retourDeleteBenefice(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        id_benefices: response.data.id,
                        dossier_id: response.data.dossier_id
                    }
                    deleteBenefice(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=row_benefice-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getTableBenefice(response.dossier_id);
            getTableDeficit(response.dossier_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.alertBenefice', function () {
    var type_request = 'POST';
    var url_request = fiscal_url.modalAlertBenefice;
    var type_output = 'html';
    var data_request = {
        action: "demande",
        id_benefices: $(this).attr('data-id'),
        dossier_id: $('#dossier_fiscal').val()
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('click', '.alertDeficit', function () {
    var type_request = 'POST';
    var url_request = fiscal_url.alertDeficit;
    var type_output = 'html';
    var data_request = {
        action: "demande",
        id_benefices: $(this).attr('data-id'),
        dossier_id: $('#dossier_fiscal').val()
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});


function getDoctva(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }
    var type_request = 'POST';
    var url_request = fiscal_url.getDoctva;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-fiscal-tva"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDoctvaCallback', loading);
}

function getDoctvaCallback(response) {
    $('#contenair-fiscal-tva').html(response);
}

$(document).on('change', '#doc_tva_annee', function (e) {
    e.preventDefault();
    var dossier_id = $(this).attr('data-id');
    getDoctvaAnnee(dossier_id);
});

function getDoctvaAnnee(id) {
    var doc_tva_annee = $('#doc_tva_annee').val();

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id,
        'doc_tva_annee': doc_tva_annee

    };
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-tva"
    }
    var url_request = fiscal_url.listeDocTva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDoctvaAnneeCallback', loading);
}

function getDoctvaAnneeCallback(response) {
    $('#content-tva').html(response);
}

$(document).on('click', '#ajoutdocTva', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        dossier_id: $(this).attr('data-id'),
        doc_tva_annee: $('#doc_tva_annee').val()
    };
    var url_request = fiscal_url.FormAddDocTva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_Modal');
});

$(document).on('click', '.btnUpdtadeDocTva', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        doc_tva_id: $(this).attr('data-id'),
        dossier_id: $(this).attr('data-dossier'),
        doc_tva_annee: $('#doc_tva_annee').val()
    };
    var url_request = fiscal_url.FormAddDocTva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_Modal');
});

$(document).on('click', '#removefiletva', function () {
    $('.doc').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        doc_tva_id: $(this).attr('data-id')
    };
    var url_request = data_url.removefiletva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefiletva');
});

function retour_removefiletva(response) {
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

function charger_Modal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

$(document).on('submit', '#AddDocTva', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#doc_tva_path').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var dossier_id = $("#dossier_id").val();
                    getDoctva(dossier_id);
                    Notiflix.Notify.success("Enregistrement réussi", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

function apercuDocumentTva(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = fiscal_url.getpathDoctva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathtva');
}

function responsepathtva(response) {
    var el_apercu = document.getElementById('aperculistdocTva')
    var dataPath = response[0].doc_tva_path
    var id_document = response[0].doc_tva_id
    apercuTva(dataPath, el_apercu, id_document);
}

function apercuTva(data, el, id_doc) {
    viderApercuTva();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuTva').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuTva').appendChild(element);
    }
}

function viderApercuTva() {
    const doc_selectionne = document.getElementsByClassName("apercuDocTva");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("aperculistdocTva") != undefined) {
        if (document.getElementById("aperculistdocTva").querySelector("#view_apercu") != null) {
            document.getElementById("aperculistdocTva").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("aperculistdocTva").querySelector("#view_apercu").remove();
        }
    }
}

$(document).on('click', '.btnSuppDocTva', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_tva_id: $(this).attr('data-id')
    };
    deleteDocTva(data_request);
});

function deleteDocTva(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = fiscal_url.deleteDocTva;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-tva"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsedeleteDocTva', loading);
}
function responsedeleteDocTva(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_tva_id: response.data.id
                    }
                    deleteDocTva(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.envoiTva', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id: $(this).attr('data-dossier'),
        doc_tva_id: $(this).attr('data-id')
    };
    var url_request = fiscal_url.envoiTva;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModal(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}

/****2031******/

function getDoc2031(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }
    var type_request = 'POST';
    var url_request = fiscal_url.getDoc2031;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-fiscal-2031"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDoc2031Callback', loading);
}

function getDoc2031Callback(response) {
    $('#contenair-fiscal-2031').html(response);
}

function getDoc2031Annee(id) {
    var doc_2031_annee = $('#doc_2031_annee').val();

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id,
        'doc_2031_annee': doc_2031_annee

    };
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-2031"
    }
    var url_request = fiscal_url.listeDoc2031;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDoc2031AnneeCallback', loading);
}

function getDoc2031AnneeCallback(response) {
    $('#content-2031').html(response);
}

$(document).on('change', '#doc_2031_annee', function (e) {
    e.preventDefault();
    var dossier_id = $(this).attr('data-id');
    getDoc2031Annee(dossier_id);
});

$(document).on('click', '#ajoutdoc2031', function (e) {
    e.preventDefault();
    ajoutdoc2031($(this).attr('data-id'), 'add', null);
});

function ajoutdoc2031(dossier_id, action, doc_2031_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: action,
        dossier_id: dossier_id,
        doc_2031_id: doc_2031_id,
        doc_2031_annee: $('#doc_2031_annee').val()
    };
    var url_request = fiscal_url.formUploadDoc2031;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal2031');

    $("#modalAjoutDoc2031").modal('show');
}

function chargerModal2031(response) {
    $('#contenaireUpload2031').html(response);
}

$(document).on('click', '.btnUpdtadeDoc2031', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        doc_2031_id: $(this).attr('data-id'),
        dossier_id: $(this).attr('data-dossier'),
    };
    var url_request = fiscal_url.FormUpdateDoc2031;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_Modal');
});

$(document).on('submit', '#Updatedoc2031', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "Updatedoc2031Callback", "null_fn", param);
});

function Updatedoc2031Callback(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDoc2031(response.data_retour.dossier_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSuppDoc2031', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_2031_id: $(this).attr('data-id')
    };
    deleteDoc2031(data_request);
});

function deleteDoc2031(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = fiscal_url.deleteDoc2031;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-2031"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsedeleteDoc2031', loading);
}
function responsedeleteDoc2031(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_2031_id: response.data.id
                    }
                    deleteDoc2031(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function apercuDocument2031(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = fiscal_url.getpathDoc2031;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepath2031');
}

function responsepath2031(response) {
    var el_apercu = document.getElementById('aperculistdoc2031')
    var dataPath = response[0].doc_2031_path
    var id_document = response[0].doc_2031_id
    apercu2031(dataPath, el_apercu, id_document);
}

function apercu2031(data, el, id_doc) {
    viderApercu2031();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu2031').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu2031').appendChild(element);
    }
}

function viderApercu2031() {
    const doc_selectionne = document.getElementsByClassName("apercuDoc2031");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("aperculistdoc2031") != undefined) {
        if (document.getElementById("aperculistdoc2031").querySelector("#view_apercu") != null) {
            document.getElementById("aperculistdoc2031").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("aperculistdoc2031").querySelector("#view_apercu").remove();
        }
    }
}

$(document).on('click', '.envoi2031', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id: $(this).attr('data-dossier'),
        doc_2031_id: $(this).attr('data-id')
    };
    var url_request = fiscal_url.envoi2031;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

$(document).on('click', '#acompte_tva', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitAcompteTva();
    }, 800);
});

function submitAcompteTva() {
    var form = $("#AddAcompteTva");
    var type_output = 'json';

    fn.ajax_form_data_custom(form, type_output, 'retourAddAcompteTva');
}

function retourAddAcompteTva(response) {
    if (response.status == true) {
        var dossier_id = $('#dossier_id').val();
        // getDoctvaAnnee(dossier_id);

    } else {
        fn.response_control_champ(response);
    }
}

function getDocConfidentiel(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id,
    }
    var type_request = 'POST';
    var url_request = fiscal_url.getDocConfidentiel;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-fiscal-doc_confidentiel"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDocConfidentielCallback', loading);
}

function getDocConfidentielCallback(response) {
    $('#contenair-fiscal-doc_confidentiel').html(response);
}

function ajoutDocConfidentiels(dossier_id, action, doc_confidentiel_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'dossier_id': dossier_id,
        'doc_confidentiel_id': doc_confidentiel_id
    };

    var url_request = fiscal_url.formUploadDocFiscalConfidentiels;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentFiscalconfidetiels');
    $("#modalAjoutConfidentiel").modal('show');
}
function chargercontentFiscalconfidetiels(response) {
    $('#UploadConfidentiel').html(response);
}

$(document).on('click', '#ajoutDocConfidentiels', function () {
    ajoutDocConfidentiels($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '.btnSuppDocConfidentiel', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_id: $(this).attr('data-id')
    };

    deleteDocConfidentiel(data_request);
});

function deleteDocConfidentiel(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = fiscal_url.deleteDocConfidentiel;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-doc_confidentiel"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocConfidentiel', loading);
}

function retourDeleteDocConfidentiel(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_id: response.data.id
                    }
                    deleteDocConfidentiel(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowfiscal-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocConfidentiel', function (e) {
    e.preventDefault();
    var doc_id = $(this).attr("data-id");
    var dossier_id = $(this).attr("data-dossier");

    var type_request = 'POST';
    var type_output = 'html';

    var data_request = {
        action: "edit",
        doc_id: doc_id,
        dossier_id: dossier_id,
    };
    var url_request = fiscal_url.modalupdateDocConfidentiel;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});
function charger_contentModal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentConfidentiel', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocConfidentiel", "null_fn", param);
});
function retourupdatedocConfidentiel(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocConfidentiel(response.data_retour.dossier_id);
    } else {
        fn.response_control_champ(response);
    }
}

function apercuDocumentConfidentiel(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = fiscal_url.getpathDocConfidentiel;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathConfidentiel');
}

function responsepathConfidentiel(response) {
    var el_apercu = document.getElementById('apercuFiscaldocConfidentiel')
    var dataPath = response[0].doc_path
    var id_document = response[0].doc_id
    apercuFiscalConditionel(dataPath, el_apercu, id_document);
}

function apercuFiscalConditionel(data, el, id_doc) {
    viderApercuFiscalConditionel();
    var doc_selectionner = document.getElementById("docrowfiscal-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercudoc_Confidentiel').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercudoc_Confidentiel').appendChild(element);
    }
}

function viderApercuFiscalConditionel() {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercudoc_Confidentiel") != undefined) {
        if (document.getElementById("apercudoc_Confidentiel").querySelector("#view_apercu") != null) {
            document.getElementById("apercudoc_Confidentiel").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudoc_Confidentiel").querySelector("#view_apercu").remove();
        }
    }
}

$(document).on('keyup', '#commentaire_doc_Confidentiel', function () {
    clearTimeout(timer);

    timer = setTimeout(function () {
        var data = {
            commentaire: $('#commentaire_doc_Confidentiel').val(),
            dossier_id: $('#dossier_fiscal_confidentiel').val()
        };

        addModifcommentaireDocConfidentiel(data);
    }, 800);
});
function addModifcommentaireDocConfidentiel(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data

    var url_request = fiscal_url.addModifcommentaireDocConfidentiel;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'addModifcommentaireDocConfidentielRetour');
}
function addModifcommentaireDocConfidentielRetour(response) {
    var jsonObject = JSON.parse(response);
    if (jsonObject.status == true) {
        Notiflix.Notify.success(jsonObject.form_validation.message, { position: 'left-bottom', timeout: 2000 });
    } else {
        Notiflix.Notify.failure(jsonObject.form_validation.message, { position: 'left-bottom', timeout: 2000 });
    }
}