var exportcomptable_url = {
    page_exportcomptable: `${const_app.base_url}ExportComptable/page_exportcomptable`,
    getContentExportComptable: `${const_app.base_url}ExportComptable/getContentExportComptable`,
    listeExportcomptable: `${const_app.base_url}ExportComptable/listeExportcomptable`,
    generer_fichier: `${const_app.base_url}ExportComptable/generer_fichier`,
}

function pgExportComptable() {
    var type_request = 'POST';
    var url_request = exportcomptable_url.page_exportcomptable;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-exportcomptable"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pgExportComptable_Callback', loading);
}
function pgExportComptable_Callback(response) {
    $('#contenair-exportcomptable').html(response);
}

function getContentExportComptable() {
    var type_request = 'POST';
    var url_request = exportcomptable_url.getContentExportComptable;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "content-exportcomptable"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getContentExportComptable_Callback', loading);
}
function getContentExportComptable_Callback(response) {
    $('#content-exportcomptable').html(response);
}

function listeExportcomptable(filtreDateExport) {
    var type_request = 'POST';
    var url_request = exportcomptable_url.listeExportcomptable;
    var type_output = 'html';
    var data_request = { filtreDateExport: filtreDateExport };
    var loading = {
        type: "content",
        id_content: "liste-exportcomptable"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'listeExportcomptable_Callback', loading);
}
function listeExportcomptable_Callback(response) {
    $('#liste-exportcomptable').html(response);
}

$(document).on('change', '#filtreDateExport', function () {
    var selectedDate = $(this).val();
    setTimeout(function () {
        listeExportcomptable(selectedDate);
    }, 800);
});

$(document).on('click', '#generer_fichier', function () {
    var filtreDateExport = $('#filtreDateExport').val();
    generer_fichier(filtreDateExport);
});

function generer_fichier(filtreDateExport) {
    var type_request = 'POST';
    var url_request = exportcomptable_url.generer_fichier;
    var type_output = 'JSON';
    var data_request = { filtreDateExport: filtreDateExport };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'generer_fichier_Callback');
}
function generer_fichier_Callback(response) {
    if (response.status == true) {
        $('#filtreDateExport').val(response.data_retour)
        listeExportcomptable(response.data_retour);
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}