var sepa_url = {
    listMandatSepa :  `${const_app.base_url}Sepa/listMandatSepa`,
    FormMandatSepa :  `${const_app.base_url}Sepa/FormMandatSepa`,
    sendMandatSepa :  `${const_app.base_url}Sepa/sendMandatSepa`,
    SignerMandatSepa :  `${const_app.base_url}Sepa/SignerMandatSepa`,
    getPathSepa :  `${const_app.base_url}Sepa/getPathSepa`,
    removefileSepa :  `${const_app.base_url}Sepa/removefileSepa`,
}

var timer = null;

$(document).ready(function () {

});

function getMandatSepa(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id
    };

    var url_request = sepa_url.listMandatSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'listMandatSepaCallback');
}

function listMandatSepaCallback(response) {
    $('#contenair-sepa').html(response);
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

//Ajout

$(document).on('click', '.ajoutMandatSepa', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        dossier_id: $(this).attr('data-id'),
    };
    var url_request = sepa_url.FormMandatSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandatSepa');
});

function charger_contentMandatSepa(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('keyup', '#sepa_iban', function (e) {
    e.preventDefault();
    if (verifyIBAN($('#sepa_iban').val()) == false) {
        $('div').children('.error_file').text("Format iban incorrect");
        $('.error_file').removeClass("d-none");
    }else{
        $('.error_file').addClass("d-none");
    } 
});

function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

$(document).on('submit', '#AddMandatSepa', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#sepa_rib').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var dossier_id = $("#dossier_id").val();
                    getMandatSepa(dossier_id);
                    Notiflix.Notify.success("Enregistrement réussi", { position: 'left-bottom', timeout: 3000 });
                }else{
                    fn.response_control_champ(obj);
                }
            }
        });
    }
});

//update

$(document).on('click', '.updateMandatSepa', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        dossier_id: $(this).attr('data-id'),
        sepa_id: $(this).attr('data-sepa')
    };
    var url_request = sepa_url.FormMandatSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandatSepa');
});

//Envoi

$(document).on('click', '.sendMandatSepa', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        sepa_id: $(this).attr('data-id'),
    };
    sendMandatSepa(data_request);
});

function sendMandatSepa(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = sepa_url.sendMandatSepa;
    var loading = {
        type: "content",
        id_content: "contenair-sepa"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMandatSepaCallBack', loading);
}
function sendMandatSepaCallBack(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        sepa_id: response.data.id
                    }
                    sendMandatSepa(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var dossier_id = $("#dossier_id").val();

            getMandatSepa(dossier_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

//Signer

$(document).on('click', '.signerMandatSepa', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        dossier_id: $("#dossier_id").val(),
        sepa_id : $(this).attr('data-id')
    };
    var url_request = sepa_url.SignerMandatSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandatSepa');
});

$(document).on('submit', '#AddSignMandatSepa', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#sepa_path').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var dossier_id = $("#dossier_id").val();
                    getMandatSepa(dossier_id);

                    Notiflix.Notify.success("Enregistrement réussi", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

function apercuSepa(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = sepa_url.getPathSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandatSepa');
}

function responsepathMandatSepa(response) {
    var el_apercu = document.getElementById('apercu_list_Sepa')
    var dataPath = response[0].sepa_path
    var id_document = response[0].sepa_id
    apercuDocMandatSepa(dataPath, el_apercu, id_document);
}

function apercuDocMandatSepa(data, el, id_doc) {
    viderApercuMandatSepa();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuMandatSepa').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuMandatSepa').appendChild(element);
    }
}

function viderApercuMandatSepa() {
    const doc_selectionne = document.getElementsByClassName("apercu_docSepa");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuMandatSepa") != undefined) {
        if (document.getElementById("apercuMandatSepa").querySelector("#view_apercu") != null) {
            document.getElementById("apercuMandatSepa").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuMandatSepa").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '#removeribSepa', function () {
    $('.doc').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        sepa_id: $('#sepa_id').val()
    };

    var url_request = sepa_url.removefileSepa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourRemovefile');
});

function retourRemovefile(response) {
    const obj = JSON.parse(response);
    $('#doc').remove();
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

