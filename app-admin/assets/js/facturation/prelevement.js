var prelevement_url = {
    list_prelevement: `${const_app.base_url}Facturation/Prelevement/liste_prelevement`,
    telecharger_fichier: `${const_app.base_url}Facturation/Prelevement/telecharger_fichier`,
    detailsPrelevement: `${const_app.base_url}Facturation/Prelevement/detailsPrelevement`,
    regenerer_Fichier_Xml: `${const_app.base_url}Facturation/Prelevement/regenererFichierXml`,
}

var myModaldetailsPrelevement = new bootstrap.Modal(document.getElementById("myModaldetailsPrelevement"), {
    keyboard: false
});

function listePrelevement() {
    var type_request = 'POST';
    var url_request = prelevement_url.list_prelevement;
    var type_output = 'html';
    var pagination_view = $('#pagination_view_prelev').val();
    $row_data = 100;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;

    var data_request = {
        row_data: $row_data,
        pagination_page: $pagination_page
    };
    var loading = {
        type: "content",
        id_content: "content-table-prelevement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'listePrelevement_Callback', loading);
}

function listePrelevement_Callback(response) {
    $('#content-table-prelevement').html(response);
}

/***** pagination *****/

$(document).on('click', '.pagination-vue-tablePrelevement', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${100}`;
        $('#pagination_view_prelev').val(limit);
        listePrelevement();
    }
});


$(document).on('change', '.row_viewPrelevement', function (e) {
    e.preventDefault();
    listePrelevement();
});

$(document).on('click', '#pagination-vue-nextPrelevement', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-tablePrelevement");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-tablePrelevement-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-previewPrelevement', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-tablePrelevement");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-tablePrelevement-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('click', '#tous_cocher_prelevement', function () {
    var isChecked = $(this).prop('checked');
    $('.facture_prelevement:enabled').prop('checked', isChecked);
    cocher_prelevement();
});

$(document).on('click', '.facture_prelevement', function () {
    $('#tous_cocher_prelevement').attr('checked', false);
    cocher_prelevement();
});

function cocher_prelevement() {
    var numb_cocher = 0;
    numb_cocher = $('.facture_prelevement:checked').length;
    $('#telechargertous_prelevement').attr('disabled', true);
    if (numb_cocher > 0) {
        $('#telechargertous_prelevement').attr('disabled', false);
    }
}

$(document).on('click', '#telechargertous_prelevement', function () {
    var links = [];
    $(".facture_prelevement:checked").each(function () {
        var valeur = $(this).attr('data-doc_prelevement_id');
        links.push(valeur);
    });

    telecharger_fichier(links);
});

function telecharger_fichier(table_doc) {
    var type_request = 'POST';
    var url_request = prelevement_url.telecharger_fichier;
    var type_output = 'html';
    var data_request = table_doc

    var loading = {
        type: "content",
        id_content: "content-table-prelevement"
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'telecharger_fichier_Callback', loading);
}

function telecharger_fichier_Callback(response) {
    var parts = response.split('/');
    forceDownload_zip(`${const_app.base_url}` + response, parts[3]);

}
function forceDownload_zip(url, fileName) {
    var link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

$(document).on('click', '.info_prelevement', function () {
    var idc_prelevement = $(this).attr('data-idc_prelevement');
    detailsPrelevement(idc_prelevement);
});

function detailsPrelevement(idc_prelevement) {
    var type_request = 'POST';
    var url_request = prelevement_url.detailsPrelevement;
    var type_output = 'html';
    var data_request = { idc_prelevement: idc_prelevement }

    var loading = {
        type: "content",
        id_content: "content-table-prelevement"
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'detailsPrelevement_Callback', loading);
}

function detailsPrelevement_Callback(response) {
    $("#details-prelevement").html(response);
    myModaldetailsPrelevement.toggle();
}
$(document).on('click', '.generationFichierXml', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        prelevement_id: $(this).attr('data-idprelevement')
    };
    regenererFichierXml(data_request);
});

function regenererFichierXml(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = prelevement_url.regenerer_Fichier_Xml;
    var loading = {
        type: "content",
        id_content: "content-table-prelevement"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'regenererFichierXmlCallback', loading);
}
function regenererFichierXmlCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        prelevement_id: response.data.idprelevement
                    }
                    regenererFichierXml(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            var parts = response.url.split('/');
            forceDownload_xml(`${const_app.base_url}` + response.url, parts[4]);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

