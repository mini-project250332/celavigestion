var reglements_url = {
    pageReglement: `${const_app.base_url}Facturation/Reglements/pageReglement`,
}

$(document).on('click', '.saisir_reglements', function () {
    var fact_id = $(this).attr('data-fact_id');
    pageReglement(fact_id);
});

function pageReglement(fact_id) {
    var type_request = 'POST';
    var url_request = reglements_url.pageReglement;
    var type_output = 'html';
    var data_request = { fact_id: fact_id };
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageReglement_Callback', loading);
}
function pageReglement_Callback(response) {
    $('#contenair-facture-impaye').html(response);
}

$(document).on('click', '#annulerReglement', function () {
    getFactureImpaye();
});

$(document).on('submit', '#saveReglement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveReglement", loading);
});
function retoursaveReglement(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFactureImpaye();
        getFacturePaye();
    } else {
        fn.response_control_champ(response);
    }
}