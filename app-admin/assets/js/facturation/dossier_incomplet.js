var dossier_incomplet_url = {
    list_dossier_incomplet: `${const_app.base_url}Facturation/Dossier_incomplet/ListeDossierIncomplet`,
}

function getTableDossier_incomplet() {
    var type_request = 'POST';
    var url_request = dossier_incomplet_url.list_dossier_incomplet;
    var type_output = 'html';
    var pagination_view = $('#pagination_view').val();
    $row_data = 100;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;

    var data_request = {
        proprietaire_lot_sans_mandat: $('#proprietaire_lot_sans_mandat').val(),
        programme_lot_sans_mandat: $('#programme_lot_sans_mandat').val(),
        row_data: $row_data,
        pagination_page: $pagination_page
    };
    var loading = {
        type: "content",
        id_content: "content-table-dossier_incomplet"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTableDossier_incomplet_Callback', loading);
}

function getTableDossier_incomplet_Callback(response) {
    $('#content-table-dossier_incomplet').html(response);
}

$(document).on('keyup', '#proprietaire_lot_sans_mandat', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableDossier_incomplet();
    }, 500);
});

$(document).on('keyup', '#programme_lot_sans_mandat', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableDossier_incomplet();
    }, 500);
});

/***** pagination *****/

$(document).on('click', '.pagination-vue-table', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${100}`;
        $('#pagination_view').val(limit);
        getTableDossier_incomplet();
    }
});


$(document).on('change', '.row_view', function (e) {
    e.preventDefault();
    getTableDossier_incomplet();
});

$(document).on('click', '#pagination-vue-next', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('click', '.ficheLot', function () {
    var lot_id = $(this).attr('data-id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');
    var href = $('#proprietaire-menu-web').attr('href');
    window.location.href = href + `?_p@roprietaire=${client_id}&_lo@t_id=${lot_id}&_nom=${nom}`;
});
