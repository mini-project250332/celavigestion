var remboursements_url = {
    pageRemboursement: `${const_app.base_url}Facturation/Remboursement/pageRemboursement`,
    pageRemboursementAvoir: `${const_app.base_url}Facturation/Remboursement/pageRemboursementAvoir`,
}

$(document).on('click', '.saisir_Remboursements', function () {
    var Idreglement = $(this).attr('data-reglementId');
    pageRemboursement(Idreglement);
});

function pageRemboursement(Idreglement) {
    var type_request = 'POST';
    var url_request = remboursements_url.pageRemboursement;
    var type_output = 'html';
    var data_request = { Idreglement: Idreglement };
    var loading = {
        type: "content",
        id_content: "contenair-facture-Paye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageRemboursement_Callback', loading);
}
function pageRemboursement_Callback(response) {
    $('#contenair-facture-Paye').html(response);
}

$(document).on('click', '#annulerRemboursement', function () {
    getFacturePaye();
});

$(document).on('submit', '#saveRemboursement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-facture-Paye"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveRemboursement", loading);
});
function retoursaveRemboursement(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturePaye();
        getFactureImpaye();
    } else {
        fn.response_control_champ(response);
    }
}


//Remboursement Avoir
$(document).on('click', '.saisir_RemboursementsAvoir', function () {
    var fact_id = $(this).attr('data-fact_id');
    pageRemboursementAvoir(fact_id);
});

function pageRemboursementAvoir(fact_id) {
    var type_request = 'POST';
    var url_request = remboursements_url.pageRemboursementAvoir;
    var type_output = 'html';
    var data_request = { fact_id: fact_id };
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageRemboursementAvoir_Callback', loading);
}
function pageRemboursementAvoir_Callback(response) {
    $('#contenair-facture-impaye').html(response);
}

$(document).on('click', '#annulerremboursementsAvoir', function () {
    getFactureImpaye();
});

$(document).on('submit', '#saveRemboursementAvoir', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveRemboursementAvoir", loading);
});


function retoursaveRemboursementAvoir(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturePaye();
        getFactureImpaye();
    } else {
        fn.response_control_champ(response);
    }
}