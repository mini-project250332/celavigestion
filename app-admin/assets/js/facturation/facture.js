var facture_url = {
    getTableFacture: `${const_app.base_url}Facturation/Facture/getTableFacture`,
    listExportPDf: `${const_app.base_url}Facturation/Facture/listExportPDf`,
}

function getTableFacture(annee) {
    var type_request = 'POST';
    var url_request = facture_url.getTableFacture;
    var type_output = 'html';
    var row_view = $('#row_view_NouvelleFacture').val();
    var pagination_view = $('#pagination_view_NouvelleFacture').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 100;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;
    var data_request = {
        annee: annee,
        facture_filtre_proprietaire: $('#facture_filtre_proprietaire').val(),
        facture_filtre_progrm: $('#facture_filtre_progrm').val(),
        num_dossier_facture: $('#num_dossier_facture').val(),
        num_facture: $('#num_facture').val(),
        etatfacture: $('#paiementEtat').val(),
        date_debut: $('#date_debut').val(),
        date_fin: $('#date_fin').val(),
        row_data: $row_data,
        pagination_page: $pagination_page,
    };
    var loading = {
        type: "content",
        id_content: "contenair-facture-content"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTableFacture_Callback', loading);
}

function getTableFacture_Callback(response) {
    $('#content-table-facture').html(response);
}

/***** pagination *****/

$(document).on('click', '.pagination-vue-table-facturenouvelle', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view_NouvelleFacture').val()}`;
        $('#pagination_view_NouvelleFacture').val(limit);
        getTableFacture($('#tri_annee_facture').val());
    }
});


$(document).on('change', '.row_view_NouvelleFacture', function (e) {
    e.preventDefault();
    getTableFacture($('#tri_annee_facture').val());
});


$(document).on('click', '#pagination-vue-next-facturenouvelle', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-facturenouvelle .pagination-vue-table-facturenouvelle");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-facturenouvelle-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview-Facturenouvelle', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-facturenouvelle .pagination-vue-table-facturenouvelle");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-facturenouvelle-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('change', '#tri_annee_facture', function () {
    getTableFacture($(this).val())
});

$(document).on('keyup', '#num_dossier_facture', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 500);
});

$(document).on('keyup', '#facture_filtre_progrm', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 500);
});

$(document).on('keyup', '#facture_filtre_proprietaire', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 500);
});

$(document).on('keyup', '#num_facture', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 500);
});

$(document).on('change', '#date_debut', function name(params) {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 800);
});

$(document).on('change', '#date_fin', function name(params) {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getTableFacture($('#tri_annee_facture').val());
    }, 800);
});

$(document).on('change', '#paiementEtat', function () {
    getTableFacture($('#tri_annee_facture').val());
});
$(document).on('click', '.fiche-dossier', function (e) {
    var dossier_id = $(this).attr('data-dossier_id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');

    var href = $('#proprietaire-menu-web').attr('href');
    newLocation = href + `?_p@roprietaire=${client_id}&_do@t_id=${dossier_id}&_nom=${nom} `;
    window.open(newLocation, '_blank');
});


/**** export pdf ****/

$(document).on('click', '#exportPdf_factures', function () {
    var annee = $('#tri_annee_facture').val();
    var type_request = 'POST';
    var url_request = facture_url.listExportPDf;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        facture_filtre_proprietaire: $('#facture_filtre_proprietaire').val(),
        facture_filtre_progrm: $('#facture_filtre_progrm').val(),
        num_dossier_facture: $('#num_dossier_facture').val(),
        num_facture: $('#num_facture').val(),
    };
    var loading = {
        type: "content",
        id_content: "content-table-facture"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ExportPDf_factures_Callback', loading);
});

function ExportPDf_factures_Callback(response) {
    var href = `${const_app.base_url}` + response;
    forceDownload(href);
}