var facturation_url = {
    // appelle des sous menu facturation
    page_facturation: `${const_app.base_url}Facturation/page_facturation`,
    //Preparation facture
    page_preparation_facture: `${const_app.base_url}Facturation/Preparation_facturation/page_preparation_facture`,
    //A facturer 
    page_facture_valide: `${const_app.base_url}Facturation/A_Facturer/page_facture_valide`,
    // Facturer 
    facture: `${const_app.base_url}Facturation/Facture/facture`,
    // Dossier incomplet 
    dossier_incomplet: `${const_app.base_url}Facturation/Dossier_incomplet/dossier_incomplet`,
    //Facture impaye
    page_facture_impaye: `${const_app.base_url}Facturation/Facturation_impaye/page_facture_impaye`,
    //Facture Payé page_facture_paye
    page_facture_paye: `${const_app.base_url}Facturation/Facturation_paye/page_facture_paye`,
    //Liste prelevement fichier XML
    listeprelevement: `${const_app.base_url}Facturation/Prelevement/prelevement`,

}

function pgFacturation() {
    var type_request = 'POST';
    var url_request = facturation_url.page_facturation;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pgFacturation_Callback', loading);
}

function pgFacturation_Callback(response) {
    $('#contenair-facturation').html(response);
}

function getPreparationFacture() {

    var type_request = 'POST';
    var url_request = facturation_url.page_preparation_facture;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-preparation-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getPreparationFacture_Callback', loading);
}

function getPreparationFacture_Callback(response) {
    $('#contenair-preparation-facturation').html(response);
}

function getAllFactureValide() {
    var type_request = 'POST';
    var url_request = facturation_url.page_facture_valide;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-facturation-valide"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getAllFactureValide_Callback', loading);
}

function getAllFactureValide_Callback(response) {
    $('#contenair-facturation-valide').html(response);
}

function getFacture() {
    var type_request = 'POST';
    var url_request = facturation_url.facture;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-facture-content"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFacture_Callback', loading);
}

function getFacture_Callback(response) {
    $('#contenair-facture-content').html(response);
}

function getDossierIncomplet() {
    var type_request = 'POST';
    var url_request = facturation_url.dossier_incomplet;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-dossier-incomplet"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDossierIncomplet_Callback', loading);
}

function getDossierIncomplet_Callback(response) {
    $('#contenair-dossier-incomplet').html(response);
}

function getFactureImpaye() {
    var type_request = 'POST';
    var url_request = facturation_url.page_facture_impaye;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFactureImpaye_Callback', loading);
}

function getFactureImpaye_Callback(response) {
    $('#contenair-facture-impaye').html(response);
}

function getFacturePaye() {
    var type_request = 'POST';
    var url_request = facturation_url.page_facture_paye;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-facture-Paye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFacturePaye_Callback', loading);
}

function getFacturePaye_Callback(response) {
    $('#contenair-facture-Paye').html(response);
}

function getFichierSepa() {
    var type_request = 'POST';
    var url_request = facturation_url.listeprelevement;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-fichierSepa-content"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFichierSepa_Callback', loading);
}

function getFichierSepa_Callback(response) {
    $('#contenair-fichierSepa-content').html(response);
}
