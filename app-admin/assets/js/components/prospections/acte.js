var acte_prospect_lot_url = {
    listeDocActe: `${const_app.base_url}ActeProspect/listeDocActe`,
    formUploadDocActe: `${const_app.base_url}ActeProspect/formUploadDocActe`,
    deleteDocActe: `${const_app.base_url}ActeProspect/removeDocActe`,
    modalupdateDocActe: `${const_app.base_url}ActeProspect/FormUpdateActe`,
    getDocpath: `${const_app.base_url}ActeProspect/getDocpath`,
}

function getLotActe(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'lot_id': id,
    };
    var url_request = acte_prospect_lot_url.listeDocActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocActe');
}

function loadListeDocActe(response) {
    $('#contenair-acte').html(response);
}

$(document).on('click', '#ajoutdocActe_prospect_lot', function (e) {
    e.preventDefault();
    ajoutdocActe_Lot_Prospect($(this).attr('data-id'), 'add', null);
});

function ajoutdocActe_Lot_Prospect(lot_id, action, docp_acte_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'lot_id': lot_id,
        'docp_acte_id': docp_acte_id
    };
    var url_request = acte_prospect_lot_url.formUploadDocActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal_Lot_Prospect');

    $("#modalAjoutDocActe").modal('show');
}

function chargercontentModal_Lot_Prospect(response) {
    $('#contenaireUploadActe').html(response);
}

$(document).on('click', '.btnSuppDoc_Prospect_lot', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        docp_acte_id: $(this).attr('data-id')
    };
    deleteDocActe_ProspectLot(data_request);
});

function deleteDocActe_ProspectLot(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_prospect_lot_url.deleteDocActe;
    var loading = {
        type: "content",
        id_content: "contenair-acte"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocActe_ProspectLot', loading);
}

function retourDeleteDocActe_ProspectLot(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        docp_acte_id: response.data.id
                    }
                    deleteDocActe_ProspectLot(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=rowdocprospectlot_acte-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});


$(document).on('click', '.btnUpdtadeDocActe_prospect_lot', function (e) {
    e.preventDefault();
    var docp_acte_id = $(this).attr("data-id");
    var lot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        docp_acte_id: docp_acte_id,
        lot_id: lot_id
    };
    var url_request = acte_prospect_lot_url.modalupdateDocActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_prospect');
});

function charger_contentModal_prospect(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentActe_Prospect', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocActe_prospect", "null_fn", param);
});

function retourupdatedocActe_prospect(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getLotActe(response.data_retour.lot_id);
    } else {
        fn.response_control_champ(response);
    }
}

function apercuDocumentActeProspect(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'docp_acte_id': id_doc,
    };
    var url_request = acte_prospect_lot_url.getDocpath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseDOcpath_prospectActe');
}

function responseDOcpath_prospectActe(response) {
    var el_apercu = document.getElementById('apercu_list_doc_prospect_acte');
    var dataPath = response[0].docp_acte_path;
    var id_document = response[0].docp_acte_id;
    apercu_prospect_acte(dataPath, el_apercu, id_document);
}

function apercu_prospect_acte(data, el, id_doc)
{
    viderApercu_acte_prospect();
    var doc_selectionner = document.getElementById("rowdocprospectlot_acte-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu').appendChild(element);
    }
}

function viderApercu_acte_prospect() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu_prospect_acte");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercu_list_doc_prospect_acte") != undefined) {
        if (document.getElementById("apercu_list_doc_prospect_acte").querySelector("#view_apercu") != null) {
            document.getElementById("apercu_list_doc_prospect_acte").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_list_doc_prospect_acte").querySelector("#view_apercu").remove();
        }
    }
}