var data_url = {
    header: `${const_app.base_url}Prospections/getheaderlist`,
    prospect: `${const_app.base_url}Prospections/listeProspect`,
    transformerClient: `${const_app.base_url}Prospections/transformerClient`,
    updateDateSignatureMandat: `${const_app.base_url}Prospections/updateDateSignatureMandat`,
    formNew: `${const_app.base_url}Prospections/formNewProspect`,
    formUpdate: `${const_app.base_url}Prospections/formUpdateProspect`,
    ficheProspect_page: `${const_app.base_url}Prospections/pageficheProspect`,
    delete: `${const_app.base_url}Prospections/removeProspect`,
    removefilecin: `${const_app.base_url}Prospections/removefilecin`,
    modalEnvoiMailCNI: `${const_app.base_url}SendMail/modalEnvoiMailCNI`,
    demande_cni: `${const_app.base_url}SendMail/demande_cni`,
    alert_compte_parametre: `${const_app.base_url}Prospections/alert_compte_parametre`,
    removefilerib: `${const_app.base_url}Prospections/removefilerib`,
    /****lot*****/
    formNewLot: `${const_app.base_url}Prospections/formNewLot`,
    listeLot_page: `${const_app.base_url}Prospections/listLot`,
    ficheLot: `${const_app.base_url}Prospections/FicheLot`,
    getLot: `${const_app.base_url}Prospections/getFicheLot`,
    getFormLot: `${const_app.base_url}Prospections/getFormLot`,
    deleteLot: `${const_app.base_url}Prospections/removeLot`,
    modalFormGestionnaire: `${const_app.base_url}Prospections/FormGestionnaire`,
    modalFormProgramme: `${const_app.base_url}Prospections/FormProgramme`,
    /***mandat***/
    listeMandat: `${const_app.base_url}Prospections/listeMandat`,
    FormMandat: `${const_app.base_url}Prospections/FormMandat`,
    UploadMandat: `${const_app.base_url}Prospections/FormUploadMandat`,
    getPathMandat: `${const_app.base_url}Prospections/getPathMandat`,
    deleteDocMandat: `${const_app.base_url}Prospections/removeDocMandat`,
    deleteMandat: `${const_app.base_url}Prospections/removeMandat`,
    getPath: `${const_app.base_url}Prospections/getPath`,
    EnvoiMandat: `${const_app.base_url}SendMail/EnvoiMandat`,
    SignerMandat: `${const_app.base_url}Prospections/SignerMandat`,
    closMandat: `${const_app.base_url}Prospections/closMandat`,
    cloturerMandat: `${const_app.base_url}Prospections/cloturerMandat`,
    sendMandat: `${const_app.base_url}Prospections/sendMandat`,
    /*****com******/
    listeCom: `${const_app.base_url}Prospections/listCommunication`,
    getFormCom: `${const_app.base_url}Prospections/getFormCom`,
    deleteCom: `${const_app.base_url}Prospections/removeCom`,
    formUpload: `${const_app.base_url}Prospections/formUpload`,
    updateInfoProspect: `${const_app.base_url}Prospections/updateParcoursProspect`,
    getFormTache: `${const_app.base_url}Prospections/getFormTache`,
    deleteTache: `${const_app.base_url}Prospections/removeTache`,
    valideTache: `${const_app.base_url}Prospections/valideTache`,
    listeProspectSupp: `${const_app.base_url}Prospections/listeProspectSupp`,
    activerProspect: `${const_app.base_url}Prospections/activerProspect`,
    listeDocument: `${const_app.base_url}Prospections/listDocument`,
    getDocpath: `${const_app.base_url}Prospections/getDocpath`,
    deleteDoc: `${const_app.base_url}Prospections/removeDoc`,
    modalupdateDoc: `${const_app.base_url}Prospections/formDocument`,

    ficheClient_prospect: `${const_app.base_url}Clients/ficheClient`,
    verifier_compte: `${const_app.base_url}Prospections/verifier_compte`,
    envoiMail: `${const_app.base_url}SendMail/envoiMail`,
    modalformEnvoiWelcome: `${const_app.base_url}SendMail/modalformEnvoiWelcome`,
    getFormMail: `${const_app.base_url}Prospections/getFormMail`,
    listeProspectConverti : `${const_app.base_url}Prospections/listeProspectConverti`,
    listeProspectPerdu: `${const_app.base_url}Prospections/listeProspectPerdu`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgProspect() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.header;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_headerListe', loading);
}
function load_headerListe(response) {
    $('#contenair-prospect').html(response);
}

$(document).on('click', '#retourListeProspect', function (e) {
    e.preventDefault();
    var currentUrl = window.location.href;
    var parts = currentUrl.split('?');
    if (parts.length === 2) {
        var baseUrl = parts[0];
        window.location.href = baseUrl;
    }
    pgProspect();
});

/***** filtre *****/

$(document).on('keyup', '#text_filtre', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listProspect();
    }, 800);
});

$(document).on('change', '#produit_id', function (e) {
    e.preventDefault();
    listProspect();
});

$(document).on('change', '#origine_cli_id', function (e) {
    e.preventDefault();
    listProspect();
});

$(document).on('change', '#origine_dem_id', function (e) {
    e.preventDefault();
    listProspect();
});

$(document).on('change', '#parcours_vente_id', function (e) {
    e.preventDefault();
    listProspect();
});

$(document).on('change', '#etat_prospect_id', function (e) {
    e.preventDefault();
    listProspect();
});



$(document).on('change', '#nationalite_filtre', function (e) {
    e.preventDefault();
    if ($(this).val() != "autre") {
        listProspect();
    } else {
        $('#select-filtre').toggleClass('d-none');
        $('#select-input-filtre').toggleClass('d-none');
    }
});


$(document).on('keyup', '#text_nationalite', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listProspect();
    }, 800);
});


$(document).on('click', '#return_select_filtre_nationalite', function (e) {
    e.preventDefault();
    $('#select-filtre').toggleClass('d-none');
    $('#select-input-filtre').toggleClass('d-none');
    $("#text_nationalite").val("");
    $("#nationalite_filtre").val($("#nationalite_filtre option:first").val());
});


/***** pagination *****/

$(document).on('click', '.pagination-vue-table', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view').val()}`;
        $('#pagination_view').val(limit);
        listProspect();
    }
});


$(document).on('change', '.row_view', function (e) {
    e.preventDefault();
    listProspect();
});

$(document).on('click', '#pagination-vue-next', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-${array_offset[index_prev]}`).trigger("click");
    }
});


/***** filtre *****/

function listProspect() {
    var type_request = 'POST', type_output = 'html';
    var text_filtre = $('#text_filtre').val(),
        produit_id = $('#produit_id').val(),
        origine_cli_id = $('#origine_cli_id').val(),
        nationalite_filtre = ($('#nationalite_filtre').val() != "autre") ? $('#nationalite_filtre').val() : $('#text_nationalite').val(),
        origine_dem_id = $('#origine_dem_id').val(),
        parcours_vente_id = $('#parcours_vente_id').val(),
        etat_prospect_id = $('#etat_prospect_id').val(),
        date_filtre = $('#date_filtre').val(),
        row_view = $('#row_view').val(),
        pagination_view = $('#pagination_view').val();

    $row_data = (typeof row_view !== 'undefined') ? row_view : 200;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;

    var data_request = {
        text_filtre: text_filtre,
        produit_id: produit_id,
        origine_cli_id: origine_cli_id,
        nationalite_filtre: nationalite_filtre,
        origine_dem_id: origine_dem_id,
        parcours_vente_id: parcours_vente_id,
        etat_prospect_id: etat_prospect_id,
        date_filtre: date_filtre,
        row_data: $row_data,
        pagination_page: $pagination_page,
    };

    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.prospect;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listListe', loading);
}
function load_listListe(response) {
    $('#data-list').html(response);
}

$(document).on('click', '#btnFormNewProspect', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.formNew;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formNew', loading);
});
function load_formNew(response) {
    $('#contenair-prospect').html(response);
}

$(document).on('submit', '#formAddAjout', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddProspect", loading);
});
function retourAddProspect(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        //pgProspect();
        var data_request = {
            prospect_id: response.data_retour
        };
        ficheProspect(data_request);

        $("#btn-update-prospect").trigger("click")

    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnFicheProspectNewTab', function (e) {
    e.preventDefault();
    const prospectId = $(this).attr('data-id');
    var href = $('#prospect-open-tab-url').val();
    window.open(`${href}?_open@prospect_tab=${prospectId}`, "_blank");
});

$(document).on('click', '.btnFormUpdateProspect', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        prospect_id: $(this).attr('data-id')
    };
    ficheProspect(data_request);
});


function ficheProspect(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdate', loading);
}
function load_formUpdate(response) {
    $('#contenair-prospect').html(response);
}

$(document).on('click', '#btn-update-prospect', function (e) {
    e.preventDefault();
    $("#annulerUpdateProspect").toggleClass('d-none');
    $("#save-update-prospect").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationProspect($(this).attr('data-id'), 'form');
});

$(document).on('click', '#annulerUpdateProspect', function (e) {
    e.preventDefault();
    $('#btn-update-prospect').toggleClass('d-none');
    $("#save-update-prospect").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationProspect($(this).attr('data-id'), 'fiche');
});

function getFicheIdentificationProspect(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'prospect_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.ficheProspect_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheProspect', loading);
}
function retourFicheProspect(response) {
    $('#fiche-identifiant-prospect').html(response);
}

/***** update real time ****/

$(document).on('keyup', '#prospect_phonemobile', function () {
    var prospect_phonemobile = $(this).val();
    const noSpecialCharacters = prospect_phonemobile.replace(/[^a-zA-Z0-9 ]/g, '');
    const number = noSpecialCharacters.replace(/\D/g, '');
    const phonemobile = number.replace(/\s/g, '');

    $(this).val(phonemobile);
});

$(document).on('keyup', '#prospect_phonefixe', function () {
    var prospect_phonefixe = $(this).val();
    const noSpecialCharacters = prospect_phonefixe.replace(/[^a-zA-Z0-9 ]/g, '');
    const number = noSpecialCharacters.replace(/\D/g, '');
    const phonefixe = number.replace(/\s/g, '');

    $(this).val(phonefixe);
});


$(document).on('submit', '#updateInfoProspect', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#cin').prop('files')[0];
    fd.append("file", file);
    var file_rib = $('#file_rib').prop('files')[0];
    fd.append("file_rib", file_rib);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });

    var required_data = true;

    if (!$('#prospect_nom').val()) {
        required_data = false;
        $('.help_nom').text('Ce champ ne doit pas être vide.');
    }
    else if (!$('#prospect_email').val()) {
        required_data = false;
        $('.help_email1').text('Ce champ ne doit pas être vide.');
    }
    else if (!$('#produit_id').val()) {
        required_data = false;
        $('.help_prestation').text('Ce champ ne doit pas être vide.');
    }

    if ($('#prospect_email').val() && !(validateEmail($('#prospect_email').val()))) {
        required_data = false;
        $('.help_email1').text("L'information que vous avez entrée est du format incorrect.");
    }

    if ($('#prospect_iban').val() && verifyIBAN($('#prospect_iban').val()) == false) {
        required_data = false;
        $('.help_iban').text("L'IBAN n'est pas valide. Il n'a pas été enregistré.");
    }

    if (required_data == true) {
        $('.help').each(function () {
            $(this).text('');
        });
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});


function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

function validateEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

$(document).on('change', '#cin', function () {
    $('#affichage_cin').empty();
    $('#affichage_cin').append("<div class='row'><div class='col'><button class='btn btn-sm btn-secondary fs--2' type='button' id='historique_mail_prospect'>Historique d'email</button></div></div>");
});

$(document).on('click', '#removefilecin', function () {
    $('.doc').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        prospect_id: $(this).attr('data-id')
    };
    var url_request = data_url.removefilecin;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilecin');
});

function retour_removefilecin(response) {
    $('#affichage_cin').empty();
    $('#affichage_cin').append("<div class='row'><div class='col-7'><div class='fs--2 text-danger text-center pt-1'><i>Attention, vous devez renseigné la CNI pour ce prospect.</i></div></div><div class='col-2 text-end'><button class='btn btn-sm btn-secondary fs--2' type='button' id='historique_mail_prospect'>Historique</button></div><div class='col-3 text-end'><div class='text-end'><button class='btn btn-sm btn-primary fs--2' type='button' title='Demander CNI' id='submit_cin'>Demander CNI</button></div></div></div>");
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

$(document).on('click', '#removefilerib', function () {
    $('.doc_rib').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        prospect_id: $(this).attr('data-id')
    };
    var url_request = data_url.removefilerib;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilerib');
});

function retour_removefilerib(response) {
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

function retourUpdate(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationProspect(response.data_retour.prospect_id, 'fiche');
        $("#annulerUpdateProspect").toggleClass('d-none');
        $("#save-update-prospect").toggleClass('d-none');
        $("#btn-update-prospect").toggleClass('d-none');
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#submit_cin', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        email: $('#prospect_email').val(),
        util_id: $('#util_id').val(),
        'prospect_id': $(this).attr('data-id'),
    };
    var url_request = data_url.modalEnvoiMailCNI;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal_prospect');
});

var Modal_mail_prospect = new bootstrap.Modal(document.getElementById("FormModalMail"), {
    keyboard: false
});

function chargerModal_prospect(response) {
    $("#modalContentMail").html(response);
    Modal_mail_prospect.toggle();
}

$(document).on('click', '#sendMailCNI', function () {
    if ($('#destinataire').val() && validateEmail($('#destinataire').val()) && $('#mess_email').val() && validateEmail($('#mess_email').val())) {
        var type_request = 'POST';
        var type_output = 'html';
        var data_request = {
            prospect_id: $('#prospect_id').val(),
            email: $('#destinataire').val(),
            util_id: $('#util_id').val(),
            object: $('#object_CNI').val(),
            message_text: $('#message_text').val().replace(/\n/g, "<br/>"),
        };
        var url_request = data_url.demande_cni;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'demande_cni_Callback');
        Modal_mail_prospect.toggle();
    }
    else if ($('#destinataire').val() && !(validateEmail($('#destinataire').val()))) {
        Notiflix.Notify.failure("Format du destinataire non valide.", { position: 'left-bottom', timeout: 3000 });
    }
    else if (!$('#mess_email').val() && !(validateEmail($('#mess_email').val()))) {
        Notiflix.Notify.failure("Emeteur vide dans le paramètre modèle de mail", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Vous devez remplir le champ destinataire", { position: 'left-bottom', timeout: 3000 });
    }
});

function demande_cni_Callback(response) {
    const res = JSON.parse(response);
    if (res.status == 200) {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
        const message = `Email envoyé à ${res.destinataire} ayant comme objet ${res.objet}`;
        historySendMailCNIProspect(message, res.prospect_id, res.history);
    }
    else {
        Notiflix.Notify.failure("Email non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('keyup', '#updateInfoProspect input, #updateInfoProspect textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $('#updateInfoProspect').trigger('submit');
    }, 800);
});
$(document).on('change', '#updateInfoProspect select, #updateInfoProspect input', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $('#updateInfoProspect').trigger('submit');
    }, 800);
});

$(document).on('click', '#save-update-prospect', function () {

    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $("#annulerUpdateProspect").toggleClass('d-none');
        $("#save-update-prospect").toggleClass('d-none');
        $("#btn-update-prospect").toggleClass('d-none');
        getFicheIdentificationProspect($(this).attr('data-id'), 'fiche');
    }, 300);
});

$(document).on('change', '#prospect_nationalite', function () {

    if ($(this).val() == "Française") {
        $('.select2').val(66).trigger('change');
    }
    if ($(this).val() == "Britannique") {
        $('.select2').val(189).trigger('change');
    }

    if ($(this).val() == "Irlandaise") {
        $('.select2').val(109).trigger('change');
    }
});

$(document).on('keyup', '#prospect_phonemobile', function () {
    var prospect_phonemobile = $(this).val();
    const noSpecialCharacters = prospect_phonemobile.replace(/[^a-zA-Z0-9 ]/g, '');
    const phonemobile = noSpecialCharacters.replace(/\s/g, '');
    $(this).val(phonemobile);
});

$(document).on('keyup', '#prospect_phonefixe', function () {
    var prospect_phonefixe = $(this).val();
    const noSpecialCharacters = prospect_phonefixe.replace(/[^a-zA-Z0-9 ]/g, '');
    const phonefixe = noSpecialCharacters.replace(/\s/g, '');
    $(this).val(phonefixe);
});

$(document).on('click', '.parcours', function (e) {
    e.preventDefault();
    var vente_id = $(this).attr("data-id");
    var prospect_id = $(this).attr("data-prospect");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { vente_id: vente_id, prospect_id: prospect_id };
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.updateInfoProspect;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour', loading);
});

function retour(response) {
    response = JSON.parse(response);
    if (response.status == true) {
        Notiflix.Notify.success("Parcours modifié", { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationProspect(response.data_retour.prospect_id, 'form');
    } else {
        fn.response_control_champ(response);
    }
}

/******* *****/


$(document).on('click', '#btnAnnuler', function (e) {
    pgProspect();
});

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        prospect_id: $(this).attr('data-id')
    };
    deleteProspect(data_request);
});
function deleteProspect(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteProspect', loading);
}
function retourDeleteProspect(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        prospect_id: response.data.id
                    }
                    deleteProspect(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowProspect-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/*********Lots*******/

function getLotProspect(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'prospect_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    var url_request = data_url.listeLot_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeLot', loading);
}

function loadListeLot(response) {
    $('#contenair-lot').html(response);
}

function getFormLot(prospect_id, action, lot_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'prospect_id': prospect_id,
        'lot_id': lot_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    var url_request = data_url.getFormLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormLot', loading);
}
function retourFormLot(response) {
    $('#contenair-lot').html(response);
}

$(document).on('click', '#btnFormNewlot', function (e) {
    e.preventDefault();
    getFormLot($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerAddLot', function (e) {
    e.preventDefault();
    getLotProspect($(this).attr('data-id'));
});

$(document).on('click', '.retourProspect', function (e) {
    e.preventDefault();
    var data_request = {
        prospect_id: $(this).attr('data-id')
    };
    ficheProspect(data_request);
});

$(document).on('click', '#btnRetourListeLot', function (e) {
    e.preventDefault();
    var data_request = {
        prospect_id: $(this).attr('data-prospect')
    };
    getLotProspectRetour(data_request);
    // $('.retourProspect').trigger('click');

});

function getLotProspectRetour(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.formUpdate;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdateRetour', loading);
}
function load_formUpdateRetour(response) {
    $('#contenair-prospect').html(response);

    $(".nav-link").each(function () {
        if ($('a').hasClass('active')) {
            $('a').removeClass('active');
        }
    });
    $('#lot-tab').addClass('active');

    $(".tab-pane").each(function () {
        if ($('.tab-pane').hasClass('show active')) {
            $('.tab-pane').removeClass('show active');
        }
    });
    $('#tab-lot').addClass('show active');
}

$(document).on('click', '.btnFicheLot', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        lot_id: $(this).attr('data-id'),
        prospect_id: $(this).attr('data-prospect_id')
    };
    getFicheLot(data_request);
});

function getFicheLot(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.getLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheLot', loading);
}
function retourFicheLot(response) {
    $('#contenair-prospect').html(response);
}

function getFicheIdentificationLot(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'lot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    var url_request = data_url.ficheLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFicheLot', loading);
}
function returnFicheLot(response) {
    $('#fiche-identifiant-lot').html(response);
}

$(document).on('submit', '#cruLot', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddlot", loading);
});

function retourAddlot(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        // getLotProspect(response.data_retour.prospect_id);
        var data_request = {
            lot_id: response.data_retour.lot_id,
            prospect_id: response.data_retour.prospect_id
        };
        getFicheLot(data_request);

    } else {
        fn.response_control_champ(response);
    }
}

/***** update real time ****/

$(document).on('submit', '#updateLot', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAdd", loading);
});

function retourAdd(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot(response.data_retour.lot_id, 'fiche');
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#updateLot input, #cruLot textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateLot();
    }, 800);
});
$(document).on('change', '#updateLot select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateLot();
    }, 800);
});

function submitUpdateLot() {
    var form = $("#updateLot");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output);
}


$(document).on('click', '#btn-update-lot', function (e) {
    e.preventDefault();
    getFicheIdentificationLot($(this).attr('data-id'), 'form');
});

$(document).on('click', '#annulerUpdateLot', function (e) {
    e.preventDefault();
    getFormLot($(this).attr('data-prospect_id'), 'fiche', $(this).attr('data-id'));
});

$(document).on('click', '.btnConfirmSupLot', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        lot_id: $(this).attr('data-id')
    };
    delete_lot(data_request);
});
function delete_lot(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteLot;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteLot', loading);
}
function retourDeleteLot(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        lot_id: response.data.id
                    }
                    delete_lot(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowLot-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/********Mandat/lot**********/

function getLotMandat(id, id_p) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'lot_id': id,
        'prospect_id': id_p
    };
    var url_request = data_url.listeMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeMandat');
}

function loadListeMandat(response) {
    $('#contenair-mandat').html(response);
}

$(document).on('click', '.ajoutMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        lot_id: $(this).attr('data-id'),
        prospect_id: $(this).attr('data-prospect'),
    };
    var url_request = data_url.FormMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('click', '.updatemandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        lot_id: $(this).attr('data-id'),
        prospect_id: $(this).attr('data-prospect'),
        mandat_id: $(this).attr('data-mandat'),
    };
    var url_request = data_url.FormMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#AddMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddMandat", loading);
});

function retourAddMandat(response) {
    if (response.status == true) {
        myModal.toggle();
        getLotMandat(response.data_retour.lot_id, response.data_retour.prospect_id);
    } else {
        fn.response_control_champ(response);
    }
}

/****envoyer mandat******/

$(document).on('click', '.sendMandat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        mandat_id: $(this).attr('data-id'),
    };

    sendMandat(data_request);
});

function sendMandat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.sendMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMandatCallBack', loading);
}
function sendMandatCallBack(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        mandat_id: response.data.id
                    }
                    sendMandat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var lot_id = $("#lot_id_mandat").val();
            var prospect_id = $("#prospect_id").val();

            getLotMandat(lot_id, prospect_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/***Signer mandat*****/

$(document).on('click', '.signerMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        lot_id: $("#lot_id_mandat").val(),
        prospect_id: $("#prospect_id").val()
    };
    var url_request = data_url.SignerMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#AddSignMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#mandat_path').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var lot_id = $("#lot_id_mandat").val();
                    var prospect_id = $("#prospect_id").val();

                    getLotMandat(lot_id, prospect_id);
                    Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

$(document).on('click', '.cloturerMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        lot_id: $("#lot_id_mandat").val(),
        prospect_id: $("#prospect_id").val()
    };
    var url_request = data_url.cloturerMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

/****Clos sans suite mandat******/

$(document).on('click', '.clos_sans_suiteMandat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        mandat_id: $(this).attr('data-id'),
    };

    closMandat(data_request);
});

function closMandat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.closMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'closMandatCallback', loading);
}
function closMandatCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        mandat_id: response.data.id
                    }
                    closMandat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var lot_id = $("#lot_id_mandat").val();
            var prospect_id = $("#prospect_id").val();

            getLotMandat(lot_id, prospect_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


$(document).on("click", ".pdf_pack", function (e) {
    e.preventDefault();
    fn.load_open(const_app.base_url + '/Prospections/pdf_modelMandat?lot_id=' + $(this).attr("data-id") + '&& type_mandat_id=' + $(this).attr("data-pack"));
});

$(document).on('click', '#recupMandat', function (e) {
    e.preventDefault();
    ajoutDocMandat($(this).attr('data-id'), $(this).attr('data-prospect'), 'add', null);
});

function ajoutDocMandat(lot_id, prospect_id, action, docmandat_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'lot_id': lot_id,
        'prospect_id': prospect_id,
        'docmandat_id': docmandat_id
    };
    var url_request = data_url.UploadMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModalMandat');

    $("#modalAjoutDocMandat").modal('show');
}

function chargercontentModalMandat(response) {
    $('#contenaireUploadMandat').html(response);

}

function apercuDocumentMandat(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getPathMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepath');
}

function responsepath(response) {
    var el_apercu = document.getElementById('apercu_list_doc')
    var dataPath = response[0].docmandat_path
    var id_document = response[0].docmandat_id
    apercu(dataPath, el_apercu, id_document);
}

function apercuMandat(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getPath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandat');
}

function responsepathMandat(response) {
    var el_apercu = document.getElementById('apercudocMandat')
    var dataPath = response[0].mandat_path
    var id_document = response[0].mandat_id
    apercuDocMandat(dataPath, el_apercu, id_document);
}


function apercuDocMandat(data, el, id_doc) {
    viderApercuMandat();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu').appendChild(element);
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu').appendChild(element);
    }
}

function viderApercuMandat() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })

    if (document.getElementById("apercudocMandat") != undefined) {
        if (document.getElementById("apercudocMandat").querySelector("#view_apercu") != null) {
            document.getElementById("apercudocMandat").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudocMandat").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '.btnsuppMandat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        docmandat_id: $(this).attr('data-id')
    };
    delete_docMandat(data_request);
});

function delete_docMandat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteDocMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDocMandat', loading);
}
function returnDocMandat(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        docmandat_id: response.data.id
                    }
                    delete_docMandat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/**************Envoi mandat ****************/
$(document).on('click', '.envoiMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id: $('#lot_id_mandat').val(),
        prospect_id: $('#prospect_id').val(),
        mandat_id: $(this).attr('data-id')
    };
    var url_request = data_url.EnvoiMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModal(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}

/***********modal gestionnaire**********/

$(document).on('click', '#addGestionnaire', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormGestionnaire;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);

    myModal.toggle();
}

function charger_contentMandat(response) {
    $("#modal-main-content").html(response);
    $('#type_mandat_id').trigger('change');
    myModal.toggle();
}

$(document).on('submit', '#AddGestion', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddGestion", "null_fn", param);
});

function retour_AddGestion(response) {

    if (response.status == true) {
        myModal.toggle();
        var gestionnaire_id = $("#gestionnaire_id");
        gestionnaire_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
    } else {
        fn.response_control_champ(response);
    }
}

/***********modal programme**********/

$(document).on('click', '#addProgramme', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormProgramme;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#AddProgram', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddProgram", "null_fn", param);
});

function retour_AddProgram(response) {
    if (response.status == true) {
        myModal.toggle();
        var progm_id = $("#progm_id");
        progm_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
    } else {
        fn.response_control_champ(response);
    }
}

/*********communication*********/

function getCommunication(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'prospect_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    var url_request = data_url.listeCom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeCom', loading);
}

function loadListeCom(response) {
    $('#contenair-com').html(response);
}

function getFormCom(prospect_id, action, comprospect_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'prospect_id': prospect_id,
        'comprospect_id': comprospect_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    var url_request = data_url.getFormCom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormCom', loading);
}
function retourFormCom(response) {
    $('#contenair-com').html(response);
}

$(document).on('click', '#btnFormCom', function (e) {
    e.preventDefault();
    getFormCom($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerAddCom', function (e) {
    e.preventDefault();
    getCommunication($(this).attr('data-id'));
});

$(document).on('click', '#updateCom', function (e) {
    e.preventDefault();
    getFormCom($(this).attr('data-prospect'), 'edit', $(this).attr('data-id'));
});

$(document).on('submit', '#cruCom', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddCom", loading);
});

function retourAddCom(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getCommunication(response.data_retour.prospect_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.deleteCom', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        comprospect_id: $(this).attr('data-id')
    };
    delete_com(data_request);
});

function delete_com(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteCom;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteCom', loading);
}
function retourDeleteCom(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        comprospect_id: response.data.id
                    }
                    delete_com(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowCom-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


/******Tâche*******/

function getFormTache(prospect_id, action, tache_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'prospect_id': prospect_id,
        'tache_id': tache_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }

    var url_request = data_url.getFormTache;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormCom', loading);
}

$(document).on('click', '#btnFormTache', function (e) {
    e.preventDefault();
    getFormTache($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#updateTache', function (e) {
    e.preventDefault();
    getFormTache($(this).attr('data-prospect'), 'edit', $(this).attr('data-id'));
});


$(document).on('submit', '#cruTache', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddTache", loading);
});

function retourAddTache(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getCommunication(response.data_retour.prospect_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSuppTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    delete_tache(data_request);
});

function delete_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteTache;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteTache', loading);
}
function retourDeleteTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    delete_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnValideTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    valide_tache(data_request);
});

function valide_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.valideTache;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValideTache', loading);
}
function retourValideTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    valide_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}



/*********Prospect supprimé*********/

$(document).ready(function () {
    pgProspectSupp();
});

function pgProspectSupp() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.listeProspectSupp;

    var param = {
        type: "content",
        id_content: "contenair-supp"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pagelisteprospectsupp', param);
}

function charger_pagelisteprospectsupp(response) {
    $('#contenair-supp').html(response)
}

/****document***/

function getDocProspect(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'prospect_id': id
    };
    // var loading = {
    //     type: "content",
    //     id_content: "contenair-document"
    // }
    var url_request = data_url.listeDocument;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDoc');
}

function loadListeDoc(response) {
    $('#contenair-document').html(response);
}

$(document).on('click', '#ajoutdoc', function (e) {
    e.preventDefault();
    ajoutDoc($(this).attr('data-id'), 'add', null);
});

function ajoutDoc(prospect_id, action, docp_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'prospect_id': prospect_id,
        'docp_id': docp_id
    };
    var url_request = data_url.formUpload;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal_Doc');

    $("#modalAjoutDoc").modal('show');
}

function chargercontentModal_Doc(response) {
    $('#contenaireUpload').html(response);
}

function apercuDocument(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getDocpath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseDOcpath');
}

function responseDOcpath(response) {
    var el_apercu = document.getElementById('apercu_list_doc')
    var dataPath = response[0].docp_path
    var id_document = response[0].docp_id
    apercu(dataPath, el_apercu, id_document);


}

function apercu(data, el, id_doc) {
    viderApercu();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu').appendChild(element);
    }
}

function viderApercu() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })

    if (document.getElementById("apercu_list_doc") != undefined) {
        if (document.getElementById("apercu_list_doc").querySelector("#view_apercu") != null) {
            document.getElementById("apercu_list_doc").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_list_doc").querySelector("#view_apercu").remove();
        }

    }
}

/*****supprimer document****/

$(document).on('click', '.btnSuppDoc', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        docp_id: $(this).attr('data-id')
    };
    delete_doc(data_request);
});
function delete_doc(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteDoc;
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDoc', loading);
}
function retourDeleteDoc(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        docp_id: response.data.id
                    }
                    delete_doc(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDoc', function (e) {
    e.preventDefault();
    var docp_id = $(this).attr("data-id");
    var prospect_id = $(this).attr("data-prospect");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", docp_id: docp_id, prospect_id: prospect_id };
    var url_request = data_url.modalupdateDoc;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});


$(document).on('submit', '#UpdateDocument', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedoc", "null_fn", param);
});

function retourupdatedoc(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocProspect(response.data_retour.prospect_id);
    } else {
        fn.response_control_champ(response);
    }
}

function forceDownload_mandat_new(href) {
    var link = document.createElement('a');
    link.href = href;
    link.download = href;
    var str = link.download.split("/");
    link.download = str[10]
    document.body.appendChild(link);
    link.click();
}

$(document).on('click', '.zommer-apercu-list', function () {

    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() + 50)

})

$(document).on('click', '.dezommer-apercu-list', function () {
    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() - 50)

})

$(document).on('click', '.orginal-apercu-list', function () {

    $('#apercu_list_doc').find('object').css({ height: 'auto', width: 'auto' })

})


/*******Transformer client*******/

$(document).on('click', '#transformclient', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        prospect_id: $(this).attr('data-id')
    };
    TransformClient(data_request);
});

function TransformClient(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.transformerClient;
    var loading = {
        type: "content",
        id_content: "contenair-prospect"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourTransformClient', loading);
}

function retourTransformClient(response) {
    if (response.status == 200) {
        if (response.action == "error") {
            $("#modalErrorTransformClient").modal('toggle');
            const parent = document.querySelector('#list_error_transform');
            parent.innerHTML = '';
            for (let i = 0; i < response.data.text.length; i++) {
                const child = document.createElement('li');
                child.textContent = response.data.text[i];
                parent.appendChild(child);
            }
        } else if (response.action == "demande") {

            $("#modalTransformClient").modal('toggle');
            var data = {
                action: "confirm",
                prospect_id: response.data.id
            }

            $("#confirmTransform").click(function () {
                var util_id = $('#util_id').val();
                var data = {
                    action: "confirm",
                    prospect_id: response.data.id,
                    util_id: util_id
                }

                if ($('#checkbox_bienvenu').is(':checked')) {
                    verifier_compte(util_id, data);
                } else {
                    TransformClient(data);
                }
            });

        } else {
            var data = {
                client_id: response.data.id
            }

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });

            setTimeout(() => {

                if (allowedAccessRight([1, 3])) {
                    window.location.href = response.data.url;
                } else {
                    window.location.href = `${const_app.base_url}Prospections/Prospections`;
                }
            }, 500);

        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function verifier_compte(util_id, donnee) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        util_id: util_id,
        donnee: donnee
    };
    var url_request = data_url.verifier_compte;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'verifier_compte_CallBack');
}

function verifier_compte_CallBack(response) {
    if (response.status == 200) {
        modalformEnvoiWelcome(response);

    } else {
        alert_compte_parametre(response.message);
    }
}

function alert_compte_parametre(message) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        message: message
    };
    var url_request = data_url.alert_compte_parametre;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_compte_parametre_CallBack');
    $('#modalFormAlert').modal('show');

}
function alert_compte_parametre_CallBack(response) {
    $('#message_alert_compte').text(response);
}

function modalformEnvoiWelcome(data_response) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        util_id: data_response.data.util_id,
        prospect_id: data_response.data_mail.prospect_id
    };
    var url_request = data_url.modalformEnvoiWelcome;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_Modal_welcome');

}

function charger_Modal_welcome(response) {
    var ModalBienvenu = new bootstrap.Modal(document.getElementById("FormModalBienvenu"), {
        keyboard: false
    });

    ModalBienvenu.toggle();
    $('#util_id').val(response.user_resposable.util_id);
    $('#prospect_id').val(response.prospect.prospect_id);
    $('#emetteur').val(response.user_resposable.util_nom + ' ' + response.user_resposable.util_prenom + ' (' + response.user_resposable.mess_email + ')');
    $('#destinataire').val(response.prospect.prospect_email);
    $('#object_mail').val(response.modele.pmail_objet);
    $('#message_text').val(response.message_text);

    if (typeof response.image !== 'undefined') {
        $('#image_signature').removeClass('d-none');
        var myImage = document.getElementById("image_signature");
        myImage.src = response.image;
    }
    else {
        $('#signature_aucun').removeClass('d-none');
    }
}

function envoiMailBienvenu(util_id, prospect_id, destinataire, object, message_text) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        util_id: util_id,
        prospect_id: prospect_id,
        destinataire: destinataire,
        object: object,
        message_text: message_text
    };
    var url_request = data_url.envoiMail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'envoiMailBienvenu_CallBack');
}

function envoiMailBienvenu_CallBack(response) {
    if (response.status == 200) {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Email non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#voirficheclient', function (e) {
    e.preventDefault();
    var client_id = $(this).attr('data-after_id');
    if (client_id && client_id !== null && client_id !== "") window.location.href = `${const_app.base_url}/Clients/ficheClient?client=${client_id}`;
});

var Modal_CNI = new bootstrap.Modal(document.getElementById("FormModalCNI"), {
    keyboard: false
});

$(document).on('click', '#historique_mail_prospect', function (e) {
    $("#modalContentCNI").html("");
    Modal_CNI.toggle();
    viewHistorySendMail_Cni_prospect("modalContentCNI", $('#prospect_id').val())
});


/*********Prospect converti*********/

$(document).ready(function () {
    pgProspectConverti();
});

function pgProspectConverti() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.listeProspectConverti;

    var param = {
        type: "content",
        id_content: "contenair-converti"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pageprospectConverti', param);
}

function charger_pageprospectConverti(response) {
    $('#contenair-converti').html(response)
}

/*********Prospect perdu*********/

$(document).ready(function () {
    pgProspectPerdu();
});

function pgProspectPerdu() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.listeProspectPerdu;

    var param = {
        type: "content",
        id_content: "contenair-perdu"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pageprospectPerdu', param);
}

function charger_pageprospectPerdu(response) {
    $('#contenair-perdu').html(response)
}