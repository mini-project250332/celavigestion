var historique_url = {
    addHistorique: `${const_app.base_url}Historique/AddHistorique`,
    getHistorique: `${const_app.base_url}Historique/GetHistorique`
}

function addHistorique(type_action_id, page_id, action, histo_reference_id = 0, container_id = "", surplus = null) {
    var utilId = getUtilId();
    var type_request = 'POST';
    var url_request = historique_url.addHistorique;
    var type_output = 'json';
    var data_request = {
        'util_id': utilId,
        'tpact_id': type_action_id,
        'page_id': page_id,
        'action': action,
        'histo_reference_id': histo_reference_id,
        'container_id': container_id,
        'surplus': surplus
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'addHistoriqueCallback');
}

function addHistoriqueCallback(response) {
    var type_action_id = response?.data_retour?.request.type_action_id;
    var page_id = response?.data_retour?.request.page_id;
    var histo_reference_id = response?.data_retour?.request.histo_reference_id;
    var container_id = response?.data_retour?.request.container_id;
    getHistorique(type_action_id, page_id, histo_reference_id, container_id);
}

// Historique tab mail
function getHistoriqueMailLoyer(type_action_id, page_id, histo_reference_id = 0, container_id = "") {
    var utilId = getUtilId();
    var type_request = 'POST';
    var url_request = historique_url.getHistorique;
    var type_output = 'html';
    var data_request = {
        'util_id': utilId,
        'container_id': container_id,
        'tpact_id': type_action_id,
        'page_id': page_id,
        'histo_reference_id': histo_reference_id,
        'mail_loyer': 1
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'historiqueMailLoyerCallback');
}

function historiqueMailLoyerCallback(response) {
    var ModalHistory = new bootstrap.Modal(document.getElementById("modalMailHistoryContent"), {
        keyboard: false
    });
    ModalHistory.toggle();
    $(`#contain_tab_mail_historique_loyer`).html(response);
}

function getHistorique(type_action_id, page_id, histo_reference_id = 0, container_id = "", first = "true", historiqueCallback = 'historiqueCallback') {
    var utilId = getUtilId();
    var type_request = 'POST';
    var url_request = historique_url.getHistorique;
    var type_output = 'json';
    var data_request = {
        'util_id': utilId,
        'container_id': container_id,
        'tpact_id': type_action_id,
        'page_id': page_id,
        'histo_reference_id': histo_reference_id,
        'first': first
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, historiqueCallback);
    $('.pace-progress').css('display', 'none');
    $('.pace-progress-inner').css('display', 'none');
}

function historiqueCallback(response) {

    if (response?.data_retour?.containerId) {
        var containerId = response?.data_retour?.containerId;
        var histories = response?.data_retour?.historiques;
        var isFirst = response?.data_retour?.request.first === "true" ? true : false;
        var liste = $(`#${containerId}`);
        liste.empty();

        var lastHistory = isFirst ? histories.slice(0, 5) : histories;

        if (lastHistory.length == 0) {
            liste.append(`
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLabel">Historique de l'envoi de mail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row m-0">
                    <div class="col py-1">
                        <div class="item-history text-center w-100">
                            Aucun historique
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                </div>
            </div> 
            `);
            return false;
        }

        liste.append(`
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Historique d'envoi d'email</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div> 
        `);

        $.each(lastHistory, function (index, item) {
            const date = new Date(item.histo_date);
            const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            const month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
            const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
            const min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            const sec = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            liste.append(`
                <div class="row m-0">
                    <div class="col">
                        <div class="item-history">
                            <span class="item-history-date">${day}/${month}/${date.getFullYear()}  ${hour}:${min}:${sec}</span>  -  ${item.histo_action} 
                        </div>
                    </div>
                </div>
            `);
        });

        var type_action_id = response?.data_retour?.request.type_action_id;
        var page_id = response?.data_retour?.request.page_id;
        var histo_reference_id = response?.data_retour?.request.histo_reference_id;
        var container_id = response?.data_retour?.request.container_id;

        if (isFirst && histories.length > 5) {
            liste.append(`<button class="read-more-history" 
            onClick="readMore('${type_action_id}', '${page_id}','${histo_reference_id}','${container_id}', 'false')">
                Voir plus
            </button>`)
        }

        if (!isFirst) {
            liste.append(`<button class="read-more-history" 
            onClick="readMore('${type_action_id}', '${page_id}','${histo_reference_id}','${container_id}', 'true')">Voir moins</button>`)
        }

        liste.append(`
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
        `);

    }
}

function readMore(type_action_id, page_id, histo_reference_id, container_id, first) {
    getHistorique(type_action_id, page_id, histo_reference_id, container_id, first);
}

function getUtilId() {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    return user_decrypted.util_id;
}

// ************ Historique envoi mail ****************
function historySendMail(mail_detail, facture_id, history_detail) {
    // 6 : envoi mail (action) // 6 : facture_loyer_lot (page)
    addHistorique(6, 6, mail_detail, facture_id, "", history_detail);
}

function historySendMailCNIClient(mail_detail, client_id, history_detail) {
    // 6 : envoi mail (action) // 7 : demande_cni_client (page)
    addHistorique(6, 7, mail_detail, client_id, "", history_detail);
}

function historySendMailCNIProspect(mail_detail, prospect_id, history_detail) {
    // 6 : envoi mail (action) // 8 : demande_cni_prospect (page)
    addHistorique(6, 8, mail_detail, prospect_id, "", history_detail);
}

function historySendMailTaxe(mail_detail, taxe_id, history_detail) {
    // 6 : envoi mail (action) // 9 : envoi_taxe_lot (page)
    addHistorique(6, 9, mail_detail, taxe_id, "", history_detail);
}

function viewHistorySendMail(container_id, facture_id = 0) {
    // 6 : envoi mail (action) // 6 : facture_loyer_lot (page)
    getHistorique(6, 6, facture_id, container_id);
}

function viewHistorySendMail_Cni_cli(container_id, client_id = 0) {
    // 6 : envoi mail (action) // 7 : demande_cni_client (page)
    getHistorique(6, 7, client_id, container_id);
}

function viewHistorySendMail_Cni_prospect(container_id, prospect_id = 0) {
    // 6 : envoi mail (action) // 7 : demande_cni_client (page)
    getHistorique(6, 8, prospect_id, container_id);
}

function viewHistorySendMail_taxe(container_id, taxe_id = 0) {
    // 6 : envoi mail (action) // 9 : envoi_taxe_lot (page)
    getHistorique(6, 9, taxe_id, container_id);
}

// ********** Action bouton - Historique envoi mail *************
$(document).on('click', '#modalHistoryDocumentFacture', function (e) {
    e.preventDefault();
    $("#modalContent").html("");
    Modal.toggle();
    viewHistorySendMail("modalContent", $(this).attr('data-fact_id'))
});

$(document).on('click', '#viewHistoriqueMail', function (e) {
    e.preventDefault();
    getHistoriqueMailLoyer(6, 6, 0, "modalContent");
});

$(document).on('click', '#btn_loyer_historique', function (e) {
    e.preventDefault();
    $("#modalContent").html("");
    Modal.toggle();
    getHistorique(0, 3, 0, "modalContent");
});


// ******** Historique Loyer *****************
function historyAjoutDansLoyer(detail, loyer_id) {
    // 1 : ajout (action) // 3 : loyer_lot (page)
    addHistorique(1, 3, detail, loyer_id);
}

function historySupprDansLoyer(detail, loyer_id) {
    // 2 : suppression (action) // 3 : loyer_lot (page)
    addHistorique(2, 3, detail, loyer_id);
}

// ******** Historique bail *****************
function historyModifDansBail(detail, bail_id) {
    // 3 : modification (action) // 1 : bail_lot (page)
    addHistorique(3, 1, detail, bail_id);
}

function historyAddDansBail(detail, bail_id = 0) {
    // 3 : modification (action) // 1 : bail_lot (page)
    addHistorique(1, 1, detail, bail_id);
}
