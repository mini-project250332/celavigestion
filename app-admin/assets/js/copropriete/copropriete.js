var doc_copropriete = {
    getDocCopropriete: `${const_app.base_url}Clients/getDocCopropriete`,
    formUpload: `${const_app.base_url}Clients/formUploadCopropriete`,
    deleteDocCopropriete: `${const_app.base_url}Clients/deleteDocCopropriete`,
    modalupdateDocCopropriete: `${const_app.base_url}Clients/modalupdateDocCopropriete`,
    addModifcommentaireDocCopropriete: `${const_app.base_url}Clients/addModifcommentaireDocCopropriete`,
    getpathDocCopropriete: `${const_app.base_url}Clients/getpathDocCopropriete`,
}

var timer;

function getDocCopropriete(cliLot_id) {
    var data_request = {
        'cliLot_id': cliLot_id,
    }
    var type_request = 'POST';
    var url_request = doc_copropriete.getDocCopropriete;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-lot-copropriete"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDocCoproprieteCallback', loading);
}
function getDocCoproprieteCallback(response) {
    $('#contenair-lot-copropriete').html(response);
}

$(document).on('click', '#ajoutdocCopropriete', function (e) {
    e.preventDefault();
    ajoutDocCopropriete($(this).attr('data-id'), 'add', null);
});

function ajoutDocCopropriete(cliLot_id, action, doc_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_id': doc_id
    };
    var url_request = doc_copropriete.formUpload;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModalCopropriete');

    $("#modalAjoutCopropriete").modal('show');
}

function chargercontentModalCopropriete(response) {
    $('#UploadCopropriete').html(response);
}

$(document).on('click', '.btnSuppDocCopropriete', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_id: $(this).attr('data-id')
    };

    deleteDocCopropriete(data_request);
});

function deleteDocCopropriete(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = doc_copropriete.deleteDocCopropriete;
    var loading = {
        type: "content",
        id_content: "contenair-fiscal-doc_Copropriete"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocCopropriete', loading);
}

function retourDeleteDocCopropriete(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_id: response.data.id
                    }
                    deleteDocCopropriete(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowcopropriete-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocCopropriete', function (e) {
    e.preventDefault();
    var doc_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-cliLot_id");

    var type_request = 'POST';
    var type_output = 'html';

    var data_request = {
        action: "edit",
        doc_id: doc_id,
        cliLot_id: cliLot_id,
    };
    var url_request = doc_copropriete.modalupdateDocCopropriete;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});
function charger_contentModal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentCopropriete', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocCopropriete", "null_fn", param);
});
function retourupdatedocCopropriete(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocCopropriete(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('keyup', '#commentaire_doc_Copropriete', function () {
    clearTimeout(timer);

    timer = setTimeout(function () {
        var data = {
            commentaire: $('#commentaire_doc_Copropriete').val(),
            cliLot_id: $('#cliLot_id_copropriete').val()
        };

        addModifcommentaireDocCopropriete(data);
    }, 800);
});
function addModifcommentaireDocCopropriete(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data

    var url_request = doc_copropriete.addModifcommentaireDocCopropriete;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'addModifcommentaireDocCoproprieteRetour');
}
function addModifcommentaireDocCoproprieteRetour(response) {
    var jsonObject = JSON.parse(response);
    if (jsonObject.status == true) {
        Notiflix.Notify.success(jsonObject.form_validation.message, { position: 'left-bottom', timeout: 2000 });
    } else {
        Notiflix.Notify.failure(jsonObject.form_validation.message, { position: 'left-bottom', timeout: 2000 });
    }
}

function apercuDocumentCopropriete(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = doc_copropriete.getpathDocCopropriete;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathCopropriete');
}

function responsepathCopropriete(response) {
    var el_apercu = document.getElementById('apercuFiscaldocCopropriete')
    var dataPath = response[0].doc_path
    var id_document = response[0].doc_id
    apercuFiscalConditionel(dataPath, el_apercu, id_document);
}

function apercuFiscalConditionel(data, el, id_doc) {
    viderApercuFiscalConditionel();
    var doc_selectionner = document.getElementById("docrowcopropriete-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercudoc_Copropriete').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercudoc_Copropriete').appendChild(element);
    }
}

function viderApercuFiscalConditionel() {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercudoc_Copropriete") != undefined) {
        if (document.getElementById("apercudoc_Copropriete").querySelector("#view_apercu") != null) {
            document.getElementById("apercudoc_Copropriete").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudoc_Copropriete").querySelector("#view_apercu").remove();
        }
    }
}