var timer = null;

$(document).on('submit', '#authApp', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "div-signin-box"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_authentification", loading);
});

function retour_authentification(response) {
    var text = response.form_validation.message, icon = "", type_alert = "";
    if (response.status == true) {

        icon = (response.data_retour == 1) ? `<i class="bi bi-check-circle"></i>` : `<i class="bi bi-exclamation-circle"></i>`;
        type_alert = (response.data_retour == 1) ? "alert-success" : "alert-warning";

        var message = `
            <div class="alert ${type_alert} border-2 d-flex align-items-center p-1 w-100" role="alert">
                ${icon}
                <p class="mb-0 flex-1 fs--1 text-center">${text}</p>
                <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`;
    } else {
        if (response.data_retour == null) {
            control_data_form_complexe_return(response);
            message = "";
        } else {
            message = `
                <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                    <i class="bi bi-x-circle me-1"></i>
                    <p class="mb-0 flex-1 fs--1 text-center">${text}</p>
                    <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
        }
    }
    $('#div-message').html(message);
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    if (response.data_retour == 1) {
        var user_crypted = CryptoJSAesJson.encrypt(response.form_validation.userdata, const_app.code_crypt);
        localStorage.setItem("userinfo", user_crypted);

        timer = setTimeout(() => {
            if (response.form_validation.userdata.rol_util_libelle == "Expert Comptable") {
                window.location.href = `${const_app.base_url}ExpertComptable`;
            } else {
                window.location.href = `${const_app.base_url}Auth/bienvenu`;
            }
        }, 500);
    } else {
        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 1000);
    }
}

$(document).on('click', '#btn_bienvenu', function () {
    if ($('#rol_util_libelle').val() == 'Gestionnaire de compte') {
        window.location.href = `${const_app.base_url}Taches`;
    } else {
        window.location.href = `${const_app.base_url}Clients`;
    }
});

$(document).on('click', '#btn_tache', function () {
    window.location.href = `${const_app.base_url}Taches`;
});

function nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function getMessageContenu() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = `${const_app.base_url}Auth/getMessageContenu`;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getMessageContenu_Callback');
}
function getMessageContenu_Callback(response) {
    const obj = JSON.parse(response);
    $('.message_contenu').html(nl2br(obj.message))
}

function getTacheContenu() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = `${const_app.base_url}Auth/getTacheContenu`;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getTacheContenu_Callback');
}
function getTacheContenu_Callback(response) {
    $('.tache_contenu').html(response);
}