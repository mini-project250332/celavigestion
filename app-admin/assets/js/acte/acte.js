var acte_url = {
    getActeImmo: `${const_app.base_url}Acte/getActeImmo`,
    listeDocumentActe: `${const_app.base_url}Acte/listActe`,
    formUploadDocActe: `${const_app.base_url}Acte/formUploadDocActe`,
    deleteDocActe: `${const_app.base_url}Acte/removeDocActe`,
    modalupdateDocActe: `${const_app.base_url}Acte/FormUpdateActe`,
    immoGeneral: `${const_app.base_url}Acte/getImmoGeneral`,
    ImmoAgencement: `${const_app.base_url}Acte/getImmoAgencement`,
    UpdateImmo: `${const_app.base_url}Acte/formAlertImmo`,
    InfoImmo: `${const_app.base_url}Acte/formInfoImmo`,
    ImmoGrosOeuvre: `${const_app.base_url}Acte/getImmoGrosOeuvre`,
    ImmoFacade: `${const_app.base_url}Acte/getImmoFacade`,
    ImmoInstallation: `${const_app.base_url}Acte/getImmoInstallation`,
    ImmoMobilier: `${const_app.base_url}Acte/getImmoMobilier`,
    formUploadDocGrosOeuvre: `${const_app.base_url}Acte/formUploadDocGrosOeuvre`,
    formUploadDocFacade: `${const_app.base_url}Acte/formUploadDocFacade`,
    formUploadDocInstallation: `${const_app.base_url}Acte/formUploadDocInstallation`,
    formUploadDocAgencement: `${const_app.base_url}Acte/formUploadDocAgencement`,
    formUploadDocMobilier: `${const_app.base_url}Acte/formUploadDocMobilier`,
    delete_docOeuvre: `${const_app.base_url}Acte/removeDocOeuvre`,
    modalupdateDocOeuvre: `${const_app.base_url}Acte/FormUpdateOeuvre`,
    updateDocOeuvreTraitement: `${const_app.base_url}Acte/updateDocOeuvreTraitement`,
    getPathOeuvre: `${const_app.base_url}Acte/getPathOeuvre`,
    delete_docFacade: `${const_app.base_url}Acte/removeDocFacade`,
    modalupdateDocFacade: `${const_app.base_url}Acte/FormUpdateFacade`,
    updateDocFacadeTraitement: `${const_app.base_url}Acte/updateDocFacadeTraitement`,
    getPathFacade: `${const_app.base_url}Acte/getPathFacade`,
    delete_docInstallation: `${const_app.base_url}Acte/removeDocInstallation`,
    modalupdateDocInstallation: `${const_app.base_url}Acte/FormUpdateInstallation`,
    updateDocInstallationTraitement: `${const_app.base_url}Acte/updateDocInstallationTraitement`,
    getPathInstallation: `${const_app.base_url}Acte/getPathInstallation`,
    delete_docAgencement: `${const_app.base_url}Acte/removeDocAgencement`,
    modalupdateDocAgencement: `${const_app.base_url}Acte/FormUpdateAgencement`,
    updateDocAgencementTraitement: `${const_app.base_url}Acte/updateDocAgencementTraitement`,
    getPathAgencement: `${const_app.base_url}Acte/getPathAgencement`,
    delete_docMobilier: `${const_app.base_url}Acte/removeDocMobilier`,
    modalupdateDocMobilier: `${const_app.base_url}Acte/FormUpdateMobilier`,
    updateDocMobilierTraitement: `${const_app.base_url}Acte/updateDocMobilierTraitement`,
    getPathMobilier: `${const_app.base_url}Acte/getPathMobilier`,
    getPathActe: `${const_app.base_url}Acte/getPathActe`,
}


/************Acte*************/

function getDocActe(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
    };
    var url_request = acte_url.listeDocumentActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocActe');
}

function loadListeDocActe(response) {
    $('#contenair-acte').html(response);
}

function submitActe() {
    var form = $("#cruActe");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output, 'returnActe');
}

function returnActe(response) {
    $('input[name=action_acte]').val(response.data_retour.action_acte);
    $('#acte_id').val(response.data_retour.acte_id);
    $('input[name=immo_id]').val(response.data_retour.acte_id);
}


$(document).on('keyup', '#cruActe input, #cruActe textarea', function (e) {
    e.preventDefault();
    var cliLot_id = $('input[name=cliLot_id]').val();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitActe();
        getActeImmo(cliLot_id);
        getImmoGeneral(cliLot_id);
        getImmoGrosOeuvre(cliLot_id);
        getImmofacade(cliLot_id);
        getImmoInstallation(cliLot_id);
        getImmoAgencement(cliLot_id);
        getImmoMobilier(cliLot_id);

    }, 800);
});
$(document).on('change', '#cruActe select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitActe();
    }, 800);
});


function getLastDayOfYear(year) {
    return new Date(year, 11, 31);
}

$(document).on('keyup', '#acte_valeur_acquisition_ht', function (e) {
    e.preventDefault();
    calculImmotableau();
});


$(document).on('keyup', '#acte_valeur_mobilier_ht', function (e) {
    e.preventDefault();
    var input2 = parseFloat($('#acte_valeur_mobilier_ht').val());
    $('#mobilier').html(input2.toFixed(2));
    calculImmotableau();
});

$(document).on('keyup', '#acte_date_debut_amortissement', function (e) {
    e.preventDefault();

    calculImmotableau();

    var cliLot_id = $('input[name=cliLot_id]').val();
    $('.message_verification').removeClass('d-none');

    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $('.message_verification').addClass('d-none');
        submitActe();
        getActeImmo(cliLot_id);
        getImmoGeneral(cliLot_id);
        getImmoGrosOeuvre(cliLot_id);
        getImmofacade(cliLot_id);
        getImmoInstallation(cliLot_id);
        getImmoAgencement(cliLot_id);
        getImmoMobilier(cliLot_id);
    }, 3000);
});

$(document).on('change', '#acte_date_debut_amortissement', function (e) {
    e.preventDefault();

    calculImmotableau();

    var cliLot_id = $('input[name=cliLot_id]').val();
    $('.message_verification').removeClass('d-none');

    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $('.message_verification').addClass('d-none');
        submitActe();
        getActeImmo(cliLot_id);
        getImmoGeneral(cliLot_id);
        getImmoGrosOeuvre(cliLot_id);
        getImmofacade(cliLot_id);
        getImmoInstallation(cliLot_id);
        getImmoAgencement(cliLot_id);
        getImmoMobilier(cliLot_id);
    }, 3000);
});

function calculImmotableau() {
    var input_valeur = parseFloat($('#acte_valeur_acquisition_ht').val());
    var input_taux_terrain = parseFloat($('#acte_taux_terrain').val());
    var input_taux_construction = parseFloat($('#acte_taux_construction').val());
    var acte_valeur_terrain_ht = (input_valeur * input_taux_terrain) / 100;
    var acte_valeur_construction_ht = (input_valeur * input_taux_construction) / 100;
    var frais_acquisition = parseFloat($('#acte_frais_acquisition').val());

    if (frais_acquisition != 0) {
        var acte_valeur_terrain_ht = ((input_valeur + frais_acquisition) * input_taux_terrain) / 100;
        var acte_valeur_construction_ht = ((input_valeur + frais_acquisition) * input_taux_construction) / 100;
    }

    $('#acte_valeur_terrain_ht').val(acte_valeur_terrain_ht.toFixed(2));
    $('#acte_valeur_construction_ht').val(acte_valeur_construction_ht.toFixed(2));

    var acte_valeur_construction_ht = parseFloat($('#acte_valeur_construction_ht').val());
    var oeuvre = (acte_valeur_construction_ht * 40) / 100;
    var facade = (acte_valeur_construction_ht * 20) / 100;
    var installation = (acte_valeur_construction_ht * 20) / 100;
    var agencement = (acte_valeur_construction_ht * 20) / 100;
    var mobilier = parseFloat($('#acte_valeur_mobilier_ht').val());

    $('#oeuvre').html(oeuvre.toFixed(2));
    $('#facade').html(facade.toFixed(2));
    $('#installation').html(installation.toFixed(2));
    $('#agencement').html(agencement.toFixed(2));

    var acte_date_debut_amortissement = new Date($('#acte_date_debut_amortissement').val());
    var acte_annee = acte_date_debut_amortissement.getFullYear();
    var date_fin_amortissement = getLastDayOfYear(acte_annee);
    var Difference_In_Time = date_fin_amortissement.getTime() - acte_date_debut_amortissement.getTime();
    var Difference_In_Days = Math.round(Difference_In_Time / (1000 * 3600 * 24));

    /****Valeur annuité****/
    var annuiteOeuvre = oeuvre / 50;
    var annuiteFacade = facade / 20;
    var annuiteInstallation = installation / 15;
    var annuiteAgencement = agencement / 10;
    var annuiteMobilier = mobilier / 7;

    /**Valeur première année***/
    var premiereAnneeOeuvre = Math.round(annuiteOeuvre / 365 * Difference_In_Days);
    var premiereAnneeFacade = Math.round(annuiteFacade / 365 * Difference_In_Days);
    var premiereAnneeInstallation = Math.round(annuiteInstallation / 365 * Difference_In_Days);
    var premiereAnneeAgencement = Math.round(annuiteAgencement / 365 * Difference_In_Days);
    var premiereAnneeMobilier = Math.round(annuiteMobilier / 365 * Difference_In_Days);

    var Annee_1 = new Date().getFullYear() - 1;
    var diffAnnee = Annee_1 - acte_annee;
    var amortOeuvre = 0;
    var amortFacade = 0;
    var amortInstallation = 0;
    var amortAgencement = 0;
    var amortMobilier = 0;

    var lastamortOeuvre = 0;
    var lastamortFacade = 0;
    var lastamortInstallation = 0;
    var lastamortAgencement = 0;
    var lastamortMobilier = 0;
    var element = 0;

    /**Oeuvre**/
    elementOeuvre = parseFloat(premiereAnneeOeuvre);
    if (acte_annee >= Annee_1) {
        lastamortOeuvre = annuiteOeuvre * 0;
        amortOeuvre = 0;
    } else {
        lastamortOeuvre = annuiteOeuvre * (diffAnnee - 1);
        amortOeuvre = Math.round(lastamortOeuvre + elementOeuvre);
    }

    //lastamortOeuvre = annuiteOeuvre * (diffAnnee-1);
    //amortOeuvre = Math.round(lastamortOeuvre + elementOeuvre);    

    /**Façade**/
    elementFacade = parseFloat(premiereAnneeFacade);
    if (acte_annee >= Annee_1) {
        lastamortFacade = 0;
        amortFacade = 0;
    } else {
        lastamortFacade = annuiteFacade * (diffAnnee - 1);
        amortFacade = Math.round(lastamortFacade + elementFacade);
    }

    /**Installation**/
    elementInstallation = parseFloat(premiereAnneeInstallation);
    if (acte_annee >= Annee_1) {
        lastamortInstallation = 0;
        amortInstallation = 0;
    } else if (diffAnnee > 15) {
        lastamortInstallation = annuiteInstallation * 14;
        amortInstallation = Math.round(lastamortInstallation + elementInstallation);
    } else {
        lastamortInstallation = annuiteInstallation * (diffAnnee - 1);
        amortInstallation = Math.round(lastamortInstallation + elementInstallation);
    }

    /**Agencement**/
    elementAgencement = parseFloat(premiereAnneeAgencement);
    if (acte_annee >= Annee_1) {
        lastamortAgencement = 0;
        amortAgencement = 0;
    } else if (diffAnnee > 10) {
        lastamortAgencement = annuiteAgencement * 9;
        amortAgencement = Math.round(lastamortAgencement + elementAgencement);
    } else {
        lastamortAgencement = annuiteAgencement * (diffAnnee - 1);
        amortAgencement = Math.round(lastamortAgencement + elementAgencement);
    }

    /**Mobilier**/
    elementMobilier = parseFloat(premiereAnneeMobilier);
    if (acte_annee >= Annee_1) {
        lastamortMobilier = 0;
        amortMobilier = 0;
    } else if (diffAnnee > 7) {
        lastamortMobilier = annuiteMobilier * 6;
        amortMobilier = Math.round(lastamortMobilier + elementMobilier);
    } else {
        lastamortMobilier = annuiteMobilier * (diffAnnee - 1);
        amortMobilier = Math.round(lastamortMobilier + elementMobilier);
    }

    $('#amortOeuvre').html(amortOeuvre);
    $('#amortFacade').html(amortFacade);
    $('#amortInstallation').html(amortInstallation);
    $('#amortAgencement').html(amortAgencement);
    $('#amortMobilier').html(amortMobilier);

    var resteAmortOeuvre = 0;
    var resteAmortFacade = 0;
    var resteAmortInstallation = 0;
    var resteAmortAgencement = 0;
    var resteAmortMobilier = 0;

    if (acte_annee >= Annee_1) {
        resteAmortOeuvre = 0;
        resteAmortFacade = 0;
        resteAmortInstallation = 0;
        resteAmortAgencement = 0;
        resteAmortMobilier = 0;
    } else {
        resteAmortOeuvre = Math.round(oeuvre - amortOeuvre);
        resteAmortFacade = Math.round(facade - amortFacade);
        resteAmortInstallation = Math.round(installation - amortInstallation);
        resteAmortAgencement = Math.round(agencement - amortAgencement);
        resteAmortMobilier = Math.round(mobilier - amortMobilier);
    }

    var totalValeur = oeuvre + facade + installation + agencement + mobilier;
    var totalAmort = amortOeuvre + amortFacade + amortInstallation + amortAgencement + amortMobilier;
    var totalResteAmort = resteAmortOeuvre + resteAmortFacade + resteAmortInstallation + resteAmortAgencement + resteAmortMobilier;

    $('#totalValeur').text(totalValeur.toFixed(2));
    $('#totalAmort').text(totalAmort.toFixed(2));
    $('#totalResteAmort').text(totalResteAmort.toFixed(2));

    $('#resteAmortOeuvre').text(resteAmortOeuvre.toFixed(2));
    $('#resteAmortfacade').text(resteAmortFacade.toFixed(2));
    $('#resteAmortInstallation').text(resteAmortInstallation.toFixed(2));
    $('#resteAmortAgencement').text(resteAmortAgencement.toFixed(2));
    $('#resteAmortMobilier').text(resteAmortMobilier.toFixed(2));
}

// Ajouter document acte

$(document).on('click', '#ajoutdocActe', function (e) {
    e.preventDefault();
    ajoutdocActe($(this).attr('data-id'), 'add', null);
});

function ajoutdocActe(cliLot_id, action, doc_acte_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_acte_id': doc_acte_id
    };
    var url_request = acte_url.formUploadDocActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentActe');

    $("#modalAjoutActe").modal('show');
}

function chargercontentActe(response) {
    $('#UploadActe').html(response);
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);

    myModal.toggle();
}

var ModalLg = new bootstrap.Modal(document.getElementById("ModalFormLg"), {
    keyboard: false
});

function chargerModalLg(response) {
    $("#modalContentLg").html(response);
    ModalLg.toggle();
}

$(document).on('click', '.btnUpdtadeDocActe', function (e) {
    e.preventDefault();
    var doc_acte_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_acte_id: doc_acte_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentActe', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocActe", "null_fn", param);
});

function retourupdatedocActe(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocActe(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}


$(document).on('click', '#updateActe', function (e) {
    e.preventDefault();
    var type_acquisition = $("#acte_type_acquisition").val();
    if (type_acquisition == "Reprise") {
        $("#acte_valeur_terrain_ht").attr('disabled', false);
    }
    $(".terminerActe").toggleClass('d-none');
    $("#acte_type_acquisition").attr('disabled', false);
    $("#acte_date_acquisition").attr('disabled', false);
    $("#acte_date_notarie").attr('disabled', false);
    $("#acte_valeur_acquisition_ht").attr('disabled', false);
    $("#acte_valeur_mobilier_ht").attr('disabled', false);
    $("#acte_montant_tva_immo").attr('disabled', false);
    $("#acte_montant_tva_frais_acquisition").attr('disabled', false);
    $("#acte_date_remboursement").attr('disabled', false);
    $("#acte_date_reg").attr('disabled', false);
    $("#acte_date_debut_amortissement").attr('disabled', false);
    $("#acte_frais_acquisition").attr('disabled', false);
    $("#commentaire_acte").attr('disabled', false);
    $(this).toggleClass('d-none');
});

$(document).on('click', '.terminerActe', function (e) {
    e.preventDefault();
    $("#updateActe").toggleClass('d-none');
    $("#acte_type_acquisition").attr('disabled', true);
    $("#acte_date_acquisition").attr('disabled', true);
    $("#acte_date_notarie").attr('disabled', true);
    $("#acte_valeur_acquisition_ht").attr('disabled', true);
    $("#acte_montant_tva_immo").attr('disabled', true);
    $("#acte_montant_tva_frais_acquisition").attr('disabled', true);
    $("#acte_valeur_terrain_ht").attr('disabled', true);
    $("#acte_date_remboursement").attr('disabled', true);
    $("#acte_date_reg").attr('disabled', true);
    $("#acte_date_debut_amortissement").attr('disabled', true);
    $("#acte_valeur_mobilier_ht").attr('disabled', true);
    $("#acte_frais_acquisition").attr('disabled', true);
    $("#commentaire_acte").attr('disabled', true);
    $(this).toggleClass('d-none');
});

$(document).on('click', '.btnSuppDocActe', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_acte_id: $(this).attr('data-id')
    };
    deleteDocActe(data_request);
});

function deleteDocActe(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.deleteDocActe;
    var loading = {
        type: "content",
        id_content: "contenair-acte"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteActe', loading);
}

function retourDeleteActe(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_acte_id: response.data.id
                    }
                    deleteDocActe(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowActe-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function getImmoGeneral(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.immoGeneral;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmo', loading);
}

function returnImmo(response) {
    $('#fiche-identifiant-general').html(response);
}

function getActeImmo(cliLot_id) {

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'cliLot_id': cliLot_id
    };

    var url_request = acte_url.getActeImmo;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour');
}

function retour(response) {
    if (response.act_donnee == null) {
        $('#immo').hide();
        $('#message_immo').show();
    }
    else if (response.act_donnee.acte_valeur_acquisition_ht == null || response.act_donnee.acte_date_debut_amortissement == null || response.act_donnee.acte_date_debut_amortissement == '0000-00-00') {
        $('#immo').hide();
        $('#message_immo').show();
    } else {
        $('#immo').show();
        $('#message_immo').hide();
    }
}

$(document).on('click', '#update_immo', function (e) {
    e.preventDefault();
    var cliLot_id = $(this).attr('data-lot');
    var annee = $(this).attr('data-id');
    var oeuvre = $(this).attr('data-oeuvre');
    var facade = $(this).attr('data-facade');
    var installation = $(this).attr('data-installation');
    var agence = $(this).attr('data-agence');
    var mobilier = $(this).attr('data-mobilier');
    var total = $(this).attr('data-total');
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id,
        annee: annee,
        oeuvre: oeuvre,
        facade: facade,
        installation: installation,
        agence: agence,
        mobilier: mobilier,
        total: total,
    };
    var url_request = acte_url.UpdateImmo;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalLg');
});

$(document).on('submit', '#UpdateImmo', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdateimmo", "null_fn", param);
});

function retourupdateimmo(response) {
    if (response.status == true) {
        ModalLg.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmoGeneral(response.data_retour.cliLot_id.value);
        getImmoGrosOeuvre(response.data_retour.cliLot_id.value);
        getImmofacade(response.data_retour.cliLot_id.value);
        getImmoInstallation(response.data_retour.cliLot_id.value);
        getImmoAgencement(response.data_retour.cliLot_id.value);
        getImmoMobilier(response.data_retour.cliLot_id.value);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#info_immo', function (e) {
    e.preventDefault();
    var immo_general_id = $(this).attr('data-immo-id');
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        immo_general_id: immo_general_id,
    };
    var url_request = acte_url.InfoImmo;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModalForm(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}

/*********Immo-GrosOeuvre***********/

function getImmoGrosOeuvre(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.ImmoGrosOeuvre;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmoGrosOeuvre', loading);
}

function returnImmoGrosOeuvre(response) {
    $('#fiche-identifiant-grosOeuvre').html(response);
}

$(document).on('click', '#ajoutDocOeuvre', function (e) {
    e.preventDefault();
    ajoutDocOeuvre($(this).attr('data-id'), 'add', null);
});

function ajoutDocOeuvre(cliLot_id, action, doc_oeuvre_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_oeuvre_id': doc_oeuvre_id
    };
    var url_request = acte_url.formUploadDocGrosOeuvre;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentOeuvre');

    $("#modalAjoutOeuvre").modal('show');
}

function chargercontentOeuvre(response) {
    $('#UploadOeuvre').html(response);
}

$(document).on('click', '.btnSuppDocOeuvre', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_oeuvre_id: $(this).attr('data-id')
    };
    delete_docOeuvre(data_request);
});

function delete_docOeuvre(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.delete_docOeuvre;
    var loading = {
        type: "content",
        id_content: "contenair-immo"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocOeuvre', loading);
}
function retourDeleteDocOeuvre(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_oeuvre_id: response.data.id
                    }
                    delete_docOeuvre(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrowOeuvre-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocOeuvre', function (e) {
    e.preventDefault();
    var doc_oeuvre_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_oeuvre_id: doc_oeuvre_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocOeuvre;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentOeuvre', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocOeuvre", "null_fn", param);
});

function retourupdatedocOeuvre(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmoGrosOeuvre(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document gros oeuvre***********/

$(document).on('click', '.btnTraiteOeuvre', function () {
    updateTraitementOeuvre($(this).attr('data-id'));
});

function updateTraitementOeuvre(doc_oeuvre_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_oeuvre_id': doc_oeuvre_id,
    };

    var url_request = acte_url.updateDocOeuvreTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementOeuvreCallback');
}

function updateTraitementOeuvreCallback(response) {
    getImmoGrosOeuvre(response.cliLot_id);
}

function apercuDocumentOeuvre(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathOeuvre;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathOeuvre');
}

function responsepathOeuvre(response) {
    var el_apercu = document.getElementById('apercu_docOeuvre')
    var dataPath = response[0].doc_oeuvre_path
    var id_document = response[0].doc_oeuvre_id
    apercuOeuvre(dataPath, el_apercu, id_document);
}

function apercuOeuvre(data, el, id_doc) {
    viderApercuOeuvre();
    var doc_selectionner = document.getElementById("docrowOeuvre-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuOeuvre').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuOeuvre').appendChild(element);
    }
}

function viderApercuOeuvre() {
    const doc_selectionne = document.getElementsByClassName("apercu_docOeuvre");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuOeuvre") != undefined) {
        if (document.getElementById("apercuOeuvre").querySelector("#view_apercu") != null) {
            document.getElementById("apercuOeuvre").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuOeuvre").querySelector("#view_apercu").remove();
        }

    }
}


/*********Immo-Facade***********/

function getImmofacade(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.ImmoFacade;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmoFacade', loading);
}

function returnImmoFacade(response) {
    $('#fiche-identifiant-facade').html(response);
}

$(document).on('click', '#ajoutDocFacade', function (e) {
    e.preventDefault();
    ajoutDocFacade($(this).attr('data-id'), 'add', null);
});

function ajoutDocFacade(cliLot_id, action, doc_facade_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_facade_id': doc_facade_id
    };
    var url_request = acte_url.formUploadDocFacade;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentFacade');

    $("#modalAjoutFacade").modal('show');
}

function chargercontentFacade(response) {
    $('#UploadFacade').html(response);
}

$(document).on('click', '.btnSuppDocFacade', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_facade_id: $(this).attr('data-id')
    };
    delete_docFacade(data_request);
});

function delete_docFacade(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.delete_docFacade;
    var loading = {
        type: "content",
        id_content: "contenair-immo"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocFacade', loading);
}
function retourDeleteDocFacade(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_facade_id: response.data.id
                    }
                    delete_docFacade(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrowfacade-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocFacade', function (e) {
    e.preventDefault();
    var doc_facade_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_facade_id: doc_facade_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocFacade;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentFacade', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocFacade", "null_fn", param);
});

function retourupdatedocFacade(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmofacade(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document facade***********/

$(document).on('click', '.btnTraiteFacade', function () {
    updateTraitementFacade($(this).attr('data-id'));
});

function updateTraitementFacade(doc_facade_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_facade_id': doc_facade_id,
    };

    var url_request = acte_url.updateDocFacadeTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementFacadeCallback');
}

function updateTraitementFacadeCallback(response) {
    getImmofacade(response.cliLot_id);
}

function apercuDocumentFacade(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathFacade;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathFacade');
}

function responsepathFacade(response) {
    var el_apercu = document.getElementById('apercu_docFacade')
    var dataPath = response[0].doc_facade_path
    var id_document = response[0].doc_facade_id
    apercuFacade(dataPath, el_apercu, id_document);
}

function apercuFacade(data, el, id_doc) {
    viderApercuFacade();
    var doc_selectionner = document.getElementById("docrowfacade-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuFacade').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuFacade').appendChild(element);
    }
}

function viderApercuFacade() {
    const doc_selectionne = document.getElementsByClassName("apercu_docfacade");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuFacade") != undefined) {
        if (document.getElementById("apercuFacade").querySelector("#view_apercu") != null) {
            document.getElementById("apercuFacade").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuFacade").querySelector("#view_apercu").remove();
        }

    }
}


/*********Immo-Agencement***********/

function getImmoAgencement(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.ImmoAgencement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmoAgence', loading);
}

function returnImmoAgence(response) {
    $('#fiche-identifiant-agencement').html(response);
}

$(document).on('click', '#ajoutDocAgencement', function (e) {
    e.preventDefault();
    ajoutDocAgencement($(this).attr('data-id'), 'add', null);
});

function ajoutDocAgencement(cliLot_id, action, doc_agencement_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_agencement_id': doc_agencement_id
    };
    var url_request = acte_url.formUploadDocAgencement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentAgencement');

    $("#modalAjoutAgencement").modal('show');
}

function chargercontentAgencement(response) {
    $('#UploadAgencement').html(response);
}

$(document).on('click', '.btnSuppDocAgencement', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_agencement_id: $(this).attr('data-id')
    };
    delete_docAgencement(data_request);
});

function delete_docAgencement(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.delete_docAgencement;
    var loading = {
        type: "content",
        id_content: "contenair-immo"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocAgencement', loading);
}
function retourDeleteDocAgencement(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_agencement_id: response.data.id
                    }
                    delete_docAgencement(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrowagencement-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocAgencement', function (e) {
    e.preventDefault();
    var doc_agencement_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_agencement_id: doc_agencement_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocAgencement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentAgencement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocAgencement", "null_fn", param);
});

function retourupdatedocAgencement(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmoAgencement(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document Agencement***********/

$(document).on('click', '.btnTraiteAgencement', function () {
    updateTraitementAgencement($(this).attr('data-id'));
});

function updateTraitementAgencement(doc_agencement_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_agencement_id': doc_agencement_id,
    };

    var url_request = acte_url.updateDocAgencementTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementAgencementCallback');
}

function updateTraitementAgencementCallback(response) {
    getImmoAgencement(response.cliLot_id);
}

function apercuDocumentAgencement(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathAgencement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathAgencement');
}

function responsepathAgencement(response) {
    var el_apercu = document.getElementById('apercu_docAgencement')
    var dataPath = response[0].doc_agencement_path
    var id_document = response[0].doc_agencement_id
    apercuAgencement(dataPath, el_apercu, id_document);
}

function apercuAgencement(data, el, id_doc) {
    viderApercuAgencement();
    var doc_selectionner = document.getElementById("docrowagencement-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuAgencement').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuAgencement').appendChild(element);
    }
}

function viderApercuAgencement() {
    const doc_selectionne = document.getElementsByClassName("apercu_docAgencement");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuAgencement") != undefined) {
        if (document.getElementById("apercuAgencement").querySelector("#view_apercu") != null) {
            document.getElementById("apercuAgencement").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuAgencement").querySelector("#view_apercu").remove();
        }

    }
}

/*********Immo-Installation***********/

function getImmoInstallation(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.ImmoInstallation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmoInstallation', loading);
}

function returnImmoInstallation(response) {
    $('#fiche-identifiant-installation').html(response);
}

$(document).on('click', '#ajoutDocInstallation', function (e) {
    e.preventDefault();
    ajoutDocInstallation($(this).attr('data-id'), 'add', null);
});

function ajoutDocInstallation(cliLot_id, action, doc_installation_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_installation_id': doc_installation_id
    };
    var url_request = acte_url.formUploadDocInstallation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentInstallation');

    $("#modalAjoutInstallation").modal('show');
}

function chargercontentInstallation(response) {
    $('#UploadInstallation').html(response);
}

$(document).on('click', '.btnSuppDocInstallation', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_installation_id: $(this).attr('data-id')
    };
    delete_docInstallation(data_request);
});

function delete_docInstallation(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.delete_docInstallation;
    var loading = {
        type: "content",
        id_content: "contenair-immo"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocInstallation', loading);
}
function retourDeleteDocInstallation(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_installation_id: response.data.id
                    }
                    delete_docInstallation(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrowinstallation-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocInstallation', function (e) {
    e.preventDefault();
    var doc_installation_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_installation_id: doc_installation_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocInstallation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentInstallation', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocInstallation", "null_fn", param);
});

function retourupdatedocInstallation(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmoInstallation(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document Installation***********/

$(document).on('click', '.btnTraiteInstallation', function () {
    updateTraitementInstallation($(this).attr('data-id'));
});

function updateTraitementInstallation(doc_installation_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_installation_id': doc_installation_id,
    };

    var url_request = acte_url.updateDocInstallationTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementInstallationCallback');
}

function updateTraitementInstallationCallback(response) {
    getImmoInstallation(response.cliLot_id);
}

function apercuDocumentInstallation(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathInstallation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathInstallation');
}

function responsepathInstallation(response) {
    var el_apercu = document.getElementById('apercu_docInstallation')
    var dataPath = response[0].doc_installation_path
    var id_document = response[0].doc_installation_id
    apercuInstallation(dataPath, el_apercu, id_document);
}

function apercuInstallation(data, el, id_doc) {
    viderApercuInstallation();
    var doc_selectionner = document.getElementById("docrowinstallation-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuInstallation').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuInstallation').appendChild(element);
    }
}

function viderApercuInstallation() {
    const doc_selectionne = document.getElementsByClassName("apercu_docinstallation");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuInstallation") != undefined) {
        if (document.getElementById("apercuInstallation").querySelector("#view_apercu") != null) {
            document.getElementById("apercuInstallation").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuInstallation").querySelector("#view_apercu").remove();
        }

    }
}


/*********Immo-Mobilier***********/

function getImmoMobilier(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = acte_url.ImmoMobilier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnImmoMobilier', loading);
}

function returnImmoMobilier(response) {
    $('#fiche-identifiant-mobilier').html(response);
}

$(document).on('click', '#ajoutDocMobilier', function (e) {
    e.preventDefault();
    ajoutDocMobilier($(this).attr('data-id'), 'add', null);
});

function ajoutDocMobilier(cliLot_id, action, doc_mobilier_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_mobilier_id': doc_mobilier_id
    };
    var url_request = acte_url.formUploadDocMobilier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentMobilier');

    $("#modalAjoutMobilier").modal('show');
}

function chargercontentMobilier(response) {
    $('#UploadMobilier').html(response);
}

$(document).on('click', '.btnSuppDocMobilier', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_mobilier_id: $(this).attr('data-id')
    };
    delete_docMobilier(data_request);
});

function delete_docMobilier(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = acte_url.delete_docMobilier;
    var loading = {
        type: "content",
        id_content: "contenair-immo"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocMobilier', loading);
}
function retourDeleteDocMobilier(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_mobilier_id: response.data.id
                    }
                    delete_docMobilier(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrowMobilier-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocMobilier', function (e) {
    e.preventDefault();
    var doc_mobilier_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_mobilier_id: doc_mobilier_id, cliLot_id: cliLot_id };
    var url_request = acte_url.modalupdateDocMobilier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentMobilier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocMobilier", "null_fn", param);
});

function retourupdatedocMobilier(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getImmoMobilier(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document Mobilier***********/

$(document).on('click', '.btnTraiteMobilier', function () {
    updateTraitementMobilier($(this).attr('data-id'));
});

function updateTraitementMobilier(doc_mobilier_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_mobilier_id': doc_mobilier_id,
    };

    var url_request = acte_url.updateDocMobilierTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementMobilierCallback');
}

function updateTraitementMobilierCallback(response) {
    getImmoMobilier(response.cliLot_id);
}

function apercuDocumentMobilier(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathMobilier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMobilier');
}

function responsepathMobilier(response) {
    var el_apercu = document.getElementById('apercu_docMobilier')
    var dataPath = response[0].doc_mobilier_path
    var id_document = response[0].doc_mobilier_id
    apercuMobilier(dataPath, el_apercu, id_document);
}

function apercuMobilier(data, el, id_doc) {
    viderApercuMobilier();
    var doc_selectionner = document.getElementById("docrowMobilier-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuMobilier').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuMobilier').appendChild(element);
    }
}

function viderApercuMobilier() {
    const doc_selectionne = document.getElementsByClassName("apercu_docMobilier");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuMobilier") != undefined) {
        if (document.getElementById("apercuMobilier").querySelector("#view_apercu") != null) {
            document.getElementById("apercuMobilier").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuMobilier").querySelector("#view_apercu").remove();
        }

    }
}


$(document).on('change', '#acte_type_acquisition', function () {
    var acte_type_acquisition = $('#acte_type_acquisition').val();
    if (acte_type_acquisition == "Reprise") {
        $("#acte_valeur_terrain_ht").attr('disabled', false);
    } else {
        $("#acte_valeur_terrain_ht").attr('disabled', true);
    }
});


$(document).on('keyup', '#acte_valeur_terrain_ht', function (e) {
    e.preventDefault();
    var valeur_acquisition = parseFloat($('#acte_valeur_acquisition_ht').val());
    var valeur_terrain = parseFloat($('#acte_valeur_terrain_ht').val());
    var valeur_construction = valeur_acquisition - valeur_terrain;
    var valeur_mobilier = parseFloat($('#acte_valeur_mobilier_ht').val());
    var totaux = valeur_mobilier + valeur_construction;
    $('#acte_valeur_construction_ht').val(valeur_construction);
    $('#totalValeur').text(totaux.toFixed(2));

    var acte_taux_terrain = valeur_terrain * 100 / valeur_acquisition;
        
    $('#acte_taux_terrain').val(acte_taux_terrain.toFixed(2) + '%');
    var acte_taux_construction = 100 - acte_taux_terrain;
    $('#acte_taux_construction').val(acte_taux_construction.toFixed(2) + '%');

});


function apercuDocumentActe(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsePathActe');
}

function responsePathActe(response) {
    var el_apercu = document.getElementById('apercu_doc_Acte')
    var dataPath = response[0].doc_acte_path
    var id_document = response[0].doc_acte_id
    apercuDocActe(dataPath, el_apercu, id_document);
}

function apercuDocActe(data, el, id_doc) {
    viderApercuActe();
    var doc_selectionner = document.getElementById("docrowActe-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuActe').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuActe').appendChild(element);
    }
}

function viderApercuActe() {
    const doc_selectionne = document.getElementsByClassName("apercuActe");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuActe") != undefined) {
        if (document.getElementById("apercuActe").querySelector("#view_apercu") != null) {
            document.getElementById("apercuActe").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuActe").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '#btn_bail_alert_acte', function () {
    alert_dateSign_mandat('Acte');
});

$(document).on('keyup', '#acte_frais_acquisition', function (e) {
    e.preventDefault();
    calculImmotableau();
});