var saisie_url = {
    saisieTempsGeneral: `${const_app.base_url}SaisieTemps/FormSaisieTemps`,
    saisieTempsClient : `${const_app.base_url}SaisieTemps/FormSaisieTempsClient`,
    saisieTempsDossier : `${const_app.base_url}SaisieTemps/FormSaisieTempsDossier`,
    saisieTempsLot : `${const_app.base_url}SaisieTemps/FormSaisieTempsLot`,
    listeSaisieTemps : `${const_app.base_url}SaisieTemps/listeTempsPasse`,
    listeJournaliere : `${const_app.base_url}SaisieTemps/listeTempsJournaliere`,
}

var timer = null;

function pgSaisieTemps() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-saisie"
    }
    var url_request = saisie_url.listeSaisieTemps;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerpagelisteSaisieTemps', loading);
}

function chargerpagelisteSaisieTemps(response) {
    $('#contenair-saisie').html(response)
}

$(document).on('change', '#filtre_date', function (e) {
    e.preventDefault();
    getListeTempsPasse();
});

$(document).on('change', '#util_id', function(e) {
    e.preventDefault();
    getListeTempsPasse();
});

function getListeTempsPasse() {
    var type_request = 'POST';
    var type_output = 'html';
    var filtre_date = $('#filtre_date').val();
    var util_id = $('#util_id').val()
    var data_request = {
        filtre_date : filtre_date,
        util_id : util_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-saisie"
    }
    var url_request = saisie_url.listeJournaliere;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlistejournaliere', loading);
}

function retourlistejournaliere(response) {
    $('#content-saisie').html(response);
}

$(document).on('click', '#btnSaisieTempsGeneral', function (e) {    
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = saisie_url.saisieTempsGeneral;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'contentModalCallBack');
});


var modal = new bootstrap.Modal(document.getElementById("myModal"), {
    keyboard: false
});

function contentModalCallBack(response) {
    $("#modalLg").html(response);
    modal.toggle();
}

$(document).on('submit', '#AjoutTempsGeneral', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modalLg"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutTempsGeneral", loading);
});

function retourAjoutTempsGeneral(response) {
    if (response.status == true) {
        modal.toggle();
        var data = $(".total").html();
        var duree = data.split('H');
        var total = parseFloat(duree[1]);
        var hour = total + parseFloat(response.data_retour.duree);
        if (hour < 10 ) {
            var text = `0H0`+ hour;
        }else{
            var text = `0H`+ hour;
        }        
        $(".total").html(text);
        pgSaisieTemps();

    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSaisieTempsClient', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { 
        action: "add" ,
        client_id : $(this).attr('data-id')
    };
    var url_request = saisie_url.saisieTempsClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'contentModalCallBack');
});


$(document).on('submit', '#AjoutTempsClient', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modalLg"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutTempsClient", loading);
});

function retourAjoutTempsClient(response) {
    if (response.status == true) {
        modal.toggle();  
        var data = $(".total").html();
        var duree = data.split('H');
        var total = parseFloat(duree[1]);
        var hour = total + parseFloat(response.data_retour.duree);
        var text = `0H`+ hour ;
        $(".total").html(text);     
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSaisieTempsDossier', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { 
        action: "add" ,
        dossier_id : $(this).attr('data-id')
    };
    var url_request = saisie_url.saisieTempsDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'contentModalCallBack');
});


$(document).on('submit', '#AjoutTempsDossier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modalLg"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutTempsDossier", loading);
});

function retourAjoutTempsDossier(response) {
    if (response.status == true) {
        modal.toggle();   
        var data = $(".total").html();
        var duree = data.split('H');
        var total = parseFloat(duree[1]);
        var hour = total + parseFloat(response.data_retour.duree);
        var text = `0H`+ hour ;
        $(".total").html(text);     
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSaisieTempsLot', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { 
        action: "add" ,
        cliLot_id : $(this).attr('data-id'),
        client_id : $(this).attr('data-client')
    };
    var url_request = saisie_url.saisieTempsLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'contentModalCallBack');
});

$(document).on('submit', '#AjoutTempsLot', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modalLg"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAjoutTempsLot", loading);
});

function retourAjoutTempsLot(response) {
    if (response.status == true) {
        modal.toggle();   
        var data = $(".total").html();
        var duree = data.split('H');
        var total = parseFloat(duree[1]);
        var hour = total + parseFloat(response.data_retour.duree);
        var text = `0H`+ hour ;
        $(".total").html(text);   
    } else {
        fn.response_control_champ(response);
    }
}

function roundNearest5(num) {
    return Math.round(num / 5) * 5;
  }

$(document).on('keyup', '#travail_duree', function () {
    var duree = $(this).val();
    const duree5 = roundNearest5(duree);    
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        $(this).val(duree5);
    }, 800);
   
});





