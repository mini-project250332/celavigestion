
var cloture_annuelle_url = {
    listCloture_Annuelle: `${const_app.base_url}ClotureAnnuelle/listCloture_Annuelle`,
    demander_cloture: `${const_app.base_url}ClotureAnnuelle/demander_cloture`,
    valider_cloture: `${const_app.base_url}ClotureAnnuelle/valider_cloture`,
    annuler_cloture: `${const_app.base_url}ClotureAnnuelle/annuler_cloture`,
    confirmDemande: `${const_app.base_url}ClotureAnnuelle/confirmDemande`,
    sendMailcloture: `${const_app.base_url}SendMail/sendMailcloture`,
    sendMailclotureExpercomptable: `${const_app.base_url}SendMail/sendMailclotureExpercomptable`,
}

var timer = null;

var myModalcloture = new bootstrap.Modal(document.getElementById("modalFormCloture"), {
    keyboard: false
});

function getCloture_Annuelle(dossier_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': dossier_id,
    };
    var loading = {
        type: "content",
        id_content: "contenair-cloture_annuelle"
    }

    var url_request = cloture_annuelle_url.listCloture_Annuelle;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'RetourCloture_Annuelle', loading);
}
function RetourCloture_Annuelle(response) {
    $('#contenair-cloture_annuelle').html(response);
}

// validated for clôture
$(document).on('click', '.valider_cloture', function () {
    var data_request = {
        action: "demande",
        cloture_id: $(this).attr('data-id'),
    };

    valider_cloture(data_request);
});

function valider_cloture(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = cloture_annuelle_url.valider_cloture;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourvalider_cloture');
}
function retourvalider_cloture(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cloture_id: response.data.id,
                    }
                    valider_cloture(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getCloture_Annuelle(response.data.dossier_id);
            sendMailclotureExpercomptable(response.data.dossier_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}
function sendMailclotureExpercomptable(dossier_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id: dossier_id
    };
    var url_request = cloture_annuelle_url.sendMailclotureExpercomptable;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retoursendMailclotureExpercomptable');
}
function retoursendMailclotureExpercomptable(response) {
    if (response.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

// annuler clôture
$(document).on('click', '.annuler_cloture', function () {
    var data_request = {
        action: "demande",
        cloture_id: $(this).attr('data-id'),
    };

    annuler_cloture(data_request);
});

function annuler_cloture(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = cloture_annuelle_url.annuler_cloture;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourannuler_cloture');
}
function retourannuler_cloture(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cloture_id: response.data.id,
                    }
                    annuler_cloture(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getCloture_Annuelle(response.data.dossier_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.demander_cloture', function () {
    var data_request = {
        cloture_id: $(this).attr('data-id'),
    };

    demander_cloture(data_request);
});

function demander_cloture(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var url_request = cloture_annuelle_url.demander_cloture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalCloture');
}
function charger_contentModalCloture(response) {
    $("#modal-main-content-cloture").html(response);
    myModalcloture.toggle();
}

$(document).on('click', '#confirmDemande', function () {
    var data_request = {
        cloture_id: $(this).attr('data-id'),
        util_expert_comptable: $('#util_expert_comptable').val()
    };

    if ($('#util_expert_comptable').val()) {
        myModalcloture.toggle();
        confirmDemande(data_request);
    } else {
        $('.message_erreur_exp').text('Vous devez choisir un expert-comptable');
        setTimeout(function () {
            $('.message_erreur_exp').text('');
        }, 3000);
    }
});

function confirmDemande(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var url_request = cloture_annuelle_url.confirmDemande;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourconfirmDemande');
}
function retourconfirmDemande(response) {
    const datas = JSON.parse(response);
    if (datas.status == 200) {
        Notiflix.Notify.success(datas.message, { position: 'left-bottom', timeout: 3000 });
        getCloture_Annuelle(datas.data.dossier_id);
        sendMailcloture(datas.data.dossier_id);
    }
    else {
        Notiflix.Notify.failure(datas.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function sendMailcloture(dossier_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id: dossier_id
    };
    var url_request = cloture_annuelle_url.sendMailcloture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retoursendMailcloture');
}
function retoursendMailcloture(response) {
    if (response.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}