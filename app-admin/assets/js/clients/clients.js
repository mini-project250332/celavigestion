var data_url = {
    header: `${const_app.base_url}Clients/getheaderlist`,
    client: `${const_app.base_url}Clients/listeClient`,
    delete: `${const_app.base_url}Clients/removeClient`,
    formNew: `${const_app.base_url}Clients/formNewClient`,
    ficheClient: `${const_app.base_url}Clients/ficheClient`,
    ficheClient_page: `${const_app.base_url}Clients/pageficheClient`,
    removefilecin: `${const_app.base_url}Clients/removefilecin`,
    removefilecin2: `${const_app.base_url}Clients/removefilecin2`,
    modalEnvoiMailCNI: `${const_app.base_url}SendMail/modalEnvoiMailCNI_Client`,
    demande_cni: `${const_app.base_url}SendMail/demande_cni_client`,
    /**** dossier ****/
    getListDossier: `${const_app.base_url}Clients/getListDossier`,
    getFormDossier: `${const_app.base_url}Clients/getFormDossier`,
    removeDossier: `${const_app.base_url}Clients/removeDossier`,
    modalFormClient: `${const_app.base_url}Clients/FormClient`,
    getDossier: `${const_app.base_url}Clients/getFicheDossier`,
    ficheDossier: `${const_app.base_url}Clients/getFormDossier`,
    getInfoCgp: `${const_app.base_url}Clients/getInfoCgp`,
    modalFormCgp: `${const_app.base_url}Clients/FormCgp`,
    /****** lot *********/
    getLotDossier: `${const_app.base_url}Clients/getLotDossier`,
    getFormLot: `${const_app.base_url}Clients/getFormLot`,
    getLot: `${const_app.base_url}Clients/getFicheLot`,
    ficheLot: `${const_app.base_url}Clients/FicheLot`,
    delete_lot: `${const_app.base_url}Clients/removeLot`,
    modalFormGestionnaire: `${const_app.base_url}Clients/FormGestionnaire`,
    modalFormsyndicat: `${const_app.base_url}Clients/FormSyndicat`,
    modalFormProgramme: `${const_app.base_url}Clients/FormProgramme`,
    formAlertAjoutLot: `${const_app.base_url}Clients/formAlertAjoutLot`,
    genererDocExploitant: `${const_app.base_url}Clients/genererDocExploitant`,
    genererDocSyndic: `${const_app.base_url}Clients/genererDocSyndic`,
    genererDocSie: `${const_app.base_url}Clients/genererDocSie`,
    genererDocSip: `${const_app.base_url}Clients/genererDocSip`,
    /*******document*******/
    formUpload: `${const_app.base_url}Clients/formUpload`,
    listeDocument: `${const_app.base_url}Clients/listDocument`,
    getDocpath: `${const_app.base_url}Clients/getDocpath`,
    modalupdateDocClient: `${const_app.base_url}Clients/FormUpdateDocClient`,
    delete_docClient: `${const_app.base_url}Clients/removedocClient`,
    /*******communication**************/
    listeCom: `${const_app.base_url}Clients/listCommunication`,
    getFormCom: `${const_app.base_url}Clients/getFormCom`,
    getFormTache: `${const_app.base_url}Clients/getFormTache`,
    deleteCom: `${const_app.base_url}Clients/removeCom`,
    deleteTache: `${const_app.base_url}Clients/removeTache`,
    valideTache: `${const_app.base_url}Clients/valideTache`,
    /*******Document taxe*******/
    listeDocumentTaxe: `${const_app.base_url}Clients/listeDocumentTaxe`,
    formUploadTaxe: `${const_app.base_url}Clients/formUploadTaxe`,
    getpathDoc: `${const_app.base_url}Clients/getpathDoc`,
    delete_docTaxe: `${const_app.base_url}Clients/removedocTaxe`,
    updateDocTaxeTraitement: `${const_app.base_url}Clients/updateDocTaxeTraitement`,
    modalupdateDocTaxe: `${const_app.base_url}Clients/FormUpdateTaxe`,
    valideTaxe: `${const_app.base_url}Clients/valideTaxe`,
    AnnulerTaxe: `${const_app.base_url}Clients/annulerTaxe`,
    listeDocTaxe: `${const_app.base_url}Clients/listeDocTaxe`,

    /***********Prêts*********/
    listeDocumentPret: `${const_app.base_url}Clients/listeDocumentPret`,
    formUploadPret: `${const_app.base_url}Clients/formUploadPret`,
    getpathPret: `${const_app.base_url}Clients/getpathPret`,
    delete_docPret: `${const_app.base_url}Clients/removeDocPret`,
    formUpdateDocPret: `${const_app.base_url}Clients/formUpdateDocPret`,
    updateDocPretTraitement: `${const_app.base_url}Clients/updateDocPretTraitement`,
    addPresencePret: `${const_app.base_url}Clients/addPresencePret`,

    deletePret: `${const_app.base_url}Clients/removePret`,
    AddPret: `${const_app.base_url}Clients/AddPret`,
    UpdatePret: `${const_app.base_url}Clients/UpdatePret`,
    AddAnneePret: `${const_app.base_url}Clients/AddAnneePret`,
    formAlertDeletePret: `${const_app.base_url}Clients/formAlertDeletePret`,
    formAlertUpdatePret: `${const_app.base_url}Clients/formAlertUpdatePret`,

    /*********Mandat**********/
    listeDocumentMandat: `${const_app.base_url}Clients/listeMandat`,
    FormMandat: `${const_app.base_url}Clients/FormMandat`,
    alertMandat: `${const_app.base_url}Clients/alertMandat`,
    UploadMandat: `${const_app.base_url}Clients/FormUploadMandat`,
    getPathMandat: `${const_app.base_url}Clients/getPathMandat`,
    getPath_mandat: `${const_app.base_url}Clients/getPath`,
    deleteDocMandat: `${const_app.base_url}Clients/removeDocMandat`,
    deleteMandat: `${const_app.base_url}Clients/removeMandat`,
    sendMandat: `${const_app.base_url}Clients/sendMandat`,
    SignerMandat: `${const_app.base_url}Clients/SignerMandat`,
    cloturerMandat: `${const_app.base_url}Clients/cloturerMandat`,
    retracterMandat: `${const_app.base_url}Clients/retracterMandat`,
    suspensionMandat: `${const_app.base_url}Clients/suspensionMandat`,
    annulersuspensionMandat: `${const_app.base_url}Clients/annulersuspensionMandat`,
    getPathFicJustificatif: `${const_app.base_url}Clients/getPathFicJustificatif`,

    /***********Bail*********/
    listeDocumentBail: `${const_app.base_url}Clients/listeDocumentBail`,
    getpathDocbail: `${const_app.base_url}Clients/getpathDocbail`,

    /***********Modal Indexation Loyer*********/
    getModalIndexationLoyer: `${const_app.base_url}Clients/getModalIndexationLoyer`,

    /***********Modal Historique Bail*********/
    getModalHistoriqueBail: `${const_app.base_url}Clients/getModalHistoriqueBail`,

    getModalHistoriqueContentBail: `${const_app.base_url}Clients/getModalHistoriqueContentBail`,

    /***********Loyer*********/
    listeDocumentLoyer: `${const_app.base_url}Clients/listeDocumentLoyer`,
    formUploadDocLoyer: `${const_app.base_url}Clients/formUploadDocLoyer`,
    getPathLoyer: `${const_app.base_url}Clients/getPathLoyer`,

    getInfoProgramme: `${const_app.base_url}Clients/getInfoProgramme`,

    /*******Document lot************/
    formUploadDocLot: `${const_app.base_url}Clients/formUploadDocLot`,
    remove_file: `${const_app.base_url}Clients/remove_ribLot`,
    updateDateSignatureMandatClient: `${const_app.base_url}Clients/updateDateSignatureMandatClient`,

    /****Envoi mail fiche identification lot***/
    EnvoiExploitant: `${const_app.base_url}SendMail/EnvoiExploitant`,
    EnvoiSyndic: `${const_app.base_url}SendMail/EnvoiSyndic`,
    EnvoiSie: `${const_app.base_url}SendMail/EnvoiSie`,
    EnvoiSip: `${const_app.base_url}SendMail/EnvoiSip`,

    /****alert IBAN***/
    alert_Iban: `${const_app.base_url}Clients/alert_Iban`,

    /***historique adresse*/
    historiqueAdresse: `${const_app.base_url}Clients/historiqueAdresse`,


    /*** encaissement *****/
    getEncaissement: `${const_app.base_url}Clients/Encaissement/getEncaissement`,
    getListeEncaissement: `${const_app.base_url}Clients/Encaissement/getListeEncaissement`,
    get_formAjoutEncaissement: `${const_app.base_url}Clients/Encaissement/get_formulaireEncaissement`,
    deleteCommunications: `${const_app.base_url}Clients/Encaissement/deleteCommunications`,
    getPath: `${const_app.base_url}Clients/Encaissement/getPath`,
    supprimerFile: `${const_app.base_url}Clients/Encaissement/supprimerFile`,

    annuler_clotureEnc: `${const_app.base_url}Clients/Encaissement/annuler_clotureEnc`,
    finSaisi_encais: `${const_app.base_url}Clients/Encaissement/fin_saisi_encais`,

    Modifier_restant_du: `${const_app.base_url}Clients/Clients/Modifier_restant_du`,

    /**** *****/

    checked_identity : `${const_app.base_url}Clients/Clients/checked_identity`,

}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgClient() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.header;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_headerListe', loading);
}
function load_headerListe(response) {
    $('#contenair-client').html(response);
}

$(document).on('click', '#retourListeClient', function (e) {
    e.preventDefault();
    var currentUrl = window.location.href;
    var parts = currentUrl.split('?');
    if (parts.length === 2) {
        var baseUrl = parts[0];
        window.location.href = baseUrl;
    }
    pgClient();
});

/***** pagination *****/

$(document).on('click', '.pagination-vue-table', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view').val()}`;
        $('#pagination_view').val(limit);
        listClient();
    }
});


$(document).on('change', '.row_view', function (e) {
    e.preventDefault();
    listClient();
});

$(document).on('click', '#pagination-vue-next', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-${array_offset[index_prev]}`).trigger("click");
    }
});

/****filtre****/

$(document).on('keyup', '#text_filtre', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});

$(document).on('keyup', '#filtre_adresse', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});

$(document).on('keyup', '#filtre_pgm', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});

$(document).on('keyup', '#filtre_dossier', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});

$(document).on('keyup', '#filtre_siret', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});

$(document).on('keyup', '#filtre_mandat', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listClient();
    }, 800);
});


$(document).on('change', '#origine_cli_id', function (e) {
    e.preventDefault();
    listClient();
});

$(document).on('change', '#gestionnaire_id', function (e) {
    e.preventDefault();
    listClient();
});

$(document).on('change', '#cin_client', function (e) {
    e.preventDefault();
    listClient();
});

$(document).on('change', '#filtre_util', function (e) {
    e.preventDefault();
    listClient();
});

function listClient() {
    var type_request = 'POST';
    var type_output = 'html';
    var text_filtre = $('#text_filtre').val();
    var filtre_adresse = $('#filtre_adresse').val();
    var origine_cli_id = $('#origine_cli_id').val();
    var gestionnaire_id = $('#gestionnaire_id').val();
    var date_filtre = $('#date_filtre').val();
    var filtre_pgm = $('#filtre_pgm').val();
    var filtre_dossier = $('#filtre_dossier').val();
    var filtre_siret = $('#filtre_siret').val();
    var filtre_mandat = $('#filtre_mandat').val();
    var row_view = $('#row_view').val();
    var pagination_view = $('#pagination_view').val();
    var cin_client = $('#cin_client').val();
    var utilisateur = $('#filtre_util').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 200;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;

    var data_request = {
        text_filtre: text_filtre,
        origine_cli_id: origine_cli_id,
        gestionnaire_id: gestionnaire_id,
        date_filtre: date_filtre,
        filtre_pgm: filtre_pgm,
        filtre_adresse: filtre_adresse,
        filtre_dossier: filtre_dossier,
        filtre_siret: filtre_siret,
        filtre_mandat: filtre_mandat,
        row_data: $row_data,
        pagination_page: $pagination_page,
        cin_client: cin_client,
        utilisateur: utilisateur
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.client;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listListe', loading);
}
function load_listListe(response) {
    $('#data-list').html(response);
}

$(document).on('click', '#btnFormNewClient', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.formNew;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formNew', loading);
});
function load_formNew(response) {
    $('#contenair-client').html(response);
}

$(document).on('click', '#btnAnnuler', function (e) {
    pgClient();
});


$(document).on('submit', '#formAddAjout', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAdd", loading);
});
function retourAdd(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            client_id: response.data_retour
        };
        ficheClient(data_request);


    } else {
        fn.response_control_champ(response);
    }
}

function ficheClient(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.ficheClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_formUpdate', loading);
}

$(document).on('click', '.btnFicheCLientNewTab', function (e) {
    e.preventDefault();
    const clientId = $(this).attr('data-id');
    var href = $('#client-open-tab-url').val();
    window.open(`${href}?_open@client_tab=${clientId}`, "_blank");
});


$(document).on('click', '.btnFicheClient', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        client_id: $(this).attr('data-id')
    };
    ficheClient(data_request);
});
function load_formUpdate(response) {
    if (localStorage.getItem('ClientName') || localStorage.removeItem('Clientid')) {
        localStorage.removeItem('ClientName');
        localStorage.removeItem('Clientid');
    }
    $('#contenair-client').html(response);
}

$(document).on('click', '.btnConfirmSup', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        client_id: $(this).attr('data-id')
    };
    deleteClient(data_request);
});
function deleteClient(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete;
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteClient', loading);
}
function retourDeleteClient(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        client_id: response.data.id
                    }
                    deleteClient(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowClient-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}



$(document).on('click', '#btn-update-client', function (e) {
    e.preventDefault();
    $("#annulerUpdateClient").toggleClass('d-none');
    $("#save-update-client").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationClient($(this).attr('data-id'), 'form');
});

$(document).on('click', '#annulerUpdateClient', function (e) {
    e.preventDefault();
    $('#btn-update-client').toggleClass('d-none');
    $("#save-update-client").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getFicheIdentificationClient($(this).attr('data-id'), 'fiche');
});

function getFicheIdentificationClient(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'client_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.ficheClient_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheClient', loading);
}
function retourFicheClient(response) {
    $('#fiche-identifiant-client').html(response);
}

$(document).on('submit', '#updateInfoClient', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();

    /*
        var file = $('#cin').prop('files')[0];
        fd.append("file", file);

        var file2 = $('#cin2').prop('files')[0];
        fd.append("file2", file2);
    */

    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;
    if (!$('#client_nom').val()) {
        required_data = false;
        $('.client_nom_help').text('Ce champ ne doit pas être vide.');
    }
    else if (!$('#client_phonemobile').val()) {
        required_data = false;
        $('.client_phonemobile_help').text('Ce champ ne doit pas être vide.');
    }
    else if (!$('#client_pays').val()) {
        required_data = false;
        $('.client_pays_help').text('Ce champ ne doit pas être vide.');
    }
    else if (!$('#client_date_creation').val()) {
        required_data = false;
        $('.client_date_creation_help').text('Ce champ ne doit pas être vide.');
    }
    if (required_data == true) {
        $('.help').each(function () {
            $(this).text('');
        });
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {

            }
        });
    }
});

$(document).on('click', '#save-update-client', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        Notiflix.Notify.success('La modification est effectuée avec succès.', { position: 'left-bottom', timeout: 3000 });
        // getFicheIdentificationClient($('#client_id').val(), 'fiche');
        var data_request = {
            client_id: $('#client_id').val()
        };
        ficheClient(data_request);
        $("#annulerUpdateClient").toggleClass('d-none');
        $("#save-update-client").toggleClass('d-none');
        $("#btn-update-client").toggleClass('d-none');
    }, 300);
})

$(document).on('change', '#client_nationalite', function () {
    if ($(this).val() == "Française") {
        $('.select2').val(66).trigger('change');
    }
    if ($(this).val() == "Britannique") {
        $('.select2').val(189).trigger('change');
    }

    if ($(this).val() == "Irlandaise") {
        $('.select2').val(109).trigger('change');
    }
});

$(document).on('keyup', '#client_phonemobile', function () {
    var client_phonemobile = $(this).val();
    const noSpecialCharacters = client_phonemobile.replace(/[^a-zA-Z0-9 ]/g, '');
    const number = noSpecialCharacters.replace(/\D/g, '');
    const phonemobile = number.replace(/\s/g, '');
    $(this).val(phonemobile);
});

$(document).on('keyup', '#client_phonefixe', function () {
    var client_phonefixe = $(this).val();
    const noSpecialCharacters = client_phonefixe.replace(/[^a-zA-Z0-9 ]/g, '');
    const number = noSpecialCharacters.replace(/\D/g, '');
    const phonefixe = number.replace(/\s/g, '');
    $(this).val(phonefixe);
});

$(document).on('keyup', '#updateInfoClient input, #updateInfoClient textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 800);
});

$(document).on('change', '#updateInfoClient select, #updateInfoClient input', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateIdentification();
    }, 1500);
});

function submitUpdateIdentification() {
    $('#updateInfoClient').trigger('submit');
}

/***** dossier *****/

function getListeDossier(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        client_id: id
    };
    var loading = {
        type: "content",
        id_content: "contenair-dossier"
    }
    var url_request = data_url.getListDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeDossier', loading);
}
function load_listeDossier(response) {
    $('#contenair-dossier').html(response);
}

function getFormDossier(client_id, action, dossier_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'dossier_id': dossier_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-dossier"
    }
    var url_request = data_url.getFormDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormDossier', loading);
}
function retourFormDossier(response) {
    $('#contenair-dossier').html(response);
}

function getFicheIdentificationDossier(client_id, action, dossier_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'dossier_id': dossier_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.ficheDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFicheDossier', loading);
}

function returnFicheDossier(response) {
    $('#fiche-identifiant-dossier').html(response);
}

$(document).on('click', '#btnFormAddDossier', function (e) {
    e.preventDefault();
    getFormDossier($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerFormDossier', function (e) {
    e.preventDefault();
    getListeDossier($(this).attr('data-id'));
});

$(document).on('click', '#retour_fiche_dossier', function (e) {
    e.preventDefault();
    getFicheIdentificationDossier($(this).attr('data-client_id'), 'fiche', $(this).attr('data-id'));
});

$(document).on('submit', '#cruDossier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddDossier", loading);
});

function changetypedossier() {
    var selected_option = $('#dossier_type_adresse').val();
    if (selected_option == "Adresse spécifique") {
        $(".lot_principal").hide();
        $(".specifique").show();
    } else {
        $(".specifique").hide();
        $(".lot_principal").hide();
    }
}

$(document).on('change', '#dossier_type_adresse', function (e) {
    changetypedossier();
});



function changetypeformejuridique() {
    var selected_option = $('#forme_juridique').val();
    if (selected_option == 1) {

        $(".indivision-section").show();

    } else {

        $(".indivision-section").hide();
    }
}

$(document).on('change', '#dossier_type_adresse', function (e) {
    changetypeformejuridique();
});

function submitUpdateDossier() {
    var form = $("#updateDossier");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output);
}

function submitUpdateDossierChange() {
    var form = $("#updateDossier");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output, 'retourupdateDossierChange');
}

function retourupdateDossierChange(response) {
    var client = response.data_retour.client_id;
    var client_id = client.length > 0 ? client[0] : client;
    getFicheIdentificationDossier(client_id, response.data_retour.action, response.data_retour.dossier_id);
}

$(document).on('keyup', '#updateDossier input, #updateDossier textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }

    totalInput();

    timer = setTimeout(() => {
        submitUpdateDossier();
    }, 800);
});

function totalInput() {
    var total = 0;

    $('.inputValue').each(function () {
        var val = parseFloat($(this).val()) || 0;
        total += val;

        if (total > 100) {
            var last = total - val;
            var retenu = 100 - last;
            retenu = retenu < 0 ? 0 : retenu;
            $(this).val(retenu);
        }
    });
}

$(document).on('keyup', '#dossier_siret', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    var input = $(this);
    timer = setTimeout(() => {
        if (input.id = "dossier_siret") {
            let condition = {
                'verify': 'min',
                'min': { value: 14 }
            }
            if (!verify_maxLength(input.val(), condition)) {
                input.parent().parent('.form-group').addClass('has-error');
                input.parent('div').children('.help').text('valeur incorrecte');
            } else {
                input.parent().parent('.form-group').removeClass('has-error');
                input.parent('div').children('.help').text('');
                submitUpdateDossier();
            }
        } else {
            submitUpdateDossier();
        }
    }, 800);
});

function verify_maxLength(char = null, clause = null) {
    if (char == null || char == '' || clause == null)
        return false;
    char = char.replaceAll(' ', '');
    switch (clause.verify) {
        case 'min':
            return (char.length >= clause.min.value) ? true : false;
            break;
        case 'max':
            return (char.length <= clause.max.value) ? true : false;
            break;
        default: // all
            return ((char.length >= clause.min.value) && (char.length <= clause.max.value)) ? true : false;
            break;
    }
}

$(document).on('change', '#updateDossier select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateDossierChange();
    }, 800);
});

$(document).on('change', '#updateDossier :checkbox', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateDossier();
    }, 800);
});

$(document).on('change', '#cgp_id', function () {
    var cgp_id = $(this).val();
    getInfoCgp(cgp_id);
});

function getInfoCgp(cgp_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'cgp_id': cgp_id,
    };
    var url_request = data_url.getInfoCgp;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getInfoCgpCallback');
}

function getInfoCgpCallback(response) {
    $('#cgp_email').val(response.data_retour.cgp_email);
    $('#cgp_tel').val(response.data_retour.cgp_tel);
    $('#cgp_adresse1').val(response.data_retour.cgp_adresse1);
    $('#cgp_adresse2').val(response.data_retour.cgp_adresse2);
    $('#cgp_adresse3').val(response.data_retour.cgp_adresse3);
    $('#cgp_cp').val(response.data_retour.cgp_cp);
    $('#cgp_ville').val(response.data_retour.cgp_ville);
    $('#cgp_pays').val(response.data_retour.cgp_pays);
}

$(document).on('click', '#addCgp', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormCgp;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#AddCgp', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddCgp", "null_fn", param);
});

function retour_AddCgp(response) {
    if (response.status == true) {
        myModal.toggle();
        var cgp_id = $("#cgp_id");
        var cgp_email = $("#cgp_email");
        var cgp_tel = $("#cgp_tel");
        var cgp_adresse1 = $("#cgp_adresse1");
        var cgp_adresse2 = $("#cgp_adresse2");
        var cgp_adresse3 = $("#cgp_adresse3");
        var cgp_cp = $("#cgp_cp");
        var cgp_ville = $("#cgp_ville");
        var cgp_pays = $("#cgp_pays");
        cgp_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
        cgp_email.val(response.form_validation.liste.email);
        cgp_tel.val(response.form_validation.liste.tel);
        cgp_adresse1.val(response.form_validation.liste.adresse1);
        cgp_adresse2.val(response.form_validation.liste.adresse2);
        cgp_adresse3.val(response.form_validation.liste.adresse3);
        cgp_cp.val(response.form_validation.liste.cp);
        cgp_ville.val(response.form_validation.liste.ville);
        cgp_pays.val(response.form_validation.liste.pays);

    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('submit', '#updateDossier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddDossier", loading);
});

function retourAddDossier(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        if (response.data_retour.action == "edit") {
            getFormDossier(response.data_retour.client_id, 'fiche', response.data_retour.dossier_id);
        } else {
            var data_request = {
                dossier_id: response.data_retour.dossier_id,
                client_id: response.data_retour.client_id
            };
            getFicheDossier(data_request);
        }
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.fiche-dossier-client', function (e) {
    e.preventDefault();
    var data_request = {
        dossier_id: $(this).attr('data-id'),
        client_id: $(this).attr('data-client_id')
    }
    getFicheDossier(data_request);
});

function getFicheDossier(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.getDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheDossier', loading);
}
function retourFicheDossier(response) {
    $('#contenair-client').html(response);
}

$(document).on('click', '#btn-update-dossier', function (e) {
    e.preventDefault();
    getFicheIdentificationDossier($(this).attr('data-client'), 'edit', $(this).attr('data-id'));
});


$(document).on('click', '.delete-dossier-client', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        dossier_id: $(this).attr('data-id')
    }
    deleteDossier(data);
});

function deleteDossier(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.removeDossier;
    var loading = {
        type: "content",
        id_content: "contenair-dossier"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDossier', loading);
}
function retourDeleteDossier(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        dossier_id: response.data.id
                    }
                    deleteDossier(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowDossier-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/***********modal add client**********/

$(document).on('click', '#addClient', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

/********Lot*********/
function getLotDossier(client_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': client_id,
        // 'cliLot_id' : cliLot_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    var url_request = data_url.getLotDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeLot', loading);
}
function load_listeLot(response) {
    $('#contenair-lot').html(response);
}

$(document).on('click', '#btnAlertlot', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.formAlertAjoutLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

function getFormLot(client_id, action, cliLot_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'cliLot_id': cliLot_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    var url_request = data_url.getFormLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormLot', loading);
}
function retourFormLot(response) {
    $('#contenair-lot').html(response);
}

$(document).on('click', '#btnFormNewlot', function (e) {
    e.preventDefault();
    getFormLot($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerAddLot', function (e) {
    e.preventDefault();
    getLotDossier($(this).attr('data-id'));
});

$(document).on('click', '.btnFicheLot', function (e) {
    e.preventDefault();
    var data_request = {
        //    action : "edit",
        cliLot_id: $(this).attr('data-id'),
        client_id: $(this).attr('data-client_id')
    };
    getFicheLot(data_request);
});

$(document).on('click', '.getFicheLot', function (e) {
    e.preventDefault();
    var data_request = {
        cliLot_id: $(this).attr('data-cliLot_id'),
        client_id: $(this).attr('data-client_id')
    };

    getFicheLot(data_request);

    /*
        $("#lot_Tab_list .nav-link").each(function () {
            if ($(this).hasClass('active')) {
                var nav_link = $(this).attr('id');
                localStorage.setItem('nav_link_value', nav_link);
            }
        });

        $(".tab_lot").each(function () {
            if ($(this).hasClass('active')) {
                var tab_pane = $(this).attr('id');
                localStorage.setItem('tab_pane_value', tab_pane);
            }
        });
        retourGetFicheLot(data_request);
    */
});

function retourGetFicheLot(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.getLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'callback_retourGetFicheLot', loading);
}
function callback_retourGetFicheLot(response) {
    $('#contenair-client').html(response);

    var navLinkValue = localStorage.getItem('nav_link_value');
    var tabPaneValue = localStorage.getItem('tab_pane_value');

    localStorage.removeItem('nav_link_value');
    localStorage.removeItem('tab_pane_value');

    if (navLinkValue !== null && tabPaneValue !== null) {
        $(".nav-link").each(function () {
            if ($('a').hasClass('active')) {
                $('a').removeClass('active');
            }
        });

        $('#' + navLinkValue).addClass('active');

        $(".tab-pane").each(function () {
            if ($('.tab-pane').hasClass('show active')) {
                $('.tab-pane').removeClass('show active');
            }
        });

        $('#' + tabPaneValue).addClass('show active');
    }
}

function getFicheLot(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.getLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheLot', loading);
}
function retourFicheLot(response) {
    $('#contenair-client').html(response);
}

function getFicheIdentificationLot(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-client"
    }
    var url_request = data_url.ficheLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFicheLot', loading);
}
function returnFicheLot(response) {
    $('#fiche-identifiant-lot').html(response);
}

$(document).on('submit', '#cruLot', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddLot", loading);
});

function retourAddLot(response) {
    if (response.status == true) {
        console.log(response.data_retour.client_id, response.data_retour.cliLot_id);
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            //    action : "edit",
            cliLot_id: response.data_retour.cliLot_id,
            client_id: response.data_retour.client_id
        };
        getFicheLot(data_request);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.retourClient', function (e) {
    e.preventDefault();
    var ClientName = $(this).attr('data-nom');
    var client_id = $(this).attr('data-id');

    var data_request = {
        client_id: client_id
    };
    ficheClient(data_request);
    var currentUrl = window.location.href;
    var parts = currentUrl.split('?');
    if (parts.length === 2) {
        var baseUrl = parts[0];
        localStorage.setItem('ClientName', ClientName);
        localStorage.setItem('Clientid', client_id);
        window.location.href = baseUrl;
    }
});

/***********modal programme**********/

$(document).on('click', '#addProgramme', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormProgramme;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#AddProgram', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddProgram", "null_fn", param);
});

function retour_AddProgram(response) {
    if (response.status == true) {
        myModal.toggle();
        var progm_id = $("#progm_id");
        progm_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
    } else {
        fn.response_control_champ(response);
    }
}

/***********modal gestionnaire**********/
var myModalLot = new bootstrap.Modal(document.getElementById("modalFormLot"), {
    keyboard: false
});

$(document).on('click', '#addGestionnaire', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormGestionnaire;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalLot');
});

function charger_contentModalLot(response) {
    $("#modal-main-content-lot").html(response);
    myModalLot.toggle();
}

$(document).on('click', '#addSyndicat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "add" };
    var url_request = data_url.modalFormsyndicat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalLot');
});

$(document).on('submit', '#AddGestion', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddGestion", "null_fn", param);
});

function retour_AddGestion(response) {
    if (response.status == true) {
        myModalLot.toggle();
        var gestionnaire_id = $("#gestionnaire_id");
        gestionnaire_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('submit', '#AddSyndicat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_AddSyndicat", "null_fn", param);
});

function retour_AddSyndicat(response) {
    if (response.status == true) {
        myModalLot.toggle();
        var syndicat_id = $("#syndicat_id");
        syndicat_id.append(`<option value="${response.form_validation.liste.id}" selected="selected">
                                       ${response.form_validation.liste.libelle}
                                  </option>`);
    } else {
        fn.response_control_champ(response);
    }
}

/**********update real time lot client***************/

$(document).on('click', '#btn-update-lot', function (e) {
    e.preventDefault();
    getFicheIdentificationLot($(this).attr('data-id'), 'form');
});

function submitUpdateLot() {
    $('#updateInfoLot').trigger('submit');
}

$(document).on('keyup', '#updateInfoLot input', function (e) {
    e.preventDefault();
    var cliLot_id = $("#cliLot_id").val();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateLot();
        getDocActe(cliLot_id);
    }, 800);
});

$(document).on('change', '#rib', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateLot();
    }, 800);
});

$(document).on('change', '#updateInfoLot select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateLot();
    }, 800);
});


$(document).on('submit', '#updateInfoLot', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file_data = $('input[type="file"]')[0].files; // for multiple files
    for (var i = 0; i < file_data.length; i++) {
        fd.append("file_" + i, file_data[i]);
    }
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });

    fd.append($('#produitcli_id').attr('name'), $('#produitcli_id').val());

    if ($('#cliLot_iban_client').val()) {
        if (verifyIBAN($('#cliLot_iban_client').val()) == true) {
            $('.help_iban').text("");
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: fd,
                contentType: false,
                processData: false,
                success: function (data) {
                    getDocBails($("#cliLot_id").val());
                    getDocLoyers($("#cliLot_id").val());
                    getDoctaxeAnnee($("#cliLot_id").val());
                }
            });
        }
        else {
            $('.help_iban').text("L'IBAN n'est pas valide. Il n'a pas été enregistré.");
            $('.help_iban').css('color', 'red');
        }
    }
    else {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (data) {
                getDocBails($("#cliLot_id").val());
                getDocLoyers($("#cliLot_id").val());
                getDoctaxeAnnee($("#cliLot_id").val());
            }
        });
    }
});

function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

$(document).on('click', '#save-lot', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        Notiflix.Notify.success('La modification est effectuée avec succès.', { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot($('#cliLot_id').val(), 'fiche');
    }, 500);
});

$(document).on('click', '#removefilerib', function () {
    $('.doc').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val()
    };

    var url_request = data_url.remove_file;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefile');
});

function retour_removefile(response) {
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

/********Supprimer lot******/
$(document).on('click', '.btnConfirmSupLot', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    delete_lot(data_request);
});

function delete_lot(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_lot;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourdeletelot', loading);
}
function retourdeletelot(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    delete_lot(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowLot-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/********Supprimer lot Dossier******/

$(document).on('click', '.btnConfirmSupLotDossier', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    delete_lotDossier(data_request);
});

function delete_lotDossier(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_lot;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourdelete_lotDossier', loading);
}
function retourdelete_lotDossier(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    delete_lotDossier(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowLot-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getFacturation($('#dossier_id').val(), 'fiche');
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/********Document**********/

function getDocClient(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id
    };
    // var loading = {
    //     type: "content",
    //     id_content: "contenair-document"
    // }
    var url_request = data_url.listeDocument;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDoc');
}

function loadListeDoc(response) {
    $('#contenair-document').html(response);
}

$(document).on('click', '#ajoutdoc', function (e) {
    e.preventDefault();
    ajoutDoc($(this).attr('data-id'), 'add', null);
});

function ajoutDoc(client_id, action, doc_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'doc_id': doc_id
    };
    var url_request = data_url.formUpload;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');

    $("#modalAjoutDoc").modal('show');
}

function chargercontentModal(response) {
    $('#contenaireUpload').html(response);

}

function apercuDocument(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getDocpath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseDOcpath');
}

function responseDOcpath(response) {
    var el_apercu = document.getElementById('apercu_list_doc')
    var dataPath = response[0].doc_path
    var id_document = response[0].doc_id
    apercu(dataPath, el_apercu, id_document);
}

function apercu(data, el, id_doc) {
    viderApercu();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu').appendChild(element);
    }
}

function viderApercu() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercu_list_doc") != undefined) {
        if (document.getElementById("apercu_list_doc").querySelector("#view_apercu") != null) {
            document.getElementById("apercu_list_doc").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_list_doc").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '.zommer-apercu-list', function () {

    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() + 50)

})

$(document).on('click', '.dezommer-apercu-list', function () {
    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() - 50)

})

$(document).on('click', '.orginal-apercu-list', function () {

    $('#apercu_list_doc').find('object').css({ height: 'auto', width: 'auto' })

})

// update doc propriétaire

$(document).on('click', '.btnUpdtadeDoc', function (e) {
    e.preventDefault();
    var doc_id = $(this).attr("data-id");
    var client_id = $(this).attr("data-client");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_id: doc_id, client_id: client_id };
    var url_request = data_url.modalupdateDocClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentClient', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocClient", "null_fn", param);
});

function retourupdatedocClient(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocClient(response.data_retour.client_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*****supprimer document propriétaire****/

$(document).on('click', '.btnSuppDoc', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_id: $(this).attr('data-id')
    };
    delete_docClient(data_request);
});
function delete_docClient(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_docClient;
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocClient', loading);
}
function retourDeleteDocClient(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_id: response.data.id
                    }
                    delete_docClient(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/************communication*************/
function getCommunication(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    var url_request = data_url.listeCom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeCom', loading);
}

function loadListeCom(response) {
    $('#contenair-com').html(response);
}

function getFormCom(client_id, action, comclient_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'comclient_id': comclient_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    var url_request = data_url.getFormCom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormCom', loading);
}
function retourFormCom(response) {
    $('#contenair-com').html(response);
}

$(document).on('click', '#btnFormCom', function (e) {
    e.preventDefault();
    getFormCom($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#annulerAddCom', function (e) {
    e.preventDefault();
    getCommunication($(this).attr('data-id'));
});

$(document).on('click', '#updateCom', function (e) {
    e.preventDefault();
    getFormCom($(this).attr('data-client'), 'edit', $(this).attr('data-id'));
});

$(document).on('submit', '#cruCom', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddCom", loading);
});

function retourAddCom(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getCommunication(response.data_retour.client_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.deleteCom', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        comclient_id: $(this).attr('data-id')
    };
    delete_com(data_request);
});

function delete_com(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteCom;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteCom', loading);
}
function retourDeleteCom(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        comclient_id: response.data.id
                    }
                    delete_com(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowCom-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/********Tache********/

function getFormTache(client_id, action, tache_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'tache_id': tache_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }

    var url_request = data_url.getFormTache;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormCom', loading);
}

$(document).on('click', '#btnFormTache', function (e) {
    e.preventDefault();
    getFormTache($(this).attr('data-id'), 'add', null);
});

$(document).on('click', '#updateTache', function (e) {
    e.preventDefault();
    getFormTache($(this).attr('data-client'), 'edit', $(this).attr('data-id'));
});


$(document).on('submit', '#cruTache', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddTache", loading);
});

function retourAddTache(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getCommunication(response.data_retour.client_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSuppTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    delete_tache(data_request);
});

function delete_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteTache;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteTache', loading);
}
function retourDeleteTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    delete_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnValideTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    valide_tache(data_request);
});

function valide_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.valideTache;
    var loading = {
        type: "content",
        id_content: "contenair-com"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValideTache', loading);
}
function retourValideTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    valide_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/*********** taxe foncière ***************/

function getDoctaxe(id) {
    var taxeannee = new Date().getFullYear();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'taxeannee': taxeannee

    };
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    var url_request = data_url.listeDocumentTaxe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocTaxe', loading);
}

function loadListeDocTaxe(response) {
    $('#contenair-document').html(response);
}

$(document).on('click', '#ajoutdocTaxe', function (e) {
    e.preventDefault();
    ajoutDocTaxe($(this).attr('data-id'), 'add', null);
});

function ajoutDocTaxe(cliLot_id, action, doc_taxe_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_taxe_id': doc_taxe_id,
        'annee': $('#anneetaxe').val()
    };
    var url_request = data_url.formUploadTaxe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal');

    $("#modalAjoutDoc").modal('show');
}

$(document).on('keyup', '#cruTaxe input, #cruTaxe textarea', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitTaxe();
    }, 800);
});
$(document).on('change', '#cruTaxe select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitTaxe();
    }, 800);
});

function submitTaxe() {
    $('#annee').val($('#anneetaxe').val());
    var form = $("#cruTaxe");
    //var annee = $('select[name=annee] option').filter(':selected').val();
    var type_output = 'json';

    fn.ajax_form_data_custom(form, type_output, 'retourtaxe');
}

function retourtaxe(response) {
    $('#validetaxe').attr({
        'data-id': response.data_retour.taxe_id
    });
    $('#annulertaxe').attr({
        'data-id': response.data_retour.taxe_id
    });
    $('#prenom').html(response.data_retour.util_prenom);
    $('#date').html(response.data_retour.taxe_date_validation);

    $('#action_taxe').val(response.data_retour.action);
    $('input[name=taxe_id]').val(response.data_retour.taxe_id);
    $('input[name=action_taxe]').val(response.data_retour.action);
}

$(document).on('change', '#anneetaxe', function (e) {
    e.preventDefault();
    var cliLot_id = $(this).attr('data-id');
    getDoctaxeAnnee(cliLot_id)
});

function getDoctaxeAnnee(id) {
    var taxeannee = $('#anneetaxe').val();

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'annee': taxeannee

    };
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    var url_request = data_url.listeDocTaxe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retouranneetaxe', loading);
}

function retouranneetaxe(response) {
    $('#content-taxe').html(response);
    var taxeannee = $('select[name=anneetaxe] option').filter(':selected').text();
    $('#annees').html(taxeannee);
    $('#anneee').html(taxeannee);
}


function apercuDocumentTaxe(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getpathDoc;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepath');
}

function responsepath(response) {
    var el_apercu = document.getElementById('aperculistdocTaxe')
    var dataPath = response[0].doc_taxe_path
    var id_document = response[0].doc_taxe_id
    apercuDocTaxe(dataPath, el_apercu, id_document);
}

function apercuDocTaxe(data, el, id_doc) {
    viderApercuDocTaxe();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercu').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercu').appendChild(element);
    }
}

function viderApercuDocTaxe() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("aperculistdocTaxe") != undefined) {
        if (document.getElementById("aperculistdocTaxe").querySelector("#view_apercu") != null) {
            document.getElementById("aperculistdocTaxe").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("aperculistdocTaxe").querySelector("#view_apercu").remove();
        }
    }
}

function forceDownload_mandat_new(href) {
    var link = document.createElement('a');
    link.href = href;
    link.download = href;
    var str = link.download.split("/");
    link.download = str[10]
    document.body.appendChild(link);
    link.click();
}

/*****supprimer document taxe****/

$(document).on('click', '.btnSuppDocTaxe', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_taxe_id: $(this).attr('data-id')
    };
    delete_docTaxe(data_request);
});
function delete_docTaxe(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_docTaxe;
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocTaxe', loading);
}
function retourDeleteDocTaxe(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_taxe_id: response.data.id
                    }
                    delete_docTaxe(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocTaxe', function (e) {
    e.preventDefault();
    var doc_taxe_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_taxe_id: doc_taxe_id, cliLot_id: cliLot_id };
    var url_request = data_url.modalupdateDocTaxe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentTaxe', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedoctaxe", "null_fn", param);
});

function retourupdatedoctaxe(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDoctaxe(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document taxe***********/

$(document).on('click', '.btnTraitetaxe', function () {
    updateTraitementTaxe($(this).attr('data-id'));
});

function updateTraitementTaxe(doc_taxe_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_taxe_id': doc_taxe_id,
    };

    var url_request = data_url.updateDocTaxeTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementTaxeCallback');
}

function updateTraitementTaxeCallback(response) {
    getDoctaxe(response.cliLot_id);
}

/**********valider information taxe************/

$(document).on('click', '.btnValideInfotaxe', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        taxe_id: $(this).attr('data-id'),
        annee: $('#anneetaxe').val()
    };

    valide_taxe(data_request);
});

function valide_taxe(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.valideTaxe;
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValideTaxe', loading);
}

function retourValideTaxe(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        taxe_id: response.data.id,
                        annee: response.data.annee
                    }
                    valide_taxe(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            // var utilId = $("#util_id").val();
            // var cliLot_id = $("#cliLot_id").val();
            // addHistorique("tom", "Validation des informations", null, null, cliLot_id, "tom-history");

            $(".btnValideInfotaxe").toggleClass('d-none');
            $('input[name=taxe_montant]').attr('disabled', 'disabled')
            $('input[name=taxe_tom]').attr('disabled', 'disabled');
            $(".btnAnnulerInfotaxe").toggleClass('d-none');
            $(".envoiTom").toggleClass('d-none');
            $(".envoiTomAuClient").toggleClass('d-none');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            var liste = $('#liste-history');
            liste.empty();

            $.each(response.data.history, function (i, item) {
                liste.append(`
                    <ul class="p-0" style="list-style-type : none"><li class="fs--1">Informations ${(item.htaxe_valide == "0") ? 'annulée' : 'validées'} par ${item.util_prenom} ${item.htaxe_date_validation}</li><ul>
                `);
            });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/**************Annuler validation taxe****************/

$(document).on('click', '.btnAnnulerInfotaxe', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        taxe_id: $(this).attr('data-id'),
        annee: $('#anneetaxe').val()
    };
    Annuler_taxe(data_request);
});

function Annuler_taxe(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.AnnulerTaxe;
    var loading = {
        type: "content",
        id_content: "contenair-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourAnnulerTaxe', loading);
}

function retourAnnulerTaxe(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        taxe_id: response.data.id,
                        annee: response.data.annee
                    }
                    Annuler_taxe(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            // var utilId = $("#util_id").val();
            // var cliLot_id = $("#cliLot_id").val();
            // addHistorique("tom", "Annulation des informations", null, null, cliLot_id, "tom-history");

            // $(`tr[id=rowTache-${response.data.id}]`).remove();
            $(".btnValideInfotaxe").toggleClass('d-none');
            document.getElementById("taxe_montant").disabled = false;
            document.getElementById("taxe_tom").disabled = false;
            // $(".labelinfotaxe").toggleClass('d-none');
            $(".btnAnnulerInfotaxe").toggleClass('d-none');
            $(".envoiTom").toggleClass('d-none');

            var liste = $('#liste-history');
            liste.empty();
            $.each(response.data.history, function (i, item) {
                liste.append(`
                <ul class="p-0" style="list-style-type : none"><li class="fs--1">Informations ${(item.htaxe_valide == "0") ? 'annulées' : 'validées'} par ${item.util_prenom} ${item.htaxe_date_validation}</li></ul>
                `);
            });

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });

        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/************Bails***********/

function getDocBails(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_client_id: $("#lot_client_id").val(),
        'cliLot_id': id,
    };

    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }

    var url_request = data_url.listeDocumentBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocBail', loading);
}

function loadListeDocBail(response) {
    $('#contenair-bail').html(response);
}

function apercuDocumentBail(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getpathDocbail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathbail');
}

function responsepathbail(response) {
    var el_apercu = document.getElementById('apercudocBail')
    var dataPath = response[0].doc_bail_path
    var id_document = response[0].doc_bail_id
    apercuBail(dataPath, el_apercu, id_document);
}

function apercuBail(data, el, id_doc) {
    viderApercuBail();
    var doc_selectionner = document.getElementById("docrowbail-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuBail').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuBail').appendChild(element);
    }
}

function viderApercuBail() {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercudocBail") != undefined) {
        if (document.getElementById("apercudocBail").querySelector("#view_apercu") != null) {
            document.getElementById("apercudocBail").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudocBail").querySelector("#view_apercu").remove();
        }

    }
}

/************Modal Indexation de loyer***********/

$(document).on('click', '#btn_history_bail', function (e) {
    // e.preventDefault();
    // $("#modalContent").html("");
    // Modal.toggle();
    // const bail_id = $("#bail_id").val();
    // getHistorique(0, 1, bail_id, "modalContent");
    // $("#modal_historique_bail").modal('toggle');
});

function getModalIndexationLoyer(cliLot_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id
    };
    var url_request = data_url.getModalIndexationLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadModalIndexationLoyer');
    $('.pace-progress').css('display', 'none');
    $('.pace-progress-inner').css('display', 'none');
}

function loadModalIndexationLoyer(response) {
    $('#contain_modal_indexation_loyer').html(response);
}

/************Modal Historique Bail***********/
function getModalHistoriqueBail(cliLot_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id
    };
    var url_request = data_url.getModalHistoriqueBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadModalHistoriqueBail');
}

function loadModalHistoriqueBail(response) {
    $('#contain_modal_historique_bail').html(response);
}

/************Modal Historique Content Bail***********/
function getModalHistoriqueContentBail(bail_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        bail_id: bail_id
    };
    var url_request = data_url.getModalHistoriqueContentBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadModalHistoriqueContentBail');
}

function loadModalHistoriqueContentBail(response) {
    $('#content-detail-bail-history').html(response);
}

/************Loyers***********/

function getDocLoyers(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var url_request = data_url.listeDocumentLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocLoyer');
}

function loadListeDocLoyer(response) {
    $('#contenair-loyer').html(response);
}

$(document).on('click', '#ajoutDocLoyer', function (e) {
    e.preventDefault();
    ajoutDocLoyer($(this).attr('data-id'), 'add', null);
});

function ajoutDocLoyer(cliLot_id, action, doc_loyer_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_loyer_id': doc_loyer_id,
        'annee': $('#annee_loyer').val()
    };
    var url_request = data_url.formUploadDocLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentLoyer');

    $("#modalAjoutLoyer").modal('show');
}

function chargercontentLoyer(response) {
    $('#UploadLoyer').html(response);
}

function apercuDocumentLoyer(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getPathLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathDocLoyer');
}

function responsepathDocLoyer(response) {
    var el_apercu = document.getElementById('apercu_docLoyer')
    var dataPath = response[0].doc_loyer_path
    var id_document = response[0].doc_loyer_id
    apercuDocLoyer(dataPath, el_apercu, id_document);
}

function apercuDocLoyer(data, el, id_doc) {
    viderApercudocLoyer();
    var doc_selectionner = document.getElementById("docrowloyer-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuLoyer').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuLoyer').appendChild(element);
    }
}

function viderApercudocLoyer() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuLoyer") != undefined) {
        if (document.getElementById("apercuLoyer").querySelector("#view_apercu") != null) {
            document.getElementById("apercuLoyer").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuLoyer").querySelector("#view_apercu").remove();
        }

    }
}

/************Prêts***********/

function getDocPret(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenair-pret"
    }
    var url_request = data_url.listeDocumentPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocPret', loading);
}

function loadListeDocPret(response) {
    $('#contenair-pret').html(response);
}

$(document).on('click', '#ajoutDocPret', function (e) {
    e.preventDefault();
    ajoutDocPret('add');
});

function ajoutDocPret(cliLot_id, action, doc_pret_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_pret_id': doc_pret_id
    };
    var url_request = data_url.formUploadPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontent');

    $("#modalAjout").modal('show');
}

function chargercontent(response) {
    $('#Upload').html(response);
}

function apercuDocumentPret(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getpathPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathPret');
}

function responsepathPret(response) {
    var el_apercu = document.getElementById('apercu_docPret')
    var dataPath = response[0].doc_pret_path
    var id_document = response[0].doc_pret_id
    apercudoc(dataPath, el_apercu, id_document);

}

function apercudoc(data, el, id_doc) {
    viderApercudoc();
    var doc_selectionner = document.getElementById("docrow-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuPret').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuPret').appendChild(element);
    }
}

function viderApercudoc() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuPret") != undefined) {
        if (document.getElementById("apercuPret").querySelector("#view_apercu") != null) {
            document.getElementById("apercuPret").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuPret").querySelector("#view_apercu").remove();
        }

    }
}


/*****supprimer document Pret****/

$(document).on('click', '.btnSuppDocPret', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_pret_id: $(this).attr('data-id')
    };
    delete_docPret(data_request);
});
function delete_docPret(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_docPret;
    var loading = {
        type: "content",
        id_content: "contenair-pret"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocPret', loading);
}
function retourDeleteDocPret(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_pret_id: response.data.id
                    }
                    delete_docPret(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=docrow-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocPret', function (e) {
    e.preventDefault();
    var doc_pret_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_pret_id: doc_pret_id, cliLot_id: cliLot_id };
    var url_request = data_url.formUpdateDocPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentPret', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdatedocPret", "null_fn", param);
});

function retourUpdatedocPret(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocPret(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/*********traité et non traité document Prêt***********/

$(document).on('click', '.btnTraitePret', function () {
    updateTraitementPret($(this).attr('data-id'));
});

function updateTraitementPret(doc_pret_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_pret_id': doc_pret_id,
    };

    var url_request = data_url.updateDocPretTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementPretCallback');
}

function updateTraitementPretCallback(response) {
    getDocPret(response.cliLot_id);
}

/*********Table pret********/

$(document).on('click', '.updateMontant', function (e) {
    e.preventDefault(e);
    var pret_id = $(this).attr('data-id');
    $('#pret_id').val(pret_id);

    var selector = "#rowmontant-" + pret_id + " .input_montant";
    var montant = "#rowmontant-" + pret_id + " .montant";
    var deleteMontant = "#deleteMontant-" + pret_id;
    var updateMontant = "#updateMontant-" + pret_id;
    var annulerMontant = "#annulerMontant-" + pret_id;
    var validerMontant = "#validerMontant-" + pret_id;
    $(selector).show();
    $(montant).hide();
    $(updateMontant).hide();
    $(deleteMontant).hide();
    $(validerMontant).show();
    $(annulerMontant).show();

});

$(document).on('click', '.annulerMontant', function (e) {
    e.preventDefault();
    $(".updateMontant").show();
    $(".deleteMontant").show();
    $(".alertPret").show();
    $(".validerMontant").hide();
    $(".input_montant").hide();
    $(".annulerMontant").hide();
    $(".montant").show();

    // $(this).toggleClass('');

});



/*****Ajouter une année de plus dans le tableau pret******/

$(document).on('click', '.add-annee', function (e) {
    e.preventDefault(e);
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.AddAnneePret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');

});

$(document).on('click', '#annulerAjoutMontant', function () {
    // $(`[id=row-montant`).remove();
    $("#row-montant").toggleClass('d-none');
});

$(document).on('click', '.validerAnnee', function () {
    myModal.toggle();
    var text = $("#anneePret option:selected").val();
    $("#row-montant").toggleClass('d-none');
    $(".pret_annee").val(text);
});

$(document).on('click', '.ajouterAnnee', function () {
    $("#row-montant").toggleClass('d-none');
    var dernierannne = $("tbody tr .aannee").last().text();
    var capital_restant_duN_1 = $("tbody tr .capital_restant").last().attr("data-capital");
    var anneesupp = parseFloat(dernierannne) + 1;
    var capital_restant = parseFloat(capital_restant_duN_1);
    $('#pret_annee').val(anneesupp);
    $("input[name=pret_annee]").attr('disabled', 'disabled');
    $("#pret_capital_restant_prec").val(capital_restant);
    $("input[name=pret_capital_restant_prec]").attr('disabled', 'disabled');
    $(document).on('keyup', '#pret_capital_amorti', function () {
        var capital_rest = parseFloat($('#pret_capital_restant_prec').val());
        var capital_amorti = parseFloat($('#pret_capital_amorti').val());
        $('#pret_capital_restant').val(capital_rest - capital_amorti);
    });
});

$(document).on('keyup', '#pret_capital_amorti', function (e) {
    e.preventDefault();
    $(document).on('keyup', '#pret_interet', function () {
        $(document).on('keyup', '#pret_assurance', function () {
            var input1 = $('#pret_capital_amorti').val();
            var input2 = $('#pret_interet').val();
            var input3 = $('#pret_assurance').val();

            var input1 = parseFloat(input1.replace(/,/g, '.'));
            var input2 = parseFloat(input2.replace(/,/g, '.'));
            var input3 = parseFloat(input3.replace(/,/g, '.'));

            $('#pret_remboursement').val(input1 + input2 + input3);
        });
    });
});

$(document).on('keyup', '#pret_capital_restant_prec', function (e) {
    e.preventDefault();
    $(document).on('keyup', '#pret_capital_amorti', function () {
        var capital_rest = $('#pret_capital_restant_prec').val();
        var capital_amorti = $('#pret_capital_amorti').val();

        var capital_rest = parseFloat(capital_rest.replace(/,/g, '.'));
        var capital_amorti = parseFloat(capital_amorti.replace(/,/g, '.'));
        $('#pret_capital_restant').val(capital_rest - capital_amorti);
    });
});

$(document).on('click', '#validerAjoutMontant', function (e) {
    e.preventDefault(e);
    var pret_annee = $("input[name=pret_annee]").val();
    var cliLot_id = $("input[name=cliLot_id]").val();
    var pret_capital_restant_prec = $("#pret_capital_restant_prec").val();
    var pret_capital_deblocage = $("#pret_capital_deblocage").val();
    var pret_ecart_crd = $("#pret_ecart_crd").val();
    var pret_capital_amorti = $("#pret_capital_amorti").val();
    var pret_interet = $("#pret_interet").val();
    var pret_assurance = $("#pret_assurance").val();
    var pret_remboursement = parseFloat(pret_capital_amorti.replace(/,/g, '.')) + parseFloat(pret_interet.replace(/,/g, '.')) + parseFloat(pret_assurance.replace(/,/g, '.'));
    var pret_capital_restant = parseFloat(pret_capital_restant_prec.replace(/,/g, '.')) - parseFloat(pret_capital_amorti.replace(/,/g, '.'));
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'pret_annee': pret_annee,
        'cliLot_id': cliLot_id,
        'pret_capital_restant_prec': pret_capital_restant_prec,
        'pret_capital_deblocage': pret_capital_deblocage,
        'pret_ecart_crd': pret_ecart_crd,
        'pret_remboursement': pret_remboursement,
        'pret_capital_amorti': pret_capital_amorti,
        'pret_capital_restant': pret_capital_restant,
        'pret_interet': pret_interet,
        'pret_assurance': pret_assurance,
    };
    var loading = {
        type: "content",
        id_content: "contenair-pret"
    }
    var url_request = data_url.AddPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, "retourAddPret", loading);

});

function retourAddPret(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocPret(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.validerMontant', function (e) {
    e.preventDefault(e);
    var pret_id = $(this).attr('data-id');
    var cliLot_id = $("input[name=cliLot_id]").val();
    var pret_capital_restant_prec = $("#pret_capital_restant_prec-" + pret_id).val();
    var pret_capital_deblocage = $("#pret_capital_deblocage-" + pret_id).val();
    var pret_ecart_crd = $("#pret_ecart_crd-" + pret_id).val();
    var pret_remboursement = $("#pret_remboursement-" + pret_id).val();
    var pret_capital_amorti = $("#pret_capital_amorti-" + pret_id).val();
    var pret_capital_restant = $("#pret_capital_restant-" + pret_id).val();
    var pret_interet = $("#pret_interet-" + pret_id).val();
    var pret_assurance = $("#pret_assurance-" + pret_id).val();

    var pret_remboursement = parseFloat(pret_capital_amorti.replace(/,/g, '.')) + parseFloat(pret_interet.replace(/,/g, '.')) + parseFloat(pret_assurance.replace(/,/g, '.'));
    var pret_capital_restant = parseFloat(pret_capital_restant_prec.replace(/,/g, '.')) - parseFloat(pret_capital_amorti.replace(/,/g, '.'));

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'pret_id': pret_id,
        'cliLot_id': cliLot_id,
        'pret_capital_restant_prec': pret_capital_restant_prec,
        'pret_capital_deblocage': pret_capital_deblocage,
        'pret_ecart_crd': pret_ecart_crd,
        'pret_remboursement': pret_remboursement,
        'pret_capital_amorti': pret_capital_amorti,
        'pret_capital_restant': pret_capital_restant,
        'pret_interet': pret_interet,
        'pret_assurance': pret_assurance,
    };
    var loading = {
        type: "content",
        id_content: "contenair-pret"
    }
    var url_request = data_url.UpdatePret;
    fn.ajax_request(type_request, url_request, type_output, data_request, "retourUpdatePret", loading);

});

function retourUpdatePret(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocPret(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

/**************delete montant prêt*************/

$(document).on('click', '.deleteMontant', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        pret_id: $(this).attr('data-id'),
        cliLot_id: $(this).attr('data-lot')
    };
    deletePret(data_request);
});
function deletePret(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deletePret;
    var loading = {
        type: "content",
        id_content: "contenair-pret"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeletePret', loading);
}
function retourDeletePret(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        pret_id: response.data.id,
                        cliLot_id: response.data.cliLot_id,
                    }
                    deletePret(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowmontant-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getDocPret(response.data.cliLot_id)
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/*****alert delete pret****/

$(document).on('click', '.alertPret', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.formAlertDeletePret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

/*****alert update pret****/

$(document).on('click', '.alertUpdate', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = data_url.formAlertUpdatePret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});


/*******Mandat pour un lot********/

function getLotMandat(id, id_cli) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'client_id': id_cli
    };

    var url_request = data_url.listeDocumentMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadListeDocMandat');
}

function loadListeDocMandat(response) {
    $('#contenair-mandat').html(response);
}

$(document).on('click', '.ajoutMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        cliLot_id: $(this).attr('data-id'),
        client_id: $(this).attr('data-client'),
    };
    var url_request = data_url.FormMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});
function charger_contentMandat(response) {
    $("#modal-main-content").html(response);
    $('#type_mandat_id').trigger('change');
    myModal.toggle();
}

$(document).on('click', '.alertMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
    };
    var url_request = data_url.alertMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat_alertMandat');
});
function charger_contentMandat_alertMandat(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('click', '.updatemandatCli', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        cliLot_id: $(this).attr('data-id'),
        client_id: $(this).attr('data-client'),
        mandat_id: $(this).attr('data-mandat'),
    };
    var url_request = data_url.FormMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('change', '#type_mandat_id', function (e) {
    var selected_option = $('#type_mandat_id').val();
    if (selected_option == 1) {
        $("#mandat_montant_ht").val(129);
    } else if (selected_option == 2) {
        $("#mandat_montant_ht").val(39);
    } else {
        $("#mandat_montant_ht").val(295);
    }
});

$(document).on('submit', '#AddMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddMandat", loading);
});

function retourAddMandat(response) {
    if (response.status == true) {
        myModal.toggle();
        getLotMandat(response.data_retour.cliLot_id, response.data_retour.client_id);
        return;
    } else {
        fn.response_control_champ(response);
    }
}

/****envoyer mandat******/

$(document).on('click', '.sendMandatCli', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        mandat_id: $(this).attr('data-id'),
    };
    sendMandatCli(data_request);
});

function sendMandatCli(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.sendMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMandatCallBack', loading);
}
function sendMandatCallBack(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        mandat_id: response.data.id
                    }
                    sendMandatCli(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var cliLot_id = $("#cliLot_id").val();
            var client_id = $("#client_id").val();

            getLotMandat(cliLot_id, client_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/***Signer mandat*****/

$(document).on('click', '.signerMandatCli', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        cliLot_id: $("#cliLot_id").val(),
        client_id: $("#client_id").val()
    };
    var url_request = data_url.SignerMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#AddSignMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#mandat_path').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var cliLot_id = $("#cliLot_id").val();
                    var client_id = $("#client_id").val();

                    getDocBails(cliLot_id);
                    getDocActe(cliLot_id);
                    getLotMandat(cliLot_id, client_id);

                    Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

/********cloturer mandat*********/

$(document).on('click', '.cloturerMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        cliLot_id: $("#cliLot_id").val(),
        client_id: $("#client_id").val()
    };
    var url_request = data_url.cloturerMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#ClotureMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#mandat_fichier_justificatif').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (file == null) {
        $('div').children('.error_file').text("Veuillez renseigner le fichier justificatif");
        //Notiflix.Notify.failure("Veuillez renseigner le fichier justificatif", { position: 'left-bottom', timeout: 3000 });
    }

    if (required_data == true) {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var cliLot_id = $("#cliLot_id").val();
                    var client_id = $("#client_id").val();
                    var bail_id_mandat = $("#bail_id_mandat").val();
                    var pdl_id = $("#pdl_id").val();
                    var bail_loyer_variable = $("#bail_loyer_variable").val();
                    var annee_loyer = $("#annee_loyer").val();

                    getDocBails(cliLot_id);
                    getLotMandat(cliLot_id, client_id);
                    getTableFacture(bail_id_mandat, pdl_id, bail_loyer_variable, annee_loyer, cliLot_id)

                    Notiflix.Notify.success("Mandat clôturé avec succès", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});


/********Retracter mandat*********/

$(document).on('click', '.retracterMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        cliLot_id: $("#cliLot_id").val(),
        client_id: $("#client_id").val()
    };
    var url_request = data_url.retracterMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#RetractMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#mandat_fichier_justificatif').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var mandat_date_signature = new Date($('#mandat_date_signature').val());
    var date_retract = new Date(mandat_date_signature.getTime() + 15 * 24 * 60 * 60 * 1000);
    var mandat_date_demande = new Date($('#mandat_date_demande').val());

    var required_data = true;

    if (file == null) {
        $('div').children('.error_file').text("Veuillez renseigner le fichier justificatif");
        // Notiflix.Notify.failure("Veuillez renseigner le fichier justificatif", { position: 'left-bottom', timeout: 3000 });
    }

    if (required_data == true) {
        if (mandat_date_demande < date_retract) {
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: fd,
                contentType: false,
                processData: false,
                success: function (response) {
                    const obj = JSON.parse(response);
                    if (obj.status == true) {
                        myModal.toggle();
                        var cliLot_id = $("#cliLot_id").val();
                        var client_id = $("#client_id").val();
                        var bail_id_mandat = $("#bail_id_mandat").val();
                        var pdl_id = $("#pdl_id").val();
                        var bail_loyer_variable = $("#bail_loyer_variable").val();
                        var annee_loyer = $("#annee_loyer").val();

                        getDocBails(cliLot_id);
                        getLotMandat(cliLot_id, client_id);
                        getTableFacture(bail_id_mandat, pdl_id, bail_loyer_variable, annee_loyer, cliLot_id)

                        Notiflix.Notify.success("Mandat retracté avec succès", { position: 'left-bottom', timeout: 3000 });
                    }
                }
            });
        } else {
            $('div').children('.error_delai').text("Impossible d'enregistrer la rétractation car le délai des 15 jours est dépassé.");
            //Notiflix.Notify.failure("Impossible d'enregistrer la rétractation car le délai des 15 jours est dépassé.", { position: 'left-bottom', timeout: 3000 });
        }

    }
});

/********Suspendre mandat*********/

$(document).on('click', '.suspensionMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        cliLot_id: $("#cliLot_id").val(),
        client_id: $("#client_id").val()
    };
    var url_request = data_url.suspensionMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#SuspendreMandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "suspendreMandatCallback", loading);
});

function suspendreMandatCallback(response) {
    if (response.status == true) {
        var annee_loyer = $("#annee_loyer").val();
        myModal.toggle();
        getLotMandat(response.cliLot_id, response.client_id);
        getDocBails(response.cliLot_id);
        getTableFacture(response.bail_id, response.pdl_id, response.bail_loyer_variable, annee_loyer, response.cliLot_id)
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.annulersuspensionMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mandat_id: $(this).attr('data-id'),
        cliLot_id: $("#cliLot_id").val(),
        client_id: $("#client_id").val()
    };
    var url_request = data_url.annulersuspensionMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentMandat');
});

$(document).on('submit', '#annulerSuspension_Mandat', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "annulerSuspensionCallback", loading);
});

function annulerSuspensionCallback(response) {
    if (response.status == true) {
        var annee_loyer = $("#annee_loyer").val();
        myModal.toggle();
        getLotMandat(response.cliLot_id, response.client_id);
        getDocBails(response.cliLot_id);
        getTableFacture(response.bail_id, response.pdl_id, response.bail_loyer_variable, annee_loyer, response.cliLot_id)
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#recupMandat', function (e) {
    e.preventDefault();
    ajoutDocMandat($(this).attr('data-id'), $(this).attr('data-client'), 'add', null);
});

function ajoutDocMandat(cliLot_id, client_id, action, doc_mandat_cli_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'client_id': client_id,
        'doc_mandat_cli_id': doc_mandat_cli_id
    };
    var url_request = data_url.UploadMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'Charger_modal');

    $("#modalDoc").modal('show');
}

function Charger_modal(response) {
    $('#contentUpload').html(response);

}

// function apercuDocumentMandat(id_doc) {
//     var type_request = 'POST';
//     var type_output = 'json';
//     var data_request = {
//         'id_doc': id_doc,
//     };
//     var url_request = data_url.getPathMandat;
//     fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathDocMandat');
// }

// function responsepathDocMandat(response) {
//     var el_apercu = document.getElementById('apercu_list_docMandat')
//     var dataPath = response[0].doc_mandat_cli_path
//     var id_document = response[0].doc_mandat_cli_id
//     apercuDocMandat(dataPath, el_apercu, id_document);
// }

function apercuMandat(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getPath_mandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandat');
}

function responsepathMandat(response) {
    var el_apercu = document.getElementById('apercu_list_docMandat')
    var dataPath = response[0].mandat_path
    var id_document = response[0].mandat_id
    apercuDocMandat(dataPath, el_apercu, id_document);
}

function apercuFicJustificatif(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = data_url.getPathFicJustificatif;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandatFic');
}

function responsepathMandatFic(response) {
    var el_apercu = document.getElementById('apercu_list_docMandat')
    var dataPath = response[0].mandat_fichier_justificatif
    var id_document = response[0].mandat_id
    apercuDocMandat(dataPath, el_apercu, id_document);
}

function apercuDocMandat(data, el, id_doc) {
    viderApercuMandat();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuMandat').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuMandat').appendChild(element);
    }
}

function viderApercuMandat() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuMandat") != undefined) {
        if (document.getElementById("apercuMandat").querySelector("#view_apercu") != null) {
            document.getElementById("apercuMandat").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuMandat").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '.deleteMandat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        mandat_cli_id: $(this).attr('data-id')
    };
    deleteMandat(data_request);
});

function deleteMandat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourMandat', loading);
}
function retourMandat(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        mandat_cli_id: response.data.id
                    }
                    deleteMandat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnsuppMandat', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_mandat_cli_id: $(this).attr('data-id')
    };
    delete_docMandat(data_request);
});

function delete_docMandat(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteDocMandat;
    var loading = {
        type: "content",
        id_content: "contenair-mandat"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDocMandat', loading);
}
function returnDocMandat(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_mandat_cli_id: response.data.id
                    }
                    delete_docMandat(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModal(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}

$(document).on('change', '#progm_id', function () {
    var prgm_id = $(this).val();
    if (prgm_id == 309) {
        $('.alertPrgm').addClass('d-none');
        $('.adresse-prgm').addClass('d-none');
        $('.adresse-lot').removeClass('d-none');
    } else {
        getInfoProgramme(prgm_id);
        $('.alertPrgm').removeClass('d-none');
        $('.adresse-prgm').removeClass('d-none');
        $('.adresse-lot').addClass('d-none');
    }
});

function getInfoProgramme(progm_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'progm_id': progm_id,
    };
    var url_request = data_url.getInfoProgramme;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getInfoProgrammeCallback');
}

function getInfoProgrammeCallback(response) {
    $('#progm_adresse1').val(response.data_retour.progm_adresse1);
    $('#progm_adresse2').val(response.data_retour.progm_adresse2);
    $('#progm_adresse3').val(response.data_retour.progm_adresse3);
    $('#progm_cp').val(response.data_retour.progm_cp);
    $('#progm_ville').val(response.data_retour.progm_ville);
    $('#progm_pays').val(response.data_retour.progm_pays);
}

/*********Document lot*********/

$(document).on('click', '#ajoutdocLot', function (e) {
    e.preventDefault();
    ajoutdocLot();
});

function ajoutdocLot() {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {

    };
    var url_request = data_url.formUploadDocLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentLot');

    $("#modalAjoutLot").modal('show');
}

function chargercontentLot(response) {
    $('#UploadLot').html(response);
}

//sort table badamena
$(document).on("click", "table thead tr th:not(.no-sort)", function () {
    var table = $(this).parents("table");
    var rows = $(this).parents("table").find("tbody tr").toArray().sort(TableComparer($(this).index()));
    var dir = ($(this).hasClass("sort-asc")) ? "desc" : "asc";

    if ($(this).hasClass("sort-asc")) {
        $('.tri-clients').empty();
        $('.tri-clients').append('<i class="fas fa-sort-amount-down-alt"></i>');
    }
    else {
        $('.tri-clients').empty();
        $('.tri-clients').append('<i class="fas fa-sort-amount-up-alt"></i>');
    }

    if (dir == "desc") {
        rows = rows.reverse();
    }

    for (var i = 0; i < rows.length; i++) {
        table.append(rows[i]);
    }

    table.find("thead tr th").removeClass("sort-asc").removeClass("sort-desc");
    $(this).removeClass("sort-asc").removeClass("sort-desc").addClass("sort-" + dir);
});

function TableComparer(index) {
    return function (a, b) {
        var val_a = TableCellValue(a, index);
        var val_b = TableCellValue(b, index);
        var result = ($.isNumeric(val_a) && $.isNumeric(val_b)) ? val_a - val_b : val_a.toString().localeCompare(val_b);
        return result;
    }
}

function TableCellValue(row, index) {
    return $(row).children("td").eq(index).text();
}

$(document).on('click', '#removefilecin', function () {
    $('.doc').hide();
    $('#input_cin2').toggleClass('d-none');
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        client_id: $('#client_id').val()
    };
    var url_request = data_url.removefilecin;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilecin');
});

function retour_removefilecin(response) {
    $('#mess_alert_cin').empty();
    $('#mess_alert_cin').append("<div class='row'><div class='col-7'><div class='fs--2 text-danger text-center pt-1'><i>Attention, vous devez renseigné la CNI pour ce prospect.</i></div></div><div class='col-2 text-end'><button class='btn btn-sm btn-secondary fs--2' type='button'  id='get_historique_cni_cli'>Historique</button></div><div class='col-3 text-end'><div class='text-end'><button class='btn btn-sm btn-primary fs--2' type='button' title='Demander CNI' id='submit_cin'>Demander CNI</button></div></div></div>");
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

$(document).on('click', '#removefilecin2', function () {
    $('.doc2').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        client_id: $('#client_id').val()
    };
    var url_request = data_url.removefilecin2;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilecin2');
});

function retour_removefilecin2(response) {
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

$(document).on('change', '#cin', function () {
    $('#input_cin2').toggleClass('d-none');
    $('#mess_alert_cin').empty();
    $('#mess_alert_cin').append("<div class='row'><div class='col'><button class='btn btn-sm btn-secondary fs--2' type='button' id='get_historique_cni_cli'>Historique d'email</button></div></div>");
});


// on change bail date signature
$(document).on('change', '#mandat_date_signature_client', function (e) {
    e.preventDefault();
    const delayTime = 1000;

    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        let timeoutId;
        var type_request = 'POST';
        var type_output = 'json';
        var data_request = {
            cliLot_id: $("#clilot_id_mandat").val(),
            mandat_cli_date_signature: $("#mandat_date_signature_client").val()
        };
        var data_request = CryptoJSAesJson.encrypt(data_request, const_app.code_crypt);
        var url_request = data_url.updateDateSignatureMandatClient;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'updateDateSignatureMandatClientCallback');
    }, delayTime);
});

function updateDateSignatureMandatClientCallback(response) {
    getDocBails(response.data.cliLot_id);
}

$(document).on('click', '#submit_cin', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        email: $('#client_email').val(),
        util_id: $('.util_id').val(),
        client_id: $(this).attr('data-id')
    };
    var url_request = data_url.modalEnvoiMailCNI;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');
});

function validateEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

$(document).on('click', '#sendMailCNI', function () {
    if ($('#destinataire').val() && validateEmail($('#destinataire').val()) && $('#mess_email').val() && validateEmail($('#mess_email').val())) {
        var type_request = 'POST';
        var type_output = 'html';
        var data_request = {
            client_id: $('#client_id').val(),
            email: $('#destinataire').val(),
            util_id: $('.util_id_modal').val(),
            object: $('#object_CNI').val(),
            message_text: $('#message_text').val().replace(/\n/g, "<br/>"),
        };
        var url_request = data_url.demande_cni;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'demande_cni_Callback');
        Modal.toggle();
    }
    else if ($('#destinataire').val() && !(validateEmail($('#destinataire').val()))) {
        Notiflix.Notify.failure("Format du destinataire non valide.", { position: 'left-bottom', timeout: 3000 });
    }
    else if (!$('#mess_email').val() && !(validateEmail($('#mess_email').val()))) {
        Notiflix.Notify.failure("Emeteur vide dans le paramètre modèle de mail.", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Vous devez remplir le champ destinataire", { position: 'left-bottom', timeout: 3000 });
    }
});

function demande_cni_Callback(response) {
    const res = JSON.parse(response);
    if (res.status == 200) {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
        const message = `Email envoyé à ${res.destinataire} ayant comme objet ${res.objet}`;
        historySendMailCNIClient(message, res.client_id, res.history);
    }
    else {
        Notiflix.Notify.failure("Email non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#get_historique_cni_cli', function (e) {
    $("#modalContent").html("");
    Modal.toggle();
    viewHistorySendMail_Cni_cli("modalContent", $('#client_id').val())
});

$(document).on('click', '#get_historique_taxe', function (e) {
    $("#modalContent").html("");
    Modal.toggle();
    viewHistorySendMail_taxe("modalContent", $('#taxe_id').val())
});

/******générer document locataire exploitant*/

$(document).on('click', '.genererDocExploitant', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    genererDocExploitant(data_request);

});

function genererDocExploitant(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.genererDocExploitant;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returndocExploitant', loading);
}

function returndocExploitant(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    genererDocExploitant(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getFicheIdentificationLot(response.data.id, 'fiche');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/******générer document syndic lot*/

$(document).on('click', '.genererDocSyndic', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    genererDocSyndic(data_request);

});

function genererDocSyndic(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.genererDocSyndic;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returndocSyndic', loading);
}

function returndocSyndic(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    genererDocSyndic(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getFicheIdentificationLot(response.data.id, 'fiche');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/******générer document sie lot*/

$(document).on('click', '.genererDocSie', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    genererDocSie(data_request);

});

function genererDocSie(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.genererDocSie;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returndocSie', loading);
}

function returndocSie(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    genererDocSie(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getFicheIdentificationLot(response.data.id, 'fiche');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


/******générer document sip lot*/

$(document).on('click', '.genererDocSip', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    genererDocSip(data_request);

});

function genererDocSip(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.genererDocSip;
    var loading = {
        type: "content",
        id_content: "contenair-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returndocSip', loading);
}

function returndocSip(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    genererDocSip(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getFicheIdentificationLot(response.data.id, 'fiche');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/*******envoi mail au locataire exploitant********/

$(document).on('click', '.envoiExploitant', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $(this).attr('data-lot'),
        doc_lot_id: $(this).attr('data-id')
    };
    var url_request = data_url.EnvoiExploitant;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');
});

/*******envoi mail au syndic********/

$(document).on('click', '.envoiSyndic', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $(this).attr('data-lot'),
        doc_syndic_id: $(this).attr('data-id')
    };
    var url_request = data_url.EnvoiSyndic;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');
});

/*******envoi mail au SIE********/

$(document).on('click', '.envoiSie', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $(this).attr('data-lot'),
        doc_sie_id: $(this).attr('data-id')
    };
    var url_request = data_url.EnvoiSie;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');
});

/*******envoi mail au SIP********/

$(document).on('click', '.envoiSip', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $(this).attr('data-lot'),
        doc_sip_id: $(this).attr('data-id')
    };
    var url_request = data_url.EnvoiSip;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');
});

function alert_Iban() {
    var data_request = {};
    var type_request = 'POST';
    var url_request = data_url.alert_Iban;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_Iban_Callback');
}

function alert_Iban_Callback(response) {
    $("#modal-main-content").html(response);
    myModal.show();
}

$(document).on('change', '#presence_emprunts', function () {
    if ($(this).val() == 2) {
        $('#ajouterAnnee').addClass('d-none');

        if ($('#count_pret').val() == 0) {
            $('#contenu_emprunts_tab').addClass('d-none');
        }
        else {
            $('#contenu_emprunts_tab').removeClass('d-none');
            $('.modif_pret').prop('disabled', true);
            $('.suppr_pret').prop('disabled', true);
        }

    } else {
        $('.modif_pret').prop('disabled', false);
        $('.suppr_pret').prop('disabled', false);
        $('#ajouterAnnee').removeClass('d-none');
        $('#contenu_emprunts_tab').removeClass('d-none');
    }

    addPresencePret($(this).val(), $("#presence_emprunts option:selected").data("id"), $('#commentaire_emprunts').val(), 'modif');
});

$(document).on('keyup', '#commentaire_emprunts', function () {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        $('#presence_emprunts').trigger('change');
    }, 2000);
});

function addPresencePret(presence_emprunts, cliLot_id, commentaire, action = 'view') {
    var data_request = {
        presence_emprunts: presence_emprunts,
        cliLot_id: cliLot_id,
        commentaire: commentaire,
        action: action
    };
    var type_request = 'POST';
    var url_request = data_url.addPresencePret;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addPresencePret_Callback');
}

function addPresencePret_Callback(response) {
    const obj = JSON.parse(response);

    if (obj.util_date_declar != null) {
        var date = convertirFormatDate(obj.util_date_declar);
        $('#date_declaration_of_emprunt').html('<span style="font-style: italic;">' + 'le ' + date + ' par le ' + obj.util_role);
    }
}

function convertirFormatDate(dateInitiale) {
    var dateObj = new Date(dateInitiale);
    var jour = ("0" + dateObj.getDate()).slice(-2);
    var mois = ("0" + (dateObj.getMonth() + 1)).slice(-2);
    var annee = dateObj.getFullYear();

    var dateFormatee = jour + "/" + mois + "/" + annee;

    return dateFormatee;
}




$(document).on('click', '#histo-contact', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        client_id: $(this).attr('data-id')
    };
    var url_request = data_url.historiqueAdresse;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalHistorique');
});

var modalHistorique = new bootstrap.Modal(document.getElementById("myModalXl"), {
    keyboard: false
});

function chargerModalHistorique(response) {
    $("#modalXl").html(response);
    modalHistorique.toggle();
}


/****** Encaissement ******/

var myModalEncaissement = new bootstrap.Modal(document.getElementById("modalForm-Encaissement"), {
    keyboard: false
});

function getEncaissement(id_cli) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id_cli
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.getEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getEncaissementCallBack', loading);
}

function getEncaissementCallBack(response) {
    $('#contenaire-encaissement').html(response);
}

function getListeEncaissement(id, annee) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cli_id': $('#client-encaissementInput').val(),
        'cliLot_id': id,
        'annee': annee
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.getListeEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetListeEncaissement', loading);
}

function retourgetListeEncaissement(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#liste-encaissement-lot_' + identifiant).html(html);
}

/***** ****/

function getListeEncaissement_info(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cli_id': $('#client-encaissementInput').val(),
        'cliLot_id': id,
        'annee': $('#filtre-date').val()
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.getListeEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetListeEncaissement', loading);
}

/***** ****/

function charger_contentModal(response) {
    $("#modal-main-contentEncaissement").html(response);
    myModalEncaissement.toggle();
}

$(document).on('click', '.btn_ajoutEncaissement', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': $(this).attr('data-id'),
        'cli_id': $('#client-encaissementInput').val(),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.get_formAjoutEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_formulaireAjoutEncaissement', loading);
});

function retourn_formulaireAjoutEncaissement(response) {
    $("#modal-main-contentEncaissement").html(response);
    myModalEncaissement.toggle();
}

$(document).on('submit', '#cruEncaissement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }

    var formData = new FormData(form[0]);

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function () {
            loading_fn(loading);
        },
        success: function (response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                getListeEncaissement_info(response.data_retour);
                $('#annee_loyer').trigger('change');
                myModalEncaissement.toggle();
                $("#modal-main-contentEncaissement").html('');
            } else {
                fn.response_control_champ(response);
            }
        },
        complete: function () {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});

$(document).on('click', '.btn_editEncaissement', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': $(this).attr('data-cli_lot_id'),
        'enc_id': $(this).attr('data-id'),
        'cli_id': $('#client-encaissementInput').val(),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.get_formAjoutEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_formulaireAjoutEncaissement', loading);
});


$(document).on('click', '.btn_supprimeEncaissement', function () {
    var data_request = {
        'enc_id': $(this).attr('data-id'),
        'action': 'demande',
        'cliLot_id': $(this).attr('data-cli_lot_id'),
        'cli_id': $('#client-encaissementInput').val(),
    };
    supprimerEnc(data_request);
});

function supprimerEnc(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.deleteCommunications;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_confirmSuppressionEnc', loading);
}

function retourn_confirmSuppressionEnc(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        'action': "confirm",
                        'enc_id': response.data.enc_id,
                        'cliLot_id': response.data.cliLot_id,
                    }
                    supprimerEnc(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.form_validation.mmessage, { position: 'left-bottom', timeout: 3000 });
            $('#annee_loyer').trigger('change');
            getListeEncaissement_info(response.data_retour.cliLot_id);
            //(response.data_retour.cliLot_id, $(`#filtre-date-${response.data_retour.cliLot_id}`).val());
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


$(document).on('change', '.filtre-date-encaissement', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-cliLot_id');
    getListeEncaissement_info(id);
});

function apercuDocEncaissement(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
        'cli_id': $('#client-encaissementInput').val(),
    };
    var url_request = data_url.getPath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathEnc');
}

function responsepathEnc(response) {
    var clilot_id = response[0].clilot_id;
    var el_apercu = document.getElementById('apercu_list_docEnc_' + clilot_id);
    var dataPath = response[0].doc_encaissement_path;
    var id_document = response[0].doc_encaissement_id;
    apercuDocEnc(dataPath, el_apercu, id_document, clilot_id);
}

function apercuDocEnc(data, el, id_doc, cliLot_id) {
    viderApercuEnc(cliLot_id);
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuEnc_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuEnc_' + cliLot_id).appendChild(element);
    }
}

function viderApercuEnc(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuEnc_" + cliLot_id) != undefined) {
        if (document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}

$(document).on('click', '.supprimer_ficher', function () {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': $(this).attr('data-id'),
        'cliLot_id': $(this).attr('data-cli_lot_id'),
        'cli_id': $('#client-encaissementInput').val(),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.supprimerFile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDelete_file', loading);
});

function returnDelete_file(response) {
    if (response.status == 200) {
        $(`.doc_encaissement_${response.data_retour.doc_encaissement_id}`).remove();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('keyup', '#accp_login', function () {
    var inputValue = $(this).val();
    var regex = /^[a-zA-Z0-9._@-]+$/;

    if (!regex.test(inputValue)) {
        $('.help_login').html("L'identifiant n'est pas valide");
    }

});


/****** finSaisi_encais ******/


$(document).on('click', '.valider_clotureEnc', function () {
    var data_request = {
        'annee': $(this).attr('data-annee'),
        'action': 'demande',
        'cliLot_id': $(this).attr('data-cliLot_id'),
    };
    finir_saisi_encais(data_request);
});


function finir_saisi_encais(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.finSaisi_encais;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_confirm_fin_saisiEnc', loading);
}

function retourn_confirm_fin_saisiEnc(response) {

    var modal_finSaisiEncaissement = document.getElementById('modal-finSaisiEncaissement');

    if (response.status == 200) {
        if (response.action == "demande") {
            $('#titre-modal-finSaisiEncaissement').text(response.data.title);
            $('#text-modal-finSaisiEncaissement').text(response.data.text);
            $('#btnConfirm-finSaisiEncaissement').text(response.data.btnConfirm);
            $('#btnAnnuler-finSaisiEncaissement').text(response.data.btnAnnuler);
            $('#confirm-cliLot_id').val(response.data.cliLot_id);
            $(modal_finSaisiEncaissement).modal('show');
        } else {
            $(modal_finSaisiEncaissement).modal('hide');
            Notiflix.Notify.success(response.data.message, { position: 'left-bottom', timeout: 3000 });
            getListeEncaissement_info(response.data.cliLot_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#btnConfirm-finSaisiEncaissement', function () {
    var cliLot_id = $('#confirm-cliLot_id').val();
    var data_request = {
        'annee': $(`#filtre-date`).val(),
        'action': 'confirm',
        'cliLot_id': $('#confirm-cliLot_id').val(),
        'enc_info': $('#confirm-enc_info').val(),
    };
    finir_saisi_encais(data_request);
});

// 

$(document).on('click', '.annuler_clotureEnc', function () {
    var data_request = {
        'action': 'demande',
        'enc_id': $(this).attr('data-enc_id'),
        'cliLot_id': $(this).attr('data-cliLot_id'),
    };
    annuler_clotureEnc(data_request);
});

function annuler_clotureEnc(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = data_url.annuler_clotureEnc;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_confirm_annuler_clotureEnc', loading);
}

function retourn_confirm_annuler_clotureEnc(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        'action': "confirm",
                        'enc_id': response.data.enc_id,
                        'cliLot_id': response.data.cliLot_id,
                    }
                    annuler_clotureEnc(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.data.message, { position: 'left-bottom', timeout: 3000 });
            getListeEncaissement_info(response.data.cliLot_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btn-info-enc', function () {
    var info = $(this).attr('data-info');
    var lang = $(this).attr('data-langVide');
    Notiflix.Confirm.show(
        'information', (($.trim(info) != "") ? info : "Aucune information"), 'Fermer',
        () => { }
    );
});

$(document).on('click', '#Modifier_restant_du', function (e) {
    e.preventDefault(e);
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $(this).attr('data-id')
    };
    var url_request = data_url.Modifier_restant_du;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#updateMontantdu', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';

    fn.ajax_form_data_custom(form, type_output, "retourupdateMontantdu");
});
function retourupdateMontantdu(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocPret(response.data_retour.cliLot_id)
        myModal.toggle();
    } else {
        fn.response_control_champ(response);
    }
}

/**** *****/

$(document).on('change', '.checkbox_check_identite_mandat, .checkbox_check_identite_acte', function (e) {
    e.preventDefault(e);
    var client_id = $(this).attr('data-client_id');
    var isChecked = $(this).prop('checked');

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        client_id: $(this).attr('data-client_id'),
        isChecked : (isChecked) ? 1 : 0,
        content : $(this).attr('data-context'),
    };
    var url_request = data_url.checked_identity;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'response_checkedIdentity');
});

function response_checkedIdentity(response){
    if(response.status){
        if(response.data_retour.content == "acte"){
            $(".charge-page-onglet[data-onglet='acte-acquisition']").trigger("click");
        }

        if(response.data_retour.content == "mandats"){
            $(".charge-page-onglet[data-onglet='mandat']").trigger("click");
        }
    }
}
