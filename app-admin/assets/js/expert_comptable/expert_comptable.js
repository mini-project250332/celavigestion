var comptable_url = {
    // appelle des sous menu facturation
    page_expertComptable: `${const_app.base_url}ExpertComptable/page_expertComptable`,
    // sous menu dossier
    getDossierExpertcomptable: `${const_app.base_url}ExpertComptable/Dossiers/getDossierExpertcomptable`,
    getSuiviDossier : `${const_app.base_url}ExpertComptable/Suivi/getSuiviDossier`,
}

function pgExpertComptable() {
    var type_request = 'POST';
    var url_request = comptable_url.page_expertComptable;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-expertcomptable"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pgExpertComptable_Callback', loading);
}

function pgExpertComptable_Callback(response) {
    $('#contenaire-expertcomptable').html(response);
}

function getDossierExpertcomptable()
{
    var type_request = 'POST';
    var url_request = comptable_url.getDossierExpertcomptable;
    var type_output = 'html';
    var data_request = {};
    // var loading = {
    //     type: "content",
    //     id_content: "contenair-dossiers"
    // }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDossier_Callback');
}

function getDossier_Callback(response) {
    $('#contenair-dossiers').html(response);
}

function getSuiviDossier()
{
    var type_request = 'POST';
    var url_request = comptable_url.getSuiviDossier;
    var type_output = 'html';
    var data_request = {};
   
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getSuivi_Callback');
}

function getSuivi_Callback(response) {
    $('#contenair-suivi').html(response);
}