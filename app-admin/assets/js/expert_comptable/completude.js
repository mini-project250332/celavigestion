var completude_url = {
    getCompletude: `${const_app.base_url}ExpertComptable/Completude/get_completudeTab`,
    getListeCompletude: `${const_app.base_url}ExpertComptable/Completude/get_listeCompletude`,
}

function get_completudeTab() {
    var type_request = 'POST';
    var url_request = completude_url.getCompletude;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-expertcomptable"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pgCompletudeTab_Callback', loading);
}

function pgCompletudeTab_Callback(response) {
    $('#tab-completude').html(response);
}

/***** pagination *****/

$(document).on('click', '.pagination-vue-table', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view').val()}`;
        $('#pagination_view').val(limit);
        getListeCompletude();
    }
});


$(document).on('change', '.row_view', function (e) {
    e.preventDefault();
    getListeCompletude();
});

$(document).on('click', '#pagination-vue-next', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-${array_offset[index_prev]}`).trigger("click");
    }
});


function getListeCompletude(){
    var type_request = 'POST';
    var url_request = completude_url.getListeCompletude;
    var type_output = 'html';

    var data_request = {
        annee : $('#filtre-annees').val(),
        expert : $('#expert-comptable').val(),
    };

    var loading = {
        type: "content",
        id_content: "contenaire-expertcomptable"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'listeCompletude_Callback', loading);
}

function listeCompletude_Callback(response) {
    $('#list-completude').html(response);
}

$(document).on('change', '#filtre-annees', function (e) {
    e.preventDefault();
    getListeCompletude();
});

$(document).on('change', '#expert-comptable', function (e) {
    e.preventDefault();
    getListeCompletude();
});