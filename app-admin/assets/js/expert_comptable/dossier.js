var dossier_url = {
    getContentDossier: `${const_app.base_url}ExpertComptable/Dossiers/getDossier`,
}

function getContentDossier() {
    var type_request = 'POST';
    var url_request = dossier_url.getContentDossier;
    var type_output = 'html';
    var data_request = {
        annee: $('#annee_dossier').val(),
        util_expert_comptable: $('#util_expert_comptable').val()
    };
    var loading = {
        type: "content",
        id_content: "content-dossiers"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getContentDossier_Callback', loading);
}

function getContentDossier_Callback(response) {
    $('#content-dossiers').html(response);
}

$(document).on('click', '.btnFicheDossier', function (e) {
    var dossier_id = $(this).attr('data-id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');
    var href = $('#proprietaire-menu-web').attr('href');
    window.location.href = href + `?_p@roprietaire=${client_id}&_do@t_id=${dossier_id}&_nom=${nom} `;
});

$(document).on('change', '#util_expert_comptable', function (e) {
    getContentDossier();
});

$(document).on('change', '#annee_dossier', function (e) {
    getContentDossier();
});