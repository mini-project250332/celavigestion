var suivi_url = {
    getContentSuivi: `${const_app.base_url}ExpertComptable/Suivi/getSuivi`,
}

function getContentSuivi() {
    var type_request = 'POST';
    var url_request = suivi_url.getContentSuivi;
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "content-suivi"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getContentSuivi_Callback',loading);
}

function getContentSuivi_Callback(response) {
    $('#content-suivi').html(response);
}
