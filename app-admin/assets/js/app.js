var app_url = {
    listGlobal: `${const_app.base_url}Search/listGlobal`,
    liste: `${const_app.base_url}Search/liste`,
    verifyIndexation: `${const_app.base_url}Search/verifyIndexation`,
}

clicked = false;

function forceDownload(href, nom_fichier = null) {
    var link = document.createElement('a');
    link.href = href;
    link.download = href;
    if (nom_fichier == null) {
        var str = link.download.split("/");
        var str_length = str.length;
        link.download = str[str_length - 1];
    }
    else {
        link.download = nom_fichier;
    }
    document.body.appendChild(link);
    link.click();
}

$(document).on('click', '#toggle_dropdownCompte', function (e) {
    var parent = $(this).closest('.icon-profil');
    dropdown_compte = parent.find('.dropdown-compte');
    if (clicked) return false;
    if (dropdown_compte.hasClass('show')) dropdown_compte.removeClass('show');
    else dropdown_compte.addClass('show');
    clicked = true;
    setTimeout(() => { clicked = false; }, 500);
});

$(document).on('click', function () {
    $('.tooltip').removeClass('show');
});

$(document).on('keyup', '#filtre_global', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listGlobal();
    }, 800);
});

$(document).on('keyup', '.filtre_global', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        list();
    }, 800);
});

sessionStorage.setItem("id", $('.contenair-main').attr('id'));
var value = sessionStorage.getItem("id");

function listGlobal() {
    var type_request = 'POST';
    var type_output = 'html';
    var filtre_global = $('#filtre_global').val();
    var data_request = {
        filtre_global: filtre_global,
    };
    var loading = {
        type: "content",
        id_content: value
    }

    var url_request = app_url.listGlobal;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listglobal', loading);
}

function load_listglobal(response) {
    $('#' + value).html(response);
    var filtre = $('#filtre_global').val();
    $('.filtre_global').val(filtre);
    $('#filtre_global').val('');
}

function list() {
    var type_request = 'POST',
        type_output = 'html';
    var filtre = $('#filtre_global').val();
    var filtre_global = filtre;
    $('.filtre_global').val(filtre);
    var data_request = {
        filtre_global: filtre_global,
    };

    var loading = {
        type: "content",
        id_content: value
    }
    var url_request = app_url.liste;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load', loading);
}

function load(response) {
    $('#data-list').html(response);
}

// ****** ACCESS USER RIGHT ****************
// **** in script access_right : remember to add class vue_limit_access for element, btn_limit_access for button
// ***** add class only_visuel_gestion for buttons to hide
// ***** script deactivate_input_gestion() for input to deactivate

$(document).ready(function () {
    only_visuel_gestion();
});

var template_no_access = `
<div class="w-100 p-3">
        <div class="bg-danger text-center p-3 text-white">
                Désolé, vous n'avez pas accès à cette fonction
        </div>
</div>
`;

function access_right(read_right = [], write_right = []) {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = parseInt(user_decrypted.rol_util_valeur);

    //if user is expert comptable (6) all menu with class "no_exper_comptable" will hide and menu with class exper_comptable will be show
    if (access_value == 6) {
        $(`.no_exper_comptable`).hide();
        $(`.exper_comptable`).show();
    } else {
        $(`.exper_comptable`).hide();
    }

    if (access_value == 1 || access_value == 3) {
        $(`.exper_comptable`).show();
    }

    if (read_right == "*") {

    } else if (!read_right.includes(access_value)) {
        $(`.vue_limit_access`).html(template_no_access);
    }

    if (write_right == "*") {

    } else if (!write_right.includes(access_value)) {
        $(`.btn_limit_access`).hide();
    }
}

function only_visuel_gestion(array_allowed = null, classname_target = null) {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = user_decrypted.rol_util_valeur;
    if (array_allowed !== null && Array.isArray(array_allowed) && classname_target !== null) {
        if (!array_allowed.includes(access_value)) loopByInterval(() => { $(`.${classname_target}`).hide(); });
        else loopByInterval(() => { $(`.${classname_target}`).show(); });
    } else {
        if (!const_app.access_write_gestion.includes(access_value)) {
            loopByInterval(() => { $(".only_visuel_gestion").hide(); });
        } else {
            loopByInterval(() => { $(".only_visuel_gestion").show(); });
        }

        // Admin and Gestionnaire de compte
        var access_read = ["1", "3", "4"];

        if (!access_read.includes(access_value)) {
            loopByInterval(() => { $(".not_access_read").hide(); });
        } else {
            loopByInterval(() => { $(".not_access_read").show(); });
        }
    }
}

function acces_comptable_only() {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = user_decrypted.rol_util_valeur;

    if (access_value == 4) {
        $(".item-sidebar").hide();
        $(".access_comptable_true").show();
    }
}

function deactivate_input_gestion() {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = user_decrypted.rol_util_valeur;
    var deactivate = false;

    if (!const_app.access_write_gestion.includes(access_value)) {
        deactivate = true;
        loopByInterval(() => { $("input").prop("disabled", deactivate); });
        loopByInterval(() => { $("select").prop("disabled", deactivate); });
        loopByInterval(() => { $("textarea").prop("disabled", deactivate); });
        loopByInterval(() => { $(".choices").css('pointer-events', 'none'); });
        loopByInterval(() => { $("select.no_effect").prop("disabled", false); });
    } else {
        loopByInterval(() => { $("input:not(.auto_effect)").prop("disabled", deactivate); });
        loopByInterval(() => { $("select:not(.auto_effect)").prop("disabled", deactivate); });
        loopByInterval(() => { $("textarea:not(.auto_effect)").prop("disabled", deactivate); });
        loopByInterval(() => { $(".choices").css('pointer-events', 'auto'); });
    }
}

function loopByInterval(callback, loop_value = 6) {
    var loop = 0;
    var intervalLoop = setInterval(function () {
        callback();
        if (++loop === loop_value) window.clearInterval(intervalLoop);
    }, 500);
}

function allowedAccessRight(access_right_values) {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = parseInt(user_decrypted.rol_util_valeur);
    return access_right_values.includes(access_value)
}

// Verify indexation
function verifyIndexation() {
    var userinfo = localStorage.getItem("userinfo");
    var user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
    var access_value = parseInt(user_decrypted.rol_util_valeur);
    if (access_value != 1) return false; // only admin

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        util_id: parseInt(user_decrypted.util_id),
    };
    var url_request = app_url.verifyIndexation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'verifyIndexationCallback');
}

function verifyIndexationCallback(response) {
    console.log('verifyIndexationCallback ', response);
}