// var userinfo = localStorage.getItem("userinfo");
// user_decrypted = CryptoJSAesJson.decrypt(userinfo, const_app.code_crypt);
// console.log(user_decrypted);

var experts_comptables_url = {
    data_affectationExpertComptable: `${const_app.base_url}ExpertsComptables/data_affectationExpertComptable`,
    affectationExpertComptable : `${const_app.base_url}ExpertsComptables/affectationExpertComptable`,
    form_resilier_missionExpertC : `${const_app.base_url}ExpertsComptables/form_resilier_missionExpertC`,
    form_modif_affectationMission : `${const_app.base_url}ExpertsComptables/form_modif_affectationMission`,
    delete_fileMission : `${const_app.base_url}ExpertsComptables/delete_fileMission`,
}

var timer = null;

var myModal_tableau_assignerExpertComptable = new bootstrap.Modal(document.getElementById("myModal_tableau_assignerExpertComptable"), {
    keyboard: false,
    backdrop: 'static'
});

$(document).on('click', '.btn-data_affectationExpertComptable', function () {
    var data_request = {
        dossier_id: $(this).attr('data-id'),
    };

    data_affectationExpertComptable(data_request);
    myModal_tableau_assignerExpertComptable.toggle();
});

function data_affectationExpertComptable(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var url_request = experts_comptables_url.data_affectationExpertComptable;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_data_affectationExpertComptable');
}
function charger_contentModal_data_affectationExpertComptable(response) {
    $("#modal-main-content-tableau_assignerExpertComptable").html(response);
}

$(document).on('click', '#closeModal_tableau_assignerExpertComptable', function () {
    $fiche_identifiant = $('#fiche-identifiant-dossier');
    getFicheIdentificationDossier($fiche_identifiant.attr('data-client'),'fiche',$fiche_identifiant.attr('data-dossier'));
	myModal_tableau_assignerExpertComptable.hide();

});

/***** ****/

var myModal_assignerExpertComptable = new bootstrap.Modal(document.getElementById("myModal_assignerExpertComptable"), {
    keyboard: false,
    backdrop: 'static'
});

$(document).on('click', '#add-newAffectation-ExpertComptable', function () {
    var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
        action : 'add'
    };

    form_affectationExpertComptable(data_request);

    myModal_tableau_assignerExpertComptable.hide();
    myModal_assignerExpertComptable.toggle();
});

function form_affectationExpertComptable(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var url_request = experts_comptables_url.affectationExpertComptable;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_affectationExpertComptable');
}

function  charger_contentModal_affectationExpertComptable(response) {
	$("#modal-main-content-assignerExpertComptable").html(response);
}

$(document).on('click', '#closeModal_assignerExpertComptable', function () {
	var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
    };

	myModal_assignerExpertComptable.hide();
	data_affectationExpertComptable(data_request);
    myModal_tableau_assignerExpertComptable.toggle();
});


/***** *****/

$(document).on('submit', '#assigner_expertC_mission', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content-assignerExpertComptable"
    }

    //var submitButton = $(e.originalEvent.submitter);

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function() {
            //loading_fn(loading);
        },
        success: function(response) {
            
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });

                var data_request = {
                    dossier_id: response.data_retour,
                };

                myModal_assignerExpertComptable.hide();
                data_affectationExpertComptable(data_request);
                myModal_tableau_assignerExpertComptable.toggle();

            } else {
                if (response && response.form_validation && response.form_validation.message !== undefined) {
                    Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
                fn.response_control_champ(response);
            }
        },
        complete: function() {
            //stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});

/******** ******/

var myModal_resilierMission = new bootstrap.Modal(document.getElementById("myModal_resilierMission"), {
    keyboard: false,
    backdrop: 'static'
});

$(document).on('click', '.btn-resilierMission_expertC', function () {
	var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
        mission_id: $(this).attr('data-mission_id'),
    };
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data_request;
    var url_request = experts_comptables_url.form_resilier_missionExpertC;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_resilierMission_expertC');
    
});

function  charger_contentModal_resilierMission_expertC(response) {
	$("#modal-main-content-resilierMission").html(response);

	myModal_tableau_assignerExpertComptable.hide();
	myModal_resilierMission.toggle();
}

$(document).on('click', '#closeModal_resilier_missionExpertC', function () {
	var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
    };

	myModal_resilierMission.hide();

	data_affectationExpertComptable(data_request);
    myModal_tableau_assignerExpertComptable.toggle();
});

$(document).on('submit', '#resilier_missionExpertC', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content-resilierMission"
    }

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function() {
            loading_fn(loading);
        },
        success: function(response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
				myModal_resilierMission.hide();

				var data_request = {
			        dossier_id: response.data_retour,
			    };
				data_affectationExpertComptable(data_request);
			    myModal_tableau_assignerExpertComptable.toggle();

            } else {
                fn.response_control_champ(response);
            }
        },
        complete: function() {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});

/***** modification mission *****/

var myModal_modif_affectationMission = new bootstrap.Modal(document.getElementById("myModal_modif_affectationMission"), {
    keyboard: false,
    backdrop: 'static'
});

$(document).on('click', '.btn-editMission_expertC', function () {
    var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
        mission_id: $(this).attr('data-mission_id'),
        action: 'update-detail-mission'
    };
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data_request;
    var url_request = experts_comptables_url.form_modif_affectationMission;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_modif_affectationMission');
});

$(document).on('click', '.btn-update-resilierMission_expertC', function () {
    var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
        mission_id: $(this).attr('data-mission_id'),
        action: 'update-resilier-mission'
    };
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data_request;
    var url_request = experts_comptables_url.form_modif_affectationMission;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_modif_affectationMission');
});

function charger_contentModal_modif_affectationMission(response) {
    $("#modal-main-content-modif_affectationMission").html(response);
    myModal_tableau_assignerExpertComptable.hide();
    myModal_modif_affectationMission.toggle();
}

$(document).on('click', '#closeModal_modif_affectationMission', function () {
    var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
    };
    myModal_modif_affectationMission.hide();
    data_affectationExpertComptable(data_request);
    myModal_tableau_assignerExpertComptable.toggle();
});


$(document).on('submit','#saveModify_missionExpertC', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content-modif_affectationMission"
    }

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function() {
            loading_fn(loading);
        },
        success: function(response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                myModal_modif_affectationMission.hide();

                var data_request = {
                    dossier_id: response.data_retour,
                };
                data_affectationExpertComptable(data_request);
                myModal_tableau_assignerExpertComptable.toggle();

            } else {
                if (response && response.form_validation && response.form_validation.message !== undefined) {
                    Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
                fn.response_control_champ(response);
            }
        },
        complete: function() {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});


/***** *****/

$(document).on('click', '.btn-supprimerFileMission', function () {
    var data_request = {
        mission_id : $(this).attr('data-id'),
        fichier : $(this).attr('data-fichier'),
    };
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data_request;
    var url_request = experts_comptables_url.delete_fileMission;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_deleteFileMission');
    
});

function retour_deleteFileMission(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        if(response.data_retour === 1){
            $('#div_affichage_fichier_mission').hide();
            $('#champ_fichier_mission').removeClass('d-none');
        }

        if(response.data_retour === 2){
            $('#div_affichage_fichier_resiliation').hide();
            $('#champ_fichier_resiliation').removeClass('d-none');
        }
    } else {
        if (response && response.form_validation && response.form_validation.message !== undefined) {
            Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        }
    }
}