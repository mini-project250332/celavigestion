var data_url = {
    listLot: `${const_app.base_url}Lots/listeLot`,
    headerLot: `${const_app.base_url}Lots/getheaderlist`,
    listficheLot: `${const_app.base_url}Clients/FicheLot`,
    getLot: `${const_app.base_url}Clients/getFicheLot`,
    delete_lot: `${const_app.base_url}Clients/removeLot`,
    Alerte_cin: `${const_app.base_url}Lots/Alerte_cin_Lot`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function loadScript(url, callback) {
    $.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
    });
}

function pgLots() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-menu-lot"
    }
    var url_request = data_url.headerLot;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pagelisteLot', loading);
}

function charger_pagelisteLot(response) {
    $('#contenair-menu-lot').html(response)
}

$(document).on('change', '#filtre_tom', function (e) {
    e.preventDefault();
    listLot();
});

$(document).on('change', '#annee_tom', function (e) {
    e.preventDefault();
    listLot();
});

$(document).on('change', '#charge_client', function (e) {
    e.preventDefault();
    listLot();
});

$(document).on('change', '#gestionnaire_id', function (e) {
    e.preventDefault();
    listLot();
});

$(document).on('change', '#filtre_lot', function (e) {
    e.preventDefault();
    listLot();
});

function listLot(bail_valid = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var row_view = $('#row_view').val();
    var pagination_view = $('#pagination_view').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 200;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;

    var data_request = {
        filtre_tom: $('#filtre_tom').val(),
        bail_valid: bail_valid,
        annee_tom: $('#annee_tom').val(),
        charge_client: $('#charge_client').val(),
        gestionnaire_id: $('#gestionnaire_id').val(),
        filtre_lot: $('#filtre_lot').val(),
        row_data: $row_data,
        pagination_page: $pagination_page,
    };

    var loading = {
        type: "content",
        id_content: "contenair-menu-lot"
    }
    var url_request = data_url.listLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_list', loading);
}

function load_list(response) {
    $('#data-list_dossier').html(response);
}

/***** pagination *****/
$(document).on('click', '.pagination-vue-table', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view').val()}`;
        $('#pagination_view').val(limit);
        listLot();
    }
});

$(document).on('change', '.row_view', function (e) {
    e.preventDefault();
    listLot();
});

$(document).on('click', '#pagination-vue-next', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul .pagination-vue-table");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('click', '.FicheLot', function (e) {
    var lot_id = $(this).attr('data-id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');
    var href = $('#proprietaire-menu-web').attr('href');
    window.location.href = href + `?_p@roprietaire=${client_id}&_lo@t_id=${lot_id}&_nom=${nom}`;
});

$(document).on('click', '.alert_cin', function () {
    var data_request = {
        'nom': $(this).attr('data-nom')
    }
    var type_request = 'POST';
    var url_request = data_url.Alerte_cin;
    var type_output = 'html';
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getAlerte_cin_LotCallback');
    $("#FormModal").modal('show');
});
function getAlerte_cin_LotCallback(response) {
    $("#modalContent").html(response);
}

function getFicheLot(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-menu-lot"
    }
    var url_request = data_url.getLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheLot', loading);
}

function retourFicheLot(response) {
    $('#contenair-menu-lot').html(response);
}

$(document).on('click', '#btnBackListLot', function (e) {
    $("#contain_list_lots").show();
    $("#contain_detail_lot").hide();
});

/********Supprimer lot******/
$(document).on('click', '.btnConfirmSupLot', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id')
    };
    delete_lot(data_request);
});

function delete_lot(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.delete_lot;
    var loading = {
        type: "content",
        id_content: "contenair-menu-lot"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourdeleteListlot', loading);
}
function retourdeleteListlot(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.id
                    }
                    delete_lot(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowLot-${response.data.id}]`).remove();
            Notiflix.Notify.success("Lot supprimé", { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnFicheLotNewTab', function (e) {
    e.preventDefault();
    const clientId = $(this).attr('data-client_id');
    var lot_id = $(this).attr('data-id');
    var href = $('#client-open-tab-url').val();
    window.open(`${href}?_open@client_tab=${clientId}&_lo@t_id=${lot_id}`, "_blank");


});