var reporting_url = {
    page_reporting: `${const_app.base_url}Reporting/PageReporting`,
    Generatepdf_reporting : `${const_app.base_url}Reporting/Generatepdf_reporting`,
    getContenuReporting : `${const_app.base_url}Reporting/getContenuReporting`,
    getPathReporting : `${const_app.base_url}Reporting/getPathReporting`,
    listeReporting : `${const_app.base_url}Reporting/listeReporting`,
    EnvoiReporting : `${const_app.base_url}SendMail/EnvoiReporting`,

}

function getReporting(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id,
    };
    var url_request = reporting_url.page_reporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_page_reporting');    
}

function load_page_reporting(response) {
    $('#contenair-reporting').html(response);
    var etat_reporting = $('#etat_reporting').val();
    if (etat_reporting == 1) {
        $('#valider_date_reporting').trigger('click');
    } 
}

$(document).on('click', '#valider_date_reporting', function () {
    var etat_reporting = $('#etat_reporting').val();
    if (etat_reporting == 0) {
        getListeReporting($('#dossier_id').val(),'add')
    } else {
        getListeReporting($('#dossier_id').val(),'edit')
    }
    
});

$(document).on('change', '#reporting_annee', function (e) {
    e.preventDefault();
    var dossier_id = $(this).attr('data-id');
    getReportingAnnee(dossier_id);
});

function getListeReporting(dossier_id,action) {    
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action : action,
        annee_reporting : $('#annee_reporting').val(),
        dossier_id :dossier_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-reporting"
    }
    var url_request = reporting_url.getContenuReporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getContenuReportingCallback', loading);
}
    
function getContenuReportingCallback(response) {
    $('.questionnaire_reporting').addClass('d-none');
    $('#contenu-reporting').html(response);
}

function getReportingAnnee(id) {
    var reporting_annee = $('#reporting_annee').val();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id,
        'reporting_annee': reporting_annee,
        'date_reporting' : $('#date_reporting').val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-reporting"
    }
    var url_request = reporting_url.listeReporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlisteReporting', loading);
}

function retourlisteReporting(response) {
    $('#content-reporting').html(response);
}

function forceDownload_reporting(url, fileName) {
    var link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }


$(document).on('click', '.btn-genenerPdf', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id : $(this).attr('data-dossier'),
        reporting_id : $(this).attr('data-id'),
        action : "edit"
    };
    var url_request = reporting_url.Generatepdf_reporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'generate_reporting');
  });

  function generate_reporting(response) { 
    const obj = JSON.parse(response);
    if (obj.status == true) {
        getListeReporting(obj.data_retour.dossier_id,'edit');
    } else {
        fn.response_control_champ(response);
    }
}

/********Aperçu reporting********/

function apercuReporting(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = reporting_url.getPathReporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsePathReporting');
}

function responsePathReporting(response) {
    var el_apercu = document.getElementById('apercu_doc_reporting')
    var dataPath = response[0].reporting_path
    var id_document = response[0].reporting_id
    apercuDocReporting(dataPath, el_apercu, id_document);
}

function apercuDocReporting(data, el, id_doc) {
    viderApercuReporting();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuReporting').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuReporting').appendChild(element);
    }
}

function viderApercuReporting() {
    const doc_selectionne = document.getElementsByClassName("apercu_reporting");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuReporting") != undefined) {
        if (document.getElementById("apercuReporting").querySelector("#view_apercu") != null) {
            document.getElementById("apercuReporting").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuReporting").querySelector("#view_apercu").remove();
        }

    }
}

//envoi reporting

$(document).on('click', '.envoyer-reporting', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        reporting_id : $(this).attr('data-id'),
        dossier_id : $(this).attr('data-dossier')
    };
    var url_request = reporting_url.EnvoiReporting;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModal(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}