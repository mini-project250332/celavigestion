var a_facture_url = {
    filtreFactureValide: `${const_app.base_url}Facturation/A_Facturer/filtreFactureValide`,
    annulerValidation: `${const_app.base_url}Facturation/A_Facturer/annulerValidation`,
    apercu_facture: `${const_app.base_url}Facturation/A_Facturer/apercu_facture`,
    modalvalider: `${const_app.base_url}Facturation/A_Facturer/modalvalider`,
    ValidationFacturer: `${const_app.base_url}Facturation/A_Facturer/ValidationFacturer`,
    facturerDossier: `${const_app.base_url}Facturation/A_Facturer/facturerDossier`,
    ModaltoutSelectionFacturer: `${const_app.base_url}Facturation/A_Facturer/ModaltoutSelectionFacturer`,
    toutSelectionFacturer: `${const_app.base_url}Facturation/A_Facturer/toutSelectionFacturer`,
    listExportPDf: `${const_app.base_url}Facturation/A_Facturer/listExportPDf`,
}

var timer;

function getfiltreFactureValide(annee) {
    var type_request = 'POST';
    var url_request = a_facture_url.filtreFactureValide;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        text_filtre_proprietaire: $('#text_filtre_proprietaire').val(),
        text_filtre_programme: $('#text_filtre_programme').val()
    };
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getfiltreFactureValide_Callback', loading);
}
function getfiltreFactureValide_Callback(response) {
    $('#filtre-facture-valide').html(response);
}

$(document).on('change', '#annee_facture_valide', function () {
    getfiltreFactureValide($('#annee_facture_valide').val());
});

$(document).on('keyup', '#text_filtre_proprietaire', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFactureValide($('#annee_facture_valide').val());
    }, 500);
});

$(document).on('keyup', '#text_filtre_programme', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFactureValide($('#annee_facture_valide').val());
    }, 500);
});

$(document).on('click', '.annuler_validation', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    var tab_lot = [];

    $('.lot_a_facture_' + dossier_id).each(function () {
        tab_lot.push($(this).val());
    });

    var data_request = {
        dossier_id: dossier_id,
        annee: $('#annee_facture_valide').val(),
        tab_lot: tab_lot,
        action: "demande"
    }
    annulerValidation(data_request);
});

function annulerValidation(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = a_facture_url.annulerValidation;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'annulerValidation_callback');
}

function annulerValidation_callback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        annee: response.data.annee,
                        dossier_id: response.data.id,
                        tab_lot: response.data.tab_lot
                    }
                    annulerValidation(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            var rowToDelete = $(".row_dossier_" + response.data.dossier_id);
            if (rowToDelete.length > 0) {
                rowToDelete.remove();
            }
            getfiltrePrepaFacture(response.data.annee);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.failure(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function countDossier() {
    var numberOfCheckedCheckboxes = 0;
    numberOfCheckedCheckboxes = $('.checkbox_facturer:checked').length;
    $('#nbr_dossier').text(numberOfCheckedCheckboxes);

    var totalCount = 0;
    $('.checkbox_facturer').each(function () {
        var nbrelot = parseInt($(this).data('nbrelot')) || 0;
        if ($(this).prop('checked')) {
            totalCount += nbrelot;
        }
    });
    $('#nbr_lot').text(totalCount);

    $('.tout_facturer_pdf').attr('disabled', true)
    // $('.tout_export').attr('disabled', true)
    if (numberOfCheckedCheckboxes > 0 && totalCount > 0) {
        // $('.tout_export').attr('disabled', false)
        $('.tout_facturer_pdf').attr('disabled', false)
    }
}

function countAfficher() {
    var numberOfCheckedCheckboxes = 0;
    var totalCount = 0;

    $('.checkbox_facturer').each(function () {
        numberOfCheckedCheckboxes += 1;
        var nbrelot = parseInt($(this).data('nbrelot')) || 0;
        totalCount += nbrelot;
    });

    $('#nbr_dossier_affiche').text(numberOfCheckedCheckboxes);
    $('#nbr_lot_affiche').text(totalCount);
}

$(document).on('click', '#tout_cocher_facturer', function () {
    var isChecked = $(this).prop('checked');
    $('.checkbox_facturer').prop('checked', isChecked);
    countDossier();
});

$(document).on('click', '.checkbox_facturer', function () {
    $('#tout_cocher_facturer').attr('checked', false)
    countDossier();
});

var myModal = new bootstrap.Modal(document.getElementById("myModalApercufacture"), {
    keyboard: false
});

var myModalValide = new bootstrap.Modal(document.getElementById("myModalValideFacture"), {
    keyboard: false
});

$(document).on('click', '.apercu_facture', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    var annee = $('#annee_facture_valide').val();
    var tab_lot = [];

    $('.lot_a_facture_' + dossier_id).each(function () {
        tab_lot.push($(this).val());
    });
    apercu_facture(dossier_id, annee, tab_lot)
});

function apercu_facture(dossier_id, annee, tab_lot) {
    var type_request = 'POST';
    var url_request = a_facture_url.apercu_facture;
    var type_output = 'html';
    var data_request = {
        dossier_id: dossier_id,
        annee: annee,
        tab_lot: tab_lot
    };
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'apercu_facture_Callback', loading);
}

function apercu_facture_Callback(response) {
    $("#modalMainApercu").html(response);
    myModal.toggle();
}

$(document).on('click', '.facturer_dossier', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    var tab_lot = [];

    $('.lot_a_facture_' + dossier_id).each(function () {
        tab_lot.push($(this).val());
    });

    var data_request = {
        dossier_id: dossier_id,
        annee: $('#annee_facture_valide').val(),
        tab_lot: tab_lot
    }
    modalValiderFacture(data_request);
});

function modalValiderFacture(data_request) {
    var type_request = 'POST';
    var url_request = a_facture_url.modalvalider;
    var type_output = 'html';
    var data_request = data_request;
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modalValiderFacture_Callback', loading);
}
function modalValiderFacture_Callback(response) {
    $("#modalMain").html(response);
    myModalValide.toggle();
}

$(document).on('click', '#confirmer_creation', function () {

    myModalValide.toggle();
    var dossier_id = $('#dossier_id_facture').val();
    var tab_lot = [];

    $('.modal_lot_id_facture').each(function () {
        tab_lot.push($(this).val());
    });

    var data_request = {
        dossier_id: dossier_id,
        annee: $('#annee_facture').val(),
        date_fature: $('#date_facture').val(),
        tab_lot: tab_lot
    }

    ValidationFacturer(data_request);
});

function ValidationFacturer(data) {
    var type_request = 'POST';
    var url_request = a_facture_url.ValidationFacturer;
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ValidationFacturer_Callback', loading);
}

function ValidationFacturer_Callback(response) {
    var obj = JSON.parse(response);

    if (obj.status == 200) {
        var rowToDelete = $("#row_dossier_" + obj.data.dossier_id);
        if (rowToDelete.length > 0) {
            rowToDelete.remove();
        }
        facturerDossier(obj.data.dossier_id, obj.data.annee, obj.data.date_fature, obj.data.tab_lot)

        Notiflix.Notify.success(obj.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        Notiflix.Notify.failure(obj.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function facturerDossier(dossier_id, annee, date_fature, tab_lot) {
    var type_request = 'POST';
    var url_request = a_facture_url.facturerDossier;
    var type_output = 'html';
    var data_request = {
        dossier_id: dossier_id,
        annee: annee,
        date_fature: date_fature,
        tab_lot: tab_lot
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'facturerDossier_Callback');
}

function facturerDossier_Callback(response) {
    const obj = JSON.parse(response);
    var rowToDelete = $(".row_dossier_" + obj.dossier_id);
    if (rowToDelete.length > 0) {
        rowToDelete.remove();
    }
    getFacture();
    getFactureImpaye();
}

$(document).on('keyup', '#date_facture', function () {
    checkdate();
});

$(document).on('change', '#date_facture', function () {
    checkdate();
});

function checkdate(params) {
    var dataDate = $('#date_facture').attr('data-date');
    var inputValue = $('#date_facture').val();

    if (dataDate && inputValue) {
        if (dataDate > inputValue) {
            $('.text_alert').text('La date doit être supérieure ou égale à ' + dataDate);

            $('#confirmer_creation').attr('disabled', true);
            $('#confirmer_creation_tout').attr('disabled', true);
            setTimeout(function () {
                $('.text_alert').text('');
            }, 4000);
        } else {
            $('#confirmer_creation').attr('disabled', false);
            $('#confirmer_creation_tout').attr('disabled', false);
            $('.text_alert').text('');
        }
    }
}

$(document).on('click', '.tout_facturer_pdf', function () {
    var type_request = 'POST';
    var url_request = a_facture_url.ModaltoutSelectionFacturer;
    var type_output = 'html';
    var data_request = {
        annee: $('#annee_facture_valide').val()
    };
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ModaltoutSelectionFacturer_Callback', loading);
});

function ModaltoutSelectionFacturer_Callback(response) {
    $("#modalMain").html(response);
    myModalValide.toggle();
}

$(document).on('click', '#confirmer_creation_tout', function () {
    myModalValide.toggle();

    var date_facture = $('#date_facture').val();
    var annee = $('#annee_facture').val();
    var tab = [];
    $('.checkbox_facturer').each(function () {
        var dossier_tab = []
        if ($(this).prop('checked')) {
            var tab_lot = [];
            var dossier_id = $(this).attr('data-dossier_id');
            $('.lot_a_facture_' + dossier_id).each(function () {
                tab_lot.push($(this).val());
            });
            dossier_tab = { dossier_id: dossier_id, tab_lot: tab_lot }
            tab.push(dossier_tab);
        }
    });

    toutSelectionFacturer(tab, annee, date_facture)
});

function toutSelectionFacturer(data, annee, date_facture) {
    var type_request = 'POST';
    var url_request = a_facture_url.toutSelectionFacturer;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        date_facture: date_facture,
        tab_content: data
    };
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'toutSelectionFacturer_Callback', loading);
}
function toutSelectionFacturer_Callback(response) {
    getfiltreFactureValide($('#annee_facture_valide').val());
    getFacture();
    getFactureImpaye();

}

// $(document).on('click', '.tout_export', function () {
//     var annee = $('#annee_facture_valide').val();
//     var tab = [];
//     $('.checkbox_facturer').each(function () {
//         var dossier_tab = []
//         if ($(this).prop('checked')) {
//             var tab_lot = [];
//             var dossier_id = $(this).attr('data-dossier_id');
//             $('.lot_a_facture_' + dossier_id).each(function () {
//                 tab_lot.push($(this).val());
//             });
//             dossier_tab = { dossier_id: dossier_id, tab_lot: tab_lot }
//             tab.push(dossier_tab);
//         }
//     });

//     listExportPDf(tab, annee);
// });

$(document).on('click', '.tout_export', function () {
    var annee = $('#annee_facture_valide').val();
    listExportPDf(annee);
});

function listExportPDf(annee) {
    var type_request = 'POST';
    var url_request = a_facture_url.listExportPDf;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        text_filtre_proprietaire: $('#text_filtre_proprietaire').val(),
        text_filtre_programme: $('#text_filtre_programme').val()
    };
    var loading = {
        type: "content",
        id_content: "filtre-facture-valide"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'listExportPDf_Callback', loading);
}
function listExportPDf_Callback(response) {
    var href = `${const_app.base_url}` + response;
    forceDownload(href);
}