var prepa_facture_url = {
    filtre_preparation: `${const_app.base_url}Facturation/Preparation_facturation/filtre_preparation`,
    save_facturation: `${const_app.base_url}Facturation/Preparation_facturation/save_facturation`,
    listExportPDf: `${const_app.base_url}Facturation/Preparation_facturation/listExportPDf`,
}

function getfiltrePrepaFacture(annee) {
    var type_request = 'POST';
    var url_request = prepa_facture_url.filtre_preparation;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        text_filtre_proprietaire: $('#preparation_filtre_proprietaire').val(),
        text_filtre_programme: $('#preparation_filtre_programme').val(),
        program_id: +$('#programmeId').val(),
    };
    var loading = {
        type: "content",
        id_content: "filtre-prepa-content"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getfiltrePrepaFacture_Callback', loading);
}
function getfiltrePrepaFacture_Callback(response) {
    $('#filtre-prepa-content').html(response);
}


$(document).on('keyup', '#preparation_filtre_proprietaire', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltrePrepaFacture($('#annee_preparation').val());
    }, 500);
});

$(document).on('change', '#programmeId', function () {
    getfiltrePrepaFacture($('#annee_preparation').val());

});



$(document).on('keyup', '#preparation_filtre_programme', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltrePrepaFacture($('#annee_preparation').val());
    }, 500);
});

$(document).on('change', '#annee_preparation', function () {
    getfiltrePrepaFacture($('#annee_preparation').val());
});





function delaySaveFacturation(numLot, indexation, dossierId) {
    if (typeof delayTimer !== 'undefined') {
        clearTimeout(delayTimer);
    }
    delayTimer = setTimeout(function () {
        save_facturation(numLot, indexation, dossierId);
    }, 1500);
}

function save_facturation(Clilotid, type, dossierId) {
    var annee = $('#annee_preparation').val();
    var element = document.getElementById('tarrifPrecedent_' + Clilotid);
    var tarifText = element.textContent || element.innerText;
    var tarifSansEuro = tarifText.replace('€', '').trim();
    tarifSansEuro = tarifSansEuro.replace(',', '.');
    tarifSansEuro = tarifSansEuro.replace(/\s/g, '');
    if (!/^(-?\d+(\.\d+)?)?$/.test(tarifSansEuro)) {
        Notiflix.Notify.failure('Le champ Tarrif ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }



    var tarifNumerique = parseFloat(tarifSansEuro);
    var indexation = $('#indexation_' + Clilotid).val();
    indexation = indexation.replace(/\s/g, '');
    indexation = indexation.replace(',', '.');


    if (!/^(-?\d+(\.\d+)?)?$/.test(indexation)) {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        var inputElement = $('#indexation_' + Clilotid);
        inputElement.focus();
        return;
    }


    // var idfacturation = $('#idfacturation_' + Clilotid).val();
    var tarrif = $('#tarrif_' + Clilotid).val();
    tarrif = tarrif.replace(/\s/g, '');
    tarrif = tarrif.replace(',', '.');

    if (!/^\d+(\.\d+)?$/.test(tarrif)) {
        Notiflix.Notify.failure('Le champ tarrif ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        var inputElement = $('#tarrif_' + Clilotid);
        inputElement.focus();
        return;
    }


    if (type == 'indexation') {

        if (tarifNumerique != 0) {
            var pourcentageValue = pourcentage(tarifNumerique, indexation);
            tarrif = tarifNumerique + pourcentageValue;
            $('#tarrif_' + Clilotid).val(formatNumber(tarrif));
        }

        else {
            $('#indexation_' + Clilotid).val('');
        }


    }

    if (type == 'tarrif') {
        if (tarifNumerique != 0) {
            indexation = ((tarrif - tarifNumerique) / tarifNumerique) * 100;
            $('#indexation_' + Clilotid).val(formatNumber(indexation));
        }
        else {
            $('#indexation_' + Clilotid).val('');
        }

    }
    var c_facturation_status = 0;
    var validation = false;

    if (type == 'alldossier') {
        c_facturation_status = 1;
        validation = true;

    }

    updateSommeIndexation(dossierId);


    indexation = parseFloat(indexation).toFixed(2);
    tarrif = parseFloat(tarrif).toFixed(2);

    var commentaire = $('#commentaire_' + Clilotid).val();
    var cliLot_id = Clilotid;

    var type_request = 'POST';
    var url_request = prepa_facture_url.save_facturation;
    var type_output = 'json';
    var data_request = {
        annee: annee,
        indexation: indexation,
        tarrif: tarrif,
        commentaire: commentaire,
        cliLot_id: cliLot_id,
        // id_fact_lot: idfacturation,
        c_facturation_status: c_facturation_status,
        validation: validation,
    };

    var loading = {
        type: "content",
        id_content: ""
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'save_facturation_Callback', loading);
}

function save_facturationValidation(Clilotid, type, dossierId) {
    var c_facturation_status = 0;
    var validation = false;
    var annee = $('#annee_preparation').val();
    var element = document.getElementById('tarrifPrecedent_' + Clilotid);
    var tarifText = element.textContent || element.innerText;
    var tarifSansEuro = tarifText.replace('€', '').trim();
    tarifSansEuro = tarifSansEuro.replace(',', '.');
    tarifSansEuro = tarifSansEuro.replace(/\s/g, '');

    if (!/^(-?\d+(\.\d+)?)?$/.test(tarifSansEuro)) {
        Notiflix.Notify.failure('Le champ Tarrif  ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }

    var tarifNumerique = parseFloat(tarifSansEuro);

    if (type != 'indexation_mutiple') {

        var indexation = $('#indexation_' + Clilotid).val();
        indexation = indexation.replace(/\s/g, '');
        indexation = indexation.replace(',', '.');


    } else {
        indexation = $('#indexation_global').val();
        indexation = indexation.replace(/\s/g, '');
        indexation = indexation.replace(',', '.');
        $('#indexation_' + Clilotid).val(indexation);
    }


    if (!/^(-?\d+(\.\d+)?)?$/.test(indexation)) {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }



    var tarrif = $('#tarrif_' + Clilotid).val();
    tarrif = tarrif.replace(/\s/g, '');
    tarrif = tarrif.replace(',', '.');

    if (!/^\d+(\.\d+)?$/.test(tarrif)) {
        Notiflix.Notify.failure('Le champ tarrif ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }


    if (type == 'indexation') {
        if (tarifNumerique != 0) {
            var pourcentageValue = pourcentage(tarifNumerique, indexation);
            tarrif = tarifNumerique + pourcentageValue;
            $('#tarrif_' + Clilotid).val(tarrif.toFixed(2));
        }
    }


    if (type == 'indexation_mutiple') {
        if (tarifNumerique != 0) {
            var pourcentageValue = pourcentage(tarifNumerique, indexation);
            tarrif = tarifNumerique + pourcentageValue;
            $('#tarrif_' + Clilotid).val(tarrif.toFixed(2));
        }

        else {
            $('#indexation_' + Clilotid).val('');
        }
    }

    if (type == 'tarrif') {
        if (tarifNumerique != 0) {
            indexation = ((tarrif - tarifNumerique) / tarifNumerique) * 100;
            $('#indexation_' + Clilotid).val(indexation.toFixed(2));
        }
    }


    if (type == 'alldossier') {
        c_facturation_status = 1;
        validation = true;

    }

    updateSommeIndexation(dossierId);
    indexation = parseFloat(indexation).toFixed(2);
    tarrif = parseFloat(tarrif).toFixed(2);

    var commentaire = $('#commentaire_' + Clilotid).val();
    var cliLot_id = Clilotid;

    var type_request = 'POST';
    var url_request = prepa_facture_url.save_facturation;
    var type_output = 'json';
    var data_request = {
        annee: annee,
        indexation: indexation,
        tarrif: tarrif,
        commentaire: commentaire,
        cliLot_id: cliLot_id,
        c_facturation_status: c_facturation_status,
        validation: validation,
    };

    var loading = {
        type: "content",
        id_content: ""
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'save_facturationValidation_Callback', loading);
}


function pourcentage(indexation, valeur) {
    return (valeur * indexation) / 100;
}


function save_facturation_Callback(response) {
    Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 1000 });
    var total = TotalTarif();
    $('#total_proposition').text(customFormatNumber(total) + ' €');
    $('#total_ht').text(customFormatNumber(total) + ' €');
}

function save_facturationValidation_Callback(response) {

}



async function validerFacturation(button) {
    var dossierId = button.getAttribute('data-dossier-id');
    var lotsRecuperes = recupererLotsParDossierId(dossierId);
    var tarrifTotalDossier = convertirMontantEnNombre();
    var tarrifDossier = TotalTarifDossier(dossierId);
    var tarrifresant = tarrifTotalDossier - tarrifDossier;
    var tarifsSansIdLot = TotalTarifMadantHt();
    var tarrif_mandant = 0;
    var tarrif_Ht = 0;
    var total_tarrifMnadat = tarifsSansIdLot.totalTarrifMandant;
    var total_Ht = tarifsSansIdLot.totalTarrifHT;


    var nombre = lotsRecuperes.length;
    button.innerHTML = 'Chargement...';
    button.disabled = true;
    try {

        for (const lotNumber of lotsRecuperes) {
            await save_facturationValidation(lotNumber, 'alldossier', dossierId);
            var tarifs = TotalTarifMadantHt(lotNumber);
            tarrif_mandant += tarifs.tarrifmandat;
            tarrif_Ht += tarifs.tarrifHT;
        }
        await validerfacturatio(dossierId);
        await getAllFactureValide();

    } catch (error) {
        console.error('Une erreur s\'est produite : ', error);

    } finally {

        var nbtotaldossier = +$('#nbr_dossier_preparation_base').text();
        var nbtotalLot = +$('#nbr_lot_preparation_base').text();
        var restedossier = nbtotaldossier - 1;
        $('#nbr_dossier_preparation_base').text(restedossier);
        var resteLot = nbtotalLot - nombre;
        $('#nbr_lot_preparation_base').text(resteLot);
        $('#nbr_dossier_preparation').text('0');
        $('#nbr_lot_preparation').text('0');
        $('.tout_valider').attr('disabled', true);
        $(".checkbox_preparation").prop("checked", false);
        $("#tout_cocher_valider").prop("checked", false);
        $('#total_proposition').text(customFormatNumber(tarrifresant) + ' €');
        $('#total_ht').text(customFormatNumber(tarrifresant) + ' €');
        var tarrifMandatrestant = total_tarrifMnadat - tarrif_mandant;
        var tarifHtRestant = total_Ht - tarrif_Ht;
        $('#somme_mandatlot').text(customFormatNumber(tarrifMandatrestant) + ' €');
        $('#somme_ht').text(customFormatNumber(tarifHtRestant) + ' €');
        Notiflix.Notify.success('Validation effectuée', { position: 'left-bottom', timeout: 1000 });
    }
}


function updateSommeIndexation(dossierId) {
    sommeIndexation = 0;
    $('.indexation-input[data-dossier-id="' + dossierId + '"]').each(function () {
        var value = $(this).val();
        value = value.replace(/\s/g, '');
        value = value.replace(',', '.');
        var indexationValue = parseFloat(value) || 0;
        sommeIndexation += indexationValue;
    });
    $('#totatlTarif_' + dossierId).text(formatNumber(sommeIndexation) + ' €');


}

function validerfacturatio(dossierId) {

    var elementsToFadeOut = $('.dossier_' + dossierId);
    if (elementsToFadeOut.length > 0) {
        elementsToFadeOut.fadeOut('slow', function () {
            $(this).remove();
        });
    }
}


function indexation_global() {
    indexation = $('#indexation_global').val();
    indexation = indexation.replace(',', '.');

    if (indexation == "") {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }

    if (!/^(-?\d+(\.\d+)?)?$/.test(indexation)) {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        return;
    }



}



function recupererLotsParDossierId(dossierId) {
    var elementsHidden = document.querySelectorAll('.idlot_' + dossierId);
    var lots = [];
    elementsHidden.forEach(function (element) {
        var numLot = element.value;
        lots.push(numLot);
    });

    return lots;

}




$(document).on('click', '#tout_cocher_valider', function () {
    var isChecked = $(this).prop('checked');
    $('.checkbox_preparation:enabled').prop('checked', isChecked);
    countDossierPreparaion();
});

$(document).on('click', '.checkbox_preparation', function () {
    countDossierPreparaion();
})





$(document).on('click', '.tout_valider', async function () {
    try {
        await validerCocheeDossier();
    } catch (error) {
        console.error('Une erreur s\'est produite : ', error);
    } finally {

    }
});


$(document).on('click', '#validation_indexation', async function () {
    try {
        await validerIndexationGlobal();
    } catch (error) {
        console.error('Une erreur s\'est produite : ', error);
    } finally {


    }
});


// $(document).on("click", "table thead tr th:not(.no-sort)", function () {
//     var table = $(this).parents("table");
//     var rows = $(this).parents("table").find("tbody tr").toArray().sort(TableComparer($(this).index()));
//     var dir = ($(this).hasClass("sort-asc")) ? "desc" : "asc";

//     if ($(this).hasClass("sort-asc")) {
//         $('.tri-dossier').empty();
//         $('.tri-dossier').append('<i class="fas fa-sort-amount-down-alt"></i>');
//     }
//     else {
//         $('.tri-dossier').empty();
//         $('.tri-dossier').append('<i class="fas fa-sort-amount-up-alt"></i>');
//     }

//     if (dir == "desc") {
//         rows = rows.reverse();
//     }

//     for (var i = 0; i < rows.length; i++) {
//         table.append(rows[i]);
//     }

//     table.find("thead tr th").removeClass("sort-asc").removeClass("sort-desc");
//     $(this).removeClass("sort-asc").removeClass("sort-desc").addClass("sort-" + dir);
// });

function TableComparer(index) {
    return function (a, b) {
        var val_a = TableCellValue(a, index);
        var val_b = TableCellValue(b, index);
        var result = ($.isNumeric(val_a) && $.isNumeric(val_b)) ? val_a - val_b : val_a.toString().localeCompare(val_b);
        return result;
    }
}

function TableCellValue(row, index) {
    return $(row).children("td").eq(index).text();
}


function countDossierPreparaion() {
    var numberOfCheckedCheckboxes = 0;
    numberOfCheckedCheckboxes = $('.checkbox_preparation:checked').length;
    $('#nbr_dossier_preparation').text(numberOfCheckedCheckboxes);

    var totalCount = 0;
    $('.checkbox_preparation').each(function () {
        var nbrelot = parseInt($(this).data('nbrelot')) || 0;
        if ($(this).prop('checked')) {
            totalCount += nbrelot;
        }
    });
    $('#nbr_lot_preparation').text(totalCount);

    $('.tout_valider').attr('disabled', true);
    $('#indexation_global').attr('disabled', true);
    if (numberOfCheckedCheckboxes > 0 && totalCount > 0) {
        $('.tout_valider').attr('disabled', false);
        $('#indexation_global').attr('disabled', false);
    }
}

async function validerCocheeDossier() {

    var promises = [];
    var tarrifDossier = 0;

    var tarrifTotalDossier = convertirMontantEnNombre();

    var tarifsSansIdLot = TotalTarifMadantHt();
    var tarrif_mandant = 0;
    var tarrif_Ht = 0;
    var total_tarrifMnadat = tarifsSansIdLot.totalTarrifMandant;
    var total_Ht = tarifsSansIdLot.totalTarrifHT;


    $('.checkbox_preparation').each(function () {
        var dossierId = parseInt($(this).data('dossierid')) || 0;
        var lotsRecuperes = recupererLotsParDossierId(dossierId);

        if ($(this).prop('checked')) {
            tarrifDossier += TotalTarifDossier(dossierId);
            var lotsRecuperes = recupererLotsParDossierId(dossierId);
            lotsRecuperes.forEach(function (lotNumber) {
                promises.push(save_facturationValidation(lotNumber, 'alldossier', dossierId));
                var tarifs = TotalTarifMadantHt(lotNumber);
                tarrif_mandant += tarifs.tarrifmandat;
                tarrif_Ht += tarifs.tarrifHT;

            });
            promises.push(validerfacturatio(dossierId));
        }
    });
    await Promise.all(promises);
    affichageTotal();
    getAllFactureValide();
    Notiflix.Notify.success('Validation des dossiers effectuées', { position: 'left-bottom', timeout: 1000 });

    var tarrifresant = tarrifTotalDossier - tarrifDossier;
    var tarrifMandatrestant = total_tarrifMnadat - tarrif_mandant;
    var tarifHtRestant = total_Ht - tarrif_Ht;

    $('#total_proposition').text(customFormatNumber(tarrifresant) + ' €');
    $('#total_ht').text(customFormatNumber(tarrifresant) + ' €');
    $('#somme_mandatlot').text(customFormatNumber(tarrifMandatrestant) + ' €');
    $('#somme_ht').text(customFormatNumber(tarifHtRestant) + ' €');

}

async function validerIndexationGlobal() {
    indexation = $('#indexation_global').val();
    indexation = indexation.replace(',', '.');
    var inputElement = $('#indexation_global');

    var numberOfCheckedCheckboxes = 0;
    numberOfCheckedCheckboxes = $('.checkbox_preparation:checked').length;
    if (numberOfCheckedCheckboxes == 0) {
        Notiflix.Notify.failure('Veuillez sélectionner une ligne', { position: 'center', timeout: 3000 });
        return;
    }

    if (indexation == "") {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        inputElement.focus();
        return;
    }

    if (!/^(-?\d+(\.\d+)?)?$/.test(indexation)) {
        Notiflix.Notify.failure('Le champ indexation ne contient pas un nombre valide', { position: 'center', timeout: 3000 });
        inputElement.focus();
        return;
    }




    var promises = [];
    $('.checkbox_preparation').each(function () {
        var dossierId = parseInt($(this).data('dossierid')) || 0;
        if ($(this).prop('checked')) {
            var lotsRecuperes = recupererLotsParDossierId(dossierId);
            lotsRecuperes.forEach(function (lotNumber) {
                promises.push(save_facturationValidation(lotNumber, 'indexation_mutiple', dossierId));
            });

        }
    });
    await Promise.all(promises);
    Notiflix.Notify.success('Changement indexation effectuée', { position: 'left-bottom', timeout: 1000 });

}


function countAfficherPreparation() {
    var numberOfCheckedCheckboxes = 0;
    var totalCount = 0;

    $('.checkbox_preparation').each(function () {
        numberOfCheckedCheckboxes += 1;
        var nbrelot = parseInt($(this).data('nbrelot')) || 0;
        totalCount += nbrelot;
    });

    $('#nbr_dossier_preparation_base').text(numberOfCheckedCheckboxes);
    $('#nbr_lot_preparation_base').text(totalCount);
}

function affichageTotal() {
    var nbtotaldossier = +$('#nbr_dossier_preparation_base').text();
    var nbtotalLot = +$('#nbr_lot_preparation_base').text();
    var nbrdossierCoche = +$('#nbr_dossier_preparation').text();
    var nbrlotcochE = +$('#nbr_lot_preparation').text();
    var restedossier = nbtotaldossier - nbrdossierCoche;
    $('#nbr_dossier_preparation_base').text(restedossier);
    var resteLot = nbtotalLot - nbrlotcochE;
    $('#nbr_lot_preparation_base').text(resteLot);
    $('#nbr_dossier_preparation').text('0');
    $('#nbr_lot_preparation').text('0');
    $('.tout_valider').attr('disabled', true);
    $('#indexation_global').val('');
    $('#indexation_global').attr('disabled', true);
    $(".checkbox_preparation").prop("checked", false);
    $("#tout_cocher_valider").prop("checked", false);
}



function formatNumber(number, negatif = false) {

    const options = {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    };
    if (Math.abs(number) >= 1000) {
        options.minimumFractionDigits = 2;
        options.maximumFractionDigits = 2;
    }
    const result = new Intl.NumberFormat('fr-FR', options).format(number);
    if (negatif && parseFloat(number) > 0) {
        return "- " + result;
    }
    return result;
}


function tarrif(index, tarrifdefault) {
    var pourcentage = (tarrifdefault * index) / 100;
    var total = tarrifdefault + pourcentage;
    return total;
}


function sortTable(columnIndex, order) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("#votreTableau");
    switching = true;

    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tr");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;

            x = rows[i].getElementsByTagName("td")[columnIndex];
            y = rows[i + 1].getElementsByTagName("td")[columnIndex];

            if (order === 'asc' && x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            } else if (order === 'desc' && x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }

        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function trierColonne(className, columnIndex) {
    var span = document.querySelector('.' + className);
    var order = span.getAttribute('data-order') || 'asc';

    if (order === 'asc') {
        span.innerHTML = '<i class="fas fa-sort-amount-down-alt"></i>';
        sortTable(columnIndex, 'desc');
        span.setAttribute('data-order', 'desc');
    } else {
        span.innerHTML = '<i class="fas fa-sort-amount-up"></i>';
        sortTable(columnIndex, 'asc');
        span.setAttribute('data-order', 'asc');
    }
}

function TotalTarif() {
    var totalTarif = 0;

    $('.tarrifTotal').each(function () {
        var value = $(this).text();
        var montantSansEuroEspace = value.replace(/[€ ]/g, '');
        montantSansEuroEspace = montantSansEuroEspace.replace(/\s/g, '');
        var montant = parseFloat(montantSansEuroEspace.replace(',', '.'));

        totalTarif += montant;
    });

    // totalTarif = customFormatNumber(totalTarif);
    return totalTarif;
}



function TotalTarifDossier(idDossier) {
    var TotalTarifDossier = $('#totatlTarif_' + idDossier).text();
    TotalTarifDossier = TotalTarifDossier.replace(/[€ ]/g, '');
    TotalTarifDossier = TotalTarifDossier.replace(/\s/g, '');
    TotalTarifDossier = parseFloat(TotalTarifDossier.replace(',', '.'));
    return TotalTarifDossier;
}


function TotalTarifMadantHt(idLot = null) {
    var tarrifmandat = $('#tarrifPrecedent_' + (idLot ? idLot : '')).text();
    tarrifmandat = tarrifmandat.replace(/[€ ]/g, '');
    tarrifmandat = tarrifmandat.replace(/\s/g, '');
    tarrifmandat = parseFloat(tarrifmandat.replace(',', '.'));

    var totalTarrifMandant = $('#somme_mandatlot').text();
    totalTarrifMandant = totalTarrifMandant.replace(/[€ ]/g, '');
    totalTarrifMandant = totalTarrifMandant.replace(/\./g, '');
    totalTarrifMandant = parseFloat(totalTarrifMandant.replace(/,/g, '.'));

    var tarrifHT = $('#tarrifPrecedentHt_' + (idLot ? idLot : '')).text();
    tarrifHT = tarrifHT.replace(/[€ ]/g, '');
    tarrifHT = tarrifHT.replace(/\s/g, '');
    tarrifHT = parseFloat(tarrifHT.replace(',', '.'));

    var totalTarrifHT = $('#somme_ht').text();
    totalTarrifHT = totalTarrifHT.replace(/[€ ]/g, '');
    totalTarrifHT = totalTarrifHT.replace(/\./g, '');
    totalTarrifHT = parseFloat(totalTarrifHT.replace(/,/g, '.'));

    return { tarrifmandat: tarrifmandat, tarrifHT: tarrifHT, totalTarrifMandant: totalTarrifMandant, totalTarrifHT: totalTarrifHT };
}







function convertirMontantEnNombre() {
    var montantconvertir = $('#total_proposition').text();
    montantconvertir = montantconvertir.replace(/[€ ]/g, '');
    let cleanedNumber = montantconvertir.replace(/\./g, '');
    cleanedNumber = cleanedNumber.replace(/,/g, '.');
    return parseFloat(cleanedNumber);
}



function customFormatNumber(number) {
    let strNumber = number.toFixed(2).toString();
    let [integerPart, decimalPart] = strNumber.split('.');
    let formattedIntegerPart = '';
    for (let i = integerPart.length - 1, count = 0; i >= 0; i--) {
        formattedIntegerPart = integerPart[i] + formattedIntegerPart;
        count++;
        if (count % 3 === 0 && i !== 0) {
            formattedIntegerPart = '.' + formattedIntegerPart;
        }
    }
    let formattedNumber = formattedIntegerPart + ',' + decimalPart;
    return formattedNumber;
}



/**** export pdf ****/

$(document).on('click', '#exportPdf_apreparer', function () {
    var annee = $('#annee_preparation').val();
    var type_request = 'POST';
    var url_request = prepa_facture_url.listExportPDf;
    var type_output = 'html';
    var data_request = {
        annee: annee,
        text_filtre_proprietaire: $('#preparation_filtre_proprietaire').val(),
        text_filtre_programme: $('#preparation_filtre_programme').val(),
        program_id: +$('#programmeId').val(),
    };
    var loading = {
        type: "content",
        id_content: "filtre-prepa-content"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ExportPDf_apreparer_Callback', loading);
});


function ExportPDf_apreparer_Callback(response) {
    var href = `${const_app.base_url}` + response;
    forceDownload(href);
}














