var facturation_paye = {
    liste_facturation_paye: `${const_app.base_url}Facturation/Facturation_paye/facture_paye`,
    getdetailReglement: `${const_app.base_url}Facturation/Facturation_paye/getdetailReglement`,

}

function getfiltreFacturationPaye(annee) {
    var type_request = 'POST';
    var url_request = facturation_paye.liste_facturation_paye;
    var type_output = 'html';
    var row_view = $('#row_view_facturePaye').val();
    var pagination_view = $('#pagination_view_facturePaye').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 100;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;
    var data_request = {
        annee: annee,
        facture_filtre_proprietaire: $('#facturePaye_filtre_proprietaire').val(),
        num_dossier_facture: $('#num_dossier_facturePaye').val(),
        num_facture: $('#facturePaye_filtre_facture').val(),
        regleId: $('#regleIdPaye').val(),
        rumId: $('#num_rum_facturePaye').val(),
        row_data: $row_data,
        pagination_page: $pagination_page,
    };
    var loading = {
        type: "content",
        id_content: "filtre-paye-content"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getfiltreFacturationPaye_Callback', loading);
}

function getfiltreFacturationPaye_Callback(response) {
    $('#filtre-paye-content').html(response);
}

$(document).on('change', '#annee_facturationPaye', function () {
    getfiltreFacturationPaye($(this).val())
});

$(document).on('change', '#regleIdPaye', function () {
    getfiltreFacturationPaye($('#annee_facturationPaye').val());
});

$(document).on('keyup', '#num_dossier_facturePaye', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationPaye($('#annee_facturationPaye').val());
    }, 500);
});

$(document).on('keyup', '#facturePaye_filtre_proprietaire', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationPaye($('#annee_facturationPaye').val());
    }, 500);
});



$(document).on('keyup', '#facturePaye_filtre_facture', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationPaye($('#annee_facturationPaye').val());
    }, 500);
});

$(document).on('keyup', '#num_rum_facturePaye', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationPaye($('#annee_facturationPaye').val());
    }, 500);
});

/***** pagination *****/

$(document).on('click', '.pagination-vue-table-facturePaye', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view_facturePaye').val()}`;
        $('#pagination_view_facturePaye').val(limit);
        getfiltreFacturationPaye($('#annee_facturationPaye').val());
    }
});


$(document).on('change', '.row_view_facturePaye', function (e) {
    e.preventDefault();
    getfiltreFacturationPaye($('#annee_facturationPaye').val());
});


var myModaldetailsReglement = new bootstrap.Modal(document.getElementById("myModaldetailsReglement"), {
    keyboard: false
});

$(document).on('click', '#pagination-vue-next-factureiPaye', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-facturePaye .pagination-vue-table-facturePaye");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-facturePaye-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview-FacturePaye', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-facturePaye .pagination-vue-table-facturePaye");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-facturePaye-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('click', '.detailreglement', function () {
    var ventil_id = $(this).attr('data-reglementId');
    detailsreglement(ventil_id);
});

function detailsreglement(ventil_id) {
    var type_request = 'POST';
    var url_request = facturation_paye.getdetailReglement;
    var type_output = 'html';
    var data_request = { ventil_id: ventil_id }

    var loading = {
        type: "content",
        id_content: "filtre-paye-content"
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'detailsreglement_Callback', loading);
}

function detailsreglement_Callback(response) {
    $("#details-reglement").html(response);
    myModaldetailsReglement.toggle();
}