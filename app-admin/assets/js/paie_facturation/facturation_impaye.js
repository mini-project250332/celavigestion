var facturation_impaye = {
    liste_facturation_impaye: `${const_app.base_url}Facturation/Facturation_impaye/facture_impaye`,
    pagePrelevement: `${const_app.base_url}Facturation/Facturation_impaye/pagePrelevement`,
    saveprelevement: `${const_app.base_url}Facturation/Facturation_impaye/SavePrelevement`,
    pageSepa: `${const_app.base_url}Facturation/Facturation_impaye/pagedataxml`,
    modifierModeReglement: `${const_app.base_url}Facturation/Facturation_impaye/modifierModeReglement`,
    saveChangementRegle: `${const_app.base_url}Facturation/Facturation_impaye/saveChangementRegle`,
    export_xml: `${const_app.base_url}Facturation/Facturation_impaye/exportXml`,
    ValiderFactureAvoir: `${const_app.base_url}Facturation/Facturation_impaye/verifierFacture_avoir`,
    SaveFactureAvoir: `${const_app.base_url}Facturation/Facturation_impaye/save_FactureAvoir`,
}

function getfiltreFacturationImpaye(annee) {
    var type_request = 'POST';
    var url_request = facturation_impaye.liste_facturation_impaye;
    var type_output = 'html';
    var row_view = $('#row_view_factureimpaye').val();
    var pagination_view = $('#pagination_view_factureimpaye').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 100;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;
    var data_request = {
        annee: annee,
        facture_filtre_proprietaire: $('#factureImpaye_filtre_proprietaire').val(),
        facture_filtre_progrm: $('#factureImpaye_filtre_progrm').val(),
        num_dossier_facture: $('#num_dossier_factureImpaye').val(),
        num_facture: $('#factureImpaye_filtre_facture').val(),
        regleId: $('#regleId').val(),
        rumId: $('#num_rum_factureImpaye').val(),
        row_data: $row_data,
        pagination_page: $pagination_page,
    };
    var loading = {
        type: "content",
        id_content: "filtre-impaye-content"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getfiltreFacturationImpaye_Callback', loading);
}

function getfiltreFacturationImpaye_Callback(response) {
    $('#filtre-impaye-content').html(response);
}

/***** pagination *****/

$(document).on('click', '.pagination-vue-table-factureImpaye', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view_factureimpaye').val()}`;
        $('#pagination_view_factureimpaye').val(limit);
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }
});


$(document).on('change', '.row_view_factureimpaye', function (e) {
    e.preventDefault();
    getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
});


$(document).on('click', '#pagination-vue-next-factureimpaye', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-factureimpaye .pagination-vue-table-factureImpaye");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-factureImpaye-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview-Factureimpaye', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-factureimpaye .pagination-vue-table-factureImpaye");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-factureImpaye-${array_offset[index_prev]}`).trigger("click");
    }
});

$(document).on('change', '#annee_facturationImpaye', function () {
    getfiltreFacturationImpaye($(this).val())
});

$(document).on('change', '#regleId', function () {
    getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
});

$(document).on('keyup', '#num_dossier_factureImpaye', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }, 500);
});

$(document).on('keyup', '#factureImpaye_filtre_proprietaire', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }, 500);
});

$(document).on('keyup', '#factureImpaye_filtre_progrm', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }, 500);
});

$(document).on('keyup', '#factureImpaye_filtre_facture', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }, 500);
});

$(document).on('keyup', '#num_rum_factureImpaye', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    }, 500);
});


$(document).on('click', '#enregistrer_prelevement', function () {
    var table_fact_id = [];
    $('.facture_impaye').each(function () {
        if ($(this).prop('checked')) {
            table_fact_id.push($(this).attr('data-fact_id'));
        }
    });
    pagePrelevement(table_fact_id);
});

function pagePrelevement(data) {
    var type_request = 'POST';
    var url_request = facturation_impaye.pagePrelevement;
    var type_output = 'html';
    var data_request = { table_num_facture: data };
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pagePrelevement_Callback', loading);
}

function pagePrelevement_Callback(response) {
    $('#contenair-facture-impaye').html(response);
}

$(document).on('click', '#annulerPrelevement', function () {
    getFactureImpaye();
});

$(document).on('click', '#retourFactureImpaye', function () {
    getFactureImpaye();
});

$(document).on('click', '#tous_cocher_impaye', function () {
    var isChecked = $(this).prop('checked');
    $('.facture_impaye:enabled').prop('checked', isChecked);
    cocher_impayer();
});

$(document).on('click', '.facture_impaye', function () {
    $('#tous_cocher_impaye').attr('checked', false);
    cocher_impayer();
});

$(document).on('click', '#Saveprelevement', function () {
    var facture = recuperationDossierIDFactureID();
    savePrelevementFacture(facture);

});

function cocher_impayer() {
    var numb_cocher = 0;
    numb_cocher = $('.facture_impaye:checked').length;
    $('#enregistrer_prelevement').attr('disabled', true);
    if (numb_cocher > 0) {
        $('#enregistrer_prelevement').attr('disabled', false);
    }
}

function recuperationDossierIDFactureID() {
    var factures = [];
    $('.lignefacture:not(tfoot tr)').each(function () {
        var fact_id = parseInt($(this).data('facture_id')) || 0;
        var facture = {
            "fact_id": fact_id,
            "dossier_id": $("#dosssier_" + fact_id).text(),
            "montant_preleve": convertirnombre($('#montantreglement_' + fact_id).val())
        };

        factures.push(facture);
    });

    var factJson = JSON.stringify(factures);
    return factJson;
}


$(document).on('change', '#dateprelevement', function () {
    var defaultDate = new Date(getDefaultDate());
    var selectedDate = new Date($(this).val());
    if (selectedDate.getTime() < defaultDate.getTime() || selectedDate.getDay() === 0 || selectedDate.getDay() === 6) {
        $('#dateError').text('Erreur : Veuillez choisir une date ultérieure à la date par défaut et qui n\'est pas un week-end.');
        $("#Saveprelevement").prop("disabled", true);
    } else {
        $('#dateError').text('');
        $("#Saveprelevement").prop("disabled", false);
    }
});

function getDefaultDate() {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() + 2);
    while (currentDate.getDay() === 0 || currentDate.getDay() === 6) {
        currentDate.setDate(currentDate.getDate() + 1);
    }
    var formattedDate = currentDate.toISOString().split('T')[0];
    return formattedDate;
}

function savePrelevementFacture(data) {

    var datePrelevement = $('#dateprelevement').val();
    var banqueId = $('#banque_celavi').val();
    if (!datePrelevement || !banqueId || !data) {
        Notiflix.Notify.failure('Veuillez remplir tous les champs obligatoires.', { position: 'center', timeout: 3000 });
        return;
    }
    // Envoi des données au serveur
    var type_request = 'POST';
    var url_request = facturation_impaye.saveprelevement;
    var type_output = 'json';
    var data_request = {
        dateprelevement: datePrelevement,
        banqueId: banqueId,
        factures: data
    };

    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'savePrelevement_Callback', loading);
}



function savePrelevement_Callback(response) {
    getFacturePaye();
    pagedataxml(response);
}

function pagedataxml(data) {
    var type_request = 'POST';
    var url_request = facturation_impaye.pageSepa;
    var type_output = 'html';
    var data_request = { table_num_facture: data };
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pagedataxml_Callback', loading);
}

function pagedataxml_Callback(response) {
    $('#contenair-facture-impaye').html(response);
}

function verifierValeur(input) {
    var enteredValue = input.value;
    enteredValue = enteredValue.replace(/\s/g, '');
    enteredValue = enteredValue.replace(',', '.');
    var idfact = input.id.split('_')[1];
    var inputElement = $('#montantreglement_' + idfact);
    if (!/^(-?\d+(\.\d+)?)?$/.test(enteredValue)) {
        inputElement.css('border', '1px solid red');
        inputElement.focus();
        return;
    }

    inputElement.css('border', '');
    TotalmontantPreleve()

}

function convertirnombre(nombre) {
    console.log(nombre);
    nombre = nombre.replace(/\s/g, '');
    nombre = nombre.replace(',', '.');
    if (!/^(-?\d+(\.\d+)?)?$/.test(nombre)) {
        return;
    }

    return nombre;

}

function TotalmontantPreleve() {
    var total = 0;
    $('.montantAPrelever').each(function () {
        var montantPreleve = $(this).val();
        montantPreleve = montantPreleve.replace(/\s/g, '');
        montantPreleve = montantPreleve.replace(',', '.');
        var montantNumerique = parseFloat(montantPreleve);

        if (!isNaN(montantNumerique)) {
            total += montantNumerique;
        }
    });

    total = customFormatNumber(total);

    $("#montantTotal_preleve").text(total + ' €')
}

function customFormatNumber(number) {
    let strNumber = number.toFixed(2).toString();
    let [integerPart, decimalPart] = strNumber.split('.');
    let formattedIntegerPart = '';
    for (let i = integerPart.length - 1, count = 0; i >= 0; i--) {
        formattedIntegerPart = integerPart[i] + formattedIntegerPart;
        count++;
        if (count % 3 === 0 && i !== 0) {
            formattedIntegerPart = '.' + formattedIntegerPart;
        }
    }
    let formattedNumber = formattedIntegerPart + ',' + decimalPart;
    return formattedNumber;
}
var myModalmodereglement = new bootstrap.Modal(document.getElementById("myModalmodereglement"), {
    keyboard: false
});

$(document).on('click', '.modifier_reglement', function () {
    var fact_id = $(this).attr('data-fact_id');
    modifierModeReglement(fact_id);
});

function modifierModeReglement(fact_id) {
    var type_request = 'POST';
    var url_request = facturation_impaye.modifierModeReglement;
    var type_output = 'html';
    var data_request = { fact_id: fact_id };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modifierModeReglement_Callback');
}

function modifierModeReglement_Callback(response) {
    $("#modalMainRegle").html(response);
    myModalmodereglement.toggle();
}

$(document).on('click', '#confirmerChangementregl', function () {
    var fact_id = $('#reglement_fact_id').val();
    var mode_paiement = $('#nouveau_mode_reglement').val();
    var commentaire = $('#nouveau_commentaire').val();

    var data_request = {
        fact_id: fact_id,
        mode_paiement: mode_paiement,
        commentaire: commentaire
    }

    saveChangementRegle(data_request);
});

function saveChangementRegle(data) {
    var type_request = 'POST';
    var url_request = facturation_impaye.saveChangementRegle;
    var type_output = 'html';
    var data_request = data
    fn.ajax_request(type_request, url_request, type_output, data_request, 'saveChangementRegle_Callback');
}

function saveChangementRegle_Callback(response) {
    var obj = JSON.parse(response);
    if (obj.status == 200) {
        myModalmodereglement.toggle();
        Notiflix.Notify.success(obj.message, { position: 'left-bottom', timeout: 3000 });
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
    } else {
        Notiflix.Notify.failure(obj.message, { position: 'left-bottom', timeout: 3000 });
    }
}

//Export xml
$(document).on('click', '#export_xml', function () {
    var reglements = [];
    $('.lignePrelevement').each(function () {
        reglements.push($(this).attr('data-reglement_id'));
    });
    exportXml(reglements);
});

function exportXml(data) {
    var idprelevement = $('.idprelevemnt').val();
    var type_request = 'POST';
    var type_output = 'html';

    var data_request = {
        idprelevement: idprelevement,
        Idreglement: data,
    };
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }

    var url_request = facturation_impaye.export_xml;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'export_xmlCallback', loading);

}

function export_xmlCallback(response) {
    getFichierSepa();
    $("#export_xml").prop("disabled", true);
    $("#retourFactureImpaye").prop("disabled", false);
    var parts = response.split('/');
    forceDownload_xml(`${const_app.base_url}` + response, parts[4]);
}

function forceDownload_xml(url, fileName) {
    var link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

var myModalValideFactureAvoir = new bootstrap.Modal(document.getElementById("myModalValiderFactureAvoir"), {
    keyboard: false
});

$(document).on('click', '.creerAvoir', function () {

    var idfacture = $(this).attr('data-fact_id');
    var data_request = {
        idfacture: idfacture,
        annee: $('#annee_facturationImpaye').val(),
    }
    modalValiderFactureAvoir(data_request);
});

function modalValiderFactureAvoir(data_request) {
    var type_request = 'POST';
    var url_request = facturation_impaye.ValiderFactureAvoir;
    var type_output = 'html';
    var data_request = data_request;
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modalValiderFactureAvoir_Callback');
}
function modalValiderFactureAvoir_Callback(response) {
    $("#modalMainFactureAvoir").html(response);
    myModalValideFactureAvoir.toggle();
}

$(document).on('click', '#confirmer_creationAvoir', function () {


    var justificationValue = $('#justificationAvoir').val();

    if (justificationValue == '') {
        // Afficher un message d'erreur
        $('#justificationError').text('Veuillez fournir une justification.');
        return; // Arrêter le traitement ultérieur
    }

    // Si la justification n'est pas vide, effacer le message d'erreur (s'il y en a un) et poursuivre avec data_request
    $('#justificationError').text('');

    var data_request = {
        idfacture: $('#numFactAvoir').val(),
        annee: $('#annee_facturationImpaye').val(),
        dateFactureAvoir: $('#date_factureAvoir').val(),
        facture_justification: $('#justificationAvoir').val(),

    }
    myModalValideFactureAvoir.toggle();
    ValidationFacturerAvoir(data_request);
});

function ValidationFacturerAvoir(data) {
    var type_request = 'POST';
    var url_request = facturation_impaye.SaveFactureAvoir;
    var type_output = 'json';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-facture-impaye"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ValidationFacturerAvoir_Callback');
}

function ValidationFacturerAvoir_Callback(response) {


    // Notiflix.Notify.success("ok", { position: 'left-bottom', timeout: 3000 });
    var obj = response;
    if (obj.status == 200) {
        Notiflix.Notify.success(obj.message, { position: 'left-bottom', timeout: 3000 });
        getfiltreFacturationPaye($('#annee_facturationImpaye').val());
        getfiltreFacturationImpaye($('#annee_facturationImpaye').val());
        getTableFacture($('#annee_facturationImpaye').val());
        getPreparationFacture();
    } else {
        Notiflix.Notify.failure(obj.message, { position: 'left-bottom', timeout: 3000 });
    }


}