var mandat_url = {
    headerMandat: `${const_app.base_url}Mandats/getheaderlist`,
    listMandat: `${const_app.base_url}Mandats/listMandat`,
    export_Mandat: `${const_app.base_url}Mandats/export_Mandat`
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

function pgMandats() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-menu-mandat"
    }
    var url_request = mandat_url.headerMandat;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pagelisteMandatCallBack', loading);
}

function pagelisteMandatCallBack(response) {
    $('#contenair-menu-mandat').html(response)
}

$(document).on('change', '#etat_mandat_id', function (e) {
    e.preventDefault();
    listMandat();
});

$(document).on('keyup', '#numero_mandat', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listMandat();
    }, 400);
});


function listMandat() {
    var type_request = 'POST';
    var type_output = 'html';
    var text_filtre_mandat = $('#text_filtre_mandat').val();
    var etat_mandat_id = $('#etat_mandat_id').val();
    var numero_mandat = $('#numero_mandat').val();
    var nom_programme = $('#nom_programme').val();
    var data_request = {
        text_filtre_mandat: text_filtre_mandat,
        etat_mandat_id: etat_mandat_id,
        numero_mandat: numero_mandat,
    };

    var loading = {
        type: "content",
        id_content: "contenair-menu-mandat"
    }
    var url_request = mandat_url.listMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadlistMandat', loading);
}

function loadlistMandat(response) {
    $('#data-list').html(response);
}

$(document).on('click', '.ficheMandat', function (e) {
    var lot_id = $(this).attr('data-id');
    var type_mandat = $(this).attr('data-type');
    var client_id = $(this).attr('data-proprietaire');
    if (type_mandat == 1) {
        var href = $('#proprietaire-menu-web').attr('href');
        window.location.href = href + `?_p@roprietaire=${client_id}&_lo@t_id=${lot_id}`;
    } else {
        var href = $('#proprietaire-menu-prospect').attr('href');
        window.location.href = href + `?_p@rospect=${client_id}&_lo@t_id=${lot_id}`;
    }

});


$(document).on('click', '.btnFicheMandatNewTab', function (e) {
    e.preventDefault();
    const clientId = $(this).attr('data-proprietaire');
    var type_mandat = $(this).attr('data-type');
    var lot_id = $(this).attr('data-id');
    if (type_mandat == 1) {
        var href = $('#client-open-tab-url').val();
        window.open(`${href}?_open@client_tab=${clientId}&_lo@t_id=${lot_id}`, "_blank");
    } else {
        var href = $('#prospect-open-tab-url').val();
        window.open(`${href}?_open@prospect_tab=${clientId}&_lo@t_id=${lot_id}`, "_blank");
    }

});

function forceDownload_mandat(url, fileName) {
    var link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

$(document).on('click', '#export_pdf_mandat', function () {
    var type_request = 'POST';
    var type_output = 'html';

    var data_request = {

    };

    var loading = {
        type: "content",
        id_content: "contenair-menu-mandat"
    }

    var url_request = mandat_url.export_Mandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'exporter_Mandat', loading);
});

function exporter_Mandat(response) {
    var parts = response.split('/');
    forceDownload_mandat(`${const_app.base_url}` + response, parts[3]);
}