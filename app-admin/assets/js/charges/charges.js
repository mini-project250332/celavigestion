var charges_url = {
    getMenuCharges: `${const_app.base_url}Charges/getMenuCharges`,
    getListeDocumentCharges: `${const_app.base_url}Charges/getListeDocumentCharges`,
    getListCharge: `${const_app.base_url}Charges/getListCharge`,
    addCharge: `${const_app.base_url}Charges/addCharge`,
    deleteCharges: `${const_app.base_url}Charges/deleteCharges`,
    getUpdateCharge: `${const_app.base_url}Charges/getUpdateCharge`,
    formUploadDocCharge: `${const_app.base_url}Charges/formUploadDocCharge`,
    getpathDoccharge: `${const_app.base_url}Charges/getpathDoccharge`,
    deleteDocCharge: `${const_app.base_url}Charges/deleteDocCharge`,
    modalupdateDocCharge: `${const_app.base_url}Charges/modalupdateDocCharge`,
    updateDocChargeTraitement: `${const_app.base_url}Charges/updateDocChargeTraitement`,
    validerDepense: `${const_app.base_url}Charges/validerDepense`,
    modaljoinCharge: `${const_app.base_url}Charges/modaljoinCharge`,
    annulersaisie: `${const_app.base_url}Charges/annulersaisie`,
    addDocSeulCharge: `${const_app.base_url}Charges/addDocSeulCharge`,
    removefile: `${const_app.base_url}Charges/removefile`,

}

function getMenuCharges(cliLot_id) {
    var data_request = {
        'cliLot_id': cliLot_id,
    }
    var type_request = 'POST';
    var url_request = charges_url.getMenuCharges;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-charges"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getMenuChargesCallback', loading);
}

function getMenuChargesCallback(response) {
    $('#contenair-charges').html(response);
}

$(document).on('change', '#annee_charges', function () {
    getListDocumentCharge($(this).val(), $('#cliLot_id_charge').val());
})

function getListDocumentCharge(annee, cliLot_id) {
    var data_request = {
        'annee': annee,
        'cliLot_id': cliLot_id,
    }
    var type_request = 'POST';
    var url_request = charges_url.getListeDocumentCharges;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "container-charge-document"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getListDocumentChargeCallback', loading);
}

function getListDocumentChargeCallback(response) {
    $('#container-charge-document').html(response);
}

function getListCharge(cliLot_id, annee) {
    var data_request = {
        'annee': annee,
        'cliLot_id': cliLot_id,
    }
    var type_request = 'POST';
    var url_request = charges_url.getListCharge;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "container-charge-table"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getListChargeChargeCallback', loading);
}

function getListChargeChargeCallback(response) {
    $('#container-charge-table').html(response);
}

$(document).on('change', '#type_charge_id', function () {
    $('#charge_ht').attr('disabled', true);
    $('#charge_tva_taux').attr('disabled', false);
    if ($('#type_charge_id').val() == 10) {
        $('#charge_tva').attr('disabled', false);
        $('#charge_tva_taux').val(1);
        calculCharge_special();
    }
    else if ($('#type_charge_id').val() == 2) {
        $('#charge_tva').attr('disabled', false);
        $('#charge_ht').attr('disabled', false);
        $('#charge_tva_taux').val(1);
        $('#charge_tva_taux').attr('disabled', true);
    }
    else {
        $('#charge_tva').attr('disabled', true);
        $('#charge_tva_taux').val(0);
        calculCharge();
    }
});

$(document).on('keyup', '#montant_charge_ttc', function () {
    if ($('#type_charge_id').val() == 2) {
        const ttc = parseFloat($('#montant_charge_ttc').val());
        $('#charge_ttc').val(ttc.toFixed(2));
    } else if ($('#charge_tva_taux').val() == 1) {
        calculCharge_special();
    } else {
        calculCharge();
    }
});

$(document).on('keyup', '#charge_tva', function () {
    if ($('#type_charge_id').val() != 2) {
        calculCharge_special();
    }
});

$(document).on('change', '#charge_tva_taux', function () {
    if ($('#charge_tva_taux').val() == 1) {
        $('#charge_tva').attr('disabled', false);
        $('#charge_tva_taux').val(1);
        calculCharge_special();
    } else {
        $('#charge_tva').attr('disabled', true);
        calculCharge();
    }
});

function calculCharge_special() {
    if ($('#montant_charge_ttc').val() && $('#charge_tva').val()) {
        const ttc = parseFloat($('#montant_charge_ttc').val());
        const tva = parseFloat($('#charge_tva').val());
        const ht = ttc - tva;
        $('#charge_ht').val(ht.toFixed(2));
        $('#charge_tva').val(tva);
        $('#charge_ttc').val(ttc.toFixed(2));
    }
}

function calculCharge() {
    if ($('#montant_charge_ttc').val()) {
        const ttc = parseFloat($('#montant_charge_ttc').val());
        const taux_tva = parseFloat($('#charge_tva_taux').val());
        const ht = ttc / (1 + (taux_tva / 100));
        const tva = (ht * taux_tva) / 100;
        $('#charge_ht').val(ht.toFixed(2));
        $('#charge_tva').val(tva.toFixed(2));
        $('#charge_ttc').val(ttc.toFixed(2));
    }
}

function addChargeLot(data) {
    // var data_request = {
    //     'charge_id': charge_id,
    //     'date_facture': date_facture,
    //     'montant_ht': montant_ht,
    //     'tva': tva,
    //     'montant_ttc': montant_ttc,
    //     'tva_taux': tva_taux,
    //     'annee': annee,
    //     'type_charge_id': type_charge_id,
    //     'cliLot_id': cliLot_id,
    // }
    var type_request = 'POST';
    var url_request = charges_url.addCharge;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "container-charge-table"
    }

    fn.ajax_request(type_request, url_request, type_output, data, 'addChargeLotCallback', loading);
}

function addChargeLotCallback(response) {
    const obj = JSON.parse(response)
    // if (obj.status == true) {
    //     adddocumentchage(obj.data_retour.request.annee, obj.data_retour.request.cliLot_id, obj.data_retour.charge);
    //     getListCharge(obj.data_retour.request.cliLot_id, obj.data_retour.request.annee);
    // }
    if (obj.status == true) {
        adddocumentchage(obj.data_retour.request.annee, obj.data_retour.request.cliLot_id, obj.data_retour.charge, function (data) {
            // Cette fonction sera appelée après le succès de la requête AJAX
            getListCharge(obj.data_retour.request.cliLot_id, obj.data_retour.request.annee);
        });
    }

}

$(document).on('click', '#addCharge', function () {

    if ($('#charge_ht').val() &&
        $('#date_facture').val() &&
        $('#type_charge_id').val() &&
        $('#charge_tva').val()) {
        var cliLotIdValue = $('#cliLot_id_charge').val();
        var fileInput = $('#file_' + cliLotIdValue)[0];
        var selectedFiles = fileInput.files;
        var data_request = {
            'charge_id': $('#charge_id').val(),
            'date_facture': $('#date_facture').val(),
            'montant_ht': $('#charge_ht').val(),
            'tva': $('#charge_tva').val(),
            'montant_ttc': $('#charge_ttc').val(),
            'tva_taux': $('#charge_tva_taux').val(),
            'annee': $('#annee_charges').val(),
            'type_charge_id': $('#type_charge_id').val(),
            'cliLot_id': $('#cliLot_id_charge').val(),
        };

        // for (var i = 0; i < selectedFiles.length; i++) {
        //     data_request['files_' + (i + 1)] = selectedFiles[i];
        // }
        // console.log(data_request);
        // addChargeLot(
        //     // $('#charge_id').val(),
        //     // $('#date_facture').val(),
        //     // $('#charge_ht').val(),
        //     // $('#charge_tva').val(),
        //     // $('#charge_ttc').val(),
        //     // $('#charge_tva_taux').val(),
        //     // $('#annee_charges').val(),
        //     // $('#type_charge_id').val(),
        //     // $('#cliLot_id_charge').val()
        // );
        addChargeLot(data_request);

        $('#montant_charge_ttc').val('')
        $('#date_facture').val('');
        $('#charge_ht').val('');
        $('#charge_tva').val('');
        $('#charge_ttc').val('');


        $("#ModalCharge").modal('hide');
        $('.modal-backdrop').remove();
    } else {
        Notiflix.Notify.failure("Veuillez compléter les champs.", {
            position: 'left-bottom',
            timeout: 3000
        });
    }
});
function videformulaire() {

}

$(document).on('click', '#add_depense', function () {
    $('#charge_ht').attr('disabled', true);
    $('#charge_tva_taux').attr('disabled', false);
    $('.charge_modal').text("Ajout d'une dépense");
    $('#charge_id').val('');
    $('#type_charge_id').val(1);
    $('#montant_charge_ttc').val('');
    $('#date_facture').val('');
    $('#charge_ht').val('');
    $('#charge_tva').val('');
    $('#charge_ttc').val('');
    $('#charge_tva').attr('disabled', true);
    $(".name_doc").html('');
});

$(document).on('click', '#supprCharge', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        charge_id: $(this).attr('data-charge_id')
    }
    deleteCharges(data);
});

function adddocumentchage2(annee, cliLot_id, charge_id) {

    var cliLotIdValue = $('#cliLot_id_charge').val();
    var formData = new FormData();
    var fileInput = $('#file_' + cliLotIdValue)[0];
    var selectedFiles = fileInput.files;
    for (var i = 0; i < selectedFiles.length; i++) {
        formData.append(i, selectedFiles[i]);
    }

    formData.append('annee', annee);
    formData.append('cliLot_id', cliLot_id);
    formData.append('charge_id', charge_id);
    console.log(formData);
    $.ajax({
        url: charges_url.addDocSeulCharge,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,

        success: function (data) {

        }
    });

}

function adddocumentchage(annee, cliLot_id, charge_id, callback) {
    var cliLotIdValue = $('#cliLot_id_charge').val();
    var formData = new FormData();
    var fileInput = $('#file_' + cliLotIdValue)[0];
    var selectedFiles = fileInput.files;
    for (var i = 0; i < selectedFiles.length; i++) {
        formData.append(i, selectedFiles[i]);
    }

    formData.append('annee', annee);
    formData.append('cliLot_id', cliLot_id);
    formData.append('charge_id', charge_id);
    console.log(formData);

    $.ajax({
        url: charges_url.addDocSeulCharge,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,

        success: function (data) {
            // Appel du callback si fourni
            if (typeof callback === 'function') {
                callback(data);
            }
            $('#file_' + cliLotIdValue).val("");
        }
    });
}


function deleteCharges(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = charges_url.deleteCharges;

    var loading = {
        type: "content",
        id_content: "container-charge-table"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'deleteChargesCallback', loading);
}

function deleteChargesCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        charge_id: response.data.charge_id
                    }
                    deleteCharges(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=charge_row-${response.data.charge_id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getListCharge(response.charge_info.cliLot_id, response.charge_info.annee);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#modifierDepense', function () {
    updateCharge($(this).attr('data-charge_id'));
});

function updateCharge(charge_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'charge_id': charge_id,
    };
    var url_request = charges_url.getUpdateCharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateChargeCallback');
}

function updateChargeCallback(response) {
    $('.charge_modal').text("Modification d'une dépense");
    $('#charge_id').val(response.data_retour.charge_id);
    $('#type_charge_id').val(response.data_retour.type_charge_id);
    $('#montant_charge_ttc').val(response.data_retour.montant_ttc);
    $('#date_facture').val(response.data_retour.date_facture);
    $('#charge_ht').val(response.data_retour.montant_ht);
    $('#charge_tva_taux').val(response.data_retour.tva_taux);
    $('#charge_tva').val(response.data_retour.tva);
    $('#charge_ttc').val(response.data_retour.montant_ttc);

    if (response.data_retour.type_charge_id == 2) {
        $('#charge_tva').attr('disabled', false);
        $('#charge_ht').attr('disabled', false);
        $('#charge_tva_taux').val(1);
        $('#charge_tva_taux').attr('disabled', true);
    }

    if (response.data_retour.tva_taux == 1) {
        $('#charge_tva').attr('disabled', false);
    } else {
        $('#charge_tva').attr('disabled', true);
    }

    if (Array.isArray(response.form_validation)) {
        var docNames = "";
        $.each(response.form_validation, function (index, doc) {
            docNames += `<div class="doc_${doc.doc_charge_id}">
                        <span class="name_doc badge rounded-pill badge-soft-secondary" style="cursor: pointer;">${doc.doc_charge_nom}</span>
                        <span class="badge rounded-pill badge-soft-light btn-close text-white" data-id =${doc.doc_charge_id} id="removefile"  style="cursor: pointer;" title="Supprimer fichier">.</span>
                        </div>`;
        });

        $(".name_doc").html(docNames);
    }
}

$(document).on('click', '#removefile', function (e) {
    var id = $(this).attr('data-id');
    removefile(id);
});

function removefile(doc_charge_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        doc_charge_id: doc_charge_id,
    };

    var url_request = charges_url.removefile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'removefileCallBack');
}

function removefileCallBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == true) {
        $('.doc_' + obj.data_retour.doc_charge_id).empty();
        Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#ajoutDocCharge', function () {
    ajoutDocCharge($(this).attr('data-id'), 'add', null);
});

function ajoutDocCharge(cliLot_id, action, doc_charge_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_charge_id': doc_charge_id,
        'annee': $('#annee_charges').val()
    };

    var url_request = charges_url.formUploadDocCharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentCharge');

    $("#modalAjoutCharge").modal('show');
}

function chargercontentCharge(response) {
    $('#UploadCharge').html(response);
}

function apercuDocumentCharge(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = charges_url.getpathDoccharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathcharge');
}

function responsepathcharge(response) {
    var el_apercu = document.getElementById('apercuChargedoc')
    var dataPath = response[0].doc_charge_path
    var id_document = response[0].doc_charge_id
    apercuCharge(dataPath, el_apercu, id_document);
}

function apercuCharge(data, el, id_doc) {
    viderApercuCharge();

    var doc_selectionner = document.getElementById("docrowcharge-" + id_doc);

    if (doc_selectionner) {
        doc_selectionner.style.backgroundColor = '#abb1ba';
    }

    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuCharge').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuCharge').appendChild(element);
    }
}

function viderApercuCharge() {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuChargedoc") != undefined) {
        if (document.getElementById("apercuChargedoc").querySelector("#view_apercu") != null) {
            document.getElementById("apercuChargedoc").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuChargedoc").querySelector("#view_apercu").remove();
        }
    }
}


$(document).on('click', '.btnSuppDocCharge', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_charge_id: $(this).attr('data-id')
    };

    deleteDocCharge(data_request);
});

function deleteDocCharge(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = charges_url.deleteDocCharge;
    var loading = {
        type: "content",
        id_content: "contenair-charges"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocCharge', loading);
}

function retourDeleteDocCharge(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_charge_id: response.data.id
                    }
                    deleteDocCharge(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowcharge-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnUpdtadeDocCharge', function (e) {
    e.preventDefault();
    var doc_charge_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var doc_charge_annee = $(this).attr("data-doc_charge_annee");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        doc_charge_id: doc_charge_id,
        cliLot_id: cliLot_id,
        doc_charge_annee: doc_charge_annee
    };
    var url_request = charges_url.modalupdateDocCharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);

    myModal.toggle();
}

$(document).on('submit', '#UpdateDocumentCharge', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocCharge", "null_fn", param);
});

function retourupdatedocCharge(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getListDocumentCharge(response.data_retour.doc_charge_annee, response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnTraiteCharge', function () {
    updateTraitementCharge($(this).attr('data-id'));
});

function updateTraitementCharge(doc_charge_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_charge_id': doc_charge_id,
    };

    var url_request = charges_url.updateDocChargeTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementCallback');
}

function updateTraitementCallback(response) {
    getListDocumentCharge($('#annee_charges').val(), response.cliLot_id);
}

$(document).on('click', '#validerDepense', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        charge_id: $(this).attr('data-charge_id'),
        cliLot_id: $(this).attr('data-lot'),
        annee: $('#annee_charge').val()
    };
    validerDepense(data_request);
});

function validerDepense(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = charges_url.validerDepense;
    var loading = {
        type: "content",
        id_content: "contenair-charges"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDepenses', loading);
}

function returnDepenses(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        charge_id: response.data.id,
                        cliLot_id: response.data.cliLot_id,
                        annee: response.data.annee
                    }
                    validerDepense(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getListCharge(response.data.cliLot_id, response.data.annee);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.joinCharge', function (e) {
    e.preventDefault();
    var doc_charge_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var doc_charge_annee = $(this).attr("data-doc_charge_annee");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        doc_charge_id: doc_charge_id,
        cliLot_id: cliLot_id,
        doc_charge_annee: doc_charge_annee
    };
    var url_request = charges_url.modaljoinCharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateChargeDoc', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateChargeDoc", "null_fn", param);
});

function retourUpdateChargeDoc(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getListDocumentCharge(response.data_retour.doc_charge_annee, response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#annuler_saisie', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        cliLot_id: $(this).attr('data-id'),
        annee: $(this).attr('data-annee')
    };
    annulersaisie(data_request);
});

function annulersaisie(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = charges_url.annulersaisie;
    var loading = {
        type: "content",
        id_content: "contenair-charges"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnAnnulersaisie', loading);
}

function returnAnnulersaisie(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.data.cliLot_id,
                        annee: response.data.annee
                    }
                    annulersaisie(data);
                },
                () => { },
                {
                    width: '500px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getListDocumentCharge(response.data.annee, response.data.cliLot_id);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


$(document).on('click', '.btn_info_dep', function () {
    var info = $(this).attr('data-info');
    var lang = $(this).attr('data-langVide');
    Notiflix.Confirm.show(
        'information', (($.trim(info) != "") ? info : lang), 'Fermer',
        () => { }
    );
});