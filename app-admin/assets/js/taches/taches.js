var data_url = {
    header: `${const_app.base_url}Taches/getheaderlist`,
    listeTache: `${const_app.base_url}Taches/listeTache`,
    getFormTache: `${const_app.base_url}Taches/getFormTache`,
    listTacheValide: `${const_app.base_url}Taches/listTacheValide`,
    listProspect: `${const_app.base_url}Taches/listProspect`,
    deleteTache: `${const_app.base_url}Taches/removeTache`,
    valideTache: `${const_app.base_url}Taches/valideTache`,
}

var timer = null;

$(document).ready(function () {

});

function chargeUrl() {

}

// $(document).ready(function() {
//     pgTaches();
// });

function pgTaches() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    var url_request = data_url.header;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pagelistetache', loading);
}

function charger_pagelistetache(response) {
    $('#contenair-tache').html(response)
}

$(document).on('change', '#createur', function (e) {
    e.preventDefault();
    listTache();
});

$(document).on('change', '#participant', function (e) {
    e.preventDefault();
    listTache();
});

$(document).on('change', '#theme_id', function (e) {
    e.preventDefault();
    listTache();
});

function listTache() {
    var type_request = 'POST', type_output = 'html';
    var createur = $('#createur').val();
    var participant = $('#participant').val();
    var theme_id = $('#theme_id').val();
    var date_filtre = $('#date_filtre').val();
    var data_request = {
        createur: createur,
        participant: participant,
        theme_id: theme_id,
        date_filtre: date_filtre

    };

    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    var url_request = data_url.listeTache;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listListe', loading);
}
function load_listListe(response) {
    $('#data-list').html(response);
}

function getFormTache(action, tache_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'tache_id': tache_id,
    };
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }

    var url_request = data_url.getFormTache;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormTache', loading);
}

function retourFormTache(response) {
    $('#contenair-tache').html(response);
}

$(document).on('click', '#btnFormTache', function (e) {
    e.preventDefault();
    getFormTache('add', null);
});

$(document).on('click', '#updateTache', function (e) {
    e.preventDefault();
    getFormTache('edit', $(this).attr('data-id'));
});

$(document).on('click', '#annulerAddTache', function (e) {
    e.preventDefault();
    pgTaches();
});

$(document).on('click', '#btnTacheValide', function (e) {
    e.preventDefault();
    $(".tachevalide").toggleClass('d-none');
    $(".tachenonvalide").toggleClass('d-none');
    $("#btnTacheNonValide").toggleClass('d-none');
    $("#btnFormTache").toggleClass('d-none');
    $(this).toggleClass('d-none');
    getTachevalide();
});

$(document).on('click', '#btnTacheNonValide', function (e) {
    e.preventDefault();
    $(".tachevalide").toggleClass('d-none');
    $(".tachenonvalide").toggleClass('d-none');
    $("#btnTacheValide").toggleClass('d-none');
    $("#btnFormTache").toggleClass('d-none');
    $(this).toggleClass('d-none');
    listTache();
});

function getTachevalide() {
    var type_request = 'POST';
    var type_output = 'html';
    var createur = $('#createur').val();
    var participant = $('#participant').val();
    var theme_id = $('#theme_id').val();
    var date_filtre = $('#date_filtre').val();
    var data_request = {
        createur: createur,
        participant: participant,
        theme_id: theme_id,
        date_filtre: date_filtre

    };
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    var url_request = data_url.listTacheValide;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourlistetache', loading);
}
function retourlistetache(response) {
    $('#data-list').html(response);
}

function showinputclient() {
    if ($("#client").is(':checked')) {
        $('#client_div').show();
        $('#prospect').hide();
        $('#prospectlabel').hide();
    } else {
        $('#client_div').hide();
        $('#prospectlabel').show();
        $('#prospect').show();
    }
}

function showinputprospect() {
    if ($("#prospect").is(':checked')) {
        $('#prospect_div').show();
        $('#client').hide();
        $('#clientlabel').hide();
    } else {
        $('#prospect_div').hide();
        $('#clientlabel').show();
        $('#client').show();
    }
}

$(document).on('submit', '#cruTache', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddTache", loading);
});

function retourAddTache(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        pgTaches();
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnSuppTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    delete_tache(data_request);
});

function delete_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.deleteTache;
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteTache', loading);
}
function retourDeleteTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    delete_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.btnValideTache', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        tache_id: $(this).attr('data-id')
    };
    valide_tache(data_request);
});

function valide_tache(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.valideTache;
    var loading = {
        type: "content",
        id_content: "contenair-tache"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValideTache', loading);
}
function retourValideTache(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        tache_id: response.data.id
                    }
                    valide_tache(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=rowTache-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.FicheClientNewTab', function (e) {
    e.preventDefault();
    const clientId = $(this).attr('data-id');
    var href = $('#client-open-tab-url').val();
    window.open(`${href}?_open@client_tab=${clientId}`, "_blank");
});

$(document).on('click', '.FicheProspectNewTab', function (e) {
    e.preventDefault();
    const prospectId = $(this).attr('data-id');
    var href = $('#prospect-open-tab-url').val();
    window.open(`${href}?_open@prospect_tab=${prospectId}`, "_blank");
});