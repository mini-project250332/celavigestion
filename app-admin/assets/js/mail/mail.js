var mail_url = {
    EnvoiTom: `${const_app.base_url}SendMail/EnvoiTom`,
    EnvoiTomClient: `${const_app.base_url}SendMail/EnvoiTomClient`,
    declarerErreur: `${const_app.base_url}SendMail/declarerErreur`,
    sendMailTaxe: `${const_app.base_url}SendMail/Envoi_Mail_Taxe`,
    sendMailClient: `${const_app.base_url}SendMail/Envoi_Mail_Taxe_client`,
    checkValueEmeteur: `${const_app.base_url}SendMail/checkValueEmeteur`,
    sendMailTaxeErreur: `${const_app.base_url}SendMail/Envoi_Mail_TaxeErreur`,
    modalEnvoiMailFacture: `${const_app.base_url}SendMail/modalEnvoiMailFacture`,
    sendMailFacture: `${const_app.base_url}SendMail/Envoi_Mail_Facture`,
    modalEnvoiMailMandat: `${const_app.base_url}SendMail/modalEnvoiMailMandat`,
    sendMailMandat: `${const_app.base_url}SendMail/sendMailMandat`,
    sendMailBailValidation: `${const_app.base_url}SendMail/sendMailBailValidation`,
    sendMailBailCloture: `${const_app.base_url}SendMail/sendMailBailCloture`,
    sendMailBailcreerAvenant: `${const_app.base_url}SendMail/sendMailBailcreerAvenant`,
    sendMailMandatProspect: `${const_app.base_url}SendMail/sendMailMandatProspect`,
    sendMailTva: `${const_app.base_url}SendMail/sendMailTva`,
    sendMail2031: `${const_app.base_url}SendMail/sendMail2031`,
    sendMailBailprotocol: `${const_app.base_url}SendMail/sendMailBailprotocol`,
    sendMaillocataire: `${const_app.base_url}SendMail/sendMaillocataire`,
    sendMailsyndic: `${const_app.base_url}SendMail/sendMailsyndic`,
    sendMailsie: `${const_app.base_url}SendMail/sendMailsie`,
    sendMailsip: `${const_app.base_url}SendMail/sendMailsip`,
    sendMailPno: `${const_app.base_url}SendMail/sendMailPno`,
    sendMailPnoProspect: `${const_app.base_url}SendMail/sendMailPnoProspect`,
    sendMailReporting: `${const_app.base_url}SendMail/sendMailReporting`,
    sendMailGestionnaire: `${const_app.base_url}SendMail/sendMailGestionnaire`,
}

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModalMail(response) {
    Modal.toggle();
    $("#modalContent").html(response);
    $('#emeteur_erreur').trigger("change");
    $('#emeteur_decla').trigger("change");
    $('#emeteur_facture').trigger("change");
    $('#emeteur_mandat').trigger("change");
}

$(document).on('click', '.envoiTom', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val(),
        annee: $('#anneetaxe').val()
    };
    var url_request = mail_url.EnvoiTom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
});

$(document).on('click', '.envoiTomAuClient', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val(),
        annee: $('#anneetaxe').val()
    };
    var url_request = mail_url.EnvoiTomClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
});

$(document).on('click', '.declarerErreur', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val(),
        annee: $('#anneetaxe').val()
    };
    var url_request = mail_url.declarerErreur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
});


$(document).on('change', '#emeteur_erreur', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mess_id: $(this).val()
    };
    var url_request = mail_url.checkValueEmeteur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'onchangeCallBack');
});

function onchangeCallBack(response) {
    var obj = JSON.parse(response);
    if (obj.mess_signature != null) {
        $('#image_signature').removeClass('d-none');
        $('#image_signature').attr("src", `${const_app.base_url}` + obj.mess_signature);
        $('#signature_erreur').addClass('d-none');
    } else {
        $('#signature_erreur').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
}

$(document).on('change', '#emeteur_decla', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mess_id: $(this).val()
    };
    var url_request = mail_url.checkValueEmeteur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'onchangeDeclaCallBack');
});

function onchangeDeclaCallBack(response) {
    var obj = JSON.parse(response);
    if (obj.mess_signature != null) {
        $('#image_signature').removeClass('d-none');
        $('#image_signature').attr("src", `${const_app.base_url}` + obj.mess_signature);
        $('#signature_decla').addClass('d-none');
    }
    else if (obj == null) {
        $('#signature_decla').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
    else {
        $('#signature_decla').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
}

function isEmail(email) {
    var EmailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return EmailRegex.test(email);
}

function validation_mail(email) {
    var email_check = true;
    if (email.includes(",") == true) {
        var list_email = email.split(',');
        var arrayLength = list_email.length;
        for (var i = 0; i < arrayLength; i++) {
            if (isEmail(list_email[i].trim()) == false) {
                email_check = false;
                break;
            }
            else {
                email_check = true;
            }
        }
    } else {
        email_check = isEmail(email);
    }
    return email_check;
}

$(document).on('click', '#sendMailTaxe', function () {
    var table_doc = new Array();
    $('.document_taxe_id').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        annee: $('#annee_modal_taxe').val(),
        cliLot_id: $('#cliLot_id').val(),
        taxe_id: $('#taxe_id').val(),
        emeteur_decla: $('#emeteur_decla').val(),
        destinataire: $('#destinataire').val(),
        destinataire_copie: $('#destinataire_copie').val(),
        object: $('#object').val(),
        message_text: $('#message_text').val().replace(/\n/g, "<br/>"),
        list_id_doc_taxe: table_doc
    };
    var url_request = mail_url.sendMailTaxe;

    var loading = {
        type: "content",
        id_content: "content-taxe"
    }

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'send_MailCalBack', loading);
            Modal.toggle();
        }
        else if (!($('#destinataire_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'send_MailCalBack', loading);
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function send_MailCalBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
        const message = `Email envoyé au Gestionnaire ${obj.destinataire} ayant comme objet ${obj.objet}`;
        historySendMailTaxe(message, obj.taxe_id, obj.history);
        envoiTomClient(obj.cliLot_id, obj.annee);
    }
}

$(document).on('click', '#sendMailClient', function () {
    var table_doc = new Array();
    $('.document_taxe_id').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        annee: $('#annee_modal_taxe').val(),
        cliLot_id: $('#cliLot_id').val(),
        taxe_id: $('#taxe_id').val(),
        emeteur_decla: $('#emeteur_decla').val(),
        destinataire: $('#destinataire').val(),
        object: $('#object').val(),
        message_text: $('#message_text').val().replace(/\n/g, "<br/>"),
        list_id_doc_taxe: table_doc
    };
    var url_request = mail_url.sendMailClient;

    var loading = {
        type: "content",
        id_content: "content-taxe"
    }

    if (isEmail((data_request.destinataire).trim())) {
        fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailClientCallBack', loading);
        Modal.toggle();
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendMailClientCallBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
        const message = `Email envoyé au Propriétaire ${obj.destinataire} ayant comme objet ${obj.objet}`;
        historySendMailTaxe(message, obj.taxe_id, obj.history);
    }
}

function envoiTomClient(cliLot_id, annee) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id,
        annee: annee
    };
    var url_request = mail_url.EnvoiTomClient;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
}

$(document).on('click', '#removefile', function () {
    var id_div = $(this).attr('data-id');
    $('.line-' + id_div).remove();
    if (!$('.doc').length) {
        $(".list_doc").append(' <p style="font-weight: 500;font-size: 14px; margin-left: -26px;">Pas de fichier joints</p>');
    }
});

$(document).on('click', '#sendMailTaxeErreur', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        taxe_id: $('#taxe_id').val(),
        emeteur_erreur: $('#emeteur_erreur').val(),
        destinataire: $('#destinataire_erreur').val(),
        destinataire_copie: $('#destinataire_erreur_copie').val(),
        object: $('#object_erreur').val(),
        message_text: $('#message-text_erreur').val().replace(/\n/g, "<br/>"),
    };
    var url_request = mail_url.sendMailTaxeErreur;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_erreur_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailTaxeErreurCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_erreur_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailTaxeErreurCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendMailTaxeErreurCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success('Email envoyé .', { position: 'left-bottom', timeout: 3000 });
        const message = `Email envoyé à ${obj.destinataire} ayant comme objet ${obj.objet}`;
        historySendMailTaxe(message, obj.taxe_id, obj.history);
    }
}

$(document).on('click', '#modalDocumentFacture', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val(),
        annee: $('#annee_loyer').val(),
        doc_loyer_id: $(this).attr('data-id')
    };
    var url_request = mail_url.modalEnvoiMailFacture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
});

$(document).on('change', '#emeteur_facture', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mess_id: $(this).val()
    };
    var url_request = mail_url.checkValueEmeteur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'onchangeFactureCallBack');
});

function onchangeFactureCallBack(response) {
    var obj = JSON.parse(response);
    if (obj.mess_signature != null) {
        $('#image_signature').removeClass('d-none');
        $('#image_signature').attr("src", `${const_app.base_url}` + obj.mess_signature);
        $('#signature_facture').addClass('d-none');
    }
    else if (obj == null) {
        $('#signature_facture').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
    else {
        $('#signature_facture').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
}

$(document).on('click', '#sendMailFacture', function () {
    var table_doc = new Array();
    $('.document_loyer').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_facture: $('#emeteur_facture').val(),
        destinataire: $('#destinataire_facture').val(),
        destinataire_copie: $('#destinataire_facture_copie').val(),
        object: $('#object').val(),
        message_text: $('#message_text_facture').val().replace(/\n/g, "<br/>"),
        list_id_doc_loyer: table_doc
    };
    var url_request = mail_url.sendMailFacture;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_facture_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailFactureCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_facture_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailFactureCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendMailFactureCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getFactureLoyer_PDF(obj.fact_loyer_id, true)

        // Add to email history
        const message = `Email envoyé à ${obj.destinataire} ayant comme objet ${obj.objet}`;
        historySendMail(message, obj.fact_loyer_id, obj.history);
    }
}

$(document).on('click', '.envoiMailMandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: $('#cliLot_id').val(),
        mandat_cli_id: $(this).attr('data-id')
    };
    var url_request = mail_url.modalEnvoiMailMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalMail');
});

$(document).on('change', '#emeteur_mandat', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        mess_id: $(this).val()
    };
    var url_request = mail_url.checkValueEmeteur;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'onchangeMandatCallBack');
});

function onchangeMandatCallBack(response) {
    var obj = JSON.parse(response);
    if (obj.mess_signature != null) {
        $('#image_signature').removeClass('d-none');
        $('#image_signature').attr("src", `${const_app.base_url}` + obj.mess_signature);
        $('#signature_mandat').addClass('d-none');
    }
    else if (obj == null) {
        $('#signature_mandat').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
    else {
        $('#signature_mandat').removeClass('d-none');
        $('#image_signature').addClass('d-none');
    }
}

$(document).on('click', '#sendMailMandat', function () {
    var table_doc = new Array();
    $('.document_mandat').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_mandat: $('#emeteur_mandat').val(),
        destinataire: $('#destinataire_mandat').val(),
        destinataire_copie: $('#destinataire_mandat_copie').val(),
        object: $('#object_mandat').val(),
        message_text: $('#message_text_mandat').val().replace(/\n/g, "<br/>"),
        list_id_doc_mandat: table_doc
    };
    var url_request = mail_url.sendMailMandat;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_mandat_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailMandatCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_mandat_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailMandatCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendMailMandatCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

// Send mail validation bail
$(document).on('click', '#ask_bail_validation', function () {

    var count_email_sent = parseInt($("#mail_bail_sent").val());
    if (count_email_sent > 0) {
        $("#resend_modal_validation_bail_mail").modal('show');
        return false;
    }

    var type_request = 'POST';

    var type_output = 'json';

    var data_request = {
        bail_id: $(this).attr('data-bail_id'),
        cli_lot_id: $(this).attr('data-cli_lot_id'),
        util_id: getUtilId()
    };

    var url_request = mail_url.sendMailBailValidation;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailBailValidationCallback');
});

$(document).on('click', '#ask_bail_validation_in_bail', function () {

    var count_email_sent = parseInt($("#mail_bail_sent").val());

    if (count_email_sent > 0) {
        $("#resend_modal_validation_bail_mail_in_bail").modal('show');
        return false;
    }

    var type_request = 'POST';

    var type_output = 'json';

    var data_request = {
        bail_id: $(this).attr('data-bail_id'),
        cli_lot_id: $(this).attr('data-cli_lot_id'),
        util_id: getUtilId()
    };

    var url_request = mail_url.sendMailBailValidation;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailBailValidationCallback');
});


function sendMailBailValidationCallback(response) {
    if (response.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
        $("#message_error_mail_bail").html(response.detail);
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        $("#message_success_mail_bail").html(response.detail);

        const demandeur = response.data_proprietaire.util_prenom + ' ' + response.data_proprietaire.util_nom;

        var today = new Date();
        var day = today.getDate() < 10 ? '0' + today.getDate() : today.getDate();
        var month = (today.getMonth() + 1) < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1);
        var date = day + '/' + month + '/' + today.getFullYear();
        var hours = today.getHours() < 10 ? '0' + today.getHours() : today.getHours();
        var minutes = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();
        var time = hours + ":" + minutes;
        const message = `Le mail de demande de validation a déjà été envoyé par ${demandeur} aux administrateurs le ${date} à ${time}.`;

        $("#message_information_mail_bail").append("<br/>");
        $("#message_information_mail_bail").append("<br/>");
        $("#message_information_mail_bail").prepend(message);
        addHistorique(6, 1, message, response.bailId);
    }
}

$(document).on('click', '#confirmResendModalValidationBailMail', function () {
    $("#resend_modal_validation_bail_mail").modal('hide');
    $("#resend_modal_validation_bail_mail_in_bail").modal('hide');
    $("#mail_bail_sent").val(0);
    $("#ask_bail_validation").trigger('click');
});

$(document).on('click', '#cancelNewIndexationLoyer', function () {
    $("#resend_modal_validation_bail_mail").modal('hide');
});

$(document).on('click', '#cancelNewIndexationLoyer_in_bail', function () {
    $("#resend_modal_validation_bail_mail_in_bail").modal('hide');
});

function verifyEmailBailValidationSent(response) {
    if (response?.data_retour?.historiques.length > 0) {
        $("#message_information_mail_bail").html("");
        $("#mail_bail_sent").val(response?.data_retour?.historiques.length);
        response?.data_retour?.historiques.forEach(history => {
            $("#message_information_mail_bail").append(history.histo_action);
            $("#message_information_mail_bail").append("<br/>");
            $("#message_information_mail_bail").append("<br/>");
        });
    }
}

$(document).on('click', '#btn_demande_cloture', function () {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        bail_id: $('#bail_id').val(),
        cli_lot_id: $(this).attr('data-cliLot_id'),
        util_id: getUtilId()
    };
    var url_request = mail_url.sendMailBailCloture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailBailCloture_Callback');
});

function sendMailBailCloture_Callback(response) {
    if (response.status == 200) {
        Notiflix.Notify.success("Mail de validation envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#btn_demande_creer_avenant', function () {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        bail_id: $('#bail_id').val(),
        cli_lot_id: $(this).attr('data-cliLot_id'),
        util_id: getUtilId()
    };
    var url_request = mail_url.sendMailBailcreerAvenant;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailBailcreerAvenant_Callback');
});

function sendMailBailcreerAvenant_Callback(response) {
    if (response.status == 200) {
        Notiflix.Notify.success("Mail de validation envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#sendMailMandatProspect', function () {
    var table_doc = new Array();
    $('.document_mandat').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_mandat: $('#emeteur_mandat').val(),
        destinataire: $('#destinataire_mandat').val(),
        destinataire_copie: $('#destinataire_mandat_copie').val(),
        object: $('#object_mandat').val(),
        message_text: $('#message_text_mandat').val().replace(/\n/g, "<br/>"),
        list_id_doc_mandat: table_doc,
        mandat_id: $(this).attr('data-id'),
        lot_id: $('#lot_id_mandat').val(),
        prospect_id: $('#prospect_id').val(),
    };

    var url_request = mail_url.sendMailMandatProspect;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_mandat_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMandatCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_mandat_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMandatCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendMandatCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getLotMandat(obj.lot_id, obj.prospect_id);
    }
}

/*******Sens mail tva********/

$(document).on('click', '#sendMailTva', function () {
    var table_doc = new Array();
    $('.document_tva').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_tva: $('#emeteur_tva').val(),
        destinataire: $('#destinataire_tva').val(),
        destinataire_copie: $('#destinataire_tva_copie').val(),
        object: $('#object_tva').val(),
        message_text: $('#message_text_tva').val().replace(/\n/g, "<br/>"),
        list_id_doc_tva: table_doc,
        doc_tva_id: $(this).attr('data-id'),
        dossier_id: $('#dossier_id').val(),
    };

    var url_request = mail_url.sendMailTva;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_tva_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendTvaCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_tva_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendTvaCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendTvaCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getDoctva(obj.dossier_id);
    }
}

/*******Sens mail 2031********/

$(document).on('click', '#sendMail2031', function () {
    var table_doc = new Array();
    $('.document_2031').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_2031: $('#emeteur_2031').val(),
        destinataire: $('#destinataire_2031').val(),
        destinataire_copie: $('#destinataire_2031_copie').val(),
        object: $('#object_2031').val(),
        message_text: $('#message_text_2031').val().replace(/\n/g, "<br/>"),
        list_id_doc_2031: table_doc,
        doc_2031_id: $(this).attr('data-id'),
        dossier_id: $('#dossier_id').val(),
    };

    var url_request = mail_url.sendMail2031;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_2031_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'send2031CalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_2031_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'send2031CalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function send2031CalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getDoc2031(obj.dossier_id);
    }
}

$(document).on('click', '#btn_demande_valider_protocol', function () {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        bail_id: $('#bail_id').val(),
        cli_lot_id: $(this).attr('data-cliLot_id'),
        util_id: getUtilId()
    };
    var url_request = mail_url.sendMailBailprotocol;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailBailprotocol_Callback');
});

function sendMailBailprotocol_Callback(response) {
    if (response.status == 200) {
        Notiflix.Notify.success("Mail de validation envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
}


/*******Sens mail au locataire********/

$(document).on('click', '#sendMailLocataire', function () {
    var table_doc = new Array();
    $('.document_locataire').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_locataire: $('#emeteur_locataire').val(),
        destinataire: $('#destinataire_locataire').val(),
        destinataire_copie: $('#destinataire_locataire_copie').val(),
        object: $('#object_locataire').val(),
        message_text: $('#message_text_locataire').val().replace(/\n/g, "<br/>"),
        list_id_doc_locataire: table_doc,
        doc_lot_id: $(this).attr('data-id'),
        cliLot_id: $('#cliLot_id').val(),
    };

    var url_request = mail_url.sendMaillocataire;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_locataire_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendlocataireCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_locataire_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendlocataireCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendlocataireCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot(obj.cliLot_id, 'fiche');
    }
}

/*******Sens mail au Syndic********/

$(document).on('click', '#sendMailSyndic', function () {
    var table_doc = new Array();
    $('.document_syndic').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_syndic: $('#emeteur_syndic').val(),
        destinataire: $('#destinataire_syndic').val(),
        destinataire_copie: $('#destinataire_syndic_copie').val(),
        object: $('#object_syndic').val(),
        message_text: $('#message_text_syndic').val().replace(/\n/g, "<br/>"),
        list_id_doc_syndic: table_doc,
        doc_syndic_id: $(this).attr('data-id'),
        cliLot_id: $('#cliLot_id').val(),
    };

    var url_request = mail_url.sendMailsyndic;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_syndic_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsyndicCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_syndic_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsyndicCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendsyndicCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot(obj.cliLot_id, 'fiche');
    }
}


/*******Sens mail au Sie********/

$(document).on('click', '#sendMailSie', function () {
    var table_doc = new Array();
    $('.document_sie').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_sie: $('#emeteur_sie').val(),
        destinataire: $('#destinataire_sie').val(),
        destinataire_copie: $('#destinataire_sie_copie').val(),
        object: $('#object_sie').val(),
        message_text: $('#message_text_sie').val().replace(/\n/g, "<br/>"),
        list_id_doc_sie: table_doc,
        doc_sie_id: $(this).attr('data-id'),
        cliLot_id: $('#cliLot_id').val(),
    };

    var url_request = mail_url.sendMailsie;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_sie_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsieCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_sie_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsieCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendsieCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot(obj.cliLot_id, 'fiche');
    }
}

/*******Send mail au Sip********/

$(document).on('click', '#sendMailSip', function () {
    var table_doc = new Array();
    $('.document_sip').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_sip: $('#emeteur_sip').val(),
        destinataire: $('#destinataire_sip').val(),
        destinataire_copie: $('#destinataire_sip_copie').val(),
        object: $('#object_sip').val(),
        message_text: $('#message_text_sip').val().replace(/\n/g, "<br/>"),
        list_id_doc_sip: table_doc,
        doc_sip_id: $(this).attr('data-id'),
        cliLot_id: $('#cliLot_id').val(),
    };

    var url_request = mail_url.sendMailsip;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_sip_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsipCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_sip_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendsipCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendsipCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getFicheIdentificationLot(obj.cliLot_id, 'fiche');
    }
}


/*******Send mail PNO propriétaire********/

$(document).on('click', '#sendMailPno', function () {
    var table_doc = new Array();
    $('.document_sip').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_pno: $('#emeteur_pno').val(),
        destinataire: $('#destinataire_pno').val(),
        destinataire_copie: $('#destinataire_pno_copie').val(),
        object: $('#object_pno').val(),
        message_text: $('#message_text_pno').val().replace(/\n/g, "<br/>"),
        list_id_doc_pno: table_doc,
        pno_id: $(this).attr('data-id'),
        cliLot_id: $('#cliLot_id').val(),
        client_id: $('#client_id').val(),
    };

    var url_request = mail_url.sendMailPno;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_Pno_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendPnoCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_Pno_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendPnoCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendPnoCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getLotPno(obj.cliLot_id, obj.client_id);
    }
}

/*******Send mail PNO prospect********/

$(document).on('click', '#sendMailPnoProspect', function () {
    var table_doc = new Array();
    $('.document_sip').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_pno: $('#emeteur_pno').val(),
        destinataire: $('#destinataire_pno').val(),
        destinataire_copie: $('#destinataire_pno_copie').val(),
        object: $('#object_pno').val(),
        message_text: $('#message_text_pno').val().replace(/\n/g, "<br/>"),
        list_id_doc_pno: table_doc,
        pno_id: $(this).attr('data-id'),
        lot_id: $('#lot_id').val(),
        prospect_id: $('#prospect_id').val(),
    };

    var url_request = mail_url.sendMailPnoProspect;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_Pno_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendPnoProspectCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_Pno_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendPnoProspectCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendPnoProspectCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getLotPno(obj.cliLot_id, obj.client_id);
    }
}

/*******Send mail PNO prospect********/

$(document).on('click', '#sendMailReporting', function () {
    var table_doc = new Array();
    $('.document_reporting').each(function () {
        table_doc.push($(this).val());
    });
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        emeteur_reporting: $('#emeteur_reporting').val(),
        destinataire: $('#destinataire_reporting').val(),
        destinataire_copie: $('#destinataire_reporting_copie').val(),
        object: $('#object_reporting').val(),
        message_text: $('#message_text_reporting').val().replace(/\n/g, "<br/>"),
        list_id_doc_reporting: table_doc,
        reporting_id: $(this).attr('data-id'),
        dossier_id: $('#dossier_id').val(),
    };

    var url_request = mail_url.sendMailReporting;

    if (isEmail((data_request.destinataire).trim())) {
        if ($('#destinataire_reporting_copie').val() && validation_mail(data_request.destinataire_copie) == true) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendreportingCalBack');
            Modal.toggle();
        }
        else if (!($('#destinataire_reporting_copie').val())) {
            fn.ajax_request(type_request, url_request, type_output, data_request, 'sendreportingCalBack');
            Modal.toggle();
        }
        else {
            Notiflix.Notify.failure("Veuillez séparer l'email par une virgule ou bien saisir l'email", { position: 'left-bottom', timeout: 3000 });
        }
    }
    else {
        Notiflix.Notify.failure("Email de destinataire non validé", { position: 'left-bottom', timeout: 3000 });
    }
});

function sendreportingCalBack(response) {
    obj = JSON.parse(response);
    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        Notiflix.Notify.success("Mail envoyé", { position: 'left-bottom', timeout: 3000 });
        getListeReporting(obj.dossier_id, 'edit');
    }
}

function sendMailGestionnaire(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data;
    var url_request = mail_url.sendMailGestionnaire;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailGestionnaire_Callback');
}

function sendMailGestionnaire_Callback(response) {

}