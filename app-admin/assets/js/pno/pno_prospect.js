var pno_prospect_url = {
    pagePno: `${const_app.base_url}Pnoprospect/pagePno`,
    FormAddPno: `${const_app.base_url}Pnoprospect/FormAddPno`,
    getPathPno: `${const_app.base_url}Pnoprospect/getPathPno`,
    save_path_pno: `${const_app.base_url}Pnoprospect/save_path_pno`,
    save_path_pno_client : `${const_app.base_url}Pnoprospect/save_path_pno_client`,
    remove_file : `${const_app.base_url}Pnoprospect/remove_file`,
    sendPno : `${const_app.base_url}Pnoprospect/sendPno`,
    SignerPno : `${const_app.base_url}Pnoprospect/SignerPno`,
    ClorePnoCli : `${const_app.base_url}Pnoprospect/ClorePnoCli`,
    EnvoiMailPno : `${const_app.base_url}SendMail/EnvoiMailPnoProspect`,
    EnvoiMailPnoChere : `${const_app.base_url}SendMail/EnvoiMailPnoChereProspect`,
    cloturerPno : `${const_app.base_url}Pnoprospect/cloturerPno`,
    alertCloturePno : `${const_app.base_url}Pnoprospect/alertCloturePno`,
}

var timer = null;

$(document).on('click', '#ouicelaviegestion', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#ouicelaviegestion').is(':checked')) {
            $("#noncelaviegestion").prop("checked", false);
            if ($("#label_choix1").hasClass("d-none") && $('#ajoutPnoclient').hasClass('d-none')) {
                $('#label_choix1').removeClass('d-none');
                $('#ajoutPnoclient').removeClass('d-none');
                $('#table_pno').removeClass('d-none');
            }
            if (!$('#questionnaire2').hasClass('d-none')) {
                $('#questionnaire2').addClass('d-none');
            }
            if (!$('#questionnaire3').hasClass('d-none')) {
                $('#questionnaire3').addClass('d-none');
            }
            if (!$('#formulaire2').hasClass('d-none')) {
                $('#formulaire2').addClass('d-none');
                $('#label_choix2').addClass('d-none');
            }
            if (!$('#formulaire3').hasClass('d-none')) {
                $('#formulaire3').addClass('d-none');
                $('#label_choix3').addClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }
            $("#ouisyndic").prop("checked", false);
            $("#nonsyndic").prop("checked", false);
            $("#nesaitpassyndic").prop("checked", false);
            $("#ouiclient").prop("checked", false);
            $("#nonclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
        }
        submitPno();
    }, 800);


});

$(document).on('click', '#noncelaviegestion', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#noncelaviegestion').is(':checked')) {
            $('#questionnaire2').removeClass('d-none');
            $("#ouicelaviegestion").prop("checked", false);
            if (!$("#label_choix1").hasClass("d-none") && !$('#ajoutPnoclient').hasClass('d-none')) {
                $('#label_choix1').addClass('d-none');
                $('#ajoutPnoclient').addClass('d-none');
                $('#table_pno').addClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#ouisyndic', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#ouisyndic').is(':checked')) {
            $("#nonsyndic").prop("checked", false);
            $("#nesaitpassyndic").prop("checked", false);
            if ($("#label_choix2").hasClass("d-none") && $('#formulaire2').hasClass('d-none')) {
                $('#label_choix2').removeClass('d-none');
                $('#formulaire2').removeClass('d-none');
            }
            if (!$('#questionnaire3').hasClass('d-none')) {
                $('#questionnaire3').addClass('d-none');
            }
            if (!$('#formulaire3').hasClass('d-none')) {
                $('#formulaire3').addClass('d-none');
                $('#label_choix3').addClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }
            $("#ouiclient").prop("checked", false);
            $("#nonclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#nonsyndic', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#nonsyndic').is(':checked')) {
            $('#questionnaire3').removeClass('d-none');
            $("#ouisyndic").prop("checked", false);
            $("#nesaitpassyndic").prop("checked", false);
            if (!$("#formulaire2").hasClass("d-none") && !$('#label_choix2').hasClass('d-none')) {
                $('#label_choix2').addClass('d-none');
                $('#formulaire2').addClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }
            $("#ouiclient").prop("checked", false);
            $("#nonclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#nesaitpassyndic', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#nesaitpassyndic').is(':checked')) {
            $('#questionnaire3').removeClass('d-none');
            $("#nonsyndic").prop("checked", false);
            $("#ouisyndic").prop("checked", false);
            if (!$("#formulaire2").hasClass("d-none") && !$('#label_choix2').hasClass('d-none')) {
                $('#label_choix2').addClass('d-none');
                $('#formulaire2').addClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }

            $("#ouiclient").prop("checked", false);
            $("#nonclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#ouiclient', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#ouiclient').is(':checked')) {
            $("#nonclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
            if ($("#label_choix3").hasClass("d-none") && $('#formulaire3').hasClass('d-none')) {
                $('#label_choix3').removeClass('d-none');
                $('#formulaire3').removeClass('d-none');
            }
            if (!$('#button_send_mail_pno').hasClass('d-none')) {
                $('#button_send_mail_pno').addClass('d-none')
            }
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#nonclient', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#nonclient').is(':checked')) {
            $("#ouiclient").prop("checked", false);
            $("#nesaitpasclient").prop("checked", false);
            if (!$("#formulaire3").hasClass("d-none") && !$('#label_choix3').hasClass('d-none')) {
                $('#label_choix3').addClass('d-none');
                $('#formulaire3').addClass('d-none');
            }
            // if (!$('#button_send_mail_pno').hasClass('d-none')) {
            //     $('#button_send_mail_pno').addClass('d-none')
            // }

            if ($('#nesaitpassyndic').is(':checked')) {
                $('#button_send_mail_pno').removeClass('d-none');
            }
        }
        submitPno();
    }, 800);
});

$(document).on('click', '#nesaitpasclient', function () {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        if ($('#nesaitpasclient').is(':checked')) {
            $("#nonclient").prop("checked", false);
            $("#ouiclient").prop("checked", false);
            if (!$("#formulaire3").hasClass("d-none") && !$('#label_choix3').hasClass('d-none')) {
                $('#label_choix3').addClass('d-none');
                $('#formulaire3').addClass('d-none');
            }

            if ($('#nesaitpassyndic').is(':checked')) {
                $('#button_send_mail_pno').removeClass('d-none');
            }
        }
        submitPno();
    }, 800);
});

$(document).on('keyup', '#pno_montant', function (e) {
    e.preventDefault();

    var montantcli = parseFloat($(".pno_montant_client").val());
    var montantsyndic = parseFloat($(".pno_montant_syndic").val());

    if (montantcli > 70) {
        $("#montant_client").addClass('text text-danger');
        $(".alert_montant_client span").html('Montant PNO chère');
        $("#send_mail_pno_chere_client").removeClass('d-none');
    }else{
        $("#montant_client").removeClass('text text-danger');
        $(".alert_montant_client span").html('');
        $("#send_mail_pno_chere_client").addClass('d-none');
    } 

    if (montantsyndic > 70) {
        $("#montant_syndic").addClass('text text-danger');
        $(".alert_montant_syndic span").html('Montant PNO chère');
        $("#send_mail_pno_chere_syndic").removeClass('d-none');
    }else{
        $("#montant_syndic").removeClass('text text-danger');
        $(".alert_montant_syndic span").html('');
        $("#send_mail_pno_chere_syndic").addClass('d-none');
    } 

    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitPno();
    }, 800);
});

$(document).on('change', '.pno_path', function (e) {
    e.preventDefault();
    var formData = new FormData();
    formData.append("pno_id", $("#pno_id").val());
    formData.append("file", $("#pno_path")[0].files[0]);   
   
    $.ajax({
        url: pno_prospect_url.save_path_pno,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            var lot_id = $("#lot_id").val();
            var prospect_id = $("#prospect_id").val();
            getLotPnoProspect(lot_id,prospect_id);
        }
    });
});

$(document).on('change', '#pno_path_client', function (e) {
    e.preventDefault();
    console.log(ouiclient);
    var formData = new FormData();
    formData.append("pno_id", $("#pno_id").val());
    formData.append("file", $("#pno_path_client")[0].files[0]);   
   
    $.ajax({
        url: pno_prospect_url.save_path_pno_client,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            var lot_id = $("#lot_id").val();
            var prospect_id = $("#prospect_id").val();
            getLotPnoProspect(lot_id,prospect_id);
        }
    });
});

function submitPno() {
    $('#annee').val($('#anneetaxe').val());
    var form = $("#AddPno");
    var type_output = 'json';

    fn.ajax_form_data_custom(form, type_output, 'retourPno');
}

function retourPno(response) {
    $('input[name=pno_id]').val(response.data_retour.pno_id);
    $('input[name=action]').val(response.data_retour.action);
    $("#send_mail_pno").attr("data-id", response.data_retour.pno_id);
    $("#send_mail_pno_chere").attr("data-id", response.data_retour.pno_id);
    //getLotPnoProspect(response.data_retour.lot_id, response.data_retour.prospect_id);
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

$(document).on('click', '#ajoutPnoclient', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "add",
        lot_id: $(this).attr('data-id'),
        prospect_id: $(this).attr('data-client'),
        pno_id: $(this).attr('data-pno-id')
    };
    var url_request = pno_prospect_url.FormAddPno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalPno');
});

function charger_contentModalPno(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

function getLotPnoProspect(lot_id, prospect_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'lot_id': lot_id,
        'prospect_id': prospect_id
    };

    var url_request = pno_prospect_url.pagePno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getLotPnoProspect_Callback');
}

function getLotPnoProspect_Callback(response) {
    $('#contenair-pno').html(response);
    var ouicelaviegestion = $('#ouicelaviegestion').prop('value');
    var noncelaviegestion = $('#noncelaviegestion').prop('value');
    var ouisyndic = $('#ouisyndic').prop('value');
    var nonsyndic = $('#nonsyndic').prop('value');
    var nesaitpassyndic = $('#nesaitpassyndic').prop('value');
    var ouiclient = $('#ouiclient').prop('value');
    var nonclient = $('#nonclient').prop('value');
    var nesaitpasclient = $('#nesaitpasclient').prop('value');
    var montantcli = parseFloat($(".pno_montant_client").val());
    var montantsyndic = parseFloat($(".pno_montant_client").val());

    if (montantcli > 70) {
        $('#pno_montant').trigger('keyup');
    }

    if (montantsyndic > 70) {
        $('#pno_montant').trigger('keyup');
    }

    if (ouicelaviegestion == 1) {
        $('#ouicelaviegestion').trigger('click');
    }

    if (noncelaviegestion == 2) {
        $('#noncelaviegestion').trigger('click');
    }

    if (ouisyndic == 1) {
        $('#questionnaire2').removeClass('d-none');
        $('#ouisyndic').trigger('click');
    }

    if (nonsyndic == 2) {
        $('#questionnaire2').removeClass('d-none');
        $('#nonsyndic').trigger('click');
    }

    if (nesaitpassyndic == 3) {
        $('#questionnaire2').removeClass('d-none');
        $('#nesaitpassyndic').trigger('click');
    }

    if (ouiclient == 1) {
        $('#questionnaire3').removeClass('d-none');
        $('#ouiclient').trigger('click');
    }

    if (nonclient == 2) {
        $('#questionnaire3').removeClass('d-none');
        $('#nonclient').trigger('click');
    }
    if (nesaitpasclient == 3) {
        $('#questionnaire3').removeClass('d-none');
        $('#nesaitpasclient').trigger('click');
    }

}

$(document).on('submit', '#CreerPno', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourCreerPno", loading);
});

function retourCreerPno(response) {
    if (response.status == true) {
        myModal.toggle();
        getLotPnoProspect(response.data_retour.lot_id, response.data_retour.prospect_id);
    } else {
        fn.response_control_champ(response);
    }
}

/********Aperçu PNO********/

function apercuPno(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = pno_prospect_url.getPathPno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsePathPno');
}

function responsePathPno(response) {
    var el_apercu = document.getElementById('apercu_doc_pno')
    var dataPath = response[0].pno_path
    var id_document = response[0].pno_id
    apercuDocPno(dataPath, el_apercu, id_document);
}

function apercuDocPno(data, el, id_doc) {
    viderApercuPno();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuPno').appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuPno').appendChild(element);
    }
}

function viderApercuPno() {
    const doc_selectionne = document.getElementsByClassName("apercu_Pno");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuPno") != undefined) {
        if (document.getElementById("apercuPno").querySelector("#view_apercu") != null) {
            document.getElementById("apercuPno").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuPno").querySelector("#view_apercu").remove();
        }

    }
}

$(document).on('click', '#removefilePno', function () {
    $('.doc').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id: $('#lot_id').val()
    };

    var url_request = pno_prospect_url.remove_file;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefile');
});

function retour_removefile(response) {
    const obj = JSON.parse(response);
    $('.pno_file').addClass('d-none');
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

/****envoyer pno******/

$(document).on('click', '.sendPnoCli', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        pno_id: $(this).attr('data-id'),
    };
    sendPnoCli(data_request);
});

function sendPnoCli(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = pno_prospect_url.sendPno;
    var loading = {
        type: "content",
        id_content: "contenair-pno"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'sendPnoCallBack', loading);
}
function sendPnoCallBack(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        pno_id: response.data.id
                    }
                    sendPnoCli(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var lot_id = $("#lot_id").val();
            var prospect_id = $("#prospect_id").val();
            
            getLotPnoProspect(lot_id,prospect_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/***Signer pno*****/

$(document).on('click', '.signerPnoCli', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        pno_id: $(this).attr('data-id'),
        lot_id : $("#lot_id").val(),
        prospect_id : $("#prospect_id").val()
    };
    var url_request = pno_prospect_url.SignerPno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalPno');
});

$(document).on('submit', '#AddSignPno', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#path_pno').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;

    if (required_data == true) {      
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var lot_id = $("#lot_id").val();
                    var prospect_id = $("#prospect_id").val();
            
                    getLotPnoProspect(lot_id, prospect_id);
                    Notiflix.Notify.success("Modification enregistrée", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

/****clos sans suite pno******/

$(document).on('click', '.clos_sans_suitePno', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        pno_id: $(this).attr('data-id'),
    };
    ClorePnoCli(data_request);
});

function ClorePnoCli(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = pno_prospect_url.ClorePnoCli;
    var loading = {
        type: "content",
        id_content: "contenair-pno"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'clorePnoCallBack', loading);
}
function clorePnoCallBack(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        pno_id: response.data.id
                    }
                    ClorePnoCli(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {

            var lot_id = $("#lot_id").val();
            var prospect_id = $("#prospect_id").val();
            
            getLotPnoProspect(lot_id,prospect_id);

            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/*******envoi mail********/

$(document).on('click', '#send_mail_pno', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id : $('#lot_id').val(),
        prospect_id : $('#prospect_id').val(),
        pno_id : $(this).attr('data-id')
    };
    var url_request = pno_prospect_url.EnvoiMailPno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

var Modal = new bootstrap.Modal(document.getElementById("FormModal"), {
    keyboard: false
});

function chargerModal(response) {
    $("#modalContent").html(response);
    Modal.toggle();
}

/*******envoi mail très chère********/

$(document).on('click', '#send_mail_pno_chere', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id : $('#lot_id').val(),
        prospect_id : $('#prospect_id').val(),
        pno_id : $(this).attr('data-id')
    };
    var url_request = pno_prospect_url.EnvoiMailPnoChere;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModal');

});

/*******clôturer PNO**********/

$(document).on('click', '.cloturerPno', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id : $('#lot_id').val(),
        prospect_id : $('#prospect_id').val(),
        pno_id : $(this).attr('data-id')
    };
    var url_request = pno_prospect_url.cloturerPno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalPno');
});

$(document).on('submit', '#CloturePno', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file = $('#pno_fichier_cloture').prop('files')[0];
    fd.append("file", file);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });
    var required_data = true;
    
    if (file == null) {
        $('div').children('.error_file').text("Veuillez renseigner le fichier justificatif");
        //Notiflix.Notify.failure("Veuillez renseigner le fichier justificatif", { position: 'left-bottom', timeout: 3000 });
    }

    if (required_data == true) {      
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    myModal.toggle();
                    var lot_id = $("#lot_id").val();
                    var prospect_id = $("#prospect_id").val();
                    getLotPnoProspect(lot_id,prospect_id);
                   
                    Notiflix.Notify.success("PNO clôturé avec succès", { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});


/*******Alert clôture PNO**********/

$(document).on('click', '.alertCloturePno', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        lot_id : $('#lot_id').val(),
        prospect_id : $('#prospect_id').val(),
        pno_id : $(this).attr('data-id')
    };
    var url_request = pno_prospect_url.alertCloturePno;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalPno');
});

