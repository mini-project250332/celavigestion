var dossier_url = {
    headerDossier: `${const_app.base_url}Dossiers/getheaderlist`,
    listDossier: `${const_app.base_url}Dossiers/getListDossier`,
    getListeLoyerbyDossier: `${const_app.base_url}Dossiers/getListeLotbyDossier`,
    getDocumentDossier: `${const_app.base_url}Dossiers/getDocumentDossier`,
    formUpload: `${const_app.base_url}Dossiers/formUpload`,
    getDocpath: `${const_app.base_url}Dossiers/getDocpath`,
    modalupdateDocDossier: `${const_app.base_url}Dossiers/FormUpdateDossier`,
    delete_docDossier: `${const_app.base_url}Dossiers/removeDocDossier`,
    getFormLot: `${const_app.base_url}Dossiers/getFormLot`,
    formAlertDeleteDossier: `${const_app.base_url}Dossiers/formAlertDeleteDossier`,
    Alerte_cin: `${const_app.base_url}Dossiers/Alerte_cin`,
    getFacturation: `${const_app.base_url}Dossiers/getFacturation`,
    removefilerib: `${const_app.base_url}Dossiers/removefilerib`,
    export_csv_dossier: `${const_app.base_url}Dossiers/export_csv_dossier`,

    infoReglement: `${const_app.base_url}Dossiers/infoReglement`,
    pageReglementDossier: `${const_app.base_url}Dossiers/pageReglementDossier`,
    ValiderFactureAvoir: `${const_app.base_url}Dossiers/verifierFacture_avoir`,
    SaveFactureAvoir: `${const_app.base_url}Dossiers/save_FactureAvoir`,
    pageRemboursementAvoir: `${const_app.base_url}Dossiers/pageRemboursementAvoir`,

    pageRemboursement: `${const_app.base_url}Dossiers/pageRemboursement`,
    pageRejetPrelevement: `${const_app.base_url}Dossiers/pageRejetPrelevement`,


}

var timer = null;

$(document).ready(function () {

});

function pgDossiers() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-menu-dossier"
    }
    var url_request = dossier_url.headerDossier;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_pageDossier', loading);
}

function charger_pageDossier(response) {
    $('#contenair-menu-dossier').html(response)
}

$(document).on('keyup', '#filtre_dossier_menu', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listDossier();
    }, 800);
});

$(document).on('keyup', '#filtre_siret_menu', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listDossier();
    }, 800);
});

$(document).on('keyup', '#text_filtre_dossier', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listDossier();
    }, 800);
});

$(document).on('change', '#gestionnaire_id_doss', function (e) {
    e.preventDefault();
    listDossier();
});

$(document).on('keyup', '#filtre_programme', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listDossier();
    }, 800);
});

$(document).on('keyup', '#filtre_mandat_menu', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        listDossier();
    }, 800);
});

$(document).on('change', '#acompte_tva', function (e) {
    e.preventDefault();
    listDossier();
});

$(document).on('change', '#filtre_util_dossier', function (e) {
    e.preventDefault();
    listDossier();
});

function listDossier() {
    var type_request = 'POST';
    var type_output = 'html';
    var filtre_dossier_menu = $('#filtre_dossier_menu').val();
    var filtre_siret_menu = $('#filtre_siret_menu').val(),
        gestionnaire_id_doss = $('#gestionnaire_id_doss').val(),
        date_filtre = $('#date_filtre').val(),
        filtre_programme = $('#filtre_programme').val(),
        text_filtre_dossier = $('#text_filtre_dossier').val(),
        filtre_mandat_menu = $('#filtre_mandat_menu').val(),
        acompte_tva = $('#acompte_tva').val();
    var row_view = $('#row_view_dossier').val();
    var pagination_view = $('#pagination_view_dossie').val();
    $row_data = (typeof row_view !== 'undefined') ? row_view : 200;
    $pagination_page = (typeof pagination_view !== 'undefined') ? (($.trim(pagination_view) != "") ? pagination_view : `0-${$row_data}`) : `0-${$row_data}`;
    var data_request = {
        text_filtre_dossier: text_filtre_dossier,
        filtre_dossier_menu: filtre_dossier_menu,
        filtre_siret_menu: filtre_siret_menu,
        gestionnaire_id_doss: gestionnaire_id_doss,
        date_filtre: date_filtre,
        filtre_programme: filtre_programme,
        filtre_mandat_menu: filtre_mandat_menu,
        row_data: $row_data,
        pagination_page: $pagination_page,
        acompte_tva: acompte_tva,
        util_id: $('#filtre_util_dossier').val(),
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-dossier"
    }
    var url_request = dossier_url.listDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listDossier', loading);
}

function load_listDossier(response) {
    $('#data-list').html(response);
}

/***** Export CSV */

function forceDownload_csv_dossier(url, fileName) {
    var link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

$(document).on('click', '#export-Excel-Dossier', function () {
    var type_request = 'POST';
    var type_output = 'html';

    var filtre_dossier_menu = $('#filtre_dossier_menu').val();
    var filtre_siret_menu = $('#filtre_siret_menu').val(),
        gestionnaire_id_doss = $('#gestionnaire_id_doss').val(),
        date_filtre = $('#date_filtre').val(),
        filtre_programme = $('#filtre_programme').val(),
        text_filtre_dossier = $('#text_filtre_dossier').val(),
        filtre_mandat_menu = $('#filtre_mandat_menu').val(),
        acompte_tva = $('#acompte_tva').val();

    var data_request = {
        text_filtre_dossier: text_filtre_dossier,
        filtre_dossier_menu: filtre_dossier_menu,
        filtre_siret_menu: filtre_siret_menu,
        gestionnaire_id_doss: gestionnaire_id_doss,
        date_filtre: date_filtre,
        filtre_programme: filtre_programme,
        filtre_mandat_menu: filtre_mandat_menu,
        acompte_tva: acompte_tva,
        util_id: $('#filtre_util_dossier').val(),
    };


    var loading = {
        type: "content",
        id_content: "contenair-menu-dossier"
    }

    var url_request = dossier_url.export_csv_dossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'exporter_Dossier_csv', loading);
});

function exporter_Dossier_csv(response) {
    var parts = response.split('/');
    forceDownload_csv_dossier(`${const_app.base_url}` + response, parts[3]);
}

/***** pagination *****/

$(document).on('click', '.pagination-vue-table-dossier', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
        var limit = `${$(this).attr('data-offset')}-${$('#row_view_dossier').val()}`;
        $('#pagination_view_dossie').val(limit);
        listDossier();
    }
});


$(document).on('change', '.row_view_dossier', function (e) {
    e.preventDefault();
    listDossier();
});

$(document).on('click', '#pagination-vue-next-dossier', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-dossier .pagination-vue-table-dossier");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_last = array_offset[array_offset.length - 1];
    if (value_last != current_active) {
        index_next = array_offset.indexOf(current_active) + 1;
        $(`#pagination-vue-table-dossier-${array_offset[index_next]}`).trigger("click");
    }
});

$(document).on('click', '#pagination-vue-preview-dossier', function (e) {
    e.preventDefault();
    var $j_object = $("#pagination-ul-dossier .pagination-vue-table-dossier");
    var current_active = null;
    var array_offset = [];

    $j_object.each(function () {
        if ($(this).hasClass('active')) {
            current_active = $(this).attr('data-offset');
        }
        array_offset.push($(this).attr('data-offset'));
    });

    var value_first = array_offset[0];
    if (value_first != current_active) {
        index_prev = array_offset.indexOf(current_active) - 1;
        $(`#pagination-vue-table-dossier-${array_offset[index_prev]}`).trigger("click");
    }
});

/****filtre****/

$(document).on('click', '.fiche-dossier', function (e) {
    var dossier_id = $(this).attr('data-id');
    var client_id = $(this).attr('data-client_id');
    var nom = $(this).attr('data-nom');

    var href = $('#proprietaire-menu-web').attr('href');
    window.location.href = href + `?_p@roprietaire=${client_id}&_do@t_id=${dossier_id}&_nom=${nom} `;
});

$(document).on('click', '.alert_cin', function () {
    var data_request = {
        'nom': $(this).attr('data-nom')
    }
    var type_request = 'POST';
    var url_request = dossier_url.Alerte_cin;
    var type_output = 'html';

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getAlerte_cinCallback');
    $("#FormModal").modal('show');
});

function getAlerte_cinCallback(response) {
    $("#modalContent").html(response);
}

function getListeLotbyDossier(dossier_id, client_id) {
    var data_request = {
        'dossier_id': dossier_id,
        'client_id': client_id,
    }
    var type_request = 'POST';
    var url_request = dossier_url.getListeLoyerbyDossier;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-dossier-lot"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getListeDossierCallback', loading);
}
function getListeDossierCallback(response) {
    $('#contenair-dossier-lot').html(response);
}

function getListDocumentDossier(dossier_id) {
    var data_request = {
        'dossier_id': dossier_id
    }
    var type_request = 'POST';
    var url_request = dossier_url.getDocumentDossier;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-dossier-document"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getListDocumentDossierCallback', loading);
}

function getListDocumentDossierCallback(response) {
    $('#contenair-dossier-document').html(response);
}

function apercuDocument_dossier(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = dossier_url.getDocpath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseDOcpath');
}


function charger_contentModal_Alert(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('click', '.btnUpdtadeDocDossier', function (e) {
    e.preventDefault();
    var doc_id = $(this).attr("data-id");
    var dossier_id = $(this).attr("data-dossier");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_id: doc_id, dossier_id: dossier_id };
    var url_request = dossier_url.modalupdateDocDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal_Alert');
});

$(document).on('submit', '#UpdateDocumentDossiers', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocdossier", "null_fn", param);
});

function retourupdatedocdossier(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getListDocumentDossier(response.data_retour.dossier_id);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '#ajoutdoc_dossier', function (e) {
    e.preventDefault();
    ajoutDoc_Dossier($(this).attr('data-id'), 'add', null);
});

function ajoutDoc_Dossier(dossier_id, action, doc_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'dossier_id': dossier_id,
        'doc_id': doc_id
    };
    var url_request = dossier_url.formUpload;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentModal_Dossier');

    $("#modalAjoutDoc_dossier").modal('show');
}

function chargercontentModal_Dossier(response) {
    $('#contenaireUpload_dossier').html(response);
}

$(document).on('click', '.btnSuppDocDossier', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_id: $(this).attr('data-id')
    };
    delete_docDossier(data_request);
});

function delete_docDossier(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = dossier_url.delete_docDossier;
    var loading = {
        type: "content",
        id_content: "contenair-dossier-document"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocDossier', loading);
}

function retourDeleteDocDossier(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_id: response.data.id
                    }
                    delete_docDossier(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`[id=rowdoc-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function getFormLotDossier(client_id, dossier_id, action, cliLot_id = null) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'client_id': client_id,
        'cliLot_id': cliLot_id,
        'dossier_id': dossier_id
    };
    var loading = {
        type: "content",
        id_content: "contenair-dossier-lot"
    }
    var url_request = dossier_url.getFormLot;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFormLotDossier', loading);
}
function retourFormLotDossier(response) {
    $('#contenair-dossier-lot').html(response);
}

$(document).on('click', '#btnlot_creation', function () {
    getFormLotDossier($(this).attr('data-id'), $('#dossier_id').val(), 'add', null);
});

$(document).on('click', '#annulerAddLotDossier', function () {
    getListeLotbyDossier($(this).attr('data-dossier_id'), $(this).attr('data-id'));
});

$(document).on('submit', '#cruLotDossier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier-lot"
    }
    fn.ajax_form_data_custom(form, type_output, "retourAddLotDossier", loading);
});



function retourAddLotDossier(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        var data_request = {
            cliLot_id: response.data_retour.cliLot_id,
            client_id: response.data_retour.client_id
        };
        getFicheLot(data_request);
    } else {
        fn.response_control_champ(response);
    }
}

$(document).on('click', '.btnConfirmSupLotAlert', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var url_request = dossier_url.formAlertDeleteDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModalDossier');
});

function charger_contentModalDossier(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('change', '#indivision', function () {
    if ($(this).val() == "Non") {
        $('.client_rof').addClass('d-none');
    }
    else {
        $('.client_rof').removeClass('d-none');
    }
});

function enforceMinMax(el) {
    if (el.value != "") {
        if (parseInt(el.value) < parseInt(el.min)) {
            el.value = el.min;
        }
        if (parseInt(el.value) > parseInt(el.max)) {
            el.value = el.max;
        }
    }
}

//sort table 
$(document).on("click", "table thead tr th:not(.no-sort)", function () {
    var table = $(this).parents("table");
    var rows = $(this).parents("table").find("tbody tr").toArray().sort(TableComparer($(this).index()));
    var dir = ($(this).hasClass("sort-asc")) ? "desc" : "asc";

    if ($(this).hasClass("sort-asc")) {
        $('.tri-dossier').empty();
        $('.tri-dossier').append('<i class="fas fa-sort-amount-down-alt"></i>');
    }
    else {
        $('.tri-dossier').empty();
        $('.tri-dossier').append('<i class="fas fa-sort-amount-up-alt"></i>');
    }

    if (dir == "desc") {
        rows = rows.reverse();
    }

    for (var i = 0; i < rows.length; i++) {
        table.append(rows[i]);
    }

    table.find("thead tr th").removeClass("sort-asc").removeClass("sort-desc");
    $(this).removeClass("sort-asc").removeClass("sort-desc").addClass("sort-" + dir);
});

function TableComparer(index) {
    return function (a, b) {
        var val_a = TableCellValue(a, index);
        var val_b = TableCellValue(b, index);
        var result = ($.isNumeric(val_a) && $.isNumeric(val_b)) ? val_a - val_b : val_a.toString().localeCompare(val_b);
        return result;
    }
}

function TableCellValue(row, index) {
    return $(row).children("td").eq(index).text();
}

function getFacturation(dossier_id, action) {
    var data_request = {
        'action': action,
        'dossier_id': dossier_id
    }
    var type_request = 'POST';
    var url_request = dossier_url.getFacturation;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFacturationCallback', loading);
}

function getFacturationCallback(response) {
    $('#contenair-dossier-facturation').html(response);
}

$(document).on('click', '#modifierFacturationDossier', function () {
    var dossier_id = $(this).attr('data-id');
    getFacturation(dossier_id, 'modif');
});

$(document).on('click', '#terminer_facturation', function (e) {
    getFacturation($('#fact_dossier_id').val(), 'fiche');
});

$(document).on('submit', '#updateFacturation', function (e) {
    e.preventDefault();
    var form = $(this);
    var fd = new FormData();
    var file_rib = $('#file_rib').prop('files')[0];
    fd.append("file_rib", file_rib);
    var other_data = $(this).serializeArray();
    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });

    if ($('#iban').val()) {
        if (verifyIBAN($('#iban').val()) == true) {
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: fd,
                contentType: false,
                processData: false,
                success: function (response) {
                    const obj = JSON.parse(response);
                    if (obj.status == true) {
                        Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                    }
                    else {
                        Notiflix.Notify.failure(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                    }
                }
            });
        } else {
            alert_Iban();
        }
    }
    else {
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
                else {
                    Notiflix.Notify.failure(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
            }
        });
    }
});

$(document).on('change', '#updateFacturation select', function (e) {
    $('#updateFacturation').trigger('submit');
});

var debounceTimer;

$(document).on('keyup', '#updateFacturation input', function (e) {
    e.preventDefault();

    clearTimeout(debounceTimer);

    debounceTimer = setTimeout(function () {
        $('#updateFacturation').trigger('submit');
    }, 4000);
});

$(document).on('change', '#updateFacturation input', function (e) {
    $('#updateFacturation').trigger('submit');
});

function verifyIBAN(iban) {
    iban = iban.replace(/\s+/g, "").toUpperCase();

    if (iban.length < 4 || iban.length > 34) {
        return false;
    }

    if (!(/^[A-Z]{2}/.test(iban))) {
        return false;
    }

    iban = iban.substr(4) + iban.substr(0, 4);

    var ibanDigits = "";
    for (var i = 0; i < iban.length; i++) {
        var charCode = iban.charCodeAt(i);
        if (charCode >= 65 && charCode <= 90) {
            ibanDigits += (charCode - 55).toString();
        } else {
            ibanDigits += iban.charAt(i);
        }
    }

    var remainder = Number(BigInt(ibanDigits) % 97n).toString();;

    return remainder === "1";
}

$(document).on('click', '#removefilerib', function () {
    $('.doc_rib').hide();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        dossier_id: $(this).attr('data-id')
    };
    var url_request = dossier_url.removefilerib;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_removefilerib');
});

function retour_removefilerib(response) {
    $('#doc').empty();
    const obj = JSON.parse(response);
    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
}

var reglmt_modal = new bootstrap.Modal(document.getElementById("reglmt_modal"), {
    keyboard: false
});

$(document).on('click', '.montant_reglt', function () {
    var fact_id = $(this).attr('data-id');

    infoReglement(fact_id);
});

function infoReglement(fact_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        fact_id: fact_id
    };
    var url_request = dossier_url.infoReglement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retour_infoReglement');
}

function retour_infoReglement(response) {
    $("#modal-main-content-reglmt_modal").html(response);
    reglmt_modal.toggle();
}

$(document).on('click', '.saisier_reglement_dossier', function () {
    var fact_id = $(this).attr('data-fact_id');
    var dossier_id = $(this).attr('data-dossier_id');
    pageReglementDossier(fact_id, dossier_id);
});

function pageReglementDossier(fact_id, dossier_id) {
    var type_request = 'POST';
    var url_request = dossier_url.pageReglementDossier;
    var type_output = 'html';
    var data_request = { fact_id: fact_id, dossier_id: dossier_id };
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageReglementDossier_Callback', loading);
}
function pageReglementDossier_Callback(response) {
    $('#contenair-dossier-facturation').html(response);
}

$(document).on('click', '#annulerReglementDossier', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    getFacturation(dossier_id, 'fiche');
});

$(document).on('submit', '#saveReglementDossier', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveReglementDossier", loading);
});
function retoursaveReglementDossier(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturation(response.data_retour, 'fiche');
    } else {
        fn.response_control_champ(response);
    }
}

var myModalValideFactureAvoir = new bootstrap.Modal(document.getElementById("myModalValiderFactureAvoir"), {
    keyboard: false
});

$(document).on('click', '.creerAvoirDossier', function () {

    var idfacture = $(this).attr('data-fact_id');
    var data_request = {
        idfacture: idfacture,
    }
    modalValiderFactureAvoir(data_request);
});

function modalValiderFactureAvoir(data_request) {
    var type_request = 'POST';
    var url_request = dossier_url.ValiderFactureAvoir;
    var type_output = 'html';
    var data_request = data_request;
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modalValiderFactureAvoir_Callback', loading);
}
function modalValiderFactureAvoir_Callback(response) {
    $("#modalMainFactureAvoir").html(response);
    myModalValideFactureAvoir.toggle();
}

$(document).on('click', '#confirmer_creationAvoirDossier', function () {
    var justificationValue = $('#justificationAvoir').val();

    if (justificationValue == '') {
        // Afficher un message d'erreur
        $('#justificationError').text('Veuillez fournir une justification.');
        return; // Arrêter le traitement ultérieur
    }

    // Si la justification n'est pas vide, effacer le message d'erreur (s'il y en a un) et poursuivre avec data_request
    $('#justificationError').text('');

    var data_request = {
        idfacture: $('#numFactAvoir').val(),
        annee: $('#annee_facturationImpaye').val(),
        dateFactureAvoir: $('#date_factureAvoir').val(),
        facture_justification: $('#justificationAvoir').val(),
    }
    myModalValideFactureAvoir.toggle();
    ValidationFacturerAvoir(data_request);
});

function ValidationFacturerAvoir(data) {
    var type_request = 'POST';
    var url_request = dossier_url.SaveFactureAvoir;
    var type_output = 'json';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'ValidationFacturerAvoir_Callback', loading);
}

function ValidationFacturerAvoir_Callback(response) {
    if (response.status == 200) {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        getFacturation(response.dossier_id, 'fiche');
    } else {
        Notiflix.Notify.failure(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

//Remboursement avoir
$(document).on('click', '.saisir_RemboursementsAvoir', function () {
    var fact_id = $(this).attr('data-fact_id');
    var dossier_id = $(this).attr('data-dossier_id');
    pageRemboursementAvoir(fact_id, dossier_id);
});

function pageRemboursementAvoir(fact_id, dossier_id) {
    var type_request = 'POST';
    var url_request = dossier_url.pageRemboursementAvoir;
    var type_output = 'html';
    var data_request = { fact_id: fact_id, dossier_id: dossier_id };
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageRemboursementAvoir_Callback', loading);
}
function pageRemboursementAvoir_Callback(response) {
    $('#contenair-dossier-facturation').html(response);
}

$(document).on('click', '#annulerremboursementsAvoir', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    getFacturation(dossier_id, 'fiche');
});

$(document).on('submit', '#saveRemboursementAvoir', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveRemboursementAvoir", loading);
});

function retoursaveRemboursementAvoir(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturation(response.data_retour, 'fiche');
    } else {
        fn.response_control_champ(response);
    }
}

//Remboursement règlement
$(document).on('click', '.saisir_Remboursements', function () {
    reglmt_modal.toggle();
    var Idreglement = $(this).attr('data-reglementId');
    var dossier_id = $(this).attr('data-dossier_id');
    pageRemboursement(Idreglement, dossier_id);
});

function pageRemboursement(Idreglement, dossier_id) {
    var type_request = 'POST';
    var url_request = dossier_url.pageRemboursement;
    var type_output = 'html';
    var data_request = { Idreglement: Idreglement, dossier_id: dossier_id };
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageRemboursement_Callback', loading);
}
function pageRemboursement_Callback(response) {
    $('#contenair-dossier-facturation').html(response);
}

$(document).on('click', '#annulerRemboursement', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    getFacturation(dossier_id, 'fiche');
});

$(document).on('submit', '#saveRemboursement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveRemboursement", loading);
});
function retoursaveRemboursement(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturation(response.data_retour, 'fiche');
    } else {
        fn.response_control_champ(response);
    }
}

//Rejet prelevement

$(document).on('click', '.saisir_RejetPrelevement', function () {
    reglmt_modal.toggle();
    var Idreglement = $(this).attr('data-reglementId');
    var dossier_id = $(this).attr('data-dossier_id');
    pageRejetPrelevement(Idreglement, dossier_id);
});

function pageRejetPrelevement(Idreglement, dossier_id) {
    var type_request = 'POST';
    var url_request = dossier_url.pageRejetPrelevement;
    var type_output = 'html';
    var data_request = { Idreglement: Idreglement, dossier_id: dossier_id };
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageRejetPrelevement_Callback', loading);
}
function pageRejetPrelevement_Callback(response) {
    $('#contenair-dossier-facturation').html(response);
}

$(document).on('click', '#annulerRejet_Prelevement', function () {
    var dossier_id = $(this).attr('data-dossier_id');
    getFacturation(dossier_id, 'fiche');
});

$(document).on('submit', '#saveRejet_Prelevement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-dossier-facturation"
    }
    fn.ajax_form_data_custom(form, type_output, "retoursaveRejetPrelevement", loading);
});
function retoursaveRejetPrelevement(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation, { position: 'left-bottom', timeout: 3000 });
        getFacturation(response.data_retour, 'fiche');
    } else {
        fn.response_control_champ(response);
    }
}
