
var loyer_url = {
    getLoyerFacture: `${const_app.base_url}Loyer/GetTableFacture`,
    addFactureLoyer: `${const_app.base_url}Loyer/AddFactureLoyer`,
    addEncaissement: `${const_app.base_url}Loyer/AddEncaissement`,
    getDetailEncaissement: `${const_app.base_url}Loyer/GetDetailEncaissement`,
    removeEncaissement: `${const_app.base_url}Loyer/removeEncaissement`,
    getDetailFacture: `${const_app.base_url}Loyer/GetDetailFacture`,
    getFacture: `${const_app.base_url}Facture/get_facture_loyer`,
    deleteFacture: `${const_app.base_url}Loyer/DeleteFactureLoyer`,
    deleteDocumentLoyer: `${const_app.base_url}Loyer/deleteDocumentLoyer`,
    FormUpdateDocLoyer : `${const_app.base_url}Loyer/FormUpdateDocLoyer`,
}

$(document).on('change', '#bail_loyer', function () {
    getTableFacture($(this).val(), $('option:selected', this).attr('data-pdl_id'), $('option:selected', this).attr('data-bail_loyer_variable'), $('#annee_loyer').val());
});

$(document).on('click', '#AddFactureLoyer', function () {
    const fact_avoir = $('#fact_avoir').val();
    if (fact_avoir == 1) {
        addFactureLoyer(
            $('#annee_loyer').val(),
            $('#fact_loyer_num').val(),
            $('#fact_loyer_date').val(),
            0, // $('#fact_loyer_app_ht').val()
            $('#fact_loyer_app_tva').val(),
            0, // $('#fact_loyer_app_ttc').val()
            0, // $('#fact_loyer_park_ht').val()
            $('#fact_loyer_park_tva').val(),
            0, // $('#fact_loyer_park_ttc').val()
            0, // $('#fact_loyer_charge_ht').val()
            $('#fact_loyer_charge_tva').val(),
            0, // $('#fact_loyer_charge_ttc').val()
            0, // $('#fact_loyer_produit_ht').val()
            $('#fact_loyer_produit_tva').val(),
            0, // $('#fact_loyer_produit_ttc').val()
            $('#bail_loyer').val(),
            $('#periode_semestre_id').val(),
            $('#periode_timestre_id').val(),
            $('#periode_annee_id').val(),
            $('#periode_mens_id').val(),
            $('option:selected', '#bail_loyer').attr('data-pdl_id'),
            $('option:selected', '#bail_loyer').attr('data-bail_loyer_variable'),
            $('#facture_commentaire').val(),
            $('#mode_paiement_label').val(),
            $('#echeance_paiement_label').val(),
            $('#emetteur_nom').val(),
            $('#emet_adress1').val(),
            $('#emet_adress2').val(),
            $('#emet_adress3').val(),
            $('#emet_cp').val(),
            $('#emet_ville').val(),
            $('#emet_pays').val(),
            $('#dest_nom').val(),
            $('#dest_adresse1').val(),
            $('#dest_adresse2').val(),
            $('#dest_adresse3').val(),
            $('#dest_cp').val(),
            $('#dest_ville').val(),
            $('#dest_pays').val(),
            'modal_facture',
            $('#fact_id').val(),
            true
        );
    } else {
        addFactureLoyer(
            $('#annee_loyer').val(),
            $('#fact_loyer_num').val(),
            $('#fact_loyer_date').val(),
            $('#fact_loyer_app_ht').val(),
            $('#fact_loyer_app_tva').val(),
            $('#fact_loyer_app_ttc').val(),
            $('#fact_loyer_park_ht').val(),
            $('#fact_loyer_park_tva').val(),
            $('#fact_loyer_park_ttc').val(),
            $('#fact_loyer_charge_ht').val(),
            $('#fact_loyer_charge_tva').val(),
            $('#fact_loyer_charge_ttc').val(),
            $('#fact_loyer_produit_ht').val(),
            $('#fact_loyer_produit_tva').val(),
            $('#fact_loyer_produit_ttc').val(),
            $('#bail_loyer').val(),
            $('#periode_semestre_id').val(),
            $('#periode_timestre_id').val(),
            $('#periode_annee_id').val(),
            $('#periode_mens_id').val(),
            $('option:selected', '#bail_loyer').attr('data-pdl_id'),
            $('option:selected', '#bail_loyer').attr('data-bail_loyer_variable'),
            $('#facture_commentaire').val(),
            $('#mode_paiement_label').val(),
            $('#echeance_paiement_label').val(),
            $('#emetteur_nom').val(),
            $('#emet_adress1').val(),
            $('#emet_adress2').val(),
            $('#emet_adress3').val(),
            $('#emet_cp').val(),
            $('#emet_ville').val(),
            $('#emet_pays').val(),
            $('#dest_nom').val(),
            $('#dest_adresse1').val(),
            $('#dest_adresse2').val(),
            $('#dest_adresse3').val(),
            $('#dest_cp').val(),
            $('#dest_ville').val(),
            $('#dest_pays').val(),
            'modal_facture',
            $('#fact_id').val(),
            false
        );
    }
});


function calculTva(ht, tva) {
    return result = (ht * tva) / 100;
}

function getFactureLoyer_PDF(fact_loyer_id, no_need_pdf = false) {
    var data_request = {
        'fact_loyer_id': fact_loyer_id,
        'no_need_pdf': no_need_pdf
    }
    var type_request = 'POST';
    var url_request = loyer_url.getFacture;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "container-loyer-facture"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFactureLoyer_PDF_Callback', loading);
}

function getFactureLoyer_PDF_Callback(response) {
    const obj = JSON.parse(response);
    getLoyers(obj.data_retour.year, obj.data_retour.cliLot_id);
}

function addFactureLoyer(
    fact_annee,
    fact_loyer_num,
    fact_loyer_date,
    fact_loyer_app_ht,
    fact_loyer_app_tva,
    fact_loyer_app_ttc,
    fact_loyer_park_ht,
    fact_loyer_park_tva,
    fact_loyer_park_ttc,
    fact_loyer_charge_ht,
    fact_loyer_charge_tva,
    fact_loyer_charge_ttc,
    fact_loyer_produit_ht,
    fact_loyer_produit_tva,
    fact_loyer_produit_ttc,
    bail_loyer,
    periode_semestre_id,
    periode_timestre_id,
    periode_annee_id,
    periode_mens_id,
    pdl_id,
    bail_loyer_variable,
    facture_commentaire,
    mode_paiement,
    echeance_paiement,
    emetteur_nom,
    emet_adress1,
    emet_adress2,
    emet_adress3,
    emet_cp,
    emet_ville,
    emet_pays,
    dest_nom,
    dest_adresse1,
    dest_adresse2,
    dest_adresse3,
    dest_cp,
    dest_ville,
    dest_pays,
    modal_id,
    fact_loyer_id = null,
    avoir = false
) {
    var data_request = {
        'fact_annee': fact_annee,
        'fact_loyer_num': fact_loyer_num,
        'fact_loyer_date': fact_loyer_date,
        'fact_loyer_app_ht': fact_loyer_app_ht,
        'fact_loyer_app_tva': fact_loyer_app_tva,
        'fact_loyer_app_ttc': fact_loyer_app_ttc,
        'fact_loyer_park_ht': fact_loyer_park_ht,
        'fact_loyer_park_tva': fact_loyer_park_tva,
        'fact_loyer_park_ttc': fact_loyer_park_ttc,
        'fact_loyer_charge_ht': fact_loyer_charge_ht,
        'fact_loyer_charge_tva': fact_loyer_charge_tva,
        'fact_loyer_charge_ttc': fact_loyer_charge_ttc,
        'fact_loyer_produit_ht': fact_loyer_produit_ht,
        'fact_loyer_produit_tva': fact_loyer_produit_tva,
        'fact_loyer_produit_ttc': fact_loyer_produit_ttc,
        'bail_id': bail_loyer,
        'periode_semestre_id': periode_semestre_id,
        'periode_timestre_id': periode_timestre_id,
        'periode_annee_id': periode_annee_id,
        'periode_mens_id': periode_mens_id,
        'pdl_id': pdl_id,
        'bail_loyer_variable': bail_loyer_variable,
        'facture_commentaire': facture_commentaire,
        'mode_paiement': mode_paiement,
        'echeance_paiement': echeance_paiement,
        'emetteur_nom': emetteur_nom,
        'emet_adress1': emet_adress1,
        'emet_adress2': emet_adress2,
        'emet_adress3': emet_adress3,
        'emet_cp': emet_cp,
        'emet_ville': emet_ville,
        'emet_pays': emet_pays,
        'dest_nom': dest_nom,
        'dest_adresse1': dest_adresse1,
        'dest_adresse2': dest_adresse2,
        'dest_adresse3': dest_adresse3,
        'dest_cp': dest_cp,
        'dest_ville': dest_ville,
        'dest_pays': dest_pays,
        'modal_id': modal_id,
        'fact_loyer_id': fact_loyer_id
    }

    if (!avoir && !fact_loyer_app_ht) {
        Notiflix.Notify.failure("Montant de l'appartement invalide.", {
            position: 'left-bottom',
            timeout: 3000
        });
        return true;
    }

    var type_request = 'POST';
    var url_request = loyer_url.addFactureLoyer;
    var type_output = 'json';

    var loading = {
        type: "content",
        id_content: "container-table-facture_encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addFactureLoyerCallback', loading);
}

function addFactureLoyerCallback(response) {
    if (response.status === true) {
        console.log("addFactureLoyerCallback ", response);
        $(`#${response.data_retour.request.modal_id}`).modal('hide');
        $(".modal-backdrop.fade.show").css("display", "none");
        $('.modal-backdrop').remove();
        $('#bail_loyer').val(response.data_retour.request.bail_id);
        getTableFacture(response.data_retour.request.bail_id, response.data_retour.request.pdl_id, response.data_retour.request.bail_loyer_variable, $('#annee_loyer').val());

        if (response.data_retour.fact_loyer_id) {
            getFactureLoyer_PDF(response.data_retour.fact_loyer_id);
        } else {
            getFactureLoyer_PDF(response.data_retour.request.fact_loyer_id);
        }

        // Ajout historique
        historyAjoutDansLoyer("Ajout d'une facture", response.data_retour.fact_loyer_id);
    }

}

function getTableFacture(bail_id, pdl_id, bail_loyer_variable, year, lot_id = 0) {
    var type_request = 'POST';
    var url_request = loyer_url.getLoyerFacture;
    var type_output = 'html';

    var data_request = {
        'bail_id': bail_id,
        'pdl_id': pdl_id,
        'bail_loyer_variable': bail_loyer_variable,
        'year': year,
        'lot_id': lot_id
    };

    var loading = {
        type: "content",
        id_content: "container-table-facture_encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getLoyerFactureCallback', loading);
}

function getLoyerFactureCallback(response) {
    var bail_facturation_loyer_celaviegestion = response.substring(0, 10).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#btnAddFactureLoyer').removeClass('d-none');
    if(bail_facturation_loyer_celaviegestion == 0)
    {
        $('#btnAddFactureLoyer').addClass('d-none');
    }
    var etat_mandat_id = $('#etat_mandat_id').val();
    const date_now = new Date();
    var mandat_date_fin = new Date($('#mandat_date_fin').val());
    if ((etat_mandat_id == 6 && mandat_date_fin < date_now) || etat_mandat_id == 7) {
        $('#btnAddNewEncaissement').addClass('d-none');
        $('#btnAddFactureLoyer').addClass('d-none');
    }
    $('#container-table-facture_encaissement').html(html);
}

$(document).on('click','#btnAddFactureLoyer',function(){
    $('#periode_timestre_id').prop("disabled", false);
    $('#periode_annee_id').prop("disabled", false);
    $('#periode_semestre_id').prop("disabled", false);
    $('#periode_mens_id').prop("disabled", false);

    $('#fact_loyer_app_ht').prop("disabled", false);
    $('#fact_loyer_park_ht').prop("disabled", false);
    $('#fact_loyer_charge_ht').prop("disabled", false);

    $('#fact_loyer_app_tva').prop("disabled", false);
    $('#fact_loyer_park_tva').prop("disabled", false);
    $('#fact_loyer_charge_tva').prop("disabled", false);
    $('#fact_loyer_app_tva_pource').prop("disabled", true);
    $('#fact_loyer_park_tva_pource').prop("disabled", true);
    $('#fact_loyer_charge_tva_pource').prop("disabled", true);
    $('#fact_loyer_app_ttc').prop("disabled", true);
    $('#fact_loyer_park_ttc').prop("disabled", true);
    $('#fact_loyer_charge_ttc').prop("disabled", true);
});

function addEncaissement(
    enc_date, enc_periode, enc_montant, enc_mode, enc_annee, bail_id, pdl_id, bail_loyer_variable, enc_id = null,
    enc_app_ttc, enc_park_ttc, enc_charge_ttc, enc_charge_deductible_ttc, enc_produit_ttc, enc_total_ttc,
    enc_app_tva, enc_park_tva, enc_charge_tva, enc_charge_deductible_tva, enc_produit_tva,
    enc_app_montant_tva, enc_park_montant_tva, enc_charge_montant_tva,
    enc_charge_deductible_montant_tva, enc_produit_montant_tva, enc_total_montant_tva,
    enc_app_ht, enc_park_ht, enc_charge_ht, enc_charge_deductible_ht, enc_produit_ht,
    enc_total_ht, enc_commentaire, tpchrg_deduct_id = 1, tpchrg_prod_id = 1
) {
    var data_request = {
        'enc_id': enc_id,
        'enc_date': enc_date,
        'enc_periode': enc_periode,
        'enc_annee': enc_annee,
        'enc_montant': enc_montant,
        'enc_mode': enc_mode,
        'bail_id': bail_id,
        'modal_id': 'modal_encaissement',
        'pdl_id': pdl_id,
        'bail_loyer_variable': bail_loyer_variable,
        'enc_app_ttc': enc_app_ttc,
        'enc_park_ttc': enc_park_ttc,
        'enc_charge_ttc': enc_charge_ttc,
        'enc_charge_deductible_ttc': enc_charge_deductible_ttc,
        'enc_produit_ttc': enc_produit_ttc,
        'enc_total_ttc': enc_total_ttc,
        'enc_app_tva': enc_app_tva,
        'enc_park_tva': enc_park_tva,
        'enc_charge_tva': enc_charge_tva,
        'enc_charge_deductible_tva': enc_charge_deductible_tva,
        'enc_produit_tva': enc_produit_tva,
        'enc_app_montant_tva': enc_app_montant_tva,
        'enc_park_montant_tva': enc_park_montant_tva,
        'enc_charge_montant_tva': enc_charge_montant_tva,
        'enc_charge_deductible_montant_tva': enc_charge_deductible_montant_tva,
        'enc_produit_montant_tva': enc_produit_montant_tva,
        'enc_total_montant_tva': enc_total_montant_tva,
        'enc_app_ht': enc_app_ht,
        'enc_park_ht': enc_park_ht,
        'enc_charge_ht': enc_charge_ht,
        'enc_charge_deductible_ht': enc_charge_deductible_ht,
        'enc_produit_ht': enc_produit_ht,
        'enc_total_ht': enc_total_ht,
        'enc_commentaire': enc_commentaire,
        'tpchrg_deduct_id': tpchrg_deduct_id,
        'tpchrg_prod_id': tpchrg_prod_id
    }

    var type_request = 'POST';
    var url_request = loyer_url.addEncaissement;
    var type_output = 'json';

    var loading = {
        type: "content",
        id_content: "container-table-facture_encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addEncaissementCallback', loading);
}

function addEncaissementCallback(response) {
    if (response.status === true) {
        console.log(response.data_retour);
        $(`#${response.data_retour.request.modal_id}`).modal('hide');
        $(`.modal-backdrop.fade.show`).css('display', 'none');
        $('#bail_loyer').val(response.data_retour.request.bail_id);
        getTableFacture(response.data_retour.request.bail_id, response.data_retour.request.pdl_id, response.data_retour.request.bail_loyer_variable, response.data_retour.request.enc_annee);
        historyAjoutDansLoyer("Ajout d'un encaissement", response.data_retour.fact_loyer_id);
    }
}

// Getv detail encaissement
function getDetailEncaissement(
    bail_id, enc_annee, enc_periode
) {
    var data_request = {
        'bail_id': bail_id,
        'enc_annee': enc_annee,
        'enc_periode': enc_periode,
    }

    var type_request = 'POST';
    var url_request = loyer_url.getDetailEncaissement;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contain-list-detail-encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDetailEncaissementCallback', loading);
}

function getDetailEncaissementCallback(response) {
    $('#contain-list-detail-encaissement').html(response);
}

// Get detail facture
function getDetailFacture(
    bail_id, facture_annee, periode, periode_id, cli_lot_id
) {
    var data_request = {
        'bail_id': bail_id,
        'facture_annee': facture_annee,
        'periode': periode,
        'periode_id': periode_id,
        'cli_lot_id': cli_lot_id
    }

    var type_request = 'POST';
    var url_request = loyer_url.getDetailFacture;
    var type_output = 'html';

    var loading = {
        type: "content",
        id_content: "contain-list-detail-encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getDetailFactureCallback', loading);
}

function getDetailFactureCallback(response) {
    $('#contain-list-detail-facture').html(response);
}


// Delete encaissement
function removeEncaissement(enc_id, action = "demande") {
    var data = {
        action: action,
        enc_id: enc_id
    };
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = loyer_url.removeEncaissement;
    var loading = {
        type: "content",
        id_content: "contain-list-detail-encaissement"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteEncaissement', loading);
}

function retourDeleteEncaissement(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    const enc_id = response.data.id;
                    removeEncaissement(enc_id, "confirm");
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            const bail_id = $("#detail_bail_id").val();
            const pdl_id = $("#detail_pdl_id").val();
            const enc_periode = $("#detail_enc_periode").val();
            const enc_annee = $("#detail_enc_annee").val();
            const bail_loyer_variable = $("#detail_bail_loyer_variable").val();

            getDetailEncaissement(bail_id, enc_annee, enc_periode);
            getTableFacture(bail_id, pdl_id, bail_loyer_variable, enc_annee);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });

            historySupprDansLoyer("Suppression d'un encaissement");
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

// Delete Facture
function deleteFacture(fact_id, action = "demande") {
    var data = {
        action: action,
        fact_id: fact_id
    };
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = loyer_url.deleteFacture;
    var loading = {
        type: "content",
        id_content: "container-table-facture_encaissement"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteFacture', loading);
}

function retourDeleteFacture(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    const fact_id = response.data.id;
                    deleteFacture(fact_id, "confirm");
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            const bail_id = $("#detail_facture_bail_id").val();
            const pdl_id = $("#detail_facture_pdl_id").val();
            const enc_annee = $("#detail_facture_enc_annee").val();
            const bail_loyer_variable = $("#detail_facture_bail_loyer_variable").val();
            const periode = $("#detail_facture_periode").val();
            const periode_id = $("#detail_facture_periode_id").val();
            const cli_lot_id = $("#cli_lot_id").val();

            getTableFacture(bail_id, pdl_id, bail_loyer_variable, enc_annee);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getDetailFacture(bail_id, enc_annee, periode, periode_id, cli_lot_id);

            historySupprDansLoyer("Suppression d'une facture");

        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '.retourDossier', function (e) {
    e.preventDefault();
    var data_request = {
        dossier_id: $(this).attr('data-dossier_id'),
        client_id: $(this).attr('data-client_id')
    }
    getFicheDossier(data_request);
});

// Delete doc loyer
$(document).on('click', '#deleteDocLoyer', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_loyer_id: $(this).attr('data-doc_loyer_id')
    };
    deleteDocumentLoyer(data_request);
});

function deleteDocumentLoyer(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = loyer_url.deleteDocumentLoyer;
    var loading = {
        type: "content",
        id_content: "container-loyer-facture"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteDocumentLoyer', loading);
}

function retourDeleteDocumentLoyer(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_loyer_id: response.data.doc_loyer_id
                    }
                    deleteDocumentLoyer(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getLoyers($("#annee_loyer").val(), $('#cli_lot_id').val());
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

$(document).on('click', '.renommer-fact', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        action: "edit",
        doc_loyer_id: $(this).attr('data-id'),
        cliLot_id : $('#cli_lot_id').val(),
        annee_loyer : $("#annee_loyer").val()
    };
    var url_request = loyer_url.FormUpdateDocLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargerModalFact');
});

function chargerModalFact(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#UpdateDocument', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "returnUpdatedocFat", "null_fn", param);
});

function returnUpdatedocFat(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getLoyers($("#annee_loyer").val(),response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}


