var com_url = {
    headerCommunications: `${const_app.base_url}Communications/getheaderlist`,
    listCommunications: `${const_app.base_url}Communications/listCommunications`,
    get_FormNewCommunications: `${const_app.base_url}Communications/getFormNewCommunications`,
    get_FicheCommunications: `${const_app.base_url}Communications/getFicheCommunications`,
    getInformationComs : `${const_app.base_url}Communications/getInformationComs`,
    getContenuComs : `${const_app.base_url}Communications/getContenuComs`,
    getDestinataireComs : `${const_app.base_url}Communications/getDestinataireComs`,
    getFormDestinataireComs : `${const_app.base_url}Communications/getFormDestinataireComs`,
    loadDestinataire : `${const_app.base_url}Communications/loadDestinataire`,
    getApercuComs : `${const_app.base_url}Communications/getApercuComs`,
    valideEnvoyeComs : `${const_app.base_url}Communications/valideEnvoyeComs`,
    valided_operationMail : `${const_app.base_url}Communications/valided_operationMail`,
}

var timer = null;

function pageComsCallBack(response) {
    $('#contenair-menu-communications').html(response)
}

/****** ******/

function pgCommunications() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.headerCommunications;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageComsCallBack', loading);
}

function listCommunications(){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        statut_com : $('#statut-com').val()
    };

    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.listCommunications;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadlistComs', loading);
}

$(document).on('change', '#statut-com', function (e) {
    e.preventDefault();
    listCommunications();
});

function loadlistComs(response) {
    $('#data-list').html(response);
}

/****** form coms *****/

$(document).on('click', '#add-coms', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }

    var url_request = com_url.get_FormNewCommunications;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageComsCallBack', loading);
});

$(document).on('click', '#annuler-add-coms', function () {
    pgCommunications();
});

$(document).on('submit', '#cruCommunications', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    fn.ajax_form_data_custom(form, type_output, "returnCruComs", loading);
});

function returnCruComs(response){
    if (response.status == true) {
        var data_request = {
            coms_id: response.data_retour.coms_id
        };
        getFicheComs(data_request);
    } else {
        fn.response_control_champ(response);
    }
}

/****** ****/
$(document).on('click', '#retourListeCom', function () {
    pgCommunications();
});

/****** *****/

$(document).on('click', '.btnFicheCom', function () {
    var data_request = {
        coms_id: $(this).attr('data-id')
    };
    getFicheComs(data_request);
});

function getFicheComs(data) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = data;
    var loading = {
        type: "content",
        id_content: "contenair-menu-communication"
    }
    var url_request = com_url.get_FicheCommunications;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'pageComsCallBack', loading);
}


/****** *****/

function get_informationComs(page){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'coms_id': $("#coms_id_nav").val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.getInformationComs;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnFicheCom', loading);
}

function returnFicheCom(response){
    $('#tab-communication').html(response);
}


$(document).on('click', '#update-creation-coms', function () {
    get_informationComs('form');
});

$(document).on('click', '#annuler-update-coms', function () {
    get_informationComs('fiche');
});

$(document).on('submit', '#updateCommunications', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    fn.ajax_form_data_custom(form, type_output, "returnUpdateComs", loading);
});

function returnUpdateComs(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        get_informationComs('fiche');
    } else {
        fn.response_control_champ(response);
    }
}


/***** *****/

function getContenuComs(page){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'coms_id': $("#coms_id_nav").val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.getContenuComs;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnContenuComs', loading);
}

function returnContenuComs(response){
    $('#tab-contenu').html(response);
}

$(document).on('click', '#update-contenu-coms', function () {
    getContenuComs('form');
});

$(document).on('click', '#annuler-contenu-coms', function () {
    getContenuComs('fiche');
});

$(document).on('change', '#coms_typeContenu', function () {
    let typeContenu = $(this).val();
    if(typeContenu == 1){
        $('#dynamic-message').addClass('d-none');
        //$('#divChampsFision').addClass('d-none');
        //$('#coms_contenu').css("height", "200px");
    }else{
        $('#dynamic-message').removeClass('d-none');
        //$('#divChampsFision').removeClass('d-none');
        //$('#coms_contenu').css("height", "515px");
    }
});

$(document).on('submit', '#getModelMail', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output, "returnChargerModeleContenu");
});

function returnChargerModeleContenu(response){
    $('#coms_contenu').val(response.pmail_contenu);
    modal_modelMail.toggle();
}


$(document).on('submit', '#cruContenuComs', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function() {
            loading_fn(loading);
        },
        success: function(response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                getContenuComs('fiche')
            } else {
                fn.response_control_champ(response);
            }
        },
        complete: function() {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});

/****** *****/

function getDestinataireComs(page){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'coms_id': $("#coms_id_nav").val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.getDestinataireComs;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDestinataireComs', loading);
}

function returnDestinataireComs(response){
    $('#tab-destinataire').html(response);
}

/*
    $(document).on('click', '#modal-formDestinataire-coms', function (e) {
        var type_request = 'POST';
        var type_output = 'html';
        var data_request = {
            'coms_id': $("#coms_id_nav").val()
        };
        var loading = {
            type: "content",
            id_content: "contenair-modale-destinataire"
        }
        var url_request = com_url.getFormDestinataireComs;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'returngetModalDestinataireComs', loading);
    });

    function returngetModalDestinataireComs(response){
        $('#body-modalForm-destinataire').html(response);
        modalForm_destinataire.toggle();
    }
*/

$(document).on('click', '#btn-formDestinataire-coms', function (e) {
    getDestinataireComs('form')       
});

$(document).on('click', '#fermer-form-destinataire', function (e) {
    getDestinataireComs('fiche')       
});

/****** ******/

var filtres = {
    "filtre-propretiaire-des": "clientetat",
    "filtre-programme-des": "program",
    "filtre-utilisateur-des": "suiviclient",
    "filtre-mandat-des": "etatmandat"
};

// Fonction de filtrage
function filtrerTableau() {
    var valeurs = {};
    for (var selectId in filtres) {
        valeurs[selectId] = $("#" + selectId).val();
    }
    var nombreResultats = 0;
    $("#proprietaire-destinataire tbody tr").each(function () {
        var ligne = $(this);
        var afficher = true;
        for (var selectId in filtres) {
            var selectValue = valeurs[selectId];

            var attribut = ligne.data(filtres[selectId]);

            if (selectValue !== "0" && selectValue !== attribut.toString()) {
                afficher = false;
                break;
            }
        }
        if (afficher) {
            ligne.show();
            nombreResultats++;
        } else {
            ligne.hide();
        }
    });
    $("#nombre-resultats").text(nombreResultats);
    // checkbox all result
    var checkbox_allResult = $("#check-resultat").prop("checked");
    if(checkbox_allResult){
        $(".client_row:visible").prop("checked", checkbox_allResult);
    }
}

$(document).on('change', '#filtre-propretiaire-des, #filtre-programme-des, #filtre-utilisateur-des, #filtre-mandat-des',filtrerTableau);

$(document).on('change', '#check-resultat', function (e) {
    e.preventDefault();
    var isChecked = $(this).prop("checked");
    $(".client_row:visible").prop("checked", isChecked);
    if(!isChecked)
        $(".client_row").prop("checked", isChecked);
});

$(document).on('change', '.client_row', function (e) {
    e.preventDefault();
    var allChecked = $(".client_row:visible").length === $(".client_row:checked:visible").length;
    $("#check-resultat").prop("checked", allChecked);
});

function recuperer_destinataireChecked() {
    var valeurs = [];
    $(".client_row:checked").each(function () {
        var valeur = $(this).val();
        valeurs.push(valeur);
    });
    return valeurs;
}

$(document).on('click', '#charger-destinataire-mail', function (e) {
    e.preventDefault();
    var destinataireChecked = recuperer_destinataireChecked();
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'destinataire' : JSON.stringify(destinataireChecked),
        'coms_id': $("#coms_id_nav").val()
    };

    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.loadDestinataire;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnLoadDestinataire', loading);
});

function returnLoadDestinataire(response){
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDestinataireComs("fiche");
        //modalForm_destinataire.toggle();
    } else {
        if(response.type = "warning")
            Notiflix.Notify.warning(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        else
            Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }   
}

/****** ******/

function getApercuComs(page){
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'page': page,
        'coms_id': $("#coms_id_nav").val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.getApercuComs;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnApercuComs', loading);
}

function returnApercuComs(response) {
    $('#tab-apercu').html(response);
}

$(document).on('click', '#valider-coms', function (e) {
    e.preventDefault();
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'coms_id': $("#coms_id_nav").val()
    };
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    var url_request = com_url.valided_operationMail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnValiderComs', loading);

});

function returnValiderComs(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getApercuComs("fiche");
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/****** *****/
//
$(document).on('click', '#valider-envoie-coms', function (e) {
    e.preventDefault();
    var data = {
        'action': "demande",
        'coms_id': $("#coms_id_nav").val()
    };
    valideEnvoyeComs(data);
});

function valideEnvoyeComs(data){
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data;
    var url_request = com_url.valideEnvoyeComs;
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnValidEnvoyeComs', loading);
}

function returnValidEnvoyeComs(response){
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        'action': "confirm",
                        'coms_id': $("#coms_id_nav").val()
                    }
                    valideEnvoyeComs(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getApercuComs("fiche");
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/****** *******/

function getContenutNavCom(contenu){
    var page = "fiche";
    switch(contenu) {
        case "communication":
            get_informationComs(page);
            break;
        case "contenu":
            getContenuComs(page);
            break;
        case "destinataire":
            getDestinataireComs(page);
            break;
        case "apercu":
            getApercuComs(page);
            break;
    }
}