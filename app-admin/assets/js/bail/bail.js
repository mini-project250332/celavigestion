var bail_url = {
    addBail: `${const_app.base_url}Bail/AddBail`,
    getBail: `${const_app.base_url}Bail/GetBail`,
    addIndexationLoyer: `${const_app.base_url}Bail/AddIndexationLoyer`,
    getIndexationLoyer: `${const_app.base_url}Bail/GetIndexationLoyer`,
    getLoyerReference: `${const_app.base_url}Bail/GetLoyerReference`,
    addFranciseLoyer: `${const_app.base_url}Bail/AddFranciseLoyer`,
    getFranciseLoyer: `${const_app.base_url}Bail/GetFranciseLoyer`,
    deleteFranchise: `${const_app.base_url}Bail/removeFranchise`,
    deleteIndexation: `${const_app.base_url}Bail/removeIndexation`,
    getLoyer: `${const_app.base_url}Loyer/getLoyer`,
    verifyLoyerValide: `${const_app.base_url}Loyer/verifyLoyerValide`,
    formUploadDocBail: `${const_app.base_url}Bail/formUploadDocBail`,
    getpathDoc: `${const_app.base_url}Bail/getpathDoc`,
    modalupdateDocBail: `${const_app.base_url}Bail/FormUpdateBail`,
    deleteDocBail: `${const_app.base_url}Bail/removeDocBail`,
    updateDocBailTraitement: `${const_app.base_url}Bail/updateDocBailTraitement`,
    validationBail: `${const_app.base_url}Bail/validationBail`,
    automatique_Bail: `${const_app.base_url}Bail/automatique_Bail`,
    supprimer_facture: `${const_app.base_url}Bail/supprimer_facture`,
    nouveauBail: `${const_app.base_url}Bail/NouveauBail`,
    nouveauAvenant: `${const_app.base_url}Bail/NouveauAvenant`,
    generer_indexation: `${const_app.base_url}Bail/generer_indexation`,
    generer_indexation_fixe: `${const_app.base_url}Bail/generer_indexation_fixe`,

    genererFacture: `${const_app.base_url}Bail/genererFacture`,

    alert_Siret_TVa: `${const_app.base_url}Bail/alert_Siret_TVa`,
    // gererAvoirAndFactures: `${const_app.base_url}Bail/gererAvoirAndFactures`,
    cloturerBail: `${const_app.base_url}Bail/cloturerBail`,
    validerCloture: `${const_app.base_url}Bail/validerCloture`,
    modal_protocole: `${const_app.base_url}Bail/modal_protocole`,
    saveProtocole: `${const_app.base_url}Bail/saveProtocole`,
    getProtocoleBail: `${const_app.base_url}Bail/getProtocoleBail`,
    validerProtocol: `${const_app.base_url}Bail/validerProtocol`,
    consulterProtocol: `${const_app.base_url}Bail/consulterProtocol`,
    modifierProtocol: `${const_app.base_url}Bail/modifierProtocol`,
    generer_indexation_en_attente: `${const_app.base_url}Bail/generer_indexation_en_attente`,
    trie_indice_reference: `${const_app.base_url}Bail/trie_indice_reference`,
    tri_indice_plafonnement: `${const_app.base_url}Bail/tri_indice_plafonnement`,
    alert_dateSigne_mandat: `${const_app.base_url}Bail/alert_dateSigne_mandat`,
    create_indexationZero: `${const_app.base_url}Bail/create_indexationZero`,
    delete_indexationZero: `${const_app.base_url}Bail/delete_indexationZero`,
}

function addBail(
    bail_id,
    bail_debut,
    bail_fin,
    bail_resiliation_triennale,
    bail_loyer_variable,
    bail_remboursement_tom,
    bail_facturation_loyer_celaviegestion,
    bail_charge_recuperable,
    bail_date_premiere_indexation,
    bail_date_prochaine_indexation,
    bail_commentaire,
    bail_annee,
    bail_mois,
    periodicite_loyer_id,
    nature_prise_effet_id,
    type_bail_id,
    lot_client_id,
    gestionnaire_id,
    type_echeance_id,
    bail_art_605_id,
    bail_art_606_id,
    indice_loyer_id,
    moment_facturation_bail_id,
    periode_indexation_id,
    bail_forfait_charge_indexer,
    bail_date_echeance,
    preneur_id,
    bail_indexation,
    indice_valeur_loyer_id,
    bail_var_capee = 0,
    bail_var_plafonnee = 0,
    bail_var_appliquee = 0,
    bail_avenant = 0,
    bail_avenant_id_origin = null,
    premier_mois_trimes = null,
    bail_date_debut_mission = null,
    bail_var_min = null,
    bail_mode_calcul = 0,
    indice_valeur_plafonnement = 0
) {
    var type_request = 'POST';
    var url_request = bail_url.addBail;
    var type_output = 'json';

    var data_request = {
        'bail_id': bail_id,
        'bail_debut': bail_debut,
        'bail_fin': bail_fin,
        'bail_resiliation_triennale': bail_resiliation_triennale,
        'bail_loyer_variable': bail_loyer_variable,
        'bail_remboursement_tom': bail_remboursement_tom,
        'bail_facturation_loyer_celaviegestion': bail_facturation_loyer_celaviegestion,
        'bail_charge_recuperable': bail_charge_recuperable,
        'bail_date_premiere_indexation': bail_date_premiere_indexation,
        'bail_date_prochaine_indexation': bail_date_prochaine_indexation,
        'bail_date_echeance': bail_date_echeance,
        'bail_indexation': bail_indexation,
        'bail_commentaire': bail_commentaire,
        'bail_annee': bail_annee,
        'bail_mois': bail_mois,
        'pdl_id': periodicite_loyer_id,
        'natef_id': nature_prise_effet_id,
        'tpba_id': type_bail_id,
        'cliLot_id': lot_client_id,
        'gestionnaire_id': gestionnaire_id,
        'tpech_id': type_echeance_id,
        'bail_art_605': bail_art_605_id,
        'bail_art_606': bail_art_606_id,
        'indloyer_id': indice_loyer_id,
        'momfac_id': moment_facturation_bail_id,
        'prdind_id': periode_indexation_id,
        'bail_forfait_charge_indexer': bail_forfait_charge_indexer,
        'indval_id': indice_valeur_loyer_id,
        'preneur_id': preneur_id,
        'bail_var_capee': bail_var_capee,
        'bail_var_plafonnee': bail_var_plafonnee,
        'bail_var_appliquee': bail_var_appliquee,
        'bail_avenant': bail_avenant,
        'bail_avenant_id_origin': bail_avenant_id_origin,
        'premier_mois_trimes': premier_mois_trimes,
        'bail_date_debut_mission': bail_date_debut_mission,
        'bail_var_min': bail_var_min,
        'bail_mode_calcul': bail_mode_calcul,
        'indice_valeur_plafonnement': indice_valeur_plafonnement
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'addBail_Callback');
}

function addBail_Callback(response) {
    getBail($('#lot_client_id').val());

    var lastBailId = response.data_retour?.last_bail_id;
    var bailTxt = response.data_retour?.isBail;
    if (lastBailId) {
        historyAddDansBail(`Création d'un ${bailTxt}`, lastBailId);
        getIndexationLoyer(lastBailId);
        getLoyerReference(lastBailId);
        getDocBails($('#lot_client_id').val());
    } else {
        historyModifDansBail(`Modification des informations du ${bailTxt}`, lastBailId);
    }
}

// Get bail by client lot
function getBail(cliLot_id) {
    var type_request = 'POST';
    var url_request = bail_url.getBail;
    var type_output = 'json';

    var data_request = {
        'cliLot_id': cliLot_id,
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getBailCallback');
}

function getBailCallback(response) {
    if (response.data_retour != null) {
        const current_bail = response.data_retour[0];
        const date_now = new Date();
        var etat_mandat_id = $('#etat_mandat_id').val();
        var mandat_date_fin = new Date($('#mandat_date_fin').val());
        $(".aucun_bail").hide();
        $(".detail_bail").show();
        $('#bail_id').val(current_bail.bail_id);
        $('#bail_debut').val(current_bail.bail_debut);
        $('#bail_fin').val(current_bail.bail_fin);
        $('#bail_resiliation_triennale').val(current_bail.bail_resiliation_triennale);
        $('#bail_loyer_variable').val(current_bail.bail_loyer_variable);
        $('#bail_date_premiere_indexation').val(current_bail.bail_date_premiere_indexation);
        $('#bail_date_prochaine_indexation').val(current_bail.bail_date_prochaine_indexation);
        $('#bail_date_echeance').val(current_bail.bail_date_echeance);
        $('#bail_indexation').val(current_bail.bail_indexation);
        $('#bail_commentaire').val(current_bail.bail_commentaire);
        $('#bail_annee').val(current_bail.bail_annee);
        $('#bail_mois').val(current_bail.bail_mois);
        $('#periodicite_loyer_id').val(current_bail.pdl_id);
        if (current_bail.pdl_id == 5) {
            $('#1er_trimes_decal_affichage').removeClass('d-none');
        }
        $('#nature_prise_effet_id').val(current_bail.natef_id);
        $('#type_bail_id').val(current_bail.tpba_id);
        $('#gestionnaire_id_bail').val(current_bail.gestionnaire_id);
        $('#type_echeance_id').val(current_bail.tpech_id);
        $('#bail_art_605_id').val(current_bail.bail_art_605);
        $('#bail_art_606_id').val(current_bail.bail_art_606);
        $('#indice_loyer_id').val(current_bail.indloyer_id);

        $('#last_indice_valeur_loyer_id').val(current_bail.indval_id);
        $('#moment_facturation_bail_id').val(current_bail.momfac_id);
        $('#periode_indexation').val(current_bail.prdind_id);
        $('#bail_forfait_charge_indexer').val(current_bail.bail_forfait_charge_indexer);
        $('#countIndexation').val(response.data_retour["indexations"].length);
        $('#preneur_id').val(current_bail.preneur_id);
        $('#cliLot_id ').val(current_bail.cliLot_id);

        $('#bail_var_capee').val(current_bail.bail_var_capee);
        $('#bail_var_plafonnee').val(current_bail.bail_var_plafonnee);
        $('#bail_var_min').val(current_bail.bail_var_min == 0 ? '' : current_bail.bail_var_min);
        $('#bail_var_appliquee').val(current_bail.bail_var_appliquee);
        $('#premier_mois_trimes').val(current_bail.premier_mois_trimes);
        $('#bail_date_debut_mission').val(current_bail.bail_date_debut_mission);

        if (current_bail.prdind_id == 2 && current_bail.indloyer_id != 7) {
            $('#mod_calcul').removeClass('d-none');
            $('#bail_mode_calcul').val(current_bail.bail_mode_calcul);
        }

        if (current_bail.prdind_id == 3) {
            $('#bail_date_premiere_indexation').attr('type', 'text');
            $('#bail_date_premiere_indexation').val('Non applicable');
            $('#bail_date_premiere_indexation').removeClass('input_first_disabled');
            $('#bail_date_premiere_indexation').prop('disabled', true);

            $('#bail_date_prochaine_indexation').attr('type', 'text');
            $('#bail_date_prochaine_indexation').val('Non applicable');
            $('#bail_date_prochaine_indexation').prop('disabled', true);

            $('.non_applicable').removeClass('d-none');
            $('.applicable').addClass('d-none');
            $('#indice_loyer_id').prop('disabled', true);
            $('#indice_loyer_id').removeClass('input_first_disabled');
            $('#indice_loyer_id').val('non_applicable');

            $('#indice_valeur_loyer_id').empty();
            $('#indice_valeur_loyer_id').removeClass('input_first_disabled');
            $('#indice_valeur_loyer_id').prop('disabled', true);
            $('#indice_valeur_loyer_id').append('<option value="1" selected>Non applicable</option>');

            $('#generer_indexation').prop('disabled', true);
        }

        if (current_bail.prdind_id == 1 || current_bail.prdind_id == 2) {
            trie_indice_reference(current_bail.indloyer_id, current_bail.indval_id);
        }

        tri_indice_plafonnement(current_bail.indice_valeur_plafonnement);

        if (current_bail.bail_loyer_variable === "2") {
            $(".main_indexation_loyer").hide();
        } else {
            $(".main_indexation_loyer").show();
        }

        current_bail.bail_remboursement_tom == 1 ? $('#bail_remboursement_tom').prop('checked', true) : $('#bail_remboursement_tom').prop('checked', false);
        current_bail.bail_charge_recuperable == 1 ? $('#bail_charge_recuperable').prop('checked', true) : $('#bail_charge_recuperable').prop('checked', false);
        current_bail.bail_facturation_loyer_celaviegestion == 1 ? $('#bail_facturation_loyer_celaviegestion').prop('checked', true) : $('#bail_facturation_loyer_celaviegestion').prop('checked', false);

        var todaysDate = new Date();
        year = convertDate(todaysDate).split("-", 1).toString();

        $('#btn_new_bail').prop('disabled', true);

        if (current_bail.bail_avenant == 1) {
            $('#tacite_en_cours').text('Bail Avenant en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
            $('#tacite_prolongation').val('non');
            $('#isAvenant').val(1);
            $('#btn_history_bail').text('Historique des baux');

            // change text button
            $('.bail_action_valid').text("Valider l'avenant");
            $('.bail_action_devalid').text("Annuler la validation de l'avenant");
            // $('.bail_action_cloture').text("Cloturer l'avenant");

        } else {
            if (current_bail.bail_cloture == 1) {
                $('#tacite_en_cours').text('Bail cloturé du ' + ChangeFormateDate(current_bail.bail_date_cloture) + ' :');
                $('#tacite_prolongation').val('non');
            } else if (current_bail.bail_fin != "0000-00-00") {
                var todaysDate = new Date();
                current_bail.bail_fin <= convertDate(todaysDate) ? $('#tacite_en_cours').text('Bail en tacite du ' + ChangeFormateDate(current_bail.bail_fin) + ' :') : $('#tacite_en_cours').text('Bail en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
                current_bail.bail_fin <= convertDate(todaysDate) ? $('#tacite_prolongation').val('oui') : $('#tacite_prolongation').val('non');
            } else {
                $('#tacite_en_cours').text('Bail en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
                $('#tacite_prolongation').val('non');
            }
        }

        // Verify if bail is validated
        if (current_bail.bail_valide == 0) {
            $('#bail_validation').text("(Non validé)");
            $('#bail_validation').addClass("bail_non_valide");
            $('#bail_validation').removeClass("bail_valide");
            $(".contain_btn_edit_bail").show();
            $(".contain_addfranchisemodal").show();
            $('.bail_action_devalid').addClass('d-none');
            $('.bail_action_valid').removeClass('d-none');
            $('#ask_bail_validation_in_bail').show();
        } else {
            $('#bail_validation').text("(Validé)");
            $('#bail_validation').addClass("bail_valide");
            $('#bail_validation').removeClass("bail_non_valide");
            $('.bail_action_valid').addClass('d-none');
            $('.bail_action_devalid').removeClass('d-none');
            $(".contain_btn_edit_bail").hide();
            $(".contain_addfranchisemodal").hide();
            $('#ask_bail_validation_in_bail').hide();
        }

        if ((etat_mandat_id == 6 && mandat_date_fin < date_now) || etat_mandat_id == 7) {
            $('.bail_action_devalid').addClass('d-none');
            $('.bail_action_valid').addClass('d-none');
            $('#ask_bail_validation_in_bail').hide();
        }

        if (current_bail.bail_cloture == 0) {
            $('#btn_new_bail').prop('disabled', true);
            $('#btn_new_avenant').prop('disabled', false);
            $('.btn_bail_cloture').prop('disabled', false);
            $('#btn_demande_cloture').prop('disabled', false);
            $('#btn_demande_creer_avenant').prop('disabled', false);
        } else {
            $('#btn_new_bail').prop('disabled', false);
            $('#btn_new_avenant').prop('disabled', true);
            $('#btn_new_protocole').prop('disabled', true);
            $('.btn_bail_cloture').prop('disabled', true);
            $('#btn_demande_cloture').prop('disabled', true);
            $('#btn_demande_creer_avenant').prop('disabled', true);

            $('.bail_action_devalid').addClass('d-none');
            $('.bail_action_valid').addClass('d-none');
        }

        // Button cloturer le bail
        if (current_bail.bail_cloture == 1) $('.bail_action_cloture').hide();

        const bail_id = current_bail.bail_id;
        getFranchiseLoyer(bail_id);
        getIndexationLoyer(bail_id);
        getLoyerReference(bail_id);
        getDocLoyers($('#lot_client_id').val());
        getLoyers(year, $('#lot_client_id').val());
        getTableFacture(bail_id, current_bail.pdl_id, current_bail.bail_loyer_variable, year);
        getModalHistoriqueBail(current_bail.cliLot_id);

    }
    else {
        var todaysDate = new Date();
        year = convertDate(todaysDate).split("-", 1).toString();
        getLoyers(year, $('#lot_client_id').val());
        $(".aucun_bail").show();
    }
}

$(document).on('change', '#indice_loyer_id', function () {
    trie_indice_reference($(this).val());
});

function trie_indice_reference(indice_loyer_id, indval_id = 0) {
    var type_request = 'POST';
    var url_request = bail_url.trie_indice_reference;
    var type_output = 'html';

    var data_request = {
        'indloyer_id': indice_loyer_id,
        'indval_id': indval_id
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'trie_indice_referenceCallback');
}
function trie_indice_referenceCallback(response) {
    const obj = JSON.parse(response);
    var select = $('#indice_valeur_loyer_id');
    select.empty();

    if (obj.indloyer_id != 7) {
        var options = [{ value: 0, text: 'les valeurs :', attr_value: 0 }];
    } else {
        var options = [{ value: 0, text: 'Indice Fixe 2%', attr_value: 0 }];
    }

    if (obj.indice_valeur_loyer.length != 0) {
        for (let index = 0; index < obj.indice_valeur_loyer.length; index++) {
            var option_text = obj.indice_valeur_loyer[index].indval_valide == 1
                ? obj.indice_valeur_loyer[index].indval_trimestre + ' ' + obj.indice_valeur_loyer[index].indval_annee + ' (' + obj.indice_valeur_loyer[index].indval_valeur + ')'
                : obj.indice_valeur_loyer[index].indval_trimestre + ' ' + obj.indice_valeur_loyer[index].indval_annee + ' (Non publié)';

            var option_text = option_text.replace('null', '');
            var single_option = {
                value: obj.indice_valeur_loyer[index].indval_id, text: option_text, attr_value: obj.indice_valeur_loyer[index].indval_valide
            };
            options.push(single_option);
        }
    }

    $.each(options, function (index, option) {
        var newOption = $('<option>');
        newOption.val(option.value);
        newOption.text(option.text);
        newOption.attr('data-indice_valid', option.attr_value)
        select.append(newOption);
    });

    $('#indice_valeur_loyer_id').val(obj.indval_id_selected);
}

function tri_indice_plafonnement(indice_valeur_plafonnement = 0) {
    var type_request = 'POST';
    var url_request = bail_url.tri_indice_plafonnement;
    var type_output = 'html';

    var data_request = {
        'indice_valeur_plafonnement': indice_valeur_plafonnement,
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'tri_indice_plafonnementCallback');
}
function tri_indice_plafonnementCallback(response) {
    const obj = JSON.parse(response);
    var select = $('#indice_valeur_plafonnement');
    select.empty();
    var options = [{ value: 0, text: 'Aucun', attr_value: 0 }];

    if (obj.indice_loyer.length != 0) {
        for (let index = 0; index < obj.indice_loyer.length; index++) {
            var option_text = obj.indice_loyer[index].indloyer_description;
            var single_option = {
                value: obj.indice_loyer[index].indloyer_id, text: option_text
            };
            options.push(single_option);
        }

        $.each(options, function (index, option) {
            var newOption = $('<option>');
            newOption.val(option.value);
            newOption.text(option.text);
            select.append(newOption);
        });

        $('#indice_valeur_plafonnement').val(obj.indval_id_selected);
    }
}

function ChangeFormateDate(oldDate) {
    if (!oldDate) return "-";
    return oldDate.toString().split("-").reverse().join("/");
}

function convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
}

function getLoyers(year, cliLot_id) {
    var type_request = 'POST';
    var url_request = bail_url.getLoyer;
    var type_output = 'html';

    var data_request = {
        'year': year,
        'cliLot_id': cliLot_id
    };

    var loading = {
        type: "content",
        id_content: "container-loyer-facture"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getLoyerCallback', loading);
}

function getLoyerCallback(response) {
    $('#container-loyer-facture').html(response);
}

// verify loyer valide*
function verifyLoyerValide(year, cliLot_id) {
    var type_request = 'POST';
    var url_request = bail_url.verifyLoyerValide;
    var type_output = 'html';

    var data_request = {
        'year': year,
        'cliLot_id': cliLot_id
    };

    var loading = {
        type: "content",
        id_content: "container-loyer-facture"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'verifyLoyerValideCallback', loading);
    $('.pace-progress').css('display', 'none');
    $('.pace-progress-inner').css('display', 'none');
}

function verifyLoyerValideCallback(response) {
    if (response) {
        $('#tableau_detail_bail').html(response);
    }
}

// Add new indexation loyer for a bail ID
function addIndexationLoyer(
    indlo_id, indlo_debut, indlo_fin, indlo_appartement_ht, indlo_appartement_tva, indlo_appartement_ttc,
    indlo_parking_ht, indlo_parking_tva, indlo_parking_ttc, bail_id,
    indlo_charge_ht, indlo_charge_tva, indlo_charge_ttc,
    indlo_autre_prod_ht, indlo_autre_prod_tva, indlo_autre_prod_ttc,
    indlo_indice_base, indlo_indice_reference, indlo_variation_plafonnee, indlo_variation_capee,
    indlo_variation_loyer, indlo_variation_appliquee, modal_id
) {
    var type_request = 'POST';
    var url_request = bail_url.addIndexationLoyer;
    var type_output = 'json';
    var data_request = {
        'indlo_id': indlo_id,
        'indlo_debut': indlo_debut,
        'indlo_fin': indlo_fin,
        'indlo_appartement_ht': indlo_appartement_ht,
        'indlo_appartement_tva': indlo_appartement_tva,
        'indlo_appartement_ttc': indlo_appartement_ttc,
        'indlo_parking_ht': indlo_parking_ht,
        'indlo_parking_tva': indlo_parking_tva,
        'indlo_parking_ttc': indlo_parking_ttc,
        'bail_id': bail_id,
        'indlo_charge_ht': indlo_charge_ht,
        'indlo_charge_tva': indlo_charge_tva,
        'indlo_charge_ttc': indlo_charge_ttc,
        'indlo_autre_prod_ht': indlo_autre_prod_ht,
        'indlo_autre_prod_tva': indlo_autre_prod_tva,
        'indlo_autre_prod_ttc': indlo_autre_prod_ttc,
        'indlo_indice_base': indlo_indice_base,
        'indlo_indice_reference': indlo_indice_reference,
        'indlo_variation_plafonnee': indlo_variation_plafonnee,
        'indlo_variation_capee': indlo_variation_capee,
        'indlo_variation_loyer': indlo_variation_loyer,
        'indlo_variation_appliquee': indlo_variation_appliquee,
        'modal_id': modal_id
    };

    var loading = {
        type: "content",
        id_content: "container-indexation-loyer"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addIndexationLoyerCallback', loading);
}

function addIndexationLoyerCallback(response) {
    if (response.status === true) {
        $(`#${response.data_retour.request.modal_id}`).modal('hide');

        getIndexationLoyer(response.data_retour.request.bail_id);
        getLoyerReference(response.data_retour.request.bail_id);
        getTableFacture(
            response.data_retour.request.bail_id,
            response.data_retour.bail_info.pdl_id,
            response.data_retour.bail_info.bail_loyer_variable,
            $('#annee_loyer').val(),
            response.data_retour.bail_info.cliLot_id
        );

        historyModifDansBail("Ajout d'indexation du bail", response.data_retour.request.bail_id);

        // add one for count indexation
        $('#countIndexation').val(parseInt($('#countIndexation').val()) + 1);
    }
}

// Get indexation loyer by bail ID
function getIndexationLoyer(bail_id) {
    var type_request = 'POST';
    var url_request = bail_url.getIndexationLoyer;
    var type_output = 'html';

    var data_request = {
        'bail_id': bail_id
    };

    var loading = {
        type: "content",
        id_content: "container-indexation-loyer"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getIndexationLoyerCallback', loading);
}

function getIndexationLoyerCallback(response) {
    $('#container-indexation-loyer').html(response);
    const lot_client_id = $('#lot_client_id').val();
    getModalIndexationLoyer(lot_client_id);
}

// Get loyer de reference by bail ID
function getLoyerReference(bail_id) {
    var type_request = 'POST';
    var url_request = bail_url.getLoyerReference;
    var type_output = 'html';

    var data_request = {
        'bail_id': bail_id
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getLoyerReferenceCallback');
}

function getLoyerReferenceCallback(response) {
    $('#contain_tab_loyer_reference').html(response);
}


// Add new franchise loyer for a bail ID
function addFranchiseLoyer(
    franc_id, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit = null, taux_tva_franchise = 0, montant_ttc_deduit = null, franchise_mode_calcul, franchise_pourcentage, modal_id
) {
    var type_request = 'POST';
    var url_request = bail_url.addFranciseLoyer;
    var type_output = 'json';

    var data_request = {
        'franc_id': franc_id,
        'franc_debut': franc_debut,
        'franc_fin': franc_fin,
        'franc_explication': franc_explication,
        'bail_id': bail_id,
        'type_deduction': type_deduction,
        'montant_ht_deduit': montant_ht_deduit,
        'taux_tva_franchise': taux_tva_franchise,
        'montant_ttc_deduit': montant_ttc_deduit,
        'franchise_mode_calcul': franchise_mode_calcul,
        'franchise_pourcentage': franchise_pourcentage,
        'modal_id': modal_id
    };

    var loading = {
        type: "content",
        id_content: "container-franchise-loyer"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'addFranchiseLoyerCallback', loading);
}

function addFranchiseLoyerCallback(response) {
    if (response.status === true) {
        $(`#${response.data_retour.request.modal_id}`).modal('hide');
        $('.modal-backdrop').remove();
        getFranchiseLoyer(response.data_retour.request.bail_id);

        historyModifDansBail("Ajout de franchise de loyer du bail", response.data_retour.request.bail_id);
    }
}

// Get franchise loyer by bail ID
function getFranchiseLoyer(bail_id) {
    var type_request = 'POST';
    var url_request = bail_url.getFranciseLoyer;
    var type_output = 'html';

    var data_request = {
        'bail_id': bail_id
    };

    var loading = {
        type: "content",
        id_content: "container-franchise-loyer"
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getFranchiseLoyerCallback');
}

function getFranchiseLoyerCallback(response) {
    $('#container-franchise-loyer').html(response);
}

// ***  Special indexation de loyer ***

// get indice valeur  au format AAAA-MM-DD
function getIndiceValeur(date) {
    const currentDate = new Date(date);
    const annee = currentDate.getFullYear();
    const periode = currentDate.getMonth();
}

// retourne en pourcentage %
function calculVariation(valeur_indice_ref, valeur_indice_base) {
    let result = (valeur_indice_ref - valeur_indice_base) / valeur_indice_base;
    result *= 100;
    return result.toFixed(2);
}

function calculNouveauLoyer(loyer_trimestriel, valeur_indice_ref, valeur_indice_base) {
    const result = (loyer_trimestriel * valeur_indice_ref) / valeur_indice_base;
    return result.toFixed(2);
}

function toggleFranchise(index) {
    $(`.franchise-view-${index}`).toggleClass('d-none');
    $(`.franchise-edit-${index}`).toggleClass('d-none');
}

$(document).on('click', '#addfranchisemodal', function () {
    $('.help_montant_ttc_deduit').empty();
    $('.help_franchise_pourcentage').empty();
    $('.help_franc_explication').empty();
});

function updateFranchise(franc_id) {
    const franc_debut = $("#franc_debut").val();
    const franc_fin = $("#franc_fin").val();
    const franc_explication = $("#franc_explication").val();
    const type_deduction = $("#type_deduction").val();
    const montant_ht_deduit = $("#montant_ht_deduit").val().replace(/,/g, '.');
    const taux_tva_franchise = $("#taux_tva_franchise").val();
    const montant_ttc_deduit = $("#montant_ttc_deduit").val().replace(/,/g, '.');
    const bail_id = $("#bail_id").val();
    const franchise_mode_calcul = $("#franchise_mode_calcul").val();
    const franchise_pourcentage = $("#franchise_pourcentage").val();

    if (new Date(franc_fin) < new Date(franc_debut)) {
        Notiflix.Notify.failure("Date de fin ultérieure à la date de début.", {
            position: 'left-bottom',
            timeout: 3000
        });
        return false;
    }

    $('.help_montant_ttc_deduit').empty();
    $('.help_franchise_pourcentage').empty();
    if (type_deduction == 1) {
        if (franc_explication) {
            addFranchiseLoyer(franc_id, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
        } else {
            $('.help_franc_explication').text('Ce champ de doit pas être vide');
        }
    } else {
        if (franchise_mode_calcul == 0) {
            if (franc_debut && franc_fin && montant_ttc_deduit && franc_explication) {
                addFranchiseLoyer(franc_id, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
            } else {
                if (!montant_ttc_deduit) {
                    $('.help_montant_ttc_deduit').text('Ce champ de doit pas être vide');
                }

                if (!franc_explication) {
                    $('.help_franc_explication').text('Ce champ de doit pas être vide');
                }
            }
        } else {
            if (franc_debut && franc_fin && franchise_pourcentage && franc_explication) {
                addFranchiseLoyer(franc_id, franc_debut, franc_fin, franc_explication, bail_id, type_deduction, montant_ht_deduit, taux_tva_franchise, montant_ttc_deduit, franchise_mode_calcul, franchise_pourcentage, "modal_franchise");
            } else {
                if (!franchise_pourcentage) {
                    $('.help_franchise_pourcentage').text('Ce champ de doit pas être vide');
                }

                if (!franc_explication) {
                    $('.help_franc_explication').text('Ce champ de doit pas être vide');
                }
            }
        }
    }
}

function calcul_ttc_franchise() {
    if ($('#montant_ht_deduit').val()) {
        $('.help_montant_ttc_deduit').empty();
        var ht = $('#montant_ht_deduit').val().replace(/,/g, '.');
        var taux = $('#taux_tva_franchise').val().replace(/,/g, '.');
        var ttc = parseFloat(ht) + (parseFloat(ht) * parseFloat(taux) / 100);
        $('#montant_ttc_deduit').val(ttc.toFixed(2));
    }
}

function calcul_ht_franchise() {
    if ($('#montant_ttc_deduit').val()) {
        var ttc = $('#montant_ttc_deduit').val().replace(/,/g, '.');
        var taux = $('#taux_tva_franchise').val().replace(/,/g, '.');
        var ht = parseFloat(ttc) / (1 + (parseFloat(taux) / 100));
        $('#montant_ht_deduit').val(ht.toFixed(2));
    }
}

$(document).on('keyup', '#montant_ht_deduit', function () {
    calcul_ttc_franchise();
});

$(document).on('change', '#taux_tva_franchise', function () {
    if ($('#montant_ht_deduit').val()) {
        calcul_ttc_franchise();
    } else {
        calcul_ht_franchise();
    }
});

$(document).on('keyup', '#montant_ttc_deduit', function () {
    calcul_ht_franchise();
});

$(document).on('change', '#franchise_mode_calcul', function () {
    if ($(this).val() == 0) {
        if ($('#display_montant_ht_deduit').hasClass('d-none') && $('#display_montant_ttc_deduit').hasClass('d-none') && !$('#display_pourcentage').hasClass('d-none')) {
            $('#display_montant_ht_deduit').removeClass('d-none');
            $('#display_montant_ttc_deduit').removeClass('d-none');
            $('#display_pourcentage').addClass('d-none')
        }
    }
    else {
        if (!$('#display_montant_ht_deduit').hasClass('d-none') && !$('#display_montant_ttc_deduit').hasClass('d-none') && $('#display_pourcentage').hasClass('d-none')) {
            $('#display_montant_ht_deduit').addClass('d-none');
            $('#display_montant_ttc_deduit').addClass('d-none');
            $('#display_pourcentage').removeClass('d-none')
        }
    }
});

$(document).on('click', '#addindexationmodal', function () {
    $('.update_indexation_title').hide();
    $('.ajout_indexation_title').show();
    $('#addNewIndexationLoyer').text('Ajouter');

    $("#indlo_variation_loyer").val(0);
    $("#indlo_variation_appliquee").val(0);
    $("#indlo_id").val('');
    $("#indval_id").val('');
    $("#indlo_variation_plafonnee").val('');
    $("#indlo_variation_capee").val('');

    // Show other show
    // PHP will manage the count of indexation
    $(".contain_first_indexation").show();

    const indexations_count = $("#indexations_count").val();
    if (indexations_count === "1") {
        $("#indlo_debut").attr("disabled", true);
        $("#indlo_indice_base").attr("disabled", true);
    } else {
        $("#indlo_debut").attr("disabled", false);
        $("#indlo_indice_base").attr("disabled", false);
    }
});

$(document).on('click', '#update_indexation', function (event) {
    // Disable field

    $('.update_indexation_title').show();
    $('.ajout_indexation_title').hide();
    $('#addNewIndexationLoyer').text('Enregistrer');
    var indlo_id = $(this).attr('data-indlo_id');
    var indlo_debut = $(this).attr('data-indlo_debut');
    var indlo_fin = $(this).attr('data-indlo_fin');
    var indlo_appartement_ht = $(this).attr('data-indlo_appartement_ht');
    var indlo_appartement_tva = $(this).attr('data-indlo_appartement_tva');
    var indlo_appartement_ttc = $(this).attr('data-indlo_appartement_ttc');
    var indlo_parking_ht = $(this).attr('data-indlo_parking_ht');
    var indlo_parking_tva = $(this).attr('data-indlo_parking_tva');
    var indlo_parking_ttc = $(this).attr('data-indlo_parking_ttc');
    var indlo_charge_ht = $(this).attr('data-indlo_charge_ht');
    var indlo_charge_tva = $(this).attr('data-indlo_charge_tva');
    var indlo_charge_ttc = $(this).attr('data-indlo_charge_ttc');
    var indlo_autre_prod_ht = $(this).attr('data-indlo_autre_prod_ht');
    var indlo_autre_prod_tva = $(this).attr('data-indlo_autre_prod_tva');
    var indlo_autre_prod_ttc = $(this).attr('data-indlo_autre_prod_ttc');
    var indlo_indice_base = $(this).attr('data-indlo_indice_base');
    var indlo_indice_reference = $(this).attr('data-indlo_indice_reference');
    var indlo_variation_plafonnee = $(this).attr('data-indlo_variation_plafonnee');
    var indlo_variation_capee = $(this).attr('data-indlo_variation_capee');
    var indlo_variation_loyer = $(this).attr('data-indlo_variation_loyer');
    var indlo_variation_appliquee = $(this).attr('data-indlo_variation_appliquee');

    $("#indlo_id").val(indlo_id);
    $("#indlo_debut").val(indlo_debut);
    $("#indlo_fin").val(indlo_fin);
    $("#indlo_appartement_ht").val(indlo_appartement_ht);
    $("#indlo_appartement_tva").val(indlo_appartement_tva);
    $("#indlo_appartement_ttc").val(indlo_appartement_ttc);
    $("#indlo_parking_ht").val(indlo_parking_ht);
    $("#indlo_parking_tva").val(indlo_parking_tva);
    $("#indlo_parking_ttc").val(indlo_parking_ttc);
    $("#indlo_charge_ht").val(indlo_charge_ht);
    $("#indlo_charge_tva").val(indlo_charge_tva);
    $("#indlo_charge_ttc").val(indlo_charge_ttc);
    $("#indlo_autre_prod_ht").val(indlo_autre_prod_ht);
    $("#indlo_autre_prod_tva").val(indlo_autre_prod_tva);
    $("#indlo_autre_prod_ttc").val(indlo_autre_prod_ttc);
    $("#indlo_indice_base").val(indlo_indice_base);
    $("#indlo_indice_reference").val(indlo_indice_reference);
    $("#indlo_variation_plafonnee").val(indlo_variation_plafonnee);
    $("#indlo_variation_capee").val(indlo_variation_capee);
    $("#indlo_variation_loyer").val(indlo_variation_loyer);
    $("#indlo_variation_appliquee").val(indlo_variation_appliquee);

    // verify if first indexation
    if (indlo_id && $(this).attr('data-indlo_index') === "0") {
        $(".contain_first_indexation").hide();
        $("#indlo_debut").attr("disabled", false);
        $("#indlo_indice_base").attr("disabled", false);
    } else {
        $(".contain_first_indexation").show();
        $("#indlo_debut").attr("disabled", true);
        $("#indlo_indice_base").attr("disabled", true);
    }
});

$(document).on('click', '#deleteIndexationLoyer', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        indlo_id: $(this).attr('data-id')
    }
    deleteIndexation(data);
});

$(document).on('change', '#annee_loyer', function () {
    getLoyers($(this).val(), $('#lot_client_id').val());
});

function deleteIndexation(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.deleteIndexation;

    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteIndexation', loading);
}

function retourDeleteIndexation(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        indlo_id: response.data.id
                    }
                    deleteIndexation(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=indexation_row-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });

            const lot_client_id = $('#lot_client_id').val();
            getModalIndexationLoyer(lot_client_id);

            getTableFacture(
                response.bail_info.bail_id,
                response.bail_info.pdl_id,
                response.bail_info.bail_loyer_variable,
                $('#annee_loyer').val(),
                response.bail_info.cliLot_id
            );

            getLoyerReference(response.bail_info.bail_id);
            historyModifDansBail("Suppression d'indexation  de loyer du bail", response.bail_info.bail_id);

            // remove one for count indexation
            $('#countIndexation').val(parseInt($('#countIndexation').val()) - 1);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/**Supprimer franchise loyer ***/
$(document).on('click', '#removeFranchise', function (e) {
    e.preventDefault();
    var data = {
        action: "demande",
        franc_id: $(this).attr('data-id')
    }
    deleteFranchise(data);
});

function deleteFranchise(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.deleteFranchise;

    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteFranchise', loading);
}

function retourDeleteFranchise(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        franc_id: response.data.id
                    }
                    deleteFranchise(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=franchise_row-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });

            historyModifDansBail("Suppression de franchise de loyer du bail", 0);

        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

// Ajouter document bail

$(document).on('click', '#ajoutDocBail', function (e) {
    e.preventDefault();
    ajoutDocBail($(this).attr('data-id'), 'add', null);
});

function ajoutDocBail(cliLot_id, action, doc_bail_id = null) {

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'action': action,
        'cliLot_id': cliLot_id,
        'doc_bail_id': doc_bail_id
    };
    var url_request = bail_url.formUploadDocBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'chargercontentBail');

    $("#modalAjoutBail").modal('show');
}

function chargercontentBail(response) {
    $('#UploadBail').html(response);
}



$(document).on('click', '.zommer-apercu-list', function () {

    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() + 50)

})

$(document).on('click', '.dezommer-apercu-list', function () {
    $('#apercu_list_doc').find('object').width($('#apercu_list_doc').find('object').width() - 50)

})

$(document).on('click', '.orginal-apercu-list', function () {

    $('#apercu_list_doc').find('object').css({ height: 'auto', width: 'auto' })

})

/*********traité et non traité document Bail***********/

$(document).on('click', '.btnTraiteBail', function () {
    updateTraitementBail($(this).attr('data-id'));
});

function updateTraitementBail(doc_bail_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'doc_bail_id': doc_bail_id,
    };

    var url_request = bail_url.updateDocBailTraitement;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateTraitementBailCallback');
}

function updateTraitementBailCallback(response) {
    getDocBails(response.cliLot_id);
}

/*******Update document bail**********/

$(document).on('click', '.btnUpdtadeDocBail', function (e) {
    e.preventDefault();
    var doc_bail_id = $(this).attr("data-id");
    var cliLot_id = $(this).attr("data-lot");
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = { action: "edit", doc_bail_id: doc_bail_id, cliLot_id: cliLot_id };
    var url_request = bail_url.modalupdateDocBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'charger_contentModal');
});

$(document).on('submit', '#UpdateDocumentBail', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var param = {
        type: "content",
        id_content: "modal-main-content"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdatedocBail", "null_fn", param);
});

function retourupdatedocBail(response) {
    if (response.status == true) {
        myModal.toggle();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getDocBails(response.data_retour.cliLot_id);
    } else {
        fn.response_control_champ(response);
    }
}

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function charger_contentModal(response) {
    $("#modal-main-content").html(response);

    myModal.toggle();
}

$(document).on('click', '.btnSuppDocBail', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        doc_bail_id: $(this).attr('data-id')
    };
    deleteDocBail(data_request);
});

function deleteDocBail(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.deleteDocBail;
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteBail', loading);
}
function retourDeleteBail(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        doc_bail_id: response.data.id
                    }
                    deleteDocBail(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`div[id=docrowbail-${response.data.id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

// Bail actions
$(document).on('click', '.btn_bail_validation', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        bail_valide: $(this).attr('data-valid'),
        bail_id: $('#bail_id').val()
    };
    const countIndexation = $("#countIndexation").val();
    if ($('#periode_indexation').val() == 3) {
        $(".input_first_disabled").attr("disabled", true);
        $(".btn_edit_bail").removeClass('d-none');
        $(".btn_finish_bail").addClass('d-none');
        validationBail(data_request);
    }
    else if ($(this).attr('data-valid') == 1 && (countIndexation <= 1 || countIndexation == '')) {
        Notiflix.Notify.failure("Attention, demande de validation refusée car vous n'avez pas générés les indexations de loyers pour le bail.", {
            position: 'left-bottom',
            width: "500px",
            timeout: 4000
        });
    }
    else {
        $(".input_first_disabled").attr("disabled", true);
        $(".btn_edit_bail").removeClass('d-none');
        $(".btn_finish_bail").addClass('d-none');
        validationBail(data_request);
    }
});

var myModalalertSiret = new bootstrap.Modal(document.getElementById("modalFormAlert_Siret"), {
    keyboard: false
});

$(document).on('click', '#alert_tva_siret', function () {
    var siret_dossier = $('#siret_dossier').val();
    var tvaintracommunautaire_dossier = $('#tvaintracommunautaire_dossier').val();
    // if (siret_dossier == 1 && tvaintracommunautaire_dossier == 0) {
    //     var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    // } else 
    if (siret_dossier == 0 && tvaintracommunautaire_dossier == 1) {
        var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    } else {
        var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b> et le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    }
    var titre = 'Demande de validation impossible car le dossier est incomplet';
    alert_Siret_TVa(titre, contenu);
});

$(document).on('click', '#alert_tva_siret_loyer', function () {
    var siret_dossier = $('#siret_dossier').val();
    var tvaintracommunautaire_dossier = $('#tvaintracommunautaire_dossier').val();
    if (siret_dossier == 1 && tvaintracommunautaire_dossier == 0) {
        var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    } else if (siret_dossier == 0 && tvaintracommunautaire_dossier == 1) {
        var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    } else {
        var contenu = 'Vous ne pouvez pas demander la validation du bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b> et le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    }
    var titre = 'Demande de validation impossible car le dossier est incomplet';
    alert_Siret_TVa(titre, contenu);
});

$(document).on('click', '.alert_tva_siret', function () {
    var siret_dossier = $('#siret_dossier').val();
    var tvaintracommunautaire_dossier = $('#tvaintracommunautaire_dossier').val();
    if (siret_dossier == 1 && tvaintracommunautaire_dossier == 0) {
        var contenu = 'Vous ne pouvez pas valider le bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
    } else
        if (siret_dossier == 0 && tvaintracommunautaire_dossier == 1) {
            var contenu = 'Vous ne pouvez pas valider le bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
        } else {
            var contenu = 'Vous ne pouvez pas valider le bail car le dossier est incomplet. <br> En effet, vous devez renseigner dans la Fiche Dossier du lot concerné le <b>Numéro Siret</b> et le numéro de <b>TVA intracommunautaire</b>. <br>Pour renseigner cette information, il faut aller dans le dossier concerné, puis onglet "Identification".'
        }
    var titre = 'Validation du bail impossible car le dossier est incomplet';
    alert_Siret_TVa(titre, contenu);
});

function alert_Siret_TVa(titre, contenu) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        titre: titre,
        contenu: contenu
    };
    var url_request = bail_url.alert_Siret_TVa;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_Siret_TVa_callback');
}

function alert_Siret_TVa_callback(response) {
    $("#modal-main-content-alertSiretTva").html(response);
    myModalalertSiret.toggle();
}

function validationBail(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.validationBail;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourValidationBail');
}

function retourValidationBail(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        bail_valide: response.data.bail_valide,
                        bail_id: response.data.bail_id,
                    }
                    validationBail(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getBail(response.data.clilotId);

            if (response.data.bail_valide == 1) {
                // if ($('#isAvenant').val() == 1) {
                //     gererAvoirAndFactures($('#bail_debut').val(), $('#bail_id').val())
                // }
                $("#header-loyer-document").show();
                $(".contain_btn_edit_bail").hide();
                // sendMailGestionnaire(response.data);
            } else {
                $(".contain_btn_edit_bail").show();
                supprimer_facture(response.data.bail_id);
            }
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#creerFacture', function () {
    var data = {
        bail_id: $(this).attr('data-bail_id'),
        action: "demande",
    }
    genererFacture(data);
});

function genererFacture(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.genererFacture;

    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourGenererFacture');
}
function retourGenererFacture(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        bail_id: response.data.bail_id,
                    }
                    genererFacture(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            facture_automatique(response.data.bail_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function facture_automatique(bail_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'bail_id': bail_id
    }
    var loading = {
        type: "content",
        id_content: "contenair-loyer"
    }

    var url_request = bail_url.automatique_Bail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'facture_automatique_Callback', loading);
}

function facture_automatique_Callback(response) {
    var todaysDate = new Date();
    year = convertDate(todaysDate).split("-", 1).toString();
    getBail(response.cliLot_id);
}

function supprimer_facture(bail_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'bail_id': bail_id
    }
    var url_request = bail_url.supprimer_facture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'supprimer_facture_Callback');
}

function supprimer_facture_Callback(response) {
}

// Cloturer bail
$(document).on('click', '.btn_bail_cloture', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        bail_id: $('#bail_id').val(),
        cliLot_id: $('#lot_client_id').val()
    };
    cloturerBail(data_request);
});

function cloturerBail(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data;
    var url_request = bail_url.cloturerBail;
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'cloturerBail_Callback', loading);
}

function cloturerBail_Callback(response) {
    $("#modalValidationbailcloture").modal('show');
    $('#bail_id_cloture').val(response.bail_id);
    $('#cliLot_id_cloture').val(response.cliLot_id);
}

$(document).on('click', '#annulerCloture', function () {
    $("#modalValidationbailcloture").modal('hide');
});

$(document).on('click', '#confirmerCloture', function () {
    $("#modalValidationbailcloture").modal('hide');
    var data_request = {
        bail_id: $('#bail_id_cloture').val(),
        cliLot_id: $('#cliLot_id_cloture').val(),
        date_cloture: $('#bail_date_cloture').val()
    };
    validerCloture(data_request);
});

function validerCloture(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data_request;
    var url_request = bail_url.validerCloture;
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'validerCloture_Callback', loading)
}
function validerCloture_Callback(response) {
    Notiflix.Notify.success("Bail cloturé", { position: 'left-bottom', timeout: 3000 });
    getBail(response.cliLot_id);
}

// nouveau bail
$(document).on('click', '#btn_new_bail', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        bail_id: $('#bail_id').val(),
        cliLot_id: $('#lot_client_id').val()
    };
    nouveauBail(data_request);
});

function nouveauBail(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = bail_url.nouveauBail;
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourNouveauBail', loading);
}

function retourNouveauBail(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        bail_id: response.data.bail_id,
                    }
                    nouveauBail(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            // Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            // getBail(response.data.clilotId);
            $("#btn_creer_bail").trigger("click");
        }
    } else {
        Notiflix.Notify.failure(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

// nouveau avenant
$(document).on('click', '#btn_new_avenant', function (e) {
    e.preventDefault();
    var data_request = {
        action: "demande",
        bail_id: $('#bail_id').val(),
        cliLot_id: $('#lot_client_id').val()
    };
    nouveauAvenant(data_request);
});

function nouveauAvenant(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data;
    var url_request = bail_url.nouveauAvenant;
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourNouveauAvenant', loading);
}

function retourNouveauAvenant(response) {
    $("#modalValidationAvenant").modal('show');
    $('#bail_id_avenant').val(response.bail_id);
    $('#cliLot_id_avenant').val(response.cliLot_id);

    $('#bail_date_prochaine_indexation').val(response.bail_date_prochaine_indexation);
    $('#bail_facturation_loyer_celaviegestion').val(response.bail_facturation_loyer_celaviegestion);
    $('#bail_forfait_charge_indexer').val(response.bail_forfait_charge_indexer);
    $('#bail_remboursement_tom').val(response.bail_remboursement_tom);
    $('#bail_var_appliquee').val(response.bail_var_appliquee);
    $('#bail_var_capee').val(response.bail_var_capee);
    $('#bail_var_plafonnee').val(response.bail_var_plafonnee);
    $('#bail_var_min').val(response.bail_var_min);
    $('#gestionnaire_id_bail').val(response.gestionnaire_id);
    $('#indloyer_id').val(response.indloyer_id);
    $('#indval_id').val(response.indval_id);
    $('#momfac_id').val(response.momfac_id);
    $('#natef_id').val(response.natef_id);
    $('#pdl_id').val(response.pdl_id);
    $('#prdind_id').val(response.prdind_id);
    $('#premier_mois_trimes').val(response.premier_mois_trimes);
    $('#preneur_id').val(response.preneur_id);
    $('#tpba_id').val(response.tpba_id);
    $('#tpech_id').val(response.tpech_id);
    $('#premier_mois_trimes').val(response.premier_mois_trimes);
    $('#bail_art_605').val(response.bail_art_605);
    $('#bail_art_606').val(response.bail_art_606);
}

$(document).on('click', '#annulerAvenant', function () {
    $("#modalValidationAvenant").modal('hide');
});

$(document).on('click', '#confirmerAvenant', function () {
    $("#modalValidationAvenant").modal('hide');
    var bail_avenant_date_debut = new Date($('#bail_avenant_date_debut').val());
    bail_avenant_date_debut.setFullYear(bail_avenant_date_debut.getFullYear() + 1);
    var newDate = bail_avenant_date_debut.toISOString().substr(0, 10);

    addBail(
        null,
        $('#bail_avenant_date_debut').val(),
        newDate,
        0,
        0,
        $('#bail_remboursement_tom').val(),
        $('#bail_facturation_loyer_celaviegestion').val(),
        0,
        $('#bail_avenant_date_debut').val(),
        $('#bail_date_prochaine_indexation').val(),
        '',
        1,
        0,
        $('#pdl_id').val(),
        $('#natef_id').val(),
        $('#tpba_id').val(),
        $('#cliLot_id_avenant').val(),
        $('#gestionnaire_id_bail').val(),
        $('#tpech_id').val(),
        $('#bail_art_605').val(),
        $('#bail_art_606').val(),
        $('#indloyer_id').val(),
        $('#momfac_id').val(),
        $('#prdind_id').val(),
        $('#bail_forfait_charge_indexer').val(),
        $('#bail_avenant_date_debut').val(),
        $('#preneur_id').val(),
        1,
        $('#indval_id').val(),
        $('#bail_var_capee').val(),
        $('#bail_var_plafonnee').val(),
        $('#bail_var_appliquee').val(),
        1,
        $('#bail_id_avenant').val(),
        $('#premier_mois_trimes').val(),
        null,
        null,
        0,
        0
    );
});

// function gererAvoirAndFactures(date_prise_en_charge, bail_id) {
//     var type_request = 'POST';
//     var url_request = bail_url.gererAvoirAndFactures;
//     var type_output = 'html';
//     var data_request = {
//         bail_id: bail_id,
//         date_prise_en_charge: date_prise_en_charge,
//     };
//     fn.ajax_request(type_request, url_request, type_output, data_request, 'gererAvoirAndFactures_Callback');
// }

// function gererAvoirAndFactures_Callback(response) {

// }

$(document).on('click', '#generer_indexation', function () {
    var optionDataValid = $('#indice_valeur_loyer_id option:selected').attr('data-indice_valid');

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        bail_id: $('#bail_id').val(),
    };

    var indice_loyer_id = $('#indice_loyer_id').val();

    if (indice_loyer_id == 7) {
        var url_request = bail_url.generer_indexation_fixe;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'generer_indexations_Callback');
        return;
    }

    if ($('#indice_valeur_loyer_id').val() == 0) {
        $('#indice_valeur_loyer_id').trigger('change');
    } else if ($('#indlo_appartement_ht_ref').val() == 0 && $('#indlo_parking_ht_ref').val() == 0 && $('#indlo_charge_ht_ref').val() == 0) {
        Notiflix.Notify.failure("Le loyer de référence doit contenir au moins une valeur HT non zéro", {
            position: 'left-bottom',
            timeout: 4000
        });
    } else if (optionDataValid != 1) {
        var url_request = bail_url.generer_indexation_en_attente;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'generer_indexations_Callback');
    } else {
        var url_request = bail_url.generer_indexation;
        fn.ajax_request(type_request, url_request, type_output, data_request, 'generer_indexations_Callback');
    }
});

function generer_indexations_Callback(response) {
    getDocBails($('#lot_client_id').val());
}

$(document).on('click', '#btn_new_protocole', function () {
    modal_protocole();
});

function modal_protocole() {
    var type_request = 'POST';
    var url_request = bail_url.modal_protocole;
    var type_output = 'html';
    var data_request = {

    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modal_protocole_Callback');
}

function modal_protocole_Callback(response) {
    $('#modalValidationProtocol').modal('show');
    $('#modal-main-content-modal_protocol').html(response);
}

$(document).on('click', '#annulerProtocol', function () {
    $('#modalValidationProtocol').modal('hide');
});

$(document).on('click', '#confirmerProtocol', function () {
    var alldata = new FormData();
    var file = $('#file_protocol').prop('files')[0];
    alldata.append("file", file);

    var donnee = {
        bail_id: $('#bail_id').val(),
        id_protocol: $('#id_protocol').val(),
        date_sign_protocole: $('#date_sign_protocole').val(),
        type_abandon: $('#type_abandon').val(),
        pourcentage_protocol: $('#pourcentage_protocol').val(),
        info_complementaire: $('#info_complementaire').val(),
        date_debut_protocole: $('#date_debut_protocole').val(),
        date_fin_protocole: $('#date_fin_protocole').val(),
        cliLot_id: $('#cliLot_id').val()
    }

    alldata.append('data', JSON.stringify(donnee));
    alldata.append('file', file);

    var url_request = bail_url.saveProtocole;

    $.ajax({
        url: url_request,
        type: 'POST',
        data: alldata,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (response) {
            $('#modalValidationProtocol').modal('hide');
            Notiflix.Notify.success('Enregistrement Réussi', { position: 'left-bottom', timeout: 3000 });
            getDocBails(response.cliLot_id);
        },
    });
});

function getProtocoleBail(cliLot_id) {
    var type_request = 'POST';
    var url_request = bail_url.getProtocoleBail;
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id,
    };
    var loading = {
        type: "content",
        id_content: "contenair-protocol"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getProtocoleBail_Callback', loading);
}
function getProtocoleBail_Callback(response) {
    $('#container-protocol').html(response);
}

$(document).on('click', '#validerProtocol', function () {
    var id_protocol = $(this).attr('data-id');
    var data = {
        id_protocol: id_protocol,
        action: "demande",
    };
    validerProtocol(data);
});

function validerProtocol(data) {
    var type_request = 'POST';
    var url_request = bail_url.validerProtocol;
    var type_output = 'html';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'validerProtocol_Callback', loading);
}

function validerProtocol_Callback(response) {
    var response = JSON.parse(response);
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        id_protocol: response.data.id
                    }

                    validerProtocol(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            getProtocoleBail(response.data.cliLot_id);
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#consulterProtocol', function () {
    var id_protocol = $(this).attr('data-id');
    var type_request = 'POST';
    var url_request = bail_url.consulterProtocol;
    var type_output = 'html';
    var data_request = {
        id_protocol: id_protocol
    }
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'consulterProtocol_Callback', loading);
});
function consulterProtocol_Callback(response) {
    $('#modalValidationProtocol').modal('show');
    $('#modal-main-content-modal_protocol').html(response);
    $('#confirmerProtocol').hide();
    $('#date_sign_protocole').attr('disabled', true);
    $('#type_abandon').attr('disabled', true);
    $('#pourcentage_protocol').attr('disabled', true);
    $('#info_complementaire').attr('disabled', true);
    $('#date_debut_protocole').attr('disabled', true);
    $('#date_fin_protocole').attr('disabled', true);
    $('#fichier').hide();
}

$(document).on('click', '#envoiMailProtocol', function () {
    $('#btn_demande_valider_protocol').trigger('click');
});

$(document).on('click', '#modifierProtocol', function () {
    var id_protocol = $(this).attr('data-id');
    var type_request = 'POST';
    var url_request = bail_url.modifierProtocol;
    var type_output = 'html';
    var data_request = {
        id_protocol: id_protocol
    }
    var loading = {
        type: "content",
        id_content: "contenair-bail"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'modifierProtocol_Callback', loading);
});

function modifierProtocol_Callback(response) {
    $('#modalValidationProtocol').modal('show');
    $('#modal-main-content-modal_protocol').html(response);
    $('#confirmerProtocol').attr('disabled', false);
}

$(document).on('click', '#btn_bail_alert', function () {
    alert_dateSign_mandat('Bail');
});

function alert_dateSign_mandat(onglet) {
    var type_request = 'POST';
    var url_request = bail_url.alert_dateSigne_mandat;
    var type_output = 'html';
    var data_request = {
        onglet: onglet
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'alert_dateSign_mandat_Callback');
}

function alert_dateSign_mandat_Callback(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

function create_indexationZero(bail_id) {
    var type_request = 'POST';
    var url_request = bail_url.create_indexationZero;
    var type_output = 'html';
    var data_request = {
        bail_id: bail_id
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'create_indexationZero_callback');
    $('.pace-progress').css('display', 'none');
    $('.pace-progress-inner').css('display', 'none');
}
function create_indexationZero_callback(response) { }

function delete_indexationZero(bail_id) {
    var type_request = 'POST';
    var url_request = bail_url.delete_indexationZero;
    var type_output = 'html';
    var data_request = {
        bail_id: bail_id
    };
    fn.ajax_request(type_request, url_request, type_output, data_request, 'delete_indexationZero_callback');
    $('.pace-progress').css('display', 'none');
    $('.pace-progress-inner').css('display', 'none');
}
function delete_indexationZero_callback(response) { }