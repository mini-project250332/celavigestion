var endpoint_ = {
    updateLoyerReference: `${const_app.base_url}Bail/UpdateLoyerReference`,
    indexationExist: `${const_app.base_url}Bail/indexationExist`,
    DeleteIndexation: `${const_app.base_url}Bail/DeleteIndexation`
}

var myModalLoyerReference = new bootstrap.Modal(document.getElementById("modalValidationReference"), {
    keyboard: false
});

const delayTime = 1500;
let timeoutId;

$(document).on('keyup', '#indlo_appartement_ht_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateAppartLoyerRefTable();
    }, delayTime);
});

$(document).on('keyup', '#indlo_parking_ht_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateParkingLoyerRefTable();
    }, delayTime);
});

$(document).on('keyup', '#indlo_charge_ht_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateChargeLoyerRefTable();
    }, delayTime);
});

$(document).on('change', '#indlo_appartement_tva_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateAppartLoyerRefTable();
    }, delayTime);
});

$(document).on('change', '#indlo_parking_tva_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateParkingLoyerRefTable();
    }, delayTime);
});

$(document).on('change', '#indlo_charge_tva_ref', function (e) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
        updateChargeLoyerRefTable();
    }, delayTime);
});

$(document).on('change', '#indice_valeur_loyer_id', function (e) {
    updateLoyerReference();
});

function updateAppartLoyerRefTable(need_update = true) {
    let ht = $("#indlo_appartement_ht_ref").val();
    let tva = $("#indlo_appartement_tva_ref").val();
    tva = (+ht) * ((+tva) / 100);
    let ttc = (+ht) + (+tva);

    $('#indlo_appartement_tva_euro_ref').val(tva.toFixed(2));
    $('#indlo_appartement_ttc_ref').val(ttc.toFixed(2));
    updateLoyerReference(need_update);
}

function updateParkingLoyerRefTable(need_update = true) {
    let ht = $("#indlo_parking_ht_ref").val();
    let tva = $("#indlo_parking_tva_ref").val();
    tva = (+ht) * ((+tva) / 100);
    let ttc = (+ht) + (+tva);

    $('#indlo_parking_tva_euro_ref').val(tva.toFixed(2));
    $('#indlo_parking_ttc_ref').val(ttc.toFixed(2));
    updateLoyerReference(need_update);
}

function updateChargeLoyerRefTable(need_update = true) {
    let ht = $("#indlo_charge_ht_ref").val();
    let tva = $("#indlo_charge_tva_ref").val();
    tva = (+ht) * ((+tva) / 100);
    let ttc = (+ht) + (+tva);

    $('#indlo_charge_tva_euro_ref').val(tva.toFixed(2));
    $('#indlo_charge_ttc_ref').val(ttc.toFixed(2));
    updateLoyerReference(need_update);
}

function updateLoyerReference(need_update = true, date_fin = null) {
    if (!need_update) return false;
    var type_request = 'POST';
    var type_output = 'json';
    var url_request = endpoint_.updateLoyerReference;
    var data_request = {
        'indlo_id': $("#indexation_ref_id").val(),
        'indlo_appartement_ht': $("#indlo_appartement_ht_ref").val(),
        'indlo_appartement_tva': $("#indlo_appartement_tva_ref").val(),
        'indlo_appartement_ttc': $("#indlo_appartement_ttc_ref").val(),
        'indlo_parking_ht': $("#indlo_parking_ht_ref").val(),
        'indlo_parking_tva': $("#indlo_parking_tva_ref").val(),
        'indlo_parking_ttc': $("#indlo_parking_ttc_ref").val(),
        'indlo_charge_ht': $("#indlo_charge_ht_ref").val(),
        'indlo_charge_tva': $("#indlo_charge_tva_ref").val(),
        'indlo_charge_ttc': $("#indlo_charge_ttc_ref").val(),
        'indlo_indice_base': $("#indice_valeur_loyer_id").val(),
        'indlo_debut': $("#bail_date_premiere_indexation").val(),
        'indlo_fin': date_fin == null ? $("#bail_date_prochaine_indexation").val() : date_fin
    };

    if ($("#indice_valeur_loyer_id").val() == 0 && $('#periode_indexation').val() != 3 && $('#indice_loyer_id').val() != 7) {
        updateBailInfo();

        fn.ajax_request(type_request, url_request, type_output, data_request, 'updateLoyerReferenceCallback');
        setTimeout(() => {
            $(".input_first_disabled_ref").attr("disabled", false);
        }, 500);

        Notiflix.Notify.failure("Veuillez renseigner l'indice de référence.", {
            position: 'left-bottom',
            timeout: 3000
        });
    } else if (parseInt($("#count_indexation_loyer_ref").val()) > 1) {
        var data_request = {
            action: "demande",
            bail_id: $('#bail_id').val(),
        };

        IndexationExist(data_request);
    } else {
        updateBailInfo();

        fn.ajax_request(type_request, url_request, type_output, data_request, 'updateLoyerReferenceCallback');
        setTimeout(() => {
            $(".input_first_disabled_ref").attr("disabled", false);
        }, 500);
    }
}

function updateLoyerReferenceCallback(response) {
    getIndexationLoyer(response.data_retour.details.bail_id);

    if ($('#periode_indexation').val() == 3) {
        create_indexationZero(response.data_retour.details.bail_id);
    }
}

function IndexationExist(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = data;
    var url_request = endpoint_.indexationExist;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourIndexationExist');
    myModalLoyerReference.toggle();
}
function retourIndexationExist(response) {
    $('#bail_id_reference').val(response.bail_id);
}

$(document).on('click', '#annuler_changement_reference', function () {
    getLoyerReference($('#bail_id_reference').val());
    restoreBail($('#lot_client_id').val());
    setTimeout(() => {
        $(".input_first_disabled_ref").attr("disabled", false);
    }, 1000);
});

$(document).on('click', '#confirmer_changement_reference', function () {
    ModifLoyeReference();
    updateBailDateFin_loyer_reference();
    DeleteIndexation($('#bail_id_reference').val());
    if ($('#periode_indexation').val() == 3) {
        create_indexationZero($('#bail_id').val());
    }
    else {
        delete_indexationZero($('#bail_id').val());
    }
});

function ModifLoyeReference(indlo_id) {
    var type_request = 'POST';
    var url_request = endpoint_.updateLoyerReference;
    var type_output = 'json';

    var data_request = {
        'indlo_id': $("#indexation_ref_id").val(),
        'indlo_appartement_ht': $("#indlo_appartement_ht_ref").val(),
        'indlo_appartement_tva': $("#indlo_appartement_tva_ref").val(),
        'indlo_appartement_ttc': $("#indlo_appartement_ttc_ref").val(),
        'indlo_parking_ht': $("#indlo_parking_ht_ref").val(),
        'indlo_parking_tva': $("#indlo_parking_tva_ref").val(),
        'indlo_parking_ttc': $("#indlo_parking_ttc_ref").val(),
        'indlo_charge_ht': $("#indlo_charge_ht_ref").val(),
        'indlo_charge_tva': $("#indlo_charge_tva_ref").val(),
        'indlo_charge_ttc': $("#indlo_charge_ttc_ref").val(),
        'indlo_indice_base': $("#indice_valeur_loyer_id").val(),
        'indlo_debut': $("#bail_date_premiere_indexation").val(),
        'indlo_fin': $("#bail_date_prochaine_indexation").val()
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'ModifLoyeReferenceCallback');
}

function ModifLoyeReferenceCallback(response) {

}

function restoreBail(cliLot_id) {
    var type_request = 'POST';
    var url_request = bail_url.getBail;
    var type_output = 'json';

    var data_request = {
        'cliLot_id': cliLot_id,
    };

    fn.ajax_request(type_request, url_request, type_output, data_request, 'restoreBailCallback');
}

function restoreBailCallback(response) {
    if (response.data_retour != null) {
        const current_bail = response.data_retour[0];
        $(".aucun_bail").hide();
        $(".detail_bail").show();
        $('#bail_id').val(current_bail.bail_id);
        $('#bail_debut').val(current_bail.bail_debut);
        $('#bail_fin').val(current_bail.bail_fin);
        $('#bail_resiliation_triennale').val(current_bail.bail_resiliation_triennale);
        $('#bail_loyer_variable').val(current_bail.bail_loyer_variable);
        $('#bail_date_premiere_indexation').val(current_bail.bail_date_premiere_indexation);
        $('#bail_date_prochaine_indexation').val(current_bail.bail_date_prochaine_indexation);
        $('#bail_date_echeance').val(current_bail.bail_date_echeance);
        $('#bail_indexation').val(current_bail.bail_indexation);
        $('#bail_commentaire').val(current_bail.bail_commentaire);
        $('#bail_annee').val(current_bail.bail_annee);
        $('#bail_mois').val(current_bail.bail_mois);
        $('#periodicite_loyer_id').val(current_bail.pdl_id);
        if (current_bail.pdl_id == 5) {
            $('#1er_trimes_decal_affichage').removeClass('d-none');
        }
        $('#nature_prise_effet_id').val(current_bail.natef_id);
        $('#type_bail_id').val(current_bail.tpba_id);
        $('#gestionnaire_id_bail').val(current_bail.gestionnaire_id);
        $('#type_echeance_id').val(current_bail.tpech_id);
        $('#bail_art_605_id').val(current_bail.bail_art_605);
        $('#bail_art_606_id').val(current_bail.bail_art_606);
        $('#indice_loyer_id').val(current_bail.indloyer_id);

        $('#last_indice_valeur_loyer_id').val(current_bail.indval_id);
        $('#moment_facturation_bail_id').val(current_bail.momfac_id);
        $('#periode_indexation').val(current_bail.prdind_id);
        $('#bail_forfait_charge_indexer').val(current_bail.bail_forfait_charge_indexer);
        $('#countIndexation').val(response.data_retour["indexations"].length);
        $('#preneur_id').val(current_bail.preneur_id);
        $('#cliLot_id ').val(current_bail.cliLot_id);

        $('#bail_var_capee').val(current_bail.bail_var_capee);
        $('#bail_var_plafonnee').val(current_bail.bail_var_plafonnee);
        $('#bail_var_appliquee').val(current_bail.bail_var_appliquee);
        $('#premier_mois_trimes').val(current_bail.premier_mois_trimes);
        $('#bail_date_debut_mission').val(current_bail.bail_date_debut_mission);

        trie_indice_reference(current_bail.indloyer_id, current_bail.indval_id);

        tri_indice_plafonnement(current_bail.indice_valeur_plafonnement);

        if (current_bail.bail_loyer_variable === "2") {
            $(".main_indexation_loyer").hide();
        } else {
            $(".main_indexation_loyer").show();
        }

        current_bail.bail_remboursement_tom == 1 ? $('#bail_remboursement_tom').prop('checked', true) : $('#bail_remboursement_tom').prop('checked', false);
        current_bail.bail_charge_recuperable == 1 ? $('#bail_charge_recuperable').prop('checked', true) : $('#bail_charge_recuperable').prop('checked', false);
        current_bail.bail_facturation_loyer_celaviegestion == 1 ? $('#bail_facturation_loyer_celaviegestion').prop('checked', true) : $('#bail_facturation_loyer_celaviegestion').prop('checked', false);

        var todaysDate = new Date();
        year = convertDate(todaysDate).split("-", 1).toString();

        if (current_bail.prdind_id == 2 && current_bail.indloyer_id != 7) {
            $('#mod_calcul').removeClass('d-none');
            $('#bail_mode_calcul').val(current_bail.bail_mode_calcul);
        }

        if (current_bail.bail_avenant == 1) {
            $('#tacite_en_cours').text('Bail Avenant en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
            $('#tacite_prolongation').val('non');

            // change text button
            $('.bail_action_valid').text("Valider l'avenant");
            $('.bail_action_devalid').text("Annuler la validation de l'avenant");
            // $('.bail_action_cloture').text("Cloturer l'avenant");

        } else {
            if (current_bail.bail_cloture == 1) {
                $('#tacite_en_cours').text('Bail cloturé du ' + ChangeFormateDate(current_bail.bail_date_cloture) + ' :');
                $('#tacite_prolongation').val('non');
            } else if (current_bail.bail_fin != "0000-00-00") {
                var todaysDate = new Date();
                current_bail.bail_fin <= convertDate(todaysDate) ? $('#tacite_en_cours').text('Bail en tacite du ' + ChangeFormateDate(current_bail.bail_fin) + ' :') : $('#tacite_en_cours').text('Bail en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
                current_bail.bail_fin <= convertDate(todaysDate) ? $('#tacite_prolongation').val('oui') : $('#tacite_prolongation').val('non');
            } else {
                $('#tacite_en_cours').text('Bail en cours du ' + ChangeFormateDate(current_bail.bail_debut) + ' :');
                $('#tacite_prolongation').val('non');
            }
        }

        // Verify if bail is validated
        if (current_bail.bail_valide == 0) {
            $('#bail_validation').text("(Non validé)");
            $('#bail_validation').addClass("bail_non_valide");
            $('#bail_validation').removeClass("bail_valide");
            $(".contain_btn_edit_bail").show();
            $(".contain_addfranchisemodal").show();
            $('.bail_action_devalid').addClass('d-none');
            $('.bail_action_valid').show();
        } else {
            $('#bail_validation').text("(Validé)");
            $('#bail_validation').addClass("bail_valide");
            $('#bail_validation').removeClass("bail_non_valide");
            $('.bail_action_valid').hide();
            $('.bail_action_devalid').removeClass('d-none');
            $(".contain_btn_edit_bail").hide();
            $(".contain_addfranchisemodal").hide();
        }

        // Button cloturer le bail
        if (current_bail.bail_cloture == 1) $('.bail_action_cloture').hide();
    }
}

function DeleteIndexation(bail_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        bail_id: bail_id
    };
    var url_request = endpoint_.DeleteIndexation;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourDeleteIndexation');
}

function retourDeleteIndexation(response) {
    updateBailInfo();
    setTimeout(() => {
        $(".input_first_disabled_ref").attr("disabled", false);
        $('#count_indexation_loyer_ref').val('');
        $("#countIndexation").val('');
        getIndexationLoyer(response.data_post.bail_id);
    }, 1000);
}

function updateBailDateFin_loyer_reference() {
    const bail_annee = $("#bail_annee").val();
    const bail_mois = $("#bail_mois").val();
    const bail_debut = $("#bail_debut").val();
    if (bail_debut) {
        const date_debut = new Date(bail_debut);
        let date_fin = new Date(date_debut.setFullYear(date_debut.getFullYear() + parseInt(bail_annee)));
        date_fin = new Date(date_fin.setMonth(date_fin.getMonth() + parseInt(bail_mois)));
        const monthly = date_fin.getMonth() + 1;
        const month = monthly < 10 ? '0' + monthly : monthly;
        const date = date_fin.getDate() < 10 ? '0' + date_fin.getDate() : date_fin.getDate();
        $("#bail_fin").val(`${date_fin.getFullYear()}-${month}-${date}`);
    }
}