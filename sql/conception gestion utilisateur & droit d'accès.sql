
-- -----------------------------------------------------
-- Schema c_estlaviedb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS c_estlaviedb ;

-- -----------------------------------------------------
-- Schema c_estlaviedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS c_estlaviedb DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema c_estlaviedb
-- -----------------------------------------------------
USE c_estlaviedb ;

-- -----------------------------------------------------
-- Table c_estlaviedb.c_typeaccount
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_typeaccount ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_typeaccount (
  taccount_id INT NOT NULL AUTO_INCREMENT,
  taccount_libelle VARCHAR(45) NULL,
  taccount_sigle VARCHAR(5) NULL,
  PRIMARY KEY (taccount_id))
ENGINE = InnoDB;

CREATE UNIQUE INDEX tusers_id_UNIQUE ON c_estlaviedb.c_typeaccount (taccount_id ASC);

-- -----------------------------------------------------
-- Table c_estlaviedb.c_users
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_users ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_users (
  users_id INT NOT NULL AUTO_INCREMENT,
  users_name VARCHAR(45) NULL,
  users_firstname VARCHAR(45) NULL,
  users_datebirth DATE NULL,
  users_cellphone VARCHAR(20) NULL,
  users_phone VARCHAR(20) NULL,
  users_mail VARCHAR(50) NULL,
  users_cp VARCHAR(10) NULL,
  users_rue VARCHAR(50) NULL,
  users_city VARCHAR(50) NULL,
  users_photo VARCHAR(100) NULL,
  users_stamp VARCHAR(45) NULL,
  PRIMARY KEY (users_id))
ENGINE = InnoDB;

CREATE UNIQUE INDEX users_id_UNIQUE ON c_estlaviedb.c_users (users_id ASC);


-- -----------------------------------------------------
-- Table c_estlaviedb.c_accountuser
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_accountuser ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_accountuser (
  auser_id INT NOT NULL AUTO_INCREMENT,
  auser_login VARCHAR(50) NULL,
  auser_password VARCHAR(50) NULL,
  auser_status INT NULL,
  auser_stamp VARCHAR(45) NULL,
  taccount_id INT NOT NULL,
  users_id INT NOT NULL,
  PRIMARY KEY (auser_id),
  CONSTRAINT fk_c_accountuser_c_typeaccount
    FOREIGN KEY (taccount_id)
    REFERENCES c_estlaviedb.c_typeaccount (taccount_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_c_accountuser_c_users1
    FOREIGN KEY (users_id)
    REFERENCES c_estlaviedb.c_users (users_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX auser_id_UNIQUE ON c_estlaviedb.c_accountuser (auser_id ASC);

CREATE INDEX fk_c_accountuser_c_typeaccount_idx ON c_estlaviedb.c_accountuser (taccount_id ASC);

CREATE INDEX fk_c_accountuser_c_users1_idx ON c_estlaviedb.c_accountuser (users_id ASC);


-- -----------------------------------------------------
-- Table c_estlaviedb.c_permission
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_permission ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_permission (
  permission_id INT NOT NULL AUTO_INCREMENT,
  permission_section VARCHAR(45) NULL,
  permission_libelle VARCHAR(45) NULL,
  permission_code VARCHAR(5) NULL,
  PRIMARY KEY (permission_id))
ENGINE = InnoDB;

CREATE UNIQUE INDEX permission_id_UNIQUE ON c_estlaviedb.c_permission (permission_id ASC);


-- -----------------------------------------------------
-- Table c_estlaviedb.c_permissionuser
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_permissionuser ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_permissionuser (
  auser_id INT NOT NULL,
  permission_id INT NOT NULL,
  permissionuser_stamp VARCHAR(45) NULL,
  PRIMARY KEY (auser_id, permission_id),
  CONSTRAINT fk_c_permissionuser_c_accountuser1
    FOREIGN KEY (auser_id)
    REFERENCES c_estlaviedb.c_accountuser (auser_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_c_permissionuser_c_permission1
    FOREIGN KEY (permission_id)
    REFERENCES c_estlaviedb.c_permission (permission_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX fk_c_permissionuser_c_accountuser1_idx ON c_estlaviedb.c_permissionuser (auser_id ASC);

CREATE INDEX fk_c_permissionuser_c_permission1_idx ON c_estlaviedb.c_permissionuser (permission_id ASC);


-- -----------------------------------------------------
-- Table c_estlaviedb.c_flowaccess
-- -----------------------------------------------------
DROP TABLE IF EXISTS c_estlaviedb.c_flowaccess ;

CREATE TABLE IF NOT EXISTS c_estlaviedb.c_flowaccess (
  flowa_id INT NOT NULL AUTO_INCREMENT,
  flowa_token VARCHAR(50) NULL,
  flowa_datecreate DATETIME NULL,
  flowa_datelogin DATETIME NULL,
  flowa_datelogout DATETIME NULL,
  flowa_ip VARCHAR(20) NULL,
  flowa_agent VARCHAR(20) NULL,
  flowa_action INT(3) NULL,
  flowa_status INT NULL,
  auser_id INT NULL,
  PRIMARY KEY (flowa_id),
  CONSTRAINT fk_c_flowaccess_c_accountuser1
    FOREIGN KEY (auser_id)
    REFERENCES c_estlaviedb.c_accountuser (auser_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX flowa_id_UNIQUE ON c_estlaviedb.c_flowaccess (flowa_id ASC);

CREATE INDEX fk_c_flowaccess_c_accountuser1_idx ON c_estlaviedb.c_flowaccess (auser_id ASC);
