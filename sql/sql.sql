-- phpMyAdmin SQL Dump

-- version 5.0.2

-- https://www.phpmyadmin.net/

--

-- Hôte : 127.0.0.1:3306

-- Généré le : mar. 17 mai 2022 à 12:04

-- Version du serveur :  5.7.31

-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

START TRANSACTION;

SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */

;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */

;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */

;

/*!40101 SET NAMES utf8mb4 */

;

--

-- Base de données : `c_estlaviedb`

--

-- --------------------------------------------------------

--

-- Structure de la table `c_client`

--

DROP TABLE IF EXISTS `c_client`;

CREATE TABLE
    IF NOT EXISTS `c_client` (
        `client_id` int(11) NOT NULL AUTO_INCREMENT,
        `client_nom` varchar(120) DEFAULT NULL,
        `client_prenom` varchar(120) DEFAULT NULL,
        `client_entreprise` varchar(50) NOT NULL,
        `client_date_creation` datetime DEFAULT NULL,
        `client_date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `client_email` varchar(50) NOT NULL,
        `client_phonemobile` varchar(120) DEFAULT NULL,
        `client_phonefixe` varchar(120) DEFAULT NULL,
        `client_adresse1` varchar(120) DEFAULT NULL,
        `client_adresse2` varchar(120) DEFAULT NULL,
        `client_adresse3` varchar(120) DEFAULT NULL,
        `client_cp` varchar(120) DEFAULT NULL,
        `client_ville` varchar(120) DEFAULT NULL,
        `client_pays` varchar(120) DEFAULT NULL,
        `client_nationalite` varchar(120) DEFAULT NULL,
        `client_commentaire` text,
        `etat_client_id` int(11) NOT NULL,
        `origine_cli_id` int(11) NOT NULL,
        `util_id` int(11) NOT NULL,
        PRIMARY KEY (`client_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_client`

--

INSERT INTO
    `c_client` (
        `client_id`,
        `client_nom`,
        `client_prenom`,
        `client_entreprise`,
        `client_date_creation`,
        `client_date_modification`,
        `client_email`,
        `client_phonemobile`,
        `client_phonefixe`,
        `client_adresse1`,
        `client_adresse2`,
        `client_adresse3`,
        `client_cp`,
        `client_ville`,
        `client_pays`,
        `client_nationalite`,
        `client_commentaire`,
        `etat_client_id`,
        `origine_cli_id`,
        `util_id`
    )
VALUES (
        1,
        'Manao',
        '',
        'Entreprise I',
        '2022-05-17 00:00:00',
        '2022-05-17 09:48:57',
        'brillantloroque@gmail.com',
        '+261346863299',
        '',
        'Imandry Fianarantsoa / Madagascar',
        '',
        '',
        '301',
        'Fianarantsoa',
        'Madagascar',
        'Française',
        'test',
        0,
        1,
        10
    );

-- --------------------------------------------------------

--

-- Structure de la table `c_communication_prospect`

--

DROP TABLE IF EXISTS `c_communication_prospect`;

CREATE TABLE
    IF NOT EXISTS `c_communication_prospect` (
        `comprospect_id` int(11) NOT NULL AUTO_INCREMENT,
        `comprospect_texte` varchar(250) DEFAULT NULL,
        `comprospect_date_creation` datetime NOT NULL,
        `prospect_id` int(11) NOT NULL,
        `util_id` int(11) NOT NULL,
        `type_comprospect_id` int(11) NOT NULL,
        PRIMARY KEY (`comprospect_id`),
        KEY `fk_c_communication_prospect_c_prospect1_idx` (`prospect_id`),
        KEY `fk_c_communication_prospect_c_type_communication_prospect1_idx` (`type_comprospect_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_etat_prospect`

--

DROP TABLE IF EXISTS `c_etat_prospect`;

CREATE TABLE
    IF NOT EXISTS `c_etat_prospect` (
        `etat_prospect_id` int(11) NOT NULL AUTO_INCREMENT,
        `etat_prospect_libelle` varchar(45) DEFAULT NULL,
        PRIMARY KEY (`etat_prospect_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_gestionnaire`

--

DROP TABLE IF EXISTS `c_gestionnaire`;

CREATE TABLE
    IF NOT EXISTS `c_gestionnaire` (
        `gestionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
        `gestionnaire_nom` varchar(120) DEFAULT NULL,
        `gestionnaire_adresse1` varchar(120) DEFAULT NULL,
        `gestionnaire_adresse2` varchar(120) DEFAULT NULL,
        `gestionnaire_adresse3` varchar(120) DEFAULT NULL,
        `gestionnaire_cp` varchar(45) DEFAULT NULL,
        `gestionnaire_ville` varchar(120) DEFAULT NULL,
        `gestionnaire_pays` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`gestionnaire_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_historiqueacces`

--

DROP TABLE IF EXISTS `c_historiqueacces`;

CREATE TABLE
    IF NOT EXISTS `c_historiqueacces` (
        `hacces_id` int(11) NOT NULL AUTO_INCREMENT,
        `hacces_token` text,
        `hacces_datecreate` datetime DEFAULT NULL,
        `hacces_datelogin` datetime DEFAULT NULL,
        `hacces_datelogout` datetime DEFAULT NULL,
        `hacces_ip` varchar(20) DEFAULT NULL,
        `hacces_agent` varchar(20) DEFAULT NULL,
        `hacces_action` int(3) DEFAULT NULL,
        `hacces_status` int(11) DEFAULT NULL,
        `util_id` int(11) DEFAULT NULL,
        PRIMARY KEY (`hacces_id`),
        UNIQUE KEY `flowa_id_UNIQUE` (`hacces_id`),
        KEY `fk_c_historiqueacces_c_utilisateur1_idx` (`util_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 5 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_historiqueacces`

--

INSERT INTO
    `c_historiqueacces` (
        `hacces_id`,
        `hacces_token`,
        `hacces_datecreate`,
        `hacces_datelogin`,
        `hacces_datelogout`,
        `hacces_ip`,
        `hacces_agent`,
        `hacces_action`,
        `hacces_status`,
        `util_id`
    )
VALUES (
        1,
        'c0fac08618b2639077c0f63942c417b79da2ecb0c77fddc3dc44b46fbfc72274',
        '2022-05-11 12:37:58',
        '2022-05-11 12:38:39',
        NULL,
        '::1',
        'Chrome',
        2,
        1,
        10
    ), (
        2,
        '879ba909d918e5d950e3964e38d3a801c4c1bf66134a1cc636f31360c015abb5',
        '2022-05-12 07:46:00',
        NULL,
        '2022-05-12 10:01:24',
        '::1',
        'Chrome',
        3,
        1,
        NULL
    ), (
        3,
        '3af4db3079f8a14120410770035a2d13bb042cd4bdf093eeb26277c7a06c6c65',
        '2022-05-12 10:01:24',
        '2022-05-12 11:05:29',
        NULL,
        '::1',
        'Chrome',
        2,
        1,
        10
    ), (
        4,
        '6479a9e8bfb5d4ad7aefa14ab0f44a4e4e61522ec45da4712e0afcd638203a66',
        '2022-05-17 07:05:39',
        '2022-05-17 07:05:54',
        NULL,
        '::1',
        'Chrome',
        2,
        1,
        10
    );

-- --------------------------------------------------------

--

-- Structure de la table `c_lot_prospect`

--

DROP TABLE IF EXISTS `c_lot_prospect`;

CREATE TABLE
    IF NOT EXISTS `c_lot_prospect` (
        `lot_id` int(11) NOT NULL AUTO_INCREMENT,
        `lot_nom` varchar(120) DEFAULT NULL,
        `lot_adresse1` varchar(120) DEFAULT NULL,
        `lot_adresse2` varchar(120) DEFAULT NULL,
        `lot_adresse3` varchar(120) DEFAULT NULL,
        `lot_cp` varchar(45) DEFAULT NULL,
        `lot_ville` varchar(120) DEFAULT NULL,
        `lot_pays` varchar(120) DEFAULT NULL,
        `lot_principale` varchar(45) DEFAULT NULL,
        `prospect_id` int(11) NOT NULL,
        `gestionnaire_id` int(11) NOT NULL,
        `typelot_id` int(11) NOT NULL,
        `progm_id` int(11) NOT NULL,
        `typologielot_id` int(11) NOT NULL,
        PRIMARY KEY (`lot_id`),
        KEY `fk_c_lot_prospect_c_prospect1` (`prospect_id`),
        KEY `fk_c_lot_prospect_c_gestionnaire1` (`gestionnaire_id`),
        KEY `fk_c_lot_prospect_c_typelot1` (`typelot_id`),
        KEY `fk_c_lot_prospect_c_programme1` (`progm_id`),
        KEY `fk_c_lot_prospect_c_typologieLot1` (`typologielot_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_origine_client`

--

DROP TABLE IF EXISTS `c_origine_client`;

CREATE TABLE
    IF NOT EXISTS `c_origine_client` (
        `origine_cli_id` int(11) NOT NULL AUTO_INCREMENT,
        `origine_cli_libelle` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`origine_cli_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_origine_client`

--

INSERT INTO
    `c_origine_client` (
        `origine_cli_id`,
        `origine_cli_libelle`
    )
VALUES (1, 'Nouveau client'), (2, 'CGP'), (3, 'Client CLV'), (4, 'Client IPierre'), (5, 'Client Synergestion');

-- --------------------------------------------------------

--

-- Structure de la table `c_origine_demande`

--

DROP TABLE IF EXISTS `c_origine_demande`;

CREATE TABLE
    IF NOT EXISTS `c_origine_demande` (
        `origine_dem_id` int(11) NOT NULL AUTO_INCREMENT,
        `origine_dem_libelle` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`origine_dem_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 5 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_origine_demande`

--

INSERT INTO
    `c_origine_demande` (
        `origine_dem_id`,
        `origine_dem_libelle`
    )
VALUES (1, 'Site internet'), (2, 'Publicité'), (3, 'Bouche à oreille'), (4, 'Prescripteurs');

-- --------------------------------------------------------

--

-- Structure de la table `c_parcours_vente`

--

DROP TABLE IF EXISTS `c_parcours_vente`;

CREATE TABLE
    IF NOT EXISTS `c_parcours_vente` (
        `vente_id` int(11) NOT NULL AUTO_INCREMENT,
        `vente_libelle` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`vente_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_permission`

--

DROP TABLE IF EXISTS `c_permission`;

CREATE TABLE
    IF NOT EXISTS `c_permission` (
        `permission_id` int(11) NOT NULL AUTO_INCREMENT,
        `permission_section` varchar(45) DEFAULT NULL,
        `permission_libelle` varchar(45) DEFAULT NULL,
        `permission_code` varchar(5) DEFAULT NULL,
        PRIMARY KEY (`permission_id`),
        UNIQUE KEY `permission_id_UNIQUE` (`permission_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_permissiontypeutilisateur`

--

DROP TABLE IF EXISTS `c_permissiontypeutilisateur`;

CREATE TABLE
    IF NOT EXISTS `c_permissiontypeutilisateur` (
        `permissiontutli_stamp` varchar(45) DEFAULT NULL,
        `permission_id` int(11) NOT NULL,
        `tutil_id` int(11) NOT NULL,
        KEY `fk_c_permissionuser_c_permission1_idx` (`permission_id`),
        KEY `fk_c_permissionuser_c_typeaccount1_idx` (`tutil_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_produit`

--

DROP TABLE IF EXISTS `c_produit`;

CREATE TABLE
    IF NOT EXISTS `c_produit` (
        `produit_id` int(11) NOT NULL AUTO_INCREMENT,
        `produit_libelle` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`produit_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_programme`

--

DROP TABLE IF EXISTS `c_programme`;

CREATE TABLE
    IF NOT EXISTS `c_programme` (
        `progm_id` int(11) NOT NULL AUTO_INCREMENT,
        `progm_nom` varchar(120) DEFAULT NULL,
        `progm_adresse1` varchar(120) DEFAULT NULL,
        `progm_adresse2` varchar(120) DEFAULT NULL,
        `progm_adresse3` varchar(120) DEFAULT NULL,
        `progm_cp` varchar(45) DEFAULT NULL,
        `progm_ville` varchar(120) DEFAULT NULL,
        `progm_pays` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`progm_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_prospect`

--

DROP TABLE IF EXISTS `c_prospect`;

CREATE TABLE
    IF NOT EXISTS `c_prospect` (
        `prospect_id` int(11) NOT NULL AUTO_INCREMENT,
        `prospect_nom` varchar(120) DEFAULT NULL,
        `prospect_prenom` varchar(120) DEFAULT NULL,
        `prospect_email` varchar(120) DEFAULT NULL,
        `prospect_entreprise` varchar(120) DEFAULT NULL,
        `prospect_date_creation` datetime DEFAULT NULL,
        `prospect_date_modification` timestamp NULL DEFAULT NULL,
        `prospect_phonemobile` varchar(120) DEFAULT NULL,
        `prospect_phonefixe` varchar(120) DEFAULT NULL,
        `prospect_adresse1` varchar(120) DEFAULT NULL,
        `prospect_adresse2` varchar(120) DEFAULT NULL,
        `prospect_adresse3` varchar(120) DEFAULT NULL,
        `prospect_cp` varchar(120) DEFAULT NULL,
        `prospect_ville` varchar(120) DEFAULT NULL,
        `prospect_pays` varchar(120) DEFAULT NULL,
        `prospect_nationalite` varchar(120) DEFAULT NULL,
        `prospect_commentaire` text,
        `etat_prospect_id` int(11) NOT NULL,
        `origine_cli_id` int(11) NOT NULL,
        `origine_dem_id` int(11) NOT NULL,
        `produit_id` int(11) NOT NULL,
        `vente_id` int(11) NOT NULL,
        `util_id` int(11) NOT NULL,
        PRIMARY KEY (`prospect_id`),
        KEY `fk_c_prospect_etat_prospect1` (`etat_prospect_id`),
        KEY `fk_c_prospect_origine_client1` (`origine_cli_id`),
        KEY `fk_c_prospect_origine_demande1` (`origine_dem_id`),
        KEY `fk_c_prospect_produit1` (`produit_id`),
        KEY `fk_c_prospect_parcours_vente1` (`vente_id`),
        KEY `fk_c_prospect_c_utilisateur1` (`util_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_typelot`

--

DROP TABLE IF EXISTS `c_typelot`;

CREATE TABLE
    IF NOT EXISTS `c_typelot` (
        `typelot_id` int(11) NOT NULL,
        `typelot_libelle` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`typelot_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_typelot`

--

INSERT INTO
    `c_typelot` (
        `typelot_id`,
        `typelot_libelle`
    )
VALUES (1, 'Non renseigné'), (2, 'Bail commercial'), (3, 'Bail habitation'), (4, 'Mandat de gestion'), (5, 'Location saisonnière');

-- --------------------------------------------------------

--

-- Structure de la table `c_typeutilisateur`

--

DROP TABLE IF EXISTS `c_typeutilisateur`;

CREATE TABLE
    IF NOT EXISTS `c_typeutilisateur` (
        `tutil_id` int(11) NOT NULL AUTO_INCREMENT,
        `tutil_libelle` varchar(45) DEFAULT NULL,
        `tutil_sigle` varchar(45) DEFAULT NULL,
        PRIMARY KEY (`tutil_id`),
        UNIQUE KEY `tusers_id_UNIQUE` (`tutil_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_typeutilisateur`

--

INSERT INTO
    `c_typeutilisateur` (
        `tutil_id`,
        `tutil_libelle`,
        `tutil_sigle`
    )
VALUES (
        1,
        'Administrateur principal',
        'root'
    ), (2, 'Administrateur', 'admin');

-- --------------------------------------------------------

--

-- Structure de la table `c_type_communication_prospect`

--

DROP TABLE IF EXISTS `c_type_communication_prospect`;

CREATE TABLE
    IF NOT EXISTS `c_type_communication_prospect` (
        `type_comprospect_id` int(11) NOT NULL AUTO_INCREMENT,
        `type_comprospect_libelle` varchar(45) DEFAULT NULL,
        PRIMARY KEY (`type_comprospect_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_typologielot`

--

DROP TABLE IF EXISTS `c_typologielot`;

CREATE TABLE
    IF NOT EXISTS `c_typologielot` (
        `typologielot_id` int(11) NOT NULL AUTO_INCREMENT,
        `typologielot_libelle` varchar(100) DEFAULT NULL,
        PRIMARY KEY (`typologielot_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--

-- Structure de la table `c_utilisateur`

--

DROP TABLE IF EXISTS `c_utilisateur`;

CREATE TABLE
    IF NOT EXISTS `c_utilisateur` (
        `util_id` int(11) NOT NULL AUTO_INCREMENT,
        `util_nom` varchar(45) DEFAULT NULL,
        `util_prenom` varchar(45) DEFAULT NULL,
        `util_datenaissance` date DEFAULT NULL,
        `util_phonemobile` varchar(20) DEFAULT NULL,
        `util_phonefixe` varchar(20) DEFAULT NULL,
        `util_mail` varchar(50) DEFAULT NULL,
        `util_cp` varchar(10) DEFAULT NULL,
        `util_adresse` varchar(50) DEFAULT NULL,
        `util_ville` varchar(50) DEFAULT NULL,
        `util_photo` varchar(100) DEFAULT NULL,
        `util_login` varchar(50) DEFAULT NULL,
        `util_password` text,
        `util_status` int(11) DEFAULT NULL,
        `util_datecreate` datetime DEFAULT NULL,
        `util_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `util_etat` int(11) NOT NULL DEFAULT '1',
        `tutil_id` int(11) NOT NULL,
        PRIMARY KEY (`util_id`),
        UNIQUE KEY `users_id_UNIQUE` (`util_id`),
        KEY `fk_c_utilisateur_c_typeutilisateur1_idx` (`tutil_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 11 DEFAULT CHARSET = utf8;

--

-- Déchargement des données de la table `c_utilisateur`

--

INSERT INTO
    `c_utilisateur` (
        `util_id`,
        `util_nom`,
        `util_prenom`,
        `util_datenaissance`,
        `util_phonemobile`,
        `util_phonefixe`,
        `util_mail`,
        `util_cp`,
        `util_adresse`,
        `util_ville`,
        `util_photo`,
        `util_login`,
        `util_password`,
        `util_status`,
        `util_datecreate`,
        `util_stamp`,
        `util_etat`,
        `tutil_id`
    )
VALUES (
        9,
        'Tset',
        'Loroque Brillant',
        '2022-05-04',
        NULL,
        ' 33346863299',
        'mirana.manao@gmail.com',
        '301',
        'Imandry Fianarantsoa / Madagascar',
        'Fianarantsoa',
        NULL,
        'tset',
        'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e',
        0,
        '2022-05-04 07:11:36',
        '2022-05-09 05:32:10',
        1,
        2
    ), (
        10,
        'ANDRIAMAMPIADANA',
        'Loroque Brillant',
        '2022-05-11',
        '',
        '+33346863299',
        'brillantloroque.manao@gmail.com',
        '301',
        'Imandry Fianarantsoa / Madagascar',
        'Fianarantsoa',
        NULL,
        'brillantloroque.manao@gmail.com',
        '2a7251e27b97b73e1f12ef956b2bccf498cde373c0a05af295063c5aec4608ed7a755d78ae1de5f0c06a2a9b88449a4f3b1d4192d8944558012804adacb0220c',
        1,
        '2022-05-11 07:07:42',
        '2022-05-11 07:07:42',
        1,
        1
    );

--

-- Contraintes pour les tables déchargées

--

--

-- Contraintes pour la table `c_communication_prospect`

--

ALTER TABLE
    `c_communication_prospect`
ADD
    CONSTRAINT `fk_c_communication_prospect_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_communication_prospect_c_type_communication_prospect1` FOREIGN KEY (`type_comprospect_id`) REFERENCES `c_type_communication_prospect` (`type_comprospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--

-- Contraintes pour la table `c_historiqueacces`

--

ALTER TABLE
    `c_historiqueacces`
ADD
    CONSTRAINT `fk_c_historiqueacces_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--

-- Contraintes pour la table `c_lot_prospect`

--

ALTER TABLE `c_lot_prospect`
ADD
    CONSTRAINT `fk_c_lot_prospect_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_lot_prospect_c_programme1` FOREIGN KEY (`progm_id`) REFERENCES `c_programme` (`progm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_lot_prospect_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_lot_prospect_c_typelot1` FOREIGN KEY (`typelot_id`) REFERENCES `c_typelot` (`typelot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_lot_prospect_c_typologieLot1` FOREIGN KEY (`typologielot_id`) REFERENCES `c_typologielot` (`typologielot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--

-- Contraintes pour la table `c_permissiontypeutilisateur`

--

ALTER TABLE
    `c_permissiontypeutilisateur`
ADD
    CONSTRAINT `fk_c_permissionuser_c_permission1` FOREIGN KEY (`permission_id`) REFERENCES `c_permission` (`permission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_permissionuser_c_typeaccount1` FOREIGN KEY (`tutil_id`) REFERENCES `c_typeutilisateur` (`tutil_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--

-- Contraintes pour la table `c_prospect`

--

ALTER TABLE `c_prospect`
ADD
    CONSTRAINT `fk_c_prospect_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_prospect_etat_prospect1` FOREIGN KEY (`etat_prospect_id`) REFERENCES `etat_prospect` (`etat_prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_prospect_origine_client1` FOREIGN KEY (`origine_cli_id`) REFERENCES `origine_client` (`origine_cli_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_prospect_origine_demande1` FOREIGN KEY (`origine_dem_id`) REFERENCES `origine_demande` (`origine_dem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_prospect_parcours_vente1` FOREIGN KEY (`vente_id`) REFERENCES `parcours_vente` (`vente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD
    CONSTRAINT `fk_c_prospect_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`produit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--

-- Contraintes pour la table `c_utilisateur`

--

ALTER TABLE `c_utilisateur`
ADD
    CONSTRAINT `fk_c_utilisateur_c_typeutilisateur1` FOREIGN KEY (`tutil_id`) REFERENCES `c_typeutilisateur` (`tutil_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */

;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */

;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */

;

ALTER TABLE `c_programme`
ADD
    `progm_num` VARCHAR(50) NOT NULL AFTER `progm_id`;

ALTER TABLE `c_programme`
ADD
    `progm_date_creation` DATE NOT NULL AFTER `progm_pays`,
ADD
    `progm_date_modification` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `progm_date_creation`;

ALTER TABLE `c_programme`
ADD
    `progm_etat` INT NOT NULL DEFAULT '1' AFTER `progm_pays`;

ALTER TABLE `c_gestionnaire`
ADD
    `gestionnaire_date_creation` DATE NOT NULL AFTER `gestionnaire_pays`,
ADD
    `gestionnaire_date_modification` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `gestionnaire_date_creation`;

ALTER TABLE `c_gestionnaire`
ADD
    `gestionnaire_etat` INT NOT NULL DEFAULT '1' AFTER `gestionnaire_pays`;

ALTER TABLE `c_origine_client`
ADD
    `origine_cli_etat` INT(1) NOT NULL DEFAULT '1' AFTER `origine_cli_libelle`;

ALTER TABLE
    `c_origine_demande`
ADD
    `origine_dem_etat` INT(1) NOT NULL DEFAULT '1' AFTER `origine_dem_libelle`;

ALTER TABLE `c_produit`
ADD
    `produit_etat` INT(1) NOT NULL DEFAULT '1' AFTER `produit_libelle`;

ALTER TABLE `c_prospect`
ADD
    `prospect_email1` VARCHAR(50) NOT NULL AFTER `prospect_email`;

ALTER TABLE `c_prospect`
ADD
    `etat_prospect` INT(1) NOT NULL DEFAULT '1' AFTER `prospect_commentaire`;

ALTER TABLE `c_prospect`
ADD
    `type_prospect_id` INT(4) NOT NULL AFTER `etat_prospect`;

ALTER TABLE `c_prospect`
ADD
    `prospect_lieuNais` VARCHAR(50) NOT NULL AFTER `prospect_prenom`;

ALTER TABLE `c_prospect`
ADD
    `prospect_siutation` VARCHAR(50) NOT NULL AFTER `prospect_lieuNais`;

ALTER TABLE `c_lot_prospect`
ADD
    `lot_num` VARCHAR(10) NOT NULL AFTER `lot_nom`;

ALTER TABLE `c_lot_prospect`
ADD
    `etat_lot` INT(1) NOT NULL DEFAULT '1' AFTER `lot_pays`;

ALTER TABLE
    `c_lot_prospect` CHANGE `prospect_id` `prospect_id` INT(11) NULL DEFAULT NULL,
    CHANGE `gestionnaire_id` `gestionnaire_id` INT(11) NULL DEFAULT NULL,
    CHANGE `typelot_id` `typelot_id` INT(11) NULL DEFAULT NULL,
    CHANGE `progm_id` `progm_id` INT(11) NULL DEFAULT NULL,
    CHANGE `typologielot_id` `typologielot_id` INT(11) NULL DEFAULT NULL;

--

-- Structure de la table `c_typeprospect`

--

DROP TABLE IF EXISTS `c_typeprospect`;

CREATE TABLE
    IF NOT EXISTS `c_typeprospect` (
        `type_prospect_id` int(11) NOT NULL AUTO_INCREMENT,
        `type_prospect_libelle` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`type_prospect_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 5 DEFAULT CHARSET = latin1;

--

-- Déchargement des données de la table `c_typeprospect`

--

INSERT INTO
    `c_typeprospect` (
        `type_prospect_id`,
        `type_prospect_libelle`
    )
VALUES (1, 'Propriétaires indivis'), (
        2,
        ' Propriétaire/Usufruitier'
    ), (3, 'SCI'), (4, 'Sarl/SNC/EI/EURL');

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */

;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */

;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */

;

-- -----------------------------------------------------

-- Table `c_contactGestionanire`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_contactGestionanire` (
        `cnctGestionnaire_id` INT NOT NULL AUTO_INCREMENT,
        `cnctGestionanire_nom` VARCHAR(120) NULL,
        `cnctGestionnaire_prenom` VARCHAR(120) NULL,
        `cnctGestionnaire_fonction` VARCHAR(120) NULL,
        `cnctGestionnaire_service` VARCHAR(120) NULL,
        `cnctGestionnaire_tel1` VARCHAR(45) NULL,
        `cnctGestionnaire_tel2` VARCHAR(45) NULL,
        `cnctGestionnaire_email1` VARCHAR(120) NULL,
        `cnctGestionnaire_email2` VARCHAR(120) NULL,
        `gestionnaire_id` INT NOT NULL,
        PRIMARY KEY (`cnctGestionnaire_id`),
        CONSTRAINT `fk_c_contactGestionanire_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

ALTER TABLE
    `c_contactGestionanire`
ADD
    `etat_contact` INT(11) NOT NULL AFTER `cnctGestionnaire_email2`;

-- -----------------------------------------------------

-- Table `c_lot_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_lot_client` (
        `cliLot_id` INT NOT NULL AUTO_INCREMENT,
        `cliLot_num` VARCHAR(120) NULL,
        `cliLot_nom` VARCHAR(120) NULL,
        `cliLot_adresse1` VARCHAR(120) NULL,
        `cliLot_adresse2` VARCHAR(120) NULL,
        `cliLot_adresse3` VARCHAR(120) NULL,
        `cliLot_cp` VARCHAR(120) NULL,
        `cliLot_ville` VARCHAR(120) NULL,
        `cliLot_pays` VARCHAR(120) NULL,
        `cliLot_principale` VARCHAR(45) NULL,
        `cliLot_etat` INT(11) NOT NULL,
        `dossier_id` INT(11) NOT NULL,
        `typologielot_id` INT NOT NULL,
        `typelot_id` INT NOT NULL,
        `gestionnaire_id` INT NOT NULL,
        `progm_id` INT NOT NULL,
        PRIMARY KEY (`cliLot_id`),
        CONSTRAINT `fk_c_lot_client_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typologieLot1` FOREIGN KEY (`typologielot_id`) REFERENCES `c_typologieLot` (`typologielot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typelot1` FOREIGN KEY (`typelot_id`) REFERENCES `c_typelot` (`typelot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_programme1` FOREIGN KEY (`progm_id`) REFERENCES `c_programme` (`progm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_theme`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_theme` (
        `theme_id` INT NOT NULL,
        `theme_libelle` VARCHAR(120) NULL,
        PRIMARY KEY (`theme_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_tache`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_tache` (
        `tache_id` INT NOT NULL,
        `tache_nom` VARCHAR(120) NULL,
        `tache_date_creation` DATETIME NULL,
        `tache_date_echeance` DATETIME NULL,
        `tache_prioritaire` VARCHAR(45) NULL,
        `tache_texte` TEXT NULL,
        `prospect_id` INT(11) NOT NULL,
        `theme_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`tache_id`),
        CONSTRAINT `fk_c_tache_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_tache_c_theme1` FOREIGN KEY (`theme_id`) REFERENCES `c_theme` (`theme_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_tache_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

INSERT INTO
    `c_theme` (`theme_id`, `theme_libelle`)
VALUES (1, 'Administratif'), (2, 'Commercial'), (3, 'RH');

-- -----------------------------------------------------

-- Table `document_prospect`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_prospect` (
        `docp_id` INT UNSIGNED NOT NULL,
        `docp_nom` VARCHAR(120) NULL,
        `docp_path` VARCHAR(120) NULL,
        `docp_creation` DATETIME NULL,
        `prospect_id` INT(11) NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`docp_id`),
        CONSTRAINT `fk_document_prospect_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_document_prospect_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

CREATE TABLE
    IF NOT EXISTS `c_document_client` (
        `doc_id` INT NOT NULL AUTO_INCREMENT,
        `doc_nom` VARCHAR(120) NULL,
        `doc_path` VARCHAR(120) NULL,
        `doc_date_creation` DATETIME NULL,
        `doc_etat` VARCHAR(11) NULL,
        `util_id` INT(11) NOT NULL,
        `client_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_id`),
        CONSTRAINT `fk_c_document_client_c_client1` FOREIGN KEY (`client_id`) REFERENCES `c_client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `produit_clients`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_produit_clients` (
        `produitcli_id` INT NOT NULL AUTO_INCREMENT,
        `produitcli_libelle` VARCHAR(100) NULL,
        PRIMARY KEY (`produitcli_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `document_lot_taxe`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_lot_taxe` (
        `doc_taxe_id` INT NOT NULL AUTO_INCREMENT,
        `doc_taxe_nom` VARCHAR(120) NULL,
        `doc_taxe_path` VARCHAR(120) NULL,
        `doc_taxe_creation` DATETIME NULL,
        `doc_taxe_etat` INT(11) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_taxe_id`),
        CONSTRAINT `fk_document_lot_taxe_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_document_lot_taxe_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_taxe_fonciere_lot`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_taxe_fonciere_lot` (
        `taxe_id` INT NOT NULL AUTO_INCREMENT,
        `taxe_montant` DECIMAL(15, 2) NULL,
        `taxe_tom` DECIMAL(15, 2) NULL,
        `taxe_date_modification` TIMESTAMP NOT NULL,
        `taxe_valide` INT(11) NULL,
        `taxe_date_validation` DATETIME NULL,
        `taxe_envoi_mail` INT(11) NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`taxe_id`),
        CONSTRAINT `fk_c_taxe_fonciere_lot_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_taxe_fonciere_lot_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_historiquetaxe`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_historiquetaxe` (
        `htaxe_id` INT NOT NULL AUTO_INCREMENT,
        `htaxe_date_validation` DATETIME NULL,
        `htaxe_valide` INT(11) NULL,
        `util_prenom` VARCHAR(120) NULL,
        `taxe_id` INT NOT NULL,
        PRIMARY KEY (`htaxe_id`),
        CONSTRAINT `fk_c_historiquetaxe_c_taxe_fonciere_lot1` FOREIGN KEY (`taxe_id`) REFERENCES `c_taxe_fonciere_lot` (`taxe_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_param_modelemail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_param_modelemail` (
        `pmail_id` INT(11) NOT NULL AUTO_INCREMENT,
        `pmail_libelle` VARCHAR(100) NULL DEFAULT NULL,
        `pmail_description` TEXT NULL DEFAULT NULL,
        `pmail_objet` TEXT NULL DEFAULT NULL,
        `pmail_contenu` LONGTEXT NULL DEFAULT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`pmail_id`),
        CONSTRAINT `fk_c_param_modelemail_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------

-- Table `c_messagerie_paramserveur`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_messagerie_paramserveur` (
        `mess_id` INT(11) NOT NULL AUTO_INCREMENT,
        `mess_libelle` VARCHAR(255) NOT NULL,
        `mess_description` LONGTEXT NOT NULL,
        `mess_protocole` VARCHAR(50) NOT NULL,
        `mess_type` VARCHAR(50) NOT NULL,
        `mess_adressemailserveur` VARCHAR(255) NOT NULL,
        `mess_typeauthentification` VARCHAR(25) NOT NULL,
        `mess_port` INT(11) NOT NULL,
        `mess_nomsource` VARCHAR(255) NOT NULL,
        `mess_email` VARCHAR(255) NOT NULL,
        `mess_motdepasse` VARCHAR(255) NOT NULL,
        `mess_compteretourmail` VARCHAR(255) NOT NULL,
        `mess_priorite` VARCHAR(100) NOT NULL,
        `mess_charset` VARCHAR(50) NOT NULL,
        `mess_pardefaut` INT(1) NOT NULL DEFAULT '1' COMMENT '0: par défaut; 1: le contraire',
        `mess_etat` INT(1) NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`mess_id`),
        CONSTRAINT `fk_c_messagerie_paramserveur_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------

-- Table `c_type_mandat`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_mandat` (
        `type_mandat_id` INT NOT NULL AUTO_INCREMENT,
        `type_mandat_libelle` VARCHAR(120) NULL,
        PRIMARY KEY (`type_mandat_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_mandat`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_mandat` (
        `mandat_id` INT(11) NOT NULL,
        `mandat_date_creation` DATETIME NULL,
        `mandat_lieu_creation` VARCHAR(120) NULL,
        `lot_id` INT NOT NULL,
        `type_mandat_id` INT NOT NULL,
        PRIMARY KEY (`mandat_id`),
        CONSTRAINT `fk_c_mandat_c_lot_prospect1` FOREIGN KEY (`lot_id`) REFERENCES `c_lot_prospect` (`lot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_mandat_c_type_mandat1` FOREIGN KEY (`type_mandat_id`) REFERENCES `c_type_mandat` (`type_mandat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `type_tache`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_tache` (
        `type_tache_id` INT NOT NULL AUTO_INCREMENT,
        `type_tache_libelle` VARCHAR(120) NULL,
        PRIMARY KEY (`type_tache_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_mandat`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_mandat` (
        `docmandat_id` INT NOT NULL AUTO_INCREMENT,
        `docmandat_nom` VARCHAR(120) NULL,
        `docmandat_path` VARCHAR(120) NULL,
        `docmandat_creation` DATETIME NULL,
        `docmandat_etat` INT(11) NOT NULL,
        `lot_id` INT NOT NULL,
        PRIMARY KEY (`docmandat_id`),
        CONSTRAINT `fk_c_document_mandat_c_lot_prospect1` FOREIGN KEY (`lot_id`) REFERENCES `c_lot_prospect` (`lot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_pret`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_pret` (
        `doc_pret_id` INT NOT NULL AUTO_INCREMENT,
        `doc_pret_nom` VARCHAR(250) NULL,
        `doc_pret_path` VARCHAR(250) NULL,
        `doc_pret_creation` DATETIME NULL,
        `doc_pret_traitement` TIMESTAMP NULL,
        `doc_pret_etat` INT(1) NULL,
        `doc_pret_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_pret_id`),
        CONSTRAINT `fk_c_document_pret_c_lot_client2` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_pret_c_utilisateur2` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_pret_lot`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_pret_lot` (
        `pret_id` INT NOT NULL AUTO_INCREMENT,
        `pret_annee` VARCHAR(45) NULL,
        `pret_capital_restant_prec` DECIMAL(15, 2) NULL,
        `pret_capital_deblocage` DECIMAL(15, 2) NULL,
        `pret_ecart_crd` DECIMAL(15, 2) NULL,
        `pret_remboursement` DECIMAL(15, 2) NULL,
        `pret_capital_amorti` DECIMAL(15, 2) NULL,
        `pret_capital_restant` DECIMAL(15, 2) NULL,
        `pret_interet` DECIMAL(15, 2) NULL,
        `pret_assurance` DECIMAL(15, 2) NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`pret_id`),
        CONSTRAINT `fk_c_pret_lot_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 20/10/2022

-- -----------------------------------------------------

-- Table `c_document_client_mandat`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_client_mandat` (
        `doc_mandat_cli_id` INT NOT NULL AUTO_INCREMENT,
        `doc_mandat_cli_nom` VARCHAR(120) NULL,
        `doc_mandat_cli_path` VARCHAR(120) NULL,
        `doc_mandat_cli_creation` DATETIME NULL,
        `doc_mandat_cli_etat` INT(1) NULL,
        `doc_mandat_cli_type` INT(1) NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_mandat_cli_id`),
        CONSTRAINT `fk_c_document_client_mandat_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_client_mandat_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_mandat_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_mandat_client` (
        `mandat_cli_id` INT NOT NULL AUTO_INCREMENT,
        `mandat_cli_date_creation` DATETIME NULL,
        `mandat_cli_lieu_creation` VARCHAR(120) NULL,
        `mandat_cli_etat` INT(1) NULL,
        `mandat_cli_path` VARCHAR(250) NULL,
        `client_id` INT(11) NOT NULL,
        `type_mandat_id` INT NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`mandat_cli_id`),
        CONSTRAINT `fk_c_mandat_client_c_client1` FOREIGN KEY (`client_id`) REFERENCES `c_client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_mandat_client_c_type_mandat1` FOREIGN KEY (`type_mandat_id`) REFERENCES `c_type_mandat` (`type_mandat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_mandat_client_c_lot_client2` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Stenny le 28-10-2022

-- -----------------------------------------------------

-- Table `c_typeaction`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_typeaction` (
        `tpact_id` INT NOT NULL AUTO_INCREMENT,
        `tpact_description` VARCHAR(255) NOT NULL,
        PRIMARY KEY (`tpact_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_typeaction` (
        `tpact_id`,
        `tpact_description`
    )
VALUES (1, 'ajout'), (2, 'suppression'), (3, 'modification'), (4, 'validation'), (5, 'refus'), (6, 'envoi mail');

-- Mis à jour par Stenny le 28-10-2022

-- -----------------------------------------------------

-- Table `c_page`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_page` (
        `page_id` INT NOT NULL AUTO_INCREMENT,
        `page_nom` VARCHAR(45) NOT NULL,
        PRIMARY KEY (`page_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_page` (`page_id`, `page_nom`)
VALUES (1, 'bail_lot'), (2, 'tom_lot'), (3, 'loyer_lot'), (4, 'pret_lot'), (5, 'identification_lot');

-- Mis à jour par Stenny le 28-10-2022

-- -----------------------------------------------------

-- Table `c_historique`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_historique` (
        `histo_id` INT NOT NULL AUTO_INCREMENT,
        `histo_date` DATETIME NULL,
        `histo_action` VARCHAR(255) NULL,
        `histo_reference_id` INT NULL,
        `util_id` INT(11) NOT NULL,
        `tpact_id` INT NOT NULL,
        `page_id` INT NOT NULL,
        PRIMARY KEY (`histo_id`),
        INDEX `fk_c_historique_c_utilisateur1_idx` (`util_id` ASC),
        INDEX `fk_c_historique_c_typeaction1_idx` (`tpact_id` ASC),
        INDEX `fk_c_historique_c_page1_idx` (`page_id` ASC),
        CONSTRAINT `fk_c_historique_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_historique_c_typeaction1` FOREIGN KEY (`tpact_id`) REFERENCES `c_typeaction` (`tpact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_historique_c_page1` FOREIGN KEY (`page_id`) REFERENCES `c_page` (`page_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_type_bail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_bail` (
        `tpba_id` INT NOT NULL AUTO_INCREMENT,
        `tpba_description` VARCHAR(255) NULL,
        PRIMARY KEY (`tpba_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_bail` (`tpba_description`)
VALUES ('commercial meublé'), ('commercial non meublé'), ('bail dérogatoire'), ('mandat de gestion'), ('saisonnier'), ('autre');

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_nature_prise_effet`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_nature_prise_effet` (
        `natef_id` INT NOT NULL AUTO_INCREMENT,
        `natef_description` VARCHAR(255) NOT NULL,
        PRIMARY KEY (`natef_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_nature_prise_effet` (`natef_description`)
VALUES ('à la livraison'), ('date signature bail'), ('date signature acte');

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_moment_facturation_bail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_moment_facturation_bail` (
        `momfac_id` INT NOT NULL AUTO_INCREMENT,
        `momfact_nom` VARCHAR(45) NULL,
        PRIMARY KEY (`momfac_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_moment_facturation_bail` (`momfact_nom`)
VALUES ('échu'), ('échoir');

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_periodicite_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periodicite_loyer` (
        `pdl_id` INT NOT NULL AUTO_INCREMENT,
        `pdl_description` VARCHAR(255) NOT NULL,
        `pdl_valeur` INT NOT NULL,
        PRIMARY KEY (`pdl_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_periodicite_loyer` (
        `pdl_description`,
        `pdl_valeur`
    )
VALUES ('mensuelle', 12), ('trimestrielle', 4), ('semestrielle', 2), ('annuelle', 1);

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_periode_indexation`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periode_indexation` (
        `prdind_id` INT NOT NULL AUTO_INCREMENT,
        `prdind_valeur` INT NOT NULL,
        PRIMARY KEY (`prdind_id`)
    ) ENGINE = InnoDB;

INSERT INTO `c_periode_indexation` (`prdind_valeur`) VALUES (1), (3);

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_bail_art`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_bail_art` (
        `bart_id` INT NOT NULL AUTO_INCREMENT,
        `bart_nom` VARCHAR(45) NULL,
        PRIMARY KEY (`bart_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_bail_art` (`bart_nom`)
VALUES ('bailleur'), ('preneur');

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_indice_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_indice_loyer` (
        `indloyer_id` INT NOT NULL AUTO_INCREMENT,
        `indloyer_description` VARCHAR(45) NULL,
        PRIMARY KEY (`indloyer_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_indice_loyer` (`indloyer_description`)
VALUES ('ICC'), ('ICL');

-- Mis à jour par Stenny le 31-10-2022

-- -----------------------------------------------------

-- Table `c_type_echeance`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_echeance` (
        `tpech_id` INT NOT NULL AUTO_INCREMENT,
        `tpech_valeur` INT NULL,
        PRIMARY KEY (`tpech_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_echeance` (`tpech_valeur`)
VALUES (5), (10), (15), (20);

-- Mis à jour par Tsilavina le 07-11-2022

-- -----------------------------------------------------

-- Table `c_franchise_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_franchise_loyer` (
        `franc_id` INT NOT NULL AUTO_INCREMENT,
        `franc_debut` DATE NOT NULL,
        `franc_fin` DATE NOT NULL,
        `franc_explication` LONGTEXT NOT NULL,
        `bail_id` INT NOT NULL,
        `etat_franchise` INT NOT NULL DEFAULT '1',
        PRIMARY KEY (`franc_id`),
        INDEX `fk_c_franchise_bail_c_bail1_idx` (`bail_id` ASC),
        CONSTRAINT `fk_c_franchise_bail_c_bail1` FOREIGN KEY (`bail_id`) REFERENCES `c_bail` (`bail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Tsilavina le 11-11-2022

-- -----------------------------------------------------

-- Table `c_periode_semestre`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periode_semestre` (
        `periode_semestre_id` INT NOT NULL AUTO_INCREMENT,
        `periode_semestre_libelle` VARCHAR(45) NULL,
        PRIMARY KEY (`periode_semestre_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_periode_trimestre`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periode_trimestre` (
        `periode_timestre_id` INT NOT NULL AUTO_INCREMENT,
        `periode_trimesrte_libelle` VARCHAR(45) NULL,
        PRIMARY KEY (`periode_timestre_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_periode_annee`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periode_annee` (
        `periode_annee_id` INT NOT NULL,
        `periode_annee_libelle` VARCHAR(45) NULL,
        PRIMARY KEY (`periode_annee_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_periode_mensuel`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_periode_mensuel` (
        `periode_mens_id` INT NOT NULL AUTO_INCREMENT,
        `periode_mens_libelle` VARCHAR(45) NULL,
        PRIMARY KEY (`periode_mens_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_facture_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_facture_loyer` (
        `fact_loyer_id` INT NOT NULL AUTO_INCREMENT,
        `fact_loyer_num` VARCHAR(45) NULL,
        `fact_loyer_date` DATE NULL,
        `fact_loyer_app_ht` FLOAT NULL,
        `fact_loyer_app_tva` FLOAT NULL,
        `fact_loyer_app_ttc` FLOAT NULL,
        `fact_loyer_park_ht` FLOAT NULL,
        `fact_loyer_park_tva` FLOAT NULL,
        `fact_loyer_park_ttc` FLOAT NULL,
        `fact_loyer_charge_ht` FLOAT NULL,
        `fact_loyer_charge_tva` FLOAT NULL,
        `fact_loyer_charge_ttc` FLOAT NULL,
        `fact_loyer_produit_ht` FLOAT NULL,
        `fact_loyer_produit_tva` FLOAT NULL,
        `fact_loyer_produit_ttc` FLOAT NULL,
        `bail_id` INT NOT NULL,
        `periode_semestre_id` INT NULL,
        `periode_timestre_id` INT NULL,
        `periode_annee_id` INT NULL,
        `periode_mens_id` INT NULL,
        `facture_nombre` INT NOT NULL,
        PRIMARY KEY (`fact_loyer_id`),
        FOREIGN KEY (`bail_id`) REFERENCES `c_bail` (`bail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_semestre1` FOREIGN KEY (`periode_semestre_id`) REFERENCES `c_periode_semestre` (`periode_semestre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_trimestre1` FOREIGN KEY (`periode_timestre_id`) REFERENCES `c_periode_trimestre` (`periode_timestre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_annee1` FOREIGN KEY (`periode_annee_id`) REFERENCES `c_periode_annee` (`periode_annee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_mensuel1` FOREIGN KEY (`periode_mens_id`) REFERENCES `c_periode_mensuel` (`periode_mens_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

INSERT INTO
    `c_periode_mensuel` (
        `periode_mens_id`,
        `periode_mens_libelle`
    )
VALUES (1, 'Janvier'), (2, 'Février'), (3, 'Mars'), (4, 'Avril'), (5, 'Mai'), (6, 'Juin'), (7, 'Juillet'), (8, 'Août'), (9, 'Septembre'), (10, 'Octobre'), (11, 'Novembre'), (12, 'Décembre'), (13, 'Variable');

INSERT INTO
    `c_periode_trimestre` (
        `periode_timestre_id`,
        `periode_trimesrte_libelle`
    )
VALUES (1, 'T1'), (2, 'T2'), (3, 'T3'), (4, 'T4'), (5, 'Variable');

INSERT INTO
    `c_periode_semestre` (
        `periode_semestre_id`,
        `periode_semestre_libelle`
    )
VALUES (1, 'S1'), (2, 'S2'), (3, 'Variable');

INSERT INTO
    `c_periode_annee` (
        `periode_annee_id`,
        `periode_annee_libelle`
    )
VALUES ('0', 'Variable'), ('1', '2021'), ('2', '2022'), ('3', '2023');

ENGINE = InnoDB;

-- Mise à jour par Stenny le 14-11-2022

-- -----------------------------------------------------

-- Table `c_dossier`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_dossier` (
        `dossier_id` INT(11) NOT NULL AUTO_INCREMENT,
        `dossier_num` VARCHAR(10) NULL DEFAULT NULL,
        `dossier_libelle` VARCHAR(120) NULL DEFAULT NULL,
        `dossier_d_creation` DATETIME NOT NULL,
        `dossier_d_modification` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `dossier_etat` INT(1) NOT NULL,
        `dossier_rcs` VARCHAR(9) NULL,
        `dossier_siret` VARCHAR(14) NULL,
        `dossier_ape` VARCHAR(5) NULL,
        `dossier_tva_intracommunautaire` VARCHAR(20) NULL,
        `dossier_adresse1` VARCHAR(120) NULL,
        `dossier_adresse2` VARCHAR(120) NULL,
        `dossier_adresse3` VARCHAR(120) NULL,
        `dossier_cp` VARCHAR(120) NULL,
        `dossier_ville` VARCHAR(120) NULL,
        `dossier_pays` VARCHAR(120) NULL,
        `dossier_type_adresse` VARCHAR(50) NULL,
        `client_id` VARCHAR(120) NOT NULL,
        `sie_id` INT NOT NULL,
        PRIMARY KEY (`dossier_id`)
    ) ENGINE = InnoDB;

-- Mis à jour par Stenny le 13-11-2022

-- -----------------------------------------------------

-- Table `c_indexation_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_indexation_loyer` (
        `indlo_id` INT NOT NULL AUTO_INCREMENT,
        `indlo_debut` DATE NULL,
        `indlo_fin` DATE NULL,
        `indlo_appartement_ht` FLOAT NULL,
        `indlo_appartement_tva` FLOAT NULL,
        `indlo_appartement_ttc` FLOAT NULL,
        `indlo_parking_ht` FLOAT NULL,
        `indlo_parking_tva` FLOAT NULL,
        `indlo_parking_ttc` FLOAT NULL,
        `bail_id` INT NOT NULL,
        `indlo_charge_ht` FLOAT NULL DEFAULT 0,
        `indlo_charge_tva` FLOAT NULL DEFAULT 0,
        `indlo_charge_ttc` FLOAT NULL DEFAULT 0,
        `indlo_autre_prod_ht` FLOAT NULL DEFAULT 0,
        `indlo_autre_prod_tva` FLOAT NULL DEFAULT 0,
        `indlo_autre_prod_ttc` FLOAT NULL DEFAULT 0,
        `indlo_variation_plafonnee` FLOAT NULL,
        `indlo_variation_capee` FLOAT NULL,
        `indlo_indice_base` INT NOT NULL,
        `indlo_indice_reference` INT NULL,
        `indlo_variation_loyer` FLOAT NULL,
        `indlo_variation_appliquee` FLOAT NULL,
        `etat_indexation` INT NOT NULL DEFAULT 1,
        PRIMARY KEY (`indlo_id`),
        INDEX `fk_c_periode_indexation_loyer_c_bail1_idx` (`bail_id` ASC),
        INDEX `fk_c_indexation_loyer_c_indice_valeur_loyer1_idx` (`indlo_indice_base` ASC),
        INDEX `fk_c_indexation_loyer_c_indice_valeur_loyer2_idx` (`indlo_indice_reference` ASC),
        CONSTRAINT `fk_c_periode_indexation_loyer_c_bail1` FOREIGN KEY (`bail_id`) REFERENCES `c_bail` (`bail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_indexation_loyer_c_indice_valeur_loyer1` FOREIGN KEY (`indlo_indice_base`) REFERENCES `c_indice_valeur_loyer` (`indval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_indexation_loyer_c_indice_valeur_loyer2` FOREIGN KEY (`indlo_indice_reference`) REFERENCES `c_indice_valeur_loyer` (`indval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Stenny le 14-11-2022

-- -----------------------------------------------------

-- Table `c_taux_tva`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_taux_tva` (
        `tva_id` INT NOT NULL AUTO_INCREMENT,
        `tva_libelle` VARCHAR(120) NULL,
        `tva_valeur` INT,
        `tva_compte_comptable` LONGTEXT NULL DEFAULT NULL,
        PRIMARY KEY (`tva_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_taux_tva` (`tva_libelle`, `tva_valeur`)
VALUES ('0%', 0), ('5%', 5), ('10%', 10), ('20%', 20);

-- Mis à jour par Stenny le 15-11-2022

-- -----------------------------------------------------

-- Table `c_indice_valeur_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_indice_valeur_loyer` (
        `indval_id` INT NOT NULL AUTO_INCREMENT,
        `indval_libelle` VARCHAR(255) NOT NULL,
        `indval_valeur` FLOAT NOT NULL,
        `indval_annee` YEAR NULL,
        `indval_trimestre` VARCHAR(2) NULL,
        `indval_date_parution` VARCHAR(10) NULL,
        `indloyer_id` INT NULL,
        PRIMARY KEY (`indval_id`),
        INDEX `fk_c_indice_valeur_loyer_c_indice_loyer1_idx` (`indloyer_id` ASC),
        CONSTRAINT `fk_c_indice_valeur_loyer_c_indice_loyer1` FOREIGN KEY (`indloyer_id`) REFERENCES `c_indice_loyer` (`indloyer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Philipine le 15-11-2022

-- -----------------------------------------------------

-- Table `c_document_bail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_bail` (
        `doc_bail_id` INT NOT NULL AUTO_INCREMENT,
        `doc_bail_nom` VARCHAR(120) NULL,
        `doc_bail_path` VARCHAR(120) NULL,
        `doc_bail_creation` DATETIME NULL,
        `doc_bail_traitement` TIMESTAMP NULL,
        `doc_bail_etat` TINYINT(1) NULL,
        `doc_bail_traite` TINYINT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_bail_id`),
        CONSTRAINT `fk_c_document_bail_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_bail_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Stenny le 17-11-2022

-- -----------------------------------------------------

-- Table `c_bail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_bail` (
        `bail_id` INT NOT NULL AUTO_INCREMENT,
        `bail_debut` DATE NULL,
        `bail_fin` DATE NULL,
        `bail_resiliation_triennale` TINYINT(1) NULL,
        `bail_cloture` TINYINT(1) NULL DEFAULT 0,
        `bail_date_cloture` DATE NULL,
        `bail_loyer_variable` TINYINT(1) NULL,
        `bail_remboursement_tom` TINYINT(1) NULL,
        `bail_facturation_loyer_celaviegestion` TINYINT(1) NULL,
        `bail_charge_recuperable` TINYINT(1) NULL,
        `bail_forfait_charge_indexer` TINYINT(1) NULL DEFAULT 1,
        `bail_date_premiere_indexation` DATE NULL,
        `bail_date_prochaine_indexation` DATE NULL,
        `bail_date_echeance` DATE NULL,
        `bail_indexation` TINYINT(1) NULL,
        `bail_commentaire` LONGTEXT NULL,
        `pdl_id` INT NOT NULL,
        `natef_id` INT NOT NULL,
        `tpba_id` INT NOT NULL,
        `cliLot_id` INT NOT NULL,
        `gestionnaire_id` INT NOT NULL,
        `tpech_id` INT NOT NULL,
        `bail_art_605` INT NOT NULL,
        `bail_art_606` INT NOT NULL,
        `indloyer_id` INT NOT NULL,
        `momfac_id` INT NOT NULL,
        `prdind_id` INT NOT NULL,
        `bail_annee` INT NOT NULL,
        `bail_mois` INT NOT NULL,
        PRIMARY KEY (`bail_id`),
        INDEX `fk_c_bail_c_periodicite_loyer1_idx` (`pdl_id` ASC),
        INDEX `fk_c_bail_c_type_date_bail1_idx` (`natef_id` ASC),
        INDEX `fk_c_bail_c_type_bail1_idx` (`tpba_id` ASC),
        INDEX `fk_c_bail_c_lot_client1_idx` (`cliLot_id` ASC),
        INDEX `fk_c_bail_c_gestionnaire1_idx` (`gestionnaire_id` ASC),
        INDEX `fk_c_bail_c_echeance_loyer1_idx` (`tpech_id` ASC),
        INDEX `fk_c_bail_c_bail_art1_idx` (`bail_art_605` ASC),
        INDEX `fk_c_bail_c_bail_art2_idx` (`bail_art_606` ASC),
        INDEX `fk_c_bail_c_indice_loyer1_idx` (`indloyer_id` ASC),
        INDEX `fk_c_bail_c_moment_facture_bail1_idx` (`momfac_id` ASC),
        INDEX `fk_c_bail_c_periode_indexation1_idx` (`prdind_id` ASC),
        CONSTRAINT `fk_c_bail_c_periodicite_loyer1` FOREIGN KEY (`pdl_id`) REFERENCES `c_periodicite_loyer` (`pdl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_type_date_bail1` FOREIGN KEY (`natef_id`) REFERENCES `c_nature_prise_effet` (`natef_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_type_bail1` FOREIGN KEY (`tpba_id`) REFERENCES `c_type_bail` (`tpba_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_echeance_loyer1` FOREIGN KEY (`tpech_id`) REFERENCES `c_type_echeance` (`tpech_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_bail_art1` FOREIGN KEY (`bail_art_605`) REFERENCES `c_bail_art` (`bart_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_bail_art2` FOREIGN KEY (`bail_art_606`) REFERENCES `c_bail_art` (`bart_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_indice_loyer1` FOREIGN KEY (`indloyer_id`) REFERENCES `c_indice_loyer` (`indloyer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_moment_facture_bail1` FOREIGN KEY (`momfac_id`) REFERENCES `c_moment_facturation_bail` (`momfac_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_bail_c_periode_indexation1` FOREIGN KEY (`prdind_id`) REFERENCES `c_periode_indexation` (`prdind_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 17-11-2022

CREATE TABLE
    IF NOT EXISTS `c_sie` (
        `sie_id` INT NOT NULL AUTO_INCREMENT,
        `sie_libelle` VARCHAR(120) NULL,
        `sie_email` VARCHAR(120) NULL,
        `sie_tel` VARCHAR(14) NULL,
        `sie_adresse1` VARCHAR(120) NULL,
        `sie_adresse2` VARCHAR(120) NULL,
        `sie_adresse3` VARCHAR(120) NULL,
        `sie_cp` VARCHAR(120) NULL,
        `sie_ville` VARCHAR(120) NULL,
        `sie_pays` VARCHAR(120) NULL,
        PRIMARY KEY (`sie_id`)
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 18-11-2022

-- -----------------------------------------------------

-- Table `c_acte`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_acte` (
        `acte_id` INT NOT NULL AUTO_INCREMENT,
        `acte_type_acquisition` VARCHAR(50) NULL,
        `acte_date_acquisition` DATETIME NULL,
        `acte_valeur_acquisition_ht` FLOAT NULL,
        `acte_valeur_terrain_ht` FLOAT NULL,
        `acte_valeur_construction_ht` FLOAT NULL,
        `acte_valeur_mobilier_ht` FLOAT NULL,
        `acte_montant_tva_immo` FLOAT NULL,
        `acte_montant_tva_frais_acquisition` FLOAT NULL,
        `acte_date_remboursement` DATETIME NULL,
        `acte_date_reg` DATETIME NULL,
        `acte_date_debut_amortissement` DATETIME NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`acte_id`),
        CONSTRAINT `fk_c_acte_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 18-11-2022

-- -----------------------------------------------------

-- Table `c_document_acte`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_acte` (
        `doc_acte_id` INT NOT NULL AUTO_INCREMENT,
        `doc_acte_nom` VARCHAR(120) NULL,
        `doc_acte_path` VARCHAR(120) NULL,
        `doc_acte_creation` DATETIME NULL,
        `doc_acte_traitement` TIMESTAMP NULL,
        `doc_acte_etat` TINYINT(1) NULL,
        `doc_acte_traite` TINYINT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_acte_id`),
        CONSTRAINT `fk_c_document_acte_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_acte_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Stenny 23/11/2022

-- -----------------------------------------------------

-- Table c_facture_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_facture_loyer` (
        `fact_loyer_id` INT NOT NULL AUTO_INCREMENT,
        `fact_loyer_num` VARCHAR(45) NULL,
        `fact_loyer_date` DATE NULL,
        `fact_loyer_app_ht` FLOAT NULL,
        `fact_loyer_app_tva` FLOAT NULL,
        `fact_loyer_app_ttc` FLOAT NULL,
        `fact_loyer_park_ht` FLOAT NULL,
        `fact_loyer_park_tva` FLOAT NULL,
        `fact_loyer_park_ttc` FLOAT NULL,
        `fact_loyer_charge_ht` FLOAT NULL,
        `fact_loyer_charge_tva` FLOAT NULL,
        `fact_loyer_charge_ttc` FLOAT NULL,
        `bail_id` INT NOT NULL,
        `periode_semestre_id` INT NULL,
        `periode_timestre_id` INT NULL,
        `periode_annee_id` INT NULL,
        `periode_mens_id` INT NULL,
        `facture_nombre` INT NOT NULL,
        `facture_annee` INT NOT NULL,
        `dossier_id` INT NOT NULL,
        `facture_commentaire` LONGTEXT NULL,
        `mode_paiement` VARCHAR(250) NOT NULL,
        `echeance_paiement` VARCHAR(250) NOT NULL,
        `emetteur_nom` VARCHAR(150) NOT NULL,
        `emet_adress1` VARCHAR(150) NULL,
        `emet_adress2` VARCHAR(150) NULL,
        `emet_adress3` VARCHAR(150) NULL,
        `emet_cp` VARCHAR(150) NULL,
        `emet_pays` VARCHAR(150) NULL,
        `emet_ville` VARCHAR(150) NULL,
        `dest_nom` VARCHAR(150) NOT NULL,
        `dest_adresse1` VARCHAR(150) NULL,
        `dest_adresse2` VARCHAR(150) NULL,
        `dest_adresse3` VARCHAR(150) NULL,
        `dest_cp` VARCHAR(150) NULL,
        `dest_pays` VARCHAR(150) NULL,
        `dest_ville` VARCHAR(150) NULL,
        PRIMARY KEY (`fact_loyer_id`),
        INDEX `fk_c_facture_loyer_c_bail1_idx` (`bail_id` ASC),
        INDEX `fk_c_facture_loyer_c_periode_semestre1_idx` (`periode_semestre_id` ASC),
        INDEX `fk_c_facture_loyer_c_periode_trimestre1_idx` (`periode_timestre_id` ASC),
        INDEX `fk_c_facture_loyer_c_periode_annee1_idx` (`periode_annee_id` ASC),
        INDEX `fk_c_facture_loyer_c_periode_mensuel1_idx` (`periode_mens_id` ASC),
        CONSTRAINT `fk_c_facture_loyer_c_bail1` FOREIGN KEY (`bail_id`) REFERENCES `c_bail` (`bail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_semestre1` FOREIGN KEY (`periode_semestre_id`) REFERENCES `c_periode_semestre` (`periode_semestre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_trimestre1` FOREIGN KEY (`periode_timestre_id`) REFERENCES `c_periode_trimestre` (`periode_timestre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_annee1` FOREIGN KEY (`periode_annee_id`) REFERENCES `c_periode_annee` (`periode_annee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_facture_loyer_c_periode_mensuel1` FOREIGN KEY (`periode_mens_id`) REFERENCES `c_periode_mensuel` (`periode_mens_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Philipine le 21-11-2022

-- -----------------------------------------------------

-- Table `c_immo`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_immo` (
        `immo_id` INT NOT NULL AUTO_INCREMENT,
        `immo_gros_oeuvre` FLOAT NULL,
        `immo_facade` FLOAT NULL,
        `immo_installation` FLOAT NULL,
        `immo_agencement` FLOAT NULL,
        `immo_mobilier` VARCHAR(45) NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`immo_id`),
        CONSTRAINT `fk_c_immo_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Philipine 2022-11-23

-- -----------------------------------------------------

-- Table `c_lot_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_lot_client` (
        `cliLot_id` INT NOT NULL AUTO_INCREMENT,
        `cliLot_num` VARCHAR(120) NULL,
        `cliLot_nom` VARCHAR(120) NULL,
        `cliLot_adresse1` VARCHAR(120) NULL,
        `cliLot_adresse2` VARCHAR(120) NULL,
        `cliLot_adresse3` VARCHAR(120) NULL,
        `cliLot_cp` VARCHAR(120) NULL,
        `cliLot_ville` VARCHAR(120) NULL,
        `cliLot_pays` VARCHAR(120) NULL,
        `cliLot_principale` VARCHAR(45) NULL,
        `cliLot_etat` INT(11) NOT NULL,
        `cliLot_mode_paiement` VARCHAR(50) NULL,
        `cliLot_encaiss_loyer` VARCHAR(50) NULL,
        `cliLot_iban_client` VARCHAR(25) NULL,
        `cliLot_bic_client` VARCHAR(30) NULL,
        `dossier_id` INT(11) NOT NULL,
        `typologielot_id` INT NOT NULL,
        `typelot_id` INT NOT NULL,
        `gestionnaire_id` INT NOT NULL,
        `progm_id` INT NOT NULL,
        `produitcli_id` varchar(10) NOT NULL,
        `cli_id` INT NOT NULL,
        PRIMARY KEY (`cliLot_id`),
        CONSTRAINT `fk_c_lot_client_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typologieLot1` FOREIGN KEY (`typologielot_id`) REFERENCES `c_typologieLot` (`typologielot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typelot1` FOREIGN KEY (`typelot_id`) REFERENCES `c_typelot` (`typelot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_programme1` FOREIGN KEY (`progm_id`) REFERENCES `c_programme` (`progm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 25-11-2022

-- -----------------------------------------------------

-- Table `c_acte`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_acte` (
        `acte_id` INT NOT NULL AUTO_INCREMENT,
        `acte_type_acquisition` VARCHAR(50) NULL,
        `acte_date_notarie` DATETIME NULL,
        `acte_date_acquisition` DATETIME NULL,
        `acte_valeur_acquisition_ht` FLOAT NULL,
        `acte_valeur_terrain_ht` FLOAT NULL,
        `acte_valeur_construction_ht` FLOAT NULL,
        `acte_valeur_mobilier_ht` FLOAT NULL,
        `acte_montant_tva_immo` FLOAT NULL,
        `acte_montant_tva_frais_acquisition` FLOAT NULL,
        `acte_date_remboursement` DATETIME NULL,
        `acte_date_reg` DATETIME NULL,
        `acte_date_debut_amortissement` DATETIME NULL,
        `cliLot_id` INT NOT NULL,
        `immo_id` INT NOT NULL,
        PRIMARY KEY (`acte_id`),
        CONSTRAINT `fk_c_acte_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Tsilavina 2022-11-28

-- -----------------------------------------------------

-- Table `c_lot_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_lot_client` (
        `cliLot_id` INT NOT NULL AUTO_INCREMENT,
        `cliLot_num` VARCHAR(120) NULL,
        `cliLot_nom` VARCHAR(120) NULL,
        `cliLot_adresse1` VARCHAR(120) NULL,
        `cliLot_adresse2` VARCHAR(120) NULL,
        `cliLot_adresse3` VARCHAR(120) NULL,
        `cliLot_cp` VARCHAR(120) NULL,
        `cliLot_ville` VARCHAR(120) NULL,
        `cliLot_pays` VARCHAR(120) NULL,
        `cliLot_principale` VARCHAR(45) NULL,
        `cliLot_etat` INT(11) NOT NULL,
        `cliLot_mode_paiement` VARCHAR(50) NULL,
        `cliLot_encaiss_loyer` VARCHAR(50) NULL,
        `cliLot_iban_client` VARCHAR(25) NULL,
        `cliLot_bic_client` VARCHAR(30) NULL,
        `dossier_id` INT(11) NOT NULL,
        `typologielot_id` INT NOT NULL,
        `typelot_id` INT NOT NULL,
        `gestionnaire_id` INT NOT NULL,
        `progm_id` INT NOT NULL,
        `produitcli_id` varchar(10) NOT NULL,
        `cli_id` INT NOT NULL,
        PRIMARY KEY (`cliLot_id`),
        CONSTRAINT `fk_c_lot_client_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typologieLot1` FOREIGN KEY (`typologielot_id`) REFERENCES `c_typologieLot` (`typologielot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_typelot1` FOREIGN KEY (`typelot_id`) REFERENCES `c_typelot` (`typelot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_lot_client_c_programme1` FOREIGN KEY (`progm_id`) REFERENCES `c_programme` (`progm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Tsilavina 2022-11-30

-- -----------------------------------------------------

-- Table `c_lot_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_dossier` (
        `doc_id` INT NOT NULL AUTO_INCREMENT,
        `doc_nom` VARCHAR(120) NULL,
        `doc_path` VARCHAR(120) NULL,
        `doc_date_creation` DATETIME NULL,
        `doc_etat` VARCHAR(11) NULL,
        `util_id` INT(11) NOT NULL,
        `dossier_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_id`),
        CONSTRAINT `fk_c_document_dossier_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Mise à jour Tsialvina le 01/12/2022

-- Table `c_messagerie_paramserveur_utilisateur`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_messagerie_paramserveur_utilisateur` (
        `mess_id` INT(11) NOT NULL AUTO_INCREMENT,
        `mess_libelle` VARCHAR(255) NOT NULL,
        `mess_description` LONGTEXT NULL,
        `mess_protocole` VARCHAR(50) NOT NULL,
        `mess_type` VARCHAR(50) NOT NULL,
        `mess_adressemailserveur` VARCHAR(255) NOT NULL,
        `mess_typeauthentification` VARCHAR(25) NOT NULL,
        `mess_port` INT(11) NOT NULL,
        `mess_nomsource` VARCHAR(255) NOT NULL,
        `mess_email` VARCHAR(255) NOT NULL,
        `mess_motdepasse` VARCHAR(255) NOT NULL,
        `mess_compteretourmail` VARCHAR(255) NOT NULL,
        `mess_priorite` VARCHAR(100) NOT NULL,
        `mess_charset` VARCHAR(50) NOT NULL,
        `mess_pardefaut` INT(1) NOT NULL DEFAULT '0' COMMENT '0: par défaut; 1: le contraire',
        `mess_etat` INT(1) NOT NULL,
        `util_id` INT(11) NOT NULL,
        `mess_signature` VARCHAR(200) NULL,
        PRIMARY KEY (`mess_id`),
        CONSTRAINT `fk_c_messagerie_paramserveur_utilisateur_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------

-- Mise à jour Stenny le 02/12/2022

-- -----------------------------------------------------

-- -----------------------------------------------------

-- Table `c_type_charge_lot`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_charge_lot` (
        `tpchrg_lot_id` INT NOT NULL AUTO_INCREMENT,
        `tpchrg_lot_nom` VARCHAR(255) NULL,
        `tpchrg_lot_numero` INT(11) DEFAULT 0,
        `tpchrg_lot_report_liasse_fiscale_ht` INT NOT NULL DEFAULT 0,
        `tpchrg_lot_report_decla_tva3562` INT NOT NULL DEFAULT 0,
        PRIMARY KEY (`tpchrg_lot_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_charge_lot` (
        `tpchrg_lot_id`,
        `tpchrg_lot_nom`,
        `tpchrg_lot_numero`,
        `tpchrg_lot_report_liasse_fiscale_ht`,
        `tpchrg_lot_report_decla_tva3562`
    )
VALUES (
        1,
        'Appels de fonds du syndic',
        0,
        1,
        0
    ), (
        2,
        'Décomptes de charges du syndic – Autres charges externes',
        342,
        1,
        1
    ), (3, 'Autres taxes', 0, 1, 0), (
        4,
        'Assurance Emprunt – Charges Financières',
        294,
        1,
        0
    ), (
        5,
        'Assurance PNO – Autres charges externes',
        342,
        1,
        0
    ), (
        6,
        'Frais de déplacement – Autres charges externes',
        342,
        1,
        0
    ), (
        7,
        'Energie – Autres charges externes',
        342,
        1,
        1
    ), (
        8,
        'Frais accessoire emprunt – Autres charges externes',
        342,
        1,
        0
    ), (
        9,
        'Commissions – Autres charges externes',
        342,
        1,
        1
    ), (
        10,
        'Honoraires avocat et tiers – Autres charges externes',
        342,
        1,
        1
    ), (
        11,
        'Honoraires de gestion – Autres charges externes',
        342,
        1,
        1
    ), (
        12,
        'Petit équipement – Autres charges eSxternes',
        342,
        1,
        1
    ), (
        13,
        'Services hôtelier – Autres charges externes',
        342,
        1,
        1
    ), (
        14,
        'Publicité – Autres charges externes',
        342,
        1,
        1
    ), (
        15,
        'Internet – Autres charges externes',
        342,
        1,
        1
    ), (
        16,
        'Autres Charges – Autres charges externes',
        342,
        1,
        1
    );

-- -----------------------------------------------------

-- Mise à jour Stenny le 02/12/2022

-- -----------------------------------------------------

-- -----------------------------------------------------

-- Table `c_type_charge_deductible_lot_encaissement`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_charge_deductible_lot_encaissement` (
        `tpchrg_deduct_id` INT NOT NULL AUTO_INCREMENT,
        `tpchrg_deduct_nom` VARCHAR(255) NULL,
        `tpchrg_deduct_numero` INT(11) DEFAULT 0,
        `tpchrg_deduct_report_liasse_fiscale_ht` INT NOT NULL DEFAULT 0,
        `tpchrg_deduct_report_decla_tva3562` INT NOT NULL DEFAULT 0,
        PRIMARY KEY (`tpchrg_deduct_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_charge_deductible_lot_encaissement` (
        `tpchrg_deduct_id`,
        `tpchrg_deduct_nom`,
        `tpchrg_deduct_numero`,
        `tpchrg_deduct_report_liasse_fiscale_ht`,
        `tpchrg_deduct_report_decla_tva3562`
    )
VALUES (
        1,
        'Appels de fonds du syndic',
        0,
        1,
        0
    ), (
        2,
        'Décomptes de charges du syndic – Autres charges externes',
        342,
        1,
        1
    ), (3, 'Autres taxes', 0, 1, 0), (
        4,
        'Assurance Emprunt – Charges Financières',
        294,
        1,
        0
    ), (
        5,
        'Assurance PNO – Autres charges externes',
        342,
        1,
        0
    ), (
        6,
        'Frais de déplacement – Autres charges externes',
        342,
        1,
        0
    ), (
        7,
        'Energie – Autres charges externes',
        342,
        1,
        1
    ), (
        8,
        'Frais accessoire emprunt – Autres charges externes',
        342,
        1,
        0
    ), (
        9,
        'Commissions – Autres charges externes',
        342,
        1,
        1
    ), (
        10,
        'Honoraires avocat et tiers – Autres charges externes',
        342,
        1,
        1
    ), (
        11,
        'Honoraires de gestion – Autres charges externes',
        342,
        1,
        1
    ), (
        12,
        'Petit équipement – Autres charges externes',
        342,
        1,
        1
    ), (
        13,
        'Services hôtelier – Autres charges externes',
        342,
        1,
        1
    ), (
        14,
        'Publicité – Autres charges externes',
        342,
        1,
        1
    ), (
        15,
        'Internet – Autres charges externes',
        342,
        1,
        1
    ), (
        16,
        'Autres Charges – Autres charges externes',
        342,
        1,
        1
    );

-- -----------------------------------------------------

-- Mise à jour Stenny le 02/12/2022

-- -----------------------------------------------------

-- -----------------------------------------------------

-- Table `c_type_produit_lot_encaissement`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_produit_lot_encaissement` (
        `tpchrg_prod_id` INT NOT NULL AUTO_INCREMENT,
        `tpchrg_prod_nom` VARCHAR(255) NULL,
        `tpchrg_prod_numero` INT(11) DEFAULT 0,
        `tpchrg_prod_report_liasse_fiscale_ht` INT NOT NULL DEFAULT 0,
        `tpchrg_prod_report_decla_tva3562` INT NOT NULL DEFAULT 0,
        PRIMARY KEY (`tpchrg_prod_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_produit_lot_encaissement` (
        `tpchrg_prod_id`,
        `tpchrg_prod_nom`
    )
VALUES (1, 'TOM'), (2, 'Forfait charges'), (3, 'Intérêts de retard'), (4, 'Client IPierre'), (5, 'Forfait conciergerie');

-- Mis à jour par Stenny le 02-12-2022

-- -----------------------------------------------------

-- Table `c_encaissement`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_encaissement` (
        `enc_id` INT NOT NULL AUTO_INCREMENT,
        `enc_date` DATE NULL,
        `enc_periode` VARCHAR(40) NULL,
        `enc_annee` YEAR NULL,
        `enc_montant` FLOAT NULL,
        `enc_mode` VARCHAR(40) NULL,
        `bail_id` INT NOT NULL,

`enc_app_ttc` FLOAT DEFAULT 0,
`enc_park_ttc` FLOAT DEFAULT 0,
`enc_charge_ttc` FLOAT DEFAULT 0,
`enc_charge_deductible_ttc` FLOAT DEFAULT 0,
`enc_produit_ttc` FLOAT DEFAULT 0,
`enc_total_ttc` FLOAT DEFAULT 0,

`enc_app_tva` INT DEFAULT 0,
`enc_park_tva` INT DEFAULT 0,
`enc_charge_tva` INT DEFAULT 0,
`enc_charge_deductible_tva` INT DEFAULT 0,
`enc_produit_tva` INT DEFAULT 0,
`enc_app_montant_tva` FLOAT DEFAULT 0,
`enc_park_montant_tva` FLOAT DEFAULT 0,
`enc_charge_montant_tva` FLOAT DEFAULT 0,
`enc_charge_deductible_montant_tva` FLOAT DEFAULT 0,
`enc_produit_montant_tva` FLOAT DEFAULT 0,
`enc_total_montant_tva` FLOAT DEFAULT 0,
`enc_app_ht` FLOAT DEFAULT 0,
`enc_park_ht` FLOAT DEFAULT 0,
`enc_charge_ht` FLOAT DEFAULT 0,
`enc_charge_deductible_ht` FLOAT DEFAULT 0,
`enc_produit_ht` FLOAT DEFAULT 0,
`enc_total_ht` FLOAT DEFAULT 0,

`enc_commentaire` LONGTEXT NULL, 

`tpchrg_deduct_id` INT NOT NULL, `tpchrg_prod_id` INT NOT NULL, 

PRIMARY KEY (`enc_id`),
INDEX `fk_c_encaissement_c_bail1_idx` (`bail_id` ASC),
CONSTRAINT `fk_c_encaissement_c_bail1` FOREIGN KEY (`bail_id`) REFERENCES `c_bail` (`bail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT `fk_c_encaissement_c_type_charge_deductible_lot_encaissement1` FOREIGN KEY (`tpchrg_deduct_id`) REFERENCES `c_type_charge_deductible_lot_encaissement` (`tpchrg_deduct_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT `fk_c_encaissement_c_type_produit_lot_encaissement2` FOREIGN KEY (`tpchrg_prod_id`) REFERENCES `c_type_produit_lot_encaissement` (`tpchrg_prod_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Mise à jour par Tsilavina 02/12/2022

-- -----------------------------------------------------

-- -----------------------------------------------------

-- Table `c_type_charge_lot`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_charge_lot` (
        `tpchrg_lot_id` INT NOT NULL AUTO_INCREMENT,
        `tpchrg_lot_nom` VARCHAR(50) NULL,
        `tpchrg_lot_numero` INT NULL,
        `tpchrg_lot_report_liasse_fiscale_ht` INT NULL,
        `tpchrg_lot_report_decla_tva3562` INT NULL,
        PRIMARY KEY (`tpchrg_lot_id`)
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_charge`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_charge` (
        `charge_id` INT NOT NULL AUTO_INCREMENT,
        `date_facture` DATE NOT NULL,
        `montant_ht` FLOAT NOT NULL,
        `tva` FLOAT NOT NULL,
        `montant_ttc` FLOAT NOT NULL,
        `tva_taux` FLOAT NOT NULL,
        `annee` VARCHAR(10) NOT NULL,
        `type_charge_id` INT NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`charge_id`),
        INDEX `fk_c_charge_c_type_charge_lot1_idx` (`type_charge_id` ASC),
        INDEX `fk_c_charge_c_lot_client1_idx` (`cliLot_id` ASC),
        CONSTRAINT `fk_c_charge_c_type_charge_lot1` FOREIGN KEY (`type_charge_id`) REFERENCES `c_type_charge_lot` (`tpchrg_lot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_charge_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_charge_lot` (
        `tpchrg_lot_id`,
        `tpchrg_lot_nom`,
        `tpchrg_lot_numero`,
        `tpchrg_lot_report_liasse_fiscale_ht`,
        `tpchrg_lot_report_decla_tva3562`
    )
VALUES (
        1,
        'Appels de fonds du syndic',
        0,
        1,
        0
    ), (
        2,
        'Décomptes de charges du syndic –Autres charges externes',
        342,
        1,
        1
    ), (3, 'Autres taxes', 0, 1, 0), (
        4,
        'Assurance Emprunt – Charges Financières',
        294,
        1,
        0
    ), (
        5,
        'Assurance PNO – Autres charges externes',
        342,
        1,
        0
    ), (
        6,
        'Frais de déplacement – Autres charges externes',
        342,
        1,
        0
    ), (
        7,
        'Energie – Autres charges externes',
        342,
        1,
        1
    ), (
        8,
        'Frais accessoire emprunt – Autres charges externes',
        342,
        1,
        0
    ), (
        9,
        'Commissions – Autres charges externes',
        342,
        1,
        1
    ), (
        10,
        'Honoraires avocat et tiers – Autres charges externes',
        342,
        1,
        1
    ), (
        11,
        'Honoraires de gestion – Autres charges externes',
        342,
        1,
        1
    ), (
        12,
        'Petit équipement – Autres charges externes',
        342,
        1,
        1
    ), (
        13,
        'Services hôtelier – Autres charges externes',
        342,
        1,
        1
    ), (
        14,
        'Publicité – Autres charges externes',
        342,
        1,
        1
    ), (
        15,
        'Internet – Autres charges externes',
        342,
        1,
        1
    ), (
        16,
        'Autres Charges – Autres charges externes',
        342,
        1,
        1
    );

-- Mise à jour par Tsilavina 2022-12-05

-- -----------------------------------------------------

-- Table `c_document_charge`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_charge` (
        `doc_charge_id` INT NOT NULL AUTO_INCREMENT,
        `doc_charge_nom` VARCHAR(120) NULL,
        `doc_charge_path` VARCHAR(120) NULL,
        `doc_charge_creation` DATETIME NULL,
        `doc_charge_traitement` TIMESTAMP NULL,
        `doc_charge_etat` TINYINT(1) NULL,
        `doc_charge_traite` TINYINT(1) NULL,
        `doc_charge_annee` VARCHAR(10),
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_charge_id`),
        CONSTRAINT `fk_c_document_charge_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_charge_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 05-12-2022

-- -----------------------------------------------------

-- Table `c_acte`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_acte` (
        `acte_id` INT NOT NULL AUTO_INCREMENT,
        `acte_type_acquisition` VARCHAR(50) NULL,
        `acte_date_notarie` DATETIME NULL,
        `acte_date_acquisition` DATETIME NULL,
        `acte_valeur_acquisition_ht` FLOAT NULL,
        `acte_valeur_terrain_ht` FLOAT NULL,
        `acte_valeur_construction_ht` FLOAT NULL,
        `acte_valeur_mobilier_ht` FLOAT NULL,
        `acte_montant_tva_immo` FLOAT NULL,
        `acte_montant_tva_frais_acquisition` FLOAT NULL,
        `acte_date_remboursement` DATETIME NULL,
        `acte_date_reg` DATETIME NULL,
        `acte_date_debut_amortissement` DATETIME NULL,
        `acte_taux_terrain` VARCHAR(5) NULL,
        `acte_taux_construction` VARCHAR(5) NULL,
        `cliLot_id` INT NOT NULL,
        `immo_id` INT NOT NULL,
        PRIMARY KEY (`acte_id`),
        CONSTRAINT `fk_c_acte_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 13-12-2022

-- -----------------------------------------------------

-- Table `c_document_oeuvre`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_oeuvre` (
        `doc_oeuvre_id` INT NOT NULL AUTO_INCREMENT,
        `doc_oeuvre_nom` VARCHAR(120) NULL,
        `doc_oeuvre_path` VARCHAR(120) NULL,
        `doc_oeuvre_creation` DATETIME NULL,
        `doc_oeuvre_traitement` TIMESTAMP NULL,
        `doc_oeuvre_etat` TINYINT(1) NULL,
        `doc_oeuvre_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_oeuvre_id`),
        CONSTRAINT `fk_c_document_oeuvre_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_oeuvre_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_facade`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_facade` (
        `doc_facade_id` INT NOT NULL AUTO_INCREMENT,
        `doc_facade_nom` VARCHAR(120) NULL,
        `doc_facade_path` VARCHAR(120) NULL,
        `doc_facade_creation` DATETIME NULL,
        `doc_facade_traitement` TIMESTAMP NULL,
        `doc_facade_etat` TINYINT(1) NULL,
        `doc_facade_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_facade_id`),
        CONSTRAINT `fk_c_document_facade_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_facade_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_installation`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_installation` (
        `doc_installation_id` INT NOT NULL AUTO_INCREMENT,
        `doc_installation_nom` VARCHAR(120) NULL,
        `doc_installation_path` VARCHAR(120) NULL,
        `doc_installation_creation` DATETIME NULL,
        `doc_installation_traitement` TIMESTAMP NULL,
        `doc_installation_etat` TINYINT(1) NULL,
        `doc_installation_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_installation_id`),
        CONSTRAINT `fk_c_document_installation_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_installation_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_agencement`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_agencement` (
        `doc_agencement_id` INT NOT NULL AUTO_INCREMENT,
        `doc_agencement_nom` VARCHAR(120) NULL,
        `doc_agencement_path` VARCHAR(120) NULL,
        `doc_agencement_creation` DATETIME NULL,
        `doc_agencement_traitement` TIMESTAMP NULL,
        `doc_agencement_etat` TINYINT(1) NULL,
        `doc_agencement_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_agencement_id`),
        CONSTRAINT `fk_c_document_agencement_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_agencement_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_document_mobilier`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_mobilier` (
        `doc_mobilier_id` INT NOT NULL AUTO_INCREMENT,
        `doc_mobilier_nom` VARCHAR(120) NULL,
        `doc_mobilier_path` VARCHAR(120) NULL,
        `doc_mobilier_creation` DATETIME NULL,
        `doc_mobilier_traitement` TIMESTAMP NULL,
        `doc_mobilier_etat` TINYINT(1) NULL,
        `doc_mobilier_traite` INT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`doc_mobilier_id`),
        CONSTRAINT `fk_c_document_mobilier_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_mobilier_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Tsilavina le 12-12-2022

-- -----------------------------------------------------

-- Table `c_deficits`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_deficits` (
        `id_deficits` INT NOT NULL AUTO_INCREMENT,
        `deficits_annee` VARCHAR(8) NOT NULL,
        `deficite_anterieur` FLOAT NOT NULL,
        `dossier_id` INT(11) NOT NULL,
        PRIMARY KEY (`id_deficits`),
        INDEX `fk_c_deficits_c_dossier1_idx` (`dossier_id` ASC),
        CONSTRAINT `fk_c_deficits_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_benefices`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_benefices` (
        `id_benefices` INT NOT NULL AUTO_INCREMENT,
        `benefices_annee` VARCHAR(8) NOT NULL,
        `benefices` FLOAT NOT NULL,
        `dossier_id` INT(11) NOT NULL,
        PRIMARY KEY (`id_benefices`),
        INDEX `fk_c_benefices_c_dossier1_idx` (`dossier_id` ASC),
        CONSTRAINT `fk_c_benefices_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Table `c_benefice_deficits`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_benefice_deficits` (
        `id_benefice_deficits` INT NOT NULL AUTO_INCREMENT,
        `id_benefices` INT NULL,
        `id_deficits` INT NULL,
        `deficit_utilise` FLOAT NOT NULL,
        PRIMARY KEY (`id_benefice_deficits`),
        INDEX `fk_c_benefice_deficits_c_benefices1_idx` (`id_benefices` ASC),
        INDEX `fk_c_benefice_deficits_c_deficits1_idx` (`id_deficits` ASC),
        CONSTRAINT `fk_c_benefice_deficits_c_benefices1` FOREIGN KEY (`id_benefices`) REFERENCES `c_benefices` (`id_benefices`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_benefice_deficits_c_deficits1` FOREIGN KEY (`id_deficits`) REFERENCES `c_deficits` (`id_deficits`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mise à jour par Tsilavina le 14-12-2022

-- -----------------------------------------------------

-- Table `c_document_loyer`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_fiscal` (
        `doc_fiscal_id` INT NOT NULL AUTO_INCREMENT,
        `doc_fiscal_nom` VARCHAR(120) NULL,
        `doc_fiscal_path` VARCHAR(120) NULL,
        `doc_fiscal_creation` DATETIME NULL,
        `doc_fiscal_traitement` TIMESTAMP NULL,
        `doc_fiscal_etat` TINYINT(1) NULL,
        `doc_fiscal_traite` TINYINT(1) NULL,
        `prenom_util` VARCHAR(120) NULL,
        `util_id` INT(11) NOT NULL,
        `dossier_id` INT NOT NULL,
        PRIMARY KEY (`doc_fiscal_id`),
        CONSTRAINT `fk_c_document_fiscal_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_document_fiscal_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 15-12-2022

-- -----------------------------------------------------

-- Table `c_immo_general`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_immo_general` (
        `immo_general_id` INT NOT NULL AUTO_INCREMENT,
        `immo_general_annee` VARCHAR(45) NULL,
        `immo_general_gros_oeuvre` FLOAT NULL,
        `immo_general_facade` FLOAT NULL,
        `immo_general_installation` FLOAT NULL,
        `immo_general_agencement` FLOAT NULL,
        `immo_general_mobilier` FLOAT NULL,
        `immo_general_total` FLOAT NULL,
        `immo_general_date_modification` DATETIME NULL,
        `immo_general_justification` TEXT NULL,
        `cliLot_id` INT NOT NULL,
        `util_id` INT NOT NULL,
        PRIMARY KEY (`immo_general_id`),
        CONSTRAINT `fk_c_immo_general_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_immo_general_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 19-12-2022

-- -----------------------------------------------------

-- Table `c_mandat`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_mandat` (
        `mandat_id` int(11) NOT NULL AUTO_INCREMENT,
        `mandat_date_creation` datetime DEFAULT NULL,
        `mandat_lieu_creation` varchar(120) DEFAULT NULL,
        `mandat_montant` FLOAT NULL,
        `mandat_etat` TINYINT(1) NULL,
        `mandat_path` varchar(120) DEFAULT NULL,
        `lot_id` int(11) NOT NULL,
        `type_mandat_id` int(11) DEFAULT NULL,
        `prospect_id` int(11) NOT NULL,
        PRIMARY KEY (`mandat_id`),
        KEY `fk_c_mandat_c_lot_prospect1` (`lot_id`),
        KEY `fk_c_mandat_c_type_mandat1` (`type_mandat_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8;

-- -----------------------------------------------------

-- Table `c_mandat_client`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_mandat_client` (
        `mandat_cli_id` INT NOT NULL AUTO_INCREMENT,
        `mandat_cli_date_creation` DATETIME NULL,
        `mandat_cli_lieu_creation` VARCHAR(120) NULL,
        `mandat_cli_montant` FLOAT NULL,
        `mandat_cli_etat` INT(1) NULL,
        `mandat_cli_path` VARCHAR(250) NULL,
        `client_id` INT(11) NOT NULL,
        `type_mandat_id` INT NOT NULL,
        `cliLot_id` INT NOT NULL,
        PRIMARY KEY (`mandat_cli_id`),
        CONSTRAINT `fk_c_mandat_client_c_client1` FOREIGN KEY (`client_id`) REFERENCES `c_client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_mandat_client_c_type_mandat1` FOREIGN KEY (`type_mandat_id`) REFERENCES `c_type_mandat` (`type_mandat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_mandat_client_c_lot_client2` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- Mis à jour par Philipine le 22-12-2022

ALTER TABLE `c_prospect`
ADD
    `etat_prospectcli` INT NOT NULL DEFAULT '1' AFTER `pays_id`;

ALTER TABLE `c_client`
ADD
    `etat_client` INT NOT NULL DEFAULT '2' AFTER `util_id`;

-- -----------------------------------------------------

-- Mise à jour Tsialvina le 15/12/2022

-- Table `c_messagerie_paramserveur_utilisateur`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_messagerie_paramserveur_utilisateur` (
        `mess_id` INT(11) NOT NULL AUTO_INCREMENT,
        `mess_libelle` VARCHAR(255) NOT NULL,
        `mess_description` LONGTEXT NULL,
        `mess_protocole` VARCHAR(50) NOT NULL,
        `mess_type` VARCHAR(50) NOT NULL,
        `mess_adressemailserveur` VARCHAR(255) NOT NULL,
        `mess_typeauthentification` VARCHAR(25) NOT NULL,
        `mess_port` INT(11) NOT NULL,
        `mess_nomsource` VARCHAR(255) NOT NULL,
        `mess_email` VARCHAR(255) NOT NULL,
        `mess_motdepasse` VARCHAR(255) NOT NULL,
        `mess_compteretourmail` VARCHAR(255) NOT NULL,
        `mess_priorite` VARCHAR(100) NOT NULL,
        `mess_charset` VARCHAR(50) NOT NULL,
        `mess_etat` INT(1) NOT NULL,
        `util_id` INT(11) NOT NULL,
        `mess_signature` VARCHAR(200) NULL,
        `mess_motdepasse_crypte` VARCHAR(255) NOT NULL,
        PRIMARY KEY (`mess_id`),
        CONSTRAINT `fk_c_messagerie_paramserveur_utilisateur_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------

-- Mise à jour Tsialvina le 22/12/2022

-- Table `c_param_modelemail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_param_modelemail` (
        `pmail_id` INT(11) NOT NULL AUTO_INCREMENT,
        `pmail_libelle` VARCHAR(100) NULL DEFAULT NULL,
        `pmail_description` TEXT NULL DEFAULT NULL,
        `pmail_objet` TEXT NULL DEFAULT NULL,
        `pmail_contenu` LONGTEXT NULL DEFAULT NULL,
        `emeteur_mail` INT(11) NULL DEFAULT NULL,
        `signature` INT(11) NULL DEFAULT NULL,
        `util_id` INT(11) NOT NULL,
        `fichier_joint` VARCHAR(250) NULL,
        PRIMARY KEY (`pmail_id`),
        CONSTRAINT `fk_c_param_modelemail_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARACTER SET = utf8;

-- INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES

-- (1, 'Celavigestion -  Information taxe foncière', 'Modèle de mail utilisé lors de l\'envoi des informations sur la taxe foncière. Il permet d\'envoyer les informations de la taxe foncière et le montant de la TOM  par mail au gestionnaire. Utilisable depuis le menu propriétaire, Fiche Lot, sous onglet taxe foncière, bouton envoyer un mail au gestionnaire lors de la validation des informations.', 'Celavigestion -  @taxe_fonciere', 'Madame, Monsieur,\r\n\r\nVous trouverez en pièce jointe une copie de la taxe foncière de notre client qui nous a dûment mandaté ………………..lié par bail avec votre société. Le bail qui vous unit avec notre client prévoit une prise en charge et remboursement par votre société de la taxe sur les ordures ménagères figurant sur l’avis joint. Je vous remercie d’effectuer le remboursement de manière analogue au paiement des loyers.\r\n\r\nNom du programme  :  @nom_programme \r\nAnnée : @annee\r\nMontant de la TOM à rembourser : @montant_tom\r\n\r\nPJ : Avis de taxe foncière.\r\n\r\nBien cordialement,\r\n\r\nNom et coordonnées du chargé de clientèle\r\n', 1, 1, 1, 'Taxes Foncière concernée en pièce jointe'),

-- (2, 'Celavigestion -  Proposition mandat', 'Modèle de mail utilisé lors de l\'envoi d\'une proposition d\'un mandat. Il permet d\'envoyer un mandat par mail au gestionnaire. Utilisable depuis le menu propriétaire, Fiche Lot, sous onglet mandat, bouton envoyé un mail sur la liste des documents', 'Celavigestion -  @proposition_mandat', 'Bonjour M. / Mme @nom_client @prenom_client , \n\n\nNous vous envoyons le(s) proposition(s) de mandat disponible qui vous intéressent : le @pack_confort,\nOn vous laisse signer et merci de nous retourner la proposition signée.\n\nCe message a été envoyé depuis Celavigestion le @date_envoi_mail\n\nBien à vous,\n--\nL\'Administration Celavigestion\nBy Celavigestion Properties\n', 1, 1, 1, 'Mandat concernée en pièce jointe'),

-- (3, 'Celavigestion -  Déclaration erreur sur la taxe', 'Modèle de mail utilisé lors de la déclaration d\'erreur sur la taxe. Il permet d\'envoyer une déclaration d\'erreur sur la taxe par mail au gestionnaire. Utilisable depuis le menu propriétaire, Fiche Lot, sous onglet taxe foncière, bouton envoyer un mail au gestionnaire lors de la validation des informations.', 'Celavigestion -  @erreur_taxe', 'Bonjour,\n\nIl y a une erreur sur les informations concernant la taxe foncière de l\'année @annee du client @nom_client @prenom_client.\n\nMontant de la taxe foncière : @montant_taxe\nMontant de la TOM : @montant_tom\n\nCe message a été envoyé depuis Celavigestion le @date_envoi_mail\n\nBien à vous,\n--\nL\'Administration Celavigestion\nBy Celavigestion Properties\n', 1, 1, 1, 'Pas de fichier joint pour ce modèle de mail'),

-- (4, 'Celavigestion -  Envoi facture Loyer', 'Modèle de mail utilisé lors de l\'envoi de factures de loyer. Il permet d\'envoyer la facture par mail au gestionnaire. Utilisable depuis le menu Propriétaire, Fiche Lot, sous onglet Loyer, bouton sur la liste des documents.', 'Celavigestion -  @facture_Loyer', 'Bonjour,\n\nMerci de trouver ci-joint les informations  concernant la facture de loyer du client @nom_client.\n\nCe message a été envoyé depuis Celavigestion le @date_envoi_mail\n\nBien à vous,\n--\nL\'Administration Celavigestion\nBy Celavigestion Properties\n', 1, 1, 1, 'Facture du loyer concernée en pièce jointe');

-- Mis à jour par Stenny le 26-12-2022

ALTER TABLE `c_facture_loyer`
ADD
    `fact_email_sent` INT(1) NOT NULL DEFAULT 0;

-- Mise à jour par Tsilavina le 03-01-2023

INSERT INTO
    `c_typologielot` (
        `typologielot_id`,
        `typologielot_libelle`
    )
VALUES (NULL, 'Résidence affaires ');

-- Mis à jour par Stenny le 03-01-2023

-- Delete the c_type_echeance table

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_echeance` (
        `tpech_id` INT NOT NULL AUTO_INCREMENT,
        `tpech_valeur` INT NULL,
        PRIMARY KEY (`tpech_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_echeance` (`tpech_valeur`)
VALUES (1), (5), (10), (15), (20), (30);

-- -----------------------------------------------------

-- Mise à jour  Stenny le 04/01/2023

-- Table `c_role_utilisateur`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_role_utilisateur` (
        `rol_util_id` INT NOT NULL AUTO_INCREMENT,
        `rol_util_valeur` INT UNSIGNED NULL DEFAULT 0,
        `rol_util_libelle` VARCHAR (50) NOT NULL,
        PRIMARY KEY (`rol_util_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_role_utilisateur` (
        `rol_util_valeur`,
        `rol_util_libelle`
    )
VALUES (1, "Administrateur"), (2, "Responsable"), (3, "Gestionnaire de compte"), (4, "Comptable"), (5, "Visuel");

-- -----------------------------------------------------

-- Mise à jour  Stenny le 05/01/2023

-- *** IMPORTANT *** make sure that default rol_util_id table c_utilisateur is 1

-- -----------------------------------------------------

ALTER TABLE `c_utilisateur`
ADD
    `rol_util_id` INT(11) NOT NULL DEFAULT 1;

ALTER TABLE `c_utilisateur`
ADD
    CONSTRAINT `fk_c_utilisateur_c_role_utilisateur1` FOREIGN KEY (`rol_util_id`) REFERENCES `c_role_utilisateur`(`rol_util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Mis à jour par Tsilavina le 04-01-2023

ALTER TABLE `c_lot_client`
ADD
    `rib` VARCHAR(250) NULL AFTER `cli_id`

-- -----------------------------------------------------

-- Mise à jour  Stenny le 10/01/2023

-- Table `c_type_contact_gestionnaire`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_contact_gestionnaire` (
        `type_cont_id` INT NOT NULL AUTO_INCREMENT,
        `type_cont_libelle` VARCHAR (50) NOT NULL,
        PRIMARY KEY (`type_cont_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_contact_gestionnaire` (`type_cont_libelle`)
VALUES ("standard"), ("comptabilité"), ("direction"), ("propriétaires");

ALTER TABLE
    `c_contactgestionnaire`
ADD
    `type_cont_id` INT(11) NOT NULL DEFAULT 1;

ALTER TABLE
    `c_contactgestionnaire`
ADD
    CONSTRAINT `fk_c_utilisateur_c_type_contact_gestionnaire1` FOREIGN KEY (`type_cont_id`) REFERENCES `c_type_contact_gestionnaire`(`type_cont_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Mis à jour par Philipine le 12-01-2023

ALTER TABLE `c_dossier`
ADD
    `regime` VARCHAR(100) NULL AFTER `dossier_pays`,
ADD
    `cloture_jour` VARCHAR(2) NULL AFTER `regime`,
ADD
    `cloture_mois` VARCHAR(2) NULL AFTER `cloture_jour`,
ADD
    `cloture_jour_fin` VARCHAR(2) NULL AFTER `cloture_mois`,
ADD
    `cloture_mois_fin` VARCHAR(2) NULL AFTER `cloture_jour_fin`

ALTER TABLE `c_lot_client`
ADD
    `activite` VARCHAR(50) NULL AFTER `rib`,
ADD
    `defiscalisation` VARCHAR(50) NULL AFTER `activite`,
ADD
    `fin_defiscalisation` DATE NULL AFTER `defiscalisation`,
ADD
    `tva` VARCHAR(50) NULL AFTER `fin_defiscalisation`,
ADD
    `rembst_tom` VARCHAR(50) NULL AFTER `tva`,
ADD
    `facture_loyer` VARCHAR(50) NULL AFTER `rembst_tom`,
ADD
    `facture_charge` VARCHAR(50) NULL AFTER `facture_loyer`,
ADD
    `facture_tom` VARCHAR(50) NULL AFTER `facture_charge`,
ADD
    `comptabilite` VARCHAR(50) NULL AFTER `facture_tom`;

-- -----------------------------------------------------

-- Mise à jour  Stenny le 12/01/2023

-- Table `c_client`

-- -----------------------------------------------------

ALTER TABLE `c_prospect`
ADD
    `prospect_after_id` INT(11) DEFAULT null;

-- -----------------------------------------------------

-- Mise à jour par Philipine le 18-01-2023

-- Table `c_cgp`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_cgp` (
        `cgp_id` int(11) NOT NULL AUTO_INCREMENT,
        `cgp_nom` varchar(120) DEFAULT NULL,
        `cgp_prenom` varchar(120) DEFAULT NULL,
        `cgp_email` varchar(120) DEFAULT NULL,
        `cgp_tel` varchar(120) DEFAULT NULL,
        `cgp_adresse1` varchar(120) DEFAULT NULL,
        `cgp_adresse2` varchar(120) DEFAULT NULL,
        `cgp_adresse3` varchar(120) DEFAULT NULL,
        `cgp_cp` varchar(45) DEFAULT NULL,
        `cgp_ville` varchar(120) DEFAULT NULL,
        `cgp_pays` varchar(120) DEFAULT NULL,
        `cgp_etat` int(1) DEFAULT NULL,
        PRIMARY KEY (`cgp_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE
    `c_cgp` CHANGE `cgp_etat` `cgp_etat` INT(1) NOT NULL DEFAULT '1';

ALTER TABLE `c_dossier`
ADD
    `cgp_id` INT(11) NOT NULL DEFAULT '1' AFTER `sie_id`;

INSERT INTO
    `c_cgp` (
        `cgp_id`,
        `cgp_nom`,
        `cgp_prenom`,
        `cgp_email`,
        `cgp_tel`,
        `cgp_adresse1`,
        `cgp_adresse2`,
        `cgp_adresse3`,
        `cgp_cp`,
        `cgp_ville`,
        `cgp_pays`,
        `cgp_etat`
    )
VALUES (
        NULL,
        'PHILIPINE',
        'Esther',
        'philipine.manao@gmail.com',
        '062514569',
        'ANJOMA',
        NULL,
        NULL,
        '301',
        'FIANARANTSOA',
        NULL,
        '1'
    ), (
        NULL,
        'TSILAVINA',
        'Henintsoa',
        'tsilavina.manao@gmail.com',
        '01456987',
        'ISAHA',
        NULL,
        NULL,
        '301',
        'FIANARANTSOA',
        NULL,
        '1'
    );

-- Mise à jour  Stenny le 17/01/2023

-- Table `c_document_loyer`

-- -----------------------------------------------------

ALTER TABLE `c_document_loyer`
ADD
    `fact_loyer_id` INT(11) DEFAULT null;

ALTER TABLE `c_document_loyer`
ADD
    CONSTRAINT `fk_c_document_loyer_c_facture_loyer1` FOREIGN KEY (`fact_loyer_id`) REFERENCES `c_facture_loyer` (`fact_loyer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- -----------------------------------------------------

-- Mise à jour  Stenny le 17/01/2023

-- Table `c_bail`

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `indval_id` INT(11) DEFAULT null;

-- -----------------------------------------------------

-- Mise à jour  Stenny le 17/01/2023

-- Table `c_indexation_loyer`

-- -----------------------------------------------------

ALTER TABLE
    `c_indexation_loyer`
ADD
    `indlo_ref_loyer` INT(11) DEFAULT 0;

ALTER TABLE
    `c_indexation_loyer` CHANGE `indlo_indice_base` `indlo_indice_base` INT(11) NULL;

-- -----------------------------------------------------

-- Mise à jour  Tsilavina le 19/01/2023

-- Table `c_dossier`

-- -----------------------------------------------------

ALTER TABLE `c_dossier`
ADD
    `indivision` VARCHAR(10) NULL AFTER `cgp_id`,
ADD
    `rof` FLOAT NULL AFTER `indivision`;

-- -----------------------------------------------------

-- Mise à jour  Tsilavina le 20/01/2023

-- Table `c_taux_tva`

-- -----------------------------------------------------

DROP TABLE IF EXISTS`c_taux_tva`;

CREATE TABLE
    IF NOT EXISTS `c_taux_tva` (
        `tva_id` INT NOT NULL AUTO_INCREMENT,
        `tva_libelle` VARCHAR(120) NULL,
        `tva_valeur` FLOAT,
        `tva_compte_comptable` LONGTEXT NULL DEFAULT NULL,
        PRIMARY KEY (`tva_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_taux_tva` (`tva_libelle`, `tva_valeur`)
VALUES ('0%', 0), ('2.10%', 2.10), ('5.5%', 5.5), ('10%', 10), ('20%', 20);

-- -----------------------------------------------------

-- Mise à jour par Philipine le 23-01-2023

-- Table `c_rib`

-- -----------------------------------------------------

ALTER TABLE
    `c_historique` CHANGE `histo_action` `histo_action` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- -----------------------------------------------------

-- Mise à jour  Stenny le 23/01/2023

-- Table `c_bail`

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_valide` TINYINT(1) DEFAULT 0;

ALTER TABLE
    `c_bail` CHANGE `bail_cloture` `bail_cloture` TINYINT(1) NULL DEFAULT '0';

-- -----------------------------------------------------

-- Mise à jour par Philipine le 23-01-2023

-- Table `c_rib`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_rib` (
        `rib_id` int(11) NOT NULL AUTO_INCREMENT,
        `rib_iban` varchar(120) DEFAULT NULL,
        `rib_bic` varchar(120) DEFAULT NULL,
        `rib_etat` int(1) DEFAULT NULL,
        PRIMARY KEY (`rib_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- -----------------------------------------------------

-- Mise à jour par Stenny le 24-01-2023

-- Table `c_historique_mail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_historique_mail` (
        `hist_mail_id` int(11) NOT NULL AUTO_INCREMENT,
        `hist_mail_emetteur` varchar(120) DEFAULT NULL,
        `hist_mail_destinataire` varchar(120) DEFAULT NULL,
        `hist_mail_destinataire_copie` varchar(120) DEFAULT NULL,
        `hist_mail_objet` varchar(255) DEFAULT NULL,
        `hist_mail_message` LONGTEXT DEFAULT NULL,
        `hist_mail_fichiers_joints` varchar(120) DEFAULT NULL,
        `histo_id` int(11) DEFAULT NULL,
        PRIMARY KEY (`hist_mail_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- -----------------------------------------------------

-- Mise à jour  Stenny le 24/01/2023

-- -----------------------------------------------------

ALTER TABLE
    `c_document_loyer` CHANGE `fact_loyer_id` `fact_loyer_id` INT(11) NULL;

ALTER TABLE
    `c_utilisateur` CHANGE `tutil_id` `tutil_id` INT(11) NULL DEFAULT '1';

-- Mis à jour par Philipine le 26-01-2023

-- -----------------------------------------------------

-- Table `c_type_travail`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_travail` (
        `type_travail_id` INT NOT NULL AUTO_INCREMENT,
        `type_travail_libelle` VARCHAR(50) NULL,
        PRIMARY KEY (`type_travail_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_travail` (
        `type_travail_id`,
        `type_travail_libelle`
    )
VALUES (NULL, 'Ajout dossier'), (NULL, 'Ajout lot'), (NULL, 'Ajout taxe');

-- -----------------------------------------------------

-- Table `c_type_saisie_temps`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_type_saisie_temps` (
        `saisie_temps_id` INT NOT NULL AUTO_INCREMENT,
        `saisie_temps_libelle` VARCHAR(50) NULL,
        PRIMARY KEY (`saisie_temps_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_type_saisie_temps` (
        `saisie_temps_id`,
        `saisie_temps_libelle`
    )
VALUES (NULL, 'Sasie temps général'), (
        NULL,
        'Saisie temps propriétaire'
    ), (NULL, 'Saisie temps dossier'), (NULL, 'Saisie temps lot');

-- -----------------------------------------------------

-- Mise à jour  Stenny le 25/01/2023

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `preneur_id` INT DEFAULT 0;

ALTER TABLE
    `c_bail` CHANGE `bail_cloture` `bail_cloture` TINYINT(1) NULL DEFAULT '0';

-- -----------------------------------------------------

-- Mise à jour  Stenny le 30/01/2023

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_mot_de_passe_fruit` (
        `mtpfr_id` int(11) NOT NULL AUTO_INCREMENT,
        `mtpfr_nom` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`mtpfr_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO
    `c_mot_de_passe_fruit` (`mtpfr_nom`)
VALUES ('pomme'), ('poire'), ('mangue'), ('ananas'), ('kiwi'), ('figue'), ('framboise'), ('melon'), ('prune'), ('fraise'), ('cerise'), ('orange');

CREATE TABLE
    IF NOT EXISTS `c_access_proprietaire` (
        `accp_id` int(11) NOT NULL AUTO_INCREMENT,
        `mtpfr_id` varchar(120) DEFAULT NULL,
        `client_id` int(11) DEFAULT NULL,
        `accp_login` varchar(120) DEFAULT NULL,
        PRIMARY KEY (`accp_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE
    `c_access_proprietaire`
ADD
    CONSTRAINT `fk_c_client_c_access_proprietaire1` FOREIGN KEY (`client_id`) REFERENCES `c_client`(`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Mis à jour par Philipine le 31-01-2023

-- -----------------------------------------------------

-- Table `c_travail_effectue`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_travail_effectue` (
        `travail_id` INT NOT NULL AUTO_INCREMENT,
        `travail_date` DATE NULL,
        `travail_commentaire` TEXT NULL,
        `travail_duree` VARCHAR(50) NULL,
        `type_travail_id` INT NOT NULL,
        `cliLot_id` INT NULL,
        `dossier_id` INT(11) NULL,
        `client_id` INT(11) NULL,
        `util_id` INT(11) NOT NULL,
        `saisie_temps_id` INT NOT NULL,
        PRIMARY KEY (`travail_id`),
        CONSTRAINT `fk_c_travail_effectue_c_type_travail1` FOREIGN KEY (`type_travail_id`) REFERENCES `c_type_travail` (`type_travail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_travail_effectue_c_lot_client1` FOREIGN KEY (`cliLot_id`) REFERENCES `c_lot_client` (`cliLot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_travail_effectue_c_dossier1` FOREIGN KEY (`dossier_id`) REFERENCES `c_dossier` (`dossier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_travail_effectue_c_client1` FOREIGN KEY (`client_id`) REFERENCES `c_client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_travail_effectue_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_c_travail_effectue_c_type_saisie_temps1` FOREIGN KEY (`saisie_temps_id`) REFERENCES `c_type_saisie_temps` (`saisie_temps_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE = InnoDB;

-- -----------------------------------------------------

-- Mis à jour par Philipine le 02-02-2023

-- -----------------------------------------------------

ALTER TABLE
    `c_typelot` CHANGE `typelot_id` `typelot_id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO
    `c_typelot` (
        `typelot_id`,
        `typelot_libelle`
    )
VALUES (
        NULL,
        `Location meublée à l\'année`
    ), (NULL, 'Plus à bail');

INSERT INTO
    `c_gestionnaire` (
        `gestionnaire_id`,
        `gestionnaire_nom`,
        `gestionnaire_adresse1`,
        `gestionnaire_adresse2`,
        `gestionnaire_adresse3`,
        `gestionnaire_cp`,
        `gestionnaire_ville`,
        `gestionnaire_pays`,
        `gestionnaire_etat`,
        `gestionnaire_date_creation`,
        `gestionnaire_date_modification`
    )
VALUES (
        NULL,
        'Plus de gestionnaire car plus à bail',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        '1',
        '2023-02-02',
        CURRENT_TIMESTAMP
    );

-- -----------------------------------------------------

-- Mis à jour par Philipine le 10-02-2023

-- -----------------------------------------------------

ALTER TABLE `c_type_travail` ADD `type_travail_etat` INT DEFAULT 1;

-- -----------------------------------------------------

-- Mis à jour par Tsilavina le 20-02-2023

-- -----------------------------------------------------

DROP TABLE IF EXISTS `c_message_accueil`;

CREATE TABLE
    IF NOT EXISTS `c_message_accueil` (
        `mess_id` INT(11) NOT NULL AUTO_INCREMENT,
        `message` TEXT NOT NULL,
        PRIMARY KEY (`mess_id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARSET = utf8;

INSERT INTO
    `c_message_accueil` (`mess_id`, `message`)
VALUES (
        1,
        'Bienvenu sur le portail CELAVigestion.\r\n\r\nVous allez pouvoir gérer les informations de vos biens immobiliers.\r\nUtilisez le menu en haut pour renseigner toutes vos informations.\r\n\r\nBonne Utilisation!'
    );

-- -----------------------------------------------------

-- Mis à jour par Stenny le 20-02-2023

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_var_capee` FLOAT DEFAULT 0;

ALTER TABLE `c_bail` ADD `bail_var_plafonnee` FLOAT DEFAULT 0;

ALTER TABLE `c_bail` ADD `bail_var_appliquee` FLOAT DEFAULT 0;

ALTER TABLE `c_client`
ADD
    `cin` VARCHAR(255) NULL DEFAULT NULL AFTER `etat_client`;

ALTER TABLE `c_client`
ADD
    `etat_cin` INT NOT NULL DEFAULT '0' AFTER `cin`;

INSERT INTO
    `c_periode_annee` (
        `periode_annee_id`,
        `periode_annee_libelle`
    )
VALUES ('4', '2024'), ('5', '2025');

-- -----------------------------------------------------

-- Mis à jour par Stenny le 24-02-2023

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_avenant` INT DEFAULT 0;

ALTER TABLE `c_bail` ADD `bail_avenant_id_origin` INT DEFAULT NULL;

-- -----------------------------------------------------

-- Mis à jour par Tsilavina le 03-03-2023

-- -----------------------------------------------------

CREATE TABLE
    `c_tache` (
        `tache_id` int(11) NOT NULL AUTO_INCREMENT,
        `tache_nom` varchar(120) DEFAULT NULL,
        `tache_date_creation` datetime DEFAULT NULL,
        `tache_date_echeance` datetime DEFAULT NULL,
        `tache_date_validationn` datetime NOT NULL,
        `tache_prioritaire` varchar(45) DEFAULT NULL,
        `tache_texte` text DEFAULT NULL,
        `tache_realise` varchar(11) NOT NULL,
        `tache_valide` int(11) NOT NULL,
        `tache_createur` varchar(250) NOT NULL,
        `prospect_id` int(11) DEFAULT NULL,
        `client_id` int(11) DEFAULT NULL,
        `prospect_id_associe` varchar(250) DEFAULT NULL,
        `client_id_associe` varchar(255) DEFAULT NULL,
        `tache_etat` int(11) NOT NULL,
        `type_tache_id` int(11) NOT NULL,
        `theme_id` int(11) NOT NULL,
        `prospect_participant` varchar(10) DEFAULT NULL,
        `participant` varchar(120) DEFAULT NULL,
        `client_participant` varchar(255) DEFAULT NULL,
    ) ENGINE = InnoDB DEFAULT CHARSET = latin1;

ALTER TABLE `c_tache`
ADD PRIMARY KEY (`tache_id`),
ADD
    KEY `fk_c_tache_c_prospect1` (`prospect_id`),
ADD
    KEY `fk_c_tache_c_theme1` (`theme_id`);

ALTER TABLE
    `c_lot_client` CHANGE `typologielot_id` `typologielot_id` INT(11) NOT NULL DEFAULT '1';

-- Mis à jour par Stenny le 02-03-2023

-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_util_id_cloture` INT DEFAULT NULL;

-- -----------------------------------------------------

-- Mis à jour par Tsilavina le 02-03-2023

-- -----------------------------------------------------

CREATE TABLE
    `c_syndicat` (
        `syndicat_id` int(11) NOT NULL,
        `syndicat_nom` varchar(120) DEFAULT NULL,
        `syndicat_adresse1` varchar(120) DEFAULT NULL,
        `syndicat_adresse2` varchar(120) DEFAULT NULL,
        `syndicat_adresse3` varchar(120) DEFAULT NULL,
        `syndicat_cp` varchar(45) DEFAULT NULL,
        `syndicat_ville` varchar(120) DEFAULT NULL,
        `syndicat_pays` varchar(120) DEFAULT NULL,
        `syndicat_etat` int(11) NOT NULL DEFAULT 1,
        `syndicat_date_creation` date NOT NULL,
        `syndicat_date_modification` timestamp NOT NULL DEFAULT current_timestamp()
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE `c_syndicat` ADD PRIMARY KEY (`syndicat_id`);

ALTER TABLE
    `c_syndicat` CHANGE `syndicat_id` `syndicat_id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE
    `c_type_contact_syndicat` (
        `type_cont_id` int(11) NOT NULL,
        `type_cont_libelle` varchar(50) NOT NULL
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

ALTER TABLE
    `c_type_contact_syndicat`
ADD
    PRIMARY KEY (`type_cont_id`);

ALTER TABLE
    `c_type_contact_syndicat` CHANGE `type_cont_id` `type_cont_id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO
    `c_type_contact_syndicat` (
        `type_cont_id`,
        `type_cont_libelle`
    )
VALUES (1, 'standard'), (2, 'comptabilité'), (3, 'direction'), (4, 'propriétaires');

CREATE TABLE
    `c_contactsyndicat` (
        `cnctSyndicat_id` int(11) NOT NULL,
        `cnctSyndicat_nom` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_prenom` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_fonction` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_service` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_tel1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_tel2` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_email1` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `cnctSyndicat_email2` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
        `etat_contact` int(11) NOT NULL DEFAULT 1,
        `syndicat_id` int(11) NOT NULL DEFAULT 2,
        `type_cont_id` int(11) NOT NULL DEFAULT 1
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE
    `c_contactsyndicat`
ADD
    PRIMARY KEY (`cnctSyndicat_id`),
ADD
    KEY `fk_c_contactSyndicat_c_syndicat1` (`syndicat_id`),
ADD
    KEY `fk_c_utilisateur_c_type_contact_sindicat1` (`type_cont_id`);

ALTER TABLE
    `c_contactsyndicat` CHANGE `cnctSyndicat_id` `cnctSyndicat_id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO
    `c_syndicat` (
        `syndicat_id`,
        `syndicat_nom`,
        `syndicat_adresse1`,
        `syndicat_adresse2`,
        `syndicat_adresse3`,
        `syndicat_cp`,
        `syndicat_ville`,
        `syndicat_pays`,
        `syndicat_date_creation`,
        `syndicat_date_modification`,
        `syndicat_etat`
    )
VALUES (
        1,
        'NOM GESTIONNAIRE',
        NULL,
        NULL,
        NULL,
        'ADRESSE',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        0
    ), (
        2,
        'ACCELIS GESTION',
        '',
        '',
        '',
        '77159',
        'FERRIERES EN BRIE',
        '',
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        3,
        'ADAGIO (GROUPE PIERRE&VACANCES)',
        NULL,
        NULL,
        NULL,
        '49395',
        'SAUMUR',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        4,
        'ADONIS HONFLEUR',
        NULL,
        NULL,
        NULL,
        '76595',
        'LE HAVRE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        5,
        'ADONIS HOTELS & RESIDENCES',
        NULL,
        NULL,
        NULL,
        '83495',
        'La Seyne Sur Mer',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        6,
        'AJC IMMOBILIER',
        NULL,
        NULL,
        NULL,
        '13205',
        'SAINT REMY DE PROVENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        7,
        'ALPVISION RESIDENCES GESTION',
        NULL,
        NULL,
        NULL,
        '74161',
        'SAINT-JULIEN-EN-GENEVOIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        8,
        'ALTITUDE SARL',
        NULL,
        NULL,
        NULL,
        '38405',
        'SAINT MARTIN D URIAGE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        9,
        'APPART CITY',
        NULL,
        NULL,
        NULL,
        '34072',
        'MONTPELLIER CEDEX 3',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        10,
        'AQUITAINE PROMOTION',
        NULL,
        NULL,
        NULL,
        '33395',
        'TALENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        11,
        'ARREAU RESIDENCE TOURISME',
        NULL,
        NULL,
        NULL,
        '65235',
        'ARREAU',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        12,
        'ARTES TOURSIME',
        NULL,
        NULL,
        NULL,
        '58995',
        'LILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        13,
        'AUVENCE',
        NULL,
        NULL,
        NULL,
        '33695',
        'MERIGNAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        14,
        'AVECOEUR',
        NULL,
        NULL,
        NULL,
        '26125',
        'SAINT RESTITUT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        15,
        'BELLEME LEISURE LIMITED',
        NULL,
        NULL,
        NULL,
        '61125',
        'BELLEME',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        16,
        'BNP PARIBAS IMMOBILIER RESIDENTIEL',
        NULL,
        NULL,
        NULL,
        '92862',
        'ISSY LES MOULINEAUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        17,
        'BOIVERT ET PARAYRE',
        NULL,
        NULL,
        NULL,
        '33995',
        'MONTPELLIER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        18,
        'C EST LA VIE MANDAT DE GESTION',
        NULL,
        NULL,
        NULL,
        '34495',
        'BEZIERS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        19,
        'CAMIOLE RESORTSN',
        NULL,
        NULL,
        NULL,
        '83435',
        'TOURRETTES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        20,
        'CAP WEST CARQUEFOU BELLE ETOILE 1',
        NULL,
        NULL,
        NULL,
        '44235',
        'LA CHAPELLE SUR ERDRE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        21,
        'CAPIVAL GESTION',
        NULL,
        NULL,
        NULL,
        '68998',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        22,
        'CERS',
        NULL,
        NULL,
        NULL,
        '75111',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        23,
        'CHASTAGNOL IMMOBILIER',
        NULL,
        NULL,
        NULL,
        '38405',
        'CHAMROUSSE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        24,
        'CHATEAU DE LA FONTAINE AUX COSSONS',
        NULL,
        NULL,
        NULL,
        '91635',
        'VAUGRIGNEUSE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        25,
        'CHATEAUFORM',
        NULL,
        NULL,
        NULL,
        '95335',
        'PERSAN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        26,
        'CIMES ET NEIGE IMMOBILIER',
        NULL,
        NULL,
        NULL,
        '5285',
        'PUY SAINT VINCENT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        27,
        'CITYLODGE DU CAMPUS DE NIORT',
        NULL,
        NULL,
        NULL,
        '31115',
        'ROQUETTES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        28,
        'CLEAN VOLABEE',
        NULL,
        NULL,
        NULL,
        '6395',
        'CANNES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        29,
        'COLBERTVAL EXPLOITATION PIERREVAL',
        NULL,
        NULL,
        NULL,
        '86955',
        'FUTUROSCOPE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        30,
        'COLISEE',
        NULL,
        NULL,
        NULL,
        '33065',
        'BORDEAUX CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        31,
        'COMADIM RESIDENCES SERVICES',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        32,
        'DECLIC IMMOBILIER',
        NULL,
        NULL,
        NULL,
        '34425',
        'SAINT JEAN DE VEDAS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        33,
        'DELOS SARL',
        NULL,
        NULL,
        NULL,
        '31325',
        'MERVILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        34,
        'DG HOLIDAYS',
        NULL,
        NULL,
        NULL,
        '75008',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        35,
        'DG NOVA PARK',
        NULL,
        NULL,
        NULL,
        '75008',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        36,
        'DIRECT GESTION SA',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        37,
        'DIVERS AIO',
        NULL,
        NULL,
        NULL,
        '69001',
        'LYON 6EME ARRONDISSEMENT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        38,
        'DIVERS AIO2',
        NULL,
        NULL,
        NULL,
        '69001',
        'LYON 6EME ARRONDISSEMENT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        39,
        'DIVERS CIF',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS 8EME ARRONDISSEMENT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        40,
        'DOMIDEP',
        NULL,
        NULL,
        NULL,
        '38295',
        'BOURGOIN JALLIEU',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        41,
        'DOMITEAM',
        NULL,
        NULL,
        NULL,
        '34195',
        'SETE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        42,
        'DOMITYS',
        NULL,
        NULL,
        NULL,
        '75111',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        43,
        'DOMUSVI',
        NULL,
        NULL,
        NULL,
        '92145',
        'SURESNES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        44,
        'DOMUSVI ( LES HAUTS DE MENTON)',
        NULL,
        NULL,
        NULL,
        '74365',
        'METZ TESSY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        45,
        'DOMUSVI (JARDINS MEDICIS)',
        NULL,
        NULL,
        NULL,
        '78245',
        'MEZY SUR SEINE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        46,
        'DREISKI GESTION IMMOBILIERE',
        NULL,
        NULL,
        NULL,
        '24095',
        'BERGERAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        47,
        'EAGLE MANAGEMENT',
        NULL,
        NULL,
        NULL,
        '61125',
        'BELLEME',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        48,
        'ECOGEST',
        NULL,
        NULL,
        NULL,
        '69001',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        49,
        'EHPAD JARDINS DE FANTON',
        NULL,
        NULL,
        NULL,
        '13816',
        'LA PENNE SUR HUVEAUNE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        50,
        'ELC MIEUX VIVRE',
        NULL,
        NULL,
        NULL,
        '33875',
        'CAMBES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        51,
        'ELEVATION ALPS',
        NULL,
        NULL,
        NULL,
        '74105',
        'MORZINE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        52,
        'EMANROSE',
        NULL,
        NULL,
        NULL,
        '83495',
        'LA SEYNE SUR MER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        53,
        'EMERA',
        NULL,
        NULL,
        NULL,
        '11295',
        'LIMOUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        54,
        'ESPACE2 VACANCES',
        NULL,
        NULL,
        NULL,
        '75111',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        55,
        'EURO FINANCE IMMOBILIER',
        NULL,
        NULL,
        NULL,
        '20195',
        'BASTIA',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        56,
        'EUROGROUP',
        NULL,
        NULL,
        NULL,
        '72999',
        'CHAMBERY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        57,
        'FLAINE EXPLOITATION',
        NULL,
        NULL,
        NULL,
        '72999',
        'CHAMBERY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        58,
        'FLOREA SAINT ESPRIT SARL',
        NULL,
        NULL,
        NULL,
        '33295',
        'BORDEAUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        59,
        'FONCIA DOMITIA',
        NULL,
        NULL,
        NULL,
        '34962',
        'MONTPELLIER CEDEX 2',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        60,
        'GALPIGES ET ASSOCIES SARL',
        NULL,
        NULL,
        NULL,
        '75001',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        61,
        'GARDEN CITY',
        NULL,
        NULL,
        NULL,
        '13001',
        'MARSEILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        62,
        'GDP VENDOME',
        NULL,
        NULL,
        NULL,
        '74003',
        'ANNECY CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        63,
        'GENEVA RESIDENCE',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        64,
        'GESTION ET CAPITAL (GESCAP)',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        65,
        'GESTLAC',
        NULL,
        NULL,
        NULL,
        '33603',
        'PESSAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        66,
        'GHB GROUPE HOTELIER BATAILLE',
        NULL,
        NULL,
        NULL,
        '83495',
        'LA SEYNE SUR MER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        67,
        'GLOBAL EXPLOITATION',
        NULL,
        NULL,
        NULL,
        '34065',
        'MONTPELLIER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        68,
        'GOELIA',
        NULL,
        NULL,
        NULL,
        '91037',
        'EVRY CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        69,
        'GRAND BLEU',
        NULL,
        NULL,
        NULL,
        '66750',
        'SAINT CYPRIEN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        70,
        'GROUPE HMC',
        NULL,
        NULL,
        NULL,
        '64595',
        'ANGLET',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        71,
        'HABITAT GESTION',
        NULL,
        NULL,
        NULL,
        '54405',
        'NANCY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        72,
        'HDS SAINT RAPHAEL',
        NULL,
        NULL,
        NULL,
        '13095',
        'AIX EN PROVENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        73,
        'HOT JASS',
        NULL,
        NULL,
        NULL,
        '48125',
        'AUMONT AUBRAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        74,
        'HOTEL DU GOLF DE FONTCAUDE JUVIGNAC (FILIALE PROME',
        NULL,
        NULL,
        NULL,
        '34195',
        'SETE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        75,
        'HOTEL V.H.S.',
        NULL,
        NULL,
        NULL,
        '6395',
        'CANNES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        76,
        'HOTELIERE BIBLIOTHEQUE',
        NULL,
        NULL,
        NULL,
        '75008',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        77,
        'HOTELS DE PARIS',
        NULL,
        NULL,
        NULL,
        '75011',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        78,
        'HOTESIA VTF',
        NULL,
        NULL,
        NULL,
        '13085',
        'AIX EN PROVENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        79,
        'ICADE RESIDENCES SERVICES',
        NULL,
        NULL,
        NULL,
        '75014',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        80,
        'INTERHOME',
        NULL,
        NULL,
        NULL,
        '8147',
        'LATTBRUGG',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        81,
        'ISIS',
        NULL,
        NULL,
        NULL,
        '37995',
        'GRENOBLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        82,
        'JPR GESTION COTE AZUR',
        NULL,
        NULL,
        NULL,
        '5995',
        'NICE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        83,
        'KORIAN',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        84,
        'KORIAN CHATEAU DE LORMOY',
        NULL,
        NULL,
        NULL,
        '25865',
        'GENEUILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        85,
        'KORIAN LE GATINAIS',
        NULL,
        NULL,
        NULL,
        '91715',
        'MAISSE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        86,
        'LA GIRANDIERE (GROUPE RESIDE ETUDES)',
        NULL,
        NULL,
        NULL,
        '75003',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        87,
        'LA SAISONNERAIE',
        NULL,
        NULL,
        NULL,
        '26495',
        'BOURG LES VALENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        88,
        'LA VILLA VICTOR HUGO',
        NULL,
        NULL,
        NULL,
        '71195',
        'LE CREUSOT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        89,
        'LAFAMILLE IMMO',
        NULL,
        NULL,
        NULL,
        '6315',
        'CAP D AIL',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        90,
        'LE CLOS DES VIGNES (DOMUSVI)',
        NULL,
        NULL,
        NULL,
        '5995',
        'GRASSE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        91,
        'LE MIRE',
        NULL,
        NULL,
        NULL,
        '44595',
        'SAINT NAZAIRE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        92,
        'LE VILLAGE DES HAMEAUX DE CAMARGUE',
        NULL,
        NULL,
        NULL,
        '13195',
        'ARLES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        93,
        'LES CORALIES',
        NULL,
        NULL,
        NULL,
        '38455',
        'CHOZEAU',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        94,
        'LES COTTAGES DU LAC',
        NULL,
        NULL,
        NULL,
        '29295',
        'QUIMPERLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        95,
        'LES CYPRES',
        NULL,
        NULL,
        NULL,
        '30395',
        'VILLENEUVE LES AVIGNON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        96,
        'LES JARDINS D ARCADIE EXPLOITATION',
        NULL,
        NULL,
        NULL,
        '68998',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        97,
        'LES JARDINS D OLY',
        NULL,
        NULL,
        NULL,
        '31315',
        'AUZEVILLE TOLOSANE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        98,
        'LES JARDINS DE CYBELE',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        99,
        'LES JARDINS DE SORMIOU',
        NULL,
        NULL,
        NULL,
        '13004',
        'MARSEILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        100,
        'LES PEROLINES',
        NULL,
        NULL,
        NULL,
        '38485',
        'SAINT ANDRE LE GAZ',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        101,
        'LES PRIVILODGES APPARTHOTELS',
        NULL,
        NULL,
        NULL,
        '64 00',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        102,
        'LOCABED',
        NULL,
        NULL,
        NULL,
        '33995',
        'MONTPELLIER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        103,
        'LOCATION DE VACANCES A LA SEMAINE',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        104,
        'LOCATION MEUBLEE A L ANNEE',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        105,
        'LOGEST IMMO',
        NULL,
        NULL,
        NULL,
        '66745',
        'SAINT CYPRIEN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        106,
        'LRPO',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        107,
        'MAEVA GESTION',
        NULL,
        NULL,
        NULL,
        '75942',
        'PARIS CEDEX 19',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        108,
        'MANDELIEU RESORTS',
        NULL,
        NULL,
        NULL,
        '6405',
        'BIOT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        109,
        'MAXIMM HOME',
        NULL,
        NULL,
        NULL,
        '6185',
        'ROQUEBRUNE CAP MARTIN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        110,
        'MER & GOLF',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        111,
        'MMV',
        NULL,
        NULL,
        NULL,
        '6695',
        'ST LAURENT DU VAR',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        112,
        'NEHO',
        NULL,
        NULL,
        NULL,
        '6195',
        'NICE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        113,
        'NEMEA APPART ETUDES',
        NULL,
        NULL,
        NULL,
        '33695',
        'MERIGNAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        114,
        'NEWCO APPART IVRY',
        NULL,
        NULL,
        NULL,
        '86955',
        'FUTUROSCOPE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        115,
        'NEXITY',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        116,
        'NEXITY STUDEA',
        NULL,
        NULL,
        NULL,
        '75796',
        'PARIS CEDEX 08',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        117,
        'NOBLE AGE',
        NULL,
        NULL,
        NULL,
        '44119',
        'VERTOU',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        118,
        'NORMANDY COUNTRY CLUB',
        NULL,
        NULL,
        NULL,
        '75008',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        119,
        'ODALYS',
        NULL,
        NULL,
        NULL,
        '',
        'CALVIS SON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        120,
        'ODALYS CITY',
        NULL,
        NULL,
        NULL,
        '92095',
        'BOULOGNE BILLANCOURT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        121,
        'ODALYS I ROUS',
        NULL,
        NULL,
        NULL,
        '13586',
        'AIX EN PROVENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        122,
        'ODALYS ML LAVILLE',
        NULL,
        NULL,
        NULL,
        '13586',
        'AIX EN PROVENCE CEDEX 3',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        123,
        'ODALYS V ETASSE',
        NULL,
        NULL,
        NULL,
        '13586',
        'AIX EN PROVENCE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        124,
        'ODELIA',
        NULL,
        NULL,
        NULL,
        '69418',
        'LYON CEDEX 3',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        125,
        'OFFICE HOTELIER DU LOGEMENT ETUDIANT LE SAINT MAND',
        NULL,
        NULL,
        NULL,
        '69002',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        126,
        'OHLE LE PRADO',
        NULL,
        NULL,
        NULL,
        '69344',
        'LYON CEDEX 07',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        127,
        'ORPEA',
        NULL,
        NULL,
        NULL,
        '92801',
        'PUTEAUX CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        128,
        'PIERRE ET VACANCES',
        NULL,
        NULL,
        NULL,
        '75942',
        'PARIS CEDEX 19',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        129,
        'PIERREVAL',
        NULL,
        NULL,
        NULL,
        '22185',
        'PLERIN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        130,
        'PRIVILEGE HOTELS ET RESORTS',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        131,
        'PROVENCIA GESTION',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        132,
        'PV-CP CITY',
        NULL,
        NULL,
        NULL,
        '75942',
        'PARIS CEDEX 9',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        133,
        'RESIDENCE ACADEMIE MONTPELIIER',
        NULL,
        NULL,
        NULL,
        '34735',
        'VENDARGUES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        134,
        'RESIDENCE ANDRESY',
        NULL,
        NULL,
        NULL,
        '78565',
        'ANDRESY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        135,
        'RESIDENCE LES PINS (KORIAN)',
        NULL,
        NULL,
        NULL,
        '11095',
        'NARBONNE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        136,
        'RESIDENCE SENIOR TOULOUSE TIBAOUS SARL',
        NULL,
        NULL,
        NULL,
        '31645',
        'SAINT ORENS DE GAMEVILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        137,
        'RESIDENCE SERVICE GESTION',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        138,
        'RESIDENCE SERVICE GESTION',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        139,
        'RESIDENCES EPERNAY SARL',
        NULL,
        NULL,
        NULL,
        '82295',
        'CAUSSADE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        140,
        'RESIDENCES SERVICES GESTION',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        141,
        'RESIDHOTEL',
        NULL,
        NULL,
        NULL,
        '6245',
        'MOUGINS CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        142,
        'RESIDIS',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        143,
        'RESITEL GROUPE LAGRANGE',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        144,
        'SARAH KAVANAGH VILLAS AND APARTMENTS',
        NULL,
        NULL,
        NULL,
        '1 44',
        'NICE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        145,
        'SARL CHATEAU DES GIPIERES',
        NULL,
        NULL,
        NULL,
        '26565',
        'MONTBRUN LES BAINS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        146,
        'SARL GESTION DE RESIDENCES DE TOURISME DEAUVILLE',
        NULL,
        NULL,
        NULL,
        '14795',
        'DEAUVILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        147,
        'SARL LA MAISON DU PAYS DE RAUZAN',
        NULL,
        NULL,
        NULL,
        '33415',
        'RAUZAN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        148,
        'SARL LES AURELIADES',
        NULL,
        NULL,
        NULL,
        '44345',
        'GUERANDE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        149,
        'SARL PONT AVEN GESTION SENIOR',
        NULL,
        NULL,
        NULL,
        '75003',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        150,
        'SARL RESIDENCE DES COTEAUX',
        NULL,
        NULL,
        NULL,
        '33295',
        'BORDEAUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        151,
        'SARL SDP',
        NULL,
        NULL,
        NULL,
        '73525',
        'SAINT SORLIN D ARVES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        152,
        'SARL VAL SENART',
        NULL,
        NULL,
        NULL,
        '6695',
        'SAINT LAURENT DU VAR',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        153,
        'SAS BALCONS DE RECOIN',
        NULL,
        NULL,
        NULL,
        '58525',
        'DORNECY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        154,
        'SAS CHAMP DE L ORMEAU',
        NULL,
        NULL,
        NULL,
        '72695',
        'ROUILLON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        155,
        'SAS CHARLES D ORLEANS',
        NULL,
        NULL,
        NULL,
        '16095',
        'COGNAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        156,
        'SAS CHATEAU DE LA MANDERIE',
        NULL,
        NULL,
        NULL,
        '45285',
        'OUZOUER DES CHAMPS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        157,
        'SAS FINANCIERE SANTE',
        NULL,
        NULL,
        NULL,
        '33295',
        'BORDEAUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        158,
        'SAS GUSTAVE COURBET',
        NULL,
        NULL,
        NULL,
        '14235',
        'CAUMONT L EVENTE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        159,
        'SAS HAMEAU DE VALLOIRE',
        NULL,
        NULL,
        NULL,
        '69105',
        'SAINTE FOY LES LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        160,
        'SAS L OMBRIERE',
        NULL,
        NULL,
        NULL,
        '17615',
        'ST JEAN D ANGLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        161,
        'SAS LA CADIERE',
        NULL,
        NULL,
        NULL,
        '13695',
        'MARIGNANE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        162,
        'SAS LA PIERRE MEULIERE',
        NULL,
        NULL,
        NULL,
        '86205',
        'VOUNEUIL SUR SEINE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        163,
        'SAS LA SAPINIERE',
        NULL,
        NULL,
        NULL,
        '10125',
        'AUXON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        164,
        'SAS LA SAVANE',
        NULL,
        NULL,
        NULL,
        '33465',
        'GUJAN MESTRAS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        165,
        'SAS LCDH',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        166,
        'SAS LE VAL DE SERRES',
        NULL,
        NULL,
        NULL,
        '5695',
        'SERRES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        167,
        'SAS LES DEMEURES CHAMPENOISES',
        NULL,
        NULL,
        NULL,
        '51195',
        'EPERNAY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        168,
        'SAS LES DEMEURES DU VENTOUX',
        NULL,
        NULL,
        NULL,
        '84805',
        'AUBIGNAN',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        169,
        'SAS LES JONQUILLES',
        NULL,
        NULL,
        NULL,
        '13008',
        'MARSEILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        170,
        'SAS MAMA SHELTER',
        NULL,
        NULL,
        NULL,
        '75015',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        171,
        'SAS ORELLE AVENIR',
        NULL,
        NULL,
        NULL,
        '25635',
        'ROULANS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        172,
        'SAS PAUL VALERY',
        NULL,
        NULL,
        NULL,
        '62995',
        'CLERMONT FERRAND',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        173,
        'SAS PROPRIETAIRES DOMAINE DU GOLF FABREGUES',
        NULL,
        NULL,
        NULL,
        '34685',
        'FABREGUES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        174,
        'SAS RESIDENCE DES IMPRESSIONNISTES',
        NULL,
        NULL,
        NULL,
        '51495',
        'VILLE EN SELVE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        175,
        'SAS RESIDENCE DU TERTRE',
        NULL,
        NULL,
        NULL,
        '33121',
        'FRONSAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        176,
        'SAS RESIDENCE LOUIS IX',
        NULL,
        NULL,
        NULL,
        '47305',
        'LAMONTJOIE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        177,
        'SAS VACANTEL',
        NULL,
        NULL,
        NULL,
        '74997',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        178,
        'SAS VAL FLEURY',
        NULL,
        NULL,
        NULL,
        '41395',
        'SAINT GEORGES SUR CHER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        179,
        'SAS VILLA SYLVIA',
        NULL,
        NULL,
        NULL,
        '95327',
        'DOMONT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        180,
        'SAS ZENITUDE MERIGNAC',
        NULL,
        NULL,
        NULL,
        '33695',
        'MERIGNAC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        181,
        'SASU HOTEL DU BAOU',
        NULL,
        NULL,
        NULL,
        '6605',
        'LA GAUDE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        182,
        'SASU VACANCES BLEUES DIVONNE',
        NULL,
        NULL,
        NULL,
        '13001',
        'MARSEILLE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        183,
        'SEM PROMODEV',
        NULL,
        NULL,
        NULL,
        '65415',
        'VAL LOURON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        184,
        'SERBC SARL',
        NULL,
        NULL,
        NULL,
        '66815',
        'VERNET LES BAINS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        185,
        'SERIFOS',
        NULL,
        NULL,
        NULL,
        '34495',
        'BEZIERS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        186,
        'SERSE SAS',
        NULL,
        NULL,
        NULL,
        '59442',
        'WASQUEHAL',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        187,
        'SGIT',
        NULL,
        NULL,
        NULL,
        '13794',
        'AIX EN PROVENCE CEDEX 3',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        188,
        'SMAS GROUPE LAGRANGE',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        189,
        'SNC BORDEAUX BONNAC',
        NULL,
        NULL,
        NULL,
        '32995',
        'BORDEAUX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        190,
        'SOCIETE DEXPLOITATION RESIDENCE IMPERATRICE EUGENI',
        NULL,
        NULL,
        NULL,
        '44095',
        'NANTES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        191,
        'SOCIETE HOTELIERE DU LOGEMENT ETUDIANT(SHLE)',
        NULL,
        NULL,
        NULL,
        '69344',
        'LYON CEDEX 07',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        192,
        'SOCIETE LES BAINS',
        NULL,
        NULL,
        NULL,
        '7125',
        'SAINT PERAY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        193,
        'SOCIETE SAINT AGNANT',
        NULL,
        NULL,
        NULL,
        '17615',
        'SAINT AGNANT',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        194,
        'SODEREV GROUPE LAGRANGE',
        NULL,
        NULL,
        NULL,
        '75012',
        'PARIS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        195,
        'SOLEIL ET VACANCES',
        NULL,
        NULL,
        NULL,
        '30595',
        'VAUVET',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        196,
        'SOLORECAR (CERISE)',
        NULL,
        NULL,
        NULL,
        '92495',
        'RUEIL-MALMAISON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        197,
        'SOLORES',
        NULL,
        NULL,
        NULL,
        '92495',
        'RUEIL MALMAISON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        198,
        'SOLORESSONNE - EXHORE',
        NULL,
        NULL,
        NULL,
        '92495',
        'RUEIL MALMAISON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        199,
        'STELLA MANAGEMENT',
        NULL,
        NULL,
        NULL,
        '92295',
        'LEVALLOIS PERRET',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        200,
        'STEVA',
        NULL,
        NULL,
        NULL,
        '92115',
        'MONTROUGE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        201,
        'STUDI HOME',
        NULL,
        NULL,
        NULL,
        '91295',
        'MASSY',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        202,
        'SUN HOLIDAYS AVIGNON LTD',
        NULL,
        NULL,
        NULL,
        '',
        'BT92 COUNTY FERMANAGH NORTHERN IRELAND',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        203,
        'TAGERIM 66',
        NULL,
        NULL,
        NULL,
        '66745',
        'SAINT CYPRIEN SUD',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        204,
        'TDF BREST',
        NULL,
        NULL,
        NULL,
        '37115',
        'CHAVEIGNES',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        205,
        'TERRE HABITAT',
        NULL,
        NULL,
        NULL,
        '83265',
        'SAINT CYR SUR MER',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        206,
        'TERRES DE FRANCE',
        NULL,
        NULL,
        NULL,
        '37535',
        'Saint-Cyr-sur-Loire',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        207,
        'VACANCES MONT BLANC',
        NULL,
        NULL,
        NULL,
        '74165',
        'SAINT GERVAIS LES BAINS',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        208,
        'VALORITY ETUDES',
        NULL,
        NULL,
        NULL,
        '69001',
        'LYON',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        209,
        'VENEFRANCE',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        210,
        'VHS',
        NULL,
        NULL,
        NULL,
        '',
        '',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        211,
        'VICTORY GESTION',
        NULL,
        NULL,
        NULL,
        '38695',
        'CORENC',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        212,
        'VILLAGE CENTER GESTION',
        NULL,
        NULL,
        NULL,
        '34196',
        'SETE CEDEX',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        213,
        'VILLAGE LE HAMEAU DE NEPTUNE',
        NULL,
        NULL,
        NULL,
        '34275',
        'LA GRANDE MOTTE',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        214,
        'ZENITUDE HOTEL RESIDENCES',
        NULL,
        NULL,
        NULL,
        '67295',
        'SCHILTIGHEIM',
        NULL,
        NULL,
        '2023-02-02 14:26:24',
        1
    ), (
        215,
        'MIMI',
        'ALLO',
        'ALLO',
        'ALLO',
        '111',
        'ALLO',
        'ALLO',
        '2023-01-10 09:47:35',
        '2023-02-02 14:26:24',
        0
    ), (
        216,
        'MAMA',
        'MAMAM',
        'MAMAM',
        'MAZMAZM',
        '201',
        'ZZEZE',
        'ZAEZEZE',
        '2023-01-13 08:50:50',
        '2023-02-02 14:26:24',
        0
    ), (
        217,
        'MAMA',
        'DFSDKJ',
        'KFDJSKJ',
        'SEJFKLDSJ',
        '212',
        'DSFS',
        'FDFDSDS',
        '2023-01-13 08:52:33',
        '2023-02-02 14:26:24',
        0
    ), (
        218,
        'MIMI',
        'SQDQS',
        'SQDSQ',
        'SQDSQ',
        'QSDSQ',
        'QSDSQ',
        'SQDSQDSQD',
        '2023-01-13 08:54:43',
        '2023-02-02 14:26:24',
        0
    ), (
        219,
        'MOMO',
        'FDSZKF',
        'HG',
        'GHJ',
        'G',
        'GJG',
        'GHF',
        '2023-01-13 08:55:29',
        '2023-02-02 14:26:24',
        0
    ), (
        220,
        '788',
        '787',
        '987',
        '987',
        '897',
        '897',
        '89798',
        '2023-01-13 08:57:48',
        '2023-02-02 14:26:24',
        1
    ), (
        221,
        'Plus de gestionnaire car plus à bail',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        '2023-02-02 00:00:00',
        '2023-02-02 14:26:33',
        1
    ), (
        222,
        'Plus de gestionnaire car plus à bail',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        '2023-02-02 00:00:00',
        '2023-02-16 05:46:49',
        1
    );

ALTER TABLE `c_lot_client`
ADD
    `syndicat_id` INT NOT NULL AFTER `gestionnaire_id`;

ALTER TABLE c_lot_client
ADD
    FOREIGN KEY (syndicat_id) REFERENCES c_syndicat(syndicat_id);

-- -----------------------------------------------------

-- Mis à jour par Stenny le 06-03-2023

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_remind_indexation` (
        `remind_id` int(11) NOT NULL AUTO_INCREMENT,
        `remind_annee` varchar(120) DEFAULT NULL,
        `remind_trimestre` int(11) DEFAULT NULL,
        `remind_envoye` int(11) DEFAULT 0,
        PRIMARY KEY (`remind_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- -----------------------------------------------------

-- Mis à jour par Stenny le 07-03-2023

-- -----------------------------------------------------

ALTER TABLE `c_mandat`
ADD
    `mandat_date_signature` DATE NULL DEFAULT NULL AFTER `mandat_montant`;

-- -----------------------------------------------------

-- Mis à jour par Philipine le 07-03-2023

-- -----------------------------------------------------

-- Document Acte prospect

CREATE TABLE
    IF NOT EXISTS `c_document_prospect_acte` (
        `docp_acte_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `docp_acte_nom` varchar(120) DEFAULT NULL,
        `docp_acte_path` varchar(120) DEFAULT NULL,
        `docp_acte_creation` datetime DEFAULT NULL,
        `docp_acte_etat` int(11) NOT NULL,
        `lot_id` int(11) NOT NULL,
        `util_id` int(11) NOT NULL,
        PRIMARY KEY (`docp_acte_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = latin1;

-- Document Bail prospect

CREATE TABLE
    IF NOT EXISTS `c_document_prospect_bail` (
        `docp_bail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `docp_bail_nom` varchar(120) DEFAULT NULL,
        `docp_bail_path` varchar(120) DEFAULT NULL,
        `docp_bail_creation` datetime DEFAULT NULL,
        `docp_bail_etat` int(11) NOT NULL,
        `lot_id` int(11) NOT NULL,
        `util_id` int(11) NOT NULL,
        PRIMARY KEY (`docp_bail_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = latin1;

-- -----------------------------------------------------

-- Mise à jour par Tsilavina le 07-03-2023

-- -----------------------------------------------------

ALTER TABLE `c_prospect`
ADD
    `cin` VARCHAR(255) NULL DEFAULT NULL AFTER `prospect_after_id`;

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        NULL,
        "C\'est la Vie - Demande CNI",
        'Modèle de mail utilisé lors de l\'envoi d\'une demande de carte nationale d\'identité sur un prospect.',
        'C\'est la Vie - Demande d\'une Carte nationale d\'identité',
        'Bonjour, \r\n\r\nVeuillez nous communiquer une photo de votre carte nationale d\'identité (CNI).\r\n\r\nBien à vous. \r\n\r\nL\'Administration du CELAVigestion\r\n',
        '1',
        '1',
        '1',
        NULL
    );

-- -----------------------------------------------------

-- Mis à jour par Stenny le 09-03-2023

-- -----------------------------------------------------

ALTER TABLE `c_mandat_client`
ADD
    `mandat_cli_date_signature` DATE NULL DEFAULT NULL AFTER `mandat_cli_montant`;

ALTER TABLE
    `c_lot_client` CHANGE `gestionnaire_id` `gestionnaire_id` INT(11) NULL;

-- -----------------------------------------------------

-- Mis à jour par Stenny le 10-03-2023

-- -----------------------------------------------------

ALTER TABLE
    `c_mandat_client` CHANGE `type_mandat_id` `type_mandat_id` INT(11) NULL;

-- -----------------------------------------------------

-- Mis à jour par Stenny le 13-03-2023

-- -----------------------------------------------------

ALTER TABLE `c_dossier` ADD `util_id` INT(11) NULL;

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        NULL,
        'C\'est la Vie - Demande CNI',
        'Modèle de mail utilisé lors de l\'envoi d\'une demande de carte nationale d\'identité sur un prospect.',
        'C\'est la Vie - Demande d\'une Carte nationale d\'identité',
        'Bonjour, \r\n\r\nVeuillez nous communiquer une photo de votre carte nationale d\'identité (CNI).\r\n\r\nBien à vous. \r\n\r\nL\'Administration du CELAVigestion\r\n',
        '1',
        '1',
        '1',
        NULL
    );

'
-- -----------------------------------------------------
-- Mise à jour par  Tsilavina le 13-03-2023
-- -----------------------------------------------------
UPDATE `c_periodicite_loyer` SET `pdl_description` = 'trimestrielle civile' WHERE `c_periodicite_loyer`.`pdl_id` = 2;
INSERT INTO `c_periodicite_loyer` (`pdl_id`, `pdl_description`, `pdl_valeur`) VALUES (NULL, 'trimestrielle décalée', '4');
ALTER TABLE `c_bail` ADD `premier_mois_trimes` INT NULL DEFAULT NULL AFTER `bail_util_id_cloture`;

-- -----------------------------------------------------
-- Mise à jour par  Tsilavina le 15-03-2023
-- -----------------------------------------------------

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (6, 'C\'est la Vie - Bienvenue', 'Modèle de mail utilisé lors de la transformation du prospects en client.', 'C\'est la Vie - Bienvenue', 'Bonjour, \r\n\r\nNous sommes ravis de vous compter parmi nos nouveaux clients.\r\n\r\nL\'équipe de CELAVigestion vous remercie.\r\n\r\nL\'Administration du CELAVigestion\r\n', '1', '1', '1', NULL);

-- -----------------------------------------------------
-- Mise à jour par  Philipine le 16-03-2023
-- -----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_date_debut_mission` DATE NULL AFTER `premier_mois_trimes`;
-- -----------------------------------------------------
-- Mise à jour par  Tsilavina le 16-03-2023
-- -----------------------------------------------------

INSERT INTO `c_page` (`page_id`, `page_nom`) VALUES ('6', 'facture_loyer_lot');
INSERT INTO `c_page` (`page_id`, `page_nom`) VALUES ('7', 'demande_cni_client');
INSERT INTO `c_page` (`page_id`, `page_nom`) VALUES ('8', 'demande_cni_prospect');

-- -----------------------------------------------------
-- Mise à jour par  Philipine le 17-03-2023
-- -----------------------------------------------------
ALTER TABLE `c_indice_valeur_loyer` ADD `indval_valide` INT NOT NULL DEFAULT '0' AFTER `indval_date_parution`;

ALTER TABLE `c_indice_loyer`  ADD `indloyer_periode` VARCHAR(250) NULL  AFTER `indloyer_description`,  ADD `indloyer_date_saisie_an_jour` VARCHAR(10) NULL  AFTER `indloyer_periode`,  ADD `indloyer_date_saisie_an_mois` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_an_jour`,  ADD `indloyer_date_saisie_t1_jour` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_an_mois`,  ADD `indloyer_date_saisie_t1_mois` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t1_jour`,  ADD `indloyer_date_saisie_t2_jour` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t1_mois`,  ADD `indloyer_date_saisie_t2_mois` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t2_jour`,  ADD `indloyer_date_saisie_t3_jour` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t2_mois`,  ADD `indloyer_date_saisie_t3_mois` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t3_jour`,  ADD `indloyer_date_saisie_t4_jour` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t3_mois`,  ADD `indloyer_date_saisie_t4_mois` VARCHAR(10) NULL  AFTER `indloyer_date_saisie_t4_jour`;

-- -----------------------------------------------------
-- Mise à jour par  Philipine le 22-03-2023
-- -----------------------------------------------------
ALTER TABLE `c_indice_valeur_loyer` ADD `util_prenom` VARCHAR(250) NULL AFTER `indval_valide`;
ALTER TABLE `c_indice_valeur_loyer` ADD `indval_date_registre` DATE NULL AFTER `util_prenom`;

ALTER TABLE `c_lot_client` CHANGE `cliLot_iban_client` `cliLot_iban_client` VARCHAR(33) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

UPDATE `c_param_modelemail` SET `pmail_contenu` = Madame et Monsieur @nom_client @prenom_client \r\n Adresse @adresse_client \r\n\r\n\r\n Le @date_envoi_mail\r\n\r\n\r\n\r\nChère Madame @nom_client @prenom_client, cher Monsieur @nom_client @prenom_client ,\r\n\r\n\r\nToute l’équipe de CELAVIGESTION tient à vous remercier de la confiance que vous lui témoignez. Vous avez fait le choix de nous confier la gestion de votre bien en location meublée, c’est le choix de la sureté et sérénité.\r\n\r\nNous mettons à votre disposition une chargée de clientèle, dont les coordonnées figurent ci-après, qui sera votre point de contact pour toutes les communications entre vous et les sociétés CELAVIGESTION (ainsi que Cristalis.)\r\n\r\nMadame @nom_utilisateur @prenom_utilisateur \r\nAdresse email : @email_utilisateur \r\n@telephone_utilisateur \r\n\r\nSpécialisé dans le bail commercial des locations meublées depuis 2004, CELAVIGESTION vous accompagne au quotidien dans la gestion complexe de votre investissement en location meublée : établissement des factures, calcul des indexations, décryptage du bail, gestion des rapports avec le locataire, etc…\r\n\r\nD’ici quelques mois vous aurez accès à tous vos documents sur notre nouvel extranet ainsi qu’un suivi de toutes les tâches accomplies pour votre compte.\r\n\r\nVeuillez recevoir, chère Madame @nom_client @prenom_client , cher Monsieur @nom_client @prenom_client , l’expression de mes sentiments les meilleurs.\r\n\r\n\r\nBertrand Le Mire\r\nPrésidence SAS CELAVIGESTION\r\n' WHERE `c_param_modelemail`.`pmail_id` = 6;

'
-- -----------------------------------------------------
-- Mise à jour par  Tsilavina le 23-03-2023
-- -----------------------------------------------------
ALTER TABLE `c_client` ADD `cin2` VARCHAR(255) NULL AFTER `etat_cin`;

-- -----------------------------------------------------
-- Mise à jour par  Philipine le 24-03-2023
-- -----------------------------------------------------

ALTER TABLE `c_indice_valeur_loyer` ADD `util_prenom_valide` VARCHAR(250) NULL AFTER `indval_date_registre`, ADD `indval_date_validation` DATE NULL AFTER `util_prenom_valide`;
ALTER TABLE `c_indice_valeur_loyer` ADD `indval_date_generation` DATE NULL AFTER `indval_date_validation`;

-- -----------------------------------------------------
-- Mise à jour par  Philipine le 28-03-2023
-- -----------------------------------------------------

INSERT INTO `c_page` (`page_id`, `page_nom`) VALUES ('9', 'envoi_taxe_lot');

--------------------------------------------------------
-- Mise à jour par  Tsilavina le 24-03-2023
-- -----------------------------------------------------

ALTER TABLE `c_indice_loyer` ADD `indloyer_valide` INT(1) NOT NULL DEFAULT '0' AFTER `indloyer_date_saisie_t4_mois`;

-- Dernière mise à jour en prod le 13-04-2023

--------------------------------------------------------
-- Mise à jour par  Tsilavina le 07-04-2023
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `c_lot_indexe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_lot_indexe` (
  `lot_index_id` INT NOT NULL AUTO_INCREMENT,
  `bail_id` INT(11) NULL,
  `indval_id` INT(11) NULL,
  PRIMARY KEY (`lot_index_id`))
ENGINE = InnoDB;

ALTER TABLE `c_lot_indexe` ADD `etat_facture` INT NOT NULL DEFAULT '0' AFTER `indval_id`, ADD `date_creation_facture` DATETIME NULL AFTER `etat_facture`;


-- -----------------------------------------------------
-- Mise à jour par  Philipine le 13-04-2023
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `c_etat_mandat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_etat_mandat` (
  `etat_mandat_id` INT NOT NULL AUTO_INCREMENT,
  `etat_mandat_libelle` VARCHAR(45) NULL,
  PRIMARY KEY (`etat_mandat_id`))
ENGINE = InnoDB;

INSERT INTO `c_etat_mandat` (`etat_mandat_id`, `etat_mandat_libelle`) VALUES (NULL, 'Mandat créé'), (NULL, 'Mandat envoyé'), (NULL, 'Mandat signé'), (NULL, 'Clos sans suite '), (NULL, 'Rétracté '), (NULL, 'Clôturé');

-- -----------------------------------------------------
-- Table `c_mandat_new`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_mandat_new` (
  `mandat_id` INT NOT NULL AUTO_INCREMENT,
  `mandat_date_creation` DATE NULL,
  `mandat_date_envoi` DATE NULL,
  `mandat_date_signature` DATE NULL,
  `mandat_type_produit_facture` VARCHAR(120) NULL,
  `mandat_montant_ht` FLOAT NULL,
  `mandat_lieu_facturation` VARCHAR(120) NULL,
  `mandat_date_fin` DATE NULL,
  `mandat_date_action_cloture` DATE NULL,
  `mandat_type_motif_cloture` VARCHAR(120) NULL,
  `mandat_motif_cloture` TEXT NULL,
  `lot_id` INT NULL,
  `etat_mandat_id` INT NOT NULL,
  `syndicat_id` INT(11) NOT NULL,
  `util_id` INT(11) NOT NULL,
  `cliLot_id` INT NULL,
  `prospect_id` INT(11) NULL,
  `client_id` INT(11) NULL,
  PRIMARY KEY (`mandat_id`),  
  CONSTRAINT `fk_c_mandat_c_lot_prospect1`
    FOREIGN KEY (`lot_id`)
    REFERENCES `c_lot_prospect` (`lot_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_etat_mandat1`
    FOREIGN KEY (`etat_mandat_id`)
    REFERENCES `c_etat_mandat` (`etat_mandat_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_syndicat1`
    FOREIGN KEY (`syndicat_id`)
    REFERENCES `c_syndicat` (`syndicat_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_utilisateur1`
    FOREIGN KEY (`util_id`)
    REFERENCES `c_utilisateur` (`util_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_lot_client1`
    FOREIGN KEY (`cliLot_id`)
    REFERENCES `c_lot_client` (`cliLot_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_prospect1`
    FOREIGN KEY (`prospect_id`)
    REFERENCES `c_prospect` (`prospect_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_mandat_c_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `c_client` (`client_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE `c_mandat_new` ADD `mandat_etat` TINYINT(1) NULL AFTER `mandat_motif_cloture`;

ALTER TABLE `c_mandat_new` ADD `type_mandat_id` INT(11) NULL AFTER `client_id`;

ALTER TABLE `c_mandat_new` ADD `mandat_path` VARCHAR(250) NULL AFTER `mandat_etat`;
ALTER TABLE `c_lot_indexe` ADD `nmbr_facture` INT NOT NULL DEFAULT '0' AFTER `date_creation_facture`;

--------------------------------------------------------
-- Mise à jour par  Tsilavina le 13-04-2023
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_cron_reindex_indice` (
  `cron_index_id` INT NOT NULL AUTO_INCREMENT,
  `indloyer_id` INT(11) NULL,
  `date_valid_indice` DATETIME NULL,
  `date_reindex` DATETIME NULL,
  `etat_reindex` INT NOT NULL DEFAULT '0',
  PRIMARY KEY (`cron_index_id`))
ENGINE = InnoDB;

--------------------------------------------------------
-- Mise à jour par  Philipine le 25-04-2023
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_document_tva` (
  `doc_tva_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_tva_nom` varchar(250) DEFAULT NULL,
  `doc_tva_path` varchar(250) DEFAULT NULL,
  `doc_tva_date_creation` datetime DEFAULT NULL,
  `doc_tva_etat` int(1) NOT NULL DEFAULT '1',
  `doc_date_envoi` date DEFAULT NULL,
  `dossier_id` int(11) NOT NULL,
  `util_id` int(11) NOT NULL,
  PRIMARY KEY (`doc_tva_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `c_document_tva` ADD `doc_tva_annee` VARCHAR(10) NULL AFTER `doc_tva_path`;
ALTER TABLE `c_document_tva` CHANGE `doc_date_envoi` `doc_tva_date_envoi` DATE NULL DEFAULT NULL;
ALTER TABLE `c_document_tva` ADD `doc_tva_montant` FLOAT NULL AFTER `doc_tva_date_creation`;

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Déclaration TVA', 'Modèle de mail utilisé lors de l\'envoi d\'une déclaration TVA.', 'C\'est la Vie - Déclaration TVA', 'Madame et Monsieur                                                                                                                                                                            \r\n\r\n\r\nVotre expert-comptable Crisralis a préparé votre déclaration de TVA concernant votre investissement en location meublée.\r\n \r\nLe montant de la TVA nette annuelle à payer résulte de la différence entre la TVA reçue sur vos loyers  à 5.5% ou 10% et de la TVA payée sur vos charges à 20%.\r\n \r\nVotre TVA est cette année payante pour @montant_tva\r\n \r\nCe montant sera télé-déclaré au service des impôts par l’expert-comptable et le télé paiement interviendra fin avril début mai de cette année. Votre compte devra donc être alimenté en conséquence.\r\nIl est important de vous assurer que votre banque a bien enregistré le mandat de prélèvement interentreprises que nous vous avons envoyé lors de la création de votre espace professionnel en ligne.\r\nSi ce n’est pas le cas le télé-paiement sera refusé et vous aurez alors des pénalités de retard de paiement et de non respect de la procédure.\r\n \r\nNous ne saurons être tenus pour responsable de ce problème.\r\n \r\nNous restons à votre disposition pour de plus amples informations.\r\n \r\nBien cordialement,\r\n\r\n\r\nVotre administrateur de biens CELAVIGESTION,\r\n\r\nParc de Brais,\r\n39 route de Fondeline\r\n44600 Saint-Nazaire\r\nFrance\r\n', '1', '1', '1', NULL);


INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Déclaration TVA', 'Modèle de mail utilisé lors de l\'envoi d\'une déclaration TVA.', 'C\'est la Vie - Déclaration TVA', 'Dear Sir and Mrs\r\n\r\nYour chartered accountant Cristalis prepared your VAT return for your property in France.\r\nYou will find in attachment a copy of the document that will be sent to the tax office.\r\nYou have nothing to do.\r\nFor your information, we will lodge your VAT declaration and ask for a refund of @montant_tva.€ that you will receive on your bank account.\r\nIt corresponds to the VAT you paid during the year.\r\n \r\nYour property manager, CELAVIGESTION\r\nParc de Brais,\r\n39 route de Fondeline\r\n44600 Saint-Nazaire\r\nFrance\r\n', '1', '1', '1', NULL);

--------------------------------------------------------
-- Mise à jour par  Philipine le 27-04-2023
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `c_document_2031`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_document_2031` (
  `doc_2031_id` INT NOT NULL AUTO_INCREMENT,
  `doc_2031_nom` VARCHAR(250) NULL,
  `doc_2031_path` VARCHAR(250) NULL,
  `doc_2031_date_creation` DATETIME NULL,
  `doc_2031_date_envoi` VARCHAR(45) NULL,
  `doc_2031_annee` VARCHAR(10) NULL,
  `doc_2031_etat` TINYINT(1) NULL,
  `dossier_id` INT(11) NOT NULL,
  `util_id` INT(11) NOT NULL,
  PRIMARY KEY (`doc_2031_id`),
  CONSTRAINT `fk_c_document_2031_c_dossier1`
    FOREIGN KEY (`dossier_id`)
    REFERENCES `c_dossier` (`dossier_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_document_2031_c_utilisateur1`
    FOREIGN KEY (`util_id`)
    REFERENCES `c_utilisateur` (`util_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE `c_document_2031` CHANGE `doc_2031_etat` `doc_2031_etat` TINYINT(1) NOT NULL DEFAULT '1';

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Liasse fiscale ', 'Modèle de mail utilisé lors de l\'envoi des liasses fiscales.', 'C\'est la Vie - Liasse fiscale ', 'Madame, Monsieur,\r\n \r\nVotre expert-comptable Cristalis a préparé votre liasse fiscale concernant votre investissement en location meublée dont nous sommes administrateur de biens. Ce dernier nous a demandé de vous en transmettre une copie.\r\n \r\nCe document comprend le bilan, le compte de résultat et les annexes. Il doit être conservé. Il figure en pièce jointe de cet email.\r\n \r\nVotre déclaration BIC sera envoyée également par votre expert-comptable au service des impôts, vous n’avez rien à faire.\r\n   \r\nBien cordialement\r\n\r\nVotre administrateur de biens CELAVIGESTION,\r\n\r\nParc de Brais,\r\n39 route de Fondeline\r\n44600 Saint-Nazaire\r\nFrance\r\n', '1', '1', '1', NULL);


INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Liasse fiscale ', 'Modèle de mail utilisé lors de l\'envoi des liasses fiscales.', 'C\'est la Vie - Liasse fiscale ', 'Dear Sir and Mrs\r\n\r\nYour chartered accountant Cristalis prepared your Tax return for your French property investment, for which we are your property manager.\r\nYou will find in attachment a copy of the document the chartered accountant will send to the Tax office. \r\nThis document includes one balance sheet, a P&L account and appendices. An English version is provided to you for your information.\r\nYou have nothing to do.\r\n\r\nYours sincerely,\r\n \r\nYour property manager, CELAVIGESTION\r\nParc de Brais,\r\n39 route de Fondeline\r\n44600 Saint-Nazaire\r\nFrance', '1', '1', '1', NULL);

--fin Dernier mis à jour le 20-04-2023
-- New sql à mettre à prod

-- Mis à jour par Philipine le 24-04-2023

-- -----------------------------------------------------
-- Table `c_mandat_type_cloture`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_mandat_type_cloture` (
  `type_cloture_id` INT NOT NULL AUTO_INCREMENT,
  `type_cloture_libelle` VARCHAR(120) NULL,
  PRIMARY KEY (`type_cloture_id`))
ENGINE = InnoDB;

INSERT INTO `c_mandat_type_cloture` (`type_cloture_id`, `type_cloture_libelle`) VALUES (NULL, 'Clôture par CELAVI (respect du préavis)\r\n'), (NULL, 'Clôture par CELAVI (déshérence sans préavis)'), (NULL, 'Clôture par CLIENT (respect du préavis)'), (NULL, 'Clôture par Ventes'), (NULL, 'Clôture pour fin d’activité (respect du préavis)');

ALTER TABLE `c_mandat_new` CHANGE `mandat_date_action_cloture` `mandat_date_action_cloture` DATETIME NULL DEFAULT NULL;

ALTER TABLE `c_mandat_new` CHANGE `mandat_type_motif_cloture` `type_cloture_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `c_mandat_new` ADD `mandat_date_demande` DATE NULL AFTER `mandat_date_fin`;

ALTER TABLE `c_mandat_new` ADD `mandat_fichier_justificatif` VARCHAR(250) NULL AFTER `mandat_path`;

-- Mis à jour par Philipine le 04-05-2023
-- -----------------------------------------------------
-- Table `c_mandat_type`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_mandat_type` (
  `mandat_type_id` INT NOT NULL AUTO_INCREMENT,
  `mandat_type_libelle` VARCHAR(50) NULL,
  PRIMARY KEY (`mandat_type_id`))
ENGINE = InnoDB;

INSERT INTO `c_mandat_type` (`mandat_type_id`, `mandat_type_libelle`) VALUES (NULL, 'Mandat client'), (NULL, 'Mandat prospect');

ALTER TABLE `c_mandat_new` ADD `mandat_type_id` INT NULL AFTER `type_mandat_id`;

-- Mis à jour par Philipine le 05-05-2023

ALTER TABLE `c_sie` ADD `sie_etat` INT NOT NULL DEFAULT '1' AFTER `sie_email`;

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Mandat d\'administration de biens', 'Modèle de mail utilisé lors de l\'envoi des liasses fiscales.', 'C\'est la Vie - Mandat d\'administration de biens', 'Madame,Monsieur\r\n\r\nEn tant qu\'administrateur de biens pour le compte de Monsieur Benôit SOT, propriétaire au sein de la Résidence Val Sénart à QUINCY-SOUS-SENART, et de par mon statut de mandataire, je me permets de vous contacter pour vous informer que dorénavant, notre société s\'occupera de collecter les loyers pour le compte de mon client.\r\n\r\nVous trouverez ci-joint le mandat signé par Monsieur SOT ainsi que le RIB de notre de notre société, sur lequel les loyers devront être versés à partir de ce jour. Je vous remercie donc de supprimer le RIB de Mr SOT de vos données afin qu\'aucune erreur ne soit faite.\r\n\r\nJe vous remercie de faire apparaître le nom de mon client comme référence lors de vos virements bancaires afin que nous puissions facilement déterminer la provenance et le destinataire des sommes versées.\r\nJe vous saurai gré également de me faire parvenir tous documents et courriers qui le concerneraient (Bail,avenant,quiitances,etc.).Enfin vous voudrez bien me communiquer les identifiants et mot de passe sur votre extranet bailleur le cas échéant.\r\n\r\n\r\nVous pouvez nous faire parvenir l\'ensemble de ces documents à l\'adresse e-mail suivante : clementine@celavigestion.fr et ou par courrier postal à l\'adresse suivante : \r\n\r\nCELAVI GESTION\r\n39 Route de Fondeline, Parc de Brais\r\n44 600 SAINT-NAZAIRE\r\n\r\n\r\nDans l\'attente d\'une confirmation de votre part, je vous prie d\'agréer, Madame, Monsieur, l\'expression de mes salutations les meilleures.', '1', '1', '1', NULL);
'
-- Mis à jour par Tsilavina le 05-05-2023
-- -----------------------------------------------------
-- Table `c_indice_loyer`
-- -----------------------------------------------------
ALTER TABLE `c_indice_loyer` ADD `mode_calcul` INT NOT NULL DEFAULT '1' AFTER `indloyer_valide`;

-- Fin denier mis à jour en prod le 09-05-2023

-- New sql à mettre à prod

-- Mis à jour par Philipine le 10-05-2023

INSERT INTO
    `c_etat_mandat` (
        `etat_mandat_id`,
        `etat_mandat_libelle`
    )
VALUES (NULL, 'Suspendu');

ALTER TABLE `c_mandat_new`
ADD
    `mandat_motif_suspension` TEXT NULL AFTER `mandat_motif_cloture`,
ADD
    `mandat_date_supension` DATE NULL AFTER `mandat_motif_suspension`;

ALTER TABLE
    `c_mandat_new` CHANGE `mandat_motif_suspension` `mandat_motif_suspension` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Mis à jour par Philipine le 12-05-2023

-- Table c_document_locataire

CREATE TABLE
    IF NOT EXISTS `c_document_locataire` (
        `doc_lot_id` INT NOT NULL AUTO_INCREMENT,
        `doc_lot_nom` VARCHAR(120) NULL,
        `doc_lot_path` VARCHAR(120) NULL,
        `doc_date_creation` DATETIME NULL,
        `cliLot_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_lot_id`)
    ) ENGINE = InnoDB;

ALTER TABLE
    `c_document_locataire` CHANGE `cliLot_id` `cliLot_id` INT(11) NULL;

-- Mis à jour par Philipine le 15-05-2023

-- Table c_document_syndic

CREATE TABLE
    IF NOT EXISTS `c_document_syndic` (
        `doc_syndic_id` INT NOT NULL AUTO_INCREMENT,
        `doc_syndic_nom` VARCHAR(120) NULL,
        `doc_syndic_path` VARCHAR(120) NULL,
        `doc_syndic_date_creation` DATETIME NULL,
        `cliLot_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_syndic_id`)
    ) ENGINE = InnoDB;

-- Table c_document_sie

CREATE TABLE
    IF NOT EXISTS `c_document_sie` (
        `doc_sie_id` INT NOT NULL AUTO_INCREMENT,
        `doc_sie_nom` VARCHAR(120) NULL,
        `doc_sie_path` VARCHAR(120) NULL,
        `doc_sie_date_creation` DATETIME NULL,
        `cliLot_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_sie_id`)
    ) ENGINE = InnoDB;

-- Table c_document_sip

CREATE TABLE
    IF NOT EXISTS `c_document_sip` (
        `doc_sip_id` INT NOT NULL AUTO_INCREMENT,
        `doc_sip_nom` VARCHAR(120) NULL,
        `doc_sip_path` VARCHAR(120) NULL,
        `doc_sip_date_creation` DATETIME NULL,
        `cliLot_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_sip_id`)
    ) ENGINE = InnoDB;

--------------------------------------------------------

-- Mis à jour par Tsilavina le 10-05-2023

-- -----------------------------------------------------

-- Table `c_document_bail`

-- -----------------------------------------------------

ALTER TABLE `c_document_bail`
ADD
    `doc_protocole` INT NOT NULL DEFAULT '0' AFTER `cliLot_id`;

ALTER TABLE `c_document_bail`
ADD
    `id_protocol` INT NULL AFTER `doc_protocole`;

-- -----------------------------------------------------

-- Table `c_protocol`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_protocol` (
        `id_protocol` INT(11) NOT NULL AUTO_INCREMENT,
        `date_sign_protocole` DATE NOT NULL,
        `type_abandon` INT(11) NOT NULL,
        `pourcentage_protocol` FLOAT NOT NULL DEFAULT 0,
        `info_complementaire` TEXT NOT NULL,
        `date_debut_protocole` DATE NOT NULL,
        `date_fin_protocole` DATE NOT NULL,
        `bail_id` INT(11) NOT NULL,
        `etat_protocol` INT(11) NOT NULL DEFAULT '0',
        `doc_path` VARCHAR(255) NULL,
        `doc_nom` VARCHAR(255) NULL,
        PRIMARY KEY (`id_protocol`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

--------------------------------------------------------

-- Mis à jour par Philipine le 17-05-2023

-- -----------------------------------------------------

ALTER TABLE
    `c_document_locataire`
ADD
    `doc_date_envoi` DATE NULL AFTER `doc_date_creation`;

ALTER TABLE
    `c_document_locataire`
ADD
    `util_id` INT(11) NULL AFTER `cliLot_id`;

ALTER TABLE
    `c_document_locataire` CHANGE `doc_date_envoi` `doc_date_envoi` DATETIME NULL DEFAULT NULL;

ALTER TABLE
    `c_document_syndic`
ADD
    `doc_syndic_date_envoi` DATETIME NULL AFTER `cliLot_id`,
ADD
    `util_id` INT(11) NULL AFTER `doc_syndic_date_envoi`;

ALTER TABLE `c_document_sie`
ADD
    `doc_sie_date_envoi` DATETIME NULL AFTER `cliLot_id`,
ADD
    `util_id` INT(11) NULL AFTER `doc_sie_date_envoi`;

ALTER TABLE `c_document_sip`
ADD
    `doc_sip_date_envoi` DATETIME NULL AFTER `cliLot_id`,
ADD
    `util_id` INT(11) NULL AFTER `doc_sip_date_envoi`;

-- Mis à jour par Philipine le 22-05-2023

-- -----------------------------------------------------

-- Table `c_etat_pno`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_etat_pno` (
        `etat_pno_id` INT NOT NULL AUTO_INCREMENT,
        `etat_pno_libelle` VARCHAR(45) NULL,
        PRIMARY KEY (`etat_pno_id`)
    ) ENGINE = InnoDB;

INSERT INTO
    `c_etat_pno` (
        `etat_pno_id`,
        `etat_pno_libelle`
    )
VALUES (NULL, 'PNO créé'), (NULL, 'PNO envoyé'), (NULL, 'PNO signé');

-- -----------------------------------------------------

-- Table `c_pno`

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_pno` (
        `pno_id` INT NOT NULL AUTO_INCREMENT,
        `pno_date_creation` DATE NULL,
        `pno_date_envoi` DATE NULL,
        `pno_date_signature` DATE NULL,
        `pno_lieu_creation` VARCHAR(120) NULL,
        `pno_celavi` INT(1) NULL,
        `pno_syndic` INT(1) NULL,
        `pno_client` INT(1) NULL,
        `lot_id` INT NULL,
        `util_id` INT(11) NOT NULL,
        `cliLot_id` INT NULL,
        `prospect_id` INT(11) NULL,
        `client_id` INT(11) NULL,
        PRIMARY KEY (`pno_id`)
    ) ENGINE = InnoDB;

ALTER TABLE `c_pno` CHANGE `util_id` `util_id` INT(11) NULL;

ALTER TABLE `c_pno`
ADD
    `pno_path` VARCHAR(250) NULL AFTER `pno_client`;

ALTER TABLE `c_pno`
ADD
    `etat_pno_id` INT NOT NULL DEFAULT '1' AFTER `client_id`;

ALTER TABLE `c_pno`
ADD
    `pno_montant` FLOAT NULL AFTER `pno_lieu_creation`;

INSERT INTO
    `c_etat_pno` (
        `etat_pno_id`,
        `etat_pno_libelle`
    )
VALUES (NULL, 'PNO Clos sans suite');

--------------------------------------------------------

-- Mis à jour par Tsilavina le 22-05-2023

-- -----------------------------------------------------

ALTER TABLE `c_sie` ADD `sie_rib` VARCHAR(255) NULL AFTER `sie_etat`;

ALTER TABLE `c_sie`
ADD
    `file_rib_sie` VARCHAR(255) NULL AFTER `sie_rib`;

ALTER TABLE `c_sie`
ADD
    `date_maj` DATETIME NULL AFTER `file_rib_sie`;

--------------------------------------------------------

-- Mis à jour par Tsilavina le 25-05-2023

-- -----------------------------------------------------

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        '12',
        'C\'est la Vie -  Information taxe foncière',
        'Modèle de mail utilisé lors de l\'envoi des informations sur la taxe foncière. Il permet d\'envoyer les informations de la taxe foncière et le montant de la TOM  par mail au gestionnaire. Utilisable depuis le menu propriétaire, Fiche Lot, sous onglet taxe foncière, bouton envoyer un mail au gestionnaire lors de la validation des informations.',
        'C\'est la Vie -  @taxe_fonciere',
        'Madame, Monsieur,\r\n\r\nSuite à la réception de votre taxe foncière que vous avez fait parvenir, nous avons demandé au gestionnaire le remboursement de la TOM pour un montant de @montant_tom. Nous vous remercions de nous tenir informé de la date de remboursement et du montant reçu dans les mois qui viennent.\r\n\r\nDear Sir,\r\n.',
        '1',
        '1',
        '1',
        'Taxes Foncière concernée en pièce jointe'
    );

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        NULL,
        'C\'est la Vie - Information taxe foncière',
        'Modèle de mail utilisé lors de l\'envoi des informations sur la taxe foncière. Il permet d\'envoyer les informations de la taxe foncière et le montant de la TOM par mail au gestionnaire. Utilisable depuis le menu propriétaire, Fiche Lot, sous onglet taxe foncière, bouton envoyer un mail au gestionnaire lors de la validation des informations.',
        'C\'est la Vie - @taxe_fonciere',
        'Dear Sir,\r\n\r\nFollowing the reception of your land tax you sent us, we asked your management company to refund you the household garbage tax for an amount of @montant_tom. Please advise us when you get it on your bank account withing the following weeks or months.\r\n',
        '1',
        '1',
        '1',
        'Taxes Foncière concernée en pièce jointe'
    );

--------------------------------------------------------

-- Mis à jour par Philipine le 25-05-2023

-- -----------------------------------------------------

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        NULL,
        'C\'est la Vie -  Information PNO',
        'Modèle de mail utilisé lors de l\'envoi des informations de la PNO.',
        'C\'est la Vie - Informations PNO',
        'Cher @nom_client @prenom_client ,\r\n\r\nSuite à l’examen de votre dossier et notamment des pièces fournies par vos soins, nous constatons que ni votre syndic ni vous-même ne semblés avoir souscrit d’assurance Propriétaire Non Occupant pour votre bien en gestion chez CELAVIGESTION @nom_programme.\r\n\r\nNous vous rappelons que cette assurance est obligatoire Depuis le 1er janvier 2015, loi n° 2014-366 du 24 mars 2014 dite la loi “Alur“.\r\n\r\nL’assurance Propriétaire Non Occupant (PNO), souvent abrégé en assurance “PNO”, est en fait une assurance habitation que doivent souscrire les propriétaires qui n’occupent pas eux-mêmes leur bien. Elle couvrira le propriétaire si un sinistre survient. Ajoutons que l’assurance PNO couvre les dommages subis en cas de vétusté du logement, les éventuels vices de construction (défauts altérant la qualité de la construction et pouvant rendre l’habitation dangereuse voire inhabitable), le défaut de construction (les défauts dans un ouvrage, résultant de la mauvaise exécution des travaux) ou encore les dégâts des installations électriques ou gaz. L’assurance est aussi sollicitée en cas de dégât des eaux, d’incendie, d’explosion, de catastrophes naturelles, de grêle, de tempête… ou de trouble de jouissance (utilisation du logement pour de la location saisonnière de courte durée). Elle intervient également pour des sinistres que ne couvrent ni l\'assurance de la copropriété, ni l\'assurance du locataire.\r\n\r\nNotre société CELAVIGESTION vous propose une « PNO » à partir de 49€ par an au lieu de 70/110€ chez les assureurs particuliers. Ce tarif très compétitif a été négocié pour les clients de CELAVIGESTION exclusivement.\r\n\r\nJe vous laisse me contacter pour tout renseignement sur le mail bertrand@celavigestion.fr \r\n\r\nBien cordialement\r\n',
        NULL,
        NULL,
        '1',
        NULL
    );

INSERT INTO
    `c_param_modelemail` (
        `pmail_id`,
        `pmail_libelle`,
        `pmail_description`,
        `pmail_objet`,
        `pmail_contenu`,
        `emeteur_mail`,
        `signature`,
        `util_id`,
        `fichier_joint`
    )
VALUES (
        '15',
        'C\'est la Vie -  Information PNO',
        'Modèle de mail utilisé lors de l\'envoi des informations de la PNO très chère.',
        'C\'est la Vie - Informations PNO très chère',
        'Cher @nom_client @prenom_client ,\r\n\r\nSuite à l’examen de votre dossier et notamment des pièces fournies par vos soins, nous constatons que vous avez souscrit une PNO d’un montant de @montant_pno\r\n\r\nNous vous rappelons que cette assurance est obligatoire Depuis le 1er janvier 2015, loi n° 2014-366 du 24 mars 2014 dite la loi “Alur“.\r\n\r\nL’assurance Propriétaire Non Occupant (PNO), souvent abrégé en assurance “PNO”, est en fait une assurance habitation que doivent souscrire les propriétaires qui n’occupent pas eux-mêmes leur bien. Elle couvrira le propriétaire si un sinistre survient. Ajoutons que l’assurance PNO couvre les dommages subis en cas de vétusté du logement, les éventuels vices de construction (défauts altérant la qualité de la construction et pouvant rendre l’habitation dangereuse voire inhabitable), le défaut de construction (les défauts dans un ouvrage, résultant de la mauvaise exécution des travaux) ou encore les dégâts des installations électriques ou gaz. L’assurance est aussi sollicitée en cas de dégât des eaux, d’incendie, d’explosion, de catastrophes naturelles, de grêle, de tempête… ou de trouble de jouissance (utilisation du logement pour de la location saisonnière de courte durée). Elle intervient également pour des sinistres que ne couvrent ni l\'assurance de la copropriété, ni l\'assurance du locataire.\r\n\r\nNotre société CELAVIGESTION vous propose une « PNO » à partir de 49€ par an au lieu de 70/110€ chez les assureurs particuliers. Ce tarif très compétitif a été négocié pour les clients de CELAVIGESTION exclusivement.\r\n\r\nJe vous laisse me contacter pour tout renseignement sur le mail bertrand@celavigestion.fr \r\n\r\nBien cordialement\r\n',
        NULL,
        NULL,
        '1',
        NULL
    );

INSERT INTO
    `c_etat_pno` (
        `etat_pno_id`,
        `etat_pno_libelle`
    )
VALUES (NULL, 'PNO clôturé');

--------------------------------------------------------

-- Mis à jour par Philipine le 26-05-2023

-- -----------------------------------------------------

ALTER TABLE `c_pno`
ADD
    `pno_date_cloture` DATE NULL AFTER `pno_path`;

ALTER TABLE `c_pno`
ADD
    `pno_fichier_cloture` VARCHAR(250) NULL AFTER `pno_date_cloture`;

-- Dernière mis-à-jour le 30-05-2023

-- New sql à mettre en prod

--------------------------------------------------------

-- Mise à jour par Tsilavina le 01-06-2023

-- -----------------------------------------------------

CREATE TABLE
    `c_facturation_lot` (
        `id_fact_lot` int(11) NOT NULL,
        `cliLot_id` int(11) NOT NULL,
        `mode_paiement` int(11) NOT NULL,
        `montant_ht_facture` float NOT NULL,
        `montant_ht_figurant` float NOT NULL
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

ALTER TABLE `c_facturation_lot` ADD PRIMARY KEY (`id_fact_lot`);

ALTER TABLE
    `c_facturation_lot` CHANGE `id_fact_lot` `id_fact_lot` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE
    `c_mode_paiement_facturation` (
        `id_mode_paie` int(11) NOT NULL,
        `libelle` varchar(70) NOT NULL
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

ALTER TABLE
    `c_mode_paiement_facturation`
ADD
    PRIMARY KEY (`id_mode_paie`);

ALTER TABLE
    `c_mode_paiement_facturation` CHANGE `id_mode_paie` `id_mode_paie` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO
    `c_mode_paiement_facturation` (`id_mode_paie`, `libelle`)
VALUES (1, 'Non renseigné'), (2, 'Virement'), (3, 'Prélèvement'), (4, 'Pris sur les loyers');

-- Dernière mis-à-jour le 12-06-2023

-- New sql à mettre en prod

--------------------------------------------------------

-- Mise à jour par Philipine le 12-06-2023

-- -----------------------------------------------------

--------------------------------------------------------

-- Table c_reporting

-- -----------------------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_reporting` (
        `reporting_id` INT NOT NULL AUTO_INCREMENT,
        `reporting_path` VARCHAR(120) NULL,
        `reporting_date_generation` DATE NULL,
        `reporting_date_envoi` DATE NULL,
        `dossier_id` int(11) NOT NULL,
        PRIMARY KEY (`reporting_id`)
    ) ENGINE = InnoDB;

ALTER TABLE `c_reporting`
ADD
    `reporting_etat` INT NOT NULL DEFAULT '0' AFTER `reporting_date_envoi`;

ALTER TABLE `c_reporting`
ADD
    `reporting_annee` VARCHAR(10) NULL AFTER `reporting_etat`;

ALTER TABLE `c_reporting`
ADD
    `reporting_trimestre` VARCHAR(50) NULL AFTER `reporting_id`;

ALTER TABLE
    `c_reporting` CHANGE `reporting_etat` `reporting_etat` VARCHAR(50) NULL DEFAULT '0';

ALTER TABLE `c_reporting`
ADD
    `reporting_nom_trimestre` VARCHAR(50) NULL AFTER `reporting_trimestre`;

ALTER TABLE
    `c_taxe_fonciere_lot`
ADD
    `taxe_date_envoi_mail` DATE NULL AFTER `taxe_envoi_mail`;

--------------------------------------------------------

-- Mise à jour par Tsilavina le 13-06-2023

-- -----------------------------------------------------

ALTER TABLE
    `c_indice_valeur_loyer`
ADD
    `util_prenom_publie` VARCHAR(50) NULL AFTER `indval_date_registre`,
ADD
    `indval_date_publie` DATE NULL AFTER `util_prenom_publie`;

ALTER TABLE
    `c_indexation_loyer`
ADD
    `indlo_indince_non_publie` INT NOT NULL DEFAULT '0' AFTER `etat_indexation`;

ALTER TABLE `c_facture_loyer`
ADD
    `fact_prorata` VARCHAR(255) NULL AFTER `dest_ville`;

--------------------------------------------------------

-- Mise à jour par Philipine le 19-06-2023

-- -----------------------------------------------------

ALTER TABLE `c_lot_client`
ADD
    `cliLot_num_parking` VARCHAR(120) NULL AFTER `cliLot_num`;

ALTER TABLE `c_lot_prospect`
ADD
    `lot_num_parking` VARCHAR(120) NULL AFTER `lot_nom`;

--------------------------------------------------------

-- Mise à jour par Tsilavina le 19-06-2023

-- -----------------------------------------------------

ALTER TABLE
    `c_sie` CHANGE `date_maj` `date_maj` TIMESTAMP NULL DEFAULT NULL;

ALTER TABLE
    `c_facturation_lot`
ADD
    `dossier_id` INT NULL AFTER `montant_ht_figurant`;

ALTER TABLE `c_dossier`
ADD
    `iban` VARCHAR(255) NULL AFTER `util_id`,
ADD
    `bic` VARCHAR(255) NULL AFTER `iban`;

ALTER TABLE `c_prospect`
ADD
    `prospect_iban` VARCHAR(255) NULL AFTER `prospect_commentaire`,
ADD
    `prospect_bic` VARCHAR(255) NULL AFTER `prospect_iban`;

-- Dernière mis-à-jour le 20-06-2023

-- New sql à mettre en prod

--------------------------------------------------------

-- Mise à jour par Philipine le 20-06-2023

-- -----------------------------------------------------

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Reporting ', 'Modèle de mail utilisé lors de l\'envoi du reporting client.', 'C\'est la Vie - Reporting ', 'Cher @nom_client @prenom_client ,\r\n \r\nVeuillez trouver ci-joint le rapport de votre gestion trimestrielle\r\n\r\nBien cordialement,\r\n', NULL, NULL, '1', NULL)';

ALTER TABLE `c_reporting` ADD `util_id` INT(11) NULL AFTER `dossier_id`;
INSERT INTO `c_periode_indexation` (`prdind_id`, `prdind_valeur`) VALUES (NULL, '0');

--------------------------------------------------------
-- Mise à jour par Philipine le 20-06-2023
-- -----------------------------------------------------

ALTER TABLE `c_prospect` ADD `prospect_residence_fiscale` VARCHAR(250) NULL AFTER `prospect_nationalite`;
ALTER TABLE `c_client` ADD `client_residence_fiscale` VARCHAR(120) NULL AFTER `client_nationalite`;
ALTER TABLE `c_sie` CHANGE `date_maj` `date_maj` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- Dernière mis-à-jour le 28-06-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Tsilavina le 28-06-2023
-- -----------------------------------------------------
ALTER TABLE `c_prospect` ADD `file_rib` VARCHAR(255) NULL AFTER `cin`;
ALTER TABLE `c_dossier` ADD `file_rib` VARCHAR(255) NULL AFTER `bic`;
INSERT INTO `c_periode_indexation` (`prdind_id`, `prdind_valeur`) VALUES (NULL, '0');

-- Dernière mis-à-jour le 30-06-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 03-07-2023
-- -----------------------------------------------------

ALTER TABLE `c_reporting` ADD `reporting_date_debut` DATE NULL AFTER `reporting_path`;

--------------------------------------------------------
-- Mise à jour par Tsilavina le 05-07-2023
-- -----------------------------------------------------
CREATE TABLE `c_pret_lot_presence` (
  `id_p_presence` int(11) NOT NULL AUTO_INCREMENT,
  `etat_presence` int(11) NOT NULL,
  `cliLot_id` int(11) NOT NULL,
  PRIMARY KEY (`id_p_presence`)
);

-- Dernière mis-à-jour le 07-07-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 07-07-2023
-- -----------------------------------------------------

ALTER TABLE `c_type_travail` ADD `type_traivail_texte` TEXT NULL AFTER `type_travail_etat`;

--------------------------------------------------------
-- Mise à jour par Philipine le 10-07-2023
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `c_banque`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_banque` (
  `banque_id` INT NOT NULL AUTO_INCREMENT,
  `banque_nom` VARCHAR(45) NULL,
  `banque_num_compte` VARCHAR(45) NULL,
  `banque_bic` VARCHAR(45) NULL,
  `banque_iban` VARCHAR(45) NULL,
  `banque_cpt_comptable` VARCHAR(12) NULL,
  `banque_code_journal` VARCHAR(10) NULL,
  `banque_etat` TINYINT(1) NULL DEFAULT 1 COMMENT '1 actif\n0 inactif',
  PRIMARY KEY (`banque_id`))
ENGINE = InnoDB;
--------------------------------------------------------
-- Mise à jour par Tsilavina le 10-07-2023
-- -----------------------------------------------------
CREATE TABLE `c_informations_entreprise_cestlavie` (
  `id_entreprise` INT(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` VARCHAR(60) NOT NULL,
  `adresse1` VARCHAR(200)  NULL,
  `adresse2` VARCHAR(200) NULL,
  `adresse3` VARCHAR(200) NULL,
  `cp` VARCHAR(200)  NULL,
  `ville` VARCHAR(200) NULL,
  `pays` VARCHAR(200)  NULL,
  `capital` VARCHAR(200)  NULL,
  `rcs` VARCHAR(200)  NULL,
  `siret` VARCHAR(200)  NULL,
  `ape` VARCHAR(200)  NULL,
  `tva_intra` VARCHAR(200)  NULL,
  `carte_pro` VARCHAR(200)  NULL,
  `libelle` VARCHAR(200)  NULL,
  PRIMARY KEY (`id_entreprise`)
);

-- Dernière mis-à-jour le 12-07-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 12-07-2023
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `c_pourcentage_evaluation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_pourcentage_evaluation` (
  `pourcentage_id` INT NOT NULL AUTO_INCREMENT,
  `pourcentage_valeur` FLOAT NULL,
  `pourcentage_date_creation` DATE NULL,
  `gestionnaire_id` INT NOT NULL,
  PRIMARY KEY (`pourcentage_id`))
ENGINE = InnoDB;

ALTER TABLE `c_informations_entreprise_cestlavie` 
ADD `mode_prelevement` TEXT NULL AFTER `libelle`,
ADD `mode_virement` TEXT NULL AFTER `mode_prelevement`, 
ADD `mode_cheque` TEXT NULL AFTER `mode_virement`; 

ALTER TABLE `c_informations_entreprise_cestlavie` 
ADD `journal_ventes` VARCHAR(50) NULL AFTER `mode_cheque`, 
ADD `journal_od` VARCHAR(50) NULL AFTER `journal_ventes`; 

-- Dernière mis-à-jour le 13-07-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 14-07-2023
-- -----------------------------------------------------

ALTER TABLE `c_acte` ADD `acte_frais_acquisition` FLOAT NOT NULL DEFAULT '0' AFTER `acte_valeur_mobilier_ht`;
--------------------------------------------------------
-- Mise à jour par Tsilavina le 14-07-2023
-- -----------------------------------------------------
ALTER TABLE `c_pret_lot_presence` ADD `commentaire` TEXT NULL AFTER `cliLot_id`; 
ALTER TABLE `c_acte` ADD `commentaire_acte` TEXT NULL AFTER `immo_id`; 


-- Dernière mis-à-jour le 20-07-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Tsilavina le 18-07-2023
-- -----------------------------------------------------

ALTER TABLE `c_franchise_loyer` ADD `type_deduction` INT NULL AFTER `etat_franchise`, 
ADD `montant_ttc_deduit` FLOAT NULL AFTER `type_deduction`;
ALTER TABLE `c_facture_loyer` ADD `montant_deduction` FLOAT NULL AFTER `fact_email_sent`;  
ALTER TABLE `c_facture_loyer` ADD `libelle_facture_deduit` TEXT NULL AFTER `montant_deduction`; 

--------------------------------------------------------
-- Mise à jour par Philipine le 21-07-2023
-- ----------------------------------------------------

ALTER TABLE `c_programme` ADD `progm_pourcentage_evaluation` FLOAT NULL AFTER `progm_date_modification`;

--------------------------------------------------------
-- Mise à jour par Philipine le 25-07-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_communication` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_libelle` varchar(250) DEFAULT NULL,
  `com_date_creation` datetime DEFAULT NULL,
  `com_date_validation` datetime DEFAULT NULL,
  `com_date_envoi` datetime DEFAULT NULL,
  `com_util_id` int(11) DEFAULT NULL,
  `com_util_valide` varchar(250) DEFAULT NULL,
  `com_util_envoi` varchar(250) DEFAULT NULL,
  `com_etat` tinyint(1) DEFAULT NULL,
  `com_texte` text,
  PRIMARY KEY (`com_id`))
  ENGINE = InnoDB;

  ALTER TABLE `c_communication` ADD `type_comprospect_id` INT NULL AFTER `com_texte`;

  ALTER TABLE `c_communication` ADD `com_description` TEXT NULL AFTER `com_texte`;
  ALTER TABLE `c_communication` CHANGE `com_texte` `com_contenu` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
  ALTER TABLE `c_communication` ADD `com_objet` VARCHAR(250) NULL AFTER `com_etat`;
  ALTER TABLE `c_communication` CHANGE `com_util_id` `util_id` INT(11) NULL DEFAULT NULL;
  ALTER TABLE `c_communication` ADD `com_type` TINYINT(1) NOT NULL DEFAULT '1' AFTER `com_etat`;
  ALTER TABLE `c_communication` ADD `client_id` INT(11) NULL AFTER `type_comprospect_id`;
  ALTER TABLE `c_communication` CHANGE `client_id` `client_id` VARCHAR(120) NULL DEFAULT NULL;
  ALTER TABLE `c_communication` ADD `signature` INT(1) NULL AFTER `com_description`;

-- Dernière mis-à-jour le 31-07-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 31-07-2023
-- ----------------------------------------------------

-- -----------------------------------------------------
-- Table `c_famille_article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_famille_article` (
  `fam_id` INT NOT NULL AUTO_INCREMENT,
  `fam_libelle` VARCHAR(45) NULL,
  PRIMARY KEY (`fam_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `c_type_article_facturation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_type_article_facturation` (
  `type_fact_id` INT NOT NULL AUTO_INCREMENT,
  `type_fact_libelle` VARCHAR(45) NULL,
  PRIMARY KEY (`type_fact_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `c_article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_article` (
  `art_id` INT NOT NULL AUTO_INCREMENT,
  `art_code` VARCHAR(45) NULL,
  `art_libelle_fr` VARCHAR(80) NULL,
  `art_libelle_en` VARCHAR(80) NULL,
  `art_cpt_comptable` VARCHAR(12) NULL,
  `art_tarif` DOUBLE NULL,
  `fam_id` INT NOT NULL,
  `type_fact_id` INT NOT NULL COMMENT '1 actif\n0 inactif',
  `art_etat` TINYINT(1) NULL,
  PRIMARY KEY (`art_id`),
  CONSTRAINT `fk_c_article_c_famille_article1`
    FOREIGN KEY (`fam_id`)
    REFERENCES `c_famille_article` (`fam_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_article_c_type_article_facturation1`
    FOREIGN KEY (`type_fact_id`)
    REFERENCES `c_type_article_facturation` (`type_fact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `c_type_article_facturation` (`type_fact_id`, `type_fact_libelle`) VALUES (NULL, 'Facturation annuelle'), (NULL, 'Services complémentaires');
INSERT INTO `c_famille_article` (`fam_id`, `fam_libelle`) VALUES (NULL, 'Vente');

--------------------------------------------------------
-- Mise à jour par Philipine le 28-07-2023
-- ----------------------------------------------------

RENAME TABLE `c_estlaviedb`.`c_taxe_foncière_lot` TO `c_estlaviedb`.`c_taxe_fonciere_lot`;

-- Dernière mis-à-jour le 03-08-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Tsilavina le 25-07-2023
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_access_proprietaire_client` (
  `accp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mtpfr_id` varchar(120) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `accp_login` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`accp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `c_access_proprietaire_client` 
ADD  CONSTRAINT `fk_c_client_c_access_proprietaire_client1` 
FOREIGN KEY (`client_id`) 
REFERENCES `c_client`(`client_id`)
ON DELETE NO ACTION ON UPDATE NO ACTION;

DROP TABLE IF EXISTS `c_utilisateur_client`;
CREATE TABLE IF NOT EXISTS `c_utilisateur_client` (
  `util_id` int(11) NOT NULL AUTO_INCREMENT,
  `util_login` varchar(50) DEFAULT NULL,
  `util_password` text,
  `client_id` int(11) NOT NULL,
  `etat_user` int(11) NOT NULL,
  `util_datecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`util_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `c_historiqueacces_client`;
CREATE TABLE IF NOT EXISTS `c_historiqueacces_client` (
  `hacces_id` int(11) NOT NULL AUTO_INCREMENT,
  `hacces_token` text,
  `hacces_datecreate` datetime DEFAULT NULL,
  `hacces_datelogin` datetime DEFAULT NULL,
  `hacces_datelogout` datetime DEFAULT NULL,
  `hacces_ip` varchar(20) DEFAULT NULL,
  `hacces_agent` varchar(20) DEFAULT NULL,
  `hacces_action` int(3) DEFAULT NULL,
  `hacces_status` int(11) DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hacces_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `c_historiqueacces_client` ADD FOREIGN KEY (`util_id`) 
REFERENCES `c_utilisateur_client`(`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DROP TABLE IF EXISTS `c_message_accueil_intranet`;
CREATE TABLE IF NOT EXISTS `c_message_accueil_intranet` (
`mess_id` INT(11) NOT NULL AUTO_INCREMENT,
`message` TEXT NOT NULL,
PRIMARY KEY (`mess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `c_message_accueil_intranet` (`mess_id`, `message`) VALUES (1, 'Bonjour @nom,\r\n\r\nBienvenu sur le portail Intranet.\r\nUtilisez le menu de gauche pour consulter vos informations..\r\n\r\nBonne Utilisation!');

--------------------------------------------------------
-- Mise à jour par Philipine le 02-08-2023
-- ----------------------------------------------------

ALTER TABLE `c_prospect` ADD `prospect_date_fin_validite_cni` DATE NULL AFTER `file_rib`;
ALTER TABLE `c_client` ADD `client_date_fin_validite_cni` DATE NULL AFTER `cin2`;

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Date de validité CNI', 'Modèle de mail utilisé pour demander une nouvelle pièce d\'identité à jour. Il permet de demander une nouvelles pièce d\'identité à jour aux mandants. Utilisable depuis la tâche automatique de détection de la date de validité CNI .', 'C\'est la Vie - Demande de pièce d\'identité à jour', 'Cher @nom_client @prenom_client,\r\n\r\nLa pièce d’identité que vous nous avez fourni arrive en fin de validité. Le Code Monétaire et Financier, article 561, met en place une obligation pour l’agent immobilier de vérifier l’identité de ses mandants. \r\n\r\nVous voudrez bien nous faire parvenir par mail votre nouvelle pièce d’identité à jour. \r\n\r\nBien cordialement \r\n\r\nVotre chargé de clientèle \r\n\r\n@charge_client \r\n', '1', '1', '1', NULL);
INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'C\'est la Vie - Date de validité CNI', 'Modèle de mail utilisé pour demander une nouvelle pièce d\'identité à jour. Il permet de demander une nouvelles pièce d\'identité à jour aux mandants. Utilisable depuis la tâche automatique de détection de la date de validité CNI .', 'C\'est la Vie - Demande de pièce d\'identité à jour', 'Dear Mr Mrs @nom_client @prenom_client,\r\n\r\nYour passport is about to expire. The French monetary code, article 561, the monetary code sets up an identity verification obligation for the real estate professional. \r\n\r\nThank you to send us a copy by email of your new passport. \r\n\r\nKind regards \r\n\r\nYour account manager\r\n\r\n@charge_client \r\n\r\n', '1', '1', '1', NULL);


ALTER TABLE `c_taxe_fonciere_lot` CHANGE `util_prenom` `util_prenom` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `c_document_lot_taxe` CHANGE `util_id` `util_id` INT(11) NULL;
ALTER TABLE `c_document_lot_taxe` ADD `util_client` INT NULL AFTER `cliLot_id`;

-- Dernière mis-à-jour le 09-08-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 09-08-2023
-- ----------------------------------------------------
ALTER TABLE `c_communication` ADD `progm_id` INT(11) NULL AFTER `client_id`;


-- Dernière mis-à-jour le 16-08-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Tsilavina le 16-08-2023
-- ----------------------------------------------------

ALTER TABLE `c_franchise_loyer` ADD `montant_ht` FLOAT NULL AFTER `type_deduction`, ADD `tva` FLOAT NULL AFTER `montant_ht`;
ALTER TABLE `c_facture_loyer` ADD `tva_deduit` FLOAT NULL AFTER `libelle_facture_deduit`;

--------------------------------------------------------
-- Mise à jour par Tsilavina le 17-08-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_lien_active_intranet` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`cle_temporaire` TEXT NOT NULL,
`lien` VARCHAR(255) NOT NULL,
`date_creation` DATETIME NOT NULL,
`etat` INT(11) NOT NULL,
PRIMARY KEY (`id`));

-- Mise à jour par Philipine le 17-08-2023
-- ----------------------------------------------------
ALTER TABLE `c_communication` ADD `com_fichier` VARCHAR(255) NULL AFTER `signature`;

-- Dernière mis-à-jour le 24-08-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Tsilavina le 24-08-2023
-- ----------------------------------------------------

ALTER TABLE `c_bail` ADD `bail_var_min` FLOAT NULL DEFAULT NULL AFTER `bail_var_plafonnee`;

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 28-08-2023
-- Table `c_document_prgm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_document_prgm` (
  `doc_progm_id` INT NOT NULL AUTO_INCREMENT,
  `doc_progm_nom` VARCHAR(120) NULL,
  `doc_progm_path` VARCHAR(120) NULL,
  `doc_progm_creation` DATETIME NULL,
  `doc_progm_etat` INT(11) NULL,
  `util_id` INT(11) NOT NULL,
  `progm_id` INT NOT NULL,
  PRIMARY KEY (`doc_progm_id`),
  CONSTRAINT `fk_document_lot_progm_c_utilisateur1`
    FOREIGN KEY (`util_id`)
    REFERENCES `c_utilisateur` (`util_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_lot_progm_c_programme1`
    FOREIGN KEY (`progm_id`)
    REFERENCES `c_programme` (`progm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
--------------------------------------------------------
-- Mise à jour par Philipine le 28-08-2023
-- ----------------------------------------------------

ALTER TABLE `c_dossier` ADD `acompte_tva` INT NULL AFTER `file_rib`;

-- Dernière mis-à-jour le 29-08-2023
-- New sql à mettre en prod

--------------------------------------------------------
-- Mise à jour par Philipine le 29-08-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_proprietaire_seclectionne` (
  `prop_select_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL,
  PRIMARY KEY (`prop_select_id`)
) 
ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

ALTER TABLE `c_communication` ADD `id_operation` INT NULL AFTER `progm_id`;

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 28-08-2023
-- Table `c_utilisateur_client`
-- -----------------------------------------------------
ALTER TABLE `c_utilisateur_client` ADD `defaultlanguage` VARCHAR(50) NOT NULL DEFAULT 'french' AFTER `util_datecreate`;
INSERT INTO `c_message_accueil_intranet` (`mess_id`, `message`) VALUES (NULL, 'Hello @nom,\r\n\r\nWelcome to the Intranet portal.\r\nUse the menu on the left to consult your information...\r\n\r\nEnjoy your visit!');

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 08-09-2023
-- Table `c_document_gestionnaire`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_document_gestionnaire` (
  `doc_gest_id` INT NOT NULL AUTO_INCREMENT,
  `doc_gest_nom` VARCHAR(120) NULL,
  `doc_gest_path` VARCHAR(120) NULL,
  `doc_gest_creation` DATETIME NULL,
  `doc_gest_etat` INT(11) NULL,
  `util_id` INT(11) NOT NULL,
  `gestionnaire_id` INT NOT NULL,
  PRIMARY KEY (`doc_gest_id`),
  CONSTRAINT `fk_document_lot_gest_c_utilisateur1`
    FOREIGN KEY (`util_id`)
    REFERENCES `c_utilisateur` (`util_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_lot_gest_c_gestionnaire1`
    FOREIGN KEY (`gestionnaire_id`)
    REFERENCES `c_gestionnaire` (`gestionnaire_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- Dernière mis-à-jour le 12-09-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 08-09-2023
-- Table `c_nationalite`
-- -----------------------------------------------------

CREATE TABLE `c_nationalite` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `libelle` VARCHAR(120) NOT NULL,
     PRIMARY KEY (`id`)
);

INSERT INTO `c_nationalite` (`id`, `libelle`) VALUES (NULL, 'Française'), (NULL, 'Britannique'), (NULL, 'Irlandaise'), (NULL, 'Etats Unis'), (NULL, 'Canadienne'), (NULL, 'Espagnole'), (NULL, 'Italienne'), (NULL, 'Pays Bas'), (NULL, 'Belgique'), (NULL, 'Norvégienne'), (NULL, 'Allemagne'), (NULL, 'Suisse'), (NULL, 'Portugaise'), (NULL, 'Mexicaine'), (NULL, 'Pologne'), (NULL, 'Emirats Arabes Unis'), (NULL, 'Jordanie'), (NULL, 'Slovaquie'), (NULL, 'Danoise'), (NULL, 'Australienne');

-- Dernière mis-à-jour le 13-09-2023
-- New sql à mettre en prod


--------------------------------------------------------
-- Mise à jour par Loroque le 15-09-2023 // Conception communication
-- ----------------------------------------------------

/***** communications *****/

CREATE TABLE IF NOT EXISTS `c_communications` (
  `coms_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_libelle` varchar(250) DEFAULT NULL,
  `coms_date_creation` datetime DEFAULT NULL,
  `coms_date_validation` datetime DEFAULT NULL,
  `coms_date_envoi` datetime DEFAULT NULL,
  `coms_type_id` tinyint(1) DEFAULT NULL,
  `coms_etat` tinyint(1) DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_id`)
);

ALTER TABLE `c_communications` ADD `coms_description` TEXT NOT NULL AFTER `coms_libelle`;
ALTER TABLE `c_communications` ADD `coms_status` INT NULL DEFAULT NULL AFTER `coms_date_envoi`;

CREATE TABLE IF NOT EXISTS `c_communications_contenu` (
  `coms_con_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_con_objet` varchar(250) DEFAULT NULL,
  `coms_con_contenu` text,
  `coms_util_emetteur` tinyint(1) DEFAULT NULL,
  `coms_con_signature` tinyint(1) DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  `coms_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_con_id`)
);

ALTER TABLE `c_communications_contenu` ADD `coms_con_type_ontenu` TINYINT NULL DEFAULT NULL AFTER `coms_con_signature`;

CREATE TABLE IF NOT EXISTS `c_communications_pjoin` (
  `coms_pjoin_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_pjoin_type` varchar(11) DEFAULT NULL,
  `coms_pjoin_contenu` text,
  `coms_con_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_pjoin_id`)
);

ALTER TABLE `c_communications_pjoin` ADD `coms_pjoin_libelle` VARCHAR(250) NULL DEFAULT NULL AFTER `coms_pjoin_type`;
ALTER TABLE `c_communications_pjoin` ADD `coms_pjoin_paths` VARCHAR(250) NULL DEFAULT NULL AFTER `coms_pjoin_libelle`;
ALTER TABLE `c_communications_pjoin` ADD `coms_pjoin_size` FLOAT NULL DEFAULT NULL AFTER `coms_pjoin_contenu`;
ALTER TABLE `c_communications_pjoin` ADD `coms_id` INT NULL DEFAULT NULL AFTER `coms_con_id`;

CREATE TABLE IF NOT EXISTS `c_communications_destinataire` (
  `coms_des_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_des_clientId` int(11) DEFAULT NULL,
  `coms_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_des_id`)
);

ALTER TABLE `c_communications_destinataire` CHANGE `coms_des_clientId` `client_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `c_communications_destinataire` CHANGE `client_id` `client_destinataire_id` TEXT NULL DEFAULT NULL;

ALTER TABLE `c_communications` CHANGE `coms_status` `coms_status` INT(11) NULL DEFAULT '0';
ALTER TABLE `c_communications_pjoin` CHANGE `coms_pjoin_type` `coms_pjoin_type` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 19-09-2023
-- Table `c_franchise_loyer`
-- -----------------------------------------------------

ALTER TABLE `c_franchise_loyer` ADD `franchise_mode_calcul` INT NULL DEFAULT '0' AFTER `montant_ttc_deduit`, 
ADD `franchise_pourcentage` FLOAT NULL AFTER `franchise_mode_calcul`;

UPDATE `c_indice_loyer` SET `mode_calcul` = '2' WHERE `c_indice_loyer`.`indloyer_id` = 7;
INSERT INTO `c_indice_valeur_loyer` (`indval_id`, `indval_libelle`, `indval_valeur`, `indval_annee`, `indval_trimestre`, `indval_date_parution`, `indval_valide`, `util_prenom`, `indval_date_registre`, `util_prenom_publie`, `indval_date_publie`, `util_prenom_valide`, `indval_date_validation`, `indval_date_generation`, `indloyer_id`) VALUES 
(NULL, 'INDICE FIXE 2%', '2', '2003', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2004', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2005', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2006', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2007', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2008', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2009', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2010', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2011', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2012', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2013', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2014', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2015', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2016', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2017', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2018', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2019', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2020', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2021', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2022', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7'),
(NULL, 'INDICE FIXE 2%', '2', '2023', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7');


-- Mise à jour par Brillant le 27-09-2023
-- -----------------------------------------------------

ALTER TABLE `c_communications` ADD `util_validate` INT(4) NULL DEFAULT NULL AFTER `coms_date_validation`;
ALTER TABLE `c_communications` ADD `util_envoi` INT NULL DEFAULT NULL AFTER `coms_date_envoi`;
ALTER TABLE `c_communications` ADD `data_coms` LONGTEXT NULL DEFAULT NULL AFTER `util_id`;



-- Mise à jour par Philipine le 26-09-2023
-- -----------------------------------------------------

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES
(1, 'Administratif / Clôture du dossier au greffe ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(2, 'Administratif / Création d’activité au greffe ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(3, 'Administratif / Modification formulaire au greffe ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(4, 'Administratif / Changement d’adresse chez le gestionnaire et syndic et SIE'),
(5, 'Administratif / Inscription au CGA'),
(6, 'Gestion locative / Demande d’un compte d’exploitation au gestionnaire'),
(7, 'Gestion locative / Calcul d’un loyer variable'),
(8, 'Gestion des contentieux / Litige/question au paiement des loyers'),
(9, 'Gestion des contentieux / Litige/question lié à l’indexation'),
(10, 'Gestion des contentieux / Litige/question sur la TOM'),
(11, 'Gestion des contentieux / Litige/question sur une retenue déduite du loyer'),
(12, 'Gestion des contentieux / Litige/question sur des travaux'),
(13, 'Gestion des contentieux / Litige/question sur un autre point du bail'),
(14, 'Gestion des contentieux / Etablissement d’un CDP ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(15, 'Gestion des contentieux / Transmission d’un dossier à l’avocat ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(16, 'Gestion des contentieux / Envoie d’une LRAR ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(17, 'Gestion des contentieux / Gestion d’un avis à tiers détenteur ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(18, 'PNO / Gestion d’un sinistre ATTENTION VOIR MANDAT PRESTATION PAYANTE'),
(19, 'PNO / Mise en place d’une PNO'),
(20, 'Relation avec l’expert-comptable/CGA / Demande de dégrèvement suite dépôt tardif de l’expert-comptable'),
(21, 'Relation avec l’expert-comptable/CGA / Transmission de documents à l’expert-comptable'),
(22, 'Relation avec l’expert-comptable/CGA / Lien avec le CGA');

-- -----------------------------------------------------
-- Mise à jour par Philipine le 27-09-2023
-- ----------------------------------------------------
ALTER TABLE `c_document_tva` ADD `doc_type_montant` VARCHAR(250) NULL AFTER `doc_tva_date_creation`;

-- -----------------------------------------------------

-- Mise à jour par Brillant le 27-09-2023
-- ----------------------------------------------------
ALTER TABLE `c_communications_pjoin` CHANGE `coms_pjoin_contenu` `coms_pjoin_contenu` BLOB NULL DEFAULT NULL;

-- -----------------------------------------------------
-- Mise à jour par Brillant le 29-09-2023 (sql location meublée)
-- ----------------------------------------------------
ALTER TABLE `c_access_proprietaire` CHANGE `accp_login` `acc_lm_login` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- Dernière mis-à-jour le 04-10-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 04-10-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_mandat_sepa` (
  `sepa_id` int(11) NOT NULL AUTO_INCREMENT,
  `sepa_date_creation` date DEFAULT NULL,
  `sepa_date_envoi` date DEFAULT NULL,
  `sepa_iban` varchar(255) DEFAULT NULL,
  `sepa_rib` varchar(255) DEFAULT NULL,
  `sepa_bic` varchar(255) DEFAULT NULL,
  `sepa_lieu_signature` varchar(250) DEFAULT NULL,
  `sepa_date_signature` date DEFAULT NULL,
  `sepa_path` varchar(250) DEFAULT NULL,
  `sepa_etat` int(1) NOT NULL DEFAULT '1',
  `dossier_id` int(11) NOT NULL,
  `etat_mandat_id` int(11) NOT NULL,
  PRIMARY KEY (`sepa_id`)
) 
ENGINE=MyISAM AUTO_INCREMENT=100002 DEFAULT CHARSET=latin1;

-- Mise à jour par Tsilavina le 05-10-2023
-- Table `c_bail`
-- -----------------------------------------------------
ALTER TABLE `c_bail` ADD `bail_mode_calcul` INT NOT NULL DEFAULT '0' AFTER `bail_date_debut_mission`;
ALTER TABLE `c_bail` ADD `indice_valeur_plafonnement` INT NOT NULL DEFAULT '0' AFTER `bail_var_appliquee`;

-- Mise à jour par Philipine le 05-10-2023
ALTER TABLE `c_client` ADD `client_rof` FLOAT NULL AFTER `client_date_fin_validite_cni`;

-- Mise à jour par Brillant le 06-10-2023
-- Table `c_commnunications`

ALTER TABLE `c_communications` ADD `coms_date_demande` DATETIME NULL DEFAULT NULL AFTER `util_envoi`;
ALTER TABLE `c_communications` ADD `util_demande` INT NULL DEFAULT NULL AFTER `coms_date_demande`;

-- Mise à jour par Tsilavina le 09-10-2023
-- Table `c_bail`
-- -----------------------------------------------------
ALTER TABLE `c_bail` ADD `bail_facture` INT NOT NULL DEFAULT '0' AFTER `bail_valide`;

-- Attention ,cette requete est tres dangereuse
-- -----------------------------------------------------
UPDATE `c_bail` SET `bail_facture` = '1' WHERE `c_bail`.`bail_valide` = 1
-- Dernière mis-à-jour le 11-10-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 13-10-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_historique_adresse` (
  `hist_adresse_id` int(11) NOT NULL AUTO_INCREMENT,
  `hist_adresse_1_ancien` varchar(250) DEFAULT NULL,
  `hist_adresse_2_ancien` varchar(250) DEFAULT NULL,
  `hist_adresse_3_ancien` varchar(250) DEFAULT NULL,
  `hist_email_1_ancien` varchar(250) DEFAULT NULL,
  `hist_email_2_ancien` varchar(250) DEFAULT NULL,
  `hist_tel_1_ancien` varchar(120) DEFAULT NULL,
  `hist_tel_2_ancien` varchar(120) DEFAULT NULL,
  `paysmonde_id_ancien` int(11) DEFAULT NULL,
  `pays_id_ancien` int(11) DEFAULT NULL,
  `hist_adresse_1_nouv` varchar(250) DEFAULT NULL,
  `hist_adresse_2_nouv` varchar(250) DEFAULT NULL,
  `hist_adresse_3_nouv` varchar(250) DEFAULT NULL,
  `hist_email_1_nouv` varchar(250) DEFAULT NULL,
  `hist_email_2_nouv` varchar(250) DEFAULT NULL,
  `hist_tel_1_nouv` varchar(120) DEFAULT NULL,
  `hist_tel_2_nouv` varchar(120) DEFAULT NULL,
  `paysmonde_id_nouv` int(11) DEFAULT NULL,
  `pays_id_nouv` int(11) DEFAULT NULL,
  `hist_date_modif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`hist_adresse_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- -----------------------------------------------------
-- Mise à jour par Philipine le 17-10-2023
-- ----------------------------------------------------
ALTER TABLE `c_theme` CHANGE `theme_id` `theme_id` INT(11) NOT NULL AUTO_INCREMENT;
INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Autre');

ALTER TABLE `c_type_charge_lot` CHANGE `tpchrg_lot_nom` `tpchrg_lot_nom` VARCHAR(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

INSERT INTO `c_type_charge_lot` (`tpchrg_lot_id`, `tpchrg_lot_nom`, `tpchrg_lot_numero`, `tpchrg_lot_report_liasse_fiscale_ht`, `tpchrg_lot_report_decla_tva3562`) VALUES (NULL, 'Factures expert-comptable avocats Celavigestion et CGA', NULL, NULL, NULL), (NULL, 'Autres impôts', NULL, NULL, NULL), (NULL, 'Petits travaux', NULL, NULL, NULL), (NULL, 'Voyages et déplacements', NULL, NULL, NULL), (NULL, 'Autres factures', NULL, NULL, NULL);

-- Dernière mise en prod le 24-10-2024
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 25-10-2023
-- ----------------------------------------------------
ALTER TABLE `c_pret_lot` ADD `pret_capital_restant_du` FLOAT NULL AFTER `pret_assurance`;
ALTER TABLE `c_pret_lot` CHANGE `pret_capital_restant_du` `pret_capital_restant_du` DECIMAL(15,2) NULL DEFAULT NULL;


-- Dernière mise en prod le 26-10-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 27-10-2023
-- ----------------------------------------------------

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Ajout d\'emprunt'), (NULL, 'Modification d\'emprunt');
ALTER TABLE `c_pret_lot` ADD `pret_date_modif` DATE NULL AFTER `pret_capital_restant_du`;
ALTER TABLE `c_pret_lot` CHANGE `pret_date_modif` `pret_date_modif` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

-- -----------------------------------------------------
-- Mise à jour par Philipine le 31-10-2023
-- ----------------------------------------------------

ALTER TABLE `c_document_charge` ADD `charge_id` INT NULL AFTER `cliLot_id`;
ALTER TABLE `c_document_charge` ADD `util_client` INT NULL AFTER `charge_id`;
ALTER TABLE `c_document_charge` CHANGE `util_id` `util_id` INT(11) NULL;

-- Mise à jour par Brillant le 31-10-2023

CREATE TABLE IF NOT EXISTS `c_document_encaissement` (
  `doc_encaissement_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_encaissement_nom` varchar(255) NOT NULL,
  `doc_encaissement_path` varchar(255) NOT NULL,
  `doc_encaissement_creation` datetime NOT NULL,
  `clilot_id` int(11) NOT NULL,
  `enc_id` int(11) NOT NULL,
  PRIMARY KEY (`doc_encaissement_id`)
);

CREATE TABLE IF NOT EXISTS `c_type_encaissement` (
  `typeEnc_id` int(11) NOT NULL AUTO_INCREMENT,
  `typeEnc_libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`typeEnc_id`)
)

ALTER TABLE `c_encaissement` CHANGE `bail_id` `bail_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `c_encaissement` ADD `clilot_id` INT(11) NULL DEFAULT NULL AFTER `tpchrg_prod_id`;
ALTER TABLE `c_encaissement` ADD `typeEnc_id` INT(4) NULL DEFAULT NULL AFTER `clilot_id`;
ALTER TABLE `c_encaissement` ADD `enc_valide` TINYINT(1) NOT NULL DEFAULT '0' AFTER `enc_commentaire`;
ALTER TABLE `c_encaissement` ADD `enc_etat` TINYINT(1) NOT NULL DEFAULT '1' AFTER `enc_valide`;

INSERT INTO `c_type_encaissement` (`typeEnc_id`, `typeEnc_libelle`) VALUES (NULL, 'Loyer'), (NULL, 'TOM'), (NULL, 'Remboursement de charges'), (NULL, 'Autres');

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES ('28', 'Saisie de dépenses par le propriétaire');
INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Ajout encaissement'), (NULL, 'Modification encaissement');

-- -----------------------------------------------------
-- Mise à jour par Philipine le 03-10-2023
-- ----------------------------------------------------
ALTER TABLE `c_charge` ADD `charge_date_saisie` DATE NULL AFTER `cliLot_id`, ADD `charge_date_validation` DATE NULL AFTER `charge_date_saisie`, ADD `prenom_util` VARCHAR(120) NULL AFTER `charge_date_validation`;

ALTER TABLE `c_charge` ADD `charge_etat_valide` TINYINT(1) NOT NULL DEFAULT '0' AFTER `prenom_util`;

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 06-10-2023
-- ----------------------------------------------------
INSERT INTO `c_role_utilisateur` (`rol_util_id`, `rol_util_valeur`, `rol_util_libelle`) VALUES ('6', '6', 'Expert Comptable');

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 07-10-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_cloture_annuelle` ( 
  `cloture_id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id`int(11) NOT NULL,  
  `annee` varchar(255) NOT NULL, 
  `date_demande_validation` date NULL, 
  `util_demande_validation` int(11) NULL, 
  `date_validation` date NULL, 
  `util_validation` int(11) NULL, 
  `util_expert_comptable` int(11) NULL, 
  `etat` int(11) DEFAULT 0, 
  PRIMARY KEY (`cloture_id`) 
)

-- -----------------------------------------------------
-- Mise à jour par Philipine le 13-10-2023
-- ----------------------------------------------------
INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Saisie CNI par le propriétaire');

-- Dernière mise en prod le 20-11-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 20-11-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_type_charge_lot_intranet` (
  `tpchrg_lot_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpchrg_lot_nom` varchar(250) DEFAULT NULL,
  `tpchrg_lot_numero` int(11) DEFAULT NULL,
  `tpchrg_lot_report_liasse_fiscale_ht` int(11) DEFAULT NULL,
  `tpchrg_lot_report_decla_tva3562` int(11) DEFAULT NULL,
  PRIMARY KEY (`tpchrg_lot_id`)
)

--------------------------------------
--Mise à jour par Séverin le 21-11-2023
--------------------------------------
DROP TABLE IF EXISTS `c_type_charge_lot_en`;
CREATE TABLE IF NOT EXISTS `c_type_charge_lot_en` (
  `tpchrg_lot_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpchrg_lot_nom` varchar(250) DEFAULT NULL,
  `tpchrg_lot_numero` int(11) DEFAULT NULL,
  `tpchrg_lot_report_liasse_fiscale_ht` int(11) DEFAULT NULL,
  `tpchrg_lot_report_decla_tva3562` int(11) DEFAULT NULL,
  PRIMARY KEY (`tpchrg_lot_id`)
)

INSERT INTO `c_type_charge_lot_intranet` (`tpchrg_lot_id`, `tpchrg_lot_nom`, `tpchrg_lot_numero`, `tpchrg_lot_report_liasse_fiscale_ht`, `tpchrg_lot_report_decla_tva3562`) VALUES
(1, 'Appels de fonds du syndic', 0, 1, 0),
(2, 'Décomptes de charges du syndic –Autres charges ext', 342, 1, 1),
(4, 'Assurance Emprunt – Charges Financières', 294, 1, 0),
(5, 'Assurance PNO – Autres charges externes', 342, 1, 0),
(6, 'Frais de déplacement – Autres charges externes', 342, 1, 0),
(7, 'Energie – Autres charges externes', 342, 1, 1),
(8, 'Frais accessoire emprunt – Autres charges externes', 342, 1, 0),
(9, 'Commissions – Autres charges externes', 342, 1, 1),
(10, 'Honoraires avocat et tiers – Autres charges extern', 342, 1, 1),
(11, 'Honoraires de gestion – Autres charges externes', 342, 1, 1),
(12, 'Petit équipement – Autres charges externes', 342, 1, 1),
(13, 'Services hôtelier – Autres charges externes', 342, 1, 1),
(14, 'Publicité – Autres charges externes', 342, 1, 1),
(15, 'Internet – Autres charges externes', 342, 1, 1),
(16, 'Autres Charges – Autres charges externes', 342, 1, 1),
(17, 'Factures expert-comptable avocats Celavigestion et CGA', NULL, NULL, NULL),
(18, 'Autres impôts', NULL, NULL, NULL),
(19, 'Petits travaux', NULL, NULL, NULL);

-- Mise à jour par Brillant le 14-11-2023
-- ----------------------------------------------------
ALTER TABLE `c_lot_client` ADD `envoi_loye_gest` INT(1) NOT NULL DEFAULT '1' AFTER `comptabilite`;

-- -----------------------------------------------------
-- Mise à jour par Tsilavina le 17-11-2023
-- ----------------------------------------------------

CREATE TABLE IF NOT EXISTS `c_facture_automatique` ( 
  `fact_auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliLot_id`int(11) NOT NULL,  
  `doc_loyer_id` int(11) NOT NULL, 
  `gestionaire_id` int(11) NOT NULL,
  `date_facture` date NOT NULL, 
  `etat` int(11) DEFAULT 0, 
  PRIMARY KEY (`fact_auto_id`) 
)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `c_type_charge_lot_en`
--

INSERT INTO `c_type_charge_lot_en` (`tpchrg_lot_id`, `tpchrg_lot_nom`, `tpchrg_lot_numero`, `tpchrg_lot_report_liasse_fiscale_ht`, `tpchrg_lot_report_decla_tva3562`) VALUES
(1, 'Syndic calls of funds', 0, 1, 0),
(2, 'Syndic expenses statement', 342, 1, 1),
(4, 'Mortgage insurance', 294, 1, 0),
(5, 'Property insurance', 342, 1, 0),
(6, 'Travel expenses', 342, 1, 0),
(7, 'Energy', 342, 1, 1),
(8, 'Other mortage expenses', 342, 1, 0),
(9, 'Fees', 342, 1, 1),
(10, 'Lawyers fees', 342, 1, 1),
(11, 'Management fees', 342, 1, 1),
(12, 'Small equipment', 342, 1, 1),
(13, 'Hôtel services', 342, 1, 1),
(14, 'Advertising', 342, 1, 1),
(15, 'Internet', 342, 1, 1),
(16, 'Other expenses', 342, 1, 1),
(17, 'maintenance', NULL, NULL, NULL),
(18, 'Chartered accountant fees, CEALVIGESTION fees', NULL, NULL, NULL),
(19, 'Other taxes', NULL, NULL, NULL),
(20, 'small works', NULL, NULL, NULL);
COMMIT;

-- Dernière mise en prod le 22-11-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 22-11-2023
-- ----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_type_encaissement_en` (
  `typeEnc_id` int(11) NOT NULL AUTO_INCREMENT,
  `typeEnc_libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`typeEnc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `c_type_encaissement_en`
--

INSERT INTO `c_type_encaissement_en` (`typeEnc_id`, `typeEnc_libelle`) VALUES
(1, 'Rent'),
(2, 'Household refuse tax refund'),
(3, 'Expenses refund'),
(4, 'Other amount received');

-- -----------------------------------------------------
-- Mise à jour par Philipine le 22-11-2023
-- ----------------------------------------------------

ALTER TABLE `c_facture_automatique` ADD `etat_proprietaire` INT NOT NULL DEFAULT '0' AFTER `etat`;

-- -----------------------------------------------------
-- Mise à jour par Brillant le 22-11-2023
-- ----------------------------------------------------

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `pmail_langue`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES
(19, 'CELAVIGESTION -  Envoi facture Loyer', 'Modèle de mail utilisé lors de l\'envoi de factures de loyer. Il permet d\'envoyer la facture par mail au gestionnaire.', 'CEVALIGESTION -  @facture_Loyer', 'Bonjour,\r\n\r\nMerci de trouver ci-joint les informations  concernant la facture de loyer de notre client commun @nom_client propriétaire-bailleur dans la résidence @nom_programme\r\n\r\nVous voudrez bien effectuer le paiement sur le compte indiqué sur la facture.\r\n\r\nJe vous remercie, en cas de question tant sur le paiement que sur la facture jointe, de me contacter directement.\r\n\r\nCe message a été envoyé depuis CELAVIGESTION le @date_envoi_mail\r\n\r\nBien cordialement\r\n\r\n', 0, 1, 1, 1, 'Facture du loyer concernée en pièce jointe');

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `pmail_langue`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES (NULL, 'CELAVIGESTION - Envoi facture Loyer', 'Modèle de mail utilisé lors de l\'envoi de factures de loyer. Il permet d\'envoyer la facture par mail au propriétaire.', 'CEVALIGESTION - @facture_Loyer', 'Bonjour @nom_client @prenom_client,\r\n\r\nMerci de trouver ci-joint les informations concernant la facture de loyer de la résidence @nom_programme\r\n\r\nVous voudrez bien effectuer le paiement sur le compte indiqué sur la facture.\r\n\r\nJe vous remercie, en cas de question tant sur le paiement que sur la facture jointe, de me contacter directement.\r\n\r\nCe message a été envoyé depuis CELAVIGESTION le @date_envoi_mail\r\n\r\nBien cordialement\r\n\r\n', '0', '1', '1', '1', 'Facture du loyer concernée en pièce jointe');

-- -----------------------------------------------------
-- Mise à jour par Philipine le 23-11-2023
-- ----------------------------------------------------

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Ajout de la TOM');
ALTER TABLE `c_charge` ADD `charge_cloture` TINYINT(1) NOT NULL DEFAULT '0' AFTER `charge_etat_valide`;
ALTER TABLE `c_charge` ADD `charge_date_cloture` DATE NULL AFTER `charge_date_validation`;
INSERT INTO `c_etat_prospect` (`etat_prospect_id`, `etat_prospect_libelle`, `etat_couleur`) VALUES (NULL, 'Abandonné', '2C7BE5');

-- -----------------------------------------------------
-- Mise à jour par Brillant le 27-11-2023
-- ----------------------------------------------------
ALTER TABLE `c_encaissement` ADD `enc_control` INT(1) NOT NULL DEFAULT '0' AFTER `enc_id`;
ALTER TABLE `c_encaissement` ADD `enc_date_cloture` DATE NULL DEFAULT NULL AFTER `enc_control`;

-- Dernière mise en prod le 23-11-2023
-- New sql à mettre en prod

-- -----------------------------------------------------
-- Mise à jour par Philipine le 28-11-2023
-- ----------------------------------------------------

ALTER TABLE `c_charge` ADD `charge_cloture` INT NOT NULL DEFAULT '0' 
AFTER `charge_date_validation`, ADD `charge_date_cloture` DATE NULL AFTER `charge_cloture`;
-- -----------------------------------------------------
-- Mise à jour par Brillant le 29-11-2023
-- ----------------------------------------------------
ALTER TABLE `c_communications_destinataire` ADD `client_destinataire_error` TEXT NULL DEFAULT NULL AFTER `client_destinataire_id`;

-- -----------------------------------------------------

-- Mise à jour par Philipine le 30-11-2023
-- ----------------------------------------------------
ALTER TABLE `c_taxe_fonciere_lot` ADD `taxe_cloture` INT NOT NULL DEFAULT '0' AFTER `annee`;
ALTER TABLE `c_taxe_fonciere_lot` ADD `taxe_date_cloture` DATE NULL AFTER `taxe_cloture`;

-- Mise à jour par Brillant le 30-11-2023
-- ----------------------------------------------------
ALTER TABLE `c_encaissement` ADD `enc_info` TEXT NULL DEFAULT NULL AFTER `enc_control`;


-- Dernière mise en prod le 01-12-2023
-- New sql à mettre en prod

--Mise à jour par Séverin le 07/12/2023
ALTER TABLE c_charge
ADD COLUMN c_charge_commentaire VARCHAR(250) DEFAULT NULL;

ALTER TABLE c_pret_lot_presence
ADD COLUMN util_id INT DEFAULT NULL,
ADD COLUMN util_nom VARCHAR(250) DEFAULT NULL,
ADD COLUMN util_date_declar DATE DEFAULT NULL,
ADD COLUMN util_role VARCHAR(255) DEFAULT NULL;
UPDATE c_pret_lot_presence
SET util_date_declar = CURRENT_DATE,
    util_role = 'conseiller'
WHERE etat_presence = 2
    AND (util_date_declar IS NULL OR util_role IS NULL);

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES (NULL, 'Déclaration par le propriétaire qu\'il n\'y a pas de données pour l\'emprunt');

INSERT INTO `c_theme` (`theme_id`, `theme_libelle`) VALUES 
(NULL, 'Déclaration par le propriétaire d\'aucune dépense à saisir'), 
(NULL, 'Déclaration par le propriétaire que la saisie des dépenses est terminée\n');

--Mise à jour par Tsilavina le 07/12/2023

ALTER TABLE `c_facture_automatique` CHANGE `doc_loyer_id` `fact_loyer_id` INT(11) NOT NULL;


-----------------------------------------
--Mise à jour par Tsilavina le 12/12/2023
-----------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_confidentiel` (
        `doc_id` INT NOT NULL AUTO_INCREMENT,
        `doc_nom` VARCHAR(120) NULL,
        `doc_path` VARCHAR(120) NULL,
        `doc_date_creation` DATETIME NULL,
        `doc_etat` VARCHAR(11) NOT NULL DEFAULT 1,
        `util_id` INT(11) NOT NULL,
        `comment_etat` INT(11) NOT NULL DEFAULT 0,
        `commentaire` VARCHAR(255) NULL,
        `dossier_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_id`)
    ) ENGINE = InnoDB;

-----------------------------------------
--Mise à jour par Tsilavina le 13/12/2023
-----------------------------------------

CREATE TABLE
    IF NOT EXISTS `c_document_copropriete` (
        `doc_id` INT NOT NULL AUTO_INCREMENT,
        `doc_nom` VARCHAR(120) NULL,
        `doc_path` VARCHAR(120) NULL,
        `doc_date_creation` DATETIME NULL,
        `doc_etat` VARCHAR(11) NOT NULL DEFAULT 1,
        `util_id` INT(11) NOT NULL,
        `comment_etat` INT(11) NOT NULL DEFAULT 0,
        `commentaire` VARCHAR(255) NULL,
        `cliLot_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_id`)
    ) ENGINE = InnoDB;


    -- Dernière mise en prod le 14-12-2023
-- New sql à mettre en prod
    --Mise à jour par Séverin le 14/12/2023
ALTER TABLE c_dossier
ADD COLUMN mention_cga INT DEFAULT 0;

 --Mise à jour par Séverin le 15/12/2023
ALTER TABLE `c_document_pret` ADD `client_id` INT NULL AFTER `util_id`;

-- Dernière mise en prod le 15-12-2023
-- New sql à mettre en prod
--Mise à jour le 21/12/2023

ALTER TABLE `c_dossier` ADD `c_forme_juridique_id` INT NOT NULL DEFAULT '1' AFTER `mention_cga`, ADD INDEX (`c_forme_juridique_id`);
DROP TABLE IF EXISTS `c_forme_juridique`;
CREATE TABLE IF NOT EXISTS `c_forme_juridique` (
  `c_forme_juridique_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_forme_juridique_libelle` varchar(250) NOT NULL,
  PRIMARY KEY (`c_forme_juridique_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
INSERT INTO `c_forme_juridique` (`c_forme_juridique_id`, `c_forme_juridique_libelle`) VALUES
(1, 'Individuelle'),
(2, 'SARL'),
(3, 'SAS'),
(4, 'SASU'),
(5, 'EURL'),
(6, ' Entreprise de droit étranger');

--MEP 21/12/2023


-- Mise à jour par Brillant le 28/12/2023

-- abreviation : 
-- assiger_ECD_id -> assiger expert comptable à un dossier - identifiant
CREATE TABLE IF NOT EXISTS `c_expertcomptable_assiger_dossier` (
  `assiger_ECD_id` int(5) NOT NULL AUTO_INCREMENT, 
  `date_assigner` datetime DEFAULT NULL,
  `expert_id` int(5) NOT NULL,
  `dossier_id` int(5) NOT NULL,
  `util_id` int(5) NOT NULL,
  PRIMARY KEY (`assiger_ECD_id`)
);

-- abreviation : 
-- assiger_MD_id -> Mission expert comptable à un dossier - identifiant

CREATE TABLE IF NOT EXISTS `c_expertcomptable_mission_dossier` (
  `expert_MD_id` int(5) NOT NULL AUTO_INCREMENT,
  `assiger_ECD_id` int(5) NOT NULL,
  `date_creation`  datetime DEFAULT NULL,

  `date_debut_mission` datetime DEFAULT NULL,
  `date_resiliation_mission` datetime DEFAULT NULL,

  `util_id` int(5) NOT NULL,
  PRIMARY KEY (`expert_MD_id`)
);

ALTER TABLE `c_expertcomptable_mission_dossier` ADD `fichier_mission` TEXT NULL DEFAULT NULL AFTER `date_resiliation_mission`;
ALTER TABLE `c_expertcomptable_mission_dossier` ADD `fichier_resiliation` TEXT NULL DEFAULT NULL AFTER `fichier_mission`;
 --Mise à jour par Séverin le 05-01-2024--
 INSERT INTO `c_type_charge_lot_en` (`tpchrg_lot_id`, `tpchrg_lot_nom`, `tpchrg_lot_numero`, `tpchrg_lot_report_liasse_fiscale_ht`, `tpchrg_lot_report_decla_tva3562`) VALUES (NULL, 'Travel and trips', NULL, NULL, NULL), (NULL, 'Other invoices', NULL, NULL, NULL);
 --MEP 05-01-2024


 --Mise à jour par Séverin le 16-01-2024--
 ALTER TABLE `c_facturation_lot` ADD `annee` VARCHAR(250) NOT NULL AFTER `dossier_id`;
 UPDATE c_facturation_lot
SET annee = 2023;
ALTER TABLE `c_facturation_lot` ADD `c_facturation_indexation` FLOAT NOT NULL AFTER `annee`, ADD `c_facturation_commentaire` VARCHAR(250) NOT NULL AFTER `c_facturation_indexation`;
-- Mise à jour par Séverin le 22-01-2024
ALTER TABLE `c_facturation_lot` CHANGE `c_facturation_commentaire` `c_facturation_commentaire` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;

--Mise à jour par Tsilavina le 22-01-2024--
CREATE TABLE
    IF NOT EXISTS `c_document_facturation` (
        `doc_id` INT NOT NULL AUTO_INCREMENT,
        `doc_facturation_nom` VARCHAR(120) NULL,
        `doc_facturation_path` VARCHAR(255) NULL,
        `doc_facturation_creation` DATETIME NULL,
        `doc_facturation_etat` VARCHAR(11) NULL,
        `doc_facturation_annee` VARCHAR(10) NULL,
        `util_id` INT(11) NOT NULL,
        `fact_id` INT(11) NOT NULL,
        PRIMARY KEY (`doc_id`)
    ) ENGINE = InnoDB;

    INSERT INTO `c_article` (`art_id`, `art_code`, `art_libelle_fr`, `art_libelle_en`, `art_cpt_comptable`, `art_tarif`, `fam_id`, `type_fact_id`, `art_etat`) VALUES
(1, '0001', 'PRESTATIONS SELON MANDAT 2024', 'ACCORDING TO CONTRACT', '706100', 350, 1, 1, 1);

CREATE TABLE `c_facture` ( 
    `fact_id` int(11) NOT NULL, 
    `date_facture` datetime DEFAULT NULL, 
    `numero_facture` varchar(100) DEFAULT NULL, 
    `annee` varchar(120) DEFAULT NULL, 
    `montant_ht` float DEFAULT NULL, 
    `montant_tva` float DEFAULT NULL, 
    `montant_ttc` float DEFAULT NULL, 
    `article_id` int(11) DEFAULT NULL, 
    `dossier_id` int(11) DEFAULT NULL, 
    `cliLot_id` varchar(120) DEFAULT NULL, 
    `mode_paiement` int(11) DEFAULT NULL, 
    `etat_paiement` int(11) NOT NULL DEFAULT '0',
     PRIMARY KEY (`fact_id`) ) 
    ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Brillant : Mandat sepa 

ALTER TABLE `c_mandat_sepa` ADD `sepa_numero` VARCHAR(10) NOT NULL AFTER `sepa_id`;
------ 

UPDATE c_mandat_sepa
JOIN c_dossier ON c_mandat_sepa.dossier_id = c_dossier.dossier_id
SET c_mandat_sepa.sepa_numero = c_dossier.dossier_id;

------ 
ALTER TABLE `c_mandat_sepa` ADD `sepa_migration` INT(1) NULL DEFAULT NULL AFTER `etat_mandat_id`;
------

UPDATE c_mandat_sepa
SET 
    c_mandat_sepa.sepa_iban = COALESCE(c_mandat_sepa.sepa_iban, (
        SELECT c_dossier.iban
        FROM c_dossier
        WHERE c_mandat_sepa.dossier_id = c_dossier.dossier_id
    )),
    c_mandat_sepa.sepa_rib = COALESCE(c_mandat_sepa.sepa_rib, (
        SELECT c_dossier.file_rib
        FROM c_dossier
        WHERE c_mandat_sepa.dossier_id = c_dossier.dossier_id
    )),
    c_mandat_sepa.sepa_bic = COALESCE(c_mandat_sepa.sepa_bic, (
        SELECT c_dossier.bic
        FROM c_dossier
        WHERE c_mandat_sepa.dossier_id = c_dossier.dossier_id
    )),
    c_mandat_sepa.sepa_migration = 1
WHERE 
    c_mandat_sepa.sepa_iban IS NULL 
    AND c_mandat_sepa.sepa_rib IS NULL 
    AND c_mandat_sepa.sepa_bic IS NULL;


---------- 

UPDATE `c_article` SET `art_libelle_fr` = 'PRESTATIONS @annee SELON MANDAT', `art_libelle_en` = '@annee SERVICES ACCORDING TO MANDATE' WHERE `c_article`.`art_id` = 1;
ALTER TABLE `c_facture` ADD `mode_paiement` INT NULL AFTER `cliLot_id`, ADD `etat_paiement` INT NOT NULL DEFAULT '0' AFTER `mode_paiement`;

INSERT INTO `c_param_modelemail` (`pmail_id`, `pmail_libelle`, `pmail_description`, `pmail_objet`, `pmail_contenu`, `pmail_langue`, `emeteur_mail`, `signature`, `util_id`, `fichier_joint`) VALUES
(21, 'CELAVIGESTION - Envoi facture annuelle - clients en prélèvements', 'Modèle de mail utilisé lors de l\'envoi de factures annuelle. Il permet d\'envoyer la facture par mail au propriétaire.', 'CEVALIGESTION - @facture_annuelle', 'Cher(e) @prenom, @nom\n\n \nVous trouverez dans votre espace client CELAVIGESTION notre facture d’honoraires @annee concernant les travaux effectués pour votre compte.\n\nConformément au mandat que vous avez signé ainsi qu\'au mandat de prélèvement SEPA  , le montant de cette facture sera prélevé dans 15 jours. Nous vous serions reconnaissant de bien vouloir vérifier l’approvisionnement de votre compte afin d’éviter tous frais inutiles.\n\nNous vous remercions de votre confiance et  vous rappelons que Madame @chargee_de_clientele reste à votre disposition pour toute question concernant votre investissement en location meublée.\n\nCeci est un message automatique, merci de ne pas y répondre car votre réponse ne sera pas traitée. Pour toute question, veuillez vous adresser à votre chargée de clientèle.\n\n \nSincères salutations,\n\n\n L’équipe CELAVIGESTION\n\n', 0, 1, 1, 1, 'Facture  concernée en pièce jointe'),
(22, 'CELAVIGESTION - Envoi facture annuelle - clients en prélèvements', 'Modèle de mail utilisé lors de l\'envoi de factures annuelle. Il permet d\'envoyer la facture par mail au propriétaire.', 'CEVALIGESTION - @facture_annuelle', 'Dear @prenom @nom\n\n \nYou will find, in your personal CELAVIGESTION space,  our @annee invoice regarding the works carried out on your behalf.\n\nAccording to the mandate and the direct debit you signed with CELAVIGESTION, this invoice will be debited from your bank account in 15 days.\n\nWould you please check the balance of your bank account to ensure it is in credit,  in order to avoid any unnecessary charges.\n\nWe would like to thank you for your confidence and remind you that all our team and especially your customer care Mrs @chargee_de_clientele is available to answer any of your questions regarding your French property investment.\n\nThis is an automatic message, thank you for not answering this message. For any question, please contact your customer care.\n\n \n\nYours Sincerely\n\n \nYour CELAVIGESTION customer care\n', 2, 1, 1, 1, 'Facture  concernée en pièce jointe'),
(23, 'CELAVIGESTION - Envoi facture annuelle - encaissement clients', 'Modèle de mail utilisé lors de l\'envoi de factures annuelle. Il permet d\'envoyer la facture par mail au propriétaire.', 'CEVALIGESTION - @facture_annuelle', 'Cher(e) @prenom, @nom\n\n \nVous trouverez dans votre espace client CELAVIGESTION notre facture d’honoraires @annee concernant les travaux effectués pour votre compte.\n\nVous n’avez rien à faire, cette facture sera déduite de vos loyers encaissés au cours de ce trimestre.\n\nNous vous remercions de votre confiance et Madame @chargee_de_clientele reste à votre disposition pour toute question concernant votre investissement en location meublée.\n\nCeci est un message automatique, merci de ne pas y répondre car votre réponse ne sera pas traitée. Pour toute question, veuillez vous adresser à votre chargée de clientèle.\n\n \nSincères salutations\n\n \nL’équipe CELAVIGESTION\n', 0, 1, 1, 1, 'Facture  concernée en pièce jointe'),
(24, 'CELAVIGESTION - Envoi facture annuelle - encaissement clients', 'Modèle de mail utilisé lors de l\'envoi de factures annuelle. Il permet d\'envoyer la facture par mail au propriétaire.', 'CEVALIGESTION - @facture_annuelle', 'Dear @prenom @nom\n\n\nYou will find, in your personal CELAVIGESTION space, our @annee invoice regarding the works carried out on your behalf.\n\nYou have nothing to do as this invoice will be deducted from your rental incomes this quarter.\n\nWe would like to thank you for your confidence and remind you that all our team and especially your customer care Mrs @chargee_de_clientele is available to answer any of your questions regarding your French property investment.\n\nThis is an automatic message, thank you for not answering this message. For any question, please contact your customer care.\n\n \nYours Sincerely\n\n \n Your CELAVIGESTION customer care', 1, 1, 1, 1, 'Facture  concernée en pièce jointe');

'

-- Tsilavina : ICS 

CREATE TABLE
    IF NOT EXISTS `c_ics` (
        `ics_id` int(11) NOT NULL AUTO_INCREMENT,
        `nom_emeteur` varchar(255) DEFAULT NULL,
        `ics` varchar(255) DEFAULT NULL,
        PRIMARY KEY (`ics_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;


CREATE TABLE IF NOT EXISTS `c_prelevement` (
  `idc_prelevement` INT NOT NULL AUTO_INCREMENT,
  `date_encaissement` DATE NOT NULL,
  `id_banque` INT NOT NULL,
  `nom_fichier_sepa` INT NULL,
  `date_action` DATETIME NOT NULL,
  PRIMARY KEY (`idc_prelevement`)
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `c_reglements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_reglements` (
  `idc_reglements` INT NOT NULL AUTO_INCREMENT,
  `mode_reglement` INT NOT NULL,
  `date_reglement` DATE NOT NULL,
  `id_banque` INT NOT NULL,
  `dossier_id` INT NOT NULL,
  `montant_total` FLOAT NOT NULL,
  `libelle_reglement` VARCHAR(100) NULL,
  `date_action` DATETIME NOT NULL,
  `util_id` INT NOT NULL,
  `idc_prelevement` INT NULL,
  PRIMARY KEY (`idc_reglements`),
  INDEX `fk_c_reglements_c_prelevement1_idx` (`idc_prelevement` ASC),
  CONSTRAINT `fk_c_reglements_c_prelevement1`
    FOREIGN KEY (`idc_prelevement`)
    REFERENCES `c_prelevement` (`idc_prelevement`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `c_reglementVentilation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `c_reglementVentilation` (
  `idc_reglementVentilation` INT NOT NULL AUTO_INCREMENT,
  `dossier_id` INT NOT NULL,
  `id_facture` INT NOT NULL,
  `montant` FLOAT NOT NULL,
  `idc_reglements` INT NOT NULL,
  PRIMARY KEY (`idc_reglementVentilation`),
  INDEX `fk_c_reglementVentilation_c_reglements1_idx` (`idc_reglements` ASC),
  CONSTRAINT `fk_c_reglementVentilation_c_reglements1`
    FOREIGN KEY (`idc_reglements`)
    REFERENCES `c_reglements` (`idc_reglements`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = InnoDB;



--------- Brillant requete SQL 


SELECT * 
FROM c_mandat_sepa
Join c_lot_client ON c_lot_client.dossier_id = c_mandat_sepa.dossier_id 
Join c_mandat_new ON c_mandat_new.cliLot_id = c_lot_client.cliLot_id
WHERE c_lot_client.cliLot_principale = 1
AND c_mandat_sepa.sepa_date_signature IS NULL
AND c_mandat_new.mandat_date_signature IS NOT NULL 


--- ajouter champ de verification date signature mandat sepa
ALTER TABLE `c_mandat_sepa` ADD `sepa_migration_datesignature` INT(1) NULL DEFAULT NULL AFTER `sepa_migration`;


UPDATE c_mandat_sepa
JOIN c_lot_client ON c_lot_client.dossier_id = c_mandat_sepa.dossier_id
JOIN c_mandat_new ON c_mandat_new.cliLot_id = c_lot_client.cliLot_id
SET
  c_mandat_sepa.sepa_date_signature = c_mandat_new.mandat_date_signature,
  c_mandat_sepa.sepa_migration_datesignature = 1
WHERE
  c_lot_client.cliLot_principale = 1
  AND c_mandat_sepa.sepa_date_signature IS NULL
  AND c_mandat_new.mandat_date_signature IS NOT NULL;




-------- test recuperer liste c_dossier iban, bic, rib null , en lié avec lot principale
-------- et que iban , rib et bic dans le lot principal ne sont pas vide

SELECT * FROM c_dossier Join c_lot_client ON c_lot_client.dossier_id = c_dossier.dossier_id 
WHERE  c_lot_client.cliLot_principale = 1 
AND (c_dossier.iban IS NULL AND c_dossier.bic IS NULL AND c_dossier.file_rib IS NULL) 
AND ( c_lot_client.cliLot_iban_client IS NOT NULL AND c_lot_client.cliLot_bic_client IS NOT NULL AND c_lot_client.rib IS NOT NULL)


------------------  ajouter champ de verification iban , bic , rib

ALTER TABLE `c_dossier` ADD `dossier_migration` INT NULL DEFAULT NULL AFTER `c_forme_juridique_id`;


UPDATE c_dossier
JOIN c_lot_client ON c_lot_client.dossier_id = c_dossier.dossier_id
SET
  c_dossier.iban = c_lot_client.cliLot_iban_client,
  c_dossier.bic = c_lot_client.cliLot_bic_client,
  c_dossier.file_rib = c_lot_client.rib,
  c_dossier.dossier_migration = 1
WHERE
  c_lot_client.cliLot_principale = 1
  AND (c_dossier.iban IS NULL AND c_dossier.bic IS NULL AND c_dossier.file_rib IS NULL)
  AND (c_lot_client.cliLot_iban_client IS NOT NULL AND c_lot_client.cliLot_bic_client IS NOT NULL AND c_lot_client.rib IS NOT NULL);


-------------------- 


Update c_mandat_sepa
SET 
    c_mandat_sepa.sepa_date_signature = NULL,
    c_mandat_sepa.sepa_migration_datesignature = NULL
WHERE
    c_mandat_sepa.sepa_migration_datesignature = 1
    AND c_mandat_sepa.etat_mandat_id = 2
    AND c_mandat_sepa.sepa_date_signature IS NOT NULL

/**************/

ALTER TABLE `c_facture`  ADD `reglement_etat_modif` INT NOT NULL DEFAULT '0'  AFTER `mode_paiement`, 
ADD `commentaire_modification` TEXT NULL  AFTER `reglement_etat_modif`,  
ADD `ancien_etat_reglement` INT NULL  AFTER `commentaire_modification`;
---------------
--Séverin 14-02-2024--
--table c_document_prelevement---
CREATE TABLE IF NOT EXISTS `c_document_prelevement` (
  `doc_prelevement_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_prelevement_nom` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `doc_prelevement_path` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `doc_prelevement_creation` datetime DEFAULT NULL,
  `doc_prelevement_etat` varchar(11) DEFAULT NULL,
  `doc_prelevement_annee` varchar(10) DEFAULT NULL,
  `doc_prelevement_util_id` int(11) NOT NULL,
  `doc_prelevement_regleID` varchar(250) DEFAULT NULL,
  `doc_prelevement_prelevementID` int(11) NOT NULL,
  PRIMARY KEY (`doc_prelevement_id`)
) ENGINE=InnoDB;



--------------------------


SELECT * FROM `c_mandat_sepa` 
Join c_facture_annuelle ON c_facture_annuelle.dossier_id = c_mandat_sepa.dossier_id
WHERE `sepa_date_signature` = '2024-02-28' AND `etat_mandat_id` = 3 AND `sepa_iban` IS NULL

---------------------------


UPDATE `c_mandat_sepa`
JOIN `c_facture_annuelle` ON `c_facture_annuelle`.`dossier_id` = `c_mandat_sepa`.`dossier_id`
SET 
    `c_mandat_sepa`.`sepa_date_signature` = NOW(),
    `c_mandat_sepa`.`etat_mandat_id` = 3
WHERE 
    `c_mandat_sepa`.`sepa_date_signature` IS NULL 
    AND `c_mandat_sepa`.`etat_mandat_id` = 1;


------------------------------- 

UPDATE c_mandat_sepa
JOIN c_facture_annuelle ON c_facture_annuelle.dossier_id = c_mandat_sepa.dossier_id
JOIN c_dossier ON c_dossier.dossier_id = c_mandat_sepa.dossier_id
SET
  c_mandat_sepa.sepa_iban = c_dossier.iban,
  c_mandat_sepa.sepa_bic = c_dossier.bic,
  c_mandat_sepa.sepa_rib = c_dossier.file_rib
WHERE
  c_mandat_sepa.sepa_date_signature = '2024-02-28' 
  AND c_mandat_sepa.etat_mandat_id = 3 
  AND c_mandat_sepa.sepa_iban IS NULL;


-------------------------


UPDATE c_mandat_sepa
SET sepa_bic = UPPER(sepa_bic);



/****** ****/

UPDATE c_client
SET check_identite = 
    CASE 
        WHEN cin IS NULL OR cin = '' THEN 0
        ELSE 1
    END;


ALTER TABLE c_client
DROP COLUMN cin,
DROP COLUMN client_date_fin_validite_cni;


REATE TABLE IF NOT EXISTS `historique_verificationidentite` (
  `histvi_id` int(11) NOT NULL AUTO_INCREMENT,
  `histvi_date` datetime NOT NULL,
  `histvi_action` tinyint(4) NOT NULL COMMENT 'si 1 coché, si 0 decoché',
  `util_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`histvi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;