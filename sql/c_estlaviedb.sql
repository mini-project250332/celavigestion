-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 20 mai 2022 à 13:56
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `c_estlaviedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `c_client`
--

DROP TABLE IF EXISTS `c_client`;
CREATE TABLE IF NOT EXISTS `c_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_nom` varchar(120) DEFAULT NULL,
  `client_prenom` varchar(120) DEFAULT NULL,
  `client_entreprise` varchar(50) NOT NULL,
  `client_lieu_naissance` varchar(100) NOT NULL,
  `client_situation_matrimoniale` varchar(50) NOT NULL,
  `client_date_creation` date DEFAULT NULL,
  `client_date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_email` varchar(50) NOT NULL,
  `client_email1` varchar(50) NOT NULL,
  `client_phonemobile` varchar(120) DEFAULT NULL,
  `client_phonefixe` varchar(120) DEFAULT NULL,
  `client_adresse1` varchar(120) DEFAULT NULL,
  `client_adresse2` varchar(120) DEFAULT NULL,
  `client_adresse3` varchar(120) DEFAULT NULL,
  `client_cp` varchar(120) DEFAULT NULL,
  `client_ville` varchar(120) DEFAULT NULL,
  `client_pays` varchar(120) DEFAULT NULL,
  `client_nationalite` varchar(120) DEFAULT NULL,
  `client_commentaire` text,
  `etat_client_id` int(11) NOT NULL DEFAULT '1',
  `origine_cli_id` int(11) NOT NULL,
  `util_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_client`
--

INSERT INTO `c_client` (`client_id`, `client_nom`, `client_prenom`, `client_entreprise`, `client_lieu_naissance`, `client_situation_matrimoniale`, `client_date_creation`, `client_date_modification`, `client_email`, `client_email1`, `client_phonemobile`, `client_phonefixe`, `client_adresse1`, `client_adresse2`, `client_adresse3`, `client_cp`, `client_ville`, `client_pays`, `client_nationalite`, `client_commentaire`, `etat_client_id`, `origine_cli_id`, `util_id`) VALUES
(1, 'Manao', '', 'Entreprise I', '', '', '2022-05-17', '2022-05-17 09:48:57', 'brillantloroque@gmail.com', '', '+261346863299', '', 'Imandry Fianarantsoa / Madagascar', '', '', '301', 'Fianarantsoa', 'Madagascar', 'Française', 'test', 0, 1, 10),
(2, 'Marie ', 'Terrien', 'Celavigestion', '', '', '2022-05-13', '2022-05-17 14:05:51', 'brillantloroque@gmail.com', '', '+261346863299', '', '39 route de Fondeline, Parc de Brais', '', '', '44600', 'Saint-Nazaire', 'France', 'Française', 'Invite your friends and start working together in seconds. Everyone you invite will receive a welcome email.', 0, 5, 10),
(3, 'ANDRIAMAMPIADANA', 'Loroque Brillant', 'Entreprise III', '', 'Célibataire', '2022-05-19', '2022-05-18 12:21:50', 'brillantloroque@gmail.com', '', '+261346863299', '', 'Imandry Fianarantsoa / Madagascar', '', '', '301', 'Fianarantsoa', 'Madagascar', 'Française', 'Tets', 1, 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `c_communication_prospect`
--

DROP TABLE IF EXISTS `c_communication_prospect`;
CREATE TABLE IF NOT EXISTS `c_communication_prospect` (
  `comprospect_id` int(11) NOT NULL AUTO_INCREMENT,
  `comprospect_texte` varchar(250) DEFAULT NULL,
  `comprospect_date_creation` datetime NOT NULL,
  `prospect_id` int(11) NOT NULL,
  `util_id` int(11) NOT NULL,
  `type_comprospect_id` int(11) NOT NULL,
  PRIMARY KEY (`comprospect_id`),
  KEY `fk_c_communication_prospect_c_prospect1_idx` (`prospect_id`),
  KEY `fk_c_communication_prospect_c_type_communication_prospect1_idx` (`type_comprospect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_dossier`
--

DROP TABLE IF EXISTS `c_dossier`;
CREATE TABLE IF NOT EXISTS `c_dossier` (
  `dossier_id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_num` varchar(10) DEFAULT NULL,
  `dossier_libelle` varchar(120) DEFAULT NULL,
  `dossier_d_creation` datetime NOT NULL,
  `dossier_d_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dossier_etat` int(4) NOT NULL DEFAULT '1',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`dossier_id`),
  KEY `i_client` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_dossier`
--

INSERT INTO `c_dossier` (`dossier_id`, `dossier_num`, `dossier_libelle`, `dossier_d_creation`, `dossier_d_modification`, `dossier_etat`, `client_id`) VALUES
(2, '00001', 'Test', '2022-05-20 12:16:57', '2022-05-20 09:26:20', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `c_etat_prospect`
--

DROP TABLE IF EXISTS `c_etat_prospect`;
CREATE TABLE IF NOT EXISTS `c_etat_prospect` (
  `etat_prospect_id` int(11) NOT NULL AUTO_INCREMENT,
  `etat_prospect_libelle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`etat_prospect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_gestionnaire`
--

DROP TABLE IF EXISTS `c_gestionnaire`;
CREATE TABLE IF NOT EXISTS `c_gestionnaire` (
  `gestionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `gestionnaire_nom` varchar(120) DEFAULT NULL,
  `gestionnaire_adresse1` varchar(120) DEFAULT NULL,
  `gestionnaire_adresse2` varchar(120) DEFAULT NULL,
  `gestionnaire_adresse3` varchar(120) DEFAULT NULL,
  `gestionnaire_cp` varchar(45) DEFAULT NULL,
  `gestionnaire_ville` varchar(120) DEFAULT NULL,
  `gestionnaire_pays` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`gestionnaire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_historiqueacces`
--

DROP TABLE IF EXISTS `c_historiqueacces`;
CREATE TABLE IF NOT EXISTS `c_historiqueacces` (
  `hacces_id` int(11) NOT NULL AUTO_INCREMENT,
  `hacces_token` text,
  `hacces_datecreate` datetime DEFAULT NULL,
  `hacces_datelogin` datetime DEFAULT NULL,
  `hacces_datelogout` datetime DEFAULT NULL,
  `hacces_ip` varchar(20) DEFAULT NULL,
  `hacces_agent` varchar(20) DEFAULT NULL,
  `hacces_action` int(3) DEFAULT NULL,
  `hacces_status` int(11) DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hacces_id`),
  UNIQUE KEY `flowa_id_UNIQUE` (`hacces_id`),
  KEY `fk_c_historiqueacces_c_utilisateur1_idx` (`util_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_historiqueacces`
--

INSERT INTO `c_historiqueacces` (`hacces_id`, `hacces_token`, `hacces_datecreate`, `hacces_datelogin`, `hacces_datelogout`, `hacces_ip`, `hacces_agent`, `hacces_action`, `hacces_status`, `util_id`) VALUES
(1, 'c0fac08618b2639077c0f63942c417b79da2ecb0c77fddc3dc44b46fbfc72274', '2022-05-11 12:37:58', '2022-05-11 12:38:39', NULL, '::1', 'Chrome', 2, 1, 10),
(2, '879ba909d918e5d950e3964e38d3a801c4c1bf66134a1cc636f31360c015abb5', '2022-05-12 07:46:00', NULL, '2022-05-12 10:01:24', '::1', 'Chrome', 3, 1, NULL),
(3, '3af4db3079f8a14120410770035a2d13bb042cd4bdf093eeb26277c7a06c6c65', '2022-05-12 10:01:24', '2022-05-12 11:05:29', NULL, '::1', 'Chrome', 2, 1, 10),
(4, '6479a9e8bfb5d4ad7aefa14ab0f44a4e4e61522ec45da4712e0afcd638203a66', '2022-05-17 07:05:39', '2022-05-17 07:05:54', NULL, '::1', 'Chrome', 2, 1, 10),
(5, '92e76622ed37556c64ddd426290631914222a6e4f1848ffd9163562f64347c53', '2022-05-18 07:53:45', '2022-05-18 07:54:06', NULL, '::1', 'Chrome', 2, 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `c_lot_prospect`
--

DROP TABLE IF EXISTS `c_lot_prospect`;
CREATE TABLE IF NOT EXISTS `c_lot_prospect` (
  `lot_id` int(11) NOT NULL AUTO_INCREMENT,
  `lot_nom` varchar(120) DEFAULT NULL,
  `lot_adresse1` varchar(120) DEFAULT NULL,
  `lot_adresse2` varchar(120) DEFAULT NULL,
  `lot_adresse3` varchar(120) DEFAULT NULL,
  `lot_cp` varchar(45) DEFAULT NULL,
  `lot_ville` varchar(120) DEFAULT NULL,
  `lot_pays` varchar(120) DEFAULT NULL,
  `lot_principale` varchar(45) DEFAULT NULL,
  `prospect_id` int(11) NOT NULL,
  `gestionnaire_id` int(11) NOT NULL,
  `typelot_id` int(11) NOT NULL,
  `progm_id` int(11) NOT NULL,
  `typologielot_id` int(11) NOT NULL,
  PRIMARY KEY (`lot_id`),
  KEY `fk_c_lot_prospect_c_prospect1` (`prospect_id`),
  KEY `fk_c_lot_prospect_c_gestionnaire1` (`gestionnaire_id`),
  KEY `fk_c_lot_prospect_c_typelot1` (`typelot_id`),
  KEY `fk_c_lot_prospect_c_programme1` (`progm_id`),
  KEY `fk_c_lot_prospect_c_typologieLot1` (`typologielot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_origine_client`
--

DROP TABLE IF EXISTS `c_origine_client`;
CREATE TABLE IF NOT EXISTS `c_origine_client` (
  `origine_cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `origine_cli_libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`origine_cli_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_origine_client`
--

INSERT INTO `c_origine_client` (`origine_cli_id`, `origine_cli_libelle`) VALUES
(1, 'Nouveau client'),
(2, 'CGP'),
(3, 'Client CLV'),
(4, 'Client IPierre'),
(5, 'Client Synergestion');

-- --------------------------------------------------------

--
-- Structure de la table `c_origine_demande`
--

DROP TABLE IF EXISTS `c_origine_demande`;
CREATE TABLE IF NOT EXISTS `c_origine_demande` (
  `origine_dem_id` int(11) NOT NULL AUTO_INCREMENT,
  `origine_dem_libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`origine_dem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_origine_demande`
--

INSERT INTO `c_origine_demande` (`origine_dem_id`, `origine_dem_libelle`) VALUES
(1, 'Site internet'),
(2, 'Publicité'),
(3, 'Bouche à oreille'),
(4, 'Prescripteurs');

-- --------------------------------------------------------

--
-- Structure de la table `c_parcours_vente`
--

DROP TABLE IF EXISTS `c_parcours_vente`;
CREATE TABLE IF NOT EXISTS `c_parcours_vente` (
  `vente_id` int(11) NOT NULL AUTO_INCREMENT,
  `vente_libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`vente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_permission`
--

DROP TABLE IF EXISTS `c_permission`;
CREATE TABLE IF NOT EXISTS `c_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_section` varchar(45) DEFAULT NULL,
  `permission_libelle` varchar(45) DEFAULT NULL,
  `permission_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permission_id_UNIQUE` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_permissiontypeutilisateur`
--

DROP TABLE IF EXISTS `c_permissiontypeutilisateur`;
CREATE TABLE IF NOT EXISTS `c_permissiontypeutilisateur` (
  `permissiontutli_stamp` varchar(45) DEFAULT NULL,
  `permission_id` int(11) NOT NULL,
  `tutil_id` int(11) NOT NULL,
  KEY `fk_c_permissionuser_c_permission1_idx` (`permission_id`),
  KEY `fk_c_permissionuser_c_typeaccount1_idx` (`tutil_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_produit`
--

DROP TABLE IF EXISTS `c_produit`;
CREATE TABLE IF NOT EXISTS `c_produit` (
  `produit_id` int(11) NOT NULL AUTO_INCREMENT,
  `produit_libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`produit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_programme`
--

DROP TABLE IF EXISTS `c_programme`;
CREATE TABLE IF NOT EXISTS `c_programme` (
  `progm_id` int(11) NOT NULL AUTO_INCREMENT,
  `progm_nom` varchar(120) DEFAULT NULL,
  `progm_adresse1` varchar(120) DEFAULT NULL,
  `progm_adresse2` varchar(120) DEFAULT NULL,
  `progm_adresse3` varchar(120) DEFAULT NULL,
  `progm_cp` varchar(45) DEFAULT NULL,
  `progm_ville` varchar(120) DEFAULT NULL,
  `progm_pays` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`progm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_prospect`
--

DROP TABLE IF EXISTS `c_prospect`;
CREATE TABLE IF NOT EXISTS `c_prospect` (
  `prospect_id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_nom` varchar(120) DEFAULT NULL,
  `prospect_prenom` varchar(120) DEFAULT NULL,
  `prospect_email` varchar(120) DEFAULT NULL,
  `prospect_entreprise` varchar(120) DEFAULT NULL,
  `prospect_date_creation` datetime DEFAULT NULL,
  `prospect_date_modification` timestamp NULL DEFAULT NULL,
  `prospect_phonemobile` varchar(120) DEFAULT NULL,
  `prospect_phonefixe` varchar(120) DEFAULT NULL,
  `prospect_adresse1` varchar(120) DEFAULT NULL,
  `prospect_adresse2` varchar(120) DEFAULT NULL,
  `prospect_adresse3` varchar(120) DEFAULT NULL,
  `prospect_cp` varchar(120) DEFAULT NULL,
  `prospect_ville` varchar(120) DEFAULT NULL,
  `prospect_pays` varchar(120) DEFAULT NULL,
  `prospect_nationalite` varchar(120) DEFAULT NULL,
  `prospect_commentaire` text,
  `etat_prospect_id` int(11) NOT NULL,
  `origine_cli_id` int(11) NOT NULL,
  `origine_dem_id` int(11) NOT NULL,
  `produit_id` int(11) NOT NULL,
  `vente_id` int(11) NOT NULL,
  `util_id` int(11) NOT NULL,
  PRIMARY KEY (`prospect_id`),
  KEY `fk_c_prospect_etat_prospect1` (`etat_prospect_id`),
  KEY `fk_c_prospect_origine_client1` (`origine_cli_id`),
  KEY `fk_c_prospect_origine_demande1` (`origine_dem_id`),
  KEY `fk_c_prospect_produit1` (`produit_id`),
  KEY `fk_c_prospect_parcours_vente1` (`vente_id`),
  KEY `fk_c_prospect_c_utilisateur1` (`util_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_typelot`
--

DROP TABLE IF EXISTS `c_typelot`;
CREATE TABLE IF NOT EXISTS `c_typelot` (
  `typelot_id` int(11) NOT NULL,
  `typelot_libelle` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`typelot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_typelot`
--

INSERT INTO `c_typelot` (`typelot_id`, `typelot_libelle`) VALUES
(1, 'Non renseigné'),
(2, 'Bail commercial'),
(3, 'Bail habitation'),
(4, 'Mandat de gestion'),
(5, 'Location saisonnière');

-- --------------------------------------------------------

--
-- Structure de la table `c_typeutilisateur`
--

DROP TABLE IF EXISTS `c_typeutilisateur`;
CREATE TABLE IF NOT EXISTS `c_typeutilisateur` (
  `tutil_id` int(11) NOT NULL AUTO_INCREMENT,
  `tutil_libelle` varchar(45) DEFAULT NULL,
  `tutil_sigle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tutil_id`),
  UNIQUE KEY `tusers_id_UNIQUE` (`tutil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_typeutilisateur`
--

INSERT INTO `c_typeutilisateur` (`tutil_id`, `tutil_libelle`, `tutil_sigle`) VALUES
(1, 'Administrateur principal', 'root'),
(2, 'Administrateur', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `c_type_communication_prospect`
--

DROP TABLE IF EXISTS `c_type_communication_prospect`;
CREATE TABLE IF NOT EXISTS `c_type_communication_prospect` (
  `type_comprospect_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_comprospect_libelle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`type_comprospect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_typologielot`
--

DROP TABLE IF EXISTS `c_typologielot`;
CREATE TABLE IF NOT EXISTS `c_typologielot` (
  `typologielot_id` int(11) NOT NULL AUTO_INCREMENT,
  `typologielot_libelle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`typologielot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `c_utilisateur`
--

DROP TABLE IF EXISTS `c_utilisateur`;
CREATE TABLE IF NOT EXISTS `c_utilisateur` (
  `util_id` int(11) NOT NULL AUTO_INCREMENT,
  `util_nom` varchar(45) DEFAULT NULL,
  `util_prenom` varchar(45) DEFAULT NULL,
  `util_datenaissance` date DEFAULT NULL,
  `util_phonemobile` varchar(20) DEFAULT NULL,
  `util_phonefixe` varchar(20) DEFAULT NULL,
  `util_mail` varchar(50) DEFAULT NULL,
  `util_cp` varchar(10) DEFAULT NULL,
  `util_adresse` varchar(50) DEFAULT NULL,
  `util_ville` varchar(50) DEFAULT NULL,
  `util_photo` varchar(100) DEFAULT NULL,
  `util_login` varchar(50) DEFAULT NULL,
  `util_password` text,
  `util_status` int(11) DEFAULT NULL,
  `util_datecreate` datetime DEFAULT NULL,
  `util_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `util_etat` int(11) NOT NULL DEFAULT '1',
  `tutil_id` int(11) NOT NULL,
  PRIMARY KEY (`util_id`),
  UNIQUE KEY `users_id_UNIQUE` (`util_id`),
  KEY `fk_c_utilisateur_c_typeutilisateur1_idx` (`tutil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `c_utilisateur`
--

INSERT INTO `c_utilisateur` (`util_id`, `util_nom`, `util_prenom`, `util_datenaissance`, `util_phonemobile`, `util_phonefixe`, `util_mail`, `util_cp`, `util_adresse`, `util_ville`, `util_photo`, `util_login`, `util_password`, `util_status`, `util_datecreate`, `util_stamp`, `util_etat`, `tutil_id`) VALUES
(9, 'Tset', 'Loroque Brillant', '2022-05-04', NULL, ' 33346863299', 'mirana.manao@gmail.com', '301', 'Imandry Fianarantsoa / Madagascar', 'Fianarantsoa', NULL, 'tset', 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e', 0, '2022-05-04 07:11:36', '2022-05-09 05:32:10', 1, 2),
(10, 'ANDRIAMAMPIADANA', 'Loroque Brillant', '2022-05-11', '', '+33346863299', 'brillantloroque.manao@gmail.com', '301', 'Imandry Fianarantsoa / Madagascar', 'Fianarantsoa', NULL, 'brillantloroque.manao@gmail.com', '2a7251e27b97b73e1f12ef956b2bccf498cde373c0a05af295063c5aec4608ed7a755d78ae1de5f0c06a2a9b88449a4f3b1d4192d8944558012804adacb0220c', 1, '2022-05-11 07:07:42', '2022-05-11 07:07:42', 1, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `c_communication_prospect`
--
ALTER TABLE `c_communication_prospect`
  ADD CONSTRAINT `fk_c_communication_prospect_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_communication_prospect_c_type_communication_prospect1` FOREIGN KEY (`type_comprospect_id`) REFERENCES `c_type_communication_prospect` (`type_comprospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `c_historiqueacces`
--
ALTER TABLE `c_historiqueacces`
  ADD CONSTRAINT `fk_c_historiqueacces_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `c_lot_prospect`
--
ALTER TABLE `c_lot_prospect`
  ADD CONSTRAINT `fk_c_lot_prospect_c_gestionnaire1` FOREIGN KEY (`gestionnaire_id`) REFERENCES `c_gestionnaire` (`gestionnaire_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_lot_prospect_c_programme1` FOREIGN KEY (`progm_id`) REFERENCES `c_programme` (`progm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_lot_prospect_c_prospect1` FOREIGN KEY (`prospect_id`) REFERENCES `c_prospect` (`prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_lot_prospect_c_typelot1` FOREIGN KEY (`typelot_id`) REFERENCES `c_typelot` (`typelot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_lot_prospect_c_typologieLot1` FOREIGN KEY (`typologielot_id`) REFERENCES `c_typologielot` (`typologielot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `c_permissiontypeutilisateur`
--
ALTER TABLE `c_permissiontypeutilisateur`
  ADD CONSTRAINT `fk_c_permissionuser_c_permission1` FOREIGN KEY (`permission_id`) REFERENCES `c_permission` (`permission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_permissionuser_c_typeaccount1` FOREIGN KEY (`tutil_id`) REFERENCES `c_typeutilisateur` (`tutil_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `c_prospect`
--
ALTER TABLE `c_prospect`
  ADD CONSTRAINT `fk_c_prospect_c_utilisateur1` FOREIGN KEY (`util_id`) REFERENCES `c_utilisateur` (`util_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_prospect_etat_prospect1` FOREIGN KEY (`etat_prospect_id`) REFERENCES `etat_prospect` (`etat_prospect_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_prospect_origine_client1` FOREIGN KEY (`origine_cli_id`) REFERENCES `origine_client` (`origine_cli_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_prospect_origine_demande1` FOREIGN KEY (`origine_dem_id`) REFERENCES `origine_demande` (`origine_dem_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_prospect_parcours_vente1` FOREIGN KEY (`vente_id`) REFERENCES `parcours_vente` (`vente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_c_prospect_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`produit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `c_utilisateur`
--
ALTER TABLE `c_utilisateur`
  ADD CONSTRAINT `fk_c_utilisateur_c_typeutilisateur1` FOREIGN KEY (`tutil_id`) REFERENCES `c_typeutilisateur` (`tutil_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
