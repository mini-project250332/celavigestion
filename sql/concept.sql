/***** communications *****/

/*
CREATE TABLE IF NOT EXISTS `c_communications_type` (
  `coms_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_type_libelle` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`coms_type_id`)
);


CREATE TABLE IF NOT EXISTS `c_communications_modeleContenu` (
  `coms_modeleC_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_modeleC_libelle` varchar(250) DEFAULT NULL,
  `coms_modeleC_date_creation` datetime DEFAULT NULL,
  `coms_modeleC_date_validation` datetime DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_id`)
);

*/
/**** ****/

CREATE TABLE IF NOT EXISTS `c_communications` (
  `coms_id` int(11) NOT NULL AUTO_INCREMENT,
  `coms_libelle` varchar(250) DEFAULT NULL,
  `coms_date_creation` datetime DEFAULT NULL,
  `coms_date_validation` datetime DEFAULT NULL,
  `coms_date_envoi` datetime DEFAULT NULL,
  `coms_type_id` tinyint(1) DEFAULT NULL,
  `coms_etat` tinyint(1) DEFAULT NULL,
  `util_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coms_id`)
);

ALTER TABLE `c_communications` ADD `coms_description` TEXT NOT NULL AFTER `coms_libelle`;
ALTER TABLE `c_communications` ADD `coms_status` INT NULL DEFAULT NULL AFTER `coms_date_envoi`;

