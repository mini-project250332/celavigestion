ALTER TABLE `c_article` ADD `art_code_tva` VARCHAR(120) NOT NULL AFTER `art_cpt_comptable`, 
ADD `art_pourcentage_tva` FLOAT NOT NULL AFTER `art_code_tva`, 
ADD `art_compte_comptable` VARCHAR(255) NOT NULL AFTER `art_pourcentage_tva`;