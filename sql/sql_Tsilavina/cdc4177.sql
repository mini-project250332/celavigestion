-- Ajout colonne transcomp_id nullable, numéro transfert ( clés étrangère)
ALTER TABLE `c_facture` ADD `transcomp_id` INT NULL DEFAULT NULL COMMENT 'numéro transfert' AFTER `fact_JustificationAvoir`;
ALTER TABLE `c_reglements` ADD `transcomp_id` INT NULL DEFAULT NULL COMMENT 'numéro transfert' AFTER `id_reglementDu`;

CREATE TABLE IF NOT EXISTS `c_transfert_comptable` (
  `transcomp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `transcomp_date` DATETIME DEFAULT NULL,
  `transcomp_util_id` INT(11) NOT NULL,
  `transcomp_nbecrit` INT(11)  NULL,
  `transcomp_fichier` VARCHAR(255) NULL,
  PRIMARY KEY (`transcomp_id`)
) ENGINE=InnoDB;