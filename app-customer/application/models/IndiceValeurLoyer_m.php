<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class IndiceValeurLoyer_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_indice_valeur_loyer";
    protected $_primary_key = "indval_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    function orderByDateParution($indloyer_id)
    {
        $sql = "SELECT *, STR_TO_DATE(`indval_date_parution`, '%d-%m-%Y') as dt FROM `c_indice_valeur_loyer` WHERE indloyer_id = $indloyer_id ORDER BY dt ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
}
