<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Program_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_programme";
    protected $_primary_key = "progm_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
