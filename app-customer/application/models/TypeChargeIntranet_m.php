<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TypeChargeIntranet_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_charge_lot_intranet";
    protected $_primary_key = "tpchrg_lot_id ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}