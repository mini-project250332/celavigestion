<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AccesClient_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_access_proprietaire_client";
    protected $_primary_key = "accp_id ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
