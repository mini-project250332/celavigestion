<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utilisateur_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_utilisateur_client";
    protected $_primary_key = "util_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
