<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class DocumentLoyer_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_document_loyer";
    protected $_primary_key = "doc_loyer_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function GetbyYear($cliLot_id, $year)
    {
        $res = $this->db->select('*')
            ->from($this->_table)
            ->join('c_utilisateur', 'c_utilisateur.util_id = c_document_loyer.util_id')
            ->join('c_facture_loyer', 'c_facture_loyer.fact_loyer_id  = c_document_loyer.fact_loyer_id ')
            ->where('cliLot_id = ' . $cliLot_id . ' AND doc_loyer_annee = ' . $year . ' AND doc_loyer_etat = 1')
            ->get()
            ->result();

        $res_doc_null = $this->db->select('*')
            ->from($this->_table)
            ->join('c_utilisateur', 'c_utilisateur.util_id = c_document_loyer.util_id')
            ->where('cliLot_id = ' . $cliLot_id . ' AND doc_loyer_annee = ' . $year . ' AND doc_loyer_etat = 1 AND c_document_loyer.fact_loyer_id is null')
            ->get()
            ->result();

        return array_merge($res, $res_doc_null);
    }

    public function get_document($table)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->where_in('doc_loyer_id', $table)
            ->get()
            ->result();
        return $res;
    }
}