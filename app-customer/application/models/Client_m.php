<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_client";
    protected $_primary_key = "client_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function update_password($email, $password)
    {
        $res = $this->db
            ->set('util_password', $password, TRUE)
            ->where('util_mail = "' . $email . '"')
            ->update($this->_table);
        return $res;
    }
}
