<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LienIntranet_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_lien_active_intranet";
    protected $_primary_key = "id ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
