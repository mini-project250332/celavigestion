<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TypeEcheance_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_echeance";
    protected $_primary_key = "tpech_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
