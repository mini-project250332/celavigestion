<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TypeProduitLotEncaissement_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_type_produit_lot_encaissement";
    protected $_primary_key = "tpchrg_prod_id";
    protected $_order = "";
    protected $_filter = "intval";
}
