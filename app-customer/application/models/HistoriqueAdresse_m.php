<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class HistoriqueAdresse_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_historique_adresse";
    protected $_primary_key = "hist_adresse_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
