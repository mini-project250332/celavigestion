<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acte_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_acte";
    protected $_primary_key = "acte_id";
    protected $_order = "";
    protected $_filter = "intval";

}
