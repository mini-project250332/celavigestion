<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Taxe_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_taxe_fonciere_lot";
    protected $_primary_key = "taxe_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function getpasttaxe($cliLot_id)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->where('cliLot_id = ' . $cliLot_id . ' AND annee < ' . date('Y'))
            ->get()
            ->result();
        return $res;
    }
}
