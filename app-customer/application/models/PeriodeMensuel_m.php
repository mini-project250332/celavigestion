<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PeriodeMensuel_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_periode_mensuel";
    protected $_primary_key = "periode_mens_id ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
