<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class IndiceLoyer_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_indice_loyer";
    protected $_primary_key = "indloyer_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
