<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Historiqueacces_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_historiqueacces_client";
    protected $_primary_key = "hacces_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
