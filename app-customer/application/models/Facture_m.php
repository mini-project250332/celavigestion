<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facture_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_facture";
    protected $_primary_key = "fact_id";
    protected $_order = "";
    protected $_filter = "intval";
}
