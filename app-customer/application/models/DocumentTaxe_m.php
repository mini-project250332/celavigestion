<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DocumentTaxe_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_document_lot_taxe";
    protected $_primary_key = "doc_taxe_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function get_document($table)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->where_in('doc_taxe_id ', $table)
            ->get()
            ->result();
        return $res;
    }
}
