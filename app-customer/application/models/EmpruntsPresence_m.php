<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmpruntsPresence_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_pret_lot_presence";
    protected $_primary_key = "id_p_presence ";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }
}
