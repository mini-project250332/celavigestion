<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bail_m extends CLI_Model
{
    protected $_base_donnee_defaut = FALSE;
    protected $_table = "c_bail";
    protected $_primary_key = "bail_id";
    protected $_order = "";
    protected $_filter = "intval";

    function __construct()
    {
        parent::__construct();
    }

    public function GetbyYear($cliLot_id)
    {
        $res = $this->db->select("*")
            ->from($this->_table)
            ->join("c_lot_client", "c_lot_client.cliLot_id = c_bail.cliLot_id")
            ->join("c_periodicite_loyer", "c_periodicite_loyer.pdl_id = c_bail.pdl_id")
            ->join("c_nature_prise_effet", "c_nature_prise_effet.natef_id = c_bail.natef_id")
            ->join("c_type_bail", "c_type_bail.tpba_id = c_bail.tpba_id")
            ->join("c_gestionnaire", "c_gestionnaire.gestionnaire_id = c_bail.gestionnaire_id")
            ->join("c_type_echeance", "c_type_echeance.tpech_id = c_bail.tpech_id")
            ->join("c_bail_art", "c_bail_art.bart_id = c_bail.bail_art_605")
            ->join("c_indice_loyer", "c_indice_loyer.indloyer_id = c_bail.indloyer_id", 'left')
            ->join("c_moment_facturation_bail", "c_moment_facturation_bail.momfac_id = c_bail.momfac_id")
            ->join("c_periode_indexation", "c_periode_indexation.prdind_id = c_bail.prdind_id")
            ->where('c_bail.cliLot_id = ' . $cliLot_id)
            ->get()
            ->result();
        return $res;
    }
}
