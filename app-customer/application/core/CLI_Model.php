<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    class CLI_Model extends CI_Model {
        
        // Default paramètres Model
        protected $_base_donnee_defaut = TRUE;
        protected $_table = "";
        protected $_primary_key = "";
        protected $_order = "";
        protected $_filter = "intval";

        public function __construct(){
            parent::__construct();
           
        }

        // Default Fonctions model
        
        // Fonction save and update data
        function save_data( $data, $clause = NULL){
            $res = FALSE;
            if( $clause && is_array($clause) ) {
                $this->db->where ($clause);
                $request = $this->db->update($this->_table, $data);

                if ($request) {
                    $res = $clause;
                }
            } else {
                $request = $this->db->insert($this->_table, $data);
                if ($request) {
                    $res = $this->db->insert_id();
                }
            }
            return $res;
        }

        function getInsertID() {
            $res = $this->db->insert_id();
            return $res;
        }

        // Fonction delete data
        function delete_data( $clause ) {
            $res = FALSE;
            if( $clause && is_array($clause) ) {
                $this->db->where($clause);
                $res = $this->db->delete($this->_table);
            }
            return $res;
        }

        // Fonction multiple insert data (-batch-)
        function multi_insert_data($data){
            $res = FALSE;
            $this->db->insert_batch($this->_table,$data);
            $request =  $this->db->affected_rows();
            if ($request) {
                $res = TRUE;
            }
            return $res;
        }

        // Fonction multiple update data (-batch-)
        function multi_update_data($data){
            $res = FALSE;
            $request =  $this->db->update_batch($this->_table,$data,$this->_primary_key);
            if ($request) {
                $res = TRUE;
            }
            return $res;
        }

        // Fonction execute  SQL query in input
        function request_query( $sql, $is_update_query = false, $method = "result"){
            if( is_string($sql) ) {
                $res = $this->db->query( $sql );
                if( !$is_update_query && $res ) {
                    return $res->$method();
                } else {
                    return $res;
                }
            }
        }

        // Function Get data
        /*
            $params_get = array(
                'distinct' => FALSE,
                'clause' => NULL,
                'or_clause' => NULL,
                'like' => NULL,
                'or_like' => NULL,
                'method' => "result",
                'columns' => "*" ,
                'join' => NULL,
                'order_by_columns' => NULL,
                'limit' => NULL, // array('offset' => Int , 'limit' => Int) // Int
                'join_orientation' => NULL 
            );
        */
        function get($params_get){
            // Distinct result
            if ( isset($params_get['distinct']) ){
                if ( $params_get['distinct'] ) $this->db->distinct();
            }
            // table
            $this->db->from( $this->_table );
            // columns table return
            if ( isset($params_get['columns']) ){ 
                $columns = $params_get['columns'];
                $_columns = ""; 
                if ( is_array( $columns ) ){
                    $count_columns = count( $columns );
                    $i = 0;
                    foreach( $columns as $col ) {
                        $_columns .= $col;
                        $i++;
                        if( $i < $count_columns ) $_columns .= ","; 
                    }
                } else {
                    $_columns = $columns;
                }
            }else{
                $_columns = "*";
            }
            
            $this->db->select( $_columns );
            // Join tables
            if ( isset($params_get['join']) ){
                $_join_tables = $params_get['join'];
                if ( isset($params_get['join_orientation']) ){
                    $_join_orient = $params_get['join_orientation'];
                }else{
                    $_join_orient = NULL;
                }
                if ( is_array($_join_tables) ) {
                    if( !is_null($_join_orient )){
                        foreach( $_join_tables as $table => $column_join ){
                            $this->db->join($table, $column_join, $_join_orient);
                        }               
                    }else{
                        foreach( $_join_tables as $table => $onField ){
                            $this->db->join($table, $onField);
                        }
                    }
                }
            }
            // Order by
            if( isset($params_get['order_by_columns']) ) $this->db->order_by( $params_get['order_by_columns'] );
            // Limit
            if( isset($params_get['limit']) ){
                if(is_array($params_get['limit'])){
                    if(isset($params_get['limit']['offset']) && isset($params_get['limit']['limit']))
                        $this->db->limit(intval($params_get['limit']['offset']),intval($params_get['limit']['limit']));
                }else{
                    $this->db->limit(intval($params_get['limit']));
                }
            }
            // Clause where
            if(  isset($params_get['clause']) && is_array( $params_get['clause'] ) ) $this->db->where( $params_get['clause'] );
            // Clause where_in
            if(  isset($params_get['clause_in']) && is_array( $params_get['clause_in'] ) ) $this->db->where_in( $params_get['clause_in'] );
            // Clause or_where
            if( isset($params_get['or_clause']) && is_array( $params_get['or_clause'] ) ) $this->db->or_where( $params_get['or_clause'] );
            // Clause where_not_in
            if( isset($params_get['clause_not_in']) && is_array( $params_get['clause_not_in'] ) ) $this->db->where_not_in( $params_get['clause_not_in'] );
            // Clause like
            if( isset($params_get['like']) && is_array($params_get['like']) ) $this->db->like( $params_get['like'] );
            // Clause or_like
            if( isset($params_get['or_like']) && is_array( $params_get['or_like'] ) ) $this->db->or_like( $params_get['or_like'] );
            // method
            if( isset($params_get['method']) ){
                $method = $params_get['method'];
            }else{
                $method = "result";
            }
            $res = $this->db->get()->$method();
            // print_r( $res );  
            //omeo anah le requête exécutés
            // return
            return $res;
        }
    }