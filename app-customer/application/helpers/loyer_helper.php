<?php

if( ! function_exists("genererListeMoisDansAnnee") ) {

    function genererListeMoisDansAnnee( $annee, $full = false )
    {
        $listeMois = array();

        if( ! intval($annee) ) {
            return null;
        }

        for( $numeroMois = 1; $numeroMois < 13; $numeroMois++ )
        {
            $mois["debut"] = premierJourMois( $annee, $numeroMois);
            $mois["fin"] = dernierJourMois($annee, $numeroMois);
            $mois["nom"] = nomMois($numeroMois, $full) ;

            array_push($listeMois, $mois);
        }

        return $listeMois;
    }
}


if( ! function_exists("genererListeMoisDansAnneeEnglish") ) {

    function genererListeMoisDansAnneeEnglish( $annee, $full = false )
    {
        $listeMois = array();

        if( ! intval($annee) ) {
            return null;
        }

        for( $numeroMois = 1; $numeroMois < 13; $numeroMois++ )
        {
            $mois["debut"] = premierJourMois( $annee, $numeroMois);
            $mois["fin"] = dernierJourMois($annee, $numeroMois);
            $mois["nom"] = nomMoisEnglish($numeroMois, $full) ;

            array_push($listeMois, $mois);
        }

        return $listeMois;
    }
}



if( ! function_exists("premierJourMois") ) {
    function premierJourMois( $annee, $mois ) {
        return date("Y-m-d", strtotime($mois.'/01/'.$annee.' 00:00:00'));
    }
}
   

if( ! function_exists("dernierJourMois") ) {
    function dernierJourMois($annee, $mois) {
        return date("Y-m-d", strtotime('-1 second',strtotime('+1 month',strtotime($mois.'/01/'.$annee.' 00:00:00'))));
    }
}

if( ! function_exists("nomMois") ) {
    function nomMois( $mois, $full = false) {
        $months = ["", "Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sept", "Oct", "Nov", "Déc"];
        $fullMonths = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
        $listeMois = null;
        if($full) {
            $listeMois = $fullMonths;
        } else {
            $listeMois = $months;
        }

        $index = (intval($mois)) ? intval($mois) : 0;
        return $listeMois[$index];
    }
}


if( ! function_exists("nomMoisEnglish") ) {
    function nomMoisEnglish( $mois, $full = false) {
        $months = ["", "Jan", "Feb", "March", "April", "May", "June", "July", "August", "Sept", "Oct", "Nov", "Dec"];
       // $fullMonths = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
       $fullMonths = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $listeMois = null;
        if($full) {
            $listeMois = $fullMonths;
        } else {
            $listeMois = $months;
        }

        $index = (intval($mois)) ? intval($mois) : 0;
        return $listeMois[$index];
    }
}

