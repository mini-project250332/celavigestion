<?php
    if(!defined('BASEPATH')) exit('No direct script access allowed');

    function download_file($file){

		$file_location = base_url().$file['docp_path']; 
        $file_name = $file['docp_path']; 

        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename=\"". basename($file_name) ."\""); 

        readfile ($file_location); 
        exit(); 

	}
?>