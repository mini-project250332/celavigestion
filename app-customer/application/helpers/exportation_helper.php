<?php

if (!function_exists("exporation_pdf")) {
    function exporation_pdf($view_file, $filename, $data = null, $lot_id, $annee, $fact_loyer_id = null)
    {
        $CI = get_instance();

        if (substr(PHP_VERSION, 0, 6) === "7.3.21" || substr(PHP_VERSION, 0, 6) === "7.3.26") {
            $CI->load->library('mpdf-5.7-php7/Mpdf');
        } else {
            $CI->load->library('mpdf-5.7-php5/mpdf', 'mpdf');
        }

        $CI->load->helper("url");
        $CI->load->model('DocumentLoyer_m');

        $mpdf = new \Mpdf\Mpdf();

        $html = (is_null($data)) ? $CI->load->view($view_file, array(), true) : $CI->load->view($view_file, $data, true);

        $html = mb_convert_encoding($html, "UTF-8");

        $mpdf->WriteHTML($html);

        $string = str_replace('-', '_', $filename);
        $pdf_save_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $url = $pdf_save_name . '.pdf';

        $url_path = APPPATH . "../../documents/clients/lots/Loyer/$annee/$lot_id";
        $url_doc = "../documents/clients/lots/Loyer/$annee/$lot_id/$url";

        makeDirPath($url_path);

        $data = array(
            'doc_loyer_nom' => $filename,
            'doc_loyer_path' => str_replace('\\', '/', $url_doc),
            'doc_loyer_creation' => date('Y-m-d H:i:s'),
            'doc_loyer_etat' => 1,
            'doc_loyer_annee' => $annee,
            'cliLot_id ' => $lot_id,
            'util_id' => $_SESSION['session_utilisateur_client']['util_id'],
            'fact_loyer_id' => $fact_loyer_id
        );

        $CI->load->DocumentLoyer_m->save_data($data);

        $url_file = $url_path . "/" . $pdf_save_name . ".pdf";
        $mpdf->Output($url_file, "F");
    }
}

if (!function_exists("makeDirPath")) {
    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }
}
