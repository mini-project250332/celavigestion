<?php $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1; ?>

<div class="h-100 w-100 p-0 py-1 add-tom" style="overflow-y:auto;">
    <div id="content-pret-customer_<?= $cliLot_id ?>">
        <div class="col-12 p-0 py-1 fiche-pret_<?= $cliLot_id ?> <?= !empty($doc_pret) && !empty($presence_pret) && $presence_pret->etat_presence == 1 ? "" : "d-none" ?>">
            <div class="row m-0">
                <div class="col p-1">
                    <div class="card shadow p-3 mb-5 rounded-0 h-100">
                        <div class="card-body p-0">
                            <h5 class="text-center">
                                <?= $this->lang->line('info_titre_pret') . ' ' . $program->progm_nom ?></span>

                            </h5>
                            <h5 class="information"> <?= $this->lang->line('title_tom_information'); ?> </h5><br>
                            <table class="table table-borderless mb-0">
                                <tbody>
                                    <tr class="border-bottom">
                                        <th class="ps-0"><?= $this->lang->line('document_tom'); ?></th>
                                        <td class="pe-0">
                                            <?php if (!empty($doc_pret)) : ?>
                                                <?php foreach ($doc_pret as $doc_prets) : ?>
                                                    <span class="badge rounded-pill badge-soft-info" style="cursor: pointer; font-size: 12px" onclick="forceDownload('<?= base_url() . $doc_prets->doc_pret_path ?>','<?= $doc_prets->doc_pret_nom ?>')">
                                                        <?= $doc_prets->doc_pret_nom ?>
                                                    </span>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <th class="ps-0" style="width: 250px;"><?= str_replace('@annee', $date, $this->lang->line('capital_restant_du')) ?> : </th>
                                        <td class="pe-0 fw-bold">
                                            <?= isset($pret->pret_capital_restant_du) ? $pret->pret_capital_restant_du . ' ' . '€' : $this->lang->line('ajout_capital_pret') ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-primary" id="modif_<?= $cliLot_id ?>">
                                <i class="fas fa-pencil-alt"></i>
                                <?= $this->lang->line('button_modifier_information'); ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="py-4 fiche-pret_<?= $cliLot_id ?>">
                    <div class="div-client-conseil">
                        <div class="image-client-conseil">
                            <div class="image-silouette">
                                <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                            </div>
                            <div class="text-conseil flex-grow-1">
                                <div class="contenu-conseil">

                                    <div class="bubble bubble-bottom-left">
                                        <span>
                                            <?= $this->lang->line('texte-conseil-clientPret'); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col p-1">
            <div class="card shadow p-3 mb-5 rounded-0 h-100 form-pret_<?= $cliLot_id ?> <?= empty($doc_pret) && !empty($presence_pret) && $presence_pret->etat_presence == 1 ? "" : "d-none" ?>">
                <div class="card-body">
                    <h4 class="text-center">
                        <?= $this->lang->line('alert_ajout_pret') ?></span>

                    </h4>
                    <div class="row mt-5">
                        <div class="col-3"></div>
                        <div class="col-8">
                            <input type="hidden" id="pret_id<?= $cliLot_id ?>" value="<?= isset($pret->pret_id) ? $pret->pret_id : "" ?>">
                            <input type="hidden" name="" id="cliLot_id_<?= $cliLot_id ?>" value="<?= $cliLot_id ?>">
                            <?php if (!empty($doc_pret)) : ?>
                                <?php foreach ($doc_pret as $documents) : ?>
                                    <div class="ms-10 doc_<?= $documents->doc_pret_id ?>">
                                        <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $documents->doc_pret_path ?>')" data-url="<?= '' ?>" title="Télécharger fichier" style="cursor: pointer;">
                                            <?= $documents->doc_pret_nom ?>
                                        </span>
                                        <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefile_pret" data-id="<?= $documents->doc_pret_id ?>" style="cursor: pointer;" title="Supprimer fichier">.</span>
                                    </div>
                                <?php endforeach ?>
                            <?php endif ?>
                            <div class="form-group inline p-0">
                                <div class="col-sm-12">
                                    <label data-width="200" style="font-size: 17px;"><?= $this->lang->line('fichier_emprunt') ?> * : </label>
                                    <input type="file" class="form-control m-1 w-50" id="file_<?= $cliLot_id ?>" name="file_<?= $cliLot_id ?>" multiple>
                                    <div class="help_pret">
                                        <i class="text-danger fs--2 help_file_<?= $cliLot_id ?>"></i>
                                    </div>
                                </div>
                                <div class="help fs--1 ms-10">
                                    <i class="msg_<?= $cliLot_id ?>"><?= $this->lang->line('alert_file_taxe') ?></i>
                                </div>
                            </div>
                            <div class="form-group inline p-0 mb-2">
                                <div class="col-sm-12">
                                    <label data-width="250" style="font-size: 16px;"><?= str_replace('@annee', $date, $this->lang->line('capital_restant_du')) ?>* :</label>
                                    <div class="input-group w-50">
                                        <input type="text" placeholder="0" class="form-control mb-0 chiffre form-pret text-end" id="pret_montant_<?= $cliLot_id ?>" name="pret_montant_<?= $cliLot_id ?>" style="height: 35px;" value="<?= isset($pret->pret_capital_restant_du) ? $pret->pret_capital_restant_du : '' ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">€</span>
                                        </div>
                                    </div>
                                    <div class="help_pret">
                                        <i class="text-danger fs--2 help_pret_montant_<?= $cliLot_id ?>"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mt-2 ms-10">
                                <button type="button" class="btn btn-secondary annuler_pret_<?= $cliLot_id ?>"><?= $this->lang->line('button_annuler') ?></button>
                                <button type="button" class="btn btn-primary save_pret_<?= $cliLot_id ?>" data-lang="<?= $_SESSION['session_utilisateur_client']['lang'] ?>"><?= $this->lang->line('button_enregistrer') ?></button>
                            </div>
                        </div>

                        <div class="col"></div>
                    </div>
                    <br />
                    <?php if (empty($doc_pret) && !empty($presence_pret) && $presence_pret->etat_presence == 1) : ?>
                        <h4 class="text-center" id="avant_declaration">
                            <?= $this->lang->line('text_declaration') ?></span>
                        </h4>


                        <div class="col-12">
                            <div class="d-grid gap-2 col-6 mx-auto">
                                <button class="btn btn-primary save_declaration_<?= $cliLot_id ?>" type="button" data-id="<?= $cliLot_id ?>" data-lang="<?= $_SESSION['session_utilisateur_client']['lang'] ?>"> <?= $this->lang->line('boutton_declaration') ?></button>
                            </div>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
            <br />
            <?php if (empty($doc_pret) && !empty($presence_pret) && $presence_pret->etat_presence == 2) : ?>
                <?php if ($presence_pret->util_date_declar != null && $presence_pret->util_role == 'propriétaire') : ?>
                    <h4 class="text-center">
                        <?= $this->lang->line('text_apres_declaration_priopriétaire1') ?> <?= date('d/m/Y', strtotime($presence_pret->util_date_declar)) ?> <?= $this->lang->line('text_apres_declaration_priopriétaire2') ?></span>
                    </h4>
                <?php endif; ?>


                <?php if ($presence_pret->util_date_declar != null && $presence_pret->util_role == 'conseiller') : ?>

                    <h4 class="text-center">
                        <?= $this->lang->line('text_apres_declaration_conseiller1') ?> <?= date('d/m/Y', strtotime($presence_pret->util_date_declar)) ?> , <?= $this->lang->line('text_apres_declaration_priopriétaire2') ?></span>
                    </h4>
                <?php endif; ?>

                <?php if ($presence_pret->util_date_declar == null && $presence_pret->util_role == 'conseiller') : ?>

                    <h4 class="text-center">
                        <?= $this->lang->line('text_apres_declaration_conseiller3') ?> , <?= $this->lang->line('text_apres_declaration_priopriétaire2') ?></span>
                    </h4>

                <?php endif; ?>

            <?php endif; ?>
            <div class="py-4 form-pret_<?= $cliLot_id ?> <?= !empty($doc_pret) ? "d-none" : "" ?>">
                <div class="div-client-conseil">
                    <div class="image-client-conseil">
                        <div class="image-silouette">
                            <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                        </div>
                        <div class="text-conseil flex-grow-1">
                            <div class="contenu-conseil">

                                <div class="bubble bubble-bottom-left">
                                    <span>
                                        <?= $this->lang->line('texte-conseil-clientPret'); ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="Modaldeclaration_<?= $cliLot_id ?>">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <div class="modal-header" style="background-color: #808080; color: #ffffff;">
                        <h5 class="modal-title fs-5" style="color: #ffffff;" id="ModaldeclarationLabel"><?= $this->lang->line('text_modal_confirm'); ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <p><?= $this->lang->line('text_modal_libelle'); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= $this->lang->line('button_annuler') ?></button>
                        <button type="button" class="btn btn-primary confirmButton_<?= $cliLot_id ?>"> <?= $this->lang->line('boutton_confirm') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.save_pret_<?= $cliLot_id ?>').click(function(e) {
        savePret(<?= $cliLot_id ?>, $(this).attr('data-lang'));
    });

    $('#modif_<?= $cliLot_id ?>').click(function(e) {
        $('.fiche-pret_<?= $cliLot_id ?>').addClass('d-none');
        $('.form-pret_<?= $cliLot_id ?>').removeClass('d-none');
    });

    $('.annuler_pret_<?= $cliLot_id ?>').click(function(e) {
        $('.fiche-pret_<?= $cliLot_id ?>').removeClass('d-none');
        $('.form-pret_<?= $cliLot_id ?>').addClass('d-none');
    });

    // $('.save_declaration_<?= $cliLot_id ?>').click(function(e) {
    //     $("#Modaldeclaration_<?= $cliLot_id ?>").modal('show');
    //     $('.modal-backdrop').remove();

    // });

    // Ajoutez cet événement de clic pour le bouton de confirmation
    // $('.confirmButton_<?= $cliLot_id ?>').click(function(e) {
    //     confirmDeclaration(<?= $cliLot_id ?>);

    // });

    $(document).on('click', '.save_declaration_<?= $cliLot_id ?>', function(e) {
        e.preventDefault();
        var data = {
            action: "demande",
            cliLot_id: $(this).attr('data-id')
        }
        confirmDeclaration2(data);
    });

    function afficherTextApresDeclaration() {
        var titre1 = document.getElementById('avant_declaration');
        var titre2 = document.getElementById('apres_declaration');
        titre1.style.display = 'none';
        titre2.style.display = 'block';

        // afficherTextApresDeclaration();
        // var contenu = document.getElementById('content-pret-customer_<?= $cliLot_id ?>');
        // contenu.style.display = 'none';
        // $("#Modaldeclaration_<?= $cliLot_id ?>").modal('hide');
        // $('.modal-backdrop').remove();

    }
</script>