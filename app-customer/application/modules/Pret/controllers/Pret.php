<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Pret extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Pret";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/pret/pret.js",
        );
        $this->_css_personnaliser = array(
            "css/pret/pret.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "pret";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "pret/contenaire-pret" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getPret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Pret/pret/pret-page", $data);
            }
        }
    }

    public function fichePret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentPret_m', 'pret');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Pret_m');
                $this->load->model('EmpruntsPresence_m');

                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_pret_etat' => 1
                    )
                );

                $params_presence = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'method' => 'row'
                );

                $params_lot = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),

                    'method' => 'row'
                );

                $params_pret = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'pret_annee' =>  $date
                    ),
                    'method' => 'row'
                );

                $data['pret'] = $this->Pret_m->get($params_pret);
                $data['presence_pret'] = $this->EmpruntsPresence_m->get($params_presence);
                $data['program'] = $this->lot->get($params_lot);
                $data['doc_pret'] = $this->pret->get($params);
                $data['cliLot_id'] = $data_post['cliLot_id'];
                //print_r($data);
                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Pret/pret/fiche-pret-page", $data);
            }
        }
    }

    public function savePret()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                $this->load->model('DocumentPret_m', 'pret');
                $this->load->model('Pret_m');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $this->load->model('Tache_m', 'tache');

                $data_retour = array("cliLot_id" => $_POST['cliLot_id']);
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => $this->lang->line('info_enreg_erreur'),
                );
                $data = array(
                    'cliLot_id' => $_POST['cliLot_id'],
                    'pret_annee' => $date,
                    'pret_capital_restant_du' => $_POST['pret_montant_'],
                    'pret_etat' => 1,
                    'pret_date_modif' => date('Y-m-d H:i:s')
                );

                $clause = null;

                if ($_POST['pret_id'] != '') {
                    $clause = array("pret_id" => $_POST['pret_id']);
                }

                $request = $this->Pret_m->save_data($data, $clause);

                $request_client = $this->client->get(
                    array(
                        'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                        'method' => 'row',
                        'join' => array(
                            'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        ),
                        'join_orientation' => 'left'
                    )
                );

                $params_lot = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cliLot_id' => $_POST['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),

                    'method' => 'row'
                );

                $data['lot'] = $this->lot->get($params_lot);

                $params_tache = $this->tache->get(
                    array(
                        'clause' => array(
                            'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                            'theme_id' => 24,
                            'tache_nom' => $_POST['pret_id'] != '' ? "Changement d'emprunt sur le lot" . " " . $data['lot']->progm_nom : "Ajout d'emprunt sur le lot" . " " . $data['lot']->progm_nom,
                            'tache_createur' => $request_client->util_prenom,
                            'tache_valide' => 0,
                            'tache_etat' => 1
                        ),
                        'method' => 'row',
                    )
                );

                $data_tache = array(
                    'tache_nom' => $_POST['pret_id'] != '' ? "Changement d'emprunt sur le lot" . " " . $data['lot']->progm_nom : "Ajout d'emprunt sur le lot" . " " . $data['lot']->progm_nom,
                    'tache_date_creation' => date('Y-m-d H:i:s'),
                    'tache_date_echeance' => date('Y-m-d H:i:s'),
                    'tache_prioritaire' => 2,
                    'tache_texte' => "Le propriétaire a modifié ses documents et/ou des informations d'emprunt sur l'Extranet. Veuillez consulter la page du lot puis l'onglet emprunt pour voir les modifications",
                    'tache_valide' => 0,
                    'tache_createur' => $request_client->util_prenom,
                    'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'tache_etat' => 1,
                    'theme_id' => 24,
                    'type_tache_id' => 3,
                    'participant' => $request_client->util_id
                );

                if (empty($params_tache)) {
                    $request_tache = $this->tache->save_data($data_tache);
                }

                foreach ($_FILES as $key => $value) {
                    $annee = $date;
                    $cliLot_id = $_POST['cliLot_id'];
                    $targetDir = "../documents/clients/lots/emprunt/$annee/$cliLot_id";
                    $temp_name = $value['tmp_name'];
                    $data_file['doc_pret_nom'] = $value['name'];
                    $data_file['doc_pret_creation'] = date('Y-m-d H:i:s');
                    $data_file['doc_pret_etat'] = 1;
                    $data_file['doc_pret_traite'] = 0;
                    $data_file['cliLot_id'] = $cliLot_id;
                    $data_file['client_id'] = $_SESSION['session_utilisateur_client']['client_id'];
                    $data_file['doc_pret_path'] = str_replace('\\', '/', $targetDir . DIRECTORY_SEPARATOR . $data_file['doc_pret_nom']);
                    $this->makeDirPath($targetDir);
                    if (!file_exists($data_file['doc_pret_path'])) {
                        move_uploaded_file($temp_name, $data_file['doc_pret_path']);
                    }
                    $request = $this->pret->save_data($data_file);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => (empty($pret_id)) ? $this->lang->line('info_enreg') : $this->lang->line('info_enreg_modif')));
                }
                echo json_encode($retour);
            }
        }
    }

    public function declarationPret2()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->load->model('EmpruntsPresence_m');
                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                $data_retour = array("cliLot_id" => $_POST['cliLot_id']);
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => $this->lang->line('info_enreg_erreur'),
                );
                $data = array(
                    'util_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'util_nom' => $_SESSION['session_utilisateur_client']['client_nom'] . ' ' . $_SESSION['session_utilisateur_client']['client_prenom'],
                    'util_date_declar' => date('Y-m-d H:i:s'),
                    'util_role' => 'propriétaire',
                    'etat_presence' => '2',
                );

                $clause = null;

                if ($_POST['cliLot_id'] != '') {
                    $clause = array("cliLot_id" => $_POST['cliLot_id']);
                }

                $request = $this->EmpruntsPresence_m->save_data($data, $clause);
                if (!empty($request)) {

                    // Tache 
                    $this->load->model('Client_m', 'client');
                    $request_client = $this->client->get(
                        array(
                            'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                            'method' => 'row',
                            'join' => array(
                                'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            ),
                            'join_orientation' => 'left'
                        )
                    );
                    $params_lot = array(
                        'clause' => array(
                            'cliLot_etat' => 1,
                            'cliLot_id' => $_POST['cliLot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        ),
                        'method' => 'row'
                    );


                    $this->load->model('ClientLot_m', 'lot');
                    $data['lot'] = $this->lot->get($params_lot);

                    $data_tache = array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 37,
                        'tache_nom' => "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " qu'il n'y a pas de données pour l'emprunt " . $date . " à télécharger pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",
                        'tache_texte' => "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " aucun emprunt " . $date . " à télécharger pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1,
                        'tache_date_creation' => date('Y-m-d H:i:s'),
                        'tache_date_echeance' => date('Y-m-d H:i:s'),
                        'tache_prioritaire' => 2,
                        'type_tache_id' => 3,
                        'participant' => $request_client->util_id,
                        'tache_realise' => 0,
                    );

                    $this->load->model('Tache_m', 'tache');

                    $request_tache = $this->tache->save_data($data_tache);
                    if ($request_tache) {
                        // La requête a été exécutée avec succès
                        echo "La tâche a été sauvegardée avec succès.";
                    } else {
                        // Il y a eu une erreur lors de l'exécution de la requête
                        echo "Une erreur s'est produite lors de la sauvegarde de la tâche.";
                    }
                    $retour = retour(true, "success", $data_retour, array("message" => $this->lang->line('info_enreg_modif')));
                }
                echo json_encode($retour);
            }
        }
    }

    public function declarationPret()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('EmpruntsPresence_m');
            //$data_retour = array("cliLot_id" => $_POST['cliLot_id']);
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => $this->lang->line('info_enreg_erreur'),
            );

            $data = decrypt_service($_POST["data"]);
            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'cliLot_id' => $data['cliLot_id'],
                        'title' => "Confirmation",
                        'text' => $this->lang->line('text_modal_libelle'),
                        'btnConfirm' => $this->lang->line('confirmation'),
                        'btnAnnuler' => $this->lang->line('annuler')
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {

                $data_save = array(
                    'util_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'util_nom' => $_SESSION['session_utilisateur_client']['client_nom'] . ' ' . $_SESSION['session_utilisateur_client']['client_prenom'],
                    'util_date_declar' => date('Y-m-d H:i:s'),
                    'util_role' => 'propriétaire',
                    'etat_presence' => '2',
                );
                $clause = null;
                if ($data['cliLot_id'] != '') {
                    $clause = array("cliLot_id" => $data['cliLot_id']);
                }

                $request = $this->EmpruntsPresence_m->save_data($data_save, $clause);
                if ($request) {

                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'cliLot_id' => $data['cliLot_id'],
                        ),
                        'message' => "Confirmation réussie",
                    );
                    // Tache 
                    $this->load->model('Client_m', 'client');
                    $request_client = $this->client->get(
                        array(
                            'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                            'method' => 'row',
                            'join' => array(
                                'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            ),
                            'join_orientation' => 'left'
                        )
                    );
                    $params_lot = array(
                        'clause' => array(
                            'cliLot_etat' => 1,
                            'cliLot_id' => $data['cliLot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        ),
                        'method' => 'row'
                    );


                    $this->load->model('ClientLot_m', 'lot');
                    $data['lot'] = $this->lot->get($params_lot);

                    $data_tache = array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 37,
                        'tache_nom' => "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " qu'il n'y a pas de données pour l'emprunt " . date('Y') . " à télécharger pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",
                        'tache_texte' => "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " aucun emprunt " . date('Y') . " à télécharger pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1,
                        'tache_date_creation' => date('Y-m-d H:i:s'),
                        'tache_date_echeance' => date('Y-m-d H:i:s'),
                        'tache_prioritaire' => 2,
                        'type_tache_id' => 3,
                        'participant' => $request_client->util_id,
                        'tache_realise' => 0,
                    );

                    $this->load->model('Tache_m', 'tache');

                    $request_tache = $this->tache->save_data($data_tache);
                    // if ($request_tache) {
                    //     // La requête a été exécutée avec succès
                    //     echo "La tâche a été sauvegardée avec succès.";
                    // } else {
                    //     // Il y a eu une erreur lors de l'exécution de la requête
                    //     echo "Une erreur s'est produite lors de la sauvegarde de la tâche.";
                    // }


                }
            }
            $retour['taxe_info'] = $data['cliLot_id'];
            echo json_encode($retour);
        }
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function removefile()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentPret_m');
                $data_retour = array("doc_pret_id" => $_POST['data']['doc_pret_id']);
                $data = array(
                    'doc_pret_etat' => 0,
                );
                $clause = array("doc_pret_id" => $_POST['data']['doc_pret_id']);
                $request = $this->DocumentPret_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => $this->lang->line('delete_file')));
                }
                echo json_encode($retour);
            }
        }
    }
}
