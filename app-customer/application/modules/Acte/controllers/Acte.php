<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Acte extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Acte";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/acte/acte.js",
        );
        $this->_css_personnaliser = array(
            "css/acte/acte.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "acte";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "acte/contenaire-acte" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getActe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Acte/acte/acte-page", $data);
            }
        }
    }

    public function ficheActe()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentActe_m', 'acte');
                $this->load->model('Acte_m');
                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_acte_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_acte.util_id')
                );

                $params_acte = array(
                    'clause' => array('cliLot_id' => $data_post['cliLot_id']),
                    'method' => 'row',
                );

                $data['acte'] =  $this->Acte_m->get($params_acte);
                $data['doc_acte'] = $this->acte->get($params);
                $data['cliLot_id'] = $data_post['cliLot_id'];
                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Acte/acte/fiche-acte-page", $data);
            }
        }
    }

    public function getPathActe()
    {

        $this->load->model('DocumentActe_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_acte_id', 'doc_acte_path', 'cliLot_id'),
                    'clause' => array('doc_acte_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentActe_m->get($params);
                echo json_encode($request);
            }
        }
    }
}
