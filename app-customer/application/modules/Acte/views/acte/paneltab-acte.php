<div class="contenair-title pt-2 pb-2">
    <div class="px-2">
        <i class="fas fa-file-alt"></i> &nbsp;&nbsp;
        <h5><?= $this->lang->line('list_acte'); ?></h5>
    </div>
    <div class="px-2"></div>
</div>

<div class="flex-grow-1" id="fiche-acte-lot_<?= $cliLot_id ?>">

</div>

<script>
    $(document).ready(function() {
        getFicheActeLot(<?= $cliLot_id; ?>);
    });
</script>