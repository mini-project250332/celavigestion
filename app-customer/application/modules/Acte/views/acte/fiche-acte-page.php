<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">
            <div class="w-100">

                <div class="liste-document-customer px-1 pt-2">

                    <?php if (!empty($doc_acte)) : ?>

                        <?php foreach ($doc_acte as $doc_actes) : ?>
                            <div class="d-flex mb-2 align-items-center">
                                <div class="flex-shrink-1 file-thumbnail">
                                    <img class="img-fluid" src="<?= base_url('assets/images/docs.png'); ?>" alt="">
                                </div>
                                <div class="ms-2 flex-grow-1 d-flex align-items-center ">
                                    
                                    <div class="flex-grow-1 d-flex flex-column">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 300px;">
                                            <span class="mb-0 fw-semi-bold fs-0 text-dark" >
                                                <?= $doc_actes->doc_acte_nom; ?>
                                            </span>
                                        </div>
                                        <div class="fs--1 mt-1">
                                            <p class="mb-1">
                                                <span class="fw-semi-bold mb-1"><?= $this->lang->line('depose_par') ?> :</span>
                                                <span class="fw-semi-bold px-2">
                                                    <?= $doc_actes->util_prenom; ?>
                                                </span>
                                                <span class="fw-semi-bold mb-1"><?= $this->lang->line('le') ?> </span>
                                                <span class="fw-medium text-600 ms-2"><?= date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $doc_actes->doc_acte_creation))) ?></span>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="px-2 d-flex flex-shrink-1 ">
                                        <btn class="btn btn-primary btn-sm me-1 text-600 text-white d-flex align-items-center" onclick="forceDownload('<?= base_url() . $doc_actes->doc_acte_path ?>','<?= $doc_actes->doc_acte_nom ?>')">
                                            <i class=" fas fa-file-download"></i> 
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('telecharger') ?></span>
                                        </btn>
                                        <btn class="btn btn-info btn-sm text-600 text-white d-flex align-items-center" onclick="apercuDocumentActe(<?= $doc_actes->doc_acte_id ?>)">
                                            <i class="fas fa-eye"></i> 
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('voir_document') ?></span>
                                        </btn>
                                    </div>
                                </div>
                            </div>
                            <hr class="bg-200 my-1">
                        <?php endforeach; ?>

                    <?php else : ?>

                        <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                            <div class="bg-info me-3 icon-item">
                                <span class="fas fa-info-circle text-white fs-3"></span>
                            </div>
                            <p class="mb-0 flex-1"><?= $this->lang->line('aucun_doc') ?></p>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
            <hr>
            <div class="w-100 py-1">
                <div class="row m-0 border">
                    <div class="col px-0">
                        <div class="card" style="box-shadow:none!important;">
                            <div class="card-header rounded p-2" style="background: #1F659E;">
                                <h5 class="fs-0 text-white">
                                    <i class="fas fa-info-circle"></i>
                                    <?= $this->lang->line('information_acte') ?>
                                </h5>
                            </div>
                            <div class="card-body bg-light">
                                <div class="row">
                                    <div class="col">
                                        <h6 class="fw-semi-bold ls mb-3" style="text-decoration:underline;">
                                            <?= $this->lang->line('info_general_acte') ?>
                                        </h6>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-1"><?= $this->lang->line('type_acquisition') ?> :</p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= isset($acte->acte_type_acquisition) ? $acte->acte_type_acquisition : "" ?></p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_acte_notarie') ?> :</p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_date_notarie) ? date('d-m-Y', strtotime($acte->acte_date_notarie)) : ''; ?></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <h6 class="fw-semi-bold ls mb-3" style="text-decoration:underline;">
                                            <?= $this->lang->line('montant_acquisition') ?>
                                        </h6>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-1"><?= $this->lang->line('valeur_acquisition') ?> :</p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_valeur_acquisition_ht) ? number_format($acte->acte_valeur_acquisition_ht) . ' ' . '€' : ''; ?></p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-1"><?= $this->lang->line('frais_acquisition') ?> :</p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_frais_acquisition) ? number_format($acte->acte_frais_acquisition) . ' ' . '€' : 0; ?></p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-1"><?= $this->lang->line('valeur_terrain') ?></p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_valeur_terrain_ht) ? number_format($acte->acte_valeur_terrain_ht) . ' ' . '€' : ''; ?></p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-0"><?= $this->lang->line('valeur_construction') ?> : </p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_valeur_construction_ht) ? number_format($acte->acte_valeur_construction_ht) . ' ' . '€' : ''; ?></p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <p class="fw-semi-bold mb-0"><?= $this->lang->line('valeur_mobilier') ?> : </p>
                                            </div>
                                            <div>
                                                <p class="mb-1"><?= !empty($acte->acte_valeur_mobilier_ht) ? number_format($acte->acte_valeur_mobilier_ht) . ' ' . '€' : ''; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-4">
            <div class="div-client-conseil">
                <div class="image-client-conseil">
                    <div class="image-silouette">
                        <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                    </div>
                    <div class="row">
                        <div class="text-conseil flex-grow-1">
                            <div class="contenu-conseil">
                                <div class="bubble bubble-bottom-left" style="margin-top: -69px;">
                                    <span>
                                        <?= $this->lang->line('texte-conseil-clientActe'); ?>
                                    </span>
                                </div>
                                <div class="bubble bubble-bottom-left" style="margin-top: 35px;margin-left: -11px;">
                                    <span>
                                        <?= $this->lang->line('texte-conseil-clientActe1'); ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="splitter"></div>

    <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
        <div id="apercu_doc_Acte_<?= $cliLot_id ?>">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <?= $this->lang->line('apercu_doc'); ?>
                </div>
            </div>
            <div id="apercuActe_<?= $cliLot_id ?>">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                    </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-minus" aria-hidden="true"></i>
                    </button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-retweet"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    // $(".panel-left").resizable({
    //     handleSelector: ".splitter",
    //     resizeHeight: false,
    // });
</script>