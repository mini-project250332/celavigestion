<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta name="description" content="<?php echo description; ?>">
    <meta name="author" content="<?php echo author; ?>">
    <title>
        <?php echo title; ?>
    </title>
    <!-- Meta -->

    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />

    <?php if (isset($css_libraries)) {
        foreach ($css_libraries as $css_lib) {
            echo '<link type="text/css" href="' . base_url('../assets/' . $css_lib) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style par default com/_css -->
    <?php if (isset($css_style)) {
        foreach ($css_style as $css_s) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_s) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Style personnaliser -->
    <?php if (isset($css_personnaliser)) {
        foreach ($css_personnaliser as $css_p) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_p) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Javascript constant application -->
    <script type="text/javascript">
        const const_app = {
            <?php
            if (isset($const_js)) {
                foreach ($const_js as $js_n => $val_n) {
                    echo $js_n . " : '" . $val_n . "',\n";
                }
            }
            ?>
        }
    </script>

    <!-- Lirairie JS par default "require" -->
    <?php
    if (isset($js_libraries)) {
        foreach ($js_libraries as $js_lib) {
            echo '<script src="' . base_url('../assets/' . $js_lib) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <?php
    if (isset($js_requerie)) {
        foreach ($js_requerie as $js_r) {
            echo '<script src="' . base_url('assets/' . $js_r) . '" type="text/javascript"></script>';
        }
    }
    ?>
    <style type="text/css">
        .pace .pace-progress {
            display: none !important;
        }

        .pace .pace-progress-inner {
            display: none !important;
        }
    </style>
    <script async src="https://www.google.com/recaptcha/api.js?hl=<?= $_SESSION['lang'] == 'french' ? 'fr' : 'en' ?>" async defer"></script>
</head>

<body>
    <div class="div-formLogin" id="reinit_password_form">
        <div class="login-form-box mb-3">
            <div id="div-signin-box p-2" class="signin-box-div-forgetpass">

                <div class="row">
                    <div class="col lang_logo text-end">
                        <a href="<?php echo base_url('Auth/switch_language/french'); ?>">
                            <img src="<?php echo base_url('../assets/img/FR.png'); ?>" id="lang_french">
                        </a>
                        <a href="<?php echo base_url('Auth/switch_language/english'); ?>">
                            <img src="<?php echo base_url('../assets/img/EN.png'); ?>" id="lang_english">
                        </a>
                    </div>
                </div>

                <div class="c_logo">
                    <div class="text_logo_mdp">
                        <img src="<?php echo base_url('../assets/img/celavi.jpg'); ?>">
                    </div>
                </div>
                <input type="hidden" id="email_dest" value="" />
                <div class="col">
                    <br>
                    <div class="title_groupe">
                        <div>
                            <h5 class="title_login_forget_pass"> <?= $this->lang->line('reinit_pass_title'); ?> </h5>
                            <br>
                            <h5 class="sous_title_login">
                                <?= $this->lang->line('info_passw'); ?>
                            </h5>
                        </div>
                    </div>

                    <div class="form_login_group">
                        <div class="form-group">
                            <div>
                                <label><?= $this->lang->line('new_pass'); ?></label>
                                <input type="password" class="form-control" id="new_password_reinit" name="" placeholder="<?= $this->lang->line('new_pass'); ?>" data-required="true">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="form-group mt-2">
                            <div>
                                <label><?= $this->lang->line('conf_pass'); ?></label>
                                <input type="password" class="form-control" id="confirm_password_reinit" name="" placeholder="<?= $this->lang->line('conf_pass'); ?>" data-required="true">
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <div class="g-recaptcha" data-sitekey="<?= SITE_KEY ?>"></div>
                        </div>
                    </div>
                    <br>
                    <div id="div-message"></div>
                    <div class="form_login_button p-2">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="h-100 w-100" id="container-new-password"></div>
                                </div>
                                <div class="col-6">

                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col text-end">

                                        </div>
                                        <div class="col text-end">
                                            <button class="btn btn-info" id="btn_send_new_password" data-id="<?= $this->session->userdata('lang') == NULL ? 'french' : $this->session->userdata('lang') ?>">
                                                <span><?= $this->lang->line('btn_valider'); ?></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($js_script)) {
        foreach ($js_script as $js_s) {
            echo '<script src="' . base_url('/assets/' . $js_s) . '" type="text/javascript"></script>';
        }
    }
    ?>
</body>

<script>
    $(document).ready(function() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const emailCrypted = urlParams.get('crypto');
        var postData = {
            'text': JSON.stringify(emailCrypted)
        };

        $.ajax({
            type: 'POST',
            url: `${const_app.base_url}Auth/decrypt_forgot_password`,
            data: postData,
            dataType: 'json',
            error: (result) => {
                if (result.status == 200 && result.readyState == 4) {
                    var email = result.responseText;
                    $("#email_dest").val(email);
                    verifyEmail(email);
                } else {
                    Notiflix.Notify.failure("Une erreur est survenue", {
                        position: 'left-bottom',
                        timeout: 3000
                    });
                }
            }
        });
    });

    function verifyEmail(email) {
        var emailData = {
            'email': email
        };

        $.ajax({
            type: 'POST',
            url: `${const_app.base_url}Auth/verify_email`,
            data: emailData,
            dataType: 'json',
            error: (result) => {
                if (result.status == 403) {
                    $('#btn_send_new_password').prop("disabled", true);
                    $('#new_password_reinit').prop("disabled", true);
                    $('#confirm_password_reinit').prop("disabled", true);
                    Notiflix.Notify.failure("Token invalide.", {
                        position: 'left-bottom',
                        timeout: 3000
                    });
                    return false;
                }
            },
            success: (result) => {
                // console.log("success ", result);
            }
        });
    }
</script>


</html>