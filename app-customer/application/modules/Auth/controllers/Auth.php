<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Auth extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Auth";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            'js/auth/auth.js',
        );

        $this->_css_personnaliser = array(
            "css/auth/auth.css"
        );

        $this->load->model('Historiqueacces_m', 'hacces');
        $this->load->model('Utilisateur_m', 'utilisateur');
        $this->load->model('AccesClient_m', 'accesclient');
        $this->load->model('Client_m', 'client');

        $lang_code = $this->session->userdata('lang') ?? $this->defaultLanguage();
        $this->lang->load('welcome', $lang_code);
    }

    public function index()
    {
        redirect("Auth/login/" . (isset($this->session->userdata['token']) ? $this->session->userdata['token'] : $this->genererToken()));
    }

    public function login($token = null)
    {
        if ($token != null) {
            $data['token'] = $token;
            if (isset($this->session->userdata['token'])) {
                $verify = $this->checkSession($token);
                if ($verify['status'] == true) {
                    if ($verify['db_status'] == 2) {
                        redirect("Auth/login/" . $this->genererToken());
                    } else {
                        $this->login_page('login');
                    }
                } else {
                    $this->session->sess_destroy();
                    redirect("Auth/login/" . $this->genererToken());
                }
            } else {
                $data = array(
                    'hacces_token' => $token,
                    'hacces_datecreate' => Carbon::now()->toDateTimeString(),
                    'hacces_ip' => $_SERVER['REMOTE_ADDR'],
                    'hacces_agent' => $this->agent->browser(),
                    'hacces_action' => 1,
                    // 1 demande // 2 login (status = 1 ok | 0 non ok)  // 3 logout (status = 1 ok | 0 non ok), // 4 expirer
                    'hacces_status' => 1
                );
                $request = $this->hacces->save_data($data);
                if (!empty($request)) {
                    $this->session->set_userdata('token', $token);
                    $this->session->set_userdata('create', $data['hacces_datecreate']);
                    $this->login_page('login');
                }
            }
        } else {
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function checkSession($token = null)
    {
        $params = array(
            'clause' => array(
                'hacces_token' => $token,
                'hacces_status ' => 1,
            ),
            'clause_in' => array('hacces_action' => [1, 2]),
            'method' => "row",
        );
        $retour = array("status" => false, "db_status" => 0);
        $request = $this->hacces->get($params);
        if (!empty($request)) {
            $date_creation = Carbon::createFromFormat('Y-m-d H:i:s', $request->hacces_datecreate);
            $now = Carbon::now();
            if ($now->gt($date_creation) && $date_creation->diffInHours($now) < 6) {
                $retour = array("status" => true, "db_status" => intval($request->hacces_action));
            } else {
                $data = array('hacces_action' => 4);
                $clause = array('hacces_id' => $request->hacces_id);
                $this->hacces->save_data($data, $clause);
            }
        }
        return $retour;
    }

    private function genererToken()
    {
        $token = openssl_random_pseudo_bytes(32);
        $token = bin2hex($token);
        return $token;
    }

    public function authUser()
    {
        if (is_ajax()) {
            if ($_POST) {
                if (isset($_POST['data']['g-recaptcha-response']['value']) && !empty($_POST['data']['g-recaptcha-response']['value'])) {
                    $verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_KEY . '&response=' . $_POST['data']['g-recaptcha-response']['value']);
                    $response = json_decode($verify);
                    if ($response->success) {
                        unset($_POST['data']['g-recaptcha-response']);
                        $message = array();
                        $_POST['data']['util_password']['value'] = trim(decrypt_service($_POST['data']['util_password']['value']));
                        $control = $this->control_data($_POST['data']);
                        $retour = retour(null, null, null, $control['information']);
                        if ($control['return']) {
                            $clause = format_data($_POST['data']);
                            $this->load->model('Utilisateur_m', 'users');
                            $verify_ident = array(
                                'distinct' => FALSE,
                                'clause' => $clause,
                                'or_clause' => NULL,
                                'like' => NULL,
                                'or_like' => NULL,
                                'method' => "row",
                                'columns' => '*',
                            );
                            $request = $this->users->get($verify_ident);

                            $data_historique = array(
                                'hacces_action' => 2,
                                'hacces_status' => 0,
                                'hacces_datelogin' => Carbon::now()->toDateTimeString()
                            );

                            $clause_historique = array('hacces_token' => $this->session->userdata['token']);

                            if (!empty($request)) {
                                $retour = retour(true, "success", 1, array("message" => $this->lang->line('correct_sign')));
                                $paramclient = $this->client->get(array(
                                    'clause' => array('client_id' => $request->client_id),
                                    'method' => 'row'
                                ));

                                if ($_SESSION['lang'] != $request->defaultlanguage) {
                                    $data_lang['defaultlanguage'] = $_SESSION['lang'];
                                    $clause_lang = array('util_id' => $request->util_id);
                                    $this->users->save_data($data_lang, $clause_lang);
                                }

                                $data['session_utilisateur_client'] = array(
                                    'util_id' => $request->util_id,
                                    'client_id' => $request->client_id,
                                    'client_nom' => $paramclient->client_nom,
                                    'client_prenom' => $paramclient->client_prenom,
                                    'check_identite' => $paramclient->check_identite,
                                    'lang' => $_SESSION['lang'] != $request->defaultlanguage ? $_SESSION['lang'] : $request->defaultlanguage,
                                );
                                $data['session_compte'] = array(
                                    'is_logged' => 1,
                                );
                                $data['login'] = $data_historique['hacces_datelogin'];
                                $this->session->set_userdata($data);
                                $data_historique['hacces_status'] = 1;
                                $data_historique['util_id'] = $request->util_id;
                            } else {
                                // verify if from client CELAVI
                                $verify_compte_client_origin = array(
                                    'clause' => array(
                                        'c_mot_de_passe_fruit.mtpfr_nom' => trim($_POST['data']['util_password']['value']),
                                        'c_access_proprietaire_client.accp_login' => trim($_POST['data']['util_login']['value'])
                                    ),
                                    'join' => array(
                                        "c_mot_de_passe_fruit" => "c_mot_de_passe_fruit.mtpfr_id = c_access_proprietaire_client.mtpfr_id"
                                    ),
                                    'method' => 'row'
                                );

                                $request = $this->accesclient->get($verify_compte_client_origin);

                                if (!empty($request)) {
                                    // Save user info
                                    $check_user = $this->users->get(
                                        array(
                                            'clause' => array('util_login' => $request->accp_login),
                                            'method' => 'row'
                                        )
                                    );

                                    if (empty($check_user)) {
                                        $newUser['util_login'] = $request->accp_login;
                                        $newUser['util_password'] = $request->mtpfr_nom;
                                        $newUser['client_id'] = $request->client_id;
                                        $newUser['etat_user'] = 0;
                                        $newUser['defaultlanguage'] = $_SESSION['lang'];
                                        $requestAddUser = $this->users->save_data($newUser, null);

                                        $get_last_user_id = array(
                                            'order_by_columns' => "util_id DESC",
                                            'method' => 'row'
                                        );
                                        $last_id = $this->users->get($get_last_user_id);
                                    } else {
                                        $last_id = $check_user;
                                    }

                                    $retour = retour(true, "success", 1, array("message" => $this->lang->line('correct_sign')));
                                    $paramclient = $this->client->get(array(
                                        'clause' => array('client_id' => $request->client_id),
                                        'method' => 'row'
                                    ));
                                    $data['session_utilisateur_client'] = array(
                                        'util_id' => $last_id->util_id,
                                        'client_id' => $last_id->client_id,
                                        'client_nom' => $paramclient->client_nom,
                                        'client_prenom' => $paramclient->client_prenom,
                                        'check_identite' => $paramclient->check_identite,
                                        'lang' => $last_id->defaultlanguage
                                    );
                                    $data['session_compte'] = array(
                                        'is_logged' => 1,
                                    );

                                    $data['login'] = $data_historique['hacces_datelogin'];
                                    $this->session->set_userdata($data);
                                    $data_historique['hacces_status'] = 1;
                                    $data_historique['util_id'] = $last_id->util_id;
                                    $this->hacces->save_data($data_historique, $clause_historique);
                                } else {
                                    $retour = retour(false, "error", 2, array("message" => $this->lang->line('pass_incorrect')));
                                    echo json_encode($retour);
                                    return false;
                                }
                            }
                            $this->hacces->save_data($data_historique, $clause_historique);
                        } else {
                            $retour = retour(null, null, null, $control['information']);
                        }
                        echo json_encode($retour);
                    }
                } else {
                    $retour = retour(false, "error", 2, array("message" => $this->lang->line('reCaptcha_coch')));
                    echo json_encode($retour);
                }
            }
        }
    }

    public function forgetpass()
    {
        $this->login_page('motdepasseoublie');
    }

    public function newpass()
    {
        $this->load->model('LienIntranet_m', 'lien');
        $Linktoken = json_decode($_GET['crypto']);
        $searchValue = ($Linktoken->ct);
        $searchValue = str_replace(' ', '+', $searchValue);

        $Link = $this->lien->get(array(
            'clause' => array(
                'etat' => 0,
                'cle_temporaire' => $searchValue
            ),
            'method' => 'row'
        ));

        $found = false;
        $currentDateTime = new DateTime();

        if (!empty($Link)) {
            $givenDateTime = new DateTime($Link->date_creation);
            if ($givenDateTime > $currentDateTime) {
                $found = true;
            }
        }

        if ($found == true) {
            $_SESSION['Linktoken'] = $Link->id;
            $this->login_page('newpassword');
        } else {
            $this->login_page('url_deja_utilise');
        }
    }

    public function logout()
    {
        $data = array(
            'hacces_action' => 3,
            'hacces_datelogout' => Carbon::now()->toDateTimeString()
        );
        $clause = array('hacces_token' => $this->session->userdata['token']);
        $request = $this->hacces->save_data($data, $clause);
        if (!empty($request)) {
            $this->session->sess_destroy();
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function newPassword()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['data']['g_recaptcha_response']) && !empty($_POST['data']['g_recaptcha_response'])) {
                $verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_KEY . '&response=' . $_POST['data']['g_recaptcha_response']);
                $response = json_decode($verify);
                $this->load->model('Client_m', 'client');
                $this->load->model('Utilisateur_m', 'utilsateur');
                if ($response->success) {
                    $this->load->model('LienIntranet_m', 'lien');
                    $clause = array("id" => $_SESSION['Linktoken']);
                    $data = array('etat' => 1);
                    $this->lien->save_data($data, $clause);

                    unset($_POST['data']['g_recaptcha_response']);
                    $data_post = $_POST["data"];
                    $email = $data_post['email'];
                    $password = $data_post['password'];
                    $passwordCrypted = hash('sha512', $password);

                    $check_client = array(
                        'clause' => array('client_email' => trim($email)),
                        'method' => "row",
                    );
                    $check = $this->client->get($check_client);

                    if (!empty($check)) {
                        $check_user = array(
                            'clause' => array('client_id' => $check->client_id),
                            'method' => 'row'
                        );
                        $result = $this->utilsateur->get($check_user);
                    }
                    $data = array(
                        'util_password' =>  $passwordCrypted,
                        'etat_user' => 1
                    );
                    $clause = array("client_id" => $result->client_id);
                    $request = $this->utilisateur->save_data($data, $clause);

                    if ($request) {
                        $Auth = "Auth/RetournerLogin/" . $data_post['lang_code'];
                        $retour = array('status' => true, 'Auth' => $Auth);
                        echo json_encode($retour);
                    }
                }
            } else {
                $retour = array('status' => false, 'message' =>  $this->lang->line('reCaptcha_coch'));
                echo json_encode($retour);
            }
        }
    }

    public function encrypt_forgot_password()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $text = $_POST['text'];
            $data = encrypt_service($text);
            echo $data;
        }
    }
    public function decrypt_forgot_password()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $text = $_POST['text'];
            $text = str_replace('\"', '"', $text);
            $text = str_replace('"{', '{', $text);
            $text = str_replace('}"', '}', $text);
            $text = str_replace(' ', '+', $text);
            $data = decrypt_service($text);
            echo $data;
        }
    }

    public function verify_email()
    {
        $this->load->model('Client_m', 'client');
        $this->load->model('Utilisateur_m', 'utilsateur');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $check_client = array(
                'clause' => array('client_email' => trim($email)),
                'method' => "row",
            );

            $check = $this->client->get($check_client);
            if (!empty($check)) {
                $check_user = array(
                    'clause' => array('client_id' => $check->client_id),
                    'method' => 'row'
                );
                $result = $this->utilsateur->get($check_user);
            }

            if (!isset($result)) {
                echo  $this->session->userdata('lang');
                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
            } else {
                echo $check->client_nom . ' ' . $check->client_prenom;
            }
        }
    }

    public function checkUtilLogin()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m', 'utilsateur');
            $data_post = $_POST['data'];
            $params = array(
                'clause' => array('util_login' => $data_post['util_login']),
                'method' => "row",
            );
            $result = $this->utilsateur->get($params);

            echo json_encode($result);
        }
    }

    public function change_pass()
    {
        redirect("Auth/changepsswrd/" . $this->session->userdata['token']);
    }

    public function changepsswrd($token_ = null)
    {
        $token = is_array($token_) ? $token_[0] : $token_;
        if ($token != null) {
            if (isset($this->session->userdata['token']) && $this->session->userdata['token'] == $token) {
                $this->login_page('changepassword');
            } else {
                redirect("Auth/login/" . $this->genererToken());
            }
        } else {
            redirect("Auth/login/" . $this->genererToken());
        }
    }

    public function checkUtil()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m', 'utilsateur');
            $data_post = $_POST['data'];
            $params = array(
                'clause' => array('util_id' => $data_post['util_id']),
                'columns' => array('util_id', 'util_login'),
                'method' => "row",
            );
            $result = $this->utilsateur->get($params);

            echo json_encode($result);
        }
    }

    public function svePasswrd()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->load->model('Utilisateur_m', 'utilsateur');
            if (isset($_POST['data']['g_recaptcha_response']) && !empty($_POST['data']['g_recaptcha_response'])) {
                $verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_KEY . '&response=' . $_POST['data']['g_recaptcha_response']);
                $response = json_decode($verify);
                if ($response->success) {
                    $data_post = $_POST["data"];
                    $data = array(
                        'util_login' => $data_post['util_login'],
                        'util_password' => hash('sha512', $data_post['password']),
                        'etat_user' => 1
                    );
                    $clause = array("util_id" => $data_post['util_id']);
                    $result = $this->utilsateur->save_data($data, $clause);
                    if ($result) {
                        $retour = array('status' => true, 'message' => $this->lang->line('reussi'));
                        echo json_encode($retour);
                    }
                }
            } else {
                $retour = array('status' => false, 'message' => $this->lang->line('reCaptcha_coch'));
                echo json_encode($retour);
            }
        }
    }

    public function switch_language($lang_code)
    {
        $this->session->set_userdata('lang', $lang_code);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function RetournerLogin($lang_code)
    {
        $this->session->set_userdata('lang', $lang_code);
        redirect('Auth');
    }
}
