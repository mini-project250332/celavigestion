<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Liste_facture extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Liste_facture";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/liste_facture/liste_facture.js",
        );

        $this->_css_personnaliser = array(
            'css/liste_facture/liste_facture.css'
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "liste_facture";
        $this->_data['sous_module'] = "liste_facture/contenaire-liste-facture";
        $this->render('contenaire');
    }

    public function getListDossier()
    {
        $this->load->model('Dossier_m', 'dossier');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array(
                        'dossier_etat' => 1,
                        'c_dossier.client_id' => intval($_SESSION['session_utilisateur_client']['client_id'])
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_dossier.client_id'
                    ),
                );

                $data['dossier'] =  $this->dossier->get($params);

                $this->renderComponant("Liste_facture/liste_facture/dossier-facture", $data);
            }
        }
    }


    function getfichierfactureCelavi()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('Facture_m', 'facture_annuelle');
                $data_post = $_POST['data'];

                $dossier = $this->dossier->get([
                    'clause' => ['dossier_id' => $data_post['dossier_id']],
                    'columns' => ['dossier_id', 'client_id']
                ]);
                $client_id =    $_SESSION['session_utilisateur_client']['client_id'];

                $liste_dossier = [];

                foreach ($dossier as $key => $value) {
                    if ($value->client_id != "") {
                        $array_client = explode(',', $value->client_id);
                        if (in_array($client_id, $array_client)) {
                            $client = array_map(function ($value_client) {
                                return $this->getClient($value_client);
                            }, $array_client);

                            $dossier[$key]->client = implode(' , ', $client);
                            $liste_dossier[] = $dossier[$key];
                        }
                    }
                }

                $tous_doc = [];
                if (!empty($liste_dossier)) {
                    foreach ($liste_dossier as $value) {
                        $documents = $this->facture_annuelle->get([
                            'clause' => ['dossier_id' => $value->dossier_id],
                            'join' => ['c_document_facturation' => 'c_document_facturation.fact_id = c_facture.fact_id']
                        ]);

                        $tous_doc = array_merge($tous_doc, $documents);
                    }
                }

                $data['document'] = $tous_doc;
                echo $data_post['dossier_id'] . "@@@@@@@";
                $this->renderComponant("Liste_facture/liste_facture/fiche-facture", $data);
            }
        }
    }

    function apercuFacture()
    {
        $this->load->model('DocumentFacturation_m', 'doc_facturation');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('doc_id' => $data_post['id_doc']),
                    'method' => 'row'
                );
                $request = $this->doc_facturation->get($params);
                echo json_encode($request);
            }
        }
    }
}
