<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">
            <div class="w-100">
                <div class="liste-document-customer px-1 pt-2">
                    <?php if (!empty($document)) : ?>
                        <?php foreach ($document as $documents) : ?>
                            <div id="rowdoc-<?= $documents->doc_id ?>" class="d-flex mb-2 align-items-center">
                                <div class="flex-shrink-1 file-thumbnail">
                                    <img class="img-fluid" src="<?= base_url('assets/images/docs.png'); ?>" alt="">
                                </div>
                                <div class="ms-2 flex-grow-1 d-flex align-items-center ">
                                    <div class="flex-grow-1 d-flex flex-column">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 300px;">
                                            <span class="mb-0 fw-semi-bold fs-0 text-dark">
                                                <?= $documents->doc_facturation_nom ?>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="px-2 d-flex flex-shrink-1 ">
                                        <btn class="btn btn-primary btn-sm me-1 text-600 text-white" onclick="forceDownload('<?= base_url() . (($documents->doc_facturation_path)) ?>')">
                                            <i class=" fas fa-file-download"></i>
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('telecharger') ?></span>
                                        </btn>
                                        <btn class="btn btn-info btn-sm text-600 text-white" onclick="<?= 'apercuFacture(' . $documents->doc_id . ')' ?>">
                                            <i class="fas fa-eye"></i>
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('voir_document') ?></span>
                                        </btn>
                                    </div>
                                </div>
                            </div>
                            <hr class="bg-200 my-1">
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                            <div class="bg-info me-3 icon-item">
                                <span class="fas fa-info-circle text-white fs-3"></span>
                            </div>
                            <p class="mb-0 flex-1"><?= $this->lang->line('aucun_facture'); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="splitter"></div>

    <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
        <div id="apercu_facture_CELAVI">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <?= $this->lang->line('apercu_doc'); ?>
                </div>
            </div>
            <div id="apercu_facture_CELAVI">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                    </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-minus" aria-hidden="true"></i>
                    </button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-retweet"></i></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>