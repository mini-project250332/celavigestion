<div class="contenair-title bg-200 rounded">
    <div class="px-2">
    <i class="fas fa-file-invoice"></i>&nbsp;
        <h5 class="fs-1"><?= $this->lang->line('liste_facture') ?></h5>
    </div>
</div>

<div class="w-100 ">
    <nav class="pt-1">
        <?php if (!empty($dossier)) : ?>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <?php foreach ($dossier as $key => $dossiers) : ?>
                    <button class="nav-link tab-menu <?= $key == 0 ? 'active' : '' ?>" id="nav-dossier_<?= $dossiers->dossier_id ?>-tab" data-bs-toggle="tab" data-bs-target="#nav-dossier_<?= $dossiers->dossier_id ?>" type="button" role="tab" aria-controls="nav-dossier_<?= $dossiers->dossier_id ?>" aria-selected="true">
                        <?= "Dossier n°" . " " . str_pad($dossiers->dossier_id, 4, '0', STR_PAD_LEFT) ?>
                    </button>
                <?php endforeach ?>
            </div>
        <?php else : ?>
            <div id="contenaire-menu" class="w-100 p-2">
                <div class="col-12 p-0 py-1">
                    <div class="row m-0">
                        <div class="col p-1"> <br>
                            <div class="card rounded-0 h-100">
                                <div class="card-body ">
                                    <div class="card-body text-center mt-5 bienvenu">
                                        <?= $this->lang->line('aucun_dossier') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </nav>

    <?php if (!empty($dossier)) : ?>
        <div class="tab-content" id="nav-tabContent">
            <?php foreach ($dossier as $key => $dossiers) : ?>
                <div class="tab-pane tab-content fade <?= $key == 0 ? 'active show' : '' ?>" id="nav-dossier_<?= $dossiers->dossier_id ?>" role="tabpanel" aria-labelledby="nav-dossier_<?= $dossiers->dossier_id ?>-tab">
                    <?php $this->load->view('liste_facture/vosfichiercelavi', $dossiers); ?>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>