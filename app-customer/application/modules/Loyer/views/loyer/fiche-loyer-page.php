<?php function formatDateFr($date)
{
    $date = str_replace('-"', '/', $date);
    $newDate = date("d/m/Y", strtotime($date));
    return $newDate;
}
if (isset($bail) && !empty($bail)) :
    $countbail = count($bail);
endif
?>

<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative pt-2 h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">
            <div class="w-100">

                <?php if (!empty($document)) : ?>

                    <div class="liste-document-customer px-1 pt-2">

                        <?php foreach ($document as $doc) : ?>
                            <div class="d-flex mb-2 align-items-center">
                                <div class="flex-shrink-1 file-thumbnail">
                                    <img class="img-fluid" src="<?= base_url('assets/images/docs.png'); ?>" alt="">
                                </div>
                                <div class="ms-2 flex-grow-1 d-flex align-items-center ">

                                    <div class="flex-grow-1 d-flex flex-column">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 300px;">
                                            <span class="mb-0 fw-semi-bold fs-0 text-dark" >
                                               <?= $doc->doc_loyer_nom; ?>
                                            </span>
                                        </div>

                                        <div class="fs--1 mt-1">
                                            <p class="mb-1">
                                                <span class="fw-semi-bold mb-1"><?= $this->lang->line('depose_par') ?> :</span>
                                                <span class="fw-semi-bold px-2">
                                                    <?= $doc->util_prenom; ?>
                                                </span>
                                                <span class="fw-semi-bold mb-1"><?= $this->lang->line('le') ?> </span>
                                                <span class="fw-medium text-600 ms-2"><?= date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $doc->doc_loyer_creation))) ?></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="px-2">
                                        <btn class="btn btn-primary btn-sm me-1 text-600 text-white fs--1" onclick="forceDownload('<?= base_url() . $doc->doc_loyer_path ?>','<?= $doc->doc_loyer_nom ?>')">
                                            <i class=" fas fa-file-download"></i>
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('telecharger') ?></span>
                                        </btn>
                                        <btn class="btn btn-info btn-sm text-600 text-white" onclick="apercuDocumentLoyer(<?= $doc->doc_loyer_id ?>)">
                                            <i class="fas fa-eye"></i>
                                            <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('voir_document') ?></span>
                                        </btn>
                                    </div>
                                </div>
                            </div>
                            <hr class="bg-200 my-1">
                        <?php endforeach; ?>
                    </div>

                <?php else : ?>
                    <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                        <div class="bg-info me-3 icon-item">
                            <span class="fas fa-info-circle text-white fs-3"></span>
                        </div>
                        <p class="mb-0 flex-1"><?= $this->lang->line('aucun_doc') ?></p>
                    </div>
                <?php endif; ?>

                <hr>
                <?php if (isset($bail) && !empty($bail) && $bail[$countbail - 1]->bail_valide == 1) : ?>
                    
                    <div class="row m-0">
                        <div class="col fw-bold pb-2">
                            <label style="font-size: 16px;"><?= $this->lang->line('consultation_facture_encaissement') ?></label>
                        </div>

                        <div class="col text-center">
                            <select class="no_effect form-select" aria-label="Default select example" id="bail_loyer_<?= $cliLot_id ?>">
                                <?php foreach ($bail as $key => $bails) : ?>
                                    <option value="<?= $bails->bail_id ?>" <?= $key == ($countbail - 1) ? 'selected' : '' ?> data-pdl_id="<?= $bails->pdl_id ?>" data-bail_loyer_variable="<?= $bails->bail_loyer_variable ?>">Bail du&nbsp;<?= formatDateFr($bails->bail_debut) ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div id="container-table-facture_encaissement_<?= $cliLot_id ?>"></div>

                <?php endif ?>

                <div class="py-3">
                    <div class="div-client-conseil">
                        <div class="image-client-conseil">
                            <div class="image-silouette">
                                <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                            </div>
                            <div class="text-conseil flex-grow-1">
                                <div class="contenu-conseil">

                                    <div class="bubble bubble-bottom-left">
                                        <span>
                                            <?= $this->lang->line('texte-conseil-encaissement'); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="splitter"></div>

    <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
        <div id="apercu_docLoyer_<?= $cliLot_id ?>">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                    <?= $this->lang->line('apercu_doc') ?>
                </div>
            </div>
            <div id="apercuLoyer_<?= $cliLot_id ?>">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                    </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-search-minus" aria-hidden="true"></i>
                    </button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                        <i class="fa fa-retweet"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if (isset($bail) && !empty($bail)) : ?>
    <?php
    $first_bail = $bail[$countbail - 1]->bail_id;
    $pdl_id = $bail[$countbail - 1]->pdl_id;
    $bail_loyer_variable = $bail[$countbail - 1]->bail_loyer_variable;
    ?>
    <?php if ($bail[$countbail - 1]->bail_valide == 1) : ?>
        <script>
            getTableFacture(
                <?= $first_bail ?>,
                <?= $pdl_id ?>,
                <?= $bail_loyer_variable ?>,
                $('#annee_loyer_<?= $cliLot_id ?>').val(),
                <?= $cliLot_id ?>
            );

            $(document).on('change', '#bail_loyer_<?= $cliLot_id ?>', function() {
                var client_lot = $(this).val();

                getTableFacture(
                    client_lot,
                    <?= $pdl_id ?>,
                    <?= $bail_loyer_variable ?>,
                    $('#annee_loyer_<?= $cliLot_id ?>').val(),
                    <?= $cliLot_id ?>
                );
            });
        </script>
    <?php endif ?>
<?php endif ?>