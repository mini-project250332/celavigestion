<?php
$cliLot_mode_paiement = "";
$cliLot_encaiss_loyer = "";
$cliLot_iban_client = "";
if (isset($lot_facture)) {
    $cliLot_mode_paiement = $lot_facture->cliLot_mode_paiement;
    $cliLot_encaiss_loyer = " sur " . $lot_facture->cliLot_encaiss_loyer;
    $cliLot_iban_client = $lot_facture->cliLot_iban_client;
?>
    <input type="hidden" value="<?= $lot_facture->cliLot_id ?>" id="cli_lot_id" />
<?php } ?>

<input type="hidden" value="<?php if ($bail_loyer[0]->bail_facturation_loyer_celaviegestion == 1)
                                echo "1";
                            else
                                echo "0"; ?>" id="paidByCELAVI" />

<div class="row m-0">
    <div class="col px-0">
        <?php foreach ($bail_loyer as $bail_l) :
            switch ($bail_l->pdl_id):
                    // Mensuel
                case 1: ?>
                    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    $total_facture_ttc = 0;
                    ?>
                    <div class="row m-0">
                        <div class="col-auto px-0">
                            <table class="table table-hover fs--1">
                                <thead>
                                    <tr class="text-white">
                                        <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                                            <br />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div class="text-uppercase"><?= $this->lang->line('loyer_facture'); ?></div>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div><?= $this->lang->line('loyer_appartement'); ?></div>
                                            <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                                            <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                                            <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div><?= $this->lang->line('loyer_parking'); ?></div>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div> <?= $this->lang->line('loyer_forfait'); ?></div>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <div><?= $this->lang->line('loyer_franchise'); ?></div>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                                            <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                                        </th>
                                    </tr>
                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_facture'); ?></th>
                                    </tr>

                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <?= $this->lang->line('total_encaissement'); ?>
                                        </th>
                                    </tr>

                                    <tr class="text-end">
                                        <th class="text-white text-start p-2" style="background:#2569C3;">
                                            <?= $this->lang->line('loyer_solde'); ?>
                                            <div></div>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col table_facture px-0" style="margin-left : 0px !important;margin-right:0px !important;">
                            <table class="table table-hover fs--1" style="width: 10px;overflow-x:auto">
                                <thead>
                                    <tr class="text-white">
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <th class="p-2 colored_table"> <?= $mois_table['nom'] ?> </th>
                                        <?php endforeach ?>

                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <th class="p-2 colored_table"> Variable </th>
                                        <?php endif ?>

                                        <th class="p-2 colored_table"> Total </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-200">
                                    <!-- Appartement -->
                                    <tr>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2 space-top"><br></td>
                                        <?php endforeach ?>

                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2 space-top"> <br></td>
                                        <?php endif ?>
                                        <td class="p-2 space-top"><br></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $totalAppartementHT = 0;
                                        $totalAppartementTVA = 0;
                                        $totalAppartementTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2 space-top">
                                                <?php
                                                $appartementHT = 0;
                                                $appartementTVA = 0;
                                                $appartementTTC = 0;
                                                $loop = 0;
                                                $franchiseTTC = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $appartementHT += $facture->fact_loyer_app_ht;
                                                        $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                                        $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                                        $totalAppartementHT += $facture->fact_loyer_app_ht;
                                                        $totalAppartementTVA += $appartementTVA;
                                                        $totalAppartementTTC += $appartementTTC;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] += $appartementTTC - $franchiseTTC;
                                                $total_facture_ttc += $appartementTTC - $franchiseTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="appartement">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $appartementHT = $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                                $appartementTVA = $indexation_mois['indexation'][0]->indlo_appartement_ttc - $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                                $appartementTTC = $indexation_mois['indexation'][0]->indlo_appartement_ttc;

                                                                $total_facture_ttc_periode[$key_table] += $appartementTTC;
                                                                $total_facture_ttc += $appartementTTC;
                                                            }
                                                        }
                                                    } ?>
                                                    <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                                                    <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                                <?php } ?>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans appartement -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2 space-top">
                                                <?php
                                                $appartementHT = 0;
                                                $appartementTVA = 0;
                                                $appartementTTC = 0;
                                                $appartementTTCBail = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $appartementHT += $facture->fact_loyer_app_ht;
                                                        $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                                        $appartementTTC += $facture->fact_loyer_app_ttc;

                                                        $totalAppartementHT += $facture->fact_loyer_app_ht;
                                                        $totalAppartementTVA += $appartementTVA;
                                                        $totalAppartementTTC += $appartementTTC;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }

                                                $total_facture_ttc_variable += $appartementTTC;
                                                $total_facture_ttc += $appartementTTC;
                                                ?>
                                                <br>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="appartement">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans appartement -->
                                        <td class="p-2 space-top fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="appartement">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalAppartementHT += $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                        $totalAppartementTVA += $indexation_mois['indexation'][0]->indlo_appartement_ttc - $indexation_mois['indexation'][0]->indlo_appartement_ht;
                                                        $totalAppartementTTC += $indexation_mois['indexation'][0]->indlo_appartement_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Parking -->
                                    <tr>
                                        <?php
                                        $totalParkingHT = 0;
                                        $totalParkingTVA = 0;
                                        $totalParkingTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $parkingHT = 0;
                                                $parkingTVA = 0;
                                                $parkingTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $parkingHT += $facture->fact_loyer_park_ht;
                                                        $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                                        $parkingTTC += $facture->fact_loyer_park_ttc;

                                                        $totalParkingHT += $facture->fact_loyer_park_ht;
                                                        $totalParkingTVA += $parkingTVA;
                                                        $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] += $parkingTTC;
                                                $total_facture_ttc += $parkingTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="parking">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $parkingHT = $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                                $parkingTVA = $indexation_mois['indexation'][0]->indlo_parking_ttc - $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                                $parkingTTC = $indexation_mois['indexation'][0]->indlo_parking_ttc;

                                                                $total_facture_ttc_periode[$key_table] += $parkingTTC;
                                                                $total_facture_ttc += $parkingTTC;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                    <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                    <div class="text-end"> <?= format_number_($parkingTVA) ?></div>
                                                    <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                                <?php } ?>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans parking -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $parkingHT = 0;
                                                $parkingTVA = 0;
                                                $parkingTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $parkingHT += $facture->fact_loyer_park_ht;
                                                        $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                                        $parkingTTC += $facture->fact_loyer_park_ttc;

                                                        $totalParkingHT += $facture->fact_loyer_park_ht;
                                                        $totalParkingTVA += $parkingTVA;
                                                        $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_variable += $parkingTTC;
                                                $total_facture_ttc += $parkingTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="parking">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans parking -->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="parking">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalParkingHT += $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                        $totalParkingTVA += $indexation_mois['indexation'][0]->indlo_parking_ttc - $indexation_mois['indexation'][0]->indlo_parking_ht;
                                                        $totalParkingTTC += $indexation_mois['indexation'][0]->indlo_parking_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                                                <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Forfait charges -->
                                    <tr>
                                        <?php
                                        $totalChargeHT = 0;
                                        $totalChargeTVA = 0;
                                        $totalChargeTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $chargeHT = 0;
                                                $chargeTVA = 0;
                                                $chargeTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $chargeHT += $facture->fact_loyer_charge_ht;
                                                        $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                                        $chargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $totalChargeHT += $facture->fact_loyer_charge_ht;
                                                        $totalChargeTVA += $chargeTVA;
                                                        $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_periode[$key_table] -= $chargeTTC;
                                                $total_facture_ttc -= $chargeTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="charge">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>

                                                <?php if ($loop != 0) { ?>
                                                    <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                                <?php  } else {
                                                    foreach ($indexation_mensuel as $indexation_mois) {
                                                        if ($indexation_mois['nom'] == $mois_table['nom']) {
                                                            if ($indexation_mois['indexation']) {
                                                                $chargeHT = $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                                $chargeTVA = $indexation_mois['indexation'][0]->indlo_charge_ttc - $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                                $chargeTTC = $indexation_mois['indexation'][0]->indlo_charge_ttc;

                                                                $total_facture_ttc_periode[$key_table] -= $chargeTTC;
                                                                $total_facture_ttc -= $chargeTTC;
                                                            }
                                                        }
                                                    } ?>
                                                    <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                    <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                                <?php } ?>
                                            <?php endforeach ?>

                                            <!-- Variable dans Forfait charges -->
                                            <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $chargeHT = 0;
                                                $chargeTVA = 0;
                                                $chargeTTC = 0;
                                                $loop = 0;
                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $chargeHT += $facture->fact_loyer_charge_ht;
                                                        $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                                        $chargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $totalChargeHT += $facture->fact_loyer_charge_ht;
                                                        $totalChargeTVA += $chargeTVA;
                                                        $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                                        $loop += 1;
                                                        $totalLoop += 1;
                                                    }
                                                }
                                                $total_facture_ttc_variable -= $chargeTTC;
                                                $total_facture_ttc -= $chargeTTC;
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="charge">
                                                        (<?= $loop ?>)
                                                    </span>
                                                </div>
                                                <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans Forfait charges -->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="charge">
                                                    (<?= $totalLoop ?>)
                                                </span>
                                            </div>

                                            <?php if ($totalLoop != 0) { ?>
                                                <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                                            <?php  } else {
                                                foreach ($indexation_mensuel as $indexation_mois) {
                                                    if ($indexation_mois['indexation']) {
                                                        $totalChargeHT += $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                        $totalChargeTVA += $indexation_mois['indexation'][0]->indlo_charge_ttc - $indexation_mois['indexation'][0]->indlo_charge_ht;
                                                        $totalChargeTTC += $indexation_mois['indexation'][0]->indlo_charge_ttc;
                                                    }
                                                } ?>
                                                <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                                                <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <!-- Franchise de loyer -->
                                    <tr>
                                        <?php
                                        $totalFranchiseHT = 0;
                                        $totalFranchiseTVA = 0;
                                        $totalFranchiseTTC = 0;
                                        $totalLoop = 0;
                                        ?>
                                        <?php foreach ($list_mois as $key_table => $mois_table) : ?>
                                            <td class="p-2">
                                                <?php
                                                $franchiseHT = 0;
                                                $franchiseTVA = 0;
                                                $franchiseTTC = 0;

                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == ($key_table + 1)) {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $franchiseTVA = $franchiseTTC - $franchiseHT;
                                                        $totalFranchiseHT += $franchiseHT;
                                                        $totalFranchiseTVA += $franchiseTVA;
                                                        $totalFranchiseTTC += $franchiseTTC;
                                                    }
                                                }

                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="<?= $key_table + 1 ?>" data-label="franchise"></span>
                                                </div><br>
                                                <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($franchiseTVA) ?></div>
                                                <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                                            </td>
                                        <?php endforeach ?>

                                        <!-- Variable dans franchise -->
                                        <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                                            <td class="p-2">
                                                <?php
                                                $franchiseHT = 0;
                                                $franchiseTVA = 0;
                                                $franchiseTTC = 0;

                                                foreach ($factures as $facture) {
                                                    if ($facture->periode_mens_id == "13") {
                                                        $franchiseHT = $facture->montant_deduction;
                                                        $franchiseTVA = $facture->tva_deduit;
                                                        $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                                        $franchiseTVA = $franchiseTTC - $franchiseHT;
                                                        $totalFranchiseHT += $franchiseHT;
                                                        $totalFranchiseTVA += $franchiseTVA;
                                                        $totalFranchiseTTC += $franchiseTTC;
                                                    }
                                                }
                                                ?>
                                                <div class="text-end">
                                                    <span class="facture_count" data-periode="periode_mens_id" data-periode_id="13" data-label="franchise"></span>
                                                </div><br>
                                                <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                                                <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                                                <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                                            </td>
                                        <?php endif ?>

                                        <!-- Total dans franchsie-->
                                        <td class="p-2 fw-bold">
                                            <div class="text-end">
                                                <span class="facture_count" data-periode="periode_mens_id" data-periode_id="-1" data-label="franchise"></span>
                                            </div><br>
                                            <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                                            <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                                            <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                                        </td>
                                    </tr>

                                    <!-- Total Factures TTC -->
                                    <?php foreach ($list_mois as $key_fact_ttc => $mois_table) : ?>
                                        <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                            <?= format_number_($total_facture_ttc_periode[$key_fact_ttc]) ?>
                                        </td>
                                    <?php endforeach ?>
                                    <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                                        <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                            <?= format_number_($total_facture_ttc_variable) ?>
                                        </td>
                                    <?php endif ?>
                                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                                        <?= format_number_($total_facture_ttc) ?>
                                    </td>


                                    <!-- Encaissement mensieul -->
                                    <tr class="text-end">
                                        <?php
                                        $total_mensuel = 0;
                                        $encaissement_tab_variable = 0;
                                        ?>
                                        <?php foreach ($indexation_mensuel as $key_mensuel => $value) : ?>
                                            <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                                                <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                                                    <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                                        <?php if ($value_s['nom'] == $value['nom']) : ?>
                                                            <div>
                                                                <?php
                                                                $total_mensuel += $value_s['somme'];
                                                                echo format_number_($value_s['somme']);
                                                                ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </td>
                                        <?php endforeach; ?>

                                        <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                                            <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                                                <?php
                                                $totalEncaissement = 0;
                                                $loop = 0;
                                                foreach ($encaissements as $encaissement) :
                                                    if ($encaissement->enc_periode === "variable") {
                                                        $totalEncaissement += $encaissement->enc_montant;
                                                        $loop += 1;
                                                        $total_mensuel += $encaissement->enc_montant;
                                                    } ?>
                                                <?php endforeach; ?>
                                                <?php $encaissement_tab_variable = $totalEncaissement; ?>
                                                <?php echo format_number_($totalEncaissement); ?>
                        </div>
                        </td>
                    <?php endif ?>

                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <b>
                            <?php echo format_number_($total_mensuel); ?>
                        </b>
                    </td>
                    </tr>

                    <!-- Solde Mensuel -->
                    <tr class="text-end">
                        <?php
                        $solde_total = 0;
                        $solde = 0;
                        foreach ($indexation_mensuel as $key_mensuel => $mensuel) : ?>
                            <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                                <?php
                                if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                                    <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                        <?php if ($value_s['nom'] == $mensuel['nom']) : ?>
                                            <?php
                                            $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                                $value_s['somme'] - $total_facture_ttc_periode[$key_mensuel]
                                                : $value_s['somme'] - $indexation_mensuel_total_ttc_theorique_par_colonne[$key_mensuel];
                                            $solde_total += $solde;
                                            ?>
                                            <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                                <?= format_number_($solde) ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </td>
                        <?php endforeach ?>

                        <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                            <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                                <?php
                                $solde_variable = 0;
                                $solde_variable -= $encaissement_tab_variable;
                                $solde_total += $solde_variable;
                                ?>
                                <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                                    <?= format_number_($solde_variable) ?>
                                </div>
                            </td>
                        <?php endif ?>

                        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                            <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                                <?= format_number_($solde_total) ?>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                    </table>
                    </div>
    </div>
    <?php break; ?>

    <!-- trimestriel -->
<?php
                case 2: ?>
    <?php
                    $listTimestre = array(
                        array("nom" => "T1", "value" => "Mars"),
                        array("nom" => "T2", "value" => "Juin"),
                        array("nom" => "T3", "value" => "Sept"),
                        array("nom" => "T4", "value" => "Déc"),
                    );
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0);
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T1</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T2</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T3</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">T4</th>

                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase"><?= $this->lang->line('loyer_facture'); ?></div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_appartement'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>

                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $appartementHT = $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_trimes['indexation'][0]->indlo_appartement_ttc - $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_trimes['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;

                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalAppartementHT += $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_trimes['indexation'][0]->indlo_appartement_ttc - $indexation_trimes['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_trimes['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_parking'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $parkingHT = $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_trimes['indexation'][0]->indlo_parking_ttc - $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_trimes['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalParkingHT += $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_trimes['indexation'][0]->indlo_parking_ttc - $indexation_trimes['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_trimes['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_forfait'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trimes) {
                                if ($indexation_trimes['nom'] == $trimestre['value']) {
                                    if ($indexation_trimes['indexation']) {
                                        $chargeHT = $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_trimes['indexation'][0]->indlo_charge_ttc - $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_trimes['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trimes) {
                            if ($indexation_trimes['indexation']) {
                                $totalChargeHT += $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_trimes['indexation'][0]->indlo_charge_ttc - $indexation_trimes['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_trimes['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <?= $this->lang->line('loyer_franchise'); ?><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                </th>
                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;

                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == ($key_trimestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $key_trimestre + 1 ?>" data-label="franchise">
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?></div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- Total Factures TTC -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_facture'); ?></th>
                <?php foreach ($listTimestre as $key_fact_trimestre => $trimestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_trimestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement Trimestriel civile-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_encaissement'); ?></th>
                <?php
                    $totalTimestre = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($listTimestre as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <div>
                                        <?php
                                        $totalTimestre += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalTimestre += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($totalTimestre); ?>
                    </b>
                </td>
            </tr>

            <!-- Solde Timestre-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('loyer_solde'); ?></th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listTimestre as $key_trimestre_encaiss => $trimestre) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $trimestre['value']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_trimestre_encaiss]
                                        : $value_s['somme'] - $indexation_trimestriel_total_ttc_theorique_par_colonne[$key_trimestre_encaiss];
                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>

        </tbody>
    </table>
    <?php break; ?>

<?php
                case 3: ?>
    <?php
                    $listSemestriel = array(
                        array("nom" => "S1", "value" => "Juin"),
                        array("nom" => "S2", "value" => "Déc")
                    );
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0);
                    $total_facture_ttc = 0;
    ?>
    <!-- semestriel -->
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">S1</th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">S2</th>

                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase"><?= $this->lang->line('loyer_facture'); ?></div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_appartement'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $appartementHT = $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_semestr['indexation'][0]->indlo_appartement_ttc - $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_semestr['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_semestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalAppartementHT += $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_semestr['indexation'][0]->indlo_appartement_ttc - $indexation_semestr['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_semestr['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <!-- Parking -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_parking'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>
                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $parkingHT = $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_semestr['indexation'][0]->indlo_parking_ttc - $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_semestr['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_semestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalParkingHT += $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_semestr['indexation'][0]->indlo_parking_ttc - $indexation_semestr['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_semestr['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <!-- Forfait charges -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_forfait'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_semestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_semestriel as $indexation_semestr) {
                                if ($indexation_semestr['nom'] == $semestre['value']) {
                                    if ($indexation_semestr['indexation']) {
                                        $chargeHT = $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_semestr['indexation'][0]->indlo_charge_ttc - $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_semestr['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_semestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans forfait charges -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        if ($bail_l->bail_facturation_loyer_celaviegestion == 1) {
                            foreach ($factures as $facture) {
                                if ($facture->periode_semestre_id == "3") {
                                    $chargeHT += $facture->fact_loyer_charge_ht;
                                    $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                    $chargeTTC += $facture->fact_loyer_charge_ttc;
                                    $totalChargeHT += $facture->fact_loyer_charge_ht;
                                    $totalChargeTVA += $chargeTVA;
                                    $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                    $loop += 1;
                                    $totalLoop += 1;
                                }
                            }
                            $total_facture_ttc_variable -= $chargeTTC;
                            $total_facture_ttc -= $chargeTTC;
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>

                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="charge">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_semestriel as $indexation_semestr) {
                            if ($indexation_semestr['indexation']) {
                                $totalChargeHT += $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_semestr['indexation'][0]->indlo_charge_ttc - $indexation_semestr['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_semestr['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <?= $this->lang->line('loyer_franchise'); ?><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listSemestriel as $key_semestre => $semestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == ($key_semestre + 1)) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="<?= $key_semestre + 1 ?>" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_semestre_id == "3") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="3" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_semestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- TOTAL FACTURES TTC -->

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_facture'); ?></th>
                <?php foreach ($listSemestriel as $key_fact_semestre => $semestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_semestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>

            <!-- total Encaissement Semestriel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_encaissement'); ?></th>
                <?php
                    $totalSemestriel = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($listSemestriel as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <div>
                                        <?php
                                        $totalSemestriel += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalSemestriel += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?>
</div>
</td>
<?php endif ?>

<td class="p-2" style="border-bottom: 1px solid #bfadad;">
    <b>
        <?php echo format_number_($totalSemestriel); ?>
    </b>
</td>
</tr>

<!-- Solde Semestriel-->
<tr class="text-end">
    <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('loyer_solde'); ?></th>
    <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listSemestriel as $key_semestre_encaiss => $semestre) : ?>
        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
            <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                    <?php if ($value_s['nom'] == $semestre['value']) : ?>
                        <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_semestre_encaiss]
                                        : $value_s['somme'] - $indexation_semestriel_total_ttc_theorique_par_colonne[$key_semestre_encaiss];
                                    $solde_total += $solde;
                        ?>
                        <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde) ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

        </td>
    <?php endforeach ?>

    <?php if ($bail_l->bail_loyer_variable == 1) : ?>
        <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
            <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
            ?>
            <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                <?= format_number_($solde_variable) ?>
            </div>
        </td>
    <?php endif ?>

    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
        <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
            <?= format_number_($solde_total) ?>
        </div>
    </td>
</tr>
</tbody>
</table>
<?php break; ?>

<!-- annuel -->
<?php
                case 4: ?>
    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = 0;
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"><?= $annee ?></th>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?><th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th> <?php endif ?>
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase"><?= $this->lang->line('loyer_facture'); ?></div>
                </th>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_appartement'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php
                    $appartementHT = 0;
                    $appartementTVA = 0;
                    $appartementTTC = 0;
                    $loop = 0;
                    $annee_id = 0;
                    $franchiseTTC = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;

                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode += $appartementTTC - $franchiseTTC;
                    $total_facture_ttc += $appartementTTC - $franchiseTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="appartement">
                            (<?= $loop ?>)</span>
                    </div>
                    <?php if ($loop != 0) { ?>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $appartementHT = $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                    $appartementTVA = $indexation_ann['indexation'][0]->indlo_appartement_ttc - $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                    $appartementTTC = $indexation_ann['indexation'][0]->indlo_appartement_ttc;

                                    $total_facture_ttc_periode += $appartementTTC;
                                    $total_facture_ttc += $appartementTTC;
                                }
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                                $total_facture_ttc_variable += $appartementTTC;
                                $total_facture_ttc += $appartementTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalAppartementHT += $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_ann['indexation'][0]->indlo_appartement_ttc - $indexation_ann['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_ann['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_parking'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;

                    $parkingHT = 0;
                    $parkingTVA = 0;
                    $parkingTTC = 0;
                    $loop = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingTTC += $facture->fact_loyer_park_ttc;

                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode += $parkingTTC;
                    $total_facture_ttc += $parkingTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="parking">
                            (<?= $loop ?>)</span>
                    </div>

                    <?php if ($loop != 0) { ?>

                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $parkingHT = $indexation_ann['indexation'][0]->indlo_parking_ht;
                                    $parkingTVA = $indexation_ann['indexation'][0]->indlo_parking_ttc - $indexation_ann['indexation'][0]->indlo_parking_ht;
                                    $parkingTTC = $indexation_ann['indexation'][0]->indlo_parking_ttc;

                                    $total_facture_ttc_periode += $parkingTTC;
                                    $total_facture_ttc += $parkingTTC;
                                }
                            }
                        } ?>

                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingTTC = 0;
                        $loop = 0;
                        $parkingTTCBail = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingTTC += $facture->fact_loyer_park_ttc;

                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingTTC;
                        $total_facture_ttc += $parkingTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?></div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>

                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalParkingHT += $indexation_ann['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_ann['indexation'][0]->indlo_parking_ttc - $indexation_ann['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_ann['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_forfait'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php
                    $chargeHT = 0;
                    $chargeTVA = 0;
                    $chargeTTC = 0;
                    $loop = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;

                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>
                <?php
                    $total_facture_ttc_periode -= $chargeTTC;
                    $total_facture_ttc -= $chargeTTC;
                ?>
                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="charge">
                            (<?= $loop ?>)</span>
                    </div>

                    <?php if ($loop != 0) { ?>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['nom'] == "Déc") {
                                if ($indexation_ann['indexation']) {
                                    $chargeHT = $indexation_ann['indexation'][0]->indlo_charge_ht;
                                    $chargeTVA = $indexation_ann['indexation'][0]->indlo_charge_ttc - $indexation_ann['indexation'][0]->indlo_charge_ht;
                                    $chargeTTC = $indexation_ann['indexation'][0]->indlo_charge_ttc;

                                    $total_facture_ttc_periode -= $chargeTTC;
                                    $total_facture_ttc -= $chargeTTC;
                                }
                            }
                        } ?>

                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        $chargeTTCBail = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;

                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                                $total_facture_ttc_variable -= $chargeTTC;
                                $total_facture_ttc -= $chargeTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charge -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="charge">
                            (<?= $totalLoop ?>)</span>
                    </div>
                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>

                    <?php  } else {
                        foreach ($indexation_annuel as $indexation_ann) {
                            if ($indexation_ann['indexation']) {
                                $totalChargeHT += $indexation_ann['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_ann['indexation'][0]->indlo_charge_ttc - $indexation_ann['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_ann['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>

                        <div class="text-end"><?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <?= $this->lang->line('loyer_franchise'); ?><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;

                    $franchiseHT = 0;
                    $franchiseTVA = 0;
                    $franchiseTTC = 0;
                    foreach ($listAnnuel as $key_annuel) : ?>
                    <?php
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == $key_annuel->periode_annee_id && $key_annuel->periode_annee_libelle == $annee_courant) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                                $annee_id = $key_annuel->periode_annee_id;
                            }
                        }
                    ?>
                <?php endforeach ?>

                <td class="p-2">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="<?= $annee_id ?>" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                    <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                </td>
                <!-- Variable dans franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_annee_id == "0") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_annee_id" data-periode_id="0" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_annee_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_facture'); ?></th>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc_periode) ?>
                </td>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement annuel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_encaissement'); ?></th>
                <?php
                    $total_annuel = 0;
                    $encaissement_tab_variable = 0;
                ?>
                <?php foreach ($indexation_annuel as $key_annuel => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['nom']) : ?>
                                    <div>
                                        <?php
                                        $total_annuel += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $total_annuel += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                        <?php echo format_number_($totalEncaissement); ?></div>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($total_annuel); ?>
                    </b>
                </td>
            </tr>


            <!-- Solde Mensuel -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('loyer_solde'); ?></th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($indexation_annuel as $key_annuel => $annuel) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $annuel['nom']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode
                                        : $value_s['somme'] - $indexation_annuel_total_ttc_theorique_par_colonne[$key_annuel];

                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>



        </tbody>
    </table>
    <?php break; ?>
    <!-- trimestriel décalée -->
<?php
                case 5: ?>
    <?php
                    $total_facture_ttc_variable = 0;
                    $total_facture_ttc_periode = array(0, 0, 0, 0);
                    $total_facture_ttc = 0;
    ?>
    <table class="table table-hover fs--1">
        <thead class="text-white">
            <tr class="text-center">
                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;"></th>
                <?php foreach ($listTimestre as $list) : ?>
                    <?php
                        $debut = date('m/Y', strtotime($list['debut']));
                        $fin = date('m/Y', strtotime($list['fin']));
                    ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">
                        <?= $list['nom'] ?> <br>
                        <span style="font-size: 10px"><i>(<?= $debut . ' - ' . $fin ?>)</i></span>
                    </th>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Variable</th>
                <?php endif ?>

                <th class="p-2" style="border-right: 1px solid #ffff;background:#2569C3;">Total</th>
            </tr>
        </thead>
        <tbody class="bg-200">
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div class="text-uppercase"><?= $this->lang->line('loyer_facture'); ?></div>
                </th>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <td class="bg-200"></td>
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="bg-200"></td>
                <?php endif ?>
                <td class="bg-200"></td>
            </tr>
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_appartement'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalAppartementHT = 0;
                    $totalAppartementTVA = 0;
                    $totalAppartementTTC = 0;
                    $totalLoop = 0;
                ?>

                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $loop = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc + $franchiseTTC;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC - $franchiseTTC;
                        $total_facture_ttc += $appartementTTC - $franchiseTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?></div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $appartementHT = $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                        $appartementTVA = $indexation_trim['indexation'][0]->indlo_appartement_ttc - $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                        $appartementTTC = $indexation_trim['indexation'][0]->indlo_appartement_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $appartementTTC;
                                        $total_facture_ttc += $appartementTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans appartement -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $appartementHT = 0;
                        $appartementTVA = 0;
                        $appartementTTC = 0;
                        $appartementTTCBail = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $appartementHT += $facture->fact_loyer_app_ht;
                                $appartementTVA += $facture->fact_loyer_app_ttc + $franchiseTTC - $facture->fact_loyer_app_ht;
                                $appartementTTC += $facture->fact_loyer_app_ttc;

                                $totalAppartementHT += $facture->fact_loyer_app_ht;
                                $totalAppartementTVA += $appartementTVA;
                                $totalAppartementTTC += $appartementTTC;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $appartementTTC;
                        $total_facture_ttc += $appartementTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="appartement">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($appartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($appartementTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans appartement -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalAppartementHT += $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTVA += $indexation_trim['indexation'][0]->indlo_appartement_ttc - $indexation_trim['indexation'][0]->indlo_appartement_ht;
                                $totalAppartementTTC += $indexation_trim['indexation'][0]->indlo_appartement_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalAppartementHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalAppartementTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_parking'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalParkingHT = 0;
                    $totalParkingTVA = 0;
                    $totalParkingTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $parkingHT = $indexation_trim['indexation'][0]->indlo_parking_ht;
                                        $parkingTVA = $indexation_trim['indexation'][0]->indlo_parking_ttc - $indexation_trim['indexation'][0]->indlo_parking_ht;
                                        $parkingtTTC = $indexation_trim['indexation'][0]->indlo_parking_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] += $parkingtTTC;
                                        $total_facture_ttc += $parkingtTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                            <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans parking -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $parkingHT = 0;
                        $parkingTVA = 0;
                        $parkingtTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $parkingHT += $facture->fact_loyer_park_ht;
                                $parkingTVA += $facture->fact_loyer_park_ttc - $facture->fact_loyer_park_ht;
                                $parkingtTTC += $facture->fact_loyer_park_ttc;
                                $totalParkingHT += $facture->fact_loyer_park_ht;
                                $totalParkingTVA += $parkingTVA;
                                $totalParkingTTC += $facture->fact_loyer_park_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable += $parkingtTTC;
                        $total_facture_ttc += $parkingtTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="parking">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"> <?= format_number_($parkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($parkingtTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans parking -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="parking">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalParkingHT += $indexation_trim['indexation'][0]->indlo_parking_ht;
                                $totalParkingTVA += $indexation_trim['indexation'][0]->indlo_parking_ttc - $indexation_trim['indexation'][0]->indlo_parking_ht;
                                $totalParkingTTC += $indexation_trim['indexation'][0]->indlo_parking_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalParkingHT) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTVA) ?> </div>
                        <div class="text-end"> <?= format_number_($totalParkingTTC) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <div><?= $this->lang->line('loyer_forfait'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></div>
                    <div class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></div>
                </th>

                <?php
                    $totalChargeHT = 0;
                    $totalChargeTVA = 0;
                    $totalChargeTTC = 0;
                    $totalLoop = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>

                        <?php if ($loop != 0) { ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php  } else {
                            foreach ($indexation_trimestriel as $indexation_trim) {
                                if ($indexation_trim['nom'] == $trimestre['value']) {
                                    if ($indexation_trim['indexation']) {
                                        $chargeHT = $indexation_trim['indexation'][0]->indlo_charge_ht;
                                        $chargeTVA = $indexation_trim['indexation'][0]->indlo_charge_ttc - $indexation_trim['indexation'][0]->indlo_charge_ht;
                                        $chargeTTC = $indexation_trim['indexation'][0]->indlo_charge_ttc;

                                        $total_facture_ttc_periode[$key_trimestre] -= $chargeTTC;
                                        $total_facture_ttc -= $chargeTTC;
                                    }
                                }
                            } ?>
                            <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                            <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                        <?php } ?>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans charge -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $chargeHT = 0;
                        $chargeTVA = 0;
                        $chargeTTC = 0;
                        $loop = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $chargeHT += $facture->fact_loyer_charge_ht;
                                $chargeTVA += $facture->fact_loyer_charge_ttc - $facture->fact_loyer_charge_ht;
                                $chargeTTC += $facture->fact_loyer_charge_ttc;
                                $totalChargeHT += $facture->fact_loyer_charge_ht;
                                $totalChargeTVA += $chargeTVA;
                                $totalChargeTTC += $facture->fact_loyer_charge_ttc;

                                $loop += 1;
                                $totalLoop += 1;
                            }
                        }
                        $total_facture_ttc_variable -= $chargeTTC;
                        $total_facture_ttc -= $chargeTTC;
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="charge">
                                (<?= $loop ?>)</span>
                        </div>
                        <div class="text-end"><?= format_number_($chargeHT, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTVA, true) ?> </div>
                        <div class="text-end"><?= format_number_($chargeTTC, true) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans forfait charges -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="appartement">
                            (<?= $totalLoop ?>)</span>
                    </div>

                    <?php if ($totalLoop != 0) { ?>
                        <div class="text-end"> <?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php  } else {
                        foreach ($indexation_trimestriel as $indexation_trim) {
                            if ($indexation_trim['indexation']) {
                                $totalChargeHT += $indexation_trim['indexation'][0]->indlo_charge_ht;
                                $totalChargeTVA += $indexation_trim['indexation'][0]->indlo_charge_ttc - $indexation_trim['indexation'][0]->indlo_charge_ht;
                                $totalChargeTTC += $indexation_trim['indexation'][0]->indlo_charge_ttc;
                            }
                        } ?>
                        <div class="text-end"> <?= format_number_($totalChargeHT, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTVA, true) ?> </div>
                        <div class="text-end"> <?= format_number_($totalChargeTTC, true) ?> </div>
                    <?php } ?>
                </td>
            </tr>

            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;">
                    <?= $this->lang->line('loyer_franchise'); ?><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ht'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_tva'); ?></span><br>
                    <span class="fw-normal">&emsp; &ensp;<?= $this->lang->line('loyer_appartement_ttc'); ?></span><br>
                </th>

                <?php
                    $totalFranchiseHT = 0;
                    $totalFranchiseTVA = 0;
                    $totalFranchiseTTC = 0;
                ?>
                <?php foreach ($listTimestre as $key_trimestre => $trimestre) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == $trimestre['periode']) {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="<?= $trimestre['periode'] ?>" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endforeach ?>

                <!-- Variable dans Franchise -->
                <?php if ($bail_l->bail_loyer_variable == 1 || $bail_l->bail_loyer_variable == 2) : ?>
                    <td class="p-2">
                        <?php
                        $franchiseHT = 0;
                        $franchiseTVA = 0;
                        $franchiseTTC = 0;
                        foreach ($factures as $facture) {
                            if ($facture->periode_timestre_id == "5") {
                                $franchiseHT = $facture->montant_deduction;
                                $franchiseTVA = $facture->tva_deduit;
                                $franchiseTTC = $franchiseHT + ($franchiseHT * $franchiseTVA / 100);
                                $franchiseTVA = $franchiseTTC - $franchiseHT;
                                $totalFranchiseHT += $franchiseHT;
                                $totalFranchiseTVA += $franchiseTVA;
                                $totalFranchiseTTC += $franchiseTTC;
                            }
                        }
                        ?>
                        <div class="text-end">
                            <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="5" data-label="franchise"></span>
                        </div><br>
                        <div class="text-end"> <?= $franchiseHT != 0 ? '-' . format_number_($franchiseHT) : format_number_($franchiseHT) ?> </div>
                        <div class="text-end"> <?= format_number_($franchiseTVA) ?> </div>
                        <div class="text-end"> <?= $franchiseTTC != 0 ? '-' . format_number_($franchiseTTC) : format_number_($franchiseTTC) ?> </div>
                    </td>
                <?php endif ?>

                <!-- Total dans Franchise -->
                <td class="p-2 fw-bold">
                    <div class="text-end">
                        <span class="facture_count" data-periode="periode_timestre_id" data-periode_id="-1" data-label="franchise"></span>
                    </div><br>
                    <div class="text-end"> <?= $totalFranchiseHT != 0 ? '-' . format_number_($totalFranchiseHT) : format_number_($totalFranchiseHT) ?> </div>
                    <div class="text-end"> <?= format_number_($totalFranchiseTVA) ?> </div>
                    <div class="text-end"> <?= $totalFranchiseTTC != 0 ? '-' . format_number_($totalFranchiseTTC) : format_number_($totalFranchiseTTC) ?> </div>
                </td>
            </tr>

            <!-- Total Factures TTC -->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_facture'); ?></th>
                <?php foreach ($listTimestre as $key_fact_trimestre => $trimestre) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_periode[$key_fact_trimestre]) ?>
                    </td>
                <?php endforeach ?>
                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                        <?= format_number_($total_facture_ttc_variable) ?>
                    </td>
                <?php endif ?>
                <td class="fw-bold p-2 text-end" style="border-bottom: 1px solid #bfadad;">
                    <?= format_number_($total_facture_ttc) ?>
                </td>
            </tr>


            <!-- Encaissement Trimestriel-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('total_encaissement'); ?></th>
                <?php
                    $totalTimestre = 0;
                    $encaissement_tab_variable = 0;
                    $loop = 0;
                ?>
                <?php foreach ($listTimestre as $key => $value) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $value['value']) : ?>
                                    <!-- <div class="count_encaissement" data-enc_periode="<?= $listTimestre[$key]['nom'] ?>">
                                                        (<?php
                                                            $loop += 1;
                                                            echo $loop;
                                                            ?>)
                                                    </div> -->
                                    <div>
                                        <?php
                                        $totalTimestre += $value_s['somme'];
                                        echo format_number_($value_s['somme']);
                                        ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $totalEncaissement = 0;
                        $loop = 0;
                        foreach ($encaissements as $encaissement) :
                            if ($encaissement->enc_periode === "variable") {
                                $totalEncaissement += $encaissement->enc_montant;
                                $loop += 1;
                                $totalTimestre += $encaissement->enc_montant;
                            } ?>
                        <?php endforeach; ?>
                        <?php echo format_number_($totalEncaissement); ?></div>
                        <?php $encaissement_tab_variable = $totalEncaissement; ?>
                    </td>
                <?php endif ?>

                <td class="p-2" style="border-bottom: 1px solid #bfadad;">
                    <b>
                        <?php echo format_number_($totalTimestre); ?>
                    </b>
                </td>
            </tr>

            <!-- Solde Timestre-->
            <tr class="text-end">
                <th class="text-white text-start p-2" style="background:#2569C3;"><?= $this->lang->line('loyer_solde'); ?></th>
                <?php
                    $solde_total = 0;
                    $solde = 0;
                    foreach ($listTimestre as $key_semestre_encaiss => $semestre) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        if (isset($sommes_encaissements) && !empty($sommes_encaissements)) :  ?>
                            <?php foreach ($sommes_encaissements as $key_s => $value_s) : ?>
                                <?php if ($value_s['nom'] == $semestre['value']) : ?>
                                    <?php
                                    $solde = ($bail_l->bail_facturation_loyer_celaviegestion == 1) ?
                                        $value_s['somme'] - $total_facture_ttc_periode[$key_semestre_encaiss]
                                        : $value_s['somme'] - $indexation_trimestriel_total_ttc_theorique_par_colonne[$key_semestre_encaiss];
                                    $solde_total += $solde;
                                    ?>
                                    <div class=" <?= $solde < 0 ? 'text-danger' : 'text-success' ?>">
                                        <?= format_number_($solde) ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </td>
                <?php endforeach ?>

                <?php if ($bail_l->bail_loyer_variable == 1) : ?>
                    <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                        <?php
                        $solde_variable = 0;
                        $solde_variable -= $encaissement_tab_variable;
                        $solde_total += $solde_variable;
                        ?>
                        <div class=" <?= $solde_variable < 0 ? 'text-danger' : 'text-success' ?>">
                            <?= format_number_($solde_variable) ?>
                        </div>
                    </td>
                <?php endif ?>

                <td class="fw-bold p-2" style="border-bottom: 1px solid #bfadad;">
                    <div class="<?= $solde_total < 0 ? 'text-danger' : 'text-success' ?>">
                        <?= format_number_($solde_total) ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <?php break; ?>
<?php endswitch ?>
<?php endforeach ?>
<br>
<br>

</div>

<span></span>