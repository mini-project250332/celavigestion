<input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_charge_<?= $cliLot_id; ?>">

<div class="contenair-title pt-2 pb-2">
    <div class="row m-0">
        <div class="col-md-6 px-0 d-flex justify-content-between pb-2">
            <div class="px-2 d-flex align-items-center">
                <i class="fas fa-file-alt"></i> &nbsp;&nbsp;
                <h5><?= $this->lang->line('list_acte'); ?></h5>
            </div>
            <div class="px-2">
                <select class="no_effect form-select fs--1 m-1" id="annee_loyer_<?= $cliLot_id; ?>" style="width: 96%">
                    <?php for ($i = intval(date("Y")) + 1; $i >= intval(date("Y")) - 1; $i--) : ?>
                        <option value="<?= $i ?>" <?= ($i == date("Y")) ? "selected" : "" ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor ?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="flex-grow-1" id="loyer-lot_<?= $cliLot_id ?>">

</div>

<script>
    $(document).ready(function() {
        getFicheLoyerLot(<?= $cliLot_id; ?>, <?= intval(date('Y')) ?>);
    });

    $(document).on('change', '#annee_loyer_<?= $cliLot_id ?>', function() {
        getFicheLoyerLot(<?= $cliLot_id; ?>, $(this).val());
    });
</script>