
<?php $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;  ?>
<div class="contenair-title pt-2 pb-2">
    <div class="px-2"></div>
    <div class="px-2"></div>
</div>

<div class="flex-grow-1" id="content-tom-customer_<?= $cliLot_id ?>">
    <div class="h-100 w-100 p-0 py-1 add-tom" style="overflow-y:auto;">
        <?php if ($taxe_empty == true || $taxe_montant == NULL) : ?>
            <div class="col-12 p-0 py-1 add-tom">
                <div class="row m-0">
                    <div class="col p-1">
                        <div class="card shadow p-3 mb-5 bg-body rounded-0 h-100">
                            <div class="card-body text-center mt-2 message-alert">
                                <?= $this->lang->line('alert_sans_tom'); ?> <span class="fw-bold"><?= $progm_nom ?></span> <?= $this->lang->line('pour_'); ?> <?= $date ?> <br><br>
                                <button type="button" class="btn btn-primary" id="new_tax_<?= $cliLot_id ?>" <?= $taxe_cloture == 1 ? 'disabled' : '' ?>>
                                    <?= $this->lang->line('button_communique_taxe') ?>
                                </button>
                            </div>
                            <?php if ($taxe_cloture == 1) : ?>
                                <div class="alert alert-info border-2 text-center mt-1" role="alert">
                                    <i><?= str_replace('@date', date("d/m/Y", strtotime($taxe_date_cloture)), $this->lang->line('mesg_fin_taxe')); ?></i>
                                </div>
                            <?php else : ?>
                                <div class="card-body text-center message-alert">
                                    <?= str_replace('@annee', $date, $this->lang->line('mesg_sans_taxe')) ?>
                                </div>
                                <div class="card-body text-center message-alert">
                                    <button type="button" class="btn btn-secondary" id="btn_aucun_taxe_<?= $cliLot_id ?>" data-id="<?= $cliLot_id ?>">
                                        <i class="fas fa-exclamation-triangle"></i> &nbsp;
                                        <?= str_replace('@annee', $date, $this->lang->line('btn_aucun_taxe')) ?>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <div class="text-end mt-4">
                                <button type="button" class="btn btn-sm btn-primary historique_taxe_<?= $cliLot_id ?>"><?= $this->lang->line('button_voirprec_tom'); ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="py-4">
                        <div class="div-client-conseil">
                            <div class="image-client-conseil">
                                <div class="image-silouette">
                                    <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                                </div>
                                <div class="text-conseil flex-grow-1">
                                    <div class="contenu-conseil">

                                        <div class="bubble bubble-bottom-left" style="width: 700px !important;">
                                            <span>
                                                <?= $this->lang->line('texte-conseil-clientTom'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="col-12 p-0 py-1">
                <div class="row m-0">
                    <div class="col p-1">
                        <div class="card shadow p-3 mb-5 rounded-0 h-100">
                            <div class="card-body ">
                                <h5 class="text-center">
                                    <?= $taxe_valide == 1 ? $alert_taxe = str_replace('@annee', $date, $this->lang->line('alert_taxe_valider')) : $alert_taxe = str_replace('@annee', $date, $this->lang->line('alert_taxe')); ?><span class="fw-bold"><?= $progm_nom ?></span><?= $this->lang->line('suite_alert_taxe'); ?><br><br>
                                </h5>
                                <h5 class="information"> <?= $this->lang->line('title_tom_information'); ?> </h5><br>
                                <table class="table table-borderless mb-0">
                                    <tbody>
                                        <tr class="border-bottom">
                                            <th class="ps-0"><?= $this->lang->line('document_tom'); ?></th>
                                            <td class="pe-0">
                                                <?php if (!empty($document)) : ?>
                                                    <?php foreach ($document as $documents) : ?>
                                                        <span class="badge rounded-pill badge-soft-secondary" style="cursor: pointer; font-size: 12px" onclick="forceDownload('<?= base_url() . $documents->doc_taxe_path ?>','<?= $documents->doc_taxe_nom ?>')">
                                                            <?= $documents->doc_taxe_nom ?>
                                                        </span>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0" style="width: 200px;"><?= $this->lang->line('montant_tax') ?></th>
                                            <td class="pe-0 fw-bold">
                                                <?= $taxe_montant . ' ' . '€' ?>
                                            </td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0"><?= $this->lang->line('montant_tom') ?></th>
                                            <td class="pe-0 fw-bold">
                                                <?= $taxe_tom . ' ' . '€' ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ($taxe_valide == 1) : ?>
                                <?php
                                setlocale(LC_TIME, 'fr_FR.utf8');
                                $timestamp = strtotime($taxe_date_envoi_mail);
                                $formattedDate = strftime('%d %B %Y', $timestamp);
                                if ($_SESSION['session_utilisateur_client']['lang'] != 'french') {
                                    $formattedDate = date("F j, Y", strtotime($taxe_date_envoi_mail));
                                }
                                ?>
                                <div class="alert alert-info text-center" role="alert">
                                    <?= $this->lang->line('info_alert'); ?><br>
                                    <?php if ($taxe_date_envoi_mail != '') : ?>
                                        <?= $this->lang->line('demande_rembours'); ?><span class="fw-bold"><?= $formattedDate; ?></span> <?= $this->lang->line('suite_demande_rembours'); ?>
                                    <?php endif ?>
                                </div>
                            <?php else : ?>
                                <div class="alert alert-warning text-center" role="alert">
                                    <?= $this->lang->line('info_warning'); ?>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary" id="modif_<?= $cliLot_id ?>"><?= $this->lang->line('button_modifier_information'); ?></button>
                                </div>
                            <?php endif ?>
                            <div class="text-end">
                                <button type="button" class="btn btn-sm btn-primary historique_taxe_<?= $cliLot_id ?>"><?= $this->lang->line('button_voirprec_tom'); ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="py-4">
                        <div class="div-client-conseil">
                            <div class="image-client-conseil">
                                <div class="image-silouette">
                                    <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                                </div>
                                <div class="text-conseil flex-grow-1">
                                    <div class="contenu-conseil">
                                        <div class="bubble bubble-bottom-left" style="width: 700px !important;">
                                            <span>
                                                <?= $this->lang->line('texte-conseil-clientTom'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<script>
    $('#new_tax_<?= $cliLot_id ?>').click(function(e) {
        getformtax(<?= $cliLot_id ?>);
    });

    $('#modif_<?= $cliLot_id ?>').click(function(e) {
        getformtax(<?= $cliLot_id ?>);
    });

    $('.historique_taxe_<?= $cliLot_id ?>').click(function(e) {
        tableHistorique(<?= $cliLot_id ?>);
    });

    $(document).on('click', '#btn_aucun_taxe_<?= $cliLot_id ?>', function(e) {
        e.preventDefault();
        var data = {
            action: "demande",
            cliLot_id: $(this).attr('data-id')
        }
        finSaisietaxe(data);
    });
</script>