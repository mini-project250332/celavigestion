<style>
    .form-taxe {
        text-align: right;
        height: 36px;
    }

    .help_tom {
        position: absolute;
        top: 1.6rem;
        left: 54%;
    }
</style>

<?php $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1; ?>

<div class="col-12 p-0 py-1 form-add-tom">
    <div class="row m-0">
        <div class="col p-1">
            <div class="card shadow p-3 mb-5 rounded-0 h-100">
                <div class="card-body">
                    <h5 class="text-center"> <?= str_replace('@annee', $date, $this->lang->line('form_title')); ?></h5>
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="col-8">
                            <div class="mt-5">
                                <?php if (!empty($taxe)) : ?>
                                    <input type="hidden" id="taxe_id_<?= $cliLot_id ?>" value="<?= $taxe->taxe_id ?>">
                                    <?php foreach ($taxe->document as $documents) : ?>
                                        <div class="ms-10 doc_<?= $documents->doc_taxe_id ?>">
                                            <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $documents->doc_taxe_path ?>')" data-url="<?= '' ?>" title="Télécharger fichier" style="cursor: pointer;">
                                                <?= $documents->doc_taxe_nom ?>
                                            </span>
                                            <span class="badge rounded-pill badge-soft-light btn-close text-white" id="removefile" data-id="<?= $documents->doc_taxe_id ?>" style="cursor: pointer;" title="Supprimer fichier">.</span>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                                <input type="hidden" name="" id="cliLot_id_<?= $cliLot_id ?>" value="<?= $cliLot_id ?>">
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"><?= $this->lang->line('fichier_taxe_fonciere') ?> </label>
                                        <input type="file" class="form-control m-1 w-50" id="file_<?= $cliLot_id ?>" name="file_<?= $cliLot_id ?>" multiple>
                                        <div class="help_tom">
                                            <i class="text-danger fs--2 help_file_<?= $cliLot_id ?>"></i>
                                        </div>
                                    </div>

                                    <div class="help fs--1 ms-10">
                                        <i class="msg_<?= $cliLot_id ?>"><?= $this->lang->line('alert_file_taxe') ?></i>
                                    </div>
                                </div>
                                <div class="form-group inline p-0 mb-2">
                                    <div class="col-sm-12">
                                        <label data-width="200"><?= $this->lang->line('montant_taxe_fonciere') ?> </label>
                                        <div class="input-group w-50">
                                            <input type="text" placeholder="0" class="form-control mb-0 chiffre form-taxe" id="taxe_montant_<?= $cliLot_id ?>" name="taxe_montant_<?= $cliLot_id ?>" value="<?= !empty($taxe) ? $taxe->taxe_montant : '' ?>">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                        <div class="help_tom">
                                            <i class="text-danger fs--2 help_taxe_montant_<?= $cliLot_id ?>"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group inline p-0">
                                    <div class="col-sm-12">
                                        <label data-width="200"><?= $this->lang->line('montant_taxe_tom') ?> </label>
                                        <div class="input-group w-50">
                                            <input type="text" placeholder="0" class="form-control mb-0 chiffre form-taxe" id="taxe_tom_<?= $cliLot_id ?>" name="taxe_tom_<?= $cliLot_id ?>" value="<?= !empty($taxe) ? $taxe->taxe_tom : '' ?>">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">€</span>
                                            </div>
                                        </div>
                                        <div class="help_tom">
                                            <i class="text-danger fs--2 help_taxe_tom_<?= $cliLot_id ?>"></i>
                                        </div>
                                    </div>

                                    <div class="help fs--1 ms-10 msg_montant_tom_<?= $cliLot_id ?> d-none">
                                        <i class=""><?= $this->lang->line('alert_montant_tom') ?></i>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mt-2 ms-10">
                                <button type="button" class="btn btn-secondary annuler_taxe_<?= $cliLot_id ?>"><?= $this->lang->line('button_annuler') ?></button>
                                <button type="button" class="btn btn-primary save_tom_<?= $cliLot_id ?>" data-lang="<?= $_SESSION['session_utilisateur_client']['lang'] ?>"><?= $this->lang->line('button_enregistrer') ?></button>
                            </div>
                        </div>
                        <div class="col"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.annuler_taxe_<?= $cliLot_id ?>').click(function(e) {
        localStorage.setItem("lastActiveTab", <?= $cliLot_id ?>);
        getListTom();
    });

    $('.save_tom_<?= $cliLot_id ?>').click(function(e) {
        saveTom(<?= $cliLot_id ?>, $(this).attr('data-lang'));
    });

    $('#taxe_tom_<?= $cliLot_id ?>').keyup(function(e) {
        messageAlert($(this).val(), <?= $cliLot_id ?>);
    });
</script>