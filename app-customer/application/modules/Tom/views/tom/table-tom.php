
<div class="col-12 p-0 py-1 table-historique">
    <div class="row m-0">
        <div class="col p-1">
            <div class="card shadow p-3 mb-5 rounded-0 h-100">
                <div class="card-body ">
                    <h5 class="text-center">
                        <?= $this->lang->line('consultation_title_taxe'); ?>
                    </h5>
                    <div class="text-end">
                        <button type="button" id="voir-annee-prec_<?= $cliLot_id ?>" class="btn btn-sm btn-secondary"> <i class="fa fa-undo"></i>&nbsp;<?= $this->lang->line('button_retour'); ?></button>
                    </div>
                    <table class="table table-borderless fs--1 mb-0 mt-2">
                        <thead class="text-900">
                            <tr>
                                <th class="text-center text-white p-2" scope="col"><?= $this->lang->line('annee'); ?></th>
                                <th class="text-center text-white p-2" scope="col"><?= $this->lang->line('fichiers'); ?></th>
                                <th class="text-center text-white p-2" scope="col"><?= $this->lang->line('montant_tab_taxe'); ?></th>
                                <th class="text-center text-white p-2" scope="col"><?= $this->lang->line('montant_tab_tom'); ?></th>
                                <th class="text-center text-white p-2" scope="col"><?= $this->lang->line('demand_tab_rem'); ?></th>
                            </tr>
                        </thead>
                        <tbody class="bg-100">
                            <?php if (!empty($taxe)) : ?>
                                <?php foreach ($taxe as $taxes) : ?>
                                    <tr class="text-center " style="cursor:pointer;">
                                        <td><?= $taxes->annee == 0 ? "Années antérieures" : $taxes->annee ?></td>
                                        <td>
                                            <?php if (!empty($taxes->document)) : ?>
                                                <?php foreach ($taxes->document as $documents) : ?>
                                                    <span class="badge rounded-pill badge-soft-secondary" style="cursor: pointer; font-size: 12px;paddin-bottom: 2px;" onclick="forceDownload('<?= base_url() . $documents->doc_taxe_path ?>','<?= $documents->doc_taxe_nom ?>')">
                                                        <?= $documents->doc_taxe_nom ?>
                                                    </span><br>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </td>
                                        <td><?= $taxes->taxe_montant ?> €</td>
                                        <td><?= $taxes->taxe_tom ?> €</td>
                                        <td><?= !empty($taxes->taxe_date_envoi_mail) ? date('d/m/Y', strtotime($taxes->taxe_date_envoi_mail)) : '-' ?></td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#voir-annee-prec_<?= $cliLot_id ?>").click(function(e) {
        localStorage.setItem("lastActiveTab", <?= $cliLot_id ?>);
        getListTom();
    });
</script>