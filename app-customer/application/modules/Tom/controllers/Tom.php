<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Tom extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Tom";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/tom/tom.js",
        );
        $this->_css_personnaliser = array(
            "css/tom/tom.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "tom";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "tom/contenaire-tom" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getListTom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('MandatNew_m');
                $this->load->model('Taxe_m');
                $this->load->model('DocumentTaxe_model');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'join_orientation' => 'left',
                    'method' => "result",
                );

                $client_lot = $this->lot->get($params);

                $data_lot = array();

                if (!empty($client_lot)) {
                    foreach ($client_lot as $key => $value) {
                        $request_date = $this->MandatNew_m->get(
                            array(
                                'clause' => array('cliLot_id' => $value->cliLot_id),
                                'order_by_columns' => 'mandat_date_signature DESC',
                                'method' => 'result'
                            )
                        );

                        $signatures = array_map(function ($element) {
                            return $element->mandat_date_signature;
                        }, $request_date);

                        array_push($data_lot, [
                            'cliLot_id' => $value->cliLot_id,
                            'progm_nom' => $value->progm_nom,
                            'date_signature' => !empty($request_date) && !empty($signatures) ? true : false,
                            'annee' => !empty($request_date)  && !empty($signatures) ?  $signatures[0] : '',
                        ]);
                    }
                }

                $data_lot = array_map(function ($item) {
                    $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                    $array = $this->Taxe_m->get(array(
                        'clause' => array('cliLot_id' => $item['cliLot_id'], 'annee' => $date),
                        'method' => 'row'
                    ));

                    $array_document = $this->DocumentTaxe_model->get(array(
                        'clause' => array('cliLot_id' => $item['cliLot_id'], 'annee' => $date, 'doc_taxe_etat' => 1),
                    ));

                    !empty($array) ? $item['taxe_empty'] = false : $item['taxe_empty'] = true;
                    !empty($array) ? $item['taxe_montant'] = $array->taxe_montant : $item['taxe_montant'] = "";
                    !empty($array) ? $item['taxe_tom'] = $array->taxe_tom : $item['taxe_tom'] = "";
                    !empty($array) ? $item['taxe_date_envoi_mail'] = $array->taxe_date_envoi_mail : $item['taxe_date_envoi_mail'] = "";
                    !empty($array) ? $item['taxe_date_modification'] = $array->taxe_date_modification : $item['taxe_date_modification'] = "";
                    !empty($array) ? $item['taxe_valide'] = $array->taxe_valide : $item['taxe_valide'] = "";
                    !empty($array) ? $item['taxe_date_cloture'] = $array->taxe_date_cloture : $item['taxe_date_cloture'] = "";
                    !empty($array) ? $item['taxe_cloture'] = $array->taxe_cloture : $item['taxe_cloture'] = "";
                    $item['document'] = $array_document;

                    return $item;
                }, $data_lot);

                $data['client_lot'] = $data_lot;
                $this->renderComponant("Tom/tom/fiche-tom", $data);
            }
        }
    }

    public function tableHistorique()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Taxe_m');
                $this->load->model('DocumentTaxe_model');

                $taxe = $this->Taxe_m->getpasttaxe($_POST['data']['cliLot_id']);

                foreach ($taxe as $key => $value) {
                    $array_document = $this->DocumentTaxe_model->get(array(
                        'clause' => array('cliLot_id' => $value->cliLot_id, 'annee' => $value->annee, 'doc_taxe_etat' => 1),
                    ));
                    $value->document = $array_document;
                }

                $data['taxe'] = $taxe;
                $data['cliLot_id'] = $_POST['data']['cliLot_id'];
                echo $_POST['data']['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Tom/tom/table-tom", $data);
            }
        }
    }

    public function getformtax()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Taxe_m');
                $this->load->model('DocumentTaxe_model');

                $taxe = $this->Taxe_m->get(array(
                    'clause' => array('cliLot_id' => $_POST['data']['cliLot_id'], 'annee' => $_POST['data']['year']),
                    'method' => 'row'
                ));

                if (!empty($taxe)) {
                    $array_document = $this->DocumentTaxe_model->get(array(
                        'clause' => array('cliLot_id' => $taxe->cliLot_id, 'annee' => $taxe->annee, 'doc_taxe_etat' => 1),
                    ));
                    $taxe->document = $array_document;
                    $data['taxe'] = $taxe;
                }

                $data['cliLot_id'] = $_POST['data']['cliLot_id'];
                echo $_POST['data']['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Tom/tom/form-tom", $data);
            }
        }
    }

    public function saveTom()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

                $this->load->model('Taxe_m');
                $this->load->model('DocumentTaxe_model');
                $this->load->model('Client_m', 'client');
                $this->load->model('Tache_m', 'tache');
                $this->load->model('ClientLot_m', 'lot');

                $data_retour = array("cliLot_id" => $_POST['cliLot_id']);
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => $this->lang->line('info_enreg_erreur'),
                );
                $data = array(
                    'cliLot_id' => $_POST['cliLot_id'],
                    'annee' => $date,
                    'taxe_montant' => $_POST['taxe_montant'],
                    'taxe_tom' => $_POST['taxe_tom']
                );
                $clause = null;

                if ($_POST['taxe_id'] != 'undefined') {
                    $clause = array("taxe_id" => $_POST['taxe_id']);
                }
                $request = $this->Taxe_m->save_data($data, $clause);

                foreach ($_FILES as $key => $value) {
                    $annee = $date;
                    $cliLot_id = $_POST['cliLot_id'];
                    $targetDir = "../documents/clients/lots/taxe/$annee/$cliLot_id";
                    $temp_name = $value['tmp_name'];
                    $data_file['doc_taxe_nom'] = $value['name'];
                    $data_file['doc_taxe_creation'] = date('Y-m-d H:i:s');
                    $data_file['doc_taxe_etat'] = 1;
                    $data_file['doc_taxe_traite'] = 0;
                    $data_file['annee'] = $annee;
                    $data_file['cliLot_id'] = $cliLot_id;
                    $data_file['util_client'] = $_SESSION['session_utilisateur_client']['client_id'];
                    $data_file['doc_taxe_path'] = str_replace('\\', '/', $targetDir . DIRECTORY_SEPARATOR . $data_file['doc_taxe_nom']);
                    $this->makeDirPath($targetDir);
                    if (!file_exists($data_file['doc_taxe_path'])) {
                        move_uploaded_file($temp_name,  $data_file['doc_taxe_path']);
                    }
                    $request = $this->DocumentTaxe_model->save_data($data_file);
                }

                $request_client = $this->client->get(array(
                    'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                    'method' => 'row',
                    'join' => array(
                        'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                    ),
                    'join_orientation' => 'left'
                ));

                $params_lot = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cliLot_id' => $_POST['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),

                    'method' => 'row'
                );

                $data['lot'] = $this->lot->get($params_lot);

                $params_tache = $this->tache->get(array(
                    'clause' => array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 30,
                        'tache_nom' => $_POST['taxe_id'] != '' ? "Changement de la TOM sur le lot" . " " . $data['lot']->progm_nom : "Ajout de la TOM sur le lot" . " " . $data['lot']->progm_nom,
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1
                    ),
                    'method' => 'row',
                ));

                $data_tache = array(
                    'tache_nom' => $_POST['taxe_id'] != '' ? "Changement de la TOM sur le lot" . " " . $data['lot']->progm_nom : "Ajout de la TOM sur le lot" . " " . $data['lot']->progm_nom,
                    'tache_date_creation' => date('Y-m-d H:i:s'),
                    'tache_date_echeance' => date('Y-m-d H:i:s'),
                    'tache_prioritaire' => 2,
                    'tache_texte' => "Le propriétaire a modifié ses documents et/ou des informations sur la TOM de l'Extranet. Veuillez consulter la page du lot puis l'onglet taxe foncière pour voir les modifications",
                    'tache_valide' => 0,
                    'tache_createur' => $request_client->util_prenom,
                    'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'tache_etat' => 1,
                    'theme_id' => 30,
                    'type_tache_id' => 3,
                    'participant' => $request_client->util_id
                );

                if (empty($params_tache)) {
                    $request_tache = $this->tache->save_data($data_tache);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => (empty($taxe_id)) ? $this->lang->line('info_enreg') : $this->lang->line('info_enreg_modif')));
                }
                echo json_encode($retour);
            }
        }
    }

    function makeDirPath($path)
    {
        return file_exists($path) || mkdir($path, 0777, true);
    }

    public function removefile()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentTaxe_model');
                $data_retour = array("doc_taxe_id" => $_POST['data']['doc_taxe_id']);
                $data = array(
                    'doc_taxe_etat' => 0,
                );
                $clause = array("doc_taxe_id" => $_POST['data']['doc_taxe_id']);
                $request = $this->DocumentTaxe_model->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => $this->lang->line('delete_file')));
                }
                echo json_encode($retour);
            }
        }
    }

    public function finSaisietaxe()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1;

            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la confirmation",
            );

            $this->load->model('Taxe_m');

            $data = decrypt_service($_POST["data"]);
            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'cliLot_id' => $data['cliLot_id'],
                        'title' => "Confirmation",
                        'text' => str_replace('@annee', $date, $this->lang->line('confirm_fin_taxe')),
                        'btnConfirm' => $this->lang->line('confirmation'),
                        'btnAnnuler' => $this->lang->line('annuler')
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $data_save = array(
                    'taxe_cloture' => 1,
                    'taxe_date_cloture' => date('Y-m-d'),
                    'annee' => $date,
                    'util_prenom' => $_SESSION['session_utilisateur_client']['client_prenom'],
                    'cliLot_id' => $data['cliLot_id']
                );
                $request = $this->Taxe_m->save_data($data_save);
                if ($request) {

                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'cliLot_id' => $data['cliLot_id'],
                        ),
                        'message' => "Confirmation réussie",
                    );


                    // Tache 
                    $this->load->model('Client_m', 'client');
                    $request_client = $this->client->get(array(
                        'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                        'method' => 'row',
                        'join' => array(
                            'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        ),
                        'join_orientation' => 'left'
                    ));
                    $params_lot = array(
                        'clause' => array(
                            'cliLot_etat' => 1,
                            'cliLot_id' => $data['cliLot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        ),
                        'method' => 'row'
                    );
                    $this->load->model('ClientLot_m', 'lot');
                    $data_lot['lot'] = $this->lot->get($params_lot);

                    $data_tache = array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 34,
                        'tache_nom' => " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " qu'il n'y a pas de taxe foncière " . $date . " pour le lot " . $data_lot['lot']->progm_nom . " (" . $data_lot['lot']->progm_id . ")",
                        'tache_texte' =>  " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " qu'il n'y a pas de taxe foncière " . $date . " pour le lot " . $data_lot['lot']->progm_nom . " (" . $data_lot['lot']->progm_id . ")",
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1,
                        'tache_date_creation' => date('Y-m-d H:i:s'),
                        'tache_date_echeance' => date('Y-m-d H:i:s'),
                        'tache_prioritaire' => 2,
                        'type_tache_id' => 3,
                        'participant' =>  $request_client->util_id,
                        'tache_realise' => 0,
                    );

                    $this->load->model('Tache_m', 'tache');
                    $request_tache = $this->tache->save_data($data_tache);
                }
            }

            $retour['taxe_info'] = $data['cliLot_id'];
            echo json_encode($retour);
        }
    }
}
