<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Mandat extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Tom";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/mandat/mandat.js",
        );
        $this->_css_personnaliser = array(
            "css/mandat/mandat.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "mandat";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "mandat/contenaire-mandat" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Mandat/mandat/mandat-page", $data);
            }
        }
    }

    public function ficheMandat()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('MandatNew_m', 'mandat');
                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'c_lot_client.cliLot_id' => $data_post['cliLot_id'],
                        'mandat_etat' => 1
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_mandat_new.client_id',
                        'c_type_mandat' => 'c_type_mandat.type_mandat_id = c_mandat_new.type_mandat_id',
                        'c_etat_mandat' => 'c_etat_mandat.etat_mandat_id = c_mandat_new.etat_mandat_id',
                        'c_lot_client' => 'c_mandat_new.cliLot_id = c_lot_client.cliLot_id',
                        'c_facturation_lot' => 'c_facturation_lot.cliLot_id = c_lot_client.cliLot_id'
                    ),
                    'join_orientation' => 'left'
                );
                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['mandat'] = $this->mandat->get($params);
                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Mandat/mandat/fiche-mandat-page", $data);
            }
        }
    }

    public function getPath()
    {
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('mandat_id', 'mandat_path', 'cliLot_id'),
                    'clause' => array('mandat_id' => $data_post['id_doc']),
                );

                $request = $this->MandatNew_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getPathFicJustificatif()
    {
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('mandat_id', 'mandat_fichier_justificatif', 'cliLot_id'),
                    'clause' => array('mandat_id' => $data_post['id_doc'])
                );

                $request = $this->MandatNew_m->get($params);
                echo json_encode($request);
            }
        }
    }

    function getfichierfactureCelavi()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('Facture_m', 'facture_annuelle');

                $dossier = $this->dossier->get(['columns' => ['dossier_id', 'client_id']]);
                $client_id =    $_SESSION['session_utilisateur_client']['client_id'];

                $liste_dossier = [];

                foreach ($dossier as $key => $value) {
                    if ($value->client_id != "") {
                        $array_client = explode(',', $value->client_id);
                        if (in_array($client_id, $array_client)) {
                            $client = array_map(function ($value_client) {
                                return $this->getClient($value_client);
                            }, $array_client);

                            $dossier[$key]->client = implode(' , ', $client);
                            $liste_dossier[] = $dossier[$key];
                        }
                    }
                }

                $tous_doc = [];
                if (!empty($liste_dossier)) {
                    foreach ($liste_dossier as $value) {
                        $documents = $this->facture_annuelle->get([
                            'clause' => ['dossier_id' => $value->dossier_id],
                            'join' => ['c_document_facturation' => 'c_document_facturation.fact_id = c_facture.fact_id']
                        ]);

                        $tous_doc = array_merge($tous_doc, $documents);
                    }
                }

                $data['document'] = $tous_doc;

                $this->renderComponant("Mandat/mandat/fiche-facture-mandat", $data);
            }
        }
    }

    function apercuFacture()
    {
        $this->load->model('DocumentFacturation_m', 'doc_facturation');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('doc_id' => $data_post['id_doc']),
                    'method' => 'row'
                );

                $request = $this->doc_facturation->get($params);
                echo json_encode($request);
            }
        }
    }
}
