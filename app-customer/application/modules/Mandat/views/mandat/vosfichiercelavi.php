<div class="contenair-title pt-2 pb-2">
    <div class="px-2">
        <i class="fas fa-file-alt"></i> &nbsp;&nbsp;
        <h5><?= $this->lang->line('list_facture'); ?></h5>
    </div>
    <div class="px-2"></div>
</div>

<div class="flex-grow-1" id="vos-fichier-celavigestion">

</div>

<script>
    $(document).ready(function() {
        getfichierfactureCelavi(<?= $cliLot_id; ?>);
    });
</script>