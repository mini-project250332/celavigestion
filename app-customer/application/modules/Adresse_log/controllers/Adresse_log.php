<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Adresse_log extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Adresse_log";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/adresse_log/adresse_log.js",
        );
        $this->_css_personnaliser = array(
            'css/adresse_log/adresse_log.css'
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "adresse_log";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "adresse_log/contenaire-adresse_log" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function loadAdresse_log()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Adresse_log/adresse_log/adresse_log-page", $data);
            }
        }
    }

    public function fichelogement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot_client');
                $this->load->model('Client_m', 'client');
                $this->load->model('Rib_m');
                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cliLot_id' => $data_post['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        'c_dossier' => 'c_dossier.dossier_id = c_lot_client.dossier_id',
                        'c_gestionnaire' => 'c_gestionnaire.gestionnaire_id = c_lot_client.gestionnaire_id',
                        'c_typologielot' => 'c_typologielot.typologielot_id = c_lot_client.typologielot_id',
                        'c_typelot' => 'c_typelot.typelot_id = c_lot_client.typelot_id',
                        'c_syndicat' => 'c_syndicat.syndicat_id = c_lot_client.syndicat_id'
                    ),
                    'join_orientation' => 'left',
                    'method' => 'row'
                );

                $param_rib = array(
                    'clause' => array(),
                    'order_by_columns' => "rib_id  DESC",
                    'method' => "row"
                );

                $data['client_lot'] = $this->lot_client->get($params);
                $data['produitcli'] = $this->getlisteProduitClient();
                $data['rib'] = $this->Rib_m->get($param_rib);

                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Adresse_log/adresse_log/fiche-adresse_log-page", $data);
            }
        }
    }
}
