<?php function formatIBAN($iban)
{
    $formattedIBAN = str_replace(' ', '', $iban);
    $formattedIBAN = chunk_split($formattedIBAN, 4, ' ');
    $formattedIBAN = trim($formattedIBAN);

    return $formattedIBAN;
}
$iban = $rib->rib_iban;
$bic = $rib->rib_bic;
?>

<style>
    .text-conseil, .contenu-conseil {
        min-width: 103px !important;
    
    }
    .image-client-conseil .image-silouette {
        max-width: 150px !important;
    }
</style>

<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">

        <div class="row m-0" class="contenair-main">

            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-header rounded" style="background: #1F659E;">
                        <h5 class="fs-0 mb-1 text-white">
                            <i class="fas fa-home"></i>
                            <?= $this->lang->line('title_adresse_log') ?>
                        </h5>
                    </div>
                    <div class="card-body ">
                        <table class="table table-borderless fs--1 mb-1">
                            <tbody>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('pgrm') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= isset($client_lot->progm_nom) ? $client_lot->progm_nom : "" ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('adresses_1') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->progm_id == 309 ? $client_lot->cliLot_adresse1 : $client_lot->progm_adresse1 ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('code_postal') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->progm_id == 309 ? $client_lot->cliLot_cp : $client_lot->progm_cp ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('ville') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->progm_id == 309 ? $client_lot->cliLot_ville : $client_lot->progm_ville ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('pays') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->progm_id == 309 ? $client_lot->cliLot_pays : $client_lot->progm_pays ?>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                        <div class="py-4">
                            <div class="div-client-conseil">
                                <div class="image-client-conseil">
                                    <div class="image-silouette">
                                        <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                                    </div>
                                    <div class="text-conseil flex-grow-1">
                                        <div class="contenu-conseil">

                                            <div class="bubble bubble-bottom-left">
                                                <span>
                                                    <?= $this->lang->line('texte-conseil-clientListeLogement'); ?>
                                                </span>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col p-1 py-1">
                <div class="card rounded-0 h-100">
                    <div class="card-header rounded" style="background: #1F659E;">
                        <h5 class="fs-0 mb-1 text-white">
                            <i class="fas fa-door-open"></i>
                            <?= $this->lang->line('title_adresse_information') ?>
                        </h5>
                    </div>
                    <div class="card-body ">
                        <table class="table table-borderless fs--1 mb-1">
                            <tbody>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('type_bail') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->typologielot_libelle ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('typologie') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->typelot_libelle ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('prestation_souscrite') ?></th>
                                    <th class="pe-0 text-end">
                                        <?php
                                        $produitcli_id = explode(',', $client_lot->produitcli_id);
                                        $filtre_produit = array_filter($produitcli, function ($produit_row) use ($produitcli_id) {
                                            return in_array($produit_row->produitcli_id, $produitcli_id);
                                        });
                                        $nom = array();
                                        foreach ($filtre_produit as $key => $value) {
                                            array_push($nom, $value->produitcli_libelle);
                                        }
                                        echo implode(', ', $nom);
                                        ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('activite') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->activite ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('defiscalisation') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->defiscalisation ?>
                                    </th>
                                </tr>

                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('defiscalisation_fin') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->fin_defiscalisation == NULL || $client_lot->fin_defiscalisation == '0000-00-00' ? 'N/A' : date("d/m/Y", strtotime($client_lot->fin_defiscalisation)) ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('remboursement_rom') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->rembst_tom ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('comptabilite') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->comptabilite ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"> <?= $this->lang->line('num_lot') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->cliLot_num ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('num_parking') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->cliLot_num_parking ?>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-header rounded" style="background: #1F659E;">
                        <h5 class="fs-0 mb-1 text-white">
                            <i class="fas fa-money-check"></i>
                            <?= $this->lang->line('title_info_payement_loyer') ?>
                        </h5>
                    </div>
                    <div class="card-body ">
                        <table class="table table-borderless fs--1 mb-1">
                            <tbody>
                                <tr class="border-bottom">
                                    <th class="ps-0 pt-2"><?= $this->lang->line('encais_loyer') ?> </th>
                                    <th class="pe-0 pt-2 text-end">
                                        <?= $client_lot->cliLot_encaiss_loyer ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0 pt-2"><?= $this->lang->line('mod_payement') ?> </th>
                                    <th class="pe-0 pt-2 text-end">
                                        <?= $client_lot->cliLot_mode_paiement ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <?php $rib = explode("/", $client_lot->rib) ?>
                                    <?php $nom_fic = $client_lot->rib != NULL ? $rib[3] : $this->lang->line('no_file'); ?>
                                    <th class="ps-0 pt-2"> <?= $this->lang->line('rib') ?> </th>
                                    <th class="pe-0 pt-2 text-end">
                                        <?php if ($client_lot->rib != NULL) : ?>
                                            <span class="badge rounded-pill badge-soft-secondary" onclick="forceDownload('<?= base_url() . $client_lot->rib ?>')" data-url="<?= base_url($client_lot->rib) ?>" style="cursor: pointer;"><?= $rib[3] ?></span>
                                        <?php else :  ?>
                                            <?= $nom_fic ?>
                                        <?php endif ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0 pt-2">
                                        <?= ($client_lot->cliLot_encaiss_loyer == "Compte Celavigestion") ? $this->lang->line('iban')  : $this->lang->line('iban')  ?>
                                    </th>
                                    <th class="pe-0 pt-2 text-end">
                                        <?php if ($client_lot->cliLot_encaiss_loyer == "Compte Celavigestion") : ?>
                                            <?= formatIBAN($iban) ?>
                                        <?php else :  ?>
                                            <?= !empty($client_lot->cliLot_iban_client) ? formatIBAN($client_lot->cliLot_iban_client) : '' ?>
                                        <?php endif ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0 pt-2">
                                        <?= ($client_lot->cliLot_encaiss_loyer == "Compte client") ? $this->lang->line('bic')  : $this->lang->line('bic') ?>
                                    </th>
                                    <th class="pe-0 pt-2 text-end">
                                        <?php if ($client_lot->cliLot_encaiss_loyer == "Compte Celavigestion") : ?>
                                            <?= $bic ?>
                                        <?php else :  ?>
                                            <?= !empty($client_lot->cliLot_bic_client) ? $client_lot->cliLot_bic_client : '' ?>
                                        <?php endif ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('tva') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->tva ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('factu_loyer') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->facture_loyer ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('factu_charge') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->facture_charge ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('factu_tom') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= $client_lot->facture_tom ?>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col p-1">
                <div class="card rounded-0 h-100">
                    <div class="card-header rounded" style="background: #1F659E;">
                        <h5 class="fs-0 mb-1 text-white">
                            <i class="fas fa-user-plus"></i>
                            <?= $this->lang->line('title_adresse_manager_syndic') ?>
                        </h5>
                    </div>
                    <div class="card-body ">
                        <table class="table table-borderless fs--1 mb-1">
                            <tbody>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('gestionnaire') ?></th>
                                    <th class="pe-0 text-end">
                                        <?= isset($client_lot->gestionnaire_nom) ? $client_lot->gestionnaire_nom : "" ?>
                                    </th>
                                </tr>
                                <tr class="border-bottom">
                                    <th class="ps-0"><?= $this->lang->line('syndic') ?> </th>
                                    <th class="pe-0 text-end">
                                        <?= isset($client_lot->syndicat_nom) ? $client_lot->syndicat_nom : "" ?>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
