<div class="contenair-title bg-200 rounded">
    <div class="px-2">
        <h5 class="fs-1">
            <i class="fas fa-house-damage"></i>&nbsp;
            <?= $this->lang->line('liste_log') ?>
        </h5>
    </div>
</div>

<div class="w-100 ">
    <nav class="pt-1">
        <?php if (!empty($client_lot)) : ?>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <?php foreach ($client_lot as $key => $clilot) : ?>
                    <button class="nav-link tab-menu <?= $key == 0 ? 'active' : '' ?>" id="nav-lot_<?= $clilot->cliLot_id ?>-tab" data-bs-toggle="tab" data-bs-target="#nav-lot_<?= $clilot->cliLot_id ?>" type="button" role="tab" aria-controls="nav-lot_<?= $clilot->cliLot_id ?>" aria-selected="true">
                        <?= $clilot->progm_nom ?>
                    </button>
                <?php endforeach ?>
            </div>
        <?php else : ?>
            <div id="contenaire-menu" class="w-100 p-2">
                <div class="col-12 p-0 py-1">
                    <div class="row m-0">
                        <div class="col p-1"> <br>
                            <div class="card rounded-0 h-100">
                                <div class="card-body ">
                                    <div class="card-body text-center mt-5 bienvenu">
                                        <?= $this->lang->line('aucun_lot') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </nav>

    <?php if (!empty($client_lot)) : ?>
        <div class="tab-content" id="nav-tabContent">
            <?php foreach ($client_lot as $key => $clilot) : ?>
                <div class="tab-pane tab-content fade <?= $key == 0 ? 'active show' : '' ?>" id="nav-lot_<?= $clilot->cliLot_id ?>" role="tabpanel" aria-labelledby="nav-lot_<?= $clilot->cliLot_id ?>-tab">
                    <?php $this->load->view('adresse_log/paneltab-logement', $clilot); ?>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>