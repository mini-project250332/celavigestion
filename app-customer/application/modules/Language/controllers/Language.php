<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Language extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Language";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function switch_language($lang_code)
    {
        $this->load->model('Utilisateur_m', 'utilsateur');
        $_SESSION['session_utilisateur_client']['lang'] = $lang_code;

        $clause_lang = array('util_id' => $_SESSION['session_utilisateur_client']['util_id']);
        $data_lang['defaultlanguage'] = $lang_code;
        $this->utilsateur->save_data($data_lang, $clause_lang);

        redirect($_SERVER['HTTP_REFERER']);
    }
}
