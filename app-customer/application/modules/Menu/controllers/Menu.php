<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Menu extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Menu";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/menu/menu.js",
        );
        $this->_css_personnaliser = array(
            "css/menu/menu.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "menu";
        $this->_data['sous_module'] = "menu/contenaire-menu";
        $this->render('contenaire');
    }

    public function Message_acceuil()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('AccueilIntranet_m');
                unset($_SESSION['lang']);

                $mess_id = $_SESSION['session_utilisateur_client']['lang'] == 'french' ? 1 : 2;
                $data['message'] = $this->AccueilIntranet_m->get(array(
                    'clause' => array('mess_id' => $mess_id),
                    'method' => 'row'
                ));

                $this->renderComponant("Menu/menu/page", $data);
            }
        }
    }
}
