<div class="row m-0">
    <div class="col col-md-10 mx-auto p-4 text-center bienvenu"> 
        <h2 class="fs-1 mt-5 ">
            <?php
                $message_acueuil = str_replace(
                    ['@nom', '@prénom'],
                    [$_SESSION['session_utilisateur_client']['client_nom'], $_SESSION['session_utilisateur_client']['client_prenom']],
                    $message->message
                );
            ?>

            <?= nl2br($message_acueuil) ?>
        </h2>   
    </div>
</div>
    