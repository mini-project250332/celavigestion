<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative h-100" style="overflow-y:auto;">

        <div class="d-flex flex-column">
            <div  class="row m-0">
                <div class="col p-1">
                    <div class="card rounded-0 h-100">
                        <div class="card-header rounded" style="background: #1F659E;">
                            <h5 class="fs-0 text-white">
                                <i class="fas fa-money-check"></i>
                                <?= $this->lang->line('fiscalite'); ?>
                            </h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless mb-0">
                                <tbody>
                                    <?php foreach ($dossier as $dossiers) : ?>
                                        <tr class="border-bottom">
                                            <th class="ps-0 pt-2"> <?= $this->lang->line('sie'); ?> :</th>
                                            <th class="pe-0 pt-2 text-end">
                                                <?= $dossiers->sie_libelle ?>
                                            </th>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0 pt-2"> <?= $this->lang->line('rcs'); ?> :</th>
                                            <th class="pe-0 pt-2 text-end">
                                                <?= $dossiers->dossier_rcs ?>
                                            </th>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0 pt-2"> <?= $this->lang->line('siret'); ?> </th>
                                            <th class="pe-0 pt-2 text-end">
                                                <?= $dossiers->dossier_siret ?>
                                            </th>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0"> <?= $this->lang->line('ape'); ?> : </th>
                                            <th class="pe-0 text-end">
                                                <?php echo isset($dossiers->dossier_ape) ? $dossiers->dossier_ape : "6820A" ?>
                                            </th>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0"> <?= $this->lang->line('tva_intracommunautaire'); ?> : </th>
                                            <th class="pe-0 text-end">
                                                <?= $dossiers->dossier_tva_intracommunautaire ?>
                                            </th>
                                        </tr>
                                        <tr class="border-bottom">
                                            <th class="ps-0"> <?= $this->lang->line('regime'); ?> : </th>
                                            <th class="pe-0 text-end">
                                                <?= $dossiers->regime ?>
                                            </th>
                                        </tr>   
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col p-1 py-1">
                    <div class="card rounded-0 h-100">
                        <div class="card-header rounded" style="background: #1F659E;">
                            <h5 class="fs-0 mb-2 text-white">
                                <i class="fas fa-file-alt"></i>
                                <?= $this->lang->line('liasse_fiscale') ?>
                            </h5>
                        </div>
                        <div class="card-body">
                            <?php if (empty($doc_fiscal)) : ?>
                                <div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                                    <div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                                    <p class="mb-0 flex-1">
                                        <?= $this->lang->line('alert_doc_fiscal') ?>
                                    </p>
                                </div>
                            <?php else : ?>
                                <div class="table-responsive scrollbar">
                                    <table class="table table-bordered overflow-hidden">
                                        <colgroup>
                                            <col class="bg-soft-primary" />
                                            <col />
                                            <col />
                                        </colgroup>
                                        <thead class="text-center border-bottom">
                                            <tr class="btn-reveal-trigger">
                                                <th scope="col"> <?= $this->lang->line('annee') ?></th>
                                                <th scope="col"> <?= $this->lang->line('documents') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">
                                            <?php foreach ($doc_fiscal as $key => $value_fiscal) : ?>
                                                <tr class="btn-reveal-trigger">
                                                    <td class="fw-bold"><?= $value_fiscal->doc_2031_annee == 0 ? $this->lang->line('annee_anterieure') : $value_fiscal->doc_2031_annee ?></td>
                                                    <td style="cursor:pointer;">
                                                        <span class="badge rounded-pill badge-soft-info" onclick="forceDownload('<?= base_url() . $value_fiscal->doc_2031_path ?>')" data-url="<?= base_url($value_fiscal->doc_2031_path) ?>">
                                                            <?= $value_fiscal->doc_2031_nom ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col p-1 py-1">
                    <div class="card rounded-0 h-100">
                        <div class="card-header rounded" style="background: #1F659E;">
                            <h5 class="fs-0 mb-2 text-white">
                                <i class="fas fa-file-contract"></i>
                                <?= $this->lang->line('decla_tva') ?>
                            </h5>
                        </div>
                        <div class="card-body">
                            <?php if (empty($doc_tva)) : ?>
                                <div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
                                    <div class="bg-warning me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                                    <p class="mb-0 flex-1">
                                        <?= $this->lang->line('alert_doc_tva') ?>
                                    </p>
                                </div>
                            <?php else : ?>
                                <div class="table-responsive scrollbar">
                                    <table class="table table-bordered overflow-hidden">
                                        <colgroup>
                                            <col class="bg-soft-primary" />
                                            <col />
                                            <col />
                                        </colgroup>
                                        <thead class="text-center border-bottom">
                                            <tr class="btn-reveal-trigger">
                                                <th scope="col"> <?= $this->lang->line('annee') ?></th>
                                                <th scope="col"> <?= $this->lang->line('documents') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">
                                            <?php foreach ($doc_tva as $key => $value_doc_tva) : ?>
                                                <tr class="btn-reveal-trigger">
                                                    <td class="fw-bold"><?= $value_doc_tva->doc_tva_annee == 0 ? "Année antérieure" : $value_doc_tva->doc_tva_annee ?></td>
                                                    <td>
                                                        <div style="cursor:pointer;">
                                                            <?php $tva = explode("/", $value_doc_tva->doc_tva_path) ?>
                                                            <?php $nom_fic = $value_doc_tva->doc_tva_path = $tva[7] ?>
                                                            <span class="badge rounded-pill badge-soft-info" onclick="forceDownload('<?= base_url() . $value_doc_tva->doc_tva_path ?>')" data-url="<?= base_url($value_doc_tva->doc_tva_path) ?>">
                                                                <?= $nom_fic ?>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-0">
                <div class="col py-4">
                    <div class="d-flex">
                        <div style="max-width: 130px;">
                            <img class="w-100" src="<?=base_url('assets/images/photo4.jpg');?>">
                        </div>
                        <div>
                            <div style="background: #d4f2ff;font-size: 18px;border-radius: 40px;padding: 24px;text-align: center;">
                                <?= $this->lang->line('texte-conseil-clientDossier'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div> 