<div class="contenair-title pt-2 pb-2">
    <div class="px-2"></div>
    <div class="px-2"></div>
</div>

<div class="flex-grow-1" id="fiche-dossier_<?= $dossier_id ?>"></div>

<script>
    $(document).ready(function() {
        getFicheDossier(<?= $dossier_id; ?>);
    });
</script>