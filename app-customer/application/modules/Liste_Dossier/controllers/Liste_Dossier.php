<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Liste_Dossier extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Liste_Dossier";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/liste_dossier/liste_dossier.js",
        );

         $this->_css_personnaliser = array(
            'css/liste_dossier/liste_dossier.css'
        );
         
        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "liste_dossier";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "liste_dossier/contenaire-liste-dossier" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getListDossier()
    {
        $this->load->model('Dossier_m', 'dossier');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $params = array(
                    'clause' => array(
                        'dossier_etat' => 1,
                        'c_dossier.client_id' => intval($_SESSION['session_utilisateur_client']['client_id'])
                    ),
                    'join' => array(
                        'c_client' => 'c_client.client_id = c_dossier.client_id'
                    ),
                );

                $data['dossier'] =  $this->dossier->get($params);

                $this->renderComponant("Liste_Dossier/liste_dossier/dossier-page", $data);
            }
        }
    }

    public function ficheDossier(){
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Dossier_m', 'dossier');
                $this->load->model('DocumentTva_m');
                $this->load->model('Document2031_m');
                $data_post = $_POST["data"];

                $params_dossier = array(
                    'clause' => array(
                        'dossier_etat' => 1,
                        'c_dossier.client_id' => intval($_SESSION['session_utilisateur_client']['client_id']),
                        'dossier_id' => $data_post['dossier_id']
                    ),
                    'order_by_columns' => 'c_dossier.dossier_id ASC',
                    'method' => "result",
                    'join' => array(
                        'c_sie' => 'c_sie.sie_id = c_dossier.sie_id'
                    ),
                );

                $params_tva = array(
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_tva_etat' => 1
                    ),
                    'order_by_columns' => 'doc_tva_annee DESC'
                );

                $params_fiscal = array(
                    'clause' => array(
                        'dossier_id' => $data_post['dossier_id'],
                        'doc_2031_etat' => 1,
                    ),
                    'order_by_columns' => 'doc_2031_annee DESC'

                );

                $data['doc_fiscal'] =  $this->Document2031_m->get($params_fiscal);
                $data['doc_tva'] =  $this->DocumentTva_m->get($params_tva);
                $data['dossier'] =  $this->dossier->get($params_dossier);

                echo $data_post['dossier_id'] . "@@@@@@@";
                $this->renderComponant("Liste_Dossier/liste_dossier/fiche-dossier-page", $data);
            }
        }
    }
}
