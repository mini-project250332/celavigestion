<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Contacts extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Contacts";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/contacts/contacts.js",
        );
        $this->_css_personnaliser = array(
            "css/contacts/contacts.css",
            "css/select2.min.css",
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "contacts";
        $this->_data['sous_module'] = "contacts/contenaire-contacts";
        $this->render('contenaire');
    }

    public function loadContacts()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['client_id'] = $_SESSION['session_utilisateur_client']['client_id'];
                $this->renderComponant("Contacts/contacts/contacts-page", $data);
            }
        }
    }

    public function pageficheClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m', 'client');
                $this->load->model('Pays_m');
                $this->load->model('PaysMonde_m');
                $data_post = $_POST["data"];
                $clients = $this->client->get(array(
                    'clause' => array('client_id' => $data_post['client_id']),
                    'method' => 'row',
                    'join' => array(
                        'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_client.paysmonde_id',
                        'c_pays' => 'c_pays.pays_id = c_client.pays_id'
                    ),
                ));

                $data['client'] = $clients;
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();

                if ($data_post['page'] == "fiche") {
                    $this->renderComponant("Contacts/contacts/fiche-contacts", $data);
                } else {
                    $this->renderComponant("Contacts/contacts/form-update-contact", $data);
                }
            }
        }
    }

    public function updateContact()
    {
        if (is_ajax()) {
            if ($_POST) {
                $this->load->model('Client_m', 'client');
                $this->load->model('HistoriqueAdresse_m', 'adresse');
                $this->load->model('Tache_m', 'tache');
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $retour = retour(false, "error", 0, array("message" => "Error"));
                    $data = format_data($_POST['data']);
                    $data_retour = array("client_id" => $data['client_id']);                                     
                    $clause = array("client_id" => $data['client_id']);

                    $data_client = array(
                        'client_email' => $data['client_email'],
                        'client_email1' => $data['client_email1'],
                        'client_phonemobile' => $data['client_phonemobile'],
                        'client_phonefixe' => $data['client_phonefixe'],
                        'client_adresse1' => $data['client_adresse1'],
                        'client_adresse2' => $data['client_adresse2'],
                        'client_adresse3' => $data['client_adresse3'],
                        'client_cp' => $data['client_cp'],
                        'client_ville' => $data['client_ville'],
                        'client_pays' => $data['client_pays'],  
                        'paysmonde_id' => $data['paysmonde_id'],
                        'pays_id' => $data['pays_id']                     
                    );

                    $data_historique = array(
                        'hist_email_1_ancien' => $data['hist_email_1_ancien'],
                        'hist_email_2_ancien' => $data['hist_email_2_ancien'],
                        'hist_tel_1_ancien' => $data['hist_tel_1_ancien'],
                        'hist_tel_2_ancien' => $data['hist_tel_2_ancien'],
                        'hist_adresse_1_ancien' => $data['hist_adresse_1_ancien'],
                        'hist_adresse_2_ancien' => $data['hist_adresse_2_ancien'],
                        'hist_adresse_3_ancien' => $data['hist_adresse_3_ancien'],
                        'paysmonde_id_ancien' => $data['paysmonde_id_ancien'],
                        'pays_id_ancien' => $data['pays_id_ancien'],
                        'hist_email_1_nouv' => $data['client_email'],
                        'hist_email_2_nouv' => $data['client_email1'],
                        'hist_tel_1_nouv' => $data['client_phonemobile'],
                        'hist_tel_2_nouv' => $data['client_phonefixe'],
                        'hist_adresse_1_nouv' => $data['client_adresse1'],
                        'hist_adresse_2_nouv' => $data['client_adresse2'],
                        'hist_adresse_3_nouv' => $data['client_adresse3'], 
                        'paysmonde_id_nouv' => $data['paysmonde_id'],
                        'pays_id_nouv' => $data['pays_id'],
                        'client_id' => $data['client_id']           
                    );

                    $request_client = $this->client->get(array(
                        'clause' => array('c_client.client_id' => $data['client_id']),
                        'method' => 'row',
                        'join' => array(
                            'c_dossier' => 'c_client.client_id = c_dossier.client_id', 
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',                            
                        ),
                        'join_orientation' => 'left'
                    ));

                    $data_tache = array(
                        'tache_nom' => "Changement d'adresse",
                        'tache_date_creation' => date('Y-m-d H:i:s'),
                        'tache_date_echeance' => date('Y-m-d H:i:s'),
                        'tache_prioritaire' => 2,
                        'tache_texte' => "Le propriétaire a modifié son adresse et/ou des informations de contact sur l'Extranet. Veuillez consulter la page Propriétaire et l'historique de son adresse/contact pour déterminer si vous devez prévenir les gestionnaires ou SIE de ces changements.",
                        'tache_valide' => 0,
                        'tache_createur' => $request_client->util_prenom,
                        'client_id' => $data['client_id'],
                        'tache_etat' => 1,
                        'theme_id' => 4,
                        'type_tache_id' => 3,
                        'participant' => $request_client->util_id
                    );

                    $request = $this->client->save_data($data_client, $clause);
                    $request_histo = $this->adresse->save_data($data_historique);

                    $params_tache = $this->tache->get(array(
                        'clause' => array(
                            'client_id' => $data['client_id'],
                            'theme_id' => 4,
                            'tache_createur' => $request_client->util_prenom,
                            'tache_valide' => 0,
                            'tache_etat' => 1
                        ),
                        'method' => 'row',
                        
                    ));

                    if (empty($params_tache)) {
                        $request_tache = $this->tache->save_data($data_tache);
                    }                    
                    
                    if (!empty($request)) {
                        $retour = retour(
                            true,
                            "success",
                            $data_retour,
                            array(
                                "message" => $this->lang->line('info_enreg_modif')
                            )
                        );
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
