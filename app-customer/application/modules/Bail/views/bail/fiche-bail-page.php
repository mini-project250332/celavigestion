<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">
            <div class="w-100">

                    <div class="liste-document-customer px-1 pt-2">

                        <?php if (!empty($doc_bail)) : ?>

                            <?php foreach ($doc_bail as $doc_bails) : ?>

                                <div class="d-flex mb-2 align-items-center">
                                    <div class="flex-shrink-1 file-thumbnail">
                                        <img class="img-fluid" src="<?= base_url('assets/images/docs.png'); ?>" alt="">
                                    </div>

                                    <div class="ms-2 flex-grow-1 d-flex align-items-center ">
                                        
                                        <div class="flex-grow-1 d-flex flex-column">
                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 300px;">
                                                <span class="mb-0 fw-semi-bold fs-0 text-dark">
                                                    <?= $doc_bails->doc_bail_nom; ?>
                                                </span>
                                            </div>
                                            <div class="fs--1 mt-1">
                                                <p class="mb-1">
                                                    <span class="fw-semi-bold mb-1"><?= $this->lang->line('depose_par') ?> :</span>
                                                    <span class="fw-semi-bold px-2">
                                                        <?= $doc_bails->util_prenom; ?>
                                                    </span>
                                                    <span class="fw-semi-bold mb-1"><?= $this->lang->line('le') ?> </span>
                                                    <span class="fw-medium text-600 ms-2"><?= date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $doc_bails->doc_bail_creation))) ?></span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="px-2 d-flex flex-shrink-1 ">
                                            <btn class="btn btn-primary btn-sm me-1 text-600 text-white" onclick="forceDownload('<?= base_url() . $doc_bails->doc_bail_path ?>','<?= $doc_bails->doc_bail_nom ?>')">
                                                <i class=" fas fa-file-download"></i>
                                                <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('telecharger') ?></span>
                                            </btn>
                                            <btn class="btn btn-info btn-sm text-600 text-white" onclick="apercuDocumentBail(<?= $doc_bails->doc_bail_id ?>)">
                                                <i class="fas fa-eye"></i>
                                                <span class="libelle-bnt-icon">&nbsp;<?= $this->lang->line('voir_document') ?></span>
                                            </btn>
                                        </div>
                                    </div>
                                </div>
                                <hr class="bg-200 my-1">
                            <?php endforeach; ?>

                        <?php else : ?>

                            <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                                <div class="bg-info me-3 icon-item">
                                    <span class="fas fa-info-circle text-white fs-3"></span>
                                </div>
                                <p class="mb-0 flex-1"><?= $this->lang->line('aucun_doc') ?></p>
                            </div>

                        <?php endif; ?>
                    </div>
                </div>
                <hr>
                <?php if (!empty($bail)) :  ?>
                    
                    <div class="w-100 py-1">
                        <div class="row m-0 border">
                            <div class="col px-0">
                                <div class="card" style="box-shadow:none!important;">
                                    <div class="card-header rounded p-2" style="background: #1F659E;">
                                        <h5 class="fs-0 text-white">
                                            <?php if (date('Y-m-d') > $bail->bail_fin) : ?>
                                                <?= $bail->bail_avenant == 1 ? $this->lang->line('bail_en_avenat_cours_en_tacite') : $this->lang->line('bail_en_cours_en_tacite') ?>&nbsp;
                                                <?= date('d/m/Y', strtotime($bail->bail_fin)) ?> &nbsp;
                                                (<?= $bail->bail_valide == 1 ? $this->lang->line('valide') : $this->lang->line('non_valide') ?>)
                                            <?php else : ?>
                                                <?= $bail->bail_avenant == 1 ? $this->lang->line('bail_en_avenat_cours') : $this->lang->line('bail_en_cours') ?>&nbsp;
                                                <?= date('d/m/Y', strtotime($bail->bail_debut)) ?> &nbsp;
                                                (<?= $bail->bail_valide == 1 ? $this->lang->line('valide') : $this->lang->line('non_valide') ?>)
                                            <?php endif ?>
                                        </h5>
                                    </div>

                                    <div class="card-body bg-light">
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('preneur') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php foreach ($gestionnaire as $gestionnaires) :
                                                            if ($gestionnaires->gestionnaire_id == $bail->preneur_id) : ?>
                                                                <p class="mb-1"><?= $gestionnaires->gestionnaire_nom ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('exploitant') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php foreach ($gestionnaire as $gestionnaires) :
                                                            if ($gestionnaires->gestionnaire_id == $bail->gestionnaire_id) : ?>
                                                                <p class="mb-1"><?= $gestionnaires->gestionnaire_nom ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('nature_prise_deffet') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php
                                                        $nature_description = array(
                                                            '1' => $this->lang->line('a_la_livraison'),
                                                            '2' => $this->lang->line('date_sign_bail'),
                                                            '3' => $this->lang->line('date_sign_acte'),
                                                            '4' => $this->lang->line('suite_prec_gestionnaire')
                                                        );
                                                        foreach ($nature_prise_effet as $nature) :
                                                            if ($nature->natef_id == $bail->natef_id) : ?>
                                                                <p class="mb-1"><?= $nature_description[$nature->natef_id] ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('duree') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php
                                                        $duree = '';
                                                        if ($bail->bail_annee > 0) {
                                                            $stringannnee = $bail->bail_annee == 1 ? $this->lang->line('an') : $this->lang->line('ans');
                                                            $duree .= $bail->bail_annee . ' ' . $stringannnee;
                                                        }
                                                        if ($bail->bail_mois > 0) {
                                                            $stringmois = $duree != '' ? ' ' . $this->lang->line('et') : '';
                                                            $duree .= $stringmois . ' ' . $bail->bail_mois . ' ' . $this->lang->line('mois');
                                                        }
                                                        ?>
                                                        <p class="mb-1"><?= $duree ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('tacite_prolongation') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php $tacite = date('Y-m-d') > $bail->bail_fin ? $this->lang->line('oui') : $this->lang->line('non') ?>
                                                        <p class="mb-1"><?= $tacite ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('type_bail') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php $type_description = array(
                                                            '1' => $this->lang->line('commercial_meuble'),
                                                            '2' => $this->lang->line('commercial_non_meuble'),
                                                            '3' => $this->lang->line('mandat_de_gestion'),
                                                            '4' => $this->lang->line('saisonnier'),
                                                            '5' => $this->lang->line('autre')
                                                        );
                                                        foreach ($type_bail as $type) :
                                                            if ($type->tpba_id == $bail->tpba_id) : ?>
                                                                <p class="mb-1"><?= $type_description[$type->tpba_id] ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_premier_facturation_par_celavi') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= date('d/m/Y', strtotime($bail->bail_date_echeance)) ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_prise_effet') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= date('d/m/Y', strtotime($bail->bail_debut)) ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_fin') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= date('d/m/Y', strtotime($bail->bail_fin)) ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('resiliation_trienale') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_resiliation_triennale == 1 ? $this->lang->line('oui') : $this->lang->line('non')  ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 py-1">
                        <div class="bg rounded" style="background: #1F659E;">
                            <h5 class="fs-0 text-white p-1"> &nbsp;
                                <?= $this->lang->line('franchise_loyer') ?>
                            </h5>
                        </div>
                        <?php if (isset($franchises) && !empty($franchises)) : ?>
                            <table class="table table-hover">
                                <thead class="fs--1 text-white" style="background:#1F659E;">
                                    <tr class="text-center">
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('debut') ?></th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('fin') ?></th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;" width="30%"><?= $this->lang->line('libelle_facture') ?></th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('type_deduction') ?></th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('ttc_deduit') ?></th>
                                        <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('pourcentage') ?></th>
                                    </tr>
                                </thead>
                                <tbody class="fs--1 bg-200">
                                    <?php foreach ($franchises as $key => $franchise) : ?>
                                        <tr id="franchise_row-<?php echo $franchise->franc_id ?>">
                                            <td class="text-center">
                                                <?php echo date_format_fr($franchise->franc_debut); ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo date_format_fr($franchise->franc_fin); ?>
                                            </td>
                                            <td>
                                                <div>
                                                    <?php echo $franchise->franc_explication ?>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?= $franchise->type_deduction == 1 ? 'Totale' : 'Partielle' ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $franchise->montant_ttc_deduit == 0 || $franchise->montant_ttc_deduit == '' ? '-' : format_number_($franchise->montant_ttc_deduit) ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $franchise->franchise_pourcentage == 0 || $franchise->franchise_pourcentage == '' ? '-' : format_number_($franchise->franchise_pourcentage) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="alert alert-secondary border-2 d-flex align-items-center" role="alert">
                                <div class="bg-info me-3 icon-item">
                                    <span class="fas fa-info-circle text-white fs-3"></span>
                                </div>
                                <p class="mb-0 flex-1"><?= $this->lang->line('aucun_franchise') ?></p>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="w-100 py-1">
                        <div class="row m-0 border">
                            <div class="col px-0">
                                <div class="card" style="box-shadow:none!important;">
                                    <div class="card-header rounded p-2" style="background: #1F659E;">
                                        <h5 class="fs-0 text-white">
                                            <?= $this->lang->line('information_loyer') ?>
                                        </h5>
                                    </div>

                                    <div class="card-body bg-light">
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('periodicite') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php $periodicite_lang = array(
                                                            '1' => $this->lang->line('mensuelle'),
                                                            '2' => $this->lang->line('trim_civile'),
                                                            '3' => $this->lang->line('semestrielle'),
                                                            '4' => $this->lang->line('annulle'),
                                                            '5' => $this->lang->line('trim_decale')
                                                        ); ?>
                                                        <?php foreach ($periodicite_loyer as $periode) :
                                                            if ($periode->pdl_id == $bail->pdl_id) : ?>
                                                                <p class="mb-1"><?= $periodicite_lang[$periode->pdl_id] ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('periode_indexation') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php $periode_lang = array(
                                                            '1' => $this->lang->line('un_an'),
                                                            '2' => $this->lang->line('trois_an'),
                                                            '3' => $this->lang->line('non_applicable'),
                                                        ); ?>
                                                        <?php foreach ($periode_lang as $key => $periode_index) :
                                                            if ($key == $bail->prdind_id) : ?>
                                                                <p class="mb-1"><?= $periode_index ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_premier_indexation') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= date('d/m/Y', strtotime($bail->bail_date_premiere_indexation)) ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('indice_utilise') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php foreach ($indice_loyer as $indice) :
                                                            if ($indice->indloyer_id == $bail->indloyer_id) : ?>
                                                                <p class="mb-1 pt-1 fs--1"> <?= $indice->indloyer_description ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('nature_loyer') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php $varibale_loyer = array(
                                                            '0' => $this->lang->line('fixe'),
                                                            '1' => $this->lang->line('fixe_variable'),
                                                            '2' => $this->lang->line('variable'),
                                                        ); ?>
                                                        <?php foreach ($varibale_loyer as $key => $variable_l) :
                                                            if ($key == $bail->bail_loyer_variable) : ?>
                                                                <p class="mb-1"><?= $variable_l ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('forfait_charge_deductible') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_forfait_charge_indexer == 1 ? $this->lang->line('oui') : $this->lang->line('oui') ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_paiement') ?></p>
                                                    </div>
                                                    <div>
                                                        <?php foreach ($type_echeance as $key => $type_ech) :
                                                            if ($type_ech->tpech_id == $bail->tpech_id) : ?>
                                                                <p class="mb-1"><?= $type_ech->tpech_valeur . ' ' . $this->lang->line('du_mois'); ?></p>
                                                        <?php endif;
                                                        endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('moment_paiement') ?></p>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <?php $moment_fact = array(
                                                                '1' => $this->lang->line('echu'),
                                                                '2' => $this->lang->line('echoir'),
                                                            ); ?>
                                                            <?php foreach ($moment_fact as $key => $moment_facts) :
                                                                if ($key == $bail->momfac_id) : ?>
                                                                    <p class="mb-1"><?= $moment_fact[$bail->momfac_id] ?></p>
                                                            <?php endif;
                                                            endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('date_premier_indexation_par_celavigestion') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= date('d/m/Y', strtotime($bail->bail_date_prochaine_indexation)) ?></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('indice_reference') ?></p>
                                                    </div>
                                                    <div>
                                                        <div>
                                                            <?php foreach ($indice_valeur_loyer as $key => $indice) :
                                                                if ($indice->indval_id == $bail->indval_id) : ?>
                                                                    <p class="mb-1"><?= $indice->indval_trimestre . ' ' . $indice->indval_annee . ' (' . $indice->indval_valeur . ')' ?></p>
                                                            <?php endif;
                                                            endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if($bail->bail_valide == 1): ?>

                        <div class="w-100 py-1">
                            <div class="row m-0 border">
                                <div class="col px-0">
                                    <div class="card" style="box-shadow:none!important;">
                                        <div class="card-header rounded p-2" style="background: #1F659E;">
                                            <h5 class="fs-0 text-white">
                                                <?= $this->lang->line('indexation_loyer') ?>
                                            </h5>
                                        </div>

                                        <div class="card-body bg-light">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <p class="fw-semi-bold mb-1"><?= $this->lang->line('variation_plafonne') ?></p>
                                                        </div>
                                                        <div>
                                                            <p class="mb-1"><?= $bail->bail_var_plafonnee ?> %</p>
                                                        </div>
                                                    </div>

                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <p class="fw-semi-bold mb-1"><?= $this->lang->line('augmentation_min') ?></p>
                                                        </div>
                                                        <div>
                                                            <p class="mb-1"><?= $bail->bail_var_min == 0 ? '' : $bail->bail_var_min . ' %' ?> </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <p class="fw-semi-bold mb-1"><?= $this->lang->line('augmentation_capee') ?></p>
                                                        </div>
                                                        <div>
                                                            <p class="mb-1"><?= $bail->bail_var_capee ?> %</p>
                                                        </div>
                                                    </div>

                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <p class="fw-semi-bold mb-1"><?= $this->lang->line('indice_plafonnement') ?></p>
                                                        </div>
                                                        <div>
                                                            <?php if ($bail->indice_valeur_plafonnement == 0) : ?>
                                                                <p class="mb-1"><?= $this->lang->line('aucun') ?></p>
                                                                <?php else :
                                                                foreach ($indice_loyer as $indice) :
                                                                    if ($indice->indloyer_id == $bail->indice_valeur_plafonnement) : ?>
                                                                        <p class="mb-1 pt-1 fs--1"> <?= $indice->indloyer_description ?></p>
                                                            <?php endif;
                                                                endforeach;
                                                            endif ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="w-100 py-1">
                            <?php if (isset($franchises) && !empty($indexations)) : ?>
                                <table class="table table-hover">
                                    <thead class="fs--1 text-white" style="background:#1F659E;">
                                        <tr class="text-center">
                                            <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;"><?= $this->lang->line('debut') ?></th>
                                            <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;"><?= $this->lang->line('fin') ?></th>
                                            <th class="p-2 pb-3" rowspan="2" style="border-right: 1px solid #ffff;" width="30%"><?= $this->lang->line('indice') ?></th>
                                            <th class="p-2 pb-3" colspan="4" style="border-right: 1px solid #ffff;"><?= $this->lang->line('montant') ?></th>
                                        </tr>
                                        <tr class="text-center">
                                            <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('type') ?></th>
                                            <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('HT') ?></th>
                                            <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('TVA') ?></th>
                                            <th class="p-2 pb-3" style="border-right: 1px solid #ffff;"><?= $this->lang->line('TTC') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody class="fs--1 bg-200">
                                        <?php if (isset($indexations) && count($indexations) > 1) { ?>
                                            <?php
                                            $index = 0;
                                            foreach ($indexations as $key => $indexation) : ?>
                                                <?php if ($indexation->indlo_ref_loyer == 0 && ($indexation->indlo_indince_non_publie == 0)) { ?>
                                                    <tr class="text-nowrap">
                                                        <td>
                                                            <div class="text-black">
                                                                <?php
                                                                if (isset($indexation->indlo_debut))
                                                                    echo date_format_fr($indexation->indlo_debut);
                                                                else
                                                                    echo "-"; ?>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="text-black">
                                                                <?php
                                                                if (isset($indexation->indlo_fin))
                                                                    echo date_format_fr($indexation->indlo_fin);
                                                                else
                                                                    echo "-"; ?>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <?php
                                                            $indx_libelle = $indexation->indval_libelle;
                                                            $indx_valeur = $indexation->indval_valeur;

                                                            if (intval($index) > 0) {
                                                                foreach ($indices as $indice) {
                                                                    if ($indice->indval_id === $indexation->indlo_indice_reference) {
                                                                        $indx_libelle = $indice->indval_libelle;
                                                                        $indx_valeur = $indice->indval_valeur;
                                                                    }
                                                                }
                                                            }
                                                            ?>

                                                            <div class="text-black text-center">
                                                                <?php echo $indx_libelle; ?>
                                                                <div>
                                                                    <b>
                                                                        <?php echo $indx_valeur; ?> <?= $indexation->mode_calcul == 2 ? '%' : '' ?>
                                                                    </b>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="text-start"><?= $this->lang->line('appartement') ?></div>
                                                            <div class="text-start"><?= $this->lang->line('parking') ?></div>
                                                            <div class="text-start"><?= $this->lang->line('forfait_charge') ?></div>
                                                            <div class="text-start"><b><?= $this->lang->line('total') ?></b></div>
                                                        </td>

                                                        <td>
                                                            <div class="text-end">
                                                                <?php echo format_number_($indexation->indlo_appartement_ht); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?php echo format_number_($indexation->indlo_parking_ht); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?= $indexation->indlo_charge_ht != 0 ? '-' : '' ?> <?php echo format_number_($indexation->indlo_charge_ht); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?php
                                                                $totalA = 0;
                                                                $totalA = $indexation->indlo_appartement_ht + $indexation->indlo_parking_ht - $indexation->indlo_charge_ht + $indexation->indlo_autre_prod_ht;
                                                                ?>
                                                                <b>
                                                                    <?php echo format_number_($totalA); ?>
                                                                </b>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="text-end">
                                                                (
                                                                <?php echo $indexation->indlo_appartement_tva; ?>%) &nbsp;
                                                                <?php
                                                                $newA = 0;
                                                                $newA = $indexation->indlo_appartement_ht * $indexation->indlo_appartement_tva / 100;
                                                                echo format_number_($newA);
                                                                ?>
                                                            </div>
                                                            <div class="text-end">
                                                                (
                                                                <?php echo $indexation->indlo_parking_tva; ?>%) &nbsp;
                                                                <?php
                                                                $newB = 0;
                                                                $newB = $indexation->indlo_parking_ht * $indexation->indlo_parking_tva / 100;
                                                                echo format_number_($newB);
                                                                ?>
                                                            </div>
                                                            <div class="text-end">
                                                                (
                                                                <?php echo $indexation->indlo_charge_tva; ?>%) &nbsp;
                                                                <?php
                                                                $newC = 0;
                                                                $newC = $indexation->indlo_charge_ht * $indexation->indlo_charge_tva / 100;
                                                                echo $newC != 0 ? '-' : '';
                                                                echo  format_number_($newC);
                                                                ?>
                                                            </div>

                                                            <div class="text-end">
                                                                <?php
                                                                $totalB = 0;
                                                                $totalB = $newA + $newB - $newC;
                                                                ?>
                                                                <b>
                                                                    <?php echo format_number_($totalB); ?>
                                                                </b>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div class="text-end">
                                                                <?php echo format_number_($indexation->indlo_appartement_ttc); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?php echo format_number_($indexation->indlo_parking_ttc); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?= $indexation->indlo_charge_ttc != 0 ? '-' : '' ?> <?php echo format_number_($indexation->indlo_charge_ttc); ?>
                                                            </div>
                                                            <div class="text-end">
                                                                <?php
                                                                $totalC = 0;
                                                                $totalC = $indexation->indlo_appartement_ttc + $indexation->indlo_parking_ttc - $indexation->indlo_charge_ttc + $indexation->indlo_autre_prod_ttc;
                                                                ?>
                                                                <b>
                                                                    <?php echo format_number_($totalC); ?>
                                                                </b>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php
                                                $index = $index + 1;
                                            endforeach ?>
                                            <?php
                                            $countIndexation = count($indexations);
                                            $dernierIndexation = $indexations[$countIndexation - 1];
                                            $increment = $dernierIndexation->prdind_id == 1 ? 1 : 3;

                                            if ($indexation->indlo_indince_non_publie == 0) {
                                                $nextDateDebut = date('Y-m-d', strtotime('+1 day', strtotime($dernierIndexation->indlo_fin)));
                                                $nextDateFin = date('Y-m-d', strtotime('+' . $increment . ' years -1 day', strtotime($dernierIndexation->indlo_fin)));
                                            } else {
                                                $nextDateDebut = date('Y-m-d', strtotime('+1 day', strtotime($dernierIndexation->indlo_fin)));
                                                $nextDateFin = date('Y-m-d', strtotime('+' . $increment . ' years -1 day', strtotime($nextDateDebut)));
                                            }
                                            ?>

                                            <?php if ($indexation->indlo_indince_non_publie == 0 || $indexation->indlo_indince_non_publie == 1 || $indexation->indlo_indince_non_publie == 2) :
                                                for ($i = 0; $i < 5; $i++) :
                                            ?>
                                                    <tr class="text-center">
                                                        <td>
                                                            <div class="text-black">
                                                                <?= date_format_fr($nextDateDebut) ?>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="text-black">
                                                                <?= date_format_fr($nextDateFin) ?>
                                                            </div>
                                                        </td>
                                                        <td><b><?= $this->lang->line('en_attente_de_pub') ?></b></td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                    </tr>
                                                <?php
                                                    $nextDateDebut = date('Y-m-d', strtotime('+' . $increment . ' years', strtotime($nextDateDebut)));
                                                    $nextDateFin = date('Y-m-d', strtotime('+' . ($increment) . 'year -1 day', strtotime($nextDateDebut)));
                                                    if ($i == 3) {
                                                        $nextDateDebutObj = new DateTime($nextDateDebut);
                                                        $nextDateFinObj = clone $nextDateDebutObj;
                                                        $nextDateFinObj->add(new DateInterval('P' . $increment . 'Y'));
                                                        $nextDateFinObj->sub(new DateInterval('P1D'));
                                                        $nextDateFin = $nextDateFinObj->format('Y-m-d');
                                                    }
                                                endfor;
                                                ?>
                                            <?php endif ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <div class="alert alert-secondary border-2 d-flex align-items-center" role="alert">
                                    <div class="bg-info me-3 icon-item">
                                        <span class="fas fa-info-circle text-white fs-3"></span>
                                    </div>
                                    <p class="mb-0 flex-1"><?= $this->lang->line('aucun_indexaton') ?></p>
                                </div>
                            <?php endif ?>
                        </div>

                    <?php endif;?>

                    <div class="w-100 py-1">
                        <div class="row m-0 border">
                            <div class="col px-0">
                                <div class="card" style="box-shadow:none!important;">
                                    <div class="card-header rounded p-2" style="background: #1F659E;">
                                        <h5 class="fs-0 text-white">
                                            <?= $this->lang->line('action_disponible') ?>
                                        </h5>
                                    </div>

                                    <div class="card-body bg-light">
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('remboursement_tom') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_remboursement_tom == 1 ? $this->lang->line('oui') : $this->lang->line('non') ?></p>
                                                    </div>
                                                </div>

                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('facturationparcelavigestion') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_facturation_loyer_celaviegestion == 1 ? $this->lang->line('oui') : $this->lang->line('non') ?> </p>
                                                    </div>
                                                </div>

                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('charge_recuperable') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_charge_recuperable == 1 ? $this->lang->line('oui') : $this->lang->line('non') ?> </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 py-1">
                        <div class="row m-0 border">
                            <div class="col px-0">
                                <div class="card" style="box-shadow:none!important;">
                                    <div class="card-header rounded p-2" style="background: #1F659E;">
                                        <h5 class="fs-0 text-white">
                                            <?= $this->lang->line('autres_information') ?>
                                        </h5>
                                    </div>

                                    <div class="card-body bg-light">
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('art_605') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_art_605 == 1 ? $this->lang->line('bailleur') : $this->lang->line('preneur') ?></p>
                                                    </div>
                                                </div>

                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="fw-semi-bold mb-1"><?= $this->lang->line('art_606') ?></p>
                                                    </div>
                                                    <div>
                                                        <p class="mb-1"><?= $bail->bail_art_606 == 1 ? $this->lang->line('bailleur') : $this->lang->line('preneur') ?> </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                <?php else :  ?>
                    <div class="alert alert-secondary border-2 d-flex align-items-center" role="alert">
                        <div class="bg-warning me-3 icon-item">
                            <span class="fas fa-info-circle text-white fs-3"></span>
                        </div>
                        <p class="mb-0 flex-1"><?= $this->lang->line('aucun_bail') ?></p>
                    </div>
                <?php endif  ?>
            </div>
        </div>

        <div class="splitter"></div>

        <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
            <div id="apercudocBail_<?= $cliLot_id ?>">
                <div id="nonApercu" style="display:none;">
                    <div class="text-center text-danger pt-10">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"><?= $this->lang->line('apercu_doc') ?></i>
                    </div>
                </div>
                <div id="apercuBail_<?= $cliLot_id ?>">
                    <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                        <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                        </button>
                        <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                            <i class="fa fa-search-minus" aria-hidden="true"></i>
                        </button>
                        <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1">
                            <i class="fa fa-retweet"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    // $(".panel-left").resizable({
    //     handleSelector: ".splitter",
    //     resizeHeight: false,
    // });
</script>