<?php
	$phoneNumber = $message_bulle->client_phonemobile;
	$phoneMobile;
	$phoneFixe = null;
	if (isset($message_bulle->paysmonde_id) && $message_bulle->paysmonde_id == 66) {
		$array = str_split($phoneNumber, 2);
		$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
		$phoneMobile = implode(".", $array);
	} else {
		$phoneMobile = isset($message_bulle->client_phonemobile) ? $message_bulle->client_phonemobile : 0;
	}

	if (isset($message_bulle->client_phonefixe) && $message_bulle->client_phonefixe != "") {
		$phone = isset($message_bulle->client_phonefixe) ? $message_bulle->client_phonefixe : 0;
		if (isset($message_bulle->pays_id) && $message_bulle->pays_id == 66) {
			$array = str_split($phone, 2);
			$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
			$phoneFixe = implode(".", $array);
		} else {
			$phoneFixe = isset($message_bulle->client_phonefixe) ? $message_bulle->client_phonefixe : 0;
		}
	}

	function formatTelephone($numero)
	{
		$numero = preg_replace("/[^0-9]/", "", $numero);
		$longueur = strlen($numero);

		if ($longueur >= 2) {
			$numero_formate = "0" . substr($numero, 1, 1) . " " . substr($numero, 2, 2) . " " . substr($numero, 4, 2) . " " . substr($numero, 6, 2) . " " . substr($numero, 8, 2) . " " . substr($numero, 10);
			return $numero_formate;
		}
	}
?>


<div style="width:100%;">
	<div class="alert alert-warning border-2 d-flex align-items-center mt-2" role="alert">
	    <div class="bg-warning me-3 icon-item">
	    	<span class="fas fa-exclamation-circle text-white fs-3"></span>
		</div>
	    <p class="mb-0 flex-1">
	        <?= $this->lang->line('message_requireCIN') ?>
	    </p>
    </div>

    <div class="row m-0" style="width:100%;">
	    <div class="col py-4">
	        <div class="d-flex">
	            <div style="max-width: 130px;">
	                <img class="w-100" src="<?=base_url('assets/images/photo4.jpg');?>">
	            </div>
	            <div>
	                <div style="background: #d4f2ff;font-size: 18px;border-radius: 40px;padding: 24px;text-align: center;">
	                    <?= $this->lang->line('texte-conseil-client') ?> <br>
							
						<?php if ($_SESSION['session_utilisateur_client']['lang'] == "french") : ?>
							<?= $this->lang->line('telephone') ?> 
							<?= !empty($message_bulle->util_phonemobile) ? formatTelephone($message_bulle->util_phonemobile) : formatTelephone($message_bulle->util_phonefixe);?>
							<br>
						<?php else : ?>
							<?php $number = !empty($message_bulle->util_phonemobile) ? formatTelephone($message_bulle->util_phonemobile) : formatTelephone($message_bulle->util_phonefixe);
							if (strlen($number) > 0) {
								$premier_caractere = substr($number, 0, 1);
								$reste_texte = substr($number, 1);
								$number = "($premier_caractere)$reste_texte";
							}
							?>
							<?= $this->lang->line('telephone') ?> 00 33 <?= $number ?><br>
						<?php endif; ?>
						<?= $this->lang->line('email') ?> <?= isset($message_bulle->util_mail) ? $message_bulle->util_mail : ''; ?>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

</div>
