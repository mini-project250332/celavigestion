<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Bail extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Bail";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/bail/bail.js",
        );
        $this->_css_personnaliser = array(
            "css/bail/bail.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "bail";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "bail/contenaire-bail" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Bail/bail/bail-page", $data);
            }
        }
    }

    public function ficheBail()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentBail_m', 'bail');
                $this->load->model('Bail_m');
                $this->load->model('NaturePriseEffet_m');
                $this->load->model('TypeBail_m');
                $this->load->model('FranchiseLoyer_m');
                $this->load->model('PeriodiciteLoyer_m');
                $this->load->model('IndiceLoyer_m');
                $this->load->model('TypeEcheance_m');
                $this->load->model('IndiceValeurLoyer_m');
                $this->load->model('IndexationLoyer_m');

                $data_post = $_POST["data"];

                $params = array(
                    'clause' => array(
                        'cliLot_id' => $data_post['cliLot_id'],
                        'doc_bail_etat' => 1
                    ),
                    'join' => array('c_utilisateur' => 'c_utilisateur.util_id = c_document_bail.util_id')
                );

                $paramsBail = array(
                    'clause' => array('c_bail.cliLot_id' => $data_post['cliLot_id'], 'c_bail.bail_cloture' => 0),
                    'order_by_columns' => "c_bail.bail_id DESC",
                    'method' => 'row'
                );

                $clause = array(
                    'order_by_columns' => 'indval_annee ASC, indval_trimestre ASC'
                );
                $data['indice_valeur_loyer'] = $this->IndiceValeurLoyer_m->get($clause);

                $data['indices'] = $this->IndiceValeurLoyer_m->get(array());

                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['doc_bail'] = $this->bail->get($params);
                $data['bail'] = $this->Bail_m->get($paramsBail);
                $data['gestionnaire'] = $this->getListGestionnaire();
                $data['nature_prise_effet'] = $this->NaturePriseEffet_m->get(array());
                $data['type_bail'] = $this->TypeBail_m->get(array());
                $data['periodicite_loyer'] = $this->PeriodiciteLoyer_m->get(array());
                $data['indice_loyer'] = $this->IndiceLoyer_m->get(array());
                $data['type_echeance'] = $this->TypeEcheance_m->get(array());

                if (!empty($data['bail'])) {
                    $paramsfrachise = array(
                        'clause' => array('c_franchise_loyer.bail_id' => $data['bail']->bail_id, 'c_franchise_loyer.etat_franchise' => 1),
                        'join' => array(
                            'c_bail' => 'c_bail.bail_id = c_franchise_loyer.bail_id'
                        ),
                        'order_by_columns' => "c_franchise_loyer.franc_debut ASC",
                    );

                    $data['franchises'] = $this->FranchiseLoyer_m->get($paramsfrachise);

                    $params_indexation = array(
                        'clause' => array(
                            'c_indexation_loyer.bail_id' => $data['bail']->bail_id,
                            'c_indexation_loyer.etat_indexation' => 1
                        ),
                        'join' => array(
                            'c_bail' => 'c_bail.bail_id = c_indexation_loyer.bail_id',
                            'c_indice_valeur_loyer' => 'c_indice_valeur_loyer.indval_id  = c_indexation_loyer.indlo_indice_base',
                            'c_indice_loyer' => 'c_indice_valeur_loyer.indloyer_id  = c_indice_loyer.indloyer_id',
                        ),
                    );

                    $data['indexations'] = $this->IndexationLoyer_m->get($params_indexation);
                }

                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Bail/bail/fiche-bail-page", $data);
            }
        }
    }

    public function getpathDocbail()
    {
        $this->load->model('DocumentBail_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];

                $params = array(
                    'columns' => array('doc_bail_id', 'doc_bail_path', 'cliLot_id'),
                    'clause' => array('doc_bail_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentBail_m->get($params);
                echo json_encode($request);
            }
        }
    }
}
