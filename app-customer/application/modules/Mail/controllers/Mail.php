<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mail extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Mail";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array();
        $this->_css_personnaliser = array();
    }

    public function envoi_reinit_Mail()
    {
        $this->load->model('LienIntranet_m', 'lien');
        $mail = new PHPMailer();

        $retour = array();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data_post = $_POST["data"];
            $email = $data_post['destinataire'];
            $utilNom = $data_post['utilNom'];
            $emailCrypted = $data_post['emailCrypted'];
            $data_retour = ['email' => $email, 'lang_code' => $this->session->userdata('lang')];

            //get  key
            $crypte = json_decode($emailCrypted);
            $key = $crypte->ct;

            //datetime + 24H
            $currentDateTime = new DateTime();
            $newDateTime = $currentDateTime->modify('+24 hours');

            if (!isset($email)) {
                $retour = array('status' => 500, 'message' => 'Mailer Error');
                echo json_encode($retour);
            } else {
                try {
                    $mail->isSMTP();
                    $mail->Host = 'ex5.mail.ovh.net';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'noreply@celavigestion.fr';
                    $mail->Password = 'FondeLine39';
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                    $mail->Port = 587;
                    $mail->CharSet = 'UTF-8';
                    $mail->setFrom('noreply@celavigestion.fr', 'CELAVIGestion');
                    $mail->addAddress($email);
                    $mail->isHTML(true);
                    $mail->Subject = mb_encode_mimeheader('Réinitialisation de mot de passe', $mail->CharSet);
                    $urlRecup = base_url('/Auth/newpass?crypto=' . $emailCrypted);
                    $mail->Body = '
                    Bonjour ' . $utilNom . ', <br/><br/>
                    Vous avez demandé à reinitialiser votre mot de passe. <br/><br/>
                    Veuillez cliquer sur le bouton pour confirmer la 
                    réinitialisation de votre mot de passe. <br/><br/> 
                    <a href=\'' . $urlRecup . '\'><b>Cliquez ici</b></a><br/><br/>
                    Si besoin, vous pouvez copier/coller ce lien dans votre navigateur.<br/><br/>
                    Ce mail est un mail automatique, merci de ne pas y répondre.<br/><br/>
                    Cordialement,<br/><br/>
                    L\'équipe CELAVI <br/><br/>
                    ---------------------------------<br/><br/>
                    Dear ' . $utilNom . ', <br/><br/>
                    You have requested to reset your password.<br/><br/>
                    Please click the button to confirm the password reset.<br/><br/>
                    <a href=\'' . $urlRecup . '\'><b>Click here</b></a><br/><br/>
                    If needed, you can copy/paste this link into your browser.<br/><br/>
                    This email is automated; please do not reply to it.<br/><br/>
                    Best regards,<br/><br/>
                    The CELAVI Team
                    ';

                    if (!$mail->send()) {
                        $retour = array('status' => 500, 'email' => $email, 'message' => 'Mailer Error');
                    } else {
                        $data = array(
                            'cle_temporaire' => $key,
                            'lien' => $urlRecup,
                            'date_creation' => $newDateTime->format('Y-m-d H:i:s'),
                            'etat' => 0
                        );
                        $this->lien->save_data($data);

                        $retour = array('status' => 200, 'email' => $data_retour, 'message' => 'Mailer sent!');
                    }
                } catch (Exception $e) {
                    $retour = array('status' => 0, 'message' => 'Erreur init PHPMailer');
                }
                echo json_encode($retour);
            }
        }
    }
}
