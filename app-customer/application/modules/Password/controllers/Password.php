<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Password extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Password";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/password/password.js",
        );
        $this->_css_personnaliser = array(
            "css/password/password.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "password";
        $this->_data['sous_module'] = "password/contenaire-mdp";
        $this->render('contenaire');
    }

    public function loadPassword()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['client_id'] = $_SESSION['session_utilisateur_client']['client_id'];
                $this->renderComponant("Password/password/password-page", $data);
            }
        }
    }

    public function pageformPwd()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];
                $data['client_id'] = $data_post['client_id'];
                $this->renderComponant("Password/password/form-mdp",$data);
                
            }
        }
    }

    public function updateMdp()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Utilisateur_m', 'users');

                $_POST['data']['util_password']['value'] = trim(decrypt_service($_POST['data']['util_password']['value']));
                $control = $this->control_data($_POST['data']);
                $retour = retour(null, null, null, $control['information']);
                if ($control['return']) {
                    $data_retour = $_POST['data']['client_id'];                                     
                    $data_post = format_data($_POST['data']);
                    $clause = array("client_id" =>  $data_post['client_id']);
                    $clause_mdp = array("client_id" =>  $data_post['client_id']);
                    $verify_ident = array(
                        'clause' => $clause_mdp,
                        'method' => "row",
                        'columns' => 'util_password'                        
                    );
                    $request = $this->users->get($verify_ident);

                    var_dump($request);
                    $util_password_new = trim(decrypt_service($_POST['data']['util_password_new']['value']));
                    
                    var_dump($util_password_new);die;
                    if (!empty($request)) {
                        $data = array(
                            'util_password' => hash('sha512', $data_post['nouveauMotDePasse']),
                            // 'util_password' => $data_post['util_password_new'],
                        );
                        $this->users->save_data($data,$clause);
                        $retour = retour(
                            true,
                            "success",
                            $data_retour,
                            array(
                                "message" =>"Mot de passe changé avec succès."
                            )
                        );
                    } else {
                        $retour = retour(
                            false,
                            "failed",
                            $data_retour,
                            array(
                                "message" =>"Ancien mot de passe incorrect."
                            )
                        );
                    }                    
                }
                echo json_encode($retour);
            }
        }
    }
}
