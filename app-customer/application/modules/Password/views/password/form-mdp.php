<input type="hidden" name="client_id" value="<?=$client_id?>">
<div class="row">
    <div class="col-12 p-0 py-1">
        <div class="card rounded-0 h-100">
            <div class="card-body">
                <div class="form-group inline">
                    <label for="ancienMotDePasse"><?= $this->lang->line('ancien_mdp'); ?></label>
                    <input type="password" class="form-control" name="util_password" id="ancienMotDePasse" placeholder="<?= $this->lang->line('saisi_ancien_mdp'); ?>" required>
                </div>
                <div class="form-group inline">
                    <label for="nouveauMotDePasse"><?= $this->lang->line('nouveau_mdp'); ?></label>
                    <input type="password" class="form-control" name="nouveauMotDePasse" id="nouveauMotDePasse" placeholder="<?= $this->lang->line('saisi_nouv_mdp'); ?>" required>
                </div>
                <div class="form-group inline">
                    <label for="confirmerNouveauMotDePasse"><?= $this->lang->line('confirm_mdp'); ?></label>
                    <input type="password" class="form-control" name="util_password_new"  id="util_password_new" placeholder="<?= $this->lang->line('confirm_nouv_mdp'); ?>" required>
                </div>
                <button type="submit" class="btn btn-primary mt-2"><?= $this->lang->line('modifier_mdp'); ?></button>
            </div>
        </div>
    </div>
</div>