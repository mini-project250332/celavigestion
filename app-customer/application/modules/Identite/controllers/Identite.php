<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Identite extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Identite";
    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/identite/identite.js",
        );

         $this->_css_personnaliser = array(
            "css/identite/identite.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "identite";
        $this->_data['sous_module'] = "identite/contenaire-identite";
        $this->render('contenaire');
    }

    public function loadIdentite()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['client_id'] = $_SESSION['session_utilisateur_client']['client_id'];
                $this->renderComponant("Identite/identite/identite-page", $data);
            }
        }
    }

    public function pageficheClient()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Client_m', 'client');
                $data_post = $_POST["data"];
                $clients = $this->client->get(array(
                    'clause' => array('c_client.client_id' => $data_post['client_id']),
                    'method' => 'row',
                    'join' => array(
                        'c_dossier' => 'c_client.client_id = c_dossier.client_id', 
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id', 
                        'c_paysmonde' => 'c_paysmonde.paysmonde_id = c_client.paysmonde_id',
                        'c_pays' => 'c_pays.pays_id = c_client.pays_id'
                    )
                ));
        
                $data['pays_indicatif'] = $this->getListPays();
                $data['pays'] = $this->getListPaysMonde();
                $data['client'] = $clients;
                if ($data_post['page'] == "fiche") {
                    $this->renderComponant("Identite/identite/fiche-identification", $data);
                } else {
                    $this->renderComponant("Identite/identite/form-update-contact", $data);
                }
            }
        }
    }

    public function update_cin(){
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'data_retour' => null,
                'status' => false,
                'type' => 'error',
                'form_validation' => array()
            );

            $this->load->model('Client_m', 'client');
            $this->load->model('Tache_m', 'tache');

            // Vérifier si le champ 'fichier_cin' est présent dans la requête POST
            if (isset($_FILES['fichier_cin']) && !empty($_FILES['fichier_cin']['name']) && $_FILES['fichier_cin']['size'] != 0) {
                
                $uploadDir = '../documents/cin/'.$_SESSION['session_utilisateur_client']['client_id'].'/';

                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                $filename = $_FILES['fichier_cin']['name'];
                $targetFile = $uploadDir . basename($filename);

                $clause = array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']);
                $request_client = $this->client->get(array(
                    'clause' => $clause,
                    'method' => 'row'
                ));

                $request_client = $this->client->get(array(
                    'clause' => $clause,
                    'method' => 'row',
                    'join' => array(
                        'c_dossier' => 'c_client.client_id = c_dossier.client_id', 
                        'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',                            
                    ),
                    'join_orientation' => 'left'
                ));

                $data_tache = array(
                    'tache_nom' => "Saisie CNI par le propriétaire",
                    'tache_date_creation' => date('Y-m-d H:i:s'),
                    'tache_date_echeance' => date('Y-m-d H:i:s'),
                    'tache_prioritaire' => 2,
                    'tache_texte' => "Le propriétaire a ajouté des informations de CNI sur l'Extranet. Veuillez consulter la page Propriétaire pour voir les changements.",
                    'tache_valide' => 0,
                    'tache_createur' => $request_client->util_prenom,
                    'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'tache_etat' => 1,
                    'theme_id' => 29,
                    'type_tache_id' => 3,
                    'participant' => $request_client->util_id
                );

                $params_tache = $this->tache->get(array(
                    'clause' => array(
                        'tache_nom' => "Saisie CNI par le propriétaire",
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 29,
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1
                    ),
                    'method' => 'row',
                    
                ));

                $fileInfo = pathinfo($targetFile);
                $counter = 1;

                while (file_exists($targetFile)) {
                    // Si le fichier existe déjà, ajouter un numéro d'incrémentation au nom du fichier
                    $filename = $fileInfo['filename'] . '_' . $counter . '.' . $fileInfo['extension'];
                    $targetFile = $uploadDir . $filename;
                    $counter++;
                }

                if (move_uploaded_file($_FILES['fichier_cin']['tmp_name'], $targetFile)) {
                    if (!empty($_POST['client_date_fin_validite_cni'])) {
                        if($_POST['client_date_fin_validite_cni'] < date('Y-m-d')){
                            $retour['form_validation']['client_date_fin_validite_cni'] = array('required' => 'la date doit-être supérieure à la date du jour');                                                 
                        }else{
                            $data_cin = array(
                                'cin' => $targetFile,
                                'etat_cin' => 1,
                                'client_date_fin_validite_cni' => $_POST['client_date_fin_validite_cni']
                            );
                            
                            if (empty($params_tache)) {
                                $request_tache = $this->tache->save_data($data_tache);
                            }
                            $request_cin = $this->client->save_data($data_cin, $clause);   
                        }                        
                    }else{
                        $retour['form_validation']['client_date_fin_validite_cni'] = array('required' => 'Ce champ ne doit pas être vide');
                    }   
                    // Vérifier la condition de réussite
                    if (!empty($request_cin)) {
                        
                        //$_SESSION['session_utilisateur_client']['client_cin'] = $targetFile;

                        $retour['data_retour'] = null;
                        $retour['status'] = true;
                        $retour['type'] = "succes";
                        $retour['form_validation']['message'] = empty($request_client->cin) ? "Une nouvelle pièce d'identité enregistrée avec succès" : "Pièce d'identité cin a été mise à jour avec succès";
                    }
                } else {
                    throw new Exception("Erreur lors de l'envoi du fichier $filename");
                }
            } else {
                // Gérer le cas où le champ 'fichier_cin' est absent ou vide
                $retour['form_validation']['fichier_cin'] = array('required' => 'Ce champ ne doit pas être vide');
            }

            // Toujours renvoyer la réponse JSON, que le fichier ait été téléchargé ou non
            echo json_encode($retour);
        }

    } 
}
