<input type="hidden" value="<?= $client->client_id ?>" name="client_id">
<div id="div-fiche-client" class="row m-0">
    <div class="col p-1 py-1">
        <div class="card rounded-0 h-100">
            <div class="card-header rounded" style="background: #1F659E;">
                <h5 class="fs-0 mb-2 text-white">
                    <i class="fas fa-address-card"></i>
                    <?= $this->lang->line('identite_titre') ?>
                </h5>
            </div>
            <div class="card-body ">
                <table class="table table-borderless  mb-0">
                    <tbody>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('nom'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->client_nom) ? $client->client_nom : ''; ?>
                            </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('prenom'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->client_prenom) ? $client->client_prenom : ''; ?>
                            </th>
                        </tr>

                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('entreprise'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->client_entreprise) ? $client->client_entreprise : ''; ?>
                            </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('nationalite'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->client_nationalite) ? $client->client_nationalite : ''; ?>
                            </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('situation_matrimonile'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?php
                                $client_situation_matrimoniale = '';
                                if ($client->client_situation_matrimoniale != '') :
                                    if ($client->client_situation_matrimoniale == 'Marié') {
                                        $client_situation_matrimoniale = $this->lang->line('marie');
                                    } elseif ($client->client_situation_matrimoniale == 'Pacsé') {
                                        $client_situation_matrimoniale = $this->lang->line('pacse');
                                    } else {
                                        $client_situation_matrimoniale = $this->lang->line('celibataire');
                                    }
                                endif ?>
                                <?= $client_situation_matrimoniale; ?>
                            </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('residence_fiscale'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->client_residence_fiscale) ? $client->client_residence_fiscale : ''; ?>
                            </th>
                        </tr>
                        <tr class="border-bottom">
                            <th class="ps-0"><?= $this->lang->line('suivi_par'); ?> : </th>
                            <th class="pe-0 text-end">
                                <?= isset($client->util_id) ? $client->util_nom . ' ' . $client->util_prenom : ''; ?>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col p-1 py-1">
        <div class="card rounded-0 h-100">
            <div class="card-header rounded" style="background: #1F659E;">
                <h5 class="fs-0 mb-2 text-white">
                    <i class="fas fa-phone-square-alt fs-1"></i>
                    <?= $this->lang->line('contact') ?>
                </h5>
            </div>
            <div class="card-body">
                <hr>
                <table class="table table-borderless mb-4">
                    <tbody>
                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('email_1'); ?> *</label>
                                <input data-type="email" type="text" class="form-control" value="<?= $client->client_email; ?>" id="client_email" name="client_email" data-formatcontrol="true" data-require="true">
                                <input data-type="email" type="hidden" class="form-control" value="<?= $client->client_email; ?>" id="hist_email_1_ancien" name="hist_email_1_ancien">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('email_2'); ?></label>
                                <input data-type="email" type="text" class="form-control" value="<?= $client->client_email1; ?>" id="client_email1" name="client_email1">
                                <input data-type="email" type="hidden" class="form-control" value="<?= $client->client_email1; ?>" id="hist_email_2_ancien" name="hist_email_2_ancien">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('phone_1'); ?> * </label>
                                <select class="form-control select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id" name="paysmonde_id">
                                    <?php foreach ($pays_indicatif as $indicatif) : ?>
                                        <option value="<?= $indicatif->paysmonde_id ?>" <?php echo isset($client->paysmonde_id) ? (($client->paysmonde_id !== NULL) ? (($client->paysmonde_id == $indicatif->paysmonde_id) ? "selected" : " ") : "") : "" ?>>
                                            <?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <select class="form-control select2 select2-hidden-accessible" hidden style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="paysmonde_id_ancien" name="paysmonde_id_ancien">
                                    <?php foreach ($pays_indicatif as $indicatif) : ?>
                                        <option value="<?= $indicatif->paysmonde_id ?>" <?php echo isset($client->paysmonde_id) ? (($client->paysmonde_id !== NULL) ? (($client->paysmonde_id == $indicatif->paysmonde_id) ? "selected" : " ") : "") : "" ?>>
                                            <?= $indicatif->paysmonde_indicatif ?> (<?= $indicatif->paysmonde_libellecourt ?>)
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <input v-on:keyup='input_form' type="text" class="form-control chiffre  mx-1" id="client_phonemobile" name="client_phonemobile" style="height: 32px; width: 80%;left: 4px" value="<?= $client->client_phonemobile ?>">
                                <input type="hidden" class="form-control chiffre  mx-1" id="hist_tel_1_ancien" name="hist_tel_1_ancien" value="<?= $client->client_phonemobile ?>">
                                <div class="client_phonemobile_help help text-danger" style="margin-top: 9px;"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"><?= $this->lang->line('phone_2'); ?></label>
                                <select class="form-select select2 select2-hidden-accessible" style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id" name="pays_id">
                                    <?php foreach ($pays as $indicatifs) : ?>
                                        <option value="<?= $indicatifs->pays_id ?>" <?php echo isset($client->pays_id) ? (($client->pays_id !== NULL) ? (($client->pays_id == $indicatifs->pays_id) ? "selected" : " ") : "") : "" ?>>
                                            <?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <select class="form-select select2 select2-hidden-accessible" hidden style="width: 25%; left: 5px;" tabindex="-1" aria-hidden="true" id="pays_id_ancien" name="pays_id_ancien">
                                    <?php foreach ($pays as $indicatifs) : ?>
                                        <option value="<?= $indicatifs->pays_id ?>" <?php echo isset($client->pays_id) ? (($client->pays_id !== NULL) ? (($client->pays_id == $indicatifs->pays_id) ? "selected" : " ") : "") : "" ?>>
                                            <?= $indicatifs->pays_indicatif ?> (<?= $indicatifs->pays_libellecourt ?>)
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <input v-on:keyup='input_form' type="text" class="form-control chiffre  mx-1" id="client_phonefixe" name="client_phonefixe" style="height: 32px;width: 80%; left: 4px" value="<?= $client->client_phonefixe ?>">
                                <input type="hidden" class="form-control chiffre  mx-1" id="hist_tel_2_ancien" name="hist_tel_2_ancien" value="<?= $client->client_phonefixe ?>">
                                <div class="help"></div>
                            </div>
                        </div>
                    </tbody>
                </table>
                <h5 class="fs-0 mb-3"><?= $this->lang->line('adresses') ?></h5>
                <hr>
                <table class="table table-borderless  mb-0">
                    <tbody>
                        <div class="form-group inline">
                            <div>
                                <label data-width="150"><?= $this->lang->line('adresses_1') ?> </label>
                                <input type="text" class="form-control maj" value="<?= $client->client_adresse1; ?>" id="client_adresse1" name="client_adresse1">
                                <input type="hidden" class="form-control maj" value="<?= $client->client_adresse1; ?>" id="hist_adresse_1_ancien" name="hist_adresse_1_ancien">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('adresses_2') ?></label>
                                <input type="text" class="form-control maj" value="<?= $client->client_adresse2; ?>" id="client_adresse2" name="client_adresse2">
                                <input type="hidden" class="form-control maj" value="<?= $client->client_adresse2; ?>" id="hist_adresse_2_ancien" name="hist_adresse_2_ancien">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('adresses_3') ?> </label>
                                <input type="text" class="form-control maj" value="<?= $client->client_adresse3; ?>" id="client_adresse3" name="client_adresse3">
                                <input type="hidden" class="form-control maj" value="<?= $client->client_adresse3; ?>" id="hist_adresse_3_ancien" name="hist_adresse_3_ancien">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('code_postal') ?> </label>
                                <input type="text" class="form-control" value="<?= $client->client_cp; ?>" id="client_cp" name="client_cp">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"> <?= $this->lang->line('ville') ?> </label>
                                <input type="text" class="form-control maj" value="<?= $client->client_ville; ?>" id="client_ville" name="client_ville">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="form-group inline">
                            <div>
                                <label data-width="150"><?= $this->lang->line('pays') ?> </label>
                                <input type="text" class="form-control maj" value="<?= $client->client_pays; ?>" id="client_pays" name="client_pays" data-formatcontrol="true" data-require="true">
                                <div class="client_pays_help text-danger help"></div>
                            </div>
                        </div>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>