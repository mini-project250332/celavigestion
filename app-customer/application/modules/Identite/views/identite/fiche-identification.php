<?php
$phoneNumber = $client->client_phonemobile;
$phoneMobile;
$phoneFixe = null;
if (isset($client->paysmonde_id) && $client->paysmonde_id == 66) {
	$array = str_split($phoneNumber, 2);
	$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
	$phoneMobile = implode(".", $array);
} else {
	$phoneMobile = isset($client->client_phonemobile) ? $client->client_phonemobile : 0;
}

if (isset($client->client_phonefixe) && $client->client_phonefixe != "") {
	$phone = isset($client->client_phonefixe) ? $client->client_phonefixe : 0;
	if (isset($client->pays_id) && $client->pays_id == 66) {
		$array = str_split($phone, 2);
		$array[0] = '(' . $array[0][0] . ')' . $array[0][1];
		$phoneFixe = implode(".", $array);
	} else {
		$phoneFixe = isset($client->client_phonefixe) ? $client->client_phonefixe : 0;
	}
}

function formatTelephone($numero)
{
	$numero = preg_replace("/[^0-9]/", "", $numero);
	$longueur = strlen($numero);

	if ($longueur >= 2) {
		$numero_formate = "0" . substr($numero, 1, 1) . " " . substr($numero, 2, 2) . " " . substr($numero, 4, 2) . " " . substr($numero, 6, 2) . " " . substr($numero, 8, 2) . " " . substr($numero, 10);
		return $numero_formate;
	}
}
?>

<div id="div-fiche-client" class="row m-0">
	<div class="col p-1 py-1">
		<div class="card rounded-0 h-100">
			<div class="card-header rounded" style="background: #1F659E;">
				<h5 class="mb-1 fs-0 text-white">
					<i class="fas fa-address-card"></i>
					<?= $this->lang->line('identite_titre') ?>
				</h5>
			</div>
			<div class="card-body ">
				<table class="table table-borderless mb-0">
					<tbody>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('nom'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_nom) ? $client->client_nom : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('prenom'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_prenom) ? $client->client_prenom : ''; ?>
							</th>
						</tr>

						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('entreprise'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_entreprise) ? $client->client_entreprise : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('nationalite'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_nationalite) ? $client->client_nationalite : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('situation_matrimonile'); ?> : </th>
							<th class="pe-0 text-end">
								<?php
								$client_situation_matrimoniale = '';
								if ($client->client_situation_matrimoniale != '') :
									if ($client->client_situation_matrimoniale == 'Marié') {
										$client_situation_matrimoniale = $this->lang->line('marie');
									} elseif ($client->client_situation_matrimoniale == 'Pacsé') {
										$client_situation_matrimoniale = $this->lang->line('pacse');
									} else {
										$client_situation_matrimoniale = $this->lang->line('celibataire');
									}
								endif ?>
								<?= $client_situation_matrimoniale; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('residence_fiscale'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_residence_fiscale) ? $client->client_residence_fiscale : ''; ?>
							</th>
						</tr>
					
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('suivi_par'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->util_id) ? $client->util_nom . ' ' . $client->util_prenom : ''; ?>
							</th>
						</tr>
					</tbody>
				</table>

				<div class="row m-0">
	                <div class="col py-4">
	                    <div class="d-flex">
	                        <div style="max-width: 130px;">
	                            <img class="w-100" src="<?=base_url('assets/images/photo4.jpg');?>">
	                        </div>
	                        <div>
	                            <div style="background: #d4f2ff;font-size: 18px;border-radius: 40px;padding: 24px;text-align: center;">
	                                <?= $this->lang->line('texte-conseil-client') ?> <br>
										
									<?php if ($_SESSION['session_utilisateur_client']['lang'] == "french") : ?>
										<?= $this->lang->line('telephone') ?> 
										<?= !empty($client->util_phonemobile) ? formatTelephone($client->util_phonemobile) : formatTelephone($client->util_phonefixe);?>
										<br>
									<?php else : ?>
										<?php $number = !empty($client->util_phonemobile) ? formatTelephone($client->util_phonemobile) : formatTelephone($client->util_phonefixe);
										if (strlen($number) > 0) {
											$premier_caractere = substr($number, 0, 1);
											$reste_texte = substr($number, 1);
											$number = "($premier_caractere)$reste_texte";
										}
										?>
										<?= $this->lang->line('telephone') ?> 00 33 <?= $number ?><br>
									<?php endif; ?>
									<?= $this->lang->line('email') ?> <?= isset($client->util_mail) ? $client->util_mail : ''; ?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

			</div>
		</div>
	</div>

	<div class="col p-1 py-1">
		<div class="card rounded-0 h-100">
			<div class="card-header rounded d-flex align-items-center" style="background: #1F659E;">
				<h5 class="mb-0 fs-0 text-white">
					<i class="fas fa-phone-square-alt fs-1"></i>
					<?= $this->lang->line('contact') ?>
				</h5>
			</div>
			<div class="card-body">
				<table class="table table-borderless mb-4">
					<tbody>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('email_1'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_email) ? $client->client_email : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('email_2'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_email1) ? $client->client_email1 : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('phone_1'); ?> : </th>
							<th class="pe-0 text-end">
								<?= isset($client->paysmonde_indicatif) ? $client->paysmonde_indicatif : ''; ?>
								<?= $phoneMobile ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('phone_2'); ?> : </th>
							<th class="pe-0 text-end">
								<?php
								$clientPhoneFixe = isset($client->client_phonefixe) ? $client->client_phonefixe : '';
								$clientPaysIndicatif = isset($client->pays_indicatif) ? $client->pays_indicatif : '';
								?>
								<?= (trim($phoneFixe) != "") ? $clientPaysIndicatif : ''; ?>
								<?= $phoneFixe ?>
							</th>
						</tr>
					</tbody>
				</table>
				<h5 class="mb-1"><?= $this->lang->line('adresses') ?></h5>
				<table class="table table-borderless mb-0">
					<tbody>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('adresses_1') ?> </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_adresse1) ? $client->client_adresse1 : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('adresses_2') ?></th>
							<th class="pe-0 text-end">
								<?= isset($client->client_adresse2) ? $client->client_adresse2 : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('adresses_3') ?></th>
							<th class="pe-0 text-end">
								<?= isset($client->client_adresse3) ? $client->client_adresse3 : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('code_postal') ?></th>
							<th class="pe-0 text-end">
								<?= isset($client->client_cp) ? $client->client_cp : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0"><?= $this->lang->line('ville') ?> </th>
							<th class="pe-0 text-end">
								<?= isset($client->client_ville) ? $client->client_ville : ''; ?>
							</th>
						</tr>
						<tr class="border-bottom">
							<th class="ps-0 pb-0"><?= $this->lang->line('pays') ?></th>
							<th class="pe-0 text-end">
								<?= isset($client->client_pays) ? $client->client_pays : ''; ?>
							</th>
						</tr>
					</tbody>
				</table>
				
				<div class="row m-0">
	                <div class="col py-4">
	                    <div class="d-flex">
	                        <div style="max-width: 130px;">
	                            <img class="w-100" src="<?=base_url('assets/images/photo4.jpg');?>">
	                        </div>
	                        <div>
	                            <div style="background: #d4f2ff;font-size: 18px;border-radius: 40px;padding: 24px;text-align: center;">
	                                <?= $this->lang->line('texte-conseil-client1'); ?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

			</div>
		</div>
		<div class="py-4">

		</div>
	</div>
</div>
