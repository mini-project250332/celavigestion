<?php echo form_tag('Contacts/updateContact', array('method' => "post", 'class' => 'h-100 px-2', 'id' => 'updateContact')); ?>
<div class="contenair-title bg-200 rounded">
    <div class="px-2">
        <h5 class="fs-1">
            <i class="fas fa-id-card-alt"></i>&nbsp;
            <?= $this->lang->line('identite_prop'); ?>
        </h5>
    </div>
    <div class="px-2">

        &nbsp;
        <button class="btn btn-outline-primary" id="update-contact" data-id="<?= $client_id; ?>" type="button" title="<?= $this->lang->line('modifier'); ?>">
            <i class="fas fa-user-edit"></i>
            <span class="libelle-bnt-icon"><?= $this->lang->line('modifier'); ?></span>
        </button>

        <button type="button" data-id="<?= $client_id; ?>" id="annuler-update-contact" class="btn btn-outline-secondary mx-1 d-none" title="<?= $this->lang->line('annuler'); ?> ">
            <i class="far fa-times-circle"></i>
            <span class="libelle-bnt-icon"><?= $this->lang->line('annuler'); ?> </span>
        </button>
        <button type="submit" data-id="<?= $client_id; ?>" id="save-update-contact" class="btn btn-outline-primary mx-1 d-none" title="<?= $this->lang->line('terminer'); ?>">
            <i class="fas fa-check-circle"></i>
            <span class="libelle-bnt-icon"><?= $this->lang->line('terminer'); ?> </span>
        </button>
        
    </div>
</div>

<div id="fiche-identifiant-client" class="contenair-main" style="height: calc(100% - 3.5rem);overflow-y: auto;">

</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        getFicheIdentificationClient(<?= $client_id; ?>, 'fiche');
    });
</script>

<div class="modal fade" id="modalForm-cni" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" id="modal-main-content">

            <div class="modal-header" style="background-color : #636E7E !important">
                <h5 class="modal-title text-white text-center"> <?= $this->lang->line('title_formulaire_cin') ?> </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <?php echo form_tag('Identite/update_cin', array('method' => "post", 'class' => '', 'id' => 'update_cin')); ?>
            <div class="modal-body">
                <div class="row m-0">
                    <div class="col py-1">

                        <div class="form-group inline">
                            <div class="row m-0">
                                <label class="col-auto"><?= $this->lang->line('fichier_encaissement') ?> :</label>
                                <div class="col">
                                    <input class="form-control fs--1" type="file" id="fichier_cin" name="fichier_cin" style="line-height: 1.5!important;">
                                </div>
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="form-group inline">
                            <div class="row m-0">
                                <label class="col-auto"> <?= $this->lang->line('date_fin_validite'); ?> : </label>
                                <div class="col">
                                    <input type="date" class="form-control fs--1" id="client_date_fin_validite_cni" name="client_date_fin_validite_cni" value="">                                    
                                </div>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= $this->lang->line('annuler'); ?></button>
                    <button type="submit" class="btn btn-primary"><?= $this->lang->line('terminer'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>

<script type="text/javascript">
    var myModal = new bootstrap.Modal(document.getElementById("modalForm-cni"), {
        keyboard: false
    });
</script>