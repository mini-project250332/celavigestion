<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Encaissement extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Encaissement";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/encaissement/encaissement.js",
        );
        $this->_css_personnaliser = array(
            "css/encaissement/encaissement.css",
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "encaissement";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "encaissement/contenaire-encaissement" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Encaissement/encaissement/encaissement_first_page", $data);
            }
        }
    }

    public function getListeEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Encaissement_m', 'encaissement');
                $this->load->model('TypeEncaissementEn_m');

                $data['cliLot_id'] = $_POST['data']['cliLot_id'];
                $langue = $_SESSION['session_utilisateur_client']['lang'];
                $data['langue'] = $langue;

                if ($langue == "french") {
                    $clause = array(
                        'clause' => array(
                            "enc_etat" => 1,
                            "clilot_id" => $data['cliLot_id'],
                            "DATE_FORMAT(enc_date, '%Y') =" => $_POST['data']['annee'],
                        ),
                        'order_by_columns' => 'enc_date DESC',
                        'join' => array(
                            'c_type_encaissement' => 'c_type_encaissement.typeEnc_id = c_encaissement.typeEnc_id',
                        ),
                    );
                } else {
                    $clause = array(
                        'clause' => array(
                            "enc_etat" => 1,
                            "clilot_id" => $data['cliLot_id'],
                            "DATE_FORMAT(enc_date, '%Y') =" => $_POST['data']['annee'],
                        ),
                        'order_by_columns' => 'enc_date DESC',
                        'join' => array(
                            'c_type_encaissement_en' => 'c_type_encaissement_en.typeEnc_id = c_encaissement.typeEnc_id',
                        ),
                    );
                }

                $encaissements = $this->encaissement->get($clause);
                $this->load->model('DocumentEncaissement_m', 'document_enc');

                if (!empty($encaissements)) {
                    foreach ($encaissements as $key => $value) {
                        $encaissements[$key]->files = $this->document_enc->get(array('clause' => array('enc_id' => $value->enc_id)));
                    }
                }
                $data['encaissements'] = $encaissements;
                $data['annee'] = $_POST['data']['annee'];

                echo $_POST['data']['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Encaissement/encaissement/liste-encaissement", $data);
            }
        }
    }

    public function get_formulaireEncaissement()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data['cliLot_id'] = $_POST['data']['cliLot_id'];
                $this->load->model('TypeEncaissement_m', 'type_enc');
                $this->load->model('TypeEncaissementEn_m');

                $langue = $_SESSION['session_utilisateur_client']['lang'];
                $data['langue'] = $langue;

                if (isset($_POST['data']['enc_id'])) {
                    $params = array(
                        'clause' => array('enc_id' => $_POST['data']['enc_id']),
                        'method' => 'row',
                    );
                    $this->load->model('Encaissement_m', 'encaissement');
                    $data['encaissement'] = $this->encaissement->get($params);

                    $this->load->model('DocumentEncaissement_m', 'document_enc');
                    $data['document_enc'] = $this->document_enc->get(
                        array('clause' => array('enc_id' => $_POST['data']['enc_id']))
                    );
                }
                $langueModeleMap = array(
                    'french' => 'type_enc',
                    'english' => 'TypeEncaissementEn_m',
                );


                if (array_key_exists($langue, $langueModeleMap)) {
                    $modele = $this->{$langueModeleMap[$langue]};
                    $data['type_enc'] = $modele->get(array());
                } else {
                    $modele = $this->{$langueModeleMap['french']};
                    $data['type_enc'] = $modele->get(array());
                }

                // $data['type_enc'] = $this->type_enc->get(array());

                $this->renderComponant("Encaissement/encaissement/form-ajoutEncaissement", $data);
            }
        }
    }

    public function crudEncaissement()
    {
        if (is_ajax() && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'data_retour' => null,
                'status' => false,
                'type' => 'error',
                'form_validation' => array()
            );

            if (empty($_POST['enc_montant']) || empty($_POST['enc_date'])) {
                if (empty($_POST['enc_montant']))
                    $retour['form_validation']['enc_montant'] = array('required' => 'Ce champ ne doit pas être vide.');

                if (empty($_POST['enc_menc_dateontant']))
                    $retour['form_validation']['enc_date'] = array('required' => 'Ce champ ne doit pas être vide.');
            } else {

                $encMontantValue = str_replace(',', '.', $_POST['enc_montant']);
                $data_enc = array(
                    'enc_montant' => $encMontantValue,
                    'enc_info' => $_POST['enc_info'],
                    'enc_date' => date('Y-m-d', strtotime($_POST['enc_date'])),
                    'typeEnc_id' => $_POST['typeEnc_id'],
                    'clilot_id' => $_POST['cliLot_id'],
                    'enc_etat' => 1
                );

                $clause = (isset($_POST['enc_id'])) ? array('enc_id' => $_POST['enc_id']) : null;

                $this->load->model('Encaissement_m', 'encaissement');
                $request_enc = $this->encaissement->save_data($data_enc, $clause);

                if (!empty($request_enc)) {
                    $clause_files = $clause ?? ['enc_id' => $request_enc];

                    $this->load->model('DocumentEncaissement_m', 'document_enc');
                    $document_files = $this->document_enc->get(array('clause' => $clause_files));

                    $retour['form_validation']['message'] = is_null($clause) ? "Information encaissement enregistré avec succès " : "Information encaissement a été mis à jour avec succès";

                    if (isset($_FILES['fichier_encaissement']) && is_array($_FILES['fichier_encaissement']['name']) && $_FILES['fichier_encaissement']['size'][0] != 0) {
                        try {
                            $fileData = $this->processUploadedFiles($_FILES['fichier_encaissement'], $document_files, $_POST['cliLot_id'], $clause_files['enc_id']);
                            $retour['form_validation']['message'] .= $this->getUploadMessage($fileData);
                        } catch (Exception $e) {
                            $retour['form_validation']['message'] .= " Erreur: " . $e->getMessage();
                        }
                    }

                    // Tache 
                    $this->load->model('Client_m', 'client');
                    $request_client = $this->client->get(
                        array(
                            'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                            'method' => 'row',
                            'join' => array(
                                'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            ),
                            'join_orientation' => 'left'
                        )
                    );

                    $params_lot = array(
                        'clause' => array(
                            'cliLot_etat' => 1,
                            'cliLot_id' => $_POST['cliLot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        ),
                        'method' => 'row'
                    );

                    $this->load->model('ClientLot_m', 'lot');
                    $data['lot'] = $this->lot->get($params_lot);
                    $this->load->model('Tache_m', 'tache');

                    $data_tache = array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => 26,
                        'tache_nom' => is_null($clause) ? "Saisie d'un encaissement pour le Propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")" : "Saisie d'un encaissement pour le Propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",
                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1
                    );

                    $params_tache = $this->tache->get(
                        array(
                            'clause' => $data_tache,
                            'method' => 'row',
                        )
                    );

                    if (empty($params_tache)) {
                        $data_tache['tache_date_creation'] = date('Y-m-d H:i:s');
                        $data_tache['tache_date_echeance'] = date('Y-m-d H:i:s');
                        $data_tache['tache_prioritaire'] = 2;
                        $data_tache['tache_texte'] = "Le propriétaire a modifié ses documents et/ou des informations d'un encaissement sur l'Extranet. Veuillez consulter la page du lot puis l'onglet encaissement pour voir les modifications";
                        $data_tache['tache_etat'] = 1;
                        $data_tache['theme_id'] = 26;
                        $data_tache['type_tache_id'] = 3;
                        $data_tache['participant'] = $request_client->util_id;
                        $request_tache = $this->tache->save_data($data_tache);
                    }

                    $retour['data_retour'] = $_POST['cliLot_id'];
                    $retour['status'] = true;
                    $retour['type'] = "succes";

                } else {
                    $retour['form_validation']['message'] = "Erreur enregistrement encaissement.";
                }
            }

            echo json_encode($retour);
        }
    }

    function processUploadedFiles($files, $document_files, $clilot_id, $enc_id)
    {
        $fileData = array();
        $uploadDir = '../documents/encaissement/';

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }

        foreach ($files['tmp_name'] as $key => $tempFile) {
            $filename = $_FILES['fichier_encaissement']['name'][$key];
            $targetFile = $uploadDir . basename($filename);
            $fileSize = $_FILES['fichier_encaissement']['size'][$key];

            $verify_pjoin = array_filter($document_files, function ($file) use ($filename, $targetFile) {
                return $file->doc_encaissement_nom === $filename && $file->doc_encaissement_path === $targetFile;
            });

            if (empty($verify_pjoin)) {
                if (move_uploaded_file($tempFile, $targetFile)) {
                    $fichier = array(
                        'doc_encaissement_nom' => $filename,
                        'doc_encaissement_path' => $targetFile,
                        'doc_encaissement_creation' => date('Y-m-d H:i'),
                        'clilot_id' => $clilot_id,
                        'enc_id' => $enc_id
                    );
                    $fileData[] = $fichier;
                } else {
                    throw new Exception("Erreur upload fichier $filename");
                }
            }
        }
        return $fileData;
    }

    function getUploadMessage($fileData)
    {
        if (!empty($fileData)) {
            $this->load->model('DocumentEncaissement_m', 'document_enc');
            $request_pjoin = $this->document_enc->multi_insert_data($fileData);
            if ($request_pjoin)
                return " <br> Fichiers téléchargés avec succès ";
            else
                return " <br> Erreur enregistrement fichier dans base. ";
        } else {
            return " <br> Aucun fichier téléchargé, ils ont tous déjà été téléchargés ou utilisés dans un autre contenu de communication auparavant. Veuillez les renommer";
        }
    }


    function deleteEncaissement()
    {
        if (is_ajax()) {
            if ($_POST) {
                $data_post = $_POST["data"];
                $data_retour = array(
                    "enc_id" => $data_post['enc_id'],
                    "action" => $data_post['action'],
                    "cliLot_id" => $data_post['cliLot_id'],
                );

                if ($data_post['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data_post['action'],
                        'data' => array(
                            'enc_id' => $data_post['enc_id'],
                            'cliLot_id' => $data_post['cliLot_id'],
                            'title' => "Confirmation la suppression de l'encaissement",
                            'text' => "Voulez-vous vraiment supprimer cet encaissement ?",
                            'btnConfirm' => "Confirmer",
                            'btnAnnuler' => "Annuler"
                        ),
                        'message' => "",
                    );
                } else if ($data_post['action'] == "confirm") {
                    $retour = retour(false, "success", $data_retour, array("message" => "Prolème de l'action supprimer"));

                    $clause = array('enc_id' => $data_post['enc_id']);
                    $retour = retour(500, "error", $clause, array("message" => "Erreur sur l'action suppr"));
                    $data['enc_etat'] = 0;

                    $this->load->model('Encaissement_m');
                    $request = $this->Encaissement_m->save_data($data, $clause);

                    if (!empty($request)) {
                        $retour = retour(200, "success", $data_retour, array("message" => "Encaissement a été supprimer avec succès"));
                    }
                }
                echo json_encode($retour);
            }
        }
    }

    public function getPath()
    {
        $this->load->model('MandatNew_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'clause' => array('doc_encaissement_id ' => $data_post['id_doc'])
                );
                $this->load->model('DocumentEncaissement_m', 'document_enc');
                $request = $this->document_enc->get($params);
                echo json_encode($request);
            }
        }
    }

    function supprimerFile()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $clause = array('doc_encaissement_id' => $data_post['id_doc']);
                $this->load->model('DocumentEncaissement_m', 'document_enc');

                $retour = retour(500, "error", $clause, array("message" => "Erreur sur l'action supprimer"));
                // $request = $this->document_enc->delete_data($clause);
                $request = true;
                if (!empty($request)) {
                    $clause['cliLot_id'] = $data_post['cliLot_id'];
                    $retour = retour(200, "success", $clause, array("message" => "Fichier a été supprimer avec succès"));
                }
                echo json_encode($retour);
            }
        }
    }

    /****** *****/

    function finSaisi_encais()
    {
        if (is_ajax()) {
            if ($_POST) {
                $data_post = $_POST["data"];
                $data_retour = array(
                    "action" => $data_post['action'],
                    "cliLot_id" => $data_post['cliLot_id'],
                    "annee" => $data_post['annee'],
                );

                if ($data_post['action'] == "demande") {
                    $retour = array(
                        'status' => 200,
                        'action' => $data_post['action'],
                        'data' => array(
                            'cliLot_id' => $data_post['cliLot_id'],
                            'annee' => $data_post['annee'],
                            'title' => $this->lang->line('title_confirm_fin_saisi_encaissement'),
                            'text' => str_replace('@var_annee', $data_post['annee'], $this->lang->line('retour_confirm_fin_saisi_encaissement')),
                            'commentaire' => $this->lang->line('commentaire'),
                            'btnConfirm' => $this->lang->line('btn_confirm'),
                            'btnAnnuler' => $this->lang->line('btn_annuler'),
                        ),
                        'message' => "",
                    );
                } else if ($data_post['action'] == "confirm") {

                    $retour = array(
                        'status' => 200,
                        'type' => 'error',
                        'data' => array(
                            'cliLot_id' => $data_post['cliLot_id'],
                            'annee' => $data_post['annee'],
                            "message" => "Prolème de l'action fin de la saisie des encaissements"
                        ),
                    );

                    $data_enc = array(
                        'enc_montant' => 0,
                        'enc_date' => date('Y-m-d', strtotime($data_post['annee'] . '/12/31')),
                        'typeEnc_id' => 4,
                        'clilot_id' => $data_post['cliLot_id'],
                        'enc_etat' => 1,
                        'enc_control' => 1,
                        'enc_info' => $data_post['enc_info'],
                        'enc_date_cloture' => date('Y-m-d')
                    );

                    $this->load->model('Encaissement_m', 'encaissement');
                    $request_enc = $this->encaissement->save_data($data_enc);

                    if (!empty($request_enc)) {
                        $retour["type"] = "success";
                        $retour["data"]["message"] = "Saisir encaissement " . $data_post['annee'] . " cloturer avec succée";

                        $clause_cliLot = array(
                            'clause' => array(
                                "enc_etat" => 1,
                                'enc_control' => 0,
                                "clilot_id" => $data_post['cliLot_id'],
                                "DATE_FORMAT(enc_date, '%Y') =" => $data_post['annee'],
                            ),
                            'order_by_columns' => 'enc_date DESC',
                            'join' => array(
                                'c_type_encaissement' => 'c_type_encaissement.typeEnc_id = c_encaissement.typeEnc_id',
                            ),
                        );

                        $getEncaissement = $this->encaissement->get($clause_cliLot);

                        // Tache 
                        $this->load->model('Client_m', 'client');
                        $request_client = $this->client->get(
                            array(
                                'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                                'method' => 'row',
                                'join' => array(
                                    'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                                    'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                                ),
                                'join_orientation' => 'left'
                            )
                        );
                        $params_lot = array(
                            'clause' => array(
                                'cliLot_etat' => 1,
                                'cliLot_id' => $data_post['cliLot_id']
                            ),
                            'join' => array(
                                'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                            ),
                            'method' => 'row'
                        );
                        $this->load->model('ClientLot_m', 'lot');
                        $data['lot'] = $this->lot->get($params_lot);

                        $data_tache = array(
                            'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                            'theme_id' => (!empty($getEncaissement)) ? 33 : 32,
                            'tache_nom' => (!empty($getEncaissement)) ? " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que la saisie des encaissements " . $data_post['annee'] . " est terminée pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")" :
                                "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que aucun encaissement " . $data_post['annee'] . " à saisir pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",

                            'tache_texte' => (!empty($getEncaissement)) ? " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que la saisie des encaissements " . $data_post['annee'] . " est terminée pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")" :
                                "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " aucun encaissement " . $data_post['annee'] . " à saisir pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",

                            'tache_createur' => $request_client->util_prenom,
                            'tache_valide' => 0,
                            'tache_etat' => 1,
                            'tache_date_creation' => date('Y-m-d H:i:s'),
                            'tache_date_echeance' => date('Y-m-d H:i:s'),
                            'tache_prioritaire' => 2,
                            'type_tache_id' => 3,
                            'participant' => $request_client->util_id,
                            'tache_realise' => 0,
                        );

                        $this->load->model('Tache_m', 'tache');
                        $request_tache = $this->tache->save_data($data_tache);
                    }
                }
                echo json_encode($retour);
            }
        }
    }
}
