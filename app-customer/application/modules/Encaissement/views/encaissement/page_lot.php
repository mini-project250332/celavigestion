<?php $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1; ?>
<div class="contenair-title pt-2 pb-2">
    <div class="row m-0">
        <div class="col-md-6 px-0 d-flex justify-content-between pb-2">
            <div class="px-2 d-flex align-items-center">
                <i class="fas fa-file-alt"></i> &nbsp;&nbsp;
                <h5><?= $this->lang->line('list_encaissement'); ?></h5>
            </div>
            <div class="px-2">
                <select class="form-select filtre-date-encaissement" name="filtre-date-<?= $cliLot_id; ?>" id="filtre-date-<?= $cliLot_id; ?>">
                    <?php for ($i = intval($date + 1); $i > 2021; $i--) : ?>
                        <option value="<?= $i ?>" <?= $i == $date ? 'selected' : '' ?>><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
    </div>
</div>

<div id="liste-encaissement-lot_<?= $cliLot_id ?>"></div>

<script>
    $(document).ready(function() {
        getListeEncaissement(<?= $cliLot_id; ?>, <?= intval(date('Y')) ?>);
    });
</script>