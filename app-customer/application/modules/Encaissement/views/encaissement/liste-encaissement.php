<?php
$lists_encaissement = array_filter($encaissements, function ($encaissement) {
    return $encaissement->enc_control == 0;
});

$cloture_encaissement = array_filter($encaissements, function ($encaissement) {
    return $encaissement->enc_control == 1;
});
$cloture_encaissement = array_values($cloture_encaissement);
?>
<style>
    @keyframes blink {
        0% {
            opacity: 1;
        }

        50% {
            opacity: 0.5;
        }

        100% {
            opacity: 1;
        }
    }

    .blink {
        animation: blink 3s infinite;
    }
</style>

<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">

            <div class="w-100 pt-2">
                <div class="row m-0">
                    <div class="col px-0 text-end">
                        <?php if (empty($cloture_encaissement)) : ?>
                            <button data-id="<?= $cliLot_id; ?>" class="btn btn-primary mb-2 btn_ajoutEncaissement <?= (empty($cloture_encaissement)) ? 'blink' : ''; ?>" <?= (!empty($cloture_encaissement)) ? 'disabled' : ''; ?>>
                                <i class="fas fa-plus"></i>&nbsp;
                                <?= $this->lang->line('plus_encaissement') ?>
                            </button>
                        <?php endif ?>
                    </div>
                </div>

                <div class="row m-0">
                    <div class="col px-0">
                        <table class="table table-bordered table-hover">
                            <thead class="fs--1 text-white" style="background:#1F659E;">
                                <tr class="text-center">
                                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                        <?= $this->lang->line('date_encaissement') ?>
                                    </th>
                                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                        <?= $this->lang->line('type_encaissement') ?>
                                    </th>
                                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;" width="30%">
                                        <?= $this->lang->line('montant_encaissement') ?>
                                    </th>
                                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                        <?= $this->lang->line('fichier_encaissement') ?>
                                    </th>
                                    <th class="p-2 pb-3" style="border-right: 1px solid #ffff;">
                                        <?= $this->lang->line('titre-action') ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-200">

                                <?php if (!empty($lists_encaissement)) : ?>
                                    <?php foreach ($lists_encaissement as $key => $value) : ?>
                                        <tr class="align-middle">
                                            <td class="text-center">
                                                <?= date('d/m/Y', strtotime($value->enc_date)); ?>
                                            </td>
                                            <td class="text-center">
                                                <span>
                                                    <?= $value->typeEnc_libelle; ?>
                                                </span>
                                            </td>
                                            <td class="text-end">
                                                <span>
                                                    <?= $value->enc_montant . ' ' . '€'; ?>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="">
                                                    <?php if (!empty($value->files)) : ?>
                                                        <?php foreach ($value->files as $file) : ?>
                                                            <div class="d-flex align-items-center">
                                                                <i class="fas fa-cloud-download-alt text-info me-1" onclick="forceDownload('<?= base_url() . $file->doc_encaissement_path ?>')" data-url="<?= base_url($file->doc_encaissement_path) ?>" style="cursor: pointer;"></i>
                                                                <span onclick="apercuDocEncaissement(<?= $file->doc_encaissement_id; ?>);" style="width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;cursor: pointer;" class="badge badge-soft-info mt-1 py-1 fs--0 text-start doc_encaissement_<?= $file->doc_encaissement_id; ?>">
                                                                    <span style=""><?= $file->doc_encaissement_nom; ?></span>
                                                                </span>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                            <td class="text-center ">
                                                <div class="d-flex">

                                                    <?php
                                                    $disable_verification = '';
                                                    if ($value->enc_valide == 1) {
                                                        $disable_verification = 'disabled';
                                                    }
                                                    if (!empty($cloture_encaissement)) {
                                                        $disable_verification = 'disabled';
                                                    }
                                                    ?>
                                                    <button class="btn btn-sm btn-outline-info icon-btn me-1 btn-info-enc" type="button" data-info="<?= $value->enc_info; ?>" data-langVide="<?= $this->lang->line('sans_information') ?>" data-annuler="<?= $this->lang->line('button_annuler') ?>">
                                                        <span class="fas fa-info"></span>
                                                    </button>
                                                    <button data-id="<?= $value->enc_id; ?>" data-cli_lot_id="<?= $value->clilot_id; ?>" class="btn btn-sm btn-outline-primary icon-btn me-1 btn_editEncaissement" type="button" <?= $disable_verification; ?>>
                                                        <span class="fas fa-pen"></span>
                                                    </button>

                                                    <button class="btn btn-sm btn-outline-danger icon-btn btn_supprimeEncaissement" type="button" data-id="<?= $value->enc_id; ?>" data-cli_lot_id="<?= $value->clilot_id; ?>" <?= $disable_verification; ?>>
                                                        <span class="fas fa-trash-alt"></span>
                                                    </button>



                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <td class="text-center fw-bold" colspan="5">
                                        <i> <?= str_replace('@var_annee', $annee, $this->lang->line('aucune_encaissement')); ?> </i>
                                    </td>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <div class="py-3 text-center">

                            <?php if (empty($lists_encaissement) && empty($cloture_encaissement)) : ?>

                                <div>
                                    <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                                        <div class="bg-info me-3 icon-item">
                                            <span class="fas fa-info-circle text-white fs-3"></span>
                                        </div>
                                        <p class="mb-0 flex-1"><?= str_replace('@var_annee', $annee, $this->lang->line('info_cloture')); ?> </p>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <?php if (!empty($lists_encaissement) && empty($cloture_encaissement)) : ?>

                                <div>
                                    <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                                        <div class="bg-info me-3 icon-item">
                                            <span class="fas fa-info-circle text-white fs-3"></span>
                                        </div>
                                        <p class="mb-0 flex-1"><?= str_replace('@var_annee', $annee, $this->lang->line('info_cloture_2')); ?> </p>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <?php if (!empty($lists_encaissement) && empty($cloture_encaissement)) : ?>

                                <btn id="fin_saisi_encaissement" data-cliLot_id="<?= $cliLot_id; ?>" data-annee="<?= $annee ?>" class="btn btn-primary text-600 text-white">
                                    <?= str_replace('@var_annee', $annee, $this->lang->line('btn_fin_saisi_encaissement')); ?>
                                </btn>

                            <?php endif; ?>

                            <?php if (empty($lists_encaissement) && empty($cloture_encaissement)) : ?>
                                <btn id="fin_saisi_encaissement_2" class="btn btn-primary text-600 text-white" data-cliLot_id="<?= $cliLot_id; ?>" data-annee="<?= $annee ?>">
                                    <?= str_replace('@var_annee', $annee, $this->lang->line('btn_aucune_encaissement_saisi')); ?>
                                </btn>
                            <?php endif; ?>

                            <?php if (!empty($cloture_encaissement)) : ?>
                                <div>
                                    <div class="alert alert-info border-2 d-flex align-items-center" role="alert">
                                        <div class="bg-info me-3 icon-item">
                                            <span class="fas fa-info-circle text-white fs-3"></span>
                                        </div>
                                        <p class="mb-0 flex-1"><?= str_replace('@date_cloture', date('d/m/Y', strtotime($cloture_encaissement[0]->enc_date_cloture)), $this->lang->line('encaissement_cloture')); ?> </p>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

                <br>

                <div class="py-4">
                    <div class="div-client-conseil">
                        <div class="image-client-conseil">
                            <div class="image-silouette">
                                <img class="" src="<?= base_url('assets/images/photo4.jpg'); ?>">
                            </div>
                            <div class="text-conseil flex-grow-1">
                                <div class="contenu-conseil">

                                    <div class="bubble bubble-bottom-left">
                                        <span>
                                            <?= $this->lang->line('texte-conseil-encaissement'); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="splitter"></div>

    <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
        <div id="apercu_list_docEnc_<?= $cliLot_id ?>">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <?= $this->lang->line('apercu_doc'); ?>
                </div>
            </div>
            <div id="apercuEnc_<?= $cliLot_id ?>">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></button>
                </div>
            </div>
        </div>
    </div>

</div>