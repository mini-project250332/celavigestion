<style>
    @keyframes blink {
        0% {
            opacity: 1;
        }

        50% {
            opacity: 0.5;
        }

        100% {
            opacity: 1;
        }
    }

    span.badge-soft-info {
        max-width: 150px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .span_conten {
        display: flex;
        align-items: center;
    }

    .span_conten .fas {
        margin-right: 5px;
    }

    .blink {
        animation: blink 3s infinite;
    }
</style>

<input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_charge_<?= $cliLot_id; ?>">
<input type="hidden" value="<?= $annee; ?>" id="annee_charge_<?= $cliLot_id; ?>">

<div class="d-flex flex-column flex-xl-row h-100">
    <div class="col position-relative  h-100" style="overflow-y:auto;">
        <div class="d-flex flex-column pb-1">
            <div class="w-100 pt-2">

                <div class="row mt-3">
                    <div class="col"></div>
                    <div class="col-4 text-end">
                        <?php if (empty($cloture)) : ?>
                            <button type="button" class="btn btn-primary blink" data-bs-toggle="modal" data-bs-target="#ModalCharge_<?= $cliLot_id ?>" id="add_depense_<?= $cliLot_id ?>" <?= !empty($cloture) ? 'disabled' : "" ?>>
                                <i class="fas fa-plus"></i>
                                <?= $this->lang->line('ajout_depense') ?>
                            </button>
                        <?php endif ?>
                    </div>
                </div>
                <table class="table table-hover fs-0 mt-3">
                    <thead class="text-white" style="background:#2569C3;">
                        <tr>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('date_depense') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('type_depenses') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('montant_ht') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('montant_tva') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('montant_ttc') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('fichiers') ?></th>
                            <th class="p-2 text-center" style="border-right: 1px solid #ffff;">Actions</th>
                        </tr>
                    </thead>
                    <tbody class="bg-200">
                        <?php if (!empty($list_charge)) : ?>
                            <?php foreach ($list_charge as $key => $value) : ?>
                                <tr id="charge_row-<?= $value->charge_id ?>">
                                    <td class="text-center p-2" style="border-right: 1px solid #ffff; white-space: nowrap;"><?= date("d/m/Y", strtotime($value->date_facture)); ?></td>
                                    <td class="text-center p-2" style="border-right: 1px solid #ffff; "><?= $value->tpchrg_lot_nom ?></td>
                                    <td class="text-end p-2" style="border-right: 1px solid #ffff; white-space: nowrap;"><?= format_number_($value->montant_ht) . ' ' . '€' ?></td>
                                    <td class="text-end p-2" style="border-right: 1px solid #ffff; white-space: nowrap;"><?= format_number_($value->tva) . ' ' . '€' ?></td>
                                    <td class="text-end p-2" style="border-right: 1px solid #ffff; white-space: nowrap;"><?= format_number_($value->montant_ttc) . ' ' . '€' ?></td>
                                    <td class="p-2" style="border-right: 1px solid #ffff; white-space: nowrap;">
                                        <?php if (!empty($value->doc_charge)) : ?>
                                            <?php foreach ($value->doc_charge as $key => $value_charge) : ?>
                                                <div class="span_conten">
                                                    <i class="fas fa-cloud-download-alt text-info" onclick="forceDownload('<?= base_url() . $value_charge->doc_charge_path ?>')" data-url="<?= base_url($value_charge->doc_charge_path) ?>" style="cursor: pointer;"></i>
                                                    <span class="badge rounded-pill badge-soft-info" style="cursor: pointer;" onclick="apercuDocumentDepenses(<?= $value_charge->doc_charge_id ?>)">
                                                        <?= $value_charge->doc_charge_nom ?>
                                                    </span> <br>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center p-2" style="border-right: 1px solid #ffff; white-space: nowrap">
                                        <?php if ($value->charge_etat_valide == 1) : ?>
                                            <button class="btn btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="<?= $this->lang->line('mesg_depense_valide') ?>">
                                                <i class="fas fa-check"></i>
                                            </button>
                                        <?php else : ?>
                                            <button class="btn btn-sm btn-outline-info icon-btn ms-1 btn_info_dep" data-info="<?= $value->c_charge_commentaire ?>" data-langVide="<?= $this->lang->line('sans_information') ?>" data-annuler="<?= $this->lang->line('button_annuler') ?>">
                                                <svg class="svg-inline--fa fa-info fa-w-6" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="info" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z"></path>
                                                </svg><!-- <span class="fas fa-info"></span> -->
                                            </button>
                                            <button class="btn btn-sm btn-outline-primary only_visuel_gestion" id="modifierDepense_<?= $cliLot_id ?>" data-bs-toggle="modal" data-bs-target="#ModalCharge_<?= $cliLot_id ?>" data-charge_id="<?= $value->charge_id ?>" <?= !empty($cloture) ? 'disabled' : "" ?>>
                                                <i class="fas fa-pencil-alt"></i>
                                            </button>
                                            <button class="btn btn-sm btn-outline-danger only_visuel_gestion" id="supprCharge_<?= $cliLot_id ?>" data-charge_id="<?= $value->charge_id ?>" <?= !empty($cloture) ? 'disabled' : "" ?>>
                                                <i class="far fa-trash-alt"></i>
                                            </button>

                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php else : ?>
                            <tr>
                                <td class="text-center fw-bold" colspan="6">
                                    <i> <?= $this->lang->line('aucune_charge') ?> <?= $annee == 0 ? "antérieure" : $annee ?> . <?= $this->lang->line('message_ajout_depense') ?></i>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                    <?php if (!empty($list_charge)) : ?>
                        <tfoot class="bg-200">
                            <tr id="">
                                <td class="fw-bold text-center" style="border-right: 1px solid #ffff;" colspan="2">TOTAL</td>
                                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff; white-space: nowrap;">
                                    <?= format_number_($totat_ht) . ' ' . '€'; ?>
                                </td>
                                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff; white-space: nowrap;">
                                    <?= format_number_($total_tva) . ' ' . '€'; ?>
                                </td>
                                <td class="text-end p-2 fw-bold" style="border-right: 1px solid #ffff; white-space: nowrap;">
                                    <?= format_number_($total_ttc) . ' ' . '€'; ?>
                                </td>
                            </tr>
                        </tfoot>
                    <?php endif ?>
                </table>
                <div class="text-center mt-3">
                    <?php if (empty($cloture) && !empty($list_charge)) : ?>
                        <div class="alert alert-info border-2 d-flex align-items-center mt-2" role="alert">
                            <i><?= str_replace('@annee', $annee, $this->lang->line('mesg_avec_depense')) ?></i>
                        </div>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" id="fin_saisie_<?= $cliLot_id ?>" data-id="<?= isset($value->charge_id) ? $value->charge_id : "" ?>">
                            <i class="fas fa-check-circle"></i>
                            <?= $this->lang->line('fin_saisie_depense') ?> <?= $annee == 0 ? "antérieure" : $annee ?>
                        </button>
                    <?php elseif (!empty($cloture)) : ?>
                        <div class="alert alert-info border-2 d-flex align-items-center mt-2" role="alert">
                            <div class="bg-info me-3 icon-item"><span class="fas fa-exclamation-circle text-white fs-3"></span></div>
                            <p class="mb-0 flex-1">
                                <?= $this->lang->line('mesg_fin_saisie') . ' ' . date("d/m/Y", strtotime($cloture->charge_date_cloture)) ?> . <?= $this->lang->line('mesg_fin_saisie1') ?>
                            </p>
                        </div>
                    <?php else : ?>
                        <div class="alert alert-info border-2 d-flex align-items-center mt-2" role="alert">
                            <i><?= str_replace('@annee', $annee, $this->lang->line('mesg_sans_depense')) ?></i>
                        </div>
                        <button type="button" class="btn btn-secondary" id="fin_saisie_<?= $cliLot_id ?>" data-id="<?= isset($value->charge_id) ? $value->charge_id : "" ?>">
                            <i class="fas fa-exclamation-triangle"></i> &nbsp;
                            <?= $this->lang->line('aucun_saisie_depense') ?> <?= $annee == 0 ? "antérieure" : $annee ?>
                        </button>
                    <?php endif ?>
                </div>


                <div class="modal fade" id="ModalCharge_<?= $cliLot_id ?>">
                    <?php echo form_tag('Depenses/cruDepenses', array('method' => "post", 'class' => '', 'id' => 'cruDepenses_' . $cliLot_id)) ?>
                    <input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_charge_<?= $cliLot_id; ?>" name="cliLot_id">
                    <input type="hidden" value="<?= $annee; ?>" id="annee_charge_<?= $cliLot_id; ?>" name="annee">
                    <input type="hidden" value="<?= $langue; ?>" id="langue" name="langue">
                    <div class="modal-dialog  modal-lg  modal-dialog-centered">
                        <div class="modal-content">

                            <input type="hidden" id="charge_id" name="charge_id" value="">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title charge_modal"></h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="col">
                                    <div class="row">
                                        <div class="col-4">
                                            <label data-width="250"><?= $this->lang->line('type_depenses') ?> :</label>
                                        </div>
                                        <div class="col-8">
                                            <select class="form-select form-control" id="type_charge_id_<?= $cliLot_id ?>" name="type_charge_id">
                                                <?php foreach ($type_charge as $key => $value) : ?>
                                                    <option value="<?= $value->tpchrg_lot_id ?>">
                                                        <?= $value->tpchrg_lot_nom ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <label data-width="250"><?= $this->lang->line('date_depense') ?> :</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="date" class="form-control" id="date_facture_<?= $cliLot_id ?>" name="date_facture">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <label data-width="250"><?= $this->lang->line('montant_ttc') ?> :</label>
                                        </div>
                                        <div class="col-8">
                                            <div class="input-group">
                                                <input type="text" placeholder="0" class="form-control mb-0 input_charge chiffre" id="montant_charge_ttc_<?= $cliLot_id ?>" style="text-align: right;height: 36px;" name="montant_charge_ttc">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">€</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-4 mt-2">
                                            <label data-width="250"><?= $this->lang->line('fichiers') ?> :</label>
                                        </div>
                                        <div class="col-8">
                                            <div class="input-group">
                                                <input type="file" class="form-control m-1 w-50" id="file_<?= $cliLot_id ?>" name="doc_charge_path" multiple>

                                            </div>
                                            <div class="name_doc">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-4 mt-2">
                                            <label data-width="250"><?= $this->lang->line('form_comment') ?> :</label>
                                        </div>
                                        <div class="col-8">

                                            <div class="form-group">
                                                <textarea class="form-control" id="depense_comment_<?= $cliLot_id ?>" style="min-height:100px;" name="charge_commenatire"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover ">
                                                <thead class="text-white" style="background:#2569C3;">
                                                    <tr>
                                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('depense_montant_ht') ?>
                                                        </th>
                                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('depense_tva') ?></th>
                                                        <th class="p-2 text-center" style="border-right: 1px solid #ffff;"><?= $this->lang->line('depense_montant_ttc') ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="bg-200">
                                                    <tr>
                                                        <td>
                                                            <div class="input-group">
                                                                <input type="text" placeholder="0" class="form-control mb-0 input_charge auto_effect chiffre" id="charge_ht_<?= $cliLot_id ?>" style="text-align: right;height: 36px;" name="charge_ht">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text" id="basic-addon2">€</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="main-input-group">
                                                                <div class="input-group">
                                                                    <select id="charge_tva_taux_<?= $cliLot_id ?>" class="select_tva input_charge form-select mb-0 select auto_effect" name="charge_tva_taux">
                                                                        <?php foreach ($tva as $key => $item) : ?>
                                                                            <option value="<?php echo $item->tva_valeur; ?>" <?php if ($item->tva_valeur == "20")
                                                                                                                                    echo "selected"; ?>>
                                                                                <?php echo $item->tva_valeur; ?>
                                                                            </option>
                                                                        <?php endforeach ?>
                                                                        <option value="1">Autre</option>
                                                                    </select>
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text" id="basic-addon2">%</span>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group input-group-montant-tva">
                                                                    <input placeholder="0" type="text" value="0" class="input_charge chiffre form-control mb-0 auto_effect" id="charge_tva_<?= $cliLot_id ?>" name="charge_tva" style="text-align: right;height: 36px;"><br>
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text" id="basic-addon2">€</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="input-group">
                                                                <input type="text" placeholder="0" class="form-control chiffre mb-0 input_charge auto_effect" id="charge_ttc_<?= $cliLot_id ?>" name="charge_ttc" style="text-align: right;height: 36px;" value="0">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text" id="basic-addon2">€</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= $this->lang->line('button_annuler') ?></button>
                                <button type="submit" class="btn btn-primary" id="addCharge_<?= $cliLot_id; ?>"><?= $this->lang->line('button_enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="splitter"></div>

    <div class="col position-relative py-2 h-100" style="overflow-y:auto;">
        <div id="apercudocDepenses_<?= $cliLot_id ?>">
            <div id="nonApercu" style="display:none;">
                <div class="text-center text-danger pt-10">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"><?= $this->lang->line('apercu_doc') ?></i>
                </div>
            </div>
            <div id="apercuDepenses_<?= $cliLot_id ?>">
                <div id="bloc_controle" class="bloc_control mb-3" style="display:none">
                    <button class="zommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"> <i class="fa fa-search-plus" aria-hidden="true"></i> </button>
                    <button class="dezommer-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-search-minus" aria-hidden="true"></i></button>
                    <button class="orginal-apercu-list m-1 btn btn-outline-dark rounded fs-1"><i class="fa fa-retweet"></i></button>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- The Modal -->


<script>
    $(document).on('click', '#add_depense_<?= $cliLot_id ?>', function() {
        var langue = $('#langue').val();
        if (langue == 'english') {
            $('.charge_modal').text("Add an expense");
        } else {
            $('.charge_modal').text("Ajout d'une dépense");
        }

        $('#charge_id').val(''),
            $('#type_charge_id').val(1);
        $('#montant_charge_ttc').val('');
        $('#date_facture').val('');
        $('#charge_ht').val('');
        $('#charge_tva').val('');
        $('#charge_ttc').val('');
    });

    $(document).on('change', '#type_charge_id_<?= $cliLot_id ?>', function() {
        if ($('#type_charge_id_<?= $cliLot_id ?>').val() == 2 || $('#type_charge_id_<?= $cliLot_id ?>').val() == 10) {
            $('#charge_tva_taux_<?= $cliLot_id ?>').val(1);
            calculCharge_special($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        } else {
            $('#charge_tva_taux_<?= $cliLot_id ?>').val(0);
            calculCharge($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        }
    });

    $(document).on('keyup', '#montant_charge_ttc_<?= $cliLot_id ?>', function() {
        if ($('#charge_tva_taux_<?= $cliLot_id ?>').val() == 1) {
            calculCharge_special($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        } else {
            calculCharge($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        }
    });

    $(document).on('keyup', '#charge_tva_<?= $cliLot_id ?>', function() {
        calculCharge_special($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
    });

    $(document).on('change', '#charge_tva_taux_<?= $cliLot_id ?>', function() {
        if ($('#charge_tva_taux_<?= $cliLot_id ?>').val() == 1) {
            $('#charge_tva_taux_<?= $cliLot_id ?>').val(1);
            calculCharge_special($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        } else {
            calculCharge($('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
        }
    });

    $('#cruDepenses_<?= $cliLot_id ?>').one('submit', function(e) {
        var cliLotIdValue = $("#cliLot_id_charge_<?= $cliLot_id; ?>").val();
        e.preventDefault();
        var form = $(this);

        var formData = new FormData();
        var fileInput = $('#file_' + cliLotIdValue)[0];
        var selectedFiles = fileInput.files;
        for (var i = 0; i < selectedFiles.length; i++) {
            formData.append(i, selectedFiles[i]);
        }
        var other_data = $(this).serializeArray();
        $.each(other_data, function(key, input) {
            formData.append(input.name, input.value);
        });
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#ModalCharge_<?= $cliLot_id; ?>").modal('hide');
                $('.modal-backdrop').remove();
                getFicheDepensesLot($('#annee_charge_<?= $cliLot_id; ?>').val(), $('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
            }
        });
    });

    $(document).on('click', '#modifierDepense_<?= $cliLot_id ?>', function() {

        $('.charge_modal').text("Modifier une dépense");
        updateCharge($(this).attr('data-charge_id'), $('#cliLot_id_charge_<?= $cliLot_id; ?>').val());

    });

    $(document).on('click', '#supprCharge_<?= $cliLot_id ?>', function(e) {
        e.preventDefault();
        var data = {
            action: "demande",
            charge_id: $(this).attr('data-charge_id')
        }
        deleteCharges(data);
    });

    $(document).on('click', '#fin_saisie_<?= $cliLot_id ?>', function(e) {
        e.preventDefault();
        var data = {
            action: "demande",
            charge_id: $(this).attr('data-id'),
            annee: $('#annee_charges_<?= $cliLot_id ?>').val(),
            cliLot_id: <?= $cliLot_id ?>,
        }
        console.log(data);
        FinSaisieCharges(data);
    });

    $(document).ready(function() {
        $('[data-bs-toggle="tooltip"]').tooltip();
    });
</script>