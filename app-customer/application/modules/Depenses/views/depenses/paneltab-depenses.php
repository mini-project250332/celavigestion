<input type="hidden" value="<?= $cliLot_id; ?>" id="cliLot_id_charge_<?= $cliLot_id; ?>">
<?php $date = date('Y-m-d', strtotime(date('Y') . '-04-01')) <= date('Y-m-d') ? date('Y') : date('Y') - 1; ?>

<div class="contenair-title pt-2 pb-2">
    <div class="row m-0">
        <div class="col-md-6 px-0 d-flex justify-content-between pb-2">
            <div class="px-2 d-flex align-items-center">
                <h5 class="fs-1"><?= $this->lang->line('mes_depenses') ?></h5>
            </div>
            <div class="px-2">
                <select class="no_effect form-select fs--1 m-1" id="annee_charges_<?= $cliLot_id; ?>" style="width: 96%">
                    <?php for ($i = intval(date("Y")); $i >= intval(date("Y")) - 1; $i--) : ?>
                        <option value="<?= $i ?>" <?= ($i == $date) ? "selected" : "" ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor ?>
                    <option value="0"><?= $this->lang->line('annee_anterieure') ?></option>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="flex-grow-1" id="fiche-depenses-lot_<?= $cliLot_id ?>"></div>

<script>
    $(document).ready(function() {
        getFicheDepensesLot($('#annee_charges_<?= $cliLot_id; ?>').val(), <?= $cliLot_id; ?>);
    });

    $(document).on('change', '#annee_charges_<?= $cliLot_id; ?>', function() {
        getFicheDepensesLot($(this).val(), $('#cliLot_id_charge_<?= $cliLot_id; ?>').val());
    })
</script>