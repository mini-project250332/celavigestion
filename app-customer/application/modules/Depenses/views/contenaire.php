<div class="container-app">
    <?php $this->load->view($sous_module); ?>
</div>


<div class="modal fade" id="modal-finSaisiEncaissement" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" id="modal-main-finSaisiEncaissement">

            <div class="modal-header">
                <div class="rounded-top-lg ps-4 pe-6">
                    <h5 class="mb-1 text-center-"  id="titre-modal-finSaisiEncaissement"></h5>
                </div>
            </div>

            <div class="modal-body p-0 pb-3">
                <div class="row m-0 ">
                    <div class="col-12 px-4 pt-4 pb-2 text-center">
                        <span id="text-modal-finSaisiEncaissement"></span>
                    </div>
                    <input type="hidden" id="confirm-cliLot_id">
                    <input type="hidden" id="confirm-charge_id">
                    <div class="col-12">
                        <div class="px-3">
                            <label id="text-champ-commentaire" class="col-auto ps-2"></label>
                        </div>
                        <div class="form-group inline">
                            <div class="row m-0">
                                <div class="col">
                                    <textarea id="confirm-enc_info" name="enc_info" style="min-height: 100px;" class="form-control"></textarea>
                                </div>
                                <div class="help"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer py-2">
                <button type="button" id="btnConfirm-finSaisiEncaissement" class="btn btn-success d-flex">
                    Confirmer
                </button>
                <button type="button" id="btnAnnuler-finSaisiEncaissement" class="btn btn-secondary d-flex" data-bs-dismiss="modal">
                    Annuler
                </button>
            </div>

        </div>
    </div>
</div>