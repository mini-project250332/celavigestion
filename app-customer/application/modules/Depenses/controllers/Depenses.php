<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Depenses extends CLI_Controller
{
    protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "Depenses";

    protected $_const_js = array(
        "timer" => 10
    );

    public function __construct()
    {
        parent::__construct();
        $this->_script = array(
            "js/depenses/depenses.js",
        );
        $this->_css_personnaliser = array(
            "css/depenses/depenses.css"
        );

        $this->lang->load('menu', $_SESSION['session_utilisateur_client']['lang']);
    }

    public function index()
    {
        $this->_data['menu_principale'] = "depenses";
        $this->_data['sous_module'] = ($_SESSION['session_utilisateur_client']['check_identite'] == 1) ? "depenses/contenaire-depenses" : 'page_notCIN';
        $this->render('contenaire');
    }

    public function getDepenses()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('ClientLot_m', 'lot');

                $params = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cli_id' => $_SESSION['session_utilisateur_client']['client_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),
                    'method' => "result",
                );
                $data['client_lot'] = $this->lot->get($params);
                $this->renderComponant("Depenses/depenses/depenses-page", $data);
            }
        }
    }

    public function ficheDepenses()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Charges_m');
                $this->load->model('TypeChargeIntranet_m');
                $this->load->model('TypeChargeLot_m');
                $this->load->model('TypeChargeLotenglish_m');
                $this->load->model('TauxTVA_m');
                $this->load->model('DocumentCharges_m');
                $data_post = $_POST["data"];

                $langue = $_SESSION['session_utilisateur_client']['lang'];
                $data['langue'] = $langue;

                if ($langue == "french") {
                    $param = array(
                        'clause' => array('annee' => $data_post['annee'], 'c_charge.cliLot_id' => $data_post['cliLot_id'], 'charge_cloture' => 0),
                        'join' => array(
                            'c_type_charge_lot' => 'c_type_charge_lot.tpchrg_lot_id  = c_charge.type_charge_id',
                        ),
                        'join_orientation' => 'left'
                    );
                } else {
                    $param = array(
                        'clause' => array('annee' => $data_post['annee'], 'c_charge.cliLot_id' => $data_post['cliLot_id'], 'charge_cloture' => 0),
                        'join' => array(
                            'c_type_charge_lot_en' => 'c_type_charge_lot_en.tpchrg_lot_id  = c_charge.type_charge_id',
                        ),
                        'join_orientation' => 'left'
                    );
                }

                $param_cloture = array(
                    'clause' => array('annee' => $data_post['annee'], 'c_charge.cliLot_id' => $data_post['cliLot_id'], 'charge_cloture' => 1),
                    'method' => 'row',

                );

                $data['cloture'] = $list_charge_cloture = $this->Charges_m->get($param_cloture);

                $data['list_charge'] = $list_charge = $this->Charges_m->get($param);

                foreach ($list_charge as $key => $value_charge) {
                    $params_doc = array(
                        'clause' => array(
                            'annee' => $data_post['annee'],
                            'c_charge.cliLot_id' => $data_post['cliLot_id'],
                            'c_charge.charge_id' => $value_charge->charge_id,
                            'doc_charge_etat' => 1
                        ),

                        'join' => array(
                            'c_charge' => 'c_document_charge.charge_id  = c_charge.charge_id'
                        ),
                        'join_orientation' => 'left'
                    );

                    $list_charge[$key]->doc_charge = $this->DocumentCharges_m->get($params_doc);
                }


                $data['cliLot_id'] = $data_post['cliLot_id'];
                $data['annee'] = $data_post['annee'];
                $totat_ht = 0;
                $total_tva = 0;
                $total_ttc = 0;
                foreach ($list_charge as $key => $value) {
                    $totat_ht += $value->montant_ht;
                    $total_tva += $value->tva;
                    $total_ttc += $value->montant_ttc;
                }

                $data['totat_ht'] = $totat_ht;
                $data['total_tva'] = $total_tva;
                $data['total_ttc'] = $total_ttc;

                // $data['type_charge'] = $this->TypeChargeIntranet_m->get(array());

                $langueModeleMap = array(
                    'french' => 'TypeChargeLot_m',
                    'english' => 'TypeChargeLotenglish_m',
                );


                if (array_key_exists($langue, $langueModeleMap)) {
                    $modele = $this->{$langueModeleMap[$langue]};
                    $data['type_charge'] = $modele->get(array());
                } else {
                    $modele = $this->{$langueModeleMap['french']};
                    $data['type_charge'] = $modele->get(array());
                }


                $data['tva'] = $this->TauxTVA_m->get(array());

                echo $data_post['cliLot_id'] . "@@@@@@@";
                $this->renderComponant("Depenses/depenses/fiche-depenses-page", $data);
            }
        }
    }

    public function cruDepenses()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('Charges_m');
                $this->load->model('DocumentCharges_m');
                $this->load->model('ClientLot_m', 'lot');
                $this->load->model('Client_m', 'client');
                $this->load->model('Tache_m', 'tache');

                $data_retour = array("cliLot_id" => $_POST['cliLot_id']);
                $retour = array(
                    'status' => 500,
                    'data' => [],
                    'message' => $this->lang->line('info_enreg_erreur'),
                );

                $data_charge = array(
                    'cliLot_id' => $_POST['cliLot_id'],
                    'annee' => $_POST['annee'],
                    'date_facture' => $_POST['date_facture'],
                    'montant_ht' => $_POST['charge_ht'],
                    'montant_ttc' => $_POST['charge_ttc'],
                    'tva' => $_POST['charge_tva'],
                    'tva_taux' => $_POST['charge_tva_taux'],
                    'type_charge_id' => $_POST['type_charge_id'],
                    'charge_date_saisie' => date('Y-m-d'),
                    'prenom_util' => $_SESSION['session_utilisateur_client']['client_prenom'],
                    'c_charge_commentaire' => $_POST['charge_commenatire']
                );

                $clause = null;

                if ($_POST['charge_id'] != '') {
                    $clause = array("charge_id" => $_POST['charge_id']);
                }

                $request = $this->Charges_m->save_data($data_charge, $clause);

                $insert_id = $this->db->insert_id();


                foreach ($_FILES as $key => $value) {
                    $annee = $_POST['annee'];
                    $cliLot_id = $_POST['cliLot_id'];
                    $targetDir = "../documents/clients/lots/depenses/$annee/$cliLot_id";
                    $temp_name = $value['tmp_name'];
                    $data_file['doc_charge_nom'] = $value['name'];
                    $data_file['doc_charge_creation'] = date('Y-m-d H:i:s');
                    $data_file['doc_charge_etat'] = 1;
                    $data_file['doc_charge_traite'] = 0;
                    $data_file['doc_charge_annee'] = $annee;
                    $data_file['cliLot_id'] = $_POST['cliLot_id'];
                    $data_file['util_client'] = $_SESSION['session_utilisateur_client']['client_id'];
                    $data_file['charge_id'] = $_POST['charge_id'] != '' ? $_POST['charge_id'] : $insert_id;
                    $data_file['doc_charge_path'] = str_replace('\\', '/', $targetDir . DIRECTORY_SEPARATOR . $data_file['doc_charge_nom']);
                    $this->makeDirPath($targetDir);
                    if (!file_exists($data_file['doc_charge_path'])) {
                        move_uploaded_file($temp_name, $data_file['doc_charge_path']);
                    }

                    $request = $this->DocumentCharges_m->save_data($data_file);
                }

                $request_client = $this->client->get(
                    array(
                        'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                        'method' => 'row',
                        'join' => array(
                            'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                            'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                        ),
                        'join_orientation' => 'left'
                    )
                );

                $params_lot = array(
                    'clause' => array(
                        'cliLot_etat' => 1,
                        'cliLot_id' => $_POST['cliLot_id']
                    ),
                    'join' => array(
                        'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                    ),

                    'method' => 'row'
                );

                $data['lot'] = $this->lot->get($params_lot);

                $params_tache = $this->tache->get(
                    array(
                        'clause' => array(
                            'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                            'theme_id' => 28,
                            'tache_nom' => 'Saisie de dépenses pour le lot' . ' ' . $data['lot']->progm_nom,
                            'tache_createur' => $request_client->util_prenom,
                            'tache_valide' => 0,
                            'tache_etat' => 1
                        ),
                        'method' => 'row',

                    )
                );

                $data_tache = array(
                    'tache_nom' => 'Saisie de dépenses pour le lot' . ' ' . $data['lot']->progm_nom,
                    'tache_date_creation' => date('Y-m-d H:i:s'),
                    'tache_date_echeance' => date('Y-m-d H:i:s'),
                    'tache_prioritaire' => 2,
                    'tache_texte' => "Le propriétaire a modifié ses documents et/ou des informations des dépenses sur l'Extranet. Veuillez consulter la page du lot puis l'onglet dépenses pour voir les modifications",
                    'tache_valide' => 0,
                    'tache_createur' => $request_client->util_prenom,
                    'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                    'tache_etat' => 1,
                    'theme_id' => 28,
                    'type_tache_id' => 3,
                    'participant' => $request_client->util_id
                );

                if (empty($params_tache)) {
                    $request_tache = $this->tache->save_data($data_tache);
                }

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => (empty($charge_id)) ? $this->lang->line('info_enreg') : $this->lang->line('info_enreg_modif')));
                }
                echo json_encode($retour);
            }
        }
    }

    public function deleteCharges()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la suppression…",
            );

            $this->load->model('Charges_m');

            $data = decrypt_service($_POST["data"]);

            $params = array(
                'clause' => array('charge_id' => $data['charge_id']),
                'method' => "row",
            );
            $request_get = $this->Charges_m->get($params);

            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'charge_id' => $data['charge_id'],
                        'title' => "Confirmation de la suppression",
                        'text' => "Voulez-vous vraiment supprimer cette Dépense ?",
                        'btnConfirm' => "Supprimer",
                        'btnAnnuler' => "Annuler"
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                $clause = array('charge_id' => $data['charge_id']);
                $request = $this->Charges_m->delete_data($clause);
                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'charge_id' => $data['charge_id'],
                        ),
                        'message' => "Charge supprimée avec succès",
                    );
                }
            }

            $retour['charge_info'] = $request_get;
            echo json_encode($retour);
        }
    }

    public function getPathDepenses()
    {

        $this->load->model('DocumentCharges_m');
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST['data'];
                $params = array(
                    'columns' => array('doc_charge_id', 'doc_charge_path', 'cliLot_id'),
                    'clause' => array('doc_charge_id' => $data_post['id_doc'])
                );

                $request = $this->DocumentCharges_m->get($params);
                echo json_encode($request);
            }
        }
    }

    public function getUpdateCharge()
    {
        $this->load->model('Charges_m');
        $this->load->model('DocumentCharges_m');

        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $data_post = $_POST["data"];

                $data['charge_id'] = $data_post['charge_id'];

                $request_progrmme_charge = $this->Charges_m->get(
                    array(
                        'clause' => array('charge_id' => $data_post['charge_id']),
                        'method' => 'row',
                    )
                );

                $request_doc_charge = $this->DocumentCharges_m->get(
                    array(
                        'clause' => array('charge_id' => $data_post['charge_id'], 'doc_charge_etat' => 1),
                    )
                );

                $data_retour = $request_progrmme_charge;
                $data_retour_doc = $request_doc_charge;

                $retour = retour(true, "success", $data_retour, $data_retour_doc, array("message" => "Info Charge"));

                echo json_encode($retour);
            }
        }
    }

    public function removefile()
    {
        if (is_ajax()) {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $this->load->model('DocumentCharges_m');
                $data_retour = array("doc_charge_id" => $_POST['data']['doc_charge_id']);
                $data = array(
                    'doc_charge_etat' => 0,
                );
                $clause = array("doc_charge_id" => $_POST['data']['doc_charge_id']);
                $request = $this->DocumentCharges_m->save_data($data, $clause);

                if (!empty($request)) {
                    $retour = retour(true, "success", $data_retour, array("message" => $this->lang->line('delete_file')));
                }
                echo json_encode($retour);
            }
        }
    }

    public function FinSaisieCharges()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $retour = array(
                'status' => 500,
                'data' => [],
                'message' => "Des erreurs ont été détectées lors de la validation",
            );

            $this->load->model('Charges_m');

            $data = decrypt_service($_POST["data"]);

            $params = array(
                'clause' => array('charge_id' => $data['charge_id']),
                'method' => "row",
            );
            $request_get = $this->Charges_m->get($params);

            if ($data['action'] == "demande") {
                $retour = array(
                    'status' => 200,
                    'action' => $data['action'],
                    'data' => array(
                        'charge_id' => $data['charge_id'],
                        'cliLot_id' => $data['cliLot_id'],
                        'annee' => $data['annee'],
                        'title' => "Confirmation",
                        'text' => $this->lang->line('confirm_fin_depense') . ' ' . $data['annee'] . ' ' . '?',
                        'commentaire' => $this->lang->line('form_comment') . ' :',
                        'btnConfirm' => $this->lang->line('confirmation'),
                        'btnAnnuler' => $this->lang->line('annuler')
                    ),
                    'message' => "",
                );
            } else if ($data['action'] == "confirm") {
                //  $clause = array('annee' => $request_get->annee, 'cliLot_id' => $request_get->cliLot_id);
                $data_charge = array(
                    'cliLot_id' => $data['cliLot_id'],
                    'annee' => $data['annee'],
                    'date_facture' => '',
                    'montant_ht' => '',
                    'montant_ttc' => '',
                    'tva' => '',
                    'tva_taux' => '',
                    'type_charge_id' => 1,
                    'charge_date_saisie' => date('Y-m-d'),
                    'charge_cloture' => 1,
                    'charge_date_cloture' => date('Y-m-d'),
                    'prenom_util' => $_SESSION['session_utilisateur_client']['client_prenom'],
                    'c_charge_commentaire' => $data['enc_info'],
                );


                $data_update = array('charge_cloture' => 1, 'charge_date_cloture' => date('Y-m-d'));
                $request = $this->Charges_m->save_data($data_charge);


                if ($request) {
                    $retour = array(
                        'status' => 200,
                        'action' => $data['action'],
                        'data' => array(
                            'cliLot_id' => $data['cliLot_id'],
                            'annee' => $data['annee'],
                        ),
                        'message' => "Confirmation réussie",
                    );





                    $clause_cliLot = array(
                        'clause' => array(
                            'charge_cloture' => 0,
                            "clilot_id" => $data['cliLot_id'],
                            "annee" => $data['annee'],
                        ),
                    );

                    $getDepense = $this->Charges_m->get($clause_cliLot);


                    // Tache 
                    $this->load->model('Client_m', 'client');
                    $request_client = $this->client->get(
                        array(
                            'clause' => array('c_client.client_id' => $_SESSION['session_utilisateur_client']['client_id']),
                            'method' => 'row',
                            'join' => array(
                                'c_dossier' => 'c_client.client_id = c_dossier.client_id',
                                'c_utilisateur' => 'c_utilisateur.util_id = c_dossier.util_id',
                            ),
                            'join_orientation' => 'left'
                        )
                    );
                    $params_lot = array(
                        'clause' => array(
                            'cliLot_etat' => 1,
                            'cliLot_id' => $data['cliLot_id']
                        ),
                        'join' => array(
                            'c_programme' => 'c_programme.progm_id = c_lot_client.progm_id',
                        ),
                        'method' => 'row'
                    );


                    $this->load->model('ClientLot_m', 'lot');
                    $data['lot'] = $this->lot->get($params_lot);

                    $data_tache = array(
                        'client_id' => $_SESSION['session_utilisateur_client']['client_id'],
                        'theme_id' => (!empty($getDepense)) ? 36 : 35,
                        'tache_nom' => (!empty($getDepense)) ? " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que la saisie des dépenses " . $data['annee'] . " est terminée pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")" :
                            "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que aucune dépense " . $data['annee'] . " à saisir pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",

                        'tache_texte' => (!empty($getDepense)) ? " Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " que la saisie des dépenses " . $data['annee'] . " est terminée pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")" :
                            "Déclaration par le propriétaire " . $request_client->client_nom . " " . $request_client->client_prenom . " aucun dépense " . $data['annee'] . " à saisir pour le lot " . $data['lot']->progm_nom . " (" . $data['lot']->progm_id . ")",

                        'tache_createur' => $request_client->util_prenom,
                        'tache_valide' => 0,
                        'tache_etat' => 1,
                        'tache_date_creation' => date('Y-m-d H:i:s'),
                        'tache_date_echeance' => date('Y-m-d H:i:s'),
                        'tache_prioritaire' => 2,
                        'type_tache_id' => 3,
                        'participant' => $request_client->util_id,
                        'tache_realise' => 0,
                    );

                    $this->load->model('Tache_m', 'tache');
                    $request_tache = $this->tache->save_data($data_tache);

                    // if ($request_tache) {
                    //     // La requête a été exécutée avec succès
                    //     echo "La tâche a été sauvegardée avec succès.";
                    // } else {
                    //     // Il y a eu une erreur lors de l'exécution de la requête
                    //     echo "Une erreur s'est produite lors de la sauvegarde de la tâche.";
                    // }

                }
            }

            // $retour['charge_info'] = $request_get;
            echo json_encode($retour);
        }
    }
}
