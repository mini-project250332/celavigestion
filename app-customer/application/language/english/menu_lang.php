<?php
//Menu
$lang['acceuil'] = "Home";
$lang['information'] = "Informations";
$lang['identity'] = "Identity";
$lang['contact'] = "To contact you";
$lang['adresses'] = "Addresses";
$lang['logements'] = "Your properties";
$lang['logements_list'] = "Properties list";
$lang['taxe_f'] = "Property tax";
$lang['compte_gere'] = "Manage your account";
$lang['deconnecter'] = "Log out";

//Identité
$lang['identite_prop'] = 'Your personal data';
$lang['identite_titre'] = 'Your address';
$lang['nom'] = 'Name';
$lang['prenom'] = 'Surname';
$lang['entreprise'] = 'Company';
$lang['nationalite'] = 'Nationality';
$lang['situation_matrimonile'] = 'Marital status';
$lang['marie'] = 'Married';
$lang['pacse'] = 'Civil partnership';
$lang['celibataire'] = 'Single';
$lang['residence_fiscale'] = 'Tax residence';
$lang['date_fin_validite'] = 'Exp date';
$lang['cni'] = 'Passport';
$lang['suivi_par'] = 'Your customer care';

//Contacts
$lang['email_1'] = 'Email 1';
$lang['email_2'] = 'Email 2';
$lang['phone_1'] = 'Phone 1';
$lang['phone_2'] = 'Phone 2';

//Bouton
$lang['modifier'] = 'Modify';
$lang['terminer'] = 'Save';
$lang['annuler'] = 'Cancel';

//Mot de passe
$lang['mdp'] = 'Password';
$lang['ancien_mdp'] = 'Previous password';
$lang['nouveau_mdp'] = 'New password';
$lang['confirm_mdp'] = 'Confirm New Password';
$lang['modifier_mdp'] = 'Change Password';
$lang['saisi_ancien_mdp'] = 'Enter old password';
$lang['saisi_nouv_mdp'] = 'Enter the new password';
$lang['confirm_nouv_mdp'] = 'Confirm new password';

//Dossier
$lang['dossier'] = 'Folder';
$lang['liste_dossier'] = 'List of files';
$lang['proprietaire'] = 'Property owners';
$lang['nb_lot'] = 'Number of lots';
$lang['lot'] = 'Lots';
$lang['sie'] = 'Tax center';
$lang['siret'] = 'Identification number';
$lang['regime'] = 'French tax status';
$lang['aucun_dossier'] = 'No files for this owner';
$lang['rcs'] = 'RCS';
$lang['ape'] = 'APE';
$lang['tva_intracommunautaire'] = 'VAT number';
$lang['cloture'] = 'Closure';
$lang['liasse_fiscale'] = 'Your tax returns';
$lang['decla_tva'] = 'Your VAT returns';
$lang['fiscalite'] = 'Your tax status';
$lang['alert_doc_fiscal'] = 'No documents found for your tax returns';
$lang['alert_doc_tva'] = 'No document available or this return does not apply to your tax status';
$lang['annee'] = 'Years';
$lang['documents'] = 'Documents';


//adresse logement and client
$lang['adresse_client'] = 'Owner\'s addresses';
$lang['adresse_log'] = 'Your property';
$lang['adress_title_client'] = 'Owner\'s address';
$lang['adress_title_lot'] = 'Owner\'s address';
$lang['aucun_lot'] = 'No apartments for this owner';
$lang['pgrm'] = 'Name of the residence :';
$lang['adresses_1'] = "Address 1 :";
$lang['adresses_2'] = "Address 2 :";
$lang['adresses_3'] = "Address 3 :";
$lang['code_postal'] = "Post code :";
$lang['ville'] = "Town :";
$lang['pays'] = "Country :";

//Menu logement
$lang['mandat'] = "Mandate";
$lang['acte'] = "Deed/Acquisition";
$lang['bail'] = "Lease contract";
$lang['encaissement'] = "Rent collection";
$lang['depenses'] = "Expenses";
$lang['emprunts'] = "Mortgages";

// liste de lot
$lang['liste_log'] = "Properties list";
$lang['nom_pgm'] = "Program name";
$lang['num_dossier'] = "File number";
$lang['ville_list_log'] = "City";
$lang['lot_pricpal'] = "Main apartment";
$lang['type_lot'] = "Lot type";
$lang['nom_gestionnaire'] = "Manager's name";
$lang['title_adresse_log'] = 'Your property';
$lang['title_adresse_information'] = 'Property information';
$lang['type_bail'] = 'Type of residence :';
$lang['typologie'] = 'Lease :';
$lang['prestation_souscrite'] = 'Celavi services:';
$lang['activite'] = 'Activity :';
$lang['defiscalisation'] = 'Tax break :';
$lang['defiscalisation_fin'] = 'End tax break :';
$lang['remboursement_rom'] = 'House hold refuse tax :';
$lang['comptabilite'] = 'Chartered accountant :';
$lang['num_lot'] = 'Unit number :';
$lang['num_parking'] = 'Parking number :';
$lang['title_info_payement_loyer'] = "Rental payments";
$lang['encais_loyer'] = 'Rent collection :';
$lang['mod_payement'] = 'Rental payment :';
$lang['rib'] = 'Bank details :';
$lang['tva'] = 'VAT :';
$lang['factu_loyer'] = 'Rent invoice :';
$lang['factu_charge'] = 'Invoice for expenses:';
$lang['factu_tom'] = 'Invoice for refuse tax :';
$lang['title_adresse_manager_syndic'] = "Management company and Syndic";
$lang['gestionnaire'] = 'Management company :';
$lang['syndic'] = 'Syndic :';
$lang['btn_modfi_rib'] = 'Change RIB';
$lang['no_file'] = 'No such file';
$lang['iban'] = 'Account number';
$lang['bic'] = 'BIC';

//Mandat
$lang['aucun_mandat'] = 'No mandate for this lot';
$lang['aucun_facture'] = 'No invoice';
$lang['list_mandat'] = 'List of mandates';
$lang['cree_le'] = 'Signed';
$lang['montant_ht'] = 'Amount';
$lang['titre_mandat'] = 'Mandate signed with Celavigestion';
$lang['texte-conseil-clientMandat'] = 'Download your mandate signed with Celavigestion.';
$lang['vos_facturecelav'] = 'Your invoices CELAVIGESTION';
$lang['list_facture'] = 'List of invoice(s)';

//commun 
$lang['depose_par'] = 'Registered by';
$lang['le'] = 'on';
$lang['telecharger'] = 'Download';
$lang['voir_document'] = 'View document';
$lang['apercu_doc'] = 'Preview not available for this file type file';

//Acte
$lang['aucun_doc'] = 'No documents for this lot';
$lang['list_acte'] = 'List of documents';
$lang['information_acte'] = 'Information about your property purchase';
$lang['info_general_acte'] = 'General information about the acquisition';
$lang['type_acquisition'] = 'Acquisition value';
$lang['date_acte_notarie'] = 'Date of the deed';
$lang['montant_acquisition'] = 'Purchase value';
$lang['valeur_acquisition'] = 'Acquisition value';
$lang['frais_acquisition'] = 'Acquisition costs';
$lang['valeur_terrain'] = 'Land value :';
$lang['valeur_construction'] = 'Construction value ';
$lang['valeur_mobilier'] = 'Furniture value ';
$lang['texte-conseil-clientActe'] = 'The acquisition value can be found in your deed.';
$lang['texte-conseil-clientActe1'] = 'The value of the land is calculated according to references linked to land values by geographical area.';

//Bail
$lang['list_bail'] = 'List of available documents';
$lang['bail_en_cours'] = 'Current Lease';
$lang['tacite_en cours'] = 'Tacit lease from';
$lang['aucun_bail'] = 'lease not validated for this lot';
$lang['bail_en_avenat_cours'] = 'Lease Current amendment from';
$lang['bail_en_avenat_cours_en_tacite'] = 'Lease Endorsement in tacit from';
$lang['bail_en_cours_en_tacite'] = 'Tacit lease from';
$lang['valide'] = 'Validated';
$lang['non_valide'] = 'Not validated';
$lang['preneur'] = 'Management company:';
$lang['exploitant'] = 'Management company :';
$lang['nature_prise_deffet'] = 'Start of the lease :';
$lang['a_la_livraison'] = 'on delivery';
$lang['date_sign_bail'] = 'date signature lease';
$lang['date_sign_acte'] = 'date signature deed';
$lang['suite_prec_gestionnaire'] = 'following a previous manager';
$lang['duree'] = 'Duration :';
$lang['an'] = 'year';
$lang['ans'] = 'years';
$lang['et'] = 'and';
$lang['mois'] = 'month';
$lang['tacite_prolongation'] = 'Prolongation :';
$lang['oui'] = 'Yes';
$lang['non'] = 'No';
$lang['type_bail'] = 'Type of lease :';
$lang['commercial_meuble'] = 'furnished commercial';
$lang['commercial_non_meuble'] = 'unfurnished commercial';
$lang['mandat_de_gestion'] = 'management mandate';
$lang['saisonnier'] = 'seasonal';
$lang['autre'] = 'other';
$lang['date_premier_facturation_par_celavi'] = 'Date of first Invoicing by Celavigestion :';
$lang['date_prise_effet'] = 'Date of start of the lease :';
$lang['date_fin'] = 'Date of end of the lease :';
$lang['resiliation_trienale'] = 'Possibilty to end every 3 years :';
$lang['franchise_loyer'] = 'Rental holyday period';
$lang['aucun_franchise'] = 'No Rent Excess for this lease';
$lang['debut'] = 'Beginning';
$lang['fin'] = 'End';
$lang['libelle_facture'] = 'Wording on invoice';
$lang['type_deduction'] = 'Type of deduction';
$lang['ttc_deduit'] = 'Tax deducted';
$lang['pourcentage'] = 'Percentage';
$lang['information_loyer'] = 'Information about rentals';
$lang['periodicite'] = 'Rent payment :';
$lang['mensuelle'] = 'Monthly';
$lang['trim_civile'] = 'Civil quarterly';
$lang['semestrielle'] = 'Semi-annual';
$lang['annulle'] = 'Annual';
$lang['trim_decale'] = 'Quarterly staggered';
$lang['periode_indexation'] = 'Indexation period :';
$lang['un_an'] = '1 year';
$lang['trois_an'] = '3 years';
$lang['non_applicable'] = 'Not applicable';
$lang['date_premier_indexation'] = 'Date of first indexation :';
$lang['indice_utilise'] = 'Indices used :';
$lang['nature_loyer'] = 'Nature of rent :';
$lang['fixe'] = 'Fixed';
$lang['fixe_variable'] = 'Fixed and Variable';
$lang['variable'] = 'Variable';
$lang['forfait_charge_deductible'] = 'Deductible indexed charges package :';
$lang['date_paiement'] = 'Date of paiement :';
$lang['du_mois'] = 'of the month';
$lang['moment_paiement'] = 'Period of payment :';
$lang['echu'] = 'expired';
$lang['echoir'] = 'drying rack';
$lang['date_premier_indexation_par_celavigestion'] = 'First indexation <br> by Celavigestion :';
$lang['indice_reference'] = 'Benchmarks :';
$lang['indexation_loyer'] = 'Rent indexation';
$lang['variation_plafonne'] = 'Capped variation :';
$lang['augmentation_min'] = 'Minimum increase :';
$lang['augmentation_capee'] = 'Capped increase :';
$lang['indice_plafonnement'] = 'Capping index :';
$lang['aucun'] = 'None';
$lang['indice'] = 'Value index';
$lang['montant'] = 'Amount';
$lang['type'] = 'Type';
$lang['HT'] = 'Before tax';
$lang['TVA'] = 'VAT';
$lang['TTC'] = 'VAT included';
$lang['appartement'] = 'Apartment';
$lang['parking'] = 'Car park';
$lang['forfait_charge'] = 'Charges';
$lang['total'] = 'Total';
$lang['en_attente_de_pub'] = 'Waiting for index <br> publication';
$lang['aucun_indexaton'] = 'No indexing';
$lang['action_disponible'] = 'Actions available';
$lang['remboursement_tom'] = 'Household refuse tax refund';
$lang['facturationparcelavigestion'] = 'Billing of rents by CELAVI Gestion';
$lang['charge_recuperable'] = 'Recoverable charges';
$lang['autres_information'] = 'Other information';
$lang['art_605'] = 'Art 605 :';
$lang['art_606'] = 'Art 606 :';
$lang['preneur'] = 'Lessee';
$lang['bailleur'] = 'Lessor';
$lang['commentaire'] = 'Comments :';

//Emprunts
$lang['list_pret'] = 'List of documents for loan';
$lang['mes_emprunts'] = 'My mortgages';
$lang['info_titre_pret'] = 'Thank you for having completed information related your mortgages.';
$lang['alert_ajout_pret'] = 'Please download all documents related to the loan of your investment';
$lang['capital_restant_du'] = 'Outstanding capital end @annee ';
$lang['fichier_emprunt'] = 'Mortgages file';
$lang['ajout_capital_pret'] = 'Please indicate the outstanding capital of your mortgages on' . ' ' . date('Y');
$lang['texte-conseil-clientPret'] = 'We calculate the set value of your property investments based on the outstanding principal of your mortgages. ';

//Dépenses
$lang['list_depenses'] = 'My expenses';
$lang['mes_depenses'] = 'List of my expenses';
$lang['annee_anterieure'] = 'Previous years';
$lang['date_depense'] = 'Expense date';
$lang['type_depenses'] = 'Type of expenditure';
$lang['montant_ttc'] = 'Amount incl. VAT';
$lang['montant_tva'] = 'VAT amount';
$lang['fichiers'] = 'Files ';
$lang['action'] = 'Actions';
$lang['ajout_depense'] = 'Add expense';

//taxe foncière
$lang['tax_fonciere'] = "Land tax";
$lang['alert_sans_tom'] = "You still haven't communicated your property tax and the amount of TOM for your ";
$lang['pour_'] = 'lot for ';
$lang['button_voirprec_tom'] = 'See previous years';
$lang['title_tom_information'] = "Here the informations :";
$lang['document_tom'] = 'Documents :';
$lang['montant_tax'] = 'Total amount of the land tax :';
$lang['montant_tom'] = 'Amount of the house refuse tax :';
$lang['alert_taxe'] = 'Please provide us with the @annee property tax information for your ';
$lang['alert_taxe_valider'] = 'Thank you for having completed the information regarding the @annee land tax for your ';
$lang['suite_alert_taxe'] = ' lot.';
$lang['info_alert'] = 'The information has been checked by your customer care CELAVI .';
$lang['demande_rembours'] = 'The refund of the house refuse tax has been sent to your management company ';
$lang['suite_demande_rembours'] = '.';
$lang['info_warning'] = 'The information has not yet been checked and validated by your CELAVI consultant.<br>They can still be modified.';
$lang['button_modifier_information'] = 'Change information';
$lang['button_communique_taxe'] = 'Communicating property tax';
$lang['consultation_title_taxe'] = 'Consultation of previous years for property tax';
$lang['annee'] = 'Year';
$lang['fichiers'] = 'Files';
$lang['montant_tab_taxe'] = 'Amount Tax';
$lang['montant_tab_tom'] = 'Amount TOM';
$lang['demand_tab_rem'] = 'Refund request date';
$lang['button_retour'] = 'Cancel';
$lang['form_title'] = 'Please provide information regarding property tax @annee';
$lang['fichier_taxe_fonciere'] = "Property tax file * :";
$lang['montant_taxe_fonciere'] = "Property tax amount * :";
$lang['montant_taxe_tom'] = "Amount TOM * :";
$lang['button_annuler'] = 'Cancel';
$lang['button_enregistrer'] = 'Save';
$lang['alert_file_taxe'] = 'Only PDF and image files are allowed. Maximum size 10 MB.';
$lang['delete_file'] = 'Successful file deletion';
$lang['info_enreg'] = 'Information successfully recorded';
$lang['info_enreg_modif'] = 'Successful modification';
$lang['info_enreg_erreur'] = 'Errors have been detected...';
$lang['alert_montant_tom'] = 'Please note that the TOM amount you have entered is significant (over €200). Please check it.';
$lang['texte-conseil-clientTom'] = 'These information will be used to as your management company to refund the house hold refuse tax if it is indicated on your lease contract.
As well, we will calculate the return of your investissement. These information will be sent to your chartered accountant as well';

/**** *****/
$lang['modifier-telecharger-piece-identite'] = 'Upload your updated passport';

$lang['texte-conseil-client'] = 'I\'m your customer care. You can contact me';
$lang['texte-conseil-client1'] = 'Please update your personal data.';
$lang['texte-conseil-clientDossier'] = 'Here you can find your tax status as registered in France reading your furnished letter activity';

$lang['texte-conseil-clientListeLogement'] = 'Here is the list of your properties recording your rental activity';
$lang['telephone'] = 'Phone :';
$lang['email'] = 'E-mail :';

/* list d'encaissement */
$lang['list_encaissement'] = 'Rents received';
$lang['plus_encaissement'] = 'Add a receipt';
$lang['date_encaissement'] = 'Collection date';
$lang['type_encaissement'] = 'Collection type';
$lang['montant_encaissement'] = 'Amount';
$lang['fichier_encaissement'] = 'Files';

$lang['title_formulaire_modif_encaissement'] = 'Modifying an encashment';
$lang['title_formulaire_encaissement'] = 'Add a rent collection';
$lang['title_formulaire_cin'] = 'Add a new passport';

$lang['message_requireCIN'] = 'Your profile (identity) has not been validated yet. Please contact your account manager to complete this process.';


$lang['aucune_encaissement'] = 'You didn’t enter any cash receipts for the year @var_annee. Use the "+ Add an incoming payment above" button to register an incoming payment';

$lang['btn_aucune_encaissement_saisi'] = 'I have no cash collections to enter for @var_annee';
$lang['btn_fin_saisi_encaissement'] = "I've finished entering cash receipts for @var_annee";

$lang['retour_confirm_fin_saisi_encaissement'] = 'Do you confirm the end of cash receipts registration for @var_annee?';
$lang['title_confirm_fin_saisi_encaissement'] = "Confirmation";

$lang['retour_suppression'] = 'Do you really want to cancel this cash collection ?';
$lang['title_retour_suppression'] = "Confirmation of cash collection cancelation";

$lang['btn_confirm'] = "Confirm";
$lang['btn_annuler'] = "Cancel";

$lang['encaissement_cloture'] = "You indicated that the registration  of cash receipts was completed on @closing_date. This can no longer be modified. If you need to make any changes, please contact your customer advisor directly.";

$lang['info_cloture'] = "If you have no cash receipts to enter for the year @var_annee, then you can declare your cash receipts registration complete with the button below.";

$lang['info_cloture_2'] = "If you have no more cash receipts to enter for the entire @annee year, you can declare that you finished entering cash receipts on @annee by using the button below.";

$lang['info_encaissement'] = 'Information';
$lang['commentaire'] = 'Commentaire';

// Loyer

$lang['loyer'] = 'Rent';
$lang['list_loyer'] = 'Your invoices issued by CELAVIGESTION';
$lang['consultation_facture_encaissement'] = 'Consultation of invoices and receipts : ';
$lang['texte-conseil-encaissement'] = 'If you do not provide any information regarding rent collected, we will consider that your rent has been cashed following invoices provided.';

$lang['loyer'] = 'Rent invoices';
$lang['list_loyer'] = 'List of available documents';
$lang['consultation_facture_encaissement'] = 'Consultation of invoices and receipts : ';

//Loyer tableau
$lang['loyer_facture'] = ' INVOICES';
$lang['loyer_appartement'] = 'Apartment';
$lang['loyer_appartement_ht'] = 'Invoiced exc. VAT';
$lang['loyer_appartement_tva'] = 'VAT';
$lang['loyer_appartement_ttc'] = 'Invoiced inc. VAT';
$lang['loyer_parking'] = 'Parking place';
$lang['loyer_forfait'] = 'Expenses';
$lang['loyer_franchise'] = 'Holiday rent';
$lang['total_facture'] = 'TOTAL INVOICED INC. VAT';
$lang['total_encaissement'] = 'RENT RECEIVED';
$lang['loyer_solde'] = 'Difference';


//Dépense modal ajout/modification 
$lang['depense_montant_ht'] = 'Amount exc. VAT';
$lang['depense_tva'] = 'VAT';
$lang['depense_montant_ttc'] = 'Amount inc. VAT';
$lang['aucune_charge'] = 'You have not yet entered any expenses for the year ';
$lang['message_ajout_depense'] = 'Use the "+ Add expense" above button to enter an expense.';

$lang['telecharger_document'] = 'Upload your documents';

$lang['fin_saisie_depense'] = 'I finished entering expenses for';
$lang['aucun_saisie_depense'] = 'I don\'t have any expenses to enter for ';

$lang['mesg_depense_valide'] = 'The expenses registration can no longer be modified as it has been checked and validated by your customer advisor. ';
$lang['confirm_fin_depense'] = 'Can you confirm the end of expenses by ';
$lang['confirmation'] = 'Confirm';
$lang['mesg_fin_saisie'] = 'You indicated that the expenses registration was completed on ';
$lang['mesg_fin_saisie1'] = 'This can no longer be modified.If you need to make any changes, please contact your customer care directly.';
$lang['mesg_avec_depense'] = 'If you have no more expenses to enter for the whole year 2023, you can declare your expense registration  for 2023 complete using the button below.';
$lang['mesg_sans_depense'] = 'If you have no expenses to enter for the year 2023, then you can declare your expense registration  complete using the button below.';

$lang['mesg_sans_taxe'] = 'If you have no property tax to enter for the year @annee, then you can declare your property tax registration complete with the button below.';

$lang['btn_aucun_taxe'] = 'Déclare que vous n\'avez pas de taxe foncière pour @annee';
$lang['confirm_fin_taxe'] = 'Can you confirm that you have no property tax for @annee ?';

$lang['mesg_fin_taxe'] = 'You indicated on @date that you had no property tax for the year ' . date('Y') . '. It is no longer possible to change this information.
If you need to make any changes, please contact your customer advisor directly.';
//Emprunt declaration
$lang['text_declaration'] = 'If you do not have a mortgage for this property,<br> there is no need to provide the information above and you must declare this information using the button below:';

$lang['boutton_declaration'] = 'Declare that you have no loans';
$lang['text_modal_confirm'] = 'Confirmation';
$lang['text_modal_libelle'] = 'Do you really want to indicate that you do not have any mortgage for this property?';
$lang['boutton_confirm'] = 'confirm';
$lang['text_apres_declaration_priopriétaire1'] = 'You declared on';
$lang['text_apres_declaration_priopriétaire2'] = ' that you do not have any mortgage for this property. <br> If this information is incorrect, please contact your customer advisor to make the change.';
$lang['text_apres_declaration_conseiller1'] = 'Declaration made by the advisor on ';
$lang['text_apres_declaration_conseiller2'] = ' by the advisor ';
$lang['text_apres_declaration_conseiller3'] = 'Declaration made by the advisor';

$lang['form_comment'] = 'Comment';
$lang['sans_information'] = 'No information';

//Session expirer
$lang['session_expirer'] = '<p>Dear User</br>, your session has expired. Please log in again to continue.</p>';
//Facture 
$lang['liste_facture'] = 'Invoices CELAVIGestion';
