<?php
//Menu
$lang['acceuil'] = "Accueil";
$lang['information'] = "Informations";
$lang['identity'] = "Identité";
$lang['contact'] = "Pour vous contacter";
$lang['adresses'] = "Adresses";
$lang['logements'] = "Logements";
$lang['logements_list'] = "Liste des logements";
$lang['taxe_f'] = "Taxe foncière";
$lang['compte_gere'] = "Gérer votre compte";
$lang['deconnecter'] = "Se déconnecter";

//Identité
$lang['identite_prop'] = 'Vos coordonnées et données personnelles';
$lang['identite_titre'] = 'Votre adresse et situation';
$lang['nom'] = 'Nom';
$lang['prenom'] = 'Prénom';
$lang['entreprise'] = 'Entreprise';
$lang['nationalite'] = 'Nationalité';
$lang['situation_matrimonile'] = 'Situation matrimoniale';
$lang['marie'] = 'Marié';
$lang['pacse'] = 'Pacsé';
$lang['celibataire'] = 'Célibataire';
$lang['residence_fiscale'] = 'Résidence fiscale';
$lang['date_fin_validite'] = 'Date de fin de validité CNI';
$lang['cni'] = 'CNI';
$lang['suivi_par'] = 'Votre chargée de clientèle Celavigestion';

//Contacts
$lang['email_1'] = 'Email 1';
$lang['email_2'] = 'Email 2';
$lang['phone_1'] = 'Télephone 1';
$lang['phone_2'] = 'Télephone 2';

//Bouton
$lang['modifier'] = 'Modifier';
$lang['terminer'] = 'Enregistrer';
$lang['annuler'] = 'Annuler';

//Mdp
$lang['mdp'] = 'Mot de passe';
$lang['ancien_mdp'] = 'Ancien Mot de Passe';
$lang['nouveau_mdp'] = 'Nouveau Mot de Passe';
$lang['confirm_mdp'] = 'Confirmer le Nouveau Mot de Passe';
$lang['modifier_mdp'] = 'Modifier le Mot de Passe';
$lang['saisi_ancien_mdp'] = 'Saisissez l\'ancien mot de passe';
$lang['saisi_nouv_mdp'] = 'Saisissez le nouveau mot de passe';
$lang['confirm_nouv_mdp'] = 'Confirmez le nouveau mot de passe';

//Dossier
$lang['dossier'] = 'Dossier';
$lang['liste_dossier'] = 'Liste des dossiers';
$lang['proprietaire'] = 'Propriétaires';
$lang['nb_lot'] = 'Nombre de lots';
$lang['lot'] = 'Lots';
$lang['sie'] = 'SIE';
$lang['siret'] = 'SIRET';
$lang['regime'] = 'Régime';
$lang['aucun_dossier'] = 'Aucun dossier pour ce propriétaire';
$lang['rcs'] = 'RCS';
$lang['ape'] = 'APE';
$lang['tva_intracommunautaire'] = 'TVA intracommunautaire :';
$lang['cloture'] = 'Clôture';
$lang['liasse_fiscale'] = 'Vos liasses fiscales';
$lang['decla_tva'] = 'Vos déclarations de TVA';
$lang['fiscalite'] = 'Votre fiscalité';
$lang['alert_doc_fiscal'] = 'Aucun document trouvé pour vos liasses fiscales';
$lang['alert_doc_tva'] = 'Aucun document disponible ou cette déclaration ne s\'applique pas à votre régime fiscal';
$lang['annee'] = 'Années';
$lang['documents'] = 'Documents';

//adresse logement and client
$lang['adresse_client'] = 'Adresses du propriétaire';
$lang['adresse_log'] = 'Votre bien';
$lang['adress_title_client'] = 'Adresse du propriétaire';
$lang['adress_title_lot'] = 'Adresse du propriétaire';
$lang['aucun_lot'] = 'Aucun lot pour ce propriétaire';
$lang['pgrm'] = 'Programme :';
$lang['adresses_1'] = "Adresse 1 :";
$lang['adresses_2'] = "Adresse 2 :";
$lang['adresses_3'] = "Adresse 3 :";
$lang['code_postal'] = "Code postal :";
$lang['ville'] = "Ville :";
$lang['pays'] = "Pays :";

//Menu logement
$lang['mandat'] = "Mandat";
$lang['acte'] = "Acte/Acquisition";
$lang['bail'] = "Bail";
$lang['encaissement'] = "Encaissements";
$lang['depenses'] = "Dépenses";
$lang['emprunts'] = "Emprunts";

// liste de lot
$lang['liste_log'] = "Liste des logements";
$lang['nom_pgm'] = "Nom programme";
$lang['num_dossier'] = "Numéro dossier";
$lang['ville_list_log'] = "Ville";
$lang['lot_pricpal'] = "Lot principal";
$lang['type_lot'] = "Type du Lot";
$lang['nom_gestionnaire'] = "Nom gestionnaire";
$lang['title_adresse_log'] = 'Votre bien';
$lang['title_adresse_information'] = 'Informations de votre bien';
$lang['type_bail'] = 'Type de bail :';
$lang['typologie'] = 'Typologie :';
$lang['prestation_souscrite'] = 'Prestation souscrites avec Celavigestion :';
$lang['activite'] = 'Activité :';
$lang['defiscalisation'] = 'Défiscalisation :';
$lang['defiscalisation_fin'] = 'Fin défiscalisation :';
$lang['remboursement_rom'] = 'Remboursement TOM :';
$lang['comptabilite'] = 'Votre expert comptable :';
$lang['num_lot'] = 'Numéro du lot :';
$lang['num_parking'] = 'Numéro de parking :';
$lang['title_info_payement_loyer'] = "Informations paiements loyers";
$lang['encais_loyer'] = 'Encaissement des loyers :';
$lang['mod_payement'] = 'Mode de paiement :';
$lang['rib'] = 'RIB :';
$lang['tva'] = 'TVA :';
$lang['factu_loyer'] = 'Facture loyer faite par Celavigestion:';
$lang['factu_charge'] = 'Facture charge faite par Celavigestion:';
$lang['factu_tom'] = 'Facture TOM :';
$lang['title_adresse_manager_syndic'] = "Gestionnaire et Syndic";
$lang['gestionnaire'] = 'Gestionnaire / Exploitant :';
$lang['syndic'] = 'Syndic :';
$lang['btn_modfi_rib'] = 'Modifier le RIB';
$lang['no_file'] = 'Pas de fichier';
$lang['iban'] = 'IBAN';
$lang['bic'] = 'BIC';

//Mandat
$lang['aucun_mandat'] = 'Aucun mandat pour ce lot';
$lang['aucun_facture'] = 'Aucune facture';
$lang['list_mandat'] = 'Liste des mandats';
$lang['titre_mandat'] = 'Mandat signé avec Celavigestion';
$lang['cree_le'] = 'Signé le';
$lang['montant_ht'] = 'Montant HT';
$lang['texte-conseil-clientMandat'] = 'Votre mandat avec Celavigestion est disponible ici.';
$lang['vos_facturecelav'] = 'Vos factures CELAVIGESTION';
$lang['list_facture'] = 'Liste de(s) facture(s)';

//commun 
$lang['depose_par'] = 'Déposé par';
$lang['le'] = 'le';
$lang['telecharger'] = 'Télécharger';
$lang['voir_document'] = 'Voir document';
$lang['apercu_doc'] = 'Aperçu non disponible pour ce type de fichier';

//Acte
$lang['aucun_doc'] = 'Aucun document pour ce lot';
$lang['list_acte'] = 'Liste des documents pour l\'acte';
$lang['information_acte'] = 'Informations de votre acte/acquisition';
$lang['info_general_acte'] = 'Information générales de l\'acquisition';
$lang['type_acquisition'] = 'Type d\'acquisition';
$lang['date_acte_notarie'] = 'Date de l\'acte notarié';
$lang['montant_acquisition'] = 'Montant de l\'acquisition';
$lang['valeur_acquisition'] = 'Valeur acquisition HT';
$lang['frais_acquisition'] = 'Frais d\'acquisition';
$lang['valeur_terrain'] = 'Valeur terrain HT';
$lang['valeur_construction'] = 'Valeur Construction HT';
$lang['valeur_mobilier'] = 'Valeur mobilier HT';
$lang['texte-conseil-clientActe'] = 'La valeur d\'acquisition correspond à celle marquée dans votre acte d\'achat';
$lang['texte-conseil-clientActe1'] = 'La valeur du terrain est calculée selon les références liées aux valeurs foncières par zone géographique';

//Bail
$lang['list_bail'] = 'Liste des documents pour le bail';
$lang['bail_en_cours'] = 'Bail en cours du';
$lang['tacite_en cours'] = 'Bail en tacite du';
$lang['aucun_bail'] = 'bail non validé pour ce lot';
$lang['bail_en_avenat_cours'] = 'Bail Avenant en cours du';
$lang['bail_en_avenat_cours_en_tacite'] = 'Bail Avenant en tacite du';
$lang['bail_en_cours_en_tacite'] = 'Bail en tacite du';
$lang['valide'] = 'Validé';
$lang['non_valide'] = 'Non validé';
$lang['preneur'] = 'Preneur à bail :';
$lang['exploitant'] = 'Exploitant (Gestionnaire) :';
$lang['nature_prise_deffet'] = 'Nature de la prise d\'effet :';
$lang['a_la_livraison'] = 'à la livraison';
$lang['date_sign_bail'] = 'date signature bail';
$lang['date_sign_acte'] = 'date signature acte';
$lang['suite_prec_gestionnaire'] = 'suite d\'un précédent gestionnaire';
$lang['duree'] = 'Durée :';
$lang['an'] = 'an';
$lang['ans'] = 'ans';
$lang['et'] = 'et';
$lang['mois'] = 'mois';
$lang['tacite_prolongation'] = 'Tacite prolongation :';
$lang['oui'] = 'Oui';
$lang['non'] = 'Non';
$lang['type_bail'] = 'Type de bail :';
$lang['commercial_meuble'] = 'commercial meublé';
$lang['commercial_non_meuble'] = 'commercial non meublé';
$lang['mandat_de_gestion'] = 'mandat de gestion';
$lang['saisonnier'] = 'saisonnier';
$lang['autre'] = 'autre';
$lang['date_premier_facturation_par_celavi'] = 'Date de première facturation par Celavigestion :';
$lang['date_prise_effet'] = 'Date de la prise d\'effet :';
$lang['date_fin'] = 'Date fin :';
$lang['resiliation_trienale'] = 'Résiliation triennale :';
$lang['franchise_loyer'] = 'Franchise de loyers';
$lang['aucun_franchise'] = 'Aucune Franchise de loyer pour ce bail';
$lang['debut'] = 'Début';
$lang['fin'] = 'Fin';
$lang['libelle_facture'] = 'Libellé sur facture';
$lang['type_deduction'] = 'Type de déduction';
$lang['ttc_deduit'] = 'TTC déduit';
$lang['pourcentage'] = 'Pourcentage';
$lang['information_loyer'] = 'Informations sur les loyers';
$lang['periodicite'] = 'Périodicité des loyers :';
$lang['mensuelle'] = 'Mensuelle';
$lang['trim_civile'] = 'Trimestrielle civile';
$lang['semestrielle'] = 'Semestrielle';
$lang['annulle'] = 'Annuelle';
$lang['trim_decale'] = 'Trimestrielle décalée';
$lang['periode_indexation'] = 'Période d\'indexation :';
$lang['un_an'] = '1 an';
$lang['trois_an'] = '3 ans';
$lang['non_applicable'] = 'Non Applicable';
$lang['date_premier_indexation'] = 'Date de première indexation :';
$lang['indice_utilise'] = 'Indices utilisés :';
$lang['nature_loyer'] = 'Nature du loyer :';
$lang['fixe'] = 'Fixe';
$lang['fixe_variable'] = 'Fixe et Variable';
$lang['variable'] = 'Variable';
$lang['forfait_charge_deductible'] = 'Forfait charges déductible indexé :';
$lang['date_paiement'] = 'Date de paiement :';
$lang['du_mois'] = 'du mois';
$lang['moment_paiement'] = 'Moment de paiement :';
$lang['echu'] = 'échu';
$lang['echoir'] = 'échoir';
$lang['date_premier_indexation_par_celavigestion'] = 'Date de première indexation par Celavigestion :';
$lang['indice_reference'] = 'Indices de référence :';
$lang['indexation_loyer'] = 'Indexation de loyers';
$lang['variation_plafonne'] = 'Variation plafonnée :';
$lang['augmentation_min'] = 'Augmentation minimum :';
$lang['augmentation_capee'] = 'Augmentation capée :';
$lang['indice_plafonnement'] = 'Indice de plafonnement :';
$lang['aucun'] = 'Aucun';
$lang['indice'] = 'Indice';
$lang['montant'] = 'Montant';
$lang['type'] = 'Type';
$lang['HT'] = 'HT';
$lang['TVA'] = 'TVA';
$lang['TTC'] = 'TTC';
$lang['appartement'] = 'Appartement';
$lang['parking'] = 'Parking';
$lang['forfait_charge'] = 'Forfait charges';
$lang['total'] = 'Total';
$lang['en_attente_de_pub'] = 'En attente de la publication <br> d\'indice';
$lang['aucun_indexaton'] = 'Aucun indexation';
$lang['action_disponible'] = 'Actions disponibles';
$lang['remboursement_tom'] = 'Remboursement de la TOM';
$lang['facturationparcelavigestion'] = 'Facturation des loyers par CELAVI Gestion';
$lang['charge_recuperable'] = 'Charges récupérables';
$lang['autres_information'] = 'Autres informations';
$lang['art_605'] = 'Art 605 :';
$lang['art_606'] = 'Art 606 :';
$lang['preneur'] = 'Preneur';
$lang['bailleur'] = 'Bailleur';
$lang['commentaire'] = 'Commentaire :';


//Emprunts
$lang['list_pret'] = 'Liste des documents pour les emprunts';
$lang['mes_emprunts'] = 'Mes emprunts';
$lang['info_titre_pret'] = 'Nous vous remercions d\'avoir communiqué les informations relatives à l\'emprunt de votre lot';
$lang['alert_ajout_pret'] = 'Veuillez télécharger tous les documents liés à l\'emprunt de votre investissement ';
$lang['capital_restant_du'] = 'Capital restant dû @annee ';
$lang['fichier_emprunt'] = 'Fichier emprunt';
$lang['ajout_capital_pret'] = 'Veuillez indiquer le montant de votre capital restant dû' . ' ' . date('Y');
$lang['texte-conseil-clientPret'] = 'Nous calculons la valeur de vos investissements à partir du capital restant dû de votre emprunt.';

//Dépenses
$lang['list_depenses'] = 'Mes dépenses';
$lang['mes_depenses'] = 'Liste de mes dépenses';
$lang['annee_anterieure'] = 'Années antérieures';
$lang['date_depense'] = 'Date dépense';
$lang['type_depenses'] = 'Type dépense';
$lang['montant_ttc'] = 'Montant TTC';
$lang['montant_tva'] = 'Montant TVA';
$lang['fichiers'] = 'Fichiers ';
$lang['action'] = 'Actions';
$lang['ajout_depense'] = 'Ajouter une dépense';
$lang['ajoutdepense'] = 'Ajout d\' une dépense';

//taxe foncière
$lang['tax_fonciere'] = "Taxe foncière";
$lang['alert_sans_tom'] = "Vous n'avez toujours pas communiqué votre taxe foncière ainsi que le montant de la TOM pour votre lot ";
$lang['pour_'] = 'pour ';
$lang['button_voirprec_tom'] = 'Voir les années précédentes';
$lang['title_tom_information'] = "Voici les informations :";
$lang['document_tom'] = 'Document(s) :';
$lang['montant_tax'] = 'Montant taxe foncière :';
$lang['montant_tom'] = 'Montant TOM :';
$lang['alert_taxe'] = 'Nous vous remercions de nous communiquer les informations relatives à la taxe foncière @annee pour votre lot ';
$lang['alert_taxe_valider'] = 'Nous vous remercions d\'avoir communiquer les informations relatives à la taxe foncière @annee pour votre lot ';
$lang['suite_alert_taxe'] = '.';
$lang['info_alert'] = 'Les informations ont été contrôlées et validées par votre conseiller CELAVI.';
$lang['demande_rembours'] = 'La demande de remboursement de la TOM a été envoyée le ';
$lang['suite_demande_rembours'] = ' au gestionnaire exploitant.';
$lang['info_warning'] = 'Les informations n\'ont pas encore été vérifiées et validées par votre conseiller CELAVI.<br>Elles peuvent encore être modifiées.';
$lang['button_modifier_information'] = 'Modifier les informations';
$lang['button_communique_taxe'] = 'Communiquer la taxe foncière';
$lang['consultation_title_taxe'] = 'Consultation des années précédentes pour la taxe foncière';
$lang['annee'] = 'Année';
$lang['fichiers'] = 'Fichiers';
$lang['montant_tab_taxe'] = 'Montant Taxe';
$lang['montant_tab_tom'] = 'Montant TOM';
$lang['demand_tab_rem'] = 'Date demande remboursement';
$lang['button_retour'] = 'Retour';
$lang['form_title'] = 'Veuillez communiquer les informations par rapport à la taxe foncière @annee';
$lang['fichier_taxe_fonciere'] = "Fichier taxe foncière * :";
$lang['montant_taxe_fonciere'] = "Montant taxe foncière * :";
$lang['montant_taxe_tom'] = "Montant TOM * :";
$lang['button_annuler'] = 'Annuler';
$lang['button_enregistrer'] = 'Enregistrer';
$lang['alert_file_taxe'] = 'Seuls les fichiers PDF et image sont autorisés. Taille maximum 10 Mo.';
$lang['delete_file'] = 'Suppression de fichier réussie';
$lang['info_enreg'] = 'Information enregistrée avec succès';
$lang['info_enreg_modif'] = 'Modification réussie';
$lang['info_enreg_erreur'] = 'Des erreurs ont été détectée...';
$lang['alert_montant_tom'] = 'Attention, le montant de la TOM que vous avez saisi est important (supérieur à 200€), merci de le vérifier.';
$lang['texte-conseil-clientTom'] = 'Ces informations vont nous servir à demander le remboursement de la TOM (Taxe sur les Ordures Ménagères) à votre exploitant locataire si votre bail le permet, à calculer la rentabilité de votre investissement également. Votre taxe foncière sera envoyée également à votre expert-comptable si vous avez signé une lettre de mission avec Cristalis.';


/**** *****/
$lang['modifier-telecharger-piece-identite'] = 'Télécharger une nouvelle pièce d\'identité';

$lang['texte-conseil-client'] = 'Je suis votre chargée clientèle. Vous pouvez me joindre :';
$lang['texte-conseil-client1'] = 'Pensez à mettre à jour vos coordonnées.';
$lang['texte-conseil-clientDossier'] = 'Votre dossier correspond à votre activité de loueur en meublé telle qu\'elle est enregistrée aux impôts';
$lang['texte-conseil-clientListeLogement'] = 'Les logements correspondent aux lots/appartements qui composent votre activité de  loueur en meublé';
$lang['telephone'] = 'Téléphone :';
$lang['email'] = 'Email :';

/* list d'encaissement */
$lang['list_encaissement'] = 'Liste des mes encaissements';
$lang['plus_encaissement'] = 'Ajouter un encaissement';
$lang['date_encaissement'] = 'Date d\'encaissement';
$lang['type_encaissement'] = 'Type d\'encaissement';
$lang['montant_encaissement'] = 'Montant';
$lang['fichier_encaissement'] = 'Fichiers';
$lang['montant_encaissements'] = ' Montant de l\'encaissement ';


$lang['titre-action'] = 'Action';
$lang['title_formulaire_modif_encaissement'] = 'Modification d\'un encaissement';
$lang['title_formulaire_encaissement'] = 'Ajout d\'un encaissement';
$lang['title_formulaire_cin'] = 'Ajout une nouvelle pièce d\'identité';

/*
$lang['message_requireCIN'] = 'Vous ne pouvez pas accéder à cette information tant que votre pièce d\'identité ne sera pas communiquée.
Pour cela, vous pouvez accéder au menu Informations, Identité et mettre à jour votre pièce d\'identité en utilisant le bouton "Télécharger une nouvelle pièce d\'identité".'; */

$lang['message_requireCIN'] = 'Votre profil (identité) n\'a pas encore été validé. Veuillez prendre contact avec votre chargé de clientèle pour réaliser cette opération.';


/****** *****/

$lang['aucune_encaissement'] = 'Vous n\'avez pas encore saisi d\'encaissements pour l\'année @var_annee. Utilisez le bouton "+ Ajouter un encaissement" ci-dessus pour saisir un encaissement';
$lang['btn_aucune_encaissement_saisi'] = 'Je n\'ai pas d\'encaissement à saisir pour @var_annee';
$lang['btn_fin_saisi_encaissement'] = 'J\'ai terminé la saisie des encaissements pour @var_annee';

$lang['retour_confirm_fin_saisi_encaissement'] = 'Confirmez-vous la fin de la saisie des encaissements pour @var_annee ?';
$lang['title_confirm_fin_saisi_encaissement'] = "Confirmation";

$lang['retour_suppression'] = 'Voulez-vous vraiment supprimer cet encaissement ?';
$lang['title_retour_suppression'] = "Confirmation de la suppression de l'encaissement";
//$lang['retour_suppression'] = 'Voulez-vous vraiment supprimer cet encaissement ?';

$lang['btn_confirm'] = "Confirmer";
$lang['btn_annuler'] = "Annuler";

$lang['encaissement_cloture'] = "Vous avez indiqué que la saisie des encaissements était terminée le @date_cloture. Ils ne sont plus modifiables. Si vous devez apporter des modifications, merci de contacter directement votre conseiller clientèle.";

$lang['info_cloture'] = "Si vous n'avez aucun encaissement à saisir pour l'année @var_annee, alors vous pouvez déclarer que votre saisie des encaissements est terminée avec le bouton ci-dessous";

$lang['info_cloture_2'] = "Si vous n'avez plus d'encaissement à saisir sur toute l'année @var_annee, vous pouvez déclarer que la saisie des encaissements sur @var_annee est terminée en utilisant le bouton ci-dessous.";


$lang['info_encaissement'] = 'Information';
$lang['commentaire'] = 'Commentaire';


// Loyer
$lang['loyer'] = 'Loyers';
$lang['list_loyer'] = 'Vos factures établies par CELAVIGESTION';
$lang['consultation_facture_encaissement'] = 'Consultation des factures et encaissements :';
$lang['texte-conseil-encaissement'] = 'Sans information de votre part sur vos encaissements nous considérons que les loyers ont été encaissés conformément aux factures téléchargées.';


//Loyer tableau
$lang['loyer_facture'] = 'LOYERS FACTURÉS';
$lang['loyer_appartement'] = 'Appartement';
$lang['loyer_appartement_ht'] = 'HT facturé';
$lang['loyer_appartement_tva'] = 'TVA facturée';
$lang['loyer_appartement_ttc'] = 'TTC facturé';

$lang['loyer_parking'] = 'Parking';
$lang['loyer_forfait'] = 'Forfait charges';
$lang['loyer_franchise'] = 'Franchise de loyer';
$lang['total_facture'] = 'TOTAL FACTURES TTC';
$lang['total_encaissement'] = 'TOTAL ENCAISSEMENT';
$lang['loyer_solde'] = 'Solde';

//Dépense modal ajout/modification 

$lang['depense_montant_ht'] = 'Montant HT';
$lang['depense_tva'] = 'TVA';
$lang['depense_montant_ttc'] = 'Montant TTC';
$lang['aucune_charge'] = 'Vous n\'avez pas encore saisi de dépense pour l\'année ';
$lang['message_ajout_depense'] = 'Utilisez le bouton "+ Ajouter une dépense" ci-dessus pour saisir une dépense';


$lang['telecharger_document'] = 'Téléchargez vos documents';

$lang['fin_saisie_depense'] = 'j\'ai terminé la saisie des dépenses pour';
$lang['aucun_saisie_depense'] = 'je n\'ai pas de dépense à saisir pour';

$lang['mesg_depense_valide'] = 'La saisie des dépenses n\'est plus modifiable car elle a été contrôlée et validée par votre conseiller clientèle';
$lang['confirmation'] = 'Confirmer';
$lang['confirm_fin_depense'] = 'Confirmez-vous la fin de la saisie des dépenses pour ';
$lang['mesg_fin_saisie'] = 'Vous avez indiqué que la saisie des dépenses était terminée le';
$lang['mesg_fin_saisie1'] = 'Elles ne sont plus modifiables. Si vous devez apporter des modifications, merci de contacter directement votre conseiller clientèle.';
$lang['mesg_avec_depense'] = 'Si vous n\'avez plus de dépense à saisir sur toute l\'année @annee, vous pouvez déclarer que la saisie des dépenses sur @annee est terminée en utilisant le bouton ci-dessous.';
$lang['mesg_sans_depense'] = 'Si vous n\'aurez aucune dépense à saisir pour l\'année @annee, alors vous pouvez déclarer que votre saisie des dépenses est terminée avec le bouton ci-dessous.';

$lang['mesg_sans_taxe'] = 'Si vous n\'avez aucune taxe foncière à saisir pour l\'année @annee, alors vous pouvez déclarer que votre saisie de taxe foncière est terminée avec le bouton ci-dessous.';

$lang['btn_aucun_taxe'] = 'Déclarez que vous n\'avez pas de taxe foncière pour @annee';
$lang['confirm_fin_taxe'] = 'Pouvez-vous confirmer que vous n\'avez pas de taxe foncière pour @annee ? ';

$lang['mesg_fin_taxe'] = 'Vous avez indiqué  le @date que vous n\'aviez pas de taxe foncière pour l\'année ' . date('Y') . '. Il n\'est plus possible de modifier cette information.
Si vous devez apporter des modifications, merci de contacter directement votre conseiller clientèle.';
//Emprunts déclarations
$lang['text_declaration'] = 'Si vous n\'avez pas d\'emprunt pour ce bien immobilier, <br>il est inutile de renseigner les informations ci-dessus et vous devez déclarer cette information avec le bouton ci-dessous :';

$lang['boutton_declaration'] = 'Déclarez que vous n\'avez pas d\'emprunt';
$lang['text_modal_confirm'] = 'Confirmation';
$lang['text_modal_libelle'] = 'Voulez vous réellement indiquer que vous n\'avez pas d\'emprunt pour ce bien immobilier ?';
$lang['boutton_confirm'] = 'confirmer';
$lang['text_apres_declaration_priopriétaire1'] = 'Vous avez déclaré le ';
$lang['text_apres_declaration_priopriétaire2'] = ' qu\'il n\'y a pas d\'emprunt pour ce bien immobilier. <br> Si cette information est fausse, merci de contacter votre conseiller clientèle pour qu\'il fasse le changement.';
$lang['text_apres_declaration_conseiller1'] = 'Déclaration effectuée par le conseiller le ';
$lang['text_apres_declaration_conseiller2'] = ' par le conseiller ';
$lang['text_apres_declaration_conseiller3'] = 'Déclaration effectuée par le conseiller';

//Commentaire
$lang['form_comment'] = 'Commentaire';
$lang['sans_information'] = 'Aucune information';

//Session expirer
$lang['session_expirer'] = '<p> Cher utilisateur, votre session a expiré. Veuillez vous reconnecter pour continuer.</p>';

//Facture 
$lang['liste_facture'] = 'Factures CELAVIGestion';