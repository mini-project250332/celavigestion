<?php
$name='PTSans-NarrowBold';
$type='TTF';
$desc=array (
  'Ascent' => 1018,
  'Descent' => -276,
  'CapHeight' => 700,
  'Flags' => 262148,
  'FontBBox' => '[-422 -273 1011 1009]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 750,
);
$up=-75;
$ut=50;
$ttffile='/home/www/fw/application/libraries/ttfonts/PTN77F.ttf';
$TTCfontID='0';
$originalsize=487796;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='pt_sans_narrowB';
$panose=' 8 2 2 b 7 6 2 2 3 2 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>