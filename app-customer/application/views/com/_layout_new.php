<main class="main">
	<div class="container-fluid" data-layout="container">
		<nav class="navbar navbar-light navbar-vertical navbar-expand-xl">
			<div class="d-flex align-items-center">
				<div class="toggle-icon-wrapper">
					<button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip" data-bs-placement="left" title="" data-bs-original-title="Toggle Navigation" aria-label="Toggle Navigation">
						<span class="navbar-toggle-icon">
							<span class="toggle-line"></span>
						</span>
					</button>
				</div>
				<a class="navbar-brand" href="../index.html">
					<div class="d-flex align-items-center py-3">
						<img class="me-2" src="../assets/img/icons/spot-illustrations/falcon.png" alt="" width="40">
						<span class="font-sans-serif">falcon</span>
					</div>
				</a>
			</div>

			<div class="collapse navbar-collapse" id="navbarVerticalCollapse">
				<div class="navbar-vertical-content scrollbar">
					<ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
						<li class="nav-item">
							<a class="nav-link dropdown-indicator collapsed" href="#dashboard" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
								<div class="d-flex align-items-center">
									<span class="nav-link-icon"></span>
									<span class="nav-link-text ps-1">Dashboard</span>
								</div>
							</a>
						</li>
					</ul>
				</div>

		</nav>


		<div class="content">
			<nav class="navbar navbar-light navbar-glass navbar-top navbar-expand">

				<button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation">
					<span class="navbar-toggle-icon">
						<span class="toggle-line"></span>
					</span>
				</button>

				<a class="navbar-brand me-1 me-sm-3" href="">
					<div class="d-flex align-items-center">
						<img class="me-2" src="" alt="" width="40"><span class="font-sans-serif">falcon</span>
					</div>
				</a>

				<ul class="navbar-nav align-items-center d-none d-lg-block">
					<li class="nav-item">
					</li>
				</ul>

				<ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
					<li class="nav-item"></li>
					<li class="nav-item dropdown">
						<a class="nav-link pe-0 ps-2" id="navbarDropdownUser" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<div class="avatar avatar-xl">
								<img class="rounded-circle" src="../assets/img/team/3-thumb.png" alt="">
							</div>
						</a>
						<div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
							<div class="bg-white dark__bg-1000 rounded-2 py-2">
								<a class="dropdown-item fw-bold text-warning" href="#!">
									<span class="fas fa-crown me-1"></span>
									<span>Go Pro</span>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#!">Set status</a>
								<a class="dropdown-item" href="../pages/user/profile.html">Profile &amp; account</a>
								<a class="dropdown-item" href="#!">Feedback</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="../pages/user/settings.html">Settings</a>
								<a class="dropdown-item" href="../pages/authentication/card/logout.html">Logout</a>
							</div>
						</div>
					</li>

				</ul>
			</nav>


			<div class="card">


			</div>

		</div>
</main>



<?php if (isset($page)) $this->load->view($page) ?>

<div class="modal fade" id="modal-main" tabindex="-1" aria-labelledby="ui-dialog-title-ajoutSociete" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="modal-main-content">

		</div>
	</div>
</div>

<div class="modal modal-fixed-top fade" id="big-modal" tabindex="-1" role="dialog" aria-labelledby="big-modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-vertical" role="document">
		<div id="content-bigModal" class="modal-content"></div>
	</div>
</div>