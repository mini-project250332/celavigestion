<?php
if (!$this->session->has_userdata("session_utilisateur_client")) {
    $token = ($this->session->has_userdata("token")) ? $this->session->userdata('token') : NULL;
    $this->session->sess_destroy();
    redirect('Auth/logout/' . $token);
}
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <meta name="description" content="<?php echo description; ?>">
    <meta name="author" content="<?php echo author; ?>">
    <title><?php echo title; ?></title>
    <!-- Meta -->

    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />
    <!-- Style par default com/_css -->
    <?php $this->load->view("com/_css"); ?>

    <!-- Style personnaliser -->
    <?php if (isset($css_personnaliser)) {
        foreach ($css_personnaliser as $css_p) {
            echo '<link type="text/css" href="' . base_url('assets/' . $css_p . VERSION_SCRIPT) . '" rel="stylesheet">';
        }
    } ?>

    <!-- Javascript constant application -->
    <script type="text/javascript">
        const const_app = {
            <?php
            if (isset($const_js)) {
                foreach ($const_js as $js_n => $val_n) {
                    echo $js_n . " : '" . $val_n . "',\n";
                }
            }
            ?>
        }
    </script>

    <!-- Lirairie JS par default "require" -->
    <?php
    if (isset($js_libraries)) {
        foreach ($js_libraries as $js_lib) {
            echo '<script src="' . base_url('../assets/' . $js_lib) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <?php
    if (isset($js_requerie)) {
        foreach ($js_requerie as $js_r) {
            echo '<script src="' . base_url('assets/' . $js_r) . '" type="text/javascript"></script>';
        }
    }
    ?>

    <script type="text/javascript">
        var text_sessionExpirer = "<?= $this->lang->line('session_expirer'); ?>";
    </script>
</head>

<body>