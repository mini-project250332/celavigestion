<header class="header-app">
	<div class="left-header">
		<button type="button" class="navbar-toggler" id="navbar_toggler" style="display: none;">
			<span>
				<i class="fa fa-bars"></i>
			</span>
		</button>
		<div class="div-logo">
			<div class="logo-img">
				<img src="<?php echo base_url('../assets/img/celavi.jpg'); ?>">
			</div>
		</div>
		<div class="leftcontainer-header">
			<div class="menu-header">
				<ul class="menu-web">

				</ul>
				<ul class="menu-web">
					<li class="user_name">
						<div class="libelle-header">
							<h5>
								<?= $_SESSION['session_utilisateur_client']['client_nom'] . ' ' . $_SESSION['session_utilisateur_client']['client_prenom'] ?>
							</h5>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>


	<div class="right-header">
		<div class="icon-profil">
			<a id="toggle_dropdownCompte" class="" href="javascript:void(0);">
				<img src="<?php echo base_url('assets/images/user.png'); ?>" alt="ANDRIAMAMPIADANA">
			</a>
			<div class="dropdown-compte">
				<div class="div-content-compte">
					<div class="div-compte">
						<div class="photo-compte-user">
							<img src="<?php echo base_url('assets/images/user.png'); ?>">
							<div class="btn-change-photo">

							</div>
						</div>
						<div class="fs--1">
							<?= $_SESSION['session_utilisateur_client']['client_nom'] . ' ' . $_SESSION['session_utilisateur_client']['client_prenom'] ?>
						</div>
						<!-- <a class="btn btn-sm btn-outline-primary w-100"> <?= $this->lang->line('compte_gere'); ?> </a> -->
						<div class="row">
							<div class="col lang_logo text-end">
								<a href="<?php echo base_url('Language/switch_language/french'); ?>">
									<img src="<?php echo base_url('../assets/img/FR.png'); ?>" id="lang_french">
								</a>
								<a href="<?php echo base_url('Language/switch_language/english'); ?>">
									<img src="<?php echo base_url('../assets/img/EN.png'); ?>" id="lang_english">
								</a>
							</div>
						</div>
					</div>
					<div>
						<a href="<?php echo base_url('Auth/logout'); ?>" class="btn btn-sm btn-primary w-100">
							<?= $this->lang->line('deconnecter'); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
