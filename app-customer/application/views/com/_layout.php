<?php $this->load->view("com/_headerPage"); ?>
<div class="contenair-main">
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto sidenav" id="sidenav" style="padding: 0;">
                <!-- min-vh-100 -->
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white ">
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        
                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "menu") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Menu/Menu'); ?>" class="nav-link px-0 align-middle text-white ">
                                <i class="bi-house"></i>
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('acceuil'); ?>
                                </span>
                            </a>
                        </li>

                        <div class="main_menu">
                            <i class="fa fa-info" aria-hidden="true"></i>
                            <span class="ms-1 d-sm-inline">
                                <?= $this->lang->line('information'); ?>
                            </span>
                        </div>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "identite") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Identite/Identite'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('identity'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "password") ? 'active' : ''; ?> d-none">
                            <a href="<?php echo base_url('Password/Password'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('mdp'); ?>
                                </span>
                            </a>
                        </li>

                        <div class="main_menu">
                            <i class="fas fa-folder"></i>
                            <span class="ms-1 d-sm-inline">
                                <?= $this->lang->line('dossier'); ?>
                            </span>
                        </div>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "liste_dossier") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Liste_Dossier/Liste_Dossier'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('liste_dossier'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "liste_facture") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Liste_facture/Liste_facture'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('liste_facture'); ?>
                                </span>
                            </a>
                        </li>

                        <div class="main_menu">
                            <i class="fa fa-address-book" aria-hidden="true"></i>
                            <span class="ms-1 d-sm-inline">
                                <?= $this->lang->line('logements'); ?>
                            </span>
                        </div>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "adresse_log") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Adresse_log/Adresse_log'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('logements_list'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "mandat") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Mandat/Mandat'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('mandat'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "acte") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Acte/Acte'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('acte'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "bail") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Bail/Bail'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('bail'); ?>
                                </span>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "loyer") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Loyer/Loyer'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('loyer'); ?>
                                </span>
                            </a>
                        </li>

                        <div class="main_menu">
                            <i class="fas fa-cloud-download-alt"></i>
                            <span class="ms-1 d-sm-inline">
                                <?= $this->lang->line('telecharger_document'); ?>
                            </span>
                        </div>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "encaissement") ? 'active' : ''; ?> ">
                            <a href="<?php echo base_url('Encaissement/Encaissement'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('encaissement'); ?>
                                </span>
                                <i class="fas fa-cloud-download-alt blink-icon "></i>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "tom") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Tom/Tom'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('taxe_f'); ?>
                                </span>
                                <i class="fas fa-cloud-download-alt blink-icon " style="margin-left: 9px;"></i>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "depenses") ? 'active' : ''; ?> ">
                            <a href="<?php echo base_url('Depenses/Depenses'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('depenses'); ?>
                                </span>
                                <i class="fas fa-cloud-download-alt blink-icon" style="margin-left: 33px;"></i>
                            </a>
                        </li>

                        <li class="nav-item <?php echo (isset($menu_principale) && $menu_principale == "pret") ? 'active' : ''; ?>">
                            <a href="<?php echo base_url('Pret/Pret'); ?>" class="nav-link align-middle text-white">
                                <span class="ms-1 d-sm-inline">
                                    <?= $this->lang->line('emprunts'); ?>
                                </span>
                                <i class="fas fa-cloud-download-alt blink-icon " style="margin-left: 31px;"></i>
                            </a>
                        </li>

                    </ul>
                    <hr>
                </div>
            </div>
            <div class="col py-2" id="app-contenu">
                <?php if (isset($page)) $this->load->view($page) ?>
            </div>
        </div>
    </div>
</div>