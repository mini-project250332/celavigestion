<?php if (isset($css_libraries)) {
    foreach ($css_libraries as $css_lib) {
        echo '<link type="text/css" href="' . base_url('../assets/' . $css_lib . VERSION_SCRIPT) . '" rel="stylesheet">';
    }
} ?>

<!-- Style par default com/_css -->
<?php if (isset($css_style)) {
    foreach ($css_style as $css_s) {
        echo '<link type="text/css" href="' . base_url('assets/' . $css_s . VERSION_SCRIPT) . '" rel="stylesheet">';
    }
} ?>