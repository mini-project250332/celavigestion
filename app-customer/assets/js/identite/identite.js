var identite_url = {
    loadIdentite: `${const_app.base_url}Identite/loadIdentite`,
    ficheClient_page: `${const_app.base_url}Identite/pageficheClient`,
}

var timer = null;

function pgIdentite() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-identite"
    }
    var url_request = identite_url.loadIdentite;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadIdentiteCallBack', loading);
}

function loadIdentiteCallBack(response) {
    $('#contenaire-identite').html(response);
}

function getFicheIdentificationClient(id,page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id,
        'page' : page
    };
    var loading = {
        type: "content",
        id_content: "contenaire-identite"
    }
    var url_request = identite_url.ficheClient_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheClient', loading);
}

function retourFicheClient(response) {
    $('#fiche-identifiant-client').html(response);
}

$(document).on('click', '#update-contact', function (e) {
    e.preventDefault();
    $("#update-contact").toggleClass('d-none');
    $("#save-update-contact").toggleClass('d-none');
    $("#annuler-update-contact").toggleClass('d-none');
    $("#update-cni").toggleClass('d-none');
    getFicheIdentificationClient($(this).attr('data-id'), 'form');
});

$(document).on('click', '#annuler-update-contact', function (e) {
    e.preventDefault();
    $("#update-contact").toggleClass('d-none');
    $("#save-update-contact").toggleClass('d-none');
    $("#annuler-update-contact").toggleClass('d-none');
    $("#update-cni").toggleClass('d-none');
    getFicheIdentificationClient($(this).attr('data-id'), 'fiche');
});

$(document).on('submit', '#updateContact', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-identite"
    }
    fn.ajax_form_data_custom(form, type_output, "retourUpdateContact", loading);
});
function retourUpdateContact(response) {
    if (response.status == true) {
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        $("#update-contact").toggleClass('d-none');
        $("#save-update-contact").toggleClass('d-none');
        $("#annuler-update-contact").toggleClass('d-none');
        getFicheIdentificationClient(response.data_retour.client_id,'fiche');

    } else {
        fn.response_control_champ(response);
    }
}


$(document).on('click', '#update-cni', function () {
    myModal.toggle();//pgIdentite();
});

$(document).on('submit', '#update_cin', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function() {
            loading_fn(loading);
        },
        success: function(response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                myModal.toggle();
                location.reload();
            } else {
                fn.response_control_champ(response);
            }
        },
        complete: function() {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});