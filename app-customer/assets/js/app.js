var app_url = {

}

$(document).on('click', '#toggle_dropdownCompte', function (e) {
    var parent = $(this).closest('.icon-profil');
    dropdown_compte = parent.find('.dropdown-compte');
    dropdown_compte.toggleClass('show');
});

$(document).on('click', '#navbar_toggler', function (e) {
    var visible = $("#sidenav").toggle().is(":visible");
    if (visible) {
        $("#sidenav").animate({
            width: 200,
            opacity: 1
        }, 500);
    } else {
        $("#sidenav").show();
        $("#sidenav").animate({
            width: 0,
            opacity: 0.2
        }, 500, "linear", function () {
            $("#sidenav").hide();
        });
    }
});

function forceDownload(href, nom_fichier = null) {
    var link = document.createElement('a');
    link.href = href;
    link.download = href;
    if (nom_fichier == null) {
        var str = link.download.split("/");
        var str_length = str.length;
        link.download = str[str_length - 1];
    }
    else {
        link.download = nom_fichier;
    }
    document.body.appendChild(link);
    link.click();
}