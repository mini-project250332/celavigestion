var loyer_url = {
    getLoyer: `${const_app.base_url}Loyer/getLoyer`,
    ficheLoyer: `${const_app.base_url}Loyer/ficheLoyer`,
    getLoyerFacture: `${const_app.base_url}Loyer/GetTableFacture`,
    getpathDocLoyer: `${const_app.base_url}Loyer/getpathDocLoyer`,
}

var timer = null;

function getDocLoyer() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-loyer"
    }
    var url_request = loyer_url.getLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetDocLoyer', loading);
}
function retourgetDocLoyer(response) {
    $('#contenaire-loyer').html(response);
}

function getFicheLoyerLot(id, year) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'year': year
    };
    var loading = {
        type: "content",
        id_content: "contenaire-loyer"
    }
    var url_request = loyer_url.ficheLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheLoyer', loading);
}
function retourficheLoyer(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#loyer-lot_' + identifiant).html(html);
}

function getTableFacture(bail_id, pdl_id, bail_loyer_variable, year, lot_id = 0) {
    var type_request = 'POST';
    var url_request = loyer_url.getLoyerFacture;
    var type_output = 'html';

    var data_request = {
        'bail_id': bail_id,
        'pdl_id': pdl_id,
        'bail_loyer_variable': bail_loyer_variable,
        'year': year,
        'lot_id': lot_id
    };

    var loading = {
        type: "content",
        id_content: "container-table-facture_encaissement_" + lot_id
    }

    fn.ajax_request(type_request, url_request, type_output, data_request, 'getLoyerFactureCallback', loading);
}
function getLoyerFactureCallback(response) {
    var cliLot_id = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#container-table-facture_encaissement_' + cliLot_id).html(html);
}

function apercuDocumentLoyer(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = loyer_url.getpathDocLoyer;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathLoyer');
}

function responsepathLoyer(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercu_docLoyer_' + cliLot_id)
    var dataPath = response[0].doc_loyer_path
    var id_document = response[0].doc_loyer_id
    apercuLoyer(dataPath, el_apercu, id_document, cliLot_id);
}

function apercuLoyer(data, el, id_doc, cliLot_id) {
    console.log(data);
    viderApercuLoyer(cliLot_id);
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuLoyer_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuLoyer_' + cliLot_id).appendChild(element);
    }
}

function viderApercuLoyer(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercu_docLoyer_" + cliLot_id) != undefined) {
        if (document.getElementById("apercu_docLoyer_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercu_docLoyer_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_docLoyer_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}