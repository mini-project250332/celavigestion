var acte_url = {
    getActe: `${const_app.base_url}Acte/getActe`,
    ficheActe: `${const_app.base_url}Acte/ficheActe`,
    getPathActe: `${const_app.base_url}Acte/getPathActe`,
}

var timer = null;

function getDocAte() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-acte"
    }
    var url_request = acte_url.getActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getActeCallBack', loading);
}

function getActeCallBack(response) {
    $('#contenaire-acte').html(response);
}

function getFicheActeLot(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-acte"
    }
    var url_request = acte_url.ficheActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheActe', loading);
}

function retourficheActe(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-acte-lot_' + identifiant).html(html);
}

function apercuDocumentActe(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = acte_url.getPathActe;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsePathActe');
}

function responsePathActe(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercu_doc_Acte_' + cliLot_id);
    var dataPath = response[0].doc_acte_path;
    var id_document = response[0].doc_acte_id;

    apercuDocActe(dataPath, el_apercu, id_document, cliLot_id);
}

function apercuDocActe(data, el, id_doc, cliLot_id) {
    viderApercuActe(cliLot_id);
    // var doc_selectionner = document.getElementById("docrowActe-" + id_doc);
    // doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuActe_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuActe_' + cliLot_id).appendChild(element);
    }
}

function viderApercuActe(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercuActe");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuActe_" + cliLot_id) != undefined) {
        if (document.getElementById("apercuActe_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercuActe_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuActe_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}