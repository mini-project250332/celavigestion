var liste_doss_url = {
    getListDossier: `${const_app.base_url}Liste_Dossier/getListDossier`,
    ficheDossier:  `${const_app.base_url}Liste_Dossier/ficheDossier`,
}

function listeDossier() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-liste-dossier"
    }
    var url_request = liste_doss_url.getListDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeDossier', loading);
}
function load_listeDossier(response) {
    $('#contenaire-liste-dossier').html(response);
}

function getFicheDossier(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-liste-dossier"
    }
    var url_request = liste_doss_url.ficheDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheDossier', loading);
}

function retourficheDossier(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-dossier_' + identifiant).html(html);
}
