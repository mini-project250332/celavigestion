var data_url = {
    message_acceuil: `${const_app.base_url}Menu/Message_acceuil`,
}

function Message_acceuil() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-acceuil"
    }
    var url_request = data_url.message_acceuil;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'Message_acceuil_Callback', loading);
}
function Message_acceuil_Callback(response) {
    $('#contenaire-acceuil').html(response);
}