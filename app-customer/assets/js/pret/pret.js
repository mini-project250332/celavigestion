var pret_url = {
    getPret: `${const_app.base_url}Pret/getPret`,
    fichePret: `${const_app.base_url}Pret/fichePret`,
    getPathPret: `${const_app.base_url}Pret/getPathPret`,
    savePret: `${const_app.base_url}Pret/savePret`,
    removefile: `${const_app.base_url}Pret/removefile`,
    declarationPret: `${const_app.base_url}Pret/declarationPret`,
}

var timer = null;

function getDocPret() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-pret"
    }
    var url_request = pret_url.getPret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getPretCallBack', loading);
}

function getPretCallBack(response) {
    $('#contenaire-pret').html(response);
    var lastActiveTab = localStorage.getItem("lastActiveTab");
    if (lastActiveTab) {
        $(".nav-link").each(function () {
            if ($('button').hasClass('active')) {
                $('button').removeClass('active');
            }
        });
        $('#nav-lot_' + lastActiveTab + '-tab').addClass('active');

        $(".tab-pane").each(function () {
            if ($('.tab-pane').hasClass('active show')) {
                $('.tab-pane').removeClass('active show');
            }
        });
        $('#nav-lot_' + lastActiveTab).addClass('show active');
        localStorage.removeItem("lastActiveTab");
    }
}



function getFichePretLot(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-pret"
    }
    var url_request = pret_url.fichePret;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourfichePret', loading);
}

function retourfichePret(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-pret-lot_' + identifiant).html(html);
}

function savePret(cliLot_id, lang) {
    $('.help_pret_montant_' + cliLot_id).empty();
    $('.help_file_' + cliLot_id).empty();
    var alert_vide = lang == 'french' ? 'Ce champ ne doit pas être vide.' : 'This field must not be empty.';
    var alert_file = lang == 'french' ? "Seuls les fichiers PDF et image sont autorisés. Taille maximum 10 Mo." : "Only PDF and image files are allowed. Maximum size 10 MB.";

    if (!$('#pret_montant_' + cliLot_id).val()) {
        $('.help_pret_montant_' + cliLot_id).text(alert_vide);
    }

    if ($('#file_' + cliLot_id)[0].files.length < 1) {
        $('.help_file_' + cliLot_id).text(alert_vide);
    }

    if ($('#pret_montant_' + cliLot_id).val() && $('#file_' + cliLot_id)[0].files.length > 0) {
        var formData = new FormData();
        formData.append('pret_montant_', $('#pret_montant_' + cliLot_id).val());
        formData.append('pret_id', $('#pret_id' + cliLot_id).val());
        formData.append('cliLot_id', $('#cliLot_id_' + cliLot_id).val());

        var fileInput = $('#file_' + cliLot_id)[0];
        var selectedFiles = fileInput.files;

        for (var i = 0; i < selectedFiles.length; i++) {
            if (selectedFiles[i].size <= 10 * 1024 * 1024) {
                formData.append(i, selectedFiles[i]);
                $('.msg_' + cliLot_id).text(alert_file);
                $('.msg_' + cliLot_id).removeClass('text-danger');
            } else {
                var text_mo_file = lang == 'french' ? "Le fichier " + selectedFiles[i].name + " dépasse la limite de 10 Mo." : "The file " + selectedFiles[i].name + " exceeds the 10 MB limit.";
                $('.msg_' + cliLot_id).text(text_mo_file);
                $('.msg_' + cliLot_id).addClass('text-danger');
                return false;
            }
        }

        $.ajax({
            url: pret_url.savePret,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    localStorage.setItem("lastActiveTab", obj.data_retour.cliLot_id);
                    getDocPret();
                    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
}

$(document).on('click', '#removefile_pret', function (e) {
    var id = $(this).attr('data-id');
    removefilePret(id);
});

function removefilePret(doc_pret_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        doc_pret_id: doc_pret_id,
    };

    var url_request = pret_url.removefile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'removefileCallBack');
}
function removefileCallBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == true) {
        $('.doc_' + obj.data_retour.doc_pret_id).empty();
        Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function confirmDeclaration(cliLot_id) {
    var formData = new FormData();
        formData.append('cliLot_id', cliLot_id);
      $.ajax({
          url: pret_url.declarationPret,
          type: 'POST',
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
              const obj = JSON.parse(response);
              if (obj.status == true) {
                  localStorage.setItem("lastActiveTab", obj.data_retour.cliLot_id);
                  getDocPret();
                  Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
              }
          },
          error: function (xhr, status, error) {
              console.error(error);
          }
      });
  
}

function confirmDeclaration2(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = pret_url.declarationPret;
    

    var loading = {
        type: "content",
        id_content: "contenaire-pret"
    } 

    fn.ajax_request(type_request, url_request, type_output, data_request, 'confirmDeclarationCallback', loading);
    
}


function confirmDeclarationCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.taxe_info
                    }
                    confirmDeclaration2(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            localStorage.setItem("lastActiveTab", response.taxe_info);
            getDocPret();
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}