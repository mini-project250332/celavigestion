var adresse_log_url = {
    loadAdresse_log: `${const_app.base_url}Adresse_log/loadAdresse_log`,
    fichelogement: `${const_app.base_url}Adresse_log/fichelogement`,
}

var timer = null;

function pgAdresse_log() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-adresse_log"
    }
    var url_request = adresse_log_url.loadAdresse_log;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadAdresse_logCallBack', loading);
}

function loadAdresse_logCallBack(response) {
    $('#contenaire-adresse_log').html(response);
}

function getFichelogementLot(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-adresse_log"
    }
    var url_request = adresse_log_url.fichelogement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourAdresse_log', loading);
}

function retourAdresse_log(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-logement-lot_' + identifiant).html(html);
}
