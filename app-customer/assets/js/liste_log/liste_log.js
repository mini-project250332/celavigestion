var liste_log_url = {
    getLotDossier: `${const_app.base_url}Liste_log/getLotDossier`,
}

function listeLot() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-liste_log"
    }
    var url_request = liste_log_url.getLotDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeLot', loading);
}
function load_listeLot(response) {
    $('#contenaire-liste_log').html(response);
}
