var adresse_client_url = {
    loadAdresse_client: `${const_app.base_url}Adresse_client/loadAdresse_client`,
    ficheClient_page: `${const_app.base_url}Adresse_client/pageficheClient`,
}

var timer = null;

function pgAdresse_client() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-adresse_client"
    }
    var url_request = adresse_client_url.loadAdresse_client;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadAdresse_clientCallBack', loading);
}

function loadAdresse_clientCallBack(response) {
    $('#contenaire-adresse_client').html(response);
}

function getFicheAdresse_client(id,page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id,
        'page' : page
    };
    var loading = {
        type: "content",
        id_content: "contenaire-adresse_client"
    }
    var url_request = adresse_client_url.ficheClient_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourAdresse_client', loading);
}

function retourAdresse_client(response) {
    $('#fiche-adresse_client-client').html(response);
}

$(document).on('click', '#update-adresse', function (e) {
    e.preventDefault();
    $("#update-adresse").toggleClass('d-none');
    $("#save-update-adresse").toggleClass('d-none');
    getFicheAdresse_client($(this).attr('data-id'), 'form');
});

$(document).on('click', '#save-update-adresse', function (e) {
    e.preventDefault();
    $("#update-adresse").toggleClass('d-none');
    $("#save-update-adresse").toggleClass('d-none');
    getFicheAdresse_client($(this).attr('data-id'), 'fiche');
});

$(document).on('change', '#updateAdresse select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateAdresse();
    }, 800);
});

$(document).on('keyup', '#updateAdresse input', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateAdresse();
    }, 800);
});

function submitUpdateAdresse() {
    var form = $("#updateAdresse");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output);
}