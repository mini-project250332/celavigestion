var contacts_url = {
    loadContacts: `${const_app.base_url}Contacts/loadContacts`,
    ficheClient_page: `${const_app.base_url}Contacts/pageficheClient`,
}

var timer = null;

function pgContacts() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-contacts"
    }
    var url_request = contacts_url.loadContacts;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadContactsCallBack', loading);
}

function loadContactsCallBack(response) {
    $('#contenaire-contacts').html(response);
}

function getFicheContactsClient(id, page) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id,
        'page' : page
    };
    var loading = {
        type: "content",
        id_content: "contenaire-contacts"
    }
    var url_request = contacts_url.ficheClient_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheClient', loading);
}

function retourFicheClient(response) {
    $('#fiche-contacts-client').html(response);
}

$(document).on('click', '#update-contact', function (e) {
    e.preventDefault();
    $("#update-contact").toggleClass('d-none');
    $("#save-update-contact").toggleClass('d-none');
    getFicheContactsClient($(this).attr('data-id'), 'form');
});


$(document).on('change', '#updateContact select', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateContact();
    }, 800);
});

$(document).on('keyup', '#updateContact input', function (e) {
    e.preventDefault();
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    timer = setTimeout(() => {
        submitUpdateContact();
    }, 800);
});

function submitUpdateContact() {
    var form = $("#updateContact");
    var type_output = 'json';
    fn.ajax_form_data_custom(form, type_output);
}

$(document).on('click', '#save-update-contact', function (e) {
    e.preventDefault();
    $("#update-contact").toggleClass('d-none');
    $("#save-update-contact").toggleClass('d-none');
    getFicheContactsClient($(this).attr('data-id'), 'fiche');
});


