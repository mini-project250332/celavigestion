var encaissement_url = {
    getEncaissement: `${const_app.base_url}Encaissement/getEncaissement`,
    getListeEncaissement: `${const_app.base_url}Encaissement/getListeEncaissement`,
    get_formAjoutEncaissement: `${const_app.base_url}Encaissement/get_formulaireEncaissement`,
    deleteEncaissement: `${const_app.base_url}Encaissement/deleteEncaissement`,
    getPath: `${const_app.base_url}Encaissement/getPath`,
    supprimerFile: `${const_app.base_url}Encaissement/supprimerFile`,
    finSaisi_encais: `${const_app.base_url}Encaissement/finSaisi_encais`,
}

var timer = null;

var myModal = new bootstrap.Modal(document.getElementById("modalForm"), {
    keyboard: false
});

function getEncaissement() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.getEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getEncaissementCallBack', loading);
}

function getEncaissementCallBack(response) {
    $('#contenaire-encaissement').html(response);
}

function getListeEncaissement(id, annee) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'annee': annee
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.getListeEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetListeEncaissement', loading);
}

function retourgetListeEncaissement(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#liste-encaissement-lot_' + identifiant).html(html);
}

function charger_contentModal(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('click', '.btn_ajoutEncaissement', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': $(this).attr('data-id'),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.get_formAjoutEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_formulaireAjoutEncaissement', loading);
});

function retourn_formulaireAjoutEncaissement(response) {
    $("#modal-main-content").html(response);
    myModal.toggle();
}

$(document).on('submit', '#cruEncaissement', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenair-menu-communications"
    }

    var formData = new FormData(form[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        dataType: type_output,
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        beforeSend: function () {
            loading_fn(loading);
        },
        success: function (response) {
            if (response.status == true) {
                Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                getListeEncaissement(response.data_retour, $(`#filtre-date-${response.data_retour}`).val());
                myModal.toggle();
                $("#modal-main-content").html('');
            } else {
                fn.response_control_champ(response);
            }
        },
        complete: function () {
            stop_loading_fn(loading);
        },
        error: function (xhr, status, error) {
            console.log("Erreur :", error);
        },
    });
});

$(document).on('click', '.btn_editEncaissement', function () {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': $(this).attr('data-cli_lot_id'),
        'enc_id': $(this).attr('data-id'),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.get_formAjoutEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_formulaireAjoutEncaissement', loading);
});


$(document).on('click', '.btn_supprimeEncaissement', function () {
    var data_request = {
        'enc_id': $(this).attr('data-id'),
        'action': 'demande',
        'cliLot_id': $(this).attr('data-cli_lot_id'),
    };
    supprimerEnc(data_request);
});

function supprimerEnc(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.deleteEncaissement;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_confirmSuppressionEnc', loading);
}

function retourn_confirmSuppressionEnc(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        'action': "confirm",
                        'enc_id': response.data.enc_id,
                        'cliLot_id': response.data.cliLot_id,
                    }
                    supprimerEnc(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.form_validation.mmessage, { position: 'left-bottom', timeout: 3000 });
            getListeEncaissement(response.data_retour.cliLot_id, $(`#filtre-date-${response.data_retour.cliLot_id}`).val());
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}


$(document).on('change', '.filtre-date-encaissement', function (e) {
    e.preventDefault();
    var id = $(this).attr('id');
    var split = id.split("-");
    getListeEncaissement(split[2], $(this).val());
});

function apercuDocEncaissement(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = encaissement_url.getPath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathEnc');
}

function responsepathEnc(response) {
    var clilot_id = response[0].clilot_id;
    var el_apercu = document.getElementById('apercu_list_docEnc_' + clilot_id);
    var dataPath = response[0].doc_encaissement_path;
    var id_document = response[0].doc_encaissement_id;
    apercuDocEnc(dataPath, el_apercu, id_document, clilot_id);
}

function apercuDocEnc(data, el, id_doc, cliLot_id) {
    viderApercuEnc(cliLot_id);
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuEnc_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuEnc_' + cliLot_id).appendChild(element);
    }
}

function viderApercuEnc(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuEnc_" + cliLot_id) != undefined) {
        if (document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuEnc_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}


$(document).on('click', '.supprimer_ficher', function () {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': $(this).attr('data-id'),
        'cliLot_id': $(this).attr('data-cli_lot_id'),
    };
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.supprimerFile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'returnDelete_file', loading);
});

function returnDelete_file(response) {
    if (response.status == 200) {
        $(`.doc_encaissement_${response.data_retour.doc_encaissement_id}`).remove();
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

/****** finSaisi_encais ******/


$(document).on('click', '#fin_saisi_encaissement', function () {
    var data_request = {
        'annee': $(this).attr('data-annee'),
        'action': 'demande',
        'cliLot_id': $(this).attr('data-cliLot_id'),
    };
    finir_saisi_encais(data_request);
});


$(document).on('click', '#fin_saisi_encaissement_2', function () {
    var data_request = {
        'annee': $(this).attr('data-annee'),
        'action': 'demande',
        'cliLot_id': $(this).attr('data-cliLot_id'),
    };
    finir_saisi_encais(data_request);
});

function finir_saisi_encais(data_request) {
    var type_request = 'POST';
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-encaissement"
    }
    var url_request = encaissement_url.finSaisi_encais;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourn_confirm_fin_saisiEnc', loading);
}

function retourn_confirm_fin_saisiEnc(response) {
    var modal_finSaisiEncaissement = document.getElementById('modal-finSaisiEncaissement');

    if (response.status == 200) {
        if (response.action == "demande") {
            $('#titre-modal-finSaisiEncaissement').text(response.data.title);
            $('#text-modal-finSaisiEncaissement').text(response.data.text);
            $('#text-champ-commentaire').text(response.data.commentaire);
            $('#btnConfirm-finSaisiEncaissement').text(response.data.btnConfirm);
            $('#btnAnnuler-finSaisiEncaissement').text(response.data.btnAnnuler);
            $('#confirm-cliLot_id').val(response.data.cliLot_id);

            $(modal_finSaisiEncaissement).modal('show');
            //modal_finSaisiEncaissement.toggle();
        } else {
            //modal_finSaisiEncaissement.toggle();
            $(modal_finSaisiEncaissement).modal('hide');
            Notiflix.Notify.success(response.data.message, { position: 'left-bottom', timeout: 3000 });
            getListeEncaissement(response.data.cliLot_id, $(`#filtre-date-${response.data.cliLot_id}`).val());
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#btnConfirm-finSaisiEncaissement', function () {
    var cliLot_id = $('#confirm-cliLot_id').val();
    var data_request = {
        'annee': $(`#filtre-date-${cliLot_id}`).val(),
        'action': 'confirm',
        'cliLot_id': $('#confirm-cliLot_id').val(),
        'enc_info': $('#confirm-enc_info').val(),
    };
    finir_saisi_encais(data_request);
});



$(document).on('click', '.btn-info-enc', function () {
    var info = $(this).attr('data-info');
    var lang = $(this).attr('data-langVide');
    var language = $(this).attr('data-annuler');
    Notiflix.Confirm.show(
        'information', (($.trim(info) != "") ? info : lang), language,
        () => { }
    );
});
