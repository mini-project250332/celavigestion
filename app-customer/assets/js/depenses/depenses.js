var depenses_url = {
    getDepenses: `${const_app.base_url}Depenses/getDepenses`,
    ficheDepenses: `${const_app.base_url}Depenses/ficheDepenses`,
    deleteCharges: `${const_app.base_url}Depenses/deleteCharges`,
    getPathDepenses: `${const_app.base_url}Depenses/getPathDepenses`,
    getUpdateCharge: `${const_app.base_url}Depenses/getUpdateCharge`,
    removefile: `${const_app.base_url}Depenses/removefile`,
    FinSaisieCharges: `${const_app.base_url}Depenses/FinSaisieCharges`,
}

var timer = null;

function getDepenses() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-depenses"
    }
    var url_request = depenses_url.getDepenses;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getBailCallBack', loading);
}

function getBailCallBack(response) {
    $('#contenaire-depenses').html(response);

}

function getFicheDepensesLot(annee, id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id,
        'annee': annee
    };
    var loading = {
        type: "content",
        id_content: "contenaire-depenses"
    }
    var url_request = depenses_url.ficheDepenses;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheDepenses', loading);
}

function retourficheDepenses(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-depenses-lot_' + identifiant).html(html);
}

function calculCharge_special(cliLot_id) {
    if ($('#montant_charge_ttc_' + cliLot_id).val() && $('#charge_tva').val()) {
        const ttc = parseFloat($('#montant_charge_ttc_' + cliLot_id).val());
        const tva = parseFloat($('#charge_tva_' + cliLot_id).val());
        const ht = ttc - tva;
        $('#charge_ht_' + cliLot_id).val(ht.toFixed(2));
        $('#charge_tva_' + cliLot_id).val(tva);
        $('#charge_ttc_' + cliLot_id).val(ttc.toFixed(2));
    }
}

function calculCharge(cliLot_id) {
    if ($('#montant_charge_ttc_' + cliLot_id).val()) {
        const ttc = parseFloat($('#montant_charge_ttc_' + cliLot_id).val());
        const taux_tva = parseFloat($('#charge_tva_taux_' + cliLot_id).val());
        const ht = ttc / (1 + (taux_tva / 100));
        const tva = (ht * taux_tva) / 100;
        $('#charge_ht_' + cliLot_id).val(ht.toFixed(2));
        $('#charge_tva_' + cliLot_id).val(tva.toFixed(2));
        $('#charge_ttc_' + cliLot_id).val(ttc.toFixed(2));
    }
}

function deleteCharges(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = depenses_url.deleteCharges;

    var loading = {
        type: "content",
        id_content: "contenaire-depenses"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'deleteChargesCallback', loading);
}

function deleteChargesCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        charge_id: response.data.charge_id
                    }
                    deleteCharges(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            $(`tr[id=charge_row-${response.data.charge_id}]`).remove();
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getFicheDepensesLot(response.charge_info.annee, response.charge_info.cliLot_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function apercuDocumentDepenses(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = depenses_url.getPathDepenses;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsePathDepenses');
}

function responsePathDepenses(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercudocDepenses_' + cliLot_id);
    var dataPath = response[0].doc_charge_path;
    var id_document = response[0].doc_charge_id;

    apercuDocDepenses(dataPath, el_apercu, id_document, cliLot_id);
}

function apercuDocDepenses(data, el, id_doc, cliLot_id) {
    viderApercuDepenses(cliLot_id);
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuDepenses_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuDepenses_' + cliLot_id).appendChild(element);
    }
}

function viderApercuDepenses(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercuDepenses_");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuDepenses_" + cliLot_id) != undefined) {
        if (document.getElementById("apercuDepenses_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercuDepenses_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuDepenses_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}

function updateCharge(charge_id, cLilot_id) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'charge_id': charge_id,
        'cLilot_id': cLilot_id
    };
    var url_request = depenses_url.getUpdateCharge;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'updateChargeCallback');
}

function updateChargeCallback(response) {
    var langue = $('#langue').val();
    if (langue == 'english') {
        $('.charge_modal').text("Editing an expense");
    } else {
        $('.charge_modal').text("Modification d'une dépense");
    }

    $('#charge_id').val(response.data_retour.charge_id);
    $('#type_charge_id_' + response.data_retour.cliLot_id).val(response.data_retour.type_charge_id);
    $('#montant_charge_ttc_' + response.data_retour.cliLot_id).val(response.data_retour.montant_ttc);
    $('#date_facture_' + response.data_retour.cliLot_id).val(response.data_retour.date_facture);
    $('#charge_ht_' + response.data_retour.cliLot_id).val(response.data_retour.montant_ht);
    $('#charge_tva_taux_' + response.data_retour.cliLot_id).val(response.data_retour.tva_taux);
    $('#charge_tva_' + response.data_retour.cliLot_id).val(response.data_retour.tva);
    $('#charge_ttc_' + response.data_retour.cliLot_id).val(response.data_retour.montant_ttc);
    $('#depense_comment_' + response.data_retour.cliLot_id).val(response.data_retour.c_charge_commentaire);

    if (Array.isArray(response.form_validation)) {
        var docNames = "";
        $.each(response.form_validation, function (index, doc) {
            docNames += `<div class="doc_${doc.doc_charge_id}">
                        <span class="name_doc badge rounded-pill badge-soft-secondary" style="cursor: pointer;">${doc.doc_charge_nom}</span>
                        <span class="badge rounded-pill badge-soft-light btn-close text-white" data-id =${doc.doc_charge_id} id="removefile"  style="cursor: pointer;" title="Supprimer fichier">.</span>
                        </div>`;
        });

        $(".name_doc").html(docNames);
    }
}

$(document).on('click', '#removefile', function (e) {
    var id = $(this).attr('data-id');
    removefile(id);
});

function removefile(doc_charge_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        doc_charge_id: doc_charge_id,
    };

    var url_request = depenses_url.removefile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'removefileCallBack');
}

function removefileCallBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == true) {
        $('.doc_' + obj.data_retour.doc_charge_id).empty();
        Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function FinSaisieCharges2(data) {

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = depenses_url.FinSaisieCharges;

    var loading = {
        type: "content",
        id_content: "contenaire-depenses"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'FinSaisieChargesCallback', loading);
}

function FinSaisieChargesCallback2(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        charge_id: response.data.charge_id
                    }
                    FinSaisieCharges(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getFicheDepensesLot(response.charge_info.annee, response.charge_info.cliLot_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function FinSaisieCharges(data) {

    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = depenses_url.FinSaisieCharges;

    var loading = {
        type: "content",
        id_content: "contenaire-depenses"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'FinSaisieChargesCallback', loading);
}

function FinSaisieChargesCallback(response) {
    var modal_finSaisiEncaissement = document.getElementById('modal-finSaisiEncaissement');
    if (response.status == 200) {
        if (response.action == "demande") {

            $('#titre-modal-finSaisiEncaissement').text(response.data.title);
            $('#text-modal-finSaisiEncaissement').text(response.data.text);
            $('#text-champ-commentaire').text(response.data.commentaire);
            $('#btnConfirm-finSaisiEncaissement').text(response.data.btnConfirm);
            $('#btnAnnuler-finSaisiEncaissement').text(response.data.btnAnnuler);
            $('#confirm-cliLot_id').val(response.data.cliLot_id);
            $('#confirm-charge_id').val(response.data.charge_id);


            $(modal_finSaisiEncaissement).modal('show');

        } else {
            $(modal_finSaisiEncaissement).modal('hide');
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            getFicheDepensesLot(response.data.annee, response.data.cliLot_id);
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}

$(document).on('click', '#btnConfirm-finSaisiEncaissement', function () {
    var cliLot_id = $('#confirm-cliLot_id').val();
    var data_request = {
        'annee': $(`#annee_charges_${cliLot_id}`).val(),
        'action': 'confirm',
        'cliLot_id': $('#confirm-cliLot_id').val(),
        'enc_info': $('#confirm-enc_info').val(),
        'charge_id': $('#confirm-charge_id').val(),
    };
    console.log(data_request);
    FinSaisieCharges(data_request);
});


$(document).on('click', '.btn_info_dep', function () {
    var info = $(this).attr('data-info');
    var lang = $(this).attr('data-langVide');
    var language = $(this).attr('data-annuler');
    Notiflix.Confirm.show(
        'information', (($.trim(info) != "") ? info : lang), language,
        () => { }
    );
});