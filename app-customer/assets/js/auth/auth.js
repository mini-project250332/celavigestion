var mail_url = {
    sendMailReinitPassword: `${const_app.base_url}Mail/envoi_reinit_Mail`,
    sendNewPassword: `${const_app.base_url}Auth/newPassword`,
    encrypt: `${const_app.base_url}Auth/encrypt_forgot_password`,
    decrypt: `${const_app.base_url}Auth/decrypt_forgot_password`,
    verify_email: `${const_app.base_url}Auth/verify_email`,
    checkUtilLogin: `${const_app.base_url}Auth/checkUtilLogin`,
    checkUtil: `${const_app.base_url}Auth/checkUtil`,
    savePasswrd: `${const_app.base_url}Auth/svePasswrd`,
}

var timer = null;

$(document).on('submit', '#authApp', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "div-signin-box"
    }
    fn.ajax_form_data_custom(form, type_output, "retour_authentification", loading);
});

function retour_authentification(response) {
    var text = response.form_validation.message, icon = "", type_alert = "";
    if (response.status == true) {
        icon = (response.data_retour == 1) ? `<i class="bi bi-check-circle"></i>` : `<i class="bi bi-exclamation-circle"></i>`;
        type_alert = (response.data_retour == 1) ? "alert-success" : "alert-warning";

        var message = `
            <div class="alert ${type_alert} border-2 d-flex align-items-center p-1 w-100" role="alert">
                ${icon}
                <p class="mb-0 flex-1 fs--1 text-center">${text}</p>
                <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`;
        grecaptcha.reset();
    } else {
        if (response.data_retour == null) {
            control_data_form_complexe_return(response);
            message = "";
            grecaptcha.reset();
        } else {
            message = `
                <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                    <i class="bi bi-x-circle me-1"></i>
                    <p class="mb-0 flex-1 fs--1 text-center">${text}</p>
                    <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
            grecaptcha.reset();
        }
    }

    $('#div-message').html(message);
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    if (response.data_retour == 1) {
        timer = setTimeout(() => {
            checkUtilLogin($('#util_login').val());
        }, 1000);
    } else {
        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 3000);
    }
}

function checkUtilLogin(util_login) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'util_login': util_login
    };

    var url_request = mail_url.checkUtilLogin;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'checkUtilLoginCallBack');
}

function checkUtilLoginCallBack(response) {
    if (response.trim() !== '') {
        try {
            const obj = JSON.parse(response);
            if (obj.etat_user != 0) {
                window.location.href = `${const_app.base_url}Menu`;
            }
            else {
                sessionStorage.setItem("myVar", obj.util_id);
                window.location.href = `${const_app.base_url}Auth/change_pass`;
            }
        } catch (error) {
            console.error('Erreur lors de l\'analyse JSON :', error);
        }
    } else {
        window.location.href = `${const_app.base_url}Auth/login`;
    }
}

function checkUtil(util_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'util_id': util_id
    };

    var url_request = mail_url.checkUtil;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'checkUtilCallBack');
}

function checkUtilCallBack(response) {
    const obj = JSON.parse(response);
    $('#util_id').val(obj.util_id);
    $('#identifiant_change').val(obj.util_login);
}

$(document).on('click', '#btn_change_password', function () {
    var lang_code = $(this).attr('data-id');
    if ($('#identifiant_change').val() && $('#new_password').val() && $('#confirm_password').val()) {
        if ($('#new_password').val() === $('#confirm_password').val()) {
            var newPassword = $('#new_password').val();
            var minNumberofChars = 8;
            var bool_password = true;
            var regularExpression = /^(?=.*[0-9])(?=.*[! "#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[a-zA-Z0-9! "#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{6,16}$/;
            if (newPassword.length < minNumberofChars) {
                bool_password = false;
            }
            if (!regularExpression.test(newPassword)) {
                bool_password = false;
            }
            if (bool_password == true) {
                var type_request = 'POST';
                var type_output = 'html';
                var data_request = {
                    'util_login': $('#identifiant_change').val(),
                    'password': $('#new_password').val(),
                    'util_id': $('#util_id').val(),
                    'g_recaptcha_response': $("#g-recaptcha-response").val()
                };

                var url_request = mail_url.savePasswrd;
                fn.ajax_request(type_request, url_request, type_output, data_request, 'savePasswrdCallback');
            }
            else {
                if (lang_code == 'french') {
                    message = `
                    <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                        <i class="bi bi-x-circle me-1"></i>
                        <p class="mb-0 flex-1 fs--1 text-center">Attention, le mot de passe saisi ne respecte pas les règles de sécurité. 
                        Vous devez saisir un mot de passe qui comporte au minimum 8 caractères, dont au moins un minuscule, un majuscule, un chiffre et un caractère spécial.</p>
                        <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`;
                }
                else {
                    message = `
                    <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                        <i class="bi bi-x-circle me-1"></i>
                        <p class="mb-0 flex-1 fs--1 text-center">Please note that the password you have entered does not comply with security regulations.  
                        You must enter a password with at least 8 characters, including at least one lowercase, one uppercase, one number and one special character.</p>
                        <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`;
                }

                $('#div-message').html(message);
                grecaptcha.reset();
                if (timer != null) {
                    clearTimeout(timer);
                    timer = null;
                }
                timer = setTimeout(() => {
                    $('#div-message').html("");
                }, 6000);
            }
        }
        else {
            if (lang_code == 'french') {
                message = `
                <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                    <i class="bi bi-x-circle me-1"></i>
                    <p class="mb-0 flex-1 fs--1 text-center">Les mots de passe ne correspondent pas</p>
                    <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
            } else {
                message = `
                <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                    <i class="bi bi-x-circle me-1"></i>
                    <p class="mb-0 flex-1 fs--1 text-center">Passwords don't match</p>
                    <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
            }

            $('#div-message').html(message);
            grecaptcha.reset();
            if (timer != null) {
                clearTimeout(timer);
                timer = null;
            }
            timer = setTimeout(() => {
                $('#div-message').html("");
            }, 3000);
        }
    }
    else {
        if (lang_code == 'french') {
            message = `
            <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                <i class="bi bi-x-circle me-1"></i>
                <p class="mb-0 flex-1 fs--1 text-center">Veuillez remplir tous les champs</p>
                <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`;
        } else {
            message = `
            <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                <i class="bi bi-x-circle me-1"></i>
                <p class="mb-0 flex-1 fs--1 text-center">Please fill in all fields</p>
                <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`;
        }

        $('#div-message').html(message);
        grecaptcha.reset();
        if (timer != null) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 3000);
    }
});

function savePasswrdCallback(response) {
    const obj = JSON.parse(response);
    if (obj.status == true) {
        Notiflix.Notify.success(obj.message, { position: 'left-bottom', timeout: 2000 });
        window.location.href = `${const_app.base_url}Menu`
    }
    else {
        message = `
        <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
            <i class="bi bi-x-circle me-1"></i>
            <p class="mb-0 flex-1 fs--1 text-center">${obj.message}</p>
            <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>`;
        $('#div-message').html(message);
        grecaptcha.reset();
        if (timer != null) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 3000);
    }
}

$(document).on('click', '#btn_annuler_forgetpass', function () {
    window.location.href = `${const_app.base_url}Auth/RetournerLogin/` + $(this).attr('data-id');
});

function handleSubmit() {
    var recaptcha = true;
    var recaptchaResponse = grecaptcha.getResponse();

    if (!recaptchaResponse) {
        recaptcha = false;
    }
    return recaptcha;
}

// Envoi email de reinitialisation de mot de passe
$(document).on('click', '#btn_reinit_password', function () {
    var lang_code = $('#lang_code').val()
    var text = lang_code == 'french' ? 'Ce champ ne doit pas être vide.' : 'This field must not be empty.';

    if (!$('#email_reinit_password').val()) {
        $('.help_email_reinit_password').text(text);
        $('.help_email_reinit_password').css('color', 'red');
        grecaptcha.reset();
        return false;
    }

    if (handleSubmit() == false) {
        var txt_recaptcha = lang_code == 'french' ? 'Veuillez cocher la case reCaptcha' : 'Please check the reCaptcha box';
        message = `
        <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
            <i class="bi bi-x-circle me-1"></i>
            <p class="mb-0 flex-1 fs--1 text-center">${txt_recaptcha}</p>
            <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>`;
        $('#div-message').html(message);

        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 3000);
        return false;
    }

    var email = $('#email_reinit_password').val();
    var emailData = {
        'email': email
    };
    $('#btn_reinit_password').prop("disabled", true);
    $('#btn_reinit_password').prop("disabled", true);

    $.ajax({
        type: 'POST',
        url: mail_url.verify_email,
        data: emailData,
        dataType: 'json',
        error: (result) => {
            $('#btn_reinit_password').prop("disabled", false);
            $('#btn_reinit_password').prop("disabled", false);
            if (result.status == 403) {
                if ($('#lang_code').val() == 'french') {
                    $('.content_change').empty();
                    $('.content_change').append(
                        "<label style='font-size: 13px;'>L'adresse email que vous avez renseignée ne correspond à aucun compte de ce site.<br>"
                        + " Pas de réinitialisation possible. Veuillez prendre contact avec notre service commercial pour accéder à votre compte.</label>"
                        + "<div class='col text-end'>"
                        + "<button class='btn btn-default btn_annuler_forgetpass' id='btn_annuler_forgetpass' data-id='" + $('#lang_code').val() + "' style='margin-top: 4%;'>"
                        + "<span>Retour</span>"
                        + "</button>&nbsp;"
                        + "</div>"
                    );
                } else {
                    $('.content_change').empty();
                    $('.content_change').append(
                        "<label style='font-size: 13px;'>The email address you entered does not correspond to any account on this site.<br>"
                        + " No reset is possible. Please contact our sales department to access your account.</label>"
                        + "<div class='col text-end'>"
                        + "<button class='btn btn-default btn_annuler_forgetpass' id='btn_annuler_forgetpass' data-id='" + $('#lang_code').val() + "' style='margin-top: 4%;'>"
                        + "<span>Return</span>"
                        + "</button>&nbsp;"
                        + "</div>"
                    );
                }
                return false;
            } else if (result.status == 200) {
                sendMailReinitPassword(email.trim(), result.responseText);
            }
        }
    });
});

function sendMailReinitPassword(email, util_nom) {
    var type_request = 'POST';
    var type_output = 'html';

    var postForm = {
        'text': email
    };

    emailCrypted = "";

    $.ajax({
        type: 'POST',
        url: mail_url.encrypt,
        data: postForm,
        dataType: 'json',
        success: function (data) {
            emailCrypted = data;
            var data_request = {
                destinataire: email,
                utilNom: util_nom,
                emailCrypted: JSON.stringify(emailCrypted),
            };

            var url_request = mail_url.sendMailReinitPassword;

            var loading = {
                type: "content",
                id_content: "container-reinit-password"
            }

            $('#btn_annuler_forgetpass').prop("disabled", true);
            $('#btn_reinit_password').prop("disabled", true);

            if (validation_mail(email) == true) {
                fn.ajax_request(type_request, url_request, type_output, data_request, 'sendMailReinitPasswordCallBack', loading);
            } else {
                Notiflix.Notify.failure("Email invalide", { position: 'left-bottom', timeout: 3000 });
                $('#btn_annuler_forgetpass').prop("disabled", false);
                $('#btn_reinit_password').prop("disabled", false);
            }
            console.log(" *** sent *** ");
        }
    });
}

function sendMailReinitPasswordCallBack(response) {
    obj = JSON.parse(response);

    if (obj.status == 500) {
        Notiflix.Notify.failure("Mail non envoyé", { position: 'left-bottom', timeout: 3000 });
    }
    else {
        $('.content_change').empty();
        if (obj.email.lang_code == 'french') {
            $('.content_change').append(
                "<label style='font-size: 13px;'>Un email vous a été envoyé à l'adresse <b>" + obj.email.email + "</b> pour vous permettre de réinitialiser votre mot de passe.</label>"
                + "<div class='col text-end'>"
                + "<button class='btn btn-default btn_annuler_forgetpass' id='btn_annuler_forgetpass' data-id='" + obj.email.lang_code + "' style='margin-top: 4%;'>"
                + "<span>Retour</span>"
                + "</button>&nbsp;"
                + "</div>"
            );
        } else {
            $('.content_change').append(
                "<label style='font-size: 13px;'>An email has been sent to <b>" + obj.email.email + "</b> to enable you to reset your password.</label>"
                + "<div class='col text-end'>"
                + "<button class='btn btn-default btn_annuler_forgetpass' id='btn_annuler_forgetpass'  data-id='" + obj.email.lang_code + "' style='margin-top: 4%;'>"
                + "<span>Cancel</span>"
                + "</button>&nbsp;"
                + "</div>"
            );
        }
    }
    $('.lang_logo ').addClass('d-none');
    $('#btn_annuler_forgetpass').prop("disabled", false);
    $('#btn_reinit_password').prop("disabled", false);
}

function validation_mail(email) {
    var email_check = true;
    if (email.includes(",") == true) {
        var list_email = email.split(',');
        var arrayLength = list_email.length;
        for (var i = 0; i < arrayLength; i++) {
            if (isEmail(list_email[i].trim()) == false) {
                email_check = false;
                console.log(list_email[i]);
                break;
            }
            else {
                email_check = true;
            }
        }
    } else {
        email_check = isEmail(email);
    }
    return email_check;
}

function isEmail(email) {
    var EmailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return EmailRegex.test(email);
}

// Send new password
$(document).on('click', '#btn_send_new_password', function () {
    var type_request = 'POST';
    var type_output = 'text';

    var email = $('#email_dest').val();
    var password = $('#new_password_reinit').val();
    var confirmpassword = $('#confirm_password_reinit').val();

    if (password == "" || password == " ") {
        var mess1 = $(this).attr('data-id') == 'french' ? "Mot de passe invalide" : "Invalid password";
        Notiflix.Notify.failure(mess1, { position: 'left-bottom', timeout: 3000 });
        grecaptcha.reset();
        return false;
    }

    if (password !== confirmpassword) {
        var mess2 = $(this).attr('data-id') == 'french' ? "Les mots de passe ne correspondent pas" : "The passwords don't match";
        Notiflix.Notify.failure(mess2, { position: 'left-bottom', timeout: 3000 });
        grecaptcha.reset();
        return false;
    }

    var regularExpression = /^(?=.*[0-9])(?=.*[! "#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[a-zA-Z0-9! "#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{6,16}$/;

    if (!regularExpression.test(password)) {
        var mess3 = $(this).attr('data-id') == 'french' ? "Attention, le mot de passe saisi ne respecte pas les règles de sécurité." : "Warning, the password entered does not comply with security rules.";
        Notiflix.Notify.failure(mess3, { position: 'left-bottom', timeout: 3000 });
        grecaptcha.reset();
        return false;
    }

    var data_request = {
        email: email,
        lang_code: $(this).attr('data-id'),
        password: password + "",
        g_recaptcha_response: $("#g-recaptcha-response").val()
    };

    var url_request = mail_url.sendNewPassword;

    var loading = {
        type: "content",
        id_content: "container-new-password"
    }

    $('#btn_send_new_password').prop("disabled", true);

    if (validation_mail(email) == true) {
        fn.ajax_request(type_request, url_request, type_output, data_request, 'sendNewPasswordCallBack', loading);
    } else {
        Notiflix.Notify.failure("Email invalide", { position: 'left-bottom', timeout: 3000 });
        $('#btn_send_new_password').prop("disabled", false);
        grecaptcha.reset();
    }
});

function sendNewPasswordCallBack(response) {
    $('#btn_send_new_password').prop("disabled", false);
    const obj = JSON.parse(response);
    if (obj.status == true) {
        window.location.href = const_app.base_url + "" + obj.Auth;
    } else {
        message = `
                <div class="alert alert-danger border-2 d-flex align-items-center p-1 w-100" role="alert">
                    <i class="bi bi-x-circle me-1"></i>
                    <p class="mb-0 flex-1 fs--1 text-center">${obj.message}</p>
                    <button class="btn-close fs--2" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`;
        $('#div-message').html(message);
        grecaptcha.reset();
        if (timer != null) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(() => {
            $('#div-message').html("");
        }, 3000);
    }
}
