var bail_url = {
    getBail: `${const_app.base_url}Bail/getBail`,
    ficheBail: `${const_app.base_url}Bail/ficheBail`,
    getpathDocbail: `${const_app.base_url}Bail/getpathDocbail`,
}

var timer = null;

function getDocBail() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-bail"
    }
    var url_request = bail_url.getBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getBailCallBack', loading);
}

function getBailCallBack(response) {
    $('#contenaire-bail').html(response);
}

function getFicheBailLot(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-bail"
    }
    var url_request = bail_url.ficheBail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheBail', loading);
}

function retourficheBail(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-bail-lot_' + identifiant).html(html);
}

function apercuDocumentBail(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = bail_url.getpathDocbail;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathbail');
}

function responsepathbail(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercudocBail_' + cliLot_id)
    var dataPath = response[0].doc_bail_path
    var id_document = response[0].doc_bail_id
    apercuBail(dataPath, el_apercu, id_document, cliLot_id);
}

function apercuBail(data, el, id_doc, cliLot_id) {
    viderApercuBail(cliLot_id);
    // var doc_selectionner = document.getElementById("docrowbail-" + id_doc);
    // doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuBail_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuBail_' + cliLot_id).appendChild(element);
    }
}

function viderApercuBail(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercu_doc");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercudocBail_" + cliLot_id) != undefined) {
        if (document.getElementById("apercudocBail_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercudocBail_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercudocBail_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}