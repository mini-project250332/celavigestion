var liste_facture_url = {
    getListDossier: `${const_app.base_url}Liste_facture/getListDossier`,
    ficheDossier: `${const_app.base_url}Liste_facture/ficheDossier`,
    getfichierfactureCelavi: `${const_app.base_url}Liste_facture/getfichierfactureCelavi`,
    apercuFacture: `${const_app.base_url}Liste_facture/apercuFacture`,
}

function listeDossierFacture() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-liste-dossier"
    }
    var url_request = liste_facture_url.getListDossier;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'load_listeDossierFacture', loading);
}
function load_listeDossierFacture(response) {
    $('#contenaire-liste-dossier').html(response);
}


//Facture
function getfichierfactureCelavi(id_dossier) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'dossier_id': id_dossier,
    };
    var loading = {
        type: "content",
        id_content: "vos-fichier-celavigestion"
    }
    var url_request = liste_facture_url.getfichierfactureCelavi;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetfichierfactureCelavi', loading);
}
function retourgetfichierfactureCelavi(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#vos-fichier-celavigestion_' + identifiant).html(html);
    // $('#vos-fichier-celavigestion').html(response);
}

function apercuFacture(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = liste_facture_url.apercuFacture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseapercuFacture');
}

function responseapercuFacture(response) {
    console.log(response);
    var el_apercu = document.getElementById('apercu_facture_CELAVI');
    var dataPath = response.doc_facturation_path;
    var id_document = response.doc_id;

    apercuDocFacture(dataPath, el_apercu, id_document);
}

function apercuDocFacture(data, el, id_doc) {
    viderApercuFacture();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector("#apercu_facture_CELAVI").appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector("#apercu_facture_CELAVI").appendChild(element);
    }
}

function viderApercuFacture() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercu_facture_CELAVI") != undefined) {
        if (document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu") != null) {
            document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu").remove();
        }
    }
}