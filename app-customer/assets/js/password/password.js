var mdp_url = {
    loadPwd: `${const_app.base_url}Password/loadPassword`,
    ficheClient_page : `${const_app.base_url}Password/pageficheClient`,
    formMdp_page : `${const_app.base_url}Password/pageformPwd`,
}

var timer = null;

function pgPassword() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-mdp"
    }
    var url_request = mdp_url.loadPwd;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'loadPwdCallBack', loading);
}

function loadPwdCallBack(response) {
    $('#contenaire-mdp').html(response);
}

function getFicheMdpClient(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'client_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-mdp"
    }
    var url_request = mdp_url.formMdp_page;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourFicheMdp', loading);
}

function retourFicheMdp(response) {
    $('#fiche-mdp-client').html(response);
}

$(document).on('submit', '#updateMdp', function (e) {
    e.preventDefault();
    var form = $(this);
    var type_output = 'json';
    var loading = {
        type: "content",
        id_content: "contenaire-mdp"
    }
    fn.ajax_form_data_custom(form, type_output, "retourupdateMdp", loading);
});

function retourupdateMdp(response) {
    if (response.status == true) {
        console.log(response);
        Notiflix.Notify.success(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        getFicheMdpClient(response.data_retour.value.client_id);
    } else {
        Notiflix.Notify.failure(response.form_validation.message, { position: 'left-bottom', timeout: 3000 });
        fn.response_control_champ(response);
    }
}
