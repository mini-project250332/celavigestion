var mandat_url = {
    getMandat: `${const_app.base_url}Mandat/getMandat`,
    ficheMandat: `${const_app.base_url}Mandat/ficheMandat`,
    getPath: `${const_app.base_url}Mandat/getPath`,
    getPathFicJustificatif: `${const_app.base_url}Mandat/getPathFicJustificatif`,
    getfichierfactureCelavi: `${const_app.base_url}Mandat/getfichierfactureCelavi`,
    apercuFacture: `${const_app.base_url}Mandat/apercuFacture`,
}

var timer = null;

function getMandat() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-mandat"
    }
    var url_request = mandat_url.getMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getMandatCallBack', loading);
}

function getMandatCallBack(response) {
    $('#contenaire-mandat').html(response);
}

function getFicheMandatLot(id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        'cliLot_id': id
    };
    var loading = {
        type: "content",
        id_content: "contenaire-mandat"
    }
    var url_request = mandat_url.ficheMandat;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourficheMandat', loading);
}

function retourficheMandat(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#fiche-mandat-lot_' + identifiant).html(html);
}

function apercuMandat(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = mandat_url.getPath;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandat');
}

function responsepathMandat(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercu_list_docMandat_' + cliLot_id);
    var dataPath = response[0].mandat_path;
    var id_document = response[0].mandat_id;

    apercuDocMandat(dataPath, el_apercu, id_document, cliLot_id);
}

function apercuFicJustificatif(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = mandat_url.getPathFicJustificatif;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responsepathMandatFic');
}

function responsepathMandatFic(response) {
    var cliLot_id = response[0].cliLot_id;
    var el_apercu = document.getElementById('apercu_list_docMandat_' + cliLot_id);
    var dataPath = response[0].mandat_fichier_justificatif;
    var id_document = response[0].mandat_id;

    apercuDocMandat(dataPath, el_apercu, id_document);
}

function apercuDocMandat(data, el, id_doc, cliLot_id) {
    viderApercuMandat(cliLot_id);
    // var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    // doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector('#apercuMandat_' + cliLot_id).appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector('#apercuMandat_' + cliLot_id).appendChild(element);
    }
}

function viderApercuMandat(cliLot_id) {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercuMandat_" + cliLot_id) != undefined) {
        if (document.getElementById("apercuMandat_" + cliLot_id).querySelector("#view_apercu") != null) {
            document.getElementById("apercuMandat_" + cliLot_id).querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercuMandat_" + cliLot_id).querySelector("#view_apercu").remove();
        }
    }
}

function getfichierfactureCelavi() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "vos-fichier-celavigestion"
    }
    var url_request = mandat_url.getfichierfactureCelavi;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'retourgetfichierfactureCelavi', loading);
}
function retourgetfichierfactureCelavi(response) {
    $('#vos-fichier-celavigestion').html(response);
}

function apercuFacture(id_doc) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = {
        'id_doc': id_doc,
    };
    var url_request = mandat_url.apercuFacture;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'responseapercuFacture');
}

function responseapercuFacture(response) {
    console.log(response);
    var el_apercu = document.getElementById('apercu_facture_CELAVI');
    var dataPath = response.doc_facturation_path;
    var id_document = response.doc_id;

    apercuDocFacture(dataPath, el_apercu, id_document);
}

function apercuDocFacture(data, el, id_doc) {
    viderApercuFacture();
    var doc_selectionner = document.getElementById("rowdoc-" + id_doc);
    doc_selectionner.style.backgroundColor = '#abb1ba';
    disponible = ['txt', 'png', 'jpeg', 'jpg', 'pdf', 'gif', 'PNG', 'JPG', 'PDF', 'TXT', 'mp4', 'avi', 'mp3', 'mkv', 'm4a', 'mpeg', 'mpg', 'AMR', 'GIF', 'MKV', 'MP4', 'WebM'];
    var nb = data.split('/').length
    var nom_ressource = data.split('/')[nb - 1]
    var nbRessource = nom_ressource.split('.').length
    var type = nom_ressource.split('.')[nbRessource - 1]

    var element = document.createElement("object");
    if (disponible.indexOf(type) == -1) {
        element.setAttribute("style", "height:100%; width: 100%");
        el.querySelector('#nonApercu').setAttribute('style', 'display : block');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
    } else if (type == "pdf" || type == "PDF") {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element = document.createElement('iframe');
        document.getElementById('bloc_controle').setAttribute('style', 'display : none');
        element.setAttribute('src', '/assets/pdf/web/viewer.html?file=' + `${const_app.base_url}` + data);
        element.setAttribute('class', "pdfembed");
        element.setAttribute('style', "width: 100%; height:700px");
        element.setAttribute('type', "text/html");
        element.setAttribute('frameborder', "0");
        element.setAttribute('id', "view_apercu");
        el.querySelector("#apercu_facture_CELAVI").appendChild(element)
    } else {
        el.querySelector('#nonApercu').setAttribute('style', 'display : none');
        document.getElementById('bloc_controle').setAttribute('style', 'display : block');

        element.setAttribute("style", "height:100%; width: 100% ;");
        element.setAttribute("id", "view_apercu");
        element.setAttribute("data", `${const_app.base_url}` + data)
        el.querySelector("#apercu_facture_CELAVI").appendChild(element);
    }
}

function viderApercuFacture() {
    const doc_selectionne = document.getElementsByClassName("apercu_docu");
    Object.entries(doc_selectionne).forEach(([key, value]) => {
        value.style.backgroundColor = ""
    })
    if (document.getElementById("apercu_facture_CELAVI") != undefined) {
        if (document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu") != null) {
            document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu").setAttribute("style", "display : none");
            document.getElementById("apercu_facture_CELAVI").querySelector("#view_apercu").remove();
        }
    }
}