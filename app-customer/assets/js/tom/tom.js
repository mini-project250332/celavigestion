var data_url = {
    getListTom: `${const_app.base_url}Tom/getListTom`,
    getformtax: `${const_app.base_url}Tom/getformtax`,
    tableHistorique: `${const_app.base_url}Tom/tableHistorique`,
    saveTom: `${const_app.base_url}Tom/saveTom`,
    removefile: `${const_app.base_url}Tom/removefile`,
    finSaisietaxe : `${const_app.base_url}Tom/finSaisietaxe`,
}

var timer = null;

function getListTom() {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {};
    var loading = {
        type: "content",
        id_content: "contenaire-tom"
    }
    var url_request = data_url.getListTom;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getListTomCallBack', loading);
}
function getListTomCallBack(response) {
    $('#contenaire-tom').html(response);
    var lastActiveTab = localStorage.getItem("lastActiveTab");
    if (lastActiveTab) {
        $(".nav-link").each(function () {
            if ($('button').hasClass('active')) {
                $('button').removeClass('active');
            }
        });
        $('#nav-lot_' + lastActiveTab + '-tab').addClass('active');

        $(".tab-pane").each(function () {
            if ($('.tab-pane').hasClass('active show')) {
                $('.tab-pane').removeClass('active show');
            }
        });
        $('#nav-lot_' + lastActiveTab).addClass('show active');
        localStorage.removeItem("lastActiveTab");
    }
}

function getformtax(cliLot_id) {
    var currentDate = new Date();
    var year = currentDate.getFullYear();

    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id,
        year: year
    };
    var loading = {
        type: "content",
        id_content: "content-tom-customer_" + cliLot_id
    }
    var url_request = data_url.getformtax;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'getformtaxCallBack', loading);
}
function getformtaxCallBack(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#content-tom-customer_' + identifiant).html(html);
}

function tableHistorique(cliLot_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        cliLot_id: cliLot_id,
    };
    var loading = {
        type: "content",
        id_content: "content-tom-customer_" + cliLot_id
    }
    var url_request = data_url.tableHistorique;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'tableHistoriqueCallBack', loading);
}
function tableHistoriqueCallBack(response) {
    var identifiant = response.substring(0, 100).split("@@@@@@@")[0];
    var html = response.split("@@@@@@@")[1];
    $('#content-tom-customer_' + identifiant).html(html);
}

function saveTom(cliLot_id, lang) {
    $('.help_taxe_tom_' + cliLot_id).empty();
    $('.help_taxe_montant_' + cliLot_id).empty();
    $('.help_file_' + cliLot_id).empty();
    var alert_vide = lang == 'french' ? 'Ce champ ne doit pas être vide.' : 'This field must not be empty.';
    var alert_file = lang == 'french' ? "Seuls les fichiers PDF et image sont autorisés. Taille maximum 10 Mo." : "Only PDF and image files are allowed. Maximum size 10 MB.";

    if (!$('#taxe_montant_' + cliLot_id).val()) {
        $('.help_taxe_montant_' + cliLot_id).text(alert_vide);
    }

    if (!$('#taxe_tom_' + cliLot_id).val()) {
        $('.help_taxe_tom_' + cliLot_id).text(alert_vide);
    }

    if ($('#file_' + cliLot_id)[0].files.length < 1) {
        $('.help_file_' + cliLot_id).text(alert_vide);
    }

    if ($('#taxe_montant_' + cliLot_id).val() && $('#taxe_tom_' + cliLot_id).val() && $('#file_' + cliLot_id)[0].files.length > 0) {
        var formData = new FormData();
        formData.append('taxe_montant', $('#taxe_montant_' + cliLot_id).val());
        formData.append('taxe_tom', $('#taxe_tom_' + cliLot_id).val());
        formData.append('taxe_id', $('#taxe_id_' + cliLot_id).val());
        formData.append('cliLot_id', $('#cliLot_id_' + cliLot_id).val());

        var fileInput = $('#file_' + cliLot_id)[0];
        var selectedFiles = fileInput.files;

        for (var i = 0; i < selectedFiles.length; i++) {
            if (selectedFiles[i].size <= 10 * 1024 * 1024) {
                formData.append(i, selectedFiles[i]);
                $('.msg_' + cliLot_id).text(alert_file);
                $('.msg_' + cliLot_id).removeClass('text-danger');
            } else {
                var text_mo_file = lang == 'french' ? "Le fichier " + selectedFiles[i].name + " dépasse la limite de 10 Mo." : "The file " + selectedFiles[i].name + " exceeds the 10 MB limit.";
                $('.msg_' + cliLot_id).text(text_mo_file);
                $('.msg_' + cliLot_id).addClass('text-danger');
                return false;
            }
        }

        $.ajax({
            url: data_url.saveTom,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                const obj = JSON.parse(response);
                if (obj.status == true) {
                    localStorage.setItem("lastActiveTab", obj.data_retour.cliLot_id);
                    getListTom();
                    Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
                }
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
}

$(document).on('click', '#removefile', function (e) {
    var id = $(this).attr('data-id');
    removefile(id);
});

function removefile(doc_taxe_id) {
    var type_request = 'POST';
    var type_output = 'html';
    var data_request = {
        doc_taxe_id: doc_taxe_id,
    };

    var url_request = data_url.removefile;
    fn.ajax_request(type_request, url_request, type_output, data_request, 'removefileCallBack');
}
function removefileCallBack(response) {
    const obj = JSON.parse(response);
    if (obj.status == true) {
        $('.doc_' + obj.data_retour.doc_taxe_id).empty();
        Notiflix.Notify.success(obj.form_validation.message, { position: 'left-bottom', timeout: 3000 });
    }
}

function messageAlert(tom_value, cliLot_id) {
    if ($('.msg_montant_tom_' + cliLot_id).hasClass('d-none') && tom_value > 200) {
        $('.msg_montant_tom_' + cliLot_id).removeClass('d-none');
    }

    if (!$('.msg_montant_tom_' + cliLot_id).hasClass('d-none') && tom_value < 200) {
        $('.msg_montant_tom_' + cliLot_id).addClass('d-none');
    }
}

function finSaisietaxe(data) {
    var type_request = 'POST';
    var type_output = 'json';
    var data_request = CryptoJSAesJson.encrypt(data, const_app.code_crypt);
    var url_request = data_url.finSaisietaxe;

    var loading = {
        type: "content",
        id_content: "contenaire-tom"
    }
    fn.ajax_request(type_request, url_request, type_output, data_request, 'finSaisietaxeCallback', loading);
}

function finSaisietaxeCallback(response) {
    if (response.status == 200) {
        if (response.action == "demande") {
            Notiflix.Confirm.show(
                response.data.title, response.data.text, response.data.btnConfirm, response.data.btnAnnuler,
                () => {
                    var data = {
                        action: "confirm",
                        cliLot_id: response.taxe_info
                    }
                    finSaisietaxe(data);
                },
                () => { },
                {
                    width: '420px',
                    borderRadius: '4px',
                    closeButton: true,
                    clickToClose: false,
                },
            );
        } else {
            Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
            localStorage.setItem("lastActiveTab", response.taxe_info);
            getListTom();
        }
    } else {
        Notiflix.Notify.success(response.message, { position: 'left-bottom', timeout: 3000 });
    }
}