<?php if (!defined('BASEPATH')) exit('No direct script access allowed');




// POO
if (!function_exists('create_database_poo')) {
    function create_database_poo($name_database)
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $return_execut = "";
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            $return_execut = $conn->connect_error;
        }
        $sql = "CREATE DATABASE $name_database";
        if ($conn->query($sql) === TRUE) {
            $return_execut = true;
        } else {
            $return_execut = $conn->error;
        }
        $conn->close();
        return $return_execut;
    }
}
// Precedurale
if (!function_exists('create_database_proc')) {
    function create_database_proc($name_database)
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        $return_execut = "";
        $conn = mysqli_connect($servername, $username, $password);
        if (!$conn) {
            $return_execut = mysqli_connect_error();
        }
        $sql = "CREATE DATABASE $name_database";
        if (mysqli_query($conn, $sql)) {
            $return_execut = true;
        } else {
            $return_execut = mysqli_error($conn);
        }
        mysqli_close($conn);
        return $return_execut;
    }
}

if (!function_exists('ajax_downloader')) {
    function ajax_downloader($filepath)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Content-Length: ' . filesize($filepath));
        ob_clean();
        flush();
        readfile($filepath);
        exit();
    }
}

if (!function_exists('format_data')) {
    function format_data($input)
    {
        $output = array();
        foreach ($input as $key => $value_key) {
            foreach ($value_key as $key_key => $value) {
                if (array_key_exists('type', $value_key)) {
                    if ($value_key["type"] == 'password') {
                        $output += [$value_key["name"] => hash('sha512', $value_key["value"])];
                    } else {
                        $output += [$value_key["name"] => $value_key["value"]];
                    }
                } else {
                    $output += [$value_key["name"] => $value_key["value"]];
                }
            }
        }
        return $output;
    }
}

if (!function_exists('float_value')) {
    function float_value($input)
    {
        $input_replace = array(' ', ',');
        $output_replace = array('', '.');
        $output = str_replace($input_replace, $output_replace, $input);
        return floatval($output);
    }
}

if (!function_exists('retour')) {
    function retour($status = null, $type = null, $data = null, $information = null)
    {
        $retour = array(
            'status' => (isset($status) && $status) ? $status : false,
            'type' => (isset($type) && $type) ? $type : "error",
            'data_retour' => (isset($data) && $data) ? $data : null,
            'form_validation' => (isset($information) && $information) ? $information : null
        );
        return $retour;
    }
}

if (!function_exists('verify_value')) {
    function verify_value($input)
    {
        $output = false;
        if (is_array($input)) {
            if (!empty($input))
                $output = true;
        } else {
            if (trim($input) != "")
                $output = true;
        }
        return $output;
    }
}

if (!function_exists('verify_length')) {
    function verify_length($value = null, $max = null, $min = null)
    {
        $output = false;
        if ($max && $min) {
            if (strlen($value) <= $max && strlen($value) >= $min)
                $output = true;
        } else if ($max && $min == null) {
            if (strlen($value) <= $max)
                $output = true;
        } else if ($max == null && $min) {
            if (strlen($value) >= $min)
                $output = true;
        } else if ($max == null && $min == null) {
            $output = true;
        }
        return $output;
    }
}

function verify_format($type, $value, $format_required = null)
{
    if ($format_required && $format_required == "true") {
        if ($type == 'text') {
            $format = true;
        }
        if ($type == 'email') {
            $format = (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $value)) ? FALSE : TRUE;
        }
        if ($type == 'numeric') {
            $format = (is_numeric($value)) ? true : false;
        }
        if ($type == 'integer') {
            $format = (is_int($value)) ? true : false;
        }
        if ($type == 'float') {
            $format = (is_float($value)) ? true : false;
        }
        if ($type == 'double') {
            $format = (is_double($value)) ? true : false;
        }
        if ($type == 'date') {
            $d = DateTime::createFromFormat('Y-m-d', $value);
            $format = $d && $d->format('Y-m-d');
        }
        if ($type == 'date-fr') {
            $d = DateTime::createFromFormat('d/m/Y',  $value);
            $format = $d &&  $d->format("d/m/Y");
        }
        if ($type == 'date-treso') {
            $d = DateTime::createFromFormat('d/m/Y', $value);
            $format = $d && $d->format('d/m/Y');
        }
        if ($type == 'date-time') {
            $d = DateTime::createFromFormat('Y-m-d H:i:s', $value);
            $format = $d && $d->format('d/m/Y');
        }
        if ($type == 'array') {
            $format = (is_array($value)) ? true : false;
        }
        if ($type == 'object') {
            $format = (is_object($value)) ? true : false;
        }
    } else {
        $format = true;
    }
    return $format;
}

if (!function_exists('verify_email')) {
    function verify_email($str)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }
}

if (!function_exists('is_ajax')) {
    function is_ajax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest");
    }
}

if (!function_exists('form_tag')) {
    function form_tag($url, $array)
    {
        $array += ['role' => 'form', 'enctype' => 'multipart/form-data'];
        return form_open($url, $array);
    }
}

if (!function_exists('date_sql')) {
    function date_sql($date = null, $sep_input, $sep_output, $format)
    {
        if ($date != null) {
            if ($format == "jj/MM/aaaa" || $format == "jj-MM-aaaa") {
                $dateSplite = explode($sep_input, $date);
                $jours = $dateSplite[0];
                $mois = $dateSplite[1];
                $annee = $dateSplite[2];
                return $annee . $sep_output . $mois . $sep_output . $jours;
            } else if ($format == "aaaa/MM/jj" || $format == "aaaa-MM-jj") {
                $dateSplite = explode($sep_input, $date);
                $jours = $dateSplite[2];
                $mois = $dateSplite[1];
                $annee = $dateSplite[0];
                return $jours . $sep_output . $mois . $sep_output . $annee;
            }
        } else {
            return null;
        }
    }
}

if (!function_exists('weeks_between')) {
    function weeks_between($datefrom, $dateto)
    {
        $datefrom = DateTime::createFromFormat('Y-m-d', $datefrom);
        $dateto = DateTime::createFromFormat('Y-m-d', $dateto);
        $interval = $datefrom->diff($dateto);
        $week_total = $interval->format('%a') / 7;
        return floor($week_total);
    }
}


if (!function_exists('number_format_short')) {
    function number_format_short($n, $precision = 1)
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        if ($precision > 0) {
            $dotzero = '.' . str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }

        return $n_format . $suffix;
    }
}

if (!function_exists("order_by")) {
    /**
     * Fonction pour ordonner un tableau par un index defini dans un élément du tableau
     * @param array $array
     * @param string $index
     * @return array
     */
    function order_by($array, $index)
    {
        $newarray = array();
        $notadded = array();
        if (count($array) > 0) {
            foreach ($array as $data) {
                if (is_array($data)) {
                    if (!isset($newarray[$data[$index]])) $newarray[$data[$index]] = $data;
                    else $notadded[] = $data;
                } else if (is_object($data)) {
                    if (!isset($newarray[$data->$index])) $newarray[$data->$index] = $data;
                    else $notadded[] = $data;
                } else {
                    $newarray[] = $data;
                }
            }
        }
        if (!empty($notadded)) {
            $newarray = array_merge($newarray, $notadded);
        }
        ksort($newarray);
        return $newarray;
    }
}


if (!function_exists('replace_accents')) {
    function replace_accents($str)
    {
        $ch0 = array(
            "œ" => "oe",
            "Œ" => "OE",
            "æ" => "ae",
            "Æ" => "AE",
            "À" => "A",
            "Á" => "A",
            "Â" => "A",
            "à" => "A",
            "Ä" => "A",
            "Å" => "A",
            "&#256;" => "A",
            "&#258;" => "A",
            "&#461;" => "A",
            "&#7840;" => "A",
            "&#7842;" => "A",
            "&#7844;" => "A",
            "&#7846;" => "A",
            "&#7848;" => "A",
            "&#7850;" => "A",
            "&#7852;" => "A",
            "&#7854;" => "A",
            "&#7856;" => "A",
            "&#7858;" => "A",
            "&#7860;" => "A",
            "&#7862;" => "A",
            "&#506;" => "A",
            "&#260;" => "A",
            "à" => "a",
            "á" => "a",
            "â" => "a",
            "à" => "a",
            "ä" => "a",
            "å" => "a",
            "&#257;" => "a",
            "&#259;" => "a",
            "&#462;" => "a",
            "&#7841;" => "a",
            "&#7843;" => "a",
            "&#7845;" => "a",
            "&#7847;" => "a",
            "&#7849;" => "a",
            "&#7851;" => "a",
            "&#7853;" => "a",
            "&#7855;" => "a",
            "&#7857;" => "a",
            "&#7859;" => "a",
            "&#7861;" => "a",
            "&#7863;" => "a",
            "&#507;" => "a",
            "&#261;" => "a",
            "Ç" => "C",
            "&#262;" => "C",
            "&#264;" => "C",
            "&#266;" => "C",
            "&#268;" => "C",
            "ç" => "c",
            "&#263;" => "c",
            "&#265;" => "c",
            "&#267;" => "c",
            "&#269;" => "c",
            "Ð" => "D",
            "&#270;" => "D",
            "&#272;" => "D",
            "&#271;" => "d",
            "&#273;" => "d",
            "È" => "E",
            "É" => "E",
            "Ê" => "E",
            "Ë" => "E",
            "&#274;" => "E",
            "&#276;" => "E",
            "&#278;" => "E",
            "&#280;" => "E",
            "&#282;" => "E",
            "&#7864;" => "E",
            "&#7866;" => "E",
            "&#7868;" => "E",
            "&#7870;" => "E",
            "&#7872;" => "E",
            "&#7874;" => "E",
            "&#7876;" => "E",
            "&#7878;" => "E",
            "è" => "e",
            "é" => "e",
            "ê" => "e",
            "ë" => "e",
            "&#275;" => "e",
            "&#277;" => "e",
            "&#279;" => "e",
            "&#281;" => "e",
            "&#283;" => "e",
            "&#7865;" => "e",
            "&#7867;" => "e",
            "&#7869;" => "e",
            "&#7871;" => "e",
            "&#7873;" => "e",
            "&#7875;" => "e",
            "&#7877;" => "e",
            "&#7879;" => "e",
            "&#284;" => "G",
            "&#286;" => "G",
            "&#288;" => "G",
            "&#290;" => "G",
            "&#285;" => "g",
            "&#287;" => "g",
            "&#289;" => "g",
            "&#291;" => "g",
            "&#292;" => "H",
            "&#294;" => "H",
            "&#293;" => "h",
            "&#295;" => "h",
            "Ì" => "I",
            "Í" => "I",
            "Î" => "I",
            "Ï" => "I",
            "&#296;" => "I",
            "&#298;" => "I",
            "&#300;" => "I",
            "&#302;" => "I",
            "&#304;" => "I",
            "&#463;" => "I",
            "&#7880;" => "I",
            "&#7882;" => "I",
            "&#308;" => "J",
            "&#309;" => "j",
            "&#310;" => "K",
            "&#311;" => "k",
            "&#313;" => "L",
            "&#315;" => "L",
            "&#317;" => "L",
            "&#319;" => "L",
            "&#321;" => "L",
            "&#314;" => "l",
            "&#316;" => "l",
            "&#318;" => "l",
            "&#320;" => "l",
            "&#322;" => "l",
            "Ñ" => "N",
            "&#323;" => "N",
            "&#325;" => "N",
            "&#327;" => "N",
            "ñ" => "n",
            "&#324;" => "n",
            "&#326;" => "n",
            "&#328;" => "n",
            "&#329;" => "n",
            "Ò" => "O",
            "Ó" => "O",
            "Ô" => "O",
            "Õ" => "O",
            "Ö" => "O",
            "Ø" => "O",
            "&#332;" => "O",
            "&#334;" => "O",
            "&#336;" => "O",
            "&#416;" => "O",
            "&#465;" => "O",
            "&#510;" => "O",
            "&#7884;" => "O",
            "&#7886;" => "O",
            "&#7888;" => "O",
            "&#7890;" => "O",
            "&#7892;" => "O",
            "&#7894;" => "O",
            "&#7896;" => "O",
            "&#7898;" => "O",
            "&#7900;" => "O",
            "&#7902;" => "O",
            "&#7904;" => "O",
            "&#7906;" => "O",
            "ò" => "o",
            "ó" => "o",
            "ô" => "o",
            "õ" => "o",
            "ö" => "o",
            "ø" => "o",
            "&#333;" => "o",
            "&#335;" => "o",
            "&#337;" => "o",
            "&#417;" => "o",
            "&#466;" => "o",
            "&#511;" => "o",
            "&#7885;" => "o",
            "&#7887;" => "o",
            "&#7889;" => "o",
            "&#7891;" => "o",
            "&#7893;" => "o",
            "&#7895;" => "o",
            "&#7897;" => "o",
            "&#7899;" => "o",
            "&#7901;" => "o",
            "&#7903;" => "o",
            "&#7905;" => "o",
            "&#7907;" => "o",
            "ð" => "o",
            "&#340;" => "R",
            "&#342;" => "R",
            "&#344;" => "R",
            "&#341;" => "r",
            "&#343;" => "r",
            "&#345;" => "r",
            "&#346;" => "S",
            "&#348;" => "S",
            "&#350;" => "S",
            "&#347;" => "s",
            "&#349;" => "s",
            "&#351;" => "s",
            "&#354;" => "T",
            "&#356;" => "T",
            "&#358;" => "T",
            "&#355;" => "t",
            "&#357;" => "t",
            "&#359;" => "t",
            "Ù" => "U",
            "Ú" => "U",
            "Û" => "U",
            "Ü" => "U",
            "&#360;" => "U",
            "&#362;" => "U",
            "&#364;" => "U",
            "&#366;" => "U",
            "&#368;" => "U",
            "&#370;" => "U",
            "&#431;" => "U",
            "&#467;" => "U",
            "&#469;" => "U",
            "&#471;" => "U",
            "&#473;" => "U",
            "&#475;" => "U",
            "&#7908;" => "U",
            "&#7910;" => "U",
            "&#7912;" => "U",
            "&#7914;" => "U",
            "&#7916;" => "U",
            "&#7918;" => "U",
            "&#7920;" => "U",
            "ù" => "u",
            "ú" => "u",
            "û" => "u",
            "ü" => "u",
            "&#361;" => "u",
            "&#363;" => "u",
            "&#365;" => "u",
            "&#367;" => "u",
            "&#369;" => "u",
            "&#371;" => "u",
            "&#432;" => "u",
            "&#468;" => "u",
            "&#470;" => "u",
            "&#472;" => "u",
            "&#474;" => "u",
            "&#476;" => "u",
            "&#7909;" => "u",
            "&#7911;" => "u",
            "&#7913;" => "u",
            "&#7915;" => "u",
            "&#7917;" => "u",
            "&#7919;" => "u",
            "&#7921;" => "u",
            "&#372;" => "W",
            "&#7808;" => "W",
            "&#7810;" => "W",
            "&#7812;" => "W",
            "&#373;" => "w",
            "&#7809;" => "w",
            "&#7811;" => "w",
            "&#7813;" => "w",
            "Ý" => "Y",
            "&#374;" => "Y",
            "?" => "Y",
            "&#7922;" => "Y",
            "&#7928;" => "Y",
            "&#7926;" => "Y",
            "&#7924;" => "Y",
            "ý" => "y",
            "ÿ" => "y",
            "&#375;" => "y",
            "&#7929;" => "y",
            "&#7925;" => "y",
            "&#7927;" => "y",
            "&#7923;" => "y",
            "&#377;" => "Z",
            "&#379;" => "Z"
        );
        $str = strtr($str, $ch0);
        return $str;
    }
}

function mime2ext($mime)
{
    $all_mimes = '{
            "png":["image\/png","image\/x-png"],
            "bmp":["image\/bmp","image\/x-bmp","image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp","image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp","application\/x-win-bitmap"],
            "gif":["image\/gif"],
            "jpeg":["image\/jpeg","image\/pjpeg"],
            "xspf":["application\/xspf+xml"],
            "vlc":["application\/videolan"],
            "wmv":["video\/x-ms-wmv","video\/x-ms-asf"],
            "au":["audio\/x-au"],
            "ac3":["audio\/ac3"],
            "flac":["audio\/x-flac"],
            "ogg":["audio\/ogg","video\/ogg","application\/ogg"],
            "kmz":["application\/vnd.google-earth.kmz"],
            "kml":["application\/vnd.google-earth.kml+xml"],
            "rtx":["text\/richtext"],
            "rtf":["text\/rtf"],
            "jar":["application\/java-archive","application\/x-java-application","application\/x-jar"],
            "zip":["application\/x-zip","application\/zip","application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],
            "7zip":["application\/x-compressed"],
            "xml":["application\/xml","text\/xml"],
            "svg":["image\/svg+xml"],
            "3g2":["video\/3gpp2"],
            "3gp":["video\/3gp","video\/3gpp"],
            "mp4":["video\/mp4"],
            "m4a":["audio\/x-m4a"],
            "f4v":["video\/x-f4v"],
            "flv":["video\/x-flv"],
            "webm":["video\/webm"],
            "aac":["audio\/x-acc"],
            "m4u":["application\/vnd.mpegurl"],
            "pdf":["application\/pdf","application\/octet-stream"],
            "pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],
            "ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office","application\/msword"],
            "docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],
            "xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],
            "xl":["application\/excel"],
            "xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel","application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],
            "xsl":["text\/xsl"],
            "mpeg":["video\/mpeg"],
            "mov":["video\/quicktime"],
            "avi":["video\/x-msvideo","video\/msvideo","video\/avi","application\/x-troff-msvideo"],
            "movie":["video\/x-sgi-movie"],
            "log":["text\/x-log"],
            "txt":["text\/plain"],
            "css":["text\/css"],
            "html":["text\/html"],
            "wav":["audio\/x-wav","audio\/wave","audio\/wav"],
            "xhtml":["application\/xhtml+xml"],
            "tar":["application\/x-tar"],
            "tgz":["application\/x-gzip-compressed"],
            "psd":["application\/x-photoshop","image\/vnd.adobe.photoshop"],
            "exe":["application\/x-msdownload"],
            "js":["application\/x-javascript"],
            "mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],
            "rar":["application\/x-rar","application\/rar","application\/x-rar-compressed"],
            "gzip":["application\/x-gzip"],
            "hqx":["application\/mac-binhex40","application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],
            "cpt":["application\/mac-compactpro"],
            "bin":["application\/macbinary","application\/mac-binary","application\/x-binary","application\/x-macbinary"],
            "oda":["application\/oda"],
            "ai":["application\/postscript"],
            "smil":["application\/smil"],
            "mif":["application\/vnd.mif"],
            "wbxml":["application\/wbxml"],
            "wmlc":["application\/wmlc"],
            "dcr":["application\/x-director"],
            "dvi":["application\/x-dvi"],
            "gtar":["application\/x-gtar"],
            "php":["application\/x-httpd-php","application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],
            "swf":["application\/x-shockwave-flash"],
            "sit":["application\/x-stuffit"],
            "z":["application\/x-compress"],
            "mid":["audio\/midi"],
            "aif":["audio\/x-aiff","audio\/aiff"],
            "ram":["audio\/x-pn-realaudio"],
            "rpm":["audio\/x-pn-realaudio-plugin"],
            "ra":["audio\/x-realaudio"],
            "rv":["video\/vnd.rn-realvideo"],
            "jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],
            "tiff":["image\/tiff"],
            "eml":["message\/rfc822"],
            "pem":["application\/x-x509-user-cert","application\/x-pem-file"],
            "p10":["application\/x-pkcs10","application\/pkcs10"],
            "p12":["application\/x-pkcs12"],
            "p7a":["application\/x-pkcs7-signature"],
            "p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],
            "p7r":["application\/x-pkcs7-certreqresp"],
            "p7s":["application\/pkcs7-signature"],
            "crt":["application\/x-x509-ca-cert","application\/pkix-cert"],
            "crl":["application\/pkix-crl","application\/pkcs-crl"],
            "pgp":["application\/pgp"],
            "gpg":["application\/gpg-keys"],
            "rsa":["application\/x-pkcs7"],
            "ics":["text\/calendar"],
            "zsh":["text\/x-scriptzsh"],
            "cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],
            "wma":["audio\/x-ms-wma"],
            "vcf":["text\/x-vcard"],
            "srt":["text\/srt"],
            "vtt":["text\/vtt"],
            "ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],
            "csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],
            "json":["application\/json","text\/json"]}';
    $all_mimes = json_decode($all_mimes, true);
    foreach ($all_mimes as $key => $value) {
        if (array_search($mime, $value) !== false) return $key;
    }
    return false;
}
