<?php
class MY_Controller extends MX_Controller
{
	private $_css_style = array(
		"fonts/font-awesome/css/all.min.css",
		"lib/bootstrap-5.0.2/css/bootstrap.min.css",
		"lib/toastr/toastr.min.css",
		"lib/vendor/animate.css/animate.min.css",
		"lib/vendor/swiper/swiper-bundle.min.css",
		"css/theme.css",
		"css/general.css"
	);

	private $_js_requerie = array(
		"lib/jquery/jquery-3.6.0.min.js",
		"lib/bootstrap-5.0.2/js/bootstrap.min.js",
		"lib/vendor/swiper/swiper-bundle.min.js",
		"js/globals_fn.js",
		"lib/toastr/toastr.min.js",
	);

	private $_template = "";

	protected $_data = array();
	protected $_css_personnaliser = array();
	protected $_script = array();
	protected $_view_directory = "";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('cookie');
		$this->load->helper('file');

		$this->load->helper('utils');

		$this->redirectCustomer();
	}

	public function redirectCustomer()
	{
		redirect("/app-customer");
	}

	/* ---------------------------------- */
	private function load_css_style()
	{
		if (count($this->_css_style)) {
			foreach ($this->_css_style as $style) {
				$this->_data["css_style"][] = $style;
			}
		}
	}
	// load style personaliser
	protected function load_css_personnaliser()
	{
		if (count($this->_css_personnaliser)) {
			foreach ($this->_css_personnaliser as $style_perso) {
				$this->_data["css_personnaliser"][] = $style_perso;
			}
		}
	}
	// load javascript require
	private function load_js_requerie()
	{
		if (count($this->_js_requerie)) {
			foreach ($this->_js_requerie as $script) {
				$this->_data["js_requerie"][] = $script;
			}
		}
	}
	// load javascript personaliser
	protected function load_script()
	{
		if (count($this->_script)) {
			foreach ($this->_script as $js_p) {
				$this->_data["js_script"][] = $js_p;
			}
		}
	}

	// initialiser data in _data
	public function load_data($var, $data)
	{
		$this->_data[$var] = $data;
	}

	/**
	 * Retourner les vues - de type ajax
	 * @return void
	 */
	public function render_ajax($view)
	{
		$this->load->view($view, $this->_data);
	}

	/**
	 * Retourner les vues - de type page
	 * @return void
	 */

	public function render($page)
	{
		$this->_template = "com/_page";
		$this->_data["page"] =  $this->_view_directory . "/" . $page;
		$this->load_css_style();
		$this->load_css_personnaliser();
		$this->load_js_requerie();
		$this->load_script();
		$this->load->view($this->_template, $this->_data);
	}

	public function renderComponant($view, $data = NULL)
	{
		$this->load->view($view, $data);
	}

	public function page_ajax($view)
	{
		$this->load->view($view, $this->_data);
	}

	/* ------------------- */

	function control_data($input)
	{
		$data = array();
		foreach ($input as $key => $value_key) {
			foreach ($value_key as $key_key => $value) {
				$data += [
					$key => [
						"type" => (isset($value_key["type_data"])) ? $value_key["type_data"] : ((isset($value_key["type"])) ? $value_key["type"] : "text"),
						"required" => (isset($value_key["required"]) && $value_key["required"] == "true") ? verify_value($value_key["value"]) : true,
						"format" => verify_format((isset($value_key["type_data"])) ? $value_key["type_data"] : ((isset($value_key["type"])) ? $value_key["type"] : "text"), $value_key["value"], (isset($value_key["format_required"])) ? $value_key["format_required"] : null),
						"length" => verify_length($value, ((isset($value_key["max_length"])) ? $value_key["max_length"] : null), ((isset($value_key["min_length"])) ? $value_key["min_length"] : null))
					]
				];
			}
		}
		$control = array();
		if (isset($data)) {
			foreach ($data as $key_d => $value_d) {
				foreach ($value_d as $key_vd => $value_vd) {
					if ($value_vd == false) {
						$control += [
							$key_d => [
								$key_vd => $this->message_control_champ($key_vd)
							]
						];
					}
				}
			}
		}
		$output = array(
			'return' => ((empty($control)) ? true : false),
			'information' => ((!empty($control)) ? $control : '')
		);
		return $output;
	}

	function message_control_champ($key)
	{
		$output = '';
		switch ($key) {
			case 'required':
				$output = "Ce champ ne doit pas être vide.";
				break;
			case 'format':
				$output = "L'information que vous avez entrée est du format incorrect.";
				break;
			case 'length':
				$output = "Les nombres des caractères sont incorrectes.";
				break;
		}
		return $output;
	}
}
