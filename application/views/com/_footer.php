<!-- ======= Footer ======= -->
<footer id="footer">
  <div class="footer-top">
    <div class="container">
      <div class="row">
      </div>
    </div>
  </div>

  <div class="container py-4 text-center mt-10">
    <div class="copyright">
      &copy; <strong><span>Celavigestion <?= date('Y') ?></span></strong> | Tous droits réservés | Mentions légales | Politique de
      confidentialité
    </div>
  </div>
</footer>
<!-- End Footer -->
<?php $this->load->view("com/_js"); ?>

</body>

</html>