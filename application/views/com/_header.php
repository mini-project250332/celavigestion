<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<meta property="og:image" content="">
	<meta property="og:image:secure_url" content="">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="600">
	<!-- Meta -->
	
    <!-- Icone page -->
    <link rel="icon" type="image/png" id="favicon" href="" />
    <!-- Style par default com/_css -->
    <?php $this->load->view("com/_css"); ?>
    
    <!-- Style personnaliser -->
	<?php if (isset($css_personnaliser)) { 
        foreach ($css_personnaliser as $css_p) { 
		    echo '<link type="text/css" href="'.base_url('assets/'.$css_p).'" rel="stylesheet">';
	    } 
    } ?>

    <!-- Javascript namespace application -->
    <script  type="text/javascript">
    const constant_app = {
    <?php 
        if (isset($js_namespace)){
            foreach ($js_namespace as $js_n => $val_n) {
                echo $js_n." : '".$val_n."',\n";		
            }	
        }
    ?>
    }
    </script>
        
    <!-- Lirairie JS par default "require" -->
    <?php 
        if (isset($js_requerie)){
            foreach ($js_requerie as $js_r) {
                echo '<script src="'.base_url('assets/'.$js_r).'" type="text/javascript"></script>';		
            }	
        } 
    ?>

</head>
<body>
