<?php

class Accueil extends MY_Controller {
	
	protected $_data = array();
    protected $_css_personnaliser = array();
    protected $_script = array();
    protected $_view_directory = "accueil";

	public function __construct(){
		parent::__construct();
    }
	
	public function index(){

        $this->_script = array();
        $this->render(strtolower(__class__));
        
	}

}