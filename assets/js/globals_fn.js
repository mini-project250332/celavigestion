// Fonction pre-existe
/*
    Author : Loroque Brillant
*/

const fn = {

    initialised_lib: function() {
        $('[data-toggle="tooltip"]').tooltip({ trigger: $(this).attr('data-trigger') });
    },

    float_fn: function(input) {
        return parseFloat(input.replace(/ /g, '').replace(',', '.'));
    },

    base_url: function() {
        return $('input[name=base_url]').val();
    },

    load_open : function(url){
        window.open(url,'_blank');
    },

    empty_fn: function() {},

    loading_body: function() {
        $.LoadingOverlay("show", {
            background: "rgba(255,255, 255,0.6)",
            image: const_app.base_url + "assets/images/loader.gif",
        });
    },

    loading_content: function(id_) {
        $("#" + id_).LoadingOverlay("show", {
            background: "rgba(255,255, 255,0.6)",
            image: const_app.base_url + "assets/images/loader.gif",
        });
    },

    stop_loading_body: function() {
        $.LoadingOverlay("hide", true);
    },

    stop_loading_content: function(id_) {
        $("#" + id_).LoadingOverlay("hide", true);
    },

    new_window: function(url) {
        window.open(url, '_blank');
    },

    load_url: function(url) {
        window.location.href = url;
    },

    transform_text: function(text) {
        return text.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    },

    current_date: function() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        today = dd + '/' + mm + '/' + yyyy;
        return today;
    },

    get_lang: function(array_index) {
        var post = { data: array_index };
        var lang = {};
        var url = location.host + '';
        var url_ajax = location.protocol + '//' + url;
        $.post(url_ajax, post, function(data) {
            $.each(data, function(key) {
                lang[key] = data[key];
            });
        }, "json");
        return lang;
    },

    serialise_content: function(id_) {
        var objet = {};
        var disabled = $('#' + id_).find(':input:disabled').removeAttr('disabled');
        var serialise_data = $('#' + id_ + ' :input').serializeArray();
        disabled.prop('disabled', true);
        for (var i = 0; i < serialise_data.length; i++) {
            objet[serialise_data[i]['name']] = serialise_data[i]['value'];
        }
        return objet;
    },

    string_to_fn: function(name_fn, param) {
        var string = window[name_fn];
        (param !== 'undefined') ? param: param = {};

        if (typeof string !== 'function')
            return false;
        else
            window[name_fn](param);
    },

    force_download: function(href) {
        var anchor = document.createElement('a');
        anchor.href = href;
        anchor.download = href;
        document.body.appendChild(anchor);
        anchor.click();
    },

    ajax_download: function(url_request, path_file, data_input) {
        if (typeof data_input !== 'undefined') {
            data_input.path_file = path_file;
        } else {
            data_input = { path_file: path_file }
        }
        const data_request = data_input;
        $.AjaxDownloader({
            url: url_request,
            data: data_request
        });
    },

    response_control_champ: function(response) {
        $.each(response.form_validation, function(index, obj) {
            $('input[name=' + index + ']').closest('div.form-group').addClass('has-error');
            $('select[name=' + index + ']').closest('div.form-group').addClass('has-error');
            $('textarea[name=' + index + ']').closest('div.form-group').addClass('has-error');

            if (obj.length !== 'undefined') {
                $('input[name=' + index + ']').parent('div').children('.help').text(obj.length);
                $('select[name=' + index + ']').parent('div').children('.help').text(obj.length);
                $('textarea[name=' + index + ']').parent('div').children('.help').text(obj.length);
            }
            if (obj.format !== 'undefined') {
                $('input[name=' + index + ']').parent('div').children('.help').text(obj.format);
                $('select[name=' + index + ']').parent('div').children('.help').text(obj.format);
                $('textarea[name=' + index + ']').parent('div').children('.help').text(obj.format);
            }

            if (obj.required !== 'undefined') {
                $('input[name=' + index + ']').closest('div.form-group').find('.help').text(obj.required);
                $('select[name=' + index + ']').closest('div.form-group').find('.help').text(obj.required);

                $('select[name=' + index + ']').closest('div.form-group').find('.help').text(obj.required);

                $('textarea[name=' + index + ']').parent('div').children('.help').text(obj.required);
            }

            if (obj.type !== 'undefined') {
                $('input[name=' + index + ']').parent('div').children('.help').text(obj.type);
                $('select[name=' + index + ']').parent('div').children('.help').text(obj.type);
                $('textarea[name=' + index + ']').parent('div').children('.help').text(obj.type);
            }
        });
    },

    ajax_request: function(type_request, url_request, type_output, data_input, ajax_success, loading, ajax_error) {
        (typeof data_input !== 'undefined') ? data_input: data_input = {};
        (typeof ajax_error !== 'undefined') ? ajax_error: ajax_error = function() {};
        $.ajax({
            type: type_request,
            url: url_request,
            data: { data: data_input },
            dataType: type_output,
            beforeSend: function() {
                if (typeof loading !== 'undefined') {
                    loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.restart();
                    }
                }
            },
            success: function(response) {

                if (typeof response == 'string') {
                    var lowercaseResponse = response.toLowerCase();
                    var sessionErrorToCheck = ['undefined', const_app.data_session];
                    var sessionErrorPresent = sessionErrorToCheck.every(function(word) {
                        return lowercaseResponse.includes(word.toLowerCase());
                    });

                    if (sessionErrorPresent) {
                        Notiflix.Notify.warning(text_sessionExpirer, { 
                            position: 'center-top',
                            distance: '50px',
                            timeout: 4000,
                            width: '475px'
                        });
                        setTimeout(function() {
                            window.location.href = const_app.base_url;
                        },4000);
                        return;
                    }
                }
                
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response) : '';
            },

            complete: function() {
                if (typeof loading !== 'undefined') {
                    stop_loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.stop();
                    }
                }
            },
            error: ajax_error
        });
    },

    ajax_form: function(form, type_output, ajax_success, messages_reponse, loading, ajax_error) {
        (typeof ajax_error !== 'undefined') ? ajax_error: ajax_error = function() {};
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: { data: formData },
            dataType: type_output,
            cache: false,
            contentType: false,
            processData: false,
            async: false,
            beforeSend: function() {
                if (typeof loading !== 'undefined') {
                    loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.restart();
                    }
                }
            },
            success: function(response) {
                disabled.prop('disabled', true);

                if (typeof response == 'string') {
                    var lowercaseResponse = response.toLowerCase();
                    var sessionErrorToCheck = ['undefined', const_app.data_session];
                    var sessionErrorPresent = sessionErrorToCheck.every(function(word) {
                        return lowercaseResponse.includes(word.toLowerCase());
                    });

                    if (sessionErrorPresent) {
                        Notiflix.Notify.warning(text_sessionExpirer, { 
                            position: 'center-top',
                            distance: '50px',
                            timeout: 4000,
                            width: '475px'
                        });
                        setTimeout(function() {
                            window.location.href = const_app.base_url;
                        },4000);
                        return;
                    }
                }

                (typeof messages_reponse !== 'undefined') ? window[messages_reponse](response): '';
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response): '';
            },
            complete: function() {
                if (typeof loading !== 'undefined') {
                    stop_loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.stop();
                    }
                }
                disabled.prop('disabled', true);
            },
            error: ajax_error
        });
    },

    ajax_form_min: function(form, type_output, ajax_success, loading) {
        var elements = form.serialize();
        var url = form.attr('action');
        var method = form.attr('method');

        if (typeof loading !== 'undefined') {
            loading_fn(loading);
        } else {
            if (typeof Pace !== "undefined") {
                Pace.restart();
            }
        }

        if (method.toLowerCase() == 'post') {
            $.post(url, elements, function(response) {

                if (typeof response == 'string') {
                    var lowercaseResponse = response.toLowerCase();
                    var sessionErrorToCheck = ['undefined', const_app.data_session];
                    var sessionErrorPresent = sessionErrorToCheck.every(function(word) {
                        return lowercaseResponse.includes(word.toLowerCase());
                    });

                    if (sessionErrorPresent) {

                        Notiflix.Notify.warning(text_sessionExpirer, { 
                            position: 'center-top',
                            distance: '50px',
                            timeout: 4000,
                            width: '475px'
                        });

                        setTimeout(function() {
                            window.location.href = const_app.base_url;
                        },4000);
                        return; 
                    }
                }
                
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response) : '';

            }, type_output).fail(function(err, status) {

            });
        } else if (method.toLowerCase() == 'get') {
            $.get(url + '?' + elements, function(response) {
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response): '';
                //'json', 'html'
            }, type_output).fail(function(err, status) {

            });
        }
    },

    ajax_form_data_custom: function(form, type_output, ajax_success, loading, ajax_error) {
        (typeof ajax_error !== 'undefined') ? ajax_error: ajax_error = function() {};
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var formData = data_form_complexe(form.attr('id'));
        var btn_submit;
        $("#" + form.attr('id') + " :input[type=submit]").each(function() {
            btn_submit = $(this);
        });
        var icon = btn_submit.children('svg');
        var pre_btn_text = btn_submit.children('span').text();
        const pre_icon = icon.attr('data-icon');
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: { data: formData },
            dataType: type_output,
            beforeSend: function() {
                icon.attr({
                    'data-icon': 'spinner'
                });
                icon.addClass('fa-spin');
                btn_submit.children('span').text('Chargement');
                if (typeof loading !== 'undefined') {
                    loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.restart();
                    }
                }
            },
            success: function(response) {
                disabled.prop('disabled', true);

                if (typeof response == 'string') {
                    var lowercaseResponse = response.toLowerCase();
                    var sessionErrorToCheck = ['undefined', const_app.data_session];
                    var sessionErrorPresent = sessionErrorToCheck.every(function(word) {
                        return lowercaseResponse.includes(word.toLowerCase());
                    });

                    if (sessionErrorPresent) {
                        Notiflix.Notify.warning(text_sessionExpirer, { 
                            position: 'center-top',
                            distance: '50px',
                            timeout: 4000,
                            width: '475px'
                        });
                        setTimeout(function() {
                            window.location.href = const_app.base_url;
                        },5000);
                        return;
                    }
                }
                
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response) : '';
            },
            complete: function() {
                if (typeof loading !== 'undefined') {
                    stop_loading_fn(loading);
                } else {
                    if (typeof Pace !== "undefined") {
                        Pace.stop();
                    }
                }
                disabled.prop('disabled', true);
                btn_submit.children('svg').removeAttr("data-icon");
                btn_submit.children('svg').removeClass('fa-spin');
                btn_submit.children('svg').attr({
                    'data-icon': pre_icon
                });
                btn_submit.children('span').text(pre_btn_text);
            },
            error: ajax_error
        });
    },

    local_storage: function(cle, data_objet) {
        localStorage.setItem(cle, JSON.stringify(data_objet));
    },

    local_storage_signleton: function(cle, value) {
        localStorage.setItem(cle, value);
    },

    get_local_storage_signleton: function(cle) {
        return localStorage.getItem(cle);
    },

    clear_local_storage: function(data_cle) {
        storage.removeItem(data_cle);
    },

    session_storage: function(cle, data_objet) {
        sessionStorage.setItem(cle, JSON.stringify(data_objet));
    },

    session_storage_signleton: function(cle, value) {
        sessionStorage.setItem(cle, value);
    },

    get_session_storage_signleton: function(cle) {
        return sessionStorage.getItem(cle);
    },

    clear_session_storage: function(cle) {
        sessionStorage.removeItem(cle);
    },

    box_notification: function(param) {
        var icon;
        switch (param.type) {
            case 'alert':
                icon = '<span class="mx-2"><i class="' + param.icon + '"></i></span>';
                break;
            case 'success':
                icon = '<span class="mx-2"><i class="' + param.icon + '"></i></span>';
                break;
            case 'warning':
                icon = '<span class="mx-2"><i class="' + param.icon + '"></i></span>';
                break;
            case 'error':
                icon = '<span class="mx-2"><i class="' + param.icon + '"></i></span>';
                break;
            case 'info':
                icon = '<span class="mx-2"><i class="' + param.icon + '"></i></i></span>';
                break;
        }
        var text = param.text;
        var n = new Noty({
            type: param.type,
            theme: 'nest',
            text: icon + text,
            container: '',
            timeout: '6000',
            closeWith: ['click'],
            killer: true,
        });
        n.show();
    },

    ajax_content : function(type_request,url_request,type_output,data_request,ajax_success,ajax_error){
        (data_request!== 'undefined') ? data_request : data_request = {};
        (ajax_error!== 'undefined') ? ajax_error : ajax_error = function(){};
        var n = 0;
        $.ajax({
            type : type_request,
            url : url_request,
            data: data_request,
            dataType : type_output,
            beforeSend: function(){
                //$('body').addClass('loading').loader('show', {overlay: true});
            },
            success : function(response){
                if (typeof response == 'string') {
                    var lowercaseResponse = response.toLowerCase();
                    var sessionErrorToCheck = ['undefined', const_app.data_session];
                    var sessionErrorPresent = sessionErrorToCheck.every(function(word) {
                        return lowercaseResponse.includes(word.toLowerCase());
                    });

                    if (sessionErrorPresent) {
                        Notiflix.Notify.warning(text_sessionExpirer, { position: 'center-top', timeout: 5000 });
                        setTimeout(function() {
                            window.location.href = const_app.base_url;
                        },5000);
                        return; 
                    }
                }
                
                (typeof ajax_success !== 'undefined') ? window[ajax_success](response) : '';
            },
            complete: function(){
                //$('body').removeClass('loading').loader('hide');
            },
            error : ajax_error
        });
    },
}


function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
    }
}

function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
}

function isExists(selector) {
    return $(selector).length > 0;
}

String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

// param  = { type : "content" ou "body", id_content : "" }
var loading_fn = function(param) {
    if (param.type == "content") {
        fn.loading_content(param.id_content);
    } else {
        fn.loading_body();
    }
}

var stop_loading_fn = function(param) {
    if (param.type == "content") {
        fn.stop_loading_content(param.id_content);
    } else {
        fn.stop_loading_body();
    }
}

var data_form = function(id_) {
    var objet = {}
    var disabled = $('#' + id_).find(':input:disabled').removeAttr('disabled');
    var serialise_data = $('#' + id_ + ' :input').serializeArray();
    disabled.prop('disabled', true);
    for (var i = 0; i < serialise_data.length; i++) {
        objet[serialise_data[i]['name']] = serialise_data[i]['value'];
    }
    return objet;
}

var data_form_complexe = function(id_) {
    var objet = {};
    $("#" + id_ + " :input").not(':input[type=button],:input[type=submit],:input[name=""]').each(function() {
        let attr;
        if ($(this).attr('type') == "radio") {
            if ($(this).is(':checked')) {
                attr = {
                    name: $(this).attr('name'),
                    type: "text",
                    value: $(this).val()
                }
            }
        } else if($(this).attr('type') == "password"){
            attr = {
                name: $(this).attr('name'),
                type: "password",
                value: CryptoJSAesJson.encrypt($(this).val(),const_app.code_crypt),
                type_data: $(this).attr('data-type'),
                format_required: $(this).attr('data-formatcontrol'),
                min_length: $(this).attr('data-min'),
                max_length: $(this).attr('data-max'),
                required: $(this).attr('data-require'),
                disabled: $(this).attr('disabled')
            }
        } else if($(this).attr('type') == "checkbox"){
            attr = {
                name: $(this).attr('name'),
                type: "text",
                value: $(this).is(':checked') ? 1 : 0,
            }
        } else if($(this).attr('type') == "search"){
        }else{
            attr = {
                name: $(this).attr('name'),
                type: $(this).attr('type'),
                value: $(this).val(),
                type_data: $(this).attr('data-type'),
                format_required: $(this).attr('data-formatcontrol'),
                min_length: $(this).attr('data-min'),
                max_length: $(this).attr('data-max'),
                required: $(this).attr('data-require'),
                disabled: $(this).attr('disabled')
            }
        }

        objet[$(this).attr('name')] = attr;
    });
    return objet;
};

var control_data_form_complexe_return = function(response) {
    $.each(response.form_validation, function(index, obj) {
        $('input[name=' + index + ']').parent().parent('.form-group').addClass('has-error');
        $('input[name=' + index + ']').parent('div').children('.help').text(obj.required);
        $('select[name=' + index + ']').parent().parent('.form-group').addClass('has-error');
        $('select[name=' + index + ']').parent('div').children('.help').text(obj.required);
        $('textarea[name=' + index + ']').parent().parent('.form-group').addClass('has-error');
        $('textarea[name=' + index + ']').parent('div').children('.help').text(obj.required);
    });
}

var null_fn = function() {}

$(document).ready(function() {


    // $('[data-toggle="tooltip"]').tooltip({ trigger: $(this).attr('data-trigger') });
    // $('[data-toggle="selectpicker"]').selectpicker();

    // if (window.PerfectScrollbar && isExists('.perfect-scrollbar')) {
    //     $('.perfect-scrollbar:not(".tab-pane")').each(function() {
    //         new PerfectScrollbar(this, {
    //             suppressScrollX: true
    //         });
    //     });
    // }
    // $('.toggle').toggles({
    //     on: true,
    //     height: 22
    // });

    // /********************/

    $(document).ajaxStop(function() {

    });
    $(document).ajaxStart(function() {

    });

    jQuery('button').click(function() {
        var icon = jQuery(this).find('svg');
        if (icon.attr('data-icon') == 'caret-right') {
            icon.attr('data-icon', 'caret-down');
        } else if (icon.attr('data-icon') == 'caret-down') {
            icon.attr('data-icon', 'caret-right');
        }
    });

    $(document).on('keyup', 'input[data-require=true]', function(e) {
        if ($(this).val() != '') {
            if ($(this).parent().parent('.form-group').hasClass('has-error')) {
                $(this).parent().parent('.form-group').removeClass('has-error');
                $(this).parent('div').children('.help').empty();
            }
        }
    });

    $(document).on('change', 'input[data-require=true][type=date]', function(e) {
        if ($(this).val() != '') {
            if ($(this).parent().parent('.form-group').hasClass('has-error')) {
                $(this).parent().parent('.form-group').removeClass('has-error');
                $(this).parent('div').children('.help').empty();
            }
        }
    });

    $(document).on('keyup', 'textarea[data-require=true]', function(e) {
        if ($(this).val() != '') {
            if ($(this).parent().parent('.form-group').hasClass('has-error')) {
                $(this).parent().parent('.form-group').removeClass('has-error');
                $(this).parent('div').children('.help').empty();
            }
        }
    });

    $(document).on('change', 'select[data-require=true]', function(e) {
        if ($(this).val() != '') {
            if ($(this).parent().parent('.form-group').hasClass('has-error')) {
                $(this).parent().parent('.form-group').removeClass('has-error');
                $(this).parent('div').children('.help').empty();
            }
        }
    });

    $(document).on('keypress', '.chiffre', function(e) {
        var keyCode = e.which;
        var caracter = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 44, 32, 46];
        if (caracter.includes(keyCode) != true) {
            return false;
        }
    });

    $(document).on('keyup', '.maj', function(e) {
        $(this).val($(this).val().toUpperCase());
    });

    $(document).on('blur', '.capit', function(e) {
        $(this).val($(this).val().capitalize());
    });

    $(document).on('keypress', '.tel', function(e) {
        var keyCode = e.which;
        var caracter = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 32, 43, 53, 176];
        if (caracter.includes(keyCode) != true || $(this).val().length > 15) {
            return false;
        }
    });

    $(document).on('blur', '.required_form_primary', function(e) {
        if ($(this).val() != '') {
            $(this).parent('div').removeClass('has-error');
        }
    });

});

function insert_character_to_caracter(id_,text) {
    var txtarea = document.getElementById(id_);
    if (!txtarea) {
        return;
    }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false));
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart('character', -txtarea.value.length);
        ieRange.moveStart('character', strPos);
        ieRange.moveEnd('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}